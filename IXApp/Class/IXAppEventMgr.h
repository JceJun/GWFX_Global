//
//  IXAppEventMgr.h
//  IXApp
//
//  Created by mac on 2018/8/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#define MarkEvent(event)  [IXAppEventMgr markEventWithName:event]
@interface IXAppEventMgr : NSObject

+ (void)markEventWithName:(NSString *)name;

@end
