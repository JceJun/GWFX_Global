//
//  IXUserMgr.h
//  IXApp
//
//  Created by mac on 2018/8/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXSingleton.h"
@interface IXUserMgr : NSObject
SingletonH(IXUserMgr)

- (void)switchToDemoAccount:(NSInteger)swithType; // 切换至模拟账户,switchType为切换完后的操作，在AppDelegate中实现
- (void)switchToRealAccount:(NSInteger)swithType; // 切换至真实账户,switchType为切换完后的操作，在AppDelegate中实现

@end
