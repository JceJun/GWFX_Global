//
//  IXVisiterVC.m
//  IXApp
//
//  Created by Seven on 2017/9/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXVisiterVC.h"
#import <WebKit/WebKit.h>
#import "IXAlertVC.h"
#import "IXLoginMainVC.h"
#import "IXRegistStep1VC.h"

#import "IXNoResultView.h"
#import "IXLeftNavView.h"
#import "AppDelegate+UI.h"
#import "IXJSHelper.h"

#import "IXPhoneRegistVC.h"
#import "IXCpyconfig.h"

#define titleViewFrame CGRectMake(0, 0, kScreenWidth - 150, 40)

@interface IXVisiterVC ()
<
WKUIDelegate,
WKNavigationDelegate,
JSHelperDelegate,
ExpendableAlartViewDelegate
>



@property (nonatomic, strong) WKWebView         * webView;
@property (nonatomic, strong) IXJSHelper        * jsHelper;
@property (nonatomic, strong) WKUserContentController   * userContent;

@property (nonatomic, strong) UIProgressView    * progressV;
@property (nonatomic, strong) IXNoResultView    * alertView;

@property (nonatomic, strong) IXLeftNavView     * leftNavItem;
@property (nonatomic, strong) UIBarButtonItem   * backItem;
@property (nonatomic, strong) UILabel           * titleLab;
@property (nonatomic, strong) UIView    * btomV;

@property (nonatomic, copy) NSString    * metaUrl;

@end

@implementation IXVisiterVC

- (void)dealloc
{
    [self.webView removeObserver:self forKeyPath:@"title"];
    [self.webView removeObserver:self forKeyPath:@"loading"];
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"ixMarketPage"];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (hideTitle) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    self.navigationItem.titleView = self.titleLab;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];
    self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightItemWithImg:GET_IMAGE_NAME(@"webReload")
                                                                       target:self
                                                                          sel:@selector(reloadAction)
                                                                     aimWidth:55];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSForegroundColorAttributeName: MarketSymbolNameColor,
                                                                    NSFontAttributeName :PF_MEDI(15)
                                                                    };
    
    [self addWebView];
    [self.view addSubview:self.progressV];
    [self createBtomV];
    
    NSString    * urlStr  = @"http://m.gwfxglobal.com/app/en/index.html"; // PRD
    //    webHomeUrl = @"http://www.gwfxglobal.com/app/en/index.html";
    if ([CompanyName containsString:@"UAT"]) {
        urlStr = @"http://testm.gwfxglobal.com/app/en/index.html";  // UAT
    }
    
    NSString *userType = [IXDataProcessTools showCurrentAccountType];
    NSArray *arr = @[LocalizedString(@"游客"),LocalizedString(@"模拟"),LocalizedString(@"真实")];
    urlStr =[urlStr stringByAppendingFormat:@"?isLoginUser=%@",@([arr indexOfObject:userType])];
    
    
    NSURLRequest    * request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webView loadRequest:request];
    
}

- (void)addWebView
{
    [self.view addSubview:self.webView];
    
    [self.webView addObserver:self forKeyPath:@"title"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self forKeyPath:@"loading"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
}

- (void)leftItemClicked
{
    [self showAlert];
}

- (void)reloadAction
{
    [self.webView reload];
    [self showLoadFailedView:NO];
}

- (void)backAction
{
    if (_metaUrl.length && [_metaUrl containsString:@"http"]) {
        NSURL   * url = [NSURL URLWithString:_metaUrl];
        NSURLRequest    * request = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
        //显示头像
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];
    } else if ([self.webView canGoBack]) {
        [self.webView goBack];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];
    }
}

- (void)refreshLeftNavItem
{
    NSArray     * arr = self.webView.backForwardList.backList;
    
    if (arr.count){
        self.navigationItem.leftBarButtonItem = self.backItem;
    }else{
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];;
    }
    
    self.navigationItem.titleView.frame = titleViewFrame;
}

- (void)createBtomV
{
    _btomV = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight - kNavbarHeight - kTabbarHeight,
                                                               kScreenWidth,
                                                               kTabbarHeight)];
    _btomV.dk_backgroundColorPicker = DKNavBarColor;
    
    UIView  * topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
    topLine.dk_backgroundColorPicker = DKLineColor;
    [_btomV addSubview:topLine];
    
    CGFloat space = 5;
    CGFloat w = (kScreenWidth - 20) / 4;
    
    NSArray * titles = @[
                         LocalizedString(@"首页"),
                         LocalizedString(@"市场"),
                         LocalizedString(@"仓位"),
                         LocalizedString(@"资产")
                         ];
    NSArray * icon = @[
                       @"webHome_h",
                       @"marketHome_n",
                       @"postionHome_n",
                       @"assetHome_n"
                       ];
    
    for (int i = 0; i < 4; i ++) {
        CGFloat x = space/2 + (w + space)*i;
        UIImageView * imgV = [[UIImageView alloc] initWithImage:AutoNightImageNamed(icon[i])];
        imgV.frame =CGRectMake(x + (w - 22)/2, 7, 22, 22);
        [_btomV addSubview:imgV];
        
        UILabel * lab = [[UILabel alloc] initWithFrame:CGRectMake(x, 31, w, 12)];
        lab.font = PF_REGU(10);
        lab.textAlignment = NSTextAlignmentCenter;
        lab.text = titles[i];
        [_btomV addSubview:lab];

        
        UIButton    * button = [[UIButton alloc] initWithFrame:CGRectMake(x, 1, w, kTabbarHeight - 2)];
        button.backgroundColor = [UIColor clearColor];
        [_btomV addSubview:button];
        
        if (i == 0) {
            lab.textColor = MarketSymbolNameColor;
        } else {
            lab.textColor = MarketSymbolCodeColor;
            [button addTarget:self action:@selector(btn_showLogin) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    [self.view addSubview:_btomV];
}

- (void)btn_showLogin
{
    [self showAlert];
}


#pragma mark -
#pragma mark - IXJSHelper delegate

- (void)IXJSHelperPerformSelecter:(SEL)sel param:(id)param
{
    if ([self respondsToSelector:sel]) {
        [self performSelector:sel withObject:param afterDelay:0];
    }
}

bool    hideTitle;
- (void)hideTitle
{
    hideTitle = YES;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    CGFloat y = [[UIApplication sharedApplication] statusBarFrame].size.height;
    _progressV.frame = CGRectMake(0, y, kScreenWidth, 5);
    _webView.frame = CGRectMake(0, y - 20, kScreenWidth, kScreenHeight - kTabbarHeight - y + 20);
    _btomV.frame = CGRectMake(0, kScreenHeight - kTabbarHeight,
                              kScreenWidth,
                              kTabbarHeight);
    
    //修改frame后网页与其title会有20pt的偏移量，因此用此view覆盖，避免网页title上方显示网页内容
    y -= 20;
    UIView * v= [[UIView alloc] initWithFrame:CGRectMake(0, y, kScreenWidth, 20)];
    v.dk_backgroundColorPicker = DKNavBarColor;
    [self.view addSubview:v];
    
    [self.view bringSubviewToFront:_progressV];
}

#pragma mark -
#pragma mark - UIDelegate

//web页面探出警告框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptAlertPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(void))completionHandler
{
    completionHandler();
    NSLog(@" -- web view -- %s",__func__);
}

//web页面弹出确认框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptConfirmPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(BOOL result))completionHandler
{
    completionHandler(NO);
    NSLog(@" -- web view -- %s",__func__);
}

//web页面弹出输入框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt
    defaultText:(nullable NSString *)defaultText
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(NSString * _Nullable result))completionHandler
{
    completionHandler(@"input text");
    NSLog(@" -- web view -- %s",__func__);
}


#pragma mark -
#pragma mark - navigation delegate

- (void)webView:(WKWebView *)webView
decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if (!navigationAction.navigationType) {
        [webView loadRequest:navigationAction.request];
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    [self refreshLeftNavItem];
}

- (void)webView:(WKWebView *)webView
decidePolicyForNavigationResponse:(nonnull WKNavigationResponse *)navigationResponse
decisionHandler:(nonnull void (^)(WKNavigationResponsePolicy))decisionHandler
{
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView
didFailNavigation:(WKNavigation *)navigation
      withError:(NSError *)error
{
    NSLog(@" -- web view --  %s",__func__);
    [self refreshLeftNavItem];
}

//加载完成时调用
- (void)webView:(WKWebView *)webView
didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    [self refreshLeftNavItem];
    [self showLoadFailedView:NO];
//    _webView.frame = _webView.frame;
    
    NSString    * js = @"document.getElementsByTagName(\"meta\")['title'].getAttribute('content')";
    [self.webView evaluateJavaScript:js completionHandler:^(id _Nullable response, NSError * _Nullable error) {
        if ([response isKindOfClass:[NSString class]] &&
            [(NSString *)response containsString:@"http"]) {
            _metaUrl = response;
        }
    }];
}

//请求出错时调用
- (void)webView:(WKWebView *)webView
didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation
      withError:(NSError *)error
{
    NSURL    * str = error.userInfo[@"NSErrorFailingURLKey"];
    if ([str isKindOfClass:[NSURL class]] && [[UIApplication sharedApplication] canOpenURL:str]) {
        [[UIApplication sharedApplication] openURL:str];
    }
}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView
{
    [self showLoadFailedView: YES];
    NSLog(@" -- web view -- 页面内容加载失败");
}

#pragma mark -
#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat p = [change[@"new"] floatValue];
        [_progressV setProgress:p animated:YES];
        
        if (p == 1.0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [_progressV setProgress:0 animated:NO];
                _progressV.hidden = YES;
            });
        }else{
            _progressV.hidden = NO;
        }
    }
    else if ([keyPath isEqualToString:@"title"]){
        NSString    * t = change[@"new"];
        self.titleLab.text = t;
    }
}


#pragma mark -
#pragma mark - other
- (void)showLoadFailedView:(BOOL)show
{
    if (show) {
        self.alertView.alpha = 0;
        [self.view addSubview:self.alertView];
        
        [UIView animateWithDuration:0.2 animations:^{
            self.alertView.alpha = 1;
        }];
    }else{
        [self.alertView removeFromSuperview];
    }
}

- (void)showAlert
{
    IXAlertVC   * alert = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"温馨提示")
                                                   message:LocalizedString(@"请注册或登录后使用")
                                               cancelTitle:LocalizedString(@"注册")
                                                sureTitles:LocalizedString(@"登录")];
    alert.expendAbleAlartViewDelegate = self;
    [alert showView];
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (btnIndex == 0) {
//        [AppDelegate showRegist];
//        [self.navigationController pushViewController:[IXRegistStep1VC new] animated:NO];
        IXPhoneRegistVC *vc = [IXPhoneRegistVC new];
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
//        [AppDelegate showLoginMain];
        [self.navigationController pushViewController:[IXLoginMainVC new] animated:NO];
    }
    return YES;
}

#pragma mark -
#pragma mark - getter

- (UIBarButtonItem *)backItem
{
    if (!_backItem) {
        _backItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(backAction)];
    }
    return _backItem;
}

- (IXLeftNavView *)leftNavItem
{
    if (!_leftNavItem) {
        _leftNavItem = [[IXLeftNavView alloc] initWithTarget:self action:@selector(leftItemClicked)];
        _leftNavItem.typeLab.text = LocalizedString(@"游客");
    }
    return _leftNavItem;
}

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [IXCustomView createLable:titleViewFrame
                                        title:LocalizedString(@"首页")
                                         font:PF_MEDI(15)
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentCenter];
    }
    return _titleLab;
}

- (WKWebView *)webView{
    if (!_webView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        config.userContentController = [[WKUserContentController alloc] init];
        [config.userContentController addScriptMessageHandler:(id)self.jsHelper name:@"ixMarketPage"];
        config.allowsInlineMediaPlayback = YES;
        
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                               kScreenHeight - kNavbarHeight - kTabbarHeight)
                                      configuration:config];
        self.webView = _webView;
        _webView.clipsToBounds = YES;
        _webView.allowsBackForwardNavigationGestures = YES;
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.dk_backgroundColorPicker = DKNavBarColor;
        for (UIView * v in _webView.subviews) {
            v.dk_backgroundColorPicker = DKNavBarColor;
        }
    }
    
    return _webView;
}

- (IXJSHelper *)jsHelper {
    if (!_jsHelper) {
        _jsHelper = [[IXJSHelper alloc] initWithDelegate:(id)self vc:self msgName:@"ixMarketPage"];
    }
    return _jsHelper;
}

- (UIProgressView *)progressV
{
    if (!_progressV) {
        _progressV = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressV.dk_trackTintColorPicker = DKColorWithRGBs(0xE2E9F1, 0x303b4d);
        _progressV.dk_progressTintColorPicker = DKColorWithRGBs(0x11B873, 0x21ce99);
        _progressV.frame = CGRectMake(0, 0, kScreenWidth, 5);
    }
    
    return _progressV;
}

- (IXNoResultView *)alertView
{
    if (!_alertView) {
        _alertView = [IXNoResultView noResultViewWithFrame:CGRectMake(0, 20,
                                                                      kScreenWidth,
                                                                      kScreenHeight - kTabbarHeight - 20)
                                                     image:AutoNightImageNamed(@"webRequestFail")
                                                     title:LocalizedString(@"请重新刷新")];
        _alertView.dk_backgroundColorPicker = DKTableColor;
        _alertView.alpha = 0;
    }
    
    return _alertView;
}

@end
