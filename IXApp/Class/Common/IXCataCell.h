//
//  IXCataCell.h
//  IXApp
//
//  Created by Bob on 2016/12/17.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selectedCata)(NSInteger btnTag);

@interface IXCataCell : UITableViewCell

@property (nonatomic, strong) selectedCata selectedCataInfo;

- (void)setNameArr:(NSArray *)arr;

- (void)setSelectedBtnWithTag:(NSInteger)tag;

@end
