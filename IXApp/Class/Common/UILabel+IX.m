//
//  UILabel+IX.m
//  IXApp
//
//  Created by Seven on 2017/11/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "UILabel+IX.h"

@implementation UILabel (IX)

+ (instancetype)labelWithFrame:(CGRect)frame
                          text:(NSString *)text
                      textFont:(UIFont *)font
                     textColor:(DKColorPicker)color
{
    UILabel * lab = [[UILabel alloc] initWithFrame:frame];
    lab.font = font;
    lab.dk_textColorPicker = color;
    lab.text = text;
    return lab;
}

+ (instancetype)labelWithFrame:(CGRect)frame
                          text:(NSString *)text
                      textFont:(UIFont *)font
                     textColor:(DKColorPicker)color
                 textAlignment:(NSTextAlignment)alignment
{
    UILabel * lab = [[UILabel alloc] initWithFrame:frame];
    lab.font = font;
    lab.dk_textColorPicker = color;
    lab.textAlignment = alignment;
    lab.text = text;
    return lab;
}

// ---------

+ (instancetype)labelWithText:(NSString *)text
                     textFont:(UIFont *)font
                    textColor:(DKColorPicker)color
{
    UILabel * lab = [UILabel new];
    lab.font = font;
    lab.dk_textColorPicker = color;
    lab.text = text;
    return lab;
}

+ (instancetype)labelWithText:(NSString *)text
                     textFont:(UIFont *)font
                    textColor:(DKColorPicker)color
                textAlignment:(NSTextAlignment)alignment
{
    UILabel * lab = [UILabel new];
    lab.font = font;
    lab.dk_textColorPicker = color;
    lab.textAlignment = alignment;
    lab.text = text;
    return lab;
}


@end
