//
//  IXSweetTitleV.m
//  IXApp
//
//  Created by Magee on 2016/12/17.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSweetTitleV.h"

@interface IXSweetTitleV ()

@property (nonatomic,strong)UILabel *labelV;
@property (nonatomic,strong)UIActivityIndicatorView *idctr;

@property (nonatomic,strong) UIButton *tapBtn;

@property (nonatomic,assign)IXSweetShowType curType;

@end

@implementation IXSweetTitleV

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _curType = IXSweetShowCnted;
        self.tapBtn.enabled = YES;
    }
    return self;
}


- (UIButton *)tapBtn
{
    if ( !_tapBtn ) {
        _tapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _tapBtn.frame = CGRectMake( 0, 0, 200, 44);
        [_tapBtn addTarget:self action:@selector(tapTaped) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_tapBtn];
    }
    return _tapBtn;
}

- (UILabel *)labelV
{
    if (!_labelV) {
        _labelV = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 18)];
        _labelV.font = PF_MEDI(15);
        _labelV.dk_textColorPicker = DKColorWithRGBs(0x232935, 0xe9e9ea);
        _labelV.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_labelV];
    }
    return _labelV;
}

- (UIActivityIndicatorView *)idctr
{
    if (!_idctr) {
        _idctr = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _idctr.color = MarketSymbolNameColor;
        _idctr.hidesWhenStopped = YES;
        [self addSubview:_idctr];
    }
    
    UIActivityIndicatorViewStyle  style = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    _idctr.activityIndicatorViewStyle = style;
    
    return _idctr;
}

- (void)setDefaultTitle:(NSString *)defaultTitle
{
    _defaultTitle = defaultTitle;
    [self staticTitle:_defaultTitle];
}

- (void)staticTitle:(NSString *)content
{
    [_idctr stopAnimating];
    [self.labelV setText:content];
    [self.labelV sizeToFit];
    CGFloat lbWith = _labelV.frame.size.width;
    CGFloat sw = self.frame.size.width;
    [self.labelV setFrame:CGRectMake((sw - lbWith)/2, 13, lbWith, 18)];
}

- (void)dynamicTitle:(NSString *)content
{
    [self.labelV setText:content];
    [self.labelV sizeToFit];
    CGFloat lbWith = _labelV.frame.size.width;
    CGFloat sw = self.frame.size.width;
    [self.labelV setFrame:CGRectMake((sw - lbWith)/2 + 12.5, 13, lbWith, 18)];
    [self.idctr setFrame:CGRectMake(GetView_MinX(_labelV)-5-20, 12, 20, 20)];
    [self.idctr startAnimating];
}

- (void)startWitting
{
    if (_curType == IXSweetShowCnted) {
        [self.labelV setText:_defaultTitle];
        [self.labelV sizeToFit];
        CGFloat lbWith = self.labelV.frame.size.width;
        CGFloat sw = self.frame.size.width;
        [self.labelV setFrame:CGRectMake((sw - lbWith)/2 + 12.5, 13, lbWith, 18)];
        [self.idctr setFrame:CGRectMake(GetView_MinX(_labelV)-5-20, 12, 20, 20)];
        [self.idctr startAnimating];
        [self performSelector:@selector(stopWitting) withObject:nil afterDelay:15];
    }
}

- (void)stopWitting
{
    if (_curType == IXSweetShowCnted) {
        [self.idctr stopAnimating];
        CGFloat lbWith = _labelV.frame.size.width;
        CGFloat sw = self.frame.size.width;
        [self.labelV setFrame:CGRectMake((sw - lbWith)/2, 13, lbWith, 18)];
    }
}

- (void)showNetStatus:(IXSweetShowType)showType
{
    _curType = showType;
    switch (showType) {
        case IXSweetShowDisCnt: {
            [self staticTitle:LocalizedString(@"未连接")];
            _tapBtn.enabled = NO;
        }
            break;
        case IXSweetShowCnting: {
            [self dynamicTitle:LocalizedString(@"连接中")];
            _tapBtn.enabled = NO;
        }
            break;
        case IXSweetShowCnted: {
            [self staticTitle:LocalizedString(_defaultTitle)];
            _tapBtn.enabled = NO;
        }
            break;
        case IXSweetShowMaxTried: {
            [self staticTitle:LocalizedString(@"未连接(点击重连)")];
            _tapBtn.enabled = YES;
        }
            break;
        default:
            break;
    }
}

- (void)tapTaped
{
    DLog(@"##############################");
    if (self.reconnectTaped) {
        self.reconnectTaped();
    }
    _tapBtn.enabled = NO;
}

@end
