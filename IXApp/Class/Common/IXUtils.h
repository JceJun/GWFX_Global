//
//  IXUtils.h
//  PM_GTS2
//
//  Created by amanda on 15-4-29.
//  Copyright (c) 2015年 GW. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 工具类 只有+方法  数据格式转换等convert
 */
@interface IXUtils : NSObject

//图片名
+ (NSString *)tranImageName:(NSString *)str;

#pragma mark - DataConvertUtils
/**转化char到串*/
+ (NSString *)convertCharToNSString:(const char *)val;

/**转化char--GBK*/
+ (NSString *)convertCharToGBKNSString:(char *)val ;
/**转化char--BIG5*/
+ (NSString *)convertCharToBIG5NSString:(char *)val ;

#pragma mark-通用方法
/**验证手机号*/
+ (BOOL)isValidateMobile:(NSString *)mobile;

/**转化string到char*/
+ (const char *)convertNSStringToChar:(NSString *)val;

/**按精度转化double到串 */
+ (NSString *)convertDoubleToNsstring:(double)val precision:(NSInteger)n;

/**按精度转化double*/
+ (double)convertDoubleToDouble:(double)val precision:(NSInteger)n;
/**转化double*/
+ (double)convertDoubleToDouble2:(double)val;
/**转化double*/
+ (double)convertDoubleToDouble3:(double)val;

/**按精度 比较两个double大小  val1>val2 return 1;  val1<val2 return -1;  val1=val2 return 0;*/
+ (int)compare:(double)val1 With:(double)val2 precision:(NSInteger)n;

/**得到报价时间格式HH:MM:SS*/
+(NSString*)getFormatTime:(time_t)time;

/**得到时间戳*/
+(UInt64)getTimeStamp;

+ (UILabel *)createLblWithFrame:(CGRect)frame
                       WithFont:(UIFont *)font
                      WithAlign:(NSTextAlignment)align
                     wTextColor:(NSUInteger)wHexColor
                     dTextColor:(NSUInteger)dHexColor;
@end
