//
//  IXClickTableVCell.m
//  ClickAnimation
//
//  Created by Magee on 2017/6/28.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "IXClickTableVCell.h"

@interface IXClickTableVCell ()<CAAnimationDelegate>
{
    BOOL isAnimating;
}

@property (nonatomic,assign)CGPoint curPoint;
@property (nonatomic,strong)CALayer *spreadLayer;
@end

@implementation IXClickTableVCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        isAnimating = NO;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    if (!isAnimating) {
        UITouch *touch = [touches anyObject];
        self.spreadLayer.position = CGPointMake([touch locationInView:self.contentView].x,self.contentView.frame.size.height/2.f);
        
        [self addSpreadEffect];
    }
}

- (CALayer *)spreadLayer
{
    if (!_spreadLayer) {
        _spreadLayer = [CALayer layer];
        _spreadLayer.bounds = CGRectMake(0,0, 1000, 1000);
        _spreadLayer.cornerRadius = 500;
        _spreadLayer.backgroundColor = [UIColor colorWithRed:144.0/255.0 green:157.0/255.0 blue:166.0/255.0 alpha:1.0].CGColor;
        [self.contentView.layer addSublayer:_spreadLayer];
        self.contentView.layer.masksToBounds = YES;

    }
    return _spreadLayer;
}

- (void)addSpreadEffect
{
    CAMediaTimingFunction * defaultCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    CAAnimationGroup * animationGroup = [CAAnimationGroup animation];
    animationGroup.duration = .3;
    animationGroup.repeatCount = 0;
    animationGroup.removedOnCompletion = NO;
    animationGroup.fillMode=kCAFillModeForwards;
    animationGroup.timingFunction = defaultCurve;
    animationGroup.delegate = self;
 
    CAKeyframeAnimation *scaleAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale.xy"];
    scaleAnimation.values = @[@0.1, @0.9,@1];
    scaleAnimation.keyTimes = @[@0, @0.2,@1];
    scaleAnimation.duration = .3;

    CAKeyframeAnimation *opacityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.duration = .3;
    opacityAnimation.values = @[@0.4, @0.2,@0];
    opacityAnimation.keyTimes = @[@0, @0.2,@1];
    
    animationGroup.animations = @[scaleAnimation, opacityAnimation];
    [self.spreadLayer addAnimation:animationGroup forKey:@"pulse"];
}

- (void)animationDidStart:(CAAnimation *)anim
{
    isAnimating = YES;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (flag) {
        [self.spreadLayer removeFromSuperlayer];
        self.spreadLayer = nil;
        isAnimating = NO;
    }
}

@end
