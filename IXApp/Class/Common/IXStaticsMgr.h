//
//  IXStaticsMgr.h
//  ixApp
//
//  Created by Seven on 2017/4/12.
//  Copyright © 2017年 Seven. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXStaticsMgr : NSObject

+ (IXStaticsMgr *)shareInstance;


/**
 进入页面

 @param vc 当前controller
 */
- (void)enterPage:(id)vc;

/**
 k线类型更新

 @param index 类型索引
 */
- (void)updateKlineType:(NSInteger)index;

/**
 k线指标更新

 @param section 主类为0，副类为1
 @param index 子类索引
 */
- (void)updateKlineIndexWithSection:(NSInteger)section index:(NSInteger)index;

/** 开户失败 */
- (void)buildAccountFailure;

/**
 更新行情类别

 @param cataId cata id
 */
- (void)changeSymbolPageWithCataID:(NSInteger)cataId;

/** 开始登录trade */
- (void)beginLoginTrade;

/** 登录trade成功／登录失败 */

/**
 登录trade结果上传

 @param success 成功与否
 @param code 登录失败错误码
 */
- (void)endLoginTrade:(BOOL)success errorCode:(uint32_t)code;


/** 激活设备统计 */
- (void)activateDevice;
@end
