//
//  GTSUtils.m
//  PM_GTS2
//
//  Created by amanda on 15-4-29.
//  Copyright (c) 2015年 GW. All rights reserved.
//

#import "IXUtils.h"
#import "YYText.h"
@implementation IXUtils

//图片名
+ (NSString *)tranImageName:(NSString *)str
{
    if ( [BoLanKey isEqualToString:NAMECN] ) {
        return str;
    }
    
    if ( [BoLanKey isEqualToString:NAMEEN] ) {
        return [NSString stringWithFormat:@"%@_en",str];
    }
    
    return [NSString stringWithFormat:@"%@_tw",str];
}

#pragma mark -  DataConvertUtils
+ (NSString *)convertCharToNSString:(const char *)val
{
    return [[NSString alloc] initWithCString:val encoding:NSASCIIStringEncoding];
}

+ (NSString *)convertCharToGBKNSString:(char *)val
{
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSString* str = [NSString stringWithCString:val  encoding:gbkEncoding];
    return str;
}

+ (NSString *)convertCharToBIG5NSString:(char *)val
{
    NSStringEncoding big5Encoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5);
    NSString *str = [NSString stringWithCString:val encoding:big5Encoding];
    return str;
}

+ (const char *)convertNSStringToChar:(NSString *)val
{
    if(val == nil){
        val = @"";
    }
    return [val cStringUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)convertDoubleToNsstring:(double)val precision:(NSInteger)n
{
    val = [self convertDoubleToDouble:val precision:n];
    NSString *res = @"0";
    switch (n) {
        case 0:
            res = [NSString stringWithFormat:@"%.0f",val];
            break;
        case 1:
            res = [NSString stringWithFormat:@"%.1f",val];
            break;
        case 2:
            res = [NSString stringWithFormat:@"%.2f",val];
            break;
        case 3:
            res = [NSString stringWithFormat:@"%.3f",val];
            break;
        case 4:
            res = [NSString stringWithFormat:@"%.4f",val];
            break;
        case 5:
            res = [NSString stringWithFormat:@"%.5f",val];
            break;
        case 6:
            res = [NSString stringWithFormat:@"%.6f",val];
            break;
        case 7:
            res = [NSString stringWithFormat:@"%.7f",val];
            break;
        default:
            res = [NSString stringWithFormat:@"%.f",val];
            break;
    }
    return res;
}

+ (double)convertDoubleToDouble:(double)val precision:(NSInteger)n
{
    float fBase = 10.0f;
    
    double dbadd;
    if ( val>=0 )
    {
        dbadd = 0.501/**pow(fBase, 2-nPow)*/;
    }
    else
    {
        dbadd =  -0.501/**pow(fBase, 2-nPow)*/;
    }
    
    int64_t i = (int64_t)(val* pow(fBase,n) + dbadd);
    double res =  (double)(i*(1/pow(fBase,n)));
    
    if ( [IXUtils compare:res With:0 precision:n] == 0 ) {
        //为了避免-0.00
        res = 0.f;
    }
    return res;
}

+ (double)convertDoubleToDouble2:(double)val
{
    return [self convertDoubleToDouble:val precision:2];
}

+ (double)convertDoubleToDouble3:(double)val
{
    return [self convertDoubleToDouble:val precision:3];
}

+ (int)compare:(double)val1 With:(double)val2 precision:(NSInteger)n
{
    double res = val1 - val2;
    double pre = 0.5 / pow(10, n);
    if (res > pre) {
        return 1;
    } else if (res < -pre) {
        return -1;
    } else {
        return 0;
    }
}

+ (BOOL)isValidateMobile:(NSString *)mobile
{
    //手机号以13，15，17, 18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9])|(17[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

+ (char)bytes2Char:(Byte *)b :(int32_t)pos {
    char val ;
    memcpy(&val, b + pos, sizeof(char));
    return val;
}

+ (char *)bytes2String:(Byte *)b :(int32_t)pos :(int32_t)length {
    
    char *buffer = NULL;
    buffer = (char *)malloc(length + 1);
    memcpy(buffer, b + pos, length);
    return buffer;
}

+ (int)bytes2StringZlen:(Byte *)b :(int)pos{
    int i;
    for (i = pos; ; i++) {
        if (b[i] == 0)
            break;
    }
    return i - pos + 1;
}


+ (NSString *)bytes2StringZ:(Byte *)b :(int)pos :(int)len {
    if (len <= 1)
        return nil;
    return [[NSString alloc] initWithBytes:b + pos length:len - 1 encoding:NSUTF8StringEncoding];
}

+ (NSString*)getFormatTime:(time_t)time
{
    struct tm * timeinfo;
    timeinfo = localtime(&time);
    int hour = timeinfo->tm_hour;
    
    NSString *strhour = @"";
    if(hour<10) {
        strhour = [NSString stringWithFormat:@"0%d",hour];
    } else {
        strhour = [NSString stringWithFormat:@"%d",hour];
    }
    
    int min = timeinfo->tm_min;
    
    NSString *strmin = @"";
    if(min<10) {
        strmin = [NSString stringWithFormat:@"0%d",min];
    } else {
        strmin = [NSString stringWithFormat:@"%d",min];
    }
    
    int sec = timeinfo->tm_sec;
    NSString *strsec = @"";
    if(sec<10) {
        strsec = [NSString stringWithFormat:@"0%d",sec];
    } else {
        strsec = [NSString stringWithFormat:@"%d",sec];
    }
    NSString *strformat = [NSString stringWithFormat:@"%@:%@:%@",strhour,strmin,strsec];
    return strformat;
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

+(UInt64)getTimeStamp
{
    uint64_t time = 0;
    
    //    if ([GTSDataManager shareInstance].isTradeServer && [GTSDataManager shareInstance].tradeServerTime) {
    //        DLog(@"取交易服务器时间");
    //        uint64_t difftime = [GTSDataManager shareInstance].tradeServerTime.iPhonetime - [GTSDataManager shareInstance].tradeServerTime.serverTime;
    //        time =  [[NSDate date] timeIntervalSince1970] + difftime;
    //    } else if ([GTSDataManager shareInstance].isQuoteServer && [GTSDataManager shareInstance].quoteServerTime) {
    //        DLog(@"取行情服务器时间");
    //        GTSServerTimeModel *serverTime = [GTSDataManager shareInstance].quoteServerTime;
    //         uint64_t difftime = serverTime.iPhonetime - serverTime.UUTime;
    //        time = [[NSDate date] timeIntervalSince1970] + difftime;
    //    }else {
    ////        DLog(@"取手机本地时间");
    //        time = [[NSDate date] timeIntervalSince1970];
    //    }
    //
    
    return time;
}

+ (UILabel *)createLblWithFrame:(CGRect)frame
                       WithFont:(UIFont *)font
                      WithAlign:(NSTextAlignment)align
                     wTextColor:(NSUInteger)wHexColor
                     dTextColor:(NSUInteger)dHexColor
{
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    label.dk_textColorPicker = DKColorWithRGBs(wHexColor, dHexColor);
    label.textAlignment = align;
    return label;
}

@end
