//
//  IXSymbolCataView.h
//  IXApp
//
//  Created by bob on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  选中行数回调
 *
 *  @param indexPath 选择的行数
 */
typedef void(^selectIndexPath)(NSIndexPath *indexPath);

/**
 *  滑动定位回调
 *
 *  @param page 滑动到的页数
 */
typedef void(^currentPage)(NSInteger page);

@interface IXSymbolCataView : UIView

/**
 *  点击回调
 */
@property (nonatomic, copy) selectIndexPath selectedIndexPath;

/**
 *  当前选项
 */
@property (nonatomic, copy) currentPage choosePage;

/**
 *  视图高度
 */
@property (nonatomic, assign) CGFloat cellHeight;

/**
 *  产品宽度
 */
@property (nonatomic, assign) CGFloat itemWidth;

/**
 *  定位到特定行
 *
 *  @param page 行数
 */
- (void)setOffsetPage:(NSInteger)page;

/**
 *  初始化产品类别视图
 *
 *  @param frame   视图的坐标
 *  @param cataArr 产品类别对应的控制器（VC）数组
 *
 *  @return 产品类别视图
 */
- (id)initWithFrame:(CGRect)frame WithCataAry:(NSArray *)cataArr;

/**
 *  刷新数据
 *
 *  @param dataSource 数据源
 */
- (void)resetDataSource:(NSArray *)dataSource;


@end
