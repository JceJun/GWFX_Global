//
//  CAGradientLayer+IX.h
//  IXApp
//
//  Created by Seven on 2017/12/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CAGradientLayer (IX)
/**
 创建阴影
 
 @param frame 阴影frame
 @param colors 颜色值
 @return 阴影layer
 */
+ (CAGradientLayer *)topShadowWithFrame:(CGRect)frame colors:(NSArray *)colors;
+ (CAGradientLayer *)btomShadowWithFrame:(CGRect)frame colors:(NSArray *)colors;

+ (CAGradientLayer *)topShadowWithFrame:(CGRect)frame dkcolors:(NSArray <DKColorPicker>*)colors;
+ (CAGradientLayer *)btomShadowWithFrame:(CGRect)frame dkcolors:(NSArray <DKColorPicker>*)colors;

@end
