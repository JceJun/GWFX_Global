//
//  IXPublicTool.h
//  IXApp
//
//  Created by Magee on 2017/5/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXPublicTool : NSObject

+ (NSString *)decimalNumberWithCGFloat:(CGFloat)number digit:(int)digit;

@end
