//
//  IXRateCaculate.m
//  IXApp
//
//  Created by Bob on 2017/1/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRateCaculate.h"
#import "IXDBGlobal.h"
#import "IXDBSymbolMgr.h"
#import "IXDBSymbolCataMgr.h"
#import "IXDBAccountGroupSymCataMgr.h"
#import "IXDBG_SMgr.h"
#import "IXDBG_S_CataMgr.h"

#import "IXAccountBalanceModel.h"
#import "IxItemGroupSymbolCata.pbobjc.h"

@implementation IXRateCaculate

//优先判断（groupid,symbolid）下的手续费
//没有查找（groupid,symbolCateid）手续费为百万分之一
//(type == 1 && status == 0)才是有效值
+ (double)queryComissionWithSymbolId:(NSInteger)symbolId WithSymbolCataId:(NSInteger)cataId
{
    NSInteger groupId = [IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid;
    //查询commissionType(合约价值还是手数)
    NSDictionary *dic1 = [IXDBG_S_CataMgr queryG_S_CataByAccountGroupId:groupId symbolCataId:cataId];
    if (dic1) {
        [IXAccountBalanceModel shareInstance].commissionType = [dic1[kCommissionType] integerValue];
    }else{
        [IXAccountBalanceModel shareInstance].commissionType = item_group_symbol_cata_ecommission_type_ByValue;
    }
    
    //从item_group_symbol取佣金
    //从item_group_symbol_cata取佣金
    NSDecimalNumber *commission = [NSDecimalNumber decimalNumberWithString:@"0"];
    NSDictionary *dic = [IXDBG_SMgr queryG_SByAccountGroupId:groupId symbolId:symbolId];
    if (dic) {
        commission = [NSDecimalNumber decimalNumberWithString:dic[kCommission]];
    } else {
        dic = [IXDBG_S_CataMgr queryG_S_CataByAccountGroupId:groupId symbolCataId:cataId];
        if (dic) {
            commission = [NSDecimalNumber decimalNumberWithString:dic[kCommission]];
        }
    }
    
    //合约价值算需要除以1000000
    if ([commission doubleValue] != 0 &&
        [IXAccountBalanceModel shareInstance].commissionType == 0) {
        commission = [commission decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"1000000"]];
    }
    
    return [commission doubleValue];
}

+ (NSDecimalNumber *)caculateMayDealPriceWithVolume:(long)volume
                                            WithDir:(item_order_edirection)dir
                                          WithQuote:(IXQuoteM *)quote
                                         WithDigits:(NSInteger)digit
{
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:@"0"];
  
    if( volume <= 0 )
        return price;
    
    NSArray *volArr;
    NSArray *prcArr;
    
    if ( dir == item_order_edirection_DirectionBuy ) {
        volArr = quote.BuyVol;
        prcArr = quote.BuyPrc;
    }else{
        volArr = quote.SellVol;
        prcArr = quote.SellPrc;
    }
    
    //数据异常直接返回0
    if ( !volArr || !prcArr || volArr.count == 0 || prcArr.count == 0 ) {
        return [NSDecimalNumber decimalNumberWithString:@"0"];
    }
    
    //根据方向计算预计成交价
    NSDecimalNumber *tmpVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%ld",volume]];
    for ( NSInteger i = 0; i < volArr.count ; i++ ) {
        if ( prcArr.count < (i + 1) || volume <= 0 ) {
            break;
        }
        
        NSDecimalNumber *curVol = [NSDecimalNumber decimalNumberWithString:volArr[i]];
        NSDecimalNumber *curPrc = [NSDecimalNumber decimalNumberWithString:prcArr[i]];

        if ( volume >= [curVol doubleValue] ) {
            price = [price decimalNumberByAdding:[curVol decimalNumberByMultiplyingBy:curPrc]];
            volume -= [curVol doubleValue];
        }else{
            price = [price decimalNumberByAdding:[[NSDecimalNumber decimalNumberWithString:
                                                   [NSString stringWithFormat:@"%ld",volume]] decimalNumberByMultiplyingBy:curPrc]];
            volume = 0;
        }
    }
    
    if ( volume > 0 ) {
        NSDecimalNumber *relVol = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%ld",volume]];
        NSDecimalNumber *relPrc = [NSDecimalNumber decimalNumberWithString:[prcArr lastObject]];
        price = [price decimalNumberByAdding:[relVol decimalNumberByMultiplyingBy:relPrc]];

    }
    
    return [price decimalNumberByDividingBy:tmpVolume withBehavior:[self handlerPriceWithDigit:digit]];
}



+ (NSDecimalNumberHandler *)handlerPriceWithDigit:(NSInteger)digit
{
    NSDecimalNumberHandler *roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:digit
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
    
    return roundingBehavior;
}

+ (NSString *)symbolTradeState:(IXSymbolTradeState)state
{
    switch ( state ) {
        case IXSymbolTradeStateNormal:
            return LocalizedString(@"交易中");
            break;
        case IXSymbolTradeStateBuyNormal:
            return LocalizedString(@"交易中");
            break;
        case IXSymbolTradeStateSellNormal:
            return LocalizedString(@"交易中");
            break;
        case IXSymbolTradeStateCloseOnly:
            return LocalizedString(@"仅平仓");
            break;
        case IXSymbolTradeStateNoSchedule:
            return LocalizedString(@"休市中");
            
        case IXSymbolTradeStateDisEnable:
            return LocalizedString(@"未启用");
        case IXSymbolTradeStateExpiry:
            return LocalizedString(@"已过期");
        default:
            return LocalizedString(@"休假中");
            break;
    }
}
@end
