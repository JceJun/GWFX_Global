//
//  IXSysConfig.h
//  IXApp
//
//  Created by bob on 16/11/1.
//  Copyright © 2016年 IX. All rights reserved.
//  藏青色: 0x242a36 > 0x4c6072 > 0x8395a4  >99abba > 0xe2eaf2 > 0xfafcfe  天蓝色 0x242a36

#ifndef IXSysConfig_h
#define IXSysConfig_h

#endif /* IXSysConfig_h */

//#define DEV 1

/*  ***********   UI   ************* */

#define kNavbarHeight   (44 + [[UIApplication sharedApplication] statusBarFrame].size.height)
#define kTabbarHeight   (49 + kBtomMargin)
#define kScreenBound    [[UIScreen mainScreen] bounds]
#define kScreenWidth    [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight   [[UIScreen mainScreen] bounds].size.height
#define kLineHeight     1/[[UIScreen mainScreen] scale]
#define kBtomMargin     [UIApplication sharedApplication].keyWindow.safe_btomMargin
#define kTopMargin      [UIApplication sharedApplication].keyWindow.safe_topMargin

#define KEYWINDOW [UIApplication sharedApplication].keyWindow

#define ktableFooterFrame   CGRectMake(0, 0, kScreenWidth, kBtomMargin)

#define kIsiPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define kIs_iPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define VIEW_W(_VIEW_)    (_VIEW_.frame.size.width)
#define VIEW_H(_VIEW_)    (_VIEW_.frame.size.height)
#define VIEW_X(_VIEW_)    (_VIEW_.frame.origin.x)
#define VIEW_Y(_VIEW_)    (_VIEW_.frame.origin.y)

#define GetView_MaxX(_VIEW_) CGRectGetMaxX(_VIEW_.frame)
#define GetView_MinX(_VIEW_) CGRectGetMinX(_VIEW_.frame)
#define GetView_MaxY(_VIEW_) CGRectGetMaxY(_VIEW_.frame)
#define GetView_MinY(_VIEW_) CGRectGetMinY(_VIEW_.frame)

/*  *********** Colors  *************  */
#define RGB(r, g, b) \
[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]

#define HexRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define HexRGBA(rgbValue,alp) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:alp]


#define UIColorFromRGB(r, g, b) \
[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]

#define UIColorHexFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorWithHex(rgbValue,alp) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:alp]

#define MarketSymbolNameColor  UIColorHexFromRGB(0x4c6072)
#define MarketSymbolCodeColor  UIColorHexFromRGB(0xa7adb5)
#define MarketGrayPriceColor   UIColorHexFromRGB(0x99abba)


#define MarketGrayShowColor    UIColorHexFromRGB(0xa3aebb)

//asdfasdadsaf 这个颜色可以全部去掉CELL的底线
#define CellSepLineColor       UIColorHexFromRGB(0xdae6f0)
#define MarketCellSepColor     UIColorHexFromRGB(0xe2eaf2)

#define CellBackgroundColor    UIColorHexFromRGB(0xfafcfe)
#define SearchPlaceholderColor UIColorHexFromRGB(0xe2e9f1)
#define ViewBackgroundColor    UIColorHexFromRGB(0xf1f6fa)

// ----- 夜间模式颜色宏定义 ----
#define DKViewColor     DKColorWithRGBs(0xfafcfe, 0x262f3e)
#define DKNavBarColor   DKColorWithRGBs(0xffffff, 0x262f3e)
#define DKTableColor    DKColorWithRGBs(0xf1f6fa, 0x242a36)
#define DKTableHeaderColor  DKColorWithRGBs(0xfafcfe, 0x242a36)

#define DKGrayTextColor     DKColorWithRGBs(0x4c6072, 0x8395a4)
#define DKGrayTextColor1    DKColorWithRGBs(0xa7adb5, 0x8395a4)

#define DKCellTitleColor    DKColorWithRGBs(0x4c6072, 0xe9e9ea)
#define DKCellContentColor  DKColorWithRGBs(0x99abba, 0x8395a4)
#define DKTextFBorderColor  DKColorWithRGBs(0x99abba, 0x303b4d)

#define DKLineColor     DKColorWithRGBs(0xe2eaf2, 0x242a36)
#define DKLineColor1    DKColorWithRGBs(0xe2eaf2, 0x262f3e)
#define DKLineColor2    DKColorWithRGBs(0xcfdae3, 0x4c6072)
#define DKLineColor3    DKColorWithRGBs(0xe2e9f1, 0x303b4d)

#define KeyBoardAppearance [IXUserInfoMgr shareInstance].isNightMode ? \
UIKeyboardAppearanceDark:UIKeyboardAppearanceDefault

#define AutoNightImageNamed(originName) \
[IXUserInfoMgr shareInstance].isNightMode ? \
[UIImage imageNamed:[originName stringByAppendingString:@"_D"]] :\
[UIImage imageNamed:originName]

#define AutoNightColor(wHexColor,dHexColor) \
[IXUserInfoMgr shareInstance].isNightMode ? \
UIColorHexFromRGB(dHexColor) : UIColorHexFromRGB(wHexColor)


#define LocalizedString(key) [IXLocalizationModel getLocalizationWordByKey:key]
#define BoLanKey  [IXLocalizationModel getBoLanuageKey]

//夜间模式
#define DKImageNames(wImgName,dImgName)     DKImagePickerWithNames(wImgName,dImgName)
#define DKImageWithImgs(wImg,dImg)          DKImagePickerWithImages(wImg,dImg)
#define DKColorWithRGBs(wHexColor,dHexColor)    DKColorPickerWithRGB(wHexColor,dHexColor)

/*******   市场模块文字和背景色      **********/
#define SettingColor [[NSUserDefaults standardUserDefaults] objectForKey:@"COLORSETTING"]
#define SettingRed   [SettingColor isEqualToString:@"RED"]
//文字红涨绿跌（没有设置或者设置的是红色才按目前的状态取，否则取反）
#define MarketRedPriceColor \
([IXUserInfoMgr shareInstance].settingRed ?\
UIColorHexFromRGB(0xff4653) :\
UIColorHexFromRGB(0x11b873))

//日间颜色
#define mRedPriceColor ([IXUserInfoMgr shareInstance].settingRed ? 0xff4653 : 0x11b873)
//夜间颜色
#define mDRedPriceColor ([IXUserInfoMgr shareInstance].settingRed ? 0xff4d2d : 0x21ce99)

#define MarketGreenPriceColor \
([IXUserInfoMgr shareInstance].settingRed ?\
UIColorHexFromRGB(0x11b873) :\
UIColorHexFromRGB(0xff4653))

//日间颜色
#define mGreenPriceColor ((!SettingColor || SettingRed) ? 0x11b873 : 0xff4653)
//夜间颜色
#define mDGreenPriceColor ((!SettingColor || SettingRed) ? 0x21ce99 : 0xff4d2d)

//背景色红涨绿跌（没有设置或者设置的是红色才按目前的状态取，否则取反）
#define ThumbPriceRedColor \
([IXUserInfoMgr shareInstance].settingRed ?\
UIColorHexFromRGB(0xfcf1f2) :\
UIColorHexFromRGB(0xe6f7f0))

#define tPriceRedColor  (!SettingColor || SettingRed ? 0xfcf1f2 : 0xe6f7f0)
#define tDPriceRedColor  (!SettingColor || SettingRed ? 0x2c2b35 : 0x243035)

#define ThumbPriceGreenColor \
([IXUserInfoMgr shareInstance].settingRed ?\
UIColorHexFromRGB(0xe6f7f0) :\
UIColorHexFromRGB(0xfcf1f2))

#define tPriceGreenColor    (!SettingColor || SettingRed ? 0xe6f7f0 : 0xfcf1f2)
//夜间模式
#define tDPriceGreenColor    (!SettingColor || SettingRed ? 0x243035 : 0x2c2b35)

/*******   市场模块文字和背景色      **********/
#define Is_iPhoneX ((kScreenHeight == 812) ? YES:NO)
#define IS_IOS9_AND_UP  ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 9)
#define PF_MEDI(_SIZE_) (IS_IOS9_AND_UP ?[UIFont fontWithName:@"PingFangSC-Medium" size:_SIZE_] : [UIFont systemFontOfSize:_SIZE_])

#define PF_REGU(_SIZE_) (IS_IOS9_AND_UP ? [UIFont fontWithName:@"PingFangSC-Regular" size:_SIZE_] : [UIFont systemFontOfSize:_SIZE_])
#define RO_REGU(_SIZE_) [UIFont fontWithName:@"Roboto-Regular" size:_SIZE_]

#define ROBOT_FONT(_SIZE_) RO_REGU(_SIZE_)
#define PINGFANG_MEDI_FONT(_SIZE_) PF_MEDI(_SIZE_)

/*  *********** Debug  *************  */

#ifdef DEBUG
#define DLog(...) NSLog(__VA_ARGS__)
#define WLog(...) NSLog(@"WARNING:!!!!!!!!!!!!!!!!!!!!:%@",__VA_ARGS__)
#define ELog(...) NSLog(@"ERROR:######################:%@",__VA_ARGS__)
#else
#define DLog(...)
#define WLog(...)
#define ELog(...)
#endif

/*  *********** Images  *************  */

#define GET_IMAGE_NAME(name)    [UIImage imageNamed:name]
#define GET_IMAGE_PATH(path)    [UIImage imageWithContentsOfFile:path]

/*  *********** Others  *************  */

#define weakself  __weak  __typeof(self)weakSelf = self
#define strongself typeof(weakSelf) __strong strongSelf = weakSelf

#define CONNECTQUOTESERVER  @"connectQuoteServer"

#define SameString(str0,str1) [str0 isEqualToString:str1]
#define StringFromClass(cls) NSStringFromClass([cls class])

/*  *********** 友盟统计  *************  */
//自定义事件key
#define UMEnterLoginPage    @"Enter_Login_Page"
#define UMEnterRegisterPage @"Enter_Register_Page"
#define UMLoginSuccess      @"Login_Success"
#define UMRegisterSuccess   @"Register_Success"
#define UM_customerService  @"customerService"   // WebCustomerSeviceVC
#define UM_deposit1         @"deposit1"          // ChannelVC
#define UM_deposit2         @"deposit2"         // 输入金额页
#define UM_deposit3         @"deposit3"            // 入金成功页
#define UM_depositbankchoose         @"depositbankchoose"  // IXIncashStep1VC
#define UM_depositbankchooseNS         @"depositbankchooseNS"  // IXIncashStep1VC_下一步
#define UM_depositbankconfirm         @"depositbankconfirm"  // IXIncashAddCardVC
#define UM_depositbankconfirmNS        @"depositbankconfirmNS" // IXIncashAddCardVC_下一步
#define UM_depositss_f         @"depositss_f"   // ChannelOrNot
#define UM_depositss_fNS         @"depositss_fNS" // ChannelOrNot_点击下一步
#define UM_TelegraphicT         @"TelegraphicT"    // Channel选择电汇的弹窗
#define UM_depositCredit        @"depositCredit"   // 支付通道信用卡
#define UM_depositCreditNS      @"depositCreditNS"   // 支付通道信用卡_下一步

