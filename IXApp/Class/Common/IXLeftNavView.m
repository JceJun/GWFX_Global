//
//  IXLeftNavView.m
//  IXApp
//
//  Created by Seven on 2017/6/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXLeftNavView.h"
#import "UIImageView+AFNetworking.h"
#import "IXUserDefaultM.h"
#import "UIImageView+WebCache.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"

NSString * const IXLeftNavViewMarkEnableNotify = @"IXLeftNavViewMarkEnableNotify";
static  NSInteger   noReadCoiunt;

@interface IXLeftNavView ()

@property (nonatomic, strong) UIView    * markPoint;

@end

@implementation IXLeftNavView

+ (void)setMsgNoReadCount:(NSInteger)count
{
    noReadCoiunt = count;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithTarget:(id)target action:(SEL)sel
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, 60, 44);
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(-10, 14, 3, 16)];
        icon.dk_imagePicker = DKImageNames(@"nav_more", @"nav_more_D");
        [self addSubview:icon];
        
        _iconImgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 6, 32, 32)];
        _iconImgV.tag = 1;
        _iconImgV.userInteractionEnabled = YES;
        _iconImgV.layer.masksToBounds = YES;
        _iconImgV.layer.cornerRadius = VIEW_H(_iconImgV)/2;

        [_iconImgV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                     placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                              options:SDWebImageAllowInvalidSSLCertificates];
        [self addSubview:_iconImgV];
        
        _markPoint = [[UIView alloc] initWithFrame:CGRectMake(24, 7, 8, 8)];
        _markPoint.layer.cornerRadius = 4.f;
        _markPoint.layer.masksToBounds = YES;
        _markPoint.dk_backgroundColorPicker = DKColorWithRGBs(0xff4653, 0xff4d2d);
        _markPoint.hidden = noReadCoiunt == 0;
        [self addSubview:_markPoint];
        
        UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:target action:sel];
        [_iconImgV addGestureRecognizer:tg];
        
        NSString    * typeStr = [IXDataProcessTools showCurrentAccountType];
        NSInteger width = [IXEntityFormatter getContentWidth:typeStr WithFont:PF_MEDI(10)] + 10;
        _typeLab = [IXUtils createLblWithFrame:CGRectMake(35, 15, width, 14)
                                      WithFont:PF_MEDI(10)
                                     WithAlign:NSTextAlignmentCenter
                                    wTextColor:0xffffff
                                    dTextColor:0x242a36];
        _typeLab.layer.cornerRadius = 2.f;
        _typeLab.layer.masksToBounds = YES;
        _typeLab.dk_backgroundColorPicker = DKColorWithRGBs(0x99abba, 0x50a1e5);
        _typeLab.text = typeStr;
        [self addSubview:_typeLab];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(notificationAction:)
                                                     name:IXLeftNavViewMarkEnableNotify
                                                   object:nil];
    }
    
    return self;
}

- (void)notificationAction:(NSNotification *)notify
{
    if (notify.object && [notify.object integerValue] > 0) {
        _markPoint.hidden = NO;
    }else{
        _markPoint.hidden = YES;
    }
}


@end
