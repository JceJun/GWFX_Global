//
//  ExAutoModel.h
//  FXApp
//
//  Created by Larry on 2017/8/6.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExAutoModel : NSObject
// NSUserDefaults本身不支持自定义对象的存储，但它支持NSData类型
// NSData类型就必须实现归档
// 1.遵守NSCoding协议   2.实现encodeWithCoder和initWithCoder
+ (id)transferDataToObject:(NSData *)data;
+ (NSData *)transferObjectToData:(id)obj;
@end
