//
//  IXSymbolCataCell.h
//  IXApp
//
//  Created by Bill on 16/10/25.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IxItemSymbolCata.pbobjc.h"

@interface IXSymbolCataCell : UICollectionViewCell

/**
 *    产品类别信息
 */
@property (nonatomic, strong) NSDictionary *symbolCata;

@end
