//
//  IXUnOpenQuoteView.h
//  IXApp
//
//  Created by Bob on 2017/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXUnOpenQuoteView : UIView


@property (nonatomic, strong) NSString *tipTitle;

@property (nonatomic, strong) NSString *tipContent;


@end
