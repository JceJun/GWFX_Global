//
//  NSArray+Expection.m
//  IXApp
//
//  Created by Bob on 2017/6/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "NSArray+Expection.h"

@implementation NSArray (Expection)

- (double)dAtIndex:(NSInteger)index
{
    if ( ![self count] || [self count] <= index ) {
        return 0;
    }
    return [[self objectAtIndex:index] doubleValue];
}
@end
