//
//  IXCustomView.h
//  JDTest
//
//  Created by ixiOSDev on 16/11/8.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IXCustomView : NSObject

+ (UIImageView *)createImageView:(CGRect)frame image:(UIImage *)image;

+ (UITextField *)createTextField:(CGRect)frame
                 textPlaceholder:(NSString *)placeholder
                           title:(NSString *)title
                            font:(UIFont *)font
                      wTextColor:(NSUInteger)wHexColor
                      dTextColor:(NSUInteger)dHexColor
                   textAlignment:(NSTextAlignment)textAlignment;

+ (UILabel *)createLable:(CGRect)frame
                   title:(NSString *)title
                    font:(UIFont *)font
              wTextColor:(NSUInteger)wHexColor
              dTextColor:(NSUInteger)dHexColor
           textAlignment:(NSTextAlignment)textAlignment;

+ (UIButton *)createButton:(CGRect)frame
                     title:(NSString *)title
                      font:(UIFont *)font
                wTextColor:(NSUInteger)wHexColor
                dTextColor:(NSUInteger)dHexColor
             textAlignment:(NSTextAlignment)textAlignment;

@end
