//
//  UIView+IX.m
//  IXApp
//
//  Created by Seven on 2017/11/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "UIView+IX.h"

@implementation UIView (IX)

- (CGFloat)safe_btomMargin
{
     if (@available(iOS 11.0, *)){
         return [UIApplication sharedApplication].keyWindow.safeAreaInsets.bottom;
    }
    return 0.f;
}

- (CGFloat)safe_topMargin
{
    if (@available(iOS 11.0, *)){
        return [UIApplication sharedApplication].keyWindow.safeAreaInsets.top;
    }
    return 0;
}

@end
