//
//  IXComCalM.h
//  IXApp
//
//  Created by Bob on 2017/12/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXComCalM : NSObject


/**
 手数计算佣金
 
 @param comRate 佣金系数
 @param vol 数量
 @param conSize 合约单位
 @return 佣金
 */
+ (double)cacCom:(double)comRate vol:(double)vol conSize:(NSInteger)conSize;


/**
 数量计算佣金
 
 @param comRate 佣金系数
 @param vol 数量
 @param swap 汇率
 @param price 价格
 @return 佣金
 */
+ (double)cacCom:(double)comRate vol:(double)vol swap:(double)swap price:(double)price;
 
@end
