//
//  IXUserInfoMgr.h
//  IXApp
//
//  Created by Magee on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IxProtoUser.pbobjc.h"
#import "IxItemAccount.pbobjc.h"
#import "IxItemUser.pbobjc.h"
#import "IxItemSession.pbobjc.h"
#import "IXCredentialM.h"

@interface IXUserInfoMgr : NSObject

//trade
@property (nonatomic,assign)item_session_etype itemType;
@property (nonatomic,strong)proto_user_login_info *userLogInfo;//tradeUserInfo

@property (atomic, assign) BOOL swiAccount;
@property (atomic, assign) BOOL isValid;
@property (atomic, assign) proto_user_login_elogintype loginType;//登录类型

@property (atomic, assign) BOOL symFlag;
@property (atomic, assign) BOOL symCataFlag;
@property (atomic, assign) BOOL accSymCataFlag;
@property (atomic, assign) uint64_t symCount;
@property (atomic, assign) uint64_t symCataCount;
@property (atomic, assign) uint64_t accSymCataCount;

/** 是否为夜间模式 */
@property (nonatomic, assign) BOOL isNightMode;
/** 是否设置红涨绿跌 */
@property (nonatomic, assign) BOOL  settingRed;


/** 保存APP应用广告列表 */
@property (nonatomic, strong) NSMutableArray *appAdArr;

/** demo账号启用开关 */
@property (nonatomic, assign) BOOL isCloseDemo;//NO:启用(默认) YES:关闭

/** 记录登录名称 */
@property (nonatomic, strong) NSString *loginName;



+ (IXUserInfoMgr *)shareInstance;

/** 用于切换用户时清空缓存 */
- (void)clearInstance;

#pragma mark -
#pragma mark - 入金

/** 因为初始化player之后随即播放将无任何声音，所以放在单例里面操作 */
/** 系统声音附带震动 */
- (void)showSoundAlert;

/** 仅震动 */
- (void)showShakeAlert;

/** 播放声音并震动，播放系统声音时会导致连续两次震动 */
- (void)showSoundAndShakeAlert;

#pragma mark -
#pragma mark - 游客账户
//登录账号
- (NSString *)demoLoginAccount;
//登录密码
- (NSString *)demoLoginPassword;
//是否是demo登录
- (BOOL)isDemeLogin;

@end


