//
//  IXBaseNavVC+BtnItem.h
//  IXApp
//
//  Created by Magee on 2017/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBaseNavVC.h"

@interface IXBaseNavVC (BtnItem)

+ (UIBarButtonItem *)getDefaultBackWithTarget:(id)target sel:(SEL)sel;

+ (UIBarButtonItem *)getLeftBtnItemWithTitle:(NSString *)title target:(id)target sel:(SEL)sel;


//////////////////////////////////////////////////////////

+ (UIBarButtonItem *)getRightBtnItemWithImg:(UIImage *)img target:(id)target sel:(SEL)sel;

+ (UIBarButtonItem *)getRightBtnItemWithTitle:(NSString *)title target:(id)target sel:(SEL)sel;

+ (UIBarButtonItem *)getRightItemWithImg:(UIImage *)img target:(id)target sel:(SEL)sel aimWidth:(CGFloat)width;

+ (UIBarButtonItem *)getRightItemWithDayImg:(UIImage *)dImg nightImg:(UIImage *)nImg target:(id)target sel:(SEL)sel aimWidth:(CGFloat)width;

@end

