//
//  IXUnOpenQuoteView.m
//  IXApp
//
//  Created by Bob on 2017/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUnOpenQuoteView.h"


@interface IXUnOpenQuoteView ()

@property (nonatomic, strong) UIImageView *sepLineImg;

@property (nonatomic, strong) UIImageView *stateImg;

@property (nonatomic, strong) UILabel *tipTitleLbl;

@property (nonatomic, strong) UILabel *tipContentLbl;

@property (nonatomic, strong) UIView *leftBackV;

@property (nonatomic, strong) UIView *rightBackV;

@end

@implementation IXUnOpenQuoteView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if ( self ) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.leftBackV];
        [self addSubview:self.rightBackV];
        
        UIImageView *sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, 6, 86)];
        sepLineImg.backgroundColor = UIColorHexFromRGB(0xee9428);
        [self.leftBackV addSubview:sepLineImg];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake( kScreenWidth - 96, 10, 13, 13);
        [btn addTarget:self
                action:@selector(responseToHidden)
      forControlEvents:UIControlEventTouchUpInside];
        [btn setImage:[UIImage imageNamed:@"common_removeView"]
             forState:UIControlStateNormal];
        [self.rightBackV addSubview:btn];
        
        self.sepLineImg.backgroundColor = UIColorHexFromRGB(0x0d0d0d);
        self.sepLineImg.alpha = 0.11;
        
        
        [self.stateImg setImage:[UIImage imageNamed:@"icon-"]];
    }
    return self;
}

- (void)responseToHidden
{
    [self removeFromSuperview];
}

- (void)setTipTitle:(NSString *)tipTitle
{
    if ( tipTitle && tipTitle.length != 0 ) {
        self.tipTitleLbl.text = tipTitle;
    }
}


- (void)setTipContent:(NSString *)tipContent
{
    if ( tipContent && tipContent.length != 0 ) {
        self.tipContentLbl.text = tipContent;
    }
}

- (UIView *)leftBackV
{
    if ( !_leftBackV ) {
        _leftBackV = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, 68, 86)];
        _leftBackV.backgroundColor = UIColorHexFromRGB(0xfff9f1);
    }
    return _leftBackV;
}

- (UIView *)rightBackV
{
    if ( !_rightBackV ) {
        _rightBackV = [[UIView alloc] initWithFrame:CGRectMake( 68, 0, kScreenWidth - 68, 86)];
        _rightBackV.backgroundColor = UIColorHexFromRGB(0xfdf9f3);
    }
    return _rightBackV;
}

- (UIImageView *)sepLineImg
{
    if ( !_sepLineImg ) {
        _sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake(67, 0, 1, 86)];
        [_leftBackV addSubview:_sepLineImg];
    }
    return _sepLineImg;
}


- (UIImageView *)stateImg
{
    if ( !_stateImg ) {
        _stateImg = [[UIImageView alloc] initWithFrame:CGRectMake( 21, 26, 32, 32)];
        [_leftBackV addSubview:_stateImg];
    }
    return _stateImg;
}

- (UILabel *)tipTitleLbl
{
    if ( !_tipTitleLbl ) {
        _tipTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 20, 15, kScreenWidth - 110, 18)
                                          WithFont:PF_MEDI(15)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0xee9428
                                        dTextColor:0xff0000];
        [_rightBackV addSubview:_tipTitleLbl];
    }
    return _tipTitleLbl;
}

- (UILabel *)tipContentLbl
{
    if ( !_tipContentLbl ) {
        _tipContentLbl = [IXUtils createLblWithFrame:CGRectMake( 20, 43, kScreenWidth - 110, 12)
                                            WithFont:PF_MEDI(12)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0xf5be7c
                                          dTextColor:0xff0000];
        [_rightBackV addSubview:_tipContentLbl];
    }
    return _tipContentLbl;
}


@end
