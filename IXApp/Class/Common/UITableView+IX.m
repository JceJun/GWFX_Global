//
//  UITableView+IX.m
//  IXApp
//
//  Created by Seven on 2017/12/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "UITableView+IX.h"

@implementation UITableView (IX)
- (UIView *)sectionHeaderShadow
{
    UIView  * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * topL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 5, kScreenWidth, 5)
                                                        dkcolors:topColors];
    [view.layer addSublayer:topL];
    
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [view.layer addSublayer:btomL];
    
    return view;
}

- (UIView *)bottomShadow
{
    UIView  * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [view.layer addSublayer:btomL];
    
    return view;
}

@end
