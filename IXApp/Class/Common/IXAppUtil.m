//
//  IXAppUtil.m
//  IXApp
//
//  Created by Seven on 2017/4/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAppUtil.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AdSupport/AdSupport.h>
#import "sys/utsname.h"
#import "IXDateUtils.h"

@implementation IXAppUtil


#pragma mark -
#pragma mark - 权限

+ (BOOL)microphoneEnable
{
    __block BOOL enable = NO;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
        [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
            enable = granted;
        }];
    }
    if (!enable) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"JudgedMicrophonePrivicy"]) {
            [self showAuthorityAlertWith:@"麦克风"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"JudgedMicrophonePrivicy"];
        }
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"JudgedMicrophonePrivicy"];
    }
    return enable;
}

+ (BOOL)cameraEnable
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    BOOL    enable = !(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied);
    if (!enable) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"JudgedCameraPrivicy"]) {
            [self showAuthorityAlertWith:@"相机"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"JudgedCameraPrivicy"];
        }
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"JudgedCameraPrivicy"];
    }
    return enable;
}

+ (BOOL)albumEnable
{
    ALAuthorizationStatus   status = [ALAssetsLibrary authorizationStatus];
    BOOL    enable = !(status == ALAuthorizationStatusRestricted || status == ALAuthorizationStatusDenied);
    if (!enable) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"JudgedAlbumPrivicy"]) {
            [self showAuthorityAlertWith:@"相册"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"JudgedAlbumPrivicy"];
        }
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"JudgedAlbumPrivicy"];
    }
    return enable;
}

+ (NSString *)deviceType
{
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}


+ (void)showAuthorityAlertWith:(NSString *)privicyName{
    NSDictionary    * infoDic = [[NSBundle mainBundle] infoDictionary];
    NSString    * appName = infoDic[@"CFBundleDisplayName"];
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    NSString    * msg = @"";
    if ([str isEqualToString:LANEN]) {
        NSString    * sprivicyName = [NSString stringWithFormat:@"s%@",privicyName];
        msg = [NSString stringWithFormat:LocalizedString(@"隐私权限申请"),
               appName,
               LocalizedString(sprivicyName),
               LocalizedString(privicyName)];
    }else{
        msg = [NSString stringWithFormat:LocalizedString(@"隐私权限申请"),
               LocalizedString(privicyName),
               appName,
               LocalizedString(privicyName)];
    }
    
    UIAlertControllerStyle  style = [self isIpad] ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert;
    
    UIAlertController   * alert = [UIAlertController alertControllerWithTitle:nil
                                                                      message:msg
                                                               preferredStyle:style];
    UIAlertAction   * confirmAction = [UIAlertAction actionWithTitle:LocalizedString(@"好")
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [alert dismissViewControllerAnimated:YES
                                                                                           completion:nil];
                                                             }];
    [alert addAction:confirmAction];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}


#pragma mark -
#pragma mark - 获取最顶层VC
+ (BOOL)isIpad
{
    return [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad;
}

+ (UIViewController *)topViewController
{
    UIViewController *resultVC;
    resultVC = [self _topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
    while (resultVC.presentedViewController) {
        resultVC = [self _topViewController:resultVC.presentedViewController];
    }
    
    return resultVC;
}

+ (UIViewController *)_topViewController:(UIViewController *)vc
{
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    
    return nil;
}

+ (NSNumber *)appTimeZone
{
    NSDictionary    * timeZ = @{@"GMT-12"   : @(-12),
                                @"GMT-11"   : @(-11),
                                @"GMT-10"   : @(-10),
                                @"GMT-9"    : @(-9),
                                @"GMT-8"    : @(-8),
                                @"GMT-7"    : @(-7),
                                @"GMT-6"    : @(-6),
                                @"GMT-5"    : @(-5),
                                @"GMT-4"    : @(-4),
                                @"GMT-3"    : @(-3),
                                @"GMT-2"    : @(-2),
                                @"GMT-1"    : @(-1),
                                @"GMT+0"    : @(0),
                                @"GMT+1"    : @(1),
                                @"GMT+2"    : @(2),
                                @"GMT+3"    : @(3),
                                @"GMT+4"    : @(4),
                                @"GMT+5"    : @(5),
                                @"GMT+6"    : @(6),
                                @"GMT+7"    : @(7),
                                @"GMT+8"    : @(8),
                                @"GMT+9"    : @(9),
                                @"GMT+10"   : @(10),
                                @"GMT+11"   : @(11),
                                @"GMT+12"   : @(12),
                                };
    NSString    * timeZone = [IXDateUtils getUserSaveTimeZone];
    if (!timeZone.length) {
        return @0;
    }
    
    return timeZ[timeZone];
}


+ (BOOL)isValidateEmail:(NSString *)email
{
    NSString    * emailRegex = @"^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$";
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [predicate evaluateWithObject:email];
}


+ (UIImage*)createImageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.f, 0.f, 1.f, 1.f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

/**
 金额转对应字符串（带逗号）
 @param amount 金额数
 */
+ (NSString *)amountToString:(id)amount
{
    NSString    * string = [NSString stringWithFormat:@"%@",amount];
    NSString    * header = string;
    NSString    * footer = @"";
    
    if ([string containsString:@"."]) {
        string = [string stringByAppendingString:@"00"];    //防止小数点后只有一位小数
        NSArray * arr = [[string mutableCopy] componentsSeparatedByString:@"."];
        header = [arr firstObject];
        footer = [NSString stringWithFormat:@".%@",[arr lastObject]];
        if (footer.length >= 3) {
            footer = [footer substringToIndex:3];
        }
    }
    
    if (header.length <= 3) {
        return [header stringByAppendingString:footer];
    }else{
        NSMutableArray  * arr = [@[] mutableCopy];
        NSInteger   length = header.length % 3;
        NSInteger   count = header.length / 3;
        
        if (length > 0) {
            count ++;
        }else{
            length = 3;
        }
        
        NSInteger   loc = 0;
        for (int i = 0; i < count; i ++) {
            if (i > 0) {
                length = 3;
            }
            
            if (header.length >= loc + length) {
                [arr addObject:[header substringWithRange:NSMakeRange(loc, length)]];
                loc += length;
            }
        }
        
        NSMutableString * aimStr = [@"" mutableCopy];
        for (int j = 0; j < [arr count]; j ++) {
            if (j > 0) {
                [aimStr appendString:@","];
            }
            [aimStr appendString:arr[j]];
        }
        
        return [aimStr stringByAppendingString:footer];
    }
    
    return @"";
}

/**
 金额字符串转金额数
 */
+ (double)stringToAmount:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"," withString:@""];
    return [string doubleValue];
}

/** 获取设备idfa */
+ (NSString *)idfa
{
    NSString * idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    return idfa;
}

+ (NSString *)appVersion
{
    NSString * version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    return version;
}

/** 获取当前货币单位 **/
+ (NSString *)getCurrencyUnit:(NSString *)currency
{
    if (SameString(currency,@"CNY")) {
        return LocalizedString(@"元");
    } else if (SameString(currency,@"USD")) {
        return LocalizedString(@"美元");
    } else {
        return @"";
    }
}

@end
