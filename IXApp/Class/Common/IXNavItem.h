//
//  IXNavItem.h
//  IXApp
//
//  Created by Seven on 2017/7/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXNavItem : UIView


/**
 自定义导航栏左右按钮

 @param target target
 @param sel action
 @param title title
 @param isRight 是否为右侧按钮，此值影响title的对齐方式
 @return obj
 */
- (instancetype)initWithTarget:(id)target action:(SEL)sel title:(NSString *)title isRightItem:(BOOL)isRight;

@end
