//
//  IXSweetTitleV.h
//  IXApp
//
//  Created by Magee on 2016/12/17.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    IXSweetShowDisCnt,
    IXSweetShowCnting,
    IXSweetShowCnted,
    IXSweetShowMaxTried
}IXSweetShowType;

@interface IXSweetTitleV : UIView

@property (nonatomic,copy)NSString *defaultTitle;
@property (nonatomic,strong)UIView *defaultView;
@property (nonatomic,copy)void(^reconnectTaped)();

- (void)startWitting;
- (void)stopWitting;

- (void)showNetStatus:(IXSweetShowType)showType;

@end
