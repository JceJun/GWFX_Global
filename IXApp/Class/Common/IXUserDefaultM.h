//
//  IXUserDefaultM.h
//  IXApp
//
//  Created by Bob on 2017/2/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXNewGuideV.h"

typedef NS_ENUM(NSInteger, UnitSettingType) {
    UnitSettingTypeCount,   //数量
    UnitSettingTypeLot,     //手数
};

typedef NS_ENUM(NSInteger,SymCataStyle) {
    SymbolCataStyleVertical,//竖向
    SymbolCataStyleHorizontal,//横向
};

@interface IXUserDefaultM : NSObject




//缓存用户选择的分类
+ (void)saveChooseSymbolCata:(NSInteger)cataRow;

+ (NSInteger)getChooseSymbolCata;

//缓存银行卡列表
+ (void)saveBankList:(NSArray *)arr;

+ (NSArray *)getBankList;

//缓存省信息
+ (void)saveProvince:(NSArray *)arr;

+ (NSArray *)getProvince;

//缓存市信息
+ (void)saveCity:(NSArray *)arr;

+ (NSArray *)getCity;

//缓存省和市
+ (void)saveCityAndProvnice:(NSArray *)arr;

+ (NSArray *)getCityAndProvnice;


#pragma mark -
#pragma mark - 设置页设置

//保存Dom交易习惯
+ (void)saveDomResult:(BOOL)status;

+ (BOOL)getDomResult;


#pragma mark -
#pragma mark - 屏幕常亮设置&处理

/** 是否设置了屏幕常亮 */
+ (BOOL)screenNormalLight;

/** 设置常量 */
+ (void)setScreenNormalLight:(BOOL)light;

/** 根据设置决定是否设置屏幕常亮 */
+ (void)dealScreenNormalLight;


#pragma mark -
#pragma mark - 提示设置&处理

//声音提示是否可用
+ (BOOL)soundAlertEnable;
//震动提示是否可用
+ (BOOL)shakeAlertEnable;

//设置声音提示可用与否
+ (void)setSoundAlertEnable:(BOOL)enable;
//设置震动提示可用与否
+ (void)setShakeAlertEnable:(BOOL)enable;

//处理提示
+ (void)dealAlert;


#pragma mark -
#pragma mark - 夜间模式设置&处理

/** 是否为夜间模式 */
+ (BOOL)isNightMode;

/**
 设置夜间模式

 @param isNightMode 是否设置为夜间模式
 */
+ (void)setNightMode:(BOOL)isNightMode;

/**
 设置行情颜色
 
 @param isQuoteColor //YES:红涨绿跌
 */
+ (void)setQuoteColor:(BOOL)isQuoteColor;

/** 根据设置处理夜间模式 */
+ (void)dealDayNightMode;


/**
 打开关闭相册和相机时处理状态栏颜色
 */
+ (void)dealStatusBarWithAlbumState:(BOOL)open;

#pragma mark -
#pragma mark - 单位设置

+ (UnitSettingType)unitSetting;

+ (void)setDefaultUnit;

+ (void)setUnit:(UnitSettingType)type;

#pragma mark -
#pragma mark - 新手指引
+ (void)saveNewGuide:(IXNewGuide)type;

+ (BOOL)getNewGuide:(IXNewGuide)type;

#pragma mark -
#pragma mark - 产品分类显示样式
+ (void)saveSymbolCataStyle:(SymCataStyle)style;

+ (SymCataStyle)symbolCataStyle;

#pragma mark -
#pragma mark - 手机所属国家编码
+ (void)saveCountryCode:(NSString *)code;

+ (NSString *)nationalityByNationalCode:(NSString *)code;

+ (NSString *)countryCodeBynationality:(NSString *)nationality;

+ (NSString *)nationalCodeByCode:(NSString *)code;

+ (IXCountryM *)nationalityByCode:(NSString *)code;//ISO_3166_156

+ (IXCountryM *)nationalityModelByNationalCode:(NSString *)nationalCode;//CN

+ (IXCountryM *)nationalityByCountryCode:(NSString *)countryCode;//86

+ (void)saveNationality:(NSString *)nationality;

+ (NSString *)getCountryCode;

+ (NSString *)getCode;

+ (BOOL)isChina;

@end
