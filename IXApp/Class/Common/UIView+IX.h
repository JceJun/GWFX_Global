//
//  UIView+IX.h
//  IXApp
//
//  Created by Seven on 2017/11/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (IX)

- (CGFloat)safe_btomMargin;
- (CGFloat)safe_topMargin;

@end
