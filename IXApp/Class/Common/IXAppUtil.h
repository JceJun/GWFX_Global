//
//  IXAppUtil.h
//  IXApp
//
//  Created by Seven on 2017/4/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IXAppUtil : NSObject


/** 麦克风是否可用，不可用会弹出提示 */
+ (BOOL)microphoneEnable;

/** 相机是否可用，不可用会弹出提示 */
+ (BOOL)cameraEnable;

/** 相册是否可用，不可用会弹出提示 */
+ (BOOL)albumEnable;

/** 获取导航控制器中的顶层VC */
+ (UIViewController *)topViewController;

/** 设备版本 */
+ (NSString *)deviceType;

/** 返回用户设置的交易时区，例：东八区返回8,细八区返回-8 */
+ (NSNumber *)appTimeZone;

+ (BOOL)isValidateEmail:(NSString *)email;

+ (UIImage*)createImageWithColor:(UIColor *)color;


/**
 金额转对应字符串（带逗号）
 @param amount 金额数
 */
+ (NSString *)amountToString:(id)amount;

/**
 金额字符串转金额数
 */
+ (double)stringToAmount:(NSString *)string;

/** 获取设备idfa */
+ (NSString *)idfa;

/** 获取app 短版本号 */
+ (NSString *)appVersion;

/** 获取当前货币单位 **/
+ (NSString *)getCurrencyUnit:(NSString *)currency;

@end
