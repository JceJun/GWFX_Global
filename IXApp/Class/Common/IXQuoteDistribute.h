//
//  IXQuoteDistribute.h
//  IXApp
//
//  Created by Magee on 2017/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IXQuoteDistribute <NSObject>

- (void)needRefresh;

- (void)cancelVisualQuote;

- (void)subscribeVisualQuote;

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd;

@end
