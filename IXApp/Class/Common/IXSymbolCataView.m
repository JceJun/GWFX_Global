//
//  IXSymbolCataView.m
//  IXApp
//
//  Created by bob on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSymbolCataView.h"
#import "IXSymbolCataCell.h"

@interface IXSymbolCataView ()<UICollectionViewDataSource,UICollectionViewDelegate>
/**
 *  切换的控制器
 */
@property (nonatomic, strong) UICollectionView *switchView;

/**
 *  数据源
 */
@property (nonatomic, strong) NSMutableArray *dataSourceAry;

/**
 *  选中某行的回调
 */
@property (nonatomic, copy) selectIndexPath choiceIndexPath;


/**
 *  分割线
 */
@property (nonatomic, strong) UILabel *sepLineLbl;

@end

@implementation IXSymbolCataView

- (id)initWithFrame:(CGRect)frame WithCataAry:(NSArray *)cataArr{
    self = [super initWithFrame:frame];
    if ( self ) {
        self.dataSourceAry = [cataArr mutableCopy];
        [self.switchView reloadData];
    }
    return self;
}


- (void)resetDataSource:(NSArray *)dataSource
{
    [_dataSourceAry removeAllObjects];
    _dataSourceAry = [dataSource mutableCopy];
    [self.switchView reloadData];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self resetSepFrame:[NSIndexPath indexPathForRow:0 inSection:0]];
    });;
}

#pragma mark call method
- (void)setSelectedIndexPath:(selectIndexPath)selectedIndexPath{
    self.choiceIndexPath = selectedIndexPath;
}

#pragma mark UICollectionView DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.dataSourceAry.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    IXSymbolCataCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([IXSymbolCataCell class]) forIndexPath:indexPath];
    cell.symbolCata = [self.dataSourceAry objectAtIndex:indexPath.section];
    return cell;
}


#pragma mark UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    NSString *title = [self.dataSourceAry objectAtIndex:indexPath.section][@"name"];
//    if (title.length) {
//        NSInteger length = [IXEntityFormatter getContentWidth:title WithFont:PF_MEDI(13)] + 20;
//        return CGSizeMake(length, self.cellHeight);
//    }
    return (CGSize){self.itemWidth, self.cellHeight};
}

#pragma mark UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.choiceIndexPath) {
        self.choiceIndexPath(indexPath);
    }
        
    [self resetSepFrame:indexPath];

 }

#pragma mark 滑动逻辑
- (void)setOffsetPage:(NSInteger)page{
    if (page > _dataSourceAry.count) {
        page = 0;
    }
//    [self animationWithPage:page];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:page];
    [self.switchView scrollToItemAtIndexPath:indexPath
                            atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                    animated:YES];
    
    [self resetSepFrame:indexPath];
}

- (void)resetSepFrame:(NSIndexPath *)indexPath
{
    self.sepLineLbl.frame = [self getSepLineFrameWithPage:indexPath.section];
    UICollectionViewCell *cell = [_switchView cellForItemAtIndexPath:indexPath];
    
    CGRect cellRect = [_switchView convertRect:cell.frame toView:_switchView];
    CGRect frame = self.sepLineLbl.frame;
    frame.origin.x = cellRect.origin.x;
    frame.size.width = cellRect.size.width;
    self.sepLineLbl.frame = frame;
}

- (void)animationWithPage:(NSInteger)page{
    _sepLineLbl.hidden = NO;
    [UIView animateWithDuration:.3f animations:^{
        [_sepLineLbl setFrame:[self getSepLineFrameWithPage:page]];
    }];
}

#pragma mark init moduel
- (NSMutableArray *)dataSourceAry{
    if ( !_dataSourceAry ) {
        _dataSourceAry = [[NSMutableArray alloc] init];
    }
    return _dataSourceAry;
}

- (UICollectionView *)switchView{
    if ( !_switchView ) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        _switchView = [[UICollectionView alloc] initWithFrame:CGRectMake( 0,  0, kScreenWidth, self.cellHeight) collectionViewLayout:flowLayout];
        [self addSubview:_switchView];
        _switchView.showsHorizontalScrollIndicator = NO;
        _switchView.contentSize = CGSizeMake( self.itemWidth * [self.dataSourceAry count], self.cellHeight);
         _switchView.delegate = self;
        _switchView.dataSource = self;
        _switchView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
        [_switchView registerClass:[IXSymbolCataCell class]
        forCellWithReuseIdentifier:NSStringFromClass([IXSymbolCataCell class])];
        [_switchView addSubview:self.sepLineLbl];
    }
    return _switchView;
}


- (UILabel *)sepLineLbl
{
    if (!_sepLineLbl) {
        _sepLineLbl = [UILabel new];
        _sepLineLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        [self addSubview:_sepLineLbl];
    }
    return _sepLineLbl;
}


- (CGFloat)itemWidth{
    return 90;
}

- (CGFloat)cellHeight{
    return 40;
}

- (CGFloat)sepLineHeight{
    return 2;
}

- (CGRect)getSepLineFrameWithPage:(NSInteger)page{
    CGFloat orginY = [self cellHeight] - [self sepLineHeight];
    CGRect frame = CGRectMake(page * [self itemWidth], orginY, [self itemWidth], [self sepLineHeight]);
    return frame;
}


@end
