//
//  IXAccountGroupModel.m
//  IXApp
//
//  Created by Evn on 2017/12/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAccountGroupModel.h"
#import "IXDBAccountGroupMgr.h"
#import "IXTradeDataCache.h"

@interface IXAccountGroupModel()

@property (nonatomic, assign) BOOL needResNoti;

@end

@implementation IXAccountGroupModel

static IXAccountGroupModel *share = nil;
+ (IXAccountGroupModel *)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ( !share ) {
            share = [[IXAccountGroupModel alloc] init];
        }
    });
    return share;
}

- (id)init
{
    self = [super init];
    if ( self ){
        [self addObserverKey];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserverKey];
}

- (void)addObserverKey
{
    IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_ADD);
    IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_LIST);
    IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_UPDATE);
    IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_DELETE);
    IXTradeData_listen_regist(self, PB_CMD_USERLOGIN_INFO);
    IXTradeData_listen_regist(self, PB_CMD_USER_LOGIN_DATA_TOTAL);
}

- (void)removeObserverKey
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_LIST);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_DELETE);
    IXTradeData_listen_resign(self, PB_CMD_USERLOGIN_INFO);
    IXTradeData_listen_resign(self, PB_CMD_USER_LOGIN_DATA_TOTAL);
}

- (NSDictionary *)accGroupDic
{
    _accGroupDic = [IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:
                    [IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    return _accGroupDic;
}

- (void)updateCache
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].accountGroupChange = ![IXTradeDataCache shareInstance].accountGroupChange;
    });
}

- (void)clearInstance
{
    if (_accGroupDic) {
        _accGroupDic = nil;
    }
}

#pragma mark 持仓更新的监听
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:PB_CMD_ACCOUNT_GROUP_ADD]
        || [keyPath isEqualToString:PB_CMD_ACCOUNT_GROUP_UPDATE]
        || [keyPath isEqualToString:PB_CMD_ACCOUNT_GROUP_LIST]
        || [keyPath isEqualToString:PB_CMD_ACCOUNT_GROUP_DELETE]) {
        if (_needResNoti) {
            [self updateCache];
        }
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_USERLOGIN_INFO)) {
        _needResNoti = NO;
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_USER_LOGIN_DATA_TOTAL)) {
        _needResNoti = YES;
        [self updateCache];
    }
}

@end
