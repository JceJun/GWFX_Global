//
//  IXAccountBalanceModel.h
//  IXApp
//
//  Created by Bob on 2016/12/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IXQuoteM.h"
#import "IXTradeDataCache.h"

#import "IxItemAccount.pbobjc.h"
#import "IxItemSymbol.pbobjc.h"
#import "IxItemPosition.pbobjc.h"
#import "IxItemSymbolMarginSet.pbobjc.h"
#import "IxProtoSymbolMarginSet.pbobjc.h"

#import "IXDBGlobal.h"
#import "IXDBSymbolMgr.h"
#import "IXDBMarginSetMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDataProcessTools.h"

@class PFTModel;

#define FXCATAID 2109

#define PROFITMODEL   @"profitModel"
#define BASEMODEL     @"baseModel"
#define POSITIONMODEL @"positionModel"
#define MAGNSYMMODEL  @"magnSymModel"

#define SYMBOLID      @"symbolId"

#define SWAPPROFIT    @"swapProfit"
#define SWAPLOSS      @"swapLoss"
#define SWAPDIRECRION @"swapDirection"

#define NOTIFYITEM    @"ITEM"

#define IXAccountBalanceModel_listen_regist(target,keyPath)       \
[[IXAccountBalanceModel shareInstance] addObserver:target    \
forKeyPath:keyPath options:NSKeyValueObservingOptionNew \
context:NULL]

#define IXAccountBalanceModel_listen_resign(target,keyPath)       \
[[IXAccountBalanceModel shareInstance] removeObserver:target \
forKeyPath:keyPath]

#define IXAccountBalanceModel_isSameKey(key1,key2)                \
[key1 isEqualToString:key2]

extern NSString * const AB_CACHE_NUMBER_CHANGE;       //持仓数更新

#define KSYMPFT    @"symProfit"
#define KBUYVOL    @"buyVol"
#define KSELLVOL   @"sellVol"


@interface PFTModel : NSObject

@property (nonatomic, assign) double profit;
@property (nonatomic, strong) IXQuoteM *qModel;
@property (nonatomic, assign) NSInteger posId;
@property (nonatomic, assign) double nPrice;

@end

@interface IXAccountBalanceModel : NSObject

/**
 佣金类型：byVol，byLot
 */
@property (nonatomic, assign) NSInteger commissionType;

//总的买单数
@property (nonatomic, assign) double buyVol;
//总的卖单数
@property (nonatomic, assign) double sellVol;
//产品盈亏
@property (nonatomic, assign) double symProfit;

/**账户总盈亏*/
@property (nonatomic, assign) double totalProfit;

//账户货币
@property (nonatomic, strong) NSString *currency;

/**账户保证金*/
@property (atomic, assign) double totalMargin;

/*
 * 行情模型
 */
@property (atomic, strong)  NSMutableDictionary *quoteDataDic;
 
/*需要保存的订阅交叉盘字典*/
@property (nonatomic, strong) NSMutableDictionary *subSymbolDic;

/*缓存持仓数*/
@property (atomic, strong) NSMutableDictionary *cachePosNumDic;

@property (nonatomic, assign) BOOL cacheNumChangeFlag;

/*
 * 根据持仓，订阅交叉盘、直接盘和间接盘的行情
 */
@property (nonatomic, strong) NSMutableArray *dynQuoteArr;

+ (IXAccountBalanceModel *)shareInstance;

//切换账号和退出登录需要清空计算相关的缓存
- (void)clearCache;

//账户组信息更新，需要刷新当前账号信息
- (void)refreashCurrency;

//动态计算汇率：原则少赚多亏
//offsetPrc表示价格差：负为亏，反之为赚
- (double)caculateSwapWithSymbolId:(NSInteger)symbolId
                   WithOffsetPrice:(double)offsetPrc;

/**
 计算保证金的汇率

 @param symbolId 产品Id
 @param direction 方向（是否要除）
 @param offsetPrc 差价
 @return 汇率
 */
- (double)caculateMarginSwapWithSymbolId:(NSInteger)symbolId
                           WithDirection:(NSInteger)direction
                         WithOffsetPrice:(double)offsetPrc;
  

//产品的保证金层级
- (double)marginCurrentRateWithVolume:(NSInteger)symbolVolume
                           WithSymbol:(IXSymbolM *)symbol;

//接收到新的行情，行情触发盈亏和保证金更新
- (void)setQuoteDataArr:(NSArray *)quoteDataArr;


/*新增实时订阅*/
- (void)addSubSymbolWithSymbolId:(NSInteger)symbolId;

/*删除实时订阅*/
- (void)delSubSymbolWithSymbolId:(NSInteger)symbolId;

//查询缓存的产品
- (IXSymbolM *)symModelWithId:(NSInteger)symbolId;

//查询缓存的行情
- (IXQuoteM *)getQuoteDataBySymbolId:(NSInteger)symbolId;

//查询父类id
- (NSInteger)checkParentBySymbolId:(NSInteger)symbolId;

/**
 * 账号可用资金
 *
 * @return 可以资金
 */
- (double)getAvailabelFunds;


/**
 *  净值 = 浮动盈亏 + 可用余额
 *
 *  @return 净值
 */
- (double)getNet;

/**
 *  保证金比例
 *
 *  @return 乘了100的浮点数
 */
- (double)marginRate;

- (NSString *)getNprc:(item_position *)pos Quote:(IXQuoteM *)qModel;

/**
 计算持仓的盈亏

 @param pos 需要计算盈亏的持仓
 @return 盈亏
 */
- (PFTModel *)caculateProfit:(item_position *)pos;

- (PFTModel *)caculateProfitPosId:(NSInteger)posId;

@end


