//
//  NSString+FormatterPrice.m
//  IXApp
//
//  Created by Bob on 2016/12/27.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "NSString+FormatterPrice.h"

@implementation NSString (FormatterPrice)


+ (NSString *)formatterPrice:(NSString *)price WithDigits:(int)digits
{
    NSInteger location = [price rangeOfString:@"."].location;
    
    if (digits == 0) {
        @try{
            price = [price componentsSeparatedByString:@"."][0];
            return price;
        }@catch(NSException *exception) {
            
        }
    }
    
    if ( location != NSNotFound && location + digits < price.length ) {
        return [price substringToIndex:(location + digits + 1)];
    }else if ( location != NSNotFound && location + digits >= price.length ){
        NSMutableString *result = [NSMutableString stringWithFormat:@"%@",price];
        for ( NSInteger i = 0; i < ( location + digits - price.length + 1); i++ ) {
            [result appendFormat:@"0"];
        }
        return result;
    }
    else if(location == NSNotFound){
        if ( digits > 0 ) {
            NSMutableString *result = [NSMutableString stringWithFormat:@"%@.",price];
            for ( NSInteger i = 0; i < digits; i++ ) {
                [result appendFormat:@"0"];
            }
            return result;
        }
    }
    return price;
}

+ (NSString *)formatterPriceSign:(double)currentPrice WithDigits:(int)digits
{
    NSString *signStr = [NSString string];
    if ( currentPrice > 0 ) {
        signStr = @"+";
    }
    return [NSString formatterPrice:[NSString stringWithFormat:@"%@%lf",signStr,currentPrice] WithDigits:digits];
}

+ (NSString *)formatterProfit:(NSString *)profit
{
    BOOL pos = ([profit rangeOfString:@"+"].location != NSNotFound);
    if ( pos ) {
        return profit;
    }
    
    BOOL nega = ([profit rangeOfString:@"-"].location != NSNotFound);
    if ( nega ) {
        return profit;
    }
    
    if ( [profit floatValue] > 0 ) {
        return [NSString stringWithFormat:@"+%@",profit];
    }
    return profit;
}

+ (NSString *)formatterPriceSign:(double)currentPrice
{
    NSString *signStr = [NSString string];
    if ( currentPrice > 0 ) {
        signStr = @"+";
    }
    return [NSString stringWithFormat:@"%@%lf",signStr,currentPrice];
}

+ (NSString *)formatterUTF8:(NSString *)url
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault,
                                                                                 (CFStringRef)url,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",                                                                                       kCFStringEncodingUTF8 ));
}

+ (NSString *)thousandFormate:(NSString *)str withDigits:(int)digits
{
    double amount = [str doubleValue];
    if (amount < 1) {
        return str;
    }
    NSString    * string = [NSString stringWithFormat:@"%f",amount];
    NSString    * header = string;
    NSString    * footer = @"";

    if ([string containsString:@"."]) {
        NSArray * arr = [[string mutableCopy] componentsSeparatedByString:@"."];
        header = [arr firstObject];
        footer = [NSString stringWithFormat:@"%@",[arr lastObject]];
        if (digits) {
            if (footer.length > digits) {
                footer = [footer substringToIndex:digits];
            } else {
                for (int i = 0 ; (footer.length - digits); i++) {
                    footer = [footer stringByAppendingString:@"0"];
                }
            }
            footer = [NSString stringWithFormat:@".%@",footer];
        } else {
            footer = @"";
        }
       
    }
    
    if (header.length <= 3) {
        return [header stringByAppendingString:footer];
    }else{
        NSMutableArray  * arr = [@[] mutableCopy];
        NSInteger   length = header.length % 3;
        NSInteger   count = header.length / 3;
        
        if (length > 0) {
            count ++;
        }else{
            length = 3;
        }
        
        NSInteger   loc = 0;
        for (int i = 0; i < count; i ++) {
            if (i > 0) {
                length = 3;
            }
            
            if (header.length >= loc + length) {
                [arr addObject:[header substringWithRange:NSMakeRange(loc, length)]];
                loc += length;
            }
        }
        
        NSMutableString * aimStr = [@"" mutableCopy];
        for (int j = 0; j < [arr count]; j ++) {
            if (j > 0) {
                [aimStr appendString:@","];
            }
            [aimStr appendString:arr[j]];
        }
        
        return [aimStr stringByAppendingString:footer];
    }
}

+ (NSString *)doubleToString:(double)value digit:(NSInteger)digit
{
    NSDecimalNumber *prcDec = [NSDecimalNumber decimalNumberWithString:[@(value) stringValue]];
    prcDec = [prcDec decimalNumberByRoundingAccordingToBehavior:[NSString handlerDigits:digit]];
    NSString *prc = [NSString formatterPrice:[prcDec stringValue] WithDigits:digit];
    return prc;
}

+ (NSDecimalNumberHandler *)handlerDigits:(NSInteger)digits
{
    return  [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                   scale:digits
                                                        raiseOnExactness:NO
                                                         raiseOnOverflow:NO
                                                        raiseOnUnderflow:NO
                                                     raiseOnDivideByZero:YES];
}
@end
