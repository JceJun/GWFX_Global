//
//  UIViewController+statics.m
//  ixApp
//
//  Created by Seven on 2017/4/12.
//  Copyright © 2017年 Seven. All rights reserved.
//

#import "UIViewController+statics.h"
#import "IXStaticsMgr.h"
#import "IXHookUtility.h"

@implementation UIViewController (statics)

+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        SEL originAppearSel = @selector(viewWillAppear:);
        SEL newAppearSel = @selector(ix_viewWillAppear:);
        [IXHookUtility swizzlingClass:self.class
                     originalSelector:originAppearSel
                     swizzledSelector:newAppearSel];
    });
}

- (void)ix_viewWillAppear:(BOOL)animated{
    //do user statistics data save
    
    //埋点
    [[IXStaticsMgr shareInstance] enterPage:self];
    [self ix_viewWillAppear:animated];
}

- (void)ix_viewWillDisappear:(BOOL)animated{
    //do user statistics data save
    
    [self ix_viewWillDisappear:animated];
}



@end
