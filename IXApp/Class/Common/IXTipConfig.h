//
//  IXTipConfig.h
//  IXApp
//
//  Created by Bob on 2017/4/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#define PHONEMINLENGTH   5
#define PWDMAXLENGTH     17

#define NONESPACECHAR    LocalizedString(@"您输入的是空格")
#define PWDMAXCHAR       LocalizedString(@"密码最多设置18位")
#define PHONEMINCHAR     LocalizedString(@"请输入最少5位的手机号")

#define PWDVALIDCHAR     LocalizedString(@"请设置6-18位字母或数字")
#define PWDDIFF          LocalizedString(@"新密码和确认新密码不一致")


#define STOCKID          6001
#define IDXMARKETID      11
