//
//  IXLeftNavView.h
//  IXApp
//
//  Created by Seven on 2017/6/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const IXLeftNavViewMarkEnableNotify;


@interface IXLeftNavView : UIView

@property (nonatomic, strong) UIImageView   * iconImgV;
@property (nonatomic, strong) UILabel       * typeLab;

- (instancetype)initWithTarget:(id)target action:(SEL)sel;

+ (void)setMsgNoReadCount:(NSInteger)count;

@end
