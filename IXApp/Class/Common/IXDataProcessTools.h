//
//  IXDataProcessTools.h
//  IXApp
//
//  Created by Evn on 16/11/30.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXSymbolM.h"
#import "IxItemHoliday.pbobjc.h"
#import "IxItemSchedule.pbobjc.h"
#import "IxItemOrder.pbobjc.h"
#import "IXQuoteM.h"
#import "IXLastQuoteM.h"
#import "IxItemAccountGroup.pbobjc.h"
#import "IXSymModel.h"
#import "IXBOModel.h"

#define kScheduleReasonType  @"scheduleReasonType"
#define kHolidayReasonType   @"holidayReasonType"
typedef NS_ENUM(uint32_t,IXHolidayReasonType) {
    
    IXHolidayReasonTypeYes = 0,//假期
    IXHolidayReasonTypeNo
};

typedef NS_ENUM(uint32_t,IXSymbolTradeState) {
    
    IXSymbolTradeStateNormal,//正常交易
    IXSymbolTradeStateBuyNormal,//买交易
    IXSymbolTradeStateSellNormal,//卖交易
    IXSymbolTradeStateCloseOnly,//仅平仓
    IXSymbolTradeStateHoliday,//假期
    IXSymbolTradeStateNoSchedule, //休市中
    IXSymbolTradeStateDisEnable,//未启用
    IXSymbolTradeStateExpiry //过期
    
};

typedef NS_ENUM(uint32_t,IXScheduleReasonType) {
    
    IXScheduleReasonTypeYes = 0,//交易时间
    IXScheduleReasonTypeNo
};

@interface IXDataProcessTools : NSObject

/**
 *  通过产品分类ID查询产品
 *
 *  @param cataId 产品分类ID
 *  @param sIndex 起始位置
 *  @param offset 偏移量
 *
 */

+ (NSMutableArray *)querySymbolBySymbolCataId:(uint64_t)cataId
                                   startIndex:(uint64_t)sIndex
                                       offset:(uint64_t)offset;


/**
 *  通过产品分类ID查询产品(键值对)
 *
 *  @param cataId 产品分类ID
 *  @param sIndex 起始位置
 *  @param offset 偏移量
 *
 */

+ (NSMutableArray *)querySymbolBySymbolNewCataId:(uint64_t)cataId
                                      startIndex:(uint64_t)sIndex
                                          offset:(uint64_t)offset;

/**
 *  通过父类ID查询产品分类
 *
 */
+ (NSArray *)querySymbolCataByParentId:(uint64_t)parentId;

/**
 *  比较产品分类是否属于另一分类或者两者属于相同分类
 *
 *  @param cataId 产品分类ID
 *  @param sIndex 起始位置
 *  @param offset 偏移量
 *
 */
+ (BOOL)compareCataId0:(int64_t)cataId0
               cataId1:(int64_t)cataId1;

/**
 *  模糊查询产品信息
 *  @param search 查询字段
 *  @param cataId 产品分类
 *
 */
+ (NSArray *)fuzzyQuerySymbolByContent:(NSString *)search cataId:(uint64_t)cataId;

/**
 *  分页查询热门产品信息
 *
 */
+ (NSArray *)querySymbolHotStartIndex:(uint64_t)sIndex
                               offset:(uint64_t)offset;

/**
 *  查询热门产品信息(model)
 *
 *
 */

+ (NSArray *)queryAllSymbolsHot;

/**
 *  查询热门产品信息(键值对)
 *
 *
 */

+ (NSArray *)queryAllSymbolsHotNew;


/**
 *  分页查询自选产品信息
 *
 */
+ (NSArray *)querySymbolSubStartIndex:(uint64_t)sIndex
                               offset:(uint64_t)offset;

/**
 *  查询所有自选产品信息
 *
 *
 */

+ (NSArray *)queryAllSymbolsSub;


/**
 *  查询所有自选产品信息(键值对)
 *
 *
 */

+ (NSArray *)queryAllSymbolsSubNew;

/**
 *  通过产品ID查询产品信息
 *
 *
 */
+ (IXSymbolM *)querySymbolBySymbolId:(uint64)symbolId;

/**
 *  通过产品ID查询ParentId信息
 *
 *
 */
+ (NSInteger)queryParentIdBySymbolId:(uint64)symbolId;

/**
 *  查询所有自选产品信息
 *
 *
 */

+ (NSArray *)querySymbolsSubBySymbolCataId:(uint64_t)symbolCataId;


/**
 *  查询当前时间是否是假期
 *
 *  @param symArr 产品信息
 *
 */

+ (IXSymbolTradeState)symbolModelIsCanTrade:(IXSymModel *)symModel;


/**
 *  产品是否可交易
 *
 *  @param state 产品交易状态
 *  @param dir 交易方向
 *
 */
+ (BOOL)canTradeByState:(IXSymbolTradeState)state
               orderDir:(item_order_edirection)dir;

/**
 *  产品是否可交易
 *
 *  @param state 产品交易状态
 *
 */
+ (BOOL)isNormalByState:(IXSymbolTradeState)state;

/**
 *  产品是否可交易
 *
 *  @param state 产品交易状态
 *
 */
+ (BOOL)isNormalAddOptiosByState:(IXSymbolTradeState)state;

/**
 *  查询当前时间是否是假期
 *
 *  @param symArr 产品信息
 *
 */

+ (IXSymbolTradeState)symbolIsCanTrade:(IXSymbolM *)symModel;

/**
 *  查询当前时间是否是假期
 *
 *  @param symbolId 产品ID
 *
 */

+ (IXSymbolTradeState)symbolIsCanTradeNew:(uint64_t)symbolId;

/**
 *  显示数量或手数
 *
 *  @param volume 产品数量
 *  @param contractSizeNew 合约数大小
 *  @param volDigit 数量小数位
 *
 */
+ (NSString *)showCurrentVolume:(double)volume
                contractSizeNew:(int32)contractSizeNew
                       volDigit:(uint64_t)volDigit;

/**
 *  获取手数实际小数位
 *
 *  @param volume 产品手数(不是数量)
 *
 */
+ (NSInteger)volumeDigits:(NSDecimalNumber *)volume;

/**
 *  显示数量或手数单位
 *
 *  @param unitLanName 产品数量单位
 *
 */
+ (NSString *)showCurrentUnitLanName:(NSString *)unitLanName;


/**
 *  查询当前时间是否是交易时间
 *
 *  @param symModel 产品信息
 *
 */
+ (IXSymbolTradeState)queryIsScheduleBySymbol:(IXSymbolM *)symModel;

/**
 *  查询当前交易时间Model
 *
 *  @param symModel 产品信息
 *
 */
+ (item_schedule *)queryScheduleBySymbol:(IXSymbolM *)symModel;

/**
 *  MD5加密字符串
 *
 *  @param str 源字符串
 *
 */
+ (NSString *) md5StringByString:(NSString *)str;

/**
 *  字典转字符串
 *
 *  @param dic 原数据
 *
 */
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

/**
 *  获取文本内容的尺寸
 *
 *  @param str 源字符串
 *
 */
+ (CGSize)textSizeByText:(NSString *)str height:(CGFloat)height font:(UIFont *)font;

/**
 *  显示富文本
 *
 *  @param label 重设控件
 *  @param leftFont 左部字体大小
 *  @param rightFont 右步字体大小
 *  @param cntStr 文本内容
 *
 */
+ (void)resetLabel:(UILabel *)label
          leftFont:(CGFloat)leftFont
         rightFont:(CGFloat)rightFont
       WithContent:(NSString *)cntStr;

/**
 *  显示富文本颜色
 *
 *  @param label 重设控件
 *  @param leftStr 左部文本内容
 *  @param leftFont  左部字体大小
 *  @param wLeftHexColor 左部文本白间颜色
 *  @param dLeftHexColor 左部文本夜间颜色
 *  @param cntStr  文本内容
 *  @param rightFont  右部字体大小
 *  @param wRightHexColor 右部文本白间颜色
 *  @param dRightHexColor 右部文本夜间颜色
 *  @param type NO:左文本 YES:右文本
 *
 */
+ (void)resetLabel:(UILabel *)label
       leftContent:(NSString *)leftStr
          leftFont:(CGFloat)leftFont
       WithContent:(NSString *)cntStr
         rightFont:(CGFloat)rightFont
          fontType:(BOOL)type;

/*  重设控件文本颜色
 *  @param label 重设控件
 *  @param value 金额数值
 */
+ (void)resetTextColorLabel:(UILabel *)label
                      value:(double)value;


/**
 *  通过SymbolId获取产品行情小数位数
 *
 *  @param symbolId 产品ID
 *
 */
+(NSInteger)getQuotoDigitBySymbolId:(uint64_t)symbolId;

/**
 *  拆分行情价格
 *
 *  @param price 行情价格
 *  @param model 产品Model
 *
 */
+ (NSArray *)getArrSplitPrice:(float)price symbolModel:(IXSymbolM *)model;

/**
 *
 *  追加多语言以及产品label入symbolModel
 *  @param symModel 产品model
 *
 */
+ (IXSymbolM *)appendLanguageNameAndLabelBySymbolModel:(NSDictionary *)symDic;


/**
 *
 *  根据产品信息转换产品Model
 *  @param symDic 产品信息
 *
 */
+ (IXSymbolM *)symbolModelBySymbolInfo:(NSDictionary *)symDic;

/**
 *
 *  批量将产品信息转换产品Model
 *  @param symArr 产品信息
 *
 */
+ (NSArray *)symbolModelsBatchBySymbolInfos:(NSArray *)symArr;

//批量保存行情价
+ (BOOL)saveBatchQuotePrice:(NSArray *)modelArr;

//查询行情价
+ (IXQuoteM *)queryQuoteDataBySymbolId:(uint64_t)symbolId;

//保存昨收价
+ (BOOL)saveBatchYesterdayPrice:(NSArray *)modelArr;

//查询昨收价
+ (IXLastQuoteM *)queryYesterdayPriceBySymbolId:(uint64_t)symbolId;

//处理分类以及产品权限
+ (BOOL)dealwithAccGroupSymCata;

/**
 *
 *  处理nil数据
 *  @param str 源字符串信息
 *
 */
+ (NSString *)dealWithNil:(NSString *)str;

/**
 *
 *  去除"-"字符
 *  @param str 源字符串信息
 *
 */
+ (NSString *)dealWithWhiffletree:(NSString *)str;

/**
 *
 *  处理设备名称
 *  @param devName 设备名称
 *
 */
+ (NSString *)dealWithDeviceName:(NSString *)devName;

/**
 *
 *  处理单引号转义
 *  @param str 字段信息
 *
 */
+ (id)dealWithSqlSingleQuote:(id)str;

+ (NSArray *)getMarginSet:(IXSymbolM *)model;

+ (NSArray *)getMarginSetNew:(uint64_t)symbolId;

+ (NSArray *)getMarginByMarginType:(NSString *)marginType;

#warning 多语言
//获取省多语言
+ (NSString *)getProvinceName:(IXProvinceM *)model;
//获取城市多语言
+ (NSString *)getCityName:(IXCityM *)cModel;

//获取账户类型
+ (item_account_group_etype)getCurrentAccountType;

//通过账户类型获取账户组名称
+ (NSString *)getAccountNameByType:(NSInteger)type;

//显示账户类型
+ (NSString *)showCurrentAccountType;

/**
 *   获取文件主目录路径
 *
 *
 *  @return 文件主目录路径
 */
+ (NSString *)documentDirectory;

/**
 *   获取文件路径
 *
 *  @param fileName 文件名
 *
 *  @return 文件路径
 */
+ (NSString *)createFile:(NSString *)fileName;

/**
 *   时间戳格式化时间字符串（HH:mm）
 *
 *  @param timeInterval 当前时间对应时间戳
 *
 *  @return 格式化时间字符串
 */
+ (NSString *)timeIntervalToString:(NSTimeInterval)timeInterval;

+ (NSString *)formaterTimeDisplay:(NSNumber *)index;

+ (NSString *)formatterTimeDisplayHour:(NSNumber *)index;

+ (NSString *)weekdayToWeekString:(int)index;

+ (int)weekStringToWeekday:(NSString *)weekDay;

+ (NSString *)orderTypeByType:(int32_t)type;

+ (NSString *)formatterPhoneDisplay:(NSString *)phone;

+ (NSString *)formatterEmailDisplay:(NSString *)email;

+ (NSString *)formatterBankCardNoDisplay:(NSString *)cardNo;

/**
 *  金钱格式显示逗号和符号
 *  @param value 原数据值
 *  @param hidden 正数"+"是否隐藏
 *
 *
 */
+ (NSString *)moneyFormatterComma:(double)value
               positiveNumberSign:(BOOL)hidden;

/**
 *  asciiCode 转字母
 *  @param asciiCode 原数据值
 *  @param 返回字母
 *
 */
+ (NSString *)asciiCodeToString:(int)asciiCode;

+ (BOOL)isContainString:(NSString *)str withSet:(NSArray *)arr;

+ (int)stringToAsciiCode:(NSString *)str;

+ (NSInteger)dealWithTextNumberLine:(NSString *)text
                          withWidth:(CGFloat)width
                               font:(UIFont*)font;

+ (CGSize)sizeWithString:(NSString*)string
                    font:(UIFont*)font
                   width:(float)width;

+ (UIImage *)getLaunchImage;

//去重数据
+ (NSArray *)uniqData:(NSArray *)arr;

//当前国家编码
+ (NSString *)currentCountryCode;

/**
 *  绑定类型标题
 *
 *
 */
+ (NSString *)bindTypeTitle;

// Dic 转 Json
+ (NSString *)convertToJsonData:(NSDictionary *)dict;
// Json 转 Dic
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

//初始化数据库
+ (void)initDataBase;

//创建动态空表
+ (void)createDynamicTable;

//清空缓存、重新下发数据
+ (BOOL)clearDBCache;

+ (BOOL)saveDBVersion;

@end

