//
//  PHTextView.m
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "PHTextView.h"

@interface PHTextView ()

@property (nonatomic,strong) UILabel *placeholderLabel;

@end

@implementation PHTextView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidChangeNotification
                                                  object:nil];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.placeholderLabel = [[UILabel alloc]init];
        _placeholderLabel.backgroundColor= [UIColor clearColor];
        _placeholderLabel.numberOfLines=0;
        [self addSubview:_placeholderLabel];
        self.placeholderColor= [UIColor lightGrayColor];
        self.font= [UIFont systemFontOfSize:15];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textDidChange)
                                                     name:UITextViewTextDidChangeNotification
                                                   object:self];
        
    }
    return self;
}

- (void)textDidChange
{
    self.placeholderLabel.hidden = self.hasText;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize maxSize = CGSizeMake(self.frame.size.width-10,MAXFLOAT);
    CGFloat height = [self.placeholder boundingRectWithSize:maxSize
                                                                 options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                              attributes:@{NSFontAttributeName:self.placeholderLabel.font}
                                                    context:nil].size.height;
    CGRect rect = CGRectMake(4, 7, self.frame.size.width-10, height);
    self.placeholderLabel.frame = rect;
}

- (void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = placeholder;
    self.placeholderLabel.text= placeholder;
    [self setNeedsLayout];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    self.placeholderLabel.textColor= placeholderColor;
}

- (void)setText:(NSString*)text
{
    [super setText:text];
    [self textDidChange];
}

- (void)setFont:(UIFont*)font
{
    [super setFont:font];
    self.placeholderLabel.font= font;
    [self setNeedsLayout];
}

- (void)setAttributedText:(NSAttributedString*)attributedText
{
    [super setAttributedText:attributedText];
    
    [self textDidChange];
    
}

@end
