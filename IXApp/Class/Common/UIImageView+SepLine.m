
//
//  UIImageView+SepLine.m
//  IXApp
//
//  Created by Bob on 2016/12/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "UIImageView+SepLine.h"

@implementation UIImageView (SepLine)

+ (UIImageView *)addSepImageWithFrame:(CGRect)frame
                            WithColor:(UIColor *)color
                            WithAlpha:(CGFloat)alpha
{
    UIImageView *verSepImageView = [[UIImageView alloc] initWithFrame:frame];
    verSepImageView.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0,0x242a36);
    verSepImageView.alpha = alpha;
    return verSepImageView;
}

/** * 创建纯色的图片，用来做背景 */
+ (UIImage *)switchToImageWithColor:(UIColor *)color
                               size:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, 0, [UIScreen mainScreen].scale);
    [color set];
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    UIImage *ColorImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return ColorImg;
}


@end
