//
//  IXParentCataCell.m
//  IXApp
//
//  Created by Bob on 2016/12/28.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXParentCataCell.h"
#import "IXOpenChoiceV.h"


@interface IXParentCataCell ()

@property (nonatomic, strong) IXOpenChoiceV *choiceView;


@end

@implementation IXParentCataCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        [self.contentView addSubview:self.choiceView];
        self.dk_backgroundColorPicker = DKNavBarColor;
    }
    return self;
}

- (IXOpenChoiceV *)choiceView
{
    if ( !_choiceView ) {
        weakself;
        _choiceView = [[IXOpenChoiceV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 44)
                                                 WithTitle:@"我的"
                                                 WithAlign:TapChoiceAlignCenter
                                                   WithTap:^
        {
            if (weakSelf.chooseRow) {
                weakSelf.chooseRow(weakSelf.tag);
            }
        }];
        _choiceView.dk_backgroundColorPicker = DKNavBarColor;
    }
    return _choiceView;
}

- (void)setChoiceValue:(NSString *)choiceValue
{
    [_choiceView resetTitle:LocalizedString(choiceValue)];
}

- (void)hightedState:(BOOL)state
{
    [_choiceView hightedState:state];
}

@end
