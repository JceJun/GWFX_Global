//
//  CAGradientLayer+IX.m
//  IXApp
//
//  Created by Seven on 2017/12/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "CAGradientLayer+IX.h"
#import <UIKit/UIKit.h>

@implementation CAGradientLayer (IX)

+ (CAGradientLayer *)topShadowWithFrame:(CGRect)frame colors:(NSArray *)colors
{
    CAGradientLayer * topL = [CAGradientLayer layer];
    topL.startPoint = CGPointMake(0.5, 0);
    topL.endPoint = CGPointMake(0.5, 1);
    topL.colors = colors;
    topL.locations = @[@0.f];
    topL.frame = frame;
    
    NSMutableArray  * arr = [@[] mutableCopy];
    for (int i = 0; i < colors.count; i ++) {
        UIColor * clr = colors[i];
        [arr addObject:(__bridge id)clr.CGColor];
    }
    topL.colors = [arr copy];
    
    return topL;
}

+ (CAGradientLayer *)btomShadowWithFrame:(CGRect)frame colors:(NSArray *)colors
{
    CAGradientLayer * btomL = [CAGradientLayer layer];
    btomL.startPoint = CGPointMake(0.5, 0);
    btomL.endPoint = CGPointMake(0.5, 1);
    btomL.colors = colors;
    btomL.locations = @[@0.f];
    btomL.frame = frame;
    
    NSMutableArray  * arr = [@[] mutableCopy];
    for (int i = 0; i < colors.count; i ++) {
        UIColor * clr = colors[i];
        [arr addObject:(__bridge id)clr.CGColor];
    }
    btomL.colors = [arr copy];
    
    return btomL;
}

+ (CAGradientLayer *)topShadowWithFrame:(CGRect)frame dkcolors:(NSArray <DKColorPicker>*)colors
{
    CAGradientLayer * topL = [CAGradientLayer layer];
    topL.startPoint = CGPointMake(0.5, 0);
    topL.endPoint = CGPointMake(0.5, 1);
    topL.dk_colors = colors;
    topL.locations = @[@0.f];
    topL.frame = frame;
    
    return topL;
}

+ (CAGradientLayer *)btomShadowWithFrame:(CGRect)frame dkcolors:(NSArray <DKColorPicker>*)colors
{
    CAGradientLayer * btomL = [CAGradientLayer layer];
    btomL.startPoint = CGPointMake(0.5, 0);
    btomL.endPoint = CGPointMake(0.5, 1);
    btomL.dk_colors = colors;
    btomL.locations = @[@0.f];
    btomL.frame = frame;
    return btomL;
}


@end
