//
//  IXBaseNavVC.m
//  IXApp
//
//  Created by Magee on 16/11/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXBaseNavVC.h"
#import "UIImageView+SepLine.h"

@interface IXBaseNavVC ()
@end

@implementation IXBaseNavVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.dk_backgroundColorPicker = DKTableColor;
    UIImage *colorImage = [UIImageView  switchToImageWithColor:[UIColor clearColor] size:CGSizeMake(self.view.frame.size.width, kLineHeight)];
    
    [self.navigationBar setBackgroundImage:colorImage
                                                  forBarMetrics:UIBarMetricsDefault];
    
    UIColor *color = UIColorHexFromRGB(0xe2eaf2);
    if([IXUserInfoMgr shareInstance].isNightMode){
        color = UIColorHexFromRGB(0x242a36);
    }
    [self.navigationBar setShadowImage:[UIImageView switchToImageWithColor:color size:CGSizeMake(self.view.frame.size.width, kLineHeight)]];
}

@end
