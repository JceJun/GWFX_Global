//
//  IXBtomBtnV.h
//  IXApp
//
//  Created by Seven on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXBtomBtnV : UIView

@property (nonatomic, strong) UIButton  * btn;


/**
 针对iPhone X尺寸会有所不同
 */
+ (CGSize)targetSize;

@end
