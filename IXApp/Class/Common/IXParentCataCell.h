//
//  IXParentCataCell.h
//  IXApp
//
//  Created by Bob on 2016/12/28.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^clickRow)(NSInteger currentRow);
@interface IXParentCataCell : UITableViewCell

@property (nonatomic, strong) NSString *choiceValue;
@property (nonatomic, strong) clickRow chooseRow;

- (void)hightedState:(BOOL)state;

@end
