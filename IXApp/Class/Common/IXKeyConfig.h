//
//  IXKeyConfig.h
//  IXApp
//
//  Created by Bob on 2017/2/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#define EncryptPassword @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCHM9qA9gJoKc7XgXeaO3qrio3fP2KvNeOeLl4uivL7I7yrhkoQ3Y9hxXcP+x7asA6+Cztu6hle2z5DyAG2usMIsilzpOpiW9DaSeA/sdgPR9RaJBGxCk9tt3Jq9qh90kT5x+z8rQh1LcLeY6Wk5RIhAMOw4PaDeD21ucvBQ7D8+QIDAQAB"

/* ***************  交易类型  ************* */
#define LIMITORDER   LocalizedString(@"限价")
#define STOPORDER    LocalizedString(@"停损")
#define MARKETORDER  LocalizedString(@"市价")


#define kIXDomTip       @"kIXDomTip"
#define kRefreashProfit @"kRefreashProfit"
/**保存时区的key*/
#define GTScTimeZone    @"GTSTimeZone"
#define IXServerTime    @"IXServerTime"
#define IXLocaleTime    @"IXLocaleTime"

//去除数字
#define IXnTimeZone     @"IXnTimeZone"

#define kIXHeadIcon     @"IXUserIcon"
#define kIXLoginName    @"IXLoginName"
#define kIXPhoneCode    @"IXPhoneCode"
#define kLoadStartFlag  @"loadStartFlag"
#define kFirstLaunch    @"firstLaunchFlag"
#define kServiceName    [NSString stringWithFormat:@"%d.com.ixdigit.keychain",CompanyID]

#define KFormerSaveGesLock @"KGesLock"  //手势密码应该与userId绑定，原key保留用于处理由此导致的bug
#define KSaveGesLock    [NSString stringWithFormat:@"KGesLock_%llu",[IXUserInfoMgr shareInstance].userLogInfo.user.id_p]

#define KFormerSaveThumbLock  @"KSaveThumbLock" //指纹解锁应该与userId绑定，
#define KSaveThumbLock    [NSString stringWithFormat:@"KSaveThumbLock_%llu",[IXUserInfoMgr shareInstance].userLogInfo.user.id_p]


//市场列表显示类型 NSNumber/NSString型别  0：不显示买入卖出入口 1:显示买入卖出入口
#define kSaveSubSymbolCellStyle         @"SubSymbolCellStyle"
#define kNotificationAddBank            @"NotificationAddBank"
#define kNotificationAcntInfoAddBank    @"NotificationAcntInfoAddBank"
#define kNotificationCerFileUpload      @"NotificationCerFileUpload"
#define kNotificationUnit               @"NotificationUnit"

//取消订阅五级行情
#define kNotifyCancelDetailQuote @"NotifyCancelDetailQuote"

#define kNotifyChangeLine        @"NotifyChangeLine"

// 跳转通知
#define   ONCE_HAPPEN_WithDrawBankListVC_Alert   @"ONCE_HAPPEN_WithDrawBankListVC_Alert"
#define   NOTI_ROOT_GOTO_Deposit   @"NOTI_ROOT_GOTO_Deposit" // 通知资产页跳转入金
#define   NOTI_WEBHOME_GOTO_INVITE   @"NOTI_WEBHOME_GOTO_INVITE" // 通知市场页跳转入金
#define   NOTI_GOTO_DPSChannelVC @"跳转通知-入金渠道页"
#define   NOTI_ROOT_GOTO_MARKET  @"NOTI_ROOT_GOTO_MARKET" // 通知RootVC跳转市场页

