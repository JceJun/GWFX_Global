//
//  IXQuoteM.h
//  IXApp
//
//  Created by Bob on 2016/12/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExAutoModel.h"
#import "IxItemTick.pbobjc.h"

@class IXMetaData;

@interface IXQuoteM : ExAutoModel

@property (nonatomic, assign) NSInteger n1970Time;      //成交时间(标准时间)

@property (nonatomic, assign) double nOpen;           //开盘价

@property (nonatomic, assign) double nHigh;            //最高价

@property (nonatomic, assign) double nPrice;           //最新价

@property (nonatomic, strong) NSMutableArray  *BuyPrc;  //买价

@property (nonatomic, strong) NSMutableArray  *BuyVol;  //买量(股)

@property (nonatomic, strong) NSMutableArray  *SellPrc; //卖价

@property (nonatomic, strong) NSMutableArray  *SellVol; //卖量(股)

@property (nonatomic, assign) unsigned int    symbolId; //产品id

- (IXMetaData *)transferToMetaData;

- (id)initWithTick:(item_tick *)tick;

//判断是否是相同的行情模型
- (BOOL)isSameQModel:(NSObject *)model;
@end
