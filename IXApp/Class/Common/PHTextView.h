//
//  PHTextView.h
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHTextView : UITextView

@property(nonatomic,copy) NSString *placeholder;  //文字
@property(nonatomic,strong) UIColor *placeholderColor; //文字颜色

@end
