//
//  IXEntityFormatter.h
//  Communivation
//
//  Created by Bill on 16/10/25.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IXEntityFormatter : NSObject

+ (NSInteger)getLabelColorValue:(NSInteger)index;

//正则表达式判断输入的密码是否只包含数字和字母，然后位数是6-12
+ (BOOL)validPasswd:(NSString *)passwd;

/*国际化错误信息*/
+ (void)showErrorInformation:(NSString *)errorCode;
+ (NSString *)getErrorInfomation:(NSString *)errorCode;

+ (BOOL)isPureInt:(NSString *)str;

+ (BOOL)isPureFloat:(NSString *)str;

+ (CGFloat)getContentWidth:(NSString *)content
                  WithFont:(UIFont *)font;

+ (NSString *)getMarketNameWithMarketId:(NSInteger)marketId;

+ (NSComparisonResult)comparePrice:(float)price
                  WithCheckedPrice:(float)checkPrice;

/**
 *   number转string
 *
 *  @param number 需要转类型的number值
 *
 *  @return 转换后的值
 */
+ (NSString *)numberToString:(NSNumber *)number;

+ (NSString *)integerToString:(uint64)integer;

/**
 *   double转string
 *
 *  @param double 需要转类型的double值
 *
 *  @return 转换后的值
 */
+ (NSString *)doubleToString:(double)price;

/**
 *   uint32转string
 *
 *  @param uint32 需要转类型的uint32值
 *
 *  @return 转换后的值
 */
+ (NSString *)uint32ToString:(uint32)id_p;

/**
 *   获取时间戳
 *
 *  @return 时间戳
 */
+ (NSTimeInterval)getCurrentTimeInterval;

/**
 *   时间戳格式化时间字符串（HH:mm:ss）
 *
 *  @param timeInterval 当前时间对应时间戳
 *
 *  @return 格式化时间字符串
 */
+ (NSString *)timeIntervalToString:(NSTimeInterval)timeInterval;

+ (NSString *)quoteTimeToString:(NSTimeInterval)timeInterval;

+ (NSString *)timeIntervalToString:(NSTimeInterval)timeInterval formate:(NSString *)formate;

+ (NSInteger)getCellStyle;

+ (NSString *)getCurrentTimeFormaterHHMM;

+ (NSString *)weekDayStringWithTimeInterval:(NSTimeInterval)timeInterval;


//在目标target上添加关联对象，属性名propertyname(也能用来添加block)，值value
+ (void)addAssociatedWithtarget:(id)target
               withPropertyName:(NSString *)propertyName
                      withValue:(id)value;

    
+ (id)getAssociatedValueWithTarget:(id)target
                  withPropertyName:(NSString *)propertyName;

+ (BOOL)isRedUpGreenDown;

@end
