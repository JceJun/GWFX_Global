//
//  IXPosDefultView.h
//  IXApp
//
//  Created by Bob on 2017/2/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXPosDefultView : UIView

@property (nonatomic, copy) NSString *imageName;

@property (nonatomic, copy) NSString *tipMsg;

@end
