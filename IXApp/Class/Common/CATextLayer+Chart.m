//
//  CATextLayer+Chart.m
//  IXApp
//
//  Created by Seven on 2017/8/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "CATextLayer+Chart.h"
//#import "CAShapeLayer+Chart.h"

@implementation CATextLayer (Chart)

+ (instancetype)textLayerWithText:(NSString *)text
                         fontName:(NSString *)fontName
                        textColor:(UIColor *)color
                  backgroundColor:(UIColor *)backgroundColor
                            frame:(CGRect)frame
                    textAlignment:(NSTextAlignment)alignment
{
    CATextLayer * layer = [CATextLayer layer];
    layer.alignmentMode = [self stringAlignmentWith:alignment];
    layer.contentsScale = [UIScreen mainScreen].scale;
    layer.frame = frame;
    layer.fontSize = 10.f;
    layer.font = (__bridge CFTypeRef _Nullable)(fontName);
    layer.string = text;
    layer.foregroundColor = color.CGColor;
    if (backgroundColor) {
        layer.backgroundColor = backgroundColor.CGColor;
    }
    return layer;
}

+ (instancetype)textLayerWithText:(NSString *)text
                             font:(UIFont *)font
                        textColor:(UIColor *)color
                  backgroundColor:(UIColor *)backgroundColor
                            frame:(CGRect)frame
                    textAlignment:(NSTextAlignment)alignment
{
    CATextLayer * layer = [CATextLayer layer];
    layer.contentsScale = [UIScreen mainScreen].scale;
    layer.alignmentMode = [self stringAlignmentWith:alignment];
    layer.frame = frame;
    layer.fontSize = font.pointSize;
    layer.font = (__bridge CFTypeRef _Nullable)(font.fontName);
    layer.string = text;
    layer.foregroundColor = color.CGColor;
    if (backgroundColor) {
        layer.backgroundColor = backgroundColor.CGColor;
    }
    return layer;
}

//+ (CAShapeLayer *)shapedTextLayerWith:(NSString *)str
//                            font:(UIFont *)font
//                       textColor:(UIColor *)textColor
//                 backgroundColor:(UIColor *)backgroundColor
//                             textSize:(CGSize)size
//                           frame:(CGRect)frame
//{
//    CGRect  textFrame = CGRectMake(frame.origin.x + 5 + (frame.size.width - 5 - size.width)/2,
//                                   frame.origin.y + (frame.size.height - size.height)/2,
//                                   size.width,
//                                   size.height);
//    CATextLayer * textLay = [CATextLayer textLayerWithText:str
//                                                      font:font
//                                                 textColor:textColor
//                                           backgroundColor:nil
//                                                     frame:textFrame];
//
//    UIBezierPath    * path = [UIBezierPath bezierPath];
//    CGPoint p1 = CGPointMake(frame.origin.x, frame.origin.y + frame.size.height/2);
//    CGPoint p2 = CGPointMake(frame.origin.x + 5, frame.origin.y);
//    CGPoint p3 = CGPointMake(frame.origin.x + frame.size.width, frame.origin.y);
//    CGPoint p4 = CGPointMake(p3.x, p3.y + frame.size.height);
//    CGPoint p5 = CGPointMake(p2.x, p4.y);
//
//    [path moveToPoint:p1];
//    [path addLineToPoint:p2];
//    [path addLineToPoint:p3];
//    [path addLineToPoint:p4];
//    [path addLineToPoint:p5];
//
//    CAShapeLayer    * layer = [CAShapeLayer layerWithPath:path
//                                                lineWidth:0.f
//                                              strokeColor:nil
//                                                fillColor:backgroundColor];
//    [layer addSublayer:textLay];
//
//    return layer;
//}

+ (NSString *)stringAlignmentWith:(NSTextAlignment)alignment
{
    switch (alignment) {
        case NSTextAlignmentLeft:
            return kCAAlignmentLeft;
        case NSTextAlignmentRight:
            return kCAAlignmentRight;
        case NSTextAlignmentNatural:
            return kCAAlignmentNatural;
        case NSTextAlignmentJustified:
            return kCAAlignmentJustified;
        default:
            return kCAAlignmentCenter;
    }
}

@end
