//
//  IXAlertVC.m
//  IXTest
//
//  Created by Evn on 17/2/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAlertVC.h"
#import "AppDelegate.h"
@interface IXAlertVC ()
{
    
}

@property (strong, nonatomic) UIView *alertView;
@property (copy, nonatomic) NSString *topTitle;
@property (copy, nonatomic) NSString *msg;
@property (copy, nonatomic) NSString *cancelTitle;
@property (copy, nonatomic) NSString *sureTitle;

@end

@implementation IXAlertVC

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)msg
                  cancelTitle:(NSString *)cancelTitle
                   sureTitles:(NSString *)sureTitle
{
    if (self = [super init]) {
        _emptyDismiss = YES;
        self.topTitle   = title;
        self.msg        = msg;
        self.sureTitle  = sureTitle;
        self.cancelTitle= cancelTitle;
        
        _textAlignment  = NSTextAlignmentCenter;
        _titleFont  = PF_MEDI(15);
        _msgFont    = PF_MEDI(13);
        _cancelFont = PF_MEDI(15);
        _sureFont   = PF_MEDI(15);
        
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dsimissAction)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)addView:(CGRect)frame
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
    _alertView = [[UIView alloc]init];
    _alertView.bounds = frame;
    _alertView.dk_backgroundColorPicker = DKViewColor;
    _alertView.center = self.center;
    _alertView.layer.cornerRadius = 10;
    _alertView.layer.masksToBounds = YES;
    [self addSubview:_alertView];
    
    CGRect  rect = CGRectMake(0, 25, VIEW_W(_alertView), 22);
    if (!_msg.length) {
        rect = CGRectMake(0, (105-22)/2, VIEW_W(_alertView), 22);
    }
    UILabel *title = [[UILabel alloc]initWithFrame:rect];
    title.text = _topTitle;
    title.textAlignment = NSTextAlignmentCenter;
    title.font = _titleFont;
    title.dk_textColorPicker = DKCellTitleColor;
    
    [_alertView addSubview:title];
    
    
    UILabel *msg = [[UILabel alloc]initWithFrame:CGRectMake(0, GetView_MaxY(title) + 14.5, VIEW_W(_alertView), 15)];
    msg.text = _msg;
    msg.font = _msgFont;
    msg.dk_textColorPicker = DKCellContentColor;
    msg.numberOfLines = 0;
    msg.textAlignment = _textAlignment;
    [_alertView addSubview:msg];
    
    CGSize size = [msg sizeThatFits:CGSizeMake(VIEW_W(msg), MAXFLOAT)];
    msg.frame = CGRectMake(VIEW_X(msg), VIEW_Y(msg), VIEW_W(msg), size.height);
    
    UIView *downCutLine = [[UIView alloc]initWithFrame:CGRectMake(0, VIEW_Y(msg)+VIEW_H(msg) + 28, VIEW_W(_alertView), 1)];
    downCutLine.dk_backgroundColorPicker = DKLineColor;
    [_alertView addSubview:downCutLine];
    
    CGFloat buttonWidth = (_alertView.bounds.size.width-1)/2;
    
    UILabel *cancel = [[UILabel alloc] initWithFrame: CGRectMake(0, downCutLine.frame.origin.y + downCutLine.frame.size.height, buttonWidth, 46)];
    cancel.text = _cancelTitle;
    cancel.dk_textColorPicker = DKCellTitleColor;
    cancel.userInteractionEnabled = YES;
    cancel.textAlignment = NSTextAlignmentCenter;
    cancel.font = _cancelFont;
    [_alertView addSubview:cancel];
    
    UITapGestureRecognizer *cTg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelClick)];
    [cancel addGestureRecognizer:cTg];
    
    
    UILabel *sure = [[UILabel alloc] initWithFrame:CGRectMake(buttonWidth, cancel.frame.origin.y, buttonWidth, VIEW_H(cancel))];
    sure.text = _sureTitle;
    sure.dk_textColorPicker = DKCellTitleColor;
    sure.textAlignment = NSTextAlignmentCenter;
    sure.font = _sureFont;
    sure.userInteractionEnabled = YES;
    [_alertView addSubview:sure];
    
    UITapGestureRecognizer *sTg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnClick)];
    [sure addGestureRecognizer:sTg];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(buttonWidth, VIEW_Y(sure), 1, VIEW_H(sure))];
    line.dk_backgroundColorPicker = DKLineColor;
    [_alertView addSubview:line];
    
    if (!_cancelTitle || 0 == _cancelTitle.length) {
        cancel.hidden = YES;
        line.hidden = YES;
        sure.frame = CGRectMake(0, GetView_MaxY(msg) + 28, VIEW_W(_alertView), 46);
        _alertView.frame = CGRectMake(VIEW_X(_alertView),
                                      VIEW_Y(_alertView),
                                      VIEW_W(_alertView),
                                      VIEW_Y(sure)+VIEW_H(sure));
    } else {
        _alertView.frame = CGRectMake(VIEW_X(_alertView),
                                      VIEW_Y(_alertView),
                                      VIEW_W(_alertView),
                                      VIEW_Y(sure)+VIEW_H(sure));
    }
}

- (void)showView
{
    [self showViewFrame:CGRectMake(0, 0, kScreenWidth - 40, 150)];
}

- (void)showViewFrame:(CGRect)frame
{
    self.frame = [UIScreen mainScreen].bounds;
    NSArray *subViews = [[UIApplication sharedApplication].keyWindow subviews];
    for (UIView *view in subViews) {
        if ([view isKindOfClass:[IXAlertVC class]]) {
            [view removeFromSuperview];
        }
    }
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self addView:frame];
    self.alpha = 1;
}

- (void)btnClick
{
    BOOL    hide = YES;
    if (_expendAbleAlartViewDelegate && [_expendAbleAlartViewDelegate respondsToSelector:@selector(customAlertView:clickedButtonAtIndex:)]) {
        
        _alertView.tag = _index;
        hide = [_expendAbleAlartViewDelegate customAlertView:_alertView clickedButtonAtIndex:1];
    }
    if (hide) {
        [self cancelAction];
    }
}

- (void)cancelClick
{
    BOOL    hide = YES;
    if (_expendAbleAlartViewDelegate && [_expendAbleAlartViewDelegate respondsToSelector:@selector(customAlertView:clickedButtonAtIndex:)]) {
        
        _alertView.tag = _index;
        hide = [_expendAbleAlartViewDelegate customAlertView:_alertView clickedButtonAtIndex:0];
    }
    
    if (hide) {
        [self cancelAction];
    }
}

- (void)cancelAction
{
    [UIView animateWithDuration:0.25f animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)dsimissAction
{
    if (_emptyDismiss) {
        [self cancelAction];
    }
}

@end

