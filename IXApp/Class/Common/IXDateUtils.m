//
//  IXDateUtils.m
//  IXApp
//
//  Created by Bob on 16/12/01.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDateUtils.h"

@implementation IXDateUtils

+ (IXDateUtils *)sharedInstance
{
    static id obj;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ( !obj ) {
            obj = [[IXDateUtils alloc] init];
        }
    });
    return obj;
}

- (instancetype)init
{
    self = [super init];
    if(self){
        _calendar = [NSCalendar currentCalendar];
        _calendar.timeZone = [NSTimeZone timeZoneWithAbbreviation:
                              [IXDateUtils convertTimeZoneString:[IXDateUtils getDefaultTimeZone]]];
        
        _dataformatter = [[NSDateFormatter alloc] init];
        _dataformatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:
                                   [IXDateUtils convertTimeZoneString:[IXDateUtils getDefaultTimeZone]]];
    }
    return self;
}

+ (NSInteger)convertTimeZoneInt:(NSString *)time
{
    NSInteger res = 0;
    NSString *timeZone = time;
    NSString *number = [timeZone substringFromIndex:3];
    res = [number intValue];
    return res;
}

+ (NSString*)convertTimeZoneString:(NSString *)timeZoneStr
{
    NSString *strTimeZone = @"";
    NSString *timeZone = timeZoneStr;
    NSString *number = [timeZone substringFromIndex:3];
    if(number.length == 2){
        NSRange range = {0,1};
        NSRange range1 = {1,1};
        strTimeZone = [NSString stringWithFormat:@"GMT%@0%@00",[number substringWithRange:range],[number substringWithRange:range1]];
    }else{
        strTimeZone = [NSString stringWithFormat:@"GMT%@00",[timeZone substringFromIndex:3]];
    }
    
    return strTimeZone;
}

+ (NSDateComponents *)currentTimeComponents
{
    //系统时间转换成GMT0时间
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger timeOff = [zone secondsFromGMT];
    NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSDate * timeOffDate = timeOffDate = [[NSDate alloc] initWithTimeIntervalSince1970:[timeStamp longValue]];
    timeOffDate = [timeOffDate dateByAddingTimeInterval:-timeOff];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT+0000"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday|NSCalendarUnitHour|NSCalendarUnitMinute;
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:timeOffDate];
    return theComponents;
}

/**获取服务器的默认时区GMT+0或GMT+8*/
+ (NSString *)getDefaultTimeZone
{
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    return zone.abbreviation;
}

/**获取用户保存在本地的时区*/
+ (NSString *)getUserSaveTimeZone
{
    NSString *timeZone;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *name = [ud objectForKey:GTScTimeZone];
    if(name){
        if ( [name rangeOfString:@"("].location != NSNotFound ) {
            name = [name substringToIndex:[name rangeOfString:@"("].location];
        }
        timeZone = name;
    }
    else{
        timeZone = [IXDateUtils getDefaultTimeZone];
    }
    return timeZone;
}


+ (NSString *)convertGMTDatetime:(uint64_t)val
{
    uint64_t time = val;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:[IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:date];
    
    return dateStr;
}

+ (NSString *)ConvertGMTDatetimeHasHourMinue:(uint64_t)val
{
    uint64_t time = val;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh"];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:[IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:date];
    
    return dateStr;
}

+ (NSInteger)ConvertGMT0DatetimeToToday:(NSInteger)val
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:val];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT+0000"];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSDateFormatter *newFormatter = [[NSDateFormatter alloc]init];
    newFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT+0000"];
    [newFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSString *newdateStr = [NSString stringWithFormat:@"%@ 00:00:00",[formatter stringFromDate:date]];
    NSDate *newDate = [newFormatter dateFromString:newdateStr];
    return [newDate timeIntervalSince1970];
}

+ (NSInteger)ConvertGMTDatetimeToToday:(NSInteger)val
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:val];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:[IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSDateFormatter *newFormatter = [[NSDateFormatter alloc]init];
    newFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:[IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [newFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSString *newdateStr = [NSString stringWithFormat:@"%@ 00:00:00",[formatter stringFromDate:date]];
    NSDate *newDate = [newFormatter dateFromString:newdateStr];
    return [newDate timeIntervalSince1970];
}

// 从1970年来星期几
- (NSInteger)weekSince1970FromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSInteger unitFlags = NSCalendarUnitWeekOfYear;
   
    NSDateComponents *comps = [_calendar components:unitFlags fromDate:[NSDate dateWithTimeIntervalSince1970:2*24*60*60] toDate:date options:NSCalendarWrapComponents]; // 从1970年来星期几
    return [comps weekOfYear];
}

- (NSString *)longyyyyMMddHHmmssFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
   
    [_dataformatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)longyyyyMMddHHmmFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    
    [_dataformatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)yyyyMMddFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"yyyy/MM/dd"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)HHmmssFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"HH:mm:ss"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)MdHHmmFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"M/d HH:mm"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)yyyyMdFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"yyyy/MM/dd"];
    return [_dataformatter stringFromDate:date];
}

- (NSDate *)dateFromyyyyMMddHHmmStr:(NSString *)timeStr
{
    
    [_dataformatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    return [_dataformatter dateFromString:timeStr];
}

- (NSString *)HHmmFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"HH:mm"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)mmFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"mm"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)HHFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"HH"];
    return [_dataformatter stringFromDate:date];
}

- (NSString *)ddFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    [_dataformatter setDateFormat:@"dd"];
    return [_dataformatter stringFromDate:date];
}

- (CGFloat)hhFromMin:(NSInteger)timeMin
{
    NSInteger hour = (timeMin/60 +[IXDateUtils convertTimeZoneInt:[IXDateUtils getDefaultTimeZone]])%24;
    CGFloat min = timeMin%60/60.0;
    return hour+min;
}

- (NSInteger)monthFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSInteger unitFlags = NSCalendarUnitMonth;
    NSDateComponents *component = [_calendar components:unitFlags fromDate:date];
    return [component month];
}

- (NSInteger)weekofYearFromSec:(NSTimeInterval)timeSec
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSInteger unitFlags = NSCalendarUnitWeekOfYear;
    NSDateComponents *component = [_calendar components:unitFlags fromDate:date];
    return [component weekOfYear];
}

/**分时图十字光标移动时间*/
- (NSString *)timeShareMoveTimeHHmmFromSec:(NSTimeInterval)timeSec{
 
    NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    [formatter setDateFormat:@"HH:mm"];
    return [formatter stringFromDate:date];
}

/**K线头部时间*/
- (NSString *)kLineHeaderTimeHHmmssFromSec:(NSTimeInterval)timeSec
{
     NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    [formatter setDateFormat:@"HH:mm:ss"];
    return [formatter stringFromDate:date];
}

/**K线十字光标时间*/
- (NSString *)kLineCursorLineTimeLongyyyyMMddHHmmssFromSec:(NSTimeInterval)timeSec
{
     NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    return [formatter stringFromDate:date];
}

/**K线十字光标时间*/
- (NSString *)kLineCursorLineTimeYyyyMdFromSec:(NSTimeInterval)timeSec
{
     NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    [formatter setDateFormat:@"yyyy/M/d"];
    return [formatter stringFromDate:date];
}

/**K线十字光标时间*/
- (NSString *)kLineCursorLineTimeMdHHmmFromSec:(NSTimeInterval)timeSec
{
     NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    [formatter setDateFormat:@"M/d HH:mm"];
    return [formatter stringFromDate:date];
}

/**K线下边的时间*/
- (NSString *)kLineBottonTimeYyyyMdFromSec:(NSTimeInterval)timeSec
{
    NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    [formatter setDateFormat:@"yyyy/M/d"];
    return [formatter stringFromDate:date];
}

- (NSString *)kLineBottonTimeMdHHmmFromSec:(NSTimeInterval)timeSec
{
    NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    [formatter setDateFormat:@"M/d HH:mm"];
    return [formatter stringFromDate:date];
}
/**价格明细显示时间*/
- (NSString *)priceDetailTime:(NSTimeInterval)timeSec
{
    NSString *timeZone = [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]];
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
        [formatter setDateFormat:@"HH:mm:ss"];
    return [formatter stringFromDate:date];
}

@end
