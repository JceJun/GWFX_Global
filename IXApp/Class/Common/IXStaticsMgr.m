//
//  IXStaticsMgr
//  ixApp
//
//  Created by Seven on 2017/4/12.
//  Copyright © 2017年 Seven. All rights reserved.
//

#import "IXStaticsMgr.h"
#import "RSA.h"
#import "IXAFRequest.h"
#import "IXKeyConfig.h"
#import "NSString+FormatterPrice.h"
#import "IXDataProcessTools.h"
#import "IXAppUtil.h"
#import "IXSymbolDetailVC.h"
#import "IXCpyConfig.h"

// -- key --
static  NSString    * k_appType     = @"AppType";
static  NSString    * k_companyId   = @"CompanyId";
static  NSString    * k_userId      = @"UserId";
static  NSString    * k_accountId   = @"AccountId";
static  NSString    * k_phoneType   = @"PhoneType";
static  NSString    * k_time        = @"Time";
static  NSString    * k_key     = @"Key";
static  NSString    * k_option  = @"Option";

// -- value --
static  NSString    * v_kLine   = @"KayLine";
static  NSString    * v_kIndx   = @"KayIndex";
static  NSString    * v_openAF  = @"OpenAccountFail";
static  NSString    * v_symbolL = @"SymbolList";
static  NSString    * v_loginSuccess = @"LoginSuccess";
static  NSString    * v_loginFail = @"LoginFailure";

#define kAppAdfs        @"/app/appAdfs" //APP激活统计
#define kAddEventTrack  @"/app/appAddEventTrack" //新增埋点

@interface IXUserStatisticsOperation : NSInvocationOperation

//行情页索引
@property (nonatomic, assign)NSUInteger symbolPageIndex;

@end

@implementation IXUserStatisticsOperation
@end

@interface IXStaticsMgr ()

/** K线类别 */
@property (nonatomic, strong) NSArray   * kT;
/** K线指标 */
@property (nonatomic, strong) NSArray   * kI;
/** 存储需要在显示时埋点的页面，通过plist文件获取内容 */
@property (nonatomic, strong) NSDictionary  * pageInfo;
/** 用于处理行情分类页面的切换（页面停留2s之后上传页面的埋点数据） */
@property (nonatomic, strong) NSMutableArray * symbolOperationArr;

/** 记录开始登录trade时间 */
@property (nonatomic, strong) NSDate    * loginTradeDate;

/** 处理登录trade逻辑 */
@property (nonatomic, strong) NSInvocationOperation * loginTradeOperation;

@end


@implementation IXStaticsMgr

+ (IXStaticsMgr *)shareInstance{
    static IXStaticsMgr * instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [IXStaticsMgr new];
    });
    return instance;
}

#pragma mark -
#pragma mark - notification

//进入页面
- (void)enterPage:(id)vc
{
    NSString    * clsName = NSStringFromClass([vc class]);
    //pageInfo中包含的vc为需要埋点的页面
    if (!self.pageInfo[clsName]) return;
    
    NSMutableDictionary * uploadDic = [self uploadInfo];
    [uploadDic setValue:self.pageInfo[clsName] forKey:k_key];
    
    //详细报价页面特殊处理
    if ([vc isKindOfClass:[IXSymbolDetailVC class]]) {
        IXSymbolDetailVC    * v = (IXSymbolDetailVC *)vc;
        [uploadDic setValue:@(v.tradeModel.symbolModel.id_p) forKey:k_option];
    }
    
//    DLog(@"埋点 -- 进入页面 : %@ ; key : %@\n ",clsName,self.pageInfo[clsName]);
    [self uploadUserStatisticsDataWith:uploadDic];
}

//K线类型更新
- (void)updateKlineType:(NSInteger)index
{
    NSMutableDictionary * uploadDic = [self uploadInfo];
    [uploadDic setValue:v_kLine forKey:k_key];
    
    if (self.kT.count > index) {
        [uploadDic setValue:self.kT[index] forKey:k_option];
//        DLog(@"埋点 -- k线类型更新为 %@",self.kT[index]);
        [self uploadUserStatisticsDataWith:uploadDic];
    }else{
        ELog(@"埋点 - k线类型更新失败");
    }
}

//K线指标更新
- (void)updateKlineIndexWithSection:(NSInteger)section index:(NSInteger)index
{
    NSMutableDictionary * uploadDic = [self uploadInfo];
    [uploadDic setValue:v_kIndx forKey:k_key];
    
    if (self.kI.count > section && [(NSArray *)self.kI[section] count] > index){
        NSString    * option    = self.kI[section][index];
        [uploadDic setValue:option forKey:k_option];
        [self uploadUserStatisticsDataWith:uploadDic];
//        DLog(@"埋点 -- k线指标更新为：%@",option);
    }else{
        ELog(@"埋点 - k线指标更新失败");
    }
}

//开户失败
- (void)buildAccountFailure
{
//    DLog(@"埋点 -- 开户失败");
    NSMutableDictionary * uploadDic = [self uploadInfo];
    [uploadDic setValue:v_openAF forKey:k_key];
    
    [self uploadUserStatisticsDataWith:uploadDic];
}

//切换行情
- (void)changeSymbolPageWithCataID:(NSInteger)cataId
{
    //2s内不切换行情类别页面，则上传，否则取消
    BOOL    exist = NO;
    for (IXUserStatisticsOperation * op in self.symbolOperationArr) {
        if (op.symbolPageIndex == cataId) {
            exist = YES;
        }else{
            [op cancel];
        }
    }
    
    if (!exist) {
        IXUserStatisticsOperation   *op = [[IXUserStatisticsOperation alloc] initWithTarget:self
                                                                                   selector:@selector(handleSymbolPageChange:)
                                                                                     object:@(cataId)];
        op.symbolPageIndex = cataId;
        [self.symbolOperationArr addObject:op];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (!op.isCancelled) {
                [op start];
            }
            
            [self.symbolOperationArr removeAllObjects];
        });
    }
}

- (void)handleSymbolPageChange:(id)cataId
{
//    DLog(@"埋点 -- 市场行情页已更改, cataId : %@",cataId);
    NSMutableDictionary * uploadDic = [self uploadInfo];
    
    [uploadDic setValue:v_symbolL forKey:k_key];
    [uploadDic setValue:cataId forKey:k_option];
    
    [self uploadUserStatisticsDataWith:uploadDic];
}


/** 开始登录trade */
- (void)beginLoginTrade
{
    _loginTradeDate = [NSDate date];
    
    if (_loginTradeOperation && !_loginTradeOperation.isCancelled) {
        [_loginTradeOperation cancel];
    }
    _loginTradeOperation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                selector:@selector(loginTimeOut)
                                                                  object:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_loginTradeOperation start];
    });
}

/** 登录trade成功／登录失败 */
- (void)endLoginTrade:(BOOL)success errorCode:(uint32_t)code
{
    if (_loginTradeOperation && !_loginTradeOperation.isCancelled) {
        [_loginTradeOperation cancel];
    }
    
    if (!_loginTradeDate) {
        return;
    }
    
    NSInteger   timeInterval = (NSUInteger)[[NSDate date] timeIntervalSinceDate:_loginTradeDate];
    [self loginSuccess:success time:timeInterval errorCode:code];
    _loginTradeDate = nil;
}

/** 登录trade超时 */
- (void)loginTimeOut
{
    [self loginSuccess:NO time:10 errorCode:-999];
}

/**
 trade登录状态
 
 @param success 成功与否，YES为登录成功，NO表示登录失败
 @param time 登录trade耗费的时间
 */
- (void)loginSuccess:(BOOL)success time:(NSInteger)time errorCode:(uint32_t)errorCode
{
//    DLog(@"埋点 -- 登录trade成功 : %@， 耗时 : %lds",success ? @"YES" : @"NO",time);
    NSMutableDictionary * uploadDic = [self uploadInfo];
    
    NSString    * k = success ? v_loginSuccess : v_loginFail;
    [uploadDic setValue:k forKey:k_key];
    if (success) {
        [uploadDic setValue:@(time) forKey:k_option];
    }else{
        [uploadDic setValue:@(errorCode) forKey:k_option];
    }
    
    [self uploadUserStatisticsDataWith:uploadDic];
}

/** 上传用户统计数据 */
- (void)uploadUserStatisticsDataWith:(NSDictionary *)info
{
    NSString    * url = [NSString formatterUTF8:kAddEventTrack];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&_timestamp=%ld&jsonStr=\"%@\"",
                              url,
                              [timeStamp longValue] * 1000,
                              [IXDataProcessTools dictionaryToJson:info]];
    
    NSString    * paramSign = [RSA encryptString:paramStr
                                       publicKey:CompanyKey];
    
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign,
                                @"_token":@""
                                };
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [IXAFRequest uploadUserStatisticsWithJsondata:param result:^(BOOL respont, BOOL success, id obj) {
            NSDictionary    * dic = obj;
            if (![dic isKindOfClass:[NSDictionary class]]
                || ![dic[@"newRet"] isEqualToString:@"OK"]) {
                ELog([NSString stringWithFormat:@"   -- %@ --",obj]);
            }
        }];
    });
}


/** 激活设备统计 */
- (void)activateDevice
{
    NSString    * url = [NSString formatterUTF8:kAppAdfs];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    
    NSDictionary    * info = @{
                               @"companyId":@CompanyID,
                               @"agentId":@"0",
                               @"deviceId":[IXAppUtil idfa],
                               @"phoneType":[IXAppUtil deviceType],
                               @"appVersion":[IXAppUtil appVersion],
                               @"channel":AppMarket, //@"App Store",
                               @"operationSystem":@"ios",
                               };
    
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&_timestamp=%ld&param=%@",
                              url,
                              [timeStamp longValue] * 1000,
                              [IXDataProcessTools dictionaryToJson:info]];
    
    NSString    * paramSign = [RSA encryptString:paramStr
                                       publicKey:CompanyKey];
    
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign,
                                @"_token":@""
                                };
    [IXAFRequest deviceActivateStatisticWithParam:param];
}

#pragma mark -
#pragma mark - getter

- (NSArray *)kT
{
    if (!_kT) {
        _kT = @[
                @"分时",
                @"日K",
                @"周K",
                @"月K",
                @"",
                @"1min",
                @"5min",
                @"15min",
                @"30min",
                @"60min"];
    }
    return _kT;
}

- (NSArray *)kI
{
    if (!_kI) {
        _kI = @[@[@"MA",@"BBI",@"BOLL",@"MIKE",@"PBX"],
                @[@"MACD",@"ARBR",@"ATR",@"BIAS",@"CCI",@"DKBY",@"KD",@"KDJ",@"LWR",@"QHLSR",@"RSI",@"WR"]];
    }
    return _kI;
}

- (NSDictionary *)pageInfo
{
    if (!_pageInfo) {
        NSString    * filePath = [[NSBundle mainBundle] pathForResource:@"IXStaticsPage" ofType:@"plist"];
        _pageInfo = [NSDictionary dictionaryWithContentsOfFile:filePath];
    }
    return _pageInfo;
}

- (NSMutableDictionary *)uploadInfo
{
    NSMutableDictionary * info = [@{} mutableCopy];
    
    [info setValue:@"" forKey:k_key];
    [info setValue:@"" forKey:k_option];
    
    [info setValue:@"iOS" forKey:k_appType];
    [info setValue:[IXAppUtil deviceType] forKey:k_phoneType];
    [info setValue:@(CompanyID) forKey:k_companyId];
    [info setValue:@([IXUserInfoMgr shareInstance].userLogInfo.account.userid) forKey:k_userId];
    [info setValue:@([IXUserInfoMgr shareInstance].userLogInfo.account.id_p) forKey:k_accountId];
    [info setValue:@((NSUInteger)[[NSDate date] timeIntervalSince1970]) forKey:k_time];
    
    return info;
}

- (NSMutableArray *)symbolOperationArr
{
    if (!_symbolOperationArr) {
        _symbolOperationArr = [@[] mutableCopy];
    }
    return _symbolOperationArr;
}

@end
