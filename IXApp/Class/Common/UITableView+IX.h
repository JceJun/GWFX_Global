//
//  UITableView+IX.h
//  IXApp
//
//  Created by Seven on 2017/12/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (IX)

/**
 section间阴影，阴影高度为10

 @return section header view
 */
- (UIView *)sectionHeaderShadow;

/**
 最后一个section footer view阴影，阴影高度为10

 @return section footer view
 */
- (UIView *)bottomShadow;

@end
