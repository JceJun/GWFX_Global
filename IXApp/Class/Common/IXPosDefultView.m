//
//  IXPosDefultView.m
//  IXApp
//
//  Created by Bob on 2017/2/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPosDefultView.h"

@interface IXPosDefultView ()


@property (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) UIImageView *logoImg;

@end

@implementation IXPosDefultView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if ( self ) {
        ;
    }
    return self;
}

- (void)setTipMsg:(NSString *)tipMsg
{
    _tipMsg = tipMsg;
    self.tipLbl.text = tipMsg;
}

- (void)setImageName:(NSString *)imageName
{
    _imageName = imageName;
    self.logoImg.dk_imagePicker = DKImageNames(imageName, [imageName stringByAppendingString:@"_D"]);
}

- (UIImageView *)logoImg
{
    if ( !_logoImg ) {
        CGFloat originX = (kScreenWidth - 100)/2;
        _logoImg = [[UIImageView alloc] initWithFrame:CGRectMake( originX, 0, 100, 100)];
        [self addSubview:_logoImg];
    }
    return _logoImg;
}
- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 115, kScreenWidth, 22)
                                     WithFont:PF_MEDI(15)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x99abba
                                   dTextColor:0x4c6072
                   ];
        [self addSubview:_tipLbl];
    }
    return _tipLbl;
}
@end
