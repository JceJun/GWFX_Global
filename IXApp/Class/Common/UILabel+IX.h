//
//  UILabel+IX.h
//  IXApp
//
//  Created by Seven on 2017/11/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (IX)

+ (instancetype)labelWithFrame:(CGRect)frame
                          text:(NSString *)text
                      textFont:(UIFont *)font
                     textColor:(DKColorPicker)color;

+ (instancetype)labelWithFrame:(CGRect)frame
                          text:(NSString *)text
                      textFont:(UIFont *)font
                     textColor:(DKColorPicker)color
                 textAlignment:(NSTextAlignment)alignment;

// --------

+ (instancetype)labelWithText:(NSString *)text
                     textFont:(UIFont *)font
                    textColor:(DKColorPicker)color;

+ (instancetype)labelWithText:(NSString *)text
                     textFont:(UIFont *)font
                    textColor:(DKColorPicker)color
                textAlignment:(NSTextAlignment)alignment;

@end
