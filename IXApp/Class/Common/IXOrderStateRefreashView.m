//
//  IXOrderStateRefreashView.m
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOrderStateRefreashView.h"

@interface IXOrderStateRefreashView ()

@property (nonatomic, strong) UIImageView *sepLineImg;

@property (nonatomic, strong) UIImageView *stateImg;

@property (nonatomic, strong) UILabel *tipTitleLbl;

@property (nonatomic, strong) UILabel *tipDesLbl;


@property (nonatomic, strong) UILabel *tipContentLbl;

@property (nonatomic, strong) UIView *leftBackV;

@property (nonatomic, strong) UIView *rightBackV;

@property (nonatomic, assign) BOOL isShow;

@end

@implementation IXOrderStateRefreashView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if ( self ) {
        self.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x303b4d);
   
        UIImageView *sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, 6, 86)];
        sepLineImg.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
        [self.leftBackV addSubview:sepLineImg];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake( kScreenWidth - 98, 1, 31, 31);
        [btn addTarget:self
                action:@selector(responseToHidden)
      forControlEvents:UIControlEventTouchUpInside];
        [btn dk_setImage:DKImageNames(@"common_removeView", @"common_removeView_D")
                forState:UIControlStateNormal];
        [self.rightBackV addSubview:btn];
        
        self.sepLineImg.dk_backgroundColorPicker = DKLineColor;
        self.stateImg.dk_imagePicker = DKImageNames(@"openAccount_complete", @"openAccount_complete_D");
    }
    return self;
}

- (void)responseToHidden
{
    _isShow = NO;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect frame = self.frame;
        frame.size.height = 1;
        self.frame = frame;
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)show
{
    if ( !_isShow ) {
        _isShow = YES;
        
        self.alpha = 0;
        CGRect frame = self.frame;
        frame.size.height = 1;
        self.frame = frame;
        [[UIApplication sharedApplication].keyWindow addSubview:self];

        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.alpha = 1;
            CGRect frame = self.frame;
            frame.size.height = 86;
            self.frame = frame;
        } completion:nil];
    }
}

- (void)setTipTitle:(NSString *)tipTitle
{
    if ( tipTitle && tipTitle.length != 0 ) {
        self.tipTitleLbl.text = tipTitle;
    }
}

- (void)setTipDes:(NSString *)tipDes
{
    if ( tipDes && tipDes.length != 0 ) {
        self.tipDesLbl.text = tipDes;
    }
}


- (void)setTipContent:(NSString *)tipContent
{
    if ( tipContent && tipContent.length != 0 ) {
        self.tipContentLbl.text = tipContent;
    }
}

- (UIView *)leftBackV
{
    if ( !_leftBackV ) {
        _leftBackV = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, 61, 86)];
        _leftBackV.backgroundColor = [UIColor clearColor];
        [self addSubview:_leftBackV];
    }
    return _leftBackV;
}

- (UIView *)rightBackV
{
    if ( !_rightBackV ) {
        _rightBackV = [[UIView alloc] initWithFrame:CGRectMake( 61, 0, kScreenWidth - 61, 86)];
        _rightBackV.backgroundColor = [UIColor clearColor];
        [self addSubview:_rightBackV];
    }
    return _rightBackV;
}

- (UIImageView *)sepLineImg
{
    if ( !_sepLineImg ) {
        _sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 60, 0, 1, 86)];
        [_leftBackV addSubview:_sepLineImg];
    }
    return _sepLineImg;
}


- (UIImageView *)stateImg
{
    if ( !_stateImg ) {
        _stateImg = [[UIImageView alloc] initWithFrame:CGRectMake( 20, 26, 27, 27)];
        [_leftBackV addSubview:_stateImg];
    }
    return _stateImg;
}

- (UILabel *)tipTitleLbl
{
    if ( !_tipTitleLbl ) {
        _tipTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 20, 15, kScreenWidth - 110, 18)
                                          WithFont:PF_MEDI(15)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x4c6072
                                        dTextColor:0xe9e9ea];
        [_rightBackV addSubview:_tipTitleLbl];
    }
    return _tipTitleLbl;
}

- (UILabel *)tipDesLbl
{
    if ( !_tipDesLbl ) {
        _tipDesLbl = [IXUtils createLblWithFrame:CGRectMake( 20, 43, kScreenWidth - 110, 12)
                                        WithFont:PF_MEDI(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0xa7adb5
                                      dTextColor:0x8395a4];
        [_rightBackV addSubview:_tipDesLbl];
    }
    return _tipDesLbl;
}

- (UILabel *)tipContentLbl
{
    if ( !_tipContentLbl ) {
        _tipContentLbl = [IXUtils createLblWithFrame:CGRectMake( 20, 60, kScreenWidth - 110, 12)
                                            WithFont:PF_MEDI(12)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0xa7adb5
                                          dTextColor:0x8395a4];
        [_rightBackV addSubview:_tipContentLbl];
    }
    return _tipContentLbl;
}

@end
