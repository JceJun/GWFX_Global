//
//  IXNoResultView.h
//  IXApp
//
//  Created by Seven on 2017/5/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^tapActionCallback)();


/** 覆盖整个页面的无数据或请求出错提示视图 */
@interface IXNoResultView : UIView

@property (nonatomic, copy) tapActionCallback   tapCallback;

+ (instancetype)noResultViewWithFrame:(CGRect)frame image:(UIImage *)img title:(NSString *)title;
- (instancetype)initWithFrame:(CGRect)frame image:(UIImage *)img title:(NSString *)title;

@end
