//
//  IXBaseNavVC+BtnItem.m
//  IXApp
//
//  Created by Magee on 2017/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBaseNavVC+BtnItem.h"
#import "UIImageView+AFNetworking.h"
#import "IXUserDefaultM.h"
#import "IXNavItem.h"

@implementation IXBaseNavVC (BtnItem)

+ (UIBarButtonItem *)getDefaultBackWithTarget:(id)target sel:(SEL)sel
{
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithImage:GET_IMAGE_NAME(@"common_nav_leftBack")
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:target
                                                                   action:sel];
    leftBarItem.dk_tintColorPicker = DKCellTitleColor;
    [leftBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MarketSymbolNameColor}
                               forState:UIControlStateNormal];
    return leftBarItem;
}

+ (UIBarButtonItem *)getLeftBtnItemWithTitle:(NSString *)title target:(id)target sel:(SEL)sel
{
    IXNavItem  * leftItem = [[IXNavItem alloc] initWithTarget:target
                                                       action:sel
                                                        title:title
                                                  isRightItem:NO];
    
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:leftItem];
    [leftBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MarketSymbolNameColor}
                               forState:UIControlStateNormal];
    leftBarItem.dk_tintColorPicker = DKCellTitleColor;
    return leftBarItem;
}

+ (UIBarButtonItem *)getRightBtnItemWithImg:(UIImage *)img target:(id)target sel:(SEL)sel
{
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithImage:img
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:target
                                                                    action:sel];
    UIColor * titleColor = [IXUserDefaultM isNightMode] ? UIColorHexFromRGB(0xe9e9ea): MarketSymbolNameColor;
    
    [rightBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:titleColor}
                                forState:UIControlStateNormal];
    rightBarItem.dk_tintColorPicker = DKCellTitleColor;
    return rightBarItem;
}

+ (UIBarButtonItem *)getRightBtnItemWithTitle:(NSString *)title target:(id)target sel:(SEL)sel
{
    IXNavItem  * rightItem = [[IXNavItem alloc] initWithTarget:target
                                                        action:sel
                                                         title:title
                                                   isRightItem:YES];
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:rightItem];
    [rightBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MarketSymbolNameColor}
                                forState:UIControlStateNormal];
    rightBarItem.dk_tintColorPicker = DKCellTitleColor;
    return rightBarItem;
}

+ (UIBarButtonItem *)getRightItemWithImg:(UIImage *)img target:(id)target sel:(SEL)sel aimWidth:(CGFloat)width
{
    UIButton    * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, 35)];
    [btn setImage:img forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(5, width - 25, 5, 0)];
    [btn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    item.dk_tintColorPicker = DKCellTitleColor;
    
    return item;
}

+ (UIBarButtonItem *)getRightItemWithDayImg:(UIImage *)dImg nightImg:(UIImage *)nImg target:(id)target sel:(SEL)sel aimWidth:(CGFloat)width
{
    UIButton    * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, 35)];
    [btn dk_setImage:DKImageWithImgs(dImg,nImg) forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(5, width - 25, 5, 0)];
    [btn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    item.dk_tintColorPicker = DKCellTitleColor;
    
    return item;
}

@end

