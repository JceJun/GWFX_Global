//
//  IXTipAlertView.m
//  IXApp
//
//  Created by Larry on 2018/5/12.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXTipAlertView.h"
#import "UIViewExt.h"
#import "UIKit+Block.h"

@interface IXTipAlertView()
@property(nonatomic,copy) void(^btnBlock)(int index);
@property (nonatomic, strong) dispatch_source_t timer;
@property(nonatomic,strong) UIView *bottomV;
@property(nonatomic,strong)UIButton *okBtn;
@property(nonatomic,strong)UILabel *lb_message;
@property(nonatomic,strong)UIView *alertV;
@property(nonatomic,assign)NSInteger endIndex;
@end


@implementation IXTipAlertView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        [KEYWINDOW addSubview:self];
        
        [self addSubviews];
    }
    return self;
}

- (void)addSubviews{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 3;
    view.layer.masksToBounds = YES;
    [self addSubview:view];
    
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(40);
        make.right.equalTo(-40);
        make.height.equalTo(250);
        make.center.equalTo(0);
    }];
    _alertV = view;
    
    UIView *bottomV = [UIView new];
    bottomV.layer.borderColor = HexRGB(0x99abba).CGColor;
    bottomV.layer.borderWidth = 0.5;
    [view addSubview:bottomV];
    
    [bottomV makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0).offset(-1);
        make.right.equalTo(0).offset(1);
        make.bottom.equalTo(0).offset(1);
        make.height.equalTo(40);
    }];
    _bottomV = bottomV;
    
}

- (void)addBottomButtons:(NSArray *)titles{
    for (int i = 0; i < titles.count; i ++) {
        UIButton *btn = [UIButton new];
        btn.titleLabel.font = PF_REGU(13);
        [btn setTitleColor:HexRGB(0x99abba) forState:UIControlStateNormal];
        [btn setTitle:titles[i] forState:UIControlStateNormal];
        [_bottomV addSubview:btn];
        
        CGFloat viewW = kScreenWidth - 40*2;
        CGFloat btnW = viewW/titles.count;
        [btn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0 + i * btnW);
            make.width.equalTo(btnW);
            make.centerY.equalTo(0);
        }];
        
        if (i != titles.count-1) {
            UIView *rightLine = [UIView new];
            rightLine.backgroundColor = HexRGB(0x99abba);
            [btn addSubview:rightLine];
            [rightLine makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(0);
                make.width.equalTo(0.5);
                make.top.equalTo(0);
                make.bottom.equalTo(0);
            }];
        }else{
            [btn setTitleColor:HexRGB(0x4c6072) forState:UIControlStateNormal];
            _okBtn = btn;
        }
        
        weakself;
        [btn block_whenTapped:^(UIView *aView) {
            if (weakSelf.btnBlock) {
                weakSelf.btnBlock(i);
            }
            [weakSelf dismiss];
        }];
    }
    
    if (!titles.count) {
        UIButton *btn = [UIButton new];
        btn.titleLabel.font = PF_REGU(15);
        [btn setTitleColor:HexRGB(0x4c6072) forState:UIControlStateNormal];
        [btn setTitle:@"OK" forState:UIControlStateNormal];
        [_bottomV addSubview:btn];
        [btn makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(0);
        }];
    }
}


+ (void)showAlertWithMsg:(NSString *)msg buttonTitles:(NSArray *)btnTitles btnBlock:(void(^)(int index))btnBlock TimeEndAtButton:(NSInteger)endIndex{
    IXTipAlertView *tipV = [IXTipAlertView new];
    tipV.endIndex = endIndex;
    [tipV addBottomButtons:btnTitles];
    [tipV loadMeassage:msg];
    tipV.btnBlock = btnBlock;
    [tipV startGCDTimer];
    
}

- (void)loadTilte:(NSString *)title{
    
}

- (void)loadMeassage:(NSString *)msg{
    _lb_message = [UILabel new];
    _lb_message.font = PINGFANG_MEDI_FONT(14);
    _lb_message.textColor = HexRGB(0x99abba);
    _lb_message.textAlignment = NSTextAlignmentCenter;
    _lb_message.numberOfLines = 0;
    [_alertV addSubview:_lb_message];
    
    [_lb_message makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.right.equalTo(-20);
    }];
    
    CGSize textSize = [msg boundingRectWithSize:CGSizeMake(kScreenWidth - 50*2-20*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_lb_message.font} context:nil].size;
    CGFloat textH = textSize.height;
    self.lb_message.text = msg;
    [self.lb_message makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(-20);
    }];
    
//    if (textH > 250) {
        [self.lb_message.superview updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(30+30+40+textH);
        }];
//    }
}

-(void)startGCDTimer{
    [self stopTimer];
    
    weakself;
    __block int count = 9;
    NSTimeInterval period = 1; //设置时间间隔
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), period * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        // 异步线程
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *timeMsg = [NSString stringWithFormat:@"(%d)",count];
            NSString *btnText = _okBtn.titleLabel.text;
            if ([btnText containsString:@"("]) {
                btnText = [btnText componentsSeparatedByString:@"("][0];
            }
            [_okBtn setTitle:[btnText stringByAppendingString:timeMsg] forState:UIControlStateNormal];
            if (count > 0) {
                count --;
            }else{
                count = 0;
                
                if (weakSelf.btnBlock) {
                    weakSelf.btnBlock(weakSelf.endIndex);
                }
                
                [self dismiss];
            }
        });
        
        
        
        
    });
    
    dispatch_resume(_timer);
}

// 结束
-(void)stopTimer{
    if(_timer){
        dispatch_source_cancel(_timer);
        _timer = nil;
    }
}

- (void)dismiss{
    [self stopTimer];
    [self removeFromSuperview];
}




@end
