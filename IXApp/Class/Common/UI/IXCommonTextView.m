//
//  IXCommonTextView.m
//  IXApp
//
//  Created by Larry on 2018/6/22.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCommonTextView.h"
#import "UITextView+Placeholder.h"
#import "UIKit+Block.h"

@interface IXCommonTextView()
@property(nonatomic,strong)UIImageView *imgArrow;
@end

@implementation IXCommonTextView

-  (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

+ (instancetype)makeViewInSuperView:(UIView *)superView
                              lText:(NSString *)lText
                        rPlaceHoler:(NSString *)rPlaceHolder
{
    IXCommonTextView *bgView = [IXCommonTextView new];
    bgView.dk_backgroundColorPicker = DKNavBarColor;
    [superView addSubview:bgView];
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(kScreenWidth);
    }];
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = PINGFANG_MEDI_FONT(12);
    lb_title.backgroundColor = [UIColor clearColor];
    lb_title.dk_textColorPicker = DKCellTitleColor;
    lb_title.text = lText;
    lb_title.numberOfLines = 0;
    [bgView addSubview:lb_title];
    
    [lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.equalTo(10);
        make.width.equalTo(kScreenWidth*2/5-10-5);
    }];
    bgView.lb_title = lb_title;
    
    UITextView *textView = [UITextView new];
    textView.font = PINGFANG_MEDI_FONT(12);
    textView.backgroundColor = [UIColor clearColor];
    textView.dk_textColorPicker = DKCellTitleColor;
    textView.textAlignment = NSTextAlignmentLeft;
    textView.autocapitalizationType  = UITextAutocapitalizationTypeNone;
    textView.autocorrectionType = UITextAutocorrectionTypeNo;
    if (rPlaceHolder.length) {
        textView.placeholder = rPlaceHolder;
    }
    textView.scrollEnabled = NO;
    if (!rPlaceHolder.length) {
        textView.hidden = YES;
        textView.userInteractionEnabled = NO;
    }
    textView.contentInset = UIEdgeInsetsMake(-8, -5, -100, -100 );
    [bgView addSubview:textView];
    [textView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(kScreenWidth*2/5+5);
        make.top.equalTo(lb_title);
        make.width.equalTo(kScreenWidth*3/5);
        make.height.greaterThanOrEqualTo(15); //大于等于15
        make.height.lessThanOrEqualTo(2000); //小于等于400
    }];
    
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_greaterThanOrEqualTo(lb_title).offset(10).priority(1000);
        make.bottom.mas_greaterThanOrEqualTo(textView).offset(-10).priority(500);
    }];
    bgView.tv_content = textView;
    
    return bgView;
}


+ (instancetype)makeViewUpDownInSuperView:(UIView *)superView
                              uText:(NSString *) uText
                        dPlaceHoler:(NSString *)dPlaceHolder
{
    IXCommonTextView *bgView = [IXCommonTextView new];
    bgView.dk_backgroundColorPicker = DKNavBarColor;
    [superView addSubview:bgView];
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(kScreenWidth);
    }];
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = PINGFANG_MEDI_FONT(12);
    lb_title.backgroundColor = [UIColor clearColor];
    lb_title.dk_textColorPicker = DKCellTitleColor;
    lb_title.text = uText;
    lb_title.numberOfLines = 0;
    [bgView addSubview:lb_title];
    
    [lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.equalTo(10);
        make.width.equalTo(kScreenWidth - 20);
    }];
    bgView.lb_title = lb_title;
    
    UITextView *textView = [UITextView new];
    textView.font = PINGFANG_MEDI_FONT(12);
    textView.backgroundColor = [UIColor clearColor];
    textView.dk_textColorPicker = DKCellTitleColor;
    textView.textAlignment = NSTextAlignmentLeft;
    textView.placeholder = dPlaceHolder;
    textView.autocapitalizationType  = UITextAutocapitalizationTypeNone;
    textView.autocorrectionType = UITextAutocorrectionTypeNo;
    textView.scrollEnabled = NO;
    if (!dPlaceHolder.length) {
        textView.hidden = YES;
        textView.userInteractionEnabled = NO;
    }
    textView.contentInset = UIEdgeInsetsMake(-8, -5, -100, -100);
    [bgView addSubview:textView];
    [textView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.equalTo(lb_title.bottom).offset(10);
        make.width.equalTo(kScreenWidth-20);
        make.height.greaterThanOrEqualTo(15); //大于等于15
        make.height.lessThanOrEqualTo(2000); //小于等于400
    }];
    
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_greaterThanOrEqualTo(textView).offset(-10);
    }];
    bgView.tv_content = textView;
    
    return bgView;
}


- (void)addEnteranceArrow:(void(^)(void))block{
    // 加右箭头 >
    UIImageView *imgArrow = [UIImageView new];
    imgArrow.image = [UIImage imageNamed:@"openAccount_arrow_right"];
    [self addSubview:imgArrow];
    [imgArrow makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(0);
        make.right.equalTo(-15);
    }];
    _imgArrow = imgArrow;
    
    self.tv_content.hidden = NO;
    self.tv_content.userInteractionEnabled = NO;
    [self.tv_content updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(kScreenWidth*3/5-35);
    }];
    
    [self block_whenTapped:^(UIView *aView) {
        if (block) {
            block();
        }
    }];
}

- (void)removeEnteranceArrow{
    [_imgArrow removeFromSuperview];
    self.tv_content.hidden = NO;
    self.tv_content.userInteractionEnabled = YES;
    
    [self.tv_content updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(kScreenWidth*3/5);
    }];
}



@end
