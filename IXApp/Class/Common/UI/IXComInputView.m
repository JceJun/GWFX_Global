//
//  IXInputViewStyleLR.m
//  IXSDKDemo
//
//  Created by Larry on 2018/1/26.
//  Copyright © 2018年 ix. All rights reserved.
//

#import "IXComInputView.h"
#import "Masonry.h"
#import "IXTextField.h"
#import "UIKit+Block.h"
@interface IXComInputView()

@property(nonatomic,copy) void *(^action)(void);

@end

@implementation IXComInputView
{
    dispatch_source_t _timer;
}

-  (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

+ (instancetype)makeViewInSuperView:(UIView *)superView
                       lPlaceHolder:(NSString *)lPlaceHolder
                        rPlaceHoler:(NSString *)rPlaceHolder
{
    IXComInputView *bgView = [IXComInputView new];
    [superView addSubview:bgView];
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView);
        make.right.equalTo(superView);
        make.height.equalTo(50);
    }];
    
    UILabel *lb_left = [UILabel new];
    lb_left.text = lPlaceHolder;
    lb_left.textColor = HexRGB(0x808080);
    lb_left.font = ROBOT_FONT(15);
    [bgView addSubview:lb_left];
    [lb_left makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(25);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    bgView.lb_left = lb_left;
    
    
    IXTextField *tf_right = [IXTextField new];
    tf_right.placeHolderFont = PF_MEDI(13);
    tf_right.autocapitalizationType  = UITextAutocapitalizationTypeNone;
    tf_right.autocorrectionType = UITextAutocorrectionTypeNo;
//    tf_right.clearButtonMode = UITextFieldViewModeWhileEditing;
    [bgView addSubview:tf_right];
    [tf_right makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(100);
        make.right.equalTo(-25);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    
    bgView.tf_right = tf_right;
    
    return bgView;
}

+ (instancetype)makeViewInSuperView:(UIView *)superView
                             lImage:(NSString *)img
                        rPlaceHoler:(NSString *)rPlaceHolder
{
    IXComInputView *bgView = [IXComInputView new];
    [superView addSubview:bgView];
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView);
        make.right.equalTo(superView);
        make.height.equalTo(50);
    }];
    
    UIImageView *img_left = [UIImageView new];
    img_left.image = [UIImage imageNamed:img];
    [bgView addSubview:img_left];
    [img_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(25);
        make.size.equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(bgView);
    }];
    
    bgView.img_left = img_left;
    
    IXTextField *tf_right = [IXTextField new];
    tf_right.placeHolderFont = PF_MEDI(13);
    tf_right.autocapitalizationType  = UITextAutocapitalizationTypeNone;
    tf_right.autocorrectionType = UITextAutocorrectionTypeNo;
//    tf_right.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf_right.placeholder = rPlaceHolder;
    [bgView addSubview:tf_right];
    [tf_right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(img_left.right).offset(18);
        make.right.equalTo(-25 );
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    
    bgView.tf_right = tf_right;
    
    return bgView;
}

+ (IXComInputView *)makeViewInSuperView:(UIView *)superView
                           lPlaceHolder:(NSString *)lPlaceHolder
                           rPlaceHolder:(NSString *)rPlaceHolder
                           topLineStyle:(TopLineStyle)topLineStyle
                        bottomLineStyle:(BottomLineStyle)bottomLineStyle
{
    IXComInputView *bgView = [IXComInputView new];
    [superView addSubview:bgView];
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView);
        make.right.equalTo(superView);
        make.height.equalTo(44);
    }];
    
    UILabel *lb_left = [UILabel new];
    lb_left.text = lPlaceHolder;
    lb_left.dk_textColorPicker = DKGrayTextColor;

    lb_left.font = ROBOT_FONT(13);
    [bgView addSubview:lb_left];
    [lb_left makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.width.equalTo(85);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    bgView.lb_left = lb_left;
    
    UITextField *tf_right = [UITextField new];
    tf_right.font = lb_left.font;
    tf_right.dk_textColorPicker = DKCellTitleColor;
    tf_right.autocapitalizationType  = UITextAutocapitalizationTypeNone;
    tf_right.autocorrectionType = UITextAutocorrectionTypeNo;
//    tf_right.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf_right.placeholder = rPlaceHolder;
    [bgView addSubview:tf_right];
    [tf_right makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lb_left.right).offset(10);
        make.right.equalTo(-15);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    bgView.tf_right = tf_right;
    
    UIView *lb_top = [UIView new];
    lb_top.dk_backgroundColorPicker = DKLineColor;
    [bgView addSubview:lb_top];
    if (topLineStyle == TopLineStyleNone) {
        lb_top.hidden = YES;
    }
    
    [lb_top makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.height.equalTo(0.5);
        if (topLineStyle == TopLineStyleSeparate) {
            make.left.equalTo(15);
        }else{
            make.left.equalTo(0);
        }
        make.right.equalTo(0);
    }];
    
    UIView *lb_bottom = [UIView new];
    lb_bottom.dk_backgroundColorPicker = DKLineColor;
    [bgView addSubview:lb_bottom];
    if (bottomLineStyle == BottomStyleNone) {
        lb_bottom.hidden = YES;
    }
    [lb_bottom makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(0);
        make.height.equalTo(0.5);
        if (bottomLineStyle == BottomStyleSeparate) {
            make.left.equalTo(15);
        }else{
            make.left.equalTo(0);
        }
        make.right.equalTo(0);
    }];
    
    return bgView;
}

+ (IXComInputView *)makeVerifyInSuperView:(UIView *)superView
                             lPlaceHolder:(NSString *)lPlaceHolder
                             rPlaceHolder:(NSString *)rPlaceHolder
                                   action:(void (^)(void))action
{
    IXComInputView *bgView = [IXComInputView new];
    [superView addSubview:bgView];
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView);
        make.right.equalTo(superView);
        make.height.equalTo(50);
    }];
    
    bgView.action = [action copy];
    
    UILabel *lb_left = [UILabel new];
    lb_left.text = lPlaceHolder;
    lb_left.dk_textColorPicker = DKGrayTextColor;
    lb_left.font =  ROBOT_FONT(15);
    [bgView addSubview:lb_left];
    [lb_left makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(25);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    bgView.lb_left = lb_left;
    
    UILabel *btn_verify = [UILabel new];
    btn_verify.dk_textColorPicker = DKCellTitleColor;
    btn_verify.font = RO_REGU(13);
    btn_verify.text = @"发送验证码";
    btn_verify.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:btn_verify];
    [btn_verify makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.right.equalTo(-25);
        make.size.equalTo(CGSizeMake(100, 25));
    }];
    
    [btn_verify block_whenTapped:^(UIView *aView) {
        [bgView verifyBtnClick];
    }];
    bgView.btn_verify = btn_verify;
    
    IXTextField *tf_right = [IXTextField new];
    tf_right.dk_textColorPicker = DKCellTitleColor;
    tf_right.font = lb_left.font;
    tf_right.autocapitalizationType  = UITextAutocapitalizationTypeNone;
    tf_right.autocorrectionType = UITextAutocorrectionTypeNo;
//    tf_right.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf_right.placeholder = rPlaceHolder;
    tf_right.keyboardType = UIKeyboardTypeNumberPad;
    [bgView addSubview:tf_right];
    [tf_right makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(100);
        make.right.equalTo(btn_verify.left).offset(-10);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    bgView.tf_right = tf_right;
    
    return bgView;
}

- (void)addEnteranceArrow{
    // 加右箭头 >
    self.tf_right.userInteractionEnabled = NO;
    UIImageView *imgArrow = [UIImageView new];
    imgArrow.image = [UIImage imageNamed:@"openAccount_arrow_right"];
    [self addSubview:imgArrow];
    
    [imgArrow makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(0);
        make.right.equalTo(-15);
    }];
    
}


- (void)verifyBtnClick
{
    if (_action) {
        _action();
    }
}

- (void)dealloc
{
    [self stopTimer];
}


// 结束
- (void)stopTimer
{
    if(_timer){
        dispatch_source_cancel(_timer);
        _timer = nil;
    }
}

// 开始
-(void)startGCDTimer
{
    [self stopTimer];
    
    __block int timeOut = 59;
    
    NSTimeInterval period = 1.0; //设置时间间隔
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), period * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        // 异步线程
        // 倒计时结束，关闭
        if (timeOut <= 0) {
            dispatch_source_cancel(_timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.btn_verify.text = @"重新发送";
                self.btn_verify.userInteractionEnabled = YES;
                self.btn_verify.textColor = HexRGB(0xe2e2e2);
            });
            
        }else{
            
            int seconds = timeOut % 60;
            
            NSString * timeStr = [NSString stringWithFormat:@"重新发送(%0.2d)",seconds];
            
            if (seconds == 0) {
                timeStr = @"发送验证码";
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.btn_verify.text = timeStr;
                self.btn_verify.userInteractionEnabled = NO;
                self.btn_verify.textColor = HexRGB(0x808080);
            });
            
            timeOut--;
        }
    });
    dispatch_resume(_timer);
}

@end

