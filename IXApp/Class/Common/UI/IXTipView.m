//
//  IXTipView.m
//  IXBTC
//
//  Created by Seven on 2018/3/17.
//  Copyright © 2018年 IX CAPITAL MARKETS(HK) LIMITED. All rights reserved.
//

#import "IXTipView.h"

#define kBaseTag 100

@implementation TipViewM
+ (instancetype)modelWithTitle:(NSString *)title content:(NSString *)content
{
    TipViewM * m = [TipViewM new];
    m.title = title;
    m.content = content;
    return m;
}
@end

@interface IXTipView ()
@property (nonatomic, strong) UIButton  * dismissBtn;
@property (nonatomic, strong) UIView    * btomView;
@property (nonatomic, strong) UILabel   * titleLab;

@property (nonatomic, strong) NSArray   * itemArr;
@property (nonatomic, copy) NSString    * confirmBtnTitle;

@property (nonatomic, strong) NSString  * cancelTitle;  //取消按钮title
@property (nonatomic, strong) NSArray   * btnTitles;    //其他按钮title

@property (nonatomic, copy) tipVBlk     blk;
@property (nonatomic, copy) timeoutBlk  timerBlk;

@property (nonatomic, strong) NSTimer   * timer;
@property (nonatomic, strong) UILabel   * messageLab;
@property (nonatomic, copy) NSString    * message;  //保存提示信息
@property(nonatomic,strong) UIButton *endBtn;

@end

@implementation IXTipView


+ (void)showWithTitle:(NSString *)title
              message:(NSString *)msg
            highLited:(NSArray *)highLightedArr
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)btnTitles
               btnBlk:(tipVBlk)blk
{
   IXTipView *tipV =  [[self class] showWithTitle:title
                message:msg
            cancelTitle:cancelTitle
            otherTitles:btnTitles
                 btnBlk:blk
             timerEnded:nil];
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:msg];
    [highLightedArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *content = obj[@"content"];
        UIFont *font = obj[@"font"];
        UIColor *color = obj[@"color"];
        
        NSRange range = [msg rangeOfString:content];
        [AttributedStr addAttribute:NSFontAttributeName
                                    value:font
                                    range:range];
        
        [AttributedStr addAttribute:NSForegroundColorAttributeName
                              value:color
                              range:range];
        
    }];
    tipV.messageLab.text = @"";
    tipV.messageLab.attributedText = AttributedStr;
}


+ (instancetype)showWithTitle:(NSString *)title
              message:(NSString *)msg
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)btnTitles
               btnBlk:(tipVBlk)blk
{
   return [self showWithTitle:title
                message:msg
            cancelTitle:cancelTitle
            otherTitles:btnTitles
                 btnBlk:blk
             timerEnded:nil];
}

+ (instancetype)showWithTitle:(NSString *)title
              message:(NSString *)msg
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)titles
               btnBlk:(tipVBlk)blk
           timerEnded:(timeoutBlk)timerBlk
{
    TipViewM * m = [TipViewM modelWithTitle:msg content:@""];
    return  [self showWithTitle:title
                itemArr:@[m]
            cancelTitle:cancelTitle
            otherTitles:titles
                 btnBlk:blk
               timerBlk:timerBlk];
}

+ (instancetype)showWithTitle:(NSString *)title
              itemArr:(NSArray<TipViewM *>*)arr
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)btnTitles
               btnBlk:(tipVBlk)blk
{
    return [self showWithTitle:title
                itemArr:arr
            cancelTitle:cancelTitle
            otherTitles:btnTitles
                 btnBlk:blk
               timerBlk:nil];
}

+ (instancetype)showWithTitle:(NSString *)title
              itemArr:(NSArray<TipViewM *>*)arr
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)btnTitles
               btnBlk:(tipVBlk)blk
             timerBlk:(timeoutBlk)timerBlk
{
    IXTipView * tipV = [[IXTipView alloc] initWithFrame:[UIScreen mainScreen].bounds
                                                  title:title
                                                  items:arr
                                            cancelTitle:cancelTitle
                                            otherTitles:btnTitles
                                                    blk:blk
                                               timerBlk:timerBlk];
    [tipV show];
    return tipV;
}

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                        items:(NSArray<TipViewM *>*)arr
                  cancelTitle:(NSString *)cancelTitle
                  otherTitles:(NSArray <NSString*>*)btnTitles
                          blk:(tipVBlk)blk
                     timerBlk:(timeoutBlk)timerBlk
{
    if (self = [super initWithFrame:frame]) {
        _itemArr = arr;
        _cancelTitle = cancelTitle;
        _btnTitles = btnTitles ? btnTitles : @[];
        _blk = blk;
        _timerBlk = timerBlk;
        
        [self createSubview];

        _titleLab.text = title;
    }
    return self;
}

- (void)createSubview
{
    _dismissBtn = [UIButton new];
//    [_dismissBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_dismissBtn];
    [_dismissBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    _btomView = [UIView new];
    _btomView.backgroundColor = [UIColor whiteColor];
    _btomView.layer.cornerRadius = 4.f;
    _btomView.layer.masksToBounds = YES;
    [self addSubview:_btomView];
    [_btomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.width.mas_equalTo(280);
    }];
    
    _titleLab = [UILabel new];
    _titleLab.font = ROBOT_FONT(15);
    _titleLab.textColor = HexRGB(0x4c6072);
    _titleLab.text = LocalizedString(@"提示");
    
    [_btomView addSubview:_titleLab];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(0);
        make.top.equalTo(_btomView).offset(23);
    }];
    
    //内容
    __block UIView  * lastV = _titleLab;
    for (int i = 0; i < _itemArr.count; i++) {
        TipViewM * m = _itemArr[i];
        UILabel * titleLab = [UILabel new];
        titleLab.font = ROBOT_FONT(15);
        titleLab.textColor = HexRGB(0x99abba);
        titleLab.text = m.title;
        
        if (_itemArr.count == 1) {
            _message = m.title;
            _messageLab = titleLab;
        }
        titleLab.numberOfLines = 0;
        [_btomView addSubview:titleLab];
        
        UILabel * contentLab = [UILabel new];
        contentLab.font = ROBOT_FONT(15);
        contentLab.textColor = HexRGB(0x99abba);
        contentLab.text = m.content;
        contentLab.textAlignment = NSTextAlignmentRight;
        [_btomView addSubview:contentLab];
        
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_btomView).offset(25);
            make.top.equalTo(lastV.mas_bottom).offset(17);
            make.right.equalTo(contentLab.mas_left);
            lastV = titleLab;
        }];
        [contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(titleLab);
            make.right.equalTo(_btomView).offset(-25);
        }];
    }
    
    //按钮
    NSMutableArray * titles = [_btnTitles mutableCopy];
    if (_cancelTitle.length) {
        [titles insertObject:_cancelTitle atIndex:0];
    }
    if (!titles.count) {
        [titles addObject:LocalizedString(@"我知道了")];
    }
    
    for (int i = 0; i < titles.count; i ++) {
        UIButton * btn = [UIButton new];
        if (_cancelTitle.length && i == 0) {
            [btn setTitleColor:HexRGB(0x4c6072) forState:UIControlStateNormal];
        } else {
            [btn setTitleColor:HexRGB(0x4c6072) forState:UIControlStateNormal];
        }
        [btn setTitle:titles[i] forState:UIControlStateNormal];
        btn.tag = kBaseTag + i;
        btn.titleLabel.font = ROBOT_FONT(15);
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btomView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(45);
            if (i == 0) {
                make.top.equalTo(lastV.mas_bottom).offset(25);
                make.left.bottom.equalTo(_btomView);
            } else {
                make.top.width.height.equalTo(lastV);
                make.left.equalTo(lastV.mas_right);
            }

            if (i == titles.count - 1) {
                make.right.equalTo(_btomView);
            }
            lastV = btn;
        }];
        
        if (i > 0) {
            UIView  * verLine = [UIView new];
            verLine.backgroundColor = HexRGB(0x99abba);
            [_btomView addSubview:verLine];
            [verLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.left.equalTo(lastV);
                make.width.mas_equalTo(0.5);
            }];
        }
    }
    
    UIView  * horLine = [UIView new];
    horLine.backgroundColor = HexRGB(0x99abba);
    [_btomView addSubview:horLine];
    
    [horLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_btomView);
        make.top.equalTo(lastV);
        make.height.mas_equalTo(0.5);
    }];
    
    _endBtn = lastV;
}

- (void)show
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    _btomView.alpha = 0;
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [UIView animateWithDuration:0.25 animations:^{
        _btomView.alpha = 1;
        if (self.timerBlk) {
            [self startTimer];
        }
    }];
}

- (void)dismiss
{
    [_timer invalidate];
    _timer = nil;
    [UIView animateWithDuration:0.2 animations:^{
        _btomView.alpha = 0;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)btnAction:(UIButton *)btn
{
    if (self.blk) {
        self.blk((int)btn.tag - kBaseTag);
    }
    [self dismiss];
}


+ (void)showTipMsg:(NSString *)msg{
    [IXTipView showWithTitle:LocalizedString(@"提示")
                     message:msg
                 cancelTitle:nil
                 otherTitles:@[LocalizedString(@"我知道了")]
                      btnBlk:nil];
}

- (NSTimer *)timer
{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
    return _timer;
}

- (void)startTimer
{
    repet = 7;
    [self.timer fire];
}

int repet = 0;
- (void)timerAction
{
    repet --;
    
    if (repet <= 0) {
        if (self.timerBlk) {
            self.timerBlk();
        }
        
        repet = 0;
        [_timer invalidate];
        _timer = nil;
        [self dismiss];
    }
    
    NSString * msg = [_message stringByAppendingFormat:@"(%ds)",repet];
//    _messageLab.textColor = HexRGB(0x99abba);
//    _messageLab.text = msg;
    
    
    if (_btnTitles.count) {
        [_endBtn setTitle:[_endBtn.titleLabel.text stringByAppendingString:msg] forState:UIControlStateNormal];
    }
}

@end
