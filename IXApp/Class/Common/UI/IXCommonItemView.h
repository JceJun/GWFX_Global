//
//  IXCommonItemView.h
//  IXApp
//
//  Created by Larry on 2018/6/27.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, TopLineStyle) {
    TopLineStyleNone,
    TopLineStyleFull,
    TopLineStyleShadow,
};

typedef NS_ENUM(NSUInteger, BottomLineStyle) {
    BottomStyleNone,
    BottomStyleFull,
    BottomStyleShadow,
};
@interface IXCommonItemView : UIView
@property(nonatomic,strong)UILabel *lb_left;
@property(nonatomic,strong)UILabel *lb_right;
@property(nonatomic,strong)UIImageView *imgArrow;
@property(nonatomic,assign)TopLineStyle topLineStyle; // 上面是分割线
@property(nonatomic,assign)BottomLineStyle bottomLineStyle; // 下面是分割线

- (void)addEnteranceArrow;
- (void)addEnteranceArrowAndStaus:(NSString *)statusImage block:(void(^)(void))block;

+ (instancetype)makeViewInSuperView:(UIView *)superView
                       lText:(NSString *)lText
                       rText:(NSString *)rText
                       topLineStyle:(TopLineStyle)topLineStyle
                    bottomLineStyle:(BottomLineStyle)bottomLineStyle;
@end
