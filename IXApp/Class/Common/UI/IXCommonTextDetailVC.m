//
//  IXCommonTextDetailVC.m
//  IXApp
//
//  Created by Larry on 2018/8/15.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCommonTextDetailVC.h"

@interface IXCommonTextDetailVC ()
@property(nonatomic,strong)UITextView *textView;
@property(nonatomic,copy)NSString *name;

@end

@implementation IXCommonTextDetailVC


+ (instancetype)makeControllerWithTitle:(NSString *)title{
    IXCommonTextDetailVC *vc = [IXCommonTextDetailVC new];
    vc.name = title;
    return vc;
}


- (UITextView *)textView{
    if (!_textView) {
        _textView = [UITextView new];
        _textView.font = ROBOT_FONT(13);
        _textView.editable = NO;
        _textView.bounces = NO;
        _textView.textColor = HexRGB(0x4c6072);
        _textView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_textView];
        
        [_textView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(15);
            make.left.equalTo(15);
            make.right.equalTo(-15);
            make.bottom.equalTo(0);
        }];
        
    }
    return _textView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title =self.name;
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    [self addBackItem];
    [self textView];
    
    UIColor *color = [UIColor whiteColor];
    if ([IXUserInfoMgr shareInstance].isNightMode) {
        
    }else{
        color = HexRGB(0x4c6072);
    }
    
    if ([self.name isEqualToString:@"Withdrawal rules"]) {
        NSString *text = @"Withdrawal rules  \n1.There will no charge for any withdrawal from GWFX Global unless a specify case as a below.\n2.If the used margin didn't reach 50% of the deposit amount, there'll be charged 6% of the withdrawal amount.\n3.If withdrawal amount is less than 100USD, there will be charged 20USD for handling fee.\n4.Intermediary banks and local bank may also charge fees in the procedure. Please consult the relevant rules of the bank.";
        text = [text stringByAppendingString:@"\n\n"];
        text = [text stringByAppendingString:@"Processing time of withdrawal \nE-wallets – Help2pay or Skrill. Client will receive in e-wallet account within 2-3 business days after withdrawal has been authorised.\nSupporting bank of Help2pay as a below:\nMyr：May Bank, Hong Leong Bank, CIMB, Public Bank, RHB, AMbank Group \nThb: Bangkok Bank, Kasikorn Bank, Krungsri, Gov Savings Bank, SCB \nIDR: BCA, Mandiri, BNI, Bank BRI, CIMB Niaga, SCB \nBank wire - Client will receive in bank account within 5-7 business days from the authorisation of the withdrawal. Withdrawals to a bank account may be subject to further delays sometimes depending on the banking institution and the jurisdiction in question."];
        
        NSArray *highLightedArr = @[@"Withdrawal rules",@"Processing time of withdrawal"];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5;// 字体的行间距
        
        NSDictionary *attributes = @{
                                     NSForegroundColorAttributeName:color,
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:text attributes:attributes];
        [highLightedArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *content = obj;
            UIFont *font = [UIFont boldSystemFontOfSize:15];
//            UIColor *color = color;
            
            NSRange range = [text rangeOfString:content];
            [AttributedStr addAttribute:NSFontAttributeName
                                  value:font
                                  range:range];
            
            [AttributedStr addAttribute:NSForegroundColorAttributeName
                                  value:color
                                  range:range];
            
        }];
        
        self.textView.attributedText = AttributedStr;
    }
}





@end
