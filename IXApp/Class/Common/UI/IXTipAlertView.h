//
//  IXTipAlertView.h
//  IXApp
//
//  Created by Larry on 2018/5/12.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXTipAlertView : UIView


+ (void)showAlertWithMsg:(NSString *)msg buttonTitles:(NSArray *)btnTitles btnBlock:(void(^)(int index))btnBlock TimeEndAtButton:(NSInteger)endIndex;

@end
