//
//  IXCommonItemView.m
//  IXApp
//
//  Created by Larry on 2018/6/27.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCommonItemView.h"
#import "UIKit+Block.h"
#import "UIViewExt.h"
@interface IXCommonItemView()



@end


@implementation IXCommonItemView

+ (instancetype)makeViewInSuperView:(UIView *)superView
                           lText:(NSString *)lText
                           rText:(NSString *)rText
                           topLineStyle:(TopLineStyle)topLineStyle
                    bottomLineStyle:(BottomLineStyle)bottomLineStyle{
    
    IXCommonItemView *bgView = [IXCommonItemView new];
    bgView.dk_backgroundColorPicker = DKNavBarColor;
    [superView addSubview:bgView];
    [bgView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView);
        make.right.equalTo(superView);
        make.height.equalTo(44);
    }];
    
    UILabel *lb_left = [UILabel new];
    lb_left.text = lText;
    lb_left.dk_textColorPicker = DKCellTitleColor;
    lb_left.font = ROBOT_FONT(13);
    [bgView addSubview:lb_left];
    [lb_left makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    bgView.lb_left = lb_left;
    
    UILabel *lb_right = [UILabel new];
    lb_right.text = rText;
    lb_right.dk_textColorPicker = DKCellContentColor;
    lb_right.textAlignment = NSTextAlignmentRight;
    lb_right.font = ROBOT_FONT(13);
    [bgView addSubview:lb_right];
    [lb_right makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lb_left.right).offset(10);
        make.right.equalTo(-10);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    bgView.lb_right = lb_right;
    
    UIView *line_top = [UIView new];
    line_top.dk_backgroundColorPicker = DKLineColor;
    [bgView addSubview:line_top];
    if (topLineStyle == TopLineStyleNone) {
        line_top.hidden = YES;
    }else if (topLineStyle == TopLineStyleShadow) {
        line_top.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        line_top.layer.shadowOffset = CGSizeMake(0, -2);
        line_top.layer.shadowOpacity = 0.5;
        line_top.layer.masksToBounds = NO;
        superView.clipsToBounds = NO;
    }else{
        
    }
    
    [line_top makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.height.equalTo(0.5);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    
    UIView *line_bottom = [UIView new];
    line_bottom.dk_backgroundColorPicker = DKLineColor;
    [bgView addSubview:line_bottom];
    if (bottomLineStyle == BottomStyleNone) {
        line_bottom.hidden = YES;
    }else if (bottomLineStyle == BottomStyleShadow) {
        line_bottom.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        line_bottom.layer.shadowOffset = CGSizeMake(0, 2);
        line_bottom.layer.shadowOpacity = 0.5;
        line_bottom.layer.masksToBounds = NO;
        superView.clipsToBounds = NO;
    }else{
        
    }
    [line_bottom makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(0);
        make.height.equalTo(0.5);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    
    return bgView;
}

- (void)addEnteranceArrow{
    // 加右箭头 >
    UIImageView *imgArrow = [UIImageView new];
    imgArrow.image = [UIImage imageNamed:@"openAccount_arrow_right"];
    [self addSubview:imgArrow];
    [imgArrow makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(0);
        make.right.equalTo(-15);
    }];
    _imgArrow = imgArrow;
    
    [self.lb_right updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-30);
    }];
}

- (void)addEnteranceArrowAndStaus:(NSString *)statusImage block:(void(^)(void))block{
    [self addEnteranceArrow];
    
    UIImageView *statusImageV = [UIImageView new];
    if ([[statusImage lowercaseString] isEqualToString:@"approved"]) {
        statusImageV.image = [UIImage imageNamed:@"cerFile_checked"];
    }else if ([[statusImage lowercaseString] isEqualToString:@"checking"]){
        statusImageV.image = [UIImage imageNamed:@"cerFile_review"];
    }else{
        statusImageV.image = [UIImage imageNamed:@"cerFile_unload"];
    }
    [self addSubview:statusImageV];
    
    [statusImageV makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(0);
        make.right.equalTo(_imgArrow.left).offset(-5);
    }];
    
    [UIView bondSupperObject:self subObject:statusImageV byKey:@"statusImageV"];

    [self.lb_right updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-56);
    }];
    
    [self block_whenTapped:^(UIView *aView) {
        if (block) {
            block();
        }
    }];
}




@end
