//
//  IXCommonTextView.h
//  IXApp
//
//  Created by Larry on 2018/6/22.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXCommonTextView : UIView
@property(nonatomic,strong) UILabel *lb_title;
@property(nonatomic,strong) UITextView *tv_content;

+ (instancetype)makeViewInSuperView:(UIView *)superView
                              lText:(NSString *)lText
                        rPlaceHoler:(NSString *)rPlaceHolder;

+ (instancetype)makeViewUpDownInSuperView:(UIView *)superView
                                    uText:(NSString *) uText
                              dPlaceHoler:(NSString *)dPlaceHolder;

- (void)addEnteranceArrow:(void(^)(void))block;
- (void)removeEnteranceArrow;
@end
