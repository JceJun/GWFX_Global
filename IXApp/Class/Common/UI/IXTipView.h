//
//  IXTipView.h
//  IXBTC
//
//  Created by Seven on 2018/3/17.
//  Copyright © 2018年 IX CAPITAL MARKETS(HK) LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

#define alert_tip(msg)  [IXTipView showTipMsg:msg]

typedef void(^tipVBlk)(int btnIdx);
typedef void(^timeoutBlk)(void);

@interface TipViewM : NSObject
@property (nonatomic, copy) NSString    * title;
@property (nonatomic, copy) NSString    * content;
+ (instancetype)modelWithTitle:(NSString *)title content:(NSString *)content;
@end

@interface IXTipView : UIView

//NSString *content = obj[@"content"];
//UIFont *font = obj[@"font"];
//UIColor *color = obj[@"color"];
+ (void)showWithTitle:(NSString *)title
              message:(NSString *)msg
            highLited:(NSArray *)highLightedArr  
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)btnTitles
               btnBlk:(tipVBlk)blk;


+ (instancetype)showWithTitle:(NSString *)title
              message:(NSString *)msg
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)titles
               btnBlk:(tipVBlk)blk;


+ (instancetype)showWithTitle:(NSString *)title
              message:(NSString *)msg
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)titles
               btnBlk:(tipVBlk)blk
           timerEnded:(timeoutBlk)timerBlk;


+ (instancetype)showWithTitle:(NSString *)title
              itemArr:(NSArray<TipViewM *>*)arr
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString*>*)titles
               btnBlk:(tipVBlk)blk;

+ (void)showTipMsg:(NSString *)msg;

@end
