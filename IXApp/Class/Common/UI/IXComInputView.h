//
//  IXInputViewStyleLR.h
//  IXSDKDemo
//
//  Created by Larry on 2018/1/26.
//  Copyright © 2018年 ix. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "IXTextField.h"

typedef NS_ENUM(NSUInteger, TopLineStyle) {
    TopLineStyleNone,
    TopLineStyleFull,
    TopLineStyleSeparate,
};

typedef NS_ENUM(NSUInteger, BottomLineStyle) {
    BottomStyleNone,
    BottomStyleFull,
    BottomStyleSeparate,
};

@interface IXComInputView : UIView

@property(nonatomic,strong)UIImageView *img_left;  //  左边是图片
@property(nonatomic,strong)UILabel *lb_left;  //  左边是文字

@property(nonatomic,strong)UITextField *tf_right; // 输入框

@property(nonatomic,strong)UILabel *btn_verify; // 验证码按钮

@property(nonatomic,assign)TopLineStyle topLineStyle; // 上面是分割线
@property(nonatomic,assign)BottomLineStyle bottomLineStyle; // 下面是分割线



+ (instancetype)makeViewInSuperView:(UIView *)superView
                       lPlaceHolder:(NSString *)lPlaceHolder
                        rPlaceHoler:(NSString *)rPlaceHolder; // 无分割线，左边文字，右边文字

+ (instancetype)makeViewInSuperView:(UIView *)superView
                             lImage:(NSString *)img
                        rPlaceHoler:(NSString *)rPlaceHolder; // 无分割线，左边小图标，右边文字

+ (IXComInputView *)makeViewInSuperView:(UIView *)superView
                           lPlaceHolder:(NSString *)lPlaceHolder
                           rPlaceHolder:(NSString *)rPlaceHolder
                           topLineStyle:(TopLineStyle)topLineStyle
                        bottomLineStyle:(BottomLineStyle)bottomLineStyle;

+ (IXComInputView *)makeVerifyInSuperView:(UIView *)superView
                             lPlaceHolder:(NSString *)lPlaceHolder
                             rPlaceHolder:(NSString *)rPlaceHolder
                                   action:(void (^)(void))action;   // 验证码输入框

- (void)addEnteranceArrow;
-(void)startGCDTimer;


@end

