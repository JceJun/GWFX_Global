//
//  IXChooseSymbolCataView.m
//  IXApp
//
//  Created by Bob on 2016/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXChooseSymbolCataView.h"
#import "IXCataCell.h"
#import "IXParentCataCell.h"
#import "IXUserDefaultM.h"

@interface IXChooseSymbolCataView  ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView   * contenttable;
@property (nonatomic, strong) UIButton  * bgBtn;

@end

@implementation IXChooseSymbolCataView

- (id)initWithCataAry:(NSArray *)cataArr
           WithChoose:(chooseSymbolCata)symbolCata
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        //获取保存记录的分类
        //如果没有记录，默认跳转热门
        _currentRow = [IXUserDefaultM getChooseSymbolCata];
        _currentChildCata = -1;
        
        self.chooseSymbol = symbolCata;
        self.dataSourceAry = [NSMutableArray arrayWithArray:cataArr];
        
        UIButton * btn = [[UIButton alloc] initWithFrame:self.bounds];
        btn.backgroundColor = [UIColor clearColor];
        [btn addTarget:self action:@selector(responseToTap) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        [self addSubview:self.bgBtn];
        [self.bgBtn addSubview:self.contenttable];
    }
    
    return self;
}


- (void)show
{
    [self.contenttable reloadData];
    
    CGFloat height =  [_dataSourceAry count]*44;
    CGRect  aimFrame = CGRectMake(0, 0.5, kScreenWidth, height);
    CGRect  beginFrame = CGRectMake(0, - height, kScreenWidth, height);
    _contenttable.frame = beginFrame;
    _bgBtn.alpha = 0;
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _contenttable.frame = aimFrame;
        _bgBtn.alpha = 1.f;
    } completion:nil];
}


- (void)dismiss
{
    if (!self.superview) return;
    
    CGFloat height =  [_dataSourceAry count]*44;
    CGRect  aimFrame = CGRectMake(0, - height, kScreenWidth, height);

    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _contenttable.frame = aimFrame;
        _bgBtn.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataSourceAry count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( _type == Symbol_AllType || _type == Symbol_HotAllType ) {
        return 33;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ( _type == Symbol_AllType || _type == Symbol_HotAllType ) {
        return 40;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    weakself;
    if ( _type == Symbol_AllType || _type == Symbol_HotAllType ) {
        IXCataCell *cell =  [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXCataCell class])];
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        [cell setNameArr:_dataSourceAry[indexPath.row]];
        [cell setSelectedBtnWithTag:self.currentChildCata];
        
        cell.selectedCataInfo = ^(NSInteger tag){
            if (weakSelf.chooseSymbol) {
                weakSelf.currentChildCata = tag;
                [weakSelf dismissWithTag:tag];
            }
        };
        return cell;
    }else if( _type == Symbol_Cata ){
        IXParentCataCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXParentCataCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        id value = _dataSourceAry[indexPath.row];
        if( [value isKindOfClass:[NSDictionary class]] ){
            [cell setChoiceValue:[(NSDictionary *)value objectForKey:@"name"]];
        }else if( [value isKindOfClass:[NSString class]] ){
            [cell setChoiceValue:value];
        }

        [cell hightedState:( cell.tag == _currentRow )];

        cell.chooseRow = ^(NSInteger row){
            weakSelf.currentRow = row;
            [weakSelf dismissWithTag:row];
        };
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    cell.dk_backgroundColorPicker = DKNavBarColor;
    id value = _dataSourceAry[indexPath.row];
    if( [value isKindOfClass:[NSDictionary class]] ){
        cell.textLabel.text = [(NSDictionary *)value objectForKey:@"name"];
    }else if( [value isKindOfClass:[NSString class]] ){
        cell.textLabel.text = value;
    }
    cell.textLabel.textColor = MarketSymbolNameColor;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ( _type == Symbol_AllType || _type == Symbol_HotAllType ) {
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth - 20, 40)];
        UILabel *titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 19, kScreenWidth - 20, 15)
                                               WithFont:PF_MEDI(13)
                                              WithAlign:NSTextAlignmentLeft
                                             wTextColor:0xffffff
                                             dTextColor:0xe9e9ea];
        [headView addSubview:titleLbl];
        titleLbl.text = @"交易所";
        return headView;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissWithTag:indexPath.row];
}


#pragma mark -
#pragma mark - action

- (void)responseToTap
{
    if (self.chooseSymbol) {
        self.chooseSymbol(-1);
    }
    [self dismiss];
}

#pragma mark -
#pragma mark - other

- (void)dismissWithTag:(NSInteger)tag
{
    [self.contenttable reloadData];
    
    if (self.chooseSymbol) {
        self.chooseSymbol(tag);
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}

- (void)setTableFrame
{
    switch ( _type ) {
        case Symbol_Cata:{
            NSInteger count  = (self.dataSourceAry.count >  7) ? 7 : self.dataSourceAry.count;
            CGRect frame = CGRectMake( 0, 0, kScreenWidth, count * 44);
            _contenttable.frame = frame;
        }
            break;
        case Symbol_Trade:{
            CGRect frame = CGRectMake( 60, 150, kScreenWidth - 120, kScreenHeight - 300);
            _contenttable.frame = frame;
        }
            break;
        case Symbol_AllType:{
            [self setSymbolCataTable];
        }
            break;
        case Symbol_HotAllType:{
            [self setSymbolCataTable];
        }
            break;
        default:
            break;
    }
}

- (void)setSymbolCataTable
{
    CGFloat offsetY = ( _type == Symbol_AllType ) ? 33 : 73;
    CGRect frame = CGRectMake( 10, kNavbarHeight + offsetY, kScreenWidth - 20, 150);
    _contenttable.frame = frame;
    _contenttable.scrollEnabled = NO;
    _contenttable.backgroundColor = [UIColor clearColor];
    _contenttable.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIImageView *contentBack =  [[UIImageView alloc] initWithFrame:_contenttable.bounds];
    UIImage *contentImg = [UIImage imageNamed:@"symbolCata_back"];
    contentBack.image = [contentImg stretchableImageWithLeftCapWidth:contentImg.size.width/2
                                                        topCapHeight:contentImg.size.height/2];
    _contenttable.backgroundView = contentBack;
}

#pragma mark -
#pragma mark - setter

- (void)setDataSourceAry:(NSMutableArray *)dataSourceAry
{
    _dataSourceAry = dataSourceAry;
    if (_type != Symbol_Cata) {
        [self setTableFrame];
        [_contenttable reloadData];
    }
}

- (void)setType:(Symbol_Type)type
{
    _type = type;
    [self setTableFrame];
}

#pragma mark -
#pragma mark - lazy loading


- (UIButton *)bgBtn
{
    if ( !_bgBtn ) {
        _bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,
                                                            kNavbarHeight,
                                                            kScreenWidth,
                                                            kScreenHeight - kNavbarHeight)];
        _bgBtn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
        _bgBtn.clipsToBounds = YES;
        [_bgBtn addTarget:self action:@selector(responseToTap) forControlEvents:UIControlEventTouchUpInside];
        
        UIView  * line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
        line.dk_backgroundColorPicker = DKLineColor;
        [_bgBtn addSubview:line];
    }
    return _bgBtn;
}


- (UITableView *)contenttable
{
    if ( !_contenttable ) {
        _contenttable = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                      0.5,
                                                                      kScreenWidth,
                                                                      [_dataSourceAry count]*44)];
        _contenttable.delegate = self;
        _contenttable.dataSource = self;
        _contenttable.dk_backgroundColorPicker = DKTableColor;
        _contenttable.dk_separatorColorPicker = DKLineColor;
        _contenttable.scrollEnabled = NO;
//        _contenttable.separatorInset = UIEdgeInsetsMake( -1, -10, 0, 0);
        _contenttable.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_contenttable  registerClass:[UITableViewCell class]
               forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        _contenttable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_contenttable registerClass:[IXCataCell class]
              forCellReuseIdentifier:NSStringFromClass([IXCataCell class])];
        [_contenttable registerClass:[IXParentCataCell class]
              forCellReuseIdentifier:NSStringFromClass([IXParentCataCell class])];
    }

    return _contenttable;
}


@end
