//
//  UIViewController+navItem.h
//  FXApp
//
//  Created by Seven on 2017/7/18.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>


/** 只在self.navigationController不为nil下有效 */
@interface UIViewController (navItem)


/** 普通的返回按钮 */
- (void)addBackItem;
// 取消按钮 
- (void)addCancelItem;

- (void)addBackItemrget:(id)target action:(SEL)sel;

/** 带头像的返回按钮 */
- (void)addAvatarLeftItemWithTarget:(id)targett selector:(SEL)sel;

/** 显示用户头像和昵称 */
- (void)addAvatarLeftItemWithTarget:(id)targett selector:(SEL)sel nickName:(NSString *)nickName;


/** 导航栏右侧按钮*/
- (void)addDismissItemWithTitle:(NSString *)title;
/** 导航栏右侧按钮 */
- (void)addDismissItemWithTitle:(NSString *)title target:(id)target action:(SEL)sel;
/** 导航栏右侧按钮,夜间模式图片命名为imgName_D */
- (void)addRightItemWithImageNamed:(NSString *)imgName target:(id)target action:(SEL)sel;
- (void)addRightItemWithDayImgNamed:(NSString *)dayName nightImgNamed:(NSString *)nightName target:(id)target action:(SEL)sel;

// 返回指定页
- (BOOL)goBackTo:(id)vc_Class animated:(BOOL)animated;
@end
