//
//  IXOrderStateRefreashView.h
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
 
@interface IXOrderStateRefreashView : UIView

@property (nonatomic, strong) NSString *tipTitle;

@property (nonatomic, strong) NSString *tipDes;

@property (nonatomic, strong) NSString *tipContent;

- (void)show;

@end
