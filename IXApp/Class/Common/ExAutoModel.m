//
//  ExAutoModel.m
//  FXApp
//
//  Created by Larry on 2017/8/6.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "ExAutoModel.h"
#import <objc/message.h>
@interface  ExAutoModel()<NSCopying,NSMutableCopying,NSCoding>

@end

@implementation ExAutoModel

+ (id)transferDataToObject:(NSData *)data{
    id obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return obj;
}

+ (NSData *)transferObjectToData:(id)obj{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:obj];
    return data;
}

// 自动归解档
-(instancetype)initWithCoder:(NSCoder *)aCoder{
    self = [super init];
    if (self) {
        NSArray *properties = [[self class] getAllProperties];
        [properties enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *key = obj;
            id value = [aCoder decodeObjectForKey:key];
            if (value) {
                [self setValue:value forKey:key];
            }
        }];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    NSArray *properties = [[self class] getAllProperties];
    
    [properties enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *key = obj;
        id value = [self valueForKey:key];
        [aCoder encodeObject:value forKey:key];
    }];
}

// 获取所有属性
+ (NSArray *)getAllProperties{
    unsigned int count;
    Ivar *ivars = class_copyIvarList([self class], &count);
    NSMutableArray *propertiesArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0 ; i < count; i++) {
        Ivar ivar = ivars[i];
        const char *name = ivar_getName(ivar);
        [propertiesArray addObject:[NSString stringWithUTF8String:name]];
    }
    free(ivars);
    return propertiesArray;
}


- (id)copyWithZone:(NSZone *)zone
{
    NSObject *objCopy = [[[self class] allocWithZone:zone] init];
    Class clazz = [self class];
    u_int count;
    objc_property_t* properties = class_copyPropertyList(clazz, &count);
    NSMutableArray* propertyArray = [NSMutableArray arrayWithCapacity:count];
    
    for (int i = 0; i < count ; i++)
    {
        const char* propertyName = property_getName(properties[i]);
        [propertyArray addObject:[NSString  stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
    }
    
    free(properties);
    for (int i = 0; i < count ; i++)
    {
        NSString *name=[propertyArray objectAtIndex:i];
        id value=[self valueForKey:name];
        if([value respondsToSelector:@selector(copyWithZone:)]){
            [objCopy setValue:[value copy] forKey:name];
        }else{
            [objCopy setValue:value  forKey:name];
        }
    }
    return objCopy;
}

- (id)mutableCopyWithZone:(NSZone *)zone
{
    NSObject *objCopy = [[[self class] allocWithZone:zone] init];
    Class clazz = [self class];
    u_int count;
    objc_property_t* properties = class_copyPropertyList(clazz, &count);
    NSMutableArray* propertyArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        const char* propertyName = property_getName(properties[i]);
        [propertyArray addObject:[NSString  stringWithCString:propertyName     encoding:NSUTF8StringEncoding]];
    }
    free(properties);
    
    for (int i = 0; i < count ; i++)
    {
        NSString *name=[propertyArray objectAtIndex:i];
        id value=[self valueForKey:name];
        
        if([name isEqualToString:@"registeredPlugins"])
        {DLog(@"");}
        
        if([value respondsToSelector:@selector(mutableCopyWithZone:)]){
            [objCopy setValue:[value mutableCopy] forKey:name];
        }else{
            [objCopy setValue:value forKey:name];
        }
    }
    return objCopy;
}
@end
