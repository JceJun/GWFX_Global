//
//  IXCustomView.m
//  JDTest
//
//  Created by ixiOSDev on 16/11/8.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#import "IXCustomView.h"

@implementation IXCustomView

#pragma mark 创建imageview
+ (UIImageView *)createImageView:(CGRect)frame
                           image:(UIImage *)image
{
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:frame];
    imgView.image = image;
    
    return imgView;
}

+ (UITextField *)createTextField:(CGRect)frame
                 textPlaceholder:(NSString *)placeholder
                           title:(NSString *)title
                            font:(UIFont *)font
                      wTextColor:(NSUInteger)wHexColor
                      dTextColor:(NSUInteger)dHexColor
                   textAlignment:(NSTextAlignment)textAlignment
{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.placeholder = placeholder;
    textField.text = title;
    [textField setFont:font];
    textField.dk_textColorPicker = DKColorWithRGBs(wHexColor, dHexColor);
    textField.textAlignment = textAlignment;
    
    return textField;
}

+ (UILabel *)createLable:(CGRect)frame
                   title:(NSString *)title
                    font:(UIFont *)font
              wTextColor:(NSUInteger)wHexColor
              dTextColor:(NSUInteger)dHexColor
           textAlignment:(NSTextAlignment)textAlignment
{
    if (frame.size.height < 15) {
        frame.size.height = 15;
    }
    UILabel *lbl = [[UILabel alloc] initWithFrame:frame];
    lbl.text = title;
    lbl.dk_textColorPicker = DKColorWithRGBs(wHexColor, dHexColor);
    lbl.font = font;
    lbl.textAlignment = textAlignment;
    
    return lbl;
}

+ (UIButton *)createButton:(CGRect)frame
                     title:(NSString *)title
                      font:(UIFont *)font
                wTextColor:(NSUInteger)wHexColor
                dTextColor:(NSUInteger)dHexColor
             textAlignment:(NSTextAlignment)textAlignment
{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn dk_setTitleColorPicker:DKColorWithRGBs(wHexColor, dHexColor) forState:UIControlStateNormal];
    [btn.titleLabel setFont:font];
    [btn.titleLabel setTextAlignment:textAlignment];
    return btn;
}

@end
