//
//  IXDataBaseVC.h
//  IXApp
//
//  Created by Magee on 16/11/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTCPRequest.h"
#import "IXTradeDataCache.h"
#import "IXQuoteDataCache.h"
#import "IIViewDeckController.h"

@interface IXDataBaseVC : UIViewController

@property(nonatomic,assign)BOOL show;

- (void)setTitleView:(UIView *)titleView title:(NSString *)title;
- (void)showTitleWaitting;
- (void)stopTitleWaitting:(IXTCPConnectStatus)status;
//重设头像
- (void)resetHeadImage;
//重设导航条底部线条
- (void)updateNavBottomLine;
//引导用户去注册和登录
- (void)showRegistLoginAlert;

// 显示进度条
- (void)showProgressWithAimStep:(NSInteger)aimStep totalStep:(NSInteger)total originY:(CGFloat)originY;

@end
