//
//  IXDataCacheMgr.h
//  IXApp
//
//  Created by Evn on 2018/3/16.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDataCacheMgr : NSObject

+ (void)clearLoginInfo;

+ (void)clearAccountInfo;

@end
