//
//  IXADMgr.m
//  IXApp
//
//  Created by Evn on 2018/3/15.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXADMgr.h"

@implementation IXADMgr

static IXADMgr *share = nil;
+ (IXADMgr *)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ( !share ) {
            share = [[IXADMgr alloc] init];
        }
    });
    return share;
}

- (id)init
{
    self = [super init];
    if ( self ){
        _selfAdArr = [NSMutableArray new];
        _homeAdArr = [NSMutableArray new];
        _loginAdArr = [NSMutableArray new];
        _registAdArr = [NSMutableArray new];
        _positionResArr = [NSMutableArray new];
    }
    return self;
}

- (void)clearInstance
{
    [self.selfAdArr removeAllObjects];
    [self.homeAdArr removeAllObjects];
    self.selfBannerClose = NO;
    self.homeBannerClose = NO;
    self.haveCloseSelf = NO;
    self.haveCloseHome = NO;
    self.positionResBannerClose = NO;
}

@end
