//
//  IXDataBaseVC.m
//  IXApp
//
//  Created by Magee on 16/11/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXSweetTitleV.h"
#import "IXTCPRequest_net.h"
#import "IXRootTabBarVC.h"
#import "UIImageView+SepLine.h"

#import "IXPositionHomeVC.h"
#import "IXWebHomeVC.h"
#import "IXMarketHomeVC.h"
#import "IXAssetHomeVC.h"
#import "AppDelegate.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXAlertVC.h"
#import "AppDelegate+UI.h"
#import "IXAcntProgressV.h"
@interface IXDataBaseVC ()<IIViewDeckControllerDelegate,ExpendableAlartViewDelegate>

@property (nonatomic,strong)IXSweetTitleV *titleV;

@property (nonatomic, assign)IXTCPConnectStatus curType;
@property (nonatomic, strong) UILabel   * titleLab;
@property(nonatomic,strong)IXAcntProgressV  *progressV;


@end

@implementation IXDataBaseVC

- (void)dealloc
{
    IXNetStatus_listen_resign(self, IXTradeStatusKey);
    IXNetStatus_listen_resign(self, IXQuoteStatusKey);
    [[IXBORequestMgr shareInstance] removeObserver:self forKeyPath:@"headUrl"];
}

- (id)init
{
    self = [super init];
    if (self) {
        IXNetStatus_listen_regist(self, IXTradeStatusKey);
        IXNetStatus_listen_regist(self, IXQuoteStatusKey);
        [[IXBORequestMgr shareInstance] addObserver:self forKeyPath:@"headUrl"
                                            options:NSKeyValueObservingOptionNew
                                            context:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //去掉系统nabar底部自带的阴影线
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    IIViewDeckController *rootVC = (IIViewDeckController *)delegate.window.rootViewController;
    if( [rootVC isKindOfClass:[IIViewDeckController class]] ){
        IXRootTabBarVC *tabBar = (IXRootTabBarVC *)rootVC.centerController;
        IXBaseNavVC *nav = tabBar.selectedViewController;
        if ([nav.topViewController isKindOfClass:[IXMarketHomeVC class]]||
            [nav.topViewController isKindOfClass:[IXAssetHomeVC class]]||
            [nav.topViewController isKindOfClass:[IXPositionHomeVC class]]) {
            rootVC.panningMode = IIViewDeckFullViewPanning;
        }
        else if ([nav.topViewController isKindOfClass:[IXWebHomeVC class]] &&
                 nav.viewControllers.count == 1) {
            rootVC.panningMode = IIViewDeckFullViewPanning;
        }
        else{
            rootVC.panningMode = IIViewDeckNoPanning;
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    id rootVC =  delegate.window.rootViewController;
    if ( [rootVC isKindOfClass:[IIViewDeckController class]] ) {
        [(IIViewDeckController *)rootVC setPanningMode:IIViewDeckFullViewPanning];
    }
    _show = NO;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _show = YES;
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXNetStatus_isSameKey(keyPath, IXTradeStatusKey)) {
        
        IXTCPConnectStatus status = ((IXTCPRequest *)object).tradeStatus;
        _curType = status;
        switch (status) {
            case IXTCPConnectStatusDisconnect:{
                [self.titleV showNetStatus:IXSweetShowDisCnt];
            }
                break;
            case IXTCPConnectStatusConneting:{
                [self.titleV showNetStatus:IXSweetShowCnting];
            }
                break;
            case IXTCPConnectStatusConnected:{
                [self.titleV showNetStatus:IXSweetShowCnted];
            }
                break;
            case IXTCPConnectStatusMaxTried:{
                [self.titleV showNetStatus:IXSweetShowMaxTried];
            }
                break;
            default:
                break;
        }
    }
    else if (SameString(keyPath, @"headUrl")) {
        [self resetHeadImage];
    }
}

- (void)resetHeadImage{};

- (void)updateNavBottomLine
{
    UIImage *colorImage = [UIImageView  switchToImageWithColor:[UIColor clearColor] size:CGSizeMake(self.view.frame.size.width, kLineHeight)];
    [self.navigationController.navigationBar setBackgroundImage:colorImage
                                                  forBarMetrics:UIBarMetricsDefault];
    UIColor *color = UIColorHexFromRGB(0xe2eaf2);
    if([IXUserInfoMgr shareInstance].isNightMode){
        color = UIColorHexFromRGB(0x242a36);
    }
    [self.navigationController.navigationBar setShadowImage:[UIImageView switchToImageWithColor:color size:CGSizeMake(self.navigationController.view.frame.size.width, kLineHeight)]];
}

#pragma mark -
#pragma mark - TitleV

- (void)setTitle:(NSString *)title
{
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(15)}];
    self.titleLab.frame = CGRectMake((kScreenWidth-size.width)/2, 0, size.width, size.height);
    self.titleLab.text = title;
    [self.titleLab sizeToFit];
    self.navigationItem.titleView = self.titleLab;
}

- (void)setTitleView:(UIView *)titleView title:(NSString *)title
{
    [self.navigationItem setTitleView:titleView];
    _titleV.defaultView = titleView;
    _titleV.defaultTitle = title;
}

- (void)showTitleWaitting
{
    [self.navigationItem setTitleView:self.titleV];
    [self.titleV startWitting];
}

- (void)stopTitleWaitting:(IXTCPConnectStatus)status
{
    [self.titleV stopWitting];
    if ( status == IXTCPConnectStatusConnected ) {
        self.navigationItem.titleView = _titleV.defaultView;
    }else{
        self.navigationItem.titleView = _titleV;
    }
}

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLab.dk_textColorPicker = DKColorWithRGBs(0x232935, 0xe9e9ea);
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font = PF_MEDI(15);
    }
    return _titleLab;
}

- (IXSweetTitleV *)titleV
{
    if (!_titleV) {
        _titleV = [[IXSweetTitleV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth-150, 44)];
        _titleV.reconnectTaped = ^(){
            [[IXTCPRequest shareInstance] reconnect];
        };
    }
    return _titleV;
}

#pragma mark 引导用户去注册和登录
- (void)showRegistLoginAlert
{
    IXAlertVC   *alert = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"温馨提示")
                                                  message:LocalizedString(@"请注册或登录后使用")
                                              cancelTitle:LocalizedString(@"注册")
                                               sureTitles:LocalizedString(@"登录")];
    alert.expendAbleAlartViewDelegate = self;
    [alert showView];
}

- (BOOL)customAlertView:(UIView *)alertView
   clickedButtonAtIndex:(NSInteger)btnIndex
{
    [IXUserInfoMgr shareInstance].isCloseDemo = YES;
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    del.hasLogin = NO;
    if (btnIndex == 0) {
        [AppDelegate showRegist];
    } else {
        [AppDelegate showLogin];
    }
    return YES;
}

- (void)showProgressWithAimStep:(NSInteger)aimStep totalStep:(NSInteger)total originY:(CGFloat)originY
{
    if (_progressV) {
        [_progressV removeFromSuperview];
    }
    _progressV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, originY, kScreenWidth, 5)];
    [self.view addSubview:_progressV];
    if (aimStep > total) {
        ELog(@"进度条步骤出错");
        return;
    }
    [_progressV showFromMole:aimStep - 1 toMole:aimStep deno:total];
}


@end

