//
//  NSArray+Expection.h
//  IXApp
//
//  Created by Bob on 2017/6/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Expection)

- (double)dAtIndex:(NSInteger)index;

@end
