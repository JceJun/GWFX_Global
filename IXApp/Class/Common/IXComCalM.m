//
//  IXComCalM.m
//  IXApp
//
//  Created by Bob on 2017/12/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXComCalM.h"
#import "IXAccountBalanceModel.h"
#import "NSDictionary+Type.h"

@implementation IXComCalM

+ (double)cacCom:(double)comRate vol:(double)vol conSize:(NSInteger)conSize
{
    double commission = 0;
    conSize = conSize ? conSize : 1;
    commission = comRate * vol/conSize;
    return commission;
}


+ (double)cacCom:(double)comRate vol:(double)vol swap:(double)swap price:(double)price
{
    double commission = 0;
    commission = comRate * vol * swap * price;
    return commission;
}

@end
