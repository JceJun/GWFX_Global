//
//  CVProductKindCell.m
//  Communivation
//
//  Created by Bill on 16/10/25.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import "IXSymbolCataCell.h"

@interface IXSymbolCataCell ()

/**
 *   产品类别名
 */
@property (strong, nonatomic) UILabel *symbolCataName;

@end

@implementation IXSymbolCataCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if ( self ) {
        {
            CGRect frame = CGRectMake( 0, [self getOrginY], self.bounds.size.width, [self getFont]);
            _symbolCataName = [[UILabel alloc] initWithFrame:frame];
            _symbolCataName.font = PF_MEDI(13);
            _symbolCataName.textColor = MarketSymbolNameColor;
            _symbolCataName.textAlignment = NSTextAlignmentCenter;
            [self addSubview:_symbolCataName];
        }
    }
    return self;
}

#pragma mark  赋值
- (void)setSymbolCata:(NSDictionary *)symbolCata{
    _symbolCataName.text = [symbolCata objectForKey:@"name"];
}

#pragma mark 坐标相关
- (CGFloat)getHeight{
    return CGRectGetHeight(self.bounds);
}

- (NSInteger)getFont{
    return 14;
}

- (CGFloat)getOrginX{
    return 0;
}

- (CGFloat)getOrginY{
    return ([self getHeight] - [self getFont])/2;
}

@end
