//
//  NSDictionary+Type.h
//  Communivation
//
//  Created by bob on 16/11/1.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Type)

- (uint64_t)int64ForKey:(NSString *)key;

- (double)doubleForKey:(NSString *)key;

- (NSInteger)integerForKey:(NSString *)key;

- (int)intForKey:(NSString *)key;

- (NSString *)stringForKey:(NSString *)key;

@end
