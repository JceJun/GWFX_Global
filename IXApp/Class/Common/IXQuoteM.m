//
//  IXQuoteM.m
//  IXApp
//
//  Created by Bob on 2016/12/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteM.h"
#import "IXMetaData.h"

@implementation IXQuoteM

- (id)initWithTick:(item_tick *)tick
{
    self = [self init];
    if (self) {
        
        _symbolId = tick.id_p;
        _n1970Time = tick.time;
 
        _BuyPrc = [NSMutableArray array];
        _BuyVol = [NSMutableArray array];
        
        _SellPrc = [NSMutableArray array];
        _SellVol = [NSMutableArray array];
    }
    return self;
}



- (IXMetaData *)transferToMetaData
{
    IXMetaData  * meta = [IXMetaData new];
    meta.time   = self.n1970Time;
    meta.open   = self.nOpen;
    meta.close  = self.nPrice;
    meta.low    = self.nPrice;    //使用最新价作为最低价
    meta.high   = self.nHigh;
    meta.id_p   = self.symbolId;
    return meta;
}



- (BOOL)isSameQModel:(NSObject *)model
{
    unsigned int outCount;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (int i = 0; i < outCount ; i++) {
        objc_property_t property = properties[i];
        const char* char_f = property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        
        if ([propertyName isEqualToString:@"n1970Time"]) {
            NSTimeInterval propertyValue = [[self valueForKey:(NSString *)propertyName] longLongValue];
            double n1970Time = [[model valueForKey:propertyName] doubleValue];
            if (propertyValue) {
                if (propertyValue != n1970Time) {
                    return NO;
                }
            }else{
                return NO;
            }
        }else if ([propertyName isEqualToString:@"nPrice"]) {
            double propertyValue = [[self valueForKey:(NSString *)propertyName] doubleValue];
            double nPrice = [[model valueForKey:propertyName] doubleValue];
            if (propertyValue) {
                if (propertyValue != nPrice) {
                    return NO;
                }
            }else{
                return NO;
            }
        }else if ([propertyName isEqualToString:@"BuyPrc"] || [propertyName isEqualToString:@"SellPrc"]){
            NSArray * propertyValue = [self valueForKey:(NSString *)propertyName];
            NSArray * modelArr  = [model valueForKey:propertyName];
            
            if (propertyValue) {
                if (propertyValue.count > 0 && ![modelArr[0] isEqual:propertyValue[0]]) {
                    return NO;
                }
            }else{
                return NO;
            }
        }
    }
    free(properties);
    return YES;
}

@end
