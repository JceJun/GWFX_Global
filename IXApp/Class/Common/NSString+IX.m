//
//  NSString+IX.m
//  IXApp
//
//  Created by Seven on 2017/9/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "NSString+IX.h"

@implementation NSString (IX)


/**
 返回self对应的富文本，数字字体Robot，其他字体PingFang
 
 @param size 字体大小
 @return 富文本
 */
- (NSAttributedString *)attributeStringWithFontSize:(NSInteger)size textColor:(UIColor *)color
{
    return [self attributeStringWithnumberFont:RO_REGU(size) commonFont:PF_MEDI(size) textColor:color];
}


/**
 返回self对应的富文本
 
 @param numFont 数字字体
 @param otherFont 其他字符字体
 @return 富文本
 */
- (NSAttributedString *)attributeStringWithnumberFont:(UIFont *)numFont commonFont:(UIFont *)otherFont textColor:(UIColor *)color
{
    NSString    * tmp = @"1234567890";
    NSMutableArray  * subStrArr = [@[] mutableCopy];
    
    NSInteger   idx = 0;
    if (!self.length) {
        return [NSAttributedString new];
    }
    for (int i = 0; i < self.length - 1; i++) {
        NSString    * former = [self substringWithRange:NSMakeRange(i, 1)];
        NSString    * latter = [self substringWithRange:NSMakeRange(i + 1, 1)];
        
        if (([tmp containsString:former] && ![tmp containsString:latter]) ||
            (![tmp containsString:former] && [tmp containsString:latter])) {
            [subStrArr addObject:[self substringWithRange:NSMakeRange(idx, i + 1 - idx)]];
            idx = i + 1;
        }
        
        if (i + 2 == self.length) {
            [subStrArr addObject:[self substringFromIndex:idx]];
        }
    }
    
    
    //常见数组间分割字符
    NSString    * spacetr = @",，.:：/／- ";
    
    if (subStrArr.count > 2) {
        BOOL stop = NO;
        while (subStrArr.count > 2 && !stop) {
            for (int i = 1; i < subStrArr.count - 1; i++) {
                NSString    * former = subStrArr[i - 1];
                NSString    * center = subStrArr[i];
                NSString    * latter = subStrArr[i + 1];
                
                if (center.length == 1 && [spacetr containsString:center]) {
                    if ([tmp containsString:[former substringToIndex:1]] &&
                        [tmp containsString:[latter substringToIndex:1]]) {
                        NSString * str = [NSString stringWithFormat:@"%@%@%@",former,center,latter];
                        
                        [subStrArr insertObject:str atIndex:i - 1];
                        [subStrArr removeObjectAtIndex:i + 2];
                        [subStrArr removeObjectAtIndex:i + 1];
                        [subStrArr removeObjectAtIndex:i];
                        
                        break;
                    }
                }
                
                if (i == subStrArr.count - 2) {
                    stop = YES;
                }
            }
        }
        
    }
    
    
    NSMutableAttributedString   * attributeStr = [[NSMutableAttributedString alloc] init];
    
    NSDictionary    * numAttr = @{NSFontAttributeName : numFont,
                                  NSForegroundColorAttributeName : color};
    NSDictionary    * textAttr = @{NSFontAttributeName : otherFont,
                                   NSForegroundColorAttributeName : color};
    
    for (int i = 0; i < subStrArr.count; i ++) {
        NSString    * str = subStrArr[i];
        NSString    * firstCharacter = [str substringToIndex:1];
        
        NSAttributedString  * attStr = nil;
        if ([tmp containsString:firstCharacter]) {
            //数字
            attStr = [[NSAttributedString alloc] initWithString:str attributes:numAttr];
        } else {
            //其他字符
            
            attStr = [[NSAttributedString alloc] initWithString:str attributes:textAttr];
        }
        
        [attributeStr appendAttributedString:attStr];
    }
    
    return attributeStr;
}

- (CGSize)sizeWithFont:(UIFont *)font andMaxSize:(CGSize)maxSize
{
    NSDictionary *arrts = @{NSFontAttributeName:font};
    
    return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:arrts context:nil].size;
}

+ (CGSize)sizeWithText:(NSString *)text andFont:(UIFont *)font andMaxSize:(CGSize)maxSize
{
    return [text sizeWithFont:font andMaxSize:maxSize];
}

// 截取字符串
- (NSString *)subStringFrom:(NSString *)startString to:(NSString *)endString{
    if (![self containsString:startString] || ![self containsString:endString]) {
        return @"";
    }
    NSRange startRange = [self rangeOfString:startString];
    NSRange endRange = [self rangeOfString:endString];
    NSRange range = NSMakeRange(startRange.location + startRange.length-1, endRange.location - startRange.location - startRange.length+2);
    return [self substringWithRange:range];
    
}

@end
