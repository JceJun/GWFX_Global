//
//  IXDateUtils.h
//  IXApp
//
//  Created by Bob on 16/12/01.
//  Copyright © 2016年 IX. All rights reserved.
//

/**
 *  用来管理整个时区用
 */
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface IXDateUtils : NSObject

@property(nonatomic,strong)NSDateFormatter *dataformatter;
@property(nonatomic,strong)NSCalendar *calendar;

+(IXDateUtils *)sharedInstance;
/**获取用户保存在本地的时区*/
+(NSString *)getUserSaveTimeZone;
/**获取服务器的默认时区GMT+0或GMT+8*/
+(NSString *)getDefaultTimeZone;
/**将时区转化给整数*/
+ (NSInteger)convertTimeZoneInt:(NSString *)time;

/** 用户自定义时区写死转换成HH:mm:ss*/
+ (NSString *)convertGMTDatetime:(uint64_t)val;
/** 用户自定义时区转换成yyyy-MM-dd HH:mm:ss*/
+ (NSString *)ConvertGMTDatetimeHasHourMinue:(uint64_t)val;
/** 时区转换成GMT+0时区 */
+ (NSInteger)ConvertGMT0DatetimeToToday:(NSInteger)val;
+ (NSInteger)ConvertGMTDatetimeToToday:(NSInteger)val;

//获取当前时间对GTM0的时间
+ (NSDateComponents *)currentTimeComponents;
+ (NSString*)convertTimeZoneString:(NSString *)timeZoneStr;

/**以下都已系统默认时区转换*/
/**从1970年来星期几*/
- (NSInteger)weekSince1970FromSec:(NSTimeInterval)timeSec;

- (NSString *)longyyyyMMddHHmmssFromSec:(NSTimeInterval)timeSec;

- (NSString *)longyyyyMMddHHmmFromSec:(NSTimeInterval)timeSec;

- (NSString *)yyyyMMddFromSec:(NSTimeInterval)timeSec;

- (NSString *)HHmmssFromSec:(NSTimeInterval)timeSec;

- (NSString *)MdHHmmFromSec:(NSTimeInterval)timeSec;

- (NSString *)yyyyMdFromSec:(NSTimeInterval)timeSec;

- (NSDate *)dateFromyyyyMMddHHmmStr:(NSString *)timeStr;

- (NSString *)HHmmFromSec:(NSTimeInterval)timeSec;

- (NSString *)mmFromSec:(NSTimeInterval)timeSec;

- (NSString *)HHFromSec:(NSTimeInterval)timeSec;

- (NSString *)ddFromSec:(NSTimeInterval)timeSec;

- (CGFloat)hhFromMin:(NSInteger)timeMin;

- (NSInteger)monthFromSec:(NSTimeInterval)timeSec;

- (NSInteger)weekofYearFromSec:(NSTimeInterval)timeSec;


/**以下都已用户自定义时区转换*/
/**分时图十字光标移动时间*/
-(NSString *)timeShareMoveTimeHHmmFromSec:(NSTimeInterval)timeSec;
/**K线头部时间*/
- (NSString *)kLineHeaderTimeHHmmssFromSec:(NSTimeInterval)timeSec;
/**K线十字光标时间*/
-(NSString *)kLineCursorLineTimeLongyyyyMMddHHmmssFromSec:(NSTimeInterval)timeSec;
/**K线十字光标时间*/
-(NSString *)kLineCursorLineTimeYyyyMdFromSec:(NSTimeInterval)timeSec;
/**K线十字光标时间*/
-(NSString *)kLineCursorLineTimeMdHHmmFromSec:(NSTimeInterval)timeSec;
/**K线下边的时间*/
-(NSString *)kLineBottonTimeYyyyMdFromSec:(NSTimeInterval)timeSec;
/**K线下边的时间*/
-(NSString *)kLineBottonTimeMdHHmmFromSec:(NSTimeInterval)timeSec;
/**价格明细显示时间*/
-(NSString *)priceDetailTime:(NSTimeInterval)timeSec;

@end
