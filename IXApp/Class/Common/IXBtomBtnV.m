//
//  IXBtomBtnV.m
//  IXApp
//
//  Created by Seven on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBtomBtnV.h"
#import "CAGradientLayer+IX.h"

@implementation IXBtomBtnV

/**
 针对iPhone X尺寸会有所不同
 */
+ (CGSize)targetSize
{
    if (kBtomMargin == 0) {
        return CGSizeMake(kScreenWidth, 44);
    }
    return CGSizeMake(kScreenWidth, 44 + 9 + kBtomMargin);
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        if (kBtomMargin == 0) {
            [self createNormalV];
        } else {
            [self createIphoneCV];
        }
        _btn.titleLabel.font = PF_REGU(15);
        _btn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
        [self addSubview:_btn];
    }
    return self;
}

- (void)createNormalV
{
    _btn = [[UIButton alloc] initWithFrame:self.bounds];
}

- (void)createIphoneCV
{
    _btn = [[UIButton alloc] initWithFrame:CGRectMake(13, 9, kScreenWidth - 26, 44)];
    _btn.layer.cornerRadius = 2.f;
    _btn.layer.masksToBounds = YES;
    
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * lay = [CAGradientLayer topShadowWithFrame:CGRectMake(0, -5, kScreenWidth, 5) dkcolors:topColors];
    [self.layer addSublayer:lay];
    self.layer.masksToBounds = NO;
}

@end
