//
//  IXAlertVC.h
//  IXTest
//
//  Created by Evn on 17/2/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ExpendableAlartViewDelegate <NSObject>

//返回是否隐藏
@optional
- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex;
@end

@interface IXAlertVC : UIView

@property (nonatomic, weak) id<ExpendableAlartViewDelegate> expendAbleAlartViewDelegate;
@property (nonatomic, assign) int index;
@property (nonatomic, assign) NSTextAlignment textAlignment;//默认居中对齐
@property (nonatomic, strong) UIFont    * titleFont;
@property (nonatomic, strong) UIFont    * msgFont;
@property (nonatomic, strong) UIFont    * cancelFont;
@property (nonatomic, strong) UIFont    * sureFont;

/** 点击空白隐藏，默认为YES */
@property (nonatomic, assign) BOOL  emptyDismiss;

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)msg
                  cancelTitle:(NSString *)cancelTitle
                   sureTitles:(NSString *)sureTitle;

- (void)addView:(CGRect)frame;

- (void)showView;

- (void)showViewFrame:(CGRect)frame;

@end
