//
//  NSString+FormatterPrice.h
//  IXApp
//
//  Created by Bob on 2016/12/27.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FormatterPrice)


/**
 *  整型价格转小数
 *
 *  @param price  转换的价格
 *  @param digits 小数位数
 *
 *  @return 格式化时间字符串
 */
+ (NSString *)formatterPrice:(NSString *)price WithDigits:(int)digits;


+ (NSString *)formatterProfit:(NSString *)profit;

+ (NSString *)formatterPriceSign:(double)currentPrice;

+ (NSString *)formatterPriceSign:(double)currentPrice WithDigits:(int)digits;

+ (NSString *)formatterUTF8:(NSString *)url;

+ (NSString *)thousandFormate:(NSString *)str withDigits:(int)digits;

/**
 价格按位数格式化
 
 @param value 价格
 @param digit 位数
 @return 结果
 */
+ (NSString *)doubleToString:(double)value digit:(NSInteger)digit;
+ (NSDecimalNumberHandler *)handlerDigits:(NSInteger)digits;

@end
