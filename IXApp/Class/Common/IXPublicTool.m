//
//  IXPublicTool.m
//  IXApp
//
//  Created by Magee on 2017/5/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPublicTool.h"

@implementation IXPublicTool

+ (NSString *)decimalNumberWithCGFloat:(CGFloat)number digit:(int)digit
{
    NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:digit
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
    
    return [NSString formatterPrice:[[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",number]] decimalNumberByRoundingAccordingToBehavior:behavior].stringValue WithDigits:digit];
}

@end
