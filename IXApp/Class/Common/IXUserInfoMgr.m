//
//  IXUserInfoMgr.m
//  IXApp
//
//  Created by Magee on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUserInfoMgr.h"
#import "IXCpyConfig.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImageView+WebCache.h"
#import "IXUserInfoMgr_Setting.h"
#import "IXUserInfoMgr+Setting.h"
#import "RSA.h"

@interface IXUserInfoMgr ()

/** 因为初始化player之后随即播放将无任何声音，所以放在单例里面操作 */
@property (nonatomic, strong)AVAudioPlayer      * player;

@end


@implementation IXUserInfoMgr

+ (IXUserInfoMgr *)shareInstance
{
    static IXUserInfoMgr *share;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!share) {
            share = [[IXUserInfoMgr alloc] init];
            share.itemType = item_session_etype_TypeAppIos;
            share.settingRed = !SettingColor || SettingRed;
            [share configUserSetting];
            [share player];
        }
    });
    return share;
}

- (void)clearInstance
{
    _userLogInfo = nil;
    _swiAccount = NO;
    _symFlag = NO;
    _symCataFlag = NO;
    _accSymCataFlag = NO;
    _accSymCataCount = 0;
    _symCount = 0;
    _symCataCount = 0;
}

#pragma mark -
#pragma mark - show alert

- (void)showSoundAlert
{
    [self.player play];
}

- (void)showShakeAlert
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

- (void)showSoundAndShakeAlert
{
    [self showSoundAlert];
    [self showShakeAlert];
}

#pragma mark -
#pragma mark - getter

- (AVAudioPlayer *)player
{
    if (!_player) {
        NSString    * path  = [[NSBundle mainBundle] pathForResource:@"tradeAlert" ofType:@"aac"];
        NSData      * data = [NSData dataWithContentsOfFile:path];
        
        NSError * error = nil;
        _player = [[AVAudioPlayer alloc] initWithData:data error:&error];
    }
    
    return _player;
}

#pragma mark -
#pragma mark - 游客账户
- (NSString *)demoLoginAccount
{
    return @"ixvisitor@ixdigit.com";
}

- (NSString *)demoLoginPassword
{
    return @"visitor123456!!";
}
- (BOOL)isDemeLogin
{
    return SameString(self.userLogInfo.user.email, [self demoLoginAccount]);
}


@end
