//
//  IXDataCacheMgr.m
//  IXApp
//
//  Created by Evn on 2018/3/16.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDataCacheMgr.h"
#import "IXAccountBalanceModel.h"
#import "IXBORequestMgr.h"
#import "IXAccountGroupModel.h"
#import "IXADMgr.h"
#import "SFHFKeychainUtils.h"
#import "IXWUserInfo.h"
#import "IXKeyConfig.h"
#import "IXCpyConfig.h"

@implementation IXDataCacheMgr

+ (void)clearLoginInfo
{
    [[IXTCPRequest shareInstance] disConnect];
    [IXTCPRequest shareInstance].delegate = nil;
    [[IXTradeDataCache shareInstance] clearInstance];
    [[IXQuoteDataCache shareInstance] clearInstance];
    [[IXAccountBalanceModel shareInstance] clearCache];
    [[IXUserInfoMgr shareInstance] clearInstance];
    [[IXBORequestMgr shareInstance] clearInstance];
    [[IXAccountGroupModel shareInstance] clearInstance];
    [[IXADMgr shareInstance] clearInstance];
    
    [IXDataCacheMgr clearAccountInfo];
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    del.hasLogin = NO;
}

+ (void)clearAccountInfo
{
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    [SFHFKeychainUtils deleteItemForUsername:userName andServiceName:kServiceName error:nil];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kIXHeadIcon];
    
    [IXWUserInfo clear];
}

@end
