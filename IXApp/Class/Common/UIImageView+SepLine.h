//
//  UIImageView+SepLine.h
//  IXApp
//
//  Created by Bob on 2016/12/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (SepLine)

+ (UIImageView *)addSepImageWithFrame:(CGRect)frame
                            WithColor:(UIColor *)color
                            WithAlpha:(CGFloat)alpha;

+ (UIImage *)switchToImageWithColor:(UIColor *)color
                               size:(CGSize)size;

@end
