//
//  IXNavItem.m
//  IXApp
//
//  Created by Seven on 2017/7/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXNavItem.h"

@interface IXNavItem ()

@property (nonatomic, assign) BOOL isRightItem;
@property (nonatomic, strong) UILabel   * titleLab;

@end

@implementation IXNavItem

- (instancetype)initWithTarget:(id)target
                        action:(SEL)sel
                         title:(NSString *)title
                   isRightItem:(BOOL)isRight
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, 45, 40);
        
        _isRightItem = isRight;
        
        [self addTitleLabWithTitle:title];
        
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:target
                                                                                action:sel];
        [self addGestureRecognizer:tap];
    }

    return self;
}

- (void)addTitleLabWithTitle:(NSString *)title
{
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    CGFloat width = MAX(size.width, 65);
    
    CGFloat x = 0;
    if (_isRightItem) {
        x = 45 - width;
    }
    _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(x, 10, width, 20)];
    _titleLab.font = PF_MEDI(13);
    _titleLab.dk_textColorPicker = DKCellTitleColor;
    _titleLab.textAlignment = _isRightItem ? NSTextAlignmentRight : NSTextAlignmentLeft;
    _titleLab.text = title;
    [self addSubview:_titleLab];
}

@end
