//
//  NSDictionary+Type.m
//  Communivation
//
//  Created by bob on 16/11/1.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import "NSDictionary+Type.h"

@implementation NSDictionary (Type)

- (uint64_t)int64ForKey:(NSString *)key{
    id value = [self objectForKey:key];
    if ( value && ([value isKindOfClass:[NSNumber class]]
                   ||[value isKindOfClass:[NSString class]]) ) {
       return [value longLongValue];
    }
    return 0;
}

- (double)doubleForKey:(NSString *)key{
    id value = [self objectForKey:key];
    if ( value && ([value isKindOfClass:[NSNumber class]]
                   ||[value isKindOfClass:[NSString class]]) ) {
        return [value doubleValue];
    }
    return 0;
}

- (NSInteger)integerForKey:(NSString *)key{
    id value = [self objectForKey:key];
    if ( value && ([value isKindOfClass:[NSNumber class]]
                   ||[value isKindOfClass:[NSString class]]) ) {
        return [value integerValue];
    }
    return 0;
}

- (int)intForKey:(NSString *)key{
    id value = [self objectForKey:key];
    if ( value && ([value isKindOfClass:[NSNumber class]]
                   ||[value isKindOfClass:[NSString class]]) ) {
        return [value intValue];
    }
    return 0;
}

- (NSString *)stringForKey:(NSString *)key{
    id value = [self objectForKey:key];
    if ( value && [value isKindOfClass:[NSString class]] ) {
        return value;
    }else if( value && [value isKindOfClass:[NSNumber class]] ){
        return [(NSNumber *)value stringValue];
    }
    return @"";
}

@end
