//
//  IXChooseSymbolCataView.h
//  IXApp
//
//  Created by Bob on 2016/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, Symbol_Type)
{
    Symbol_Cata,
    Symbol_Trade,
    Symbol_AllType,
    Symbol_HotAllType,
};

typedef void(^chooseSymbolCata)(NSInteger indexRow);

@interface IXChooseSymbolCataView : UIView

@property (nonatomic, assign) Symbol_Type   type;
@property (nonatomic, assign) NSInteger     currentRow;
@property (nonatomic, assign) NSInteger     currentChildCata;

@property (nonatomic, copy) chooseSymbolCata    chooseSymbol;
@property (nonatomic, strong) NSMutableArray    * dataSourceAry;


/**
 *  初始化产品类别视图
 *
 *  @param cataArr    产品类别对应的控制器（VC）数组
 *  @param symbolCata 选中数据回掉
 *
 *  @return 产品类别视图
 */
- (id)initWithCataAry:(NSArray *)cataArr
           WithChoose:(chooseSymbolCata)symbolCata;


- (void)show;


- (void)dismiss;

@end
