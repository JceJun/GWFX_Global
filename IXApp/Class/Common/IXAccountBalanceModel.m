//
//  IXAccountBalanceModel.m
//  IXApp
//
//  Created by Bob on 2016/12/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAccountBalanceModel.h"

#import "IXTCPRequest.h"
#import "IXDBAccountMgr.h"
#import "IXDBAccountGroupMgr.h"
#import "IXPosCalM.h"
#import "IXMarginM.h"

#import "IXAccountGroupModel.h"

#define USDCRY @"USD"

NSString * const AB_CACHE_NUMBER_CHANGE = @"cacheNumChangeFlag";

@implementation PFTModel
@end

@interface IXAccountBalanceModel ()
{
    dispatch_queue_t dealCacheQueue;  //计算队列
    NSMutableArray *positionList;     //持仓列表
}

@property (nonatomic, assign) double needResNoti;

@property (nonatomic, strong) NSMutableDictionary *openPriceDic;        //持仓Id关联的持仓信息
@property (nonatomic, strong) NSMutableDictionary *positionRelationDic; //持仓Id对应的产品Id，用于计算单个持仓的盈亏
@property (nonatomic, strong) NSMutableDictionary *symbolRelationDic;   //产品关联的持仓，如果关联数组为空则可以取消当前订阅
@property (nonatomic, strong) NSMutableDictionary *cacheSymbolDic;      //缓存的持仓的产品
@property (nonatomic, strong) NSMutableDictionary *cacheMarketIdDic;    //缓存的MarketId，订阅行情需要使用MarketId，减少数据库查询
@property (nonatomic, strong) NSMutableDictionary *cacheParentIdDic;    //缓存的父类Id，股票取当前价，其它分类取顶层价

@property (nonatomic, strong) NSMutableDictionary *marginSetArrDic;     //保证金层级

@property (nonatomic, strong) NSMutableDictionary *askPosSymDic;        //买单持仓Dic
@property (nonatomic, strong) NSMutableDictionary *bidPosSymDic;        //卖单持仓Dic

@property (nonatomic, strong) NSMutableDictionary *swapDic;
@property (nonatomic, strong) NSMutableDictionary *marginDic;           //账户保证金字典

@property (nonatomic, assign) double equity;                            //账户净值
@property (nonatomic, assign) double totalFee;
@property (nonatomic, assign) double totalCommission;
@property (nonatomic, assign) double availableFunds;                    //账户可用资金

@property (nonatomic, assign) BOOL addTempSub;                          //查看产品详情
@property (nonatomic, assign) NSInteger detailSymId;                    //查看产品详情

@property (nonatomic, strong) NSMutableArray *tempSubArr;               //查看产品详情的订阅数组
@property (nonatomic, strong) NSMutableArray *tempSymArr;               //订阅的产品的产品Id数组
@end

@implementation IXAccountBalanceModel

#pragma mark life cycle
static IXAccountBalanceModel *share = nil;
+ (IXAccountBalanceModel *)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ( !share ) {
            share = [[IXAccountBalanceModel alloc] init];
        }
    });
    return share;
}

- (id)init
{
    self = [super init];
    if ( self ){
        [self addObserverKey];
        [self addCache];
        [self refreashCurrency];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserverKey];
}

- (void)addObserverKey
{
    //持仓更新相关的key
    IXTradeData_listen_regist(self, PB_CMD_POSITION_LIST);
    IXTradeData_listen_regist(self, PB_CMD_POSITION_ADD);
    IXTradeData_listen_regist(self, PB_CMD_POSITION_UPDATE);
    
    //切换账户相关的Key
    IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    
    //marginType刷新相关的key
    IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_CATA_ADD);
    IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_CATA_LIST);
    IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_CATA_UPDATE);

    IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_ADD);
    IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_LIST);
    IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_UPDATE);

    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_CATA_ADD);
    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_CATA_LIST);
    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_CATA_UPDATE);

    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CATA_ADD);
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CATA_LIST);
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CATA_UPDATE);

    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_UPDATE);
    
    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_ADD);
    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_LIST);
    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_UPDATE);

    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_MARGIN_ADD);
    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_MARGIN_LIST);
    IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_MARGIN_UPDATE);
    
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_MARGIN_SET_ADD);
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_MARGIN_SET_LIST);
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_MARGIN_SET_UPDATE);
    //marginType刷新相关的key
    
    IXTradeData_listen_regist(self, PB_CMD_USERLOGIN_INFO);
    IXTradeData_listen_regist(self, PB_CMD_USER_LOGIN_DATA_TOTAL);
}

- (void)removeObserverKey
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    
    IXTradeData_listen_resign(self, PB_CMD_POSITION_ADD);
    IXTradeData_listen_resign(self, PB_CMD_POSITION_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_POSITION_LIST);
    
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_CATA_LIST);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_CATA_UPDATE);

    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_ADD);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_LIST);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_UPDATE);

    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_CATA_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_CATA_UPDATE);

    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_CATA_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_CATA_UPDATE);

    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_MARGIN_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_MARGIN_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_MARGIN_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_MARGIN_SET_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_MARGIN_SET_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_MARGIN_SET_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_USERLOGIN_INFO);
    IXTradeData_listen_resign(self, PB_CMD_USER_LOGIN_DATA_TOTAL);

}

- (void)addCache
{
    dealCacheQueue = dispatch_queue_create("Account_Serial", DISPATCH_QUEUE_SERIAL);
    
    _totalMargin = 0;
    _totalProfit = 0;
    
    _totalFee = 0;
    _totalCommission = 0;
    
    _dynQuoteArr = [NSMutableArray array];
    
    positionList = [NSMutableArray array];
    
    _tempSubArr = [NSMutableArray array];
    _tempSymArr = [NSMutableArray array];
    
    _cachePosNumDic = [NSMutableDictionary dictionary];
    _cacheSymbolDic = [NSMutableDictionary dictionary];
    _cacheMarketIdDic = [NSMutableDictionary dictionary];
    _cacheParentIdDic = [NSMutableDictionary dictionary];
    
    _subSymbolDic = [NSMutableDictionary dictionary];
    _openPriceDic = [NSMutableDictionary dictionary];
    
    _symbolRelationDic = [NSMutableDictionary dictionary];
    _positionRelationDic = [NSMutableDictionary dictionary];
    
    _marginDic = [NSMutableDictionary dictionary];
    _quoteDataDic = [NSMutableDictionary dictionary];
    _marginSetArrDic = [NSMutableDictionary dictionary];
    
    _askPosSymDic = [NSMutableDictionary dictionary];
    _bidPosSymDic = [NSMutableDictionary dictionary];
}

- (void)clearCache
{
    [[IXQuoteDataCache shareInstance] saveDynQuote];
    [[IXQuoteDataCache shareInstance] saveLastClosePrice];
    
    [self.cachePosNumDic removeAllObjects];
    [self.askPosSymDic removeAllObjects];
    [self.bidPosSymDic removeAllObjects];
    
    [positionList removeAllObjects];
    [_dynQuoteArr removeAllObjects];
    [_symbolRelationDic removeAllObjects];
    
    [self.quoteDataDic removeAllObjects];
    
    [self.marginDic removeAllObjects];
    [self.swapDic removeAllObjects];
    [self.subSymbolDic removeAllObjects];

    [self.openPriceDic removeAllObjects];
    [self.positionRelationDic removeAllObjects];
    [self.symbolRelationDic removeAllObjects];
    [self.marginSetArrDic removeAllObjects];
    
    [self.cacheSymbolDic removeAllObjects];
    [self.cacheMarketIdDic removeAllObjects];
    [self.cacheParentIdDic removeAllObjects];
    
    _equity = 0;
    _totalMargin = 0;
    _totalProfit = 0;
    _availableFunds = 0;
    
    _totalFee = 0;
    _totalCommission = 0;    
}

typedef void(^asynBlock)(void);

- (void)asynF:(asynBlock)a{
    dispatch_async(dealCacheQueue, a);
}

#pragma mark 持仓更新的监听
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:PB_CMD_USERLOGIN_INFO]) {
        [self asynF:^{
            _needResNoti = NO;
            [self clearCache];
        }];
    }else if ([keyPath isEqualToString:PB_CMD_USER_LOGIN_DATA_TOTAL]){
        [self asynF:^{
            _needResNoti = YES;
            [self updatePos];
            
            __block NSMutableArray *quoteArr = [@[] mutableCopy];
            [_dynQuoteArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSDictionary *dic = (NSDictionary *)obj;
                [quoteArr addObject:[self getQuoteDataBySymbolId:[dic[@"id"] integerValue]]];
            }];
            if (quoteArr.count) {
                [self setQuoteDataArr:quoteArr];
            }
        }];
    }else if ([keyPath isEqualToString:PB_CMD_ACCOUNT_LIST] ||
              [keyPath isEqualToString:PB_CMD_ACCOUNT_GROUP_CHANGE]) {
        [self asynF:^{
            [self refreashCurrency];
        }];
    }else if ([keyPath isEqualToString:PB_CMD_POSITION_ADD]) {
        proto_position_add *pb = ((IXTradeDataCache *)object).pb_position_add;
        if (pb.result == 0 && pb.position.status == item_position_estatus_StatusOpened) {
            [self asynF:^{
                [self addPoition:pb.position];
            }];
        }
    }else if ([keyPath isEqualToString:PB_CMD_POSITION_UPDATE]){
        proto_position_update *pb = ((IXTradeDataCache *)object).pb_position_update;
        if (pb.result == 0
            && (pb.position.status == item_position_estatus_StatusClosed
                || pb.position.status == item_position_estatus_StatusOpened)) {
                [self asynF:^{
                    [self delPosition:pb.position];
                }];
            }
    }else if([keyPath isEqualToString:PB_CMD_POSITION_LIST]){
        proto_position_list *pb = ((IXTradeDataCache *)object).pb_position_list;
        [self asynF:^{
            [self refreashPositionList:pb];
        }];
    }else{
        [self asynF:^{
            if (_needResNoti && [[self.marginSetArrDic allKeys] count]) {
                [self.marginSetArrDic removeAllObjects];
            }
        }];
    }
}

#pragma mark 更新数据源
//更新当前货币类型
- (void)refreashCurrency
{
    NSString *currency = [IXAccountGroupModel shareInstance].accGroupDic[kCurrency];
    if (![currency isKindOfClass:[NSString class]] ||
        ![currency length]) {
        currency = @"";
    }
    _currency = currency;
}

//持仓列表更新
- (void)refreashPositionList:(proto_position_list *)list
{
    if(list.positionArray.count == 0)
        return;
    
    if (!positionList) {
        positionList = [NSMutableArray array];
    }
    
    [list.positionArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        item_position *pos = (item_position *)obj;
        if (![_openPriceDic.allKeys containsObject:@(pos.id_p)]) {
            [positionList addObject:pos];
            [_positionRelationDic setObject:@(pos.symbolid)
                                     forKey:@(pos.id_p)];
            
            [_openPriceDic setObject:@[@(pos.openPrice),
                                       @(pos.volume),
                                       @(pos.direction)]
                              forKey:@(pos.id_p)];
        }
    }];
 
    if (_needResNoti && positionList.count) {
        [self updatePos];
        [self postNotify];
    }
}

// 新增持仓时
- (void)addPoition:(item_position *)position
{
    if (!positionList.count) {
        positionList = [NSMutableArray array];
    }
 
    //不包含
    if (![_openPriceDic.allKeys containsObject:@(position.id_p)]) {
        
        [positionList addObject:position];
        [_positionRelationDic setObject:@(position.symbolid)
                                 forKey:@(position.id_p)];
        
        [_openPriceDic setObject:@[@(position.openPrice),
                                   @(position.volume),
                                   @(position.direction)]
                          forKey:@(position.id_p)];
        
        if (_needResNoti) {
            [self updatePos];
            [self postNotify];
        }
    }
}

/**
 持仓更新

 @param position 更新的持仓
 */
- (void)delPosition:(item_position *)position
{
    if(position.status == item_position_estatus_StatusClosed){
        __block BOOL delSuc = YES;
        [positionList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            item_position *pos = (item_position *)obj;
            if (pos.id_p == position.id_p) {
               [positionList removeObject:pos];
                delSuc = YES;
                *stop = YES;
            }
        }];
        
        if (delSuc) {
            [_openPriceDic removeObjectForKey:@(position.id_p)];
            [_positionRelationDic removeObjectForKey:@(position.id_p)];
 
            if (![[_positionRelationDic allValues] containsObject:@(position.symbolid)]) {
                [_marginDic removeObjectForKey:[self symbolId:position.symbolid]];
                [_symbolRelationDic removeObjectForKey:[self symbolId:position.symbolid]];
                [self delPosQuote:position];
            }
        }
    }else if ( position.status == item_position_estatus_StatusOpened ){
        __block BOOL delSuc = NO;
        [positionList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            item_position *pos = (item_position *)obj;
            if (pos.id_p == position.id_p) {
                [positionList replaceObjectAtIndex:idx withObject:position];
                delSuc = YES;
                *stop = YES;
            }
        }];
    
        if (delSuc) {
            //部分平仓
            [_openPriceDic setObject:@[@(position.openPrice),
                                       @(position.volume),
                                       @(position.direction)]
                              forKey:@(position.id_p)];
        }
    }
    
    if (_needResNoti) {
        [self updateCache];
        [self postNotify];
    }
}

//仓位更新对应的订阅列表更新
- (void)updatePos
{
    [self refreashSubSymbolArrInfoByCachePos];
    [self updateCache];
    [[IXTCPRequest shareInstance] subscribeDynPriceWithSymbolAry:self.dynQuoteArr];
}


//删除平仓的订阅列表
- (void)delPosQuote:(item_position *)position
{
    NSDictionary * value = [_subSymbolDic objectForKey:[self symbolId:position.symbolid]];
    NSMutableArray *canceArr = [NSMutableArray array];
    if ([value isKindOfClass:[NSDictionary class]]) {
        if ([value objectForKey:POSITIONMODEL]) {
            NSNumber *posId = [value objectForKey:SYMBOLID];
            NSDictionary *dic = [self checkMarketBySymbolId:[posId integerValue]];
            if ( dic ) {
                [canceArr addObject:dic];
            }
        }
        
        if ([value objectForKey:BASEMODEL]) {
            NSNumber *posId = [value objectForKey:SYMBOLID];
            NSDictionary *dic = [self checkMarketBySymbolId:[posId integerValue]];
            if ( dic ) {
                [canceArr addObject:dic];
            }
        }
        
        if ([value objectForKey:PROFITMODEL]) {
            NSNumber *posId = [value objectForKey:SYMBOLID];
            NSDictionary *dic = [self checkMarketBySymbolId:[posId integerValue]];
            if ( dic ) {
                [canceArr addObject:dic];
            }
        }
    }else{
        NSDictionary *dic = [self checkMarketBySymbolId:position.symbolid];
        if (dic) {
            [canceArr addObject:dic];
        }
    }
    
    [[IXTCPRequest shareInstance] unSubQuotePriceWithSymbolAry:canceArr];
    [_subSymbolDic removeObjectForKey:[self symbolId:position.symbolid]];
}

#pragma mark 维护查看产品详情时的订阅列表
//新交易页面需要新增的订阅
- (void)addSubSymbolWithSymbolId:(NSInteger)symbolId
{
    if (![_subSymbolDic objectForKey:[self symbolId:symbolId]]) {
        _addTempSub = YES;
        _detailSymId = symbolId;
        item_position *pos = [[item_position alloc] init];
        pos.symbolid = symbolId;
        [self addSubSymbolWithPosition:pos];
        [[IXTCPRequest shareInstance] subscribeDynPriceWithSymbolAry:_tempSubArr];
    }
}

//离开新交易页面需要取消的订阅
- (void)delSubSymbolWithSymbolId:(NSInteger)symbolId
{
    if ( _addTempSub ) {
        _addTempSub = NO;
        _detailSymId = 0;
        if ( _tempSubArr ) {
            [[IXTCPRequest shareInstance] unSubQuotePriceWithSymbolAry:_tempSubArr];
            [_subSymbolDic removeObjectForKey:[self symbolId:symbolId]];
            [_tempSymArr removeAllObjects];
            [_tempSubArr removeAllObjects];
        }
    }
}

#pragma mark 持仓对应的订阅列表
/*初始化订阅列表*/
- (void)refreashSubSymbolArrInfoByCachePos
{
    [self asynF:^{
        //如果没有货币类型，需要重新获取
        if (![_currency length]) {
            [self refreashCurrency];
        }
        
        for (item_position *position in positionList) {
            [_positionRelationDic setObject:@(position.symbolid)
                                     forKey:@(position.id_p)];
            
            [_openPriceDic setObject:@[@(position.openPrice),
                                       @(position.volume),
                                       @(position.direction)]
                              forKey:@(position.id_p)];
            
            [self addSubSymbolWithPosition:position];
        }
    }];
}

//持仓需要新增的关联产品
- (void)addSubSymbolWithPosition:(item_position *)position
{
    if(![_subSymbolDic objectForKey:[self symbolId:position.symbolid]]){
        
        IXSymbolM *symbolModel = [self symModelWithId:position.symbolid];
        
        //直接货币
        if ([symbolModel.profitCurrency  isEqualToString:_currency]
            ||[symbolModel.baseCurrency isEqualToString:_currency]) {
            [self addDynQuoteWithSymbolId:symbolModel.id_p];
            double value = ([symbolModel.profitCurrency isEqualToString:_currency]) ? 1 : -1;
            [_subSymbolDic setValue:@(value) forKey:[self symbolId:symbolModel.id_p]];
            [self setRelationType:@"0"
                     WithSymbolId:symbolModel.id_p
              WithCurrentSymbolId:symbolModel.id_p];
        }else{
            double swap = [self querySwapWithSymbolId:symbolModel.id_p
                                         WithPosition:position
                                            WithDivid:NO];
            NSDictionary *positionDic = @{
                                          SYMBOLID:@(symbolModel.id_p),
                                          SWAPLOSS:@(swap),
                                          SWAPPROFIT:@(swap),
                                          SWAPDIRECRION:@(1)
                                          };
            [self setRelationType:POSITIONMODEL
                     WithSymbolId:position.symbolid
              WithCurrentSymbolId:position.symbolid];
            [self addDynQuoteWithSymbolId:symbolModel.id_p];
            
            NSDictionary *baseDic = [self subDic:symbolModel Type:BASEMODEL Pos:position];
            NSDictionary *profitDic = [self subDic:symbolModel Type:PROFITMODEL Pos:position];
            NSDictionary *magnDic = [self subDic:symbolModel Type:MAGNSYMMODEL Pos:position];

            @try {
                [_subSymbolDic setValue:@{MAGNSYMMODEL:magnDic,
                                          POSITIONMODEL:positionDic,
                                          PROFITMODEL:profitDic,
                                          BASEMODEL:baseDic}
                                 forKey:[self symbolId:position.symbolid]];
                
            } @catch (NSException *e) {

            }
        }
    }
}

- (NSDictionary *)subDic:(IXSymbolM *)model
                    Type:(NSString *)type
                     Pos:(item_position *)position
{
    NSString *cry;
    if([type isEqualToString:PROFITMODEL]){
        cry = model.profitCurrency;
    }else if ([type isEqualToString:BASEMODEL]){
        cry = _currency;
    }else if ([type isEqualToString:MAGNSYMMODEL]){
        cry = model.baseCurrency;
    }
    if([cry isEqualToString:USDCRY]){
        return @{SYMBOLID:[self symbolId:model.id_p]};
    }
    
    NSDictionary *profitDic;
    IXSymbolM *profitModel;
    profitModel = [self checkSymbolInfoWithProfitCurrency:cry
                                         WithBaseCurrency:USDCRY];
    if (profitModel) {
        [self addDynQuoteWithSymbolId:profitModel.id_p];
        double swap = [self querySwapWithSymbolId:profitModel.id_p
                                     WithPosition:position
                                        WithDivid:NO];
        profitDic = @{
                      SYMBOLID:@(profitModel.id_p),
                      SWAPLOSS:@(swap),
                      SWAPPROFIT:@(swap),
                      SWAPDIRECRION:@(1)
                      };
        [self setRelationType:type
                 WithSymbolId:position.symbolid
          WithCurrentSymbolId:profitModel.id_p];
        [self addDynQuoteWithSymbolId:profitModel.id_p];
    }else{
        profitModel = [self checkSymbolInfoWithProfitCurrency:USDCRY
                                             WithBaseCurrency:cry];
        if (profitModel) {
            if ([type isEqualToString:MAGNSYMMODEL]) {
                [self addDynQuoteWithSymbolId:profitModel.id_p];
                double swap = [self querySwapWithSymbolId:profitModel.id_p
                                             WithPosition:position
                                                WithDivid:NO];
                profitDic = @{
                              SYMBOLID:@(profitModel.id_p),
                              SWAPLOSS:@(swap),
                              SWAPPROFIT:@(swap),
                              SWAPDIRECRION:@(1)
                              };
            }else{
                [self addDynQuoteWithSymbolId:profitModel.id_p];
                double swap = [self querySwapWithSymbolId:profitModel.id_p
                                             WithPosition:position
                                                WithDivid:YES];
                profitDic = @{
                              SYMBOLID:@(profitModel.id_p),
                              SWAPLOSS:@(swap),
                              SWAPPROFIT:@(swap),
                              SWAPDIRECRION:@(-1)
                              };
            }
         
            [self setRelationType:type
                     WithSymbolId:position.symbolid
              WithCurrentSymbolId:profitModel.id_p];
        }
    }
    return profitDic;
}


//设置订阅的symbol对应持仓的盈利还是基准
- (void)setRelationType:(NSString *)type
           WithSymbolId:(NSInteger)symbolId
    WithCurrentSymbolId:(NSInteger)curId
{
    id value = [_symbolRelationDic objectForKey:[self symbolId:curId]];
    
    NSMutableArray *arr;
    if (value) {
        arr = [value mutableCopy];
    }else{
        if(arr){
            [arr addObject:@{[self symbolId:symbolId]:type}];
        }else{
            arr = [@{[self symbolId:symbolId]:type} mutableCopy];
        }
    }
    
    [_symbolRelationDic setValue:arr forKey:[self symbolId:curId]];
}

//添加订阅列表
- (void)addDynQuoteWithSymbolId:(NSInteger)symbolId
{
    id value = [self checkMarketBySymbolId:symbolId];
    if ( value  &&  ![_dynQuoteArr containsObject:value] && ![_tempSymArr containsObject:@(symbolId)] ) {
        if( _addTempSub ){
            //保存进入新交易页面需要的订阅
            [_tempSubArr addObject:value];
            [_tempSymArr addObject:@(symbolId)];
        }else{
            //持仓数据
            [_dynQuoteArr addObject:value];
        }
    }
}

//初始化订阅数据(symbolId:MarketId)
- (NSDictionary *)checkMarketBySymbolId:(NSInteger)symbolId
{
    if ( [_cacheMarketIdDic objectForKey:[self symbolId:symbolId]] ) {
        return [_cacheMarketIdDic objectForKey:[self symbolId:symbolId]];
    }
    NSDictionary *dic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:symbolId];
    [_cacheMarketIdDic setValue:dic forKey:[self symbolId:symbolId]];
    return dic;
}

- (NSInteger)checkParentBySymbolId:(NSInteger)symbolId
{
    if ( [_cacheParentIdDic objectForKey:[self symbolId:symbolId]] ) {
        return [[_cacheParentIdDic objectForKey:[self symbolId:symbolId]] integerValue];
    }
    NSInteger parentId = [IXDataProcessTools queryParentIdBySymbolId:symbolId];
    [_cacheParentIdDic setValue:@(parentId) forKey:[self symbolId:symbolId]];
    return parentId;
}

#pragma mark 查询本地缓存的数据
//查询缓存的行情
- (IXQuoteM *)getQuoteDataBySymbolId:(NSInteger)symbolId
{
    //先从当前缓存队列获取行情
    IXQuoteM *quote = [_quoteDataDic objectForKey:[self symbolId:symbolId]];
    if (!quote) {
        //不存在，则从QuoteCache获取
        quote = [[IXQuoteDataCache shareInstance].dynQuoteDic objectForKey:[self symbolId:symbolId]];
        if (!quote) {
            //不存在，则从数据库获取
            quote = [IXDataProcessTools queryQuoteDataBySymbolId:symbolId];
            if (quote.BuyPrc.count && quote.SellPrc.count) {
                [_quoteDataDic setValue:quote forKey:[self symbolId:symbolId]];
            }
        }
    }
   
    if (!quote) {
        quote = [[IXQuoteM alloc] init];
    }
    return quote;
}

//查询缓存的产品
- (IXSymbolM *)symModelWithId:(NSInteger)symbolId
{
    IXSymbolM *model = [_cacheSymbolDic objectForKey:[self symbolId:symbolId]];
    if (!model) {
        NSDictionary *symbolDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
        model = [IXDataProcessTools symbolModelBySymbolInfo:symbolDic];
        [_cacheSymbolDic setValue:model forKey:[self symbolId:symbolId]];
    }
    return model;
}


//根据SymbolID从数据库查询缓存的行情
- (double)querySwapWithSymbolId:(NSInteger)symbolId
                   WithPosition:(item_position *)postion
                      WithDivid:(BOOL)divid
{
    IXQuoteM *model = [self getQuoteDataBySymbolId:symbolId];
    if (model.BuyPrc.count > 0 && model.SellPrc.count > 0) {
        
        NSString *nPrice = [self getNprc:postion Quote:model];
        double offsetPrc = [self getOffsetPrc:nPrice Pos:postion];

        //赚少赔多
        if ( offsetPrc >= 0 ) {
            return  (divid ? [model.BuyPrc[0] doubleValue] : [model.SellPrc[0] doubleValue]);
        }else{
            return  (divid ? [model.SellPrc[0] doubleValue] : [model.BuyPrc[0] doubleValue]);
        }
        return model.nPrice;
    }
    return 0;
}


/**
 *  查询Symbol
 *
 *  @param  profit ProfitCurrency
 *  @param  base baseCurrency
 *
 *  @return 盈利
 */
- (IXSymbolM *)checkSymbolInfoWithProfitCurrency:(NSString *)profit
                                WithBaseCurrency:(NSString *)base
{
    NSDictionary *dic = [IXDBSymbolMgr querySymbolByProfitCurrency:profit
                                                      baseCurrency:base];
    return [IXDataProcessTools symbolModelBySymbolInfo:dic];
}

- (NSString *)symbolId:(uint64)symbolId
{
    return [NSString stringWithFormat:@"%llu",symbolId];
}


/**
 计算盈亏和保证金；驱动UI刷新
 */
- (void)postNotify
{
    [self updateProfit];
    [self updateMargin];

    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *obj = @{NOTIFYITEM:@"1"};
        [[NSNotificationCenter defaultCenter] postNotificationName:kRefreashProfit object:obj];
    });
}

#pragma mark 行情刷新
/*
 *
 *  收到行情，判断当前Symbol是否在订阅列表，如果在订阅列表，则更新汇率，更新保证金；更新盈亏。
 *  如果返回的行情是持仓的产品，则更新当前价和盈亏；
 *  如果返回的行情是关联的产品，则更新盈亏。
 *
 */
- (void)setQuoteDataArr:(NSArray *)quoteDataArr
{
    if (_needResNoti) {
        [self asynF:^{
           BOOL update = [self updateTick:quoteDataArr];
            if (update) {
                [self postNotify];
            }
        }];
    }
}

- (BOOL)updateTick:(NSArray *)quoteDataArr
{
    __block BOOL needRefreash = NO;
    NSMutableArray *dealArr = [quoteDataArr mutableCopy];
    [dealArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXQuoteM *tick = (IXQuoteM *)obj;
        if (([_tempSymArr containsObject:@(tick.symbolId)] ||
             [[_symbolRelationDic allKeys] containsObject:[self symbolId:tick.symbolId]]) ) {
            if (tick.BuyPrc.count && tick.SellPrc.count) {
                [_quoteDataDic setValue:tick forKey:[self symbolId:tick.symbolId]];
            }
            needRefreash = YES;
        }
    }];
    return needRefreash;
}

- (void)updateMargin
{
    [_subSymbolDic.allKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //不同的仓位相同的产品可能点差不一样
        IXQuoteM *model = [self getQuoteDataBySymbolId:[obj integerValue]];
        
        //如果是产品详情的订阅行情，就不计算保证金
        if(_addTempSub && model.symbolId == _detailSymId){
            return ;
        }
 
        NSDictionary * value = _subSymbolDic[obj];
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSString *curSymKey = [(NSDictionary *)value[POSITIONMODEL] objectForKey:SYMBOLID];
            [self calMargin:curSymKey];
        }else if ([value isKindOfClass:[NSNumber class]]){
            if([_positionRelationDic.allValues containsObject:@(model.symbolId)]){
                [self calMargin:obj];
            }
        }
    }];
    
    __block NSDecimalNumber *sumMargin = [NSDecimalNumber decimalNumberWithString:@"0"];
    [_marginDic.allValues enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXMarginM *model = (IXMarginM *)obj;
        if ([model.margin doubleValue] && !isnan([model.margin doubleValue])) {
            sumMargin = [sumMargin decimalNumberByAdding:model.margin];
        }
    }];
    
    _totalMargin = [sumMargin doubleValue];
}

- (void)updateProfit
{
    __block NSDecimalNumber *sumProfit = [NSDecimalNumber decimalNumberWithString:@"0"];
    [_askPosSymDic.allKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXPosCalM *model = _askPosSymDic[obj];
        if (model) {
            double curPrc = [self refPrcWithSymId:[obj integerValue] posDir:item_position_edirection_DirectionBuy];
            if (curPrc != 0) {
                double offPrc = curPrc - model.avgPrc;
                double swap = [self caculateSwapWithSymbolId:[obj integerValue] WithOffsetPrice:offPrc];
                double profit = [self caculateProfit:offPrc WithVolume:model.tVol WithSwap:swap WithDirection:1];
                sumProfit = [sumProfit decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:[@(profit) stringValue]]];
            }
        }
    }];
    
    [_bidPosSymDic.allKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXPosCalM *model = _bidPosSymDic[obj];
        if (model) {
            double curPrc = [self refPrcWithSymId:[obj integerValue]
                                           posDir:item_position_edirection_DirectionSell];
            if (curPrc != 0) {
                double offPrc = model.avgPrc - curPrc;
                double swap = [self caculateSwapWithSymbolId:[obj integerValue]
                                             WithOffsetPrice:offPrc];
                double profit = [self caculateProfit:offPrc
                                          WithVolume:model.tVol
                                            WithSwap:swap WithDirection:1];
                sumProfit = [sumProfit decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:[@(profit) stringValue]]];
            }
         
        }
    }];
    _totalProfit = [sumProfit doubleValue];
}

//卖要乘-1
- (NSInteger)getdirWithPosId:(NSNumber *)key
{
    NSInteger posDir = [_openPriceDic[key][2] integerValue];
    if (posDir == item_order_edirection_DirectionSell) {
        posDir = -1;
    }
    return posDir;
}

//计算价格差
- (double)caculateOffsetPrice:(NSString *)curPrice WithOpenPrice:(NSString *)openPrice
{
    if ( ![curPrice length]|| ![openPrice length]) {
        return 0;
    }
    NSDecimalNumber *curPrc = [NSDecimalNumber decimalNumberWithString:curPrice];
    NSDecimalNumber *openPrc = [NSDecimalNumber decimalNumberWithString:openPrice];
 
    return [[curPrc decimalNumberBySubtracting:openPrc] doubleValue];
}

#pragma mark 计算保证金和盈亏
/**
 *  缓存需要计算的数据
 *
 */
- (void)updateCache
{
    _totalFee = 0;
    _totalCommission = 0;
    
    [_askPosSymDic removeAllObjects];
    [_bidPosSymDic removeAllObjects];
    [_cachePosNumDic removeAllObjects];
    
    __block NSMutableArray *symIdArr = [@[] mutableCopy];
    [positionList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        item_position *postion = (item_position *)obj;
        _totalFee += postion.swap;
        _totalCommission += postion.commission;
        
        NSString *key = [NSString stringWithFormat:@"%llu",postion.symbolid];
        [symIdArr addObject:key];
        
        NSInteger posNum = [[_cachePosNumDic objectForKey:key] integerValue];
        [_cachePosNumDic setValue:@(posNum+1) forKey:key];
        
        IXPosCalM *model;
        if (postion.direction == item_position_edirection_DirectionBuy) {
            model = _askPosSymDic[key];
        }else{
            model = _bidPosSymDic[key];
        }
        
        if (model) {
            model.avgPrc = (postion.volume * postion.openPrice + model.avgPrc *model.tVol)/(postion.volume + model.tVol);
            model.tVol += postion.volume;
        }else{
            model = [[IXPosCalM alloc] init];
            model.avgPrc = postion.openPrice;
            model.tVol = postion.volume;
        }
        
        if (postion.direction == item_position_edirection_DirectionBuy) {
            [_askPosSymDic setValue:model forKey:key];
        }else{
            [_bidPosSymDic setValue:model forKey:key];
        }
    }];
    
    //新增的更新持仓数
    dispatch_async(dispatch_get_main_queue(), ^{
        self.cacheNumChangeFlag = !self.cacheNumChangeFlag;
    });
}

/**
 买单保证金的汇率

 @param symId 产品Id
 @return 汇率
 */
- (double)askMarginSwap:(NSString *)symId
{
    IXPosCalM *pModel = _askPosSymDic[symId];
    IXQuoteM *qModel = [self getQuoteDataBySymbolId:[symId integerValue]];
    double swap = 0;
    if (pModel && qModel) {
        double offPrc = [self refPrcWithSymId:[symId integerValue]
                                       posDir:item_position_edirection_DirectionBuy] - pModel.avgPrc;
        NSDictionary *dic = [_subSymbolDic objectForKey:symId];
        NSInteger dir = 0;
        if ([dic isKindOfClass:[NSNumber class]]) {
            dir = [(NSNumber *)dic integerValue];
        }else if([dic isKindOfClass:[NSDictionary class]]){
            NSDictionary *posDic = dic[POSITIONMODEL];
            dir = [posDic integerForKey:SWAPDIRECRION];
        }
        if (0 != offPrc) {
            swap = [self caculateMarginSwapWithSymbolId:[symId integerValue]
                                          WithDirection:dir
                                        WithOffsetPrice:offPrc];
        }
    }
    return swap;
}
 

/**
 卖单保证金的汇率
 
 @param symId 产品Id
 @return 汇率
 */
- (double)bidMarginSwap:(NSString *)symId
{
    IXPosCalM *pModel = _bidPosSymDic[symId];
    IXQuoteM *qModel = [self getQuoteDataBySymbolId:[symId integerValue]];
    double swap = 0;
    if (pModel && qModel) {
        double offPrc = [self refPrcWithSymId:[symId integerValue]
                                       posDir:item_position_edirection_DirectionSell] - pModel.avgPrc;
        NSDictionary *dic = [_subSymbolDic objectForKey:symId];
        NSInteger dir = 0;
        if ([dic isKindOfClass:[NSNumber class]]) {
            dir = [(NSNumber *)dic integerValue];
        }else if([dic isKindOfClass:[NSDictionary class]]){
            NSDictionary *posDic = dic[POSITIONMODEL];
            dir = [posDic integerForKey:SWAPDIRECRION];
        }
        if (0 != offPrc) {
            swap = [self caculateMarginSwapWithSymbolId:[symId integerValue]
                                          WithDirection:dir
                                        WithOffsetPrice:offPrc];
        }
    }
    return swap;
}

/**
 查询需要计算产品汇率的参考价格(股票取最新价；其他分类取反向顶层价）

 @param symId 产品Id
 @return 参考价格
 */
- (double)refPrcWithSymId:(NSInteger)symId
                   posDir:(item_position_edirection)dir
{
    IXQuoteM *qModel = [self getQuoteDataBySymbolId:symId];
    double curPrc = 0;
    NSInteger parentId = [self checkParentBySymbolId:symId];
    if(parentId != STOCKID){
        NSInteger marketId = [[self checkMarketBySymbolId:symId] integerForKey:kMarketId];
        if(marketId == IDXMARKETID){
            curPrc = qModel.nPrice;
        }else{
            if (dir == item_position_edirection_DirectionSell) {
                if (qModel.BuyPrc.count) {
                    curPrc = [qModel.BuyPrc[0] doubleValue];
                }
            }else{
                if (qModel.SellPrc.count) {
                    curPrc = [qModel.SellPrc[0] doubleValue];
                }
            }
        }
    }else{
        curPrc = qModel.nPrice;
    }
    return curPrc;
}

/**
 计算保证金

 @param symId 关联的产品Id
 */
- (void)calMargin:(NSString *)symId
{
    if ([symId isKindOfClass:[NSNumber class]]) {
        symId = [(NSNumber *)symId stringValue];
    }
 
 
    IXPosCalM *askM = _askPosSymDic[symId];
    IXPosCalM *bidM = _bidPosSymDic[symId];
    
    double vol;
    item_position_edirection dir;
    if (askM.tVol < bidM.tVol) {
        vol = bidM.tVol;
        dir = item_position_edirection_DirectionSell;
    }else{
        vol = askM.tVol;
        dir = item_position_edirection_DirectionBuy;
    }
    
    IXSymbolM *symbolModel = [self symModelWithId:[symId longLongValue]];
    double currentRate = [self marginCurrentRateWithVolume:vol WithSymbol:symbolModel];
    IXQuoteM *model = [_quoteDataDic objectForKey:[self symbolId:symbolModel.id_p]];
    if (model.BuyPrc.count && model.SellPrc.count) {
        //更新保证金
        double marginPrice = 0;
        double swap = 0;
        if (dir == item_position_edirection_DirectionSell) {
            swap = [self bidMarginSwap:symId];
            if (model.SellPrc.count) {
                marginPrice = [model.SellPrc[0] doubleValue];
            }else{
                return;
            }
        }else{
            swap = [self askMarginSwap:symId];
            if (model.BuyPrc.count) {
                marginPrice = [model.BuyPrc[0] doubleValue];
            }else{
                return;
            }
        }
        
        double margin = 0;
        if(symbolModel.cataId == FXCATAID){
            margin = swap * currentRate;
        }else{
            margin = swap * marginPrice * currentRate;
        }
        
        IXMarginM *mModel = [IXMarginM new];
        mModel.model = symbolModel;
        mModel.vol = [NSDecimalNumber decimalNumberWithString:[@(vol) stringValue]];
        mModel.margin = [NSDecimalNumber decimalNumberWithString:[@(margin) stringValue]];
        mModel.posDir = dir;
        [_marginDic setValue:mModel forKey:symId];
    }
}

/**
 *  计算保证金百分比
 */
- (double)marginCurrentRateWithVolume:(NSInteger)symbolVolume WithSymbol:(IXSymbolM *)symbol
{
    if( symbolVolume <= 0 )
        return 0;
    
    NSDecimalNumber *currentRate = [NSDecimalNumber decimalNumberWithString:@"0"];
    
    NSArray *marginSetArr = [_marginSetArrDic objectForKey:[self symbolId:symbol.id_p]];
    if ( !marginSetArr ) {
        marginSetArr = [IXDataProcessTools getMarginSet:symbol];
        [_marginSetArrDic setValue:marginSetArr forKey:[self symbolId:symbol.id_p]];
    }
    
    for (NSInteger i = 0; i < marginSetArr.count; i++ ) {
        
        NSDictionary *marginDic = [marginSetArr objectAtIndex:i];
        
        NSInteger rangeLeft = [marginDic[kRangeLeft] integerValue];
        NSInteger rangeRight = [marginDic[kRangeRight] integerValue];
        //        float percent = [marginDic[kPercent] floatValue]/100.0;
        NSDecimalNumber *percent = [[NSDecimalNumber decimalNumberWithString:marginDic[kPercent]] decimalNumberByDividingBy:
                                    [NSDecimalNumber decimalNumberWithString:@"10000"]];
        long tempVolume = symbolVolume - rangeLeft;
        if ( tempVolume < 0 ) {
            break;
        }else if ( i == marginSetArr.count - 1 ) {
            //            currentRate += tempVolume * percent;
            NSDecimalNumber *volDec = [NSDecimalNumber decimalNumberWithString:
                                       [NSString stringWithFormat:@"%ld",tempVolume]];
            currentRate = [currentRate decimalNumberByAdding:[volDec decimalNumberByMultiplyingBy:percent]];
        }else{
            if ( symbolVolume < rangeRight ) {
                //                currentRate += tempVolume * percent;
                NSDecimalNumber *volDec = [NSDecimalNumber decimalNumberWithString:
                                           [NSString stringWithFormat:@"%ld",tempVolume]];
                currentRate = [currentRate decimalNumberByAdding:[volDec decimalNumberByMultiplyingBy:percent]];
            }else{
                //                currentRate += (rangeRight - rangeLeft) * percent;
                NSDecimalNumber *volDec = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%ld",
                                                                                    (long)(rangeRight - rangeLeft)]];
                currentRate = [currentRate decimalNumberByAdding:[volDec decimalNumberByMultiplyingBy:percent]];
            }
        }
    }
    return  [currentRate doubleValue];
}

/*单个盈亏的计算*/
- (double)caculateProfit:(double)offsetPrice
              WithVolume:(double)volume
                WithSwap:(double)swap
           WithDirection:(double)direction
{
    double profit = offsetPrice * volume * swap * direction;
    return profit;
}
 
- (NSString *)getNprc:(item_position *)pos Quote:(IXQuoteM *)qModel
{
    NSString *nPrice;
    IXQuoteM *quote = [self getQuoteDataBySymbolId:pos.symbolid];
    NSInteger parentId = [self checkParentBySymbolId:pos.symbolid];
    if( parentId != STOCKID){
        NSInteger marketId = [[self checkMarketBySymbolId:pos.symbolid] integerForKey:kMarketId];
        if(marketId == IDXMARKETID){
            nPrice = [NSString stringWithFormat:@"%lf",quote.nPrice];
        }else{
            if (pos.direction == item_position_edirection_DirectionSell) {
                nPrice = quote.BuyPrc[0];
            }else {
                nPrice = quote.SellPrc[0];
            }
        }
    }else{
        nPrice = [NSString stringWithFormat:@"%lf",quote.nPrice];
    }
    return nPrice;
}

//获取计算汇率的参考价格
//earn表示赚亏
//divid表示是否需要除
- (double)getRefpriceWithSymbolId:(NSInteger)symbolId
                        WithQuote:(IXQuoteM *)quote
                         WithEarn:(BOOL)earn
                        WithDivid:(BOOL)divid
{
    if( !quote || quote.BuyPrc.count < 1 || quote.SellPrc.count < 1 )
        return 0;
    
    
    if (earn) {
        //赚:汇率转换需要除，则使用ask价；需要乘，则使用bid价
        if (divid) {
            return [quote.BuyPrc[0] doubleValue];
        }else{
            return [quote.SellPrc[0] doubleValue];
        }
    }else{
        //亏:汇率转换需要除，则使用bid价；需要乘，则使用ask价
        if (divid) {
            return [quote.SellPrc[0] doubleValue];
        }else{
            return [quote.BuyPrc[0] doubleValue];
        }
    }
}

- (double)swap:(NSDictionary *)currrentInfo
          Type:(NSString *)type
           Dir:(NSInteger)directionValue
     OffsetPrc:(double)offsetPrc
         SymId:(NSInteger)symbolId
{
    double positionSwap = 0;
    NSDictionary *pnT = [currrentInfo objectForKey:type];
    NSMutableDictionary *positionModel;
    if(pnT && ![pnT isKindOfClass:[NSNumber class]]){
        if (![[pnT allKeys] containsObject:SWAPLOSS]) {
            return 1;
        }
        
        positionModel = [NSMutableDictionary dictionaryWithDictionary:pnT];
        IXQuoteM *posQuote  = [self getQuoteDataBySymbolId:[positionModel[SYMBOLID] longLongValue]];
        
        if (offsetPrc >= 0) {
            if( positionModel[SWAPPROFIT] && [positionModel[SWAPPROFIT] doubleValue] != 0 ){
                positionSwap = [positionModel[SWAPPROFIT] doubleValue];
            }
        }else{
            if( positionModel[SWAPLOSS] && [positionModel[SWAPLOSS] doubleValue] != 0 ){
                positionSwap = [positionModel[SWAPLOSS] doubleValue];
            }
        }
        
        //double posQuoteSwap = posQuote.nPrice;
        directionValue = [positionModel integerForKey:SWAPDIRECRION];
        BOOL divid = (directionValue == -1);
        if ([type isEqualToString:PROFITMODEL]) {
            divid = !divid;
        }
        double posQuoteSwap = [self getRefpriceWithSymbolId:symbolId
                                                  WithQuote:posQuote
                                                   WithEarn:(offsetPrc >= 0)
                                                  WithDivid:divid];
        if ( posQuoteSwap != 0 ) {
            positionSwap =  posQuoteSwap;
        }
        
        if (![type isEqualToString:MAGNSYMMODEL]) {
            if ([positionModel[SWAPDIRECRION] integerValue] == -1 && positionSwap != 0) {
                positionSwap = 1.0/positionSwap;
            }
        }
        
        
        if ( offsetPrc >= 0 ) {
            [positionModel setValue:@(positionSwap) forKey:SWAPPROFIT];
        }else{
            [positionModel setValue:@(positionSwap) forKey:SWAPLOSS];
        }
        
        [currrentInfo setValue:positionModel forKey:type];
    }
    return positionSwap;
}

/*  ******          计算汇率          ******
 *1、直接货币：(1 * 货币/美元)
 *2、间接货币：(1 * 盈利货币/美元)
 *3、交叉货币：(1 * 盈利货币/美元 * 美元/当前货币)
 ****************************************** */
- (double)caculateSwapWithSymbolId:(NSInteger)symbolId
                   WithOffsetPrice:(double)offsetPrc
{
    if (![_currency length]) {
        [self refreashCurrency];
    }
    @synchronized ( self ) {
        double swap = 0;
        
        NSString *symbolKey = [self symbolId:symbolId];
        id symbolInfo = _subSymbolDic[symbolKey];
        if (!symbolInfo) {
            return swap;
        }else if([symbolInfo isKindOfClass:[NSNumber class]]){
            swap = [self directionSwapWithSymId:symbolId
                                  WithDirection:[symbolInfo integerValue]
                                  WithOffsetPrc:offsetPrc];
            return swap;
        }else if([symbolInfo isKindOfClass:[NSDictionary class]]){
            NSMutableDictionary *currentInfo = [NSMutableDictionary dictionaryWithDictionary:symbolInfo];

            NSInteger bDir = [currentInfo[BASEMODEL] integerForKey:SWAPDIRECRION];

            double baseSwap = [self swap:currentInfo
                                    Type:BASEMODEL
                                     Dir:bDir
                               OffsetPrc:offsetPrc
                                   SymId:symbolId];
            
            NSInteger pDir = [currentInfo[PROFITMODEL] integerForKey:SWAPDIRECRION];
            double profitSwap = [self swap:currentInfo
                                      Type:PROFITMODEL
                                       Dir:pDir
                                 OffsetPrc:offsetPrc
                                     SymId:symbolId];
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"

            NSInteger ptDir = [currentInfo[POSITIONMODEL] integerForKey:SWAPDIRECRION];
            double positionSwap = [self swap:currentInfo
                                        Type:POSITIONMODEL
                                         Dir:ptDir
                                   OffsetPrc:offsetPrc
                                       SymId:symbolId];
#pragma clang diagnostic pop

            [_subSymbolDic setObject:currentInfo forKey:[self symbolId:symbolId]];
            
            NSDictionary *profitModel = currentInfo[PROFITMODEL];
            
            if (![_currency length]) {
                return swap;
            }
            
            //账号是美元
            if ([_currency isEqualToString:USDCRY]) {
                if (profitModel) {
                    swap = [self indirectionSwapWithProfitModel:profitModel
                                                 WithProfitSwap:profitSwap];
                }
            }else{
                if (profitModel && profitSwap) {
                    if ([profitModel integerForKey:SWAPDIRECRION] == -1) {
                        swap = baseSwap * profitSwap;
                    }else{
                        swap = baseSwap/profitSwap;
                    }
                }else{
                    swap = 0;
                }
            }
            return swap;
        }
        return swap;
    }
}

- (double)caculateMarginSwapWithSymbolId:(NSInteger)symbolId
                           WithDirection:(NSInteger)direction
                         WithOffsetPrice:(double)offsetPrc
{
    @synchronized ( self ) {
        double swap = 0;
        
        NSString *symbolKey = [self symbolId:symbolId];
        id symbolInfo = _subSymbolDic[symbolKey];
        if (!symbolInfo) {
            return swap;
        }else if([symbolInfo isKindOfClass:[NSNumber class]]){
            swap = [self directionMarginSwapWithSymId:symbolId
                                        WithDirection:direction
                                        WithOffsetPrc:offsetPrc];
            return swap;
        }else if([symbolInfo isKindOfClass:[NSDictionary class]]){
            NSMutableDictionary *currrentInfo = [NSMutableDictionary dictionaryWithDictionary:symbolInfo];
            
            NSInteger directionValue = direction;
            
            double baseSwap = [self swap:currrentInfo
                                    Type:BASEMODEL
                                     Dir:directionValue
                               OffsetPrc:offsetPrc
                                   SymId:symbolId];
            
            double profitSwap = [self swap:currrentInfo
                                      Type:PROFITMODEL
                                       Dir:directionValue
                                 OffsetPrc:offsetPrc
                                     SymId:symbolId];
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
            double positionSwap = [self swap:currrentInfo
                                        Type:POSITIONMODEL
                                         Dir:directionValue
                                   OffsetPrc:offsetPrc
                                       SymId:symbolId];
            
            double magnSwap = [self swap:currrentInfo
                                    Type:MAGNSYMMODEL
                                     Dir:directionValue
                               OffsetPrc:offsetPrc
                                   SymId:symbolId];
#pragma clang diagnostic pop

            [_subSymbolDic setObject:currrentInfo forKey:[self symbolId:symbolId]];

            if (![_currency length]) {
                return swap;
            }
            
            IXSymbolM *model = [self symModelWithId:symbolId];
            if(model.cataId == FXCATAID){
                if([_currency isEqualToString:USDCRY]){
                    swap = magnSwap;
                }else{
                    NSDictionary *bsModel = currrentInfo[BASEMODEL];
                    double bsSwap = [self indirectionSwapWithProfitModel:bsModel
                                                          WithProfitSwap:baseSwap];
                    swap = magnSwap/bsSwap;
                }
            }else{
                NSDictionary *profitModel = currrentInfo[PROFITMODEL];
                if(profitModel && [_currency length]){
                    if ([_currency isEqualToString:USDCRY]) {
                        if (profitModel) {
                            swap = [self indirectionSwapWithProfitModel:profitModel
                                                         WithProfitSwap:profitSwap];
                        }
                    }else{
                        if (profitModel && profitSwap) {
                            if ([profitModel integerForKey:SWAPDIRECRION] == -1) {
                                swap = baseSwap * profitSwap;
                            }else{
                                swap = baseSwap/profitSwap;
                            }
                        }else{
                            swap = 0;
                        }
                    }
                }
            }
            return swap;
        }
        return swap;
    }
}


/**
 直接货币汇率：如果是外汇：汇率按baseCry算；其他分类保持不变

 @param symbolId 产品Id
 @param direction baseCry是否为账户货币
 @param offsetPrc 价格差
 @return 汇率
 */
- (double)directionSwapWithSymId:(NSInteger)symbolId
                   WithDirection:(NSInteger)direction
                   WithOffsetPrc:(double)offsetPrc
{
    double swap = 0;
    if (direction == 1) {
        swap = 1.0;
    }else{
        IXQuoteM *quoteModel = [self getQuoteDataBySymbolId:symbolId];

        double price = [self getRefpriceWithSymbolId:symbolId
                                           WithQuote:quoteModel
                                            WithEarn:(offsetPrc >= 0)
                                           WithDivid:(direction == -1)];
        if(price != 0){
            swap = 1.0/price;
        }else{
            swap = 0;
        }
    }
    return swap;
}

- (double)directionMarginSwapWithSymId:(NSInteger)symbolId
                         WithDirection:(NSInteger)direction
                         WithOffsetPrc:(double)offsetPrc
{
    double swap = 0;
    
    IXQuoteM *quoteModel = [self getQuoteDataBySymbolId:symbolId];
    
    
    IXSymbolM *model = [self symModelWithId:symbolId];
    if(model.cataId == FXCATAID){
        if(direction == -1){
            swap = 1.0;
        }
        else{
            swap = [self getRefpriceWithSymbolId:symbolId
                                       WithQuote:quoteModel
                                        WithEarn:NO
                                       WithDivid:NO];
        }
    }else{
        if (direction == 1) {
            swap = 1.0;
        }else{
            double price = [self getRefpriceWithSymbolId:symbolId
                                               WithQuote:quoteModel
                                                WithEarn:(offsetPrc >= 0)
                                               WithDivid:(direction == -1)];
            if(0 != price){
                swap = 1.0/price;
            }
        }
    }

    return swap;
}

/**
 间接货币汇率

 @param profitModel 计算汇率的模型
 @param profitSwap 盈亏货币汇率
 @return 汇率
 */
- (double)indirectionSwapWithProfitModel:(NSDictionary *)profitModel
                          WithProfitSwap:(double)profitSwap
{
    double swap = 0;
    if(profitSwap != 0){
        if ([profitModel[SWAPDIRECRION] integerValue] == -1) {
            swap = profitSwap;
        }else{
            swap = 1.0/profitSwap;
        }
    }
    return swap;
}

/**
 *  可用资金 = 净值 - 保证金
 *
 *  @return 净值
 */
- (double)getAvailabelFunds
{
    _availableFunds = [self getNet] - _totalMargin;
    return _availableFunds;
}

/**
 *  净值 = 浮动盈亏 + 可用余额
 *
 *
 *  @return 净值
 */
- (double)getNet
{
    _equity = _totalProfit + [IXUserInfoMgr shareInstance].userLogInfo.account.balance - _totalCommission + _totalFee;
    return _equity;
}

/*保证金比例*/
- (double)marginRate
{
    if( _totalMargin == 0 ){
        return 0;
    }else{
        return  _equity/fabs(_totalMargin);
    }
}

- (double)getOffsetPrc:(NSString *)nPrice Pos:(item_position *)pos
{
    double offPrc = 0;
    if (nPrice) {
        NSArray *openInfo = _openPriceDic[@(pos.id_p)];
        if ([openInfo count]) {
            NSDecimalNumber *openD = [NSDecimalNumber decimalNumberWithString:[openInfo[0] stringValue]];
            NSDecimalNumber *nD = [NSDecimalNumber decimalNumberWithString:nPrice];
            
            if (pos.direction == item_position_edirection_DirectionBuy) {
                offPrc = [[nD decimalNumberBySubtracting:openD] doubleValue];
            }else{
                offPrc = [[openD decimalNumberBySubtracting:nD] doubleValue];
            }
        }
    }
    return offPrc;
}

- (PFTModel *)caculateProfitPosId:(NSInteger)posId
{
   __block item_position *pos;
    [positionList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        item_position *des = (item_position *)obj;
        if (des.id_p == posId) {
            pos = des;
            *stop = YES;
        }
    }];
    
    return [self caculateProfit:pos];
}


/**
 持仓计算当前盈亏

 @param pos 持仓
 @return 盈亏
 */
- (PFTModel *)caculateProfit:(item_position *)pos
{
    PFTModel *model = [PFTModel new];
    if (!pos) {
        return model;
    }
    
    double offPrc;
    IXQuoteM *qModel = [self getQuoteDataBySymbolId:pos.symbolid];
    NSString *nPrice = [self getNprc:pos Quote:qModel];
    if (nPrice) {
        offPrc = [self getOffsetPrc:nPrice Pos:pos];
    }else{
        return model;
    }
    
    double swap = [self caculateSwapWithSymbolId:pos.symbolid
                                 WithOffsetPrice:offPrc];
    
    double profit = offPrc * pos.volume * swap;
    
    model.profit = profit;
    model.qModel = qModel;
    model.posId = pos.id_p;
    model.nPrice = [nPrice doubleValue];
    
    return model;
}
@end
