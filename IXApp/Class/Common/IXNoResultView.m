//
//  IXNoResultView.m
//  IXApp
//
//  Created by Seven on 2017/5/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXNoResultView.h"

@implementation IXNoResultView

+ (instancetype)noResultViewWithFrame:(CGRect)frame image:(UIImage *)img title:(NSString *)title
{
    IXNoResultView  * v = [[IXNoResultView alloc] initWithFrame:frame image:img title:title];
    return v;
}


- (instancetype)initWithFrame:(CGRect)frame image:(UIImage *)img title:(NSString *)title
{
    if (self = [super initWithFrame:frame]) {
        [self createSubviewWith:img title:title];
        
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tap];
    }
    
    return self;
}

- (void)createSubviewWith:(UIImage *)img title:(NSString *)title
{
    UIImageView * noResultV = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-135)/2,
                                                               184,
                                                               135,
                                                               99)];
    noResultV.image = img;
    [self addSubview:noResultV];
    
    UILabel     * noResultLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 184+99+19, kScreenWidth, 30)];
    noResultLab.font = PF_MEDI(15);
    noResultLab.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4c6072);
    noResultLab.textAlignment = NSTextAlignmentCenter;
    noResultLab.text = title;
    [self addSubview:noResultLab];
}

- (void)tapAction
{
    if (self.tapCallback) {
        self.tapCallback();
    }
}

@end
