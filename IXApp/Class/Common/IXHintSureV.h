//
//  IXHintSureV.h
//  IXApp
//
//  Created by Evn on 2018/1/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^hintSureBlock)();
@interface IXHintSureV : UIView

@property (nonatomic, copy)hintSureBlock hintSureB;
- (void)showTitle:(NSString *)title
          content:(NSString *)content;

@end
