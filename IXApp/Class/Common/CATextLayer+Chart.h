//
//  CATextLayer+Chart.h
//  IXApp
//
//  Created by Seven on 2017/8/30.
//  Copyright © 2017年 IX. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CATextLayer (Chart)

+ (instancetype)textLayerWithText:(NSString *)text
                         fontName:(NSString *)fontName
                        textColor:(UIColor *)color
                  backgroundColor:(UIColor *)backgroundColor
                            frame:(CGRect)frame
                    textAlignment:(NSTextAlignment)alignment;

+ (instancetype)textLayerWithText:(NSString *)text
                             font:(UIFont *)font
                        textColor:(UIColor *)color
                  backgroundColor:(UIColor *)backgroundColor
                            frame:(CGRect)frame
                    textAlignment:(NSTextAlignment)alignment;

//+ (CAShapeLayer *)shapedTextLayerWith:(NSString *)str
//                                 font:(UIFont *)font
//                            textColor:(UIColor *)textColor
//                      backgroundColor:(UIColor *)backgroundColor
//                             textSize:(CGSize)size
//                                frame:(CGRect)frame;

@end
