//
//  IXAccountGroupModel.h
//  IXApp
//
//  Created by Evn on 2017/12/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXAccountGroupModel : NSObject

+ (IXAccountGroupModel *)shareInstance;
@property (nonatomic, strong)  NSDictionary *accGroupDic;
- (void)clearInstance;

@end
