//
//  IXPosCalM.h
//  IXApp
//
//  Created by Bob on 2017/12/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXPosCalM : NSObject

@property (nonatomic, assign) double tVol;

@property (nonatomic, assign) double avgPrc;

@end
