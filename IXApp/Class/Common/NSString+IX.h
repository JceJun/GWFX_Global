//
//  NSString+IX.h
//  IXApp
//
//  Created by Seven on 2017/9/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (IX)


/**
 返回self对应的富文本，数字字体Robot，其他字体PingFang
 
 @param size 字体大小
 @return 富文本
 */
- (NSAttributedString *)attributeStringWithFontSize:(NSInteger)size textColor:(UIColor *)color;


/**
 返回self对应的富文本

 @param numFont 数字字体
 @param otherFont 其他字符字体
 @return 富文本
 */
- (NSAttributedString *)attributeStringWithnumberFont:(UIFont *)numFont commonFont:(UIFont *)otherFont textColor:(UIColor *)color;

// 计算文本尺寸
+ (CGSize)sizeWithText:(NSString *)text andFont:(UIFont *)font andMaxSize:(CGSize)maxSize;

// 截取字符串 (含首尾)
- (NSString *)subStringFrom:(NSString *)startString to:(NSString *)endString;

@end
