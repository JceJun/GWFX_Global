//
//  IXUserDefaultM.m
//  IXApp
//
//  Created by Bob on 2017/2/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUserDefaultM.h"
#import "IXAppUtil.h"
#import "IXCpyConfig.h"
#import "UIImageView+SepLine.h"
#import "IXRootTabBarVC.h"
#import "IXBORequestMgr+Region.h"

#define BANKLIST        @"BANKLIST"
#define CITYLIST        @"CITYLIST"
#define PROVINCELIST    @"PROVINCELIST"
#define CAP             @"CITYLISTAndPROVICELIST"

#define kNormalLight        @"settingNormalLight"
#define kSoundAlertEnanle   @"soundAlertEnable"
#define kShakeAlertEnable   @"shakeAlertEnable"

#define kUnitSetting        @"unitSettingOption"
#define kIsNightMode        @"isNightMode"
#define kSymbolCataStyle    @"symbolCataStyle"
#define kNationalityCode    @"nationalityCode"
#define kCode               @"code" //ISO_3166_840

#define DEFAULTSYMBOLCATA @"DEFAULTSYMBOLCATA"

/** 新手指引 */
#define kNewGuideBroadside      @"NewGuideBroadside"
#define kNewGuideCata           @"NewGuideCata"
#define kNewGuidePosition       @"NewGuidePosition"
#define kNewGuideKLineDetail    @"NewGuideKLineDetail"
#define kNewGuideKlineLandscape @"NewGuideKlineLandscape"
#define kNewGuideDomIn          @"NewGuideDomIn"
#define kNewGuideDomMp          @"NewGuideDomMp"
#define kNewGuideDomLimit       @"NewGuideDomLimit"
#define kNewGuideDomClick       @"NewGuideDomClick"

@implementation IXUserDefaultM

+ (void)saveChooseSymbolCata:(NSInteger)chooseRow
{
    NSString *accId = [NSString stringWithFormat:@"%llu",[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *symCata = [NSMutableDictionary dictionaryWithDictionary:[stand objectForKey:DEFAULTSYMBOLCATA]];
    if ( symCata ) {
        [symCata setValue:@(chooseRow) forKey:accId];
    }else{
        symCata = [NSMutableDictionary dictionaryWithDictionary:@{accId:@(chooseRow)}];
    }
    [stand setValue:symCata forKey:DEFAULTSYMBOLCATA];
    [stand synchronize];
}

//默认选中热门
+ (NSInteger)getChooseSymbolCata
{
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    id symCata = [stand objectForKey:DEFAULTSYMBOLCATA];
    if ( symCata && [symCata isKindOfClass:[NSDictionary class]] ) {
        NSString *accId = [NSString stringWithFormat:@"%llu",[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        
        NSInteger chooseRow = 1;
        if ( [[symCata allKeys] containsObject:accId] ) {
            chooseRow = [symCata integerForKey:accId];
        }
        return ( chooseRow < 0 ? 1 : chooseRow);
    }
    return 1;
}

+ (void)saveDomResult:(BOOL)status
{
    [[NSUserDefaults standardUserDefaults] setValue:@(status) forKey:kIXDomTip];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)getDomResult
{
    id reult = [[NSUserDefaults standardUserDefaults] objectForKey:kIXDomTip];
    if ( !reult || ![reult isKindOfClass:[NSNumber class]] ) {
        return YES;
    }
    return [reult boolValue];
}

/** 是否设置了屏幕常亮 */
+ (BOOL)screenNormalLight
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNormalLight];
}

/** 设置常量 */
+ (void)setScreenNormalLight:(BOOL)light
{
    [[NSUserDefaults standardUserDefaults] setBool:light forKey:kNormalLight];
    [[UIApplication sharedApplication] setIdleTimerDisabled:light];
}

/** 根据设置决定是否设置屏幕常亮 */
+ (void)dealScreenNormalLight
{
    BOOL disable = [UIApplication sharedApplication].idleTimerDisabled;
    if ([self screenNormalLight] && !disable) {
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    }
    else if (![self screenNormalLight] && disable){
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

//声音提示是否可用
+ (BOOL)soundAlertEnable
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kSoundAlertEnanle];
}

//震动提示是否可用
+ (BOOL)shakeAlertEnable
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShakeAlertEnable];
}

//设置声音提示可用与否
+ (void)setSoundAlertEnable:(BOOL)enable
{
    [[NSUserDefaults standardUserDefaults] setBool:enable forKey:kSoundAlertEnanle];
    if (enable) {
        [[IXUserInfoMgr shareInstance] showSoundAlert];
    }
}

//设置震动提示可用与否
+ (void)setShakeAlertEnable:(BOOL)enable
{
    [[NSUserDefaults standardUserDefaults] setBool:enable forKey:kShakeAlertEnable];
    if (enable) {
        [[IXUserInfoMgr shareInstance] showShakeAlert];
    }
}

//处理提示
+ (void)dealAlert
{
    BOOL sound = [self soundAlertEnable];
    BOOL shake = [self shakeAlertEnable];
    
    if (sound && shake) {
        [[IXUserInfoMgr shareInstance] showSoundAndShakeAlert];
    }
    else if (sound){
        [[IXUserInfoMgr shareInstance] showSoundAlert];
    }
    else if (shake){
        [[IXUserInfoMgr shareInstance] showShakeAlert];
    }
}

//是否为夜间模式
+ (BOOL)isNightMode
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kIsNightMode];
}

+ (void)setNightMode:(BOOL)isNightMode
{
    [[NSUserDefaults standardUserDefaults] setBool:isNightMode forKey:kIsNightMode];
    
    [self dealDayNightMode];
}

+ (void)setQuoteColor:(BOOL)isQuoteColor
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (isQuoteColor) {
        [ud setValue:@"RED" forKey:@"COLORSETTING"];
        [IXUserInfoMgr shareInstance].settingRed = YES;
    } else {
        [ud setValue:@"GREEN" forKey:@"COLORSETTING"];
        [IXUserInfoMgr shareInstance].settingRed = NO;
    }
    [ud synchronize];
}

+ (void)dealDayNightMode
{
    [[DKNightVersionManager sharedManager] setSupportsKeyboard:NO]; //不使用键盘的夜间模式
    if ([self isNightMode]) {
        [IXUserInfoMgr shareInstance].isNightMode =  YES;
        [[DKNightVersionManager sharedManager] nightFalling];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }else{
        [IXUserInfoMgr shareInstance].isNightMode =  NO;
        [[DKNightVersionManager sharedManager] dawnComing];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
    [IXUserDefaultM updateNavBarUI];
}

+ (void)updateNavBarUI
{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    IIViewDeckController *rootVC = (IIViewDeckController *)delegate.window.rootViewController;
    if( [rootVC isKindOfClass:[IIViewDeckController class]] ){
        IXRootTabBarVC *tabBar = (IXRootTabBarVC *)rootVC.centerController;
        NSArray *vcArr = tabBar.viewControllers;
        for (IXBaseNavVC *nav in vcArr) {
            UIColor *color = UIColorHexFromRGB(0xe2eaf2);
            if([IXUserInfoMgr shareInstance].isNightMode){
                color = UIColorHexFromRGB(0x242a36);
            }
            [nav.navigationBar setShadowImage:[UIImageView switchToImageWithColor:color size:CGSizeMake(nav.view.frame.size.width, kLineHeight)]];
        }
    }
}

/**
 打开关闭相册和相机时处理状态栏颜色
 */
+ (void)dealStatusBarWithAlbumState:(BOOL)open
{
    if ([self isNightMode]) {
        if (open) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }else{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
    }
}

+ (UnitSettingType)unitSetting
{
    NSInteger type = [[NSUserDefaults standardUserDefaults] integerForKey:kUnitSetting];
    
    if (type == UnitSettingTypeLot) {
        return UnitSettingTypeLot;
    }
    
    return UnitSettingTypeCount;
}

+ (void)setDefaultUnit
{
    [IXUserDefaultM setUnit:(UnitSettingType)SymbolUnit];
}

+ (void)setUnit:(UnitSettingType)type
{
    [[NSUserDefaults standardUserDefaults] setInteger:type forKey:kUnitSetting];
}

#pragma mark -
#pragma mark - other



+ (void)saveBankList:(NSArray *)arr
{
    if ( arr && [arr isKindOfClass:[NSArray class]] ) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
        [[NSUserDefaults standardUserDefaults] setValue:data forKey:BANKLIST];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (NSArray *)getBankList
{
    id value = [[NSUserDefaults standardUserDefaults] objectForKey:BANKLIST];
    if ( !value ) {
        value = [NSArray array];
    }else{
        value = [NSKeyedUnarchiver unarchiveObjectWithData:value];

    }
    return value;
}

+ (void)saveCityAndProvnice:(NSArray *)arr
{
    if ( arr && [arr isKindOfClass:[NSArray class]] ) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
        [[NSUserDefaults standardUserDefaults] setValue:data forKey:CAP];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (NSArray *)getCityAndProvnice
{
    id value = [[NSUserDefaults standardUserDefaults] objectForKey:CAP];
    if ( !value ) {
        value = [NSArray array];
    }else{
        value = [NSKeyedUnarchiver unarchiveObjectWithData:value];
        
    }
    return value;
}

+ (void)saveProvince:(NSArray *)arr
{
    if ( arr && [arr isKindOfClass:[NSArray class]] ) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
        [[NSUserDefaults standardUserDefaults] setValue:data forKey:PROVINCELIST];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (NSArray *)getProvince
{
    id value = [[NSUserDefaults standardUserDefaults] objectForKey:PROVINCELIST];
    if ( !value ) {
        value = [NSArray array];
    }else{
        value = [NSKeyedUnarchiver unarchiveObjectWithData:value];
        
    }
    return value;
}


+ (void)saveCity:(NSArray *)arr
{
    if ( arr && [arr isKindOfClass:[NSArray class]] ) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
        [[NSUserDefaults standardUserDefaults] setValue:data forKey:CITYLIST];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (NSArray *)getCity
{
    id value = [[NSUserDefaults standardUserDefaults] objectForKey:CITYLIST];
    if ( !value ) {
        value = [NSArray array];
    }else{
        value = [NSKeyedUnarchiver unarchiveObjectWithData:value];
        
    }
    return value;
}

#pragma mark -
#pragma mark - 新手指引
+ (void)saveNewGuide:(IXNewGuide)type
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    switch (type) {
        case IXNewGuideBroadside:{
            [ud setBool:YES forKey:kNewGuideBroadside];
        }
            break;
        case IXNewGuideCata:{
            [ud setBool:YES forKey:kNewGuideCata];
        }
            break;
        case IXNewGuidePosition:{
            [ud setBool:YES forKey:kNewGuidePosition];
        }
            break;
        case IXNewGuideKLineDetail:{
            [ud setBool:YES forKey:kNewGuideKLineDetail];
        }
            break;
        case IXNewGuideKlineLandscape:{
            [ud setBool:YES forKey:kNewGuideKlineLandscape];
        }
            break;
        case IXNewGuideDomIn:{
            [ud setBool:YES forKey:kNewGuideDomIn];
        }
            break;
        case IXNewGuideDomMp:{
            [ud setBool:YES forKey:kNewGuideDomMp];
        }
            break;
        case IXNewGuideDomLimit:{
            [ud setBool:YES forKey:kNewGuideDomLimit];
        }
            break;
        case IXNewGuideDomClick:{
            [ud setBool:YES forKey:kNewGuideDomClick];
        }
            break;
            
        default:
            break;
    }
    [ud synchronize];
}

+ (BOOL)getNewGuide:(IXNewGuide)type;
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL ret = NO;
    switch (type) {
        case IXNewGuideBroadside:{
            ret = [ud boolForKey:kNewGuideBroadside];
        }
            break;
        case IXNewGuideCata:{
            ret = [ud boolForKey:kNewGuideCata];
        }
            break;
        case IXNewGuidePosition:{
            ret = [ud boolForKey:kNewGuidePosition];
        }
            break;
        case IXNewGuideKLineDetail:{
            ret = [ud boolForKey:kNewGuideKLineDetail];
        }
            break;
        case IXNewGuideKlineLandscape:{
            ret = [ud boolForKey:kNewGuideKlineLandscape];
        }
            break;
        case IXNewGuideDomIn:{
            ret = [ud boolForKey:kNewGuideDomIn];
        }
            break;
        case IXNewGuideDomMp:{
            ret = [ud boolForKey:kNewGuideDomMp];
        }
            break;
        case IXNewGuideDomLimit:{
            ret = [ud boolForKey:kNewGuideDomLimit];
        }
            break;
        case IXNewGuideDomClick:{
            ret = [ud boolForKey:kNewGuideDomClick];
        }
            break;
        default:
            break;
    }
    return ret;
}

#pragma mark -
#pragma mark - 产品分类显示样式
+ (void)saveSymbolCataStyle:(SymCataStyle)style
{
    [[NSUserDefaults standardUserDefaults] setInteger:style forKey:kSymbolCataStyle];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (SymCataStyle)symbolCataStyle
{
    NSInteger type = [[NSUserDefaults standardUserDefaults] integerForKey:kSymbolCataStyle];
    if (type == SymbolCataStyleHorizontal) {
        return SymbolCataStyleHorizontal;
    }
    return SymbolCataStyleVertical;
}

+ (void)saveCountryCode:(NSString *)code
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:code forKey:kNationalityCode];
    [ud synchronize];
    NSString *nationality = [IXUserDefaultM nationalityByNationalCode:code];
    if (nationality.length) {
        [ud setObject:nationality forKey:kCode];
        [ud synchronize];
    }
}

+ (NSString *)nationalityByNationalCode:(NSString *)code
{
    NSString *retStr = nil;
    NSArray *regionArr = [IXBORequestMgr region_refreshCountryList:nil];
    if (regionArr.count) {
        for (int i = 0; i < regionArr.count; i++) {
            IXCountryM *countryM = regionArr[i];
            if ([countryM.nationalCode isEqualToString:code]) {
                retStr = countryM.code;
                break;
            }
        }
    }
    return retStr;
}


+ (NSString *)countryCodeBynationality:(NSString *)code
{
    NSString *retStr = nil;
    NSArray *regionArr = [IXBORequestMgr region_refreshCountryList:nil];
    if (regionArr.count) {
        for (int i = 0; i < regionArr.count; i++) {
            IXCountryM *countryM = regionArr[i];
            if ([countryM.nationalCode isEqualToString:code]) {
                retStr = countryM.code;
                break;
            }
        }
    }
    return retStr;
}

+ (NSString *)nationalCodeByCode:(NSString *)code
{
    NSString *retStr = nil;
    NSArray *regionArr = [IXBORequestMgr region_refreshCountryList:nil];
    if (regionArr.count) {
        for (int i = 0; i < regionArr.count; i++) {
            IXCountryM *countryM = regionArr[i];
            if ([countryM.code isEqualToString:code]) {
                retStr = countryM.nationalCode;
                break;
            }
        }
    }
    return retStr;
}

+ (IXCountryM *)nationalityByCode:(NSString *)code
{
    NSArray *regionArr = [IXBORequestMgr region_refreshCountryList:nil];
    if (regionArr.count) {
        for (int i = 0; i < regionArr.count; i++) {
            IXCountryM *countryM = regionArr[i];
            if ([countryM.code isEqualToString:code]) {
                return countryM;
                break;
            }
        }
    }
    return nil;
}

+ (IXCountryM *)nationalityModelByNationalCode:(NSString *)nationalCode
{
    NSArray *regionArr = [IXBORequestMgr region_refreshCountryList:nil];
    if (regionArr.count) {
        for (int i = 0; i < regionArr.count; i++) {
            IXCountryM *countryM = regionArr[i];
            if ([countryM.nationalCode isEqualToString:nationalCode]) {
                return countryM;
                break;
            }
        }
    }
    return nil;
}

+ (IXCountryM *)nationalityByCountryCode:(NSString *)countryCode
{
    NSArray *regionArr = [IXBORequestMgr region_refreshCountryList:nil];
    if (regionArr.count) {
        for (int i = 0; i < regionArr.count; i++) {
            IXCountryM *countryM = regionArr[i];
            if (countryM.countryCode == [countryCode integerValue]) {
                return countryM;
                break;
            }
        }
    }
    return nil;
}

+ (void)saveNationality:(NSString *)nationality;
{
    [[NSUserDefaults standardUserDefaults] setObject:nationality forKey:kCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getCountryCode
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *retStr = [ud objectForKey:kNationalityCode];
    if (!retStr.length) {
        //默认获取手机的国家编码
        retStr = [IXDataProcessTools currentCountryCode];
        //如果为中国映射为印尼
        if (SameString(retStr, @"CN")) {
            retStr = @"ID";
        }
        if (retStr.length) {
            [ud setObject:retStr forKey:kNationalityCode];
            [ud synchronize];
            NSString *nationality = [IXUserDefaultM nationalityByNationalCode:retStr];
            if (nationality.length) {
                [ud setObject:nationality forKey:kCode];
                [ud synchronize];
            }
        }
    }
    return retStr;
}

+ (NSString *)getCode
{
    NSString *retStr = [[NSUserDefaults standardUserDefaults] objectForKey:kCode];
    return retStr;
}

+ (BOOL)isChina
{
    if ([[IXUserDefaultM getCountryCode] isEqualToString:@"CN"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
