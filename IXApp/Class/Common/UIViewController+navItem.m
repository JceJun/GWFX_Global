//
//  UIViewController+navItem.m
//  FXApp
//
//  Created by Seven on 2017/7/18.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "UIViewController+navItem.h"
#import "IXLeftNavView.h"
#import "IXNavItem.h"

@implementation UIViewController (navItem)

- (void)addBackItem
{
    if (self.navigationController) {
        UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithImage:GET_IMAGE_NAME(@"common_nav_leftBack")
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(backAction)];
        leftBarItem.dk_tintColorPicker = DKCellTitleColor;
        [leftBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MarketSymbolNameColor}
                                   forState:UIControlStateNormal];
        
        self.navigationItem.leftBarButtonItem = leftBarItem;
    }
}

- (void)addCancelItem{
    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
}

- (void)cancelAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)addBackItemrget:(id)target action:(SEL)sel
{
    if (self.navigationController) {
        UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithImage:GET_IMAGE_NAME(@"common_nav_leftBack")
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:target
                                                                       action:sel];
        leftBarItem.dk_tintColorPicker = DKCellTitleColor;
        [leftBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MarketSymbolNameColor}
                                   forState:UIControlStateNormal];
        
        self.navigationItem.leftBarButtonItem = leftBarItem;
    }
}

- (void)addAvatarLeftItemWithTarget:(id)targett selector:(SEL)sel
{
    if (self.navigationController) {
        IXLeftNavView  * item = [[IXLeftNavView alloc] initWithTarget:targett action:sel];
        CGSize  size = item.frame.size;
        item.frame = CGRectMake(15, 20, size.width, size.height);
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:item];
    }
}

- (void)addAvatarLeftItemWithTarget:(id)targett selector:(SEL)sel nickName:(NSString *)nickName
{
    if (self.navigationController) {
        IXLeftNavView  * item = [[IXLeftNavView alloc] initWithTarget:targett action:sel];
        item.typeLab.text = nickName;
        CGSize  size = item.frame.size;
        item.frame = CGRectMake(15, 20, size.width, size.height);
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:item];
    }
}

- (void)addDismissItemWithTitle:(NSString *)title
{
    if (self.navigationController) {
        IXNavItem   * item = [[IXNavItem alloc] initWithTarget:self
                                                        action:@selector(dismissAction)
                                                         title:title
                                                   isRightItem:YES];
        
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:item];
    }
}

- (void)addDismissItemWithTitle:(NSString *)title target:(id)target action:(SEL)sel
{
    if (self.navigationController) {
        IXNavItem   * item = [[IXNavItem alloc] initWithTarget:target
                                                        action:sel
                                                         title:title
                                                   isRightItem:YES];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:item];
    }
}

- (void)addRightItemWithImageNamed:(NSString *)imgName target:(id)target action:(SEL)sel
{
    if (self.navigationController) {
        UIButton    * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [btn setImage:AutoNightImageNamed(imgName) forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(4, 8, 4, 0)];
        [btn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    }
}

- (void)addRightItemWithDayImgNamed:(NSString *)dayName nightImgNamed:(NSString *)nightName target:(id)target action:(SEL)sel
{
    if (self.navigationController) {
        UIButton    * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [btn dk_setImage:DKImageNames(dayName, nightName) forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(4, 8, 4, 0)];
        [btn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        [btn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];

        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismissAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// 返回指定页
- (BOOL)goBackTo:(id)vc_Class animated:(BOOL)animated{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:vc_Class]) {                        [self.navigationController popToViewController:aViewController animated:animated];
            return YES;
        }}
    return NO;
}

@end
