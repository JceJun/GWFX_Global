//
//  IXHintV.m
//  IXApp
//
//  Created by Evn on 2018/1/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXHintV.h"

@interface IXHintV()

@property (nonatomic, strong)UIView *alertV;
@property (nonatomic, strong)UIScrollView *sView;
@property (nonatomic, strong)UILabel *hint;
@property (nonatomic, strong)UILabel *content;

@property (nonatomic, copy)NSString *titleStr;
@property (nonatomic, copy)NSString *contentStr;
@property (nonatomic, assign)CGFloat width;
@property (nonatomic, assign)CGFloat height;

@end

@implementation IXHintV

- (void)showTitle:(NSString *)title
          content:(NSString *)content
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75f];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    [self addGestureRecognizer:tap];
    self.userInteractionEnabled = YES;
    self.titleStr = title;
    self.contentStr = content;
    self.width = 295;
    CGSize size = [self.contentStr boundingRectWithSize:CGSizeMake(self.width - 30 , MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName:PF_MEDI(13)}
                                                context:nil].size;
    self.height = size.height + size.height*5.f/21.f;//需加上行间距
    
    [self alertV];
    [self hint];
    [self sView];
    [self content];
    [self show];
}

- (void)show
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.center = keyWindow.center;
    [keyWindow addSubview:self];
    
    self.alertV.center = keyWindow.center;
    [keyWindow addSubview:self.alertV];
}

- (UIView *)alertV
{
    if (!_alertV) {
        _alertV = [[UIView alloc]init];
        if (self.height > 100) {
            _alertV.frame = CGRectMake(0, 0, self.width, 195);
        } else {
            _alertV.frame = CGRectMake(0, 0, self.width, 92 + self.height);
        }
        _alertV.dk_backgroundColorPicker = DKViewColor;
        _alertV.layer.cornerRadius = 10;
        _alertV.layer.masksToBounds = YES;
    }
    return _alertV;
}

- (UILabel *)hint
{
    if ( !_hint ) {
        _hint = [IXUtils createLblWithFrame:CGRectMake( 15, 28, self.width - 30, 18)
                                   WithFont:PF_MEDI(15)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        _hint.text = _titleStr;
        [self.alertV addSubview:_hint];
    }
    return _hint;
}

- (UIScrollView *)sView
{
    if (!_sView) {
        _sView = [[UIScrollView alloc] init];
        if (self.height > 100) {
            _sView.frame = CGRectMake(0, GetView_MaxY(self.hint) + 20, VIEW_W(self.alertV), 100);
        } else {
            _sView.frame = CGRectMake(0, GetView_MaxY(self.hint) + 20, VIEW_W(self.alertV), self.height);
        }
        _sView.contentSize = CGSizeMake(self.width, self.height);
        _sView.showsVerticalScrollIndicator = NO;
        _sView.showsHorizontalScrollIndicator = NO;
        _sView.backgroundColor = [UIColor clearColor];
        [self.alertV addSubview:_sView];
    }
    return _sView;
}

- (UILabel *)content
{
    if (!_content) {
        _content = [IXUtils createLblWithFrame:CGRectMake( 15, 0, self.width - 30, self.height)
                                      WithFont:PF_MEDI(13)
                                     WithAlign:NSTextAlignmentCenter
                                    wTextColor:0x99abba
                                    dTextColor:0x8395a4
                    ];
        _content.numberOfLines = 0;
        _content.text = self.contentStr;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.contentStr];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:5];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.contentStr length])];
        _content.attributedText = attributedString;
        _content.lineBreakMode = NSLineBreakByCharWrapping;
        _content.textAlignment = NSTextAlignmentCenter;
        [self.sView addSubview:_content];
    }
    return _content;
}

- (void)dismiss
{
    if (self.alertV) {
        [self.alertV removeFromSuperview];
    }
    if (self) {
        [self removeFromSuperview];
    }
}

@end
