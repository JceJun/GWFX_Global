//
//  IXHintSureV.m
//  IXApp
//
//  Created by Evn on 2018/1/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXHintSureV.h"

@interface IXHintSureV()

@property (nonatomic, strong)UIView *alertV;
@property (nonatomic, strong)UIScrollView *sView;
@property (nonatomic, strong)UILabel *hint;
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UILabel *content;
@property (nonatomic, strong)UIButton *sureBtn;

@property (nonatomic, copy)NSString *titleStr;
@property (nonatomic, copy)NSString *contentStr;
@property (nonatomic, assign)CGFloat width;
@property (nonatomic, assign)CGFloat height;

@end

@implementation IXHintSureV

- (void)showTitle:(NSString *)title
          content:(NSString *)content
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75f];
    self.titleStr = title;
    self.contentStr = content;
    self.width = 295;
    CGSize size = [self.contentStr boundingRectWithSize:CGSizeMake(self.width - 30 , MAXFLOAT)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName:PF_MEDI(13)}
                                           context:nil].size;
    self.height = size.height + size.height*5.f/21.f;//需加上行间距
    
    [self alertV];
    [self hint];
    [self sView];
    [self content];
    [self uLine];
    [self sureBtn];
    [self show];
}

- (void)show
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.center = keyWindow.center;
    [keyWindow addSubview:self];
    
    self.alertV.center = keyWindow.center;
    [keyWindow addSubview:self.alertV];
}

- (UIView *)alertV
{
    if (!_alertV) {
        _alertV = [[UIView alloc]init];
        if (self.height > 100) {
            _alertV.frame = CGRectMake(0, 0, self.width, 240);
        } else {
            _alertV.frame = CGRectMake(0, 0, self.width, 137 + self.height);
        }
        _alertV.dk_backgroundColorPicker = DKViewColor;
        _alertV.layer.cornerRadius = 10;
        _alertV.layer.masksToBounds = YES;
    }
    return _alertV;
}

- (UILabel *)hint
{
    if ( !_hint ) {
        _hint = [IXUtils createLblWithFrame:CGRectMake( 15, 28, self.width - 30, 18)
                                   WithFont:PF_MEDI(15)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        _hint.text = _titleStr;
        [self.alertV addSubview:_hint];
    }
    return _hint;
}

- (UIScrollView *)sView
{
    if (!_sView) {
        _sView = [[UIScrollView alloc] init];
        if (self.height > 100) {
            _sView.frame = CGRectMake(0, GetView_MaxY(self.hint) + 20, VIEW_W(self.alertV), 100);
        } else {
            _sView.frame = CGRectMake(0, GetView_MaxY(self.hint) + 20, VIEW_W(self.alertV), self.height);
        }
        _sView.contentSize = CGSizeMake(self.width, self.height);
        _sView.showsVerticalScrollIndicator = NO;
        _sView.showsHorizontalScrollIndicator = NO;
        _sView.backgroundColor = [UIColor clearColor];
        [self.alertV addSubview:_sView];
    }
    return _sView;
}

- (UILabel *)content
{
    if (!_content) {
        _content = [IXUtils createLblWithFrame:CGRectMake( 15, 0, self.width - 30, self.height)
                                      WithFont:PF_MEDI(13)
                                     WithAlign:NSTextAlignmentCenter
                                    wTextColor:0x99abba
                                    dTextColor:0x8395a4
                    ];
        _content.numberOfLines = 0;
        _content.text = self.contentStr;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.contentStr];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:5];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.contentStr length])];
        _content.attributedText = attributedString;
        _content.lineBreakMode = NSLineBreakByCharWrapping;
        _content.textAlignment = NSTextAlignmentCenter;
        [self.sView addSubview:_content];
    }
    return _content;
}

- (UIView *)uLine
{
    if (!_uLine) {
        _uLine = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(self.alertV) - (46 + kLineHeight), self.width, kLineHeight)];
        _uLine.dk_backgroundColorPicker = DKLineColor;
        [self.alertV addSubview:_uLine];
    }
    return _uLine;
}

- (UIButton *)sureBtn
{
    if (!_sureBtn) {
        _sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, VIEW_H(self.alertV) - 46,VIEW_W(self.alertV), 46)];
        [_sureBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        _sureBtn.tag = 1;
        _sureBtn.titleLabel.font = PF_MEDI(15);
        _sureBtn.backgroundColor = [UIColor clearColor];
        [_sureBtn setTitle:LocalizedString(@"确  定") forState:UIControlStateNormal];
        [_sureBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [self.alertV addSubview:_sureBtn];
    }
    return _sureBtn;
}

- (void)click
{
    if (self.alertV) {
        [self.alertV removeFromSuperview];
    }
    if (self) {
        [self removeFromSuperview];
    }
    if (self.hintSureB) {
        self.hintSureB();
    }
}

@end
