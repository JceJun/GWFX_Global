//
//  IXRateCaculate.h
//  IXApp
// 
//  Created by Bob on 2017/1/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXRateCaculate : NSObject

//查询手续费
+ (double)queryComissionWithSymbolId:(NSInteger)symbolId
                    WithSymbolCataId:(NSInteger)cataId;

//是否可交易文字切换
+ (NSString *)symbolTradeState:(IXSymbolTradeState)state;

//计算预计成交价
+ (NSDecimalNumber *)caculateMayDealPriceWithVolume:(long)volume
                                            WithDir:(item_order_edirection)dir
                                          WithQuote:(IXQuoteM *)quote
                                         WithDigits:(NSInteger)digit;

//四舍五入
+ (NSDecimalNumberHandler *)handlerPriceWithDigit:(NSInteger)digit;
@end
