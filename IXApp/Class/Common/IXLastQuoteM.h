//
//  IXLastQuoteM.h
//  IXApp
//
//  Created by Bob on 2017/1/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExAutoModel.h"

@interface IXLastQuoteM : ExAutoModel

@property (nonatomic, assign) NSInteger symbolId;       //产品id

@property (nonatomic, assign) double nLastClosePrice;  //昨收

@end
