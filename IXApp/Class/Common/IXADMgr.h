//
//  IXADMgr.h
//  IXApp
//
//  Created by Evn on 2018/3/15.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXADMgr : NSObject

+ (IXADMgr *)shareInstance;

//自选
@property (nonatomic, strong) NSMutableArray        *selfAdArr;
@property (nonatomic, assign) BOOL                  haveCloseSelf;
@property (nonatomic, assign) BOOL                  selfBannerClose;

//行情主页
@property (nonatomic, strong) NSMutableArray        *homeAdArr;
@property (nonatomic, assign) BOOL                  haveCloseHome;
@property (nonatomic, assign) BOOL                  homeBannerClose;

//登录
@property (nonatomic, strong) NSMutableArray        *loginAdArr;
@property (nonatomic, assign) BOOL                  loginBannerClose;

//注册
@property (nonatomic, strong) NSMutableArray        *registAdArr;
@property (nonatomic, assign) BOOL                  registBannerClose;


// 平仓结果页
@property (nonatomic, strong) NSMutableArray        *positionResArr;
@property (nonatomic, assign) BOOL                  positionResBannerClose;




- (void)clearInstance;

@end
