//
//  IXEntityFormatter.m
//  Communivation
//
//  Created by Bill on 16/10/25.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import "IXEntityFormatter.h"
#import <objc/runtime.h>
#import "IXDateUtils.h"

#import "IXDBLanguageMgr.h"
#import "IXDBGlobal.h"
#import "IXLocalizationModel.h"
#import "IXDateUtils.h"

@implementation IXEntityFormatter

/*
 最初的颜色
 @(0x4886C9), @(0xEBAD62), @(0x4886C9), @(0x4886C9),
 @(0x4886C9), @(0x4886C9), @(0x4886C9), @(0x4886C9),
 @(0x4886C9), @(0x4886C9), @(0x4886C9), @(0x4886C9),
 @(0x4886C9), @(0x4886C9), @(0x4886C9), @(0x4886C9)
 */
+ (NSInteger)getLabelColorValue:(NSInteger)index
{
    NSArray *labelColorArr = @[@(0x6F75F4), @(0xE55F45), @(0x51D1FF), @(0xFFB890),
                               @(0x74DDD7), @(0xE88480), @(0x2774F1), @(0xF36B66),
                               @(0x20D9C5), @(0xFFC741), @(0x9F85E2), @(0xF3A93C),
                               @(0x52ACFF), @(0xE57D38), @(0x6F58E4), @(0xC1DD6B)];
    
    if ( index > labelColorArr.count - 1 || index < 0 ) {
        index = 0;
    }
    return [[labelColorArr objectAtIndex:index] integerValue];
}

//正则表达式判断输入的密码是否只包含数字和字母，然后位数是6-18
+ (BOOL)validPasswd:(NSString *)passwd
{
    BOOL result = NO;
    
    if ( !passwd || ![passwd isKindOfClass:[NSString class]] || ![passwd length] ) {
        return result;
    }
    
    NSString *regex = @"^[0-9A-Za-z]{6,18}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    result = [predicate evaluateWithObject:passwd];
    
    return result;
}

+ (void)showErrorInformation:(NSString *)errorCode
{
    [SVProgressHUD showErrorWithStatus:[IXEntityFormatter getErrorInfomation:errorCode]];
}

+ (NSString *)getErrorInfomation:(NSString *)errorCode
{
    NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:
                                 [IXLocalizationModel currentCheckLanguage] nameSpace:errorCode];
    if ( !languageDic || [languageDic[kValue] length] == 0) {
        
        return errorCode;

    } else {
        
        return languageDic[kValue];
    }
}



+ (NSString *)getMarketNameWithMarketId:(NSInteger)marketId
{
    switch ( marketId ) {
        case 0:
            return @"SH";   //沪股
            break;
        case 1:
            return @"SZ";   //深股
            break;
        case 2:
            return @"HK";   //港股
            break;
        case 3:
            return @"SF";   //上海股指期货
            break;
        case 4:
            return @"SC";   //上海商品
            break;
        case 5:
            return @"DC";   //大连商品
            break;
        case 6:
            return @"ZC";   //郑州商品
            break;
        case 7:
            return @"GW";   //盖威全市场
            break;
        case 8:
            return @"FX";   //外汇
            break;
        case 9:
            return @"US";   //美股
            break;
        case 10:
            return @"PM";   //贵金属
            break;
        case 11:
            return @"IND";  //指数
            break;
        case 12:
            return @"FUT";  //期货
            break;
        case 13:
            return @"CRYPTO";  //数字货币
            break;
        default:
            return @"";
            break;
    }
}

+ (BOOL)isRedUpGreenDown
{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"COLORSETTING"];
    if ( !str || [str isEqualToString:@"RED"] ) {
        return YES;
    }
    return NO;
}

+ (NSComparisonResult)comparePrice:(float)price WithCheckedPrice:(float)checkPrice
{
    NSNumber *pNumber = [NSNumber numberWithFloat:price];
    NSNumber *cNumber = [NSNumber numberWithFloat:checkPrice];
    
    return  [pNumber compare:cNumber];
}

+ (NSString *)getCurrentTimeFormaterHHMM
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:
                          [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    return [formatter stringFromDate:[NSDate date]];
}

+ (NSString *)weekDayStringWithTimeInterval:(NSTimeInterval)timeInterval
{
    NSArray *weekdays = @[@"", @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六"];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:
                            [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [calendar setTimeZone: timeZone];
    
    NSDate  * date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:date];
    
    return LocalizedString([weekdays objectAtIndex:theComponents.weekday]);
}

+ (CGFloat)getContentWidth:(NSString *)content WithFont:(UIFont *)font
{
    return [content sizeWithAttributes:@{NSFontAttributeName:font}].width;
}


+ (NSString *)integerToString:(uint64)integer
{
    return [NSString stringWithFormat:@"%ld",(long)integer];
}

+ (NSString *)numberToString:(NSNumber *)number
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    return [formatter stringFromNumber:number];
}

+ (NSTimeInterval)getCurrentTimeInterval
{
    return [[NSDate date] timeIntervalSince1970]/1000;
}

+ (NSString *)doubleToString:(double)price
{
    return [[NSNumber numberWithDouble:price] stringValue];
}

+ (NSString *)uint32ToString:(uint32)id_p
{
    return [[NSNumber numberWithUnsignedInteger:id_p] stringValue];
}

+ (NSString *)timeIntervalToString:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:
                          [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    return [formatter stringFromDate:date];
}

+ (NSString *)quoteTimeToString:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:
                          [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [formatter setDateFormat:@"MM/dd HH:mm:ss"];
    return [formatter stringFromDate:date];
}

+ (NSString *)timeIntervalToString:(NSTimeInterval)timeInterval formate:(NSString *)formate
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:
                          [IXDateUtils convertTimeZoneString:[IXDateUtils getUserSaveTimeZone]]];
    [formatter setDateFormat:formate];
    return [formatter stringFromDate:date];
}

+ (NSInteger)getCellStyle
{
    id type =  [[NSUserDefaults standardUserDefaults] objectForKey:kSaveSubSymbolCellStyle];
    if( !type || [type integerValue] == 0 )
        return 0;
    return 1;
}


//在目标target上添加关联对象，属性名propertyname(也能用来添加block)，值value
+ (void)addAssociatedWithtarget:(id)target withPropertyName:(NSString *)propertyName withValue:(id)value {
    id property = objc_getAssociatedObject(target, &propertyName);
    
    if(property == nil)
    {
        property = value;
        objc_setAssociatedObject(target, &propertyName, property, OBJC_ASSOCIATION_RETAIN);
    }
}

//获取目标target的指定关联对象值
+ (id)getAssociatedValueWithTarget:(id)target withPropertyName:(NSString *)propertyName {
    id property = objc_getAssociatedObject(target, &propertyName);
    return property;
}

+ (BOOL)isPureInt:(NSString *)str
{
    NSScanner *scan = [NSScanner scannerWithString:str];
    int val;
    return  [scan scanInt:&val] && [scan isAtEnd];
}

+ (BOOL)isPureFloat:(NSString *)str
{
    NSScanner *scan = [NSScanner scannerWithString:str];
    float val;
    return  [scan scanFloat:&val] && [scan isAtEnd];
}
@end
