//
//  IXCataCell.m
//  IXApp
//
//  Created by Bob on 2016/12/17.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXCataCell.h"

@interface IXCataCell ()

@property (nonatomic, strong) UIButton *sectionFirstBtn;

@property (nonatomic, strong) UIButton *sectionSecondBtn;

@property (nonatomic, strong) UIButton *sectionThirdBtn;

@property (nonatomic, strong) UIButton *sectionForthBtn;

@end

@implementation IXCataCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        CGFloat orginX = 15;
        CGFloat width = [self getCellWidth];
        CGRect frame = CGRectMake( orginX, 5, width, 20);
        _sectionFirstBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sectionFirstBtn.frame = frame;
        [_sectionFirstBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_unChoose"]
                                    forState:UIControlStateNormal];
        [_sectionFirstBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_choose"]
                                    forState:UIControlStateSelected];
        
        [self.contentView addSubview:_sectionFirstBtn];
        [_sectionFirstBtn addTarget:self
                             action:@selector(btnAction:)
                   forControlEvents:UIControlEventTouchUpInside];
        _sectionFirstBtn.tag = 0;
        
        frame.origin.x += (width + 10);
        _sectionSecondBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sectionSecondBtn.frame = frame;
        [_sectionSecondBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_unChoose"]
                                     forState:UIControlStateNormal];
        [_sectionSecondBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_choose"]
                                     forState:UIControlStateSelected];
        [self.contentView addSubview:_sectionSecondBtn];
        [_sectionSecondBtn addTarget:self
                              action:@selector(btnAction:)
                    forControlEvents:UIControlEventTouchUpInside];
        _sectionSecondBtn.tag = 1;
        [_sectionSecondBtn.titleLabel setFont:PF_MEDI(12)];

        frame.origin.x += (width + 10);
        _sectionThirdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sectionThirdBtn.frame = frame;
        [_sectionThirdBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_unChoose"]
                                    forState:UIControlStateNormal];
        [_sectionThirdBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_choose"]
                                    forState:UIControlStateSelected];
        [self.contentView addSubview:_sectionThirdBtn];
        [_sectionThirdBtn addTarget:self
                             action:@selector(btnAction:)
                   forControlEvents:UIControlEventTouchUpInside];
        _sectionThirdBtn.tag = 2;
        [_sectionThirdBtn.titleLabel setFont:PF_MEDI(12)];

        frame.origin.x += (width + 10);
        _sectionForthBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sectionForthBtn.frame = frame;
        [_sectionForthBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_unChoose"]
                                    forState:UIControlStateNormal];
        [_sectionForthBtn setBackgroundImage:[UIImage imageNamed:@"cataCell_choose"]
                                    forState:UIControlStateSelected];
        [self.contentView addSubview:_sectionForthBtn];
        [_sectionForthBtn addTarget:self
                             action:@selector(btnAction:)
                   forControlEvents:UIControlEventTouchUpInside];
        _sectionForthBtn.tag = 3;
        [_sectionForthBtn.titleLabel setFont:PF_MEDI(12)];
    }
    return self;
}

- (float)getCellWidth
{
    return (kScreenWidth - 80)/4.0;
}

- (void)setNameArr:(NSArray *)arr
{
    NSArray *btnArr = @[_sectionFirstBtn,_sectionSecondBtn,_sectionThirdBtn,_sectionForthBtn];
    if ( arr && [arr isKindOfClass:[NSArray class]] ) {
        for ( NSInteger i = 0; i < btnArr.count; i++ ) {
            if ( ([arr count] < i + 1) ) {
                [(UIButton *)btnArr[i] setHidden:YES];
            }else{
                id value = arr[i];
                if ( [value isKindOfClass:[NSString class]] ) {
                    [(UIButton *)btnArr[i]  setTitle:arr[i] forState:UIControlStateNormal];
                }else if ( [value isKindOfClass:[NSDictionary class]] ){
                    [(UIButton *)btnArr[i]  setTitle:arr[i][@"name"] forState:UIControlStateNormal];
                }
                [[(UIButton *)btnArr[i]  titleLabel] setFont:PF_MEDI(12)];
            }
        }
    }
}

- (void)setSelectedBtnWithTag:(NSInteger)tag
{
    [_sectionFirstBtn setSelected:(_sectionFirstBtn.tag + self.tag * 4 == tag)];
    [_sectionSecondBtn setSelected:(_sectionSecondBtn.tag + self.tag * 4 == tag)];
    [_sectionThirdBtn setSelected:(_sectionThirdBtn.tag + self.tag * 4 == tag)];
    [_sectionForthBtn setSelected:(_sectionForthBtn.tag + self.tag * 4 == tag)];
}

- (void)btnAction:(UIButton *)btn
{
    [btn setSelected:![btn isSelected]];
    __block long tag = self.tag * 4;
    if ( self.selectedCataInfo ) {
        self.selectedCataInfo(tag + btn.tag);
    }
}

@end
