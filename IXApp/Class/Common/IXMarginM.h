//
//  IXMarginM.h
//  IXApp
//
//  Created by Bob on 2017/12/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXSymbolM.h"

@interface IXMarginM : NSObject

@property (nonatomic, strong) NSDecimalNumber *vol;

@property (nonatomic, strong) NSDecimalNumber *margin;

@property (nonatomic, strong) IXSymbolM *model;

@property (nonatomic, assign) item_position_edirection posDir;

@end
