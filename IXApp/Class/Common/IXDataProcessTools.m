//
//  IXDataProcessTools.m
//  IXApp
//
//  Created by Evn on 16/11/30.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataProcessTools.h"
#import "IXDBGlobal.h"
#import "IXDataBase.h"
#import "IxItemSymbol.pbobjc.h"
#import "IxItemGroupSymbol.pbobjc.h"
#import "IXDateUtils.h"
#import<CommonCrypto/CommonDigest.h>
#import "IXQuoteM.h"
#import "IXAccSymCataM.h"
#import "IXDBManager.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"
#import "IXAccountGroupModel.h"

#define TINY_DOUBLE_VAL 0.00000001
#define ISZero(x) (x > -TINY_DOUBLE_VAL && x < TINY_DOUBLE_VAL)

typedef NS_ENUM(NSUInteger,IXGroupMarginType) {
    IXGroupMarginTypeNormal,
    IXGroupMarginTypeWeekend,
    IXGroupMarginTypeHoliday
};

@implementation IXDataProcessTools

#warning 临时改为displayName查询
#pragma mark  通过产品分类ID查询产品
+ (NSMutableArray *)querySymbolBySymbolCataId:(uint64_t)cataId
                                   startIndex:(uint64_t)sIndex
                                       offset:(uint64_t)offset
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    NSDictionary *gSymCataDic = [IXDBGroupSymCataMgr queryGroupSymbolCataBySymbolCataId:cataId accountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    NSArray *symArr;
    
    if (gSymCataDic.count) {
        
        symArr = [IXDBSymbolMgr querySymbolsBySymbolCataId:cataId startIndex:sIndex offset:offset accountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    } else {
        
        symArr = [IXDBSymbolMgr querySymbolsBySymbolCataId:cataId startIndex:sIndex offset:offset];
    }
    
    //        NSArray *symArr = [IXDBSymbolMgr querySymbolsBySymbolCataId:cataId startIndex:sIndex offset:offset];
    for (int i = 0; i < symArr.count; i++) {
        IXSymbolM *model = [[IXSymbolM alloc] init];
        model = [IXDataProcessTools symbolModelBySymbolInfo:symArr[i]];
        if (model) {
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:model.id_p];
            if (labelArr && labelArr.count > 0) {
                model.labelArr = labelArr;
            }
            if (languageDic && languageDic.count > 0) {
                if (languageDic && languageDic.count > 0) {
                    if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                        model.languageName = languageDic[kValue];
                    } else {
                        model.languageName = model.name;
                    }
                } else {
                    model.languageName = model.name;
                }
            } else {
                model.languageName = model.name;
            }
            NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
            if (unitLanDic && unitLanDic.count > 0) {
                if (unitLanDic && unitLanDic.count > 0) {
                    if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                        model.unitLanName = unitLanDic[kValue];
                    } else {
                        model.unitLanName = model.displayName;
                    }
                } else {
                    model.unitLanName = model.displayName;
                }
            } else {
                model.unitLanName = model.displayName;
            }
            [retArr addObject:model];
        }
    }
    return retArr;
}

#pragma mark  通过产品分类ID查询产品(键值对)
+ (NSMutableArray *)querySymbolBySymbolNewCataId:(uint64_t)cataId
                                      startIndex:(uint64_t)sIndex
                                          offset:(uint64_t)offset
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    NSDictionary *gSymCataDic = [IXDBGroupSymCataMgr queryGroupSymbolCataBySymbolCataId:cataId accountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    NSArray *symArr;
    
    if (gSymCataDic.count) {
        
        symArr = [IXDBSymbolMgr querySymbolsBySymbolCataId:cataId startIndex:sIndex offset:offset accountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    } else {
        
        symArr = [IXDBSymbolMgr querySymbolsBySymbolCataId:cataId startIndex:sIndex offset:offset];
    }
    for (int i = 0; i < symArr.count; i++) {
        NSDictionary *dic = symArr[i];
        if (dic.count) {
            NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
            [tmpDic setObject:dic[kID] forKey:kID];
            [tmpDic setObject:dic[kDigits] forKey:kDigits];
            [tmpDic setObject:dic[kPipsRatio] forKey:kPipsRatio];
            [tmpDic setObject:dic[kName] forKey:kName];
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:dic[kName]];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:[dic[kID] longLongValue]];
            if (labelArr && labelArr.count > 0) {
                [tmpDic setObject:labelArr forKey:kLabel];
            }
            NSString *lanName = @"";
            if (languageDic && languageDic.count > 0) {
                if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                    lanName = languageDic[kValue];
                } else {
                    lanName = dic[kName];
                }
            } else {
                lanName = dic[kName];
            }
            [tmpDic setObject:lanName forKey:kLanguageName];
            [retArr addObject:tmpDic];
        }
    }
    return retArr;
}

#pragma mark  通过父类ID查询产品分类
+ (NSArray *)querySymbolCataByParentId:(uint64_t)parentId {
    
    NSArray *agscCataArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolCataIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *agscSymArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    if ((agscCataArr && agscCataArr.count > 0) || (agscSymArr && agscSymArr.count > 0)) {
        return [IXDataProcessTools dealWithSymbolCataLanguage:[IXDBSymbolCataMgr querySymbolCataByParentId:parentId accountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid]];
    } else {
        return [IXDataProcessTools dealWithSymbolCataLanguage:[IXDBSymbolCataMgr querySymbolCataByParentId:parentId]];
    }
}

#pragma mark  比较产品分类是否属于另一分类或者两者属于相同分类
+ (BOOL)compareCataId0:(int64_t)cataId0
               cataId1:(int64_t)cataId1
{
    BOOL ret = NO;
    if (cataId0 == 0 || cataId1 == 0) {
        return ret;
    }
    if (cataId0 == cataId1) {
        ret = YES;
    } else {
        NSDictionary *cataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:cataId0];
        if (cataDic.count) {
            int64_t parentId = [cataDic[kparentId] longLongValue];
            if (parentId == cataId1) {
                ret = YES;
            }
        }
    }
    return ret;
}

#pragma mark 处理产品分类多语言
+ (NSArray *)dealWithSymbolCataLanguage:(NSArray *)cataArr
{
    if (!cataArr || cataArr.count == 0) {
        return cataArr;
    }
    NSMutableArray *retArr = [NSMutableArray array];
    for (int i = 0; i < cataArr.count; i++) {
        NSMutableDictionary *dic = [cataArr[i] mutableCopy];
        NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:dic[kName]];
        if (languageDic && languageDic.count > 0) {
            if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                [dic setObject:languageDic[kValue] forKey:kName];
            }
        }
        [retArr addObject:dic];
    }
    return retArr;
}

#pragma mark  模糊查询产品信息
+ (NSArray *)fuzzyQuerySymbolByContent:(NSString *)search cataId:(uint64_t)cataId
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    
    NSArray *symArr;
    NSArray *agscSymArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *agscCataArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolCataIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    if ((agscSymArr && agscSymArr.count > 0) || (agscCataArr && agscCataArr.count > 0)) {
        
        symArr = [IXDBSymbolMgr fuzzyQuerySymbolSymInfoByContent:search cataId:cataId accountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    } else {
        
        symArr = [IXDBSymbolMgr fuzzyQuerySymbolByContent:search cataId:cataId];
    }
    for (int i = 0; i < symArr.count; i++) {
        IXSymbolM *model = [[IXSymbolM alloc] init];
        model = [IXDataProcessTools symbolModelBySymbolInfo:symArr[i]];
        if (model) {
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *subDic = [[NSDictionary alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:model.id_p];
            subDic = [IXDBSymbolSubMgr querySymbolSubBySymbolId:model.id_p accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
            if (subDic && subDic.count > 0) {
                model.isSub = YES;
            } else {
                model.isSub = NO;
            }
            if (labelArr && labelArr.count > 0) {
                model.labelArr = labelArr;
            }
            if (languageDic && languageDic.count > 0) {
                if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                    model.languageName = languageDic[kValue];
                } else {
                    model.languageName = model.name;
                }
            } else {
                model.languageName = model.name;
            }
            NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
            if (unitLanDic && unitLanDic.count > 0) {
                if (unitLanDic && unitLanDic.count > 0) {
                    if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                        model.unitLanName = unitLanDic[kValue];
                    } else {
                        model.unitLanName = model.displayName;
                    }
                } else {
                    model.unitLanName = model.displayName;
                }
            } else {
                model.unitLanName = model.displayName;
            }
            [retArr addObject:model];
        }
    }
    return retArr;
}

#pragma mark  分页查询热门产品信息
+ (NSArray *)querySymbolHotStartIndex:(uint64_t)sIndex
                               offset:(uint64_t)offset
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    NSArray *symArr = [IXDBSymbolHotMgr querySymbolHotStartIndex:sIndex offset:offset];
    for (int i = 0; i < symArr.count; i++) {
        NSDictionary *symDic = symArr[i];
        IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
        tmpModel = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
        if (tmpModel) {
            [retArr addObject:tmpModel];
        }
    }
    return retArr;
}

#pragma mark  查询热门产品信息
+ (NSArray *)queryAllSymbolsHot
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    
    NSArray *agscSymArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *agscCataArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolCataIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *symArr;
    if ((agscSymArr && agscSymArr.count > 0) || (agscCataArr && agscCataArr.count > 0)) {
        
        symArr = [IXDBSymbolHotMgr queryAllSymbolsHotSymInfoAccountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    } else {
        
        symArr = [IXDBSymbolHotMgr queryAllSymbolsHot];
    }
    
    for (int i = 0; i < symArr.count; i++) {
        IXSymbolM *model = [[IXSymbolM alloc] init];
        model = [IXDataProcessTools symbolModelBySymbolInfo:symArr[i]];
        if (model) {
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:model.id_p];
            if (labelArr && labelArr.count > 0) {
                model.labelArr = labelArr;
            }
            if (languageDic && languageDic.count > 0) {
                if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                    model.languageName = languageDic[kValue];
                } else {
                    model.languageName = model.name;
                }
            } else {
                model.languageName = model.name;
            }
            NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
            if (unitLanDic && unitLanDic.count > 0) {
                if (unitLanDic && unitLanDic.count > 0) {
                    if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                        model.unitLanName = unitLanDic[kValue];
                    } else {
                        model.unitLanName = model.displayName;
                    }
                } else {
                    model.unitLanName = model.displayName;
                }
            } else {
                model.unitLanName = model.displayName;
            }
            [retArr addObject:model];
        }
    }
    return retArr;
}

#pragma mark  查询热门产品信息(键值对)
+ (NSArray *)queryAllSymbolsHotNew
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    
    NSArray *agscSymArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *agscCataArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolCataIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *symArr;
    if ((agscSymArr && agscSymArr.count > 0) || (agscCataArr && agscCataArr.count > 0)) {
        
        symArr = [IXDBSymbolHotMgr queryAllSymbolsHotSymInfoAccountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    } else {
        
        symArr = [IXDBSymbolHotMgr queryAllSymbolsHot];
    }
    
    for (int i = 0; i < symArr.count; i++) {
        NSDictionary *dic = symArr[i];
        if (dic.count) {
            NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
            [tmpDic setObject:dic[kID] forKey:kID];
            [tmpDic setObject:dic[kDigits] forKey:kDigits];
            [tmpDic setObject:dic[kPipsRatio] forKey:kPipsRatio];
            [tmpDic setObject:dic[kName] forKey:kName];
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:dic[kName]];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:[dic[kID] longLongValue]];
            if (labelArr && labelArr.count > 0) {
                [tmpDic setObject:labelArr forKey:kLabel];
            }
            NSString *lanName = @"";
            if (languageDic && languageDic.count > 0) {
                if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                    lanName = languageDic[kValue];
                } else {
                    lanName = dic[kName];
                }
            } else {
                lanName = dic[kName];
            }
            [tmpDic setObject:lanName forKey:kLanguageName];
            [retArr addObject:tmpDic];
        }
    }
    return retArr;
}

#pragma mark  分页查询自选产品信息
+ (NSArray *)querySymbolSubStartIndex:(uint64_t)sIndex
                               offset:(uint64_t)offset
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    NSArray *symArr = [IXDBSymbolSubMgr querySymbolSubStartIndex:sIndex offset:offset accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    for (int i = 0; i < symArr.count; i++) {
        NSDictionary *symDic = symArr[i];
        IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
        tmpModel = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
        if (tmpModel) {
            [retArr addObject:tmpModel];
        }
    }
    return retArr;
}

#pragma mark  查询所有自选产品信息
+ (NSArray *)queryAllSymbolsSub
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    
    NSArray *symArr;
    NSArray *agscSymArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *agscCataArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolCataIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    if ((agscSymArr && agscSymArr.count > 0) || (agscCataArr && agscCataArr.count > 0)) {
        
        symArr = [IXDBSymbolSubMgr queryAllSymbolsSymInfoSubAccountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p AccountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    } else {
        symArr = [IXDBSymbolSubMgr queryAllSymbolsSubAccountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    }
    for (int i = 0; i < symArr.count; i++) {
        IXSymbolM *model = [[IXSymbolM alloc] init];
        model = [IXDataProcessTools symbolModelBySymbolInfo:symArr[i]];
        if (model) {
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:model.id_p];
            if (labelArr && labelArr.count > 0) {
                model.labelArr = labelArr;
            }
            if (languageDic && languageDic.count > 0) {
                if (languageDic && languageDic.count > 0) {
                    if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                        model.languageName = languageDic[kValue];
                    } else {
                        model.languageName = model.name;
                    }
                } else {
                    model.languageName = model.name;
                }
            } else {
                model.languageName = model.name;
            }
            NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
            if (unitLanDic && unitLanDic.count > 0) {
                if (unitLanDic && unitLanDic.count > 0) {
                    if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                        model.unitLanName = unitLanDic[kValue];
                    } else {
                        model.unitLanName = model.displayName;
                    }
                } else {
                    model.unitLanName = model.displayName;
                }
            } else {
                model.unitLanName = model.displayName;
            }
            [retArr addObject:model];
        }
    }
    return retArr;
}

#pragma mark  查询所有自选产品信息(键值对)
+ (NSArray *)queryAllSymbolsSubNew
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    
    NSArray *symArr;
    NSArray *agscSymArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *agscCataArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolCataIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    if ((agscSymArr && agscSymArr.count > 0) || (agscCataArr && agscCataArr.count > 0)) {
        
        symArr = [IXDBSymbolSubMgr queryAllSymbolsSymInfoSubAccountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p AccountGroupid:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    } else {
        symArr = [IXDBSymbolSubMgr queryAllSymbolsSubAccountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    }
    for (int i = 0; i < symArr.count; i++) {
        NSDictionary *dic = symArr[i];
        if (dic.count) {
            NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
            [tmpDic setObject:dic[kID] forKey:kID];
            [tmpDic setObject:dic[kDigits] forKey:kDigits];
            [tmpDic setObject:dic[kPipsRatio] forKey:kPipsRatio];
            [tmpDic setObject:dic[kName] forKey:kName];
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:dic[kName]];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:[dic[kID] longLongValue]];
            if (labelArr && labelArr.count > 0) {
                [tmpDic setObject:labelArr forKey:kLabel];
            }
            NSString *lanName = @"";
            if (languageDic && languageDic.count > 0) {
                if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                    lanName = languageDic[kValue];
                } else {
                    lanName = dic[kName];
                }
            } else {
                lanName = dic[kName];
            }
            [tmpDic setObject:lanName forKey:kLanguageName];
            [retArr addObject:tmpDic];
        }
    }
    return retArr;
}

#pragma mark  通过产品ID查询产品信息
+ (IXSymbolM *)querySymbolBySymbolId:(uint64)symbolId
{
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
    if (symDic && symDic.count > 0) {
        IXSymbolM *model = [IXDataProcessTools symbolModelBySymbolInfo:symDic];
        if (model) {
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:model.id_p];
            if (labelArr && labelArr.count > 0) {
                model.labelArr = labelArr;
            }
            if (languageDic && languageDic.count > 0) {
                if (languageDic && languageDic.count > 0) {
                    if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                        model.languageName = languageDic[kValue];
                    } else {
                        model.languageName = model.name;
                    }
                } else {
                    model.languageName = model.name;
                }
            } else {
                
                model.languageName = model.name;
            }
            NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
            if (unitLanDic && unitLanDic.count > 0) {
                if (unitLanDic && unitLanDic.count > 0) {
                    if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                        model.unitLanName = unitLanDic[kValue];
                    } else {
                        model.unitLanName = model.displayName;
                    }
                } else {
                    model.unitLanName = model.displayName;
                }
            } else {
                model.unitLanName = model.displayName;
            }
        }
        return model;
    } else {
        
        return nil;
    }
}

+ (NSInteger)queryParentIdBySymbolId:(uint64)symbolId
{
    NSInteger index = -1;
    IXSymbolM *model = [IXDataProcessTools querySymbolBySymbolId:symbolId];
    if (model) {
        NSDictionary *cataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:model.cataId];
        if (cataDic && cataDic[kparentId]) {
            index = [cataDic[kparentId] integerValue];
        }
    }
    return index;
}

+ (NSArray *)querySymbolsSubBySymbolCataId:(uint64_t)symbolCataId
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    NSArray *symArr = [IXDBSymbolSubMgr querySymbolsSubAccountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p symbolCataId:symbolCataId];
    for (int i = 0; i < symArr.count; i++) {
        IXSymbolM *model = [[IXSymbolM alloc] init];
        model = [IXDataProcessTools symbolModelBySymbolInfo:symArr[i]];
        if (model) {
            NSArray *labelArr = [[NSArray alloc] init];
            NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
            labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:model.id_p];
            if (labelArr && labelArr.count > 0) {
                model.labelArr = labelArr;
            }
            if (languageDic && languageDic.count > 0) {
                if (languageDic && languageDic.count > 0) {
                    if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                        model.languageName = languageDic[kValue];
                    } else {
                        model.languageName = model.name;
                    }
                } else {
                    model.languageName = model.name;
                }
            } else {
                
                model.languageName = model.name;
            }
            NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
            if (unitLanDic && unitLanDic.count > 0) {
                if (unitLanDic && unitLanDic.count > 0) {
                    if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                        model.unitLanName = unitLanDic[kValue];
                    } else {
                        model.unitLanName = model.displayName;
                    }
                } else {
                    model.unitLanName = model.displayName;
                }
            } else {
                model.unitLanName = model.displayName;
            }
            [retArr addObject:model];
        }
    }
    return retArr;
}

#pragma mark 当前时间是否可交易

+ (IXSymbolTradeState)symbolIsCanTrade:(IXSymbolM *)symModel
{
    item_holiday *holiday = [IXDataProcessTools queryIsHolidayBySymbol:symModel tradable:NO];
    if (holiday) {
        return IXSymbolTradeStateHoliday;
    }
    return [IXDataProcessTools queryIsScheduleBySymbol:symModel];
}

#pragma mark  查询当前时间是否是假期
+ (IXSymbolTradeState)symbolIsCanTradeNew:(uint64_t)symbolId
{
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
    IXSymbolM *symModel = [IXDataProcessTools symbolModelBySymbolInfo:symDic];
    item_holiday *holiday = [IXDataProcessTools queryIsHolidayBySymbol:symModel tradable:NO];
    if (holiday) {
        return IXSymbolTradeStateHoliday;
    }
    return [IXDataProcessTools queryIsScheduleBySymbol:symModel];
}

+ (IXSymbolTradeState)symbolModelIsCanTrade:(IXSymModel *)symModel
{
    item_holiday *holiday = [IXDataProcessTools queryIsHolidayBySymbolModel:symModel tradable:NO];
    if (holiday) {
        return IXSymbolTradeStateHoliday;
    }
    return [IXDataProcessTools queryIsScheduleBySymbolModel:symModel];
}

#pragma mark  产品是否可交易
+ (BOOL)canTradeByState:(IXSymbolTradeState)state
               orderDir:(item_order_edirection)dir
{
    BOOL tradeState = NO;
    NSDictionary *accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
    int32_t options = [accGroupDic[kOptions] intValue];
    //当期账户是否可以交易
    if (options == 0) {
        tradeState = YES;
    } else {
        tradeState = NO;
    }
    if (!tradeState) {
        return NO;
    }
    BOOL enable = NO;
    if (state == IXSymbolTradeStateNormal) {
        enable = YES;
    } else if (state == IXSymbolTradeStateBuyNormal) {
        if (dir == item_order_edirection_DirectionBuy) {
            enable = YES;
        }
    } else if (state == IXSymbolTradeStateSellNormal) {
        if (dir == item_order_edirection_DirectionSell) {
            enable = YES;
        }
    }
    return enable;
}


#pragma mark  产品是否可交易
+ (BOOL)isNormalByState:(IXSymbolTradeState)state
{
    BOOL enable = NO;
    if (state == IXSymbolTradeStateNormal ||
        state == IXSymbolTradeStateBuyNormal ||
        state == IXSymbolTradeStateSellNormal) {
        enable = YES;
    }
    return enable;
}


#pragma mark  产品是否可交易(关联账户是否可交易)
+ (BOOL)isNormalAddOptiosByState:(IXSymbolTradeState)state
{
    BOOL tradeState = NO;
    NSDictionary *accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
    int32_t options = [accGroupDic[kOptions] intValue];
    //当期账户是否可以交易
    if (options == 0) {
        tradeState = YES;
    } else {
        tradeState = NO;
    }
    if (!tradeState) {
        return NO;
    }
    BOOL enable = NO;
    if (state == IXSymbolTradeStateNormal ||
        state == IXSymbolTradeStateBuyNormal ||
        state == IXSymbolTradeStateSellNormal) {
        enable = YES;
    }
    return enable;
}

#pragma mark  查询当前时间是否是假期

+ (item_holiday *)queryIsHolidayBySymbol:(IXSymbolM *)symModel tradable:(BOOL)tradeFlag
{
    item_holiday *holiday = [[item_holiday alloc] init];
    BOOL _enable,_flag,_tradable;
    int _status;
    int32_t _fromTime,_toTime,_totalTime;
    NSDictionary *holidayCataDic = [IXDBHolidayCataMgr queryHolidayCataByHolidayCataId:symModel.holidayCataId];
    if (!holidayCataDic) {
        return nil;
    }
    NSArray *holidayArr = [IXDBHolidayMgr queryHolidayByHolidayCataId:symModel.holidayCataId];
    if (!holidayArr || holidayArr.count == 0) {
        return nil;
    } else {
        _flag = NO;
        for (int i = 0; i < holidayArr.count; i++) {
            NSDictionary *tmpDic = [[NSDictionary alloc] init];
            tmpDic = holidayArr[i];
            _enable = [tmpDic[kEnable] boolValue];
            _status = [tmpDic[kStatus] intValue];
            _fromTime = [tmpDic[kFromTime] intValue];
            _toTime = [tmpDic[kToTime] intValue];
            _tradable = [tmpDic[KTradable] boolValue];
            if (!_enable || _status != 0 || (_tradable && !tradeFlag)) {
                
                continue;
            } else {
                NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
                _totalTime = [timeStamp doubleValue];
                if (_totalTime >= _fromTime && _totalTime <= _toTime) {
                    
                    holiday.id_p = [tmpDic[kID] doubleValue];
                    _flag = YES;
                    break;
                }
            }
        }
    }
    return !_flag ? nil : holiday;
}

+ (item_holiday *)queryIsHolidayBySymbolModel:(IXSymModel *)symModel tradable:(BOOL)tradeFlag
{
    item_holiday *holiday = [[item_holiday alloc] init];
    BOOL _enable,_flag,_tradable;
    int _status;
    int32_t _fromTime,_toTime,_totalTime;
    NSDictionary *holidayCataDic = [IXDBHolidayCataMgr queryHolidayCataByHolidayCataId:symModel.holidayCataId];
    if (!holidayCataDic) {
        return nil;
    }
    NSArray *holidayArr = [IXDBHolidayMgr queryHolidayByHolidayCataId:symModel.holidayCataId];
    if (!holidayArr || holidayArr.count == 0) {
        return nil;
    } else {
        _flag = NO;
        for (int i = 0; i < holidayArr.count; i++) {
            NSDictionary *tmpDic = [[NSDictionary alloc] init];
            tmpDic = holidayArr[i];
            _enable = [tmpDic[kEnable] boolValue];
            _status = [tmpDic[kStatus] intValue];
            _fromTime = [tmpDic[kFromTime] intValue];
            _toTime = [tmpDic[kToTime] intValue];
            _tradable = [tmpDic[KTradable] boolValue];
            if (!_enable || _status != 0 || (_tradable && !tradeFlag)) {
                
                continue;
            } else {
                NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
                _totalTime = [timeStamp doubleValue];
                if (_totalTime >= _fromTime && _totalTime <= _toTime) {
                    
                    holiday.id_p = [tmpDic[kID] doubleValue];
                    _flag = YES;
                    break;
                }
            }
        }
    }
    return !_flag ? nil : holiday;
}

#pragma mark 查询当前时间是否是交易时间
+ (IXSymbolTradeState)queryIsScheduleBySymbol:(IXSymbolM *)symModel
{
    item_schedule *schedule;
    //星期天排第0个
    NSDateComponents *result = [IXDateUtils currentTimeComponents];
    NSInteger weekday = (result.weekday + 7)%8;
    NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSTimeInterval currentTime = [timeStamp doubleValue];
    //查询scheduleCata是否有效
    NSDictionary *scheduleCataDic = [IXDBScheduleCataMgr queryScheduleCataByScheduleCataId:symModel.scheduleCataId];
    if (!scheduleCataDic) {
        return IXSymbolTradeStateNoSchedule;
    }
    NSArray *scheduleArr = [IXDBScheduleMgr queryScheduleByScheduleCataId:symModel.scheduleCataId];
    if (symModel.startTime > 0) {
        
        if (symModel.startTime > currentTime) {
            
            return IXSymbolTradeStateDisEnable;
        }
        
    }
    if (symModel.expiryTime > 0) {
        
        if (symModel.expiryTime < currentTime) {
            
            return IXSymbolTradeStateExpiry;
        }
    }
    if ( !scheduleArr || scheduleArr.count == 0) {
        return IXSymbolTradeStateNoSchedule;
    }
    IXSymbolTradeState state =  [IXDataProcessTools getTradeStateByTradable:symModel.tradable symTradale:symModel.symTradable isSetGroupSymbol:symModel.isSetGroupSym closeOnly:symModel.closeOnly];
    if (state == IXSymbolTradeStateNoSchedule) {
        return IXSymbolTradeStateNoSchedule;
    }
    //冬令时延迟交易时间(优先查询symbol表、然后再找symbolCata表)
    int scheduleDelayMinutes = symModel.scheduleDelayMinutes;
    if (scheduleDelayMinutes == 0) {
        NSDictionary *symCataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:symModel.cataId];
        if (symCataDic) {
            scheduleDelayMinutes = [symCataDic[kScheduleDelayMinutes] integerValue];
        }
    }
    //拆分跨前一天和后一天
    scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr];
    if (scheduleDelayMinutes) {
        scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr withScheduleDelayMinutes:scheduleDelayMinutes];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dayOfWeek == %@",[NSString stringWithFormat:@"%ld",(long)weekday]];
    NSArray *filterScheduleArr = [scheduleArr filteredArrayUsingPredicate:predicate];
    for ( NSDictionary *scheduleDic in filterScheduleArr ) {
        if ( ![scheduleDic[kEnable] boolValue] ||
            0 != [scheduleDic[kStatus] integerValue] || [scheduleDic[KTradable] boolValue]) {
            continue;
        }
        
        //先判断是否在区间，再判断是否跨天
        NSInteger minute = result.minute + result.hour * 60;
        if( minute >= ([scheduleDic[kStartTime] integerValue]) &&
           minute <= ([scheduleDic[kEndTime] integerValue]) ){
            schedule = [[item_schedule alloc] init];
            schedule.id_p = [scheduleDic[kID] doubleValue];
            break;
        }else if ( ([scheduleDic[kEndTime] integerValue] ) <= minute ) {
            if( ([scheduleDic[kStartTime] integerValue] ) < 0 ){
                NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"dayOfWeek == %@",
                                           [NSString stringWithFormat:@"%ld",(long)(weekday+1)]];
                NSArray *filter2ScheduleArr = [scheduleArr filteredArrayUsingPredicate:predicate2];
                for ( NSDictionary *predicate2 in filter2ScheduleArr ) {
                    if ( ![scheduleDic[kEnable] boolValue] ||
                        0 != [scheduleDic[kStatus] integerValue] ) {
                        continue;
                    }else{
                        
                        minute = minute - 1440;;
                        if ( ([predicate2[kStartTime] integerValue] ) <= minute ) {
                            schedule = [[item_schedule alloc] init];
                            schedule.id_p = [scheduleDic[kID] doubleValue];
                            break;
                        }
                    }
                    
                }
            }
        } else {
            //延时跨天
            if (([scheduleDic[kEndTime] integerValue] ) - 1440 >= minute) {
                schedule = [[item_schedule alloc] init];
                schedule.id_p = [scheduleDic[kID] doubleValue];
                break;
            }
        }
    }
    if (!schedule) {
        return IXSymbolTradeStateNoSchedule;
    }
    return [IXDataProcessTools getTradeStateByTradable:symModel.tradable symTradale:symModel.symTradable isSetGroupSymbol:symModel.isSetGroupSym closeOnly:symModel.closeOnly];
}

+ (IXSymbolTradeState)getTradeStateByTradable:(uint32_t)tradable
                                   symTradale:(uint32_t)symTradable
                             isSetGroupSymbol:(BOOL)isSetGroupSym
                                    closeOnly:(BOOL)closeOnly
{
    IXSymbolTradeState state = IXSymbolTradeStateNoSchedule;
    switch (tradable) {
        case 0:{
            state = IXSymbolTradeStateNoSchedule;
        }
            break;
        case 1:{
            if (isSetGroupSym) {
                //新增仅平仓逻辑、优先级最高
                if (closeOnly) {
                    if (symTradable == 0) {
                        state = IXSymbolTradeStateNoSchedule;
                    } else {
                        state = IXSymbolTradeStateCloseOnly;
                    }
                } else {
                    switch (symTradable) {
                        case 0:{
                            state = IXSymbolTradeStateNoSchedule;
                        }
                            break;
                        case 1:{
                            state = IXSymbolTradeStateNormal;
                        }
                            break;
                        case 2:{
                            state = IXSymbolTradeStateBuyNormal;
                        }
                            break;
                        case 3:{
                            state = IXSymbolTradeStateSellNormal;
                        }
                            break;
                        default:
                            break;
                    }
                }
            } else {
                state = IXSymbolTradeStateNormal;
            }
            
        }
            break;
        case 2:{
            if (isSetGroupSym) {
                if (closeOnly) {
                    if (symTradable == 0) {
                        state = IXSymbolTradeStateNoSchedule;
                    } else {
                        state = IXSymbolTradeStateCloseOnly;
                    }
                } else {
                    switch (symTradable) {
                        case 0:{
                            state = IXSymbolTradeStateNoSchedule;
                        }
                            break;
                        case 1:{
                            state = IXSymbolTradeStateBuyNormal;
                        }
                            break;
                        case 2:{
                            state = IXSymbolTradeStateBuyNormal;
                        }
                            break;
                        case 3:{
                            state = IXSymbolTradeStateNoSchedule;
                        }
                            break;
                        default:
                            break;
                    }
                }
            } else {
                state = IXSymbolTradeStateBuyNormal;
            }
            
        }
            break;
        case 3:{
            if (isSetGroupSym) {
                if (closeOnly) {
                    if (symTradable == 0) {
                        state = IXSymbolTradeStateNoSchedule;
                    } else {
                        state = IXSymbolTradeStateCloseOnly;
                    }
                } else {
                    switch (symTradable) {
                        case 0:{
                            state = IXSymbolTradeStateNoSchedule;
                        }
                            break;
                        case 1:{
                            state = IXSymbolTradeStateSellNormal;
                        }
                            break;
                        case 2:{
                            state = IXSymbolTradeStateNoSchedule;
                        }
                            break;
                        case 3:{
                            state = IXSymbolTradeStateSellNormal;
                        }
                            break;
                        default:
                            break;
                    }
                }
            } else {
                state = IXSymbolTradeStateSellNormal;
            }
        }
            break;
        default:
            break;
    }
    return state;
}

+ (IXSymbolTradeState)queryIsScheduleBySymbolModel:(IXSymModel *)symModel {
    
    item_schedule *schedule;
    //星期天排第0个
    NSDateComponents *result = [IXDateUtils currentTimeComponents];
    NSInteger weekday = (result.weekday + 7)%8;
    NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSTimeInterval currentTime = [timeStamp doubleValue];
    //查询scheduleCata是否有效
    NSDictionary *scheduleCataDic = [IXDBScheduleCataMgr queryScheduleCataByScheduleCataId:symModel.scheduleCataId];
    if (!scheduleCataDic) {
        return IXSymbolTradeStateNoSchedule;
    }
    NSArray *scheduleArr = [IXDBScheduleMgr queryScheduleByScheduleCataId:symModel.scheduleCataId];
    if (symModel.startTime > 0) {
        
        if (symModel.startTime > currentTime) {
            
            return IXSymbolTradeStateDisEnable;
        }
        
    }
    if (symModel.expiryTime > 0) {
        
        if (symModel.expiryTime < currentTime) {
            
            return IXSymbolTradeStateExpiry;
        }
    }
    if ( !scheduleArr || scheduleArr.count == 0) {
        
        return IXSymbolTradeStateNoSchedule;
    }
    IXSymbolTradeState state =  [IXDataProcessTools getTradeStateByTradable:symModel.tradable symTradale:symModel.symTradable isSetGroupSymbol:symModel.isSetGroupSym closeOnly:symModel.closeOnly];
    if (state == IXSymbolTradeStateNoSchedule) {
        return IXSymbolTradeStateNoSchedule;
    }
    //冬令时延迟交易时间(优先查询symbol表、然后再找symbolCata表)
    int scheduleDelayMinutes = symModel.scheduleDelayMinutes;
    if (scheduleDelayMinutes == 0) {
        NSDictionary *symCataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:symModel.cataId];
        if (symCataDic) {
            scheduleDelayMinutes = [symCataDic[kScheduleDelayMinutes] integerValue];
        }
    }
    //拆分跨前一天和后一天
    scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr];
    if (scheduleDelayMinutes) {
        scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr withScheduleDelayMinutes:scheduleDelayMinutes];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dayOfWeek == %@",[NSString stringWithFormat:@"%ld",(long)weekday]];
    NSArray *filterScheduleArr = [scheduleArr filteredArrayUsingPredicate:predicate];
    for ( NSDictionary *scheduleDic in filterScheduleArr ) {
        if ( ![scheduleDic[kEnable] boolValue] ||
            0 != [scheduleDic[kStatus] integerValue] || [scheduleDic[KTradable] boolValue]) {
            continue;
        }
        
        //先判断是否在区间，再判断是否跨天
        NSInteger minute = result.minute + result.hour * 60;
        if( minute >= ([scheduleDic[kStartTime] integerValue]) &&
           minute <= ([scheduleDic[kEndTime] integerValue] ) ){
            schedule = [[item_schedule alloc] init];
            schedule.id_p = [scheduleDic[kID] doubleValue];
            break;
        }else if ( ([scheduleDic[kEndTime] integerValue] ) <= minute ) {
            if( ([scheduleDic[kStartTime] integerValue] ) < 0 ){
                NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"dayOfWeek == %@",
                                           [NSString stringWithFormat:@"%ld",(long)(weekday+1)]];
                NSArray *filter2ScheduleArr = [scheduleArr filteredArrayUsingPredicate:predicate2];
                for ( NSDictionary *predicate2 in filter2ScheduleArr ) {
                    if ( ![scheduleDic[kEnable] boolValue] ||
                        0 != [scheduleDic[kStatus] integerValue] ) {
                        continue;
                    }else{
                        
                        minute = minute - 1440;;
                        if ( ([predicate2[kStartTime] integerValue] ) <= minute ) {
                            schedule = [[item_schedule alloc] init];
                            schedule.id_p = [scheduleDic[kID] doubleValue];
                            break;
                        }
                    }
                    
                }
            }
        } else {
            //延时跨天
            if (([scheduleDic[kEndTime] integerValue] ) - 1440 >= minute) {
                schedule = [[item_schedule alloc] init];
                schedule.id_p = [scheduleDic[kID] doubleValue];
                break;
            }
        }
    }
    if (!schedule) {
        
        return IXSymbolTradeStateNoSchedule;
    }
    return [IXDataProcessTools getTradeStateByTradable:symModel.tradable symTradale:symModel.symTradable isSetGroupSymbol:symModel.isSetGroupSym closeOnly:symModel.closeOnly];
}


#pragma mark  查询当前交易时间Model
+ (item_schedule *)queryScheduleBySymbol:(IXSymbolM *)symModel {
    
    item_schedule *schedule;
    //星期天排第0个
    NSDateComponents *result = [IXDateUtils currentTimeComponents];
    NSInteger weekday = (result.weekday + 7)%8;
    
    NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSTimeInterval currentTime = [timeStamp doubleValue];
    //查询scheduleCata是否有效
    NSDictionary *scheduleCataDic = [IXDBScheduleCataMgr queryScheduleCataByScheduleCataId:symModel.scheduleCataId];
    if (!scheduleCataDic) {
        return schedule;
    }
    NSArray *scheduleArr = [IXDBScheduleMgr queryScheduleByScheduleCataId:symModel.scheduleCataId];
    if (symModel.startTime > 0) {
        
        if (symModel.startTime > currentTime) {
            
            return schedule;
        }
        
    }
    if (symModel.expiryTime > 0) {
        
        if (symModel.expiryTime < currentTime) {
            
            return schedule;
        }
    }
    IXSymbolTradeState state =  [IXDataProcessTools getTradeStateByTradable:symModel.tradable symTradale:symModel.symTradable isSetGroupSymbol:symModel.isSetGroupSym closeOnly:symModel.closeOnly];
    if ( !scheduleArr || state == IXSymbolTradeStateNoSchedule ||
        scheduleArr.count == 0 ) {
        
        return schedule;
    }
    //冬令时延迟交易时间(优先查询symbol表、然后再找symbolCata表)
    int scheduleDelayMinutes = symModel.scheduleDelayMinutes;
    if (scheduleDelayMinutes == 0) {
        NSDictionary *symCataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:symModel.cataId];
        if (symCataDic) {
            scheduleDelayMinutes = [symCataDic[kScheduleDelayMinutes] integerValue];
        }
    }
    //拆分跨前一天和后一天
    scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr];
    if (scheduleDelayMinutes) {
        scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr withScheduleDelayMinutes:scheduleDelayMinutes];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dayOfWeek == %@",[NSString stringWithFormat:@"%ld",(long)weekday]];
    NSArray *filterScheduleArr = [scheduleArr filteredArrayUsingPredicate:predicate];
    for ( NSDictionary *scheduleDic in filterScheduleArr ) {
        if ( ![scheduleDic[kEnable] boolValue] ||
            0 != [scheduleDic[kStatus] integerValue] ) {
            continue;
        }
        //先判断是否在区间，再判断是否跨天
        NSInteger minute = result.minute + result.hour * 60;
        if( minute >= ([scheduleDic[kStartTime] integerValue] ) &&
           minute <= ([scheduleDic[kEndTime] integerValue] ) ){
            schedule = [[item_schedule alloc] init];
            schedule.id_p = [scheduleDic[kID] doubleValue];
            break;
        }else if ( ([scheduleDic[kEndTime] integerValue] ) <= minute ) {
            if( ([scheduleDic[kStartTime] integerValue] ) < 0 ){
                NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"dayOfWeek == %@",
                                           [NSString stringWithFormat:@"%ld",(long)(weekday+1)]];
                NSArray *filter2ScheduleArr = [scheduleArr filteredArrayUsingPredicate:predicate2];
                for ( NSDictionary *predicate2 in filter2ScheduleArr ) {
                    if ( ![scheduleDic[kEnable] boolValue] ||
                        0 != [scheduleDic[kStatus] integerValue] ) {
                        continue;
                    }else{
                        minute = minute - 1440;;
                        if ( ([predicate2[kStartTime] integerValue] ) >= minute ) {
                            schedule = [[item_schedule alloc] init];
                            schedule.id_p = [scheduleDic[kID] doubleValue];
                            break;
                        }
                    }
                }
            }
        } else {
            //延时跨天
            if (([scheduleDic[kEndTime] integerValue] ) - 1440 >= minute) {
                schedule = [[item_schedule alloc] init];
                schedule.id_p = [scheduleDic[kID] doubleValue];
                break;
            }
        }
    }
    return schedule;
}

#pragma mark 拆分跨天
+ (NSArray *)splitSchedule:(NSArray *)scheduleArr
{
    NSMutableArray *retArr = [NSMutableArray array];
    for (int i = 0; i < scheduleArr.count; i++) {
        NSMutableDictionary *dic = [scheduleArr[i] mutableCopy];
        NSInteger startTime = [dic[kStartTime] integerValue];
        NSInteger endTime = [dic[kEndTime] integerValue];
        NSInteger dayOfWeek = [dic[kDayOfWeek] integerValue];
        NSInteger preDayOfWeek = -1;
        NSInteger NextDayOfWeek = -1;
        if (startTime >= 0 && endTime <= 1440) {
            [retArr addObject:dic];
        } else if (startTime >= -1440 && startTime < 0 && endTime > 0 && endTime <= 1440) {
            NSMutableDictionary *preDic = [dic mutableCopy];
            NSInteger preStartTime = startTime + 1440;
            [preDic setObject:@(preStartTime) forKey:kStartTime];
            [preDic setObject:@(1440) forKey:kEndTime];
            if (dayOfWeek == 0) {
                preDayOfWeek = 6;
            } else {
                preDayOfWeek = dayOfWeek - 1;
            }
            [preDic setObject:[NSString stringWithFormat:@"%ld",(long)preDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:preDic];
            [dic setObject:@(0) forKey:kStartTime];
            [retArr addObject:dic];
        } else if (startTime >= 0 && startTime < 1440 && endTime > 1440) {
            [dic setObject:@(startTime) forKey:kStartTime];
            [dic setObject:@(1440) forKey:kEndTime];
            [retArr addObject:dic];
            NSMutableDictionary *nextDic = [dic mutableCopy];
            [nextDic setObject:@(0) forKey:kStartTime];
            [nextDic setObject:@(endTime - 1440) forKey:kEndTime];
            if (dayOfWeek == 6) {
                NextDayOfWeek = 0;
            } else {
                NextDayOfWeek = dayOfWeek + 1;
            }
            [nextDic setObject:[NSString stringWithFormat:@"%ld",(long)NextDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:nextDic];
        } else if (startTime >= 1440 && startTime < 2880 && endTime > 1440 && endTime <= 2880) {
            NSMutableDictionary *nextDic = [dic mutableCopy];
            [nextDic setObject:@(startTime - 1440) forKey:kStartTime];
            [nextDic setObject:@(endTime - 1440) forKey:kEndTime];
            if (dayOfWeek == 6) {
                NextDayOfWeek = 0;
            } else {
                NextDayOfWeek = dayOfWeek + 1;
            }
            [nextDic setObject:[NSString stringWithFormat:@"%ld",(long)NextDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:nextDic];
        } else if (startTime >= -1440 && startTime < 0 && endTime > 1440) {
            NSMutableDictionary *preDic = [dic mutableCopy];
            NSInteger preStartTime = startTime + 1440;
            [preDic setObject:@(preStartTime) forKey:kStartTime];
            [preDic setObject:@(1440) forKey:kEndTime];
            if (dayOfWeek == 0) {
                preDayOfWeek = 6;
            } else {
                preDayOfWeek = dayOfWeek - 1;
            }
            [preDic setObject:[NSString stringWithFormat:@"%ld",(long)preDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:preDic];
            [dic setObject:@(0) forKey:kStartTime];
            [dic setObject:@(1440) forKey:kEndTime];
            [retArr addObject:dic];
            NSMutableDictionary *nextDic = [dic mutableCopy];
            [nextDic setObject:@(0) forKey:kStartTime];
            [nextDic setObject:@(endTime - 1440) forKey:kEndTime];
            if (dayOfWeek == 6) {
                NextDayOfWeek = 0;
            } else {
                NextDayOfWeek = dayOfWeek + 1;
            }
            [nextDic setObject:[NSString stringWithFormat:@"%ld",(long)NextDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:nextDic];
        } else if (startTime < 0 && startTime >= -1440 && endTime <= 0 && endTime > -1440) {
            NSMutableDictionary *preDic = [dic mutableCopy];
            [preDic setObject:@(startTime + 1440) forKey:kStartTime];
            [preDic setObject:@(endTime + 1440) forKey:kEndTime];
            if (dayOfWeek == 0) {
                preDayOfWeek = 6;
            } else {
                preDayOfWeek = dayOfWeek - 1;
            }
            [preDic setObject:[NSString stringWithFormat:@"%ld",(long)preDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:preDic];
        } else {
            [retArr addObject:dic];
        }
    }
    return retArr;
}

#pragma mark 拆分跨天
+ (NSArray *)splitSchedule:(NSArray *)scheduleArr withScheduleDelayMinutes:(int)scheduleDelayMinutes
{
    NSMutableArray *retArr = [NSMutableArray array];
    for (int i = 0; i < scheduleArr.count; i++) {
        NSMutableDictionary *dic = [scheduleArr[i] mutableCopy];
        NSInteger startTime = [dic[kStartTime] integerValue] + scheduleDelayMinutes;
        NSInteger endTime = [dic[kEndTime] integerValue] + scheduleDelayMinutes;
        [dic setObject:@(startTime) forKey:kStartTime];
        [dic setObject:@(endTime) forKey:kEndTime];
        NSInteger dayOfWeek = [dic[kDayOfWeek] integerValue];
        NSInteger preDayOfWeek = -1;
        NSInteger NextDayOfWeek = -1;
        if (startTime >= 0 && endTime <= 1440) {
            [retArr addObject:dic];
        } else if (startTime >= -1440 && startTime < 0 && endTime > 0 && endTime <= 1440) {
            NSMutableDictionary *preDic = [dic mutableCopy];
            NSInteger preStartTime = startTime + 1440;
            [preDic setObject:@(preStartTime) forKey:kStartTime];
            [preDic setObject:@(1440) forKey:kEndTime];
            if (dayOfWeek == 0) {
                preDayOfWeek = 6;
            } else {
                preDayOfWeek = dayOfWeek - 1;
            }
            [preDic setObject:[NSString stringWithFormat:@"%ld",(long)preDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:preDic];
            [dic setObject:@(0) forKey:kStartTime];
            [retArr addObject:dic];
        } else if (startTime >= 0 && startTime < 1440 && endTime > 1440) {
            [dic setObject:@(startTime) forKey:kStartTime];
            [dic setObject:@(1440) forKey:kEndTime];
            [retArr addObject:dic];
            NSMutableDictionary *nextDic = [dic mutableCopy];
            [nextDic setObject:@(0) forKey:kStartTime];
            [nextDic setObject:@(endTime - 1440) forKey:kEndTime];
            if (dayOfWeek == 6) {
                NextDayOfWeek = 0;
            } else {
                NextDayOfWeek = dayOfWeek + 1;
            }
            [nextDic setObject:[NSString stringWithFormat:@"%ld",(long)NextDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:nextDic];
        } else if (startTime >= 1440 && startTime < 2880 && endTime > 1440 && endTime <= 2880) {
            NSMutableDictionary *nextDic = [dic mutableCopy];
            [nextDic setObject:@(startTime - 1440) forKey:kStartTime];
            [nextDic setObject:@(endTime - 1440) forKey:kEndTime];
            if (dayOfWeek == 6) {
                NextDayOfWeek = 0;
            } else {
                NextDayOfWeek = dayOfWeek + 1;
            }
            [nextDic setObject:[NSString stringWithFormat:@"%ld",(long)NextDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:nextDic];
        } else if (startTime >= -1440 && startTime < 0 && endTime > 1440) {
            NSMutableDictionary *preDic = [dic mutableCopy];
            NSInteger preStartTime = startTime + 1440;
            [preDic setObject:@(preStartTime) forKey:kStartTime];
            [preDic setObject:@(1440) forKey:kEndTime];
            if (dayOfWeek == 0) {
                preDayOfWeek = 6;
            } else {
                preDayOfWeek = dayOfWeek - 1;
            }
            [preDic setObject:[NSString stringWithFormat:@"%ld",(long)preDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:preDic];
            [dic setObject:@(0) forKey:kStartTime];
            [dic setObject:@(1440) forKey:kEndTime];
            [retArr addObject:dic];
            NSMutableDictionary *nextDic = [dic mutableCopy];
            [nextDic setObject:@(0) forKey:kStartTime];
            [nextDic setObject:@(endTime - 1440) forKey:kEndTime];
            if (dayOfWeek == 6) {
                NextDayOfWeek = 0;
            } else {
                NextDayOfWeek = dayOfWeek + 1;
            }
            [nextDic setObject:[NSString stringWithFormat:@"%ld",(long)NextDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:nextDic];
        } else if (startTime < 0 && startTime >= -1440 && endTime <= 0 && endTime > -1440) {
            NSMutableDictionary *preDic = [dic mutableCopy];
            [preDic setObject:@(startTime + 1440) forKey:kStartTime];
            [preDic setObject:@(endTime + 1440) forKey:kEndTime];
            if (dayOfWeek == 0) {
                preDayOfWeek = 6;
            } else {
                preDayOfWeek = dayOfWeek - 1;
            }
            [preDic setObject:[NSString stringWithFormat:@"%ld",(long)preDayOfWeek] forKey:kDayOfWeek];
            [retArr addObject:preDic];
        } else {
            [retArr addObject:dic];
        }
    }
    return retArr;
}

#pragma mark 显示数量或手数
+ (NSString *)showCurrentVolume:(double)volume
                contractSizeNew:(int32)contractSizeNew
                       volDigit:(uint64_t)volDigit
{
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
        NSString *volDigitStr = [NSString stringWithFormat:@"%%.%lldf", volDigit];
        return  [NSString stringWithFormat:volDigitStr, volume];;
    } else {
        double vol = volume;
        if (contractSizeNew) {
            vol = volume/contractSizeNew;
        }
        return [NSString stringWithFormat:@"%@",[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",vol]]];
    }
}

#pragma mark  获取手数实际小数位
+ (NSInteger)volumeDigits:(NSDecimalNumber *)volume
{
    NSString *vol = [NSString stringWithFormat:@"%@",volume];
    NSInteger digits = 0;
    NSArray *arr = [vol componentsSeparatedByString:@"."];
    if (arr.count == 2) {
        digits = [arr[1] length];
    }
    return digits;
}

#pragma mark  显示数量或手数单位
+ (NSString *)showCurrentUnitLanName:(NSString *)unitLanName
{
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
        return unitLanName;
    } else {
        return LocalizedString(@"手");
    }
}

#pragma mark 获取账户类型
+ (item_account_group_etype)getCurrentAccountType
{
    NSDictionary *accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
    if (accGroupDic && accGroupDic.count > 0) {
        
        NSString *type = [accGroupDic stringForKey:kType];
        if (type && type.length > 0) {
            
            if ([type isEqualToString:@"1"]) {
                
                return item_account_group_etype_Standard;
            } else if ([type isEqualToString:@"2"]) {
                
                return item_account_group_etype_Vip;
            } else if ([type isEqualToString:@"3"]) {
                
                return item_account_group_etype_Manager;
            } else if ([type isEqualToString:@"4"]) {
                
                return item_account_group_etype_Turnover;
            } else if ([type isEqualToString:@"8"]) {
                
                return item_account_group_etype_Agent;
            } else {
                
                return item_account_group_etype_Virtuals;
            }
        } else {
            
            return item_account_group_etype_Virtuals;
        }
    } else {
        
        return item_account_group_etype_Virtuals;
    }
}

#pragma mark  通过账户类型获取账户组名称
+ (NSString *)getAccountNameByType:(NSInteger)type
{
    switch (type) {
        case 0:  return @"VIRTUALS";
        case 1:  return @"STANDARD";
        case 2:  return @"VIP";
        case 3:  return @"MANAGER";
        case 4:  return @"TURNOVER";
        case 5:  return @"ROOM";
        case 6:  return @"DIAMOND";
        case 8:  return @"AGENT";
        default:
            break;
    }
    return @"";
}

#pragma mark 显示账户类型
+ (NSString *)showCurrentAccountType
{
    if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
        return LocalizedString(@"游客");
    }
    item_account_group_etype tyep = [IXDataProcessTools getCurrentAccountType];
    if (tyep == item_account_group_etype_Virtuals) {
        
        return LocalizedString(@"模拟");
    } else  {
        
        return  LocalizedString(@"真实");
    }
}

+ (NSString *)documentDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)createFile:(NSString *)fileName
{
    NSString *documentsPath =[IXDataProcessTools documentDirectory];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    BOOL isExist = [fileManager fileExistsAtPath:filePath];
    if (isExist) {
        return filePath;
    }
    BOOL isSuccess = [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    if (isSuccess) {
        return filePath;
    } else {
        ELog(@"创建文件路径失败");
        return nil;
    }
}

+ (NSString *)timeIntervalToString:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:[IXDateUtils convertTimeZoneString:[IXDateUtils getDefaultTimeZone]]]];
    [formatter setDateFormat:@"HH:mm"];
    return [formatter stringFromDate:date];
}

+ (NSString *)formatterTimeDisplayHour:(NSNumber *)index{
    NSInteger cIndex = [index integerValue];
    if ( (cIndex >= 0 && cIndex < 10) || ( cIndex <= 0 && cIndex > -10) ) {
        return [NSString stringWithFormat:@"%02ld",(long)cIndex];
    } else {
        return [NSString stringWithFormat:@"%ld",(long)cIndex];
    }
}

+ (NSString *)formaterTimeDisplay:(NSNumber *)index
{
    int cIndex = [index intValue];
    if (cIndex == 0) {
        return @"00";
    } else {
        if (cIndex > 0 && cIndex < 10) {
            return [NSString stringWithFormat:@"0%d",cIndex];
        } else {
            return [NSString stringWithFormat:@"%d",cIndex];
        }
    }
}

+ (NSString *)weekdayToWeekString:(int)index
{
    switch (index) {
        case 0:
            return LocalizedString(@"周日");
            break;
        case 1:
            return LocalizedString(@"周一");
            break;
        case 2:
            return LocalizedString(@"周二");
            break;
        case 3:
            return LocalizedString(@"周三");
            break;
        case 4:
            return LocalizedString(@"周四");
            break;
        case 5:
            return LocalizedString(@"周五");
            break;
        case 6:
            return LocalizedString(@"周六");
            break;
        default:
            break;
    }
    return @"";
}

+ (int)weekStringToWeekday:(NSString *)weekDay
{
    if ([weekDay isEqualToString:LocalizedString(@"周日")]) {
        return 0;
    } else if ([weekDay isEqualToString:LocalizedString(@"周一")]) {
        return 1;
    } else if ([weekDay isEqualToString:LocalizedString(@"周二")]) {
        return 2;
    } else if ([weekDay isEqualToString:LocalizedString(@"周三")]) {
        return 3;
    } else if ([weekDay isEqualToString:LocalizedString(@"周四")]) {
        return 4;
    } else if ([weekDay isEqualToString:LocalizedString(@"周五")]) {
        return 5;
    } else if ([weekDay isEqualToString:LocalizedString(@"周六")]) {
        return 6;
    } else {
        return -1;
    }
}

/**
 *  MD5加密字符串
 *
 *  @param str 源字符串
 *
 */
+ (NSString *) md5StringByString:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (uint32_t)strlen(cStr), digest ); // This is the md5 call
    NSMutableString *retStr = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [retStr appendFormat:@"%02x", digest[i]];
    }
    return  retStr;
}

#pragma mark 字典转字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

#pragma mark  获取文本内容的尺寸
+ (CGSize)textSizeByText:(NSString *)str height:(CGFloat)height font:(UIFont *)font
{
    CGSize retSize = CGSizeMake(0, 0);
    if (!str || str.length == 0 || !font) {
        return retSize;
    }
    CGSize size = CGSizeMake(MAXFLOAT, height);
    retSize = [str boundingRectWithSize:size
                                options:NSStringDrawingTruncatesLastVisibleLine |
               NSStringDrawingUsesLineFragmentOrigin |
               NSStringDrawingUsesFontLeading
                             attributes:@{ NSFontAttributeName:font}
                                context:nil].size;
    return retSize;
}

#pragma mark  显示富文本
+ (void)resetLabel:(UILabel *)label
          leftFont:(CGFloat)leftFont
         rightFont:(CGFloat)rightFont
       WithContent:(NSString *)cntStr
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:cntStr];
    
    NSRange range = [cntStr rangeOfString:@"."];
    [attrStr addAttribute:NSFontAttributeName
                    value:RO_REGU(leftFont)
                    range:NSMakeRange(0, range.location)];
    [attrStr addAttribute:NSFontAttributeName
                    value:RO_REGU(rightFont)
                    range:NSMakeRange(range.location + 1, cntStr.length - range.location - 1)];
    label.attributedText = attrStr;
}

#pragma mark 显示富文本颜色
+ (void)resetLabel:(UILabel *)label
       leftContent:(NSString *)leftStr
          leftFont:(CGFloat)leftFont
       WithContent:(NSString *)cntStr
         rightFont:(CGFloat)rightFont
          fontType:(BOOL)type
{
    if (!cntStr.length) {
        return;
    }
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:cntStr];
    UIFont *leftF = nil,*rightF = nil;
    if (type) {
        leftF = RO_REGU(leftFont);
        rightF = PF_REGU(rightFont);
        [attrStr addAttribute:NSFontAttributeName
                        value:leftF
                        range:NSMakeRange(0, leftStr.length)];
    } else {
        leftF = PF_REGU(leftFont);
        rightF = RO_REGU(rightFont);
        [attrStr addAttribute:NSFontAttributeName
                        value:rightF
                        range:NSMakeRange(leftStr.length + 1, cntStr.length - (1 + leftStr.length))];
    }
    label.attributedText = attrStr;
}

#pragma mark  重设控件文本颜色
+ (void)resetTextColorLabel:(UILabel *)label
                      value:(double)value
{
    double val = [[NSString stringWithFormat:@"%.2f",value] doubleValue];
    if(val >= 0.01) {
        label.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    } else if ((val >= 0 && val < 0.01) || (val < 0 && val > -0.01)) {
        label.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
    } else {
        label.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
}

#pragma mark 通过SymbolId获取产品行情小数位数
+ (NSInteger)getQuotoDigitBySymbolId:(uint64_t)symbolId
{
    NSInteger digits = 1;
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
    if (symDic && symDic.count > 0) {
        digits = [symDic[kDigits] integerValue];
    } else {
        digits = 1;
    }
    return digits;
}

#pragma mark  拆分行情价格小数位数
+ (NSArray *)getArrSplitPrice:(float)price symbolModel:(IXSymbolM *)model
{
    NSInteger digit = model.digits;
    NSString *strDigitFormat = [NSString stringWithFormat:@"%%.%ldf", (long)digit];
    NSString *strPrice = [NSString stringWithFormat:strDigitFormat, price];
    NSUInteger length = [strPrice length];
    int pipsRatio = model.pipsRatio;
    int point = 0;
    while(pipsRatio >= 10){
        pipsRatio /= 10;
        point++;
    }
    NSString *strPricepart1 = @"";
    NSString *strPricepart2 = @"";
    NSString *strPricepart3 = @"";
    if (ISZero(price) || ! pipsRatio || (length-1-point) <= 0) {
        NSArray *array = [[NSArray alloc] initWithObjects: strPrice, strPricepart2, strPricepart3, nil];
        return array;
    }
    
    NSUInteger pointIndex;
    NSMutableString *pointStr = [strPrice mutableCopy];
    NSArray *array;
    if (digit>0) {//有小数点情况
        pointIndex = [strPrice rangeOfString:@"."].location;
        [pointStr deleteCharactersInRange:[strPrice rangeOfString:@"."]];
        
        NSInteger startLoc = pointStr.length-point-2;
        NSInteger startLen = 2;
        if(startLoc < 0){
            startLoc = 0;
            startLen = 1;
        }
        strPricepart2 = [pointStr substringWithRange:NSMakeRange(startLoc,startLen)];
        strPricepart3 = [pointStr substringWithRange:NSMakeRange(pointStr.length-point,pointStr.length - (pointStr.length-point))];
        strPricepart1 = [pointStr substringWithRange:NSMakeRange(0,pointStr.length -strPricepart2.length-strPricepart3.length)];
        
        NSMutableString *prictStr1 = [strPricepart1 mutableCopy];
        NSMutableString *prictStr2 = [strPricepart2 mutableCopy];
        NSMutableString *prictStr3 = [strPricepart3 mutableCopy];
        
        if(pointIndex <= strPricepart1.length){
            [prictStr1 insertString:@"." atIndex:pointIndex];
            
        }else if ((strPricepart1.length < pointIndex) &&(pointIndex <= strPricepart1.length+strPricepart2.length)){
            [prictStr2 insertString:@"." atIndex:pointIndex-strPricepart1.length];
        }else{
            [prictStr3 insertString:@"." atIndex:pointIndex-strPricepart1.length-strPricepart2.length];
        }
        array = [[NSArray alloc] initWithObjects: prictStr1, prictStr2, prictStr3, nil];
    } else {
        strPricepart2 = @"";
        strPricepart3 = @"";
        strPricepart1 = [pointStr substringWithRange:NSMakeRange(0,pointStr.length -strPricepart2.length-strPricepart3.length)];
        array = [[NSArray alloc] initWithObjects: strPricepart1, strPricepart2, strPricepart3, nil];
    }
    return array;
}

#pragma mark 追加多语言以及产品label入symbolModel
+ (IXSymbolM *)appendLanguageNameAndLabelBySymbolModel:(NSDictionary *)symDic
{
    IXSymbolM *symModel = [[IXSymbolM alloc] init];
    symModel = [IXDataProcessTools symbolModelBySymbolInfo:symDic];
    if (!symModel) {
        return nil;
    }
    IXSymbolM *model = [[IXSymbolM alloc] init];
    model = symModel;
    NSArray *labelArr = [[NSArray alloc] init];
    NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
    labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:model.id_p];
    if (labelArr && labelArr.count > 0) {
        model.labelArr = labelArr;
    }
    if (languageDic && languageDic.count > 0) {
        if (languageDic && languageDic.count > 0) {
            if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                model.languageName = languageDic[kValue];
            } else {
                model.languageName = model.name;
            }
        } else {
            model.languageName = model.name;
        }
    } else {
        model.languageName = model.name;
    }
    
    NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
    if (unitLanDic && unitLanDic.count > 0) {
        if (unitLanDic && unitLanDic.count > 0) {
            if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                model.unitLanName = unitLanDic[kValue];
            } else {
                model.unitLanName = model.displayName;
            }
        } else {
            model.unitLanName = model.displayName;
        }
    } else {
        model.unitLanName = model.displayName;
    }
    return model;
}

#pragma mark   根据产品信息转换产品Model
+ (IXSymbolM *)symbolModelBySymbolInfo:(NSDictionary *)symDic
{
    if (!symDic || symDic.count == 0) {
        return nil;
    }
    IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
    tmpModel.id_p = [[IXDataProcessTools dealWithNil:symDic[kID]] longLongValue];
    tmpModel.uuid = [[IXDataProcessTools dealWithNil:symDic[kUUID]] longLongValue];
    tmpModel.enable = [[IXDataProcessTools dealWithNil:symDic[kEnable]] intValue];
    tmpModel.profitCurrency = [IXDataProcessTools dealWithNil:symDic[kProfitcurrency]];
    tmpModel.holidayCataId =[[IXDataProcessTools dealWithNil:symDic[kHolidayCataId]] longLongValue];
    tmpModel.pipsRatio = [[IXDataProcessTools dealWithNil:symDic[kPipsRatio]] intValue];
    tmpModel.uuTime = [[IXDataProcessTools dealWithNil:symDic[kUUTime]] longLongValue];
    tmpModel.source = [IXDataProcessTools dealWithNil:symDic[kSource]];
    tmpModel.digits = [[IXDataProcessTools dealWithNil:symDic[kDigits]] intValue];
    tmpModel.baseCurrency = [IXDataProcessTools dealWithNil:symDic[kBaseCurrency]];
    tmpModel.cataId = [[IXDataProcessTools dealWithNil:symDic[kCataId]] longLongValue];
    tmpModel.sequence = [[IXDataProcessTools dealWithNil:symDic[kSequence]] longLongValue];
    tmpModel.name = [IXDataProcessTools dealWithNil:symDic[kName]];
    tmpModel.scheduleCataId = [[IXDataProcessTools dealWithNil:symDic[kScheduleCataId]] longLongValue];
    tmpModel.languageName = [IXDataProcessTools dealWithNil:symDic[kLanguageName]];
    tmpModel.contractSize = [[IXDataProcessTools dealWithNil:symDic[kContractSize]] intValue];
    tmpModel.stopLevel = [[IXDataProcessTools dealWithNil:symDic[kStopLevel]] intValue];
    if (tmpModel.stopLevel == 0) {
        tmpModel.stopLevel = 1;
    }
    tmpModel.maxStopLevel = [[IXDataProcessTools dealWithNil:symDic[kMaxStopLevel]] intValue];
    tmpModel.volumesMin = [[IXDataProcessTools dealWithNil:symDic[kVolumesMin]] doubleValue];
    tmpModel.volumesMax = [[IXDataProcessTools dealWithNil:symDic[kVolumsMax]] doubleValue];
    tmpModel.volumesStep = [[IXDataProcessTools dealWithNil:symDic[kVolumesStep]] doubleValue];
    tmpModel.swapDaysPerYear = [[IXDataProcessTools dealWithNil:symDic[kSwapDaysPerYear]] doubleValue];
    tmpModel.spread = [[IXDataProcessTools dealWithNil:symDic[kSpread]] doubleValue];
    tmpModel.spreadBalance = [[IXDataProcessTools dealWithNil:symDic[kSpreadBalance]] doubleValue];
    tmpModel.positionVolumeMax = [[IXDataProcessTools dealWithNil:symDic[kPositionVolumeMax]] doubleValue];
    tmpModel.displayName = [IXDataProcessTools dealWithNil:symDic[kDisplayName]];
    tmpModel.longSwap = [[IXDataProcessTools dealWithNil:symDic[kLongSwap]] doubleValue];
    tmpModel.shortSwap = [[IXDataProcessTools dealWithNil:symDic[kShortSwap]] doubleValue];
    tmpModel.startTime = [[IXDataProcessTools dealWithNil:symDic[kStartTime]] doubleValue];
    tmpModel.expiryTime = [[IXDataProcessTools dealWithNil:symDic[kExpiryTime]] doubleValue];
    tmpModel.tradable = [[IXDataProcessTools dealWithNil:symDic[KTradable]] intValue];
    tmpModel.scheduleDelayMinutes = [[IXDataProcessTools dealWithNil:symDic[kScheduleDelayMinutes]] intValue];
    tmpModel.contractSizeNew = [[IXDataProcessTools dealWithNil:symDic[kContractSizeNew]] intValue];
    tmpModel.volDigits = [[IXDataProcessTools dealWithNil:symDic[kVolDigits]] longLongValue];
    
    return [IXDataProcessTools dealWithGroupSymbol:tmpModel];
}

#pragma mark 账户组产品信息
+ (IXSymbolM *)dealWithGroupSymbol:(IXSymbolM *)symModel
{
    NSDictionary *groupSymDic = [IXDBG_SMgr queryG_SByAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid symbolId:symModel.id_p];
    if (groupSymDic && groupSymDic.count > 0) {
        symModel.symTradable = [[IXDataProcessTools dealWithNil:groupSymDic[KTradable]] integerValue];
        symModel.isSetGroupSym = YES;
        symModel.closeOnly = [[IXDataProcessTools dealWithNil:groupSymDic[kCloseOnly]] boolValue];
        symModel.volumesMin = [[IXDataProcessTools dealWithNil:groupSymDic[kVolumesMin]] doubleValue];
        symModel.volumesMax = [[IXDataProcessTools dealWithNil:groupSymDic[kVolumsMax]] doubleValue];
        symModel.volumesStep = [[IXDataProcessTools dealWithNil:groupSymDic[kVolumesStep]] doubleValue];
        symModel.longSwap = [[IXDataProcessTools dealWithNil:groupSymDic[kLongSwap]] doubleValue];
        symModel.shortSwap = [[IXDataProcessTools dealWithNil:groupSymDic[kShortSwap]] doubleValue];
        //产品开始时间和到期时间关联groupSymbol
        uint64_t startTime = [[IXDataProcessTools dealWithNil:groupSymDic[kStartTime]] doubleValue];
        if (startTime > symModel.startTime) {
            symModel.startTime = startTime;
        }
        uint64_t expiryTime = [[IXDataProcessTools dealWithNil:groupSymDic[kExpiryTime]] doubleValue];
        //lp过期时间（0代表永不过期）
        uint64_t lpExpiryTime = [[IXDataProcessTools dealWithNil:groupSymDic[kLpExpiryTime]] doubleValue];
        if (lpExpiryTime > 0) {
            if (expiryTime == 0) {
                expiryTime = lpExpiryTime;
            } else {
                if (lpExpiryTime < expiryTime) {
                    expiryTime = lpExpiryTime;
                }
            }
        }
        if (symModel.expiryTime > 0) {
            if ((expiryTime > 0) && (expiryTime < symModel.expiryTime)) {
                symModel.expiryTime = expiryTime;
            }
        } else {
            symModel.expiryTime = expiryTime;
        }
    }
    return symModel;
}

#pragma mark   批量将产品信息转换产品Model
+ (NSArray *)symbolModelsBatchBySymbolInfos:(NSArray *)symArr
{
    if (!symArr || symArr.count == 0) {
        return nil;
    }
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < symArr.count; i++) {
        IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
        tmpModel = [IXDataProcessTools symbolModelBySymbolInfo:symArr[i]];
        if (tmpModel) {
            [retArr addObject:tmpModel];
        }
    }
    return retArr;
}

#pragma mark 批量保存行情价
+ (BOOL)saveBatchQuotePrice:(NSArray *)modelArr
{
    NSMutableArray *quoteArr = [[NSMutableArray alloc] init];
    NSMutableArray *deepArr = [[NSMutableArray alloc] init];
    BOOL flag = NO;
    
    for (int i = 0; i < modelArr.count; i++) {
        NSMutableDictionary *quoteDic = [[NSMutableDictionary alloc] init];
        IXQuoteM *tmpModel = modelArr[i];
        if (tmpModel) {
            //行情
            [quoteDic setObject:@(tmpModel.n1970Time) forKey:kNGmtTime];
            [quoteDic setObject:@(tmpModel.nOpen) forKey:kNOpen];
            [quoteDic setObject:@(tmpModel.nHigh) forKey:kNHight];
            [quoteDic setObject:@(tmpModel.nPrice) forKey:kNPrice];
            [quoteDic setObject:@(tmpModel.symbolId) forKey:kSymbolId];
            [quoteArr addObject:quoteDic];
            for (int j = 0; j < tmpModel.BuyPrc.count; j++) {
                NSMutableDictionary *deepDic = [[NSMutableDictionary alloc] init];
                // 深度价
                [deepDic setObject:tmpModel.BuyPrc[j] forKey:kBuyPrice];
                [deepDic setObject:tmpModel.BuyVol[j] forKey:kBuyVolume];
                [deepDic setObject:tmpModel.SellPrc[j] forKey:kSellPrice];
                [deepDic setObject:tmpModel.SellVol[j] forKey:kSellVolume];
                [deepDic setObject:@(tmpModel.symbolId) forKey:kSymbolId];
                [deepArr addObject:deepDic];
            }
        }
    }
    flag = [IXQuotePriceMgr saveBatchQuotePriceInfo:quoteArr];
    flag = [IXDBDeepPriceMgr saveBatchDeepPriceInfo:deepArr];
    return flag;
}

#pragma mark 查询行情价
+ (IXQuoteM *)queryQuoteDataBySymbolId:(uint64_t)symbolId
{
    IXQuoteM *qdModel = [[IXQuoteM alloc] init];
    NSDictionary *quoteDic = [IXQuotePriceMgr queryQuotePriceBySymbolId:symbolId];
    if (quoteDic && quoteDic.count > 0) {
        qdModel.n1970Time = [quoteDic[kNGmtTime] intValue];
        qdModel.nOpen = [quoteDic[kNOpen] doubleValue];
        qdModel.nHigh = [quoteDic[kNHight] doubleValue];
        qdModel.nPrice = [quoteDic[kNPrice] doubleValue];
        qdModel.symbolId = [quoteDic[kID] intValue];
    }
    NSArray *deepArr = [IXDBDeepPriceMgr queryDeepPriceBySymbolId:symbolId];
    for (int i = 0; i < deepArr.count; i++) {
        NSDictionary *deepDic = deepArr[i];
        if (deepDic && deepDic.count > 0) {
            [qdModel.BuyPrc addObject:[IXDataProcessTools dealWithNil:deepDic[kBuyPrice]]];
            [qdModel.BuyVol addObject:[IXDataProcessTools dealWithNil:deepDic[kBuyVolume]]];
            [qdModel.SellPrc addObject:[IXDataProcessTools dealWithNil:deepDic[kSellPrice]]];
            [qdModel.SellVol addObject:[IXDataProcessTools dealWithNil:deepDic[kSellVolume]]];
        }
    }
    return qdModel;
}

#pragma mark 保存昨收价
+ (BOOL)saveBatchYesterdayPrice:(NSArray *)modelArr
{
    BOOL ret = NO;
    if (modelArr && modelArr.count > 0) {
        NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
        NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
        NSTimeInterval time = [timeStamp doubleValue] + 60*60;
        for (int i = 0; i < modelArr.count; i++) {
            NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
            IXLastQuoteM *tmpModel = modelArr[i];
            if (tmpModel) {
                [tmpDic setObject:@(tmpModel.symbolId) forKey:kSymbolId];
                [tmpDic setObject:@(tmpModel.nLastClosePrice) forKey:kPrice];
                [tmpDic setObject:@(time) forKey:kDeadTimeStamp];
                [tmpArr addObject:tmpDic];
            }
        }
        if (tmpArr.count > 0) {
            ret = [IXDBYesterdayPriceMgr saveBatchYesterdayPriceInfo:tmpArr];
        }
    } else {
        return ret = NO;
    }
    return ret;
}

#pragma mark 查询昨收价
+ (IXLastQuoteM *)queryYesterdayPriceBySymbolId:(uint64_t)symbolId
{
    IXLastQuoteM *model = [[IXLastQuoteM alloc] init];
    NSDictionary *tmpDic = [IXDBYesterdayPriceMgr queryYesterdayPriceBySymbolId:symbolId];
    if (tmpDic && tmpDic.count > 0) {
        model.symbolId = [tmpDic[kID] intValue];
        model.nLastClosePrice = [tmpDic[kPrice] doubleValue];
    }
    return model;
}

+ (BOOL)dealwithAccGroupSymCata {
    
    NSArray *symArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSArray *cataArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolCataIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    NSMutableArray *tmpSymArr = [[NSMutableArray alloc] init];
    NSMutableArray *tmpCataArr =[[NSMutableArray alloc] init];
    
    //处理产品
    if (symArr && symArr.count > 0) {
        
        for (int i = 0; i < symArr.count; i++) {
            
            NSDictionary *dic = symArr[i];
            if (dic && dic.count > 0) {
                
                uint64_t symId = [[IXDataProcessTools dealWithNil:dic[kSymbolId]] intValue];
                if (symId > 0) {
                    
                    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symId];
                    if (symDic && symDic.count > 0) {
                        
                        uint64_t cataId = [[IXDataProcessTools dealWithNil:symDic[kCataId]] intValue];
                        uint64_t accGroupId = [[IXDataProcessTools dealWithNil:dic[kAccGroupId]] intValue];
                        if (cataId > 0) {
                            
                            IXAccSymCataM *model = [[IXAccSymCataM alloc] init];
                            NSDictionary *cataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:cataId];
                            uint64_t parentId = [[IXDataProcessTools dealWithNil:cataDic[kparentId]] intValue];
                            if (parentId == 0) {
                                
                                model.cataId = cataId;
                            } else {
                                
                                model.cataId = parentId;
                                model.cataIdArr = @[@(cataId)];
                            }
                            model.symbolId = symId;
                            model.accGroupId = accGroupId;
                            model.level = 1;
                            model.noDisplay = [[IXDataProcessTools dealWithNil:dic[kNoDispaly]] intValue];
                            [tmpSymArr addObject:model];
                            [tmpCataArr addObject:model];
                            
                        }
                    }
                }
            }
        }
    }
    
    //处理分类
    if (cataArr && cataArr.count > 0) {
        
        for (int i = 0; i < cataArr.count; i++) {
            
            NSDictionary *cataDic = cataArr[i];
            uint64_t cataId = [[IXDataProcessTools dealWithNil:cataDic[KSymbolCataId]] intValue];
            uint64_t accGroupId = [[IXDataProcessTools dealWithNil:cataDic[kAccGroupId]] intValue];
            if (cataId > 0) {
                
                IXAccSymCataM *model = [[IXAccSymCataM alloc] init];
                NSDictionary *cataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:cataId];
                uint64_t parentId = [[IXDataProcessTools dealWithNil:cataDic[kparentId]] intValue];
                if (parentId == 0) {
                    
                    model.cataId = cataId;
                    
                    NSArray *cata2Arr = [IXDBSymbolCataMgr querySymbolCataByParentId:cataId];
                    NSMutableArray *tmpCataArr = [[NSMutableArray alloc] init];
                    for (int i = 0; i < cata2Arr.count; i++) {
                        
                        NSDictionary *tmpDic = cata2Arr[i];
                        [tmpCataArr addObject:@([tmpDic int64ForKey:kID])];
                    }
                    if (tmpCataArr.count > 0) {
                        
                        model.cataIdArr = tmpCataArr;
                    }
                } else {
                    
                    model.cataId = parentId;
                    model.cataIdArr = @[@(cataId)];
                }
                model.accGroupId = accGroupId;
                [tmpCataArr addObject:model];
            }
        }
    }
    
    if (tmpSymArr.count == 0 && tmpCataArr.count == 0) {
        return [self saveBaseAllSymCata];
    }  else {
        
        return [IXDataProcessTools dealWithGroupSymCataBySymbolInfo:tmpSymArr symbolCataInfo:tmpCataArr];
    }
}

#pragma mark 处理账户组产品分类
+ (BOOL)dealWithGroupSymCataBySymbolInfo:(NSArray *)symArr symbolCataInfo:(NSArray *)cataArr {
    
    if (symArr.count > 0 && cataArr.count > 0) {
        
        return  [IXDataProcessTools saveGroupSymCataBySymbolInfo:symArr symbolCataInfo:cataArr];
    } else if (symArr.count > 0) {
        
        return [IXDataProcessTools saveGroupSymCataBySymbolInfo:symArr];
    } else {
        
        return [IXDataProcessTools saveGroupSymCataBySymbolCataInfo:cataArr];
    }
}

#pragma mark 处理产品以及分类权限
+ (BOOL)saveGroupSymCataBySymbolInfo:(NSArray *)symArr symbolCataInfo:(NSArray *)cataArr {
    
    BOOL symFlag = [IXDataProcessTools saveGroupSymCataBySymbolInfo:symArr];
    BOOL cataFlag = [IXDataProcessTools saveGroupSymCataBySymbolCataInfo:cataArr];
    return symFlag && cataFlag;
    
}

#pragma mark 保存产品权限
+ (BOOL)saveGroupSymCataBySymbolInfo:(NSArray *)tmpArr {
    
    NSMutableArray *symArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < tmpArr.count; i++) {
        
        IXAccSymCataM *model = tmpArr[i];
        NSMutableDictionary *symDic = [[NSMutableDictionary alloc] init];
        if (model) {
            
            if (model.cataIdArr.count > 0) {
                
                [symDic setObject:model.cataIdArr[0] forKey:kCataId];
            } else {
                
                [symDic setObject:@(model.cataId) forKey:kCataId];
            }
            
            [symDic setObject:@(model.symbolId) forKey:kSymbolId];
            [symDic setObject:@(model.accGroupId) forKey:kAccGroupId];
            [symDic setObject:@(0) forKey:kUUID];
            NSDictionary *groupSymDic = [IXDBG_SMgr queryG_SByAccountGroupId:model.accGroupId symbolId:[symDic[kSymbolId] longLongValue]];
            if (groupSymDic.count) {
                [symDic setObject:groupSymDic[kNoDispaly] forKey:kNoDispaly];
            } else {
                [symDic setObject:@(0) forKey:kNoDispaly];
            }
            [symArr addObject:symDic];
        }
    }
    if (symArr.count > 0) {
        return  [IXDBGroupSymbolMgr saveBatchGroupSymbolInfos:symArr];
    } else {
        
        return YES;
    }
}

#pragma mark 保存产品分类全量信息
+ (BOOL)saveBaseAllSymCata
{
    NSArray *allCataArr = [IXDBSymbolCataMgr querySymbolCataAllCatas];
    uint64_t accountGroupid = [IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid;
    NSMutableArray *cataArr = [[NSMutableArray alloc] init];
    NSMutableArray *tmpSymArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < allCataArr.count; i++) {
        NSDictionary *tmpDic = allCataArr[i];
        NSMutableDictionary *cataDic = [[NSMutableDictionary alloc] init];
        if (tmpDic.count) {
            [cataDic setObject:tmpDic[kID] forKey:KSymbolCataId];
            [cataDic setObject:@(accountGroupid) forKey:kAccGroupId];
            [cataDic setObject:@(0) forKey:kUUID];
            [cataDic setObject:@(0) forKey:kLevel];
            [cataArr addObject:cataDic];
            
            NSArray *symArr = [IXDBSymbolMgr querySymbolsBySymbolCataId:[cataDic[KSymbolCataId] longLongValue]];
            for (int i = 0; i < symArr.count; i++) {
                NSDictionary *symDic = symArr[i];
                if (symDic.count) {
                    NSMutableDictionary *tmpSymDic = [[NSMutableDictionary alloc] initWithDictionary:symDic];
                    NSDictionary *groupSymDic = [IXDBG_SMgr queryG_SByAccountGroupId:accountGroupid symbolId:[symDic[kID] longLongValue]];
                    if (groupSymDic.count) {
                        [tmpSymDic setObject:groupSymDic[kNoDispaly] forKey:kNoDispaly];
                    } else {
                        [tmpSymDic setObject:@(0) forKey:kNoDispaly];
                    }
                    [tmpSymArr addObject:tmpSymDic];
                }
            }
        }
    }
    
    NSMutableArray *saveSymArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < tmpSymArr.count; i++) {
        NSMutableDictionary *symDic = [[NSMutableDictionary alloc] init];
        NSDictionary *tmpDic = tmpSymArr[i];
        [symDic setObject:@([tmpDic[kCataId] longLongValue]) forKey:kCataId];
        [symDic setObject:@([tmpDic[kID] longLongValue]) forKey:kSymbolId];
        [symDic setObject:@(accountGroupid) forKey:kAccGroupId];
        [symDic setObject:@(0) forKey:kUUID];
        [symDic setObject:@([tmpDic[kNoDispaly] intValue]) forKey:kNoDispaly];
        [saveSymArr addObject:symDic];
    }
    
    BOOL ret = NO;
    if (saveSymArr.count) {
        ret = [IXDBGroupSymbolMgr saveBatchGroupSymbolInfos:saveSymArr];
        if (!ret) {
            return ret;
        }
    }
    if (cataArr.count) {
        return  [IXDBGroupSymCataMgr saveBatchGroupSymbolCatasInfos:cataArr];
    } else {
        return YES;
    }
}

#pragma mark 保存产品分类权限
+ (BOOL)saveGroupSymCataBySymbolCataInfo:(NSArray *)tmpArr {
    
    NSMutableArray *cacheSymbolArr = [NSMutableArray new];//缓存已添加的产品
    NSMutableArray *cacheCataArr = [NSMutableArray new];//缓存已添加的产品分类
    
    NSArray *agscArr = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataBySymbolIdAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid type:0];
    
    NSMutableArray *cataArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < tmpArr.count; i++) {
        
        IXAccSymCataM *model = tmpArr[i];
        if (model) {
            
            if (![cacheCataArr containsObject:@(model.cataId)]) {
                NSMutableDictionary *cataDic = [[NSMutableDictionary alloc] init];
                [cataDic setObject:@(model.cataId) forKey:KSymbolCataId];
                [cataDic setObject:@(model.accGroupId) forKey:kAccGroupId];
                [cataDic setObject:@(0) forKey:kUUID];
                [cataDic setObject:@(model.level) forKey:kLevel];
                [cataArr addObject:cataDic];
                [cacheCataArr addObject:@(model.cataId)];
            }
            if (model.cataIdArr.count > 0) {
                
                for (int j = 0; j < model.cataIdArr.count; j++) {
                    
                    if (![cacheCataArr containsObject:model.cataIdArr[j]]) {
                        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                        [dic setObject:model.cataIdArr[j] forKey:KSymbolCataId];
                        [dic setObject:@(model.accGroupId) forKey:kAccGroupId];
                        [dic setObject:@(0) forKey:kUUID];
                        [dic setObject:@(model.level) forKey:kLevel];
                        [cataArr addObject:dic];
                        [cacheCataArr addObject:model.cataIdArr[j]];
                    }
                    NSMutableArray *tmpSymArr = [[NSMutableArray alloc] init];
                    if (model.level == 1) {
                        
                        for (int k = 0; k < agscArr.count; k++) {
                            
                            NSDictionary *dic = agscArr[k];
                            uint64_t symbolId = [dic int64ForKey:kSymbolId];
                            uint64_t cataId = [dic  int64ForKey:kCataId];
                            if (symbolId > 0 && cataId == [model.cataIdArr[j] longValue]) {
                                
                                NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
                                if (symDic ) {
                                    
                                    [tmpSymArr addObject:symDic];
                                }
                            }
                            
                        }
                    } else {
                        
                        [tmpSymArr addObjectsFromArray: [IXDBSymbolMgr querySymbolsBySymbolCataId:[model.cataIdArr[j] longValue]]];
                    }                    NSMutableArray *saveSymArr = [[NSMutableArray alloc] init];
                    for (int m = 0; m < tmpSymArr.count; m++) {
                        
                        NSMutableDictionary *symDic = [[NSMutableDictionary alloc] init];
                        NSDictionary *tmpDic = tmpSymArr[m];
                        [symDic setObject:@([model.cataIdArr[j] longValue]) forKey:kCataId];
                        [symDic setObject:@([tmpDic int64ForKey:kID]) forKey:kSymbolId];
                        [symDic setObject:@(model.accGroupId) forKey:kAccGroupId];
                        [symDic setObject:@(0) forKey:kUUID];
                        NSDictionary *groupSymDic = [IXDBG_SMgr queryG_SByAccountGroupId:model.accGroupId symbolId:[symDic[kSymbolId] longLongValue]];
                        if (groupSymDic.count) {
                            [symDic setObject:groupSymDic[kNoDispaly] forKey:kNoDispaly];
                        } else {
                            [symDic setObject:@(0) forKey:kNoDispaly];
                        }
                        [saveSymArr addObject:symDic];
                    }
                    if (saveSymArr.count > 0) {
                        
                        [IXDBGroupSymbolMgr saveBatchGroupSymbolInfos:saveSymArr];
                    }
                }
            } else {
                
                
                NSMutableArray *tmpSymArr = [[NSMutableArray alloc] init];
                if (model.level == 1) {
                    
                    for (int k = 0; k < agscArr.count; k++) {
                        
                        NSDictionary *dic = agscArr[k];
                        uint64_t symbolId = [dic int64ForKey:kSymbolId];
                        uint64_t cataId = [dic  int64ForKey:kCataId];
                        if (![cacheSymbolArr containsObject:@(symbolId)]) {
                            if (symbolId > 0 && cataId > 0 && cataId == model.cataId) {
                                
                                NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
                                if (symDic ) {
                                    [tmpSymArr addObject:symDic];
                                }
                                [cacheSymbolArr addObject:@(symbolId)];
                            }
                        }
                    }
                } else {
                    
                    [tmpSymArr addObjectsFromArray:[IXDBSymbolMgr querySymbolsBySymbolCataId:model.cataId]];
                }
                if (tmpSymArr.count > 0) {
                    
                    NSMutableArray *saveSymArr = [[NSMutableArray alloc] init];
                    for (int m = 0; m < tmpSymArr.count; m++) {
                        
                        NSMutableDictionary *symDic = [[NSMutableDictionary alloc] init];
                        NSDictionary *tmpDic = tmpSymArr[m];
                        [symDic setObject:@([tmpDic int64ForKey:kCataId]) forKey:kCataId];
                        [symDic setObject:@([tmpDic int64ForKey:kID]) forKey:kSymbolId];
                        [symDic setObject:@(model.accGroupId) forKey:kAccGroupId];
                        [symDic setObject:@(0) forKey:kUUID];
                        NSDictionary *groupSymDic = [IXDBG_SMgr queryG_SByAccountGroupId:model.accGroupId symbolId:[symDic[kSymbolId] longLongValue]];
                        if (groupSymDic.count) {
                            [symDic setObject:groupSymDic[kNoDispaly] forKey:kNoDispaly];
                        } else {
                            [symDic setObject:@(0) forKey:kNoDispaly];
                        }
                        [saveSymArr addObject:symDic];
                    }
                    if (saveSymArr.count > 0) {
                        [IXDBGroupSymbolMgr saveBatchGroupSymbolInfos:saveSymArr];
                    }
                }
            }
        }
    }
    if (cataArr.count > 0) {
        return  [IXDBGroupSymCataMgr saveBatchGroupSymbolCatasInfos:cataArr];
    } else {
        return YES;
    }
}

+ (NSString *)dealWithNil:(NSString *)str
{
    NSString *retStr = [[NSString alloc] init];
    if (!str || str.length == 0) {
        retStr = @"";
    } else {
        retStr = str;
    }
    return retStr;
}

#pragma mark  去除"-"字符
+ (NSString *)dealWithWhiffletree:(NSString *)str
{
    if (!str || ![str isKindOfClass:[NSString class]] || [(NSString *)str length] == 0) {
        return str;
    } else {
        return [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
}

#pragma mark 处理设备名称
+ (NSString *)dealWithDeviceName:(NSString *)devName
{
    if (!devName || devName.length == 0 || devName.length <= 32 ) {
        return devName;
    } else {
        return [devName substringToIndex:32];
    }
}

#pragma mark  处理数据库单引号转义
+ (id)dealWithSqlSingleQuote:(id)str
{
    if (!str || ![str isKindOfClass:[NSString class]] || [(NSString *)str length] == 0) {
        
        return str;
    } else {
        return [str stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    }
}

#pragma mark 获取产品保证金
+ (NSArray *)getMarginSet:(IXSymbolM *)model
{
    NSString *marginType = nil;
    NSArray *marginArr = nil;
    //判断是否假期
    item_holiday *holiday = [IXDataProcessTools queryIsHolidayBySymbol:model tradable:YES];
    if (holiday) {
        
        /*1、item_group_symbol和item_group_symbol_cata */
        marginArr = [IXDataProcessTools queryGroupSymOrSymCataMargin:model groupMarginType:IXGroupMarginTypeHoliday];
        if (marginArr && marginArr.count > 0) {
            return marginArr;
        }
        
        /*2、item_holiday */
        NSDictionary *holidayDic = [IXDBHolidayMgr queryHolidayByHolidayId:holiday.id_p];
        marginType = holidayDic[kMarginType];
        if (marginType) {
            marginArr = [IXDataProcessTools getMarginByMarginType:marginType];
            if (marginArr && marginArr.count > 0) {
                return marginArr;
            }
        }
        
        /*3、item_holiday_cata */
        NSDictionary *holidayCataDic = [IXDBHolidayCataMgr queryHolidayCataByHolidayCataId:model.holidayCataId];
        marginType = holidayCataDic[kMarginType];
        if (marginType) {
            marginArr = [IXDataProcessTools getMarginByMarginType:marginType];
            if (marginArr && marginArr.count > 0) {
                return marginArr;
            }
        }
    }
    
    //判断是否交易时间
    item_schedule *schedule = [IXDataProcessTools queryScheduleBySymbol:model];
    if (schedule) {
        
        /*4、item_group_symbol和item_group_symbol_cata */
        marginArr = [IXDataProcessTools queryGroupSymOrSymCataMargin:model groupMarginType:IXGroupMarginTypeNormal];
        if (marginArr && marginArr.count > 0) {
            return marginArr;
        }
        
        /*5、item_schedule */
        NSDictionary *scheduleDic = [IXDBScheduleMgr queryScheduleByScheduleId:schedule.id_p];
        marginType = scheduleDic[kMarginType];
        if (marginType) {
            marginArr = [IXDataProcessTools getMarginByMarginType:marginType];
            if (marginArr && marginArr.count > 0) {
                return marginArr;
            }
        }
        /*6、item_schedule_cata */
        NSDictionary *scheduleCataDic = [IXDBScheduleCataMgr queryScheduleCataByScheduleCataId:model.scheduleCataId];
        marginType = scheduleCataDic[kMarginType];
        if (marginType) {
            marginArr = [IXDataProcessTools getMarginByMarginType:marginType];
            if (marginArr && marginArr.count > 0) {
                return marginArr;
            }
        }
    }
    
    //判断是否周末
    if ([IXDataProcessTools isWeeked:model]) {
        /*7、item_group_symbol和item_group_symbol_cata */
        marginArr = [IXDataProcessTools queryGroupSymOrSymCataMargin:model groupMarginType:IXGroupMarginTypeWeekend];
        if (marginArr && marginArr.count > 0) {
            return marginArr;
        }
    }
    
    /*8、item_symbol */
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:model.id_p];
    marginType = symDic[kMarginType];
    if (marginType) {
        marginArr = [IXDataProcessTools getMarginByMarginType:marginType];
        if (marginArr && marginArr.count > 0) {
            return marginArr;
        }
    }
    return nil;
}

+ (NSArray *)getMarginSetNew:(uint64_t)symbolId
{
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
    IXSymbolM *model = [IXDataProcessTools symbolModelBySymbolInfo:symDic];
    return [self getMarginSet:model];
}

#pragma mark 判断是否周末
+ (BOOL)isWeeked:(IXSymbolM *)model
{
    NSDateComponents *components = [IXDateUtils currentTimeComponents];
    NSInteger weekday_now = (components.weekday + 7)%8;
    int wd = weekday_now < 3 ? weekday_now + 7 : weekday_now;
    int tnow = components.hour*60 + components.minute + wd*1440;
    int min_db = 0;
    int max_db = 0;
    int delayTime = 0;
    
    //冬令时延迟交易时间(优先查询symbol表、然后再找symbolCata表)
    delayTime = model.scheduleDelayMinutes;
    if (delayTime == 0) {
        NSDictionary *symCataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:model.cataId];
        if (symCataDic) {
            delayTime = [symCataDic[kScheduleDelayMinutes] integerValue];
        }
    }
    
    //查询scheduleCata是否有效
    NSDictionary *scheduleCataDic = [IXDBScheduleCataMgr queryScheduleCataByScheduleCataId:model.scheduleCataId];
    NSArray *scheduleArr = nil;
    if (scheduleCataDic) {
        scheduleArr = [IXDBScheduleMgr queryScheduleByScheduleCataId:model.scheduleCataId];
        //拆分跨前一天和后一天
        scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr];
        if (delayTime) {
            scheduleArr = [IXDataProcessTools splitSchedule:scheduleArr withScheduleDelayMinutes:delayTime];
        }
    }
    for (int i = 0; i < scheduleArr.count; i++) {
        NSDictionary *scheduleDic = scheduleArr[i];
        int start_time_db = [scheduleDic[kStartTime] intValue];
        int end_time_db = [scheduleDic[kEndTime] intValue];
        int weekday_db = [scheduleDic[kDayOfWeek] intValue];
        if (weekday_db < 3) {
            weekday_db += 7;
        }
        if (weekday_db > 3 && weekday_db < 7) {
            int max = end_time_db;
            if (weekday_db == 4) {
                max -= 1440;
            }
            if (weekday_db == 6) {
                max += 1440;
            }
            if (max_db < max) {
                max_db = max;
            }
        }
        if (weekday_db > 6 && weekday_db < 10) {
            int min = start_time_db;
            if (weekday_db == 7) {
                min -= 1440;
            }
            if (weekday_db == 9) {
                min += 1440;
            }
            if (min_db > min) {
                min_db = min;
            }
        }
    }
    min_db += 8*1440;
    max_db += 5*1440;
    if (tnow > max_db && tnow < min_db) {
        return YES;
    } else {
        return NO;
    }
}



#pragma mark 服务器保证金拆分权限表（保证金查询新表）
+ (NSArray *)queryGroupSymOrSymCataMargin:(IXSymbolM *)model
                          groupMarginType:(IXGroupMarginType)type
{
    NSString *marginType = nil;
    NSArray *marginArr = nil;
    NSDictionary *groupSymDic = [IXDBG_SMgr queryG_SByAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid symbolId:model.id_p];
    marginType = [IXDataProcessTools dealWithQueryMarginType:groupSymDic groupMarginType:type];
    if (marginType) {
        marginArr = [IXDataProcessTools getMarginByMarginType:marginType];
        if (marginArr && marginArr.count > 0) {
            return marginArr;
        }
    }
    
    NSDictionary *groupSymCataDic = [IXDBG_S_CataMgr queryG_S_CataByAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid symbolCataId:model.cataId];
    marginType = [IXDataProcessTools dealWithQueryMarginType:groupSymCataDic groupMarginType:type];
    if (marginType) {
        marginArr = [IXDataProcessTools getMarginByMarginType:marginType];
        if (marginArr && marginArr.count > 0) {
            return marginArr;
        }
    }
    return marginArr;
}

#pragma mark 根据当前时间状态查询保证金类型
+ (NSString *)dealWithQueryMarginType:(NSDictionary *)infoDic
                      groupMarginType:(IXGroupMarginType)type
{
    NSString *marginType = nil;
    switch (type) {
        case IXGroupMarginTypeNormal:{
            marginType = infoDic[kNormalMarginType];
        }
            break;
        case IXGroupMarginTypeWeekend:{
            marginType = infoDic[kWeekendMarginType];
        }
            break;
        case IXGroupMarginTypeHoliday:{
            marginType = infoDic[kHolidayMarginType];
        }
            break;
        default:
            break;
    }
    return marginType;
}

+ (NSArray *)getMarginByMarginType:(NSString *)marginType
{
    return [IXDBMarginSetMgr queryMarginSetByMarginType:marginType];
}

+ (NSString *)getProvinceName:(IXProvinceM *)model
{
    if ( [BoLanKey isEqualToString:NAMEEN] ) {
        return  model.nameEN;
    }else if ( [BoLanKey isEqualToString:NAMECN] )
        return model.nameCN;
    return model.nameTW;
}

+ (NSString *)getCityName:(IXCityM *)cModel{
    if ( [BoLanKey isEqualToString:NAMEEN] ) {
        return  cModel.nameEN;
    }else if ( [BoLanKey isEqualToString:NAMECN] )
        return cModel.nameCN;
    return cModel.nameTW;
}

#warning 待完善
+ (NSString *)orderTypeByType:(int32_t)type
{
    NSString *retStr = nil;
    switch (type) {
        case 1:
            retStr = MARKETORDER;
            break;
        case 2:
            retStr = LIMITORDER;
            break;
        case 3:
            retStr = STOPORDER;
            break;
        case 4:
            retStr = LocalizedString(@"平仓");
            break;
        case 5:
            retStr = LocalizedString(@"止损单");
            break;
        case 6:
            retStr = LocalizedString(@"止盈单");
            break;
        case 7:
            retStr = LocalizedString(@"一键平仓");
            break;
        default:
            retStr = @"--";
            break;
    }
    return retStr;
}

+ (NSString *)formatterPhoneDisplay:(NSString *)phone
{
    if(phone.length == 0){
        return @"";
    }
    NSString *retStr = phone;
    if ( phone.length > 4 ) {
        NSString *lowStr = [phone substringFromIndex:(phone.length - 4)];
        NSString *highStr = [phone substringToIndex:3];
        retStr = [NSString stringWithFormat:@"%@****%@",highStr,lowStr];
    }
    return retStr;
}

+ (NSString *)formatterEmailDisplay:(NSString *)email
{
    if(email.length == 0){
        return @"";
    }
    NSString *retStr = email;
    NSArray *eArr = [retStr componentsSeparatedByString:@"@"];
    if (eArr.count > 1) {
        if (((NSString *)eArr[0]).length > 3) {
            NSString *highStr = [eArr[0] substringToIndex:3];
            retStr = [NSString stringWithFormat:@"%@****@%@",highStr,eArr[1]];
        }
    }
    return retStr;
}

+ (NSString *)formatterBankCardNoDisplay:(NSString *)cardNo
{
    if (cardNo.length < 5) {
        return cardNo;
    }
    return [NSString stringWithFormat:@"●●●● ●●●● ●●●● %@",[cardNo substringFromIndex:cardNo.length - 4]];
}

#pragma mark 金钱格式显示逗号和符号
+ (NSString *)moneyFormatterComma:(double)value
               positiveNumberSign:(BOOL)hidden
{
    NSString *valueStr = [NSString stringWithFormat:@"%.2f",value];
    double val = [valueStr doubleValue];
    if (val > 0 ) {
        if (hidden) {
            return [IXAppUtil amountToString:[valueStr stringByReplacingOccurrencesOfString:@"+" withString:@""]];
        } else {
            if (val >= 0.01) {
                return [NSString stringWithFormat:@"+%@",[IXAppUtil amountToString:[valueStr stringByReplacingOccurrencesOfString:@"+" withString:@""]]];
            } else {
                return [IXAppUtil amountToString:[valueStr stringByReplacingOccurrencesOfString:@"+" withString:@""]];
            }
        }
    } else if (val == 0) {
        valueStr = [valueStr stringByReplacingOccurrencesOfString:@"+" withString:@""];
        valueStr = [valueStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        return [IXAppUtil amountToString:valueStr];
    } else {
        if (val <= -0.01 ) {
           return [NSString stringWithFormat:@"-%@",[IXAppUtil amountToString:[valueStr stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
        } else {
            return [IXAppUtil amountToString:[valueStr stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        }
    }
}

#pragma mark asciiCode 转字母
+ (NSString *)asciiCodeToString:(int)asciiCode
{
    NSString *ret = [NSString stringWithFormat:@"%c",(asciiCode + 65)];
    return ret;
}

+ (BOOL)isContainString:(NSString *)str withSet:(NSArray *)arr
{
    if (arr.count && str.length) {
        return [arr containsObject:str];
    } else {
        return NO;
    }
}

+ (int)stringToAsciiCode:(NSString *)str
{
    int ret = 0;
    if (str.length) {
        ret = [str characterAtIndex:0] - 65;
    }
    return ret;
}

+ (NSInteger)dealWithTextNumberLine:(NSString *)text
                          withWidth:(CGFloat)width
                               font:(UIFont*)font
{
    CGSize size = [self sizeWithString:text font:font width:width];
    CGSize size1 = [self sizeWithString:@"Hello" font:font width:width];
    return size.height/size1.height;
}

+ (CGSize)sizeWithString:(NSString*)string
                    font:(UIFont*)font
                   width:(float)width
{
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine |   NSStringDrawingUsesFontLeading    |NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    return rect.size;
}

+ (UIImage *)getLaunchImage
{
    CGSize viewSize = kScreenBound.size;
    // 竖屏
    NSString *viewOrientation = @"Portrait";
    NSString *launImgName = nil;
    NSArray* imagesDict = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for (NSDictionary* dict in imagesDict)
    {
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]])
        {
            launImgName = dict[@"UILaunchImageName"];
        }
    }
    return [UIImage imageNamed:launImgName];
}

+ (NSArray *)uniqData:(NSArray *)arr
{
    NSMutableArray *tmpArr = [NSMutableArray new];
    for (int i = 0; i < arr.count; i++) {
        NSDictionary *dic = arr[i];
        BOOL isExist = NO;
        for (int j = 0; j < tmpArr.count; j++) {
            NSDictionary *tmpDic = tmpArr[j];
            if (SameString(dic[kCurrency], tmpDic[kCurrency]) &&
                [dic[kType] intValue] == [tmpDic[kType] intValue]) {
                isExist = YES;
                break;
            }
        }
        if (!isExist) {
            [tmpArr addObject:dic];
        }
    }
    return tmpArr;
}

+ (NSString *)currentCountryCode
{
    return [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
}

#pragma mark 绑定类型标题
+ (NSString *)bindTypeTitle
{
    if ([[IXUserInfoMgr shareInstance].userLogInfo.user.loginName containsString:@"@"]) {
        return LocalizedString(@"手机号码");
    } else {
        return LocalizedString(@"电子邮箱");
    }
}

#pragma mark 初始化数据库
+ (void)initDataBase
{
    NSString *dbPath = [IXDataProcessTools documentDirectory];
    dbPath = [dbPath stringByAppendingPathComponent:kDataBaseName];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:dbPath]) {
        NSDictionary *verDic = [IXDBVersionMgr queryDBVersion];
        if (verDic && verDic.count > 0) {
            long oldVer = [verDic[kDBVersion] longLongValue];
            NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
            version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
            long newVer = [version longLongValue];
            if (newVer > oldVer) {
                [IXDataProcessTools copyfilePath:dbPath isUpdate:YES];
            }
        } else {
            [IXDataProcessTools copyfilePath:dbPath isUpdate:NO];
        }
    } else {
        [IXDataProcessTools copyfilePath:dbPath isUpdate:NO];
    }
}

#pragma mark 处理数据库文件路径拷贝
+ (void)copyfilePath:(NSString *)dbPath isUpdate:(BOOL)isUpdate
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *rsDbPath = [[NSBundle mainBundle] pathForResource:kPlatformPrefix ofType:kDataBaseSuffix];
    if ([fm fileExistsAtPath:rsDbPath]) {
        BOOL ret = NO;
        if (isUpdate && dbPath.length) {
            ret = [fm removeItemAtPath:dbPath error:nil];
        } else {
            ret = YES;
        }
        if (ret && [fm copyItemAtPath:rsDbPath toPath:dbPath error:nil]) {
            [IXDBManager sharedInstance];
        } else {
            if (![fm createFileAtPath:dbPath contents:nil attributes:nil]) {
                DLog(@"create db fail");
            } else {
                [IXDBManager sharedInstance];
                [IXDataProcessTools createDynamicTable];
            }
        }
    } else {
        if (![fm createFileAtPath:dbPath contents:nil attributes:nil]) {
            DLog(@"create db fail");
        } else {
            [IXDBManager sharedInstance];
            [IXDataProcessTools createDynamicTable];
        }
    }
}

#pragma mark 创建动态空表
+ (void)createDynamicTable
{
    [IXDBAccountMgr saveAccountInfo:nil userId:0];
    [IXDBHolidayCataMgr saveHolidayCataInfo:nil];
    [IXDBHolidayMgr saveHolidayInfo:nil];
    [IXDBSymbolCataMgr saveSymbolCataInfo:nil];
    [IXDBGroupSymCataMgr saveGroupSymbolCataInfo:nil];
    [IXDBSymbolHotMgr saveSymbolHotInfo:nil];
    [IXDBSymbolMgr saveSymbolInfo:nil];
    [IXDBGroupSymbolMgr saveGroupSymbolInfo:nil];
    [IXDBSymbolSubMgr saveSymbolSubInfo:nil accountId:0];
    [IXDBSymbolLableMgr saveSymbolLableInfo:nil];
    [IXDBCompanyMgr saveCompanyInfo:nil];
    [IXDBScheduleCataMgr saveScheduleCataInfo:nil];
    [IXDBScheduleMgr saveScheduleInfo:nil];
    [IXDBLanguageMgr saveLanguageInfo:nil];
    [IXDBAccountGroupMgr saveAccountGroupInfo:nil];
    [IXDBAccountGroupSymCataMgr saveAccountGroupSymCataInfo:nil];
    [IXDBMarginSetMgr saveMarginSetInfo:nil];
    [IXDBSearchBrowseMgr saveSearchBrowserHistoryInfoSymbolId:nil userId:0 accountId:0];
    [IXDBHolidayMarginMgr saveHolidayMarginInfo:nil];
    [IXDBScheduleMarginMgr saveScheduleMarginInfo:nil];
    [IXDBEodTimeMgr saveEodTimeInfo:nil];
    [IXDBQuoteDelayMgr saveQuoteDelayInfo:nil];
    [IXDBYesterdayPriceMgr saveYesterdayPriceInfo:nil];
    [IXQuotePriceMgr saveQuotePriceInfo:nil];
    [IXDBDeepPriceMgr saveDeepPriceInfo:nil];
    [IXDataProcessTools saveDBVersion];
    [IXDBG_SMgr saveG_SInfo:nil];
    [IXDBG_S_CataMgr saveG_S_CataInfo:nil];
    [IXDBLpchaccMgr saveLpchaccInfo:nil];
    [IXDBLpchaccSymbolMgr saveLpchaccSymbolInfo:nil];
    [IXDBLpchannelMgr saveLpchannelInfo:nil];
    [IXDBLpchannelSymbolMgr saveLpchannelSymbolInfo:nil];
    [IXDBLpibBindMgr saveLpibBindInfo:nil];
}

+ (BOOL)clearDBCache
{
    BOOL ret = NO;
    NSString *dbPath = [IXDataProcessTools documentDirectory];
    dbPath = [dbPath stringByAppendingPathComponent:kDataBaseName];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:dbPath]) {
        if ([fm removeItemAtPath:dbPath error:nil]) {
            ret = YES;
            NSString *rsDbPath = [[NSBundle mainBundle] pathForResource:kPlatformPrefix ofType:kDataBaseSuffix];
            if ([fm fileExistsAtPath:rsDbPath]) {
                if ([fm copyItemAtPath:rsDbPath toPath:dbPath error:nil]) {
                    [IXDBManager sharedInstance];
                } else {
                    DLog(@"db copy fail");
                }
            } else {
                DLog(@"resource db is not exist");
            }
        } else {
            DLog(@"db delete fail");
        }
    } else {
        DLog(@"db is not exist");
        if ([fm createFileAtPath:dbPath contents:nil attributes:nil]) {
            [IXDBManager sharedInstance];
        } else {
            DLog(@"create db fail");
        }
    }
    return ret;
}

+ (BOOL)saveDBVersion
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSDictionary *dic = @{
                          kDBVersion:version
                          };
    return [IXDBVersionMgr saveDBVersionInfo:dic];
}

// 字典转json字符串方法
+ (NSString *)convertToJsonData:(NSDictionary *)dict
{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString;
    if (!jsonData) {
        NSLog(@"%@",error);
    }else{
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    return mutStr;
}

// Json 转 Dic
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}



@end

