//
//  IXTraceWebV.h
//  IXApp
//
//  Created by Evn on 2018/4/4.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <WebKit/WebKit.h>

typedef NS_ENUM(NSInteger ,TraceWebUrlType) {
    TraceWebUrlTypeTouchRegist,
    TraceWebUrlTypeRegistSuccess,
    TraceWebUrlTypeTouchIncash,
    TraceWebUrlTypeIncashSuccess
};
@interface IXTraceWebV : WKWebView

+ (instancetype)initWithWebUrlType:(TraceWebUrlType)type;
- (void)refreshWebUrlType:(TraceWebUrlType)type;

@end
