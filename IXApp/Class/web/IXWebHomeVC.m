//
//  IXWebHomeVC.m
//  IXApp
//
//  Created by Seven on 2017/5/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXWebHomeVC.h"
#import <WebKit/WebKit.h>
#import "IXNoResultView.h"
#import "IXLeftNavView.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "IXBORequestMgr.h"
#import "IXCpyConfig.h"
#import "IXJSHelper.h"
#import "AppDelegate+UI.h"
#import "IXIncashStep1VC.h"
#import "IXDPSChannelVC.h"
#import "ChatTableViewController.h"
#import "IXWebCustomerSeviceVC.h"
#import "IXSmybolDetailMgr.h"
#import "IXSymbolDetailNewVC.h"
#import "IXQuoteDistribute.h"
#import "GTUdpSocketManager.h"
#import "IXBORequestMgr+Asset.h"
#import "IXAccountBalanceModel.h"
#import "IXUserMgr.h"
#import "IXCommonWebVC.h"
#import "IXAlertVC.h"
#import "IXPhoneRegistVC.h"
#import "IXLoginMainVC.h"
#import "MJRefresh.h"
#define titleViewFrame CGRectMake(0, 0, kScreenWidth - 150, 40)

@interface IXWebHomeVC ()
<
WKUIDelegate,
WKNavigationDelegate,
JSHelperDelegate,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong) WKWebView         * webView;
@property (nonatomic, strong) IXJSHelper        * jsHelper;
@property (nonatomic, strong) UIProgressView    * progressV;
@property (nonatomic, strong) IXNoResultView    * alertView;
@property (nonatomic, strong) UILabel           * titleLab;
@property (nonatomic, strong) IXLeftNavView     * leftNavItem;
@property (nonatomic, copy) NSString    * metaUrl;
@property (nonatomic, copy) NSString    * mainUrl;
@property (nonatomic, strong) dispatch_source_t timer;
@property(nonatomic,assign)NSInteger totalTime;
@end

@implementation IXWebHomeVC

- (id)init
{
    self = [super init];
    if ( self ) {
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    [_webView removeObserver:self forKeyPath:@"title"];
    [_webView removeObserver:self forKeyPath:@"loading"];
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"ixMarketPage"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self resetHeadImage];
    [self refreshLeftNavItem];
}

- (void)reloadVC{
    NSString *webHomeUrl = @"http://m.gwfxglobal.com/app/en/index.html"; // PRD
    webHomeUrl = @"http://www.gwfxglobal.com/app/en/index.html";
    if ([CompanyName containsString:@"UAT"]) {
        webHomeUrl = @"http://testm.gwfxglobal.com/app/en/index.html";  // UAT
    }
    
    NSString *userType = [IXDataProcessTools showCurrentAccountType];
    NSArray *arr = @[LocalizedString(@"游客"),LocalizedString(@"模拟"),LocalizedString(@"真实")];
    webHomeUrl =[webHomeUrl stringByAppendingFormat:@"?isLoginUser=%@",@([arr indexOfObject:userType])];
    _mainUrl = webHomeUrl;
    _metaUrl = webHomeUrl;
    
    NSURL       * url = [NSURL URLWithString:webHomeUrl];
    NSURLRequest    * request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
    
    [self refreshLeftNavItem];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = self.titleLab;
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];
    
    self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightItemWithImg:GET_IMAGE_NAME(@"web_customService")
                                                                       target:self
                                                                          sel:@selector(rightItemClicked)
                                                                     aimWidth:55];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSForegroundColorAttributeName: MarketSymbolNameColor,
                                                                    NSFontAttributeName :PF_MEDI(15)
                                                                    };
    
    [self addWebView];
    [self.view addSubview:self.progressV];
    
    [IXBORequestMgr b_getBannerAdvertisementList:@"APPHOME_ADVERTISEMENT" rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        NSLog(@"%@",obj);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDeposit) name:NOTI_GOTO_DPSChannelVC object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appInvite) name:NOTI_WEBHOME_GOTO_INVITE object:nil];
    
    
    
    //清空缓存
    [self cleanCookies];
    
    [self reloadVC];
 
}

#pragma mark 初始化视图数据
- (void)resetAccountType
{
    self.leftNavItem.typeLab.text = [IXDataProcessTools showCurrentAccountType];
    [self reloadVC];
}

- (void)addWebView
{
    [self.view addSubview:self.webView];
    
    [self.webView addObserver:self forKeyPath:@"title"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self forKeyPath:@"loading"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
}

- (void)cleanCookies
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 9.0) {
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,
                                                                     NSUserDomainMask,
                                                                     YES)
                                 objectAtIndex:0];
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        NSError *errors = nil;
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
    }else{
        WKWebsiteDataStore *dateStore = [WKWebsiteDataStore defaultDataStore];
        [dateStore fetchDataRecordsOfTypes:[WKWebsiteDataStore allWebsiteDataTypes] completionHandler:^(NSArray<WKWebsiteDataRecord *> * __nonnull records) {
            for (WKWebsiteDataRecord *record  in records)
            {
                [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:record.dataTypes
                                                          forDataRecords:@[record]
                                                       completionHandler:^{}];
            }
        }];
    }
}

#pragma mark -
#pragma mark - IXJSHelper delegate
- (void)IXJSHelperPerformSelecter:(SEL)sel param:(id)param
{
    if ([self respondsToSelector:sel]) {
        [self performSelector:sel withObject:param afterDelay:0];
    }
}

- (void)appDemoTrade{
    MarkEvent(@"首页--点击模拟交易-按钮");
    NSString *accountType = [IXDataProcessTools showCurrentAccountType];
    if (SameString(accountType, LocalizedString(@"模拟"))) {
        // 切换真实账户
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_ROOT_GOTO_MARKET object:nil];
    }else{
        // 真实
        [[IXUserMgr sharedIXUserMgr] switchToDemoAccount:5];
    }
}

- (void)appRegist
{
    MarkEvent(@"首页--点击注册--按钮");
    // 注册
    [IXUserInfoMgr shareInstance].isCloseDemo = YES;
    [AppDelegate showRegist];
}

- (void)appDeposit
{
    // 入金w
    NSString *accountType = [IXDataProcessTools showCurrentAccountType];
    if (SameString(accountType, LocalizedString(@"游客"))) {
        // 引导注册
        [self showRegistLoginAlert];
    }else if (SameString(accountType, LocalizedString(@"模拟"))) {
        // 切换真实账户
        [[IXUserMgr sharedIXUserMgr] switchToRealAccount:2];
    }else{
       // 真实
        if ([IXDataProcessTools getCurrentAccountType] != item_account_group_etype_Virtuals) {
            IXDPSChannelVC *vc = [IXDPSChannelVC new];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

// 产品详情
- (void)appSymbolDetail:(id)param{
    if (!param[@"symbolId"]) {
        return;
    }
    NSNumber *symbolId = param[@"symbolId"];
    [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolId = [symbolId stringValue];
    IXSymbolDetailNewVC *vc = [IXSymbolDetailNewVC new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)appCustomerService{
    // Native客服中心
    IXWebCustomerSeviceVC *vc = [IXWebCustomerSeviceVC new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)appInvite{
//    if (![IXBORequestMgr shareInstance].userInfo.gts2CustomerId) {
//        [self showRegistLoginAlert];
//        [IXBORequestMgr shareInstance].launchHappenDic[@"Invite"] = @(YES);
//        return;
//    }
//    NSString *userType = [IXDataProcessTools showCurrentAccountType];
//    NSArray *arr = @[LocalizedString(@"游客"),LocalizedString(@"模拟"),LocalizedString(@"真实")];
    NSString *url =@"";
    
    time_t time1 = time(NULL);
    if ([CompanyName containsString:@"UAT"]) {
        url = [NSString stringWithFormat:@"http://192.168.35.198:10276/activity/invite/friends.html?companyId=B&customerId=%@&customerNumber=%@&time=%@&from=%@",[IXBORequestMgr shareInstance].gts2CustomerId,[IXBORequestMgr shareInstance].customerNo,@(time1),@"banner"];
    }else{
        url = [NSString stringWithFormat:@"https://deposit.gwfxglobal.com/activity/invite/friends.html?companyId=RVT&customerId=%@&customerNumber=%@&time=%@&from=%@",[IXBORequestMgr shareInstance].gts2CustomerId,[IXBORequestMgr shareInstance].customerNo,@(time1),@"banner"];
    }
    NSString *userType = [IXDataProcessTools showCurrentAccountType];
    NSArray *arr = @[LocalizedString(@"游客"),LocalizedString(@"模拟"),LocalizedString(@"真实")];
    url =[url stringByAppendingFormat:@"&isLoginUser=%@",@([arr indexOfObject:userType])];
    
    IXCommonWebVC *vc = [IXCommonWebVC new];
    vc.url = url;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -
#pragma mark - btn action

- (void)backAction
{
    if (self.webView.canGoBack) {
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)leftItemClicked
{
    if ([IXUserInfoMgr shareInstance].isDemeLogin) {
        [self showRegistLoginAlert];
    } else {
        [[AppDelegate getRootVC] toggleLeftView];
    }
}

- (void)rightItemClicked
{
//    [self.webView reload];
//    [self showLoadFailedView:NO];
    [self appCustomerService];
}

- (void)formerPageAction
{
    if (_metaUrl.length && [_metaUrl containsString:@"http"]) {
        NSURL   * url = [NSURL URLWithString:_metaUrl];
        NSURLRequest    * request = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
        //显示头像
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];
    }else if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

#pragma mark -
#pragma mark - UIDelegate

//web页面探出警告框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptAlertPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(void))completionHandler
{
    completionHandler();
     DLog(@" -- web view -- %s",__func__);
}

//web页面弹出确认框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptConfirmPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(BOOL result))completionHandler
{
    completionHandler(NO);
     DLog(@" -- web view -- %s",__func__);
}

//web页面弹出输入框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt
    defaultText:(nullable NSString *)defaultText
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(NSString * _Nullable result))completionHandler
{
    completionHandler(@"input text");
     DLog(@" -- web view -- %s",__func__);
}


#pragma mark -
#pragma mark - navigation delegate

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"webView---didStartProvisionalNavigation__%@",webView.URL);
    
    [self startGCDTimer];
}

- (void)webView:(WKWebView *)webView
decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if (!navigationAction.navigationType) {
        [webView loadRequest:navigationAction.request];
    }

    decisionHandler(WKNavigationActionPolicyAllow);
    if (![navigationAction.request.URL.absoluteString containsString:@"bid.g.doubleclick.net"]) {
        _metaUrl = navigationAction.request.URL.absoluteString;
        [self refreshLeftNavItem];
    }
}

-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    // 融合页
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

- (void)webView:(WKWebView *)webView
decidePolicyForNavigationResponse:(nonnull WKNavigationResponse *)navigationResponse
decisionHandler:(nonnull void (^)(WKNavigationResponsePolicy))decisionHandler
{
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView
didFailNavigation:(WKNavigation *)navigation
      withError:(NSError *)error
{
    [self refreshLeftNavItem];
}

//加载完成时调用
- (void)webView:(WKWebView *)webView
didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    [self refreshLeftNavItem];
    
    NSString    * js = @"document.getElementsByTagName(\"meta\")['title'].getAttribute('content')";
    [self.webView evaluateJavaScript:js completionHandler:^(id _Nullable response, NSError * _Nullable error) {
        DLog(@" -- web view -- -- response -- %@ ",response);
        if ([response isKindOfClass:[NSString class]] &&
            [(NSString *)response containsString:@"http"]) {
            _metaUrl = response;
        }
    }];
    
    // 计时结束:成功
    [self stopTimer];
    [[GTUdpSocketManager sharedUdpSocketManager] sendDataWithPage:GTSVisit_index_page  success:1 elapsed:_totalTime];
}

//请求出错时调用
- (void)webView:(WKWebView *)webView
didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation
      withError:(NSError *)error
{
//    NSURL    * str = error.userInfo[@"NSErrorFailingURLKey"];
//    if ([str isKindOfClass:[NSURL class]] && [[UIApplication sharedApplication] canOpenURL:str]) {
//        [[UIApplication sharedApplication] openURL:str];
//    }
    
    // 计时结束:失败
    [self stopTimer];
    [[GTUdpSocketManager sharedUdpSocketManager] sendDataWithPage:GTSVisit_index_page  success:0 elapsed:0];
    
}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView
{
    [self showLoadFailedView:YES];
    DLog(@" -- web view -- 页面内容加载失败");
}

#pragma mark -
#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ( IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_CHANGE) ){
        [self resetAccountType];
        return;
    }
    
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat p = [change[@"new"] floatValue];
        if (p < 0.2){
            p = 0.2;
        }
        [_progressV setProgress:p animated:YES];
        
        if (p >= 1.0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [_progressV setProgress:0 animated:NO];
                _progressV.hidden = YES;
            });
        }else{
            _progressV.hidden = NO;
        }
    }
    else if ([keyPath isEqualToString:@"title"]){
        NSString    * t = change[@"new"];
        self.titleLab.text = t;
    }
}

//重设头像
- (void)resetHeadImage
{
    [self.leftNavItem.iconImgV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                                 placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                                          options:SDWebImageAllowInvalidSSLCertificates];
}


#pragma mark -
#pragma mark - other

/**
 刷新导航条

 @param enterPage 是否是进入下一页
 */
- (void)refreshLeftNavItem
{
//    NSArray     * arr = self.webView.backForwardList.backList;
    
    if (![_mainUrl isEqualToString:_metaUrl]){
        self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(backAction)];
    }else{
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];;
    }
    
    self.navigationItem.titleView.frame = titleViewFrame;
}


- (void)showLoadFailedView:(BOOL)show
{
    if (show) {
        self.alertView.alpha = 0;
        [self.view addSubview:self.alertView];
        
        [UIView animateWithDuration:0.2 animations:^{
            self.alertView.alpha = 1;
        }];
    }else{
        [self.alertView removeFromSuperview];
    }
}


#pragma mark -
#pragma mark - lazy loading

- (WKWebView *)webView{
    if (!_webView) {

        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        config.userContentController = [[WKUserContentController alloc] init];
        [config.userContentController addScriptMessageHandler:(id)self.jsHelper name:@"ixMarketPage"];
        config.allowsInlineMediaPlayback = YES;
        
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                               kScreenHeight - kNavbarHeight - kTabbarHeight)
                                      configuration:config];
        _webView.allowsBackForwardNavigationGestures = NO;
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.dk_backgroundColorPicker = DKNavBarColor;
        for (UIView * v in _webView.subviews) {
            v.dk_backgroundColorPicker = DKNavBarColor;
        }
        weakself;
        _webView.scrollView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf reloadVC];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.webView.scrollView.header endRefreshing];
            });
        }];
    }
    
    return _webView;
}

- (IXJSHelper *)jsHelper {
    if (!_jsHelper) {
        _jsHelper = [[IXJSHelper alloc] initWithDelegate:(id)self vc:self msgName:@"ixMarketPage"];
    }
    return _jsHelper;
}

- (UIProgressView *)progressV
{
    if (!_progressV) {
        _progressV = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressV.dk_trackTintColorPicker = DKColorWithRGBs(0xE2E9F1, 0x303b4d);
        _progressV.dk_progressTintColorPicker = DKColorWithRGBs(0x11B873, 0x21ce99);
        _progressV.frame = CGRectMake(0, 0, kScreenWidth, 5);
    }
    
    return _progressV;
}

- (IXNoResultView *)alertView
{
    if (!_alertView) {
        _alertView = [IXNoResultView noResultViewWithFrame:self.view.bounds
                                                     image:AutoNightImageNamed(@"webRequestFail")
                                                     title:LocalizedString(@"请重新刷新")];
        _alertView.backgroundColor = ViewBackgroundColor;
        _alertView.alpha = 0;
    }
    
    return _alertView;
}

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [IXCustomView createLable:titleViewFrame
                                        title:LocalizedString(@"首页")
                                         font:PF_MEDI(15)
                                   wTextColor:0x232935
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentCenter];
    }
    return _titleLab;
}

- (IXLeftNavView *)leftNavItem
{
    if (!_leftNavItem) {
        _leftNavItem = [[IXLeftNavView alloc] initWithTarget:self action:@selector(leftItemClicked)];
    }
    
    return _leftNavItem;
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    // Nav顶层页面处理行情
    id<IXQuoteDistribute> obj = (id<IXQuoteDistribute>)self.navigationController.topViewController;
    if (![obj isKindOfClass:[self class]]) {
        if ([obj respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)]) {
            [obj didResponseQuoteDistribute:arr cmd:cmd];
        }
    }else{
        
    }
}

-(void)startGCDTimer{
    [self stopTimer];
    
    _totalTime = 0;
    NSTimeInterval period = 1; //设置时间间隔
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), period * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        // 异步线程
        dispatch_async(dispatch_get_main_queue(), ^{
            _totalTime ++;
            
            if (_totalTime >= 60) {
                [[GTUdpSocketManager sharedUdpSocketManager] sendDataWithPage:GTSVisit_index_page  success:0 elapsed:0];
            }
        });
    });
    
    dispatch_resume(_timer);
}

// 结束
-(void)stopTimer{
    if(_timer){
        dispatch_source_cancel(_timer);
        _timer = nil;
    }
}

@end
