//
//  IXTraceWebV.m
//  IXApp
//
//  Created by Evn on 2018/4/4.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXTraceWebV.h"

@implementation IXTraceWebV

+ (instancetype)initWithWebUrlType:(TraceWebUrlType)type
{
    IXTraceWebV *webV = [[IXTraceWebV alloc] initWithFrame:CGRectZero];
    NSURL *url = [NSURL URLWithString:[IXTraceWebV webUrlByType:type]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webV loadRequest:request];
    return webV;
}

- (void)refreshWebUrlType:(TraceWebUrlType)type
{
    NSURL *url = [NSURL URLWithString:[IXTraceWebV webUrlByType:type]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self loadRequest:request];
}

+ (NSString *)webUrlByType:(TraceWebUrlType)type
{
    switch (type) {
        case TraceWebUrlTypeTouchRegist:
            //原生开户填资料页面使用webview嵌套地址（点击注册按钮后的第一个界面）
            return @"http://m.gwfxglobal.com/channel/trace/1.html";
            break;
        case TraceWebUrlTypeRegistSuccess:
            //原生开户完成页面使用webview嵌套地址 （注册成功后跳转的第一个界面）
            return @"http://m.gwfxglobal.com/channel/trace/2.html";
            break;
        case TraceWebUrlTypeTouchIncash:
            //原生入金填资料第一个页面使用webview嵌套地址（新客户点击入金进入的第一个界面--新客户指第一次点入金的用户，如果无法区分新客户，就是新老客户都嵌套）
            return @"http://m.gwfxglobal.com/channel/trace/3.html";
            break;
        case TraceWebUrlTypeIncashSuccess:
            //原生入金完成页面使用webview嵌套地址 （新客户入金成功后的页面，指的是第三方支付确认资金到账）
            return @"http://m.gwfxglobal.com/channel/trace/4.html";
            break;
        default:
            return @"";
            break;
    }
}

@end
