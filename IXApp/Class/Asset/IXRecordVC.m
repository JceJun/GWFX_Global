//
//  IXRecordVC.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRecordVC.h"
#import "IXRecordCell.h"
#import "IXOpenChoiceV.h"
#import "IXRecordModel.h"
#import "NSString+FormatterPrice.h"
#import "IXPosDefultView.h"
#import "MJRefresh.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Record.h"
#import "IXRecordCacheM.h"

@interface IXRecordVC ()
<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) IXOpenChoiceV *dateView;
@property (nonatomic, strong) IXOpenChoiceV *cateView;
@property (nonatomic, strong) IXPosDefultView *posDefView;
@property (nonatomic, assign) int currentPage;
@property (nonatomic, assign) int pageSize;
@property (nonatomic, strong) IXRecordCacheM *symCacheM;

@end

@implementation IXRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *titleLbl = [IXCustomView createLable:CGRectMake(0, 0, 260, 40)
                                            title:LocalizedString(@"交易记录")
                                             font:PF_MEDI(15)
                                       wTextColor:0x4c6072
                                       dTextColor:0xe9e9ea
                                    textAlignment:NSTextAlignmentCenter];
    self.navigationItem.titleView = titleLbl;
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    _dataArr = [[NSMutableArray alloc] init];
    _symCacheM = [[IXRecordCacheM alloc] init];
    [self.view addSubview:self.tableV];
    _currentPage = 1;
    _pageSize = 20;
    [self.tableV.header beginRefreshing];
    
    self.dataArr = [IXBORequestMgr getCachedTradeRecord];
    [self.tableV reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)refreshData
{
    NSDictionary *paramDic = @{
                               @"pageNo":[NSString stringWithFormat:@"%d",_currentPage],
                               @"pageSize":[NSString stringWithFormat:@"%d",_pageSize]
                               };
    weakself;
    [IXBORequestMgr record_tradeRecordListWithParam:paramDic result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (weakSelf.tableV.header.isRefreshing) {
            [weakSelf.tableV.header endRefreshing];
        }
        weakSelf.currentPage--;
        if (success) {
            if (obj && [obj isKindOfClass:[NSDictionary class]]) {
                NSArray * data = obj[@"data"];
                if ([data isKindOfClass:[NSArray class]]) {
                    if ([data count] > 0) {
                        weakSelf.currentPage++;
                        if (weakSelf.currentPage == 1 && weakSelf.dataArr.count > 0) {
                            [weakSelf.dataArr removeAllObjects];
                            [weakSelf.tableV reloadData];
                        }
                        [weakSelf.dataArr addObjectsFromArray:data];
                        [weakSelf.tableV.footer endRefreshing];
                    }else{
                        if (weakSelf.currentPage >= 1) {
                            [weakSelf.tableV.footer endRefreshingWithNoMoreData];
                        }
                    }
                }
            }
        } else {
            [SVProgressHUD showErrorWithStatus:errStr];
        }
        [weakSelf refreshUI];
    }];
}

- (void)refreshUI
{
    if (_dataArr.count == 0) {
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        self.posDefView.imageName = @"search_default";
        self.posDefView.tipMsg = LocalizedString(@"暂无记录");
        self.tableV.scrollEnabled = NO;
        [self.tableV reloadData];
        return;
    } else {
        [self.posDefView removeFromSuperview];
        [_tableV reloadData];
    }
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0, 80, kScreenWidth, 217)];
    }
    return _posDefView;
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IXOpenChoiceV *)dateView
{
    if (!_dateView) {
        _dateView = [[IXOpenChoiceV alloc] initWithFrame:CGRectMake(0, kNavbarHeight, kScreenWidth/2, 35)
                                               WithTitle:MARKETORDER
                                               WithAlign:TapChoiceAlignRightOutImg
                                                 WithTap:^{
                                                     ;
                                                 }];
    }
    return _dateView;
}

- (IXOpenChoiceV *)cateView
{
    if (!_cateView) {
        _cateView = [[IXOpenChoiceV alloc] initWithFrame:CGRectMake(kScreenWidth/2, kNavbarHeight, kScreenWidth/2, 35)
                                               WithTitle:MARKETORDER
                                               WithAlign:TapChoiceAlignRightOutImg
                                                 WithTap:^{
                                                     ;
                                                 }];
    }
    return _cateView;
}

#pragma mark - lazy load
- (UITableView *)tableV{
    if (!_tableV){
        CGFloat height = kScreenHeight - kNavbarHeight - kBtomMargin - 5;
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 5, kScreenWidth, height)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.header = [self header];
        _tableV.footer = [self footer];
    }
    return _tableV;
}

- (MJRefreshNormalHeader *)header
{
    weakself;
    MJRefreshNormalHeader  *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentPage = 1;
        [weakSelf refreshData];
    }];
    header.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return header;
}

- (MJRefreshBackNormalFooter *)footer
{
    weakself;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
        [weakSelf refreshData];
    }];
    footer.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return footer;
}

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [@[] mutableCopy];
    }
    return _dataArr;
}

#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 110;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"IXRecordCell";
    IXRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        //        cell.dk_backgroundColorPicker = DKTableColor;
    }
    [cell reloadUIData:_dataArr[indexPath.row] cacheSymbole:_symCacheM];
    cell.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end

