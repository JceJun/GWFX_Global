//
//  IXAdvertisementV.h
//  IXApp
//
//  Created by Seven on 2017/6/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXAdvertisementV : UIView


@property (nonatomic, copy) void(^advertisementBlock)();

@property (nonatomic, strong) UIImage    * img;

- (void)show;



@end
