//
//  IXDailyStatementVC.m
//  IXApp
//
//  Created by Seven on 2017/8/29.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDailyStatementVC.h"
#import "IXPosDefultView.h"
#import "IXCommonWebVC.h"
#import "IXBORequestMgr+Record.h"
#import "IXSettingCellA.h"
#import "MJRefresh.h"
#import "IXBOModel.h"

#define kPageSize   30

@interface IXDailyStatementVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (nonatomic, strong) UITableView       * tableV;
@property (nonatomic, strong) NSMutableArray    * dataList;
/** 无内容提示 */
@property (nonatomic, strong) IXPosDefultView   * posDefView;
@property (nonatomic, assign) NSInteger currentPage;

@end

@implementation IXDailyStatementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.title = LocalizedString(@"日结单");
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    
    [self.view addSubview:self.tableV];
    NSArray * cachedArr = [IXBORequestMgr getCachedStatementRecord];
    
    if (cachedArr.count) {
        [self.dataList addObjectsFromArray:cachedArr];
        [self.tableV reloadData];
    }
    [self.tableV.header beginRefreshing];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXSettingCellA * cell = (IXSettingCellA *)[tableView dequeueReusableCellWithIdentifier:cellIdent];
    if (!cell) {
        cell = [[IXSettingCellA alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
    }
    cell.titleFont = RO_REGU(15);
    
    IXDailyStatement    * model = _dataList[indexPath.row];
    [cell configTitle:[model timeString] content:nil];
    
    if (indexPath.row == 0) {
        [cell showTopLineWithOffsetX:0];
    } else {
        [cell showTopLineWithOffsetX:15];
    }
    
    if (indexPath.row == _dataList.count - 1) {
        [cell showBototmLineWithOffsetX:0];
    } else {
        [cell showBototmLineWithOffsetX:15];
    }
    cell.dk_backgroundColorPicker = DKNavBarColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    IXDailyStatement    * model = _dataList[indexPath.row];
    
    if (model.filePath.length) {
        IXCommonWebVC * vc = [IXCommonWebVC new];
        vc.url = model.filePath;
        vc.title = [model timeString];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark -
#pragma mark - request

- (void)requestData
{
    weakself;
    [IXBORequestMgr record_statementListWithPageNo:_currentPage
                                          pageSize:kPageSize
                                          complete:^(NSArray *list, NSString *errStr)
    {
        [weakSelf endRefreshing];
        
        if (!list.count && errStr.length) {
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            if (weakSelf.currentPage == 1) {
                [weakSelf.dataList removeAllObjects];
            }
            
            [weakSelf.dataList addObjectsFromArray:list];
            [weakSelf.tableV reloadData];
        }
        
        weakSelf.posDefView.hidden = weakSelf.dataList.count != 0;
    }];
}

- (void)endRefreshing
{
    [self.tableV.header endRefreshing];
    [self.tableV.footer endRefreshing];
}

#pragma mark -
#pragma mark - lazy loading

- (NSMutableArray *)dataList
{
    if (!_dataList) {
        _dataList = [@[] mutableCopy];
    }
    return _dataList;
}

static  NSString    * cellIdent = @"cell";
- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 5, kScreenWidth,
                                                                kScreenHeight
                                                                - kNavbarHeight - 5)
                                               style:UITableViewStylePlain];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKNavBarColor;
        _tableV.header = [self header];
        _tableV.footer = [self footer];
        [_tableV registerClass:[IXSettingCellA class] forCellReuseIdentifier:cellIdent];
        
//        UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
//        v.dk_backgroundColorPicker = DKTableColor;
//        _tableV.tableHeaderView = v;
    }
    return _tableV;
}

- (MJRefreshNormalHeader *)header
{
    weakself;
    MJRefreshNormalHeader  *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentPage = 1;
        [weakSelf requestData];
    }];
    header.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return header;
}

- (MJRefreshBackNormalFooter *)footer
{
    weakself;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage ++;
        [weakSelf requestData];
    }];
    footer.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return footer;
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0, 80, kScreenWidth, 217)];
        _posDefView.imageName = @"search_default";
        _posDefView.tipMsg = LocalizedString(@"暂无记录");
        _posDefView.hidden = YES;
        [self.tableV addSubview:self.posDefView];
    }
    return _posDefView;
}

@end
