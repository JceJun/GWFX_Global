//
//  IXReportVC.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXReportVC.h"
#import "IXReportCell.h"
#import "IXOpenChoiceV.h"
#import "NSString+FormatterPrice.h"
#import "IXReportModel.h"
#import "MJRefresh.h"
#import "IXPosDefultView.h"
#import "MJRefresh.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Record.h"

@interface IXReportVC ()<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSMutableArray *inDataArr;
@property (nonatomic, strong) NSMutableArray *outDataArr;
@property (nonatomic, strong) IXOpenChoiceV *dateView;
@property (nonatomic, strong) IXOpenChoiceV *cateView;
@property (nonatomic, strong) IXPosDefultView *posDefView;

@property (nonatomic, strong) UIButton *outBtn;
@property (nonatomic, strong) UIButton *inBtn;
@property (nonatomic, strong) UIView *dLine;
@property (nonatomic, assign) int currentIndex;
@property (nonatomic, assign) int inCurPage;
@property (nonatomic, assign) int outCurPage;
@property (nonatomic, assign) int pageSize;

@end

@implementation IXReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"出入金明细");
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    [self initData];
    [self addTitleView];
    [self addLineView];
    [self.view addSubview:self.tableV];
    [self.tableV.header beginRefreshing];
    
    self.inDataArr = [IXBORequestMgr getCachedInCashRecord];
    [self.tableV reloadData];
}

- (void)initData
{
    _currentIndex = 0;
    _inCurPage = 1;
    _outCurPage = 1;
    _pageSize = 20;
}

- (void)refreshData
{
    NSDictionary *paramDic = nil;
    
    weakself;
    if (_currentIndex == 0) {
        paramDic = @{
                     @"pageNo":[NSString stringWithFormat:@"%d",_inCurPage],
                     @"pageSize":[NSString stringWithFormat:@"%d",_pageSize]
                     };
        
        [IXBORequestMgr record_incomeListWithParam:paramDic result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            [weakSelf.tableV.header endRefreshing];
            [weakSelf.tableV.footer endRefreshing];
            
            if (success) {
                weakSelf.inCurPage--;
                if (obj && [obj isKindOfClass:[NSDictionary class]] && [[obj allKeys] containsObject:@"data"]) {
                    id data = obj[@"data"];
                    if ([data isKindOfClass:[NSArray class]]) {
                        if ([(NSArray *)data count] > 0) {
                            weakSelf.inCurPage++;
                            if (weakSelf.inCurPage == 1 && weakSelf.inDataArr.count > 0) {
                                [weakSelf.inDataArr removeAllObjects];
                                [weakSelf.tableV reloadData];
                            }
                            [weakSelf.inDataArr addObjectsFromArray:data];
                        }
                    }
                }
            } else {
                [SVProgressHUD showErrorWithStatus:errStr];
            }
            [weakSelf refreshUI];
        }];
        
    } else {
        paramDic = @{
                     @"pageNo":[NSString stringWithFormat:@"%d",_outCurPage],
                     @"pageSize":[NSString stringWithFormat:@"%d",_pageSize]
                     };
        [IXBORequestMgr record_drawCashListWithParam:paramDic result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            [weakSelf.tableV.header endRefreshing];
            [weakSelf.tableV.footer endRefreshing];
            
            if (success) {
                weakSelf.outCurPage--;
                if (obj && [obj isKindOfClass:[NSDictionary class]] && [[obj allKeys] containsObject:@"data"]) {
                    id data = obj[@"data"];
                    if ([data isKindOfClass:[NSArray class]]) {
                        if ([(NSArray *)data count] > 0) {
                            weakSelf.outCurPage++;
                            if (weakSelf.outCurPage == 1 && weakSelf.outDataArr.count > 0) {
                                [weakSelf.outDataArr removeAllObjects];
                                [weakSelf.tableV reloadData];
                            }
                            [weakSelf.outDataArr addObjectsFromArray:data];
                        }
                    }
                }
            } else {
                [SVProgressHUD showErrorWithStatus:errStr];
            }
            [weakSelf refreshUI];
        }];
    }
}

- (NSMutableArray *)inDataArr
{
    if (!_inDataArr) {
        _inDataArr = [[NSMutableArray alloc] init];
    }
    return _inDataArr;
}

- (NSMutableArray *)outDataArr
{
    if (!_outDataArr) {
        _outDataArr = [[NSMutableArray alloc] init];
    }
    return _outDataArr;
}

- (void)refreshUI
{
    if (_currentIndex == 0 && _inDataArr.count == 0) {
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        self.posDefView.imageName = @"search_default";
        self.posDefView.tipMsg = LocalizedString(@"暂无入金记录");
        [self.tableV reloadData];
        return;
    } else if (_currentIndex == 1 && _outDataArr.count == 0) {
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        self.posDefView.imageName = @"search_default";
        self.posDefView.tipMsg = LocalizedString(@"暂无出金记录");
        [self.tableV reloadData];
        return;
    } else {
        [self.posDefView removeFromSuperview];
        [_tableV reloadData];
    }
}

- (void)addTitleView
{
    NSArray *titleArr = @[LocalizedString(@"入金"),
                          LocalizedString(@"出金")];
    for (int i = 0; i < 2; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i*kScreenWidth/2, 0, kScreenWidth/2, 35)];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        btn.tag = i;
        btn.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        btn.titleLabel.font = PF_MEDI(13);
        if (i == 0) {
            _inBtn = btn;
            [btn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea) forState:UIControlStateNormal];
        } else {
            _outBtn = btn;
            [btn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4) forState:UIControlStateNormal];
        }
        [btn addTarget:self action:@selector(switchDirection:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    _dLine = [[UIView alloc] initWithFrame:CGRectMake((kScreenWidth/2 - 75)/2,  33, 75, 2)];
    _dLine.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    [self.view addSubview:_dLine];
}

- (void)switchDirection:(UIButton *)btn
{
    int index = btn.tag;
    [self.tableV.header endRefreshing];
    if (index == _currentIndex) {
        return;
    } else {
        _currentIndex = index;
    }
    switch (index) {
        case 0:{
            _dLine.frame = CGRectMake((kScreenWidth/2 - 75)/2, 33, 75, 2);
            [_inBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea)forState:UIControlStateNormal];
            [_outBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4) forState:UIControlStateNormal];
        }
            break;
        case 1:{
            _dLine.frame = CGRectMake((kScreenWidth/2 - 75)/2 + kScreenWidth/2, 33, 75, 2);
            [_inBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4)forState:UIControlStateNormal];
            [_outBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea) forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    if (_currentIndex == 0 && _inDataArr.count > 0) {
        [self refreshUI];
        [self.tableV setContentOffset:CGPointMake(0, 0)];
        return;
    }
    if (_currentIndex == 1 && _outDataArr.count > 0) {
        [self refreshUI];
        [self.tableV setContentOffset:CGPointMake(0, 0)];
        return;
    } else {
        _outDataArr = [IXBORequestMgr getCachedWithDrawRecord];
    }
    [self.tableV.header beginRefreshing];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IXOpenChoiceV *)dateView
{
    if (!_dateView) {
        _dateView = [[IXOpenChoiceV alloc] initWithFrame:CGRectMake(0, kNavbarHeight, kScreenWidth/2, 35)
                                               WithTitle:MARKETORDER
                                               WithAlign:TapChoiceAlignRightOutImg
                                                 WithTap:^{
                                                     ;
                                                 }];
    }
    return _dateView;
}

- (IXOpenChoiceV *)cateView
{
    if (!_cateView) {
        _cateView = [[IXOpenChoiceV alloc] initWithFrame:CGRectMake(kScreenWidth/2, kNavbarHeight, kScreenWidth/2, 35)
                                               WithTitle:MARKETORDER
                                               WithAlign:TapChoiceAlignRightOutImg
                                                 WithTap:^{
                                                     ;
                                                 }];
    }
    return _cateView;
}

- (void)addLineView
{
    UIView *lineV1 = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth/2, 0, 1, 35)];
    lineV1.dk_backgroundColorPicker = DKLineColor;
    [self.view addSubview:lineV1];
    
    UIView *lineV2 = [[UIView alloc] initWithFrame:CGRectMake(0, 35, kScreenWidth, 1)];
    lineV2.dk_backgroundColorPicker = DKLineColor;
    [self.view addSubview:lineV2];
}

#pragma mark - lazy load
- (UITableView *)tableV
{
    if (!_tableV){
        CGRect frame = CGRectMake(0, 40, kScreenWidth, kScreenHeight - kNavbarHeight - kBtomMargin - 40);
        _tableV = [[UITableView alloc] initWithFrame:frame];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.header = [self header];
        _tableV.footer = [self footer];
    }
    return _tableV;
}

- (MJRefreshNormalHeader *)header
{
    weakself;
    MJRefreshNormalHeader  *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (weakSelf.currentIndex == 0) {
            weakSelf.inCurPage = 1;
        } else {
            weakSelf.outCurPage = 1;
        }
        [weakSelf refreshData];
    }];
    header.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return header;
}

- (MJRefreshBackNormalFooter *)footer
{
    weakself;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (weakSelf.currentIndex == 0) {
            weakSelf.inCurPage++;
        } else {
            weakSelf.outCurPage++;
        }
        [weakSelf refreshData];
    }];
    footer.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return footer;
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0, 80, kScreenWidth, 217)];
    }
    return _posDefView;
}

#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_currentIndex == 0) {
        return _inDataArr.count;
    } else {
        return _outDataArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 143;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"IXReportCell";
    IXReportCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXReportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    }
    if (_currentIndex == 0) {
        [cell reloadUIData:_inDataArr[indexPath.row] index:_currentIndex];
    } else {
        [cell reloadUIData:_outDataArr[indexPath.row] index:_currentIndex];
    }
    cell.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end

