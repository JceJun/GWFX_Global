//
//  IXAssetHomeCell.h
//  IXApp
//
//  Created by Evn on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@class IXAssetHomeCellM;
@interface IXAssetHomeCell : IXClickTableVCell

- (void)config:(IXAssetHomeCellM *)model;
                
@end

@interface IXAssetHomeCellM : NSObject

@property (nonatomic, copy) NSString    * imgStr;
@property (nonatomic, copy) NSString    * title;
@property (nonatomic, copy) NSString    * content;
@property(nonatomic,assign) BOOL commissionStatus;

- (instancetype)initWithImgStr:(NSString *)imgStr
                         title:(NSString *)title
                       content:(NSString *)content;

@end
