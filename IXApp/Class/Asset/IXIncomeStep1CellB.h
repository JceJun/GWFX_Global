//
//  IXIncomeStep1CellB.h
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IXUserBankM;

@interface IXIncomeStep1CellB : UITableViewCell

@property (nonatomic, strong) IXUserBankM * bankM;

- (void)reloadUIWithData:(NSDictionary *)dic;

@end
