//
//  IXRechargeTipV.m
//  IXApp
//
//  Created by Evn on 2017/10/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRechargeTipV.h"
#import "UIViewExt.h"
#import "IXCpyConfig.h"

#define kLabWidth ([[UIScreen mainScreen] bounds].size.width - 50)

@interface IXRechargeTipV ()

@property (nonatomic, strong) UILabel   * msgLab;
@property (nonatomic, assign) BOOL  animating;

@end

@implementation IXRechargeTipV

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createSubView];
        self.backgroundColor = AutoNightColor(0xfcf1f2, 0x2c2b35);
        _animating = NO;
    }
    return self;
}

- (void)createSubView
{
    CGSize size = self.bounds.size;
    UIImageView * leftImgV = [[UIImageView alloc] initWithFrame:CGRectMake(12.5, (size.height-18)/2, 18, 18)];
    leftImgV.dk_imagePicker = DKImageNames(@"common_tip", @"common_tip_D");
    [self addSubview:leftImgV];
    
    UIView  * msgBtomV = [[UIView alloc] initWithFrame:CGRectMake(40, (size.height-20)/2, kLabWidth, 20)];
    msgBtomV.clipsToBounds = YES;
    [self addSubview:msgBtomV];
    
    _msgLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kLabWidth, 20)];
    _msgLab.font = PF_MEDI(12);
//    _msgLab.text = @"用户Apple以价格210.16买入1000股腾讯控股";
    _msgLab.dk_textColorPicker = DKColorWithRGBs(0xff4653, 0xff4d2d);
    [msgBtomV addSubview:_msgLab];
    
    UIButton    * dismissBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - size.height - 5,
                                                                          0, size.height,
                                                                          size.height)];
    [dismissBtn setImage:GET_IMAGE_NAME(@"icon_close") forState:UIControlStateNormal];
    
    [dismissBtn addTarget:self action:@selector(dismissBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:dismissBtn];
    
    [UIView insertSeparateView:AutoNightColor(0xdae6f0,0x242a36) yPoint:self._height-PXFLOAT(1) height:PXFLOAT(1) superView:self];
}

- (void)dismissBtnAction
{
    if (self.dismissBlock) {
        _title = @"";
        self.dismissBlock();
    }
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    _msgLab.text = title;
    
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(12)}];
    _msgLab.frame = CGRectMake(0, 0, size.width + 10, 20);
    [_msgLab sizeToFit];
    [_msgLab.layer removeAllAnimations];
    
    if (!_animating) {
        CGFloat duration = (size.width - kLabWidth) * 0.05;
        duration = MAX(1, duration);
        [self beginAnimationWith:duration];
    }
}

- (void)beginAnimationWith:(CGFloat)duration
{
    CGSize  size = [_title sizeWithAttributes:@{NSFontAttributeName:_msgLab.font}];
    _msgLab.frame = CGRectMake(0, 0, size.width + 10, 20);
    
    if (size.width <= kLabWidth) {
        _animating = NO;
        return;
    }
    
    _animating = YES;
    //#### 两个block循环调用彼此，若block中强引用了self，将导致self无法释放，故使用weak self ####
    weakself;
    [UIView animateWithDuration:duration delay:1.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.msgLab.frame = CGRectMake(-(size.width - kLabWidth), 0, size.width, 20);
    } completion:^(BOOL finished) {
        if (finished) {
            [weakSelf backAnimationWith:duration];
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((duration+1) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf backAnimationWith:duration];
            });
        }
    }];
}

- (void)backAnimationWith:(CGFloat)duration
{
    CGSize  size = [_title sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    if (size.width <= kLabWidth) {
        _animating = NO;
        return;
    }
    
    _animating =YES;
    weakself;
    [UIView animateWithDuration:duration delay:1.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.msgLab.frame = CGRectMake(0, 0, size.width, 20);
    } completion:^(BOOL finished) {
        if (finished) {
            [weakSelf beginAnimationWith:duration];
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((duration+1) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf beginAnimationWith:duration];
            });
        }
    }];
}

- (void)cancelTimer{
    if (self.timer) {
        dispatch_source_cancel(self.timer);
        self.timer = nil;
    }
}

@end
