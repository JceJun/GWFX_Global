//
//  IXDrawConfirmV.h
//  IXApp
//
//  Created by Evn on 2017/9/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^clickBtn) (NSUInteger index);

@interface IXDrawConfirmV : UIView

- (id)initWithFrame:(CGRect)frame withMoney:(NSString *)money withDrawFee:(NSString *)drawFee;
- (void)show;

@property (nonatomic, strong)clickBtn selectBtn;

@end
