//
//  IXIncomeStep3VC.m
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashResultVC.h"
#import "IXIncomeStep3CellA.h"
#import "IXIncomeStep3CellB.h"
#import "IXTouchTableV.h"
#import "IXAcntProgressV.h"
#import "IXAlertVC.h"
#import "IXOpenTipV.h"
#import "IXTraceWebV.h"

#import "IxProtoAccountBalance.pbobjc.h"
#import "IxItemAccountBalance.pbobjc.h"
#import "IXTCPRequest.h"
#import "IXAppUtil.h"
#import "IXDBAccountGroupMgr.h"
#import "IXAccountGroupModel.h"
#import "UIKit+Block.h"
#import "IXWebCustomerSeviceVC.h"
#import <UMMobClick/MobClick.h>
#import "IXCpyConfig.h"
#import "IXTipView.h"
#import "IXSkirillToSkrillVC.h"
#import "IXBORequestMgr+Asset.h"

@interface IXIncashResultVC ()
<
UITableViewDelegate,
UITableViewDataSource,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong)IXTouchTableV  * tableV;
@property (nonatomic, strong)IXOpenTipV     * tipView;
@property (nonatomic, strong)IXTraceWebV   * traceWebV;

@property (nonatomic, strong)NSArray    * titleArr;
@property (nonatomic, strong)NSArray    * itemArr;
@property (nonatomic, strong)NSString   * submitTime;
@property (nonatomic, strong)NSString   * dealTime;

@property (nonatomic, assign)BOOL       flag;
@property (nonatomic, assign)NSInteger  counts;
@property(nonatomic,assign)BOOL onceJoin;

@end

@implementation IXIncashResultVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_BALANCE_GET);
    }
    
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_BALANCE_GET);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (UMAppKey.length) {
        [MobClick event:UM_deposit3];
    }

    self.view.dk_backgroundColorPicker = DKViewColor;
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    
    self.title = LocalizedString(@"存入资金");
    _titleArr = @[LocalizedString(@"入金提交"),
                  LocalizedString(@"入金处理中"),
                  LocalizedString(@"入金成功")];
    _itemArr = @[LocalizedString(@"入金方式"),
                 LocalizedString(@"支付金额")];
    _submitTime = [IXEntityFormatter getCurrentTimeFormaterHHMM];
    _dealTime = [IXEntityFormatter getCurrentTimeFormaterHHMM];
    
    _counts = 0;
    [self.view addSubview:self.tableV];
//    [self performSelector:@selector(accountQuery) withObject:nil afterDelay:3];//延迟3秒查询
}

- (void)loadTraceWebView
{
    [[AppsFlyerTracker sharedTracker] trackEvent:@"入金-结果页-入金成功" withValues:nil];
    //嵌套webview地址
    if (!_traceWebV) {
        _traceWebV = [IXTraceWebV initWithWebUrlType:TraceWebUrlTypeIncashSuccess];
        [self.view addSubview:_traceWebV];
    } else {
        [_traceWebV refreshWebUrlType:TraceWebUrlTypeIncashSuccess];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (!_onceJoin) {
        _onceJoin = YES;
    }else{
        [[AppsFlyerTracker sharedTracker] trackEvent:@"入金-结果页" withValues:nil];
        [self accountQuery];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeTip];
}

- (void)accountQuery
{
    proto_account_balance_get *pb = [[proto_account_balance_get alloc] init];
    item_account_balance *balance = [[item_account_balance alloc] init];
    balance.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    balance.proposalNo = _incomeModel.pNo;
    pb.accountBalance = balance;
    [[IXTCPRequest shareInstance] accountQueryWithParam:pb];
}

- (void)removeTip
{
    if (_tipView) {
        _tipView.isShow = NO;
        [_tipView removeFromSuperview];
    }
}

#pragma mark -
#pragma mark - btn action

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)nextBtnClk
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark - table view

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
        v.dk_backgroundColorPicker = DKTableHeaderColor;
        
        NSDictionary    * dic = [IXAccountGroupModel shareInstance].accGroupDic;
        NSString * currency = dic[@"currency"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18)/2, kScreenWidth, 22)];
        label.text = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"入金进度"),currency];
        label.textAlignment = NSTextAlignmentCenter;
        label.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);;
        label.font = PF_MEDI(15);
        [v addSubview:label];
        
        IXAcntProgressV *prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
        [v addSubview:prgV];
        [prgV showFromMole:3 toMole:4 deno:4];
        
        return v;
    }else{
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)];
        v.dk_backgroundColorPicker = DKViewColor;
        
        return v;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 108)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:LocalizedString(@"完成") forState:UIControlStateNormal];
    nextBtn.titleLabel.font = PF_REGU(15);
    
    [v addSubview:nextBtn];
    
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 132;
    }else{
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }else{
        return 108;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return _titleArr.count;
    }else{
        return _itemArr.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *identifier = @"IXIncomeStep3CellA";
        IXIncomeStep3CellA *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[IXIncomeStep3CellA alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        switch (indexPath.row) {
            case 0:{
                cell.desc.font = RO_REGU(15);
                [cell loadUIWithTitle:_titleArr[indexPath.row]
                                 desc:_submitTime
                                  tag:indexPath.row
                              process:YES];
                break;
            }
            case 1:{
                cell.desc.font = RO_REGU(15);
                [cell loadUIWithTitle:_titleArr[indexPath.row]
                                 desc:_dealTime
                                  tag:indexPath.row
                              process:YES];
                break;
            }
            case 2:{
                cell.desc.font = PF_MEDI(13);
                [cell loadUIWithTitle:_titleArr[indexPath.row]
                                 desc:LocalizedString(@"实时到账")
                                  tag:indexPath.row
                              process:!_flag];
                break;
            }
            default:
                break;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dk_backgroundColorPicker = DKNavBarColor;
        return cell;
    }else{
        static NSString *identifier = @"IXIncomeStep3CellB";
        IXIncomeStep3CellB *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (!cell) {
            cell = [[IXIncomeStep3CellB alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        switch (indexPath.row) {
            case 0:{
                cell.desc.font = PF_MEDI(13);
                [cell loadUIWithTitle:_itemArr[indexPath.row]
                                 desc:_incomeModel.payName
                                  tag:indexPath.row];
                break;
            }
            case 1:{
                cell.desc.font = RO_REGU(15);
                [cell loadUIWithTitle:_itemArr[indexPath.row]
                                 desc:[NSString stringWithFormat:@"%@",
                                       [IXAppUtil amountToString:_incomeModel.amount]]
                                  tag:indexPath.row];
                break;
            }
            default:
                break;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dk_backgroundColorPicker = DKNavBarColor;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    [SVProgressHUD dismiss];
    if( IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_BALANCE_GET) ){
        proto_account_balance_get  *pb = ((IXTradeDataCache *)object).pb_account_balance_get;
        _counts++;
        if (pb.result == 0 && pb.accountBalance.status == item_account_balance_estatus_StatusOk) {
            _flag = YES;
            _dealTime = [IXEntityFormatter getCurrentTimeFormaterHHMM];
            
            [self.tableV reloadData];
            [self loadTraceWebView];
        }else{
            if (_counts >= 2) {
                [self removeTip];
                self.tipView.tipInfo = LocalizedString(@"支付遇到问题请联系客服");
                [self.view addSubview:_tipView];
                [self.view bringSubviewToFront:_tipView];
                
                [self showSkrillPayFaildAlert];
            }else{
                IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"入金提示")
                                                         message:LocalizedString(@"支付完成后请选择下面按钮")
                                                     cancelTitle:LocalizedString(@"取消")
                                                      sureTitles:LocalizedString(@"已完成支付")];
                VC.expendAbleAlartViewDelegate = self;
                [VC showView];
            }
        }
    }
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (btnIndex == 0) {
        [self accountQuery];
    }else{
        [self accountQuery];
    }
    
    return YES;
}


#pragma mark -
#pragma mark - lazy loading

- (IXOpenTipV *)tipView
{
    if (!_tipView) {
        _tipView = [[IXOpenTipV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 40)];
        weakself;
        _tipView.textBlock = ^(NSString *text) {
            IXWebCustomerSeviceVC *vc = [IXWebCustomerSeviceVC new];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
    }
    return _tipView;
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.separatorStyle = UITableViewCellSelectionStyleNone;
        [_tableV registerClass:[IXIncomeStep3CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXIncomeStep3CellA class])];
        [_tableV registerClass:[IXIncomeStep3CellB class]
        forCellReuseIdentifier:NSStringFromClass([IXIncomeStep3CellB class])];
    }
    
    return _tableV;
}

// Skrill支付失败
- (void)showSkrillPayFaildAlert{
    NSDictionary *channelDic = [IXBORequestMgr shareInstance].channelDic;
    if ([[channelDic[@"gatewayCode"] lowercaseString] containsString:@"skrill"]){
        [IXTipView showWithTitle:@"Kindly Remind" message:@"If you have a skrill account, you can also transfer money via skrill acoount" cancelTitle:@"Cancel" otherTitles:@[@"Deposit Now"] btnBlk:^(int btnIdx) {
            if (btnIdx == 1) {
                IXSkirillToSkrillVC *vc = [IXSkirillToSkrillVC new];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }];
    }
}

@end
