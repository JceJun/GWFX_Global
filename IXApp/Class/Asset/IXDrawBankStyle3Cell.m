//
//  IXDrawBankStyle3Cell.m
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawBankStyle3Cell.h"

@interface IXDrawBankStyle3Cell ()

@property (nonatomic, strong) UILabel *leftTitleLbl;


@end

@implementation IXDrawBankStyle3Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.contentView.dk_backgroundColorPicker = DKNavBarColor;

    }
    return  self;
}


- (UILabel *)leftTitleLbl
{
    if ( !_leftTitleLbl ) {
        _leftTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, kScreenWidth - 30, 15)
                                           WithFont:PF_MEDI(13)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4];
        [self.contentView addSubview:_leftTitleLbl];
    }
    return _leftTitleLbl;
}


- (UILabel *)rightContentLbl
{
    if ( !_rightContentLbl ) {
        _rightContentLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 14, kScreenWidth - 30, 17)
                                              WithFont:PF_MEDI(13)
                                             WithAlign:NSTextAlignmentRight
                                            wTextColor:0xa7adb5
                                            dTextColor:0x8395a4];
        [self.contentView addSubview:_rightContentLbl];
    }
    return _rightContentLbl;
}

- (void)setLeftTitle:(NSString *)leftTitle
{
    self.leftTitleLbl.text = leftTitle;
}

- (void)setRightContent:(NSString *)rightContent
{
    self.rightContentLbl.text = rightContent;
}

@end
