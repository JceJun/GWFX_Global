//
//  IXPieChartView.h
//  IXApp
//
//  Created by Bob on 2017/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXPieChartView : UIView

@property (nonatomic, strong) UIColor *trackColor;
@property (nonatomic, strong) UIColor *progressColor;

- (void)updatePercent:(CGFloat)percent animation:(BOOL)animationed;

@end
