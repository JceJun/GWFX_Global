//
//  IXCommonWebVC.m
//  IXApp
//
//  Created by Seven on 2017/8/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCommonWebVC.h"
#import <WebKit/WebKit.h>
#import "IXNoResultView.h"
#import "UIViewExt.h"
#import "IXBORequestMgr.h"
#import "AppDelegate+UI.h"
@interface IXCommonWebVC ()
 <
WKUIDelegate,
WKNavigationDelegate,
WKScriptMessageHandler
>

@property (nonatomic, strong) WKWebView         * webView;
@property (nonatomic, strong) UIProgressView    * progressV;
@property (nonatomic, strong) IXNoResultView    * alertView;

@end

@implementation IXCommonWebVC

- (void)dealloc
{
    [self.webView removeObserver:self forKeyPath:@"title"];
    [self.webView removeObserver:self forKeyPath:@"loading"];
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(backAction)];
    
    [self addDismissItemWithTitle:@"Done" target:self action:@selector(cancel)];
    
    [self addWebView];
    [self.view addSubview:self.progressV];
    
    if (_url) {
        NSURLRequest    * request = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]];
        [self.webView loadRequest:request];
    } else {
        [self showLoadFailedView:YES];
    }
}

- (void)cancel{
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)addWebView
{
    [self.view addSubview:self.webView];
    [self.webView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.webView addObserver:self forKeyPath:@"title"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self forKeyPath:@"loading"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
}

- (void)backAction
{
    if (self.webView.canGoBack) {
        [self.webView goBack];
    }else{
        if (self.navigationController.viewControllers.count > 1) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)ges_tapAction
{
    if (self.webView.URL) {
        [self.webView reload];
    } else if (_url.length){
        NSURLRequest    * request = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]];
        [self.webView loadRequest:request];
    }
}

#pragma mark -
#pragma mark - UIDelegate
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    CFDataRef exceptions = SecTrustCopyExceptions (serverTrust);
    SecTrustSetExceptions (serverTrust, exceptions);
    CFRelease (exceptions);
    completionHandler (NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:serverTrust]);
}

//web页面探出警告框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptAlertPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(void))completionHandler
{
    completionHandler();
    DLog(@" -- web view -- %s",__func__);
}

//web页面弹出确认框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptConfirmPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(BOOL result))completionHandler
{
    completionHandler(NO);
    DLog(@" -- web view -- %s",__func__);
}

//web页面弹出输入框时调用
- (void)webView:(WKWebView *)webView
runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt
    defaultText:(nullable NSString *)defaultText
initiatedByFrame:(WKFrameInfo *)frame
completionHandler:(void (^)(NSString * _Nullable result))completionHandler
{
    completionHandler(@"input text");
    DLog(@" -- web view -- %s",__func__);
}


#pragma mark -
#pragma mark - navigation delegate

- (void)webView:(WKWebView *)webView
decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if (!navigationAction.navigationType) {
        [webView loadRequest:navigationAction.request];
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    // 融合页
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

- (void)webView:(WKWebView *)webView
decidePolicyForNavigationResponse:(nonnull WKNavigationResponse *)navigationResponse
decisionHandler:(nonnull void (^)(WKNavigationResponsePolicy))decisionHandler
{
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView
didFailNavigation:(WKNavigation *)navigation
      withError:(NSError *)error
{
     DLog(@" -- web view --  %s",__func__);
}

//加载完成时调用
- (void)webView:(WKWebView *)webView
didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    [self showLoadFailedView:NO];
}

//请求出错时调用
- (void)webView:(WKWebView *)webView
didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation
      withError:(NSError *)error
{
    NSURL    * str = error.userInfo[@"NSErrorFailingURLKey"];
    if ([str isKindOfClass:[NSURL class]] && [[UIApplication sharedApplication] canOpenURL:str]) {
//        [[UIApplication sharedApplication] openURL:str];
    }
}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView
{
    for (UIView * v in _webView.subviews) {
        v.dk_backgroundColorPicker = DKNavBarColor;
    }
    [self showLoadFailedView: YES];
    DLog(@" -- web view -- 页面内容加载失败");
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    //    message.body  --  Allowed types are NSNumber, NSString, NSDate, NSArray,NSDictionary, and NSNull.
    if ([message.name isEqualToString:@"ios"]) {
        [self gotoNext:message.body];
    }
}

- (void)gotoNext:(NSDictionary *)dic{
    NSString *functionName = dic[@"functionName"];
    @try{
        [self performSelector:NSSelectorFromString(functionName)];
    }@catch(NSException *exception) {
        if([functionName isEqualToString:@""]){
            
        }
    }
}

- (void)appInviteRegist{
    if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
        [AppDelegate showRegist];
        [IXBORequestMgr shareInstance].launchHappenDic[@"Invite"] = @(YES);
        return;
    }
}

- (void)appClose{
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark -
#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat p = [change[@"new"] floatValue];
        if (p < 0.2){
            p = 0.2;
        }
        [_progressV setProgress:p animated:YES];
        
        if (p == 1.0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [_progressV setProgress:0 animated:NO];
                _progressV.hidden = YES;
            });
        }else{
            _progressV.hidden = NO;
        }
    }
    else if ([keyPath isEqualToString:@"title"]){
        NSString    * t = change[@"new"];
        self.title = t;
    }
}


#pragma mark -
#pragma mark - other
- (void)showLoadFailedView:(BOOL)show
{
    if (show) {
        self.alertView.alpha = 1;
        [self.view addSubview:self.alertView];
    }else{
        [self.alertView removeFromSuperview];
    }
}


#pragma mark -
#pragma mark - getter

- (WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                               kScreenHeight - kNavbarHeight)];
        _webView.allowsBackForwardNavigationGestures = YES;
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.dk_backgroundColorPicker = DKNavBarColor;
        for (UIView * v in _webView.subviews) {
            v.dk_backgroundColorPicker = DKNavBarColor;
        }
        [_webView.configuration.userContentController addScriptMessageHandler:self name:@"ios"];
        if (self.navigationController.viewControllers.count == 1) {
            _webView._height += kNavbarHeight;
        }
        // view.scalespagetofit只有UIWebView才有 wkwebview自适应
        NSString *jScript = @"var meta = document.createElement('meta'); \
        meta.name = 'viewport'; \
        meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'; \
        var head = document.getElementsByTagName('head')[0];\
        head.appendChild(meta);";
    
        NSMutableArray* array = [NSMutableArray arrayWithArray:_webView.configuration.userContentController.userScripts];
        for (WKUserScript *wkUScript in array)
        {
            if([wkUScript.source isEqual:jScript])
            {
                [array removeObject:wkUScript];
                break;
            }
        }
        for (WKUserScript *wkUScript in array)
        {
            [_webView.configuration.userContentController addUserScript:wkUScript];
        }
    }
    
    return _webView;
}

- (UIProgressView *)progressV
{
    if (!_progressV) {
        _progressV = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressV.dk_trackTintColorPicker = DKColorWithRGBs(0xE2E9F1, 0x303b4d);
        _progressV.dk_progressTintColorPicker = DKColorWithRGBs(0x11B873, 0x21ce99);
        _progressV.frame = CGRectMake(0, 0, kScreenWidth, 5);
    }
    
    return _progressV;
}

- (IXNoResultView *)alertView
{
    if (!_alertView) {
        _alertView = [IXNoResultView noResultViewWithFrame:self.view.bounds
                                                     image:AutoNightImageNamed(@"webRequestFail")
                                                     title:LocalizedString(@"请重新刷新")];
        _alertView.dk_backgroundColorPicker = DKTableColor;
        _alertView.alpha = 0;
        
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(ges_tapAction)];
        [_alertView addGestureRecognizer:tap];
    }
    
    return _alertView;
}



@end
