//
//  IXAssetHomeVC.m
//  IXApp
//
//  Created by Evn on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAssetHomeVC.h"
#import "IXAssetHomeCell.h"

#import "IXDrawBankVC.h"
#import "IXSymSearchVC.h"
#import "IXCommonWebVC.h"

#import "IXReportVC.h"
#import "IXRecordVC.h"
#import "IXLeftNavView.h"
#import "IXAdvertisementV.h"
#import "IXIncashStep1VC.h"

#import "IXDBAccountMgr.h"
#import "IXDBAccountGroupMgr.h"
#import "IXAccountBalanceModel.h"
#import "IXBORequestMgr+Asset.h"
#import "IXAccountGroupModel.h"

#import "IXAssetHeaderView.h"
#import "UILabel+LineSpace.h"

#import "IXSupplementAccountVC.h"
#import "IXDailyStatementVC.h"

#import "IXTCPRequest_net.h"
#import "UIImageView+AFNetworking.h"
#import "IXDBAccountGroupSymCataMgr.h"
#import "IXDBSymbolCataMgr.h"

#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXAppUtil.h"
#import "IXQuoteDistribute.h"
#import "UIImageView+WebCache.h"
#import "IXCpyConfig.h"
#import "IXDPSChannelVC.h"
#import "IXGuideView.h"

#define kHeaderViewHeight   [[UIScreen mainScreen] bounds].size.width - 40

@interface IXAssetHomeVC ()
<
UITableViewDataSource,
UITableViewDelegate
>
{
    NSMutableArray      *subDynPriceArr;          //当前订阅Price信息
}

@property (nonatomic, strong) UILabel   * usageMarginLbl;//已用保证金
@property (nonatomic, strong) UILabel   * marginScaleLbl;//保证金比例
@property (nonatomic, strong) UILabel   * profitLbl;    //实时盈亏
@property (nonatomic, strong) UILabel   * currencyLbl;  //货币类型

@property (nonatomic, strong) IXAssetHeaderView * infoView;

@property (nonatomic, strong) NSMutableArray    * dataArr;
@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) NSDictionary  * accGroupDic;
@property (nonatomic, strong) NSMutableDictionary   * priceDic;
@property (nonatomic, strong) IXLeftNavView * leftNavItem;

@end

@implementation IXAssetHomeVC

- (id)init
{
    self = [super init];
    if(self){
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_CHANGE);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_DELETE);
        IXNetStatus_listen_regist(self, IXTradeStatusKey);
        IXNetStatus_listen_regist(self, IXQuoteStatusKey);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(refreashPositionQuote)
                                                     name:kRefreashProfit
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_CHANGE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_DELETE);
    IXNetStatus_listen_resign(self, IXTradeStatusKey);
    IXNetStatus_listen_resign(self, IXQuoteStatusKey);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}





- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];
    
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightItemWithImg:[UIImage imageNamed:@"common_search_avatar"]
                              target:self
                                 sel:@selector(rightBtnItemClicked)
                            aimWidth:55];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorHexFromRGB(0x232935),
                                                                    NSFontAttributeName :PF_MEDI(15)};
    self.title = LocalizedString(@"资产");
    
    [self initData];
    [self.view addSubview:self.tableV];
    [self.tableV reloadData];
    [self loadCacheQuote];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakself;
        [self.infoView showCircleAnimationComplete:^{
            if ([weakSelf.marginScaleLbl.text isEqualToString:@"0.00%"]){
                [weakSelf.infoView setProgress:0 animation:YES];
            }
        }];
    });
    
    [self request];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self resetHeadImage];
    
    [self requetBonus];
}


- (void)initData
{
    _dataArr = [@[] mutableCopy];
    
    if ([IXDataProcessTools getCurrentAccountType] != item_account_group_etype_Virtuals
        || [[IXUserInfoMgr shareInstance] isDemeLogin]) {
        NSArray * iconArr = IncomeMode ? @[
                                           @"asset_income",
                                           @"asset_drawCash",
                                           @"asset_reward",
                                           @"asset_billlist",
                                           @"asset_dealRecord",
                                           // ,@"asset_dailyStatement"
                                           ]:@[
                                               @"asset_drawCash",
                                               @"asset_billlist",
                                               @"asset_dealRecord",
                                               // ,@"asset_dailyStatement"
                                               ];
        
        NSArray * titleArr = IncomeMode ? @[
                                            LocalizedString(@"存入资金"),
                                            LocalizedString(@"提取资金"),
                                            @"My Bonus",
                                            LocalizedString(@"出入金明细"),
                                            LocalizedString(@"交易记录"),
                                            //  ,LocalizedString(@"日结单")
                                            ] : @[
                                                  LocalizedString(@"提取资金"),
                                                  LocalizedString(@"出入金明细"),
                                                  LocalizedString(@"交易记录"),
                                                  //   ,LocalizedString(@"日结单")
                                                  ];
        
        NSArray * contentArr = IncomeMode ? @[
                                              LocalizedString(@"快速入金，实时到账"),
                                              LocalizedString(@"2小时快速处理取款请求"),
                                              @"Invite friends earn you more bonus",
                                              LocalizedString(@"查看详细的出入金交易活动"),
                                              LocalizedString(@"查看特定阶段的交易活动"),
                                              LocalizedString(@"查看每日账户结算详情")
                                              ]: @[
                                                   LocalizedString(@"2小时快速处理取款请求"),
                                                   LocalizedString(@"查看详细的出入金交易活动"),
                                                   LocalizedString(@"查看特定阶段的交易活动"),
                                                   //                                 ,LocalizedString(@"查看每日账户结算详情")
                                                   ];
        
        for (int i = 0; i < [iconArr count]; i ++) {
            IXAssetHomeCellM *model =[[IXAssetHomeCellM alloc] initWithImgStr:iconArr[i]
                                               title:titleArr[i]
                                             content:contentArr[i]];
            if ([model.title isEqualToString:@"My Bonus"]) {
                if ([IXBORequestMgr shareInstance].activityBalanceDic) {
                    if ([[IXBORequestMgr shareInstance].activityBalanceDic[@"commissionStatus"] integerValue] == 1) {
                        model.commissionStatus = YES;
                    }else{
                        model.commissionStatus = NO;
                    }
                }else{
                    model.commissionStatus = NO;
                }
            }            
            [_dataArr addObject:model];
        }
    } else {
        [_dataArr addObject:[[IXAssetHomeCellM alloc] initWithImgStr:@"asset_sign"
                                                               title:LocalizedString(@"签到赠金")
                                                             content:LocalizedString(@"每日签到，获赠模拟交易币")]];
        
        
        [_dataArr addObject:[[IXAssetHomeCellM alloc] initWithImgStr:@"asset_dealRecord"
                                                               title:LocalizedString(@"交易记录")
                                                             content:LocalizedString(@"查看特定阶段的交易活动")]];
    }
    [self.tableV reloadData];
}

#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    if ([IXUserInfoMgr shareInstance].isDemeLogin) {
        [self showRegistLoginAlert];
    } else {
        [[AppDelegate getRootVC] toggleLeftView];
    }
}

- (void)rightBtnItemClicked
{
    IXSymSearchVC *searchVC = [[IXSymSearchVC alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)loadCacheQuote
{
    NSMutableArray *_quote = [NSMutableArray array];
    for ( NSDictionary *dic in [IXAccountBalanceModel shareInstance].dynQuoteArr ) {
        IXQuoteM *model = [IXDataProcessTools queryQuoteDataBySymbolId:[dic[kID] integerValue]];
        if ( model ) {
            [_quote addObject:model];
        }
    }
    [[IXAccountBalanceModel shareInstance] setQuoteDataArr:_quote];
}



#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kHeaderViewHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    //处理table style是group时留白问题
    return 0.1;
}

- (UIView*) tableView:(UITableView *)_tableView viewForHeaderInSection:(NSInteger)section
{
    return [self createHeaderView];
}

- (void)resetFontWithLabel:(UILabel *)lbl WithContent:(NSString *)content WithFont:(UIFont *)font
{
    if ( !content || !lbl || !font ) {
        return;
    }
    lbl.adjustsFontSizeToFitWidth = YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"IXAssetHomeCell";
    IXAssetHomeCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXAssetHomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [cell config:_dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
        [self showRegistLoginAlert];
        return;
    }
    
    IXAssetHomeCellM    * m = _dataArr[indexPath.row];
    NSString    * title = m.title;
    
    [self gotoNext:title];
}


- (void)gotoNext:(NSString *)title{
    if ([title isEqualToString:LocalizedString(@"存入资金")]) {
        //        IXIncashStep1VC * incomeVC = [[IXIncashStep1VC alloc] init];
        //        incomeVC.hidesBottomBarWhenPushed = YES;
        //        [self.navigationController pushViewController:incomeVC animated:YES];
        
        IXDPSChannelVC *vc = [IXDPSChannelVC new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([title isEqualToString:LocalizedString(@"提取资金")]){
        [self pushDrawCash];
    }
    else if ([title isEqualToString:LocalizedString(@"出入金明细")]){
        IXReportVC  * reportVC = [[IXReportVC alloc] init];
        reportVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:reportVC animated:YES];
    }
    else if ([title isEqualToString:LocalizedString(@"交易记录")]){
        IXRecordVC  * recordVC = [[IXRecordVC alloc] init];
        recordVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:recordVC animated:YES];
    }
    else if ([title isEqualToString:LocalizedString(@"签到赠金")]){
        [IXBORequestMgr signIn:^(NSInteger amount, signInType type, NSString *errStr) {
            if (type == signInTypeFailure){
                //签到失败
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }else if (type == signInTypeComplete){
                //签到成功
                NSString    * amountStr = [@"+ " stringByAppendingString:[IXAppUtil amountToString:@(amount)]];
                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:
                                                      LocalizedString(@"签到成功"),
                                                      amountStr]];
            }else if (type == signInTypeAlreadySign){
                //当天已经签到
                [SVProgressHUD showInfoWithStatus:LocalizedString(@"今日模拟币已领")];
            }
        }];
    }else if (SameString(title, LocalizedString(@"日结单"))) {
        IXDailyStatementVC  * vc = [IXDailyStatementVC new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (SameString(title, @"My Bonus")){
        [IXBORequestMgr b_appAddEventTrack:@"activity__5" rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            NSLog(@"");
        }];
        [self appInvite];
        
    }
}

- (void)appInvite{
    if (![IXBORequestMgr shareInstance].userInfo.gts2CustomerId) {
        [self showRegistLoginAlert];
//        [IXBORequestMgr shareInstance].launchHappenDic[@"Invite"] = @(YES);
        return;
    }
//    NSString *userType = [IXDataProcessTools showCurrentAccountType];
//    NSArray *arr = @[LocalizedString(@"游客"),LocalizedString(@"模拟"),LocalizedString(@"真实")];
    time_t time1 = time(NULL);
    NSString *url = @"";
    if ([CompanyName containsString:@"UAT"]) {
        url = [NSString stringWithFormat:@"http://192.168.35.198:10276/activity/invite/friends2.html?companyId=B&customerId=%@&customerNumber=%@&time=%@&from=%@",[IXBORequestMgr shareInstance].gts2CustomerId,[IXBORequestMgr shareInstance].customerNo,@(time1),@"asset"];
    }else{
        url = [NSString stringWithFormat:@"https://deposit.gwfxglobal.com/activity/invite/friends2.html?companyId=RVT&customerId=%@&customerNumber=%@&time=%@&from=%@",[IXBORequestMgr shareInstance].gts2CustomerId,[IXBORequestMgr shareInstance].customerNo,@(time1),@"asset"];
    }
    
    IXCommonWebVC *vc = [IXCommonWebVC new];
    vc.url = url;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    [IXBORequestMgr b_customerActiviyBanlanceChange:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [self requetBonus];
        }
    }];
}


#pragma mark -
#pragma mark - request
//跳转取现
- (void)pushDrawCash
{
    if ([[IXBORequestMgr shareInstance].userInfo enable]) {
          [self checkDesPage];
    } else {
        [SVProgressHUD showWithStatus:LocalizedString(@"获取用户信息中...")];
        weakself;
        [IXBORequestMgr refreshUserInfo:^(BOOL success) {
            if (!success) {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
            }else{
                [SVProgressHUD dismiss];
                [weakSelf checkDesPage];
            }
        }];
    }
}

- (void)checkDesPage
{
    if ([IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams.count) {
        IXDrawBankVC *bankVC = [[IXDrawBankVC alloc] init];
        bankVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:bankVC animated:YES];
    }else{
        IXSupplementAccountVC *accountVC = [[IXSupplementAccountVC alloc] init];
        accountVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:accountVC animated:YES];
    }
}

#pragma mark -
#pragma mark - IXQuoteDistribute

- (void)needRefresh
{
    id obj = self.navigationController.topViewController;
    if ( ![obj isKindOfClass:[self class]] ) {
        
        if ([obj respondsToSelector:@selector(needRefresh)]) {
            [obj needRefresh];
        }
    }
}

//从资产页去搜索进入产品详情，需要刷新行情
- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if ( ![self.navigationController.topViewController isKindOfClass:[self class]] ) {
        id<IXQuoteDistribute> obj = (id<IXQuoteDistribute>)self.navigationController.topViewController;
        if ([obj respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)]) {
            [obj didResponseQuoteDistribute:arr cmd:cmd];
        }
    }
}

//刷新保证金和盈亏数据
- (void)refreashPositionQuote
{
    if (!_usageMarginLbl) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [IXDataProcessTools resetTextColorLabel:_profitLbl value:[IXAccountBalanceModel shareInstance].totalProfit];
        _profitLbl.text = [IXDataProcessTools moneyFormatterComma:[IXAccountBalanceModel shareInstance].totalProfit positiveNumberSign:NO];
        [self resetFontWithLabel:_profitLbl WithContent:_profitLbl.text WithFont:_profitLbl.font];
        
        _infoView.freedomAmount = [IXDataProcessTools moneyFormatterComma:[ [IXAccountBalanceModel shareInstance] getAvailabelFunds] positiveNumberSign:YES];
        _infoView.totalAmount = [IXDataProcessTools moneyFormatterComma:[[IXAccountBalanceModel shareInstance] getNet] positiveNumberSign:YES];

        _usageMarginLbl.text = [NSString stringWithFormat:@"%.2lf", [IXAccountBalanceModel shareInstance].totalMargin];
        [self resetFontWithLabel:_usageMarginLbl WithContent:_usageMarginLbl.text WithFont:_usageMarginLbl.font];
        
        double marginRate = 0;
        if (0 != [IXAccountBalanceModel shareInstance].totalMargin) {
            NSDecimalNumber *total = [NSDecimalNumber decimalNumberWithString:[_infoView.totalAmount stringByReplacingOccurrencesOfString:@"," withString:@""]];
            NSDecimalNumber *margin = [NSDecimalNumber decimalNumberWithString:_usageMarginLbl.text];
            
            if ( [margin doubleValue] != 0 ) {
                marginRate = [[[total decimalNumberByDividingBy:margin] decimalNumberByMultiplyingBy:
                               [NSDecimalNumber decimalNumberWithString:@"100"]] doubleValue];
                _marginScaleLbl.text = [NSString stringWithFormat:@"%.2f%%",marginRate];
            }else{
                _marginScaleLbl.text = @"0.00%";
            }
        }else{
            _marginScaleLbl.text = @"0.00%";
        }
        
        [self resetFontWithLabel:_marginScaleLbl WithContent:_marginScaleLbl.text WithFont:_marginScaleLbl.font];
        [_infoView setProgress:marginRate animation:YES];
    });
}


#pragma mark -
#pragma mark - kvo

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXNetStatus_isSameKey(keyPath, IXTradeStatusKey)) {
        IXTCPConnectStatus status = ((IXTCPRequest *)object).tradeStatus;
        [self showNetStatusWithConnectStatus:status];
        return;
    }
    
    if(IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_UPDATE)){
        proto_account_update  *pb = ((IXTradeDataCache *)object).pb_account_update;
        if (pb.result == 0) {
            [self refreashMarginInfo];
        }
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_ACCOUNT_CHANGE)) {
//        [self resetAccountType];
        [self initData];
        [self refreashMarginInfo];
    }

    if (IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_CHANGE)) {
        [self resetAccountType];
        _currencyLbl.text = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"货币"),self.accGroupDic[@"currency"]];
    }
    
    if( IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_SUB_DELETE) ){
        proto_symbol_sub_delete  *pb = ((IXTradeDataCache *)object).pb_symbol_sub_delete;
        if (pb.result == 0) {
            [SVProgressHUD showSuccessWithStatus:LocalizedString(@"取消自选成功")];
        } else {
            [SVProgressHUD dismiss];
        }
    }
}

- (void)refreashMarginInfo
{
    _currencyLbl.text = [NSString stringWithFormat:@"%@:%@",
                         LocalizedString(@"货币"),
                         self.accGroupDic[@"currency"]];
    _infoView.totalAmount = [IXDataProcessTools moneyFormatterComma:[[IXAccountBalanceModel shareInstance] getNet] positiveNumberSign:YES];
    
    _infoView.freedomAmount = [IXDataProcessTools moneyFormatterComma:[[IXAccountBalanceModel shareInstance] getAvailabelFunds] positiveNumberSign:YES];
    
    NSString * content = [NSString stringWithFormat:@"%.2f", [IXAccountBalanceModel shareInstance].totalMargin];
    _usageMarginLbl.text = content;
    
    if( [IXAccountBalanceModel shareInstance].totalMargin != 0  ){
        _marginScaleLbl.text = [NSString stringWithFormat:@"%.2f%%",([[_infoView.totalAmount stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue]/[_usageMarginLbl.text doubleValue]) * 100];
    }else{
        _marginScaleLbl.text = @"0.00%";
    }
}

//重设头像
- (void)resetHeadImage
{
    [self.leftNavItem.iconImgV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                                 placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                                          options:SDWebImageAllowInvalidSSLCertificates];
}

- (void)showNetStatusWithConnectStatus:(IXTCPConnectStatus)status
{
    switch (status) {
        case IXTCPConnectStatusDisconnect:{
            [self setTitleView:self.navigationItem.titleView title:LocalizedString(@"连接已断开")];
            [self stopTitleWaitting:status];
            break;
        }
        case IXTCPConnectStatusConneting:{
            [self setTitleView:self.navigationItem.titleView title:LocalizedString(@"连接中")];
            [self showTitleWaitting];
            break;
        }
        case IXTCPConnectStatusConnected:{
            [self setTitleView:self.navigationItem.titleView title:LocalizedString(@"资产")];
            [self stopTitleWaitting:status];
            break;
        }
        case IXTCPConnectStatusMaxTried:{
            [self setTitleView:self.navigationItem.titleView title:LocalizedString(@"未连接(点击重连)")];
            [self stopTitleWaitting:status];
            break;
        }
        default:
            break;
    }
}

- (void)resetAccountType
{
    self.leftNavItem.typeLab.text = [IXDataProcessTools showCurrentAccountType];
}

- (NSDictionary *)accGroupDic
{
    _accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
    return _accGroupDic;
}


#pragma mark -
#pragma mark - lazy loading

- (UITableView *)tableV{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight -
                                                                kNavbarHeight -
                                                                kTabbarHeight)
                                               style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKNavBarColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.showsHorizontalScrollIndicator = NO;
        _tableV.showsVerticalScrollIndicator = NO;
        _tableV.estimatedSectionFooterHeight = 0.f;
    }
    return _tableV;
}


- (IXLeftNavView *)leftNavItem
{
    if (!_leftNavItem) {
        _leftNavItem = [[IXLeftNavView alloc] initWithTarget:self action:@selector(leftBtnItemClicked)];
    }
    
    return _leftNavItem;
}

- (UIView *)createHeaderView
{
    UIView  * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kHeaderViewHeight)];
    headerView.dk_backgroundColorPicker = DKTableHeaderColor;
    
    _currencyLbl = [IXCustomView createLable:CGRectMake(0,
                                                        5,
                                                        kScreenWidth - 20,
                                                        20)
                                       title:[NSString stringWithFormat:@"%@:%@",
                                              LocalizedString(@"货币"),
                                              self.accGroupDic[@"currency"]]
                                        font:PF_MEDI(12)
                                  wTextColor:0x99abba
                                  dTextColor:0x8395a4
                               textAlignment:NSTextAlignmentRight];
    [headerView addSubview:_currencyLbl];

    _infoView = [[IXAssetHeaderView alloc] initWithFrame:CGRectMake(74, 20, circleR, circleR)];
    [headerView addSubview:_infoView];
    
    _infoView.acntType = [IXDataProcessTools showCurrentAccountType];
    
    NSString    * totalStr = [IXDataProcessTools moneyFormatterComma:[[IXAccountBalanceModel shareInstance] getNet] positiveNumberSign:YES];
    _infoView.totalAmount = totalStr;
    _infoView.totalAmountLab.textAlignment = NSTextAlignmentCenter;

    NSString    * freedomAmount = [IXDataProcessTools moneyFormatterComma:[[IXAccountBalanceModel shareInstance] getAvailabelFunds] positiveNumberSign:YES];
    _infoView.freedomAmount = freedomAmount;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(20,
                                                                CGRectGetMaxY(_infoView.frame) + 21 - kLineHeight,
                                                                kScreenWidth - 40, kLineHeight)];
    lineView.dk_backgroundColorPicker = DKLineColor3;
    [headerView addSubview:lineView];
    
    NSArray *titleArr = @[LocalizedString(@"已用保证金"),
                          LocalizedString(@"保证金比例"),
                          LocalizedString(@"实时盈亏")];
    float lblWidth = (kScreenWidth - 60)/3;
    float offsetX = 15;
    
    for (int i = 0; i < titleArr.count; i++) {
        UILabel *lbl = [IXCustomView createLable:CGRectMake(offsetX,GetView_MaxY(lineView) + 13, lblWidth, 21)
                                           title:titleArr[i]
                                            font:PF_MEDI(13)
                                      wTextColor:0x99abba
                                     dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentLeft];
        [headerView addSubview:lbl];
        
        UILabel *valueLbl = [IXCustomView createLable:CGRectMake(offsetX,GetView_MaxY(lbl) + 5, lblWidth, 14.5)
                                                title:@""
                                                 font:RO_REGU(15)
                                           wTextColor:0x4c6072
                                           dTextColor:0xe9e9ea
                                        textAlignment:NSTextAlignmentLeft];
        
        if (i == 0) {
            _usageMarginLbl = valueLbl;
            NSString *content = [NSString stringWithFormat:@"%.2f", [IXAccountBalanceModel shareInstance].totalMargin];
            _usageMarginLbl.text = content;
            [headerView addSubview:_usageMarginLbl];
            
            [self resetFontWithLabel:_usageMarginLbl WithContent:content WithFont:_usageMarginLbl.font];
        }else if (i == 1) {
            _marginScaleLbl = valueLbl;
            lbl.textAlignment = NSTextAlignmentCenter;
            _marginScaleLbl.textAlignment = NSTextAlignmentCenter;
            
            double marginRate = 0;
            if ( 0 != [IXAccountBalanceModel shareInstance].totalMargin ) {
                NSDecimalNumber *total = [NSDecimalNumber decimalNumberWithString:[_infoView.totalAmount stringByReplacingOccurrencesOfString:@"," withString:@""]];
                NSDecimalNumber *margin = [NSDecimalNumber decimalNumberWithString:_usageMarginLbl.text];
                
                if ( [margin doubleValue] != 0 ) {
                    marginRate = [[[total decimalNumberByDividingBy:margin] decimalNumberByMultiplyingBy:
                                   [NSDecimalNumber decimalNumberWithString:@"100"]] doubleValue];
                    _marginScaleLbl.text = [NSString stringWithFormat:@"%.2f%%",marginRate];
                }else{
                    _marginScaleLbl.text = @"0.00%";
                }
            }else{
                _marginScaleLbl.text = @"0.00%";
            }
            [headerView addSubview:_marginScaleLbl];
            
            [self resetFontWithLabel:_marginScaleLbl WithContent:_marginScaleLbl.text WithFont:_marginScaleLbl.font];
            [_infoView setProgress:marginRate animation:YES];
            
        }else if (i == 2) {
            _profitLbl = valueLbl;
            [IXDataProcessTools resetTextColorLabel:_profitLbl value:[IXAccountBalanceModel shareInstance].totalProfit];
            lbl.textAlignment = NSTextAlignmentRight;
            _profitLbl.textAlignment = NSTextAlignmentRight;
            _profitLbl.text = [IXDataProcessTools moneyFormatterComma:[IXAccountBalanceModel shareInstance].totalProfit positiveNumberSign:NO];
            
            [headerView addSubview:_profitLbl];
            
            [self resetFontWithLabel:_profitLbl WithContent:_profitLbl.text WithFont:_profitLbl.font];
        }
        
        offsetX += lblWidth + 15;
    }
    
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(headerView) - kLineHeight, kScreenWidth, kLineHeight)];
    lineView1.dk_backgroundColorPicker = DKColorWithRGBs(0xe2e9f1, 0x262f3e);
    [headerView addSubview:lineView1];
 
    
    return headerView;
}

- (void)request{
    //    [SVProgressHUD show];
    
    if ([IXBORequestMgr shareInstance].accountId) {
        // 更新银行卡信息&文件
        [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString *errStr) {
            if (success) {
                [IXBORequestMgr b_getCustomerFiles:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
                    if (success) {
                        [SVProgressHUD dismiss];
                    }else{
//                        [SVProgressHUD showInfoWithStatus:LocalizedString(@"获取客户文件信息失败")];
                    }
                }];
            }else if (errStr.length){
//                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
//                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
            }
        }];
    }
}

- (void)requetBonus{
    [IXBORequestMgr b_customerActivityBalanceQuery:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [self initData];
        }
    }];
}


@end

