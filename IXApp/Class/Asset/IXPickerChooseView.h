//
//  IXPickerChooseView.h
//  IXApp
//
//  Created by Bob on 2017/2/10.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#warning 工程中调用省市接口完成替换后，去掉多余的声明
typedef void(^pickerChooseInfo)(NSInteger section, NSInteger row);
typedef void(^chooseResultInfo)(NSInteger section, NSInteger row);

@interface IXPickerChooseView : UIView

@property (nonatomic, strong) NSMutableArray    * countryArr;
@property (nonatomic, strong) NSMutableArray    * provinceArr;
@property (nonatomic, strong) NSMutableArray    * dataArr;
@property(nonatomic,assign)BOOL onlyProvince;
@property (nonatomic, readonly) BOOL    showing;

@property (nonatomic, strong) NSArray   * seletedTitles;    //@[省code，市code]

- (id)initWithFrame:(CGRect)frame
     WithChooseInfo:(pickerChooseInfo)choose
     WithResultInfo:(chooseResultInfo)result;

- (void)show;

@end
