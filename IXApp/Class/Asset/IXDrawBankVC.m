//
//  IXDrawBankVC.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawBankVC.h"
#import "NSString+FormatterPrice.h"
#import "IXDrawBankStyle1Cell.h"
#import "IXDrawBankStyle2Cell.h"
#import "IXDrawBankStyle3Cell.h"
#import "IXDrawBankStyle4Cell.h"
#import "IXDrawFeeCell.h"
#import "IXDrawBankResultVC.h"
#import "IXOpenChooseContentView.h"
#import "IXTouchTableV.h"
//#import "IXSupplementAccountVC.h"
#import "IXDrawConfirmV.h"
#import "IXDrawRuleV.h"
#import "IXRechargeTipV.h"

#import "IXDrawSubmitModel.h"
#import "NSDictionary+Type.h"
#import "IXDrawModel.h"
#import "IXUserDefaultM.h"
#import "IXAccountBalanceModel.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXBORequestMgr+Comp.h"
#import "IXBORequestMgr+Asset.h"
#import "IXUserInfoM.h"
#import "IXBOModel.h"
#import "IXAccountGroupModel.h"

#import "IXDBAccountGroupMgr.h"
#import "IXWithdrawBankAddVC.h"
#import "IXTipView.h"
#import "IXDPSChannelVC.h"
#import "IXAddBankCardTypeVC.h"
#import "UIKit+Block.h"
#import "IXCommonTextDetailVC.h"
#import "UIViewExt.h"

#define WARNING_STR  @"If you withdraw money now, you will be disqualified from the promotion, bonus will also be deducted from your account."

@interface IXDrawBankVC ()<UITableViewDelegate,UITableViewDataSource>
{
    UIButton      *drawBtn;
}

@property (nonatomic, strong) IXDrawModel   * drawModel;
@property (nonatomic, strong) IXTouchTableV * contentTV;
@property (nonatomic, strong) IXUserBankM   * chooseBankInfo;
@property (nonatomic, strong) IXOpenChooseContentView *chooseCata;
@property (nonatomic, strong) IXRechargeTipV     *tipView;
@property (nonatomic, strong) NSMutableArray *showBankNameArr;
@property (nonatomic, copy)   NSString       *drawMoney;
@property(nonatomic,strong)UILabel *bottmTip;
@property(nonatomic,strong)UILabel *lb_rules;

@end

@implementation IXDrawBankVC

- (UILabel *)lb_rules{
    if (!_lb_rules) {
        _lb_rules = [UILabel new];
        _lb_rules.font = ROBOT_FONT(12);
        _lb_rules.dk_textColorPicker = DKCellTitleColor;
        _lb_rules.textAlignment = NSTextAlignmentCenter;
        _lb_rules.numberOfLines = 0;
        _lb_rules.text = @"Withdrawal rules";
        [self.view addSubview:_lb_rules];
        [_lb_rules block_whenTapped:^(UIView *aView) {
            IXCommonTextDetailVC *vc = [IXCommonTextDetailVC makeControllerWithTitle:@"Withdrawal rules"];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        
        [_lb_rules makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(0);
            make.bottom.equalTo(-100);
        }];
    }
    return _lb_rules;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _showBankNameArr = [NSMutableArray array];
    self.view.dk_backgroundColorPicker = DKViewColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(btn_onGoback)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(btn_onGoback)];
    self.title = LocalizedString(@"账户出金");
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self request];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeTip];
    [SVProgressHUD dismiss];
    [self.view endEditing:YES];
}

- (void)request{
    [SVProgressHUD show];
    
    // 更新银行卡信息&文件
    [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString *errStr) {
        if (success) {
            [IXBORequestMgr b_getCustomerFiles:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
                if (success) {
                    [SVProgressHUD dismiss];
                    [self getBankUrlInfo];
                }else{
                    [SVProgressHUD showInfoWithStatus:LocalizedString(@"获取客户文件信息失败")];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }else if (errStr.length){
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
        }
    }];
}

- (void)configBankNameArr
{
    
//    NSMutableArray  * bankList = [[IXBORequestMgr shareInstance].userInfo.detailInfo addedBankList];
//    NSMutableArray *bankList = [IXBORequestMgr mergedBankList];
    
    NSMutableArray *allBanks = [NSMutableArray arrayWithArray:[IXBORequestMgr mergedBankList]];
    [allBanks addObjectsFromArray:[IXBORequestMgr mergedWithDrawBankList]];
    NSMutableArray *bankList = [allBanks mutableCopy];
    
    //bankInfoArr
    self.drawModel.bankInfoArr = [@[] mutableCopy];
    for (int i = 0; i < bankList.count; i++) {
        IXUserBankM * bank = bankList[i];
        IXDrawBankModel * model = [IXDrawBankModel new];
        model.bankAccountName = bank.bankAccountName;
        model.bankAccountNumber = bank.bankAccountNumber;
        model.customerNumber = [NSString stringWithFormat:@"%ld",(long)bank.customerNumber];
        model.bank = bank.bank;
        model.bankOrder = bank.bankOrder;
        model.type = bank.type;
        [self.drawModel.bankInfoArr addObject:model];
    }
    
    //bank name array
    NSMutableArray  * arr = [NSMutableArray array];
    if (bankList.count < 6) {
        [arr addObject:LocalizedString(@"添加银行卡")];
    }
    for (int i = 0; i < bankList.count; i++) {
        IXUserBankM * bank = bankList[i];
        NSString * accountNumber = bank.bankAccountNumber;
        if (accountNumber.length > 4) {
            accountNumber = [accountNumber substringFromIndex:accountNumber.length - 4];
        }
        accountNumber = [NSString stringWithFormat:@"%@(%@)[%@]",[bank localizedName],accountNumber,bank.type];
        [arr addObject:accountNumber];
    }
    self.drawModel.bankNameArr = [arr copy];
    
    //show bank name arr
    for (int i = 0; i < bankList.count; i++) {
        IXUserBankM * bankM = bankList[i];
        if (bankM.bankAccountNumber.length) {
            [self.showBankNameArr addObject:[bankM localizedName]];
        } else {
            [self.showBankNameArr addObject:@""];
        }
    }

    self.drawModel.model = [self.drawModel.bankInfoArr firstObject];
    
    if (self.drawModel.model) {
        self.drawModel.showBankAccountNumber = [self getShowBankAccountNumber:self.drawModel.model.bankAccountNumber];
        self.drawModel.contentArr = @[
                                      LocalizedString(@"更换银行卡"),
                                      self.drawModel.model.bankAccountName,
                                      self.drawModel.showBankAccountNumber,
                                      [self.showBankNameArr firstObject]
                                      ];
        self.drawModel.titleArr = @[@[[self currentCry],@""],
                                    @[LocalizedString(@"取出到"),
                                      LocalizedString(@"姓名"),
                                      LocalizedString(@"卡号"),
                                      LocalizedString(@"银行")]];
        [self queryDrawCashState];
    } else {
        [self updateTableViewData:NO];
    }
}

- (void)updateTableViewData:(BOOL)flag{
    [self removeTip];
    if (self.drawModel.model) {
        if (!flag) {
            self.tipView.title = LocalizedString(@"请至“用户资料”完善相关信息，如有疑问，请联系客服。");
            [[UIApplication sharedApplication].keyWindow addSubview:self.tipView];
        }
    } else {
        self.drawModel.contentArr = @[LocalizedString(@"添加银行卡"),@""];
        self.drawModel.titleArr = @[@[[self currentCry],@""],
                                    @[LocalizedString(@"取出到"),
                                      @""]];
    }
    [_contentTV reloadData];
}

#pragma mark 查询用户当前账号能否取款
- (void)queryDrawCashState{
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];

    NSInteger bankOrder = self.drawModel.model.bankOrder;
    
    [IXBORequestMgr shareInstance].paramDic = [@{@"WithDrawCardType":self.drawModel.model.type} mutableCopy];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self resetBotomTip];
    });
    if ([self.drawModel.model.type isEqualToString:@"Help2pay"]) {
        [IXBORequestMgr b_getCustomerWouldCashOut:bankOrder rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            if (success) {
                [SVProgressHUD dismiss];
                if ([obj isKindOfClass:[NSDictionary class]] && [[(NSDictionary *)obj allKeys] count]) {
                    [self updateTableViewData:NO];
                } else {
                    [self updateTableViewData:YES];
                }
            } else {
                if (errStr.length) {
                    [SVProgressHUD showErrorWithStatus:errStr];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"查询失败")];
                }
            }
        }];
    }else{
        [IXBORequestMgr b_getCustomerWouldCashOutByWitCard:bankOrder rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            if (success) {
                [SVProgressHUD dismiss];
                if ([obj isKindOfClass:[NSDictionary class]] && [[(NSDictionary *)obj allKeys] count]) {
                    [self updateTableViewData:NO];
                } else {
                    [self updateTableViewData:YES];
                }
            } else {
                if (errStr.length) {
                    [SVProgressHUD showErrorWithStatus:errStr];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"查询失败")];
                }
            }
        }];
    }
}

#pragma mark 查询手续费规则
- (void)queryDrawFeeRule{
    [IXBORequestMgr drawCashFeeRuleWithParam:nil result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success && [obj isKindOfClass:[NSArray class]]) {
            IXDrawRuleV *dV = [[IXDrawRuleV alloc] initWithFrame:kScreenBound];
            [dV showDrawRule:(NSArray *)obj];
        } else {
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"查询失败")];
            }
        }
    }];
}

#pragma mark 查询取款手续费
- (void)queryDrawCashFee:(NSString *)drawMoney{
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    NSDictionary *paramDic = @{
                                @"withdrawAmount":drawMoney
                               };
    
    [IXBORequestMgr drawCashFeeWithParam:paramDic result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success && [obj isKindOfClass:[NSDictionary class]]) {
            [SVProgressHUD dismiss];
            IXDrawConfirmV *dV = [[IXDrawConfirmV alloc] initWithFrame:CGRectMake(0, 0, 295, 227) withMoney:drawMoney withDrawFee:[IXDataProcessTools dealWithNil:[(NSDictionary *)obj objectForKey:@"fee"]]];
            dV.selectBtn = ^(NSUInteger index) {
                if (index == 1) {
                    [self submitDrawInfo:drawMoney];
                } else {
                    drawBtn.userInteractionEnabled = YES;
                }
            };
            [dV show];
        } else {
            drawBtn.userInteractionEnabled = YES;
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"查询失败")];
            }
        }
    }];
}

- (void)btn_onGoback{
    [self.navigationController popViewControllerAnimated:YES];
}
 
- (NSString *)currentCry{
    NSDictionary *accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
    if (accGroupDic
        && [accGroupDic isKindOfClass:[NSDictionary class]]
        && [[accGroupDic allKeys] containsObject:@"currency"]) {
        return  [accGroupDic stringForKey:@"currency"];
    }
    return @"";
}

//先同步取款的接口，保证数据源一致；
- (void)getBankUrlInfo
{
    [SVProgressHUD showWithStatus:LocalizedString(@"获取绑定的银行卡信息...")];
    [IXBORequestMgr drawcash_checkBankInfo:^(BOOL success, NSString *errStr, NSDictionary *dic) {
        if (success) {
            if ([dic isKindOfClass:[NSDictionary class]]) {
                [SVProgressHUD dismiss];
                
                self.drawModel.balance = [NSString stringWithFormat:@"%.2lf",
                                          [dic doubleForKey:@"availableCredit"]];
                
                self.drawModel.gts2AccountId = [dic stringForKey:@"gts2AccountId"];
                self.drawModel.gts2CustomerId = [dic stringForKey:@"gts2CustomerId"];
                self.drawModel.accountGroupId = [dic stringForKey:@"accountGroupId"];
                self.drawModel.hasDeposit = [dic stringForKey:@"hasDeposit"];
                if ([dic[@"hasDeposit"] integerValue] != 1) {
                    [self guideToDeposit];
                }
                if ([dic[@"unLockAmount"] integerValue] == 1) {
                    [UIView bondSupperObject:self subObject:@(YES) byKey:@"unLockAmount"];
                }
                
                [self.contentTV reloadData];
                [self configBankNameArr];
                [self lb_rules];
                return ;
            }
        }
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取绑定银行卡信息出错")];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self.navigationController popViewControllerAnimated:YES];
          });
    }];
}

// 当用户进入取款界面时，判断用户未激活过（指未进行第一笔入金）时，弹出提示框提示用户
- (void)guideToDeposit{
    MarkEvent(@"出金界面-未入金弹出框-马上存款");
    [IXTipView showWithTitle:@"Tips" message:@"You need to deposit fund first before the withdrawals" cancelTitle:@"Cancel" otherTitles:@[@"Deposit"] btnBlk:^(int btnIdx) {
        if (btnIdx == 1) {
            IXDPSChannelVC *vc = [IXDPSChannelVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
}


- (NSString *)getShowBankAccountNumber:(NSString *)bankAccountNumber
{
    NSString *result = @"";
    if ( [bankAccountNumber length] >= 4 ) {
        NSString *lowStr;
        NSString *highStr;
        lowStr = [bankAccountNumber substringFromIndex:(bankAccountNumber.length - 4 )];
        highStr = [bankAccountNumber substringToIndex:4];
        
        result = [NSString stringWithFormat:@"%@ **** **** %@",highStr,lowStr];
    }else{
        result = [IXDataProcessTools dealWithNil:bankAccountNumber];
    }
    return result;
}

- (void)drawAction:(UIButton *)btn
{
    [self.view endEditing:YES];
    btn.userInteractionEnabled = NO;
    if (![_drawMoney length]) {
        return;
    }

    _drawMoney = [_drawMoney stringByReplacingOccurrencesOfString:@"," withString:@""];
    if(([IXEntityFormatter isPureInt:_drawMoney] || [IXEntityFormatter isPureFloat:_drawMoney])
       && [_drawMoney floatValue] != 0
       && [_drawMoney floatValue] <= [_drawModel.balance floatValue]){
        
        BOOL unLockAmount = [[UIView getSubObjFromSupperObj:self bySubObjectKey:@"unLockAmount"] boolValue];
        // 检测到用户在赠金活动期间单笔充值金额满足活动条件时，但交易手数未满足活动条件时，则弹出 "引导用户交易" 的提示框
        if (unLockAmount) {
            [IXTipView showWithTitle:@"Tips" message:[WARNING_STR stringByAppendingString:@"Are you sure you want to do this?"] cancelTitle:@"Cancel" otherTitles:@[@"YES"] btnBlk:^(int btnIdx) {
                if (btnIdx == 1) {
                    [self queryDrawCashFee:_drawMoney];
                }
            }];
        }else{
            [self queryDrawCashFee:_drawMoney];
        }
    } else {
        btn.userInteractionEnabled = YES;
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入合法的金额...")];
    }
}

- (void)submitDrawInfo:(NSString *)drawMoney
{
    IXDrawSubmitModel *submitModel = [[IXDrawSubmitModel alloc] init];
    submitModel.customerNo = _drawModel.model.customerNumber;
    submitModel.platform = PLATFORM;
    submitModel.accountNo = [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo;
    submitModel.payAmount = drawMoney;
    submitModel.fee = @"0";
    submitModel.transAmount = drawMoney;
    submitModel.gts2AccountId = _drawModel.gts2AccountId;
    submitModel.gts2CustomerId = _drawModel.gts2CustomerId;
    submitModel.accountGroupId = _drawModel.accountGroupId;
    submitModel.withdrewBankName = _drawModel.model.bank;
    submitModel.withdrewBankAccount = _drawModel.model.bankAccountNumber;
    submitModel.withdrewBankAccountName = _drawModel.model.bankAccountName;
    submitModel.accountCurrency = [IXAccountBalanceModel shareInstance].currency;
    submitModel.transCurrency = [IXAccountBalanceModel shareInstance].currency;
    submitModel.payCurrency = [IXAccountBalanceModel shareInstance].currency;
    submitModel.exchangeRate = @"1";
    
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    
//    [IXBORequestMgr b_addWithdrawNew:drawMoney withdrewBankName:_drawModel.model.bank withdrewBankAccount:_drawModel.model.bankAccountNumber withdrewBankAccountName:_drawModel.model.bankAccountName rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
//        drawBtn.userInteractionEnabled = YES;
//        if (success) {
//            [SVProgressHUD dismiss];
//            IXDrawBankResultVC *result = [[IXDrawBankResultVC alloc] init];
//            [self.navigationController pushViewController:result animated:YES];
//        } else {
//            if (errStr.length) {
//                [SVProgressHUD showErrorWithStatus:errStr];
//            } else {
//                [SVProgressHUD showErrorWithStatus:LocalizedString(@"取款失败")];
//            }
//        }
//    }];
    
    
    NSDictionary *paramDic = @{
                               @"cashout":[submitModel toJSONString]
                               };

    [IXBORequestMgr drawCashWith:paramDic complete:^(BOOL success, NSString *errStr) {
        drawBtn.userInteractionEnabled = YES;
        if (success) {
            [SVProgressHUD dismiss];
            IXDrawBankResultVC *result = [[IXDrawBankResultVC alloc] init];
            [self.navigationController pushViewController:result animated:YES];
        } else {
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"取款失败")];
            }
        }
    }];

}


#pragma mark -
#pragma mark - table view


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray *)_drawModel.titleArr[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section ) {
        case 0:{
            switch ( indexPath.row ) {
                case 0:{
                    return 56;
                }
                    break;
                case 1:{
                    return 33;
                }
                    break;
                default:
                    break;
            }
        }
        default:{
            if (self.drawModel.bankInfoArr.count) {
                return 44;
            } else {
                if (indexPath.row == 0) {
                    return 44;
                } else {
                    return 222;
                }
            }
        }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 8)];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ( 0 == section ) {
        return 0;
    }else {
        return 200;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    switch ( section ) {
        case 0:{
            return [[UIView alloc] initWithFrame:CGRectZero];
        }
            break;
        default:{
            UIView *footV = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 200)];
            
            drawBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            drawBtn.frame = CGRectMake( 15, 69+30+30, kScreenWidth - 30, 43);
            drawBtn.layer.masksToBounds = YES;
            drawBtn.layer.cornerRadius = 3;
            [drawBtn setTitle:LocalizedString(@"取出") forState:UIControlStateNormal];
            

            if ( [_drawMoney length] != 0 && _drawModel.model.bankAccountNumber.length) {
                [drawBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
                [drawBtn setUserInteractionEnabled:YES];
            }else{
                [drawBtn setBackgroundColor:MarketGrayPriceColor];
                [drawBtn setUserInteractionEnabled:NO];
            }
            [footV addSubview:drawBtn];
            [drawBtn addTarget:self
                        action:@selector(drawAction:)
              forControlEvents:UIControlEventTouchUpInside];
       
            
            if (self.drawModel.bankInfoArr.count) {
                UILabel *label = [IXUtils createLblWithFrame:CGRectMake( 15, 10, kScreenWidth - 15, 100)
                                                    WithFont:PF_MEDI(12)
                                                   WithAlign:NSTextAlignmentLeft
                                                  wTextColor:0xa7adb5
                                                  dTextColor:0x8395a4];
                
                label.text = @"";
                label.numberOfLines = 0;
                [footV addSubview:label];
                _bottmTip = label;
                [self resetBotomTip];
              
            } else {
                footV.frame = CGRectMake( 0, 0, kScreenWidth, 200);
            }
 
            if ([_drawMoney length] && _drawModel.model.bankAccountNumber.length) {
                [drawBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
                [drawBtn setUserInteractionEnabled:YES];
            }else{
                [drawBtn setBackgroundColor:MarketGrayPriceColor];
                [drawBtn setUserInteractionEnabled:NO];
            }
            
            if ( !_drawModel.balance || [_drawModel.balance floatValue] == 0 ) {
                [drawBtn setUserInteractionEnabled:NO];
                [drawBtn setBackgroundColor:MarketGrayPriceColor];
            }
            return footV;
        }
            break;
    }
}

- (void)resetBotomTip{
    NSString *bankType = self.drawModel.model.type;
    NSString *dateTips = @"";
    if ([bankType isEqualToString:@"Help2pay"]) {
        dateTips = [IXBORequestMgr shareInstance].mobileOnlineConfig[@"help2payPaymentDateTips"];
    }else{
        dateTips = [IXBORequestMgr shareInstance].mobileOnlineConfig[@"transferPaymentDateTips"];
    }
    NSString *str =  [NSString stringWithFormat:@"Expected %@ back to your account",dateTips];
    BOOL unLockAmount = [[UIView getSubObjFromSupperObj:self bySubObjectKey:@"unLockAmount"] boolValue];
    if (unLockAmount) {
        // 检测到用户在赠金活动期间单笔充值金额满足活动条件时，但交易手数未满足活动条件时，则弹出 "引导用户交易" 的提示框
        str = [str stringByAppendingFormat:@"\n%@",WARNING_STR];
    }
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:str];
    [string addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]}
                 range:[str rangeOfString:WARNING_STR]];
    
    NSRange rangeStart = [str rangeOfString:dateTips];
    
    NSRange rangeStop;
    NSInteger length = 0;
    
    length = [dateTips length];
    rangeStop = [str rangeOfString:dateTips];
    
    if(rangeStart.location != NSNotFound &&
       rangeStop.location != NSNotFound ){
        if ( [IXUserInfoMgr shareInstance].isNightMode ) {
            [string addAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0x21ce99)}
                            range:NSMakeRange( rangeStart.location,length)];
        }else{
            [string addAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0x11b873)}
                            range:NSMakeRange( rangeStart.location,length)];
        }
    }
    _bottmTip.attributedText = string;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section ) {
        case 0:{
            switch ( indexPath.row ) {
                case 0:{
                    IXDrawBankStyle1Cell *cell = [tableView dequeueReusableCellWithIdentifier:
                                                  NSStringFromClass([IXDrawBankStyle1Cell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    UIImageView *topImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, kLineHeight)];
                    topImg.dk_backgroundColorPicker = DKLineColor;
                    [cell.contentView addSubview:topImg];
                    
                    UIImageView *bottomImg = [[UIImageView alloc] initWithFrame:CGRectMake( 15, 56 - kLineHeight, kScreenWidth - 15, kLineHeight)];
                    bottomImg.dk_backgroundColorPicker = DKLineColor;
                    [cell.contentView addSubview:bottomImg];
                    
                    
                    cell.bankCurrencyType = _drawModel.titleArr[indexPath.section][indexPath.row];
                    if (_drawModel.balance) {
                        NSString *olderValue = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"最多取出"),
                                                _drawModel.balance];
                        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                        
                        dict[NSFontAttributeName] = PF_MEDI(15);
                        if ( [IXUserInfoMgr shareInstance].isNightMode ) {
                            dict[NSForegroundColorAttributeName] = UIColorHexFromRGB(0x303b4d);
                        }else{
                            dict[NSForegroundColorAttributeName] = SearchPlaceholderColor;
                        }
                        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:olderValue
                                                                                                attributes:dict];
                        cell.drawMoneyTF.attributedPlaceholder = att;
                        cell.totalAmount = [_drawModel.balance floatValue];
                        cell.balence = _drawModel.balance;
                        cell.drawMoneyTF.keyboardType = UIKeyboardTypeDecimalPad;
                        
                        weakself;
                        cell.textContent = ^(NSString *value){
                            weakSelf.drawMoney = value;
                            [weakSelf.contentTV reloadData];
                        };
                    }
                    if (_tipView) {
                        cell.userInteractionEnabled = NO;
                    } else {
                        cell.userInteractionEnabled = YES;
                    }
                    return cell;
                }
                    break;
                default:{
                    IXDrawFeeCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDrawFeeCell class])];
                    UIImageView *bottomImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 33 - kLineHeight, kScreenWidth, kLineHeight)];
                    bottomImg.dk_backgroundColorPicker = DKLineColor;
                    [cell.contentView addSubview:bottomImg];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
//                NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:olderValue
//                                                                                        attributes:dict];
//                cell.drawMoneyTF.attributedPlaceholder = att;
//                cell.totalAmount = [_drawModel.balance floatValue];
//                cell.balence = _drawModel.balance;
//                cell.drawMoneyTF.keyboardType = UIKeyboardTypeDecimalPad;
//                
//                weakself;
//                cell.textContent = ^(NSString *value){
//                    [weakSelf.contentTV reloadData];
//                };
            }
        }
            break;
            
        default:{
            if (self.drawModel.bankInfoArr.count) {
                IXDrawBankStyle3Cell *cell = [tableView dequeueReusableCellWithIdentifier:
                                              NSStringFromClass([IXDrawBankStyle3Cell class])];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                CGRect frame = (0 != indexPath.row) ? CGRectMake( 15, 0, kScreenWidth - 15, kLineHeight)
                : CGRectMake( 0, 0, kScreenWidth, kLineHeight);
                UIImageView *topImg = [[UIImageView alloc] initWithFrame:frame];
                topImg.dk_backgroundColorPicker = DKLineColor;
                [cell.contentView addSubview:topImg];
                
                
                if ( 3 == indexPath.row ) {
                    UIImageView *bottomImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
                    bottomImg.dk_backgroundColorPicker = DKLineColor;
                    [cell.contentView addSubview:bottomImg];
                }
                
                cell.leftTitle = _drawModel.titleArr[indexPath.section][indexPath.row];
                if ( _drawModel.contentArr.count > indexPath.row ) {
                    cell.rightContent = _drawModel.contentArr[indexPath.row];
                    
                    if ( [_drawModel.contentArr[indexPath.row] isEqualToString:LocalizedString(@"更换银行卡")] ) {
                        
                        CGRect frame = cell.rightContentLbl.frame;
                        frame.origin.y = 0;
                        frame.size.height = 44;
                        UIButton *changeListBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                        changeListBtn.frame = frame;
                        [changeListBtn addTarget:self
                                          action:@selector(showChooseV)
                                forControlEvents:UIControlEventTouchUpInside];
                        [cell.contentView addSubview:changeListBtn];
                        
                        cell.rightContentLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
                    }else{
                        cell.rightContentLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
                    }
                }
                if ([_drawModel.titleArr[indexPath.section][indexPath.row] isEqualToString:LocalizedString(@"卡号")]) {
                    cell.rightContentLbl.font = RO_REGU(15);
                } else {
                    cell.rightContentLbl.font = PF_MEDI(13);
                }
                
                return cell;
            } else {
                if (indexPath.row == 0) {
                    IXDrawBankStyle3Cell *cell = [tableView dequeueReusableCellWithIdentifier:
                                                  NSStringFromClass([IXDrawBankStyle3Cell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    UIImageView *topImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, kLineHeight)];
                    topImg.dk_backgroundColorPicker = DKLineColor;
                    [cell.contentView addSubview:topImg];
                    UIImageView *bottomImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
                    bottomImg.dk_backgroundColorPicker = DKLineColor;
                    [cell.contentView addSubview:bottomImg];
                    cell.rightContentLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
                    cell.leftTitle = _drawModel.titleArr[indexPath.section][indexPath.row];
                    cell.rightContent = _drawModel.contentArr[indexPath.row];
                    
                    return cell;
                } else {
                    IXDrawBankStyle4Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDrawBankStyle4Cell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    UIImageView *bottomImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 222 - kLineHeight, kScreenWidth, kLineHeight)];
                    bottomImg.dk_backgroundColorPicker = DKLineColor;
                    [cell.contentView addSubview:bottomImg];
                    return cell;
                }
            }
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1) {
        [self queryDrawFeeRule];
    } else {
        if (!self.drawModel.bankInfoArr.count) {
//            IXSupplementAccountVC *model = [[IXSupplementAccountVC alloc] init];
//            [self.navigationController pushViewController:model animated:YES];
            [self gotoBankType];
        }
    }
}


- (void)gotoBankType{
    [IXBORequestMgr shareInstance].bankAdditionInfo = @{@"origin":@"withdraw",
                                                        @"canDeposit":@"1",
                                                        @"canWithdraw":@"1",
                                                        @"effectType":@"withdraw",
                                                        @"gateway_name":@"help2pay",
                                                        @"gateway_code":@"help2pay",
                                                        };
    IXAddBankCardTypeVC *vc = [IXAddBankCardTypeVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showChooseV
{
    [self.view endEditing:YES];
    if(_drawModel.bankNameArr.count){
        [self.chooseCata resetDataSourceWithData:_drawModel.bankNameArr];
        [self.chooseCata show];
    }
}

- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds];
        _contentTV.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.bounces = NO;
        [_contentTV registerClass:[IXDrawBankStyle1Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXDrawBankStyle1Cell class])];
        [_contentTV registerClass:[IXDrawBankStyle2Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXDrawBankStyle2Cell class])];
        [_contentTV registerClass:[IXDrawBankStyle3Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXDrawBankStyle3Cell class])];
        [_contentTV registerClass:[IXDrawBankStyle4Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXDrawBankStyle4Cell class])];
        [_contentTV registerClass:[IXDrawFeeCell class] forCellReuseIdentifier:NSStringFromClass([IXDrawFeeCell class])];
        [self.view addSubview:_contentTV];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _contentTV;
}

- (IXOpenChooseContentView *)chooseCata
{
    if (!_chooseCata) {
        weakself;
        _chooseCata = [[IXOpenChooseContentView alloc] initWithDataSource:@[] WithSelectedRow:^(NSInteger row) {
            if (row >= 0 && row < weakSelf.drawModel.bankNameArr.count) {
                if ([weakSelf.drawModel.bankNameArr[row] isEqualToString:LocalizedString(@"添加银行卡")]) {
//                    IXSupplementAccountVC *model = [[IXSupplementAccountVC alloc] init];
//                    model.bankInfo = weakSelf.chooseBankInfo;
//                    [weakSelf.navigationController pushViewController:model animated:YES];
                    
//                    [IXBORequestMgr resetAddDepostiBankContainer];
                    
//                    [IXBORequestMgr shareInstance].bankAdditionInfo = @{@"origin":@"withdraw",
//                                                                        @"canDeposit":@"1",
//                                                                        @"canWithdraw":@"1",
//                                                                        @"effectType":@"withdraw",
//                                                                        @"gateway_name":@"help2pay",
//                                                                        @"gateway_code":@"help2pay",
//                                                                        };
//                    IXWithdrawBankAddVC *vc = [IXWithdrawBankAddVC new];
//                    [self.navigationController pushViewController:vc animated:YES];
                    [self gotoBankType];
                    
                }else{
                    if ([weakSelf.drawModel.bankNameArr[0] isEqualToString:LocalizedString(@"添加银行卡")]) {
                        row --;
                    }
                    IXDrawBankModel * bank = weakSelf.drawModel.bankInfoArr[row];
                    weakSelf.drawModel.model = bank;
                    
                    NSString    * showNum = [weakSelf getShowBankAccountNumber:bank.bankAccountNumber];
                    weakSelf.drawModel.contentArr = @[LocalizedString(@"更换银行卡"),
                                                       bank.bankAccountName,
                                                       showNum,
                                                       [weakSelf bankNameWith:bank]];
                    [weakSelf queryDrawCashState];
                    [weakSelf.contentTV reloadData];
                }
            }
        }];
    }
    return _chooseCata;
}

- (IXRechargeTipV *)tipView
{
    if (!_tipView) {
        _tipView = [[IXRechargeTipV alloc] initWithFrame:CGRectMake( 0, kNavbarHeight, kScreenWidth, 40)];
    }
    return _tipView;
}

- (void)removeTip
{
    if (_tipView) {
        [_tipView removeFromSuperview];
        _tipView = nil;
    }
}

- (NSString *)bankNameWith:(IXDrawBankModel*)bank
{
    NSMutableArray *bankList = [IXBORequestMgr mergedBankList];
    for (IXUserBankM * b in bankList) {
        if ([b.bank isEqualToString:bank.bank]) {
            return [b localizedName];
        }
    }
    return @"";
}

- (IXDrawModel *)drawModel
{
    if ( !_drawModel ) {
        _drawModel = [[IXDrawModel alloc] init];
    }
    return _drawModel;
}

@end
