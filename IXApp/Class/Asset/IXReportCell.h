//
//  IXReportCell.h
//  IXApp
//
//  Created by Evn on 17/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXReportCell : UITableViewCell
- (void)reloadUIData:(NSDictionary *)dataDic index:(NSInteger)index;
@end
