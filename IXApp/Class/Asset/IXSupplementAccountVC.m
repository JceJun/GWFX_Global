//
//  IXSupplementAccountVC.m
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//  补充账号资料流程： 上传图片 -》 提交文本信息 -》 提交银行卡信息

#import "IXSupplementAccountVC.h"
#import "IXSupplementAccountResultVC.h"
#import "IXPickerChooseView.h"
#import "IXDrawBankListVC.h"
#import "IXZoomScrollView.h"

#import "IXSupplementStyle1Cell.h"
#import "IXSupplementStyle2Cell.h"
#import "IXOpenChooseContentView.h"

#import "IXUserInfoM.h"
#import "IXBORequestMgr.h"
#import "IXBORequestMgr+Region.h"
#import "IXBORequestMgr+Account.h"

#import "IXSupplementFileM.h"
#import "IXSupplementBankM.h"

#import "UIImageView+WebCache.h"
#import "NSString+FormatterPrice.h"
#import "NSDictionary+Type.h"
#import "IXUserDefaultM.h"
#import "IXTouchTableV.h"
#import "NSObject+IX.h"
#import "UIImage+IX.h"
#import "IXAppUtil.h"
#import "IXBOModel.h"

@class IXSupplementFile;

@interface IXSupplementAccountVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
UIActionSheetDelegate,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV     * contentTV;
@property (nonatomic, strong) IXOpenChooseContentView   * chooseCata;
@property (nonatomic, strong) UIView    * footView;
@property (nonatomic, weak)   IXTextField     * nameTf;

@property (nonatomic, strong) NSArray   * titleArr;
@property (nonatomic, copy) NSString    * accountName;
@property (nonatomic, copy) NSString    * accountNum;

@property (nonatomic, strong) NSMutableArray    * contentArr;
@property (nonatomic, strong) IXEnableBankM     * chooseBank;
@property (nonatomic, strong) IXSupplementFileM     * fileModel;    //上传图片model
@property (nonatomic, strong) IXSupplementBankM     * bankModel;    //上传银行卡model

@property (nonatomic, assign) BOOL  supplementAccountInfo;

@end

@implementation IXSupplementAccountVC

- (instancetype)init
{
    if (self = [super init]) {
        _showResultPage = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    if (!(_modifyBankInfo && _bankInfo.imagePath && _bankInfo.imagePath.length) || [_bankInfo.proposalStatus isEqualToString:@"-1"]) {
        self.navigationItem.rightBarButtonItem =
        [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"提交")
                                       target:self
                                          sel:@selector(submitAccountInfo)];
    }
    
    _titleArr = @[
                  LocalizedString(@"姓名"),
                  LocalizedString(@"账号."),
                  LocalizedString(@"银行")
                  ];

    if (_modifyBankInfo) {
        self.title = LocalizedString(@"账户资料");
    } else {
        self.title = LocalizedString(@"添加银行卡");
    }
    
    [self setContentArr:nil];
    [self.contentTV reloadData];
    
    if (![[IXBORequestMgr shareInstance].userInfo enable]) {
        [SVProgressHUD showWithStatus:LocalizedString(@"数据加载中...")];
        weakself;
        [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString * errStr) {
            if (success && response) {
                [weakSelf initData];
            } else if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        }];
    } else {
        [self initData];
    };
}

- (void)initData
{
    if ([[IXBORequestMgr shareInstance].userInfo.detailInfo addedBankCount] > 2 && !_modifyBankInfo) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    self.accountName = nil; //[IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName;
    if (self.accountName.length) {
        [self.contentArr replaceObjectAtIndex:0 withObject:self.accountName];
    }
    if (_bankInfo) {
        _accountNum = [self formaterBankNumber:_bankInfo.bankAccountNumber];
        [self.contentArr replaceObjectAtIndex:1 withObject:_accountNum ? _accountNum : @""];
        [self.contentArr replaceObjectAtIndex:2 withObject:[_bankInfo localizedName]];
    }
    
    [self.contentTV reloadData];
    
    self.fileModel = [IXSupplementFileM new];
    
    if (_bankInfo) {
        //修改银行卡
        IXUserFileM * fileM = [[IXBORequestMgr shareInstance].userInfo.detailInfo fileWithBankOrder:_bankInfo.bankOrder];
        
        NSString    * fileId = [NSString stringWithFormat:@"%d",fileM.id_p];
        NSString    * customId = [NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
        
        IXSupplementFile *file = [[IXSupplementFile alloc] init];
        file.gts2CustomerId = customId;
        file.id = fileId;
        file.fileType = fileM.fileType;
        self.fileModel.files = [[NSMutableArray arrayWithObject:file] mutableCopy];
        [self setBankInfoByBankIndex:_bankInfo.bankOrder];
        
        self.accountName = _bankInfo.bankAccountName;
    } else {
        //添加银行卡
        NSInteger   bankIdx = [[IXBORequestMgr shareInstance].userInfo.detailInfo currentAddBankSequence];
        IXUserFileM * fileM = [[IXBORequestMgr shareInstance].userInfo.detailInfo fileWithBankOrder:bankIdx];
        
        NSString    * fileId = [NSString stringWithFormat:@"%d",fileM.id_p];
        NSString    * customId = [NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
        
        IXSupplementFile *file = [[IXSupplementFile alloc] init];
        file.gts2CustomerId = customId;
        file.id = fileId;
        file.fileType = fileM.fileType;
        self.fileModel.files = [[NSMutableArray arrayWithObject:file] mutableCopy];
        [self setBankInfoByBankIndex:bankIdx];
    }
}

- (NSString *)formaterBankNumber:(NSString *)accNo
{
    if (!accNo || accNo.length == 0 || accNo.length < 4) {
        
        return accNo;
    }
    accNo = [accNo substringWithRange:NSMakeRange(accNo.length - 4, 4)];
    return [NSString stringWithFormat:@"**** **** ****%@",accNo];
}

#pragma mark -
#pragma mark - btn action

- (void)onGoback
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submitAccountInfo
{
    [self.view endEditing:YES];
    
    if (!_modifyBankInfo) {
        IXSupplementStyle2Cell *accountCell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        UITableViewCell *bankCell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        
        NSString *account = [accountCell.bankAccountTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *bankName = bankCell.detailTextLabel.text;
        
        if( !_chooseBank || ![account length] || ![bankName length] ){
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"银行卡资料不全")];
            return;
        }
    }
    if (!_accountName.length) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入姓名")];
        return;
    }
    [self uploadImage];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_titleArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 133;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 133)];
    headView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
    
    UILabel *tipTitle = [IXUtils createLblWithFrame:CGRectMake( 0, 26, kScreenWidth, 18)
                                           WithFont:ROBOT_FONT(15)
                                          WithAlign:NSTextAlignmentCenter
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4
                         ];
    tipTitle.text = LocalizedString(@"我的银行卡");
    [headView addSubview:tipTitle];
    
    if( _supplementAccountInfo ){
        
        
        UILabel *rule1 = [IXUtils createLblWithFrame:CGRectMake( 15, 30, kScreenWidth -30, 100)
                                            WithFont:ROBOT_FONT(12)
                                           WithAlign:NSTextAlignmentCenter
                                          wTextColor:0x99abba
                                          dTextColor:0x4c6072
                          ];
        rule1.numberOfLines = 0;
        rule1.text = @"In order to ensure your asset sercurity, you can only bind your own bank account and submit account information as soon as possible.";
        [headView addSubview:rule1];
    }else{
        CGRect frame = tipTitle.frame;
        frame.origin.y = 40;
        tipTitle.frame = frame;
        
        UIImageView *stateImg = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 62)/2, 65, 62, 24)];
        [stateImg setImage:[UIImage imageNamed:@"supAccount_review_bankCard"]];
        [headView addSubview:stateImg];
    }
    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * title = _titleArr[indexPath.row];
    if (SameString(title, LocalizedString(@"姓名")) || SameString(title, LocalizedString(@"账号."))) {
        return [self configureStyle2Cell:tableView WithIndexPath:indexPath];
    }
    
    return [self configureStyle3Cell:tableView WithIndexPath:indexPath];
}

#pragma mark 初始化Cell信息
//银行卡账户和用户名
- (UITableViewCell *)configureStyle2Cell:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    IXSupplementStyle2Cell *cell = [tableView dequeueReusableCellWithIdentifier:
                                    NSStringFromClass([IXSupplementStyle2Cell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tipTitle = _titleArr[indexPath.row];
    
    NSString * title = _titleArr[indexPath.row];
    if (SameString(title, LocalizedString(@"账号."))) {
        //银行卡号可以输入
        cell.bankAccountTF.keyboardType = UIKeyboardTypeNumberPad;
        cell.bankAccountTF.userInteractionEnabled = !_modifyBankInfo;
        cell.bankAccountTF.textFont = PF_MEDI(13);
        cell.bankAccountTF.placeHolderFont = RO_REGU(13);
        cell.bankAccountTF.dk_textColorPicker = _modifyBankInfo ?  DKColorWithRGBs(0x99abba, 0x8395a4) : DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        if (_accountNum && [_accountNum length]) {
            cell.bankAccountTF.text = _accountNum;
        }else{
            cell.bankAccountTF.placeholder = LocalizedString(@"请输入银行卡账号");
        }
    } else if (SameString(title, LocalizedString(@"姓名"))){
        //姓名不能修改
        cell.bankAccountTF.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
//        if (![IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName.length) {
            cell.bankAccountTF.userInteractionEnabled = YES;
            cell.bankAccountTF.placeholder = LocalizedString(@"请输入姓名");
//        } else {
//            cell.bankAccountTF.userInteractionEnabled = NO;
//        }
        cell.bankAccountTF.delegate = self;
        _nameTf = cell.bankAccountTF;
        cell.bankAccountTF.text = _accountName;
    }
    
    weakself;
    cell.textContent = ^(NSString *value){
        if (SameString(title, LocalizedString(@"账号."))) {
            weakSelf.accountNum = value;
            NSLog(@" ----- %@ ----",value);
        }
    };
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

//选择银行
- (UITableViewCell *)configureStyle3Cell:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             NSStringFromClass([UITableViewCell class])];
    if ( !cell ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:NSStringFromClass([UITableViewCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.textLabel.font = PF_MEDI(13);
        cell.textLabel.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        
        cell.detailTextLabel.font = PF_MEDI(13);
        cell.detailTextLabel.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    }
    
    NSString *content = _contentArr[indexPath.row];
    if ([content isEqualToString:LocalizedString(@"请选择银行")] ) {
        cell.detailTextLabel.dk_textColorPicker = DKColorWithRGBs(0xe2e9f1, 0x303b4d);
    }else{
        cell.detailTextLabel.dk_textColorPicker = _modifyBankInfo ?  DKColorWithRGBs(0x99abba, 0x8395a4) : DKColorWithRGBs(0x4c6072, 0xe9e9ea);;
    }
    
    cell.detailTextLabel.text = content;
    cell.textLabel.text = _titleArr[indexPath.row];
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_modifyBankInfo) {
        return;
    }
    
    NSString * title = _titleArr[indexPath.row];
    if (SameString(title, LocalizedString(@"银行"))) {
        //选择银行
        //此处self未强引用vc，因此无需使用weak self
        IXDrawBankListVC *vc = [[IXDrawBankListVC alloc] initWithSelectedInfo:^(IXEnableBankM *bank) {
            self.chooseBank = bank;
            [self.contentArr replaceObjectAtIndex:2 withObject:[bank localizedName]];
            [self.contentTV reloadData];
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _nameTf) {
        _accountName = textField.text;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return  [textField resignFirstResponder];
}

#pragma mark -
#pragma mark - image picker

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    CGSize imagesize = image.size;
//    if (imagesize.width > 320 && imagesize.height > 320) {
//        if (imagesize.width > imagesize.height) {
//            imagesize.width = 320*imagesize.width/imagesize.height;
//            imagesize.height = 320;
//        } else {
//            imagesize.height = 320*imagesize.height/imagesize.width;
//            imagesize.width = 320;
//        }
//    }
//    image = [image scaledToSize:imagesize];
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];

    for (id childView in _contentTV.tableFooterView.subviews) {
        if ([childView isKindOfClass:[UIImageView class]] && [childView tag] == 0) {
            [childView setImage:image];
        }else{
            [childView setHidden:!([childView isKindOfClass:[UIButton class]] && [childView tag] == 1 )];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        //调用摄像头后会导致常亮失效，再此处处理屏幕常亮问题
        [IXUserDefaultM dealScreenNormalLight];
        [IXUserDefaultM dealStatusBarWithAlbumState:NO];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //调用摄像头后会导致常亮失效，再此处处理屏幕常亮问题
    [IXUserDefaultM dealScreenNormalLight];
    [IXUserDefaultM dealStatusBarWithAlbumState:NO];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - setter

- (void)setContentArr:(NSMutableArray *)contentArr
{
    if ( !contentArr || contentArr.count == 0 ) {
        _supplementAccountInfo = YES;
        
        _contentArr = [NSMutableArray array];
        
        [_contentArr addObject:@" "];
        [_contentArr addObject:@" "];
        
        if (_chooseBank) {
            [_contentArr addObject:[_chooseBank localizedName]];
        }else{
            [_contentArr addObject:LocalizedString(@"请选择银行")];
        }
        
    }else{
        _contentArr = [NSMutableArray arrayWithArray:contentArr];
    }
}

- (void)setBankInfoByBankIndex:(int)index
{
    for (int i = 0; i < [IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams.count; i++) {
        IXUserBankM *bank = [IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams[i];
        if (bank.bankOrder == index) {
            [self configBankInfo:bank];
            break;
        }
    }
}

#pragma mark -
#pragma mark - lazy loading

- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight) style:UITableViewStyleGrouped];
        _contentTV.separatorInset = UIEdgeInsetsMake( 0, -10, 0, 0);
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXSupplementStyle1Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSupplementStyle1Cell class])];
        [_contentTV registerClass:[IXSupplementStyle2Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSupplementStyle2Cell class])];
        [self.view addSubview:_contentTV];
        
        _contentTV.tableFooterView = self.footView;
        _contentTV.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
        _contentTV.dk_separatorColorPicker = DKLineColor;
    }
    return _contentTV;
}

- (UIView *)footView
{
    if ( !_footView ) {
        _footView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth,
                                                             self.contentTV.frame.size.height -
                                                             133 -
                                                             44*_titleArr.count)];
        _footView.dk_backgroundColorPicker = DKNavBarColor;
        
        UILabel *tipTitle = [IXUtils createLblWithFrame:CGRectMake( 15, 15, kScreenWidth, 15)
                                               WithFont:PF_MEDI(13)
                                              WithAlign:NSTextAlignmentLeft
                                             wTextColor:0x4c6072
                                             dTextColor:0x8395a4
                             ];
        tipTitle.text = LocalizedString(@"证明文件");
        [_footView addSubview:tipTitle];
        tipTitle.tag = 1;
        
        UIImageView *backImg = [[UIImageView alloc] initWithFrame:CGRectMake( 15, 45, kScreenWidth - 30, 120)];
        [_footView addSubview:backImg];
        [backImg setUserInteractionEnabled:YES];
        backImg.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
        backImg.layer.cornerRadius = 5;
        backImg.layer.masksToBounds = YES;

        UIImageView *markImg = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 108)/2, 56, 108, 69)];
        markImg.dk_imagePicker = DKImageNames(@"supAccount_bankCard", @"supAccount_bankCard_D");
        [_footView addSubview:markImg];
        markImg.tag = 1;
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake( 15, 45, kScreenWidth - 30, 120);
        [btn addTarget:self
                action:@selector(responseToPhoto)
      forControlEvents:UIControlEventTouchUpInside];
        [_footView addSubview:btn];
        btn.tag  = 0;
        
        UIButton *delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        delBtn.frame = CGRectMake( kScreenWidth - 45, 50, 25, 25);
        [delBtn dk_setImage:DKImageNames(@"openAccount_remove_btn", @"openAccount_remove_btn_D") forState:UIControlStateNormal];
        [_footView addSubview:delBtn];
        [delBtn addTarget:self
                   action:@selector(responseToDeletePhoto)
         forControlEvents:UIControlEventTouchUpInside];
        delBtn.hidden = YES;
        delBtn.tag = 1;
        
        UILabel *msgLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 136, kScreenWidth, 15)
                                             WithFont:PF_MEDI(13)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0xa7adb5
                                           dTextColor:0x8395a4
                           ];
        msgLbl.text = LocalizedString(@"上传银行卡正面照片");
        [_footView addSubview:msgLbl];
        
        //处理修改银行卡信息
        if (_bankInfo.imagePath && _bankInfo.imagePath.length && _modifyBankInfo) {
            [backImg sd_setImageWithURL:[NSURL URLWithString:_bankInfo.imagePath]];
            btn.enabled = NO;
            if ([_bankInfo.proposalStatus isEqualToString:@"-1"]) {
                delBtn.hidden = NO;
                UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(responseToPhoto)];
                [backImg addGestureRecognizer:tg];
            } else {
                delBtn.hidden = YES;
                UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(responseToPhoto:)];
                [backImg addGestureRecognizer:tg];
            }
            msgLbl.hidden = YES;
            markImg.hidden = YES;
        }
    }
    return _footView;
}

- (void)responseToPhoto:(UITapGestureRecognizer *)tap
{
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [IXZoomScrollView showFrom:clickedImageView image:clickedImageView.image];
}

#pragma mark -
#pragma mark - other

- (void)configBankInfo:(IXUserBankM *)bank
{
    _bankInfo = bank;
    
    if (bank) {
        _bankModel = [[IXSupplementBankM alloc] init];
        IXSupplementBank *newBank = [[IXSupplementBank alloc] init];
        newBank.bank = bank.bank;
        newBank.bankAccountName = bank.bankAccountName;
        newBank.bankAccountNumber = bank.bankAccountNumber;
        newBank.bankCountry = bank.bankCountry;
        newBank.bankProvince = bank.bankProvince;
        newBank.bankCity = bank.bankCity;
        newBank.bankCurrency = bank.bankCurrency;
        newBank.bankBranch = bank.bankBranch;
        newBank.bankAccountType = bank.bankAccountType;
        newBank.bankAddress = bank.bankAddress;
        newBank.bankOther = bank.bankOther;
        newBank.internationalRemittanceCode = bank.internationalRemittanceCode;
        newBank.id = [NSString stringWithFormat:@"%ld",(long)bank.id_p];
        newBank.bankOrder = bank.bankOrder;
        _bankModel.banks = [[NSMutableArray arrayWithObject:newBank] mutableCopy];
    }
}



//获取当前添加银行卡信息
- (IXSupplementBank *)currentSupplementBank:(int)index
{
    for (int i = 0; i < _bankModel.banks.count; i++) {
        IXSupplementBank *bank = _bankModel.banks[i];
        DLog(@"*** IXSupplementBank bank = %@ *****",bank);
        if (bank.bankOrder == index) {
            return bank;
        }
    }
    return nil;
}

//点击银行卡列表时，保存输入的账号;上传照片，点击任意区域都保存输入的信息
- (void)saveAccount
{
    IXSupplementStyle2Cell * cell = [_contentTV cellForRowAtIndexPath:
                                     [NSIndexPath indexPathForRow:1 inSection:0]];
    _accountNum = cell.bankAccountTF.text;
    [self.view endEditing:YES];
}

//删除准备提交的图片
- (void)responseToDeletePhoto
{
    for ( id childView in _contentTV.tableFooterView.subviews  ) {
        if ( [childView isKindOfClass:[UIImageView class]]  && [childView tag] == 0 ) {
            [(UIImageView *)childView setImage:nil];
        }else{
            [childView setHidden:( [childView isKindOfClass:[UIButton class]] && [childView tag] == 1 )];
        }
    }
}


#pragma mark -
#pragma mark - action sheet

//选中上传类型
- (void)responseToPhoto
{
    [self saveAccount];
    [self.chooseCata show];
}

- (IXOpenChooseContentView *)chooseCata
{
    if ( !_chooseCata ) {
        _chooseCata = [[IXOpenChooseContentView alloc] initWithDataSource:@[
                                                                            LocalizedString(@"选择相机"),
                                                                            LocalizedString(@"选择相册"),
                                                                            ]
                                                          WithSelectedRow:^(NSInteger row)
                       {
                           UIImagePickerController * picker = [[UIImagePickerController alloc] init];
                           picker.allowsEditing = NO;
                           switch (row) {
                               case 0:{
                                   if ([IXAppUtil cameraEnable]) {
                                       picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                       picker.delegate = self;
                                       [self presentViewController:picker animated:YES completion:nil];
                                   }
                               }
                                   break;
                               case 1:{
                                   if ([IXAppUtil albumEnable]) {
                                       [IXUserDefaultM dealStatusBarWithAlbumState:YES];
                                       
                                       picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                       picker.delegate = self;
                                       picker.navigationBar.translucent = NO;
                                       [self presentViewController:picker animated:YES completion:nil];
                                   }
                               }
                                   break;
                               default:
                                   break;
                           }
                       }];
    }
    return _chooseCata;
}


#pragma mark -
#pragma mark - request

//提交图片信息
- (void)uploadImage
{
    NSData *imageData;
    for ( id childView in _contentTV.tableFooterView.subviews ) {
        if ( [childView isKindOfClass:[UIImageView class]] ) {
            UIImage * img = [(UIImageView *)childView image];
            imageData = [img compressWidthWidth:0 aimLength:50 accuracyOfLength:20];
            break;
        }
    }
    
    if (imageData) {
        NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
        NSString *fileName = [NSString stringWithFormat:@"%@.png",[timeStamp stringValue]];
        
        NSDictionary *param = @{@"imageData":imageData,
                                @"timeStamp":timeStamp,
                                @"fileName":fileName};
        
        [SVProgressHUD showWithStatus:LocalizedString(@"上传银行卡中...")];
        
        [IXBORequestMgr acc_uploadBankPhotoWithParam:param
                                          WithResult:^(BOOL success,
                                                       NSString *errCode,
                                                       NSString *errStr,
                                                       id obj)
         {
             if (success) {
                 if (obj && [obj ix_isDictionary] && obj[@"fileStorePath"] && obj[@"webFilePath"]) {
                     [SVProgressHUD showSuccessWithStatus:@"上传银行卡成功"];
                     
                     if (self.fileModel.files && self.fileModel.files.count > 0) {
                         IXSupplementFile *file = self.fileModel.files[0];
                         file.filePath = [obj stringForKey:@"fileStorePath"];
                         file.ftpFilePath = [obj stringForKey:@"webFilePath"];
                         file.fileName = fileName;
                         self.fileModel.files = [[NSMutableArray arrayWithObject:file] mutableCopy];
                         [self postBankFileInfo];
                         
                         return ;
                     }
                 }
             }
             [SVProgressHUD showErrorWithStatus:LocalizedString(@"上传银行卡失败")];
         }];
        
    }else{
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"银行卡资料不全")];
    }
}

//提交银行卡文件信息
- (void)postBankFileInfo
{
    if ( !_fileModel.files || _fileModel.files.count == 0 ) {
        return;
    }
    IXSupplementFile *file = _fileModel.files[0];
    NSDictionary *param = @{
                            @"gts2CustomerId":file.gts2CustomerId,
                            NSStringFromClass([IXSupplementFileM class]):_fileModel
                            };
    
    
    [SVProgressHUD showWithStatus:LocalizedString(@"提交银行卡文件中...")];
    [IXBORequestMgr acc_uploadAttachWithParam:param
                                   WithResult:^(BOOL success,
                                                NSString *errCode,
                                                NSString *errStr,
                                                id obj)
     {
         if (success) {
             [SVProgressHUD showSuccessWithStatus:LocalizedString(@"提交银行卡文件成功")];
             if (!_modifyBankInfo) {
                 [self postBankInfo];
             } else {
                 [self.view endEditing:YES];
                 [self.navigationController popViewControllerAnimated:YES];
             }
         } else {
             if (errStr.length) {
                 [SVProgressHUD showMessage:errStr];
             } else {
                 [SVProgressHUD showErrorWithStatus:LocalizedString(@"提交银行卡文件失败")];
             }
         }
     }];
    
}

//提交银行卡信息
- (void)postBankInfo
{
    NSInteger   bankIdx = [[IXBORequestMgr shareInstance].userInfo.detailInfo currentAddBankSequence];
    DLog(@"提交银行卡信息***** bankIdx = %d ***** ",bankIdx);
    IXSupplementBank *bank = [self currentSupplementBank:bankIdx];
    bank.bank = _chooseBank.code;
    IXSupplementStyle2Cell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    bank.bankAccountName = cell.bankAccountTF.text;
    cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    bank.bankAccountNumber = cell.bankAccountTF.text;
    bank.bankProvince = @"";
    bank.bankCity = @"";
    
    NSString    * gtsId = [NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
    NSDictionary    * param = @{
                                @"gts2CustomerId":gtsId,
                                NSStringFromClass([IXSupplementBankM class]):_bankModel
                                };
    
    [SVProgressHUD showWithStatus:LocalizedString(@"数据提交中...")];
    
    [IXBORequestMgr acc_modifyBankInfoWithParam:param
                                     WithResult:^(BOOL success,
                                                  NSString *errCode,
                                                  NSString *errStr,
                                                  id obj)
     {
         if (success) {
             [SVProgressHUD showSuccessWithStatus:LocalizedString(@"提交银行卡信息成功")];
             IXSupplementAccountResultVC *resultVC = [[IXSupplementAccountResultVC alloc] init];
             resultVC.bankCode = bank.bank;
             if (_showResultPage) {
                 [self.navigationController pushViewController:resultVC animated:YES];
                 [IXBORequestMgr refreshUserInfo:nil];
             } else {
                 [self.navigationController popViewControllerAnimated:YES];
             }
             if (self.addBankBlock) {
                 self.addBankBlock(bank.bankAccountName);
             }
         } else {
             [SVProgressHUD showErrorWithStatus:LocalizedString(@"提交银行卡信息失败")];
         }
     }];
}

@end
