//
//  IXSupplementAccountResultVC.h
//  IXApp
//
//  Created by Bob on 2017/2/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXDataBaseVC.h"

@interface IXSupplementAccountResultVC : IXDataBaseVC
@property (nonatomic, strong) NSString *bankCode;
@end
