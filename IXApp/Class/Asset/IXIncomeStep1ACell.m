//
//  IXIncomeStep1ACell.m
//  IXApp
//
//  Created by Evn on 17/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeStep1ACell.h"
#import "IXBOModel.h"

@interface IXIncomeStep1ACell()

@property (nonatomic,strong)UILabel     *desc;
@property (nonatomic,strong)UIImageView *sIcon;

@end
@implementation IXIncomeStep1ACell

- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(14.5, 13, 180, 18);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _desc.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (UIImageView *)sIcon
{
    if (!_sIcon) {
        CGRect rect = CGRectMake(kScreenWidth - (16 + 15), 14, 16, 16);
        _sIcon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_sIcon];
    }
    return _sIcon;
}

- (void)reloadUIWithData:(IXPayMethod *)method
                isSelect:(BOOL)flag{
    self.desc.text = [method localizedName];

    if (flag) {
       [self.sIcon setImage:GET_IMAGE_NAME(@"openAccount_complete")];
    }else{
        [self.sIcon setImage:GET_IMAGE_NAME(@"incomeStep_unChoose")];
    }
    
    self.dk_backgroundColorPicker = DKNavBarColor;
}

@end
