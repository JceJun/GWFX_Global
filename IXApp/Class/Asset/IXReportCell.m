//
//  IXReportCell.m
//  IXApp
//
//  Created by Evn on 17/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXReportCell.h"
#import "IXAppUtil.h"
#import "CAGradientLayer+IX.h"

@interface IXReportCell()

@property (nonatomic, strong)UIImageView *bgView;
@property (nonatomic, strong)UIView *bottomBgView;
@property (nonatomic, strong)UILabel *state;
@property (nonatomic, strong)UILabel *amount;
@property (nonatomic, strong)UILabel *showDate;
@property (nonatomic, strong)UILabel *date;
@property (nonatomic, strong)UILabel *time;
@property (nonatomic, strong)UILabel *showCurrency;
@property (nonatomic, strong)UILabel *currency;
@property (nonatomic, strong)UILabel *showTradeNo;
@property (nonatomic, strong)UILabel *tradeNo;

@end

@implementation IXReportCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addShadow];
    }
    return self;
}

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, kScreenWidth, 133)];
        _bgView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)state
{
    if (!_state) {
        _state = [IXCustomView createLable:CGRectMake(15,
                                                      16,
                                                      (VIEW_W(self.bgView) - 30)/2,
                                                      21)
                                     title:@""
                                      font:PF_MEDI(13)
                                wTextColor:0x4c6072
                                dTextColor:0xe9e9ea
                             textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_state];
    }
    return _state;
}

- (UILabel *)amount
{
    if (!_amount) {
        _amount = [IXCustomView createLable:CGRectMake(15 + VIEW_W(_state),
                                                       17,
                                                       VIEW_W(_state),
                                                       15)
                                      title:@""
                                       font:RO_REGU(15)
                                 wTextColor:0x4c6072
                                 dTextColor:0x8395a4
                              textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_amount];
    }
    return _amount;
}

- (UIView *)bottomBgView
{
    if (!_bottomBgView) {
        _bottomBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, kScreenWidth, 90)];
        [self.bgView addSubview:_bottomBgView];
    }
    return _bottomBgView;
}

- (UILabel *)showDate
{
    if (!_showDate) {
        _showDate = [IXCustomView createLable:CGRectMake(15,
                                                         15,
                                                         (VIEW_W(_bgView) - 4*15)/3,
                                                         15)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x4c6072
                                   dTextColor:0x8395a4
                                textAlignment:NSTextAlignmentLeft];
        [self.bottomBgView addSubview:_showDate];
    }
    return _showDate;
}

- (UILabel *)date
{
    if (!_date) {
        _date = [IXCustomView createLable:CGRectMake(15,
                                                     GetView_MaxY(_showDate) + 9.5,
                                                     VIEW_W(_showDate),
                                                     15)
                                    title:@""
                                     font:RO_REGU(15)
                               wTextColor:0x99abba
                               dTextColor:0x8395a4
                            textAlignment:NSTextAlignmentLeft];
        [self.bottomBgView addSubview:_date];
    }
    return _date;
}

- (UILabel *)time
{
    if (!_time) {
        _time = [IXCustomView createLable:CGRectMake(15,
                                                     GetView_MaxY(_date) + 9.5,
                                                     VIEW_W(_showDate),
                                                     15)
                                    title:@""
                                     font:RO_REGU(15)
                               wTextColor:0x99abba
                               dTextColor:0x8395a4
                            textAlignment:NSTextAlignmentLeft];
        [self.bottomBgView addSubview:_time];
    }
    return _time;
}
- (UILabel *)showCurrency
{
    if (!_showCurrency) {
        _showCurrency = [IXCustomView createLable:CGRectMake(30 + VIEW_W(_showDate),
                                                             VIEW_Y(_showDate),
                                                             VIEW_W(_showDate),
                                                             15)
                                            title:@""
                                             font:PF_MEDI(13)
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentLeft];
        [self.bottomBgView addSubview:_showCurrency];
    }
    return _showCurrency;
}

- (UILabel *)currency
{
    if (!_currency) {
        _currency = [IXCustomView createLable:CGRectMake(VIEW_X(_showCurrency),
                                                         VIEW_Y(_date),
                                                         VIEW_W(_date),
                                                         15)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x99abba
                                   dTextColor:0x8395a4
                                textAlignment:NSTextAlignmentLeft];
        [self.bottomBgView addSubview:_currency];
    }
    return _currency;
}

- (UILabel *)showTradeNo
{
    if (!_showTradeNo) {
        _showTradeNo = [IXCustomView createLable:CGRectMake(15 + VIEW_W(_showDate)*2,
                                                            VIEW_Y(_showDate),
                                                            VIEW_W(_showDate) + 30,
                                                            15)
                                           title:@""
                                            font:PF_MEDI(13)
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentLeft];
        [self.bottomBgView addSubview:_showTradeNo];
    }
    return _showTradeNo;
}


- (UILabel *)tradeNo
{
    if (!_tradeNo) {
        _tradeNo = [IXCustomView createLable:CGRectMake(VIEW_X(_showTradeNo),
                                                        VIEW_Y(_date),
                                                        VIEW_W(_date) + 30,
                                                        15)
                                       title:@""
                                        font:RO_REGU(15)
                                  wTextColor:0x99abba
                                  dTextColor:0x8395a4
                               textAlignment:NSTextAlignmentLeft];
        [self.bottomBgView addSubview:_tradeNo];
    }
    return _tradeNo;
}

- (void)addShadow
{
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * topL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 5)
                                                        dkcolors:topColors];
    [self.contentView.layer addSublayer:topL];
    
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 133, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [self.contentView.layer addSublayer:btomL];
    
    self.layer.masksToBounds = NO;
    self.contentView.layer.masksToBounds = NO;
}

- (void)reloadUIData:(NSDictionary *)dataDic index:(NSInteger)index
{
    if (index == 0) {
        if ([[dataDic stringForKey:@"status"] caseInsensitiveCompare:@"fail"] == NSOrderedSame) {
            self.state.text = LocalizedString(@"入金失败");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xfbf1f2, 0x2c2b35);
        } else if ([[dataDic stringForKey:@"status"] caseInsensitiveCompare:@"success"] == NSOrderedSame){
            self.state.text = LocalizedString(@"入金成功");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        } else {
            self.state.text = LocalizedString(@"待处理");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xfbf1f2, 0x2c2b35);
        }
        self.amount.text = [NSString stringWithFormat:@"+%@",[IXAppUtil amountToString:[NSString stringWithFormat:@"%.2f",[[dataDic stringForKey:@"transAmount"]doubleValue]]]];
        self.amount.dk_textColorPicker = DKColorWithRGBs(0xff4653, 0xff4d2d);
    } else {
        if ([dataDic[@"agentCode"] isEqual:@"fail"]) {
            self.state.text = LocalizedString(@"出金失败");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xfbf1f2, 0x2c2b35);
        } else if ([dataDic[@"agentCode"] isEqual:@"wait"]) {
            self.state.text = LocalizedString(@"待处理");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xfbf1f2, 0x2c2b35);
        } else if ([dataDic[@"agentCode"] isEqual:@"success"]) {
            self.state.text = LocalizedString(@"出金成功");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        } else if ([dataDic[@"agentCode"] isEqual:@"cancel"]) {
            self.state.text = LocalizedString(@"已取消");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xfbf1f2, 0x2c2b35);
        } else {
            self.state.text = LocalizedString(@"出金失败");
            self.bottomBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xfbf1f2, 0x2c2b35);
        }
        self.amount.text = [NSString stringWithFormat:@"-%@",[IXAppUtil amountToString:[NSString stringWithFormat:@"%.2f",[[dataDic stringForKey:@"transAmount"]doubleValue]]]];
        self.amount.dk_textColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
    }
    self.showDate.text = LocalizedString(@"日期");
    id dateDic = dataDic[@"preApproveDate"];
    if ( !(dateDic && [dateDic isKindOfClass:[NSDictionary class]]) ) {
        dateDic = dataDic[@"createDate"];
    }
    NSString *time = [IXEntityFormatter timeIntervalToString:[dateDic[@"time"] longLongValue]/1000];
    NSArray *timeArr = [time componentsSeparatedByString:@" "];
    if (timeArr.count == 2) {
        self.date.text = [NSString stringWithFormat:@"%@",timeArr[0]];
        self.time.text = [NSString stringWithFormat:@"%@",timeArr[1]];
    }
    self.showCurrency.text = LocalizedString(@"币种");
    self.currency.text = [NSString stringWithFormat:@"%@",dataDic[@"accountCurrency"]];
    self.showTradeNo.text = LocalizedString(@"交易编号");
    self.tradeNo.text =[NSString stringWithFormat:@"%@",dataDic[@"pno"]] ;
}
@end

