//
//  IXSubRecordCell.m
//  IXApp
//
//  Created by Evn on 17/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubRecordCell.h"

@interface IXSubRecordCell()
@property (nonatomic, strong)UIImageView *bgView;
@property (nonatomic,strong)UILabel *title;
@property (nonatomic, strong)UILabel *amount;
@property (nonatomic, strong)UILabel *showDate;
@property (nonatomic, strong)UILabel *date;
@property (nonatomic, strong)UILabel *time;
@property (nonatomic, strong)UILabel *showResult;
@property (nonatomic, strong)UILabel *result;
@property (nonatomic, strong)UILabel *showValid;
@property (nonatomic, strong)UILabel *valid;
@end

@implementation IXSubRecordCell

- (UIImageView *)bgView {
    
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(9, 5, kScreenWidth - 2*9.5, 138)];
        UIImage *image = GET_IMAGE_NAME(@"recordCell_success");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:0];
        [_bgView setImage:image];
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)title {
    
    if (!_title) {
        
        _title = [IXCustomView createLable:CGRectMake(15,
                                                      15,
                                                      (VIEW_W(self.bgView) - 30)/2,
                                                      15)
                                     title:@""
                                      font:PF_MEDI(13)
                                wTextColor:0x4c6072
                                dTextColor:0xff0000
                             textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_title];
    }
    return _title;
}

- (UILabel *)amount {
    
    if (!_amount) {
        
        _amount = [IXCustomView createLable:CGRectMake(15 + VIEW_W(_title),
                                                       15,
                                                       VIEW_W(_title),
                                                       15)
                                      title:@""
                                       font:PF_MEDI(13)
                                 wTextColor:0x4c6072
                                 dTextColor:0xff0000
                              textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_amount];
    }
    return _amount;
}
- (UILabel *)showDate {
    
    if (!_showDate) {
        
        _showDate = [IXCustomView createLable:CGRectMake(15,
                                                         GetView_MaxY(_title) + 30,
                                                         (VIEW_W(_bgView) - 4*15)/3,
                                                         15)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x99abba
                                   dTextColor:0xff0000
                                textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showDate];
    }
    return _showDate;
}

- (UILabel *)date {
    
    if (!_date) {
        
        _date = [IXCustomView createLable:CGRectMake(15,
                                                     GetView_MaxY(_showDate) + 9.5,
                                                     VIEW_W(_showDate),
                                                     15)
                                    title:@""
                                     font:PF_MEDI(13)
                               wTextColor:0x99abba
                               dTextColor:0xff0000
                            textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_date];
    }
    return _date;
}

- (UILabel *)time {
    
    if (!_time) {
        
        _time = [IXCustomView createLable:CGRectMake(15,
                                                     GetView_MaxY(_date) + 9.5,
                                                     VIEW_W(_showDate),
                                                     15)
                                    title:@""
                                     font:PF_MEDI(13)
                               wTextColor:0x99abba
                               dTextColor:0xff0000
                            textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_time];
    }
    return _time;
}

- (UILabel *)showResult {
    
    if (!_showResult) {
        
        _showResult = [IXCustomView createLable:CGRectMake(30 + VIEW_W(_showDate),
                                                           VIEW_Y(_showDate),
                                                           VIEW_W(_showDate),
                                                           15)
                                          title:@""
                                           font:PF_MEDI(13)
                                     wTextColor:0x99abba
                                     dTextColor:0xff0000
                                  textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showResult];
    }
    return _showResult;
}

- (UILabel *)result {
    
    if (!_result) {
        
        _result = [IXCustomView createLable:CGRectMake(VIEW_X(_showResult),
                                                       VIEW_Y(_date),
                                                       VIEW_W(_date),
                                                       15)
                                      title:@""
                                       font:PF_MEDI(13)
                                 wTextColor:0x99abba
                                 dTextColor:0xff0000
                              textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_result];
    }
    return _result;
}

- (UILabel *)showValid {
    
    if (!_showValid) {
        
        _showValid = [IXCustomView createLable:CGRectMake(15 + VIEW_W(_showDate)*2,
                                                          VIEW_Y(_showDate),
                                                          VIEW_W(_showDate) + 30,
                                                          15)
                                         title:@""
                                          font:PF_MEDI(13)
                                    wTextColor:0x99abba
                                    dTextColor:0xff0000
                                 textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showValid];
    }
    return _showValid;
}


- (UILabel *)valid {
    
    if (!_valid) {
        
        _valid = [IXCustomView createLable:CGRectMake(VIEW_X(_showValid),
                                                      VIEW_Y(_date),
                                                      VIEW_W(_date) + 30,
                                                      15)
                                     title:@""
                                      font:PF_MEDI(13)
                                wTextColor:0x99abba
                                dTextColor:0xff0000
                             textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_valid];
    }
    return _valid;
}

- (void)reloadUIData:(NSDictionary *)dataDic index:(NSInteger)index {
    
    if (index == 0) {
        
        if ([dataDic[@"success"] isEqual:@0]) {
            
            self.title.text = LocalizedString(@"入金失败");
            UIImage *image = GET_IMAGE_NAME(@"recordCell_failure");
            image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:0];
            [_bgView setImage:image];
        } else {
            
            self.title.text = LocalizedString(@"入金成功");
            UIImage *image = GET_IMAGE_NAME(@"recordCell_success");
            image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:0];
            [_bgView setImage:image];
        }
        
        self.amount.text = [NSString stringWithFormat:@"+%@",dataDic[@"transAmount"]];
        self.amount.textColor = UIColorHexFromRGB(0xff4653);
        
    } else {
        
        if ([dataDic[@"success"] isEqual:@0]) {
            
            self.title.text = LocalizedString(@"出金失败");
            UIImage *image = GET_IMAGE_NAME(@"recordCell_failure");
            image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:0];
            [_bgView setImage:image];
        } else {
            
            self.title.text = LocalizedString(@"出金成功");
            UIImage *image = GET_IMAGE_NAME(@"recordCell_success");
            image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:0];
            [_bgView setImage:image];
        }
        
        self.amount.text = [NSString stringWithFormat:@"-%@",dataDic[@"transAmount"]];
        self.amount.textColor = UIColorHexFromRGB(0x11b873);
    }
    self.showDate.text = LocalizedString(@"日期");
    
    id dateDic = dataDic[@"preApproveDate"];
    if ( !(dateDic && [dateDic isKindOfClass:[NSDictionary class]]) ) {
        
        dateDic = dataDic[@"createDate"];
    }
    NSString *time = [IXEntityFormatter timeIntervalToString:[dateDic[@"time"] longLongValue]/1000];
    NSArray *timeArr = [time componentsSeparatedByString:@" "];
    if (timeArr.count == 2) {
        self.date.text = [NSString stringWithFormat:@"%@",timeArr[0]];
        self.time.text = [NSString stringWithFormat:@"%@",timeArr[1]];
    }
}

@end
