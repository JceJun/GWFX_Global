//
//  IXSubRecordCell.h
//  IXApp
//
//  Created by Evn on 17/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXSubRecordCell : UITableViewCell
- (void)reloadUIData:(NSDictionary *)dataDic index:(NSInteger)index;
@end
