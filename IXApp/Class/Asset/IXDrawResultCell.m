//
//  IXDrawResultCell.m
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawResultCell.h"
#import "IXBORequestMgr+Comp.h"
@interface IXDrawResultCell ()

@property (nonatomic, strong) UIImageView *topImg;
@property (nonatomic, strong) UIImageView *centerImg;
@property (nonatomic, strong) UIImageView *bottomImg;

@property (nonatomic, strong) UILabel *timeLbl;
@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation IXDrawResultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        self.contentView.dk_backgroundColorPicker = DKNavBarColor;
        
        self.topImg.image = [UIImage imageNamed:@"income_process"];
        self.centerImg.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x4c6072);
        [self.bottomImg dk_setImagePicker:
         DKImagePickerWithNames(@"common_cell_unchoose",@"common_cell_unchoose_D")];
    }
    return self;
}

- (void)setSubmitTime:(NSString *)submitTime
{
    self.timeLbl.text = [NSString stringWithFormat:@"%@ %@",submitTime,LocalizedString(@"申请已提交")];
    
    CGSize  size = [self.timeLbl sizeThatFits:CGSizeMake(self.timeLbl.bounds.size.width, MAXFLOAT)];
    self.timeLbl.frame = CGRectMake( 50, 16, kScreenWidth - 60, size.height);
    self.timeLbl.center = CGPointMake(self.timeLbl.center.x, self.topImg.center.y);
}

- (void)setTipMsg:(NSString *)tipMsg
{
//    self.tipLbl.text = tipMsg;
    NSString *bankType = [IXBORequestMgr shareInstance].paramDic[@"WithDrawCardType"];
    NSString *dateTips = @"";
    if ([bankType isEqualToString:@"Help2pay"]) {
        dateTips = [IXBORequestMgr shareInstance].mobileOnlineConfig[@"help2payPaymentDateTips"];
    }else{
        dateTips = [IXBORequestMgr shareInstance].mobileOnlineConfig[@"transferPaymentDateTips"];
    }
    
    tipMsg =  [NSString stringWithFormat:@"Expected %@ back to your account",dateTips];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:tipMsg];
    NSRange rangeStart = [tipMsg rangeOfString:dateTips];

    NSRange rangeStop;
    NSInteger length = 0;

    length = [dateTips length];
    rangeStop = [tipMsg rangeOfString:dateTips];
    
    if( rangeStart.location != NSNotFound &&
       rangeStop.location != NSNotFound ){
        if ( [IXUserInfoMgr shareInstance].isNightMode ) {
            [string addAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0x21ce99)}
                            range:NSMakeRange( rangeStart.location,length)];
        }else{
            [string addAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0x11b873)}
                            range:NSMakeRange( rangeStart.location,length)];
        }
    }
    self.tipLbl.attributedText = string;
    
    CGSize  size = [self.timeLbl sizeThatFits:CGSizeMake(self.tipLbl.bounds.size.width, MAXFLOAT)];
    self.tipLbl.frame = CGRectMake( 50, 16, kScreenWidth - 60, size.height);
    self.tipLbl.center = CGPointMake(self.timeLbl.center.x, self.bottomImg.center.y);
}







- (UILabel *)timeLbl
{
    if ( !_timeLbl ) {
        _timeLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 16, kScreenWidth - 60, 15)
                                      WithFont:RO_REGU(13)
                                     WithAlign:NSTextAlignmentLeft
                                    wTextColor:0x4c6072
                                    dTextColor:0xe9e9ea];
        _timeLbl.numberOfLines = 0;
        [self.contentView addSubview:_timeLbl];
    }
    return _timeLbl;
}


- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 68, kScreenWidth - 60, 15)
                                      WithFont:PF_MEDI(12)
                                     WithAlign:NSTextAlignmentLeft
                                   wTextColor:0xa7adb5
                                   dTextColor:0x4c6072];
        _tipLbl.numberOfLines = 0;
        [self.contentView addSubview:_tipLbl];
    }
    return _tipLbl;
}

- (UIImageView *)topImg
{
    if ( !_topImg ) {
        _topImg = [[UIImageView alloc] initWithFrame:CGRectMake( 15, 8, 25, 25)];
        [self.contentView addSubview:_topImg];
    }
    return _topImg;
}


- (UIImageView *)centerImg
{
    if ( !_centerImg ) {
        _centerImg = [[UIImageView alloc] initWithFrame:CGRectMake( 26, 33, 3, 30)];
        [self.contentView addSubview:_centerImg];
    }
    return _centerImg;
}


- (UIImageView *)bottomImg
{
    if ( !_bottomImg ) {
        _bottomImg = [[UIImageView alloc] initWithFrame:CGRectMake( 15, 63, 25, 25)];
        [self.contentView addSubview:_bottomImg];
    }
    return _bottomImg;
}


@end
