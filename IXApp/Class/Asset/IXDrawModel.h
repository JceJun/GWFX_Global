//
//  IXDrawModel.h
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXDrawBankModel.h"

@interface IXDrawModel : NSObject

@property (nonatomic, strong) NSArray *titleArr;

@property (nonatomic, strong) NSArray *contentArr;

@property (nonatomic, strong) NSString *balance;

@property (nonatomic, strong) NSString *gts2AccountId;

@property (nonatomic, strong) NSString *gts2CustomerId;

@property (nonatomic, strong) NSString *accountGroupId;

@property (nonatomic, strong) NSMutableArray *bankInfoArr;

@property (nonatomic, strong) NSMutableArray *bankNameArr;

@property (nonatomic, strong) NSString *showBankAccountNumber;

@property(nonatomic,copy) NSString *hasDeposit;

@property (nonatomic, strong) IXDrawBankModel *model;

@end
