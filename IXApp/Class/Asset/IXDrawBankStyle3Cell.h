//
//  IXDrawBankStyle3Cell.h
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDrawBankStyle3Cell : UITableViewCell

@property (nonatomic, strong) NSString *leftTitle;

@property (nonatomic, strong) NSString *rightContent;

@property (nonatomic, strong) UILabel *rightContentLbl;

@end
