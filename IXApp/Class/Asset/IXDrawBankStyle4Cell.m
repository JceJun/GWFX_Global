//
//  IXDrawBankStyle4Cell.m
//  IXApp
//
//  Created by Evn on 2017/9/26.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawBankStyle4Cell.h"

@interface IXDrawBankStyle4Cell()

@property (nonatomic,strong)UIImageView *icon;
@property (nonatomic,strong)UILabel     *desc;

@end

@implementation IXDrawBankStyle4Cell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self icon];
    [self desc];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UIImageView *)icon
{
    if (!_icon) {
        CGRect rect = CGRectMake((kScreenWidth - 109)/2.f, 50.f, 109.f, 77.f);
        _icon = [[UIImageView alloc] initWithFrame:rect];
        _icon.dk_imagePicker = DKImageNames(@"income_no_bank", @"income_no_bank_D");
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_icon) + 20, kScreenWidth, 15.f);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.textAlignment = NSTextAlignmentCenter;
        _desc.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4c6072);
        _desc.text = LocalizedString(@"没有绑定银行卡");
        [self.contentView addSubview:_desc];
    }
    return _desc;
}


@end
