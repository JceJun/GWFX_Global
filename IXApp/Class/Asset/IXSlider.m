//
//  IXSlider.m
//  IXApp
//
//  Created by Evn on 17/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSlider.h"

@implementation IXSlider

- (CGRect)trackRectForBounds:(CGRect)bounds {
    return CGRectMake(0,18, kScreenWidth - 16.5*2, 4);
}

@end
