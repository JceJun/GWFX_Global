//
//  IXBankCell.m
//  IXApp
//
//  Created by Evn on 17/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBankCell.h"

@interface IXBankCell()

@property (nonatomic, strong)UILabel *titleLbl;
@property (nonatomic, strong)UIView *lineView;
@property (nonatomic, strong)UIButton *bgBtn;
@property (nonatomic, strong)UIButton *iconBtn;

@end

@implementation IXBankCell

- (void)layoutSubviews {
    
    [super layoutSubviews];
    [self lineView];
}

- (UILabel *)titleLbl {
    
    if (!_titleLbl) {        
        _titleLbl = [IXCustomView createLable:CGRectMake(15,12, kScreenWidth - 30, 20)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x4c6072
                                   dTextColor:0xff0000
                                textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

- (UIButton *)bgBtn {
    
    if (!_bgBtn) {
        
        _bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 70, 0, 70, 44)];
        [self.contentView addSubview:_bgBtn];
    }
    return _bgBtn;
}

- (UIButton *)iconBtn {
    
    if (!_iconBtn) {
        
        _iconBtn = [[UIButton alloc] initWithFrame:CGRectMake(VIEW_W(_bgBtn)- 31, 14, 16, 16)];
        [_iconBtn setImage:GET_IMAGE_NAME(@"common_cell_choose") forState:UIControlStateNormal];
        [self.bgBtn addSubview:_iconBtn];
    }
    
    return _iconBtn;
}


- (UIView *)lineView {
    
    if (!_lineView) {
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
        _lineView.backgroundColor = CellSepLineColor;
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)configTitle:(NSString *)title selectState:(BOOL)flag {
    
    self.titleLbl.text = title;
    if (flag) {
        
        self.bgBtn.hidden = NO;
        self.iconBtn.hidden = NO;
    } else {
        
        self.bgBtn.hidden = YES;
        self.iconBtn.hidden = YES;
    }
}


@end
