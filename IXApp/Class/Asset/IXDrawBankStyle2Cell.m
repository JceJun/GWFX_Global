//
//  IXDrawBankStyle2Cell.m
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawBankStyle2Cell.h"

@interface IXDrawBankStyle2Cell ()

@property (nonatomic, strong) UILabel *aboutTitleLbl;
@property (nonatomic, strong) UILabel *aboutContentLbl;

@property (nonatomic, strong) UILabel *rateTitleLbl;
@property (nonatomic, strong) UILabel *rateContentLbl;


@end

@implementation IXDrawBankStyle2Cell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        [self.contentView addSubview:self.aboutTitleLbl];
        [self.contentView addSubview:self.rateTitleLbl];
    }
    return self;
}

- (UILabel *)aboutTitleLbl
{
    if ( !_aboutTitleLbl ) {
        CGFloat width = [IXDataProcessTools textSizeByText:@"约折合" height:15 font:PF_MEDI(13)].width;
        _aboutTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 9, width, 15)
                                            WithFont:PF_MEDI(13)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0x4c6072
                                          dTextColor:0xff0000];
    }
    _aboutTitleLbl.text = @"约折合";
    return _aboutTitleLbl;
}


- (UILabel *)aboutContentLbl
{
    if ( !_aboutContentLbl ) {
        _aboutContentLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 9, 200, 15)
                                              WithFont:PF_MEDI(13)
                                             WithAlign:NSTextAlignmentLeft
                                            wTextColor:0x4c6072
                                            dTextColor:0xff0000];
        [self.contentView addSubview:_aboutContentLbl];
    }
    return _aboutContentLbl;
}


- (UILabel *)rateTitleLbl
{
    if ( !_rateTitleLbl ) {
        CGFloat width = [IXDataProcessTools textSizeByText:@"今日汇率" height:15 font:PF_MEDI(13)].width;
        _rateTitleLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth/2, 9, width, 15)
                                           WithFont:PF_MEDI(13)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0x4c6072
                                         dTextColor:0xff0000];
    }
    _rateTitleLbl.text = @"今日汇率";
    return _rateTitleLbl;
}


- (UILabel *)rateContentLbl
{
    if ( !_rateContentLbl ) {
        _rateContentLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 9, 200, 15)
                                            WithFont:PF_MEDI(13)
                                           WithAlign:NSTextAlignmentLeft
                                           wTextColor:0x4c6072
                                          dTextColor:0xff0000];
        [self.contentView addSubview:_rateContentLbl];
    }
    return _rateContentLbl;
}

- (void)setMoneyInfo:(NSArray *)moneyInfo
{
    self.aboutContentLbl.text = moneyInfo[0];
    CGRect frame = _aboutContentLbl.frame;
    frame.origin.x = CGRectGetMaxX(_aboutTitleLbl.frame) + 6;
    frame.size.width = [IXDataProcessTools textSizeByText:moneyInfo[0] height:15 font:PF_MEDI(13)].width;
    _aboutContentLbl.frame = frame;
    
    frame = _rateTitleLbl.frame;
    frame.origin.x = CGRectGetMaxX(_aboutContentLbl.frame) + 34;
    _rateTitleLbl.frame = frame;
    
    self.rateContentLbl.text = moneyInfo[1];
    frame = _rateContentLbl.frame;
    frame.origin.x = CGRectGetMaxX( _rateTitleLbl.frame ) + 9;
    frame.size.width = [IXDataProcessTools textSizeByText:moneyInfo[1] height:15 font:PF_MEDI(13)].width;
    _rateContentLbl.frame = frame;
}


@end
