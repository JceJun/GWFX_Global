//
//  IXDrawFeeCell.m
//  IXApp
//
//  Created by Evn on 2017/9/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawFeeCell.h"

@interface IXDrawFeeCell()

@property (nonatomic, strong)UILabel *rule;
@property (nonatomic, strong)UIImageView *icon;

@end

@implementation IXDrawFeeCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.dk_backgroundColorPicker = DKNavBarColor;
    [self rule];
    [self icon];
}

- (UILabel *)rule
{
    if ( !_rule ) {
        _rule = [IXUtils createLblWithFrame:CGRectMake( 15, 9, 20, 19)
                                           WithFont:PF_MEDI(13)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4
                         ];
        _rule.text = LocalizedString(@"手续费规则");
        CGFloat width = [IXEntityFormatter getContentWidth:_rule.text WithFont:PF_MEDI(13)];
        _rule.frame = CGRectMake(15, 10, width, 15);
        [self.contentView addSubview:_rule];
    }
    return _rule;
}

- (UIImageView *)icon
{
    if ( !_icon ) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(GetView_MaxX(self.rule) + 5, 10, 13, 13)];
        [_icon dk_initWithImagePicker:DKImageNames(@"draw_about", @"draw_about_D")];
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

@end
