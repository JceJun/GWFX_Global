//
//  IXSupplementStyle2Cell.h
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

typedef void(^endInputValue)(NSString *value);

@protocol IXSupplementStyle2CellDelegate <NSObject>

- (void)textFieldValue:(NSString *)value tag:(NSInteger)tag;

@end

@interface IXSupplementStyle2Cell : UITableViewCell

@property (nonatomic, assign)id <IXSupplementStyle2CellDelegate> delegate;
@property (nonatomic, copy) NSString *tipTitle;

@property (nonatomic, copy) NSString *tipContent;

@property (nonatomic, strong) UILabel *tipTitleLbl;

@property (nonatomic, strong) IXTextField *bankAccountTF;

@property (nonatomic, copy) endInputValue textContent;

- (void)hiddenArrowImg;

@end
