//
//  IXSupplementBankM.h
//  IXApp
//
//  Created by Bob on 2017/2/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>
 
@interface IXSupplementBank : JSONModel

//银行类型
@property (nonatomic, strong) NSString *bank;

//银行卡持卡人名
@property (nonatomic, strong) NSString *bankAccountName;

//银行卡号码
@property (nonatomic, strong) NSString *bankAccountNumber;

//国家
@property (nonatomic, strong) NSString *bankCountry;

//省
@property (nonatomic, strong) NSString *bankProvince;

//城市
@property (nonatomic, strong) NSString *bankCity;

//币种
@property (nonatomic, strong) NSString *bankCurrency;

//分行
@property (nonatomic, strong) NSString *bankBranch;

//账户类型
@property (nonatomic, strong) NSString *bankAccountType;

//补充地址
@property (nonatomic, strong) NSString *bankAddress;

//为其他银行是填入的数据，没有则为空
@property (nonatomic, strong) NSString *bankOther;

//
@property (nonatomic, strong) NSString *internationalRemittanceCode;

//列编号
@property (nonatomic, strong) NSString *id;

@property (nonatomic, assign) int bankOrder;

@end

@protocol IXSupplementBank;
@interface IXSupplementBankM : JSONModel

//银行卡修改集合
@property (nonatomic, strong) NSMutableArray<IXSupplementBank> *banks;

@end

