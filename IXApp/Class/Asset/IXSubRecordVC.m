//
//  IXSubRecordVC.m
//  IXApp
//
//  Created by Evn on 17/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubRecordVC.h"
#import "IXSubRecordCell.h"
#import "IXOpenChoiceV.h"
#import "NSString+FormatterPrice.h"
#import "IXBORequestMgr+Record.h"
#import "IXReportModel.h"
#import "MJRefresh.h"
#import "IXPosDefultView.h"
#import "MJRefresh.h"
#import "IXCpyConfig.h"

@interface IXSubRecordVC ()
<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) IXOpenChoiceV *dateView;
@property (nonatomic, strong) IXOpenChoiceV *cateView;
@property (nonatomic, strong) IXPosDefultView *posDefView;

@property (nonatomic, strong) UIButton *outBtn;
@property (nonatomic, strong) UIButton *inBtn;
@property (nonatomic, strong) UIView *dLine;
@property (nonatomic, assign) int currentIndex;
@property (nonatomic, assign) int currentPage;
@property (nonatomic, assign) int pageSize;
@property (nonatomic, strong) NSString *tradeAccountId;
@end

@implementation IXSubRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    self.title = LocalizedString(@"订阅记录");
    self.view.backgroundColor = ViewBackgroundColor;
    
    _currentIndex = 0;
    _currentPage = 1;
    _pageSize = 20;
    _dataArr = [IXBORequestMgr getCachedInCashRecord];
    
    [self addTitleView];
    [self addLineView];
    [self.view addSubview:self.tableV];
    [self refreshData];
}

- (void)refreshData
{
    if (!_dataArr) {
          _dataArr = [[NSMutableArray alloc] init];
    }
    
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    
    NSDictionary    * paramDic = @{
                                   @"pageNo":[NSString stringWithFormat:@"%d",_currentPage],
                                   @"pageSize":[NSString stringWithFormat:@"%d",_pageSize]
                                   };
    
    if (_currentIndex == 0) {
        [IXBORequestMgr record_incomeListWithParam:paramDic result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            [SVProgressHUD dismiss];
            [self.tableV.header endRefreshing];
            
            if (obj && [obj isKindOfClass:[NSDictionary class]]) {
                id result = obj[@"collection"];
                
                if (result && [result isKindOfClass:[NSArray class]]) {
                    NSMutableArray *ret = [[NSMutableArray alloc] init];
                    if ([(NSArray *)result count] > 0) {
                        [ret addObjectsFromArray:result];
                    }

                    if (_dataArr.count > 0) {
                        [ret addObjectsFromArray:_dataArr];
                    }
                    _dataArr = [ret mutableCopy];
                }
            }
            [self refreshUI];
        }];
    } else {
        [IXBORequestMgr record_drawCashListWithParam:paramDic result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            [SVProgressHUD dismiss];
            [self.tableV.header endRefreshing];
            
            if (obj && [obj isKindOfClass:[NSDictionary class]]) {
                id result = obj[@"collection"];
                if (result && [result isKindOfClass:[NSArray class]]) {
                    
                    NSMutableArray *ret = [[NSMutableArray alloc] init];
                    if ([(NSArray *)result count] > 0) {
                        [ret addObjectsFromArray:result];
                    }
                    if (_dataArr.count > 0) {
                        [ret addObjectsFromArray:_dataArr];
                    }
                    _dataArr = [ret mutableCopy];
                }
            }
            [self refreshUI];
        }];
    }
}

- (void)refreshUI
{
    if (_dataArr.count == 0) {
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        self.posDefView.imageName = @"search_default";
        
        if (_currentIndex == 0) {
            self.posDefView.tipMsg = LocalizedString(@"暂无入金记录");
        } else {
            self.posDefView.tipMsg = LocalizedString(@"暂无出金记录");
        }

        [self.tableV reloadData];
    } else {
        [self.posDefView removeFromSuperview];
        [_tableV reloadData];
    }
}

- (void)addTitleView
{
    NSArray *titleArr = @[LocalizedString(@"成功"),
                          LocalizedString(@"失败")];
    
    for (int i = 0; i < 2; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i*kScreenWidth/2, kNavbarHeight, kScreenWidth/2, 35)];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        btn.tag = i;
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        btn.titleLabel.font = PF_MEDI(13);
        if (i == 0) {
            
            _inBtn = btn;
            
        } else {
            
            _outBtn = btn;
        }
        
        [btn addTarget:self action:@selector(switchDirection:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    _dLine = [[UIView alloc] initWithFrame:CGRectMake((kScreenWidth/2 - 75)/2, kNavbarHeight + 33, 75, 2)];
    _dLine.backgroundColor = MarketSymbolNameColor;
    [self.view addSubview:_dLine];
}

- (void)switchDirection:(UIButton *)btn
{
    int index = btn.tag;
    if (index == _currentIndex) {
        return;
    } else {
        _currentPage = 1;
        [_dataArr removeAllObjects];
        _dataArr = [IXBORequestMgr getCachedWithDrawRecord];
        _currentIndex = index;
    }
    
    switch (index) {
        case 0:{
            _dLine.frame = CGRectMake((kScreenWidth/2 - 75)/2, kNavbarHeight + 33, 75, 2);
        }
            break;
        case 1:{
            _dLine.frame = CGRectMake((kScreenWidth/2 - 75)/2 + kScreenWidth/2, kNavbarHeight + 33, 75, 2);
        }
            break;
        default:
            break;
    }
    [self refreshData];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IXOpenChoiceV *)dateView
{
    if (!_dateView) {
        _dateView = [[IXOpenChoiceV alloc] initWithFrame:CGRectMake(0, kNavbarHeight, kScreenWidth/2, 35)
                                               WithTitle:MARKETORDER
                                               WithAlign:TapChoiceAlignRightOutImg
                                                 WithTap:nil];
    }
    
    return _dateView;
}

- (IXOpenChoiceV *)cateView
{
    if (!_cateView) {
        _cateView = [[IXOpenChoiceV alloc] initWithFrame:CGRectMake(kScreenWidth/2, kNavbarHeight, kScreenWidth/2, 35)
                                                  WithTitle:MARKETORDER
                                                  WithAlign:TapChoiceAlignRightOutImg
                                                    WithTap:nil];
    }
    
    return _cateView;
}

- (void)addLineView
{
    UIView *lineV1 = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth/2, kNavbarHeight, 1, 35)];
    lineV1.backgroundColor = MarketCellSepColor;
    [self.view addSubview:lineV1];
    
    UIView *lineV2 = [[UIView alloc] initWithFrame:CGRectMake(0, kNavbarHeight + 35, kScreenWidth, 1)];
    lineV2.backgroundColor = MarketCellSepColor;
    [self.view addSubview:lineV2];
}

#pragma mark - lazy load
- (UITableView *)tableV
{
    if (!_tableV) {
        CGRect frame = self.view.bounds;
        frame.origin.y = 102;
        frame.size.height = kScreenHeight - 102;
        _tableV = [[UITableView alloc] initWithFrame:frame];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.backgroundColor = ViewBackgroundColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        weakself;
        _tableV.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            weakSelf.currentPage++;
            [weakSelf refreshData];
        }];
        
    }
    return _tableV;
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0, 80, kScreenWidth, 217)];
    }
    return _posDefView;
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 138;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"IXSubRecordCell";
    IXSubRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXSubRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = ViewBackgroundColor;
    }
    [cell reloadUIData:_dataArr[indexPath.row] index:_currentIndex];
    cell.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
