//
//  IXRechargeTipV.h
//  IXApp
//
//  Created by Evn on 2017/10/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXRechargeTipV : UIView

@property (nonatomic, copy) NSString    * title;
@property (nonatomic, copy) void(^dismissBlock)();
@property(nonatomic) dispatch_source_t timer;

- (void)cancelTimer;

@end
