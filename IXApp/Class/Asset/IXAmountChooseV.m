//
//  IXAmountChooseV.m
//  IXApp
//
//  Created by Seven on 2017/5/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAmountChooseV.h"
#import "IXAppUtil.h"

@interface IXAmountChooseV ()

@property (nonatomic, strong) UIScrollView  * scrollView;

@property (nonatomic, readwrite) NSInteger   minAmount;
@property (nonatomic, readwrite) NSInteger   maxAmount;

@property (nonatomic, strong) NSMutableArray    * btnArr;
@property (nonatomic, strong) NSMutableArray    * amountArr;

@end

@implementation IXAmountChooseV

+ (instancetype)amountChooseVWithFrame:(CGRect)frame minAmount:(NSInteger)min maxAmount:(NSInteger)max
{
    return [[IXAmountChooseV alloc] initWithFrame:frame minAmount:min maxAmount:max];
}

- (instancetype)initWithFrame:(CGRect)frame minAmount:(NSInteger)min maxAmount:(NSInteger)max
{
    if (self = [super initWithFrame:frame]) {
        [self createSubview];
        [self setMinAmount:min maxAmount:max];
        self.dk_backgroundColorPicker = DKViewColor;
    }
    
    return self;
}

- (void)createSubview
{
    CGRect rect = self.bounds;
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(15,
                                                                 rect.origin.y,
                                                                 rect.size.width - 30,
                                                                 rect.size.height)];
    _scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:_scrollView];
    
    _btnArr = [@[] mutableCopy];
    
    for (int i = 0; i < 5; i ++) {
        UIButton    * btn = [UIButton new];
        btn.tag = i;
        
        btn.layer.cornerRadius = 3.f;
        btn.layer.borderWidth = 0.5;
        btn.layer.dk_borderColorPicker = DKColorWithRGBs(0x8395a4, 0x4c6072);
        btn.clipsToBounds = YES;
        
        btn.titleLabel.font = RO_REGU(15);
        [btn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4) forState:UIControlStateNormal];
        [btn setTitleColor:AutoNightColor(0xffffff, 0xe9e9ea) forState:UIControlStateSelected];
        [btn setTitleColor:AutoNightColor(0xffffff, 0xe9e9ea) forState:UIControlStateHighlighted];
        
        [_btnArr addObject:btn];
        [_scrollView addSubview:btn];

        UIImage * wbgImg = [IXAppUtil createImageWithColor:MarketGrayPriceColor];
        UIImage * dbgImg = [IXAppUtil createImageWithColor:MarketSymbolNameColor];
        [btn dk_setBackgroundImage:DKImageWithImgs(wbgImg, dbgImg) forState:UIControlStateSelected];
        [btn dk_setBackgroundImage:DKImageWithImgs(wbgImg, dbgImg) forState:UIControlStateHighlighted];
        
        UIImage * wImg = [IXAppUtil createImageWithColor:[UIColor whiteColor]];
        UIImage * dImg = [IXAppUtil createImageWithColor:UIColorHexFromRGB(0x262f3e)];
        [btn dk_setBackgroundImage:DKImageWithImgs(wImg, dImg) forState:UIControlStateNormal];

        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)btnAction:(UIButton *)btn
{
    for (UIButton * btn in _btnArr) {
        btn.selected = NO;
    }
    btn.selected = YES;
    
    if (self.callBackAmount) {
        self.callBackAmount([_amountArr[btn.tag] floatValue], btn.titleLabel.text);
    }
    
    CGSize  contentSize = _scrollView.contentSize;
    CGSize  frameSize = _scrollView.frame.size;
    
    CGRect  btnFrame = btn.frame;
    CGFloat btnCenterX = btnFrame.origin.x + btnFrame.size.width/2;
    
    CGFloat x = 0;
    if (btnCenterX < frameSize.width/2) {
        x = 0;
    }else if (contentSize.width - btnCenterX < frameSize.width/2) {
        x = contentSize.width - frameSize.width;
    }else{
        x = btnCenterX - frameSize.width/2;
    }
    
    [_scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)resetStatus
{
    for (UIButton * btn in _btnArr) {
        btn.selected = NO;
    }
}

- (void)setMinAmount:(NSInteger)min maxAmount:(NSInteger)max
{
    NSMutableArray  * arr = [@[] mutableCopy];

    if (max < min || max <= 1000) {
        ELog(@"金额限制有误");
        return;
    }
    else if (max == min) {
        [arr addObject:@(min)];
    }
    else {
        CGFloat   space = (max - min) / 4.f;
        NSInteger   tmp = [self tmpOfCount:(NSInteger)space];
        
        //处理min和max
        if (min <= 0) {
            [arr addObject:@(100)];
        }else{
            [arr addObject:@(min)];
        }
        [arr addObject:@(max)];
        
        for (int i = 1; i < 4; i++) {
            NSInteger   amount = ((NSUInteger)(min+space*i)/tmp)*tmp;
            if (amount <= max && amount >= min && amount > 0) {
                [arr addObject:@(amount)];
            }else if (amount <= min) {
                if (min/tmp > 1) {
                    amount = (min+tmp)/tmp*tmp;
                } else {
                    amount = tmp;
                }
                [arr addObject:@(amount)];
            }
        }
    }
    
    //去重
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    for (int i = 0; i < arr.count; i ++) {
        NSString    * key = [NSString stringWithFormat:@"%@",arr[i]];
        //去0
        if ([arr[i] integerValue] > 0) {
            [dic setValue:arr[i] forKey:key];
        }
    }
    
    //排序
    _amountArr = [[dic allValues] mutableCopy];
    [_amountArr sortUsingComparator:^NSComparisonResult(NSNumber *  _Nonnull obj1, NSNumber *  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];

    [self layoutIfNeeded];
}

- (NSInteger)tmpOfCount:(NSInteger)count
{
    NSInteger   tmp = 1;
    NSString    * str = [NSString stringWithFormat:@"%ld",count];
    
    if (str.length >= 2) {
        tmp = powl(10, str.length-1);
    } else {
        if (count > 5) {
            tmp = 5;
        } else if (count > 2){
            tmp = 2;
        } else {
            tmp = 1;
        }
    }
    
    return tmp;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSMutableArray  * widthArr = [@[] mutableCopy];
    NSDictionary    * attributes = @{NSFontAttributeName:RO_REGU(15)};
    
    CGFloat totalWid = 0.f;
    for (int i = 0 ; i < [_amountArr count]; i ++) {
        NSString    * str = [IXAppUtil amountToString:_amountArr[i]];
        CGFloat w = [str sizeWithAttributes:attributes].width + 5;
        totalWid += w;
        [widthArr addObject:@(w)];
    }
    
    CGFloat space = (kScreenWidth-15-15-totalWid)/4;
    CGFloat offset = 0.f;
    CGFloat y = (self.bounds.size.height - 18) / 2;
    CGFloat x = 0;
    if (space <= 10) {
        //控件间距过小
        space = 10;
    }
    if (space > 20) {
        space = 20;
        offset = (kScreenWidth - space*(_amountArr.count-1) - 30 - totalWid)/_amountArr.count;
    }
    
    for (int i = 0; i < [_amountArr count]; i ++){
        UIButton * btn = _btnArr[i];
        CGFloat width = [widthArr[i] floatValue];
        CGRect rect = CGRectMake(x, y, width + offset, 18);
        
        x  = x + [widthArr[i] floatValue] + space + offset;
        
        btn.frame = rect;
        [btn setTitle:[IXAppUtil amountToString:_amountArr[i]] forState:UIControlStateNormal];
    }
    
    CGSize  size = CGSizeMake(x - space, _scrollView.frame.size.height);
    _scrollView.contentSize = size;
}

@end
