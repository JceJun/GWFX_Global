//
//  IXSupplementAccountVC.h
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IXUserBankM;
@class IXEnableBankM;

@interface IXSupplementAccountVC : IXDataBaseVC

@property (nonatomic, strong) IXUserBankM       * bankInfo;
@property (nonatomic, assign) BOOL  modifyBankInfo;
@property (nonatomic, assign) BOOL  showResultPage; //是否显示结果页，默认显示
@property (nonatomic, copy) void(^addBankBlock)(NSString *bankNum); //银行卡号回调

@end
