//
//  IXCommonWebVC.h
//  IXApp
//
//  Created by Seven on 2017/8/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXCommonWebVC : IXDataBaseVC

@property (nonatomic, strong) NSString * url;

@end
