//
//  IXDrawSubmitModel.h
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IXDrawSubmitModel : JSONModel

@property (nonatomic, strong) NSString *customerNo;
@property (nonatomic, strong) NSString *platform;
@property (nonatomic, strong) NSString *accountNo;
@property (nonatomic, strong) NSString *payAmount;
@property (nonatomic, strong) NSString *fee;
@property (nonatomic, strong) NSString *transAmount;
@property (nonatomic, strong) NSString *gts2CustomerId;
@property (nonatomic, strong) NSString *accountGroupId;
@property (nonatomic, strong) NSString *gts2AccountId;

@property (nonatomic, strong) NSString *withdrewBankName;
@property (nonatomic, strong) NSString *withdrewBankAccountName;
@property (nonatomic, strong) NSString *withdrewBankAccount;

@property (nonatomic, strong) NSString *accountCurrency;
@property (nonatomic, strong) NSString *transCurrency;
@property (nonatomic, strong) NSString *payCurrency;

@property (nonatomic, strong) NSString *bankPayMethod;
@property (nonatomic, strong) NSString *exchangeRate;

@end
