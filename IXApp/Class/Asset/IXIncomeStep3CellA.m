//
//  IXIncomeStep3CellA.m
//  IXApp
//
//  Created by Evn on 17/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeStep3CellA.h"

@interface IXIncomeStep3CellA()

@property (nonatomic, strong)UIImageView    * icon;
@property (nonatomic, strong)UIView     * lineView;
@property (nonatomic, strong)UIView     * uLineView;
@property (nonatomic, strong)UIView     * dLineView;
@property (nonatomic, strong)UILabel    * title;

@end

@implementation IXIncomeStep3CellA

- (UIImageView *)icon
{
    if (!_icon) {
        CGRect rect = CGRectMake(13, 12, 19, 19);
        _icon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (UIView *)lineView
{
    if (!_lineView) {
        CGRect rect = CGRectMake(0, 0, kScreenWidth, kLineHeight);
        _lineView = [[UIView alloc] initWithFrame:rect];
        _lineView.dk_backgroundColorPicker = DKLineColor;
    }
    
    return _lineView;
}

- (UIView *)uLineView
{
    if (!_uLineView) {
        CGRect rect = CGRectMake(22, 0,2,14);
        _uLineView = [[UIImageView alloc] initWithFrame:rect];
        _uLineView.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_uLineView];
    }
    
    return _uLineView;
}

- (UIView *)dLineView
{
    if (!_dLineView) {
        CGRect rect = CGRectMake(22, GetView_MaxY(_icon),2,14);
        _dLineView = [[UIView alloc] initWithFrame:rect];
        _dLineView.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_dLineView];
    }
    
    return _dLineView;
}

- (UILabel *)title
{
    if (!_title) {
        CGRect rect = CGRectMake(GetView_MaxX(_icon) + 5, 14, kScreenWidth/2 - (GetView_MaxX(_icon) + 5), 16);
        _title = [[UILabel alloc] initWithFrame:rect];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}


- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(kScreenWidth - (160 + 14.5), 14, 160, 16);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xd4d5dc);
        _desc.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (void)loadUIWithTitle:(NSString *)title
                   desc:(NSString *)desc
                    tag:(NSUInteger)tag
                process:(BOOL)flag{
    switch (tag) {
        case 0:{
            NSString    * name = [IXUserInfoMgr shareInstance].isNightMode ? @"income_complete_Dark" : @"income_complete";
            [self.icon setImage:[UIImage imageNamed:name]];
            self.dLineView.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            self.title.dk_textColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            self.desc.dk_textColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            break;
        }
        case 1:{
            [self.icon setImage:AutoNightImageNamed(@"income_process")];
            self.uLineView.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            self.dLineView.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            self.title.dk_textColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            self.desc.dk_textColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            break;
        }
        case 2:{
            if (!flag) {
                NSString    * name = [IXUserInfoMgr shareInstance].isNightMode ? @"openAccount_complete_D" : @"openAccount_complete";
                self.icon.image = [UIImage imageNamed:name];
                self.uLineView.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
                self.title.dk_textColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
                self.desc.dk_textColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
            } else {
                [self.icon setImage:AutoNightImageNamed(@"income_success")];
                self.uLineView.dk_backgroundColorPicker = DKColorWithRGBs(0xa3aebb, 0x4c6072);
                self.title.dk_textColorPicker = DKColorWithRGBs(0xa3aebb, 0x4c6072);
                self.desc.dk_textColorPicker = DKColorWithRGBs(0xa3aebb, 0x4c6072);
            }
            break;
        }
            
        default:
            break;
    }

    self.title.text = title;
    self.desc.text = desc;
}


@end
