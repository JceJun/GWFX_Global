//
//  IXAssetHeaderView.h
//  IXApp
//
//  Created by Seven on 2017/6/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#define circleR [UIScreen mainScreen].bounds.size.width - 148


/**
 约定使用宏定义circleR作为view的宽高，如需改变，请直接修改宏定义
 */
@interface IXAssetHeaderView : UIView

@property (nonatomic, copy) NSString    * totalAmount;
@property (nonatomic, copy) NSString    * freedomAmount;
@property (nonatomic, copy) NSString    * acntType;
@property (nonatomic, strong) UILabel   * totalAmountLab;

- (void)setProgress:(CGFloat)progress animation:(BOOL)animation;

- (void)showCircleAnimationComplete:(void(^)())complete;

@end
