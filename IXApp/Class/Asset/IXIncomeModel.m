//
//  IXIncomeModel.m
//  IXApp
//
//  Created by Evn on 17/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeModel.h"

@implementation IXIncomeModel


#warning 若添加其它语言，此处需要增加判断，返回对应的语言
- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end
