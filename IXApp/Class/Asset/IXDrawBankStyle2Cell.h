//
//  IXDrawBankStyle2Cell.h
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDrawBankStyle2Cell : UITableViewCell

@property (nonatomic, strong) NSArray *moneyInfo;

@end
