//
//  IXDrawRuleV.m
//  IXApp
//
//  Created by Evn on 2017/9/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawRuleV.h"

@interface IXDrawRuleV()

@property (nonatomic, strong)UIView *alertView;
@property (nonatomic, strong)UIScrollView *sView;
@property (nonatomic, strong)UILabel *hint;
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UILabel *content;
@property (nonatomic, strong)UIButton *sureBtn;

@property (nonatomic, strong)NSArray *ruleArr;
@property (nonatomic, assign)CGFloat width;
@property (nonatomic, assign)CGFloat height;//内容高度
@property (nonatomic, copy)  NSString *contentStr;

@end

@implementation IXDrawRuleV

- (void)showDrawRule:(NSArray *)ruleArr
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75f];
    _ruleArr = ruleArr;
    [self setScrollViewHeight];
    [self loadUI];
    [self show];
}

- (void)loadUI
{
    [self alertView];
    [self hint];
    [self sView];
    [self content];
    [self uLine];
    [self sureBtn];
    
}

- (void)show
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.center = keyWindow.center;
    [keyWindow addSubview:self];
    
    self.alertView.center = keyWindow.center;
    [keyWindow addSubview:self.alertView];
}

- (UIView *)alertView
{
    if (!_alertView) {
        _alertView = [[UIView alloc]init];
        CGRect frame = CGRectMake(0, 0, 295, 137 + _height);
        if (kScreenHeight - (137 + kNavbarHeight*2 + _height) < 0) {
            frame.size.height = kScreenHeight - (kNavbarHeight*2);
        }
        _alertView.bounds = frame;
        _width = 295;
        _alertView.dk_backgroundColorPicker = DKViewColor;
        _alertView.layer.cornerRadius = 10;
        _alertView.layer.masksToBounds = YES;
    }
    return _alertView;
}

- (UILabel *)hint
{
    if ( !_hint ) {
        _hint = [IXUtils createLblWithFrame:CGRectMake( 15, 28, _width - 30, 18)
                                   WithFont:PF_MEDI(15)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        _hint.text = LocalizedString(@"手续费规则");
        [self.alertView addSubview:_hint];
    }
    return _hint;
}

- (UIScrollView *)sView
{
    if (!_sView) {
        CGRect frame = CGRectMake(0, GetView_MaxY(self.hint) + 20, VIEW_W(self.alertView), _height);
        if (kScreenHeight - (137 + kNavbarHeight*2 + _height) < 0) {
            frame.size.height = kScreenHeight - (137 + kNavbarHeight*2);
        }
        _sView = [[UIScrollView alloc] initWithFrame:frame];
        _sView.contentSize = CGSizeMake(295, _height);
        _sView.showsVerticalScrollIndicator = NO;
        _sView.showsHorizontalScrollIndicator = NO;
        _sView.backgroundColor = [UIColor clearColor];
        [self.alertView addSubview:_sView];
    }
    return _sView;
}

- (UILabel *)content
{
    if (!_content) {
        _content = [IXUtils createLblWithFrame:CGRectMake( 15, 0, _width - 30, _height)
                                   WithFont:PF_MEDI(13)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x99abba
                                 dTextColor:0x8395a4
                 ];
        _content.numberOfLines = 0;
        _content.text = _contentStr;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_contentStr];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:5];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_contentStr length])];
        _content.attributedText = attributedString;
        _content.lineBreakMode = NSLineBreakByCharWrapping;
        _content.textAlignment = NSTextAlignmentCenter;
        [self.sView addSubview:_content];
    }
    return _content;
}

- (UIView *)uLine
{
    if (!_uLine) {
        _uLine = [[UIView alloc] initWithFrame:CGRectMake(0, GetView_MaxY(self.sView) + 25, _width, 1)];
        _uLine.dk_backgroundColorPicker = DKLineColor;
        [self.alertView addSubview:_uLine];
    }
    return _uLine;
}

- (UIButton *)sureBtn
{
    if (!_sureBtn) {
        _sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, GetView_MaxY(self.uLine) + 1,VIEW_W(self.alertView), 46)];
        [_sureBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        _sureBtn.tag = 1;
        _sureBtn.titleLabel.font = PF_MEDI(15);
        [_sureBtn setTitle:LocalizedString(@"我知道了") forState:UIControlStateNormal];
        [_sureBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [self.alertView addSubview:_sureBtn];
    }
    return _sureBtn;
}

- (void)click
{
    if (self.alertView) {
        [self.alertView removeFromSuperview];
    }
    if (self) {
        [self removeFromSuperview];
    }
}

- (void)setScrollViewHeight
{
    _contentStr = [[NSString alloc] init];
    for (int i = 0; i < _ruleArr.count; i++) {
        if ([_ruleArr[i] isKindOfClass:[NSString class]]) {
            if (i == 0) {
                _contentStr = [_contentStr stringByAppendingString:_ruleArr[i]];
            } else {
                _contentStr = [_contentStr stringByAppendingFormat:@"\n%@",_ruleArr[i]];
            }
        }
    }
    if (_contentStr.length == 0) {
        _contentStr = LocalizedString(@"手续费免费");
    }
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 5;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:PF_MEDI(13), NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    CGSize size = [_contentStr boundingRectWithSize:CGSizeMake(265 , MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:dic
                                            context:nil].size;
    _height = size.height + _ruleArr.count*5;
}

@end
