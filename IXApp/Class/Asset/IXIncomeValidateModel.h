//
//  IXIncomeValidateModel.h
//  IXApp
//
//  Created by Evn on 17/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IXIncomeValidateModel : JSONModel

@property (nonatomic, copy) NSString    * accountNo;
@property (nonatomic, assign) NSInteger tradeAccountId;
@property (nonatomic, copy) NSString    * payType;
@property (nonatomic, copy) NSString    * depositCurrency;
@property (nonatomic, copy) NSString    * platform;
@property (nonatomic, copy) NSString    * bank;
@property (nonatomic, copy) NSString    * bankAccountNumber;
@property (nonatomic, copy) NSString    * payMethod;
@property (nonatomic, copy) NSString    * amount;

@end

#pragma mark - 快捷入金 - 入金申请

@interface IXIncashApplyM : JSONModel

@property (nonatomic, copy) NSString    * payType;
@property (nonatomic, copy) NSString    * platform;
@property (nonatomic, copy) NSString    * accountNo;
@property (nonatomic, copy) NSString    * depositCurrency;
@property (nonatomic, copy) NSString    * bank;
@property (nonatomic, copy) NSString    * bankAccountNumber;
@property (nonatomic, copy) NSString    * phoneNumber;
@property (nonatomic, copy) NSString    * chineseName;
@property (nonatomic, copy) NSString    * idNumber;
@property (nonatomic, copy) NSString    * payMethod;
@property (nonatomic, copy) NSString    * amount;
@property (nonatomic, copy) NSString    * remark;
@property (nonatomic, copy) NSString    * pno;
@property (nonatomic, copy) NSString    * lang;
@property (nonatomic, copy) NSString    * createIp;

@end

#pragma mark - 快捷入金 - 入金验证短信重发

@interface IXIncashResendMsmM   : JSONModel

@property (nonatomic, copy) NSString    * merchantId;
@property (nonatomic, copy) NSString    * encryptedText;

@end

#pragma mark - 快捷入金 - 确认入金

@interface IXIncashConfirmPayM  : JSONModel;

@property (nonatomic, copy) NSString    * smsCode;
@property (nonatomic, copy) NSString    * merchantId;
@property (nonatomic, copy) NSString    * encryptedText;

@end
