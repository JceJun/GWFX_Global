//
//  IXDrawBankListVC.h
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IXEnableBankM;

typedef void(^selectedBankInfo)(IXEnableBankM * bank);

@interface IXDrawBankListVC : IXDataBaseVC

- (id)initWithSelectedInfo:(selectedBankInfo)bankInfo;

@end
