//
//  IXDrawRuleV.h
//  IXApp
//
//  Created by Evn on 2017/9/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDrawRuleV : UIView

- (void)showDrawRule:(NSArray *)ruleArr;

@end
