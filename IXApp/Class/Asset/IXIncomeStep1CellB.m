//
//  IXIncomeStep1CellB.m
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeStep1CellB.h"
#import "IXUserInfoM.h"

@interface IXIncomeStep1CellB ()

@property (nonatomic,strong)UIImageView *icon;
@property (nonatomic,strong)UILabel *desc;
@property (nonatomic,strong)UIImageView *sIcon;

@end

@implementation IXIncomeStep1CellB

- (UIImageView *)icon
{
    if (!_icon) {
        CGRect rect = CGRectMake(13, 12, 24, 20);
        _icon = [[UIImageView alloc] initWithFrame:rect];
        [_icon setImage:[UIImage imageNamed:@"income_bankCard_avatar"]];
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(GetView_MaxX(self.icon) + 10, 11, kScreenWidth - (GetView_MaxX(self.icon) + 10) , 22);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _desc.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (UIImageView *)sIcon
{
    if (!_sIcon) {
        CGRect rect = CGRectMake(kScreenWidth - (16 + 15), 14, 16, 16);
        _sIcon = [[UIImageView alloc] initWithFrame:rect];
        _sIcon.dk_imagePicker = DKImageNames(@"common_cell_choose", @"common_cell_choose_D");
        [self.contentView addSubview:_sIcon];
    }
    return _sIcon;
}

- (void)setBankM:(IXUserBankM *)bankM
{
    _bankM = bankM;
    NSString    * accountNo = bankM.bankAccountNumber;
    
    self.sIcon.hidden = !bankM.mark;
    if (accountNo && accountNo.length > 4) {
        accountNo = [accountNo substringWithRange:NSMakeRange(accountNo.length - 4, 4)];
    }
    
    NSString    * bankName = [bankM localizedName];
    if (bankName.length > 25) {
        bankName = [bankName substringToIndex:25];
        bankName = [bankName stringByAppendingString:@"..."];
    }
    
    self.desc.text = [NSString stringWithFormat:@"%@ (%@)",bankName,accountNo];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (void)reloadUIWithData:(NSDictionary *)dic
{
    NSString *accNo = dic[@"bankAccountNumber"];
    if ([dic[@"mark"] integerValue]) {
        self.sIcon.hidden = NO;
    } else {
        self.sIcon.hidden = YES;
    }
    if (accNo && accNo.length > 4) {
        accNo = [accNo substringWithRange:NSMakeRange(accNo.length - 4, 4)];
    }
    
    NSString    * bankName = dic[@"name"];
    if (bankName.length > 25) {
        bankName = [bankName substringToIndex:25];
        bankName = [bankName stringByAppendingString:@"..."];
    }
    
    self.desc.text = [NSString stringWithFormat:@"%@ (%@)",bankName,accNo];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

@end
