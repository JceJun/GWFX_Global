//
//  IXSupplementStyle1Cell.h
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXSupplementStyle1Cell : UITableViewCell

@property (nonatomic, strong) NSString *tipTitle;

@end
