//
//  IXIncomeModel.h
//  IXApp
//
//  Created by Evn on 17/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXIncomeModel : NSObject
@property (nonatomic, copy)NSString *depositCurrency;
@property (nonatomic, copy)NSString *bankAccountNumber;
@property (nonatomic, copy)NSString *payMethod;
@property (nonatomic, copy)NSString *payName;//支付名称
@property (nonatomic, copy)NSString *nameCN;
@property (nonatomic, copy)NSString *nameEN;
@property (nonatomic, copy)NSString *nameTW;
@property (nonatomic, copy)NSString *amount;
@property (nonatomic, copy)NSString *remark;
@property (nonatomic, copy)NSString *bank;
@property (nonatomic, copy)NSString *token;
@property (nonatomic, copy)NSString *bankAccount;
@property (nonatomic, copy)NSString *pNo;
@property (nonatomic, copy)NSString *accountId;
@property (nonatomic, copy)NSString *minPayamount;
@property (nonatomic, copy)NSString *maxPayamount;
@property (nonatomic, copy)NSString *paymentId;

@property (nonatomic, copy)NSString *email;
@property(nonatomic,copy)NSString *mobileNumberPrefix;
@property (nonatomic, copy)NSString *mobileNumber;
@property (nonatomic, copy)NSString *firstName;
@property (nonatomic, copy)NSString *lastName;

- (NSString *)localizedName;

@end
