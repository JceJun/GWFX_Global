//
//  IXRecordCell.m
//  IXApp
//
//  Created by Evn on 17/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRecordCell.h"
#import "IXDBLanguageMgr.h"
#import "IXDBGlobal.h"
#import "IXAppUtil.h"
#import "IXDBSymbolMgr.h"
#import "IXUserDefaultM.h"
#import "IXRecordCacheM.h"
#import "CAGradientLayer+IX.h"

@interface IXRecordCell()

@property (nonatomic, strong)UIImageView *bgView;
@property (nonatomic, strong)UILabel *name;
@property (nonatomic, strong)UILabel *content;
@property (nonatomic, strong)UILabel *dir;
@property (nonatomic, strong)UILabel *num;
@property (nonatomic, strong)UILabel *orderNo;
@property (nonatomic, strong)UILabel *showReqPrice;
@property (nonatomic, strong)UILabel *reqPrice;
@property (nonatomic, strong)UILabel *reqTime;
@property (nonatomic, strong)UILabel *showExePrice;
@property (nonatomic, strong)UILabel *exePrice;
@property (nonatomic, strong)UILabel *exeTime;
@property (nonatomic, strong)UILabel *showProfit;
@property (nonatomic, strong)UILabel *profit;

@end

@implementation IXRecordCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addShadow];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, kScreenWidth, 100)];
        _bgView.dk_backgroundColorPicker  = DKNavBarColor;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)name {
    
    if (!_name) {
        _name = [IXCustomView createLable:CGRectMake(15,15, 85, 15)
                                    title:@""
                                     font:PF_MEDI(13)
                               wTextColor:0x4c6072
                               dTextColor:0xe9e9ea
                            textAlignment:NSTextAlignmentLeft];
        
        [self.bgView addSubview:_name];
    }
    return _name;
}

- (UILabel *)contentLbl {
    
    if (!_content) {
        
        _content = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.name) + 5,VIEW_Y(self.name) + 3, VIEW_W(self.bgView) - (GetView_MaxX(self.name) + 5 + 15), 12)
                                       title:@""
                                        font:RO_REGU(10)
                                  wTextColor:0xa7adb5
                                  dTextColor:0x8395a4
                               textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_content];
    }
    return _content;
}

- (UILabel *)dir
{
    if (!_dir) {
        _dir = [IXCustomView createLable:CGRectMake(15,44, 15, 15)
                                   title:@""
                                    font:PF_MEDI(10)
                              wTextColor:0xffffff
                              dTextColor:0x262f3e
                           textAlignment:NSTextAlignmentCenter];
        [self.bgView addSubview:_dir];
    }
    return _dir;
}

- (UILabel *)num
{
    if (!_num) {
        _num = [IXCustomView createLable:CGRectMake(GetView_MaxX(_dir) + 6,42, VIEW_W(self.bgView)/3 - (GetView_MaxX(_dir) + 6), 18)
                                   title:@""
                                    font:RO_REGU(15)
                              wTextColor:0x4c6072
                              dTextColor:0xe9e9ea
                           textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_num];
    }
    return _num;
}

- (UILabel *)showReqPrice
{
    if (!_showReqPrice) {
        _showReqPrice = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView)/3,
                                                             VIEW_Y(self.num),
                                                             0,
                                                             18)
                                            title:@""
                                             font:PF_MEDI(12)
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showReqPrice];
    }
    return _showReqPrice;
}

- (UILabel *)reqPrice
{
    if (!_reqPrice) {
        _reqPrice = [IXCustomView createLable:CGRectMake(GetView_MaxX(_showReqPrice) + 5,
                                                         VIEW_Y(self.num),
                                                         0,
                                                         18)
                                        title:@""
                                         font:RO_REGU(15)
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_reqPrice];
    }
    return _reqPrice;
}

- (UILabel *)reqTime
{
    if (!_reqTime) {
        _reqTime = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView)*2/3,
                                                        VIEW_Y(self.num),
                                                        VIEW_W(self.bgView)/3 - 15,
                                                        18)
                                       title:@""
                                        font:RO_REGU(12)
                                  wTextColor:0x99abba
                                  dTextColor:0x8395a4
                               textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_reqTime];
    }
    return _reqTime;
}

- (UILabel *)orderNo
{
    if (!_orderNo) {
        _orderNo = [IXCustomView createLable:CGRectMake(15,
                                                        GetView_MaxY(self.num) + 15,
                                                        VIEW_W(_bgView)/3 - 15,
                                                        15)
                                       title:@""
                                        font:RO_REGU(12)
                                  wTextColor:0x99abba
                                  dTextColor:0x8395a4
                               textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_orderNo];
    }
    return _orderNo;
}

- (UILabel *)showExePrice
{
    if (!_showExePrice) {
        _showExePrice = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView)/3,
                                                             VIEW_Y(self.orderNo),
                                                             0,
                                                             15)
                                            title:@""
                                             font:PF_MEDI(12)
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showExePrice];
    }
    return _showExePrice;
}

- (UILabel *)exePrice
{
    if (!_exePrice) {
        _exePrice = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.showReqPrice) + 5,
                                                         VIEW_Y(self.orderNo),
                                                         85,
                                                         15)
                                        title:@""
                                         font:RO_REGU(15)
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_exePrice];
    }
    return _exePrice;
}

- (UILabel *)exeTime
{
    if (!_exeTime) {
        _exeTime = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView)*2/3,
                                                        VIEW_Y(self.orderNo),
                                                        VIEW_W(self.bgView)/3 - 15,
                                                        18)
                                       title:@""
                                        font:RO_REGU(12)
                                  wTextColor:0x99abba
                                  dTextColor:0x8395a4
                               textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_exeTime];
    }
    return _exeTime;
}

- (UILabel *)showProfit
{
    if (!_showProfit) {
        _showProfit = [IXCustomView createLable:CGRectMake(0,
                                                           VIEW_Y(self.profit),
                                                           0,
                                                           15) title:@""
                                           font:PF_MEDI(12)
                                     wTextColor:0x99abba
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_showProfit];
    }
    return _showProfit;
}

- (UILabel *)profit
{
    if (!_profit) {
        _profit = [IXCustomView createLable:CGRectMake(0,
                                                       18,
                                                       0,
                                                       15)
                                      title:@""
                                       font:RO_REGU(15)
                                 wTextColor:0x4c6072
                                 dTextColor:0x8395a4
                              textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_profit];
    }
    return _profit;
}

- (void)addShadow
{
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * topL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 5)
                                                        dkcolors:topColors];
    [self.contentView.layer addSublayer:topL];
    
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 105, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [self.contentView.layer addSublayer:btomL];
    
    self.layer.masksToBounds = NO;
    self.contentView.layer.masksToBounds = NO;
}

- (void)reloadUIData:(NSDictionary *)dataDic
        cacheSymbole:(IXRecordCacheM *)symCacheM
{
    
    int64 symbolId = [dataDic[kSymbolId] longLongValue];
    IXRecordSymM *symbolM = [symCacheM getCacheSymbolBySymbolId:symbolId];
    self.name.text = symbolM.lanName;
    NSInteger width = [IXEntityFormatter getContentWidth:symbolM.lanName WithFont:PF_MEDI(13)] + 1;
    CGRect frame = self.name.frame;
    frame.size.width = width;
    self.name.frame = frame;
    
    frame = self.contentLbl.frame;
    frame.origin.x = VIEW_X(self.name) + VIEW_W(self.name) + 5;
    self.content.frame = frame;
    self.content.text = [NSString stringWithFormat:@"%@:%@",symbolM.name,symbolM.marketName];
    
    if ([[dataDic stringForKey:@"direction"] isEqualToString:@"BUY"]) {
        self.dir.text = LocalizedString(@"买.");
        self.dir.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dir.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    } else {
        self.dir.text = LocalizedString(@"卖.");
        self.dir.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dir.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    frame = self.dir.frame;
    frame.origin.y = GetView_MaxY(self.name) + 12;
    self.dir.frame = frame;
    
    self.num.text = [NSString stringWithFormat:@"%@",[dataDic stringForKey:@"execvolume"]];
    int64 volume = [dataDic[@"execvolume"] longLongValue];
    if (symbolId) {
        self.num.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:volume contractSizeNew:symbolM.contractSizeNew volDigit:symbolM.volDigits] withDigits:symbolM.volDigits];
    } else {
        self.num.text = [NSString stringWithFormat:@"%lld",volume];
    }
    
    frame = self.num.frame;
    frame.origin.x = GetView_MaxX(self.dir) + 6;
    frame.origin.y = VIEW_Y(self.dir) - 2;
    self.num.frame = frame;
    
    NSString *showReqStr,*reqStr,*reqTimeStr,*showExeStr,*exeStr,*exeTimeStr;
    if ([[dataDic stringForKey:@"reportType"] isEqualToString:@"OPEN"]) {
        showReqStr = LocalizedString(@"请求价");
        showExeStr = LocalizedString(@"执行价");
        self.profit.hidden = YES;
        self.showProfit.hidden = YES;
        NSDictionary *dateDic = dataDic[@"orderRequestTime"];
        if (dateDic && [dateDic isKindOfClass:[NSDictionary class]]) {
            reqTimeStr = [IXEntityFormatter quoteTimeToString:([dateDic[@"time"] longLongValue])/1000];
        } else {
            reqTimeStr = @"--";
        }
    } else {
        showReqStr = LocalizedString(@"开仓价");
        showExeStr = LocalizedString(@"平仓价");
        self.profit.hidden = NO;
        self.showProfit.hidden = NO;
        NSDictionary *dateDic = dataDic[@"openTradeTime"];
        if (dateDic && [dateDic isKindOfClass:[NSDictionary class]]) {
            reqTimeStr = [IXEntityFormatter quoteTimeToString:([dateDic[@"time"] longLongValue])/1000];
        } else {
            reqTimeStr = @"--";
        }
    }
    
    NSDictionary *dateDic = dataDic[@"exectime"];
    if (dateDic && [dateDic isKindOfClass:[NSDictionary class]]) {
        exeTimeStr = [IXEntityFormatter quoteTimeToString:([dateDic[@"time"] longLongValue])/1000];
    } else {
        exeTimeStr = @"--";
    }
    reqStr = [NSString formatterPrice:[dataDic stringForKey:@"requestPrice"] WithDigits:[[dataDic stringForKey:@"digits"] intValue]];
    exeStr = [NSString formatterPrice:[dataDic stringForKey:@"execprice"] WithDigits:[[dataDic stringForKey:@"digits"] intValue]];
    
    
    width = [IXEntityFormatter getContentWidth:showReqStr WithFont:PF_MEDI(12)] + 4;
    self.showReqPrice.text = showReqStr;
    frame = self.showReqPrice.frame;
    frame.origin.x = VIEW_W(self.bgView)/3;
    frame.size.width = width;
    frame.origin.y = VIEW_Y(self.num);
    self.showReqPrice.frame = frame;
    
    frame = self.reqPrice.frame;
    self.reqPrice.text = reqStr;
    frame.origin.x = GetView_MaxX(self.showReqPrice);
    frame.size.width = VIEW_W(self.bgView)*2/3 - (VIEW_W(self.showReqPrice)) - 15;
    frame.origin.y = VIEW_Y(self.num);
    self.reqPrice.frame = frame;
    
    self.reqTime.text = reqTimeStr;
    frame = self.reqTime.frame;
    frame.origin.x = VIEW_W(self.bgView)*2/3;
    frame.size.width = VIEW_W(self.bgView)/3 - 15;
    frame.origin.y = VIEW_Y(self.num);
    self.reqTime.frame = frame;
    
    self.orderNo.text = [dataDic stringForKey:@"dealId"];
    frame = self.orderNo.frame;
    frame.origin.y = GetView_MaxY(self.num) + 15;
    self.orderNo.frame = frame;
    
    width = [IXEntityFormatter getContentWidth:showExeStr WithFont:PF_MEDI(12)] + 4;
    self.showExePrice.text = showExeStr;
    frame = self.showExePrice.frame;
    frame.size.width = width;
    frame.origin.y = VIEW_Y(self.orderNo);
    self.showExePrice.frame = frame;
    
    frame = self.exePrice.frame;
    self.exePrice.text = exeStr;
    frame.origin.x = GetView_MaxX(self.showExePrice);
    frame.size.width = VIEW_W(self.bgView)*2/3 - VIEW_W(self.showExePrice) - 15;
    frame.origin.y = VIEW_Y(self.orderNo);
    self.exePrice.frame = frame;
    
    self.exeTime.text = exeTimeStr;
    frame = self.exeTime.frame;
    frame.origin.x = VIEW_W(self.bgView)*2/3;
    frame.size.width = VIEW_W(self.bgView)/3 - 15;
    frame.origin.y = VIEW_Y(self.orderNo);
    self.exeTime.frame = frame;
    
    NSString *profitStr = [dataDic stringForKey:@"profit"];
    [IXDataProcessTools resetTextColorLabel:self.profit value:[profitStr doubleValue]];
    self.profit.text = [IXDataProcessTools moneyFormatterComma:[[profitStr stringByReplacingOccurrencesOfString:@"+" withString:@""] doubleValue] positiveNumberSign:NO];
    width = [IXEntityFormatter getContentWidth:self.profit.text WithFont:RO_REGU(15)] + 1;
    frame = self.profit.frame;
    frame.size.width = width;
    frame.origin.x = VIEW_W(self.bgView) - (width + 15);
    self.profit.frame = frame;
    
    frame = self.showProfit.frame;
    frame.size.width = VIEW_W(self.bgView) - (VIEW_W(self.profit) + 15 + 5);
    self.showProfit.frame = frame;
    self.showProfit.text = [NSString stringWithFormat:@"%@",LocalizedString(@"盈亏")];
}

@end

