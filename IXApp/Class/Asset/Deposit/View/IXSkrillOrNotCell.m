//
//  IXSkrillOrNotCell.m
//  IXApp
//
//  Created by Larry on 2018/5/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXSkrillOrNotCell.h"

@interface IXSkrillOrNotCell()
@property(nonatomic,strong)UILabel *lb_title;
@property(nonatomic,strong) UILabel *lb_content;
@property(nonatomic,strong)UIImageView *imgV;
@property(nonatomic,strong)UIImageView *imgV_arrow;

@end

@implementation IXSkrillOrNotCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"IXSkrillOrNotCell";
    IXSkrillOrNotCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[[self class] alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.backgroundColor = CellBackgroundColor;
    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubviews];
    }
    return self;
}

- (void)addSubviews{
//    self.bgView.backgroundColor = [UIColor whiteColor];
    [self.bgView makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(80);
    }];
    
    _imgV = [UIImageView new];
    _imgV.image = [UIImage imageNamed:@""];
    [self.bgView addSubview:_imgV];
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = PINGFANG_MEDI_FONT(13);
    lb_title.textColor = HexRGB(0x4c6072);
    lb_title.textAlignment = NSTextAlignmentCenter;
    lb_title.numberOfLines = 0;
    [self.bgView addSubview:lb_title];
    
    _lb_title = lb_title;
    
    
    UILabel *lb_content = [UILabel new];
    lb_content.font = PINGFANG_MEDI_FONT(12);
    lb_content.dk_textColorPicker = DKCellTitleColor;
    lb_content.textAlignment = NSTextAlignmentLeft;
    lb_content.numberOfLines = 0;
    [self.bgView addSubview:lb_content];
    _lb_content = lb_content;
    
    _imgV_arrow = [UIImageView new];
    _imgV_arrow.image = [UIImage imageNamed:@"openAccount_arrow_right"];
    [self.bgView addSubview:_imgV_arrow];
    
    [self addConstrains];
    
}

- (void)addConstrains{
    [_imgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.width.equalTo(48);
        make.height.equalTo(48);
        make.centerY.equalTo(0);
    }];
    
    [_lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imgV.right).offset(15);
        make.top.equalTo(15);
    }];
    
    [_imgV_arrow makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-15);
        make.width.equalTo(10);
        make.centerY.equalTo(0);
    }];
    
    [_lb_content makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_lb_title);
        make.top.equalTo(_lb_title.bottom).offset(5);
        make.right.equalTo(_imgV_arrow.left).offset(-10);
    }];
}

- (void)setItem:(IXLCellItem *)item{
    [super setItem:item];
    
//    NSString *title = item.title;
    NSDictionary *dataDic = item.dataDic;
    
    NSString *str = [NSString stringWithFormat:@"dps_%@",[[dataDic[@"title"] stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString]];
    _imgV.image = GET_IMAGE_NAME(str);
    _lb_title.text = dataDic[@"title"];
    _lb_content.text = dataDic[@"content"];
}

@end
