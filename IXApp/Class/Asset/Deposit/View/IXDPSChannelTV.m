//
//  IXDPSAmountTV.m
//  IXApp
//
//  Created by Larry on 2018/5/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDPSChannelTV.h"
#import "IXDPSChannelCell.h"
#import "IXDPSBankCurrencyCell.h"
#import "IXBORequestMgr+Asset.h"
#import "IXBORequestMgr+Comp.h"
@interface IXDPSChannelTV()
@property(nonatomic,strong)NSMutableArray *insertItems;
@end


@implementation IXDPSChannelTV

- (void)reloadTableView{
    [self addSection0];
    
    [self reloadData];
};

- (void)reloadTableViewInsertArr:(NSMutableArray *)insertArr AtIndex:(NSInteger)index{
//    weakself;
//    [_mainArr insertObject:insertArr atIndex:index];
//
//    NSMutableArray *insertItems = [NSMutableArray array];
//    [insertArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSDictionary *dicMain = obj;
//        NSString *title = dicMain[@"gatewayCode"];
//        {
//            NSString *ui_title = [title componentsSeparatedByString:@"__"][0];
//            IXLCellItem *item = [IXLCellItem itemWithTitle:ui_title
//                                               displayType:DisplayTypeDicNormal
//                                                   dataDic:dicMain
//                                                cellAction:^(NSString *key, id obj) {
//                                                    [weakSelf sendActionToUpperResponder:key object:obj];
//                                                }];
//            [insertItems addObject:item];
//        };
//    }];
//    _insertItems = insertItems;
//
//    IXLGroup *group = self.groups[0];
//    NSMutableArray *items = [NSMutableArray arrayWithArray:group.cellItems];
//    [items insert];
//    group.cellItems = items;
    
//    [self reloadData];
}

- (void)addSection0{
    [self.groups removeAllObjects];
    weakself;
    NSMutableArray *itemArr = [NSMutableArray array];
    
    
    NSMutableArray *mainArr = [NSMutableArray array];
    NSMutableArray *subArr = [NSMutableArray array];
    
//    __block NSString *recordName = @"";
    [[IXBORequestMgr shareInstance].channelList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *dataDic = [obj mutableCopy];
        __block NSString *gatewayCode = dataDic[@"gatewayCode"];
        
//        NSString *curency = dataDic[@"payCurrency"];
        
        if ([gatewayCode containsString:@"_"]) {
            // 条件加
            [subArr addObject:dataDic];
            
            gatewayCode = [dataDic[@"gatewayCode"] componentsSeparatedByString:@"_"][0];
            
            __block BOOL repeat = NO;
            [mainArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSDictionary *dic = obj;
                NSString *Code= dic[@"gatewayCode"];
                if ([Code containsString:gatewayCode]) {
                    repeat = YES;
                }
            }];
            if (!repeat) {
                [mainArr addObject:dataDic];
            }
            
//            if (![recordName containsString:gatewayCode]) {
//                recordName = gatewayCode;
//            }
        }else{
            [mainArr addObject:dataDic];
        }
    }];
    

    __block NSInteger transfer_index = -1;
    NSArray *sortArr = [IXBORequestMgr shareInstance].depositSort;
    __block NSMutableArray *sortMainArr = [NSMutableArray array]; // 渠道数组
    __block NSMutableDictionary *sortDic = [NSMutableDictionary dictionary]; // 货币数组
    [sortArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dic = obj;
        NSString *getewayName = dic[@"getewayName"];
        if (getewayName.length) {
            [sortMainArr addObject:getewayName];
            
            NSString *currencyRang = dic[@"currencyRang"];
            NSArray *currencyArr = [currencyRang componentsSeparatedByString:@"#"];
            sortDic[getewayName] = currencyArr;
        }
    }];
    
    [sortMainArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[obj lowercaseString] containsString:@"transfer"]) {
            transfer_index = idx;
        }
    }];
    
    NSMutableArray *temArr_add = [NSMutableArray array];
    NSMutableArray *temArr_delete = [mainArr mutableCopy];
    
    for (int i = 0; i < sortMainArr.count; i++) {
        NSString *getewayName = sortMainArr[i];
        [mainArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *name = obj[@"gatewayCode"];
            
            if ([[name lowercaseString] containsString:[getewayName lowercaseString]]) {
                [temArr_add addObject:obj];
                [temArr_delete removeObject:obj];
                *stop = YES;
            }
            
        }];
    }
    
    if (temArr_add.count) {
        [temArr_add addObjectsFromArray:temArr_delete];
        mainArr = [temArr_add mutableCopy];
    }
    
    
    [mainArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dicMain = obj;
        NSString *title = dicMain[@"gatewayCode"];
        {
            NSString *ui_title = [title componentsSeparatedByString:@"_"][0];
            IXLCellItem *item = [IXLCellItem itemWithTitle:ui_title
                                               displayType:DisplayTypeDicNormal
                                                   dataDic:dicMain
                                                cellAction:^(NSString *key, id obj) {
                                                    [weakSelf sendActionToUpperResponder:key object:obj];
                                                }];
            [itemArr addObject:item];
        };
    }];
    
    _mainArr = mainArr;
    _subArr = subArr;
    

    temArr_add = [NSMutableArray array];
    __block NSArray *curencyArr;
    if (_insertArr.count) {
        NSDictionary *dic = _insertArr[0];
        NSString *title = dic[@"gatewayCode"];
        NSArray *sepaArr = [title componentsSeparatedByString:@"_"];
        NSString * gatewayCode = sepaArr[0];
        
        [[sortDic allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([[gatewayCode lowercaseString] containsString:[obj lowercaseString]]) {
                curencyArr = sortDic[obj];
            }
        }];
        
        [curencyArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *name = obj;
            [_insertArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([[obj[@"gatewayCode"] lowercaseString] containsString:[name lowercaseString]]) {
                    [temArr_add addObject:obj];
                }
            }];
        }];
        
        if (temArr_add.count) {
            _insertArr = [temArr_add mutableCopy];
        }
    }
    
    for (int i = _insertArr.count - 1; i >=0; i--) {
        NSDictionary *dicMain = _insertArr[i];
        NSString *title = dicMain[@"gatewayCode"];
        NSArray *sepaArr = [title componentsSeparatedByString:@"_"];
        NSString * gatewayCode = sepaArr[0];
        NSString * curency = [sepaArr lastObject];
        curency = [curency stringByReplacingOccurrencesOfString:@"_" withString:@""];
        {
            DisplayTypeDic displayType = DisplayTypeDicNormal;
            if (i == 0) {
                displayType = DisplayTypeDicUnnormal;
                [IXBORequestMgr shareInstance].channelDic = dicMain;
            }
            IXLCellItem *item = [IXLCellItem itemWithTitle:[gatewayCode stringByAppendingFormat:@"-%@",curency]
                                               displayType:displayType
                                                   dataDic:dicMain
                                                cellAction:^(NSString *key, id obj) {
                                                    [weakSelf sendActionToUpperResponder:key object:obj];
                                                }];
            if (_index < transfer_index) {
                [itemArr insertObject:item atIndex:_index+1];
            }else{
                [itemArr insertObject:item atIndex:_index+1];
            }
        };
    }
    
    {
        DisplayTypeDic displayType = DisplayTypeDicNormal;
        if (_index == -1) {
            displayType = DisplayTypeDicUnnormal;
        }
        
        IXLCellItem *item = [IXLCellItem itemWithTitle:@"Transfer Money"
                                           displayType:displayType
                                               dataDic:nil
                                            cellAction:^(NSString *key, id obj) {
                                                [weakSelf sendActionToUpperResponder:key object:obj];
                                            }];
        if (transfer_index > -1) {
            if (_index == -1) {
                @try{
                     [itemArr insertObject:item atIndex:transfer_index];
                }@catch(NSException *exception) {
                     [itemArr insertObject:item atIndex:transfer_index-1];
                }
               
            }else{
                if (_index < transfer_index) {
                    @try{
                        [itemArr insertObject:item atIndex:transfer_index+_insertArr.count];
                    }@catch(NSException *exception) {
                        [itemArr insertObject:item atIndex:transfer_index+_insertArr.count-1];
                    }
                }else{
                    [itemArr insertObject:item atIndex:transfer_index];
                }
            }
            
        }else{
            [itemArr addObject:item];
        }
    };
    
    if (!_insertArr.count && [IXBORequestMgr shareInstance].channelDic) {
        // 无插入
        NSInteger index = [_mainArr indexOfObject:[IXBORequestMgr shareInstance].channelDic];
        if (index > transfer_index && transfer_index != -1) {
            index ++;
        }
        IXLCellItem *item = [itemArr objectAtIndex:index];
        item.displayType = DisplayTypeDicUnnormal;
    }
    
    //
    //    if (!itemArr.count) {
    //        [self showNodataView:@"暂无成交" image:nil];
    //    }
    //
    IXLGroup *group = [IXLGroup groupWithCellItems:itemArr
                                        headerItem:nil
                                        footerItem:nil]; 
    [self.groups addObject:group];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IXLGroup *group = self.groups[indexPath.section];
    IXLCellItem *item = group.cellItems[indexPath.row];
    
    if ([item.title containsString:@"-"]) {
        IXDPSBankCurrencyCell *cell = [IXDPSBankCurrencyCell cellWithTableView:tableView];
        [cell setItem:item];
        return cell;
    }else{
        IXDPSChannelCell *cell = [IXDPSChannelCell cellWithTableView:tableView];
        [cell setItem:item];
        return cell;
    }
    
    return [UITableViewCell new];
}


@end
