//
//  IXSkrillOrNotTV.m
//  IXApp
//
//  Created by Larry on 2018/5/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXSkrillOrNotTV.h"
#import "IXSkrillOrNotCell.h"
#import "IXBORequestMgr+Asset.h"
@implementation IXSkrillOrNotTV

- (void)reloadTableView{
    [self addSection0];
    
    [self reloadData];
};

- (void)addSection0{
    [self.groups removeAllObjects];
    weakself;
    NSMutableArray *itemArr = [NSMutableArray array];
    
    NSString *title; NSString *content;
    
    NSString *gatewayCode = [IXBORequestMgr shareInstance].channelDic[@"gatewayCode"];
    NSString *cardName = [gatewayCode componentsSeparatedByString:@"__"][0];
    cardName = [gatewayCode componentsSeparatedByString:@"_"][0];
    
    {
        title = @"SEND DIRECT";
        content = @"Send directly from cards to bank accounts.";
        
        IXLCellItem *item = [IXLCellItem itemWithTitle:@""
                                           displayType:DisplayTypeDicNormal
                                               dataDic:@{
                                                         @"title":title,
                                                         @"content":content,
                                                         }
                                            cellAction:^(NSString *key, id obj) {
                                                [weakSelf sendActionToUpperResponder:key object:obj];
                                            }];
        [itemArr addObject:item];
    }
    {
        title = @"SKRILL TO SKRILL";
        content = @"Use your balance to send money to an email address.";
        
        IXLCellItem *item = [IXLCellItem itemWithTitle:@""
                                           displayType:DisplayTypeDicNormal
                                               dataDic:@{
                                                         @"title":title,
                                                         @"content":content,
                                                         }
                                            cellAction:^(NSString *key, id obj) {
                                                [weakSelf sendActionToUpperResponder:key object:obj];
                                            }];
        [itemArr addObject:item];
    }
    
    //
    //    if (!itemArr.count) {
    //        [self showNodataView:@"暂无成交" image:nil];
    //    }
    //
    IXLGroup *group = [IXLGroup groupWithCellItems:itemArr
                                        headerItem:nil
                                        footerItem:nil];
    [self.groups addObject:group];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IXLGroup *group = self.groups[indexPath.section];
    IXLCellItem *item = group.cellItems[indexPath.row];
    
    IXSkrillOrNotCell *cell = [IXSkrillOrNotCell cellWithTableView:tableView];
    [cell setItem:item];
    return cell;
    
    return [UITableViewCell new];
}

@end
