//
//  IXSkrillOrNotCell.m
//  IXApp
//
//  Created by Larry on 2018/5/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXChannelOrNotCell.h"

@interface IXChannelOrNotCell()
@property(nonatomic,strong)UILabel *lb_title;
@property(nonatomic,strong) UILabel *lb_detail;
@end

@implementation IXChannelOrNotCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"IXChannelOrNotCell";
    IXChannelOrNotCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[[self class] alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = CellBackgroundColor;
    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubviews];
    }
    return self;
}

- (void)addSubviews{
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self.bgView makeConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(110);
    }];
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = PINGFANG_MEDI_FONT(15);
    lb_title.textColor = HexRGB(0x4c6072);
    lb_title.textAlignment = NSTextAlignmentCenter;
    lb_title.numberOfLines = 0;
    [self.bgView addSubview:lb_title];
    
    _lb_title = lb_title;
    
    
    UILabel *lb_detail = [UILabel new];
    lb_detail.font = PINGFANG_MEDI_FONT(12);
    lb_detail.textColor = HexRGB(0x99abba);
    lb_detail.textAlignment = NSTextAlignmentLeft;
    lb_detail.numberOfLines = 0;
    [self.bgView addSubview:lb_detail];

    _lb_detail = lb_detail;
    
    [self addConstrains];
    
}

- (void)addConstrains{
    [_lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.top.equalTo(20);
    }];
    
    [_lb_detail makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_lb_title);
        make.right.equalTo(-20);
        make.top.equalTo(_lb_title.bottom).offset(10);
        make.bottom.equalTo(-20);
    }];
}


- (void)setItem:(IXLCellItem *)item{
    [super setItem:item];
    
//    NSString *title = item.title;
    NSDictionary *dataDic = item.dataDic;
    
    _lb_title.text = dataDic[@"title"];
    _lb_detail.text = dataDic[@"detail"];
}

@end
