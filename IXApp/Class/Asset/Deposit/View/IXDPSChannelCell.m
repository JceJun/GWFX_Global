//
//  IXDPSChannelCell.m
//  IXApp
//
//  Created by Larry on 2018/5/9.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDPSChannelCell.h"
#import "UIViewExt.h"

@interface IXDPSChannelCell()
@property(nonatomic,strong)UIImageView *imgV;
@property(nonatomic,strong) UILabel *lb_title;
@property(nonatomic,strong)UIImageView *img_sl;
@end

@implementation IXDPSChannelCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"IXDPSChannelCell";
    IXDPSChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[[self class] alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubviews];
    }
    return self;
}

- (void)addSubviews{
    [self.bgView makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(54);
    }];
    
    _lb_title = [UILabel new];
    _lb_title.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    _lb_title.font = [UIFont systemFontOfSize:15];
    _lb_title.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:_lb_title];
    
    _imgV = [UIImageView new];
    _imgV.image = [UIImage imageNamed:@""];
    [self.bgView addSubview:_imgV];
    
    
    _img_sl = [UIImageView new];
    _img_sl.image = [UIImage imageNamed:@"common_cell_choose"];
    [self.bgView addSubview:_img_sl];
    
    
    [self addConstrains];
}

- (void)addConstrains{
    [_imgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.centerY.equalTo(self.bgView);
    }];
    
    [_lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imgV.right).offset(10);
        make.centerY.equalTo(self.bgView);
    }];
    
    [_img_sl makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-15);
        make.centerY.equalTo(self.bgView);
    }];
    
}


- (void)setItem:(IXLCellItem *)item{
    [super setItem:item];
    
    NSString *title = item.title;
    NSDictionary *dataDic = item.dataDic;
    
    _lb_title.text = title;
    if ([title isEqualToString:@"asiapay"]) {
        _lb_title.text = @"Credit card";
    }else if ([title isEqualToString:@"creditcardpay"]){
        _lb_title.text = @"Debit card";
    }
    
    if ([[item.title lowercaseString] containsString:@"skrill"]) {
        _imgV.image = [UIImage imageNamed:@"dps_Skrill"];
    }else if ([[item.title lowercaseString] containsString:@"help2pay"]){
        _imgV.image = [UIImage imageNamed:@"dps_Help2Pay"];
    }else if ([[item.title lowercaseString] containsString:@"transfer money"]){
        _imgV.image = [UIImage imageNamed:@"dps_teleTransfer"];
    }else if ([[item.title lowercaseString] containsString:@"fasapay"]){
        _imgV.image = [UIImage imageNamed:@"dps_fasaapy"];
    }else if ([[item.title lowercaseString] containsString:@"credit"]){
        _imgV.image = [UIImage imageNamed:@"cardPay"];
    }else if ([[item.title lowercaseString] containsString:@"epay"]){
        _imgV.image = [UIImage imageNamed:@"dps_epay"];
    }else if ([[item.title lowercaseString] containsString:@"sticpay"]){
        _imgV.image = [UIImage imageNamed:@"dps_sticpay"];
    }else{
        _imgV.image = [UIImage imageNamed:@"dps_default"];
    }
    
    if ([dataDic[@"cardpayType"] isEqualToString:@"credit"]) {
        _imgV.image = [UIImage imageNamed:@"cardPay"];
    }
    if ([title isEqualToString:@"creditcardpay"]){
        _imgV.image = [UIImage imageNamed:@"dps_debit"];
    }
    
    
    if (item.displayType == DisplayTypeDicNormal) {
        _img_sl.hidden = YES;
    }else{
        _img_sl.hidden = NO;
    }
}

@end
