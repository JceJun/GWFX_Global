//
//  IXDPSBankCurrencyCell.m
//  IXApp
//
//  Created by Larry on 2018/5/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDPSBankCurrencyCell.h"

@interface IXDPSBankCurrencyCell()
@property(nonatomic,strong) UILabel *lb_title;
@property(nonatomic,strong)UIImageView *imgV;
@property(nonatomic,strong)UIImageView *img_crrency;

@end

@implementation IXDPSBankCurrencyCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"IXDPSBankCurrencyCell";
    IXDPSBankCurrencyCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[[self class] alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubviews];
    }
    return self;
}

- (void)addSubviews{
    self.bgView.dk_backgroundColorPicker = DKTableHeaderColor;
    [self.bgView makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(44);
    }];
    
    [self.lineView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(55);
    }];
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = PINGFANG_MEDI_FONT(15);
    lb_title.textAlignment = NSTextAlignmentCenter;
    lb_title.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    lb_title.numberOfLines = 0;
    [self.bgView addSubview:lb_title];
    
    _lb_title = lb_title;
    
    _imgV = [UIImageView new];
    _imgV.image = [UIImage imageNamed:@"common_cell_choose"];
    [self.bgView addSubview:_imgV];
    _imgV.hidden = YES;
    
    
    _img_crrency = [UIImageView new];
    _img_crrency.image = [UIImage imageNamed:@""];
    [self.bgView addSubview:_img_crrency];
    
    
    [self addConstrains];
    
}

- (void)addConstrains{
    [_img_crrency makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(55);
        make.centerY.equalTo(self.bgView);
    }];
    
    [_lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_img_crrency.right).offset(15);
        make.centerY.equalTo(self.bgView);
    }];
    
    [_imgV makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-15);
        make.centerY.equalTo(self.bgView);
    }];
}


- (void)setItem:(IXLCellItem *)item{
    [super setItem:item];
    
    NSString *title = item.title;
//    NSDictionary *dataDic = item.dataDic;
    
    _lb_title.text = [title componentsSeparatedByString:@"-"][1];
    NSString *currencyImg = [NSString stringWithFormat:@"dps_%@",[[title componentsSeparatedByString:@"-"][1] uppercaseString]];
    _img_crrency.image = [UIImage imageNamed:currencyImg];
    if (!_img_crrency.image) {
        NSString *currencyImg = [NSString stringWithFormat:@"dps_%@",[[title componentsSeparatedByString:@"-"][1] lowercaseString]];
        _img_crrency.image = [UIImage imageNamed:currencyImg];
    }
    if (!_img_crrency.image) {
        _img_crrency.image = [UIImage imageNamed:@"dps_currency_df"];
    }
    if ([[title lowercaseString] containsString:@"visa"]) {
        _img_crrency.image = [UIImage imageNamed:@"dps_visa"];
    }else if ([[title lowercaseString] containsString:@"skrill wallet"]){
        _img_crrency.image = [UIImage imageNamed:@"dps_senddirect"];
    }else if ([[title lowercaseString] containsString:@"skrill to skrill"]){
        _img_crrency.image = [UIImage imageNamed:@"dps_skrilltoskrill"];
    }
    
    
    if (item.displayType == DisplayTypeDicNormal) {
        _imgV.hidden = YES;
    }else{
        _imgV.hidden = NO;
    }
    
}

@end
