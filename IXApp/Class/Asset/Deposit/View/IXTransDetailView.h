//
//  IXTransDetailView.h
//  IXApp
//
//  Created by Larry on 2018/6/27.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXTransDetailView : UIView
+ (instancetype)show;
+ (void)dismiss;
@end
