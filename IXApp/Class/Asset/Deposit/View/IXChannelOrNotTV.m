//
//  IXSkrillOrNotTV.m
//  IXApp
//
//  Created by Larry on 2018/5/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXChannelOrNotTV.h"
#import "IXChannelOrNotCell.h"
#import "IXBORequestMgr+Asset.h"
@implementation IXChannelOrNotTV

- (void)reloadTableView{
    [self addSection0];
    
    [self reloadData];
};

- (void)addSection0{
    [self.groups removeAllObjects];
    weakself;
    NSMutableArray *itemArr = [NSMutableArray array];
    
    NSString *title; NSString *detail;
    
    NSString *gatewayCode = [IXBORequestMgr shareInstance].channelDic[@"gatewayCode"];
    NSString *cardName = [gatewayCode componentsSeparatedByString:@"__"][0];
    cardName = [gatewayCode componentsSeparatedByString:@"_"][0];
    
    {
        title = [NSString stringWithFormat:@"1. Already have a %@ Account",cardName];
        detail = [NSString stringWithFormat:@"Please enter your %@ Account's password to deposit",cardName];
        
        IXLCellItem *item = [IXLCellItem itemWithTitle:@""
                                           displayType:DisplayTypeDicNormal
                                               dataDic:@{
                                                         @"title":title,
                                                         @"detail":detail,
                                                         }
                                            cellAction:^(NSString *key, id obj) {
                                                [weakSelf sendActionToUpperResponder:key object:obj];
                                            }];
        [itemArr addObject:item];
    }
    {
        title = [NSString stringWithFormat:@"2. Do not have a %@ Account",cardName];
        detail = [NSString stringWithFormat:@"Please register a %@ Account before making deposit",cardName];
        
        IXLCellItem *item = [IXLCellItem itemWithTitle:@""
                                           displayType:DisplayTypeDicNormal
                                               dataDic:@{
                                                         @"title":title,
                                                         @"detail":detail,
                                                         }
                                            cellAction:^(NSString *key, id obj) {
                                                [weakSelf sendActionToUpperResponder:key object:obj];
                                            }];
        [itemArr addObject:item];
    }
    
    //
    //    if (!itemArr.count) {
    //        [self showNodataView:@"暂无成交" image:nil];
    //    }
    //
    IXLGroup *group = [IXLGroup groupWithCellItems:itemArr
                                        headerItem:nil
                                        footerItem:nil];
    [self.groups addObject:group];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IXLGroup *group = self.groups[indexPath.section];
    IXLCellItem *item = group.cellItems[indexPath.row];
    
    IXChannelOrNotCell *cell = [IXChannelOrNotCell cellWithTableView:tableView];
    [cell setItem:item];
    return cell;
    
    return [UITableViewCell new];
}

@end
