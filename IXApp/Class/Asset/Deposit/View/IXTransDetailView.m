//
//  IXTransDetailView.m
//  IXApp
//
//  Created by Larry on 2018/6/27.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXTransDetailView.h"
#import "UIKit+Block.h"
@implementation IXTransDetailView

+ (instancetype)show{
    IXTransDetailView *view = [IXTransDetailView new];
    view.tag = 4444;
    view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [KEYWINDOW addSubview:view];
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(KEYWINDOW);
    }];
//    [view block_whenTapped:^(UIView *aView) {
//        [IXTransDetailView dismiss];
//    }];
    
    UIView *contentView = [UIView new];
    contentView.backgroundColor = [UIColor whiteColor];
    [view addSubview:contentView];
    
    [contentView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(30);
        make.right.equalTo(-30);
        make.top.equalTo(50);
        make.height.equalTo(440);
    }];
    
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = ROBOT_FONT(15);
    lb_title.dk_textColorPicker = DKCellTitleColor;
    lb_title.textAlignment = NSTextAlignmentCenter;
    lb_title.numberOfLines = 0;
    [contentView addSubview:lb_title];
    lb_title.text = @"Transaction Details";
    
    [lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(60);
        make.centerX.equalTo(0);
    }];
    
    NSArray *leftArr = @[@"Type",@"To",@"Date",@"Amount",@"Status",@"Transaction ID"];
    NSArray *rightArr = @[@"Send",
                          @"Goldenway Global Investments\n(New Zealand) Limited\nfxsettlement@222m.net",
                          @"08 Jun 18 14:08",
                          @"-1.00",
                          @"Processed",
                          @"2407778026",
                          ];
    
    
    __block UIView *lastView = nil;
    for (int i = 0; i < leftArr.count; i++) {
        UIView *itemView = [[self class] makeItemView:leftArr[i] rText:rightArr[i] superView:contentView];
        [itemView makeConstraints:^(MASConstraintMaker *make) {
            if (!lastView) {
                make.top.equalTo(lb_title.bottom).offset(50);
            }else{
                make.top.equalTo(lastView.bottom).offset(20);
            }
        }];
        lastView = itemView;
    }
    
    UIImageView *img_close = [UIImageView new];
    img_close.image = [UIImage imageNamed:@"ad_close"];
    [contentView addSubview:img_close];
    [img_close makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-15);
        make.top.equalTo(15);
    }];
    [img_close block_whenTapped:^(UIView *aView) {
        [IXTransDetailView dismiss];
    }];
    
    return view;
}

+ (void)dismiss{
    UIView *view = [KEYWINDOW viewWithTag:4444];
    if (view) {
        [view removeFromSuperview];
    }
}

+ (UIView *)makeItemView:(NSString *)lText rText:(NSString *)rText superView:(UIView *)superView{
    UIView *itemView = [UIView new];
    itemView.backgroundColor = [UIColor whiteColor];
    [superView addSubview:itemView];
    
    [itemView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    
    UILabel *lb_left = [UILabel new];
    lb_left.font = ROBOT_FONT(13);
    lb_left.dk_textColorPicker = DKCellContentColor;
    [itemView addSubview:lb_left];
    lb_left.text = lText;
    [lb_left makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.top.equalTo(10);
    }];
    
    UILabel *lb_right = [UILabel new];
    lb_right.font = ROBOT_FONT(13);
    lb_right.dk_textColorPicker = DKCellTitleColor;
    lb_right.textAlignment = NSTextAlignmentRight;
    lb_right.numberOfLines = 0;
    [itemView addSubview:lb_right];
    lb_right.text = rText;
    [lb_right makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lb_left.right).offset(30);
        make.right.equalTo(-15);
        make.top.equalTo(lb_left);
    }];
    
    [itemView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(lb_right.bottom).offset(-10);
    }];
    
    return itemView;
}

@end
