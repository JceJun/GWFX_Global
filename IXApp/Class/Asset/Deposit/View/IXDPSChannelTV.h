//
//  IXDPSAmountTV.h
//  IXApp
//
//  Created by Larry on 2018/5/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXNormalTableView.h"

@interface IXDPSChannelTV : IXNormalTableView
@property(nonatomic,strong)NSMutableArray *mainArr; //  第一层渠道
@property(nonatomic,strong)NSMutableArray *subArr;  //  第二层货币
@property(nonatomic,strong)NSMutableArray *insertArr;
@property(nonatomic,assign)NSInteger index;

- (void)reloadTableViewInsertArr:(NSMutableArray *)insertArr AtIndex:(NSInteger)index;
@end
