//
//  IXDPSChannelVC.h
//  IXApp
//
//  Created by Larry on 2018/5/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXIncomeModel;

@interface IXDPSChannelVC : IXDataBaseVC
@property (nonatomic, strong)IXIncomeModel  * incashModel;
@end
