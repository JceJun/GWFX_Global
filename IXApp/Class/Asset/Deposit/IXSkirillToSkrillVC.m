//
//  IXSkirillToSkrillVC.m
//  IXApp
//
//  Created by Larry on 2018/6/25.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXSkirillToSkrillVC.h"
#import "YYText.h"
#import "UIKit+Block.h"
#import "IXWebCustomerSeviceVC.h"
#import "IXTransDetailView.h"
#import "ChatTableViewController.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Asset.h"
#import "IXCommonWebVC.h"

@interface IXSkirillToSkrillVC ()
@property(nonatomic,strong)UIView *step1View;
@property(nonatomic,strong)UIView *step2View;
@property(nonatomic,strong)UIScrollView *scrollView;
@end

@implementation IXSkirillToSkrillVC

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(self.view);
        }];
        
    }
    return _scrollView;
}

- (UIView *)step1View{
    if (!_step1View) {
        
        _step1View = [UIView new];
        _step1View.dk_backgroundColorPicker = DKNavBarColor;
        [self.scrollView addSubview:_step1View];
        [_step1View makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(5);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(220);
        }];
        
        UILabel *lb_step = [UILabel new];
        lb_step.font = ROBOT_FONT(15);
        lb_step.dk_textColorPicker = DKCellTitleColor;
        lb_step.numberOfLines = 0;
        lb_step.text = @"STEP 1";
        [_step1View addSubview:lb_step];
        [lb_step makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(15);
        }];
        
        UILabel *lb_line1 = [UILabel new];
        lb_line1.font = ROBOT_FONT(12);
        lb_line1.dk_textColorPicker = DKCellContentColor;
        lb_line1.numberOfLines = 0;
        [_step1View addSubview:lb_line1];
        [lb_line1 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(lb_step.bottom).offset(10);
        }];
        lb_line1.text = @"Transfer money via skrill account";
        
        UIView *bgView = [UIView new];
        bgView.dk_backgroundColorPicker = DKViewColor;
        [_step1View addSubview:bgView];
        [bgView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lb_line1.bottom).offset(10);
            make.left.equalTo(15);
            make.width.equalTo(kScreenWidth - 15*2);
            make.height.equalTo(130);
        }];
        bgView.layer.cornerRadius = 5;
        bgView.layer.masksToBounds = YES;
        
        UILabel *lb_line2 = [UILabel new];
        lb_line2.font = ROBOT_FONT(12);
        lb_line2.dk_textColorPicker = DKCellContentColor;
        lb_line2.numberOfLines = 0;
        [bgView addSubview:lb_line2];
        [lb_line2 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(10);
        }];
        lb_line2.text  =@"Our Company Information:\nSkrill Account";
        
        UILabel *lb_line3 = [UILabel new];
        lb_line3.font = ROBOT_FONT(13);
        lb_line3.dk_textColorPicker = DKCellTitleColor;
        lb_line3.numberOfLines = 0;
        [bgView addSubview:lb_line3];
        [lb_line3 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(lb_line2.bottom).offset(10);
        }];
        lb_line3.text = @"fxsettlement@222m.net";
        
        UILabel *lb_line4 = [UILabel new];
        lb_line4.font = ROBOT_FONT(12);
        lb_line4.dk_textColorPicker = DKViewColor;
        lb_line4.dk_backgroundColorPicker = DKCellTitleColor;
        lb_line4.textAlignment = NSTextAlignmentCenter;
        [bgView addSubview:lb_line4];
        [lb_line4 makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(-15);
            make.centerY.equalTo(lb_line3);
            make.width.equalTo(62);
            make.height.equalTo(24);
        }];
        lb_line4.text  =@"Copy";
        lb_line4.layer.cornerRadius = 10;
        lb_line4.layer.masksToBounds = YES;
        [lb_line4 block_whenTapped:^(UIView *aView) {
            UIPasteboard *pab = [UIPasteboard generalPasteboard];
            NSString *string = lb_line3.text;
            [pab setString:string];
            
            [SVProgressHUD showSuccessWithStatus:@"Copy Success"];
        }];
        
        UILabel *lb_line5 = [UILabel new];
        lb_line5.font = ROBOT_FONT(12);
        lb_line5.dk_textColorPicker = DKCellContentColor;
        lb_line5.numberOfLines = 0;
        [bgView addSubview:lb_line5];
        [lb_line5 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(lb_line3.bottom).offset(20);
        }];
        lb_line5.text = @"Minimum Deposit";
        
        UILabel *lb_line6 = [UILabel new];
        lb_line6.font = ROBOT_FONT(13);
        lb_line6.dk_textColorPicker = DKCellTitleColor;
        lb_line6.numberOfLines = 0;
        [bgView addSubview:lb_line6];
        [lb_line6 makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(-15);
            make.centerY.equalTo(lb_line5);
        }];
        
        NSString *minPayamount = [IXBORequestMgr shareInstance].channelDic[@"minPayamount"];
        lb_line6.text = [NSString stringWithFormat:@"%@ $",minPayamount];
        
        UIView *bottomLine = [UIView new];
        bottomLine.dk_backgroundColorPicker = DKLineColor;
        [_step1View addSubview:bottomLine];
        [bottomLine makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.top.equalTo(bgView.bottom).offset(10);
            make.height.equalTo(0.5);
        }];
    }
    return _step1View;
}

- (UIView *)step2View{
    if (!_step2View) {
        _step2View = [UIView new];
        _step2View.userInteractionEnabled = YES;
        _step2View.dk_backgroundColorPicker = DKNavBarColor;
        [self.scrollView addSubview:_step2View];
        
        [_step2View makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_step1View.bottom).offset(10);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(300);
        }];
        
        [_step2View block_whenTapped:^(UIView *aView) {
           
        }];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = ROBOT_FONT(15);
        lb_title.dk_textColorPicker = DKCellTitleColor;
        [_step2View addSubview:lb_title];
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(15);
        }];
        lb_title.text =  @"SETP 2";
        
        UILabel *lb_line1 = [UILabel new];
        lb_line1.font = ROBOT_FONT(12);
        lb_line1.numberOfLines = 0;
        lb_line1.dk_textColorPicker = DKCellTitleColor;
        [_step2View addSubview:lb_line1];
        [lb_line1 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(lb_title.bottom).offset(10);
            make.width.equalTo(kScreenWidth - 15 * 2);
        }];
        weakself;
        [self textToYY:@"Please contact our customer services to verify remittance (Money Transfer) information.\nNote:Sample of Remittance (Money Transfer) Verification" label:lb_line1];
        [lb_line1 block_whenTapped:^(UIView *aView) {
            [weakSelf showMuskTipView];
        }];
        
        UIView *bgView = [UIView new];
        bgView.dk_backgroundColorPicker = DKViewColor;
        [_step2View addSubview:bgView];
        [bgView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lb_line1.bottom).offset(10);
            make.left.equalTo(15);
            make.width.equalTo(kScreenWidth - 15*2);
            make.height.equalTo(170);
        }];
        bgView.layer.cornerRadius = 5;
        bgView.layer.masksToBounds = YES;
        
        UILabel *lb_line2 = [UILabel new];
        lb_line2.font = ROBOT_FONT(13);
        lb_line2.dk_textColorPicker = DKCellTitleColor;
        lb_line2.numberOfLines = 0;
        [bgView addSubview:lb_line2];
        [lb_line2 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(35);
            make.right.equalTo(-15);
            make.top.equalTo(10);
        }];
        
        [self textToYY:@"Please Click (Icon " label:lb_line2];
        
        [lb_line2 block_whenTapped:^(UIView *aView) {
            [weakSelf appChat];
        }];
        
        
        UILabel *lb_line3 = [UILabel new];
        lb_line3.font = ROBOT_FONT(13);
        lb_line3.dk_textColorPicker = DKCellTitleColor;
        lb_line3.numberOfLines = 0;
        [bgView addSubview:lb_line3];
        [lb_line3 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(35);
            make.right.equalTo(-15);
            make.top.equalTo(lb_line2.bottom).offset(15);
        }];
        lb_line3.text = @"WhatsApp: +886 989029990\nLine:gwfxcs01";
        
        UILabel *lb_line4 = [UILabel new];
        lb_line4.font = ROBOT_FONT(13);
        lb_line4.dk_textColorPicker = DKCellTitleColor;
        [bgView addSubview:lb_line4];
        [lb_line4 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(35);
            make.right.equalTo(-15);
            make.top.equalTo(lb_line3.bottom).offset(15);
        }];
        lb_line4.text = @"Email address: cs@gwfxglobal.com";
        
        for (int i = 0;  i < 3; i++) {
            UIImageView *imgV_mark = [UIImageView new];
            imgV_mark.image = [UIImage imageNamed:@"dps_mark"];
            [bgView addSubview:imgV_mark];
        
            [imgV_mark makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(10);
                switch (i) {
                    case 0:{
                        make.top.equalTo(20);
                    }break;
                    case 1:{
                        make.top.equalTo(lb_line3).offset(3);
                    }break;
                    case 2:{
                        make.top.equalTo(lb_line4).offset(3);
                    }break;
                }
            }];
        }
        
    }
    
    return _step2View;
}

- (void)showMuskTipView{
    UIView *view = [UIView new];
    view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    [KEYWINDOW addSubview:view];
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(KEYWINDOW);
    }];
    [view block_whenTapped:^(UIView *aView) {
        [aView removeFromSuperview];
    }];
    
    UIImageView *imgV2 = [UIImageView new];
    imgV2.image = [UIImage imageNamed:@"skrill_tip1"];
    [view addSubview:imgV2];
    [imgV2 makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(0);
    }];
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(18);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"I Know" forState:UIControlStateNormal];
    nextBtn.userInteractionEnabled = NO;
    [view addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.height.equalTo(44);
        make.top.equalTo(imgV2.bottom).offset(40);
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showProgressWithAimStep:1 totalStep:3 originY:0];
    
    self.title = LocalizedString(@"存入资金");
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    [self addBackItem];
    [self addCancelItem];
    
    self.scrollView.delaysContentTouches = NO;
    self.scrollView.canCancelContentTouches = YES;
    [self step1View];
    
    [self step2View];
    
    [self.scrollView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.step2View.bottom).offset(150);
    }];
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"Skip to skrill" forState:UIControlStateNormal];
    [self.scrollView addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.width.equalTo(kScreenWidth - 15*2);
        make.top.equalTo(self.step2View.bottom).offset(20);
        make.height.equalTo(44);
    }];
    
    weakself;
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        NSString *urlStr = @"https://account.skrill.com/login?locale=en";
        [weakSelf gotoWeb:urlStr];
    }];
    
    [self lb_CSSupport];
    
}

- (void)lb_CSSupport{
    UILabel *lb_CSSupport = [UILabel new];
    lb_CSSupport.font = PINGFANG_MEDI_FONT(15);
    lb_CSSupport.textColor = HexRGB(0x4c6072);
    lb_CSSupport.textAlignment = NSTextAlignmentCenter;
    lb_CSSupport.numberOfLines = 0;
    [self.scrollView addSubview:lb_CSSupport];
    
    lb_CSSupport.text = @"CS Support";
    
    [lb_CSSupport makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-15);
        make.centerX.equalTo(self.scrollView);
    }];
    
    weakself;
    [lb_CSSupport block_whenTapped:^(UIView *aView) {
        IXWebCustomerSeviceVC *vc = [IXWebCustomerSeviceVC new];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
}


- (void)textToYY:(NSString *)title label:(UILabel *)label{
    NSString *textStr;
    NSMutableAttributedString *text;
    
    NSString *rangeStr0;
    NSString *rangeStr1;
    
    NSRange range0;
    NSRange range1;
    
    if ([title containsString:@"Please contact our customer"]) {
        rangeStr0 = @"Please contact our customer services to verify remittance (Money Transfer) information.\nNote:";
        rangeStr1 = @"Sample of Remittance (Money Transfer) Verification";
        
        textStr = [NSString stringWithFormat:@"%@%@",rangeStr0,rangeStr1];
        text = [[NSMutableAttributedString alloc] initWithString:textStr];
        
        range0 = [[text string] rangeOfString:rangeStr0 options:NSCaseInsensitiveSearch];
        [text yy_setColor:HexRGB(0x99abba)  range:range0];
    
        range1 = [[text string] rangeOfString:rangeStr1 options:NSCaseInsensitiveSearch];
        [text setAttributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)}
                         range:range1];
        
        [text yy_setTextHighlightRange:range1
                                 color:HexRGB(0x4c6072)
                       backgroundColor:[UIColor clearColor]
                             tapAction:^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect){
                                 NSLog(@"");
                             }];

    }else{
        NSString *rangeStr2;
        NSRange range2;
        
        rangeStr0 = @"Please Click (Icon   ";
        rangeStr1 = @"  ) to contact our CS representative";
        rangeStr2 = @"(Service Hours GMT+0 01:00-10:00)";
        
        NSRange range1;
        
        // 创建图片图片附件
        NSTextAttachment *attach = [[NSTextAttachment alloc] init];
        attach.image = [UIImage imageNamed:@"web_customService"];
        attach.bounds = CGRectMake(0, 0, 21, 21);
        NSAttributedString *attachString = [NSAttributedString attributedStringWithAttachment:attach];
        
        textStr = [NSString stringWithFormat:@"%@%@%@",rangeStr0,rangeStr1,rangeStr2];
        text = [[NSMutableAttributedString alloc] initWithString:textStr];
        
        range0 = [[text string] rangeOfString:rangeStr0 options:NSCaseInsensitiveSearch];
        [text yy_setColor:HexRGB(0x4c6072) range:range0];
        
        range1 = [[text string] rangeOfString:rangeStr1 options:NSCaseInsensitiveSearch];
        [text yy_setColor:HexRGB(0x4c6072) range:range1];
        
        range2 = [[text string] rangeOfString:rangeStr2 options:NSCaseInsensitiveSearch];
        [text yy_setColor:HexRGB(0x99abba) range:range2];
        
        //将图片插入到合适的位置
        [text insertAttributedString:attachString atIndex:20];
    }
    
    //文字间距
    // [text yy_setKern:@(2) range:range0];
    // 行间距
//    text.yy_lineSpacing = 8;
    
    //下划线
//    [text yy_setTextUnderline:decoration range:range0];
    
    // 高亮
//    UILabel *lb_header = [UIView getSubObjFromSupperObj:self.table.tableHeaderView bySubObjectKey:@"lb_bottom"];
    label.attributedText = text;
}

- (void)appChat{
    // Native客服中心
    ChatTableViewController *vc = [[ChatTableViewController alloc] initWithPid:ServicePid
                                                                           key:ServiceKey
                                                                           url:kServiceUrl];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)gotoWeb:(NSString  *)url{
    IXCommonWebVC * vc = [[IXCommonWebVC alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    vc.url = url;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
