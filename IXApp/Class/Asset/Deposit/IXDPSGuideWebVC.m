//
//  IXDPSGuideWebVC.m
//  IXApp
//
//  Created by Larry on 2018/5/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDPSGuideWebVC.h"
#import "IXBORequestMgr+Asset.h"

@interface IXDPSGuideWebVC ()
@property(nonatomic,strong)UIWebView *webView;
@end

@implementation IXDPSGuideWebVC

- (UIWebView *)webView{
    if (!_webView) {
        _webView = [UIWebView new];
        [self.view addSubview:_webView];
        
        [_webView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = LocalizedString(@"存入资金");
    [self addBackItem];
//    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
    
    
    
    NSString *guideUrl = [IXBORequestMgr shareInstance].channelDic[@"guideUrl"];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:guideUrl]]];
    
}

//- (void)cancelAction{
//    [self.navigationController popToRootViewControllerAnimated:YES];
//}


@end
