//
//  IXCreditCardInfoVC.m
//  IXApp
//
//  Created by Larry on 2018/6/13.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCreditCardInfoVC.h"
#import "IXComInputView.h"
#import "UIKit+Block.h"
#import "IXDPSAmountVC.h"
#import "IXIncomeModel.h"
#import "IXCountryListVC.h"
#import "IXUserDefaultM.h"
#import "IXAppUtil.h"
#import <UMMobClick/MobClick.h>
#import "IXCpyConfig.h"

@interface IXCreditCardInfoVC ()<UITextFieldDelegate>
@property(nonatomic,strong)UIView *headerV;
@property(nonatomic,strong)UIButton *nextBtn;
@property(nonatomic,strong)IXComInputView *input_fName;
@property(nonatomic,strong)IXComInputView *input_lName;
@property(nonatomic,strong)IXComInputView *input_phone;
@property(nonatomic,strong)IXComInputView *input_email;

@property(nonatomic,copy)NSString *fName;
@property(nonatomic,copy)NSString *lName;
@property(nonatomic,copy)NSString *phone;
@property(nonatomic,copy)NSString *email;
@property (nonatomic, strong)IXCountryM    *countryInfo;

@end

@implementation IXCreditCardInfoVC

- (UIView *)headerV{
    if (!_headerV) {
        _headerV = [UIView new];
        _headerV.dk_backgroundColorPicker = DKTableHeaderColor;
        [self.view addSubview:_headerV];
        
        [_headerV makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(0);
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.height.equalTo(80);
        }];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = PF_MEDI(15);
        lb_title.dk_textColorPicker = DKCellTitleColor;
        lb_title.textAlignment = NSTextAlignmentCenter;
        lb_title.numberOfLines = 0;
        lb_title.text = @"Bank Reserved Information";
        [_headerV addSubview:lb_title];
        
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(0);
        }];
    }
    return _headerV;
}

- (UIButton *)nextBtn{
    if (!_nextBtn) {
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        
        UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
        dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
        
        UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.titleLabel.font = PF_REGU(15);
        [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
        [nextBtn setTitle:LocalizedString(@"下一步") forState:UIControlStateNormal];
        [self.view addSubview:nextBtn];
        
        [nextBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.right.equalTo(-15);
            make.height.equalTo(44);
        }];
        
        weakself;
        [nextBtn block_touchUpInside:^(UIButton *aButton) {
            [weakSelf gotoNext:@"下一步"];
        }];
        nextBtn.enabled = NO;
        _nextBtn = nextBtn;
    }
    return _nextBtn;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (UMAppKey.length) {
        [MobClick event:UM_depositCredit];
    }
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;

    [self addBackItem];
    self.title = LocalizedString(@"Add Bank Account");
    
    [self headerV];
    weakself;
    IXComInputView *input_fName =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"First Name" rPlaceHolder:@"Please enter your first name" topLineStyle:TopLineStyleFull bottomLineStyle:BottomStyleSeparate];
    input_fName.tf_right.delegate = self;
    [self.view addSubview:input_fName];
    
    [input_fName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerV.bottom).offset(0);
    }];
    _input_fName = input_fName;
    
    IXComInputView *input_lName =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Last Name" rPlaceHolder:@"Please enter your last name" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleSeparate];
    input_lName.tf_right.delegate = self;
    [self.view addSubview:input_lName];
    
    [input_lName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_fName.bottom).offset(0);
    }];
    _input_lName = input_lName;
    
    
    IXComInputView *input_phone =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"-- ▾" rPlaceHolder:@"Please enter your phone number" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleSeparate];
    [input_phone.lb_left block_whenTapped:^(UIView *aView) {
        [weakSelf gotoNext:@"选择区号"];
    }];
    input_phone.tf_right.keyboardType = UIKeyboardTypeNumberPad;
    input_phone.tf_right.delegate = self;
    [self.view addSubview:input_phone];
    
    [input_phone makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_lName.bottom).offset(0);
    }];
    _input_phone = input_phone;
    
    IXComInputView *input_email =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Email" rPlaceHolder:@"Please enter your email address" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleFull];
    input_email.tf_right.delegate = self;
    [self.view addSubview:input_email];
    
    [input_email makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_phone.bottom).offset(0);
    }];
    _input_email = input_email;
    
    
    [self.nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_email.bottom).offset(50);
    }];
    
    UILabel *lb_tip1 = [UILabel new];
    lb_tip1.font = ROBOT_FONT(12);
    lb_tip1.dk_textColorPicker = DKCellContentColor;
    lb_tip1.numberOfLines = 0;
    lb_tip1.text = @"Faild to pay?";
    lb_tip1.textAlignment = NSTextAlignmentCenter;
    lb_tip1.numberOfLines = 0;
    [self.view addSubview:lb_tip1];
    
    [lb_tip1 makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.top.equalTo(self.view.bottom).offset(-50);
    }];
    
    UILabel *lb_tip2 = [UILabel new];
    lb_tip2.font = ROBOT_FONT(12);
    lb_tip2.dk_textColorPicker = DKCellTitleColor;
    lb_tip2.numberOfLines = 0;
    lb_tip2.text = @"Please call your bank to authorize this payment";
    lb_tip2.textAlignment = NSTextAlignmentCenter;
    lb_tip2.numberOfLines = 0;
    [self.view addSubview:lb_tip2];
    
    [lb_tip2 makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.top.equalTo(lb_tip1.bottom).offset(3);
    }];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setCountry:[IXUserDefaultM nationalityByCode:[IXUserDefaultM getCode]]];
}


- (void)gotoNext:(NSString *)title{
    [self.view endEditing:YES];
    if (SameString(title, @"下一步")) {
        if ([self checkInput]) {
            if (UMAppKey.length) {
                [MobClick event:UM_depositCreditNS];
            }
            
            IXIncomeModel *model = [IXIncomeModel new];
            model.firstName = _fName;
            model.lastName = _lName;
            model.mobileNumber = _phone;
            model.email = _email;
            model.mobileNumberPrefix = [@(_countryInfo.countryCode) stringValue];
            
            IXDPSAmountVC *vc = [IXDPSAmountVC new];
            vc.incashModel = model;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (SameString(title, @"选择区号")){
        weakself;
        IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
            [weakSelf setCountry:country];
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)setCountry:(IXCountryM *)country
{
    if (!self.countryInfo) {
        self.countryInfo = [[IXCountryM alloc] init];
    }
    //默认中国
    if (!country) {
        NSString *nationalCode = [IXUserDefaultM getCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityByCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        [IXUserDefaultM saveCountryCode:countryM.nationalCode];
        
    } else {
        self.countryInfo.nameCN = country.nameCN;
        self.countryInfo.nameEN = country.nameEN;
        self.countryInfo.nameTW = country.nameTW;
        self.countryInfo.countryCode = country.countryCode;
        [IXUserDefaultM saveCountryCode:country.nationalCode];
    }
    
    NSString *codeStr = [@"+" stringByAppendingString:[@(country.countryCode) stringValue]];
    codeStr =  [codeStr stringByAppendingString:@"   ▾"];
    self.input_phone.lb_left.text = codeStr;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //不接受输入空格
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:NONESPACECHAR];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self activeButton];
}

- (void)activeButton{
    _fName = _input_fName.tf_right.text;
    _lName = _input_lName.tf_right.text;
    _phone = _input_phone.tf_right.text;
    _email = _input_email.tf_right.text;
    if (_fName.length && _lName.length && _phone.length && _email.length) {
        self.nextBtn.enabled = YES;
    }else{
        self.nextBtn.enabled = NO;
    }
}

- (BOOL)checkInput{
    if (![IXAppUtil isValidateEmail:_email]) {
        [SVProgressHUD showInfoWithStatus:LocalizedString(@"请输入正确的邮箱号")];
        return NO;
    }
    return YES;
}

@end
