//
//  IXAddBankCardTypeVC.m
//  IXApp
//
//  Created by Larry on 2018/8/9.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXAddBankCardTypeVC.h"
#import "UIKit+Block.h"
#import "IXWithdrawBankAddVC.h"
#import "IXAddHelp2PayBankVC.h"
#import "IXBORequestMgr+Comp.h"

@interface IXAddBankCardTypeVC ()
@property(nonatomic,strong)UIScrollView *scrollView;
@end

@implementation IXAddBankCardTypeVC

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.width.equalTo(kScreenWidth);
            make.top.equalTo(self.view);
            make.height.equalTo(self.view);
        }];
    }
    return _scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"My Bank Account";
    [self addBackItem];
    UIView *view1 = [self makeItemByName:@"Help2Pay" detail:[IXBORequestMgr shareInstance].mobileOnlineConfig[@"help2payPaymentDateTips"]];
    [view1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
    }];
    [self.scrollView addSubview:view1];
    
    UIView *view2 = [self makeItemByName:@"Telegraphic Transfer" detail:[IXBORequestMgr shareInstance].mobileOnlineConfig[@"transferPaymentDateTips"]];
    [view2 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view1.bottom).offset(10);
    }];
    [self.scrollView addSubview:view2];
    
}

- (UIView *)makeItemByName:(NSString *)name detail:(NSString *)detail{
    UIView *view = [UIView new];
    view.dk_backgroundColorPicker = DKNavBarColor;
    [self.scrollView addSubview:view];
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.width.equalTo(kScreenWidth);
        make.height.equalTo(70);
    }];
    
    UIImageView *imgV = [UIImageView new];
    [view addSubview:imgV];
    if ([name containsString:@"Help2Pay"]) {
        imgV.image = [UIImage imageNamed:@"dps_Help2Pay"];
    }else if ([name containsString:@"Telegraphic"]){
        imgV.image = [UIImage imageNamed:@"dps_teleTransfer"];
    }
    [imgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.width.equalTo(26);
        make.height.equalTo(26);
        make.centerY.equalTo(0);
    }];
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = ROBOT_FONT(15);
    lb_title.dk_textColorPicker = DKCellTitleColor;
    [view addSubview:lb_title];
    [lb_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgV.right).offset(12);
        make.centerY.equalTo(-13);
    }];
    lb_title.text = name;
    
    UILabel *lb_detail = [UILabel new];
    lb_detail.font = ROBOT_FONT(13);
    lb_detail.dk_textColorPicker = DKCellContentColor;
    [view addSubview:lb_detail];
    [lb_detail makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgV.right).offset(12);
        make.centerY.equalTo(13);
    }];
    lb_detail.text = detail;
    weakself;
    [view block_whenTapped:^(UIView *aView) {
        [weakSelf gotoNext:name];
    }];
    
    return view;
}

- (void)gotoNext:(NSString *)name{
    if ([name containsString:@"Help2Pay"]) {
        if ([IXBORequestMgr mergedBankList].count >= 3) {
            [SVProgressHUD showErrorWithStatus:@"Your help2pay cards have reached maximum"];
            return;
        };
        
        [IXBORequestMgr shareInstance].bankAdditionInfo = @{@"origin":@"userInfo",
                                                            @"canDeposit":@"1",
                                                            @"canWithdraw":@"1",
                                                            @"effectType":@"withdraw",
                                                            @"gateway_name":@"help2pay",
                                                            @"gateway_code":@"help2pay",
                                                            };
        [IXBORequestMgr resetAddDepostiBankContainer];
        IXAddHelp2PayBankVC *vc = [IXAddHelp2PayBankVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([name containsString:@"Telegraphic"]){
        if ([IXBORequestMgr mergedWithDrawBankList].count >= 3) {
            [SVProgressHUD showErrorWithStatus:@"Your transfer money cards have reached maximum"];
            return;
        };
        
        [IXBORequestMgr shareInstance].bankAdditionInfo = @{@"origin":@"userInfo",
                                                            @"canDeposit":@"1",
                                                            @"canWithdraw":@"0",
                                                            @"effectType":@"withdraw",
                                                            @"gateway_name":@"",
                                                            @"gateway_code":@"",
                                                            };
        [IXBORequestMgr resetAddWithDrawBankContainer];
        IXWithdrawBankAddVC *vc = [IXWithdrawBankAddVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


@end
