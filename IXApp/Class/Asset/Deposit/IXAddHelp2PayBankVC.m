//
//  IXDepositBankAddVC.m
//  IXApp
//
//  Created by Larry on 2018/7/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXAddHelp2PayBankVC.h"
#import "IXComInputView.h"
#import "IXDocumentView.h"
#import "IXBORequestMgr.h"
#import "IXOpenChooseContentView.h"
#import "UIKit+Block.h"
#import "UIImageView+WebCache.h"
#import "NSDictionary+json.h"
#import "IXDrawBankListVC.h"
#import "IXUserInfoM.h"
#import "YYText.h"
#import "UIViewExt.h"
#import "IXGuideView.h"
@interface IXAddHelp2PayBankVC ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)UIView *headerView;

@property(nonatomic,strong)IXComInputView *input_name;
@property(nonatomic,strong)IXComInputView *input_bankNo;
@property(nonatomic,strong)IXComInputView *input_bank;
@property(nonatomic,strong)IXDocumentView * imgV1;

@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *bankNo;
@property(nonatomic,copy)NSString *bank;

@property(nonatomic,strong)NSDictionary *imgInfo1;
@property(nonatomic,strong)NSDictionary *uploadInfo1;

@property (nonatomic, strong) IXEnableBankM     * chooseBank;

@end

@implementation IXAddHelp2PayBankVC

- (UIView *)headerView{
    if (!_headerView) {
        _headerView = [UIView new];
        _headerView.dk_backgroundColorPicker = DKViewColor;
        [self.view addSubview:_headerView];
        [_headerView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(0);
            make.centerX.equalTo(0);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(110);
        }];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = ROBOT_FONT(13);
        lb_title.dk_textColorPicker = DKCellContentColor;
        lb_title.textAlignment = NSTextAlignmentCenter;
        lb_title.numberOfLines = 0;
        lb_title.userInteractionEnabled = YES;
        [_headerView addSubview:lb_title];
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(20);
            make.right.equalTo(-20);
            make.center.equalTo(0);
        }];
        [UIView bondSupperObject:_headerView subObject:lb_title byKey:@"lb_title"];
        
        lb_title.text = @"In order to ensure your asset sercurity, you can only bind your own bank account and submit account information as soon as possible.\n";
        weakself;
        [lb_title block_whenTapped:^(UIView *aView) {
            [weakSelf showMuskTipView];
        }];
        
    }
    return _headerView;
}




- (void)showMuskTipView{
    UIView *view = [UIView new];
    view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    [KEYWINDOW addSubview:view];
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(KEYWINDOW);
    }];
    [view block_whenTapped:^(UIView *aView) {
        [aView removeFromSuperview];
    }];
    
    UILabel *lb_Correct = [UILabel new];
    lb_Correct.font = ROBOT_FONT(13);
    lb_Correct.textColor = [UIColor whiteColor];
    lb_Correct.text = @"Correct";
    [view addSubview:lb_Correct];
    [lb_Correct makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.top.equalTo(80);
    }];
    
    UILabel *lb_content = [UILabel new];
    lb_content.font = ROBOT_FONT(11);
    lb_content.textColor = [UIColor whiteColor];
    lb_content.text = @"Your document should contain your full name, address and an issue date, which should be no older than 6 months. The name or logo of the issuer should also be clearly visible. Please make sure to scan your document against a different-colored background, so that all four corners are visible. ";
    lb_content.numberOfLines = 0;
    [view addSubview:lb_content];
    [lb_content makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lb_Correct);
        make.right.equalTo(-15);
        make.top.equalTo(lb_Correct.bottom).offset(15);
    }];
    
    UIImageView *imgV1 = [UIImageView new];
    imgV1.image = [UIImage imageNamed:@"bank_withdraw_tip2"];
    [view addSubview:imgV1];
    [imgV1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lb_content.bottom).offset(30);
        make.centerX.equalTo(0);
    }];
    
//    UILabel *lb_wrong = [UILabel new];
//    lb_wrong.font = ROBOT_FONT(13);
//    lb_wrong.textColor = [UIColor whiteColor];
//    lb_wrong.text = @"Wrong";
//    [view addSubview:lb_wrong];
//    [lb_wrong makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(lb_Correct);
//        make.top.equalTo(imgV1.bottom).offset(20);
//    }];
//
//    UIImageView *imgV2 = [UIImageView new];
//    imgV2.image = [UIImage imageNamed:@"user_withdraw_error1"];
//    [view addSubview:imgV2];
//    [imgV2 makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(lb_wrong.bottom).offset(10);
//        make.centerX.equalTo(0);
//    }];
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(18);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"I Know" forState:UIControlStateNormal];
    nextBtn.userInteractionEnabled = NO;
    [view addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.height.equalTo(44);
        make.top.equalTo(imgV1.bottom).offset(30);
    }];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Add Bank Account";
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    
    
    
    [self addBackItem];
    [self addDismissItemWithTitle:@"Submit" target:self action:@selector(submitBankInfo)];

    [self headerView];
    
    weakself;
    
    IXComInputView *input_name =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Name" rPlaceHolder:@"Enter your name" topLineStyle:TopLineStyleFull bottomLineStyle:BottomStyleNone];
    input_name.tf_right.delegate = self;
    
    [input_name makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.bottom);
    }];
    _input_name = input_name;
    
    IXComInputView *input_bankNo =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Savings Account" rPlaceHolder:@"Enter your savings account No." topLineStyle:TopLineStyleFull bottomLineStyle:BottomStyleNone];
    [input_bankNo.lb_left updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(95);
    }];
    input_bankNo.lb_left.font = ROBOT_FONT(12);
    input_bankNo.tf_right.font = ROBOT_FONT(12);
    input_bankNo.tf_right.delegate = self;
    
    [input_bankNo makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_name.bottom);
    }];
    _input_bankNo = input_bankNo;
    
    IXComInputView *input_bank =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Bank" rPlaceHolder:@"Select Bank" topLineStyle:TopLineStyleFull bottomLineStyle:BottomStyleFull];
    [input_bank addEnteranceArrow];
    [input_bank block_whenTapped:^(UIView *aView) {
        IXDrawBankListVC *vc = [[IXDrawBankListVC alloc] initWithSelectedInfo:^(IXEnableBankM *bank) {
            weakSelf.chooseBank = bank;
            weakSelf.input_bank.tf_right.text = bank.nameEN;
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    input_bank.tf_right.delegate = self;
    
    [input_bank makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_bankNo.bottom);
    }];
    _input_bank = input_bank;
    
    
    UILabel *lb_tip = [UILabel new];
    lb_tip.font = ROBOT_FONT(13);
    lb_tip.dk_textColorPicker = DKCellTitleColor;
    lb_tip.textAlignment = NSTextAlignmentCenter;
    lb_tip.numberOfLines = 0;
    lb_tip.text = @"Documents";
    [self.view addSubview:lb_tip];
    
    [lb_tip makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.top.equalTo(input_bank.bottom).offset(15);
    }];
    
    UILabel *lb_tip2 = [UILabel new];
    lb_tip2.font = ROBOT_FONT(12);
    lb_tip2.dk_textColorPicker = DKCellTitleColor;
    lb_tip2.textAlignment = NSTextAlignmentCenter;
    lb_tip2.numberOfLines = 0;
    [self.view addSubview:lb_tip2];
    [lb_tip2 makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-15);
        make.top.equalTo(lb_tip);
    }];
    
    NSString *msg = @"the photo sample and rules";
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:msg];
    NSString *content = msg;
    NSRange range = [msg rangeOfString:content];
    [AttributedStr addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:range];
    [AttributedStr addAttribute:NSUnderlineColorAttributeName value:lb_tip2.textColor range:range]; // 下划线颜色
    
    lb_tip2.attributedText = AttributedStr;
    
    [lb_tip2 block_whenTapped:^(UIView *aView) {
//        [IXGuideView makeInstance:GUIDE_HELP2PAY_ADDBANKCARD];
        [weakSelf showMuskTipView];
    }];
    
//    UILabel *lb_tip3 = [UILabel new];
//    lb_tip3.font = ROBOT_FONT(12);
//    lb_tip3.dk_textColorPicker = DKCellTitleColor;
//    lb_tip3.text = @"upload savins bank statement or bank passbook photo";
//    [self.view addSubview:lb_tip3];
//    [lb_tip3 makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(lb_tip);
//        make.top.equalTo(lb_tip.bottom).offset(15);
//    }];
    
    
    IXDocumentView * imgV1 = [IXDocumentView makeDocument:@"Bank Front" superView:self.view clearUploadInfo:^{
        weakSelf.uploadInfo1 = nil;
    }];
    imgV1.lb_tip.text = @"upload savings bank statement \nor bank passbook photo";
    [imgV1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lb_tip.bottom).offset(25);
    }];
    [imgV1 block_whenTapped:^(UIView *aView) {
        [weakSelf pickOne];
    }];
    _imgV1 = imgV1;
    
    [self initEditInfo];
    
    [self loadUI];
    [self reloadSubmitUI];
    
    
}

- (void)loadUI{
    _imgInfo1 = [IXBORequestMgr shareInstance].bankFile;
    
    if (_imgInfo1) {
        if ([_imgInfo1[@"ftpFilePath"] length]) {
            [_imgV1.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo1[@"ftpFilePath"]]];
            _imgV1.imageV.hidden = NO;
        }
        _imgV1.status = _imgInfo1[@"proposalStatus"];
    }
}

- (void)initEditInfo{
    NSDictionary *info = [IXBORequestMgr shareInstance].bank;
    if (info && info[@"bankAccountNumber"]) {
        _input_name.tf_right.text = info[@"bankAccountName"]; // 持卡人姓名
        _input_bankNo.tf_right.text = info[@"bankAccountNumber"]; // 账户号码
        _input_bank.tf_right.text = [self localizedName:info[@"bank"]];
    }
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    //不接受输入空格
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:NONESPACECHAR];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    //    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //    [self registerBtnEnable:[IXAppUtil isValidateEmail:aimStr]];
    //    _email = aimStr;
    
    return YES;
}

- (void)pickOne
{
    NSArray * arr =   @[
                        LocalizedString(@"选择相机"),
                        LocalizedString(@"选择相册"),
                        ];
    
    weakself;
    IXOpenChooseContentView * chooseV = [[IXOpenChooseContentView alloc] initWithDataSource:arr
                                                                            WithSelectedRow:^(NSInteger row)
                                         {
                                             if (row == 0){
                                                 [weakSelf takePhoto];
                                             }else{
                                                 [weakSelf joinAlbum];
                                             }
                                         }];
    [chooseV show];
}

- (void)takePhoto{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    // 调用照相机
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)joinAlbum{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    // 访问相册
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = NO;
        imagePicker.navigationBar.translucent = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

// 代理方法（当照完相或者选择完照片会调用这个代理）
#pragma mark UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    UIImage *image = nil;
    NSString *imageId;
    NSString *imageType;
    
    if ([mediaType isEqualToString:@"public.image"])
    {
        image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        NSURL *url = [info objectForKey: @"UIImagePickerControllerReferenceURL"];
        imageId = [url absoluteString];
        NSArray *tmpArray = [imageId componentsSeparatedByString: @"="];
        imageId = [tmpArray objectAtIndex:1];
        imageType = [tmpArray objectAtIndex:2];
        NSArray *tmpArray2 = [imageId componentsSeparatedByString: @"&"];
        imageId = [tmpArray2 objectAtIndex:0];
    }
    
    // 把file和fileName赋值
    if (imageId == nil) {
        double s = [NSDate timeIntervalSinceReferenceDate];
        imageId = [NSString stringWithFormat:@"%f", s];
        imageType = @"PNG";
    }
    //    NSString *fileName = [[NSString alloc] initWithFormat: @"%@.%@", imageId, imageType];
    //    NSData *img_data = UIImageJPEGRepresentation(image, 0.5);
    
    weakself;
    [picker dismissViewControllerAnimated:YES completion:^{
        weakSelf.imgV1.imageV.image = image;
        weakSelf.imgV1.imageV.hidden = NO;
        weakSelf.imgV1.lb_status.hidden = YES;
        [weakSelf submitFile:image];
    }];
}

- (void)submitFile:(UIImage *)image{
    [SVProgressHUD showWithStatus:@"Uploading.."];
    [IXBORequestMgr b_appUploadFile:image rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            _uploadInfo1 = obj;
            _imgV1.dismissV.hidden = NO;
            [self reloadSubmitUI];
            [SVProgressHUD dismiss];
        }else{
            [_imgV1.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo1[@"ftpFilePath"]]];
            _imgV1.imageV.hidden = NO;
            _imgV1.status = _imgInfo1[@"proposalStatus"];
            _imgV1.lb_status.hidden = NO;
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}

- (void)submitFileInfo{
    NSMutableArray *files = [NSMutableArray array];
    if (_uploadInfo1) {
        NSDictionary *dic1 = @{
                               @"gts2CustomerId":[IXBORequestMgr shareInstance].gts2CustomerId,
                               @"fileType":_imgInfo1[@"fileType"],
                               @"id":_imgInfo1[@"id"], // 列编号,登录接口用户信息中返回
                               
                               @"fileName":_uploadInfo1[@"fileName"],
                               @"filePath":_uploadInfo1[@"fileStorePath"], // 上传接口返回的：fileStorePath
                               @"ftpFilePath":_uploadInfo1[@"webFilePath"], // 上传接口返回的：webFilePath
                               };
        [files addObject:dic1];
    }
    if (!_uploadInfo1) {
        [self endSubmit];
        return;
    }
    
    [IXBORequestMgr b_updateCustomerFiles:files rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [self endSubmit];
        }else{
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}



- (void)submitBankInfo{
    if (![self reloadSubmitUI]) {
        [SVProgressHUD showInfoWithStatus:LocalizedString(@"银行卡资料不全")];
        return;
    }
    [SVProgressHUD show];
    NSDictionary *bank = [IXBORequestMgr shareInstance].bank;
    weakself;
    [IXBORequestMgr b_appUpdateCustomerBanks:@[bank] rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [weakSelf submitFileInfo];
        }else{
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}

- (BOOL)reloadSubmitUI{
    _name = _input_name.tf_right.text;
    _bank = _input_bank.tf_right.text;
    _bankNo = _input_bankNo.tf_right.text;
    
    if (_imgInfo1 && _name.length && _bank.length && _bankNo.length) {
        self.navigationController.navigationItem.rightBarButtonItem.customView.alpha = 1;
        
        NSMutableDictionary *bank = [[IXBORequestMgr shareInstance].bank mutableCopy];
        if (_chooseBank) {
            bank[@"bank"] = _chooseBank.code;
        }else{
            bank[@"bank"] = _bank;
        }
        
        bank[@"bankAccountName"] = _name;
        bank[@"bankName"] = _bank;
        bank[@"bankAccountNumber"] = _bankNo;
        [IXBORequestMgr shareInstance].bank = [bank mutableCopy];
        
        return YES;
    }else{
        return NO;
    }
}

- (void)endSubmit{
    [SVProgressHUD showSuccessWithStatus:@"Success"];
    [self cancelAction];
}

- (void)cancelAction{
    if (![self goBackTo:NSClassFromString(@"IXAcntBankListVC") animated:YES]) {
        if (![self goBackTo:NSClassFromString(@"IXIncashStep1VC") animated:YES]) {
            if (![self goBackTo:NSClassFromString(@"IXBankListVC") animated:YES]) {
                if (![self goBackTo:NSClassFromString(@"IXDrawBankVC") animated:YES]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [self reloadSubmitUI];
}

- (NSString *)localizedName:(NSString *)bankCode
{
    __block NSString *bankName = bankCode;
    [[IXBORequestMgr shareInstance].allBankList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXBank *bank = obj;
        if ([_bank isEqualToString:bank.code]) {
            bankName = bank.nameEN;
            *stop = YES;
        }
    }];
    return bankName;
    
}

@end
