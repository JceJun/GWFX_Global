//
//  IXDPSAmountVC.m
//  IXApp
//
//  Created by Larry on 2018/5/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDPSAmountVC.h"
#import "IXIncashHeaderV.h"
#import "IXTextField.h"

#import "IXBORequestMgr+Asset.h"
#import "IXDBAccountGroupMgr.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXAccountGroupModel.h"
#import "IXIncomeModel.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "IXDPSChannelVC.h"
#import "IXIncashResultVC.h"
#import "IXLocalizationModel.h"
#import "IXBORequestMgr+Asset.h"
#import "IXUserInfoMgr.h"
#import <UMMobClick/MobClick.h>
#import "IXCommonWebVC.h"
#import "UIViewExt.h"
#import "UIKit+Block.h"
#import "IXBORequestMgr+Comp.h"

@interface IXDPSAmountVC ()
<
UITextFieldDelegate
>

@property (nonatomic, strong) IXBaseUrlM    * baseUrl;
@property (nonatomic, strong) NSString      * incashToken;
@property (nonatomic, assign) CGFloat       * maxAmount;    //入金限额
@property (nonatomic, strong) NSString      * currency;

@property(nonatomic,copy) NSDictionary * accGroupDic;
@property(nonatomic,copy) NSString *depositCurrency;
@property(nonatomic,copy) NSString *amount;
@property(nonatomic,copy) NSString *minPayamount;
@property(nonatomic,copy) NSString *maxPayamount;
@property(nonatomic,copy) NSString *pNo;
@property(nonatomic,copy) NSString *payMethod;

@property(nonatomic,strong)UIView *headerV;
@property(nonatomic,strong)UIView *itemView;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIView *amountV;
@property(nonatomic,strong)NSDictionary *selectedDic;
@property(nonatomic,strong)NSArray *dataArr;

@property(nonatomic,strong)UITextField *amountTf;

@property(nonatomic,strong)UILabel *lb_tip;


@end

@implementation IXDPSAmountVC

- (UIView *)headerV{
    if (!_headerV) {
        _headerV = [UIView new];
        _headerV.dk_backgroundColorPicker = DKTableHeaderColor;
        [self.scrollView addSubview:_headerV];
        
        [_headerV makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(0);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(80);
        }];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = PF_MEDI(15);
        lb_title.dk_textColorPicker = DKCellTitleColor;
        lb_title.textAlignment = NSTextAlignmentCenter;
        lb_title.numberOfLines = 0;
        NSDictionary * dic = [IXAccountGroupModel shareInstance].accGroupDic;
        _currency = @"";
        if (dic[@"currency"]) {
            _currency = dic[@"currency"];
        }
        
        NSString * title = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"入金金额"),_currency];
        lb_title.text = title;
        [_headerV addSubview:lb_title];
        
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(0);
        }];
    }
    return _headerV;
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
//        _scrollView.delegate = self;
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
            make.left.equalTo(0);
            make.width.equalTo(kScreenWidth);
        }];
    }
    return _scrollView;
}

- (UIView *)amountV{
    if (!_amountV) {
        _amountV = [UIView new];
        _amountV.dk_backgroundColorPicker = DKNavBarColor;
        _amountV.layer.borderColor = HexRGB(0xe2eaf2).CGColor;
        _amountV.layer.borderWidth = 0.5;
        _amountV.layer.masksToBounds = YES;
        [self.scrollView addSubview:_amountV];
        [_amountV makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(-1);
            make.width.equalTo(kScreenWidth+2);
            make.height.equalTo(50);
        }];
        
        UILabel *lb_left = [UILabel new];
        lb_left.font = ROBOT_FONT(15);
        lb_left.dk_textColorPicker = DKCellTitleColor;
        lb_left.numberOfLines = 0;
        lb_left.text = @"Amount";
        [_amountV addSubview:lb_left];
        [lb_left makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.width.equalTo(70);
            make.centerY.equalTo(0);
        }];
        
        UITextField *tf_right = [UITextField new];
        tf_right.font = lb_left.font;
        tf_right.textColor = lb_left.textColor;
        tf_right.textAlignment = NSTextAlignmentLeft;
        tf_right.autocapitalizationType  = UITextAutocapitalizationTypeNone;
        tf_right.autocorrectionType = UITextAutocorrectionTypeNo;
        tf_right.keyboardType = UIKeyboardTypeNumberPad;
        tf_right.clearButtonMode = UITextFieldViewModeAlways;
        tf_right.placeholder = @"Please Enter Amount";
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            UIButton *clean = [tf_right valueForKey:@"_clearButton"]; //key是固定的
            [clean setImage:[UIImage imageNamed:@"close_circle"] forState:UIControlStateNormal];
        }
        [_amountV addSubview:tf_right];
        [tf_right makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lb_left.right).offset(10);
            make.right.equalTo(-25);
            make.top.equalTo(0);
            make.bottom.equalTo(0);
        }];
        _amountTf = tf_right;
        
        
    }
    return _amountV;
}

- (UIView *)itemView{
    if (!_itemView) {
        _itemView = [UIView new];
        _itemView.dk_backgroundColorPicker = DKNavBarColor;
        [self.scrollView addSubview:_itemView];
        
        [_itemView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.width.equalTo(kScreenWidth);
            //            make.height.mas_greaterThanOrEqualTo(152);
        }];
    }
    return _itemView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (UMAppKey.length) {
        [MobClick event:UM_deposit2];
    }

    self.fd_interactivePopDisabled = YES;   //禁用滑动返回手势
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    self.title = LocalizedString(@"存入资金");
    [self addBackItem];
    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
    
    [self createSubview];
    
    _accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
    _depositCurrency = @"";
    if (_accGroupDic[@"currency"]) {
        _depositCurrency = _accGroupDic[@"currency"];
    }
    _payMethod = [IXBORequestMgr shareInstance].channelDic[@"code"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(validTextContent)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    [self loadIncomModel];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)validTextContent
{
    NSString *content = _amountTf.text;
    NSRange range = [content rangeOfString:@"."];
    if ( range.location != NSNotFound ) {
        if ( content.length - range.location  > 3 ) {
            _amountTf.text = [_amountTf.text substringToIndex:(range.location + 3)];
        }
    }
}

- (void)cancelAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)createSubview
{
    _minPayamount = [IXBORequestMgr shareInstance].channelDic[@"minPayamount"];
    _maxPayamount = [IXBORequestMgr shareInstance].channelDic[@"maxPayamount"];
    NSString    * limitStr = [NSString stringWithFormat:@"%@",[IXAppUtil amountToString:_maxPayamount]];
    if ([_minPayamount integerValue] > 0) {
        limitStr = [NSString stringWithFormat:@"%@-%@",_minPayamount,[IXAppUtil amountToString:_maxPayamount]];
    }
    
    [self headerV];
    
    
    [self.amountV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerV.bottom);
    }];
    
    UILabel *lb_limit = [UILabel new];
    lb_limit.font = ROBOT_FONT(12);
    lb_limit.dk_textColorPicker = DKCellTitleColor;
    [self.scrollView addSubview:lb_limit];
    lb_limit.text= [NSString stringWithFormat:@"Limit %@",limitStr];
    [lb_limit makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_amountV.bottom).offset(10);
        make.left.equalTo(15);
    }];
    
    [self.itemView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lb_limit.bottom).offset(10);
    }];
    

    _dataArr = [IXBORequestMgr shareInstance].depositAmount;
    
    /*
    _dataArr = @[
                 @{
                     @"amount": @"50",
                     @"isDefault": @false
                     },
                 @{
                     @"amount": @"100",
                     @"isDefault": @false
                     },
                 @{
                     @"amount": @"200",
                     @"isDefault": @true
                     },
                 @{
                     @"amount": @"500",
                     @"isDefault": @true
                     },
                 @{
                     @"amount": @"1000",
                     @"given": @"Get $150 Free",
                     @"isDefault": @false
                     }
                 ];
    */
    
    NSInteger count = -1;
    for (int i = 0; i < _dataArr.count/3+1; i ++ ) {
        
        for (int j = 0; j < 3; j++) {
            count ++;
            if (count == _dataArr.count ) {
                break;
            }
            
            UIButton *itemV = [UIButton new];
            itemV.layer.borderColor = HexRGB(0xe2faf2).CGColor;
            itemV.layer.borderWidth = 1;
            itemV.layer.cornerRadius = 3;
            itemV.layer.masksToBounds = YES;
            itemV.tag = count+100;
            //            itemV.dk_backgroundColorPicker = DKNavBarColor;
//            itemV.backgroundColor = RGB_COLOR;
            [self.itemView addSubview:itemV];
            
            float w =  (kScreenWidth - 15*2 - 10*2)/3;
            [itemV makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(15 + j * (w + 10));
                make.width.equalTo(w);
                make.height.equalTo(50);
                make.top.equalTo(20 + i * (50 + 10));
                if (count == _dataArr.count - 1) {
                    make.bottom.equalTo(-20);
                }
            }];
            weakself;
            [itemV block_touchUpInside:^(UIButton *aButton) {
                [weakSelf resetItemView:aButton];
            }];
        }
    }
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(18);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"Paying" forState:UIControlStateNormal];
    [self.scrollView addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(kScreenWidth-30);
        make.top.equalTo(self.itemView.bottom).offset(40);
        make.height.equalTo(44);
        make.bottom.equalTo(-40);
    }];
    
    weakself;
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        [weakSelf payAction];
    }];
    
    [_dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dic = obj;
        UIButton *btn = [weakSelf.itemView viewWithTag:idx+100];
        BOOL selected = [dic[@"isDefault"] boolValue];
        NSString *amount = dic[@"amount"];
        NSString *given = dic[@"given"];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = ROBOT_FONT(15);
        lb_title.textColor = HexRGB(0x4c6072);
        lb_title.numberOfLines = 0;
        lb_title.backgroundColor = [UIColor clearColor];
        lb_title.text = amount;
        [btn addSubview:lb_title];
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            if (given.length) {
                make.centerX.equalTo(0);
                make.top.equalTo(5);
            }else{
                make.center.equalTo(0);
            }
        }];
        
        [UIView bondSupperObject:btn subObject:lb_title byKey:@"lb_title"];
        
        if (given.length) {
            UILabel *lb_down = [UILabel new];
            lb_down.font = ROBOT_FONT(12);
            lb_down.backgroundColor = [UIColor clearColor];
            lb_down.textColor = HexRGB(0x4c6072);
            lb_down.textAlignment = NSTextAlignmentCenter;
            lb_down.numberOfLines = 0;
            lb_down.text = given;
            [btn addSubview:lb_down];
            [lb_down makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(-5);
                make.centerX.equalTo(0);
            }];
            
            [UIView bondSupperObject:btn subObject:lb_down byKey:@"lb_down"];
        }
        
        if (selected) {
            [self resetItemView:btn];
        }
    }];
    
    UILabel *lb_tip = [UILabel new];
    lb_tip.font = ROBOT_FONT(13);
    lb_tip.dk_textColorPicker = DKCellContentColor;
    lb_tip.text = [IXBORequestMgr shareInstance].mobileOnlineConfig[@"depositPrompt"];  //@"You can only participate once during the activity";
    [self.scrollView addSubview:lb_tip];
    [lb_tip makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.top.equalTo(nextBtn.bottom).offset(20);
    }];
    

    UITapGestureRecognizer  * tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard)];
    [self.view addGestureRecognizer:tap1];
}

- (void)resetItemView:(UIButton *)btn{
    self.selectedDic = _dataArr[btn.tag-100];
    
    NSString *event = [NSString stringWithFormat:@"支付-输入金额-位置%ld",btn.tag-100+1];
    MarkEvent(event);
    
    [btn.superview.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *item = obj;
        item.backgroundColor = [UIColor whiteColor];
        UILabel *lb_title = [UIView getSubObjFromSupperObj:item bySubObjectKey:@"lb_title"];
        UILabel *lb_down = [UIView getSubObjFromSupperObj:item bySubObjectKey:@"lb_down"];
        lb_title.textColor = HexRGB(0x4c6072);
        lb_down.textColor = HexRGB(0x4c6072);
    }];
    
    UILabel *lb_title = [UIView getSubObjFromSupperObj:btn bySubObjectKey:@"lb_title"];
    UILabel *lb_down = [UIView getSubObjFromSupperObj:btn bySubObjectKey:@"lb_down"];
    lb_title.backgroundColor =  [UIColor clearColor];
    lb_down.backgroundColor = [UIColor clearColor];
    lb_title.textColor = [UIColor whiteColor];
    lb_down.textColor = [UIColor whiteColor];
    btn.backgroundColor = HexRGB(0x11b873);
    
}

-(void)setSelectedDic:(NSDictionary *)selectedDic{
    _selectedDic = selectedDic;
    _amountTf.text = _selectedDic[@"amount"];
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (string.length && [@"0123456789." rangeOfString:string].location == NSNotFound) {
        return NO;
    }
    
    if ([textField.text isEqualToString:@"0"] && [string isEqualToString:@"0"]){
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([string isEqualToString:@"."]) {
        if (![textField.text containsString:@"."]) {
            textField.text = aimStr;
        }
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    double amount = [IXAppUtil stringToAmount:textField.text];
    
    //添加逗号
    if (amount > 0) {
        textField.text = [IXAppUtil amountToString:@(amount)];
    }
}

- (void)resignKeyboard
{
    [_amountTf resignFirstResponder];
}


- (void)payAction
{
    if (!_amountTf.text.length) {
        [SVProgressHUD showMessage:LocalizedString(@"请输入金额")];
        return;
    }
    
    _amount = [_amountTf.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    if ([_amount floatValue] < [_minPayamount floatValue]) {
        [SVProgressHUD showMessage:LocalizedString(@"金额小于最小入金金额")];
        return;
    }
    if ([_amount floatValue] > [_maxPayamount floatValue]) {
        [SVProgressHUD showMessage:LocalizedString(@"金额大于最大入金金额")];
        return;
    }

    [self request_1_getConfigData];
}

#pragma mark -
#pragma mark - request

- (void)request_1_getConfigData
{
    [SVProgressHUD show];
    dispatch_group_t    group = dispatch_group_create();
    
    dispatch_group_enter(group);
    //获取baseUrl
    [IXBORequestMgr incash_requstInCashBaseUrl:^(IXBaseUrlM *urlM) {
        _baseUrl = urlM;
        dispatch_group_leave(group);
    }];
    
    dispatch_group_enter(group);
    [IXBORequestMgr incash_requestToken:^(BOOL success, NSString *token) {
        if (success) {
            _incashToken = token;
        }else{
            [SVProgressHUD showInfoWithStatus:token];
        }
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (_baseUrl && _incashToken) {
            [self request_2_validateUrl];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取配置数据失败")];
        }
    });
}

- (void)request_2_validateUrl
{
    NSString *currency = [IXBORequestMgr shareInstance].channelDic[@"payCurrency"];
    NSDictionary *paramDic = @{
                               @"depositCurrency":currency,
                               @"bank":_incashModel.bank,
                               @"bankAccountNumber":_incashModel.bankAccountNumber,
                               @"payMethod":_payMethod,
                               @"amount":_amount,
                               @"depositValidateUrl":_baseUrl.depositValidateUrl,
                               };
    
    [IXBORequestMgr incash_verifyPaymethodWith:paramDic complete:^(BOOL success, NSString *token) {
        if (success) {
            _pNo = token;
            IXIncashResultVC * vc = [[IXIncashResultVC alloc] init];
            vc.baseUrl = _baseUrl;
            [self loadIncomModel];
            vc.incomeModel = _incashModel;
            [self.navigationController pushViewController:vc animated:YES];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self request_3_submitUrl];
            });
        }else{
            [SVProgressHUD showInfoWithStatus:token];
        }
    }];
}

- (void)request_3_submitUrl
{
    [SVProgressHUD dismiss];
    NSString    * urlStr = _baseUrl.depositSubmitUrl;
    urlStr = [urlStr stringByAppendingString:@"/rgsLogin.rgs?"];
    urlStr = [urlStr stringByAppendingFormat:@"platform=IX&pageType=normal&companyId=%ld&token=%@&service=",
              (long)CompanyID,_incashToken];
    
    NSString    * serverStr = _baseUrl.depositSubmitUrl;
    serverStr = [serverStr stringByAppendingString:@"/appFundDepositConfirm.do?"];
    
    NSString *currency = _depositCurrency;
    serverStr = [serverStr stringByAppendingFormat:@"payType=mobileEgpay&platform=IX&fee=0&accountNo=%@&depositCurrency=%@&bank=%@&bankAccountNumber=%@&payMethod=%@&amount=%@&pno=%@&lang=%@",
                 [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                 currency,
                 _incashModel.bank,
                 _incashModel.bankAccountNumber,
                 _payMethod,
                 _amount,
                 _pNo,
                 [IXLocalizationModel currentCheckLanguage]];
    if ([[[IXBORequestMgr shareInstance].channelDic[@"gatewayCode"] lowercaseString] containsString:@"asiapay"]){
        NSString *remark = [IXBORequestMgr shareInstance].paramDic[@"remark"];
        if (remark.length) {
            _incashModel.remark = remark;
        }else{
            _incashModel.remark = @"";
        }
        serverStr = [serverStr stringByAppendingFormat:@"&remark=%@",_incashModel.remark];
        // &remark=iosPay
    }else if ([[IXBORequestMgr shareInstance].channelDic[@"cardpayType"] isEqualToString:@"credit"]) {
        serverStr = [serverStr stringByAppendingFormat:@"&email=%@&mobileNumberPrefix=%@&mobileNumber=%@&firstName=%@&lastName=%@",_incashModel.email,_incashModel.mobileNumberPrefix,_incashModel.mobileNumber,_incashModel.firstName,_incashModel.lastName];
    }
    
    serverStr = [NSString formatterUTF8:serverStr];
    urlStr = [urlStr stringByAppendingString:serverStr];
    
    [self gotoWeb:urlStr];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}

- (void)loadIncomModel{
    if (!_incashModel) {
        _incashModel = [IXIncomeModel new];
    }
    if ([[IXBORequestMgr shareInstance].paramDic[@"bank"] length]) {
        _incashModel.bank = [IXBORequestMgr shareInstance].paramDic[@"bank"];
        _incashModel.bankAccountNumber = [IXBORequestMgr shareInstance].paramDic[@"bankAccountNumber"];
    }else{
        _incashModel.bank = @"";
        _incashModel.bankAccountNumber = @"";
    }
    _incashModel.depositCurrency = _depositCurrency;
    _incashModel.amount = _amount;
    _incashModel.minPayamount = _minPayamount;
    _incashModel.maxPayamount = _maxPayamount;
    _incashModel.pNo = _pNo;
    _incashModel.payMethod = _payMethod;
    _incashModel.payName = [[IXBORequestMgr shareInstance].channelDic[@"gatewayCode"] stringByReplacingOccurrencesOfString:@"_" withString:@""];
    
}

- (void)gotoWeb:(NSString  *)url{
    IXCommonWebVC * vc = [[IXCommonWebVC alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    vc.url = url;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
