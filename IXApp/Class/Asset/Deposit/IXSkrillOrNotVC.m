//
//  IXChannelOrNotVC.m
//  IXApp
//
//  Created by Larry on 2018/5/10.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXSkrillOrNotVC.h"
#import "UIViewExt.h"
#import "IXSkrillOrNotTV.h"
#import "YYText.h"
#import "UIKit+Block.h"
#import "IXDPSAmountVC.h"
#import "IXWebCustomerSeviceVC.h"
#import "IXDPSGuideWebVC.h"
#import "IXBORequestMgr+Asset.h"
#import <UMMobClick/MobClick.h>
#import "IXCpyConfig.h"
#import "IXSkirillToSkrillVC.h"

@interface IXSkrillOrNotVC ()
@property(nonatomic,strong)IXSkrillOrNotTV *table;
@property(nonatomic,strong)NSString *cardName;
@end

@implementation IXSkrillOrNotVC

- (IXSkrillOrNotTV *)table{
    if (!_table) {
        _table = [[IXSkrillOrNotTV alloc] initWithFrame:CGRectZero tableStyle:UITableViewStylePlain estimatedHeaderH:NO estimatedFooterH:NO];
        _table.bounces = NO;
        [self.view addSubview:_table];
        
        
        [_table makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
        weakself;
        _table.viewAction = ^(NSString *key, id obj) {
            IXLCellItem *item = obj;
            [weakSelf gotoNext:item.dataDic[@"title"]];
        };
        
        
    }
    return _table;
}

- (UIView *)headerV{
    UIView *headerV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 132)];
    headerV.dk_backgroundColorPicker = DKNavBarColor;
//    headerV.dk_backgroundColorPicker  = CellBackgroundColor;
    
    UILabel *lb_header = [UILabel new];
    lb_header.dk_textColorPicker = DKCellTitleColor;
    lb_header.font = PINGFANG_MEDI_FONT(15);
    lb_header.textColor = HexRGB(0x4c6072);
    lb_header.textAlignment = NSTextAlignmentCenter;
    lb_header.numberOfLines = 0;
    [headerV addSubview:lb_header];
    [lb_header makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(headerV);
        make.top.equalTo(40);
    }];
    
    [UIView bondSupperObject:headerV subObject:lb_header byKey:@"lb_header"];
    
    UILabel *lb_header1 = [UILabel new];
    lb_header1.dk_textColorPicker = DKCellTitleColor;
    lb_header1.font = PINGFANG_MEDI_FONT(12);
    lb_header1.textColor = HexRGB(0x99abba);
    lb_header1.textAlignment = NSTextAlignmentCenter;
    lb_header1.numberOfLines = 0;
    [headerV addSubview:lb_header1];
    [lb_header1 makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(headerV);
        make.top.equalTo(lb_header.bottom).offset(12);
    }];
    
    [UIView bondSupperObject:headerV subObject:lb_header1 byKey:@"lb_header1"];
    
    
    return headerV;
}

- (UIView *)footerV{
    UIView *footerV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 120)];
//    footerV.backgroundColor = [UIColor whiteColor];
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:[NSString stringWithFormat:@"Skip to %@",_cardName] forState:UIControlStateNormal];
    [footerV addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.height.equalTo(44);
    }];
    
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        [self gotoNext:nextBtn.titleLabel.text];
    }];
    
    UILabel *lb_bottom = [UILabel new];
    lb_bottom.font = PINGFANG_MEDI_FONT(12);
    lb_bottom.textColor = HexRGB(0x3f87f5);
    lb_bottom.textAlignment = NSTextAlignmentCenter;
    lb_bottom.numberOfLines = 0;
    [footerV addSubview:lb_bottom];
    lb_bottom.text = [NSString stringWithFormat:@"The Guideline for %@ Account Registration",_cardName];
    
    [lb_bottom makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.top.equalTo(15);
    }];
    
    [lb_bottom block_whenTapped:^(UIView *aView) {
        [self gotoNext:lb_bottom.text];
    }];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lb_bottom.bottom).offset(40);
    }];
    
    return footerV;
}

- (void)lb_CSSupport{
    UILabel *lb_CSSupport = [UILabel new];
    lb_CSSupport.font = PINGFANG_MEDI_FONT(15);
    lb_CSSupport.textColor = HexRGB(0x4c6072);
    lb_CSSupport.textAlignment = NSTextAlignmentCenter;
    lb_CSSupport.numberOfLines = 0;
    [self.view addSubview:lb_CSSupport];
    
    lb_CSSupport.text = @"CS Support";
    
    [lb_CSSupport makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-15);
        make.centerX.equalTo(self.view);
    }];
    
    weakself;
    [lb_CSSupport block_whenTapped:^(UIView *aView) {
        [weakSelf gotoNext:@"CS Support"];
    }];
}

- (void)cancelAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    
    if (UMAppKey.length) {
        [MobClick event:UM_depositss_f];
    }
 
    self.title = LocalizedString(@"存入资金");
    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
    
    
    NSString *gatewayCode = [IXBORequestMgr shareInstance].channelDic[@"gatewayCode"];
    _cardName = [gatewayCode componentsSeparatedByString:@"__"][0];
    _cardName = [gatewayCode componentsSeparatedByString:@"_"][0];
    
    [self addBackItem];
    
    [self.table reloadTableView];
    
    [self resetHeader:_cardName];
    
    [self showProgressWithAimStep:2 totalStep:3 originY:0];
    
    [self lb_CSSupport];
}

- (void)resetHeader:(NSString *)title{
    UILabel *lb_header = [UIView getSubObjFromSupperObj:self.table.tableHeaderView bySubObjectKey:@"lb_header"];
    lb_header.text = [NSString stringWithFormat:@"The %@ Payment Gateway is opening",_cardName];
    
    UILabel *lb_header1 = [UIView getSubObjFromSupperObj:self.table.tableHeaderView bySubObjectKey:@"lb_header1"];
    lb_header1.text = [NSString stringWithFormat:@"%@ is E-wallet, you need to have \n a %@ account before using %@ for payment",_cardName,_cardName,_cardName];
}

- (void)gotoNext:(NSString *)title{
    if ([title containsString:@"1. Already have"]) {

    }else if ([title containsString:@"2. Do not have"]) {

    }else if([title containsString:@"CS Support"]){
        IXWebCustomerSeviceVC *vc = [IXWebCustomerSeviceVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if([[title lowercaseString] containsString:@"Guideline"]){
        IXDPSGuideWebVC *vc = [IXDPSGuideWebVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title containsString:@"skrill to skrill"]) {
        IXSkirillToSkrillVC *vc = [IXSkirillToSkrillVC new];
        [self.navigationController pushViewController:vc animated:YES];
        
        MarkEvent(@"支付通道-选择 Skrill To Skrill");
    }else if ([[title lowercaseString] containsString:@"wallet"]){
        MarkEvent(@"支付通道-选择 skrill wallet");
    }else if ([[title lowercaseString] containsString:@"visa"]){
        MarkEvent(@"支付通道-选择 visa by skrill");
    }else{
        // skip
        if (UMAppKey.length) {
            [MobClick event:UM_depositss_fNS];
        }
        
        IXDPSAmountVC *vc = [IXDPSAmountVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


@end
