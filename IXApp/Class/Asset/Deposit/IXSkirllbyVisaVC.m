//
//  IXSkirllbyVisaVC.m
//  IXApp
//
//  Created by Larry on 2018/10/11.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXSkirllbyVisaVC.h"
#import "IXDPSAmountVC.h"
#import "AppDelegate+UI.h"
#import "IXAccountGroupModel.h"
#import "UIViewExt.h"
#import "UIKit+block.h"
#import "IXDPSGuideWebVC.h"

@interface IXSkirllbyVisaVC ()
@property(nonatomic,strong)UIView *headerV;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *lb_attention;
@property(nonatomic,strong)UIView *bottomV;
@end

@implementation IXSkirllbyVisaVC

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        //        _scrollView.delegate = self;
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
            make.left.equalTo(0);
            make.width.equalTo(kScreenWidth);
        }];
    }
    return _scrollView;
}

- (UIView *)headerV{
    if (!_headerV) {
        _headerV = [UIView new];
        _headerV.dk_backgroundColorPicker = DKTableHeaderColor;
        [self.scrollView addSubview:_headerV];
        
        [_headerV makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(0);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(80);
        }];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = PF_MEDI(15);
        lb_title.dk_textColorPicker = DKCellTitleColor;
        lb_title.textAlignment = NSTextAlignmentCenter;
        lb_title.numberOfLines = 0;
        lb_title.text = @"You need to register a Skrill Account";
        [_headerV addSubview:lb_title];
        
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(0);
        }];
    }
    return _headerV;
}

- (UIView *)makeCellViewWithData:(NSDictionary *)dic{
    UIView *view = [UIView new];
    view.dk_backgroundColorPicker = DKNavBarColor;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.masksToBounds = YES;
    [self.scrollView addSubview:view];
    
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.width.equalTo(kScreenWidth);
//        make.height.equalTo(100);
    }];
    
    UILabel *lb_tip1 = [UILabel new];
    lb_tip1.font = ROBOT_FONT(15);
    lb_tip1.dk_textColorPicker = DKCellTitleColor;
    lb_tip1.numberOfLines = 0;
    [view addSubview:lb_tip1];
    [lb_tip1 makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(18);
        make.width.equalTo(kScreenWidth - 18 * 2);
        make.top.equalTo(18);
    }];
    NSString *str1 = dic[@"1"];
    NSString *str2 = dic[@"2"];
    NSString *str = [NSString stringWithFormat:@"%@%@",str1,str2];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:str];
    [string addAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}
                    range:[str rangeOfString:str1]];
    lb_tip1.attributedText = string;
    
    
    UILabel *lb_tip2 = [UILabel new];
    lb_tip2.font = ROBOT_FONT(12);
    lb_tip2.textColor = RGB(63, 135, 245);
    [view addSubview:lb_tip2];
    [lb_tip2 makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(18);
        make.width.equalTo(kScreenWidth - 18 * 2);
        make.top.equalTo(lb_tip1.bottom).offset(20);
    }];
    lb_tip2.text = dic[@"3"];
    
    str = dic[@"3"];
    string = [[NSMutableAttributedString alloc] initWithString:str];
    [string addAttributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                            NSForegroundColorAttributeName : RGB(63, 135, 245)
                            } range:[str rangeOfString:str]];
    lb_tip2.attributedText = string;
    weakself;
    [lb_tip2 block_whenTapped:^(UIView *aView) {
        [weakSelf gotoNext:str];
    }];

    [UIView bondSupperObject:view subObject:lb_tip2 byKey:@"lb_tip2"];
    
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(lb_tip2.bottom).offset(18);
    }];
    
    return view;
}

- (UILabel *)lb_attention{
    if (!_lb_attention) {
        _lb_attention = [UILabel new];
        _lb_attention.font = ROBOT_FONT(12);
        _lb_attention.textColor = RGB(255, 132, 0);
        _lb_attention.numberOfLines = 0;
        _lb_attention.text = @"Attention: Skrill is a third party payment platform, traders might need to pay relevant commission fees";
        [self.view addSubview:_lb_attention];
        [_lb_attention makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(18);
            make.width.equalTo(kScreenWidth - 18 * 2);
        }];
    }
    return _lb_attention;
}

- (UIView *)bottomV{
    if (!_bottomV) {
        _bottomV = [UIView new];
        _bottomV.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bottomV];
        
        [_bottomV makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(50);
            make.left.equalTo(0);
            make.width.equalTo(kScreenWidth);
            make.bottom.equalTo(0);
        }];
        
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        
        UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
        dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
        
        UIButton * nextBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn1.titleLabel.font = PF_REGU(18);
        [nextBtn1 dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
        [nextBtn1 setTitle:@"Regist one" forState:UIControlStateNormal];
        [_bottomV addSubview:nextBtn1];
        
        [nextBtn1 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(-5);
            make.width.equalTo(kScreenWidth/2-2+5);
            make.top.equalTo(0);
            make.height.equalTo(_bottomV);
        }];
        
        [nextBtn1 block_touchUpInside:^(UIButton *aButton) {
            [self gotoNext:@"web"];
        }];
        
        UIButton * nextBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn2.titleLabel.font = PF_REGU(18);
        [nextBtn2 dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
        [nextBtn2 setTitle:@"I already have one" forState:UIControlStateNormal];
        [_bottomV addSubview:nextBtn2];
        
        [nextBtn2 makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nextBtn1.right).offset(4+2);
            make.width.equalTo(kScreenWidth/2-2+2);
            make.top.equalTo(0);
            make.height.equalTo(_bottomV);
        }];
        
        [nextBtn2 block_touchUpInside:^(UIButton *aButton) {
            [self gotoNext:@"else"];
        }];
        
    }
    return _bottomV;
}

- (void)reloadUI{
    UILabel *lb_tip2 = [UIView getSubObjFromSupperObj:self bySubObjectKey:@"lb_tip2"];
    NSString *str = @"www.baidu.com";
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:str];
    [string addAttributes:@{NSForegroundColorAttributeName:DKCellContentColor}
                    range:[str rangeOfString:str]];
    lb_tip2.attributedText = string;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Guidance";
    [self addBackItem];
    [self addDismissItemWithTitle:LocalizedString(@"取消")];
    [self showProgressWithAimStep:2 totalStep:3 originY:0];
    
    [self bottomV];
    [self.scrollView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-50);
    }];
    [self headerV];
    
    NSDictionary *data = @{};
    
    data = @{@"1":@"Step1: ",
             @"2":@"Register a Skrill account",
             @"3":@"See Registration procedure",
             };
    UIView *cellView1 = [self makeCellViewWithData:data];
    [cellView1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerV.bottom);
    }];


    data = @{@"1":@"Step2: ",
             @"2":@"Log in to GWFX Global again to finish payment",
             @"3":@"See Payment procedure",
             };
    UIView *cellView2 = [self makeCellViewWithData:data];
    [cellView2 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cellView1.bottom).offset(20);
    }];
    
    [self.lb_attention makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cellView2.bottom).offset(20);
    }];
}

- (void)gotoNext:(NSString *)title{
    NSString *url;
    if ([title isEqualToString:@"web"]) {
        url = @"https://account.skrill.com/signup/v3/page1?locale=en";
        [AppDelegate gotoCommonWeb:url];
    }else if ([title containsString:@"Registration"]){
        url = @"http://www.gwfxglobal.com/app/en/skrillregister.html";
        [AppDelegate gotoCommonWeb:url];
    }else if ([title containsString:@"Payment"]){
        url = @"http://www.gwfxglobal.com/app/en/skrillpayment.html";
        [AppDelegate gotoCommonWeb:url];
    }else{
        IXDPSAmountVC *vc = [IXDPSAmountVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


@end
