//
//  IXDPSChannelVC.m
//  IXApp
//
//  Created by Larry on 2018/5/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDPSChannelVC.h"
#import "IXDPSChannelTV.h"
#import "IXIncashHeaderV.h"
#import "IXIncashFooterV.h"
#import "IXAccountGroupModel.h"
#import "YYText.h"
#import "UIViewExt.h"
#import "IXBORequestMgr+Asset.h"
#import "UIKit+Block.h"
#import "IXSkrillOrNotVC.h"
#import "IXWebCustomerSeviceVC.h"
#import "IXCpyConfig.h"
#import "IXSupplementAccountVC.h"
#import "IXIncashStep1VC.h"
#import <UMMobClick/MobClick.h>
#import "ChatTableViewController.h"
#import "NSDictionary+json.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Comp.h"
#import "UIViewExt.h"
#import "IXCreditCardInfoVC.h"
#import "IXChannelOrNotVC.h"
#import "IXCreditCardContactVC.h"
#import "IXDPSAmountVC.h"
#import "IXSkirillToSkrillVC.h"
#import "IXSkirllbyVisaVC.h"

@interface IXDPSChannelVC ()
@property(nonatomic,strong)IXDPSChannelTV *table;
@property(nonatomic,copy)NSString *currency;
@property(nonatomic,strong)UIView *bottomV;
@property(nonatomic,copy)UILabel *lb_freefee;
@property(nonatomic,strong)UIView *epayIntroView;
@property(nonatomic,strong)UIImageView *bannerView;

@end

@implementation IXDPSChannelVC

- (void)dealloc{
    [IXBORequestMgr shareInstance].channelList = nil;
    [IXBORequestMgr shareInstance].channelDic = nil;
}

- (IXDPSChannelTV *)table{
    if (!_table) {
        _table = [[IXDPSChannelTV alloc] initWithFrame:CGRectZero tableStyle:UITableViewStyleGrouped estimatedHeaderH:NO estimatedFooterH:NO];
        _table.backgroundColor = [UIColor clearColor];
        _table.bounces = NO;
        [self.view addSubview:_table];
        
        NSDictionary * dic = [IXAccountGroupModel shareInstance].accGroupDic;
        _currency = @"";
        if (dic[@"currency"]) {
            _currency = dic[@"currency"];
        }
        
        NSString * title = @"Select Deposit Method";
        IXIncashHeaderV * headV = [[IXIncashHeaderV alloc] initWithTitle:title];
        headV.progressV.hidden = YES;
//        [headV showProgressWithAimStep:1 totalStep:3];
        headV._height = 80;
        headV.titleLab._centerY = headV._centerY;
        _table.tableHeaderView = headV;
        
        
//        _table.tableFooterView = [self footerV];

        
        [_table makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.top.equalTo(self.bannerView.bottom);
            make.bottom.equalTo(-110);
        }];
        
        weakself;
        _table.viewAction = ^(NSString *key, id obj) {
            IXLCellItem *item = obj;
            [weakSelf reloadBotomText:item.title];
            
            [IXBORequestMgr shareInstance].channelDic = item.dataDic;
            
            if ([item.title containsString:@"_"] || [item.title containsString:@"-"]) {
                                
                // 分支
                IXLGroup *group = weakSelf.table.groups[0];
                [group.cellItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    IXLCellItem *item = obj;
                    item.displayType = DisplayTypeDicNormal;
                }];
                
                item.displayType = DisplayTypeDicUnnormal;
                [weakSelf.table reloadData];
                
            }else{
                // 主干
                __block NSMutableArray *insertArr = [NSMutableArray array];
                [weakSelf.table.subArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *dic = obj;
                    if ([dic[@"gatewayCode"] containsString:item.title]) {
                        [insertArr addObject:obj];
                    }
                }];
                
                NSInteger index = [weakSelf.table.mainArr indexOfObject:item.dataDic];
                weakSelf.table.index = index;
                weakSelf.table.insertArr = insertArr;
                if (!item.dataDic) {
                    weakSelf.table.index = -1;
                }
                
                [weakSelf.table reloadTableView];
            }
            
            UIButton *nextBtn = [UIView getSubObjFromSupperObj:weakSelf.bottomV bySubObjectKey:@"nextBtn"];
            nextBtn.enabled = YES;
        };
        
    }
    return _table;
}

- (UIImageView *)bannerView{
    if (!_bannerView) {
        _bannerView = [UIImageView new];
        _bannerView.userInteractionEnabled = YES;
        [self.view addSubview:_bannerView];
        _bannerView.image = [UIImage imageNamed:@"dps_channel_banner"];
        [_bannerView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.height.equalTo(40);
            make.top.equalTo(5);
        }];
        
        UIImageView *img_close = [UIImageView new];
        img_close.image = [UIImage imageNamed:@"dps_banner_close"];
        img_close.contentMode = UIViewContentModeCenter;
        [_bannerView addSubview:img_close];
        [img_close makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(0);
            make.right.equalTo(0);
            make.width.equalTo(40);
            make.height.equalTo(40);
        }];
        weakself;
        [img_close block_whenTapped:^(UIView *aView) {
            [weakSelf.bannerView updateConstraints:^(MASConstraintMaker *make) {
                weakSelf.bannerView.hidden = YES;
                make.height.equalTo(0);
            }];
            
        }];
        
        UIImageView *img_gift = [UIImageView new];
        img_gift.image = [UIImage imageNamed:@"dps_banner_gift"];
        [_bannerView addSubview:img_gift];
        [img_gift makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(0);
            make.left.equalTo(15);
        }];
        
        
        UILabel *lb_content = [UILabel new];
        lb_content.font = ROBOT_FONT(13);
//        lb_content.text = @"Deposit $100 to get $100 bounds";
        lb_content.textColor = [UIColor whiteColor];
        [_bannerView addSubview:lb_content];
        [UIView bondSupperObject:_bannerView subObject:lb_content byKey:@"lb_content"];
        [lb_content makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(0);
            make.left.equalTo(img_gift.right).offset(15);
        }];
    }
    return _bannerView;
}

- (UIView *)epayIntroView{
    if (!_epayIntroView) {
        _epayIntroView = [UIView new];
        _epayIntroView.dk_backgroundColorPicker = DKNavBarColor;
        [self.view addSubview:_epayIntroView];
        [_epayIntroView makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(0);
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.height.equalTo(50);
        }];
        
        UIView *lastV = nil;
        NSArray *titles = @[@"Bitcoin",@"Perfect Money",@"Payeen",@"Advcash"];
        for (int i = 0; i < 4; i++) {
            UIView *line = [UIView new];
            line.dk_backgroundColorPicker = DKCellTitleColor;
            [_epayIntroView addSubview:line];
            [line makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(1);
                make.centerY.equalTo(0);
                if (lastV) {
                    make.width.equalTo(10);
                    make.left.equalTo(lastV.right).offset(2);
                }else{
                    make.left.equalTo(10);
                    make.width.equalTo(0);
                }
            }];
            
            UIImageView *imgV = [UIImageView new];
            imgV.image = [UIImage imageNamed:[NSString stringWithFormat:@"epay_%d",i+1]];
            [_epayIntroView addSubview:imgV];
            [imgV makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(line.right).offset(2);
                make.centerY.equalTo(0);
            }];
            
            UILabel *lb_tile = [UILabel new];
            lb_tile.font = ROBOT_FONT(10);
            lb_tile.text = titles[i];
            lb_tile.dk_textColorPicker = DKCellTitleColor;
            [_epayIntroView addSubview:lb_tile];
            [lb_tile makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(0);
                make.left.equalTo(imgV.right).offset(5);
            }];
            
            lastV = lb_tile;
        }
        
        
        
    }
    return _epayIntroView;
}

- (UIView *) bottomV{
    if (!_bottomV) {
        _bottomV = [UIView new];
        _bottomV.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bottomV];
        
        [_bottomV makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.top.equalTo(_table.bottom);
            make.bottom.equalTo(0);
        }];
        
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        
        UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
        dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
        
        UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.titleLabel.font = PF_REGU(15);
        [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
        [nextBtn setTitle:LocalizedString(@"下一步") forState:UIControlStateNormal];
        [_bottomV addSubview:nextBtn];
        
        [nextBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            //        make.right.equalTo(-15);
            make.width.equalTo(kScreenWidth - 15*2);
            make.height.equalTo(44);
            make.top.equalTo(10);
        }];
        nextBtn.enabled = NO;
        
        weakself;
        [nextBtn block_touchUpInside:^(UIButton *aButton) {
            [weakSelf goToNext];
        }];
        
        [UIView bondSupperObject:_bottomV subObject:nextBtn byKey:@"nextBtn"];
        
        
        UILabel *lb_bottom = [UILabel new];
        lb_bottom.font = PINGFANG_MEDI_FONT(11);
        lb_bottom.textColor = HexRGB(0x99abba);
        lb_bottom.textAlignment = NSTextAlignmentCenter;
        lb_bottom.numberOfLines = 0;
        [_bottomV addSubview:lb_bottom];
        [lb_bottom makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_bottomV);
            make.top.equalTo(nextBtn.bottom).offset(10);
        }];
        
        [UIView bondSupperObject:_bottomV subObject:lb_bottom byKey:@"lb_bottom"];
        
        _lb_freefee = [UILabel new];
        _lb_freefee.font = PINGFANG_MEDI_FONT(11);
        _lb_freefee.textColor = HexRGB(0x11b873);
        [_bottomV addSubview:_lb_freefee];
        [_lb_freefee makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_bottomV);
            make.bottom.equalTo(-2);
        }];
        _lb_freefee.hidden = YES;
        _lb_freefee.text = @"No Withdrawal Fees";
        
        
        self.epayIntroView.hidden = YES;
    }
    return _bottomV;
}

- (void)resetBannerView{
    UILabel *lb_content = [UIView getSubObjFromSupperObj:_bannerView bySubObjectKey:@"lb_content"];
    if ([IXBORequestMgr shareInstance].mobileOnlineConfig[@"depositBonusTips"]) {
        NSString *depositBonusTips = [IXBORequestMgr shareInstance].mobileOnlineConfig[@"depositBonusTips"];
        _bannerView.hidden = NO;
        lb_content.text = depositBonusTips;
    }else{
        self.bannerView.hidden = YES;
        [self.bannerView updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(0);
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[AppsFlyerTracker sharedTracker] trackEvent:@"入金-选择支付通道页" withValues:nil];
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    if (UMAppKey.length) {
        [MobClick event:UM_deposit1];
    }

    self.title = LocalizedString(@"存入资金");
    [self addBackItem];
    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
    [self showProgressWithAimStep:1 totalStep:3 originY:0];
    
    
    [self table];
    
    [self bottomV];
    
    [self request];
    
//    [self.table reloadTableView];
    
    
}

- (void)request{
    [SVProgressHUD show];
    
    [IXBORequestMgr comp_requestCompanyConfigInfoWithId:CompanyID complete:^(NSString *errStr, NSString *errCode, id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            self.bannerView.hidden = YES;
            [self resetBannerView];
            [self requestChannel];
        }else{
            [self requestChannel];
        }
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestChannel];
    });
}

- (void)requestChannel{
    if (self.table.groups.count) {
        return;
    }
    
    // 获取支付通道列表
    [IXBORequestMgr b_getPayMethodListByCond:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [SVProgressHUD dismiss];
            
            [self.table reloadTableView];
            
            //                //默认选中第一行，并执行点击事件
            //                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            //                [self.table selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            //                if ([self.table.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]) {
            //                    [self.table.delegate tableView:self.table didSelectRowAtIndexPath:indexPath];
            //                }
        }else{
            [SVProgressHUD showInfoWithStatus:errStr];
        }
    }];
}

- (void)cancelAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)reloadBotomText:(NSString *)title{
    NSString *textStr;
    NSString *rangeStr;
    NSMutableAttributedString *text;
    NSRange range;
    
    _lb_freefee.hidden = YES;
    if ([[title lowercaseString] containsString:@"skrill"]) {
        
        textStr = @"Banks transfer and credit card transfers,\n such as VISA,AE,JCB,Diners Club,etc";
        rangeStr = @"VISA,AE,JCB,Diners Club,etc";
        
    }else if ([[title lowercaseString] containsString:@"help2pay"]){
        
        textStr = @"Internet banking payment for  the local banks \n in Malaysia, Vienam,Thailand,Indonesia";
        rangeStr = @"Vienam,Thailand,Indonesia";
        
        _lb_freefee.hidden = NO;
        
    }else if ([[title lowercaseString] containsString:@"fasapay"]){
        
        textStr = @"A E-wallet for payments in either USD or IDR \n by Indonesian bank accounts";
        rangeStr = @"in either USD or IDR";
        
    }else if ([[title lowercaseString] containsString:@"transfer money"]) {
        
        textStr = @"Via Wire transfer by visiting your local branch or online.\n  The normal processing time for wire  transfer is 3-5 days";
        rangeStr = @"Via Wire transfer by visiting your local branch or online.";
        
    }else if ([[title lowercaseString] containsString:@"credit"]){
        textStr = @"Failed to pay?\nPlease call your bank to authorize this payment";
        rangeStr = @"Please call your bank to authorize this payment";
    }else{
        textStr = @"";
        rangeStr = @"";
    }
    
    text = [[NSMutableAttributedString alloc] initWithString:textStr];
    range = [[text string] rangeOfString:rangeStr options:NSCaseInsensitiveSearch];
    
    [text yy_setColor:MarketSymbolNameColor range:range];
    
    UILabel *lb_bottom = [UIView getSubObjFromSupperObj:self.bottomV bySubObjectKey:@"lb_bottom"];
    lb_bottom.attributedText = text;
    
    if ([[title lowercaseString] containsString:@"epay"]) {
        self.epayIntroView.hidden = NO;
    }else{
        self.epayIntroView.hidden = YES;
    }
    
}

- (void)goToNext{
    if (!self.table.groups.count) {
        [self request];
        return;
    }
    
    NSDictionary *channelDic = [IXBORequestMgr shareInstance].channelDic;
    NSString *gatewayCode = channelDic[@"gatewayCode"];
    if (!channelDic) {
        // 电汇
        NSString *msg = @"You are being redirected to online CS to get the information for transfer money (Online CS's service time: GMT+0 Mon-Fri 01:00-10:00)";
        [IXTipAlertView showAlertWithMsg:msg buttonTitles:@[@"Cancel",@"Confirm Jump"] btnBlock:^(int index) {
            if (index == 1) {
//                IXWebCustomerSeviceVC *vc = [IXWebCustomerSeviceVC new];
//                [self.navigationController pushViewController:vc animated:YES];
                [self appChat];
            }
        } TimeEndAtButton:1];
        
        if (UMAppKey.length) {
            [MobClick event:UM_TelegraphicT];
        }
    }else if ([[channelDic[@"gatewayCode"] lowercaseString] containsString:@"asiapay"]){
        // asiapay
        IXCreditCardContactVC *vc = [IXCreditCardContactVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([[channelDic[@"gatewayCode"] lowercaseString] containsString:@"credit"]) {
        // 信用卡支付
        IXCreditCardInfoVC *vc = [IXCreditCardInfoVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([channelDic[@"gatewayType"] isEqualToString:@"cardpay"]) {
        // 银行卡支付
        IXIncashStep1VC *vc = [IXIncashStep1VC new];
        [self.navigationController pushViewController:vc animated:YES];
        //        IXSupplementAccountVC *model = [[IXSupplementAccountVC alloc] init];
        //        [self.navigationController pushViewController:model animated:YES];
        
    }else if([[channelDic[@"gatewayCode"] lowercaseString] containsString:@"skrill"]){
        // Skrill
        if ([[gatewayCode lowercaseString] containsString:@"skrill to skrill"]) {
            IXSkirillToSkrillVC *vc = [IXSkirillToSkrillVC new];
            [self.navigationController pushViewController:vc animated:YES];
            
            MarkEvent(@"支付通道-选择 Skrill To Skrill");
        }else if ([[gatewayCode lowercaseString] containsString:@"wallet"]){
            IXDPSAmountVC *vc = [IXDPSAmountVC new];
            [self.navigationController pushViewController:vc animated:YES];
            MarkEvent(@"支付通道-选择 skrill wallet");
        }else if ([[gatewayCode lowercaseString] containsString:@"visa"]){
            MarkEvent(@"支付通道-选择 visa by skrill");
            IXSkirllbyVisaVC *vc = [IXSkirllbyVisaVC new];
            [self.navigationController pushViewController:vc animated:YES];   
        }
//        IXSkrillOrNotVC *vc = [IXSkrillOrNotVC new];
//        [self.navigationController pushViewController:vc animated:YES];
    }else{
        // 其他nocard电子钱包
//        IXChannelOrNotVC *vc = [IXChannelOrNotVC new];
//        [self.navigationController pushViewController:vc animated:YES];
        IXDPSAmountVC *vc = [IXDPSAmountVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }

}

- (void)appChat{
    // Native客服中心
    ChatTableViewController *vc = [[ChatTableViewController alloc] initWithPid:ServicePid
                                                                           key:ServiceKey
                                                                           url:kServiceUrl];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
