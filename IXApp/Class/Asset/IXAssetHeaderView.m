//
//  IXAssetHeaderView.m
//  IXApp
//
//  Created by Seven on 2017/6/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAssetHeaderView.h"
#import "IXAccountBalanceModel.h"
#import "IXAccountGroupModel.h"
#import "IXPullDownMenu.h"
#import "UIKit+Block.h"
#import "IXUserMgr.h"

@interface IXAssetHeaderView ()
<
CAAnimationDelegate
>

@property (nonatomic, strong) UILabel   * acntTypeLab;
@property (nonatomic, strong) UILabel   * assetTitleLab;
@property (nonatomic, strong) UILabel   * freedomTitleLab;
@property (nonatomic, strong) UILabel   * freedomAmountLab;

@property(nonatomic,strong) IXPullDownMenu *menu;

@property (nonatomic, strong) UIView    * leftLine;
@property (nonatomic, strong) UIView    * rightLine;

@property (nonatomic, strong) CAShapeLayer  * progressLayer;
@property (nonatomic, strong) CAShapeLayer  * bgLayer;

@property (nonatomic, assign) CGFloat   formerProgress;

@property (nonatomic, assign) double    small;
@property (nonatomic, assign) double    middle;

@property (nonatomic, copy) void(^complete)();

@end


@implementation IXAssetHeaderView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self.layer addSublayer:self.bgLayer];
        [self.layer addSublayer:self.progressLayer];
        
        [self addSubview:self.totalAmountLab];
        [self addSubview:self.assetTitleLab];
        [self addSubview:self.acntTypeLab];
        [self addSubview:self.freedomTitleLab];
        [self addSubview:self.freedomAmountLab];
        
        [self addSubview:self.leftLine];
        [self addSubview:self.rightLine];
        
        [self night_updateColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(night_updateColor) name:DKNightVersionThemeChangingNotification object:nil];
        
        NSDictionary    * accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
        _small = 0;
        _middle = 0;
        
        if (accGroupDic && accGroupDic.count > 0) {
            _small = [accGroupDic[kStopOutLevel] doubleValue];
            _middle = [accGroupDic[kMarginCallLevel] doubleValue];
        }
        
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)night_updateColor
{
    if ([IXUserInfoMgr shareInstance].isNightMode) {
        _bgLayer.strokeColor = UIColorHexFromRGB(0x303b4d).CGColor;
        _progressLayer.strokeColor = UIColorHexFromRGB(0x50a1e5).CGColor;
    }else{
        _bgLayer.strokeColor = UIColorHexFromRGB(0x99abba).CGColor;
        _progressLayer.strokeColor = UIColorHexFromRGB(0x11b873).CGColor;
    }
}

//返回是否需要刷新
- (BOOL)refreshProgressWith:(CGFloat)progress
{
    BOOL    needRefesh = YES;
    if (progress <= 0) {
        //首次不做处理
    }
    else if (progress < _small) {
        if (_small >= 100 && progress >= 100 && _formerProgress >= 1) {
            needRefesh = NO;
        }
        _progressLayer.strokeColor = UIColorHexFromRGB(0xff4653).CGColor;
    }
    else if (progress < _middle ) {
        if (_middle >= 100 && progress >= 100 && _formerProgress >= 1) {
            needRefesh = NO;
        }
        _progressLayer.strokeColor = UIColorHexFromRGB(0xEBAD62).CGColor;
    } else {
        if (_middle >= 100 && _formerProgress*100 >= _middle) {
            needRefesh = NO;
        }
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            _progressLayer.strokeColor = UIColorHexFromRGB(0x50a1e5).CGColor;
        }else{
            _progressLayer.strokeColor = UIColorHexFromRGB(0x11b873).CGColor;
        }
    }
    
    return needRefesh;
}

#pragma mark -
#pragma mark - setter

- (void)setTotalAmount:(NSString *)totalAmount
{
    _totalAmount = totalAmount;
    _totalAmountLab.text = totalAmount;
    [IXDataProcessTools resetLabel:_totalAmountLab leftFont:30 rightFont:24 WithContent:totalAmount];
    
}

- (void)setFreedomAmount:(NSString *)freedomAmount
{
    _freedomAmount = freedomAmount;
    _freedomAmountLab.text = freedomAmount;
}

- (void)setAcntType:(NSString *)acntType
{
    _acntType = acntType;
    if ([_acntType isEqualToString:LocalizedString(@"游客")]) {
        _acntTypeLab.text = acntType;
    }else{
        _acntTypeLab.text = [@"  " stringByAppendingString:acntType];
    }
    
}


BOOL  circleAnimating;    //页面出现时画整圆动画是否在执行
- (void)setProgress:(CGFloat)progress animation:(BOOL)animation
{
    if (circleAnimating) {
        return;
    }
    
    BOOL needRefresh = [self refreshProgressWith:progress];
    if (!needRefresh) {
        return;
    }
    progress = progress/100;
    _formerProgress = progress;

    //暂时隐藏动画
    progress = MIN(progress, 1);
    CGFloat width = self.bounds.size.height/2;

    if (animation) {
        CGFloat duration = progress == 0 ? 0.5f : 0.2f;
        
        CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        pathAnimation.duration = duration;
        pathAnimation.fromValue = @(_formerProgress);
        pathAnimation.toValue = @(progress);
        pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        //使视图保留到最新状态
        pathAnimation.removedOnCompletion = NO;
        pathAnimation.fillMode =kCAFillModeForwards;
        
        [self.progressLayer addAnimation:pathAnimation forKey:nil];
    } else {
        UIBezierPath    * path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(width, width)
                                                                radius:width
                                                            startAngle:-M_PI/2
                                                              endAngle:progress*4*M_PI/2 - M_PI/2
                                                             clockwise:YES];
        _progressLayer.path = path.CGPath;
    }
}

- (void)showCircleAnimationComplete:(void(^)())complete
{
    circleAnimating = YES;
    _complete = complete;
    
    if ([IXUserInfoMgr shareInstance].isNightMode) {
        _progressLayer.strokeColor = UIColorHexFromRGB(0x50a1e5).CGColor;
    }else{
        _progressLayer.strokeColor = UIColorHexFromRGB(0x11b873).CGColor;
    }
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = .5f;
    pathAnimation.fromValue = @(0);
    pathAnimation.toValue = @(1);
    //使视图保留到最新状态
    pathAnimation.removedOnCompletion =NO;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.delegate = self;
    [self.progressLayer addAnimation:pathAnimation forKey:nil];
    
    _formerProgress = 1.0;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (flag) {
        circleAnimating = NO;
        
        if (_complete) {
            _complete();
            _complete = nil;
        }
    }
}

#pragma mark -
#pragma mark - getter

- (UILabel *)totalAmountLab
{
    if (!_totalAmountLab) {
        CGFloat y = self.frame.size.height/2 - 15;
        _totalAmountLab = [[UILabel alloc] initWithFrame:CGRectMake(0, y, circleR, 30)];
        _totalAmountLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xffffff);
        _totalAmountLab.backgroundColor = [UIColor clearColor];
        _totalAmountLab.textAlignment = NSTextAlignmentCenter;
        _totalAmountLab.font = RO_REGU(30);
    }
    
    return _totalAmountLab;
}

- (UILabel *)assetTitleLab
{
    if (!_assetTitleLab) {
        CGFloat y = CGRectGetMinY(self.totalAmountLab.frame) - 16 - 10.5;
        _assetTitleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, y, circleR, 16)];
        _assetTitleLab.dk_textColorPicker = DKCellContentColor;
        _assetTitleLab.font = PF_MEDI(13);
        _assetTitleLab.textAlignment = NSTextAlignmentCenter;
        _assetTitleLab.backgroundColor = [UIColor clearColor];
        _assetTitleLab.text = LocalizedString(@"资产净值");
    }
    
    return _assetTitleLab;
}

- (UILabel *)acntTypeLab
{
    if (!_acntTypeLab) {
        NSString    * typeStr = [IXDataProcessTools showCurrentAccountType];
        NSInteger width = [IXEntityFormatter getContentWidth:typeStr WithFont:PF_MEDI(10)] + 24;
        
        CGFloat y = CGRectGetMinY(self.assetTitleLab.frame) - 12 - 10.5;
        _acntTypeLab = [[UILabel alloc] initWithFrame:CGRectMake((circleR - width) / 2, y, width, 18)];
        _acntTypeLab.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        _acntTypeLab.dk_backgroundColorPicker = DKColorWithRGBs(0x99abba, 0x50a1e5);
        _acntTypeLab.font = PF_MEDI(10);
        _acntTypeLab.layer.cornerRadius = 2.f;
        _acntTypeLab.layer.masksToBounds = YES;
        
        if (![typeStr isEqualToString:LocalizedString(@"游客")]) {
            weakself;
            _acntTypeLab.textAlignment = NSTextAlignmentLeft;
            _acntTypeLab.text = [@"  " stringByAppendingString:typeStr];
            [_acntTypeLab block_whenTapped:^(UIView *aView) {
                [weakSelf showMenu];
            }];
            
            UIImageView *imgV = [UIImageView new];
            imgV.userInteractionEnabled = NO;
            imgV.frame = CGRectMake(width-10-5, 4, 10, 10);
            imgV.image = GET_IMAGE_NAME(@"pullDown");
            [_acntTypeLab addSubview:imgV];
        }else{
            _acntTypeLab.textAlignment = NSTextAlignmentCenter;
        }
    }
    
    return _acntTypeLab;
}

- (UILabel *)freedomTitleLab
{
    if (!_freedomTitleLab) {
        CGFloat y = CGRectGetMaxY(self.totalAmountLab.frame) + 10.5;
        _freedomTitleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, y, circleR, 16)];
        _freedomTitleLab.dk_textColorPicker = DKCellContentColor;
        _freedomTitleLab.font = PF_MEDI(13);
        _freedomTitleLab.textAlignment = NSTextAlignmentCenter;
        _freedomTitleLab.backgroundColor = [UIColor clearColor];
        _freedomTitleLab.text = LocalizedString(@"自由资金");
    }
    
    return _freedomTitleLab;
}

- (UILabel *)freedomAmountLab
{
    if (!_freedomAmountLab) {
        CGFloat y = CGRectGetMaxY(self.freedomTitleLab.frame) + 10;
        _freedomAmountLab = [[UILabel alloc] initWithFrame:CGRectMake(0, y, circleR, 20)];
        _freedomAmountLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xffffff);
        _freedomAmountLab.backgroundColor = [UIColor clearColor];
        _freedomAmountLab.textAlignment = NSTextAlignmentCenter;
        _freedomAmountLab.font = RO_REGU(18);
    }
    
    return _freedomAmountLab;
}

- (UIView *)leftLine
{
    if (!_leftLine) {
        CGFloat x = CGRectGetMinX(self.acntTypeLab.frame) - 10 - 35;
        CGFloat y = CGRectGetMidY(self.acntTypeLab.frame) - 0.5;
        _leftLine = [[UIView alloc] initWithFrame:CGRectMake(x, y, 35, 1)];
        _leftLine.dk_backgroundColorPicker = DKLineColor2;
    }
    
    return _leftLine;
}

- (UIView *)rightLine
{
    if (!_rightLine) {
        CGFloat x = CGRectGetMaxX(self.acntTypeLab.frame) + 10;
        CGFloat y = CGRectGetMidY(self.acntTypeLab.frame) - 0.5;
        _rightLine = [[UIView alloc] initWithFrame:CGRectMake(x, y, 35, 1)];
        _rightLine.dk_backgroundColorPicker = DKLineColor2;
    }
    
    return _rightLine;
}

- (CAShapeLayer *)progressLayer
{
    if (!_progressLayer){
        _progressLayer = [[CAShapeLayer alloc] init];
        _progressLayer.frame = self.bounds;
        _progressLayer.lineWidth = 4.f;
        _progressLayer.fillColor = [UIColor clearColor].CGColor;
        
        CGFloat width = self.bounds.size.height/2;
        UIBezierPath    * path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(width, width)
                                                                radius:width
                                                            startAngle:-M_PI/2
                                                              endAngle:3*M_PI/2
                                                             clockwise:YES];
        _progressLayer.path = path.CGPath;
    }
    
    return _progressLayer;
}

- (CAShapeLayer *)bgLayer
{
    if (!_bgLayer){
        _bgLayer = [[CAShapeLayer alloc] init];
        _bgLayer.frame = self.bounds;
        _bgLayer.lineWidth = 4.f;
        _bgLayer.fillColor = [UIColor clearColor].CGColor;
        
        UIBezierPath    * path = [UIBezierPath bezierPathWithOvalInRect:self.bounds];
        _bgLayer.path = path.CGPath;
        
    }
    
    return _bgLayer;
}

- (IXPullDownMenu *)menu
{
    if (!_menu) {
        
        _menu = [IXPullDownMenu menuWithMenuItems:[self menuItem]
                                        rowHeight:[self menuItemHeight] + 10
                                            width:[self menuItemWidth] + 40];
        
        _menu.itemClicked = ^(NSInteger index ,NSString * title) {
            if (index == 0) {
                [[IXUserMgr sharedIXUserMgr] switchToDemoAccount:3];
            }else{
                [[IXUserMgr sharedIXUserMgr] switchToRealAccount:3];
            }
        };
    }
    
    return _menu;
}

#pragma mark -
#pragma mark - menu

- (void)showMenu
{
    [self.menu showMenuFrom:self.acntTypeLab items:[self menuItem] offsetY:2];
}


- (CGFloat)menuItemWidth
{
    CGFloat width = 0;
    
    for (NSString * itemStr in [self menuItem]) {
        CGSize size = [itemStr sizeWithAttributes:@{NSFontAttributeName : PF_MEDI(13)}];
        if (size.width > width) {
            width = size.width;
        }
    }
    
    return width;
}

- (CGFloat)menuItemHeight
{
    CGSize size =  [@"hello" sizeWithAttributes:@{NSFontAttributeName : PF_MEDI(13)}];
    return size.height;
}

- (NSArray *)menuItem
{
    return @[@"Demo",@"Real"];
}

@end
