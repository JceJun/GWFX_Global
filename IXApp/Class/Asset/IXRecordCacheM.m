//
//  IXRecordCacheM.m
//  IXApp
//
//  Created by Evn on 2017/9/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRecordCacheM.h"
#import "IXDBSymbolMgr.h"
#import "IXDBLanguageMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBGlobal.h"

@implementation IXRecordCacheM

- (id)init
{
    self = [super init];
    if (self) {
        _symDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (IXRecordSymM *)getCacheSymbolBySymbolId:(uint64_t)symbolId
{
    if (symbolId == 0) {
        return nil;
    }
    //缓存有从缓存取
    IXRecordSymM *model = [_symDic objectForKey:[self symbolId:symbolId]];
    if (model) {
        return model;
    } else {
        model = [self querySymbolBySymbolId:symbolId];
        if (model) {
            [_symDic setObject:model forKey:[self symbolId:symbolId]];
        }
        return model;
    }
    return nil;
}

- (NSString *)symbolId:(uint64)symbolId
{
    return [NSString stringWithFormat:@"%llu",symbolId];
}

- (IXRecordSymM *)querySymbolBySymbolId:(uint64_t)symbolId
{
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
    if (symDic && symDic.count > 0) {
        IXRecordSymM *model = [self symbolModelBySymbolInfo:symDic];
        NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
        if (languageDic && languageDic.count > 0) {
            if (languageDic && languageDic.count > 0) {
                if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                    model.lanName = languageDic[kValue];
                } else {
                    model.lanName = model.name;
                }
            } else {
                model.lanName = model.name;
            }
        } else {
            model.lanName = model.name;
        }
        
        NSDictionary *marketDic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:symbolId];
        if (symbolId == [marketDic[kID] doubleValue]) {
            model.marketName = [IXEntityFormatter getMarketNameWithMarketId:[marketDic[kMarketId] doubleValue]];
        }
       return model;
    } else {
        return nil;
    }
}

- (IXRecordSymM *)symbolModelBySymbolInfo:(NSDictionary *)symDic
{
    IXRecordSymM *model = [[IXRecordSymM alloc] init];
    model.source = [IXDataProcessTools dealWithNil:symDic[kSource]];
    model.name = [IXDataProcessTools dealWithNil:symDic[kName]];
    model.contractSizeNew = [[IXDataProcessTools dealWithNil:symDic[kContractSizeNew]] intValue];
    model.volDigits = [[IXDataProcessTools dealWithNil:symDic[kVolDigits]] longLongValue];
    return model;
}

@end

@implementation IXRecordSymM


@end
