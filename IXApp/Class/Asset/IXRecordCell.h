//
//  IXRecordCell.h
//  IXApp
//
//  Created by Evn on 17/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IXRecordCacheM;
@interface IXRecordCell : UITableViewCell

- (void)reloadUIData:(NSDictionary *)dataDic
        cacheSymbole:(IXRecordCacheM *)symCacheM;

@end
