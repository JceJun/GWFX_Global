//
//  IXSupplementFileM.h
//  IXApp
//
//  Created by Bob on 2017/2/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IXSupplementFile : JSONModel

//上传接口返回的文件路径
@property (nonatomic, strong) NSString *filePath;

//上传接口返回的文件ftp路径
@property (nonatomic, strong) NSString *ftpFilePath;

//上传接口返回的文件名
@property (nonatomic, strong) NSString *fileName;

//文件类型
@property (nonatomic, strong) NSString *fileType;


@property (nonatomic, strong) NSString *gts2CustomerId;

//列编号
@property (nonatomic, strong) NSString *id;

@end


@protocol IXSupplementFile;
@interface IXSupplementFileM : JSONModel

//修改的文件集合
@property (nonatomic, strong) NSMutableArray<IXSupplementFile> *files;

@end

