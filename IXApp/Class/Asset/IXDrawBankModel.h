//
//  IXDrawBankModel.h
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "jsonModel.h"

@interface IXDrawBankModel : JSONModel

@property (nonatomic, strong) NSString *bank;

@property (nonatomic, strong) NSString *bankAccountName;

@property (nonatomic, strong) NSString *bankAccountNumber;

@property (nonatomic, strong) NSString *customerNumber;

@property(nonatomic,copy)NSString *type;

@property (nonatomic, assign) int bankOrder;



@end
