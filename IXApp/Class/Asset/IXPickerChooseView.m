//
//  IXPickerChooseView.m
//  IXApp
//
//  Created by Bob on 2017/2/10.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPickerChooseView.h"
#import "IXBOModel.h"

@interface IXPickerChooseView ()
<
UIPickerViewDataSource,
UIPickerViewDelegate
>


@property (nonatomic, strong) pickerChooseInfo  pickerChoose;
@property (nonatomic, strong) chooseResultInfo  resultChoose;

@property (nonatomic, strong) UIPickerView  * contentPV;
@property (nonatomic, strong) UIImageView   * backImg;
@property (nonatomic, strong) UIToolbar     * topBar;

@property (nonatomic, assign) NSInteger selectedProvinceRow;
@property (nonatomic, assign) NSInteger selectedCityRow;

@property (nonatomic, assign) CGRect    contentPVFrame; //记录初始化时contentPV的frame
@property (nonatomic, assign) CGRect    topBarFrame;    //记录初始化之后topBar的frame

@property (nonatomic, readwrite) BOOL    showing;

@end

@implementation IXPickerChooseView

- (id)initWithFrame:(CGRect)frame WithChooseInfo:(pickerChooseInfo)choose WithResultInfo:(chooseResultInfo)result
{
    self = [super initWithFrame:frame];
    if ( self ) {
        _selectedProvinceRow = 0;
        _selectedCityRow = 0;
        
        _pickerChoose = choose;
        _resultChoose = result;
        
        _backImg = [[UIImageView alloc] initWithFrame:self.bounds];
        _backImg.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
        [self addSubview:_backImg];
        
        frame.origin.y = kScreenHeight - 216 - kBtomMargin;
        frame.size.height = 216;
        _contentPV = [[UIPickerView alloc] initWithFrame:frame];
        _contentPVFrame = frame;
        _contentPV.dk_backgroundColorPicker = DKNavBarColor;
        _contentPV.delegate = self;
        _contentPV.dataSource = self;
        [self addSubview:_contentPV];
        
        UIView  * btomV = [[UIView alloc] initWithFrame:CGRectMake(0, GetView_MaxY(_contentPV), kScreenWidth, kBtomMargin)];
        btomV.dk_backgroundColorPicker = DKNavBarColor;
        [self addSubview:btomV];
        
        _topBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, kScreenHeight - 250, kScreenWidth, 44)];
        _topBar.dk_barTintColorPicker = DKTableColor;
        _topBarFrame = _topBar.frame;
        
        UIBarButtonItem * spaceBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
        [cancelBtn.titleLabel setFont:PF_MEDI(13)];
        [cancelBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];

        cancelBtn.frame = CGRectMake(15, 9, 60, 25);
        [cancelBtn addTarget:self action:@selector(cancelKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *cancelBtnItem = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
        
        UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [doneBtn setTitle:LocalizedString(@"确定") forState:UIControlStateNormal];
        [doneBtn.titleLabel setFont:PF_MEDI(13)];
        [doneBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        doneBtn.frame = CGRectMake(kScreenWidth - 50, 9, 60, 25);
        [doneBtn addTarget:self action:@selector(submitChooseRow)
          forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *doneBtnItem = [[UIBarButtonItem alloc]initWithCustomView:doneBtn];
        NSArray * buttonsArray = [NSArray arrayWithObjects:cancelBtnItem,spaceBtn,doneBtnItem,nil];
        [_topBar setItems:buttonsArray];
        [self addSubview:_topBar];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(dismiss)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)show
{
    _showing = YES;
    CGFloat height = _contentPVFrame.size.height + _topBarFrame.size.height;
    CGRect  beginPVFrame = CGRectMake(_contentPVFrame.origin.x,
                                    _contentPVFrame.origin.y + height,
                                    _contentPVFrame.size.width,
                                    _contentPVFrame.size.height);
    CGRect  beginBarFrame = CGRectMake(_topBarFrame.origin.x,
                                       _topBarFrame.origin.y + height,
                                       _topBarFrame.size.width,
                                       _topBarFrame.size.height);
    _contentPV.frame = beginPVFrame;
    _topBar.frame = beginBarFrame;
    _backImg.alpha = 0.75;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _backImg.alpha = 0.75;
        _contentPV.frame = _contentPVFrame;
        _topBar.frame = _topBarFrame;
    } completion:^(BOOL finished) {
        [_contentPV selectRow:_selectedProvinceRow inComponent:0 animated:YES];
        @try{
             [_contentPV selectRow:_selectedCityRow inComponent:1 animated:YES];
        }@catch(NSException *exception) {
            
        }

    }];
}

- (void)dismiss
{
    _showing = NO;
    if (_backImg.alpha == 0) {
        [self removeFromSuperview];
        return;
    }
    
    CGFloat height = _contentPVFrame.size.height + _topBarFrame.size.height;
    CGRect  pvAimFrame = CGRectMake(_contentPVFrame.origin.x,
                                      _contentPVFrame.origin.y + height,
                                      _contentPVFrame.size.width,
                                      _contentPVFrame.size.height);
    CGRect  barAimFrame = CGRectMake(_topBarFrame.origin.x,
                                       _topBarFrame.origin.y + height,
                                       _topBarFrame.size.width,
                                       _topBarFrame.size.height);

    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _backImg.alpha = 0;
        _contentPV.frame = pvAimFrame;
        _topBar.frame = barAimFrame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)submitChooseRow
{
    [self dismiss];
    if ( _resultChoose ) {
        _resultChoose(_selectedProvinceRow, _selectedCityRow);
    }
}

- (void)cancelKeyboardHidden
{
    [self dismiss];
}

- (void)setCountryArr:(NSMutableArray *)countryArr
{
    _countryArr = countryArr;
    [self.contentPV reloadComponent:0];
}

- (void)setProvinceArr:(NSMutableArray *)provinceArr
{
    _provinceArr = provinceArr;
    [self.contentPV reloadComponent:1];
}

- (void)setDataArr:(NSMutableArray *)dataArr
{
    _dataArr = [NSMutableArray arrayWithArray:dataArr];
    [self configSeletedInfo];
    [self.contentPV reloadAllComponents];
}

- (void)setSeletedTitles:(NSArray *)seletedTitles
{
    _seletedTitles = seletedTitles;
    [self configSeletedInfo];
    [self.contentPV reloadAllComponents];
}

- (void)configSeletedInfo
{
    if (!_seletedTitles || !_dataArr) {
        return;
    }
    
    NSString    * pCode = [_seletedTitles firstObject];
    NSString    * cCode = [_seletedTitles lastObject];
    
    __block IXProvinceM * provenceM = nil;
    
    if (pCode.length) {
        [_dataArr enumerateObjectsUsingBlock:^(IXProvinceM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isSameProvence:pCode]) {
                _selectedProvinceRow = idx;
                provenceM = obj;
                *stop = YES;
            }
        }];
    }
    
    if (cCode.length && provenceM) {
        [provenceM.subCountryDictParamList enumerateObjectsUsingBlock:^(IXCityM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isSameCity:cCode]) {
                _selectedCityRow = idx;
                *stop = YES;
            }
        }];
    }
}


#pragma mark -
#pragma mark - picker delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (_onlyProvince) {
        return 1;
    }
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch ( component ) {
        case 0:{
            return _dataArr.count;
        }
            break;
        case 1:{
            IXProvinceM *model = _dataArr[_selectedProvinceRow];
            return model.subCountryDictParamList.count;
        }
            break;
        default:
            return 0;
            break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch ( component ) {
        case 0:{
            IXProvinceM *model = [_dataArr objectAtIndex:row];
            if ( [BoLanKey isEqualToString:NAMEEN] ) {
                return  model.nameEN;
            }else if ( [BoLanKey isEqualToString:NAMECN] )
                return model.nameCN;
            return model.nameTW;
        }
            break;
        case 1:{
            IXProvinceM *model = [_dataArr objectAtIndex:_selectedProvinceRow];
            IXCityM *cityM = [model.subCountryDictParamList objectAtIndex:row];
            if ( [BoLanKey isEqualToString:NAMEEN] ) {
                return  cityM.nameEN;
            }else if ( [BoLanKey isEqualToString:NAMECN] )
                return cityM.nameCN;
            return cityM.nameTW;
        }
            break;
        default:
            return 0;
            break;
    }
}


//重写方法
- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:PF_MEDI(13)];
        pickerLabel.dk_textColorPicker = DKCellTitleColor;
        pickerLabel.adjustsFontSizeToFitWidth = YES;
    }
    // Fill the label text here
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ( component == 0 ) {
        _selectedCityRow = 0;
        _selectedProvinceRow = row;
        @try{
            [_contentPV reloadComponent:1];
            [_contentPV selectRow:_selectedCityRow inComponent:1 animated:NO];
        }@catch(NSException *exception) {
            
        }
    }else{
        _selectedCityRow = row;
    }
}


@end
