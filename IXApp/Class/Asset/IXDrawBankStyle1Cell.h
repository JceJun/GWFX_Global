//
//  IXDrawBankStyle1Cell.h
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHTextView.h"

typedef void(^endInputValue)(NSString *value);

@interface IXDrawBankStyle1Cell : UITableViewCell

@property (nonatomic, strong) UITextField *drawMoneyTF;

@property (nonatomic, strong) NSString *bankCurrencyType;

@property (nonatomic, strong) NSString *balence;

@property (nonatomic, strong) endInputValue textContent;

@property (nonatomic, assign) CGFloat   totalAmount;    //可取出的总金额

@end
