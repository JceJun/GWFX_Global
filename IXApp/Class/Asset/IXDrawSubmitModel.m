//
//  IXDrawSubmitModel.m
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawSubmitModel.h"

@implementation IXDrawSubmitModel

- (id)init
{
    self = [super init];
    if ( self ) {
        _accountCurrency = @"USD";
        _transCurrency = @"USD";
        _payCurrency = @"USD";
        
        _exchangeRate = @"1";
        _bankPayMethod = @"P";
    }
    return self;
}
@end
