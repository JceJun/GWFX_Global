//
//  IXDrawBankResultVC.m
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawBankResultVC.h"
#import "IXDrawResultCell.h"

@interface IXDrawBankResultVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *contentTV;


@end

@implementation IXDrawBankResultVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.view addSubview:self.contentTV];
    [self addBackItemrget:self action:@selector(onGoback)];
    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(onGoback)];
    
    self.title = LocalizedString(@"账户出金");
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 98;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 112)];
    headerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *tipImg = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 80)/2, 45, 80, 80)];
    tipImg.dk_imagePicker =  DKImageNames(@"openAccount_complete", @"openAccount_complete_D");
    [headerView addSubview:tipImg];
    
    UILabel *label = [IXUtils createLblWithFrame:CGRectMake( 0, 143, kScreenWidth, 18)
                                        WithFont:PF_MEDI(15)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea];
    label.text = LocalizedString(@"取款申请已提交");
    [headerView addSubview:label];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 203;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDrawResultCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                  NSStringFromClass([IXDrawResultCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.submitTime = [IXEntityFormatter getCurrentTimeFormaterHHMM];
    cell.tipMsg = @"Expected  back to your account";
    
    
    
    return cell;

}


- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:self.view.bounds];
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _contentTV.dk_backgroundColorPicker = DKTableColor;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXDrawResultCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDrawResultCell class])];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _contentTV;
}


@end
