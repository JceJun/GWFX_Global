//
//  IXAdvertisementV.m
//  IXApp
//
//  Created by Seven on 2017/6/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAdvertisementV.h"

@interface IXAdvertisementV ()

@property (nonatomic, strong) UIImageView   * imgView;
@property (nonatomic, strong) UIButton      * dismissBtn;

@end

@implementation IXAdvertisementV

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _imgView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.clipsToBounds = YES;
        _imgView.userInteractionEnabled = YES;
        _imgView.backgroundColor = [UIColor orangeColor];
        [self addSubview:_imgView];
        
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [_imgView addGestureRecognizer:tap];
        
        _dismissBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        _dismissBtn.center = CGPointMake(frame.size.width - 30, frame.size.height/2);
        [_dismissBtn setBackgroundImage:[UIImage imageNamed:@"dismiss"] forState:UIControlStateNormal];
        [_dismissBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_dismissBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_dismissBtn];

        self.alpha = 0;
    }
    return self;
}

- (void)tapAction
{
    if (self.advertisementBlock) {
        self.advertisementBlock();
    }
}

- (void)show
{
    [UIView animateWithDuration:0.15 animations:^{
        self.alpha = 1.f;
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


- (void)setImg:(UIImage *)img
{
    _img = img;
    _imgView.image = img;
}


@end
