//
//  IXReportModel.h
//  IXApp
//
//  Created by Evn on 17/2/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IXReportModel : JSONModel

@property (nonatomic, strong) NSString *tradeAccountId;

@end
