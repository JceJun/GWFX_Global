
//
//  IXDrawBankStyle1Cell.m
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawBankStyle1Cell.h"
#import "IXAppUtil.h"

@interface IXDrawBankStyle1Cell ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel *moneyTypeLbl;

@end

@implementation IXDrawBankStyle1Cell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        self.contentView.dk_backgroundColorPicker = DKNavBarColor;
        
        UIButton *allDrawBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:allDrawBtn];
        CGFloat width = [IXDataProcessTools textSizeByText:LocalizedString(@"全部取出")
                                                    height:15
                                                      font:PF_MEDI(13)].width;
        CGFloat orginX = kScreenWidth - width - 15;
        allDrawBtn.frame = CGRectMake( orginX, 0, width, 56);
        [allDrawBtn setTitle:LocalizedString(@"全部取出")
                    forState:UIControlStateNormal];
        [allDrawBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea)
                                  forState:UIControlStateNormal];
        [allDrawBtn.titleLabel setFont:PF_MEDI(13)];
        [allDrawBtn addTarget:self
                       action:@selector(responseToAllDraw)
             forControlEvents:UIControlEventTouchUpInside];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(validTextContent)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:nil];
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)removeFromSuperview
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)validTextContent
{
    NSString *content = _drawMoneyTF.text;
    NSRange range = [content rangeOfString:@"."];
    if ( range.location != NSNotFound ) {
        if ( content.length - range.location  > 3 ) {
            _drawMoneyTF.text = [_drawMoneyTF.text substringToIndex:(range.location + 3)];
        }
    }
}

- (UILabel *)moneyTypeLbl
{
    if ( !_moneyTypeLbl ) {
        _moneyTypeLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 19, 20, 18)
                                           WithFont:PF_MEDI(15)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4
                         ];
        [self.contentView addSubview:_moneyTypeLbl];
    }
    return _moneyTypeLbl;
}

- (UITextField *)drawMoneyTF
{
    if ( !_drawMoneyTF ) {
        _drawMoneyTF = [[UITextField alloc] init];
        _drawMoneyTF.delegate = self;
        _drawMoneyTF.textAlignment = NSTextAlignmentLeft;
        _drawMoneyTF.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _drawMoneyTF.font = PF_REGU(15);
        [self.contentView addSubview:_drawMoneyTF];
    }
    return _drawMoneyTF;
}

- (void)setBankCurrencyType:(NSString *)bankCurrencyType
{
    self.moneyTypeLbl.text = bankCurrencyType;
    
    CGRect frame = self.moneyTypeLbl.frame;
    frame.size.width = [IXDataProcessTools textSizeByText:bankCurrencyType
                                                   height:24
                                                     font:RO_REGU(24)].width;
    self.moneyTypeLbl.frame = frame;
    
    frame = self.drawMoneyTF.frame;
    frame = CGRectMake( CGRectGetMaxX( _moneyTypeLbl.frame ) + 10, 13, 200, 30);
    self.drawMoneyTF.frame = frame;
}

- (void)responseToAllDraw
{
    if ( _balence && [_balence isKindOfClass:[NSString class]] ) {
        _drawMoneyTF.text = _balence;
        [self textFieldDidEndEditing:_drawMoneyTF];
    }
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (string.length && [@"0123456789." rangeOfString:string].location == NSNotFound) {
        return NO;
    }
    
    if ([textField.text isEqualToString:@"0"] && [string isEqualToString:@"0"]){
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([string isEqualToString:@"."]) {
        if (![textField.text containsString:@"."]) {
            textField.text = aimStr;
        }
        return NO;
    }
    
    if ([IXAppUtil stringToAmount:aimStr] > _totalAmount) {
        textField.text = [IXAppUtil amountToString:@(_totalAmount)];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    double amount = [IXAppUtil stringToAmount:textField.text];

    //添加逗号
    if (amount > 0) {
        textField.text = [IXAppUtil amountToString:@(amount)];
    }
    
    if (_textContent) {
        _textContent(textField.text);
    }
}

@end
