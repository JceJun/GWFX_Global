//
//  IXDrawResultCell.h
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDrawResultCell : UITableViewCell

@property (nonatomic, strong) NSString *submitTime;
@property (nonatomic, strong) NSString *tipMsg;

@end
