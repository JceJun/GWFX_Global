//
//  IXSupplementAccountResultVC.m
//  IXApp
//
//  Created by Bob on 2017/2/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSupplementAccountResultVC.h"
#import "IXIncashStep1VC.h"
#import "IXAcntBankListVC.h"

@interface IXSupplementAccountResultVC ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, strong) UITableView *contentTV;

@property (nonatomic, strong) UIView *headView;

@end

@implementation IXSupplementAccountResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    [self addBackItemrget:self action:@selector(onGoback)];
    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(onGoback)];

    self.title = LocalizedString(@"添加银行卡");
    [self.view addSubview:self.contentTV];
}

- (void)onGoback
{
    NSArray *temArray = self.navigationController.viewControllers;
    BOOL flag = NO;
    for(UIViewController *temVC in temArray)
        
    {
        if ([temVC isKindOfClass:[IXIncashStep1VC class]]){
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAddBank object:_bankCode];
            flag = YES;
            [self.navigationController popToViewController:temVC animated:YES];
        } else if ([temVC isKindOfClass:[IXAcntBankListVC class]]) {
            flag = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAcntInfoAddBank object:nil];
            [self.navigationController popToViewController:temVC animated:YES];
        }
        
    }
    if (!flag) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
 
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    return cell;
}
- (UIView *)headView
{
    if ( !_headView ) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 230)];
        
        UIImageView *tipImg = [[UIImageView alloc] initWithFrame:
                               CGRectMake( (kScreenWidth - 80)/2, 55, 80, 80)];
        tipImg.dk_imagePicker = DKImageNames(@"openAccount_complete", @"openAccount_complete_D");
        [_headView addSubview:tipImg];

        UILabel *tip1 = [IXUtils createLblWithFrame:CGRectMake( 0, 155, kScreenWidth, 18)
                                           WithFont:PF_MEDI(15)
                                          WithAlign:NSTextAlignmentCenter
                                         wTextColor:0x4c6072
                                         dTextColor:0xe9e9ea];
        tip1.text = LocalizedString(@"银行卡资料成功提交");
        [_headView addSubview:tip1];
        
        UILabel *tip2 = [IXUtils createLblWithFrame:CGRectMake( 0, 188, kScreenWidth, 12)
                                           WithFont:PF_MEDI(12)
                                          WithAlign:NSTextAlignmentCenter
                                         wTextColor:0x4c6072
                                         dTextColor:0xe9e9ea];
        tip2.text = LocalizedString(@"我们将在1个工作日内处理");
        [_headView addSubview:tip2];
        
        
        UILabel *tip3 = [IXUtils createLblWithFrame:CGRectMake( 0, 209, kScreenWidth, 12)
                                           WithFont:PF_MEDI(12)
                                          WithAlign:NSTextAlignmentCenter
                                         wTextColor:0x4c6072
                                         dTextColor:0xe9e9ea];
        tip3.text = LocalizedString(@"审核通过后即可进行提款操作");
        [_headView addSubview:tip3];
        
    }
    return _headView;
}


- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:self.view.bounds];
        _contentTV.separatorInset = UIEdgeInsetsMake( 0, -10, 0, 0);
        _contentTV.dk_backgroundColorPicker = DKTableColor;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.tableHeaderView = self.headView;
        [_contentTV registerClass:[UITableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _contentTV;
}
@end
