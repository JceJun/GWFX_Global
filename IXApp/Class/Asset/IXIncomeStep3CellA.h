//
//  IXIncomeStep3CellA.h
//  IXApp
//
//  Created by Evn on 17/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXIncomeStep3CellA : UITableViewCell

@property (nonatomic, strong)UILabel    * desc;

- (void)loadUIWithTitle:(NSString *)title
                   desc:(NSString *)desc
                    tag:(NSUInteger)tag
                process:(BOOL)flag;

@end
