//
//  IXIncomeStep1ACell.h
//  IXApp
//
//  Created by Evn on 17/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IXPayMethod;

@interface IXIncomeStep1ACell : UITableViewCell

- (void)reloadUIWithData:(IXPayMethod *)method
                isSelect:(BOOL)flag;
@end
