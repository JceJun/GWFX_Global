//
//  IXIncomeStep3VC.h
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXDataBaseVC.h"
#import "IXIncomeModel.h"
@class IXBaseUrlM;

@interface IXIncashResultVC : IXDataBaseVC
@property (nonatomic,strong)IXBaseUrlM      * baseUrl;
@property (nonatomic,strong)IXIncomeModel   * incomeModel;
@end
