//
//  IXAmountChooseV.h
//  IXApp
//
//  Created by Seven on 2017/5/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 入金金额选择视图
 */
@interface IXAmountChooseV : UIView

@property (nonatomic, readonly) NSInteger   minAmount;
@property (nonatomic, readonly) NSInteger   maxAmount;

@property (nonatomic, copy) void(^callBackAmount)(CGFloat amount,NSString * amountStr);

+ (instancetype)amountChooseVWithFrame:(CGRect)frame minAmount:(NSInteger)min maxAmount:(NSInteger)max;
- (instancetype)initWithFrame:(CGRect)frame minAmount:(NSInteger)min maxAmount:(NSInteger)max;

- (void)setMinAmount:(NSInteger)min maxAmount:(NSInteger)max;

- (void)resetStatus;

@end
