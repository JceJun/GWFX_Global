//
//  IXDrawConfirmV.m
//  IXApp
//
//  Created by Evn on 2017/9/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawConfirmV.h"

@interface IXDrawConfirmV()

@property (nonatomic, strong)UIView *alertView;
@property (nonatomic, strong)UILabel *hint;
@property (nonatomic, strong)UILabel *showDrawMoney;
@property (nonatomic, strong)UILabel *drawMoney;
@property (nonatomic, strong)UILabel *showFee;
@property (nonatomic, strong)UILabel *fee;
@property (nonatomic, strong)UILabel *showRealMoney;
@property (nonatomic, strong)UILabel *realMoney;
@property (nonatomic, strong)UIButton *cancelBtn;
@property (nonatomic, strong)UIButton *sureBtn;
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UIView *cutLine;

@property (nonatomic, assign)CGFloat width;
@property (nonatomic, strong)NSString *money;
@property (nonatomic, strong)NSString *drawFee;

@end

@implementation IXDrawConfirmV

- (id)initWithFrame:(CGRect)frame withMoney:(NSString *)money withDrawFee:(NSString *)drawFee
{
    self = [super initWithFrame:kScreenBound];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75f];
        self.frame = frame;
        _width = frame.size.width;
        _money = money;
        _drawFee = drawFee;
        [self loadUI];
    }
    return self;
}

- (void)loadUI
{
    [self alertView];
    [self hint];
    [self showDrawMoney];
    [self drawMoney];
    [self showFee];
    [self fee];
    [self showRealMoney];
    [self realMoney];
    [self uLine];
    [self cancelBtn];
    [self sureBtn];
    [self cutLine];
}

- (UIView *)alertView
{
    if (!_alertView) {
        _alertView = [[UIView alloc]init];
        _alertView.bounds = self.frame;
        _alertView.dk_backgroundColorPicker = DKViewColor;
        _alertView.layer.cornerRadius = 10;
        _alertView.layer.masksToBounds = YES;
    }
    return _alertView;
}

- (void)show
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.center = keyWindow.center;
    [keyWindow addSubview:self];
    
    self.frame = keyWindow.bounds;
    
    self.alertView.center = keyWindow.center;
    [self addSubview:self.alertView];
}

- (UILabel *)hint
{
    if ( !_hint ) {
        _hint = [IXUtils createLblWithFrame:CGRectMake( 15, 28, _width - 30, 18)
                                   WithFont:PF_MEDI(15)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        _hint.text = LocalizedString(@"出金提示");
        [self.alertView addSubview:_hint];
    }
    return _hint;
}

- (UILabel *)showDrawMoney
{
    if ( !_showDrawMoney ) {
        _showDrawMoney = [IXUtils createLblWithFrame:CGRectMake( 26, GetView_MaxY(self.hint) + 25, _width - 26, 15)
                                   WithFont:PF_MEDI(13)
                                  WithAlign:NSTextAlignmentLeft
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        _showDrawMoney.text = LocalizedString(@"取款金额");
        [self.alertView addSubview:_showDrawMoney];
    }
    return _showDrawMoney;
}

- (UILabel *)drawMoney
{
    if ( !_drawMoney ) {
        _drawMoney = [IXUtils createLblWithFrame:CGRectMake( _width/2, VIEW_Y(self.showDrawMoney), (_width - 52)/2, 15)
                                            WithFont:PF_MEDI(13)
                                           WithAlign:NSTextAlignmentRight
                                          wTextColor:0x4c6072
                                          dTextColor:0xe9e9ea
                          ];
        _drawMoney.text = [NSString stringWithFormat:@"%@",_money];
        [self.alertView addSubview:_drawMoney];
    }
    return _drawMoney;
}

- (UILabel *)showFee
{
    if ( !_showFee ) {
        _showFee = [IXUtils createLblWithFrame:CGRectMake( 26, GetView_MaxY(self.showDrawMoney) + 20, _width - 26, 15)
                                            WithFont:PF_MEDI(13)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0x4c6072
                                          dTextColor:0xe9e9ea
                          ];
        _showFee.text = LocalizedString(@"手续费");
        [self.alertView addSubview:_showFee];
    }
    return _showFee;
}

- (UILabel *)fee
{
    if ( !_fee ) {
        _fee = [IXUtils createLblWithFrame:CGRectMake( _width/2, VIEW_Y(self.showFee), (_width - 52)/2, 15)
                                        WithFont:PF_MEDI(13)
                                       WithAlign:NSTextAlignmentRight
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea
                      ];
        _fee.text = [NSString stringWithFormat:@"%@",_drawFee];
        [self.alertView addSubview:_fee];
    }
    return _fee;
}

- (UILabel *)showRealMoney
{
    if ( !_showRealMoney ) {
        _showRealMoney = [IXUtils createLblWithFrame:CGRectMake( 26, GetView_MaxY(self.showFee) + 20, _width - 26, 15)
                                      WithFont:PF_MEDI(13)
                                     WithAlign:NSTextAlignmentLeft
                                    wTextColor:0x4c6072
                                    dTextColor:0xe9e9ea
                    ];
        _showRealMoney.text = LocalizedString(@"实际到账");
        [self.alertView addSubview:_showRealMoney];
    }
    return _showRealMoney;
}

- (UILabel *)realMoney
{
    if ( !_realMoney ) {
        _realMoney = [IXUtils createLblWithFrame:CGRectMake( _width/2, VIEW_Y(self.showRealMoney), (_width - 52)/2, 15)
                                  WithFont:PF_MEDI(13)
                                 WithAlign:NSTextAlignmentRight
                                wTextColor:0x4c6072
                                dTextColor:0xe9e9ea
                ];
        double realMoney = [_money doubleValue] - [_drawFee doubleValue];
        _realMoney.text = [NSString stringWithFormat:@"%.2f",realMoney];
        [self.alertView addSubview:_realMoney];
    }
    return _realMoney;
}

- (UIView *)uLine
{
    if (!_uLine) {
        _uLine = [[UIView alloc] initWithFrame:CGRectMake(0, GetView_MaxY(self.showRealMoney) + 28, _width, 1)];
        _uLine.dk_backgroundColorPicker = DKLineColor;
        [self.alertView addSubview:_uLine];
    }
    return _uLine;
}

- (UIButton *)cancelBtn
{
    if (!_cancelBtn) {
        _cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, GetView_MaxY(self.uLine) + 1, (_width - 1)/2, 46)];
        [_cancelBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        _cancelBtn.tag = 0;
        _cancelBtn.titleLabel.font = PF_MEDI(15);
        [_cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
        [_cancelBtn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [self.alertView addSubview:_cancelBtn];
    }
    return _cancelBtn;
}

- (UIButton *)sureBtn
{
    if (!_sureBtn) {
        _sureBtn = [[UIButton alloc] initWithFrame:CGRectMake((_width - 1)/2, GetView_MaxY(self.uLine) + 1, (_width - 1)/2, 46)];
        [_sureBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        _sureBtn.tag = 1;
        _sureBtn.titleLabel.font = PF_MEDI(15);
        [_sureBtn setTitle:LocalizedString(@"确认") forState:UIControlStateNormal];
        [_sureBtn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [self.alertView addSubview:_sureBtn];
    }
    return _sureBtn;
}

- (UIView *)cutLine
{
    if (!_cutLine) {
        _cutLine = [[UIView alloc] initWithFrame:CGRectMake((_width - 1)/2, GetView_MaxY(self.uLine), 1 ,VIEW_H(self.sureBtn))];
        _cutLine.dk_backgroundColorPicker = DKLineColor;
        [self.alertView addSubview:_cutLine];
    }
    return _cutLine;
}

- (void)click:(UIButton *)btn
{
    if (_selectBtn) {
        _selectBtn(btn.tag);
    }
    if (self.alertView) {
        [self.alertView removeFromSuperview];
    }
    if (self) {
        [self removeFromSuperview];
    }
}

@end
