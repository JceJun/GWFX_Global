//
//  IXRecordModel.h
//  IXApp
//
//  Created by Evn on 17/2/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IXRecordModel : JSONModel
@property (nonatomic, strong) NSString *gts2AccountId;
@end
