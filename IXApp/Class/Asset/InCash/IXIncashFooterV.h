//
//  IXIncashFooterV.h
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXIncashFooterV : UIView

- (instancetype)initWithFrame:(CGRect)frame target:(id)target action:(SEL)func;

@end
