//
//  TIXTextFieldCell.m
//  FXApp
//
//  Created by Seven on 2017/7/18.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "IXTextFieldCell.h"

@interface IXTextFieldCell ()

@property (nonatomic, strong) UIImageView   * nextImgV;

@end

@implementation IXTextFieldCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self createSubview];
    }
    return self;
}

- (void)createSubview
{
    _titleLabWidth = 80.f;
    _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(14, (kCellHeight-20)/2, 80, 20)];
    _titleLab.dk_textColorPicker = DKCellTitleColor;
    _titleLab.font = PF_MEDI(13);
    [self.contentView addSubview:_titleLab];
    
    _textF = [[IXTextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_titleLab.frame) + 10,
                                                           CGRectGetMinY(_titleLab.frame),
                                                           kScreenWidth - CGRectGetMaxX(_titleLab.frame) - 30,
                                                           20)];
    _textF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _textF.font = PF_MEDI(13);
    _textF.dk_textColorPicker = DKCellTitleColor;
    [self.contentView addSubview:_textF];
    
    _nextImgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - 28, CGRectGetMinY(_titleLab.frame), 20, 20)];
    _nextImgV.hidden = YES;
    _nextImgV.image = AutoNightImageNamed(@"broadSide_arrow_right");
    [self.contentView addSubview:_nextImgV];
}

- (void)setTitleLabWidth:(CGFloat)titleLabWidth
{
    _titleLabWidth = titleLabWidth;
    _titleLab.frame = CGRectMake(14, (kCellHeight-20)/2, titleLabWidth, 20);
    _textF.frame = CGRectMake(CGRectGetMaxX(_titleLab.frame) + 10,
                              CGRectGetMinY(_titleLab.frame),
                              kScreenWidth - CGRectGetMaxX(_titleLab.frame) - 20,
                              20);
}

- (void)setShowNextImgV:(BOOL)showNextImgV
{
    _showNextImgV = showNextImgV;
    _nextImgV.hidden = !showNextImgV;
    if (!showNextImgV) {
        _textF.frame = CGRectMake(CGRectGetMaxX(_titleLab.frame) + 10,
                                  CGRectGetMinY(_titleLab.frame),
                                  kScreenWidth - CGRectGetMaxX(_titleLab.frame) - 30,
                                  20);
    } else {
        _textF.frame = CGRectMake(CGRectGetMaxX(_titleLab.frame) + 10,
                                  CGRectGetMinY(_titleLab.frame),
                                  CGRectGetMinX(_nextImgV.frame) - CGRectGetMaxX(_titleLab.frame) - 8,
                                  20);
    }
}

@end
