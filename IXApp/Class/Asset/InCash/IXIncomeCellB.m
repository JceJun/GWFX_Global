//
//  IXIncomeCellB.m
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeCellB.h"
#import "IXUserInfoM.h"
#import "NSString+IX.h"

@interface IXIncomeCellB ()

@property (nonatomic, strong) UIImageView   * imgV;
@property (nonatomic, strong) UIImageView   * markImgV;
@property (nonatomic, strong) UILabel   * desLab;

@end

@implementation IXIncomeCellB

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createSubview];
    }
    return self;
}

- (void)createSubview
{
    CGRect rect = CGRectMake(10, 9.5, 25, 25);
    _imgV = [[UIImageView alloc] initWithFrame:rect];
    _imgV.contentMode = UIViewContentModeScaleAspectFit;
    _imgV.image = [UIImage imageNamed:@"cardPay"];
    [self.contentView addSubview:_imgV];
    
    rect = CGRectMake(GetView_MaxX(_imgV) + 10, 11, kScreenWidth - (GetView_MaxX(_imgV) + 10) , 22);
    _desLab = [[UILabel alloc] initWithFrame:rect];
    _desLab.font = PF_MEDI(13);
    _desLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    _desLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_desLab];
    
    rect = CGRectMake(kScreenWidth - (17 + 15), 13.5, 17, 17);
    _markImgV = [[UIImageView alloc] initWithFrame:rect];
    _markImgV.dk_imagePicker = DKImageNames(@"common_cell_choose", @"common_cell_choose_D");
    [self.contentView addSubview:_markImgV];
}

- (void)setBankM:(IXUserBankM *)bankM
{
    if ([bankM isKindOfClass:[IXUserBankM class]]) {
        _bankM = bankM;
        NSString    * accountNo = bankM.bankAccountNumber;
        
        if (accountNo && accountNo.length > 4) {
            accountNo = [accountNo substringWithRange:NSMakeRange(accountNo.length - 4, 4)];
        }
        
        NSString    * bankName = [bankM localizedName];
        if (bankName.length > 25) {
            bankName = [bankName substringToIndex:25];
            bankName = [bankName stringByAppendingString:@"..."];
        }
        _imgV.image = [UIImage imageNamed:@"cardPay"];
        
        NSString    * text = [NSString stringWithFormat:@"%@ (%@)",bankName,accountNo];
        NSAttributedString  * attStr = [text attributeStringWithFontSize:15
                                                               textColor:AutoNightColor(0x4c6072, 0xe9e9ea)];
        _desLab.attributedText = attStr;
    } else {
        _imgV.image = AutoNightImageNamed(@"addCard");
        _desLab.text = LocalizedString(@"添加银行卡");
        _markImgV.hidden = YES;
    }
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    _markImgV.hidden = !isSelected;
}

@end
