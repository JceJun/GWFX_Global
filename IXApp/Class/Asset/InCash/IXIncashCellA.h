//
//  IXIncashCellA.h
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXCommonBaseCell.h"

typedef NS_ENUM(NSInteger,IXIncashMode) {
    IXIncashModeCreditCard  //银行卡支付
};

/** 支付方式 - 大类 */
@interface IXIncashCellA : IXCommonBaseCell

@property (nonatomic, assign) IXIncashMode   incashMode;
@property (nonatomic, assign) BOOL  isSelected;

@end
