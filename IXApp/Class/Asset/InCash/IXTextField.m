//
//  IXTextField.m
//  FXApp
//
//  Created by Seven on 2017/7/26.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "IXTextField.h"

@implementation IXTextField

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)setPlaceholder:(NSString *)placeholder
{
    UIColor         * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
    NSDictionary    * att = @{
                              NSForegroundColorAttributeName:phColor,
                              };
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:att];
}

- (void)textChanged:(NSNotification *)notify
{
    if (notify.object == self) {
        [self dealWithTextFont];
    }
}

- (void)dealWithTextFont
{
    if (self.text.length) {
        if (self.font != _textFont) {
            self.font = _textFont;
        }
    } else {
        if (self.font != _placeHolderFont) {
            self.font = _placeHolderFont;
        }
    }
}

- (void)setPlaceHolderFont:(UIFont *)placeHolderFont
{
    _placeHolderFont = placeHolderFont;
    [self dealWithTextFont];
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _textFont = font;
    });
}

@end
