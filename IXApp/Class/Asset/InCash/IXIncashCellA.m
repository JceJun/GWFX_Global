//
//  IXIncashCellA.m
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashCellA.h"

@interface IXIncashCellA ()
@property (nonatomic, strong) UIImageView   * imgV;
@property (nonatomic, strong) UIImageView   * markImgV;
@property (nonatomic, strong) UILabel   * desLab;
@end

@implementation IXIncashCellA

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createSubview];
    }
    return self;
}

- (void)createSubview
{
    CGRect rect = CGRectMake(19, 15.5, 25, 25);
    _imgV = [[UIImageView alloc] initWithFrame:rect];
    _imgV.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_imgV];
    
    rect = CGRectMake(GetView_MaxX(_imgV) + 10, 16, kScreenWidth - (GetView_MaxX(_imgV) + 10) , 22);
    _desLab = [[UILabel alloc] initWithFrame:rect];
    _desLab.font = PF_MEDI(13);
    _desLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    _desLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_desLab];
    
    rect = CGRectMake(kScreenWidth - (17 + 15), 18.5, 17, 17);
    _markImgV = [[UIImageView alloc] initWithFrame:rect];
    _markImgV.dk_imagePicker = DKImageNames(@"common_cell_choose", @"common_cell_choose_D");
    [self.contentView addSubview:_markImgV];
}

- (void)setIncashMode:(IXIncashMode)incashMode
{
    _incashMode = incashMode;
    switch (incashMode) {
        case IXIncashModeCreditCard:
            _imgV.image = [UIImage imageNamed:@"cardPay"];
            _desLab.text = LocalizedString(@"银行卡支付");
            break;
        default:
            break;
    }
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    _markImgV.hidden = !isSelected;
}

@end
