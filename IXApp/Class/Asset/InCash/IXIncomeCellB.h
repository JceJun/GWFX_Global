//
//  IXIncomeCellB.h
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXCommonBaseCell.h"
@class IXUserBankM;

@interface IXIncomeCellB : IXCommonBaseCell

@property (nonatomic, strong) IXUserBankM   * bankM;
@property (nonatomic, assign) BOOL  isSelected;

@end
