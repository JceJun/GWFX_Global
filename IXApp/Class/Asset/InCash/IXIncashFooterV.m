//
//  IXIncashFooterV.m
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashFooterV.h"

@implementation IXIncashFooterV

- (instancetype)initWithFrame:(CGRect)frame target:(id)target action:(SEL)func
{
    if (self = [super initWithFrame:frame]) {
        self.dk_backgroundColorPicker = DKViewColor;
        
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        
        UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
        dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
        
        UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.frame = CGRectMake(15.5, frame.size.height - 67.5, kScreenWidth - 31, 44);
        nextBtn.titleLabel.font = PF_REGU(15);
        [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
        [nextBtn addTarget:target action:func forControlEvents:UIControlEventTouchUpInside];
        [nextBtn setTitle:LocalizedString(@"下一步") forState:UIControlStateNormal];
        [self addSubview:nextBtn];
    }
    return self;
}

@end
