//
//  IXIncashAddCardVC.h
//  IXApp
//
//  Created by Seven on 2017/9/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXIncomeModel;

@interface IXIncashAddCardVC : IXDataBaseVC

//@property (nonatomic, strong)IXIncomeModel  * incashModel;
@property (nonatomic, copy) void(^addBankBlock)(NSString *bankNum); //银行卡号回调

@end
