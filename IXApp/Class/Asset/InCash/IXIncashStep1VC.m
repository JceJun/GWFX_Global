//
//  IXIncashStep1VC.m
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashStep1VC.h"
#import "IXIncashStep2VC.h"
#import "IXIncashAddCardVC.h"
#import "IXIncashCardVC.h"
#import "IXSupplementAccountVC.h"

#import "IXIncashHeaderV.h"
#import "IXIncashFooterV.h"
#import "IXIncashCellA.h"
#import "IXIncomeCellB.h"
#import "IXTraceWebV.h"

#import "IXBORequestMgr+Asset.h"
#import "IXDBAccountGroupMgr.h"
#import "IXBORequestMgr.h"
#import "IXBOModel.h"
#import "NSObject+IX.h"
#import "IXAccountGroupModel.h"

#import "IXIncomeModel.h"
#import "IXCpyConfig.h"
#import <UMMobClick/MobClick.h>
#import "IXDPSAmountVC.h"
#import "IXAddHelp2PayBankVC.h"
#import "IXIncomeModel.h"
#import "IXBORequestMgr.h"

#define kCacheBank  @"cache_bank_account_num"

@interface IXIncashStep1VC ()
<
UITableViewDelegate,
UITableViewDataSource
>


@property (nonatomic, strong) UITableView       * tableV;
@property (nonatomic, strong) IXTraceWebV       * traceWebV;

@property (nonatomic, strong) NSMutableArray    * bankList;     //银行卡列表
//@property (nonatomic, strong) NSMutableArray    * paymentArr;   //支付方式
@property (nonatomic, strong) NSIndexPath       * seletedIndexPath;

@property (nonatomic, copy) NSString    * defaultBankNum;



@end

@implementation IXIncashStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;

    if (UMAppKey.length) {
        [MobClick event:UM_depositbankchoose];
    }
    
    self.title = LocalizedString(@"存入资金");
    [self addBackItem];
    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
    
    [self.view addSubview:self.tableV];
    
    _defaultBankNum = [[NSUserDefaults standardUserDefaults] stringForKey:kCacheBank];
    [self loadTraceWebView];
}

- (void)loadTraceWebView
{
    //嵌套webview地址
    if (!_traceWebV) {
        _traceWebV = [IXTraceWebV initWithWebUrlType:TraceWebUrlTypeTouchIncash];
        [self.view addSubview:_traceWebV];
    } else {
        [_traceWebV refreshWebUrlType:TraceWebUrlTypeTouchIncash];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self request];
}





#pragma mark -
#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _bankList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 54.f;
    }
    
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL selected = (indexPath.row == _seletedIndexPath.row && indexPath.section == _seletedIndexPath.section);
    
    IXIncomeCellB *cell = [tableView dequeueReusableCellWithIdentifier:cellB];
    if (!cell) {
        cell = [[IXIncomeCellB alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellB];
    }
    cell.height = 44;
    cell.dk_backgroundColorPicker = DKTableHeaderColor;
    cell.bankM = _bankList[indexPath.row];
    cell.isSelected = selected;
    cell.lineColor = DKLineColor1;
    if (cell.bankM) {
        [cell showBottomLineWithOffsetX:56];
    }
    
    if (selected) {
        IXUserBankM *bankM = _bankList[indexPath.row];
        [IXBORequestMgr shareInstance].paramDic = [NSMutableDictionary dictionary];
        [IXBORequestMgr shareInstance].paramDic[@"bank"] = bankM.bank;
        [IXBORequestMgr shareInstance].paramDic[@"bankAccountNumber"] = bankM.bankAccountNumber;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id item  =_bankList[indexPath.row];
    if ([item isKindOfClass:[NSString class]]) {
        
        [self gotoFirstAddcard];
        
//        [IXBORequestMgr resetAddDepostiBankContainer];
//        IXAddHelp2PayBankVC *vc = [IXAddHelp2PayBankVC new];
//        [self.navigationController pushViewController:vc animated:YES];
    }else{
        _seletedIndexPath = indexPath;
        [tableView reloadData];
    }
}


#pragma mark -
#pragma mark - action

- (void)cancelAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)nextStepAction
{
    if (UMAppKey.length) {
        [MobClick event:UM_depositbankchooseNS];
    }
    
//    IXUserBankM * m = _bankList[_seletedIndexPath.row - 1];
//    _incashModel.bankAccountNumber = m.bankAccountNumber;
//    _incashModel.bank = m.bank;
//    _incashModel.nameCN = m.nameCN;
//    _incashModel.nameEN = m.nameEN;
//    _incashModel.nameTW = m.nameTW;
//    _incashModel.minPayamount = [IXBORequestMgr shareInstance].channelDic[@"minPayamount"];
//    _incashModel.maxPayamount = [IXBORequestMgr shareInstance].channelDic[@"maxPayamount"];
//    _incashModel.payMethod = [IXBORequestMgr shareInstance].channelDic[@"code"];
//    _incashModel.payName = [m localizedName];
//    _incashModel.paymentId = m.code;
    
    IXDPSAmountVC *vc = [IXDPSAmountVC new];
    [self.navigationController pushViewController:vc animated:YES];
    
    [SVProgressHUD dismiss];
    
//    if (SameString(m.cardpayType, @"swift")) {
//        IXUserBankM * m = _bankList[_seletedIndexPath.row - 1];
//        IXIncashCardVC  * vc = [IXIncashCardVC new];
//        vc.incashModel = _incashModel;
//        vc.bankM = m;
//        [self.navigationController pushViewController:vc animated:YES];
//    }else {
//        IXIncashStep2VC * vc = [IXIncashStep2VC new];
//        vc.incashModel = _incashModel;
//        [self.navigationController pushViewController:vc animated:YES];
//    }

    
}


//- (void)request
//{
//    if (_incashModel.bank.length) {
//        [SVProgressHUD show];
//
//        //只有使用银行卡入金才调用此方法
//        [IXBORequestMgr incash_requestBestModeWithBankCode:_incashModel.bank
//                                                  complete:^(IXIncashPayModeM *mode, NSString * errStr, NSString * errCode)
//         {
//             if (!mode || !mode.code) {
//                 if ([errCode isEqualToString:@"0x500155"]) {
//                     [SVProgressHUD showErrorWithStatus:LocalizedString(@"该银行卡不支持入金")];
//                 } else {
//                     [SVProgressHUD showErrorWithStatus:errStr];
//                 }
//             } else {
//                 _incashModel.nameCN = mode.nameCN;
//                 _incashModel.nameEN = mode.nameEN;
//                 _incashModel.nameTW = mode.nameTW;
//                 _incashModel.minPayamount = [NSString stringWithFormat:@"%@",mode.minPayamount];
//                 _incashModel.maxPayamount = [NSString stringWithFormat:@"%@",mode.maxPayamount];
//                 _incashModel.payMethod = mode.code;
//                 _incashModel.payName = [mode localizedName];
//                 _incashModel.paymentId = mode.code;
//
//                 [SVProgressHUD dismiss];
//
//                 if (SameString(mode.cardpayType, @"swift")) {
//                     IXUserBankM * m = _bankList[_seletedIndexPath.row - 1];
//                     IXIncashCardVC  * vc = [IXIncashCardVC new];
//                     vc.incashModel = _incashModel;
//                     vc.bankM = m;
//                     [self.navigationController pushViewController:vc animated:YES];
//                 }else {
//                     IXIncashStep2VC * vc = [IXIncashStep2VC new];
//                     vc.incashModel = _incashModel;
//                     [self.navigationController pushViewController:vc animated:YES];
//                 }
//             }
//         }];
//    }
//}

- (void)request{
    [SVProgressHUD show];
    
    // 更新银行卡信息&文件
    [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString *errStr) {
        if (success) {
            [IXBORequestMgr b_getCustomerFiles:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
                if (success) {
                    [SVProgressHUD dismiss];
                    
                    NSMutableArray * arr = [IXBORequestMgr mergedBankList];
                    if (!arr.count) {
                        [self gotoFirstAddcard];
                    }
                    
                    if (_defaultBankNum.length) {
                        for (IXUserBankM * b in arr) {
                            if ([b.bankAccountNumber isEqualToString:_defaultBankNum]) {
                                b.mark = YES;
                            } else {
                                b.mark = NO;
                            }
                        }
                    } else {
                        IXUserBankM * b = [arr lastObject];
                        b.mark = YES;
                    }
                    
                    if (arr.count && arr.count < 3) {
                        [arr addObject:LocalizedString(@"添加银行卡")];
                    }
                    
                    if (arr.count > _bankList.count || (![arr.lastObject ix_isString] && [_bankList.lastObject ix_isString])) {
                        _bankList = arr;
                        [_tableV reloadData];
                    }
//                    [self dealWithBankList];
                    
                }else{
                    [SVProgressHUD showInfoWithStatus:LocalizedString(@"获取客户文件信息失败")];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }else if (errStr.length){
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
        }
    }];
}


#pragma mark -
#pragma mark - lazy loading

static  NSString    * cellA = @"step1_cell_a";
static  NSString    * cellB = @"step1_cell_b";
- (UITableView *)tableV
{
    if (!_tableV) {
        IXIncashHeaderV * headV = [[IXIncashHeaderV alloc] initWithTitle:LocalizedString(@"选择入金方式")];
        [headV showProgressWithAimStep:1 totalStep:3];
        
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)
                                               style:UITableViewStylePlain];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.tableHeaderView = headV;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        [_tableV registerClass:[IXIncashCellA class] forCellReuseIdentifier:cellA];
        [_tableV registerClass:[IXIncomeCellB class] forCellReuseIdentifier:cellB];
    }
    
    CGFloat height = MAX(100, _tableV.frame.size.height - kHeaderHeight - 44 * _bankList.count - 54 * 3);
    CGRect rect = CGRectMake(0, 0, kScreenWidth, height);
    _tableV.tableFooterView = [[IXIncashFooterV alloc] initWithFrame:rect
                                                              target:self
                                                              action:@selector(nextStepAction)];
    return _tableV;
}

- (void)gotoFirstAddcard{
    //跳转添加银行卡页
    [IXBORequestMgr resetAddDepostiBankContainer];
    [IXBORequestMgr shareInstance].bankAdditionInfo = @{@"origin":@"deposit",
                                                        @"canDeposit":@"1",
                                                        @"canWithdraw":@"1",
                                                        @"effectType":@"deposit",
                                                        @"gateway_name":@"help2pay",
                                                        @"gateway_code":@"help2pay",
                                                        };
    IXIncashAddCardVC   * vc = [IXIncashAddCardVC new];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)dealloc{
    [[IXBORequestMgr shareInstance].paramDic removeAllObjects];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
