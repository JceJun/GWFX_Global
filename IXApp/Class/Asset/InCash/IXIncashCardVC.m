//
//  IXIncashCardVC.m
//  IXApp
//
//  Created by Seven on 2017/9/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashCardVC.h"
#import "IXIncashResultVC.h"

#import "IXIncashHeaderV.h"
#import "IXTextFieldCell.h"
#import "IXVerifyCodeV.h"

#import "IXBORequestMgr+Asset.h"
#import "IXDBAccountGroupMgr.h"
#import "IXIncomeValidateModel.h"
#import "IXBORequestMgr.h"
#import "IXIncomeModel.h"
#import "IXUserInfoM.h"
#import "IXCpyConfig.h"
#import "IXBOModel.h"
#import "IXAppUtil.h"
#import "IXAccountGroupModel.h"

#import "UINavigationController+FDFullscreenPopGesture.h"

@interface IXIncashCardVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) NSArray       * titleArr;
@property (nonatomic, strong) UITextField   * phoneTf;
@property (nonatomic, strong) UITextField   * amountTf;
@property (nonatomic, strong) IXVerifyCodeV * codeV;    //验证码视图

@property (nonatomic, strong) IXIncashResendMsmM    * applyResult;
@property (nonatomic, strong) NSString      * currency;

@end

@implementation IXIncashCardVC

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (_codeV.superview) {
        [_codeV dismiss];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fd_interactivePopDisabled = YES;   //禁用滑动返回手势
    self.title = LocalizedString(@"存入资金");
    [self addBackItem];
    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
    
    [self.view addSubview:self.tableV];
    [self.tableV reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(validTextContent)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
}

- (void)validTextContent
{
    NSString *content = _amountTf.text;
    NSRange range = [content rangeOfString:@"."];
    if ( range.location != NSNotFound ) {
        if ( content.length - range.location  > 3 ) {
            _amountTf.text = [_amountTf.text substringToIndex:(range.location + 3)];
        }
    }
}

- (void)cancelAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)hideKeyboard
{
    [_tableV endEditing:YES];
}

- (void)payAction
{
    if (!_phoneTf.text.length) {
        [SVProgressHUD showMessage:LocalizedString(@"请输入银行预留手机号")];
        return;
    }
    
    if (!_amountTf.text.length) {
        [SVProgressHUD showMessage:LocalizedString(@"请输入金额")];
        return;
    }
    
    _incashModel.amount = [_amountTf.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    if ([_incashModel.amount floatValue] < [_incashModel.minPayamount floatValue]) {
        [SVProgressHUD showMessage:LocalizedString(@"金额小于最小入金金额")];
        return;
    }
    if ([_incashModel.amount floatValue] > [_incashModel.maxPayamount floatValue]) {
        [SVProgressHUD showMessage:LocalizedString(@"金额大于最大入金金额")];
        return;
    }
    
    [self startPayment];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXTextFieldCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    if (!cell) {
        cell = [[IXTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
    }
    cell.dk_backgroundColorPicker = DKNavBarColor;
    [self configCell:cell indexPath:indexPath];
    
    return cell;
}

- (void)configCell:(IXTextFieldCell *)cell indexPath:(NSIndexPath *)indexPath
{
    NSString    * title = self.titleArr[indexPath.row];
    cell.titleLab.text = title;
    cell.textF.userInteractionEnabled = YES;
    cell.textF.dk_textColorPicker = DKCellTitleColor;
    [cell showTopLineWithOffsetX:0];
    [cell hideBottomLine];
    
    if (SameString(title, LocalizedString(@"姓名"))) {
        cell.textF.userInteractionEnabled = NO;
        cell.textF.dk_textColorPicker = DKCellContentColor;
        cell.textF.text = [IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName;
    }
    else if (SameString(title, LocalizedString(@"身份证"))) {
        NSString    * idStr = [IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber;
        if (idStr.length > 10) {
            idStr = [NSString stringWithFormat:@"%@********%@",
                     [idStr substringToIndex:8],
                     [idStr substringFromIndex:idStr.length - 2]];
        }
        cell.textF.userInteractionEnabled = NO;
        cell.textF.dk_textColorPicker = DKCellContentColor;
        cell.textF.text = idStr;
        cell.textF.font = RO_REGU(15);
    }
    else if (SameString(title, LocalizedString(@"手机"))) {
        cell.textF.keyboardType = UIKeyboardTypeNumberPad;
        cell.textF.placeholder = LocalizedString(@"请输入银行预留手机号");
        cell.textF.delegate = self;
        cell.textF.font = RO_REGU(15);
        _phoneTf = cell.textF;
    }
    else if (SameString(title, LocalizedString(@"银行卡"))) {
        NSString    * num = _incashModel.bankAccountNumber;
        NSString    * subNum = num.length > 4 ? [num substringFromIndex:num.length - 4] : num;
        cell.textF.text = [NSString stringWithFormat:@"**** **** **** %@",subNum];
        cell.textF.dk_textColorPicker = DKCellContentColor;
        cell.textF.userInteractionEnabled = NO;
        cell.textF.font = RO_REGU(15);
    }
    else if (SameString(title, LocalizedString(@"金额"))) {
        [cell showBottomLineWithOffsetX:0];
        cell.textF.keyboardType = UIKeyboardTypeDecimalPad;
        cell.textF.delegate = self;
        cell.textF.font = RO_REGU(15);
        _amountTf = cell.textF;
        
        NSString    * placeHolder = LocalizedString(@"请输入金额");
        if (_incashModel.maxPayamount.length) {
            NSString * subStr = @"";
            if ([_incashModel.minPayamount integerValue] > 0) {
                subStr = [NSString stringWithFormat:@"%@-%@",
                          [IXAppUtil amountToString:_incashModel.minPayamount],
                          [IXAppUtil amountToString:_incashModel.maxPayamount]];
            } else {
                subStr = [IXAppUtil amountToString:_incashModel.maxPayamount];
            }
            placeHolder = [NSString stringWithFormat:@"%@%@",
                           LocalizedString(@"限额"),
                           subStr];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (string.length && [@"0123456789." rangeOfString:string].location == NSNotFound) {
        return NO;
    }
    
    if ([textField isEqual:_phoneTf]) {
        return YES;
    }
    
    if ([textField.text isEqualToString:@"0"] && [string isEqualToString:@"0"]){
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];;
    if ([string isEqualToString:@"."]) {
        if (![textField.text containsString:@"."]) {
            textField.text = aimStr;
        }
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:_phoneTf]) {
        return;
    }
    double amount = [IXAppUtil stringToAmount:textField.text];
    
    //添加逗号
    if (amount > 0) {
        textField.text = [IXAppUtil amountToString:@(amount)];
    }
}


#pragma mark -
#pragma mark - notify
- (void)keyboardWillShow:(NSNotification *)notify
{
    if (!_amountTf.isFirstResponder) {
         [_tableV setContentOffset:CGPointMake(0, 0) animated:YES];
        return;
    }
    
    NSDictionary * dic = notify.userInfo;
    CGRect  rect = [dic[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    CGRect  frame = [_amountTf convertRect:_amountTf.frame toView:[UIApplication sharedApplication].keyWindow];
    
    CGFloat offset = kScreenHeight - CGRectGetMaxY(frame) - rect.size.height - 20;
    if (offset <= 0) {
        [_tableV setContentOffset:CGPointMake(0, - offset) animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)notify
{
    if (_tableV.contentOffset.y > 0) {
        [_tableV setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}


#pragma mark -
#pragma mark - lazy laoding

static  NSString    * cellIdent = @"card_pay_cell";
- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)
                                               style:UITableViewStylePlain];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        [_tableV registerClass:[IXTextFieldCell class] forCellReuseIdentifier:cellIdent];
        
        _tableV.tableHeaderView = [self headerV];
        _tableV.tableFooterView = [self footerV];
    }
    return _tableV;
}


- (UIView *)headerV
{
    NSDictionary * dic = [IXAccountGroupModel shareInstance].accGroupDic;
    _currency = @"";
    if (dic[@"currency"]) {
        _currency = dic[@"currency"];
    }
    
    NSString * title = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"入金金额"),_currency];
    IXIncashHeaderV * headV = [[IXIncashHeaderV alloc] initWithTitle:title];
    [headV showProgressWithAimStep:2 totalStep:3];
    
    UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(hideKeyboard)];
    [headV addGestureRecognizer:tap];
    
    return headV;
}

- (UIView *)footerV
{
    UIView  * footerV = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                 kScreenWidth,
                                                                 kScreenHeight -
                                                                 kNavbarHeight -
                                                                 kHeaderHeight -
                                                                 44 * _titleArr.count)];
    footerV.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UIImage * image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, 40, kScreenWidth - 31, 44);
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setTitle:LocalizedString(@"前往支付") forState:UIControlStateNormal];
    [footerV addSubview:nextBtn];
    
    UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(hideKeyboard)];
    [footerV addGestureRecognizer:tap];
    
    return footerV;
}

- (NSArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = @[
                      LocalizedString(@"姓名"),
                      LocalizedString(@"身份证"),
                      LocalizedString(@"手机"),
                      LocalizedString(@"银行卡"),
                      LocalizedString(@"金额"),
                      ];
    }
    return _titleArr;
}

- (IXVerifyCodeV *)codeV
{
    if (!_codeV) {
        _codeV = [IXVerifyCodeV codeVWithPhoneNum:_phoneTf.text frame:self.view.bounds];
        
        weakself;
        _codeV.resendBlock = ^{
            [weakSelf resendVerifyCide];
        };
        _codeV.confirmBlock = ^(NSString *verifyCode) {
            [weakSelf confirmPayment:verifyCode];
        };
    }
    return _codeV;
}

#pragma mark -
#pragma mark - request

- (void)startPayment
{
    [SVProgressHUD show];
    [self applyPayment];
}

- (void)applyPayment
{
    IXIncashApplyM  * m = [IXIncashApplyM new];
    
    m.accountNo = [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo;
    m.depositCurrency = _incashModel.depositCurrency;
    m.bank = _bankM.bank;
    m.bankAccountNumber = _bankM.bankAccountNumber;
    m.phoneNumber = [_phoneTf.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    m.chineseName = [IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName;
    m.idNumber = [IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber;
    m.payMethod = _incashModel.payMethod;
    m.amount = [_amountTf.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//    m.pno = _incashModel.pNo;    //验证存款时获得的提案号
    
    [IXBORequestMgr incash_requestApplyPayment:m complete:^(IXIncashResendMsmM *applyResult, NSString *errStr) {
        if (errStr.length) {
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD dismiss];
            _applyResult = applyResult;
            [self.codeV showInVC:self];
        }
    }];
}

- (void)resendVerifyCide
{
    [IXBORequestMgr incash_requestNewVerifyCode:_applyResult complete:^(BOOL success, NSString *errStr) {
        if (errStr.length) {
            [SVProgressHUD showErrorWithStatus:errStr];
            [_codeV resendFailure];
        } else {
            [_codeV resendSuccess];
        }
    }];
}

- (void)confirmPayment:(NSString *)verifyCode
{
    [SVProgressHUD show];
    
    IXIncashConfirmPayM * m = [IXIncashConfirmPayM new];
    m.smsCode = verifyCode;
    m.merchantId = _applyResult.merchantId;
    m.encryptedText = _applyResult.encryptedText;
    
    [IXBORequestMgr incash_requestConfirmPayment:m complete:^(BOOL paymentSuc, NSString *errStr, NSString * errCode) {
        if (paymentSuc) {
            [SVProgressHUD showMessage:LocalizedString(@"入金成功")];
            [_codeV dismiss];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        } else {
            //验证码错误无须隐藏验证码输入框
            if (SameString(errCode, @"207261") ||
                SameString(errCode, @"207262") ||
                SameString(errCode, @"207263") ||
                SameString(errCode, @"207264")) {
                //验证码有误
            } else if (SameString(errCode, @"207017")) {
                //短信格式错误
                errStr = LocalizedString(@"验证码格式错误");
            } else {
                if (SameString(errCode, @"207135")) {
                    //短信验证码无效
                } else if (SameString(errCode, @"207005")) {
                    //errStr = LocalizedString(@"金额格式错误");
                } else if (SameString(errCode, @"207302")) {
//                    errStr = LocalizedString(@"手机号格式错误");
                }
                [_codeV dismiss];
            }
            [SVProgressHUD showErrorWithStatus:errStr];
            NSLog(@" -- %@ -- %@ --",errCode,errStr);
        }
    }];
}

@end
