//
//  IXIncashHeaderV.m
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashHeaderV.h"
#import "IXAcntProgressV.h"

@interface IXIncashHeaderV ()




@end

@implementation IXIncashHeaderV

- (instancetype)initWithTitle:(NSString *)title
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, kScreenWidth, kHeaderHeight);
        self.dk_backgroundColorPicker = DKTableHeaderColor;
        
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, (kHeaderHeight - 17)/2, kScreenWidth, 17.0)];
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.dk_textColorPicker = DKCellTitleColor;
        _titleLab.font = PF_MEDI(15);
        _titleLab.text = title;
        [self addSubview:_titleLab];
        
        _progressV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
        [self addSubview:_progressV];
    }
        
    return self;
}

- (void)showProgressWithAimStep:(NSInteger)aimStep totalStep:(NSInteger)total
{
    if (aimStep > total) {
        ELog(@"进度条步骤出错");
        return;
    }
    [_progressV showFromMole:aimStep - 1 toMole:aimStep deno:total];
}

@end
