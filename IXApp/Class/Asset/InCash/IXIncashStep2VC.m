//
//  IXIncashStep2VC.m
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashStep2VC.h"
#import "IXIncashResultVC.h"

#import "IXIncashHeaderV.h"
#import "IXTextField.h"

#import "IXBORequestMgr+Asset.h"
#import "IXDBAccountGroupMgr.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXAccountGroupModel.h"
#import <UMMobClick/MobClick.h>
#import "UINavigationController+FDFullscreenPopGesture.h"

@interface IXIncashStep2VC ()
<
UITextFieldDelegate
>

@property (nonatomic, strong) IXBaseUrlM    * baseUrl;
@property (nonatomic, strong) NSString      * incashToken;
@property (nonatomic, strong) IXTextField   * amountTf; //输入框
@property (nonatomic, strong) UILabel       * limitLab; //限额
@property (nonatomic, strong) UIButton      * checkBtn; //免责信息确认按钮
@property (nonatomic, strong) UIButton      * incashBtn;

@property (nonatomic, assign) CGFloat       * maxAmount;    //入金限额
@property (nonatomic, strong) NSString      * currency;

@end

@implementation IXIncashStep2VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UMAppKey.length) {
        [MobClick event:UM_deposit2];
    }
    
    
    self.fd_interactivePopDisabled = YES;   //禁用滑动返回手势
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    self.title = LocalizedString(@"存入资金");
    [self addBackItem];
    [self addDismissItemWithTitle:LocalizedString(@"取消")];
    
    [self createSubview];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(validTextContent)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)validTextContent
{
    NSString *content = _amountTf.text;
    NSRange range = [content rangeOfString:@"."];
    if ( range.location != NSNotFound ) {
        if ( content.length - range.location  > 3 ) {
            _amountTf.text = [_amountTf.text substringToIndex:(range.location + 3)];
        }
    }
}



- (void)createSubview
{
    UIScrollView    * scrollV = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollV.contentSize = CGSizeMake(kScreenWidth, self.view.bounds.size.height+0.5);
    scrollV.dk_backgroundColorPicker = DKNavBarColor;
    scrollV.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    [self.view addSubview:scrollV];
    
    NSDictionary * dic = [IXAccountGroupModel shareInstance].accGroupDic;
    _currency = @"";
    if (dic[@"currency"]) {
        _currency = dic[@"currency"];
    }
    
    NSString * title = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"入金金额"),_currency];
    IXIncashHeaderV * headV = [[IXIncashHeaderV alloc] initWithTitle:title];
    [headV showProgressWithAimStep:2 totalStep:3];
    [scrollV addSubview:headV];
    
    UIView  * topLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headV.frame), kScreenWidth, kLineHeight)];
    topLine.dk_backgroundColorPicker = DKLineColor;
    [scrollV addSubview:topLine];
    
    UIView  * btomV = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                               CGRectGetMaxY(topLine.frame),
                                                               kScreenWidth,
                                                               kScreenHeight -
                                                               CGRectGetMaxY(topLine.frame) -
                                                               kNavbarHeight)];
    btomV.dk_backgroundColorPicker = DKNavBarColor;
    [scrollV addSubview:btomV];
    
    title = LocalizedString(@"金额");
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(15)}];
    UILabel * lab = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, size.width, 20)];
    lab.dk_textColorPicker = DKCellTitleColor;
    lab.text = LocalizedString(@"金额");
    lab.font = PF_MEDI(15);
    [btomV addSubview:lab];
    
    _amountTf = [[IXTextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lab.frame)+ 15, 20, kScreenWidth - 70, 30)];
    _amountTf.keyboardType = UIKeyboardTypeDecimalPad;
    _amountTf.dk_textColorPicker = DKCellTitleColor;
    _amountTf.textFont = RO_REGU(15);
    _amountTf.placeHolderFont = PF_MEDI(15);
    _amountTf.delegate = self;
    _amountTf.placeholder = LocalizedString(@"请输入金额");
    [btomV addSubview:_amountTf];
    
    UIView  * centerLine = [[UIView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(lab.frame) + 25, kScreenWidth, kLineHeight)];
    centerLine.dk_backgroundColorPicker = DKLineColor;
    [btomV addSubview:centerLine];
    
    //限额提示
    title = LocalizedString(@"限额");
    size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    UILabel * alertL = [[UILabel alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(centerLine.frame)+14, size.width, size.height)];
    alertL.font = PF_MEDI(13);
    alertL.text = title;
    alertL.dk_textColorPicker = DKCellContentColor;
    [btomV addSubview:alertL];
    
    _limitLab = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(alertL.frame)+6,
                                                          CGRectGetMinY(alertL.frame),
                                                          kScreenWidth - 100, size.height)];
    _limitLab.font = RO_REGU(13);
    _limitLab.dk_textColorPicker = DKCellContentColor;
    [btomV addSubview:_limitLab];
    
    NSString    * limitStr = [NSString stringWithFormat:@"%@",[IXAppUtil amountToString:_incashModel.maxPayamount]];
    if ([_incashModel.minPayamount integerValue] > 0) {
        limitStr = [NSString stringWithFormat:@"%@-%@",_incashModel.minPayamount,[IXAppUtil amountToString:_incashModel.maxPayamount]];
    }
    _limitLab.text = limitStr;
    
    UIView  * btomLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(alertL.frame) + 14, kScreenWidth, kLineHeight)];
    btomLine.dk_backgroundColorPicker = DKLineColor;
    [btomV addSubview:btomLine];
    
    /*
    //免责提示
    UILabel * msgLab = [[UILabel alloc] initWithFrame:CGRectMake(15, CGRectGetMinY(btomLine.frame) + 16, kScreenWidth - 15, size.height)];
    msgLab.text = LocalizedString(@"我确认存入资金，为本人在智得账户的交易用途");
    msgLab.font = PF_MEDI(12);
    msgLab.dk_textColorPicker = DKCellContentColor;
    [btomV addSubview:msgLab];
    
    _checkBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(msgLab.frame) - 12, kScreenWidth, size.height+24)];
    _checkBtn.selected = YES;
    [_checkBtn setImage:AutoNightImageNamed(@"common_cell_unchoose") forState:UIControlStateNormal];
    [_checkBtn setImage:AutoNightImageNamed(@"common_cell_choose") forState:UIControlStateSelected];
    [_checkBtn setImageEdgeInsets:UIEdgeInsetsMake(10, kScreenWidth-size.height-18, 10, 14)];
    [_checkBtn addTarget:self action:@selector(checkAction) forControlEvents:UIControlEventTouchUpInside];
    [btomV addSubview:_checkBtn];
    */
    
    //支付按钮
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    _incashBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _incashBtn.frame = CGRectMake(15.5, CGRectGetMaxY(btomLine.frame) + 40, kScreenWidth - 31, 44);
    _incashBtn.titleLabel.font = PF_REGU(15);
    [_incashBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [_incashBtn addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
    [_incashBtn setTitle:LocalizedString(@"前往支付") forState:UIControlStateNormal];
    [btomV addSubview:_incashBtn];
    
    UITapGestureRecognizer  * tap0 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard)];
    [btomV addGestureRecognizer:tap0];
    
    UITapGestureRecognizer  * tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard)];
    [self.view addGestureRecognizer:tap1];
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (string.length && [@"0123456789." rangeOfString:string].location == NSNotFound) {
        return NO;
    }
 
    if ([textField.text isEqualToString:@"0"] && [string isEqualToString:@"0"]){
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([string isEqualToString:@"."]) {
        if (![textField.text containsString:@"."]) {
            textField.text = aimStr;
        }
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    double amount = [IXAppUtil stringToAmount:textField.text];
    
    //添加逗号
    if (amount > 0) {
        textField.text = [IXAppUtil amountToString:@(amount)];
    }
}

- (void)resignKeyboard
{
    [_amountTf resignFirstResponder];
}

- (void)checkAction
{
    _checkBtn.selected = !_checkBtn.selected;
    _incashBtn.enabled = _checkBtn.selected;
}

- (void)payAction
{
    if (!_amountTf.text.length) {
        [SVProgressHUD showMessage:LocalizedString(@"请输入金额")];
        return;
    }
    
    _incashModel.amount = [_amountTf.text stringByReplacingOccurrencesOfString:@"," withString:@""];

    if ([_incashModel.amount floatValue] < [_incashModel.minPayamount floatValue]) {
        [SVProgressHUD showMessage:LocalizedString(@"金额小于最小入金金额")];
        return;
    }
    if ([_incashModel.amount floatValue] > [_incashModel.maxPayamount floatValue]) {
        [SVProgressHUD showMessage:LocalizedString(@"金额大于最大入金金额")];
        return;
    }
    
    [self request_1_getConfigData];
}

#pragma mark -
#pragma mark - request

- (void)request_1_getConfigData
{
    dispatch_group_t    group = dispatch_group_create();
    
    dispatch_group_enter(group);
    //获取baseUrl
    [IXBORequestMgr incash_requstInCashBaseUrl:^(IXBaseUrlM *urlM) {
        _baseUrl = urlM;
        dispatch_group_leave(group);
    }];
    
    dispatch_group_enter(group);
    [IXBORequestMgr incash_requestToken:^(BOOL success, NSString *token) {
        if (success) {
            _incashToken = token;
        }
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (_baseUrl && _incashToken) {
            [self request_2_validateUrl];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取配置数据失败")];
        }
    });
}

- (void)request_2_validateUrl
{
    NSDictionary *paramDic = @{
                               @"depositCurrency":_incashModel.depositCurrency,
                               @"bank":_incashModel.bank,
                               @"bankAccountNumber":_incashModel.bankAccountNumber,
                               @"payMethod":_incashModel.payMethod,
                               @"amount":_incashModel.amount,
                               @"depositValidateUrl":_baseUrl.depositValidateUrl,
                               };
    
    [IXBORequestMgr incash_verifyPaymethodWith:paramDic complete:^(BOOL success, NSString *token) {
        if (success) {
            _incashModel.pNo = token;
            
            IXIncashResultVC * vc = [[IXIncashResultVC alloc] init];
            vc.baseUrl = _baseUrl;
            vc.incomeModel = _incashModel;
            [self.navigationController pushViewController:vc animated:YES];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self request_3_submitUrl];
//                [self request_4_submitUrl];
            });
        }else{
            [SVProgressHUD showInfoWithStatus:token];
        }
    }];
}

- (void)request_3_submitUrl
{
    NSString    * urlStr = _baseUrl.depositSubmitUrl;
    urlStr = [urlStr stringByAppendingString:@"/rgsLogin.rgs?"];
    urlStr = [urlStr stringByAppendingFormat:@"platform=IX&pageType=normal&companyId=%ld&token=%@&service=",
              (long)CompanyID,_incashToken];
    
    NSString    * serverStr = _baseUrl.depositSubmitUrl;
    serverStr = [serverStr stringByAppendingString:@"/appFundDepositConfirm.do?"];
    
    NSString *payMethod = [IXBORequestMgr shareInstance].channelDic[@"code"];
    if (!payMethod.length) {
        payMethod = _incashModel.payMethod;
    }
    
    NSString *currency = _incashModel.depositCurrency;
    serverStr = [serverStr stringByAppendingFormat:@"payType=mobileEgpay&platform=IX&fee=0&accountNo=%@&depositCurrency=%@&bank=%@&bankAccountNumber=%@&payMethod=%@&amount=%@&remark=iosPay&pno=%@&lang=%@",
                 [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                 currency,
                 _incashModel.bank,
                 _incashModel.bankAccountNumber,
                 payMethod,
                 _incashModel.amount,
                 _incashModel.pNo,
                 [IXLocalizationModel currentCheckLanguage]];
    
    serverStr = [NSString formatterUTF8:serverStr];
    urlStr = [urlStr stringByAppendingString:serverStr];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}



@end
