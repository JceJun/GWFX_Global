//
//  IXIncashHeaderV.h
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXAcntProgressV.h"
#define kHeaderHeight 160

@interface IXIncashHeaderV : UIView
@property (nonatomic, strong) UILabel   * titleLab;
@property (nonatomic, strong) IXAcntProgressV   * progressV;
- (instancetype)initWithTitle:(NSString *)title;

- (void)showProgressWithAimStep:(NSInteger)aimStep totalStep:(NSInteger)total;

@end
