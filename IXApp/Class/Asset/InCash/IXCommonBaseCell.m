//
//  IXCommonBaseCell.m
//  FXApp
//
//  Created by Seven on 2017/7/18.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "IXCommonBaseCell.h"

@interface IXCommonBaseCell ()

@property (nonatomic, strong) UIView    * topLine;
@property (nonatomic, strong) UIView    * bottomLine;

@end

@implementation IXCommonBaseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _height = 44;
        [self addLine];
        self.dk_backgroundColorPicker = DKViewColor;
    }
    return self;
}

- (void)addLine
{
    _topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
    _topLine.dk_backgroundColorPicker = DKLineColor;
    _topLine.hidden = YES;
    [self.contentView addSubview:_topLine];
    
    _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, _height - kLineHeight, kScreenWidth, kLineHeight)];
    _bottomLine.dk_backgroundColorPicker = DKLineColor;
    _bottomLine.hidden = YES;
    [self.contentView addSubview:_bottomLine];
}

#pragma mark -
#pragma mark - public method

- (void)showTopLineWithOffsetX:(CGFloat)x
{
    _topLine.hidden = NO;
    _topLine.frame = CGRectMake(x, 0, kScreenWidth - x, kLineHeight);
}

- (void)showBottomLineWithOffsetX:(CGFloat)x
{
    _bottomLine.hidden = NO;
    _bottomLine.frame = CGRectMake(x, _height - kLineHeight, kScreenWidth - x, kLineHeight);
}

- (void)hideTopLine
{
    _topLine.hidden = YES;
}

- (void)hideBottomLine
{
    _bottomLine.hidden = YES;
}

- (void)setLineColor:(DKColorPicker)lineColor
{
    _topLine.dk_backgroundColorPicker = lineColor;
    _bottomLine.dk_backgroundColorPicker = lineColor;
}

@end
