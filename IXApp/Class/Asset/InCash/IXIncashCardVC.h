//
//  IXIncashCardVC.h
//  IXApp
//
//  Created by Seven on 2017/9/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXUserBankM;
@class IXIncomeModel;

/** 银行卡入金 */
@interface IXIncashCardVC : IXDataBaseVC

@property (nonatomic, strong) IXUserBankM   * bankM;
@property (nonatomic, strong) IXIncomeModel * incashModel;

@end
