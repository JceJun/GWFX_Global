//
//  IXVerifyCodeV.m
//  IXApp
//
//  Created by Seven on 2017/9/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXVerifyCodeV.h"
#import "IXTextField.h"

#define kWidhth 295
#define kHeight 290

static  IXVerifyCodeV   * verifyCodeV;

@interface IXVerifyCodeV ()

@property (nonatomic, strong) UIView    * mainV;
@property (nonatomic, copy) NSString    * phoneNum;
@property (nonatomic, strong) UITextField   * codeTf;   //验证码输入框
@property (nonatomic, strong) UIButton  * resendBtn;    //重新发送按钮
@property (nonatomic, strong) NSTimer   * timer;
@property (nonatomic, strong) UIActivityIndicatorView   * loadingV;
@property (nonatomic, assign) NSInteger timeLeft;       //剩余时间

@end

@implementation IXVerifyCodeV

- (void)dealloc
{
    NSLog(@" -- %s --",__func__);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)codeVWithPhoneNum:(NSString *)phoneNum frame:(CGRect)rect
{
    return [[IXVerifyCodeV alloc] initWithPhoneNum:phoneNum frame:rect];
}

- (instancetype)initWithPhoneNum:(NSString *)phoneNum  frame:(CGRect)rect
{
    if (self = [super init]) {
        self.frame = rect;
        self.dk_backgroundColorPicker = DKNavBarColor;
        
        if (phoneNum.length > 6) {
            phoneNum = [NSString stringWithFormat:@"%@*****%@",
                        [phoneNum substringToIndex:3],
                        [phoneNum substringFromIndex:phoneNum.length - 3]];
        }
        _phoneNum = phoneNum;
        _timeLeft = 60;
        
        [self createSubview];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    
    return self;
}

- (void)createSubview
{
    UIButton    * resignKeyboardBtn = [[UIButton alloc] initWithFrame:self.bounds];
    [resignKeyboardBtn addTarget:self action:@selector(resignKeyboard) forControlEvents:UIControlEventTouchUpInside];
    resignKeyboardBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:resignKeyboardBtn];
    
    _mainV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWidhth, kHeight)];
    _mainV.dk_backgroundColorPicker = DKNavBarColor;
    _mainV.layer.cornerRadius = 3.f;
    _mainV.clipsToBounds = YES;
    _mainV.center = self.center;
    [self addSubview:_mainV];
    
    UIView  * line = [[UIView alloc] initWithFrame:CGRectMake(0, 57, kWidhth, kLineHeight)];
    line.dk_backgroundColorPicker = DKLineColor;
    [_mainV addSubview:line];
    
    UILabel * titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 16, kWidhth, 25)];
    titleLab.font = PF_MEDI(15);
    titleLab.dk_textColorPicker = DKCellTitleColor;
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.text = LocalizedString(@"验证手机号");
    [_mainV addSubview:titleLab];
    
    UIButton    * dismissBtn = [[UIButton alloc] initWithFrame:CGRectMake(kWidhth - 40, 15, 27, 27)];
    [dismissBtn setImage:AutoNightImageNamed(@"common_removeView") forState:UIControlStateNormal];
    [dismissBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [_mainV addSubview:dismissBtn];
    
    UILabel * alertLab = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(line.frame) + 20, kWidhth, 13)];
    alertLab.dk_textColorPicker = DKCellContentColor;
    alertLab.text = LocalizedString(@"已发送短信验证码到手机号");
    alertLab.textAlignment = NSTextAlignmentCenter;
    alertLab.font = PF_MEDI(12);
    [_mainV addSubview:alertLab];
    
    UILabel * numLab = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(alertLab.frame) + 14, kWidhth, 24)];
    numLab.textAlignment = NSTextAlignmentCenter;
    numLab.font = RO_REGU(24);
    numLab.dk_textColorPicker = DKCellTitleColor;
    numLab.text = _phoneNum;
    [_mainV addSubview:numLab];
    
    UIView  * btomV = [[UIView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(numLab.frame) + 15, 160, 44)];
    btomV.layer.dk_borderColorPicker = DKLineColor;
    btomV.layer.borderWidth = 0.5f;
    [_mainV addSubview:btomV];
    
    _codeTf = [[IXTextField alloc] initWithFrame:CGRectMake(21, 7, 140, 30)];
    _codeTf.placeholder = LocalizedString(@"请输入验证码");
    _codeTf.font = RO_REGU(15);
    _codeTf.dk_textColorPicker = DKCellTitleColor;
    [btomV addSubview:_codeTf];
    
    _resendBtn = [[UIButton alloc] initWithFrame:CGRectMake(190, CGRectGetMinY(btomV.frame), 85, 44)];
    _resendBtn.layer.borderWidth = 0.5f;
    _resendBtn.titleLabel.font = PF_REGU(15);
    _resendBtn.layer.dk_borderColorPicker = DKLineColor;
    [_resendBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
    [_resendBtn setTitle:LocalizedString(@"重新发送") forState:UIControlStateNormal];
    [_resendBtn addTarget:self action:@selector(resendAction) forControlEvents:UIControlEventTouchUpInside];
    [_mainV addSubview:_resendBtn];
    
    //确定按钮
    UIImage * image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15, CGRectGetMaxY(btomV.frame) + 20, kWidhth - 30, 44);
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setTitle:LocalizedString(@"确定") forState:UIControlStateNormal];
    [_mainV addSubview:nextBtn];
}

- (void)keyboardWillShow:(NSNotification *)notify
{
    CGRect  rect = [notify.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    if ( self.bounds.size.height - rect.size.height < CGRectGetMaxY(_mainV.frame)) {
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            _mainV.center = CGPointMake(kScreenWidth/2, self.bounds.size.height - rect.size.height - kHeight/2 - 30);
        } completion:nil];
    }
}

- (void)keyboardWillHide:(NSNotification *)notify
{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _mainV.center = self.center;
    } completion:nil];
}

#pragma mark -
#pragma mark - timer

- (void)timerAction
{
        _timeLeft --;
    if (_timeLeft > 0) {
        _resendBtn.enabled = NO;
        [_resendBtn setTitle:[NSString stringWithFormat:@"%@(%lu)",LocalizedString(@"重新发送"),_timeLeft]
                      forState:UIControlStateNormal];
        [_resendBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
        _resendBtn.titleLabel.font = PF_REGU(12);
    }
    else if (_timeLeft <= 0) {
        _resendBtn.enabled = YES;
        [_resendBtn setTitle:LocalizedString(@"重新发送") forState:UIControlStateNormal];
        [_resendBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        _resendBtn.titleLabel.font = PF_REGU(15);
        
        [_timer invalidate];
        _timer = nil;
    }
    
    NSLog(@" ------ %ld -----",_timeLeft);
}

#pragma mark -
#pragma mark - btn action

- (void)resignKeyboard
{
    [_codeTf resignFirstResponder];
}

- (void)resendAction
{
    if (self.resendBlock) {
        self.resendBlock();
    }
    [_resendBtn setTitle:nil forState:UIControlStateNormal];
    _resendBtn.userInteractionEnabled = NO;
    [_resendBtn addSubview:self.loadingV];
    [self.loadingV startAnimating];
}

- (void)confirmAction
{
    if (!_codeTf.text.length) {
        [SVProgressHUD showMessage:LocalizedString(@"请输入验证码")];
        return;
    }
    
    if (self.confirmBlock) {
        self.confirmBlock(_codeTf.text);
    }    
}


#pragma mark -
#pragma mark - public sel

- (void)showInVC:(UIViewController *)vc
{
    _mainV.alpha = 0;
    _codeTf.text = nil;
    self.backgroundColor = [UIColor clearColor];
    
    [vc.view addSubview:self];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        _mainV.alpha = 1;
    } completion:^(BOOL finished) {
        _timeLeft = 60;
        [self.timer fire];
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.backgroundColor = [UIColor clearColor];
        _mainV.alpha = 0;
    } completion:^(BOOL finished) {
        _mainV.center = self.center;
        [self removeFromSuperview];
        
        [_timer invalidate];
        _timer = nil;
    }];
}

- (void)resendSuccess
{
    _timeLeft = 60;
    [self.timer fire];
    
    [self.loadingV stopAnimating];
    _resendBtn.userInteractionEnabled = YES;
}

- (void)resendFailure
{
    [self.loadingV stopAnimating];
    _resendBtn.userInteractionEnabled = YES;
    [_resendBtn setTitle:LocalizedString(@"重新发送") forState:UIControlStateNormal];
}


#pragma mark -
#pragma mark - lazy loading

- (NSTimer *)timer
{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.f
                                                  target:self
                                                selector:@selector(timerAction)
                                                userInfo:nil
                                                 repeats:YES];
    }
    return _timer;
}

- (UIActivityIndicatorView *)loadingV
{
    if (!_loadingV) {
        UIActivityIndicatorViewStyle    style = [IXUserInfoMgr shareInstance].isNightMode? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
        _loadingV = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
        _loadingV.frame = CGRectMake(0, 0, 40, 40);
    }
    _loadingV.center = CGPointMake(_resendBtn.bounds.size.width/2, _resendBtn.bounds.size.height/2);
    return _loadingV;
}

@end
