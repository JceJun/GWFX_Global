//
//  IXIncashAddCardVC.m
//  IXApp
//
//  Created by Seven on 2017/9/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncashAddCardVC.h"
#import "IXDrawBankListVC.h"
#import "IXIncashCardVC.h"
#import "IXIncashStep2VC.h"

#import "IXIncashHeaderV.h"
#import "IXIncashFooterV.h"
#import "IXTextFieldCell.h"

#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Asset.h"
#import "IXDBAccountGroupMgr.h"
#import "IXSupplementBankM.h"
#import "IXBORequestMgr.h"
#import "IXIncomeModel.h"
#import "IXUserInfoM.h"
#import "IXBOModel.h"
#import "IXAccountGroupModel.h"
#import <UMMobClick/MobClick.h>
#import "IXCpyConfig.h"
#import "UIKit+Block.h"

@interface IXIncashAddCardVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) UIButton      * saveCardBtn;
@property (nonatomic, weak) UITextField     * cardAccTf;
@property (nonatomic, weak) UITextField     * nameTf;
@property (nonatomic, strong) IXEnableBankM * selectedBank;
@property (nonatomic, copy)NSString *chineseName;

@end

@implementation IXIncashAddCardVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    if (UMAppKey.length) {
        [MobClick event:UM_depositbankconfirm];
    }
    
    self.title = LocalizedString(@"存入资金");
    [self addBackItem];
    [self addDismissItemWithTitle:LocalizedString(@"取消") target:self action:@selector(cancelAction)];
    _chineseName = [IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName;
    
    [self.view addSubview:self.tableV];
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"Submission" forState:UIControlStateNormal];
    [self.view addSubview:nextBtn];
    weakself;
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.height.equalTo(44);
        make.bottom.equalTo(-30);
    }];
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        [weakSelf nextAction];
    }];
}

- (void)cancelAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)nextAction
{
    
    [self.view endEditing:YES];
    if (!_selectedBank) {
        [SVProgressHUD showMessage:LocalizedString(@"请选择银行")];
        return;
    }
    
    if (!_cardAccTf.text.length) {
        [SVProgressHUD showMessage:LocalizedString(@"请输入银行卡账号")];
        return;
    }
    
//    if (!_chineseName.length) {
//        [SVProgressHUD showMessage:LocalizedString(@"请输入持卡人姓名")];
//        return;
//    }
    
    NSMutableDictionary *bank = [[IXBORequestMgr shareInstance].bank mutableCopy];
    bank[@"bank"] = _selectedBank.code;
    bank[@"bankName"] = _selectedBank.nameEN;
    bank[@"bankAccountNumber"] = _cardAccTf.text;
    bank[@"bankAccountName"] = _chineseName;
    
    [IXBORequestMgr shareInstance].bank = [bank mutableCopy];
    
    [self submitBankInfo];
    
    if (UMAppKey.length) {
        [MobClick event:UM_depositbankconfirmNS];
    }
}

- (void)submitBankInfo{
    NSDictionary *bank = [IXBORequestMgr shareInstance].bank;
    
    [IXBORequestMgr b_appUpdateCustomerBanks:@[bank] rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}



- (void)saveBtnAction
{
    _saveCardBtn.selected = !_saveCardBtn.selected;
}


#pragma mark -

#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXTextFieldCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    
    if (!cell) {
        cell = [[IXTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
    }
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.showNextImgV = NO;
    cell.textF.textAlignment = NSTextAlignmentRight;
    [cell showTopLineWithOffsetX:0];
    
    switch (indexPath.row) {
        case 0:{
            cell.titleLab.text = LocalizedString(@"姓名");
            cell.titleLabWidth = 100;
//            cell.textF.text = [IXDataProcessTools dealWithNil:_chineseName];
//            if (![IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName.length) {
                cell.textF.placeholder = LocalizedString(@"请输入姓名");
                cell.textF.userInteractionEnabled = YES;
                cell.textF.delegate = self;
//            } else {
//                cell.textF.userInteractionEnabled = NO;
//            }
             _nameTf = cell.textF;
            break;
        }
        case 1:{
            cell.titleLab.text = LocalizedString(@"银行");
            if (_selectedBank) {
                cell.textF.text = [_selectedBank localizedName];
            } else {
                cell.textF.placeholder = LocalizedString(@"请选择银行");
            }
            cell.textF.userInteractionEnabled = NO;
            cell.showNextImgV = YES;
            break;
        }
        case 2: {
            cell.titleLab.text = LocalizedString(@"账号.");
            cell.titleLabWidth = 105;
            cell.textF.placeholder = LocalizedString(@"请输入银行卡账号");
            cell.textF.textFont = RO_REGU(15);
            cell.textF.placeHolderFont = PF_MEDI(13);
            cell.textF.keyboardType = UIKeyboardTypeNumberPad;
            [cell showBottomLineWithOffsetX:0];
            _cardAccTf = cell.textF;
            break;
        }
        default:
            break;
    }
    
    return cell;;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        weakself;
        IXDrawBankListVC *bankList = [[IXDrawBankListVC alloc] initWithSelectedInfo:^(IXEnableBankM *bank) {
            weakSelf.selectedBank = bank;
            [weakSelf.tableV reloadData];
        }];
        [self.navigationController pushViewController:bankList animated:YES];
    }
}

#pragma mark -
#pragma mark - lazy loading

static  NSString    * cellIdent = @"add_card_cell";
- (UITableView *)tableV
{
    if (!_tableV) {
        IXIncashHeaderV * headerV = [[IXIncashHeaderV alloc] initWithTitle:LocalizedString(@"请输入银行卡信息")];
        [headerV showProgressWithAimStep:1 totalStep:3];
        
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)
                                               style:UITableViewStylePlain];
        _tableV.tableHeaderView = headerV;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.bounces = NO;
//        _tableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        [_tableV registerClass:[IXTextFieldCell class] forCellReuseIdentifier:cellIdent];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
        gestureRecognizer.numberOfTapsRequired = 1;
        gestureRecognizer.cancelsTouchesInView = NO;
        [_tableV addGestureRecognizer:gestureRecognizer];
    }
    return _tableV;
}

- (void)hideKeyboard{
    [self.view endEditing:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


@end
