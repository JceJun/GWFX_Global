//
//  IXIncashStep2VC.h
//  IXApp
//
//  Created by Seven on 2017/9/12.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXIncomeModel;

@interface IXIncashStep2VC : IXDataBaseVC

@property (nonatomic, strong)IXIncomeModel  * incashModel;

@end
