//
//  IXTextFieldCell.h
//  FXApp
//
//  Created by Seven on 2017/7/18.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXCommonBaseCell.h"
#import "IXTextField.h"

/** 无点击效果带输入框的cell，如需处理请将输入框的代理设置为VC，底部和顶部的分割线默认隐藏 */
@interface IXTextFieldCell : IXCommonBaseCell

@property (nonatomic, strong) UILabel   * titleLab;
@property (nonatomic, strong) IXTextField   * textF;

/** 设置titleLab的宽度 */
@property (nonatomic, assign) CGFloat   titleLabWidth;
/** 显示下一页icon，默认为NO */
@property (nonatomic, assign) BOOL      showNextImgV;

@end
