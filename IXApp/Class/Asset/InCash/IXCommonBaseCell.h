//
//  IXCommonBaseCell.h
//  FXApp
//
//  Created by Seven on 2017/7/18.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kCellHeight 44

@interface IXCommonBaseCell : UITableViewCell

/** cell 高度 */
@property (nonatomic, assign) CGFloat   height;

/** 线条日夜间模式颜色，默认DKLineColor */
@property (nonatomic, strong) DKColorPicker   lineColor;

- (void)showTopLineWithOffsetX:(CGFloat)x ;
- (void)showBottomLineWithOffsetX:(CGFloat)x;
- (void)hideTopLine;
- (void)hideBottomLine;

@end
