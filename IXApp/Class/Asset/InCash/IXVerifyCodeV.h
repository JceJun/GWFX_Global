//
//  IXVerifyCodeV.h
//  IXApp
//
//  Created by Seven on 2017/9/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 手机验证码视图 */
@interface IXVerifyCodeV : UIView

@property (nonatomic, copy) void(^resendBlock)();
@property (nonatomic, copy) void(^confirmBlock)(NSString *verifyCode);

+ (instancetype)codeVWithPhoneNum:(NSString *)phoneNum frame:(CGRect)rect;
- (instancetype)initWithPhoneNum:(NSString *)phoneNum frame:(CGRect)rect;

- (void)showInVC:(UIViewController *)vc;
- (void)dismiss;
- (void)resendSuccess;
- (void)resendFailure;

@end
