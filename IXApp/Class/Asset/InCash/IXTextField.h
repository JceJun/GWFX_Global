//
//  IXTextField.h
//  FXApp
//
//  Created by Seven on 2017/7/26.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXTextField : UITextField

@property (nonatomic, strong) UIFont    * placeHolderFont;

@property (nonatomic, strong) UIFont    * textFont;

@end
