//
//  IXIncomeStep3CellB.m
//  IXApp
//
//  Created by Evn on 17/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeStep3CellB.h"

@interface IXIncomeStep3CellB()

@property (nonatomic,strong)UILabel *title;
@property (nonatomic, strong)UIView *uLineView;
@property (nonatomic, strong)UIView *lineView;

@end

@implementation IXIncomeStep3CellB

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        ;
    }
    return self;
}

- (UILabel *)title
{
    if (!_title) {
        CGRect rect = CGRectMake(14.5, 14, kScreenWidth/2 - 15, 16);
        _title = [[UILabel alloc] initWithFrame:rect];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}


- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(kScreenWidth - (180 + 14.5), 14, 180, 16);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xd4d5dc);
        _desc.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (UIView *)uLineView
{
    if (!_uLineView) {
        CGRect rect = CGRectMake(0, 0, kScreenWidth, kLineHeight);
        _uLineView = [[UIView alloc] initWithFrame:rect];
        _uLineView.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_uLineView];
    }
    
    return _uLineView;
}

- (UIView *)lineView
{
    if (!_lineView) {
        CGRect rect = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
        _lineView = [[UIView alloc] initWithFrame:rect];
        _lineView.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_lineView];
    }
    
    return _lineView;
}



- (void)loadUIWithTitle:(NSString *)title
                   desc:(NSString *)desc
                    tag:(NSUInteger)tag{
    
    if (tag == 0) {
        self.uLineView.dk_backgroundColorPicker = DKLineColor;
    }
    self.title.text = title;
    self.desc.text = desc;
    self.lineView.dk_backgroundColorPicker = DKLineColor;
}

@end
