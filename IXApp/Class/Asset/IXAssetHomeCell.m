//
//  IXAssetHomeCell.m
//  IXApp
//
//  Created by Evn on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAssetHomeCell.h"
#import "UIViewExt.h"
@interface IXAssetHomeCell()

@property (nonatomic, strong) UIImageView *iconImgV;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *contentLbl;
@property (nonatomic, strong) UIView *lineView;
@property(nonatomic,strong) UIView *redView;

@end

@implementation IXAssetHomeCell

- (UIImageView *)iconImgV
{
    if (!_iconImgV) {
        CGRect rect = CGRectMake(20.5, (55 - 30)/2, 30, 30);
        _iconImgV = [[UIImageView alloc] initWithFrame:rect];
        _iconImgV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iconImgV];
    }
    return _iconImgV;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        CGRect rect = CGRectMake(GetView_MaxX(_iconImgV) + 20.5, 6, kScreenWidth - GetView_MaxX(_iconImgV) - 20.5, 21);
        _nameLbl = [[UILabel alloc] initWithFrame:rect];
        _nameLbl.font = PF_MEDI(13);
        _nameLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xffffff);
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        CGRect rect = CGRectMake(VIEW_X(_nameLbl), GetView_MaxY(_nameLbl) , kScreenWidth - GetView_MinX(_nameLbl), 20);
        _contentLbl = [[UILabel alloc] initWithFrame:rect];
        _contentLbl.font = RO_REGU(10);
        _contentLbl.textColor = MarketSymbolCodeColor;
        _contentLbl.dk_textColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
        [self.contentView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 54.5, kScreenWidth, kLineHeight)];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (UIView *)redView
{
    if (!_redView) {
        _redView = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth-30, (55 - 8)/2, 8, 8)];
        _redView.backgroundColor = [UIColor redColor];
        _redView.layer.cornerRadius = 8/2;
        _redView.layer.masksToBounds = YES;
        _redView.hidden = YES;
        [self.contentView addSubview:_redView];
    }
    return _redView;
}

- (void)config:(IXAssetHomeCellM *)model
{
    self.iconImgV.image = GET_IMAGE_NAME(model.imgStr);
    self.nameLbl.text = model.title;
    self.contentLbl.text = model.content;
    self.lineView.dk_backgroundColorPicker = DKLineColor;
    self.dk_backgroundColorPicker = DKNavBarColor;
    if (model.commissionStatus) {
        self.redView.hidden = NO;
        [self.nameLbl sizeToFit];
        self.redView._centerY = self.nameLbl._centerY;
        self.redView._right = self.nameLbl._right + 20;
    }else{
       self.redView.hidden = YES;
    }
}

@end

@implementation IXAssetHomeCellM

- (instancetype)initWithImgStr:(NSString *)imgStr
                         title:(NSString *)title
                       content:(NSString *)content
{
    if (self = [super init]) {
        _imgStr = imgStr;
        _title  = title;
        _content = content;
    }
    return self;
}

@end
