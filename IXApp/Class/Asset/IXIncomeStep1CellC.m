//
//  IXIncomeStep1CellC.m
//  IXApp
//
//  Created by Magee on 2017/4/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeStep1CellC.h"

@interface IXIncomeStep1CellC ()

@property (nonatomic,strong)UIImageView *icon;
@property (nonatomic,strong)UILabel     *desc;
@end

@implementation IXIncomeStep1CellC

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self icon];
    [self desc];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UIImageView *)icon
{
    if (!_icon) {
        CGRect rect = CGRectMake(15.f, 12.f, 20.f, 20.f);
        _icon = [[UIImageView alloc] initWithFrame:rect];
        _icon.image = [UIImage imageNamed:@"search_add"];
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (UILabel *)desc
{
    if (!_desc) {
        
        CGRect rect = CGRectMake(GetView_MaxX(_icon) + 15, 10, 200, 23.f);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _desc.text = LocalizedString(@"添加银行卡");
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

@end
