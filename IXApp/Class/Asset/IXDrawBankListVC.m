//
//  IXDrawBankListVC.m
//  IXApp
//
//  Created by Bob on 2017/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDrawBankListVC.h"
#import "IXCpyConfig.h"
#import "NSString+FormatterPrice.h"
#import "IXUserDefaultM.h"
#import "IXBORequestMgr+Asset.h"
#import "IXUserInfoMgr.h"

@interface IXDrawBankListVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *contentTV;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) selectedBankInfo selectBank;

@end

@implementation IXDrawBankListVC

- (void)dealloc
{
    NSLog(@" -- %s -- ",__func__);
}

- (id)initWithSelectedInfo:(selectedBankInfo)bankInfo
{
    self = [super init];
    if ( self ) {
        _selectBank = bankInfo;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    self.title = LocalizedString(@"选择银行");
    [self getBankUrlInfo];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.contentTV reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}




- (void)getBankUrlInfo
{
    NSString *gatewayCode = [IXBORequestMgr shareInstance].channelDic[@"gatewayCode_org"];
    // 添加选择银行卡列表中只拉去Help2Pay的银行，不要拉egpay所有银行，不要中国银行
//    if (!gatewayCode.length) {
//        gatewayCode = @"help2pay";
//    }
//    gatewayCode = @"";
    if (gatewayCode.length) {
        NSMutableArray *bankArr = [NSMutableArray array];
        [SVProgressHUD show];
        [IXBORequestMgr b_getEnableBankListByGateWayCodeType:gatewayCode rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            if (success) {
                [SVProgressHUD dismiss];
                
                [obj enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *dic = obj;
                    IXBank *bank = [IXBank new];
                    bank.code =  dic[@"commonBankCode"];
                    bank.nameCN = dic[@"bankName"];
                    bank.nameEN = dic[@"bankNameEN"];
                    bank.nameTW = dic[@"bankNameTW"];
                    [bankArr addObject:bank];
                }];
                
                self.dataArr = bankArr;
                [self.contentTV reloadData];
            }else{
                [SVProgressHUD showInfoWithStatus:errStr];
            }
        }];
    }else{
        _dataArr = [IXBORequestMgr incash_requestEnableBankList:^(NSArray *bankList) {
            [SVProgressHUD dismiss];
            
            self.dataArr = bankList;
            [self.contentTV reloadData];
        }];
        
        if (!_dataArr || !_dataArr.count) {
            [SVProgressHUD showWithStatus:nil];
        }
    }
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                  NSStringFromClass([UITableViewCell class])];
  
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = PF_MEDI(13);
    cell.textLabel.dk_textColorPicker = DKCellTitleColor;
    
    if (indexPath.row < _dataArr.count) {
        IXEnableBankM   * m = _dataArr[indexPath.row];
        cell.textLabel.text = [m localizedName];
    }
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self onGoback];
    if ( _selectBank ) {
        _selectBank(_dataArr[indexPath.row]);
//        _selectBank = nil;
    }
}


#pragma mark -
#pragma mark - lazy loading

- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        CGRect  rect = CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight);
        _contentTV = [[UITableView alloc] initWithFrame:rect];
        _contentTV.separatorInset = UIEdgeInsetsMake( 0, -10, 0, 0);
        _contentTV.dk_backgroundColorPicker = DKTableColor;
        _contentTV.dk_separatorColorPicker = DKLineColor;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[UITableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        [self.view addSubview:_contentTV];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:ktableFooterFrame];
    }
    return _contentTV;
}


@end
