//
//  IXRecordCacheM.h
//  IXApp
//
//  Created by Evn on 2017/9/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IXRecordSymM;
@interface IXRecordCacheM : NSObject

@property (nonatomic, strong) NSMutableDictionary *symDic;

- (IXRecordSymM *)getCacheSymbolBySymbolId:(uint64_t)symbolId;

@end

@interface IXRecordSymM : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *lanName;
@property (nonatomic, copy) NSString *source;
@property (nonatomic, copy) NSString *marketName;
@property (nonatomic, assign) uint32_t contractSizeNew;
@property (nonatomic, readwrite) uint64_t volDigits;

@end
