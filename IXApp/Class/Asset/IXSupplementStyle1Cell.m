//
//  IXSupplementStyle1Cell.m
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSupplementStyle1Cell.h"

@interface IXSupplementStyle1Cell ()

@property (nonatomic, strong) UILabel *tipTitleLbl;

@property (nonatomic, strong) UIImageView *arrowImg;

@end

@implementation IXSupplementStyle1Cell

- (void)setTipTitle:(NSString *)tipTitle
{
    if ( tipTitle && tipTitle.length != 0 ) {
        self.tipTitleLbl.text = tipTitle;
    }
}

- (UILabel *)tipTitleLbl
{
    if ( !_tipTitleLbl ) {
        _tipTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, 200, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x99abba
                                        dTextColor:0x8395a4];
        [self.contentView addSubview:_tipTitleLbl];
    }
    return _tipTitleLbl;
}

- (UIImageView *)arrowImg
{
    if ( !_arrowImg ) {
        _arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake( kScreenWidth - 31, 14, 16, 16)];
    }
    [_arrowImg setImage:[UIImage imageNamed:@"About"]];
    return _arrowImg;
}

@end
