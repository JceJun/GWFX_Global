//
//  IXIncomeStep2CellA.h
//  IXApp
//
//  Created by Evn on 17/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXIncomeStep2CellA : UITableViewCell

@property (nonatomic, assign) BOOL seleted;

- (void)loadUIWithTitle:(NSString *)title
                   desc:(NSString *)desc
           isHiddenIcon:(BOOL)isHidden
       isShowSelectIcon:(BOOL)isShow
               isSelect:(BOOL)isSelect
                    tag:(NSUInteger)tag;


@end
