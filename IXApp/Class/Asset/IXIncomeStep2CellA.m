//
//  IXIncomeStep2CellA.m
//  IXApp
//
//  Created by Evn on 17/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeStep2CellA.h"

@interface IXIncomeStep2CellA()

@property (nonatomic,strong)UILabel *title;
@property (nonatomic,strong)UIImageView *icon;
@property (nonatomic,strong)UIImageView *accIcon;
@property (nonatomic,strong)UILabel *desc;
@property (nonatomic, strong)UIView *lineView;

@end

@implementation IXIncomeStep2CellA


- (void)loadUIWithTitle:(NSString *)title
                   desc:(NSString *)desc
           isHiddenIcon:(BOOL)isHidden
       isShowSelectIcon:(BOOL)isShow
               isSelect:(BOOL)isSelect
                    tag:(NSUInteger)tag{
    self.lineView.dk_backgroundColorPicker = DKLineColor;
    self.title.text = title;
    self.title.font = PF_MEDI(13);
    self.desc.text = desc;
    
    if (!isHidden) {
        self.icon.hidden = NO;
        [self.icon setImage:GET_IMAGE_NAME(@"incomeStep_accountType")];
    }else{
        self.icon.hidden = YES;
    }
    
    if (isShow) {
        CGRect frame = self.title.frame;
        frame.size.width = kScreenWidth - (14.5*2 + 16);
        self.title.frame = frame;
        self.desc.hidden = YES;
        
        _seleted = isSelect;
        if (isSelect) {
            self.accIcon.image = AutoNightImageNamed(@"common_cell_choose");
        }else{
            [self.accIcon setImage:GET_IMAGE_NAME(@"incomeStep_unChoose")];
        }
    }else{
        self.accIcon.hidden = YES;
    }
    
    switch (tag) {
        case 0:{
            self.title.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
            self.desc.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xd4d5dc);
            break;
        }
        case 1:{
            self.title.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
            self.title.font = PF_MEDI(12);
            break;
        }
        default:
            break;
    }
}

- (void)setSeleted:(BOOL)seleted
{
    _seleted = seleted;
    if (seleted) {
        self.accIcon.image = AutoNightImageNamed(@"common_cell_choose");
    }else{
        [self.accIcon setImage:GET_IMAGE_NAME(@"incomeStep_unChoose")];
    }
}


#pragma mark -
#pragma mark - lazy loading

- (UILabel *)title
{
    if (!_title) {
        CGRect rect = CGRectMake(14.5, 0,kScreenWidth/2, 44);
        _title = [[UILabel alloc] initWithFrame:rect];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _title.textAlignment = NSTextAlignmentLeft;
        _title.numberOfLines = 0;
        [self.contentView addSubview:_title];
    }
    
    return _title;
}

- (UIView *)lineView {
    
    if (!_lineView) {
        CGRect rect = CGRectMake(0, 0, kScreenWidth, kLineHeight);
        _lineView = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_lineView];
    }
    
    return _lineView;
}

- (UIImageView *)icon
{
    if (!_icon) {
        CGRect rect = CGRectMake(GetView_MaxX(_title), 14, 16, 16);
        _icon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_icon];
    }
    
    return _icon;
}

- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(kScreenWidth - (150 + 14.5), 14, 150, 16);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xd4d5dc);
        _desc.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_desc];
    }
    
    return _desc;
}

- (UIImageView *)accIcon
{
    if (!_accIcon) {
        CGRect rect = CGRectMake(kScreenWidth - (16 + 14.5), 14, 16, 16);
        _accIcon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_accIcon];
    }
    
    return _accIcon;
}


@end
