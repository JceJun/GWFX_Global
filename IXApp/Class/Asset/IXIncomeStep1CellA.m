//
//  IXIncomeStep1CellA.m
//  IXApp
//
//  Created by Magee on 2017/4/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXIncomeStep1CellA.h"

@interface IXIncomeStep1CellA ()

@property (nonatomic,strong)UIImageView *icon;
@property (nonatomic,strong)UILabel     *desc;
@end

@implementation IXIncomeStep1CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self icon];
    [self desc];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UIImageView *)icon
{
    if (!_icon) {
        CGRect rect = CGRectMake((kScreenWidth - 109)/2.f, 40.f, 109.f, 77.f);
        _icon = [[UIImageView alloc] initWithFrame:rect];
        _icon.dk_imagePicker = DKImageNames(@"income_no_bank", @"income_no_bank_D");
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_icon) + 20, kScreenWidth, 18.f);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(15);
        _desc.textAlignment = NSTextAlignmentCenter;
        _desc.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4c6072);
        _desc.text = LocalizedString(@"没有绑定银行卡");
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

@end
