//
//  IXSupplementStyle2Cell.m
//  IXApp
//
//  Created by Bob on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSupplementStyle2Cell.h"

@interface IXSupplementStyle2Cell ()<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *arrowImg;

@end

@implementation IXSupplementStyle2Cell

- (void)setTipTitle:(NSString *)tipTitle
{
    if ( tipTitle && tipTitle.length != 0 ) {
        self.tipTitleLbl.text = tipTitle;
    }
}

- (void)setTipContent:(NSString *)tipContent
{
    if ( tipContent && tipContent.length != 0 ) {
        self.bankAccountTF.text = tipContent;
    }else{
        self.bankAccountTF.text = @"";
    }
}


- (void)setSupplementContent:(NSString *)supplementContent
{
    self.bankAccountTF.text = @"";
}

- (void)hiddenArrowImg
{
    CGRect frame = self.bankAccountTF.frame;
    frame.size.width = kScreenWidth - 15 - CGRectGetMinX(self.bankAccountTF.frame);
    self.bankAccountTF.frame = frame;
}

- (UILabel *)tipTitleLbl
{
    if ( !_tipTitleLbl ) {
        _tipTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, 200, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x4c6072
                                        dTextColor:0x8395a4];
        [self.contentView addSubview:_tipTitleLbl];
    }
    return _tipTitleLbl;
}
- (IXTextField *)bankAccountTF
{
    if ( !_bankAccountTF ) {
        _bankAccountTF = [[IXTextField alloc] initWithFrame:CGRectMake( kScreenWidth - 215, 7, 200, 30)];
        _bankAccountTF.textAlignment = NSTextAlignmentRight;
        _bankAccountTF.dk_textColorPicker = DKCellContentColor;
        _bankAccountTF.font = PF_MEDI(13);
        _bankAccountTF.delegate = self;
        [self.contentView addSubview:_bankAccountTF];
        [self setTextFieldInputAccessoryView];
    }
    return _bankAccountTF;
}

- (UIImageView *)arrowImg
{
    if ( !_arrowImg ) {
        _arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake( kScreenWidth - 23, 15, 8, 13)];
    }
    _arrowImg.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
    return _arrowImg;
}

- (void)setTextFieldInputAccessoryView
{
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    [topView setBarStyle:UIBarStyleDefault];
    topView.backgroundColor = MarketCellSepColor;
    
    UIBarButtonItem * spaceBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:self
                                                                              action:nil];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:PF_MEDI(13)];
    [cancelBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(15, 9, 60, 25);
    [cancelBtn addTarget:self action:@selector(cancelKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBtnItem = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneBtn setTitle:LocalizedString(@"完成") forState:UIControlStateNormal];
    [doneBtn.titleLabel setFont:PF_MEDI(13)];
    [doneBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    doneBtn.frame = CGRectMake(kScreenWidth - 90, 9, 80, 25);
    [doneBtn addTarget:self action:@selector(dealKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneBtnItem = [[UIBarButtonItem alloc]initWithCustomView:doneBtn];
    NSArray * buttonsArray = [NSArray arrayWithObjects:cancelBtnItem,spaceBtn,doneBtnItem,nil];
    [topView setItems:buttonsArray];
    [_bankAccountTF setInputAccessoryView:topView];
    [_bankAccountTF setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_bankAccountTF setAutocapitalizationType:UITextAutocapitalizationTypeNone];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldValue:tag:)]) {
        
        [self.delegate textFieldValue:textField.text tag:textField.tag];
    }
}

- (void)dealKeyboardHidden
{
    [_bankAccountTF resignFirstResponder];
    if ( _textContent ) {
        _textContent(_bankAccountTF.text);
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (void)cancelKeyboardHidden{
    [_bankAccountTF resignFirstResponder];
}
@end
