//
//  IXJSHelper.h
//  IXApp
//
//  Created by Seven on 2017/9/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

//#define kWebMessageName @"ixMarketPage"
//IXJSHook

@protocol JSHelperDelegate <NSObject>

- (void)IXJSHelperPerformSelecter:(SEL)sel param:(id)param;

@end

@interface IXJSHelper : NSObject

@property (nonatomic, weak) id<JSHelperDelegate> delegate;
@property (nonatomic, weak) WKWebView   * webView;
@property(nonatomic,strong)NSString *msgName;

- (instancetype)initWithDelegate:(id<JSHelperDelegate>)delegate vc:(UIViewController *)vc msgName:(NSString *)msgName;

@end
