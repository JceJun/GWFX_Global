//
//  IXJSHelper.m
//  IXApp
//
//  Created by Seven on 2017/9/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXJSHelper.h"
#import "NSObject+IX.h"

@interface IXJSHelper ()

@property (nonatomic, weak) UIViewController * vc;

@end

@implementation IXJSHelper

- (void)dealloc
{
    DLog(@" -- %s --",__func__);
}

- (instancetype)initWithDelegate:(id<JSHelperDelegate>)delegate vc:(UIViewController *)vc msgName:(NSString *)msgName
{
    if (self = [super init]) {
        self.delegate = delegate;
        self.vc = vc;
        self.msgName = msgName;
    }
    return self;
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:message.body];
    DLog(@"JS交互参数：%@", dic);
    
    if ([message.name isEqualToString:_msgName] && [dic ix_isDictionary]) {
        
        DLog(@"currentThread  ------   %@", [NSThread currentThread]);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString * func = dic[@"functionName"];
            //delegate 操作
            if (self.delegate && [self.delegate respondsToSelector:@selector(IXJSHelperPerformSelecter:param:)]) {
                if (dic.allKeys.count > 1) {
                    func = [func stringByAppendingString:@":"];
                }
                [self.delegate IXJSHelperPerformSelecter:NSSelectorFromString(func) param:dic];
            }
            
        });
    } else {
        return;
    }
}


@end
