//
//  AppDelegate.h
//  IXApp
//
//  Created by bob on 16/11/1.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IXBroadsideVC.h"
#import "IXRootTabBarVC.h"
typedef NS_ENUM(NSInteger, Reg) {
    RegNone,                //没有注册
    RegStartDeal,           //注册后交易
    RegOpenRealAccount,     //注册后开户
};

#define kNotiRefreashCata @"needRefreashCata"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, assign)time_t bgStamp;

//注册
@property (assign, nonatomic) Reg startWithReg;

@property (assign, nonatomic) BOOL hasLogin;

@property(nonatomic,assign)BOOL loginAferRegist; // 注册登陆后未跳转到行情页并显示提示框


@property (assign, nonatomic) NSInteger switchAccount;

//推送信息
@property (strong, nonatomic) NSDictionary *pushInfo;

@property (strong, nonatomic) UIWindow *window;

+ (IXBroadsideVC *)getBroadSideVC;

+ (IIViewDeckController *)getRootVC;
+ (IXRootTabBarVC *)getTabBarVC;

//版本更新
- (void)getLatestVersion;

@end

