//
//  IXRootTabBarVC.m
//  IXApp
//
//  Created by Magee on 16/11/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRootTabBarVC.h"
#import "IXPositionHomeVC.h"
#import "IXMarketHomeVC.h"
#import "IXAssetHomeVC.h"
#import "IXQuoteDistribute.h"
#import "IXQuoteDataCache.h"
#import "IXAccountBalanceModel.h"
#import "IXCpyConfig.h"
#import "IXWebHomeVC.h"
#import "IXSkirillToSkrillVC.h"
#import "IXBORequestMgr+Asset.h"
#import "AppDelegate+UI.h"
#import "IXCreditCardContactVC.h"
#import "IXBORequestMgr+Comp.h"
#import "IXDPSChannelVC.h"
@interface IXRootTabBarVC ()<IXTCPRequestDelegate,UITabBarControllerDelegate>

@property (nonatomic,strong) UIViewController *curVC;

@end

@implementation IXRootTabBarVC

- (id)init
{
    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(needRefresh)
                                                     name:@"needRefresh"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateNightMode)
                                                     name:DKNightVersionThemeChangingNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoDeposit) name:NOTI_ROOT_GOTO_Deposit object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        IXCreditCardContactVC *vc = [IXCreditCardContactVC new];
//        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//        [self presentViewController:nav animated:NO completion:nil];
        
    });
    
    NSInteger   tag = 0;
    IXBaseNavVC * webNav = nil;
//    if (OpenHomeView) {
        IXWebHomeVC * webVC = [IXWebHomeVC new];
        webNav = [[IXBaseNavVC alloc] initWithRootViewController:webVC];
        webNav.tabBarItem.tag = tag;
        webNav.tabBarItem.title = LocalizedString(@"首页");
        webNav.navigationBar.translucent = NO;
        webNav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
        [webNav.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -4)];
        webNav.tabBarItem.dk_imagePicker = DKImageWithImgs([[UIImage imageNamed:@"webHome_n"]
                                                            imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                          [[UIImage imageNamed:@"webHome_n_D"]
                                                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
        
        webNav.tabBarItem.dk_selectedImagePicker = DKImageWithImgs([[UIImage imageNamed:@"webHome_h"]
                                                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                                  [[UIImage imageNamed:@"webHome_h_D"]
                                                                   imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
        tag ++;
//    }
    
    IXMarketHomeVC *subsymVC = [[IXMarketHomeVC alloc] init];
    IXBaseNavVC *subsymNav = [[IXBaseNavVC alloc] initWithRootViewController:subsymVC];
    subsymNav.tabBarItem.tag = tag;
    subsymNav.tabBarItem.title = LocalizedString(@"市场");
    subsymNav.navigationBar.translucent = NO;
    subsymNav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
    [subsymNav.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -4)];
    subsymNav.tabBarItem.dk_imagePicker = DKImageWithImgs([[UIImage imageNamed:@"marketHome_n"]
                                                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                          [[UIImage imageNamed:@"marketHome_n_D"]
                                                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
    subsymNav.tabBarItem.dk_selectedImagePicker = DKImageWithImgs([[UIImage imageNamed:@"marketHome_h"]
                                                                   imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                          [[UIImage imageNamed:@"marketHome_h_D"]
                                                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
    tag ++;

    IXPositionHomeVC *positionVC = [[IXPositionHomeVC alloc] init];
    IXBaseNavVC *positionNav = [[IXBaseNavVC alloc] initWithRootViewController:positionVC];
    positionNav.tabBarItem.tag = tag;
    positionNav.tabBarItem.title = LocalizedString(@"仓位");
    positionNav.navigationBar.translucent = NO;
    positionNav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
    [positionNav.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -4)];
    positionNav.tabBarItem.dk_imagePicker = DKImageWithImgs([[UIImage imageNamed:@"postionHome_n"]
                                                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                            [[UIImage imageNamed:@"postionHome_n_D"]
                                                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
    positionNav.tabBarItem.dk_selectedImagePicker = DKImageWithImgs([[UIImage imageNamed:@"postionHome_h"]
                                                                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                            [[UIImage imageNamed:@"postionHome_h_D"]
                                                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
    tag ++;

    IXAssetHomeVC *assetVC = [[IXAssetHomeVC alloc] init];
    IXBaseNavVC *assetNav = [[IXBaseNavVC alloc] initWithRootViewController:assetVC];
    assetNav.tabBarItem.tag = tag;
    assetNav.tabBarItem.title = LocalizedString(@"资产");
    assetNav.navigationBar.translucent = NO;
    assetNav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
    [assetNav.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -4)];
    assetNav.tabBarItem.dk_imagePicker = DKImageWithImgs([[UIImage imageNamed:@"assetHome_n"]
                                                          imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                         [[UIImage imageNamed:@"assetHome_n_D"]
                                                          imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
    assetNav.tabBarItem.dk_selectedImagePicker = DKImageWithImgs([[UIImage imageNamed:@"assetHome_h"]
                                                                  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                         [[UIImage imageNamed:@"assetHome_h_D"]
                                                          imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]);
    tag ++;

    if (webNav) {
        self.viewControllers = @[webNav,subsymNav,positionNav,assetNav];
    }else{
        self.viewControllers = @[subsymNav,positionNav,assetNav];
    }

    [self updateNightMode];
    
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (app.loginAferRegist) {
        [SVProgressHUD show];
        [IXBORequestMgr comp_requestCompanyConfigInfoWithId:CompanyID complete:^(NSString *errStr, NSString *errCode, id obj) {
            [SVProgressHUD dismiss];
            
            if ([IXBORequestMgr shareInstance].launchHappenDic[@"Invite"]) {
                [self gotoInvite:webNav];
                
            }else if ([[IXBORequestMgr shareInstance].mobileOnlineConfig[@"registerTarget"] isEqualToString:@"deposit"]) {
                [self tabBar:self.tabBar didSelectItem:subsymNav.tabBarItem];
                self.selectedViewController = subsymNav;
                
                [self gotoDeposit];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"注册-成功页（跳转入金）" withValues:nil];
            }else{
                [self tabBar:self.tabBar didSelectItem:subsymNav.tabBarItem];
                self.selectedViewController = subsymNav;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [AppDelegate showLoginAfterRigistAlert];
                });
                
                [[AppsFlyerTracker sharedTracker] trackEvent:@"注册-成功页（跳转行情）" withValues:nil];
            }
            app.loginAferRegist = NO;
        }];
    }else{
        [self tabBar:self.tabBar didSelectItem:webNav.tabBarItem];
        self.selectedViewController = webNav;
        
        if ([IXBORequestMgr shareInstance].launchHappenDic[@"Invite"]) {
            [self gotoInvite:webNav];
        }
    }
    
    self.tabBar.translucent = NO;
    self.delegate = self;
    self.tabBar.dk_tintColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
    self.tabBar.dk_barTintColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoMarket) name:NOTI_ROOT_GOTO_MARKET object:nil];
}

- (void)gotoMarket{
    if (self.viewControllers.count == 4) {
        self.selectedIndex = 1;
    }else{
        self.selectedIndex = 0;
    }
    [self tabBar:self.tabBar didSelectItem:[self.viewControllers[self.selectedIndex] tabBarItem]];
}


- (void)gotoInvite:(IXBaseNavVC *)webNav{
    [self tabBar:self.tabBar didSelectItem:webNav.tabBarItem];
    self.selectedViewController = webNav;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_WEBHOME_GOTO_INVITE object:nil];
        [[IXBORequestMgr shareInstance].launchHappenDic removeObjectForKey:@"Invite"];
    });
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    id curVC = [[(UINavigationController *)self.viewControllers[item.tag] viewControllers] lastObject];
    if ( curVC != self.curVC ) {
        [self needChangeSubscribeState:self.curVC CurVC:curVC];
        sleep(0.01);
        self.curVC = curVC;
        [[IXQuoteDataCache shareInstance] saveDynQuote];
    }
    
} 


- (void)gotoDeposit{
    IXDPSChannelVC *vc = [IXDPSChannelVC new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.selectedViewController pushViewController:vc animated:YES];
}



/*
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    //demo账号登录控制不能切换到仓位和资产
    if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
        NSArray *vcArr = ((IXBaseNavVC *)viewController).viewControllers;
        for (UIViewController *vc in vcArr) {
            if ([vc isKindOfClass:[IXPositionHomeVC class]]
                || [vc isKindOfClass:[IXAssetHomeVC class]]) {
                [(IXDataBaseVC *)self.curVC showRegistLoginAlert];
                return NO;
            }
        }
    }
    return YES;
}*/

- (void)IXTCPQuoteSuccessData:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if ( cmd == CMD_QUOTE_SUB||
         cmd == CMD_QUOTE_PUB||
         cmd == CMD_QUOTE_PUB_DETAIL||
         cmd == CMD_QUOTE_SUB_DETAIL ){
        [[IXAccountBalanceModel shareInstance] setQuoteDataArr:arr];
    }

    id<IXQuoteDistribute>obj = (id<IXQuoteDistribute>)self.curVC;
    if([obj respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)]){
        [obj didResponseQuoteDistribute:arr cmd:cmd];
    }
}

- (void)needChangeSubscribeState:(UIViewController *)preVC CurVC:(UIViewController *)currVC
{
    id<IXQuoteDistribute>obj = (id<IXQuoteDistribute>)preVC;
    if ( [obj respondsToSelector:@selector(cancelVisualQuote)] ) {
        [obj cancelVisualQuote];
    }
    
    id<IXQuoteDistribute>cur = (id<IXQuoteDistribute>)currVC;
    if ( [cur respondsToSelector:@selector(subscribeVisualQuote)] ) {
        [cur subscribeVisualQuote];
    }
}

- (void)needRefresh
{
    id obj = self.curVC;
    if ([obj respondsToSelector:@selector(needRefresh)]) {
        id<IXQuoteDistribute>obj = (id<IXQuoteDistribute>)self.curVC;
        if([obj respondsToSelector:@selector(needRefresh)]){
            [obj needRefresh];
        }
    }
}

- (void)updateNightMode
{
    [self.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull vc, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0x8395a4)}
                                         forState:UIControlStateNormal];
            [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0x50a1e5)}
                                         forState:UIControlStateSelected];
        } else {
            [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0xd5dbe5)}
                                         forState:UIControlStateNormal];
            [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorHexFromRGB(0x4c6072)}
                                         forState:UIControlStateSelected];
        }
    }];
}





@end
