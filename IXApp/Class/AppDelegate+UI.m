//
//  AppDelegate+UI.m
//  IXApp
//
//  Created by Magee on 2016/12/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "AppDelegate+UI.h"

#import "IXPreviewVC.h"
#import "IXBroadsideVC.h"
#import "IXLoginMainVC.h"
#import "IXRootTabBarVC.h"
#import "IXDataBase.h"
#import "SFHFKeychainUtils.h"
#import "GesturePWdData.h"
#import "IXVisiterVC.h"

#import "IXMarketHomeVC.h"
#import "IIViewDeckController.h"

#import "IXAcntStep1VC.h"
#import "IXTouchIdVC.h"
#import "GesturePwdVC.h"

#import "IXAccountBalanceModel.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXWUserInfo.h"
#import "IXUserDefaultM.h"
#import "IXBORequestMgr+Login.h"
#import "IxProtoHeader.pbobjc.h"
#import "IXAccountGroupModel.h"
#import "UIImageView+WebCache.h"
#import "UIKit+Block.h"
#import "IXTipView.h"
#import "IXCommonWebVC.h"
#import "IXDPSChannelVC.h"

@implementation AppDelegate (UI)

- (void)dealWithReLogin {
#warning 线程同步问题，目前暂用延迟器解决，问题的本质是 应当在tcp队列中，等待断开操作结束，然后再开始重连操作
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *userName = nil;
        NSString *passWord = nil;
        NSString *phoneCode = nil;
        userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
        phoneCode = [[NSUserDefaults standardUserDefaults] objectForKey:kIXPhoneCode];
        if (userName && userName.length > 0) {
            passWord = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:kServiceName error:nil];
        }
        if (userName.length > 0 && passWord.length > 0) {
            [AppDelegate startAutoLoginUserName:userName passWord:passWord phoneCode:phoneCode];
        } else {
            [self showUI];
        }
        
    });
}

- (void)firstLaunch
{
    BOOL firstLaunch = [[NSUserDefaults standardUserDefaults] boolForKey:kFirstLaunch];
    if( !firstLaunch ){
        [IXWUserInfo clear];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kFirstLaunch];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


- (void)showUI
{
    [self firstLaunch];
    
    self.window = [[UIWindow alloc] initWithFrame:kScreenBound];
    NSString *showPreviewYet = @"";
    @try{
        showPreviewYet = [[NSUserDefaults standardUserDefaults] objectForKey:kLoadStartFlag];
    }@catch(NSException *exception) {
        showPreviewYet = @"";
    }
    if ([showPreviewYet isEqualToString:[IXAppUtil appVersion]]) {
        [AppDelegate showLogin];
    } else {
        [AppDelegate packageCpyConfig];
        [AppDelegate showPreview];
    }
    [self.window makeKeyAndVisible];
}

+ (void)packageCpyConfig
{
    [IXUserDefaultM setDefaultUnit];
    [IXUserDefaultM saveSymbolCataStyle:SymbolCataStyle];
}

+ (void)showPreview
{
    IXPreviewVC *vc = [[IXPreviewVC alloc] init];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.window.rootViewController = vc;
}

+ (void)showLogin
{
    [IXTCPRequest shareInstance].delegate = nil;
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    
    if (OpenHomeView) {
        NSString *passWord = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:kServiceName error:nil];
        if (passWord.length) {
            [self showLoginMain];
        } else {
            [self showLoginMain];
        }
    } else {
        [self showLoginMain];
    }
}

+ (void)showLoginMain
{
    IXLoginMainVC *loginVC = [[IXLoginMainVC alloc] init];
    IXBaseNavVC *nav = [[IXBaseNavVC alloc] initWithRootViewController:loginVC];
    nav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
    nav.navigationBar.translucent = NO;

    IIViewDeckController *vc = [AppDelegate getRootVC];
    if ([vc isKindOfClass:[IIViewDeckController class]]) {
        vc = nil;
    }
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    del.window.rootViewController = nav;
}

+ (void)showRegist
{
    IXLoginMainVC *loginVC = [[IXLoginMainVC alloc] init];
    IXBaseNavVC *nav = [[IXBaseNavVC alloc] initWithRootViewController:loginVC];
    nav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
    nav.navigationBar.translucent = NO;
//    nav.navigationBar.hidden = YES;
    [loginVC registAction];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.window.rootViewController = nav;
}

+ (void)showRoot
{
    IXBroadsideVC *leftDrawer = [[IXBroadsideVC alloc] init];
    IXRootTabBarVC * center = [[IXRootTabBarVC alloc] init];
    [IXTCPRequest shareInstance].delegate = (id)center;
    
    IIViewDeckController *rootVC = [[IIViewDeckController alloc] initWithCenterViewController:center
                                                                           leftViewController:leftDrawer];
    rootVC.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    rootVC.leftSize = kScreenBound.size.width/4;
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.window.rootViewController = rootVC;
    
    if( delegate.startWithReg == RegOpenRealAccount ){
        
        IXBaseNavVC *nav = center.selectedViewController;
        IXAcntStep1VC *step = [[IXAcntStep1VC alloc] init];
        step.hidesBottomBarWhenPushed = YES;
        [nav pushViewController:step animated:YES];
    }
    delegate.startWithReg = RegNone;
}

+ (void)showVisiterPage
{
    IXVisiterVC * vc = [IXVisiterVC new];
    IXBaseNavVC * nav = [[IXBaseNavVC alloc] initWithRootViewController:vc];
    nav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
    nav.navigationBar.translucent = NO;
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.window.rootViewController = nav;
}

+ (void)loginOut
{
    [[IXTCPRequest shareInstance] disConnect];
    [IXTCPRequest shareInstance].delegate = nil;
    [[IXTradeDataCache shareInstance] clearInstance];
    [[IXQuoteDataCache shareInstance] clearInstance];
    [[IXAccountBalanceModel shareInstance] clearCache];
    [[IXAccountGroupModel shareInstance] clearInstance];
    
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    del.hasLogin = NO;
    del.bgStamp = 0;

    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    [SFHFKeychainUtils deleteItemForUsername:userName andServiceName:kServiceName error:nil];
}

+ (void)saveUserBehavoir
{
    if ( [[AppDelegate getRootVC] isKindOfClass:[IIViewDeckController class]] ) {
        
        IIViewDeckController *rootVC = [AppDelegate getRootVC];
        IXRootTabBarVC *tabVC = (IXRootTabBarVC *)rootVC.centerController;
//        IXBaseNavVC *nav = (IXBaseNavVC *)tabVC.childViewControllers[0];

        [tabVC.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj,
                                                               NSUInteger idx,
                                                               BOOL * _Nonnull stop) {
            
            IXBaseNavVC *nav = (IXBaseNavVC *)obj;
            
            if ([nav.childViewControllers[0] isKindOfClass:[IXMarketHomeVC class]]
                && [nav.childViewControllers[0] respondsToSelector:@selector(remChooseRow)]) {
                [nav.childViewControllers[0] remChooseRow];
                *stop = YES;
            }
        }];
    }
}

+ (void)startAutoLoginUserName:(NSString *)userName
                      passWord:(NSString *)passWord
                     phoneCode:(NSString *)phoneCode
{
    proto_user_login *proto = [[proto_user_login alloc] init];
    if ([userName containsString:@"@"]) {
        [proto setEmail:userName];
        [proto setLoginType:proto_user_login_elogintype_ByEmail];
        [proto setType:proto_user_login_elogintype_ByEmail];
        [IXUserInfoMgr shareInstance].loginType = proto_user_login_elogintype_ByEmail;
        [IXUserInfoMgr shareInstance].loginName = userName;
    }else{
        [proto setPhone:userName];
        [proto setLoginType:proto_user_login_elogintype_ByPhone];
        [proto setType:proto_user_login_elogintype_ByPhone];
        if (!phoneCode.length) {
            phoneCode = @"62";//没有，默认62
        }
        [proto setPhoneCode:phoneCode];
        [IXUserInfoMgr shareInstance].loginType = proto_user_login_elogintype_ByPhone;
        [IXUserInfoMgr shareInstance].loginName = nil;
    }
    
    [proto setPwd:[IXDataProcessTools md5StringByString:passWord]];
    [proto setLogintime:[IXEntityFormatter getCurrentTimeInterval]];
    [proto setCompanyToken:CompanyToken];
    [proto setCompanyid:CompanyID];
    [proto setSessionType:[IXUserInfoMgr shareInstance].itemType];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    [proto setVersion:[version intValue]];
    [proto setSecureDev:[IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]]];
    [[IXTCPRequest shareInstance] loginWithParam:proto];
 }


+ (item_data_version *)dataVersion
{
    item_data_version *dataVersion = [[item_data_version alloc] init];
    //产品
    
    NSDictionary *symbolUUID = [IXDBSymbolMgr querySymbolUUIDMax];
    dataVersion.verSym = [symbolUUID[kUUID] longLongValue];
    
    //产品分类
    NSDictionary *symbolCataUUID = [IXDBSymbolCataMgr querySymbolCatasUUIDMax];
    dataVersion.verSymcata = [symbolCataUUID[kUUID] longLongValue];
    
    //自选产品
//    NSDictionary *symbolSubUUID = [IXDBSymbolSubMgr querySymbolSubUUIDMaxAccountId:[IXKeepAliveCache shareInstance].accountId];
//    dataVersion.verSymsub = [symbolSubUUID[kUUID] longLongValue];
    
    NSDictionary *symSubDic = @{
                                kAccountId:@([IXUserInfoMgr shareInstance].userLogInfo.account.id_p)
                                };
    NSDictionary *symbolSubUUID = [IXDBSymbolSubMgr querySymbolSubUUIDMaxByAddInfo:symSubDic];
    dataVersion.verSymsub = [symbolSubUUID[kUUID] longLongValue];
    
    //公司
    NSDictionary *companyUUID = [IXDBCompanyMgr queryCompanyUUIDMax];
    dataVersion.verCompany = [companyUUID[kUUID] longLongValue];
    
    //热门产品
    NSDictionary *symbolHotUUID = [IXDBSymbolHotMgr querySymbolHotUUIDMax];
    dataVersion.verSymhot = [symbolHotUUID[kUUID] longLongValue];
    
    //假期分类
    NSDictionary *holidayCataUUID = [IXDBHolidayCataMgr queryHolidayCatasUUIDMax];
    dataVersion.verHolidayCata = [holidayCataUUID[kUUID] longLongValue];
    
    //假期
    NSDictionary *holidayUUID = [IXDBHolidayMgr queryHolidayUUIDMax];
    dataVersion.verHoliday = [holidayUUID[kUUID] longLongValue];
    
    //交易时间分类
    NSDictionary *scheduleCataUUID = [IXDBScheduleCataMgr queryScheduleCataUUIDMax];
    dataVersion.verScheduleCata = [scheduleCataUUID[kUUID] longLongValue];
    
    //交易时间
    NSDictionary *scheduleUUID = [IXDBScheduleMgr queryScheduleUUIDMax];
    dataVersion.verSchedule = [scheduleUUID[kUUID] longLongValue];
    
    //产品标签
    NSDictionary *symLableUUID = [IXDBSymbolLableMgr querySymbolLableUUIDMax];
    dataVersion.verSymLabel = [symLableUUID[kUUID] longLongValue];
    
    //marginSet
    NSDictionary *marginSetUUID = [IXDBMarginSetMgr queryMarginSetUUIDMax];
    dataVersion.verMarginSet = [marginSetUUID[kUUID] longLongValue];
    
    //语言
    NSDictionary *languageUUID = [IXDBLanguageMgr queryLanguageUUIDMax];
    dataVersion.verLanguage = [languageUUID[kUUID] longLongValue];
    
    //schedule_Margin
    NSDictionary *scheduleMarginUUID = [IXDBScheduleMarginMgr queryScheduleMarginUUIDMax];
    dataVersion.verScheduleMargin = [scheduleMarginUUID[kUUID] longLongValue];
    
    //holiday_Margin
    NSDictionary *holidayMarginUUID = [IXDBHolidayMarginMgr queryHolidayMarginUUIDMax];
    dataVersion.verHolidayMargin = [holidayMarginUUID[kUUID] longLongValue];
    
//    //账户组
//    NSDictionary *accGroupUUID = [IXDBAccountGroupMgr queryAccountGroupUUIDMax];
//    dataVersion.verAccgroup = [accGroupUUID[kUUID] longLongValue];
    
    NSDictionary *agscDic = @{
                          kAccGroupId:@([IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid)
                          };
    NSDictionary *agscUUID = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataUUIDMaxByAddInfo:agscDic];
    dataVersion.verAccgroupSymcata = [agscUUID[kUUID] longLongValue];
    
    //结算时间
    NSDictionary *eodTimeUUID = [IXDBEodTimeMgr queryEodTimeUUIDMax];
    dataVersion.verEodTime = [eodTimeUUID[kUUID] longLongValue];
    
    //账户组产品
    NSDictionary *groupSymUUID = [IXDBG_SMgr queryG_SUUIDMax];
    dataVersion.verGrpsym = [groupSymUUID[kUUID] longLongValue];
    
    //账户组产品分类
    NSDictionary *groupSymCataUUID = [IXDBG_S_CataMgr queryG_S_CataUUIDMax];
    dataVersion.verGrpsymcata = [groupSymCataUUID[kUUID] longLongValue];
    
    //lp_channel_account
    NSDictionary *lpchaccUUID = [IXDBLpchaccMgr queryLpchaccUUIDMax];
    dataVersion.verLpchacc = [lpchaccUUID[kUUID] longLongValue];
    
    //lp_channel_account_symbol
    NSDictionary *lpchaccSymUUID = [IXDBLpchaccSymbolMgr queryLpchaccSymbolUUIDMax];
    dataVersion.verLpchaccSym = [lpchaccSymUUID[kUUID] longLongValue];
    
    //lp_channel
    NSDictionary *lpchannelUUID = [IXDBLpchannelMgr queryLpchannelUUIDMax];
    dataVersion.verLpchannel = [lpchannelUUID[kUUID] longLongValue];
    
    //lp_channel_symbol
    NSDictionary *lpchannelSymUUID = [IXDBLpchannelSymbolMgr queryLpchannelSymbolUUIDMax];
    dataVersion.verLpchannelSym = [lpchannelSymUUID[kUUID] longLongValue];
    
    //lp_IbBind
    NSDictionary *lpIbBindUUID = [IXDBLpibBindMgr queryLpibBindUUIDMax];
    dataVersion.verLpibBind = [lpIbBindUUID[kUUID] longLongValue];
    
    
    return dataVersion;
}

// 显示弹窗广告位
+ (void)showAdContainer:(NSString *)imgUrl link:(NSString *)url{
    UIView *view = [UIView new];
    view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    view.tag = 666;
    [KEYWINDOW addSubview:view];
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(KEYWINDOW);
    }];
    
    UIImageView *img_main = [UIImageView new];
    [view addSubview:img_main];
    img_main.contentMode = UIViewContentModeScaleAspectFit;
    [img_main makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(0);
        make.centerY.equalTo(-30);
        make.width.equalTo(250);
    }];
    [img_main sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"common_holder"]];
    [img_main block_whenTapped:^(UIView *aView) {
        if (url.length) {
            [AppDelegate hideAdContainer];
            
            [AppDelegate gotoCommonWeb:url];
        }
    }];
    
    UIImageView *img_close = [UIImageView new];
    img_close.image = [UIImage imageNamed:@"close_circle"];
    [view addSubview:img_close];
    [img_close makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(0);
        make.bottom.equalTo(-kScreenHeight*1/6);
    }];
    [img_close block_whenTapped:^(UIView *aView) {
        [AppDelegate hideAdContainer];
    }];
}

// 隐藏弹窗广告位
+ (void)hideAdContainer{
    UIView *view = [KEYWINDOW viewWithTag:666];
    if (view) {
        [view removeFromSuperview];
    }
}

// 跳转通用网页
+ (void)gotoCommonWeb:(NSString *)url{
    IXCommonWebVC *vc = [IXCommonWebVC new];
    vc.url = url;
    IXBaseNavVC *nav = [[IXBaseNavVC alloc] initWithRootViewController:vc];
    nav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
    vc.navigationController.navigationBar.translucent = NO;
    [[AppDelegate getRootVC] presentViewController:nav animated:YES completion:nil];
}

// 任意页面跳转Native
+ (void)gotoNativeVC:(NSString *)classStr isPresent:(BOOL)isPresent param:(id)param{
    // 退回
    IXRootTabBarVC  * tabBar = [AppDelegate getTabBarVC];
    [tabBar.selectedViewController popViewControllerAnimated:YES];
    
    Class class = NSClassFromString(classStr);
    UIViewController *vc = [class new];
    if ([vc isKindOfClass:[UIViewController class]]) {
        if (isPresent) {
            
        }else{
            [tabBar.selectedViewController pushViewController:vc animated:YES];
        }
    }
}


// 注册成功后弹窗提示
+ (void)showLoginAfterRigistAlert{
    [IXTipView showWithTitle:@"Registration Successful"
                     message:@"$20 dollars bonus saved in your account and start trading now"
                 cancelTitle:nil
                 otherTitles:@[@"Start Trading"]
                      btnBlk:nil];
}




@end
