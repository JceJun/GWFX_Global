//
//  IXOpenCellM.m
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenCellM.h"

@implementation IXOpenCellM

#pragma mark 配置Cell
+ (IXDetailSymbolCell *)configSymbolDetailCellWithTableView:(UITableView *)tableView
                                               WithIndex:(NSIndexPath *)indexPath
{
    
    IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                NSStringFromClass([IXDetailSymbolCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXSymbolDeepCell *)configDeepPrcCellWithTableView:(UITableView *)tableView
                                          WithIndex:(NSIndexPath *)indexPath
{
    IXSymbolDeepCell *cell = [tableView dequeueReusableCellWithIdentifier:
                              NSStringFromClass([IXSymbolDeepCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.cellContentHeight = 23;
    
    return cell;
}

+ (IXOpenShowPrcCell *)configShowPrcCellWithTableView:(UITableView *)tableView
                                          WithIndex:(NSIndexPath *)indexPath
{
    IXOpenShowPrcCell *cell = [tableView dequeueReusableCellWithIdentifier:
                               NSStringFromClass([IXOpenShowPrcCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXOpenTypeCell *)configOpenTypeCellWithTableView:(UITableView *)tableView
                                           WithIndex:(NSIndexPath *)indexPath
{
    IXOpenTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:
                            NSStringFromClass([IXOpenTypeCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXOpenDirCell *)configOpenDirCellWithTableView:(UITableView *)tableView
                                          WithIndex:(NSIndexPath *)indexPath
{
    IXOpenDirCell *cell = [tableView dequeueReusableCellWithIdentifier:
                           NSStringFromClass([IXOpenDirCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXOpenPrcCell *)configOpenPrcCellWithTableView:(UITableView *)tableView
                                          WithIndex:(NSIndexPath *)indexPath
{
    IXOpenPrcCell *cell = [tableView dequeueReusableCellWithIdentifier:
                           NSStringFromClass([IXOpenPrcCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXPositionPrcCell *)configPositionPrcCellWithTableView:(UITableView *)tableView
                                                WithIndex:(NSIndexPath *)indexPath
{
    IXPositionPrcCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionPrcCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXOpenEptPrcCell *)configOpenEptPrcCellWithTableView:(UITableView *)tableView
                                              WithIndex:(NSIndexPath *)indexPath
{
    IXOpenEptPrcCell *cell = [tableView dequeueReusableCellWithIdentifier:
                              NSStringFromClass([IXOpenEptPrcCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXOpenShowSLCell *)configShowSLCellWithTableView:(UITableView *)tableView
                                         WithIndex:(NSIndexPath *)indexPath
{
    IXOpenShowSLCell *cell = [tableView dequeueReusableCellWithIdentifier:
                              NSStringFromClass([IXOpenShowSLCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

+ (IXOpenSLCell *)configOpenSLCellWithTableView:(UITableView *)tableView
                                         WithIndex:(NSIndexPath *)indexPath
{
    IXOpenSLCell *cell = [tableView dequeueReusableCellWithIdentifier:
                          NSStringFromClass([IXOpenSLCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
