//
//  IXQuoteLable.m
//  IXTest
//
//  Created by Evn on 16/12/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteLable.h"
#import "IXDataProcessTools.h"
#import "UILabel+LineSpace.h"

@interface IXQuoteLable()
{
    /**第一部分价格串*/
    UILabel *_part1Lbl;
    /**第二部分价格串*/
    UILabel *_part2Lbl;
    /**第三部分价格串*/
    UILabel *_part3Lbl;
    /**变化后总宽度*/
    float _sumWidth;
}

/**价格*/
@property (nonatomic, assign) float fPrice;
/**行情code*/
@property (nonatomic, assign) uint64_t symbolId;
/**颜色*/
@property(nonatomic,copy) DKColorPicker quoteColor;

@property (nonatomic, assign) QuoteLabelAlign labelAlign;

@end

@implementation IXQuoteLable

- (id)initWithFrame:(CGRect)frame WithAlign:(QuoteLabelAlign)align
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _labelAlign = align;
        
        float cellWidth = self.frame.size.width;
        _part1Lbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _part1Lbl.dk_textColorPicker = DKColorWithRGBs(0x99abba,0xffffff);
        _part1Lbl.text = @"--";
        _part1Lbl.frame = CGRectMake(0, 2, cellWidth/3, 17);
        _part1Lbl.font =  RO_REGU(20);
        
        _part2Lbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _part2Lbl.frame = CGRectMake(CGRectGetMaxX(_part1Lbl.frame), 0, cellWidth/3, 19);
        _part2Lbl.font = RO_REGU(24);

        _part3Lbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _part3Lbl.frame = CGRectMake(CGRectGetMaxX(_part2Lbl.frame), 0, cellWidth/3-25,  15);
        _part3Lbl.font = RO_REGU(18);

        [self addSubview:_part1Lbl];
        [self addSubview:_part2Lbl];
        [self addSubview:_part3Lbl];
    }
    return self;
}

- (void)setEqualFont
{
    CGRect frame = _part1Lbl.frame;
    frame.origin.y = 0;
    frame.size.height = 19;
    _part1Lbl.frame = frame;
    _part1Lbl.font = RO_REGU(24);

    _part2Lbl.font = RO_REGU(24);
    
    frame = _part3Lbl.frame;
    frame.origin.y = 0;
    frame.size.height = 19;
    _part3Lbl.frame = frame;
    _part3Lbl.font = RO_REGU(24);
    
    [UILabel changeWordSpaceForLabel:_part1Lbl WithSpace:-1];
    [UILabel changeWordSpaceForLabel:_part2Lbl WithSpace:-1];
    [UILabel changeWordSpaceForLabel:_part3Lbl WithSpace:-1];
}

- (void)refreshQuoteLabelWithSymbolModel:(IXSymbolM *)model
                                andPrice:(CGFloat)value
                           andQuoteColor:(DKColorPicker)quoteColor{

    _price = value;
    [self setFPrice:_price symbolModel:model];
    self.quoteColor = quoteColor;
}

- (void)setFPrice:(float)fPrice symbolModel:(IXSymbolM *)model {
    
    _fPrice = fPrice;
    NSString *strBuy = @"";

    NSString *strDigitFormat = [NSString stringWithFormat:@"%%.%ldf",(long)model.digits];
    strBuy = [NSString stringWithFormat:strDigitFormat,_fPrice];
    
    NSArray *arrPrice = [IXDataProcessTools getArrSplitPrice:fPrice symbolModel:model];
    _part1Lbl.text = arrPrice[0];
    _part2Lbl.text = arrPrice[1];
    _part3Lbl.text = arrPrice[2];
    if(fPrice == 0){
        _part1Lbl.text = @"--";
        _part2Lbl.text = @"";
        _part3Lbl.text = @"";
    }
    
    float fbuywidth1 = [IXQuoteLable getTextWidth:_part1Lbl.font textStr:_part1Lbl.text].width;
    float fbuywidth2 = [IXQuoteLable getTextWidth:_part2Lbl.font textStr:_part2Lbl.text].width;
    float fbuywidth3 = [IXQuoteLable getTextWidth:_part3Lbl.font textStr:_part3Lbl.text].width;
    
    _sumWidth = fbuywidth1 + fbuywidth2 + fbuywidth3;
    float originX = 0;
    
    switch ( _labelAlign ) {
        case Quote_Left:{
            originX = 0;
        }
            break;
        case Quote_Center:{
            originX = (CGRectGetWidth(self.frame) - _sumWidth)/2;
        }
            break;
        case Quote_Right:{
            originX = CGRectGetWidth(self.frame) - _sumWidth;
        }
            break;
        default:
            break;
    }
    
    _part1Lbl.frame = CGRectMake( originX, [self getLabelOrginY:_part1Lbl], fbuywidth1, [self getLabelHeight:_part1Lbl]);
    
    _part2Lbl.frame = CGRectMake(CGRectGetMaxX(_part1Lbl.frame), [self getLabelOrginY:_part2Lbl], fbuywidth2, [self getLabelHeight:_part2Lbl]);

    _part3Lbl.frame = CGRectMake(CGRectGetMaxX(_part2Lbl.frame), [self getLabelOrginY:_part3Lbl], fbuywidth3, [self getLabelHeight:_part3Lbl]);
}

- (CGFloat)getLabelHeight:(UILabel *)label
{
    return CGRectGetHeight(label.frame);
}

- (CGFloat)getLabelOrginY:(UILabel *)label
{
    return CGRectGetMinY(label.frame);
}

-(void)setQuoteColor:(DKColorPicker)quoteColor{
    _part1Lbl.dk_textColorPicker = quoteColor;
    _part2Lbl.dk_textColorPicker = quoteColor;
    _part3Lbl.dk_textColorPicker = quoteColor;
}

-(float)getLabelWidth
{
    return _sumWidth;
}

+ (CGSize)getTextWidth:(UIFont*)font textStr:(NSString*)text
{
    CGSize size =[text sizeWithAttributes:@{
                                            NSFontAttributeName:font
                                            }
                  ];
    return size;
}

@end
