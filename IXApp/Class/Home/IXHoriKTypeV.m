//
//  IXHoriKTypeV.m
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXHoriKTypeV.h"
#import "NSString+IX.h"

@interface IXHoriKTypeV ()

@property (nonatomic,strong)UIView *vernier;
@end
@implementation IXHoriKTypeV

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        self.dk_backgroundColorPicker = DKNavBarColor;
    }
    return self;
}

- (void)initUI
{
    NSArray *array = @[LocalizedString(@"分时"),
                       LocalizedString(@"日K"),
                       LocalizedString(@"周K"),
                       LocalizedString(@"月K"),
                       LocalizedString(@"1分"),
                       LocalizedString(@"5分"),
                       LocalizedString(@"15分"),
                       LocalizedString(@"30分"),
                       LocalizedString(@"60分"),
                       LocalizedString(@"4小时")];
    
    CGFloat x = 0.f;
    if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
        x = [[UIApplication sharedApplication] statusBarFrame].size.height;
    }
    CGFloat width = kScreenHeight - x - kBtomMargin;
    
    for (int i = 0; i < array.count; i++) {
        UIButton    * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(i * width / array.count, 0, width / array.count, 29 );
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString    * title = array[i];
        NSAttributedString  * normalAttTitle = [title attributeStringWithFontSize:12
                                                                        textColor:AutoNightColor(0x4c6072, 0xe9e9ea)];
        NSAttributedString  * seletettTitle = [title attributeStringWithFontSize:12
                                                                       textColor:UIColorHexFromRGB(0x99abba)];
        [btn setAttributedTitle:normalAttTitle forState:UIControlStateSelected];
        [btn setAttributedTitle:seletettTitle forState:UIControlStateNormal];
        btn.selected = i == 4;
        
        //此tag与埋点相关，若修改此tag，埋点kT数组需进行相应修改
        if (i < 4) {
            btn.tag = i;
        } else {
            btn.tag = i + 1;
        }
        [self addSubview:btn];
    }
    
    UIView *seqV = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - .5f, self.frame.size.width, .5f)];
    seqV.dk_backgroundColorPicker = DKLineColor;
    [self addSubview:seqV];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self vernier];
}

- (UIView *)vernier
{
    if (!_vernier) {
        CGFloat x = 0.f;
        if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
            x = [[UIApplication sharedApplication] statusBarFrame].size.height;
        }
        CGFloat width = kScreenHeight - x - kBtomMargin;
        _vernier = [[UIView alloc] initWithFrame:CGRectMake(4 * width/10.f, self.frame.size.height-2, width/10, 2)];
        _vernier.backgroundColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_vernier];
    }
    return _vernier;
}

- (void)btnClicked:(UIButton *)btn
{
    [self refreshBtnAppearence:btn];
    
    if (self.ktypeBtnClicked) {
        self.ktypeBtnClicked(btn.tag);
    }
}

- (void)setSelectedType:(IXKLineType)selectedType
{
    _selectedType = selectedType;
    UIButton    * btn = (UIButton *)[self viewWithTag:selectedType];
    [self refreshBtnAppearence:btn];
}

- (void)refreshBtnAppearence:(UIButton *)btn
{
    for (id obj in self.subviews) {
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)obj;
            button.selected = btn.tag == button.tag;
        }
    }
    
    [UIView animateWithDuration:.2 animations:^{
        NSInteger index = btn.tag;
        if (btn.tag > 3) {
            index --;
        }

        CGFloat x = 0.f;
        if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
            x = [[UIApplication sharedApplication] statusBarFrame].size.height;
        }
        CGFloat width = kScreenHeight - x - kBtomMargin;
        
        self.vernier.frame = CGRectMake(width/10*index, self.frame.size.height-2, width/10, 2);
    }];
}

@end

