//
//  IXQuoteThumbCell.h
//  IXApp
//
//  Created by Bob on 2016/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXSymbolM.h"
#import "IXQuoteM.h"

@protocol IXQuoteThumbCellDelegate <NSObject>

@required

- (void)responseToTradeWithDirection:(item_order_edirection)direction
                             WithTag:(NSInteger)tag;

@end

@interface IXQuoteThumbCell : UITableViewCell

@property (nonatomic, assign) id<IXQuoteThumbCellDelegate> thumbDelegate;

- (void)resetQuotePrice;

- (void)setSymbolModel:(NSDictionary *)model
        WithMarketName:(NSString *)marketName
        WithTradeState:(IXSymbolTradeState)state;


- (void)setQuotePriceInfo:(IXQuoteM *)priceInfo;

@end
