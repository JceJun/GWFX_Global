//
//  IXSmybolDetailMgr.m
//  IXApp
//
//  Created by Larry on 2018/5/16.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXSmybolDetailMgr.h"
#import "IXTradeMarketModel.h"
#import "IXDBSymbolMgr.h"
#import "IXDBGlobal.h"
#import "IXDBSymbolHotMgr.h"
//#import "IXSymListMgr.h"
#import "IXTCPRequest.h"
#import "IXTCPRequest_net.h"

#define OBJECT @"object"
#define STATUS @"status"

@implementation IXSmybolDetailMgr
SingletonM(IXSmybolDetailMgr);

- (instancetype)init
{
    self = [super init];
    if (self) {
//        /********************    产品(所有的/热门/自选)更新   ************************/
//        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CHANGE);
//
//        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_HOT_ADD);
//        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_HOT_LIST);
//        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_HOT_UPDATE);
//
//        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_ADD);
//        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_LIST);
//        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_DELETE);
    }
    return self;
}

-(void)setSymbolId:(NSString *)symbolId{
//    if (![symbolId isEqualToString:_symbolId]) {
        _symbolId = symbolId;
        
        _tradeModel = [[IXTradeMarketModel alloc] init];
        
        NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:[symbolId intValue]];
        IXSymbolM *model = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
        if (!model.contractSizeNew) {
            model.contractSizeNew = 1;
        }
        _tradeModel.symbolModel = model;
        
        _tradeState = [IXDataProcessTools symbolIsCanTrade:model];
        
        NSDictionary *dic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:[symbolId intValue]];
        _tradeModel.marketId = [dic integerForKey:kMarketId];
        
        
        NSDictionary *subDic = @{
                                 kID:@([_symbolId intValue]),
                                 kMarketId:@(_tradeModel.marketId),
                                 };
        NSArray *subArr = @[subDic];
        
        // 订阅产品组深度行情
        [[IXTCPRequest shareInstance] subscribeDetailPriceWithSymbolAry:subArr];
        
        // 订阅产品组最后一口价
        [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:subArr];
//    }
}

#pragma mark -
#pragma mark - IXQuoteDistribute delegate
- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if(cmd == CMD_QUOTE_SUB ||
       cmd == CMD_QUOTE_PUB){
//        [self refreashDynPrice:arr];
    }else if (cmd == CMD_QUOTE_CLOSE_PRICE){
//        [self refreahLastClosePrice:arr];
    }else if (cmd == CMD_QUOTE_UNSUB){
//        if(_detailModel.needReSubDynQuote){
//            _detailModel.needReSubDynQuote = NO;
//        }
    }
}


- (NSMutableArray *)symbolPosArr
{
    if(!_symbolPosArr ){
        _symbolPosArr = [NSMutableArray array];
    }
    return _symbolPosArr;
}


//        IXQuoteM *dic;
//        id value = [self dynQuoteDataModelWithSymbolId:model.id_p];
//        if( [value isKindOfClass:[NSDictionary class]] ){
//            dic = value[OBJECT];
//        }else if( [value isKindOfClass:[IXQuoteM class]] ){
//            dic = value;
//        }
//        if (dic) {
//            tradeModel.quoteModel = dic;
//        }
//        IXLastQuoteM *lastPrice = [self lcModelWithSymId:[dataDic integerForKey:kID]];
//        if ( lastPrice ) {
//            openVC.nLastClosePrice =  lastPrice.nLastClosePrice;
//        }
//        openVC.tradeModel = tradeModel;
    


//- (id)dynQuoteDataModelWithSymbolId:(NSInteger)symbolId
//{
//    NSString *symbolKey = [NSString stringWithFormat:@"%llu",symbolId];
//    IXQuoteM *model = [_detailModel.priceDic objectForKey:symbolKey];
//    if (0 == model.symbolId || 0 == model.nPrice) {
//        model = [IXDataProcessTools queryQuoteDataBySymbolId:symbolId];
//        if (0 == model.symbolId || 0 == model.nPrice) {
//            return nil;
//        }else{
//
//        }
//    }
//    return model;
//}



@end
