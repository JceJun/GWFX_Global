//
//  IXOpenMetricV.m
//  IXApp
//
//  Created by Bob on 2016/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOpenMetricV.h"

@interface IXOpenMetricV ()

@property (nonatomic, strong) UILabel *tipLbl;
@end

@implementation IXOpenMetricV

- (id)initWithFrame:(CGRect)frame WithImgName:(NSString *)imageName
{
    self = [super initWithFrame:frame];
    if(self){
        CGRect frame = CGRectMake( 20, 10, 18, 18);
        if ( [imageName isEqualToString:@"openRoot_btn_substract"] ) {
            frame.origin.y = 18.5;
            frame.size.height = 1;
        }
        UIImageView *markImg = [[UIImageView alloc] initWithFrame:frame];
        markImg.image = [UIImage imageNamed:imageName];
        [self addSubview:markImg];
        
        frame.origin.x = 0;
        frame.origin.y = 36;
        frame.size.width = 58;
        frame.size.height = 12;
        
        _tipLbl = [IXUtils createLblWithFrame:frame
                                     WithFont:RO_REGU(12)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x99abba
                                   dTextColor:0xff0000];
        [self addSubview:_tipLbl];
    }
    return self;
}

- (void)setStep:(NSString *)step
{
    _tipLbl.text = step;
}

@end
