//
//  IXOpenModel.h
//  IXApp
//
//  Created by Bob on 2016/12/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^getPriceRange)(NSString *minPrice,NSString *maxPrice);
typedef void(^getProfitRange)(NSString *minPrice,NSString *maxPrice);
typedef void(^getLossRange)(NSString *minPrice,NSString *maxPrice);

@interface IXOpenModel : NSObject

///////////////////////////
@property (nonatomic, assign) double nLastClosePrice;
@property (nonatomic, assign) BOOL refreashFeeQuote;

@property (nonatomic, assign,readonly) NSInteger parentId;
@property (nonatomic, assign,readonly) NSInteger marketId;

@property (nonatomic, assign) NSInteger cataId;
@property (nonatomic, assign) double commissionRate;

//显示止盈止损
@property (nonatomic, assign) NSInteger showSLCount;

//深度价格显示的个数
@property (nonatomic, assign) NSInteger showDeepPrcCount;

@property (nonatomic, copy) NSString *priceRangeStr;
@property (nonatomic, copy) NSString *stopRangeStr;
@property (nonatomic, copy) NSString *profitRangeStr;
///////////////////////////

///////////////////////////
@property (nonatomic, strong) NSDecimalNumber *minRequestPrice;
@property (nonatomic, strong) NSDecimalNumber *maxRequestPrice;

@property (nonatomic, strong) NSDecimalNumber *minProfit;
@property (nonatomic, strong) NSDecimalNumber *maxProfit;

@property (nonatomic, strong) NSDecimalNumber *minLoss;
@property (nonatomic, strong) NSDecimalNumber *maxLoss;

@property (nonatomic, strong) IXQuoteM *quoteModel;
@property (nonatomic, strong) IXSymbolM *symbolModel;

@property (nonatomic, strong)  NSString *orderType;
@property (nonatomic, assign) item_order_edirection orderDir;

//有行情才执行边界检查
@property (nonatomic, assign) BOOL noQuoteInfo;



//预计成交价／期望成交价
@property (nonatomic, strong) NSDecimalNumber *dealPrice;

//用户填写的挂单价
@property (nonatomic, strong) NSDecimalNumber *requestPrice;

//产品持仓数
@property (nonatomic, assign) double positionVolumes;

//基点
@property (nonatomic, strong) NSDecimalNumber *minPrice;
@property (nonatomic, strong) NSDecimalNumber *maxPrice;
@property (nonatomic, strong) NSDecimalNumber *stepPrice;

- (id)initWithSymbolModel:(IXSymbolM *)model
           WithPriceRange:(getPriceRange)price
          WithProfitRange:(getProfitRange)profit
            WithLossRange:(getLossRange)loss;

//设置价格范围
- (void)caculateRangePrice;

//限价买
- (void)caculateLimitBuy;

//限价卖
- (void)caculateLimitSell;

//止盈
- (void)caculateRangeTakeProfit;

//止损
- (void)caculateRangeStopLoss;
 

@end
