//
//  IXOpenDirCell.h
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^chooseOrderDir)(item_order_edirection dir);

@interface IXOpenDirCell : UITableViewCell

@property (nonatomic, copy) chooseOrderDir orderDirBlock;

@property (nonatomic, assign) item_order_edirection direction;

@property (nonatomic, assign) IXSymbolTradeState tradeState;

@end
