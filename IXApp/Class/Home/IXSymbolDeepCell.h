//
//  IXSymbolDeepCell.h
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXSymbolDeepCell : UITableViewCell

@property (nonatomic, assign) CGFloat cellContentHeight;

- (void)setBuyPrc:(NSString *)buyPrc
           BuyVol:(NSString *)buyVol
          SellPrc:(NSString *)sellPrc
          SellVol:(NSString *)sellVol;

@end
