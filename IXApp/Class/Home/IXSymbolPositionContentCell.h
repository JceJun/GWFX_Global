//
//  IXSymbolPositionContentCell.h
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"
#import "IXSymbolPositionM.h"

@interface IXSymbolPositionContentCell : IXClickTableVCell

@property (nonatomic, strong) IXSymbolPositionM *model;

@end
