//
//  IXMarketSymbolM.h
//  IXApp
//
//  Created by Bob on 2016/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#define POSITIONMAPSYMBOL @"positionMapSymbol"

typedef NS_ENUM( NSInteger, TABLE_TYPE) {
    TABLE_SUBSYMBOL,      //自选
    TABLE_HOTSYMBOL,      //热门商品
    TABLE_MAX,            //其他
};

typedef NS_ENUM( NSInteger, CELL_TYPE) {
    CELL_DETAIL,     //竖表单
    CELL_THUMB,      //缩略图
};

@interface IXMarketSymbolM : NSObject

////////////////////////////////////// 业务和UI需要的属性 ／／／／／／／／／／／／／／／／／／／／／／／／／／
/**
 表单类型 自选，热门，其他分类
 */
@property (nonatomic, assign) TABLE_TYPE tableType;

/**
 分类列表
 */
@property (nonatomic, strong) NSMutableArray *cataArr;
 
/**
 选择的分类行数（UI标记使用)
 */
@property (nonatomic, assign) NSInteger chooseSymbolCataPage;
////////////////////////////////////// 业务和UI需要的属性 ／／／／／／／／／／／／／／／／／／／／／／／／／／


////////////////////////////////////// 服务端通知对应数据更新 ／／／／／／／／／／／／／／／／／／／／／／／／／／
/**
 刷新自选列表
 */
@property (nonatomic,copy)void(^updateSubSym)();

/**
 刷新热门列表
 */
@property (nonatomic,copy)void(^updateHotSym)();

/**
 刷新所有产品：账号组信息更新，更新具体产品
 */
@property (nonatomic,copy)void(^updateSym)();


/**
 刷新产品分类
 */
@property (nonatomic,copy)void(^updateCata)();

/**
 账号信息更新：账户类型切换
 */
@property (nonatomic,copy)void(^updateAccount)();

/**
 更新头像
 */
@property (nonatomic,copy)void(^updateUserInfo)();

/**
 仓位更新
 */
@property (nonatomic,copy)void(^updatePosTip)(proto_position_add *pb);

/**
 网络状态更新
 */
@property (nonatomic,copy)void(^updateNetStatus)(IXTCPConnectStatus status);
////////////////////////////////////// 服务端通知对应数据更新 ／／／／／／／／／／／／／／／／／／／／／／／／／／


////////////////////////////////////// 用户操作对应数据更新／／／／／／／／／／／／／／／／／／／／／／／／／／
/**
 用户选择自选:刷新当前的商品
 */
- (void)setSubSymbol;

/**
 用户选择热门:刷新当前的商品
 */
- (void)setHotSymbol;

/**
 刷新分类信息
 */
- (void)refreashCataArr;
 
@end
