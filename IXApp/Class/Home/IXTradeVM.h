//
//  IXTradeVM.h
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IXTradeSymbolV.h"
#import "IXTradeTypeV.h"
#import "IXTradeOperatV.h"
#import "IXOpenDirectV.h"

typedef NS_ENUM(NSInteger, ROWNAME)
{
    ROWNAMEPRICE = 1,
    ROWNAMEVOLUME,
    ROWNAMEPROFIT,
    ROWNAMELOSS,
};

@protocol IXTradeVMDelegate <NSObject>

@required
- (void)chooseTradeType;

- (void)chooseExpireType;

- (void)updateVolume:(NSString *)volume WithTag:(ROWNAME)tag;

- (void)updateStoplossWithTag:(ROWNAME)tag;

- (void)updateTakeprofitWithTag:(ROWNAME)tag;

- (void)updateDirection:(item_order_edirection)direction;

- (void)caculatePrice;

@optional
- (void)setForceClose:(BOOL)force;
@end

@interface IXTradeVM : NSObject

@property (nonatomic, strong) UIScrollView *contentScroll;

@property (nonatomic ,assign) double nLastClosePrice;


@property (nonatomic, assign) id<IXTradeVMDelegate> tradeVMDelegate;
@property (nonatomic ,strong) IXTradeSymbolV     *symbolView;
@property (nonatomic ,strong) IXTradeTypeV       *typeView;
@property (nonatomic ,strong) IXOpenDirectV   *directionView;
@property (nonatomic ,strong) IXTradeOperatV  *dealPriceView;
@property (nonatomic ,strong) IXTradeOperatV  *volumeView;

- (void)resetSybolModel:(IXSymbolM *)symbolModel;
- (void)resetQuoteModel:(IXQuoteM *)dataModel;

- (void)updatePlaceOlderContent:(NSString *)content WithTag:(ROWNAME)tag;

//更新订单方向
- (void)resetDirection:(item_order_edirection)direction;

//刷新价格
- (void)resetPriceWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange;


//刷新单位
- (void)resetVolUnitName:(NSString *)unitName;

//刷新手数
- (void)resetVolumeWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange;

//刷新止盈
- (void)resetProfitWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange;

//刷新止损
- (void)resetLossWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange;

@end
