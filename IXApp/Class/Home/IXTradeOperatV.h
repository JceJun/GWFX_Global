//
//  IXTradeOperatV.h
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, Click_Type){
    Click_Subtract,
    Click_Add,
};

typedef void(^endInputValue)(NSString *value);
 
typedef void(^clickBtnWithType)(Click_Type type,NSInteger tag);

@interface IXTradeOperatV : UIView

@property (nonatomic, copy) clickBtnWithType clickBlock;

@property (nonatomic, copy) endInputValue textContent;

@property (nonatomic, assign) NSInteger digit;

@property (nonatomic, strong) UITextField *contentTF;

- (void)updateUIWithOrderType:(NSString *)orderType;

- (void)setTitleContent:(NSString *)content;

- (void)setDealPrice:(NSString *)price;

//设置步长
- (void)setStopLevel:(NSString *)stopLevel;

//设置单位
- (void)setUnitName:(NSString *)unitName;

//设置范围
- (void)updateWithLeftRange:(NSString *)left
             WithRightRange:(NSString *)right;


//设置输入框的值
- (void)updatePlaceOlderValue:(NSString *)value;

//设置标题颜色
- (void)setTitleColor:(UIColor *)color;

//设置输入框颜色以及是否可以操作
- (void)setTextFieldColor:(UIColor *)color isEnableEdit:(BOOL)enable;

@end
