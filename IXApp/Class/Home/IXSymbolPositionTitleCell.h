//
//  IXSymbolPositionTitleCell.h
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXSymbolPosTitleM.h"

@interface IXSymbolPositionTitleCell : UITableViewCell

@property (nonatomic, strong) IXSymbolPosTitleM *model;

@end
