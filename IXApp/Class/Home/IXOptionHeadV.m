//
//  IXOptionHeadV.m
//  IXApp
//
//  Created by Bob on 2016/12/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOptionHeadV.h"

@implementation IXOptionHeadV

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if ( self ) {
        
        CGFloat height = CGRectGetHeight(frame);
        
        UIView *headView = [[UIView alloc] initWithFrame:self.bounds];
        headView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        [self addSubview:headView];
        
        UIButton *allCataBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [headView addSubview:allCataBtn];
        allCataBtn.frame = CGRectMake( 21.5, 0, 35, height);

        [allCataBtn setTitle:LocalizedString(@"全部") forState:UIControlStateNormal];
//        [allCataBtn setImage:[UIImage imageNamed:@"矩形-1"] forState:UIControlStateNormal];
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            [allCataBtn setTitleColor:UIColorHexFromRGB(0x8395a4) forState:UIControlStateNormal];
        }else{
            [allCataBtn setTitleColor:UIColorHexFromRGB(0x4c6072) forState:UIControlStateNormal];
        }
        allCataBtn.dk_tintColorPicker = DKGrayTextColor;
        [allCataBtn.titleLabel setFont:PF_MEDI(12)];
        allCataBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        allCataBtn.tag = FilterType_All;
        

//        CGFloat width = [IXEntityFormatter getContentWidth:LocalizedString(@"全部") WithFont:PF_MEDI(12)];
//        [allCataBtn setTitleEdgeInsets:UIEdgeInsetsMake( 10.5, -4, 10.5, 35 - width)];
//        [allCataBtn setImageEdgeInsets:UIEdgeInsetsMake( 15.5, 28, 4, 3)];
        
        CGFloat width = [IXEntityFormatter getContentWidth:LocalizedString(@"最新价")
                                                  WithFont:PF_MEDI(12)];
        
        UIButton *newPriceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [headView addSubview:newPriceBtn];
        newPriceBtn.frame = CGRectMake( kScreenWidth - (100 + width),  0, width,  height);
        [newPriceBtn setTitle:LocalizedString(@"最新价") forState:UIControlStateNormal];
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            [newPriceBtn setTitleColor:UIColorHexFromRGB(0x8395a4) forState:UIControlStateNormal];
        }else{
            [newPriceBtn setTitleColor:UIColorHexFromRGB(0x4c6072) forState:UIControlStateNormal];
        }
        [newPriceBtn.titleLabel setFont:PF_MEDI(12)];
        newPriceBtn.tag = 1;
        
        
        UIButton *swapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [headView addSubview:swapBtn];
        swapBtn.frame = CGRectMake( kScreenWidth - 60, 0, width,  33);
        [swapBtn setTitle:LocalizedString(@"涨跌幅.") forState:UIControlStateNormal];
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            [swapBtn setTitleColor:UIColorHexFromRGB(0x8395a4) forState:UIControlStateNormal];
        }else{
            [swapBtn setTitleColor:UIColorHexFromRGB(0x4c6072) forState:UIControlStateNormal];
        }
        [swapBtn.titleLabel setFont:PF_MEDI(12)];
        swapBtn.tag = 2;
        
        width = [IXEntityFormatter getContentWidth:LocalizedString(@"涨跌幅.") WithFont:PF_MEDI(12)];
    }
    return self;
}

- (void)allCataBtnClicked:(UIButton *)btn
{
    if(self.orderKind){
        self.orderKind(btn.tag);
    }
}

@end
