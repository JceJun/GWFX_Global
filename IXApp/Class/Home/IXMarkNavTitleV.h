//
//  IXMarkNavTitleV.h
//  IXApp
//
//  Created by mac on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^responseToTap)(void);

@interface IXMarkNavTitleV : UIView

@property (nonatomic, strong) responseToTap tapContent;

- (void)setTitleContent:(NSString *)title;

@end
