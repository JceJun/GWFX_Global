//
//  IXOpenResultCellA.m
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenResultCellA.h"

@implementation IXOpenResultCellA

- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 80)/2, 45, 80, 80)];
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (UILabel *)labelU
{
    if (!_labelU) {
        _labelU = [IXUtils createLblWithFrame:CGRectMake( 0, 145, kScreenWidth, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x4c6072
                                   dTextColor:0xffffff];
        [self.contentView addSubview:_labelU];
    }
    return _labelU;
}

- (UILabel *)labelD
{
    if (!_labelD) {
        _labelD = [IXUtils createLblWithFrame:CGRectMake( 0, 170, kScreenWidth, 15)
                                     WithFont:PF_MEDI(12)
                                    WithAlign:NSTextAlignmentCenter
                                    wTextColor:0x99abba
                                   dTextColor:0x8395a4];
        [self.contentView addSubview:_labelD];
    }
    return _labelD;
}

@end
