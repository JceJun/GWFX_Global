//
//  IXSymbolDeepCell.m
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSymbolDeepCell.h"
#import "IXTradeDeepV.h"

@interface IXSymbolDeepCell ()

@property (nonatomic, strong) IXTradeDeepV *deepPrice;

@end

@implementation IXSymbolDeepCell
 

- (void)setCellContentHeight:(CGFloat)cellHeight
{
    _cellContentHeight = cellHeight;
    [self.contentView addSubview:self.deepPrice];
}


- (void)setTag:(NSInteger)tag
{
    [super setTag:tag];
    [_deepPrice setDeepCount:(tag+1)];
}

- (void)setBuyPrc:(NSString *)buyPrc
           BuyVol:(NSString *)buyVol
          SellPrc:(NSString *)sellPrc
          SellVol:(NSString *)sellVol
{
    [_deepPrice setBuyPrc:buyPrc BuyVol:buyVol SellPrc:sellPrc SellVol:sellVol];
}


- (IXTradeDeepV *)deepPrice
{
    if ( !_deepPrice ) {
        CGRect frame = CGRectMake( 0, 0, kScreenWidth, _cellContentHeight);
        _deepPrice = [[IXTradeDeepV alloc] initWithFrame:frame WithCount:self.tag];
    }
    return _deepPrice;
}
@end
