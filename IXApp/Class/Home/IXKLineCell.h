//
//  IXKLineCell.h
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXKLineType.h"

@interface IXKLineCell : UITableViewCell

@property (nonatomic,copy)void(^btnClick)(IXKLineType type);

- (void)showDisplayView:(UIView *)view
                   type:(IXKLineType)type;
@end

