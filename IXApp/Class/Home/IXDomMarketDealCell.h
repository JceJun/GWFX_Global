//
//  IXDomMarketDealCell.h
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXDomDealM.h"

#define CHOOSETAG 100

typedef void(^chooseRequestVol)();
typedef void(^inputVol)(NSString *vol);
typedef void(^postOrder)(item_order_edirection dir);

@interface IXDomMarketDealCell : UITableViewCell

@property (nonatomic, strong) IXDomDealM *model;

@property (nonatomic, strong) UIImageView *backClickView;

@property (nonatomic, strong) NSString *vol;

@property (nonatomic, strong) chooseRequestVol requstVol;

@property (nonatomic, strong) inputVol inVol;

@property (nonatomic, strong) postOrder addOrder;

@property (nonatomic, copy) NSString *unitName;
 

@end
