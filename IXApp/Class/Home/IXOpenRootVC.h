//
//  IXOpenRootVC.h
//  IXApp
//
//  Created by bob on 16/11/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXOpenModel.h"

@class IXTradeMarketModel;
@interface IXOpenRootVC : IXDataBaseVC

@property (nonatomic, strong) IXOpenModel *openModel;
@property (nonatomic, strong) IXTradeMarketModel *tradeModel;

@end
