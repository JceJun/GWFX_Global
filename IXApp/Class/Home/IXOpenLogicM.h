//
//  IXOpenLogicM.h
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IXTradeVM.h"
#import "IXOpenRootVC.h"

@interface IXOpenLogicM : NSObject

+ (void)updateTakeprofitWithTag:(ROWNAME)tag WithRoot:(IXOpenRootVC *)root;

+ (void)updateStoplossWithTag:(ROWNAME)tag WithRoot:(IXOpenRootVC *)root;


@end
