//
//  IXDomDealM.m
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDomDealM.h"
#import "IXUserDefaultM.h"

#import "NSString+FormatterPrice.h"
#import "NSArray+Expection.h"
#import "IXEntityFormatter.h"

@interface IXDomDealM ()

@property (nonatomic, strong) dispatch_source_t timer;

@property (nonatomic, assign) NSInteger preRow;

@property (nonatomic, assign) item_order_edirection preDir;

//@property (nonatomic, assign) NSInteger parentId;

@end

@implementation IXDomDealM
#pragma mark -----
#pragma mark life cycle
- (id)init
{
    if ( self = [super init] ) {
        _isVolNum = ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount );
        _preRow = -1;
        _preDir = -1;
        
        IXTradeData_listen_regist(self, PB_CMD_POSITION_ADD);
        
        IXTradeData_listen_regist(self, PB_CMD_ORDER_ADD);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_LIST);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_CANCEL);
    }
    return self;
}


- (void)dealloc
{
    NSLog(@"----%s---",__func__);
    if (_timer) {
        dispatch_source_cancel(_timer);
        _timer = nil;
    }
    IXTradeData_listen_resign(self, PB_CMD_POSITION_ADD);
    
    IXTradeData_listen_resign(self, PB_CMD_ORDER_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_LIST);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_CANCEL);
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath, PB_CMD_POSITION_ADD)){
        proto_position_add *pb = ((IXTradeDataCache *)object).pb_position_add;
        if ( pb.result == 0 && (pb.position.symbolid == _showM.symbolModel.id_p) ) {
            NSString *price = [NSString stringWithFormat:@"%lf", pb.position.openPrice];
            price = [NSString formatterPrice:price WithDigits:_showM.symbolModel.digits];
            
            NSString *vol = @"";
            double posVol = pb.position.volume;
            if ( !_isVolNum && 0 != _showM.symbolModel.contractSizeNew) {
                posVol = posVol/_showM.symbolModel.contractSizeNew;
            }
            
            if ( posVol < 1000 ) {
                
                if ( !_isVolNum && 0 != _showM.symbolModel.contractSizeNew) {
                    vol = [NSString stringWithFormat:@"%.2lf",posVol];
                }else{
                    vol = [NSString formatterPrice:[NSString stringWithFormat:@"%f",posVol] WithDigits:_showM.symbolModel.volDigits];
                }
                
            }else if( posVol >= 1000.f && posVol < 1000000.f ){
                
                if ( !_isVolNum && 0 != _showM.symbolModel.contractSizeNew) {
                    vol = [NSString stringWithFormat:@"%.2lfK",posVol/1000.f];
                }else{
                    vol = [NSString stringWithFormat:@"%.lfK",posVol/1000.f];
                }
            }
            else if( posVol >= 1000000.f ){
                
                if ( !_isVolNum && 0 != _showM.symbolModel.contractSizeNew) {
                    vol = [NSString stringWithFormat:@"%.2fM",posVol/1000000.f];
                }else{
                    vol = [NSString stringWithFormat:@"%.1fM",posVol/1000000.f];
                }
            }
            if (self.notiDomSuc) {
                self.notiDomSuc(price, vol);
            }
        }
    }else if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_ADD)) {
        proto_order_add  *pb = ((IXTradeDataCache *)object).pb_order_add;
        if ( (pb.order.symbolid == _showM.symbolModel.id_p) ) {
            if (pb.result == 0 ) {
                if (pb.order.type == item_order_etype_TypeLimit ||
                    pb.order.type == item_order_etype_TypeStop) {
                    _posArr = [IXTradeDataCache shareInstance].pb_cache_order_list;
                    [self refreashCount];
                }
            }else if(pb.result != 0 ){
                if (pb.order.status == item_order_estatus_StatusRejected
                    || pb.order.status == item_order_estatus_StatusCancelled ) {
                    if ( self.notiDomFail) {
                        self.notiDomFail([IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%d",pb.result]]);
                    }
                }
            }
        }
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_CANCEL)) {
        proto_order_cancel  *pb = ((IXTradeDataCache *)object).pb_order_cancel;
        if (pb.result == 0 && (pb.order.symbolid == _showM.symbolModel.id_p) ) {
            if (pb.order.type == item_order_etype_TypeLimit ||
                pb.order.type == item_order_etype_TypeStop) {
                
                _posArr = [IXTradeDataCache shareInstance].pb_cache_order_list;
                [self refreashCount];
            }
        }
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_UPDATE)) {
        proto_order_update  *pb = ((IXTradeDataCache *)object).pb_order_update;
        if ( (pb.order.symbolid == _showM.symbolModel.id_p) ) {
            if(pb.result != 0 ){
                if (pb.order.status == item_order_estatus_StatusRejected
                    || pb.order.status == item_order_estatus_StatusCancelled ) {
                    if ( self.notiDomFail) {
                        self.notiDomFail([IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%u",pb.result]]);
                    }
                }
            }
        }
    }

}

- (void)setShowM:(IXTradeMarketModel *)showM
{
    _showM = showM;
    
    _requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:[NSString stringWithFormat:@"%f",_showM.symbolModel.volumesMin] WithDigits:_showM.symbolModel.volDigits]];
    if ( !_isVolNum && _showM.symbolModel.contractSizeNew ) {
        _requestVolume =  [NSDecimalNumber decimalNumberWithString:
                           [NSString stringWithFormat:@"%.2lf",_showM.symbolModel.volumesMin/
                            _showM.symbolModel.contractSizeNew]];
    }
    _showVolume = _requestVolume;
}

//刷新预计成交价
- (void)resetDeepPrice
{
    NSDecimalNumber *price = [IXRateCaculate caculateMayDealPriceWithVolume:[_showM.requestVolume longValue]
                                                                    WithDir:_showM.direction
                                                                  WithQuote:_showM.quoteModel
                                                                 WithDigits:_showM.symbolModel.digits];
    
    NSString *priceStr = [NSString formatterPrice:[price stringValue]
                                       WithDigits:_showM.symbolModel.digits];
    _showM.requestPrice = [NSDecimalNumber decimalNumberWithString:priceStr];
}

- (NSTimeInterval)getCurrentTimeRelationServerTime
{
    
    long serverTime = [[[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime] longValue];
    
    long savsDateTime = [[[NSUserDefaults standardUserDefaults] objectForKey:IXLocaleTime] longValue];
    long curDateTime = [[NSDate date] timeIntervalSince1970];
    
    return (curDateTime - savsDateTime + serverTime);
}


- (void)refreashRequestPrice
{
    if ([_requestType isEqualToString:MARKETORDER]) {
//        NSInteger parentId = [IXDataProcessTools queryParentIdBySymbolId:_showM.symbolModel.id_p];
        if (STOCKID == _showM.parentId) {
            _requestPrice = [NSDecimalNumber decimalNumberWithString:
                             [@(_showM.quoteModel.nPrice) stringValue]];
        }else{
            if ( self.requestDir == item_order_edirection_DirectionBuy ) {
                _requestPrice = [NSDecimalNumber decimalNumberWithString:_showM.quoteModel.BuyPrc[0]];
            }else {
                _requestPrice = [NSDecimalNumber decimalNumberWithString:_showM.quoteModel.SellPrc[0]];
            }
        }
    }
}

- (proto_order_add *)packageOrder
{
    proto_order_add *proto = [[proto_order_add alloc] init];
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    
    item_order *order = [[item_order alloc] init];
    order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    order.symbol  =  _showM.symbolModel.name;
    order.symbolid = _showM.symbolModel.id_p;
    order.createTime =  [self getCurrentTimeRelationServerTime];
    order.clientType = [IXUserInfoMgr shareInstance].itemType;
    order.direction = self.requestDir;
    if ( !_isVolNum && self.showM.symbolModel.contractSizeNew ) {
        order.requestVolume = [self.requestVolume doubleValue] * self.showM.symbolModel.contractSizeNew;
    }else{
        order.requestVolume = [self.requestVolume doubleValue];
    }
    
    if ([self.requestType isEqualToString:MARKETORDER]) {
        order.priceRange = 10000;
        order.refPrice = [self.requestPrice doubleValue];
        order.type = item_order_etype_TypeOpen;
    } else {
        if ([self.requestType isEqualToString:LIMITORDER]) {
            order.type = item_order_etype_TypeLimit;
        } else {
            order.type = item_order_etype_TypeStop;
        }
        order.requestPrice =  [self.requestPrice doubleValue];
    }
    order.expireType = item_order_eexpire_ExpireDaily;
    proto.order = order;
    return proto;
}

- (void)checkTimeOut:(NSInteger)row WithDir:(item_order_edirection)dir
{
    //首先取消定时器；
    //如果第一次选中和第二次选中不一致，则重新计时
    //如果一致，则直接提交订单
    @synchronized (self) {
        [self stopTimer];
        
        if (_preDir != dir || _preRow != row) {
            _preRow = row;
            _preDir = dir;
            [self startTimer];
        }
    }
}

- (void)startTimer
{
    if (!_timer) {
        _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
        dispatch_time_t start = dispatch_time(DISPATCH_TIME_NOW, 3* NSEC_PER_SEC);
        dispatch_source_set_timer(_timer, start, 3 * NSEC_PER_SEC, 1);
        weakself;
        dispatch_source_set_event_handler(_timer, ^{
            [weakSelf timeOutAction];
        });
    }
    dispatch_resume(_timer);
}

-(void)stopTimer
{
    if (_timer) {
        dispatch_source_cancel(_timer);
        _timer = nil;
    }
    _preDir = -1;
    _preRow = -1;
}

- (void)timeOutAction
{
    [self stopTimer];

    [_chooseStatusArr removeAllObjects];
    for ( int i = 0; i < 10; i++ ) {
        [_chooseStatusArr addObject:@[@(NO),@(NO)]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ( self.notiUpdate ) {
            self.notiUpdate();
        }
    });
}

#pragma mark --
#pragma mark repsonse user action
- (void)chooseWithRow:(NSInteger)row WithDir:(item_order_edirection)dir
{
    [self checkTimeOut:row WithDir:dir];
    
    for ( int i = 0; i < _chooseStatusArr.count; i++ ) {
        NSMutableArray *arr = [NSMutableArray arrayWithArray:
                               _chooseStatusArr[i]];
        if ( i == row ) {
            
            //置位相反方向的值(初始值为NO)。依赖了宏定义，最好用更灵活的写法
            NSInteger type = (dir - 1);
            [arr replaceObjectAtIndex:(type + 1)%2 withObject:@(NO)];
            
            //点击两次则下单
            if ( [[arr objectAtIndex:type] boolValue] ) {
                
                [arr replaceObjectAtIndex:type withObject:@(NO)];
                
                //下单参数
                _requestDir = dir;
                
                if ( dir == item_order_edirection_DirectionBuy ) {
                    //买入:限价单层级6，停损单层级4
                    if( i >= 0 && i <= 3 ){
                        
                        if ( _showM.quoteModel.BuyPrc.count > 4 - i ) {
                            //停损
                            _requestPrice = [NSDecimalNumber decimalNumberWithString:
                                             _showM.quoteModel.BuyPrc[4-i]];
                            _requestType = STOPORDER;
                        }else{
                            _requestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
                        }
                        
                    }else{
                        //限价
                        if ( i == 4 ) {
                            _requestPrice = [NSDecimalNumber decimalNumberWithString:
                                             _showM.quoteModel.BuyPrc[0]];
                        }else{
                            
                            
                            if ( _showM.quoteModel.SellPrc.count > i- 5 ) {
                                _requestPrice = [NSDecimalNumber decimalNumberWithString:
                                                 _showM.quoteModel.SellPrc[i-5]];
                            }else{
                                _requestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
                            }
                            
                            
                        }
                        _requestType = LIMITORDER;
                    }
                    
                }else{
                    //卖出:限价单层级6，停损单层级4
                    if( i >= 0 && i <= 5 ){
                        //限价
                        if ( i == 5 ) {
                            _requestPrice = [NSDecimalNumber decimalNumberWithString:
                                             _showM.quoteModel.SellPrc[0]];
                        }else{
                            if ( _showM.quoteModel.BuyPrc.count > 4 - i ) {
                                //停损
                                _requestPrice = [NSDecimalNumber decimalNumberWithString:
                                                 _showM.quoteModel.BuyPrc[4-i]];
                            }else{
                                _requestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
                            }
                        }
                        _requestType = LIMITORDER;
                    }else{
                        //停损
                        if ( _showM.quoteModel.SellPrc.count > i- 5 ) {
                            _requestPrice = [NSDecimalNumber decimalNumberWithString:
                                             _showM.quoteModel.SellPrc[i-5]];
                        }else{
                            _requestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
                        }
                        _requestType = STOPORDER;
                        
                    }
                }
                
                if ( self.notiPostOrder ) {
                    self.notiPostOrder();
                }
            }else{
                //记录为选中状态
                [arr replaceObjectAtIndex:type withObject:@(YES)];
            }
        }else{
            
            [arr replaceObjectAtIndex:0 withObject:@(NO)];
            [arr replaceObjectAtIndex:1 withObject:@(NO)];
        }
        
        [_chooseStatusArr replaceObjectAtIndex:i withObject:arr];
    }
}

- (void)setMaxVolWithQuote
{
    _maxVol = 0;
    for ( NSInteger i = 0; i < _showM.quoteModel.BuyVol.count; i++ ) {
        if ( _maxVol < [_showM.quoteModel.BuyVol[i] doubleValue] ) {
            _maxVol =  [_showM.quoteModel.BuyVol[i] doubleValue];
        }
        
        if ( _showM.quoteModel.SellVol.count > i ) {
            if ( _maxVol < [_showM.quoteModel.SellVol[i] doubleValue] ) {
                _maxVol =  [_showM.quoteModel.SellVol[i] doubleValue];
            }
        }
    }
    [self refreashCount];
}

- (void)refreashPrcRelPosBuy:(NSInteger)index Type:(NSInteger)type
{
    NSMutableArray *res = [[self.prcRelPosBuyArr objectAtIndex:index] mutableCopy];
    if (type > 0) {
        res[0] = @([res[0] integerValue] + 1);
    }else if(type == 0){
        res[1] = @([res[1] integerValue] + 1);
    }else{
        res[2] = @([res[2] integerValue] + 1);
    }
    [self.prcRelPosBuyArr replaceObjectAtIndex:index withObject:res];

}

- (void)refreashPrcRelPosSell:(NSInteger)index Type:(NSInteger)type
{
    NSMutableArray *res = [[self.prcRelPosSellArr objectAtIndex:index] mutableCopy];
    if (type > 0) {
        res[0] = @([res[0] integerValue] + 1);
    }else if(type == 0){
        res[1] = @([res[1] integerValue] + 1);
    }else if(type < 0){
        res[2] = @([res[2] integerValue] + 1);
    }
    [self.prcRelPosSellArr replaceObjectAtIndex:index withObject:res];
}

- (void)refreashCount
{
    [self resetPrcArr];
    for ( item_order *order in _posArr ) {
        if ( order.symbolid == _showM.symbolModel.id_p ) {
            if ( order.requestPrice >= [_showM.quoteModel.BuyPrc[0] doubleValue] ) {
                for( NSInteger i = _showM.quoteModel.BuyPrc.count - 1; i >= 0; i-- ){
                    NSInteger tag = 4 - i;
                    if ( order.requestPrice > [_showM.quoteModel.BuyPrc[i] doubleValue] ) {
                        if (order.direction == item_order_edirection_DirectionBuy) {
                            [self refreashPrcRelPosBuy:tag Type:1];
                        }else{
                            [self refreashPrcRelPosSell:tag Type:1];
                        }
                        break;
                    }else if ( order.requestPrice == [_showM.quoteModel.BuyPrc[i] doubleValue] ){
                        if (order.direction == item_order_edirection_DirectionBuy) {
                            [self refreashPrcRelPosBuy:tag Type:0];
                        }else{
                            [self refreashPrcRelPosSell:tag Type:0];
                        }
                        break;
                    }
                }
            }else{
                for( NSInteger i = 0; i < _showM.quoteModel.SellPrc.count; i++ ){
                    NSInteger tag = 5 + i;
                    if ( order.requestPrice > [_showM.quoteModel.SellPrc[i] doubleValue] ) {
                        if (order.direction == item_order_edirection_DirectionBuy) {
                            [self refreashPrcRelPosBuy:tag Type:1];
                        }else{
                            [self refreashPrcRelPosSell:tag Type:1];
                        }
                        break;
                    }else if ( order.requestPrice == [_showM.quoteModel.SellPrc[i] doubleValue] ){
                        if (order.direction == item_order_edirection_DirectionBuy) {
                            [self refreashPrcRelPosBuy:tag Type:0];
                        }else{
                            [self refreashPrcRelPosSell:tag Type:0];
                        }
                        break;
                    }else{
                        if ( i == _showM.quoteModel.SellPrc.count - 1 ) {
                            if (order.direction == item_order_edirection_DirectionBuy) {
                                [self refreashPrcRelPosBuy:tag Type:-1];
                            }else{
                                [self refreashPrcRelPosSell:tag Type:-1];
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
}

- (void)resetPrcArr
{
    [self.prcRelPosBuyArr removeAllObjects];
    [self.prcRelPosSellArr removeAllObjects];
    for ( NSInteger i = 0; i < 10; i++ ) {
        [self.prcRelPosBuyArr addObject:@[@(0),@(0),@(0)]];
        [self.prcRelPosSellArr addObject:@[@(0),@(0),@(0)]];
    }
}

//获取深度价格
- (NSString *)getDeepPrcWithRow:(NSInteger)deepRow
{
    NSString *deepPrc = @"-";
    if( deepRow >= 0 && deepRow <= 4 ){
        //买入价数据源是递增的，而UI显示需要递减显示；所以做逆序处理
        if(_showM.quoteModel.BuyPrc.count > 4 - deepRow){
            deepPrc = _showM.quoteModel.BuyPrc[4 - deepRow];
        }
    }else if ( deepRow >= 5 && deepRow <= 9 ){
        deepRow = deepRow - 5;
        if( _showM.quoteModel.SellPrc.count > deepRow){
            deepPrc = _showM.quoteModel.SellPrc[deepRow];
        }
    }
    return deepPrc;
}

//获取买入手数
- (NSString *)getBuyVolWithRow:(NSInteger)deepRow
{
    NSString *buyVol = @"0";
    if( deepRow >= 0 && deepRow <= 4 ){
        //买入价数据源是递增的，而UI显示需要递减显示；所以做逆序处理
        if(_showM.quoteModel.BuyVol.count > 4 - deepRow){
            buyVol = _showM.quoteModel.BuyVol[4 - deepRow];
        }
    }
    return buyVol;
}

//获取卖出手数
- (NSString *)getSellVolWithRow:(NSInteger)deepRow
{
    NSString *selVol = @"0";
    if ( deepRow >= 5 && deepRow <= 9 ){
        deepRow = deepRow - 5;
        if( _showM.quoteModel.SellVol.count > deepRow){
            selVol = _showM.quoteModel.SellVol[deepRow];
        }
    }
    return selVol;
}

//获取选中状态
- (DOMSTATUS)getChooseStatusWithRow:(NSInteger)deepRow
{
    DOMSTATUS status = DOMSTATUSUNCHOOSE;
    NSArray *arr = self.chooseStatusArr[deepRow];
    if ( [[arr objectAtIndex:0] boolValue] ) {
        status = DOMSTATUSCHOOSEBUY;
    }else if ( [[arr objectAtIndex:1] boolValue] ){
        status = DOMSTATUSCHOOSESELL;
    }else{
        status = DOMSTATUSUNCHOOSE;
    }
    return status;
}

//////////////////////////////////////////

- (NSMutableArray *)showVolArr
{
    if ( !_showVolArr ) {
        _showVolArr = [[NSMutableArray array] mutableCopy];
        _volArr = [[NSMutableArray array] mutableCopy];
    }else{
        [_showVolArr removeAllObjects];
        [_volArr removeAllObjects];
    }
    
    double min = _showM.symbolModel.volumesMin;
    double max = _showM.symbolModel.volumesMax;
    
    uint32_t conSize = 1;
    if (!_isVolNum) {
        conSize = _showM.symbolModel.contractSizeNew ?  _showM.symbolModel.contractSizeNew : 1;
    }
    
    NSDecimalNumber *comDec = [NSDecimalNumber decimalNumberWithString:
                               [@(conSize * [_showVolume doubleValue]) stringValue]];
    if ( [comDec doubleValue] != min ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",min]];
        if ( _isVolNum || ( 0 == _showM.symbolModel.contractSizeNew ) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",min] WithDigits:_showM.symbolModel.volDigits] withDigits:_showM.symbolModel.volDigits]];
        }else{
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%.2lf",min/_showM.symbolModel.contractSizeNew] WithDigits:2] withDigits:2]];
        }
    }
    
    if ( max > min * 10 && [comDec doubleValue] != (min * 10) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 10)]];
        if ( _isVolNum || ( 0 == _showM.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 10)] WithDigits:_showM.symbolModel.volDigits] withDigits:_showM.symbolModel.volDigits]];
        }else if( 0 != _showM.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%.2lf",(min * 10)/_showM.symbolModel.contractSizeNew] WithDigits:2] withDigits:2]];
        }
    }
    
    if ( max > min * 100  && [comDec doubleValue] != (min * 100) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 100)]];
        if ( _isVolNum || ( 0 == _showM.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 100)] WithDigits:_showM.symbolModel.volDigits] withDigits:_showM.symbolModel.volDigits]];
        }else if( 0 != _showM.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%.2lf",(min * 100)/_showM.symbolModel.contractSizeNew] WithDigits:2] withDigits:2]];
        }
    }
    
    if ( max > min * 500  && [comDec doubleValue] != (min * 500) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 500)]];
        if ( _isVolNum || ( 0 == _showM.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 500)] WithDigits:_showM.symbolModel.volDigits] withDigits:_showM.symbolModel.volDigits]];
        }else if( 0 != _showM.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%.2lf",(min * 500)/_showM.symbolModel.contractSizeNew] WithDigits:2] withDigits:2]];
        }
    }
    
    if ( [comDec doubleValue] != max ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",max]];
        if ( _isVolNum || 0 == _showM.symbolModel.contractSizeNew ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",max] WithDigits:_showM.symbolModel.volDigits] withDigits:_showM.symbolModel.volDigits]];
        }else if( 0 != _showM.symbolModel.contractSizeNew ){
             [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%.2lf",max/_showM.symbolModel.contractSizeNew] WithDigits:2] withDigits:2]];
        }
    }
    
    return _showVolArr;
}

- (NSMutableArray *)prcRelPosBuyArr
{
    if ( !_prcRelPosBuyArr ) {
        _prcRelPosBuyArr = [NSMutableArray array];
    }
    return _prcRelPosBuyArr;
}

- (NSMutableArray *)prcRelPosSellArr
{
    if ( !_prcRelPosSellArr ) {
        _prcRelPosSellArr = [NSMutableArray array];
    }
    return _prcRelPosSellArr;
}

- (NSMutableArray *)chooseStatusArr
{
    if ( !_chooseStatusArr ) {
        _chooseStatusArr = [NSMutableArray array];
        for( NSInteger i = 0; i < 10; i++ ){
            [_chooseStatusArr addObject:@[@(NO),@(NO)]];
        }
    }
    return _chooseStatusArr;
}

 
@end
