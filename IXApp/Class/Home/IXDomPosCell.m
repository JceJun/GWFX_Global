//
//  IXDomPosCell.m
//  IXApp
//
//  Created by Bob on 2017/8/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDomPosCell.h"
#import "NSString+FormatterPrice.h"
#import "IXAppUtil.h"
#import "IXAccountBalanceModel.h"
#import "IXUserDefaultM.h"

@interface IXDomPosCell ()

@property (nonatomic, strong) UILabel *buyTitleLbl;
@property (nonatomic, strong) UILabel *buyContentLbl;

@property (nonatomic, strong) UILabel *sellTitleLbl;
@property (nonatomic, strong) UILabel *sellContentLbl;

@property (nonatomic, strong) UILabel *profitTitleLbl;
@property (nonatomic, strong) UILabel *profitContentLbl;

@end


@implementation IXDomPosCell

 
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self profitTitleLbl];
    [self sellTitleLbl];
    [self sellContentLbl];
    [self buyTitleLbl];
    [self buyContentLbl];
}

- (void)updateBuyVol
{
    self.buyContentLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:[IXAccountBalanceModel shareInstance].buyVol contractSizeNew:_model.showM.symbolModel.contractSizeNew volDigit:_model.showM.symbolModel.volDigits] withDigits:_model.showM.symbolModel.volDigits];
}


- (void)updateSellVol
{
    self.sellContentLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:[IXAccountBalanceModel shareInstance].sellVol contractSizeNew:_model.showM.symbolModel.contractSizeNew volDigit:_model.showM.symbolModel.volDigits] withDigits:_model.showM.symbolModel.volDigits];
}

- (void)updateProfit
{
    double profit = [IXAccountBalanceModel shareInstance].symProfit;
    NSString *profitStr = [NSString stringWithFormat:@"%.2lf",profit];
    if ( profit < 0 ) {
        self.profitContentLbl.text = [NSString stringWithFormat:@"-%@",[IXAppUtil amountToString:[profitStr stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
        self.profitContentLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }else if( profit == 0){
        self.profitContentLbl.text = @"0.00";
        self.profitContentLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    }else{
        self.profitContentLbl.text = [NSString stringWithFormat:@"+%@",[IXAppUtil amountToString:[profitStr stringByReplacingOccurrencesOfString:@"+" withString:@""]]];
        self.profitContentLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
}


- (UILabel *)buyTitleLbl
{
    if ( !_buyTitleLbl ) {
        _buyTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 117, 10, 100, 14)
                                                  WithFont:PF_MEDI(10)
                                                 WithAlign:NSTextAlignmentLeft
                                                wTextColor:0x99abba
                                                dTextColor:0x8395a4];
        [self.contentView addSubview:_buyTitleLbl];
        _buyTitleLbl.text = LocalizedString(@"买");
    }
    return _buyTitleLbl;
}


- (UILabel *)buyContentLbl
{
    if ( !_buyContentLbl ) {
        _buyContentLbl = [IXUtils createLblWithFrame:CGRectMake( 117, 30, 110, 18)
                                                    WithFont:RO_REGU(18)
                                                   WithAlign:NSTextAlignmentLeft
                                                  wTextColor:0x4c6072
                                                  dTextColor:0xe9e9ea];
        [self.contentView addSubview:_buyContentLbl];
        _buyContentLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:[IXAccountBalanceModel shareInstance].buyVol contractSizeNew:_model.showM.symbolModel.contractSizeNew volDigit:_model.showM.symbolModel.volDigits] withDigits:_model.showM.symbolModel.volDigits];
    }
    return _buyContentLbl;
}

- (UILabel *)sellTitleLbl
{
    if ( !_sellTitleLbl ) {
        _sellTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 10, 120, 14)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x99abba
                                              dTextColor:0x8395a4];
        [self.contentView addSubview:_sellTitleLbl];
        _sellTitleLbl.text = LocalizedString(@"卖");
    }
    return _sellTitleLbl;
}


- (UILabel *)sellContentLbl
{
    if ( !_sellContentLbl ) {
        _sellContentLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 30, 120, 18)
                                                  WithFont:RO_REGU(18)
                                                 WithAlign:NSTextAlignmentLeft
                                                wTextColor:0x4c6072
                                                dTextColor:0xe9e9ea];
        [self.contentView addSubview:_sellContentLbl];
        _sellContentLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:[IXAccountBalanceModel shareInstance].sellVol contractSizeNew:_model.showM.symbolModel.contractSizeNew volDigit:_model.showM.symbolModel.volDigits] withDigits:_model.showM.symbolModel.volDigits];
    }
    return _sellContentLbl;
}

- (UILabel *)profitTitleLbl
{
    if ( !_profitTitleLbl ) {
        _profitTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 104, 10, kScreenWidth - 114, 14)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentRight
                                           wTextColor:0x99abba
                                           dTextColor:0x8395a4];
        [self.contentView addSubview:_profitTitleLbl];
        _profitTitleLbl.text = LocalizedString(@"盈亏金额");
    }
    return _profitTitleLbl;
}

- (UILabel *)profitContentLbl
{
    if ( !_profitContentLbl ) {
        _profitContentLbl = [IXUtils createLblWithFrame:CGRectMake( 104, 30, kScreenWidth - 114, 18)
                                               WithFont:RO_REGU(18)
                                              WithAlign:NSTextAlignmentRight
                                             wTextColor:0x4c6072
                                             dTextColor:0xe9e9ea];
        [self.contentView addSubview:_profitContentLbl];
        _profitContentLbl.text = @"0.00";
    }
    return _profitContentLbl;
}

@end
