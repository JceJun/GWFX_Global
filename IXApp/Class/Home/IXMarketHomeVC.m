//
//  IXMarketHomeVC.m
//  IXApp
//
//  Created by bob on 16/11/21.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXMarketHomeVC.h"
#import "IXMarkNavTitleV.h"
#import "IXChooseSymbolCataView.h"
#import "IXSelfSymbolVC.h"
#import "IXQuoteContentVC.h"
#import "IXSymSearchVC.h"
#import "IXLeftNavView.h"
#import "IXQuoteDistribute.h"
#import "UIImageView+AFNetworking.h"
#import "IXUserDefaultM.h"
#import "IXUserInfoMgr.h"
#import "UIImageView+WebCache.h"
#import "IXOrderStateRefreashView.h"
#import "IXDBSymbolMgr.h"
#import "FXWheellV.h"
#import "IXBORequestMgr+BroadSide.h"
#import "IXUserInfoM.h"
#import "NSObject+IX.h"
#import "IXNewGuideV.h"
#import "IXCpyConfig.h"
#import "IXADMgr.h"
#import "IXDPSChannelVC.h"
#import "AppDelegate+UI.h"
#import "IXGuideView.h"

@interface IXMarketHomeVC ()<IXNewGuideVDelegate>
{
    BOOL topViewWithSubSymbol;
}

/**
 选择分类
 */
@property (nonatomic, strong) IXChooseSymbolCataView *chooseCata;

/**
 付费行情对应的View
 */
@property (nonatomic, strong) IXOrderStateRefreashView *unOpenQuote;

/**
 分类
 */
@property (nonatomic, strong) IXMarkNavTitleV   * navTitleView;

/**
 自选
 */
@property (nonatomic, strong) IXSelfSymbolVC    * selfSymbol;

/**
 其他分类
 */
@property (nonatomic, strong) IXQuoteContentVC  * contentVC;

/**
 侧滑菜单
 */
@property (nonatomic, strong) IXLeftNavView     * leftNavItem;

/**
 数据模型
 */
@property (nonatomic, strong) IXMarketSymbolM   * markSymM;

//////////////////////////////////////////////////////////
@property (nonatomic, strong) FXWheellV     *wheelV;//轮播图
@property (nonatomic, assign) NSInteger      adType;//当前类型 0:自选 其它:主页
@property (nonatomic, strong) IXNewGuideV *newGuideV;


@end

@implementation IXMarketHomeVC

- (void)dealloc
{
    NSLog(@"__%s__",__func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.navigationItem.titleView = self.navTitleView;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightItemWithImg:[UIImage imageNamed:@"common_search_avatar"]
                              target:self
                                 sel:@selector(rightBtnItemClicked)
                            aimWidth:55];
    
    [self reloadCataInfo];
    
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //处理第一次注册成功直接开户
    if (NewGuideEnable) {
        [self loadNewGuideV];
    }
}

#pragma mark -
#pragma mark 新手指引
- (void)loadNewGuideV
{
    if (![IXUserDefaultM getNewGuide:IXNewGuideBroadside]) {
        [self.view addSubview:self.newGuideV];
        [self.newGuideV showNewGuideType:IXNewGuideBroadside];
    } else {
        if (![IXUserDefaultM getNewGuide:IXNewGuideCata]) {
            [self.view addSubview:self.newGuideV];
            [self.newGuideV showNewGuideType:IXNewGuideCata];
        }
    }
}

- (IXNewGuideV *)newGuideV
{
    if ( !_newGuideV ) {
        _newGuideV = [[IXNewGuideV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, kScreenHeight)];
        _newGuideV.delegate = self;
    }
    return _newGuideV;
}

- (void)nextRemoveViewType:(IXNewGuide)type
{
    switch (type) {
        case IXNewGuideBroadside:{
            [self.view addSubview:self.newGuideV];
            [self.newGuideV showNewGuideType:IXNewGuideCata];
            [IXUserDefaultM saveNewGuide:IXNewGuideBroadside];
        }
            break;
        case IXNewGuideCata:{
            [IXUserDefaultM saveNewGuide:IXNewGuideCata];
        }
            break;
        default:
            break;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self resetHeadImage];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.show) {
            NSString *accoutType = [IXDataProcessTools showCurrentAccountType];
            if (![accoutType isEqualToString:LocalizedString(@"游客")]) {
                [IXGuideView makeInstance:GUIDE_MARKET];
            }
        }
    });
}


- (void)leftBtnItemClicked
{
    if ([IXUserInfoMgr shareInstance].isDemeLogin) {
        [self showRegistLoginAlert];
    } else {
        [[AppDelegate getRootVC] toggleLeftView];
    }
}

- (void)rightBtnItemClicked
{
    IXSymSearchVC *searchVC = [[IXSymSearchVC alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}


- (void)reloadCataInfo
{
    if( _markSymM.cataArr && [_markSymM.cataArr isKindOfClass:[NSArray class]] ){
        NSInteger chooseRow = [self getChooseRow];
        if (chooseRow >= _markSymM.cataArr.count || chooseRow < 0) {
            chooseRow = 1;
        }

        if (_contentVC) {
            [self.contentVC cancelQuotePrice];
            [self.contentVC.vcAry removeAllObjects];
            [self.contentVC reloadData];
        }
        
        _chooseCata.dataSourceAry = _markSymM.cataArr;
        [self.navTitleView setTitleContent:[(NSDictionary *)_markSymM.cataArr[chooseRow] objectForKey:@"name"]];
        _chooseCata.currentRow = chooseRow;
        [self resetUIWithRow:chooseRow];
    }
}

// 选择产品分类
- (void)resetUIWithRow:(NSInteger)indexRow
{
    //数组越界
    if (0 > indexRow) {
        return;
    }
    
    //添加广告
    [self loadAd:indexRow];
    
    if (indexRow >= _markSymM.cataArr.count) {
        indexRow = 1;
    }
  
    if( [self.navigationItem.titleView isKindOfClass:[IXMarkNavTitleV class]] ){
        [_navTitleView setTitleContent:[(NSDictionary *)_markSymM.cataArr[indexRow] objectForKey:@"name"]];
    }
    
    switch (indexRow) {
        case 0:{
            topViewWithSubSymbol = YES;
            [self setSubSymbol];
        }
            break;
        default:{
            topViewWithSubSymbol = NO;
            [self setCataListWithRow:indexRow];
        }
            break;
    }
}

#pragma mark - 添加广告视图
- (void)loadAd:(NSInteger)type
{
    NSDictionary *paramDic = nil;
    self.adType = type;
    if (self.adType == 0) {
        //用户已经手动关闭广告
        if ([IXADMgr shareInstance].haveCloseSelf) {
            return;
        }
        //自选
        paramDic = @{
                     @"pageNo":@(1),
                     @"pageSize":@(20),
                     @"infomationType":@"OPTIONAL_ADVERTISEMENT"
                     };
    } else {
        //用户已经手动关闭广告
        if ([IXADMgr shareInstance].haveCloseHome) {
            return;
        }
        //行情
        paramDic = @{
                     @"pageNo":@(1),
                     @"pageSize":@(20),
                     @"infomationType":@"MARKET_ADVERTISEMENT"
                     };
    }
    
    weakself;
    [IXBORequestMgr bs_bannerAdvertisementListWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
     {
         if (success && [obj ix_isDictionary] && obj[@"resultList"]) {
             if (weakSelf.adType == 0) {
                 [IXADMgr shareInstance].selfBannerClose = [obj[@"bannerClose"] boolValue];
                 [IXADMgr shareInstance].selfAdArr = [obj[@"resultList"] mutableCopy];
                 if ([IXADMgr shareInstance].selfAdArr.count) {
                     [weakSelf addwheelView];
                     [weakSelf.selfSymbol reloadTableView];
                 }
             } else {
                 [IXADMgr shareInstance].homeBannerClose = [obj[@"bannerClose"] boolValue];
                 [IXADMgr shareInstance].homeAdArr = [obj[@"resultList"] mutableCopy];
                 if ([IXADMgr shareInstance].homeAdArr.count) {
                     [weakSelf addwheelView];
                     [weakSelf.contentVC reloadData];
                 }
             }
         }
     }];
}

- (void)addwheelView
{
    if (!_wheelV) {
        _wheelV = [[FXWheellV alloc] initWithFrame:CGRectMake(0,
                                                              kScreenHeight
                                                              - kTabbarHeight
                                                              - kNavbarHeight
                                                              - 94,
                                                              kScreenWidth,
                                                              94)];
    }
    if (self.adType == 0) {
        _wheelV.bannerClose = [IXADMgr shareInstance].selfBannerClose;
        _wheelV.items = [IXADMgr shareInstance].selfAdArr;
    } else {
        _wheelV.bannerClose = [IXADMgr shareInstance].homeBannerClose;
        _wheelV.items = [IXADMgr shareInstance].homeAdArr;
    }
    [self.view addSubview:_wheelV];
}

- (void)closeAd
{
    if (_wheelV) {
        if (self.adType == 0) {
            [IXADMgr shareInstance].haveCloseSelf = YES;
            [[IXADMgr shareInstance].selfAdArr removeAllObjects];
            [IXADMgr shareInstance].selfBannerClose = NO;
            [self.selfSymbol reloadTableView];
        } else {
            [IXADMgr shareInstance].haveCloseHome = YES;
            [[IXADMgr shareInstance].homeAdArr removeAllObjects];
            [IXADMgr shareInstance].homeBannerClose = NO;
            [self.contentVC reloadData];
        }
        [_wheelV removeFromSuperview];
        _wheelV = nil;
    }
}



#pragma mark - 初始化视图数据
- (void)resetAccountType
{
    self.leftNavItem.typeLab.text = [IXDataProcessTools showCurrentAccountType];
}

//重设头像
- (void)resetHeadImage
{
    [self.leftNavItem.iconImgV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                                 placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                                          options:SDWebImageAllowInvalidSSLCertificates];
}

//加载自选Symbol数据
- (void)setSubSymbol
{
    [self.markSymM setSubSymbol];

    [self.contentVC.view removeFromSuperview];
    [self.view addSubview:self.selfSymbol.view];
    [self.selfSymbol reloadTableView];
}

//加载热门Symbol数据
- (void)setHotSymbol
{
    [self.markSymM setHotSymbol];
    
    [self.selfSymbol.view removeFromSuperview];
    [self.view addSubview:self.contentVC.view];
}

//加载具体类别的商品
- (void)setCataListWithRow:(NSInteger)indexRow
{
    _markSymM.tableType = TABLE_MAX;
    _markSymM.chooseSymbolCataPage = indexRow - 1;
    [self setControllerVCWithRow:_markSymM.chooseSymbolCataPage];
}

- (void)setControllerVCWithRow:(NSInteger)row
{
    if(self.contentVC.vcAry.count > row){
        [self.contentVC reloadCataListWithRow:row];
    }else{
        NSMutableArray *vcArr = [NSMutableArray array];
        for(NSInteger i = 1; i < self.markSymM.cataArr.count; i++){
            
            NSInteger cataId = [self.markSymM.cataArr[i][kID] integerValue];
            NSInteger marketId = [self.markSymM.cataArr[i][kMarketId] integerValue];
    
            IXQuoteDetailVC *detail = [[IXQuoteDetailVC alloc] initWithSymbolCataId:cataId
                                                                       WithMarketId:marketId];
            detail.parentVC = self;
            [vcArr addObject:detail];
        }
        [self.contentVC reloadCataList:vcArr WithRow:row];
    }
    
    if (_selfSymbol) {
        [self.selfSymbol.view removeFromSuperview];
        [self.view addSubview:self.contentVC.view];
    }
}

//记住选择的分类
//如果最后访问自选，而且有添加自选，则保存自选
//如果最后访问自选，没有添加自选，则跳到热门
//其它分类则跳转到对应分类
- (void)remChooseRow
{
    NSInteger chooseRow = 1;
    if(topViewWithSubSymbol){
        if (![self.markSymM.cataArr count]) {
            chooseRow = 0;
        }
    }else{
        chooseRow = _markSymM.chooseSymbolCataPage + 1;
    }
    [IXUserDefaultM saveChooseSymbolCata:chooseRow];
}

//获取保存记录的分类
- (NSInteger)getChooseRow
{
    return [IXUserDefaultM getChooseSymbolCata];
}

- (void)addPopView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self.unOpenQuote];
    _unOpenQuote.tipTitle = @"交易已执行";
    _unOpenQuote.tipDes = @"以115.80价格买入400股 腾讯控股";
    _unOpenQuote.tipContent = @"提交保证金 44.31HKD，佣金0.12HKD";
}

- (void)showPosition:(proto_position_add *)pb
{
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:pb.position.symbolid];
    if ( !symDic ) {
        return;
    }
    IXSymbolM *symbolModel = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
    if( !symbolModel ){
        return;
    }
    
    NSString *price = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",pb.position.openPrice]
                                    WithDigits:symbolModel.digits];
    
    BOOL _isVolNum = ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount ) ;
    double posVol = pb.position.volume;
    NSString *vol  = @"";
    
    if ( !_isVolNum && 0 != symbolModel.contractSizeNew ) {
        posVol = posVol/symbolModel.contractSizeNew;
    }
    
    if ( posVol < 1000 ) {
        
        if ( !_isVolNum && 0 != symbolModel.contractSizeNew) {
            vol = [NSString stringWithFormat:@"%.2lf",posVol];
        }else{
            vol = [NSString stringWithFormat:@"%.lf",posVol];
        }
        
    }else if( posVol >= 1000.f && posVol < 1000000.f ){
        
        if ( !_isVolNum && 0 != symbolModel.contractSizeNew) {
            vol = [NSString stringWithFormat:@"%.2lfK",posVol/1000.f];
        }else{
            vol = [NSString stringWithFormat:@"%.lfK",posVol/1000.f];
        }
    }
    else if( posVol >= 1000000.f ){
        
        if ( !_isVolNum && 0 != symbolModel.contractSizeNew) {
            vol = [NSString stringWithFormat:@"%.2fM",posVol/1000000.f];
        }else{
            vol = [NSString stringWithFormat:@"%.1fM",posVol/1000000.f];
        }
    }
    
    self.unOpenQuote.tipTitle = LocalizedString(@"交易已执行");
    
    NSString *dir = LocalizedString(@"买入.");
    if ( pb.position.direction == item_position_edirection_DirectionSell ) {
        dir = LocalizedString(@"卖出.");
    }
    
    NSString *prc = [NSString stringWithFormat:LocalizedString(@"以%@的价格"),price];
    NSString *dec = @"";
    if ( symbolModel.unitLanName && [symbolModel.unitLanName length] ) {
        dec =  [NSString stringWithFormat:@"%@%@%@%@ %@",prc,dir,vol,[IXDataProcessTools showCurrentUnitLanName:symbolModel.unitLanName],symbolModel.languageName];
    }else{
        dec =  [NSString stringWithFormat:@"%@%@%@ %@",prc,dir,vol,symbolModel.languageName];
    }
    self.unOpenQuote.tipDes = dec;
    
    NSString *commission = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",pb.position.commission]
                                         WithDigits:2];
    if ( pb.position.commission == 0 ) {
        commission = LocalizedString(@"免费");
    }
    self.unOpenQuote.tipContent = [NSString stringWithFormat:@"%@ %@",LocalizedString(@"佣金"),commission];
    
    [self.unOpenQuote show];
}


- (void)showNetStatusWithConnectStatus:(IXTCPConnectStatus)status
{
    switch ( status ) {
        case IXTCPConnectStatusDisconnect:{
            [self setTitleView:self.navTitleView title:LocalizedString(@"连接已断开")];
            [self stopTitleWaitting:status];
        }
            break;
        case IXTCPConnectStatusConneting:{
            [self setTitleView:self.navTitleView title:LocalizedString(@"连接中")];
            [self showTitleWaitting];
        }
            break;
        case IXTCPConnectStatusConnected:{
            [self setTitleView:self.navTitleView title:LocalizedString(@"已连接")];
            [self stopTitleWaitting:status];
        }
            break;
        case IXTCPConnectStatusMaxTried:{
            [self setTitleView:self.navTitleView title:LocalizedString(@"未连接(点击重连)")];
            [self stopTitleWaitting:status];
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - IXQuoteDistribute
- (void)needRefresh
{
    id obj =  self.navigationController.topViewController ;
    if ([obj isKindOfClass:[self class]]) {
        
        if (_markSymM.tableType == TABLE_SUBSYMBOL) {
            [_selfSymbol subscribeDynamicPrice];
        }else{
            if (_markSymM.chooseSymbolCataPage >= 0 &&
                self.contentVC.vcAry &&
                self.contentVC.vcAry.count > _markSymM.chooseSymbolCataPage) {
                IXQuoteDetailVC *detailVC = self.contentVC.vcAry[_markSymM.chooseSymbolCataPage];
                [detailVC subscribeDynamicPrice];
            }
        }
        
    }else{
        if ([obj respondsToSelector:@selector(needRefresh)]) {
            [obj needRefresh];
        }
    }   
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    id<IXQuoteDistribute> obj = (id<IXQuoteDistribute>)self.navigationController.topViewController;
    if (![obj isKindOfClass:[self class]]) {
        if ([obj respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)]) {
            [obj didResponseQuoteDistribute:arr cmd:cmd];
        }
    }else{
        if (_markSymM.tableType == TABLE_SUBSYMBOL) {
            if ( [_selfSymbol respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)] ) {
                [(id<IXQuoteDistribute>)_selfSymbol didResponseQuoteDistribute:arr cmd:cmd];
            }
        }else{
            if (self.contentVC.vcAry.count > _markSymM.chooseSymbolCataPage) {
                IXQuoteDetailVC *detailVC = self.contentVC.vcAry[_markSymM.chooseSymbolCataPage];
                if ( [detailVC respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)] ) {
                    [(id<IXQuoteDistribute>)detailVC didResponseQuoteDistribute:arr cmd:cmd];
                }
            }
       
        }
    }
}

- (void)cancelVisualQuote
{
    id<IXQuoteDistribute> obj = (id<IXQuoteDistribute>)self.navigationController.topViewController;
    if (![obj isKindOfClass:[self class]]) {
        if ( [obj respondsToSelector:@selector(cancelVisualQuote)] ) {
            [obj cancelVisualQuote];
        }
    }else{
        if ( _markSymM.tableType == TABLE_SUBSYMBOL  ) {
            if ( [_selfSymbol respondsToSelector:@selector(cancelVisualQuote)] ) {
                [(id<IXQuoteDistribute>)_selfSymbol cancelVisualQuote];
            }
        }else{
            if ( self.contentVC.vcAry.count > _markSymM.chooseSymbolCataPage ) {
                IXQuoteDetailVC *detailVC = self.contentVC.vcAry[_markSymM.chooseSymbolCataPage];
                if ( [detailVC respondsToSelector:@selector(cancelVisualQuote)] ) {
                    [(id<IXQuoteDistribute>)detailVC cancelVisualQuote];
                }
            }
        }
    }
}

- (void)subscribeVisualQuote
{
    id<IXQuoteDistribute> obj = (id<IXQuoteDistribute>)self.navigationController.topViewController;
    if (![obj isKindOfClass:[self class]]) {
        if ( [obj respondsToSelector:@selector(subscribeVisualQuote)] ) {
            [obj cancelVisualQuote];
        }
    }else{
        if ( _markSymM.tableType == TABLE_SUBSYMBOL  ) {
            if ( [_selfSymbol respondsToSelector:@selector(subscribeVisualQuote)] ) {
                [(id<IXQuoteDistribute>)_selfSymbol subscribeVisualQuote];
            }
        }else{
            //越界判断
            if ( self.contentVC.vcAry.count > _markSymM.chooseSymbolCataPage ) {
                IXQuoteDetailVC *detailVC = self.contentVC.vcAry[_markSymM.chooseSymbolCataPage];
                if ( [detailVC respondsToSelector:@selector(subscribeVisualQuote)] ) {
                    [(id<IXQuoteDistribute>)detailVC subscribeVisualQuote];
                }
            }
           
        }
    }
}

#pragma mark -
#pragma mark - lazy load
- (IXSelfSymbolVC *)selfSymbol
{
    if (!_selfSymbol) {
        _selfSymbol = [[IXSelfSymbolVC alloc] init];
        _selfSymbol.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight - kTabbarHeight);
        _selfSymbol.parentVC = self;
    }
    return _selfSymbol;
}

- (IXQuoteContentVC *)contentVC
{
    if (!_contentVC) {
        weakself;
        _contentVC = [[IXQuoteContentVC alloc] initWithViewControllerAry:@[] ChoosePage:^(NSInteger page) {
            if ( [weakSelf.navigationItem.titleView isKindOfClass:[IXMarkNavTitleV class]] ) {
                [weakSelf.navTitleView setTitleContent:[(NSDictionary *)weakSelf.markSymM.cataArr[page + 1] objectForKey:@"name"]];
                weakSelf.chooseCata.currentRow = (page + 1);
                [weakSelf resetUIWithRow:(page + 1)];
            }
        }];
        
        _contentVC.titleBlock = ^(NSInteger page) {
            [weakSelf.navTitleView setTitleContent:[(NSDictionary *)weakSelf.markSymM.cataArr[page + 1] objectForKey:@"name"]];
        };
        
        _contentVC.depositBlock = ^{
            IXDPSChannelVC *vc = [IXDPSChannelVC new];
            vc.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
        _contentVC.needregistBlock = ^(BOOL needRegist) {
            if (needRegist) {
                [AppDelegate showRegist];
            }else{
                [IXUserInfoMgr shareInstance].isCloseDemo = YES;
                [AppDelegate showLoginMain];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [IXUserInfoMgr shareInstance].isCloseDemo = NO;
                });
            }
        };
        [self.view addSubview:_contentVC.view];
    }
    
    return _contentVC;
}

- (IXMarketSymbolM *)markSymM
{
    if (!_markSymM) {
        _markSymM = [[IXMarketSymbolM alloc] init];
        
        weakself;
        _markSymM.updateSubSym = ^{
            [weakSelf.selfSymbol reloadTableView];
        };
        
        _markSymM.updateNetStatus = ^(IXTCPConnectStatus status) {
            [weakSelf showNetStatusWithConnectStatus:status];
        };
        
        _markSymM.updateCata = ^{
            [weakSelf reloadCataInfo];
        };
        
        _markSymM.updateSym = ^{
            if (weakSelf.markSymM.tableType == TABLE_SUBSYMBOL) {
                [weakSelf.selfSymbol reloadTableView];
            }else{
                NSInteger row = weakSelf.markSymM.chooseSymbolCataPage;
                [weakSelf.contentVC reloadCataListWithRow:row];
            }
        };
        
        _markSymM.updatePosTip = ^(proto_position_add *pb) {
            if ( weakSelf.view.window ) {
                [weakSelf showPosition:pb];
            }
        };
        
        _markSymM.updateHotSym = ^{
            [weakSelf.contentVC reloadCataListWithRow:0];
        };
        
        _markSymM.updateAccount = ^{
            [weakSelf resetAccountType];
        };
        
        _markSymM.updateUserInfo = ^{
            [weakSelf resetHeadImage];
        };
    }
    return _markSymM;
}


//选择类型
- (IXMarkNavTitleV *)navTitleView
{
    if (!_navTitleView) {
        
        _navTitleView = [[IXMarkNavTitleV alloc] initWithFrame:CGRectMake(100,
                                                                          kNavbarHeight - 41,
                                                                          kScreenWidth - 200,
                                                                          41)];
        [_navTitleView setTitleContent:[(NSDictionary *)self.markSymM.cataArr[1] objectForKey:@"name"]];
        weakself;
        _navTitleView.tapContent = ^(){
            [weakSelf.chooseCata show];
        };
    }
    return _navTitleView;
}

- (IXChooseSymbolCataView *)chooseCata
{
    if (!_chooseCata) {
        
        weakself;
        _chooseCata = [[IXChooseSymbolCataView alloc] initWithCataAry:_markSymM.cataArr
                                                           WithChoose:^(NSInteger indexRow) {
                                                               [weakSelf resetUIWithRow:indexRow];
                                                           }];
        _chooseCata.type = Symbol_Cata;
    }
    return _chooseCata;
}

- (IXLeftNavView *)leftNavItem
{
    if (!_leftNavItem) {
        _leftNavItem = [[IXLeftNavView alloc] initWithTarget:self action:@selector(leftBtnItemClicked)];
    }
    
    return _leftNavItem;
}


- (IXOrderStateRefreashView *)unOpenQuote
{
    if (!_unOpenQuote) {
        CGRect frame = CGRectMake( 0, kNavbarHeight, kScreenWidth, 86);
        _unOpenQuote = [[IXOrderStateRefreashView alloc] initWithFrame:frame];
    }
    return _unOpenQuote;
}


@end
