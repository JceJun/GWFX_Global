//
//  IXTradeTypeV.h
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,  TRADE_TYPE){
    Trade_kind,
    Trade_expire,
};

typedef void(^chooseTrade)(TRADE_TYPE type,UIView *showView);

@interface IXTradeTypeV : UIView

@property (nonatomic, copy) chooseTrade tradeCon;

- (id)initWithFrame:(CGRect)frame WithTadeType:(chooseTrade)trade;

- (void)setKind:(NSString *)kind;

- (void)setExpire:(NSString *)expire;

@end
