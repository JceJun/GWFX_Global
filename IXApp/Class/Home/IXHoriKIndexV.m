//
//  IXHoriKIndexV.m
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXHoriKIndexV.h"

@interface IXHoriKIndexV ()
<
UITableViewDelegate,
UITableViewDataSource
>
{
    NSInteger mainIdx;
    NSInteger secdIdx;
}
@property (nonatomic,strong)NSArray *ds;
@property (nonatomic,strong)UITableView *tableV;

@end

@implementation IXHoriKIndexV

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 3.f;
        self.layer.masksToBounds = YES;
        self.layer.borderWidth = .5f;
        self.layer.dk_borderColorPicker = DKColorWithRGBs(0xe2eaf2, 0x232a36);
        
        mainIdx = 0;
        secdIdx = 0;
        self.ds = @[@[@"MA",@"BBI",@"BOLL",@"MIKE",@"PBX"],
                    @[@"MACD",@"ARBR",@"ATR",@"BIAS",@"CCI",@"DKBY",@"KD",@"KDJ",@"LWR",@"QHLSR",@"RSI",@"WR"]];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self tableV];
}

- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStyleGrouped];
        _tableV.dataSource = self;
        _tableV.delegate = self;
        _tableV.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
        _tableV.dk_separatorColorPicker = DKColorWithRGBs(0xe2eaf2, 0x232a36);
        _tableV.dk_backgroundColorPicker = DKViewColor;
        [self addSubview:_tableV];
    }
    return _tableV;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _ds.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 28.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 4.f;
    }
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:(CGRect){0.0,0,4.f}];
    view.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x232a36);
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray *)_ds[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"IXHoriKIndexVCell";
    IXHoriKIndexVCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXHoriKIndexVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.title.text = _ds[indexPath.section][indexPath.row];
    
    if (indexPath.section == 0) {
        if (indexPath.row == mainIdx) {
            [cell hilighTitle:YES];
        } else {
            [cell hilighTitle:NO];
        }
    } else {
        if (indexPath.row == secdIdx) {
            [cell hilighTitle:YES];
        } else {
            [cell hilighTitle:NO];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        mainIdx = indexPath.row;
    } else {
        secdIdx = indexPath.row;
    }
    [tableView reloadData];
    if (self.idxBtnClicked) {
        self.idxBtnClicked(indexPath.section,indexPath.row);
    }
}

@end

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation IXHoriKIndexVCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x303b4d);;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 28)];
        _title.font = RO_REGU(12.f);
        _title.textColor = UIColorHexFromRGB(0x99abba);
        _title.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (void)hilighTitle:(BOOL)hilight
{
    if (hilight) {
        _title.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
    } else {
        _title.textColor = UIColorHexFromRGB(0x99abba);
    }
}

@end
