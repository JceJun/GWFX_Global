//
//  IXSymbolM.h
//  IXApp
//
//  Created by Evn on 16/12/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
 
@interface IXSymbolM : NSObject

/**唯一标示符，item的基本属性*/
@property (nonatomic,assign) uint64_t id_p;
/**更新标示符，item的基本属性*/
@property (nonatomic,assign) uint64_t uuid;
/**更新时间，item的基本属性*/
@property (nonatomic,assign) uint64_t uuTime;

/**用户组ID*/
@property (nonatomic,assign) uint32_t groupID;
/**产品名称*/
@property (nonatomic,copy) NSString *name;
/**报价源*/
@property (nonatomic,copy) NSString *source;
/**是否启用 0未启用  1启用 */
@property (nonatomic,assign) BOOL enable;
/** 假期分类ID */
@property (nonatomic, assign) uint64_t holidayCataId;
/** 交易时间分类ID */
@property (nonatomic, assign) uint64_t scheduleCataId;
/** 产品分类ID */
@property (nonatomic, assign) uint64_t cataId;
/** 产品序号 */
@property (nonatomic, assign)uint64_t sequence;
/**小数位*/
@property (nonatomic,assign) uint8_t digits;
/**合约大小*/
@property (nonatomic,assign) uint32_t contractSize;
/**点差*/
@property (nonatomic,assign) int16_t spread;
/**点差偏差*/
@property (nonatomic,assign) int16_t spreadBalance;
/**执行平仓时间*/
@property (nonatomic,assign) uint64_t forceCloseTime;
/**公司id*/
@property (nonatomic,assign) uint32_t companyID;
/**初始保證金*/
@property (nonatomic,assign) uint32_t marginInitial;
/**維持保證金*/
@property (nonatomic,assign) uint32_t marginMaintenance;
/**强平保证金*/
@property (nonatomic,assign) uint32_t marginStopout;
/**維持保證金*/
@property (nonatomic,assign) uint16_t marginHedged;
/**特殊初始保证金*/
@property (nonatomic,assign) uint32_t specialMarginInitial;
/**特殊維持保證金*/
@property (nonatomic,assign) uint32_t specialMarginMaintenance;
/**特殊强平保证金*/
@property (nonatomic,assign) uint32_t specialMarginStopout;
/**特殊锁仓百分比*/
@property (nonatomic,assign) uint16_t specialMarginHedged;
/**日初始保證金*/
@property (nonatomic,assign) uint32_t holidayMarginInitial;
/**假日初始保證金*/
@property (nonatomic,assign) uint32_t holidayMarginMaintenance;
/**假日强平保证金*/
//@property (nonatomic,assign) uint32_t holidayMarginStopout;
/**假日鎖倉保證金*/
@property (nonatomic,assign) uint32_t holidayMarginHedged;
/**手数列表*/
@property (nonatomic,copy) NSString *volumesList;
/**强制平仓价*/
@property (nonatomic,assign) double forceClosePrice;
/**市价下单超时时间*/
@property (nonatomic,assign) uint32_t orderTimeout;
/**限价/止损订单最高间隔点*/
@property (nonatomic,assign) uint16_t maxStopLevel;
/**大点比率*/
@property (nonatomic,assign) uint8_t pipsRatio;
/**长仓利息调整*/
@property (nonatomic,assign) double longSwapAdjust;
/**短仓利息调整*/
@property (nonatomic,assign) double shortSwapAdjust;
/**期金比较产品ID*/
@property (nonatomic,assign) uint32_t futureRefID;
/**期金比较产品Swap*/
@property (nonatomic,assign) double futureRefSwap;
/**期金比较容忍值*/
@property (nonatomic,assign) double futureTolrence;
@property (nonatomic, assign) uint64_t startTime;
/**过期时间*/
@property (nonatomic, assign) uint64_t expiryTime;

/* 是否可交易 */
@property (nonatomic, assign) uint64_t tradable;
/**成交范围*/
@property (nonatomic,assign) uint16_t range;
/**产品计息日数*/
@property (nonatomic,assign) uint16_t swapDaysPerYear;

/**最小手数*/
@property (nonatomic,assign) double volumesMin;

/**手數間隔*/
@property (nonatomic,assign) double volumesStep;
/**最大手數*/
@property (nonatomic,assign) double volumesMax;
/**報價超時時間*/
@property (nonatomic,assign) uint32_t quoteTimeout;
/**ITEM_SYMBOL_MODE  執行模式（MM/MTF/Exchange/MT5）*/
@property (nonatomic,assign) uint8_t mode;
/**最小手动审批手数*/
@property (nonatomic,assign) double minManualVolume;

/**最小手动审批手数*/
@property (nonatomic,assign) uint16_t stopLevel;
/**长仓利息*/
@property (nonatomic,assign) double longSwap;
/**短仓利息*/
@property (nonatomic,assign) double shortSwap;
/**3倍利息日*/
@property (nonatomic,assign) uint8_t threeDaysSwap;
/**基礎貨幣*/
@property (nonatomic,copy) NSString *baseCurrency;


/**盈虧貨幣*/
@property (nonatomic,copy) NSString *profitCurrency;
/**保證金貨幣*/
@property (nonatomic,copy) NSString *marginCurrency;
/**盈亏关联产品*/
@property (nonatomic,copy) NSString *relateSource;


/**期金比較產品ID*/
@property (nonatomic,assign) uint64_t createTime;
/**創建用戶ID*/
@property (nonatomic,assign) uint32_t createUserID;
/**修改時間*/
@property (nonatomic,assign) uint64_t modiTime;
/**修改用戶ID*/
@property (nonatomic,assign) uint32_t modiUserID;
/**用戶狀態  0.正常  1.刪除*/
@property (nonatomic,assign) uint8_t status;
/**最大持仓手数*/
@property (nonatomic,assign) double positionVolumeMax;
/** 是否是自选产品 */
@property (nonatomic,assign) BOOL isSub;
/** 语言名称 */
@property (nonatomic,strong) NSString *languageName;
/** 产品标签 */
@property (nonatomic,strong) NSArray *labelArr;
/** marketID */
@property (nonatomic,assign) uint32_t marketID;
/** market名称 */
@property (nonatomic,strong) NSString *marketName;
/** 自选产品ID */
@property (nonatomic,assign) uint64_t symbolsubId;
/** 显示名称 */
@property (nonatomic,strong) NSString *displayName;
/** 延迟交易时间 */
@property (nonatomic,assign) uint32_t scheduleDelayMinutes;
/** 产品数量单位多语言名称 */
@property (nonatomic,strong) NSString *unitLanName;
/**合约大小新*/
@property (nonatomic,assign) uint32_t contractSizeNew;
/**是否设置group_symbol*/
@property (nonatomic, assign) BOOL isSetGroupSym;
/* group_symbol是否可交易 */
@property (nonatomic, assign) uint64_t symTradable;
/**  是否仅平仓 */
@property (nonatomic, assign) BOOL  closeOnly;
/* 数量小数位 */
@property(nonatomic, readwrite) uint64_t volDigits;

@end
