//
//  IXOpenCellM.h
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IXDetailSymbolCell.h"
#import "IXSymbolDeepCell.h"
#import "IXOpenShowPrcCell.h"

#import "IXOpenTypeCell.h"
#import "IXOpenDirCell.h"

#import "IXOpenPrcCell.h"
#import "IXOpenEptPrcCell.h"
#import "IXPositionPrcCell.h"

#import "IXOpenShowSLCell.h"
#import "IXOpenSLCell.h"


@interface IXOpenCellM : NSObject

+ (IXDetailSymbolCell *)configSymbolDetailCellWithTableView:(UITableView *)tableView
                                                  WithIndex:(NSIndexPath *)indexPath;

+ (IXSymbolDeepCell *)configDeepPrcCellWithTableView:(UITableView *)tableView
                                           WithIndex:(NSIndexPath *)indexPath;

+ (IXOpenShowPrcCell *)configShowPrcCellWithTableView:(UITableView *)tableView
                                            WithIndex:(NSIndexPath *)indexPath;

+ (IXOpenTypeCell *)configOpenTypeCellWithTableView:(UITableView *)tableView
                                          WithIndex:(NSIndexPath *)indexPath;

+ (IXOpenDirCell *)configOpenDirCellWithTableView:(UITableView *)tableView
                                        WithIndex:(NSIndexPath *)indexPath;

+ (IXOpenPrcCell *)configOpenPrcCellWithTableView:(UITableView *)tableView
                                        WithIndex:(NSIndexPath *)indexPath;

+ (IXPositionPrcCell *)configPositionPrcCellWithTableView:(UITableView *)tableView
                                            WithIndex:(NSIndexPath *)indexPath;

+ (IXOpenEptPrcCell *)configOpenEptPrcCellWithTableView:(UITableView *)tableView
                                              WithIndex:(NSIndexPath *)indexPath;

+ (IXOpenShowSLCell *)configShowSLCellWithTableView:(UITableView *)tableView
                                          WithIndex:(NSIndexPath *)indexPath;

+ (IXOpenSLCell *)configOpenSLCellWithTableView:(UITableView *)tableView
                                      WithIndex:(NSIndexPath *)indexPath;

@end
