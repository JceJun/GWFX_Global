//
//  IXDomResultCell.m
//  IXApp
//
//  Created by Bob on 2017/3/23.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDomResultCell.h"


@interface IXDomResultCell ()


@property (nonatomic, strong) UILabel *leftLbl;

@property (nonatomic, strong) UILabel *rightLbl;

@end

@implementation IXDomResultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x303b4d);
    }
    return self;
}

- (void)setLeftContent:(NSString *)leftContent
{
    if ( leftContent && [leftContent isKindOfClass:[NSString class]] ) {
        self.leftLbl.text = leftContent;
    }else{
        self.leftLbl.text = @"";
    }
}

- (void)setRightContent:(NSString *)rightContent
{
//    self.rightLbl.textColor = MarketSymbolNameColor;

    if ( rightContent && [rightContent isKindOfClass:[NSString class]] ) {
        self.rightLbl.text = rightContent;
        if ( 0 == self.tag ) {
            if ( [rightContent isEqualToString:LocalizedString(@"买入")] ) {
                self.rightLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            }else{
                self.rightLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
            }
        }
    }else{
        self.rightLbl.text = @"";
    }
}

- (void)setRightFont:(UIFont *)rightFont
{
    self.rightLbl.font = rightFont;
}

- (UILabel *)leftLbl
{
    if ( !_leftLbl ) {
        _leftLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, 237, 15)
                                      WithFont:PF_MEDI(13)
                                     WithAlign:NSTextAlignmentLeft
                                    wTextColor:0x4c6072
                                    dTextColor:0x8395a4];
        [self.contentView addSubview:_leftLbl];
    }
    return _leftLbl;
}


- (UILabel *)rightLbl
{
    if ( !_rightLbl ) {
        _rightLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, 237, 15)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentRight
                                     wTextColor:0x4c6072
                                     dTextColor:0xe9e9ea];
        [self.contentView addSubview:_rightLbl];
    }
    return _rightLbl;
}


@end
