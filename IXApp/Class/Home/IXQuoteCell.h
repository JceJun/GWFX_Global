//
//  IXQuoteCell.h
//  IXApp
//
//  Created by bob on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"
#import "IXSymbolM.h"
#import "IXQuoteM.h"
#import "IXLastQuoteM.h"

@interface IXQuoteCell : IXClickTableVCell


- (void)setSymbolModel:(NSDictionary *)model
       WithPositionNum:(NSString *)positionNum
        WithMarketName:(NSString *)marketName
        WithTradeState:(IXSymbolTradeState)state;

- (void)elemetData;

- (void)resetQuotePrice;

- (void)resetLastClosePriceInfo;

- (void)setQuotePriceInfo:(IXQuoteM *)priceInfo;

- (void)setLastClosePriceInfo:(IXLastQuoteM *)model;

@end
