//
//  IXOpenResultCellB.m
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenResultCellB.h"
#import "IXUserDefaultM.h"

@interface IXOpenResultCellB()
@property (nonatomic, strong) UIImageView  *bgView;
@property (nonatomic, strong) UILabel *showName;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *showDir;
@property (nonatomic, strong) UILabel *dir;
@property (nonatomic, strong) UILabel *showType;
@property (nonatomic, strong) UILabel *type;
@property (nonatomic, strong) UILabel *showNum;
@property (nonatomic, strong) UILabel *num;
@property (nonatomic, strong) UILabel *showPrice;
@property (nonatomic, strong) UILabel *price;
@property (nonatomic, strong) UILabel *showDate;
@property (nonatomic, strong) UILabel *date;

@end

@implementation IXOpenResultCellB

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(9, 0, kScreenWidth - 18,200)];
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)showName
{
    if (!_showName) {
        CGRect rect = CGRectMake(14.5, 14,kScreenWidth - 34, 16);
        _showName = [[UILabel alloc] initWithFrame:rect];
        _showName.font = PF_MEDI(13);
        _showName.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showName.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showName];
    }
    return _showName;
}

- (UILabel *)name
{
    if (!_name) {
        CGRect rect = CGRectMake(0, 18,VIEW_W(_bgView) - 14.5, 15);
        _name = [[UILabel alloc] initWithFrame:rect];
        _name.font = PF_MEDI(13);
        _name.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _name.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_name];
    }
    return _name;
}

- (UILabel *)showDir
{
    if (!_showDir) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showName) + 14,kScreenWidth - 34, 16);
        _showDir = [[UILabel alloc] initWithFrame:rect];
        _showDir.font = PF_MEDI(13);
        _showDir.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showDir.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showDir];
    }
    return _showDir;
}

- (UILabel *)dir
{
    if (!_dir) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showName) + 14,VIEW_W(_bgView) - 14.5, 15);
        _dir = [[UILabel alloc] initWithFrame:rect];
        _dir.font = PF_MEDI(13);
        _dir.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _dir.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_dir];
    }
    return _dir;
}

- (UILabel *)showType
{
    if (!_showType) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showDir) + 14,kScreenWidth - 34, 16);        _showType = [[UILabel alloc] initWithFrame:rect];
        _showType.font = PF_MEDI(13);
        _showType.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showType.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showType];
    }
    return _showType;
}

- (UILabel *)type
{
    if (!_type) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showDir) + 14,VIEW_W(_bgView) - 14.5, 15);
        _type = [[UILabel alloc] initWithFrame:rect];
        _type.font = PF_MEDI(13);
        _type.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _type.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_type];
    }
    return _type;
}

- (UILabel *)showNum
{
    if (!_showNum) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showType) + 14,kScreenWidth - 34, 16);
        _showNum = [[UILabel alloc] initWithFrame:rect];
        _showNum.font = PF_MEDI(13);
        _showNum.text = LocalizedString(@"数量");
        _showNum.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showNum.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showNum];
    }
    return _showNum;
}

- (UILabel *)num
{
    if (!_num) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showType) + 14,VIEW_W(_bgView) - 14.5, 15);
        _num = [[UILabel alloc] initWithFrame:rect];
        _num.font = RO_REGU(15);
        _num.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _num.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_num];
    }
    return _num;
}

- (UILabel *)showPrice
{
    if (!_showPrice) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showNum) + 14,kScreenWidth - 34, 16);        _showPrice = [[UILabel alloc] initWithFrame:rect];
        _showPrice.font = PF_MEDI(13);
        _showPrice.text = LocalizedString(@"价格");
        _showPrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showPrice.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showPrice];
    }
    return _showPrice;
}

- (UILabel *)price
{
    if (!_price) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showNum) + 14,VIEW_W(_bgView) - 14.5, 15);
        _price = [[UILabel alloc] initWithFrame:rect];
        _price.font = RO_REGU(15);
        _price.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _price.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_price];
    }
    return _price;
}

- (UILabel *)showDate
{
    if (!_showDate) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showPrice) + 14,kScreenWidth - 34, 16);
        _showDate = [[UILabel alloc] initWithFrame:rect];
        _showDate.font = PF_MEDI(13);
        _showDate.text = LocalizedString(@"建立日期");
        _showDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showDate.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showDate];
    }
    return _showDate;
}

- (UILabel *)date
{
    if (!_date) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showPrice) + 14,VIEW_W(_bgView) - 14.5, 15);
        _date = [[UILabel alloc] initWithFrame:rect];
        _date.font = RO_REGU(15);
        _date.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _date.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_date];
    }
    return _date;
}

- (void)reloadUIData:(IXOpenResultModel *)model
{
    UIImage *wImage = GET_IMAGE_NAME(@"common_result_back");
    UIImage * wImg = [wImage stretchableImageWithLeftCapWidth:wImage.size.width/2 topCapHeight:wImage.size.height/2];
    UIImage *dImage = GET_IMAGE_NAME(@"common_result_back_D");
    UIImage * dImg = [dImage stretchableImageWithLeftCapWidth:dImage.size.width/2 topCapHeight:dImage.size.height/2];
    self.bgView.dk_imagePicker = DKImageWithImgs(wImg, dImg);
    self.showName.text = LocalizedString(@"商品");
    self.showDir.text = LocalizedString(@"方向");
    self.showType.text = LocalizedString(@"类型");
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
       self.showNum.text = LocalizedString(@"数量");
    } else {
       self.showNum.text = LocalizedString(@"手数");
    }
    self.showPrice.text = LocalizedString(@"价格");
    self.showDate.text = LocalizedString(@"建立日期");
    self.name.text = model.name;
    if (model.dir == item_order_edirection_DirectionBuy) {
        self.dir.text = LocalizedString(@"买入");
        self.dir.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    } else {
        self.dir.text = LocalizedString(@"卖出");
        self.dir.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    self.type.text = LocalizedString(model.cate);
    NSString *numStr = [NSString stringWithFormat:@"%@%@",model.num,[IXDataProcessTools showCurrentUnitLanName:model.unitLanName]];
    self.num.text = numStr;
    if ([[IXDataProcessTools showCurrentUnitLanName:model.unitLanName] length]) {
        [IXDataProcessTools resetLabel:self.num leftContent:model.num leftFont:15 WithContent:numStr rightFont:15 fontType:YES];
    }
    self.price.text = model.reqPrice;
    self.date.text = model.reqDate;
}

@end
