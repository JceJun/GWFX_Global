//
//  IXSymbolDetailNewVC.m
//  IXApp
//
//  Created by Larry on 2018/5/16.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXSymbolDetailNewVC.h"
#import "IXSmybolDetailMgr.h"
#import "IXSymbolDetailBtomV.h"
#import "IXTSEngine.h"
#import "IXCSEngine.h"
#import "IXKLineCell.h"
#import "IXSymbolPosTitleM.h"
#import "IXOrderStateRefreashView.h"
#import "IXHorizontalkVC.h"
#import "IXAccountBalanceModel.h"
#import "IXSmybolDetailMgr.h"
#import "IXDBSymbolSubMgr.h"
#import "IXDetailSymbolCell.h"
#import "IXSymbolDeepCell.h"
#import "IxItemSymbolSub.pbobjc.h"
#import "IXSymbolPositionM.h"
#import "NSString+FormatterPrice.h"
#import "IXDataProcessTools.h"
#import "IXTradeMarketModel.h"
#import "IXSymSearchVC.h"
#import "IXContractPropertiesVC.h"
#import "IXStaticsMgr.h"
#import "IXSymbolPositionTitleCell.h"
#import "IXSymbolPositionContentCell.h"
#import "IXOpenRootVC.h"
#import "IXDomDealVC.h"
#import "IXPositionM.h"
#import "IXPositionInfoVC.h"

@interface IXSymbolDetailNewVC ()<UITableViewDelegate,UITableViewDataSource>//IXNewGuideVDelegate


@property (nonatomic, strong) UITableView *contentTable;

@property (nonatomic, strong) IXSymbolDetailBtomV   * bottomOperationView;

@property (nonatomic, strong) IXTSEngine *tsEngine;
@property (nonatomic, strong) IXCSEngine *csEngine;
@property (nonatomic, strong) IXKLineCell *klineCell;



@property (nonatomic, strong) UIView *coverView;

@property (nonatomic, strong) IXSymbolPosTitleM *symbolPosTitleModel;

@property (nonatomic, strong) IXOrderStateRefreashView *unOpenQuote;

@property (nonatomic, assign)IXKLineType curKlineType;

@property (nonatomic, strong)IXHorizontalkVC *hkvc;

@end

@implementation IXSymbolDetailNewVC

- (void)dealloc{
    [self removeKVO];
}

- (void)initData{
    [IXAccountBalanceModel shareInstance].buyVol = 0;
    [IXAccountBalanceModel shareInstance].sellVol = 0;
    [IXAccountBalanceModel shareInstance].symProfit = 0;

}

- (void)registKVO{
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_ADD);
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_DELETE);
    
    //持仓更新相关的key
    IXTradeData_listen_regist(self, PB_CMD_POSITION_ADD);
    IXTradeData_listen_regist(self, PB_CMD_POSITION_UPDATE);
    IXTradeData_listen_regist(self, PB_CMD_POSITION_LIST);
}

- (void)removeKVO{
    NSLog(@"=======%s======",__func__);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_DELETE);
    
    //持仓更新相关的key
    IXTradeData_listen_resign(self, PB_CMD_POSITION_ADD);
    IXTradeData_listen_resign(self, PB_CMD_POSITION_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_POSITION_LIST);
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if( IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_SUB_ADD) ){
        proto_symbol_sub_add  *pb = ((IXTradeDataCache *)object).pb_symbol_sub_add;
        if (pb.result == 0) {
            if (pb.symbolSub) {
                BOOL flag = [IXDBSymbolSubMgr saveSymbolSubInfo:pb.symbolSub
                                                      accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
                if (flag) {
                    [self refreashUI];
                    [SVProgressHUD showSuccessWithStatus:LocalizedString(@"添加自选成功")];
                }
            }
        } else {
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isChangeSubStatus = NO;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LocalizedString(@"添加自选")
                                                                message:pb.comment
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_SUB_DELETE)) {
        proto_symbol_sub_delete *pb = ((IXTradeDataCache *)object).pb_symbol_sub_delete;
        if (pb.result == 0) {
            [self refreashUI];
            [SVProgressHUD showSuccessWithStatus:LocalizedString(@"取消自选成功")];
        } else {
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isChangeSubStatus = NO;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LocalizedString(@"删除自选")
                                                                message:pb.comment
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else if ( [keyPath isEqualToString:PB_CMD_POSITION_ADD] ) {
        proto_position_add *pb = ((IXTradeDataCache *)object).pb_position_add;
        if ( pb.result == 0 && (pb.position.symbolid == [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p) ) {
            proto_position_add *pb = ((IXTradeDataCache *)object).pb_position_add;
            [self addPosition:pb.position];
        }
        
    }else if ( [keyPath isEqualToString:PB_CMD_POSITION_UPDATE] ){
        proto_position_update *pb = ((IXTradeDataCache *)object).pb_position_update;
        if ( pb.result == 0 && (pb.position.symbolid == [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p) ) {
            [self delPosition:pb.position];
        }
    }else if( [keyPath isEqualToString:PB_CMD_POSITION_LIST] ){
        proto_position_list *pb = ((IXTradeDataCache *)object).pb_position_list;
        [self positionList:pb];
    }
}

- (void)addPosition:(item_position *)position
{
    [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXSymbolPositionM *model = (IXSymbolPositionM *)obj;
        if (0 == model.positionId - position.id_p) {
            return;
        }
    }];
    [self updatePos:position WithAdd:YES];
    
    IXSymbolPositionM *model = [[IXSymbolPositionM alloc] init];
    
    model.positionId = position.id_p;
    NSString *direction = LocalizedString(@"买.");
    if(position.direction == item_position_edirection_DirectionSell){
        direction = LocalizedString(@"卖.");
    }
    model.posDirection = direction;
    
    model.posNum = [NSString stringWithFormat:@"%.lf",position.volume];
    
    model.showNum = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:position.volume contractSizeNew:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.contractSizeNew volDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits] withDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits];
    
    model.posPrice = [NSString formatterPrice: [NSString stringWithFormat:@"%lf",position.openPrice]
                                   WithDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits];
    
    PFTModel *profitM = [[IXAccountBalanceModel shareInstance] caculateProfit:position];
    model.posProfit = [NSString stringWithFormat:@"%.2lf",profitM.profit];
    
    [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr insertObject:model atIndex:0];
    
    @try{
        [self.contentTable beginUpdates];
        [self.contentTable insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]]
                                 withRowAnimation:UITableViewRowAnimationNone];
        [self.contentTable endUpdates];
    }@catch(NSException *exception) {
        
    }
    
    double vol = [self.symbolPosTitleModel.positionNum doubleValue] + position.volume;
    
    double sumPrc = [_symbolPosTitleModel.nPrice doubleValue] * [self.symbolPosTitleModel.positionNum doubleValue]
    + position.volume * position.openPrice;
    NSDecimalNumber *sumDec = [NSDecimalNumber decimalNumberWithString:[@(sumPrc) stringValue]];
    
    NSDecimalNumber *volDec = [NSDecimalNumber decimalNumberWithString:[@(vol) stringValue]];
    
    NSDecimalNumber *avgPrcDec = [sumDec decimalNumberByDividingBy:volDec
                                                      withBehavior:
                                  [IXRateCaculate handlerPriceWithDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits]];
    
    _symbolPosTitleModel.showTotNum = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:vol contractSizeNew:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.contractSizeNew volDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits] withDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits];
    
    _symbolPosTitleModel.nPrice = [NSString formatterPrice:[avgPrcDec stringValue]
                                                WithDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits];
    
    [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];;
}

- (void)delPosition:(item_position *)position
{
    [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        IXSymbolPositionM *model = (IXSymbolPositionM *)obj;
        if (0 == model.positionId - position.id_p) {
            
            [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr removeObjectAtIndex:idx];
            [self updatePos:position WithAdd:NO];
            
            [self.contentTable beginUpdates];
            [self.contentTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:1]]
                                     withRowAnimation:UITableViewRowAnimationNone];
            [self.contentTable endUpdates];
            
            double vol = [self.symbolPosTitleModel.positionNum doubleValue] - position.volume;
            
            
            
            double sumPrc = [_symbolPosTitleModel.nPrice doubleValue] * [_symbolPosTitleModel.positionNum doubleValue] - position.volume * position.openPrice;
            NSDecimalNumber *sumDec = [NSDecimalNumber decimalNumberWithString:[@(sumPrc) stringValue]];
            
            NSDecimalNumber *volDec = [NSDecimalNumber decimalNumberWithString:[@(vol) stringValue]];
            
            _symbolPosTitleModel.showTotNum = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:vol contractSizeNew:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.contractSizeNew volDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits] withDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits];
            if (vol) {
                NSDecimalNumber *avgPrcDec = [sumDec decimalNumberByDividingBy:volDec
                                                                  withBehavior:
                                              [IXRateCaculate handlerPriceWithDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits]];
                
                _symbolPosTitleModel.nPrice = [NSString formatterPrice:[avgPrcDec stringValue]
                                                            WithDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits];
            }else{
                _symbolPosTitleModel.nPrice = @"--";
            }
            [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:1]
                             withRowAnimation:UITableViewRowAnimationNone];;
            
            *stop = YES;
        }
    }];
}

- (void)positionList:(proto_position_list *)pos
{
    double sumVolume = 0;
    double sumProfit = 0;
    
    NSMutableArray *arr = [@[] mutableCopy];
    if (pos) {
        arr = [pos.positionArray mutableCopy];
    }else{
        arr = [[IXTradeDataCache shareInstance].pb_cache_position_list mutableCopy];
    }
    
    NSDecimalNumber *sumDec = [NSDecimalNumber decimalNumberWithString:@"0"];
    for (item_position *position in arr ) {
        
        if (position.symbolid == [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p) {
            BOOL needAdd = YES;
            for( IXSymbolPositionM *model in [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr ) {
                if (0 == model.positionId - position.id_p) {
                    needAdd = NO;
                    break;
                }
            }
            if(!needAdd)
                continue;
            
            [self updatePos:position WithAdd:YES];
            
            IXSymbolPositionM *model = [[IXSymbolPositionM alloc] init];
            model.positionId = position.id_p;
            NSString *direction = LocalizedString(@"买.");
            if(position.direction == item_position_edirection_DirectionSell){
                direction = LocalizedString(@"卖.");
            }
            model.posDirection = direction;
            
            sumVolume += position.volume;
            model.posNum = [NSString stringWithFormat:@"%.lf",position.volume];
            
            model.showNum = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:position.volume contractSizeNew:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.contractSizeNew volDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits] withDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits];
            model.posPrice = [NSString formatterPrice: [NSString stringWithFormat:@"%lf",position.openPrice]
                                           WithDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits];
            
            PFTModel *pftM = [[IXAccountBalanceModel shareInstance] caculateProfit:position];
            model.posProfit = [NSString stringWithFormat:@"%.2lf",pftM.profit];
            
            [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr addObject:model];
            
            NSDecimalNumber *tmpSumValue = [NSDecimalNumber decimalNumberWithString:model.posNum];
            tmpSumValue = [tmpSumValue decimalNumberByMultiplyingBy:
                           [NSDecimalNumber decimalNumberWithString:model.posPrice]];
            
            sumDec = [sumDec decimalNumberByAdding:tmpSumValue
                                      withBehavior:
                      [IXRateCaculate handlerPriceWithDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits]];
            sumProfit += pftM.profit;
        }
    }
    _symbolPosTitleModel = [[IXSymbolPosTitleM alloc] init];
    _symbolPosTitleModel.positionNum = [NSString stringWithFormat:@"%.lf",sumVolume];
    _symbolPosTitleModel.showTotNum = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:sumVolume contractSizeNew:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.contractSizeNew volDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits] withDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.volDigits];
    
    if ( [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.unitLanName &&
        [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.unitLanName isKindOfClass:[NSString class]] &&
        [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.unitLanName length] ) {
        _symbolPosTitleModel.displayName = [IXDataProcessTools showCurrentUnitLanName:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.unitLanName];
    }
    
    if ( sumVolume != 0 ) {
        
        NSDecimalNumber *volDec = [NSDecimalNumber decimalNumberWithString:
                                   [NSString stringWithFormat:@"%f",sumVolume]];
        
        NSDecimalNumber *avgPrcDec = [sumDec decimalNumberByDividingBy:volDec
                                                          withBehavior:
                                      [IXRateCaculate handlerPriceWithDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits]];
        
        _symbolPosTitleModel.nPrice = [NSString formatterPrice:[avgPrcDec stringValue]
                                                    WithDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits];
    }
    
    _symbolPosTitleModel.profit = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",sumProfit]
                                                WithDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits];
    [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];;
}


- (void)updatePos:(item_position *)pos WithAdd:(BOOL)addPos
{
    double itemPos = addPos ? pos.volume : -pos.volume;
    if (pos.direction == item_order_edirection_DirectionBuy) {
        [self willChangeValueForKey:KBUYVOL];
        [IXAccountBalanceModel shareInstance].buyVol += itemPos;
        [self didChangeValueForKey:KBUYVOL];
    }else{
        [self willChangeValueForKey:KSELLVOL];
        [IXAccountBalanceModel shareInstance].sellVol += itemPos;
        [self didChangeValueForKey:KSELLVOL];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.title = LocalizedString(@"详细报价");
    
    [self initData];
    [self registKVO];
    [self.view addSubview:self.contentTable];
    [self.view addSubview:self.bottomOperationView];
    [self setRightBarItemButton];
    
    // 新手引导
//    if (NewGuideEnable) {
//        [self loadNewGuideV];
//    }
    [self updatePosCon];
}

- (void)updatePosCon
{
    if ( [IXTradeDataCache shareInstance].pb_cache_position_list.count ) {
        [self positionList:nil];
    }
}


- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel) {
        [[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel resetInitInfo];
        [self resetCellQuoteInfo];
    }
    [self subscribeDynamicPrice];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (self.navigationController.childViewControllers.count <= 2) {
        if (![IXSmybolDetailMgr sharedIXSmybolDetailMgr].shouldHKVCResponse) {
            [self cancelQuote];
            if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel) {
                [[IXQuoteDataCache shareInstance] saveOneDynQuote:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel];
            }
        }
    }
}

//订阅数据
- (void)subscribeDynamicPrice
{
    if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel && [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.marketId),
                           @"id":@([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] subscribeDetailPriceWithSymbolAry:arr];
        if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice <= 0) {
            [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:arr];
        }
    }
}

//取消订阅
- (void)cancelQuote
{
    if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel && [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.marketId),
                           @"id":@([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] unSubscribeDetailPriceWithSymbolAry:arr];
    }
}

- (void)setRightBarItemButton
{
    UIView *item = [[UIView alloc] initWithFrame:CGRectMake( 0, 29, 62, 31)];
    UIButton *contractBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    contractBtn.frame = CGRectMake( 31, 0, 31, 31);
    [item addSubview:contractBtn];
    if ([IXUserInfoMgr shareInstance].isNightMode) {
        [contractBtn setImage:[UIImage imageNamed:@"symbolDetail_contract_D"] forState:UIControlStateNormal];
    } else {
        [contractBtn setImage:[UIImage imageNamed:@"symbolDetail_contract"] forState:UIControlStateNormal];
    }
    [contractBtn setImageEdgeInsets:UIEdgeInsetsMake( 3, 5, 2, 0)];
    [contractBtn addTarget:self action:@selector(responseToContract) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *selfSymbolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    selfSymbolBtn.frame = CGRectMake( 0, 0, 31, 31);
    [item addSubview:selfSymbolBtn];
    selfSymbolBtn.tag = 1;
    [selfSymbolBtn setImage:[UIImage imageNamed:[self getSubStatusImageName]] forState:UIControlStateNormal];
    [selfSymbolBtn setImageEdgeInsets:UIEdgeInsetsMake( 3, 5, 2, 0)];
    [selfSymbolBtn addTarget:self action:@selector(responseToOptional) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:item];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (NSString *)getSubStatusImageName
{
    if ([self checkSubStatus]) {
        [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isSubSymbol = YES;
        
        if( [IXUserInfoMgr shareInstance].isNightMode ){
            return @"market_symbolDetail_col_D";
        }
        return @"market_symbolDetail_col";
    }else{
        [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isSubSymbol = NO;
        
        return @"market_symbolDetail_unCol";
    }
}

#pragma mark btn action
- (void)responseToOptional
{
    if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
        [self showRegistLoginAlert];
        return;
    }
    if (![IXSmybolDetailMgr sharedIXSmybolDetailMgr].isChangeSubStatus) {
        [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isChangeSubStatus = YES;
        [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isSubSymbol = ![IXSmybolDetailMgr sharedIXSmybolDetailMgr].isSubSymbol;
        if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].isSubSymbol) {
            [self responseToSubSymbol];
        }else{
            [self delSubSymbol];
        }
    }
}

- (void)responseToSearch
{
    IXSymSearchVC *searchVC = [[IXSymSearchVC alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)responseToContract
{
    IXContractPropertiesVC *VC = [[IXContractPropertiesVC alloc] init];
    VC.symbolModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel;
    [self.navigationController pushViewController:VC animated:YES];
}


- (UIView *)coverView
{
    if (!_coverView) {
        _coverView = [[UIView alloc] initWithFrame:[[UIApplication sharedApplication].delegate window].bounds];
        _coverView.backgroundColor = [UIColor blackColor];
        _coverView.alpha = 0.75f;
        _coverView.hidden = YES;
        [[[UIApplication sharedApplication].delegate window]  addSubview:_coverView];
    }
    return _coverView;
}

- (void)addPopView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self.unOpenQuote];
    _unOpenQuote.tipTitle = @"交易已执行";
    _unOpenQuote.tipDes = @"以115.80价格买入400股 腾讯控股";
    _unOpenQuote.tipContent = @"提交保证金 44.31HKD，佣金0.12HKD";
}

- (IXOrderStateRefreashView *)unOpenQuote
{
    if (!_unOpenQuote) {
        CGRect frame = CGRectMake( 0, kNavbarHeight, kScreenWidth, 86);
        _unOpenQuote = [[IXOrderStateRefreashView alloc] initWithFrame:frame];
    }
    return _unOpenQuote;
}

- (IXTSEngine *)tsEngine
{
    if (!_tsEngine) {
        _tsEngine = [[IXTSEngine alloc] initWithDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits
                                       scheduleCateID:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.scheduleCataId
                                            lastPrice:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice];
        _tsEngine.displayV.orientation = IXTSOrientationV;
        [_tsEngine setNightMode:[IXUserInfoMgr shareInstance].isNightMode];
        [_tsEngine setRedUpBlueDown:[IXUserInfoMgr shareInstance].settingRed];
        
        weakself;
        _tsEngine.request = ^(time_t endTime, int32_t cnt){
            item_stk *stk = [[item_stk alloc] init];
            stk.marketid = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.marketId;
            stk.id_p = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p;
            proto_quote_kdata_req *req = [[proto_quote_kdata_req alloc] init];
            req.stk = stk;
            req.ktype = MIN_1;
            req.startTime = 0;
            req.endTime = endTime;
            req.reqCount = cnt;
            [[IXTCPRequest shareInstance] subscribeChartDataWithParam:req];
        };
        _tsEngine.clicked = ^(){
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].shouldHKVCResponse = YES;
            weakSelf.hkvc.hideStatusBar = NO;
            weakSelf.hkvc.kLineType = weakSelf.curKlineType;
            [weakSelf.navigationController pushViewController:weakSelf.hkvc animated:YES];
            weakSelf.hkvc.hkvcWillDismiss = ^(){
                [IXSmybolDetailMgr sharedIXSmybolDetailMgr].shouldHKVCResponse = NO;
                [weakSelf changeKLineType:weakSelf.hkvc.kLineType];
            };
        };
    }
    return _tsEngine;
}

- (IXCSEngine *)csEngine
{
    if (!_csEngine) {
        _csEngine = [[IXCSEngine alloc] initWithDigit:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits
                                             marketID:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.marketId];
        _csEngine.displayV.orientation = IXCSOrientationV;
        _csEngine.symbolId = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p;
        [_csEngine setNightMode:[IXUserInfoMgr shareInstance].isNightMode];
        [_csEngine setRedUpBlueDown:[IXUserInfoMgr shareInstance].settingRed];
        
        weakself;
        _csEngine.request = ^(K_TYPE ktype, time_t endTime, int32_t cnt) {
            item_stk *stk = [[item_stk alloc] init];
            stk.marketid = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.marketId;
            stk.id_p = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p;
            
            proto_quote_kdata_req *req = [[proto_quote_kdata_req alloc] init];
            req.stk = stk;
            req.ktype = ktype;
            req.startTime = 0;
            req.endTime = endTime;
            req.reqCount = cnt;
            
            [[IXTCPRequest shareInstance] subscribeChartDataWithParam:req];
        };
        _csEngine.clicked = ^(){
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].shouldHKVCResponse = YES;
            weakSelf.hkvc.hideStatusBar = NO;
            weakSelf.hkvc.kLineType = weakSelf.curKlineType;
            [weakSelf.navigationController pushViewController:weakSelf.hkvc animated:YES];
            weakSelf.hkvc.hkvcWillDismiss = ^(){
                [IXSmybolDetailMgr sharedIXSmybolDetailMgr].shouldHKVCResponse = NO;
                [weakSelf changeKLineType:weakSelf.hkvc.kLineType];
            };
        };
    }
    return _csEngine;
}

- (IXHorizontalkVC *)hkvc
{
    if (!_hkvc) {
        _hkvc = [[IXHorizontalkVC alloc] initWithTradeModel:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel lastPrice:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice];
    }
    return _hkvc;
}

- (IXKLineCell *)klineCell
{
    if (!_klineCell) {
        IXKLineType cacheType = IXKLineTypeMin1;
        NSNumber    * cacheTypeN = [[NSUserDefaults standardUserDefaults] objectForKey:kCacheType];
        if (cacheTypeN) {
            cacheType = (IXKLineType)[cacheTypeN integerValue];
        }
        
        _curKlineType = IXKLineTypeMin1;
        _klineCell = [[IXKLineCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        _klineCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [_klineCell showDisplayView:self.csEngine.displayV type:IXKLineTypeMin1];
        [self.csEngine.displayV setFrame:CGRectMake(0, 30, kScreenWidth, 256)];
        [self changeKLineType:cacheType];
        
        weakself;
        _klineCell.btnClick = ^(IXKLineType type) {
            [weakSelf changeKLineType:type];
        };
    }
    return _klineCell;
}

static  NSString    * kCacheType = @"ix_cacheKLineType";

- (void)changeKLineType:(IXKLineType)type
{
    _curKlineType = type;
    [[NSUserDefaults standardUserDefaults] setObject:@(type) forKey:kCacheType];
    
    if (type == IXKLineTypeTline) {
        [self.klineCell showDisplayView:self.tsEngine.displayV type:type];
        [self.tsEngine.displayV setFrame:CGRectMake(0, 30, kScreenWidth, 256)];
        [self.tsEngine display];
        //view没有父视图则不刷新，因此此处将其从父视图上删除
        [self.csEngine.displayV removeFromSuperview];
    } else {
        [self.klineCell showDisplayView:self.csEngine.displayV type:type];
        [self.csEngine.displayV setFrame:CGRectMake(0, 30, kScreenWidth, 256)];
        [self.csEngine showKLineWithType:type];
        //view没有父视图则不刷新，因此此处将其从父视图上删除
        [self.tsEngine.displayV removeFromSuperview];
    }
    [[IXStaticsMgr shareInstance] updateKlineType:type];
}

- (UITableView *)contentTable
{
    if (!_contentTable) {
        CGFloat height = kScreenHeight - kNavbarHeight - 43 - kBtomMargin;
        _contentTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, height)];
        _contentTable.delegate = self;
        _contentTable.dataSource = self;
        _contentTable.separatorStyle = UITableViewCellSelectionStyleNone;
        _contentTable.backgroundColor = [UIColor clearColor];
        _contentTable.showsVerticalScrollIndicator = NO;
        [_contentTable registerClass:[IXKLineCell class]
              forCellReuseIdentifier:NSStringFromClass([IXKLineCell class])];
        [_contentTable registerClass:[IXSymbolDeepCell class]
              forCellReuseIdentifier:NSStringFromClass( [IXSymbolDeepCell class])];
        [_contentTable registerClass:[IXDetailSymbolCell class]
              forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        [_contentTable registerClass:[UITableViewCell class]
              forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        [_contentTable registerClass:[IXSymbolPositionTitleCell class]
              forCellReuseIdentifier:NSStringFromClass([IXSymbolPositionTitleCell class])];
        [_contentTable registerClass:[IXSymbolPositionContentCell class]
              forCellReuseIdentifier:NSStringFromClass([IXSymbolPositionContentCell class])];
        _contentTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _contentTable;
}

- (IXSymbolDetailBtomV *)bottomOperationView
{
    if(!_bottomOperationView){
        
        CGSize  size = [IXSymbolDetailBtomV targetSize];
        CGRect frame = CGRectMake( 0, kScreenHeight - kNavbarHeight - size.height, kScreenWidth, size.height);
        _bottomOperationView = [[IXSymbolDetailBtomV alloc] initWithFrame:frame];
        
        [_bottomOperationView.selBtn addTarget:self
                                        action:@selector(openOrder:)
                              forControlEvents:UIControlEventTouchUpInside];
        _bottomOperationView.selBtn.tag  = item_order_edirection_DirectionSell;
        
        [_bottomOperationView.buyBtn addTarget:self
                                        action:@selector(openOrder:)
                              forControlEvents:UIControlEventTouchUpInside];
        _bottomOperationView.buyBtn.tag  = item_order_edirection_DirectionBuy;
    }
    
    BOOL enable = NO;
    for (UIButton *btn in _bottomOperationView.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            enable = [IXDataProcessTools canTradeByState:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeState orderDir:btn.tag];
            if (!enable) {
                btn.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x4c6072);
                
                if (kBtomMargin == 0) {
                    CGRect frame = btn.frame;
                    frame.size.width = kScreenWidth/2 - 0.5;
                    if ( frame.origin.x == kScreenWidth/2 ) {
                        frame.origin.x = kScreenWidth/2 + 0.5;
                    }
                    btn.frame = frame;
                }
            }
            btn.enabled = enable;
        }
    }
    return _bottomOperationView;
}

- (void)openOrder:(UIButton *)send
{
    //demo账号、引导去注册登录
    if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
        [self showRegistLoginAlert];
        return;
    }
    if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel.SellPrc.count > 0 &&
        [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel.BuyPrc.count > 0) {
        IXOpenRootVC *openRoot = [[IXOpenRootVC alloc] init];
        openRoot.tradeModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel;
        openRoot.openModel.nLastClosePrice = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice;
        if (send.tag == 1) {
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.direction = item_order_edirection_DirectionBuy;
        }else{
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.direction = item_order_edirection_DirectionSell;
        }
        [self.navigationController pushViewController:openRoot animated:YES];
    }
}

#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource
- (void)addSepLineImg:(UITableViewCell *)cell
{
    UIImageView *sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, kLineHeight)];
    sepLineImg.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0,0x242a36);
    [cell.contentView addSubview:sepLineImg];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (0 == section) {
        return 8;
    }
    return 1 + [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr.count;
}

- (IXDetailSymbolCell *)detailSymCell
{
    IXDetailSymbolCell *cell = [self.contentTable dequeueReusableCellWithIdentifier:
                                NSStringFromClass([IXDetailSymbolCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tradeState = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeState;
    cell.marketId = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.marketId;
    cell.nLastClosePrice = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice;
    cell.symbolModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel;
    cell.quoteModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel;
    
    return cell;
}

- (UITableViewCell *)dirCell
{
    UITableViewCell *cell = [self.contentTable dequeueReusableCellWithIdentifier:
                             NSStringFromClass([UITableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.dk_backgroundColorPicker = DKNavBarColor;
    
    if(cell.contentView.subviews.count == 0){
        CGRect frame = CGRectMake( 15, 10, (kScreenWidth - 30)/2, 12);
        UILabel *leftMarketLbl = [IXUtils createLblWithFrame:frame
                                                    WithFont:PF_MEDI(12)
                                                   WithAlign:NSTextAlignmentLeft
                                                  wTextColor:0x4c6072
                                                  dTextColor:0x8395a4];
        [cell.contentView addSubview:leftMarketLbl];
        leftMarketLbl.text = LocalizedString(@"卖出价");
        
        frame.origin.x += kScreenWidth/2;
        UILabel *rightMarketLbl = [IXUtils createLblWithFrame:frame
                                                     WithFont:PF_MEDI(12)
                                                    WithAlign:NSTextAlignmentLeft
                                                   wTextColor:0x4c6072
                                                   dTextColor:0x8395a4];
        [cell.contentView addSubview:rightMarketLbl];
        rightMarketLbl.text = LocalizedString(@"买入价");
    }
    [self addSepLineImg:cell];
    return cell;
}


- (IXSymbolDeepCell *)deepPrcCell:(NSIndexPath *)indexPath
{
    if (indexPath.row >= 3 && indexPath.row <= 7) {
        IXSymbolDeepCell *cell = [self.contentTable dequeueReusableCellWithIdentifier:
                                  NSStringFromClass([IXSymbolDeepCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 3 || indexPath.row == 7) {
            cell.cellContentHeight = 23;
            if (indexPath.row == 3) {
                [self addSepLineImg:cell];
            }
        } else {
            cell.cellContentHeight = 22;
        }
        NSInteger tag = (indexPath.row - 3);
        cell.tag = tag;
        IXQuoteM *quoteModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel;
        if (quoteModel.BuyPrc.count > indexPath.row) {
            [cell setBuyPrc:[self formatterPrice:quoteModel.BuyPrc[tag]]
                     BuyVol:[self formatterPrice:quoteModel.BuyVol[tag]]
                    SellPrc:[self formatterPrice:quoteModel.SellPrc[tag]]
                    SellVol:[self formatterPrice:quoteModel.SellVol[tag]]];
        }
        return cell;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            switch (indexPath.row) {
                case 0:{
                    return [self detailSymCell];
                }
                    break;
                case 1:{
                    return self.klineCell;
                }
                    break;
                case 2:{
                    return [self dirCell];
                }
                    break;
                default:{
                    return [self deepPrcCell:indexPath];
                }
                    break;
            }
        }
            break;
        default:{
            switch (indexPath.row) {
                case 0:{
                    IXSymbolPositionTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSymbolPositionTitleCell class])];
                    cell.contentView.dk_backgroundColorPicker = DKNavBarColor;
                    
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [self addSepLineImg:cell];
                    
                    //                    UIImageView *sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 58, kScreenWidth, 0.5)];
                    //                    sepLineImg.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0,0x242a36);
                    //                    [cell.contentView addSubview:sepLineImg];
                    
                    cell.model = _symbolPosTitleModel;
                    return cell;
                }
                    break;
                    
                default:{
                    IXSymbolPositionContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSymbolPositionContentCell class])];
                    cell.contentView.dk_backgroundColorPicker = DKNavBarColor;
                    
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    UIImageView *sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 32, kScreenWidth, 1)];
                    
                    UIImage *dot_w = [[UIImage imageNamed:@"common_dotLine"]
                                      resizableImageWithCapInsets:UIEdgeInsetsMake( 0, 0, 0, 0)];
                    UIImage *dot_d = [[UIImage imageNamed:@"common_dotLine_dark"]
                                      resizableImageWithCapInsets:UIEdgeInsetsMake( 0, 0, 0, 0)];
                    sepLineImg.dk_imagePicker = DKImageWithImgs(dot_w,dot_d);
                    [cell.contentView addSubview:sepLineImg];
                    cell.tag = indexPath.row;
                    cell.model = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr[indexPath.row - 1];
                    return cell;
                }
                    break;
            }
        }
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            switch (indexPath.row) {
                case 0:
                    return 73;
                    break;
                case 1:
                    return 286;
                    break;
                case 2:
                    return 32;
                    break;
                default:{
                    if (indexPath.row == 3||
                        indexPath.row == 7){
                        return 23;
                    }
                    return 22;
                }
            }
        }
            break;
        default:{
            switch (indexPath.row) {
                case 0:{
                    if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr.count) {
                        return 59;
                    } else {
                        return 162;
                    }
                }
                    break;
                default:
                    return 33;
                    break;
            }
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:{
            if( indexPath.row >= 3 && indexPath.row <= 7 ){
                //demo账号、不允许交易
                if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
                    return;
                }
                UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
                if ([IXDataProcessTools isNormalAddOptiosByState:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeState] && cell != self.klineCell) {
                    if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel.SellPrc.count > 0 &&
                        [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel.BuyPrc.count > 0) {
                        
                        IXDomDealVC *openRoot = [[IXDomDealVC alloc] init];
                        openRoot.tradeModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel;
                        openRoot.nLastClosePrice = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice;
                        openRoot.detailVC = self;
                        [self.navigationController pushViewController:openRoot animated:YES];
                    }
                }
            }
        }
            break;
        case 1:{
            if (indexPath.row >= 1) {
                //demo账号、不允许交易
                if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
                    return;
                }
                IXPositionM *pModel =[[IXPositionM alloc] init];
                pModel.symbolModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel;
                pModel.marketId = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.marketId;
                if ([IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr && [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr.count > indexPath.row - 1) {
                    IXSymbolPositionM *model = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr[indexPath.row - 1];
                    for ( item_position *pos in   [IXTradeDataCache shareInstance].pb_cache_position_list ) {
                        if ( pos.id_p == model.positionId ) {
                            pModel.position = pos;
                            break;
                        }
                    }
                }
                
                IXQuoteM *model = [[IXAccountBalanceModel shareInstance].quoteDataDic objectForKey:
                                   [NSString stringWithFormat:@"%ld",(long)[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p]];
                pModel.quoteModel = model;
                pModel.stoploss = [NSDecimalNumber decimalNumberWithString:
                                   [NSString stringWithFormat:@"%f",pModel.position.stopLoss]];
                pModel.takeprofit = [NSDecimalNumber decimalNumberWithString:
                                     [NSString stringWithFormat:@"%f",pModel.position.takeProfit]];
                IXPositionInfoVC *VC = [[IXPositionInfoVC alloc] initWithPositionModel:pModel];
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
        default:
            break;
    }
}

#pragma mark -
#pragma mark - others
- (void)needRefresh
{
    [self subscribeDynamicPrice];
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    switch (cmd) {
        case CMD_QUOTE_KDATA:
            [self processKayLine:arr];
            break;
        case CMD_QUOTE_PUB_DETAIL:{
            if (![IXSmybolDetailMgr sharedIXSmybolDetailMgr].refreashFeeQuote) {
                [self processDynQuote:arr];
            }
        }
            break;
        case CMD_QUOTE_SUB_DETAIL:
            [self processDynQuote:arr];
            break;
        case CMD_QUOTE_CLOSE_PRICE:
            [self processDynCloseQuote:arr];
            break;
        default:
            break;
    }
}

- (void)processKayLine:(NSMutableArray *)arr
{
    if (_curKlineType == IXKLineTypeTline) {
        [self.tsEngine requestDataResponse:arr];
    } else {
        [self.csEngine klineDataResponse:arr];
    }
}

- (void)processDynQuote:(NSMutableArray *)resultAry
{
    for (IXQuoteM *quoteModel in resultAry) {
        if (quoteModel.symbolId == [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p) {
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel = quoteModel;
            [self resetCellQuoteInfo];
            
            IXMetaData *meta = [quoteModel transferToMetaData];
            
            [_tsEngine dynamicDataReseponse:meta];
            [_csEngine dynamicDataReseponse:meta];
            
            return;
        }
    }
}

- (void)processDynCloseQuote:(NSMutableArray *)resultAry
{
    for (IXLastQuoteM *model in resultAry) {
        if (model.symbolId == [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p) {
            [IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice = model.nLastClosePrice;
            for (UITableViewCell *cell in [_contentTable visibleCells]) {
                if ([cell isKindOfClass:[IXDetailSymbolCell class]]) {
                    ((IXDetailSymbolCell *)cell).nLastClosePrice = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].nLastClosePrice;
                    break;
                }
            }
            break;
        }
    }
}

- (void)resetCellQuoteInfo
{
    @synchronized (self) {
        IXQuoteM *quoteModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel;
        [self refreashProfit];
        
        for (UITableViewCell *cell in [_contentTable visibleCells]) {
            if ([cell isKindOfClass:[IXDetailSymbolCell class]]) {
                ((IXDetailSymbolCell *)cell).quoteModel = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.quoteModel;
            } else if ([cell isKindOfClass:[IXSymbolDeepCell class]]) {
                IXSymbolDeepCell *quoteCell = (IXSymbolDeepCell *)cell;
                NSInteger tag = quoteCell.tag;
                if (quoteModel.BuyPrc.count > tag) {
                    [quoteCell setBuyPrc:[self formatterPrice:quoteModel.BuyPrc[tag]]
                                  BuyVol:[self formatterPrice:quoteModel.BuyVol[tag]]
                                 SellPrc:[self formatterPrice:quoteModel.SellPrc[tag]]
                                 SellVol:[self formatterPrice:quoteModel.SellVol[tag]]];
                }
            } else if ([cell isKindOfClass:[IXSymbolPositionTitleCell class]]) {
                IXSymbolPositionTitleCell *quoteCell = (IXSymbolPositionTitleCell *)cell;
                _symbolPosTitleModel.profit = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].totalProfit;
                quoteCell.model = _symbolPosTitleModel;
            } else if ([cell isKindOfClass:[IXSymbolPositionContentCell class]]) {
                IXSymbolPositionContentCell *quoteCell = (IXSymbolPositionContentCell *)cell;
                IXSymbolPositionM *model = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr[quoteCell.tag-1];
                quoteCell.model = model;
            }
        }
    }
}

- (void)refreashProfit
{
    double sumProfit = 0;
    for ( IXSymbolPositionM *model in [IXSmybolDetailMgr sharedIXSmybolDetailMgr].symbolPosArr ) {
        
        PFTModel *pftM = [[IXAccountBalanceModel shareInstance] caculateProfitPosId:model.positionId];
        NSDecimalNumber *dec = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",pftM.profit]];
        dec = [dec decimalNumberByRoundingAccordingToBehavior:[IXRateCaculate handlerPriceWithDigit:2]];
        
        model.posProfit = [NSString formatterPriceSign:[dec doubleValue] WithDigits:2];
        
        sumProfit += [dec doubleValue];
    }
    
    NSDecimalNumber *dec = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",sumProfit]];
    dec = [dec decimalNumberByRoundingAccordingToBehavior:[IXRateCaculate handlerPriceWithDigit:2]];
    [IXSmybolDetailMgr sharedIXSmybolDetailMgr].totalProfit = [NSString formatterPriceSign:[dec doubleValue] WithDigits:2];
    
    [self willChangeValueForKey:KSYMPFT];
    [IXAccountBalanceModel shareInstance].symProfit = [dec doubleValue];
    [self didChangeValueForKey:KSYMPFT];
}

//获取产品对应的持仓数据
- (NSMutableDictionary *)posInfo
{
    NSMutableDictionary *posDic = [NSMutableDictionary dictionary];
    for(item_position *position in [IXTradeDataCache shareInstance].pb_cache_position_list){
        if (position.symbolid == [IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p) {
            PFTModel *pftM = [[IXAccountBalanceModel shareInstance] caculateProfit:position];
            NSString *curProfit = [NSString stringWithFormat:@"%.2lf",pftM.profit];
            [posDic setValue:@[position,curProfit] forKey:[NSString stringWithFormat:@"%llu",position.id_p]];
        }
    }
    return posDic;
}

- (NSArray *)getProfitWithPosInfo:(NSMutableDictionary *)posInfo
{
    NSDecimalNumber *sumProfit = [NSDecimalNumber decimalNumberWithString:@"0"];
    NSDecimalNumber *sumPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
    NSDecimalNumber *sumVolume = [NSDecimalNumber decimalNumberWithString:@"0"];
    for(NSArray *posArr in [posInfo allValues]){
        item_position *position = posArr[0];
        double curVol = position.volume;
        double curPrc = position.openPrice;
        sumProfit = [sumProfit decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:posArr[1]]
                                        withBehavior:[self handler]];
        NSDecimalNumber *decVol = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",curVol]];
        NSDecimalNumber *decPrc = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",curPrc]];
        sumVolume = [sumVolume decimalNumberByAdding:decVol];
        sumPrice = [sumPrice decimalNumberByAdding:[decVol decimalNumberByMultiplyingBy:decPrc]];
    }
    
    if ( [sumVolume floatValue] != 0 ) {
        sumPrice = [sumPrice decimalNumberByDividingBy:sumVolume withBehavior:[self handler]];
    }
    
    return @[[sumProfit stringValue],[sumPrice stringValue]];
}


- (NSDecimalNumberHandler *)handler
{
    return   [NSDecimalNumberHandler
              decimalNumberHandlerWithRoundingMode:NSRoundPlain
              scale:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits
              raiseOnExactness:NO
              raiseOnOverflow:NO
              raiseOnUnderflow:NO
              raiseOnDivideByZero:YES];
}

- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price WithDigits:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.digits];
}

#pragma mark 添加自选
- (void)responseToSubSymbol
{
    if(![self checkSubStatus]) {
        proto_symbol_sub_add *subAdd = [[proto_symbol_sub_add alloc] init];
        item_symbol_sub *itemSub = [[item_symbol_sub alloc] init];
        [itemSub setAccountid:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        [itemSub setSymbolid:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p];
        subAdd.symbolSub = itemSub;
        [[IXTCPRequest shareInstance] addSubSymbolWithParam:subAdd];
    }
}

#pragma mark 删除自选
- (void)delSubSymbol
{
    NSDictionary *subDic = [self checkSubStatus];
    if (subDic && subDic[kID]) {
        proto_symbol_sub_delete *delSubSymbol = [[proto_symbol_sub_delete alloc] init];
        [delSubSymbol setId_p:[subDic[kID] doubleValue]];
        [delSubSymbol setAccountid:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        [[IXTCPRequest shareInstance] delSubSymbolWithParam:delSubSymbol];
    }
}

- (NSDictionary *)checkSubStatus
{
    if (![IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel) {
        return nil;
    }
    return  [IXDBSymbolSubMgr querySymbolSubBySymbolId:[IXSmybolDetailMgr sharedIXSmybolDetailMgr].tradeModel.symbolModel.id_p
                                             accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
}


- (void)refreashUI
{
    [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isChangeSubStatus = NO;
    UIView *itemView = self.navigationItem.rightBarButtonItem.customView;
    for (UIButton *btn in [itemView subviews]) {
        if (1 == btn.tag) {
            NSString *imageName = [IXSmybolDetailMgr sharedIXSmybolDetailMgr].isSubSymbol ? @"market_symbolDetail_col" : @"market_symbolDetail_unCol";
            [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
            [btn setImageEdgeInsets:UIEdgeInsetsMake( 3, 5, 2, 0)];
            break;
        }
    }
}

- (IXSymbolPosTitleM *)symbolPosTitleModel
{
    if (!_symbolPosTitleModel) {
        _symbolPosTitleModel = [[IXSymbolPosTitleM alloc] init];
    }
    return _symbolPosTitleModel;
}



@end
