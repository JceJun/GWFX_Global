//
//  IXOpenModel.m
//  IXApp
//
//  Created by Bob on 2016/12/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOpenModel.h"
#import "NSString+FormatterPrice.h"
#import "IXDBG_S_CataMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBGlobal.h"

@interface IXOpenModel ()

@property (nonatomic, strong) getPriceRange priceRange;
@property (nonatomic, strong) getProfitRange profitRange;
@property (nonatomic, strong) getLossRange lossRange;

//计算止盈止损范围的参考价格
@property (nonatomic, strong) NSDecimalNumber *refePrice;

@end

@implementation IXOpenModel

- (id)init
{
    self = [super init];
    if ( self ) {
        
        _showSLCount = 0;
        _showDeepPrcCount = 1;
        
        _parentId = -1;
        _commissionRate = 0;
    
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_ADD);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_LIST);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_ADD);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_LIST);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_CATA_UPDATE);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_LIST);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_LIST);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_CATA_UPDATE);
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    _commissionRate = [IXRateCaculate queryComissionWithSymbolId:_symbolModel.id_p
                                                WithSymbolCataId:_cataId];
}


- (id)initWithSymbolModel:(IXSymbolM *)model
           WithPriceRange:(getPriceRange)price
          WithProfitRange:(getProfitRange)profit
            WithLossRange:(getLossRange)loss
{
    self = [self init];
    if (self) {
        _priceRange = price;
        _profitRange = profit;
        _lossRange = loss;
        _symbolModel = model;
        
        NSDictionary *dic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:model.id_p];
        _marketId = [dic integerForKey:kMarketId];
        
        

        _parentId =  [IXDataProcessTools queryParentIdBySymbolId:model.id_p];
        
//        _minPrice = [NSDecimalNumber decimalNumberWithString:
//                     [NSString stringWithFormat:@"%d",model.stopLevel * model.pipsRatio]];
        _minPrice = [NSDecimalNumber decimalNumberWithString:
                     [NSString stringWithFormat:@"%d",model.stopLevel]];
        _minPrice = [_minPrice decimalNumberByMultiplyingByPowerOf10:-model.digits];
        
//        _maxPrice = [NSDecimalNumber decimalNumberWithString:
//                     [NSString stringWithFormat:@"%d",model.maxStopLevel * model.pipsRatio]];
        _maxPrice = [NSDecimalNumber decimalNumberWithString:
                     [NSString stringWithFormat:@"%d",model.maxStopLevel]];
        _maxPrice =  [_maxPrice decimalNumberByMultiplyingByPowerOf10:-model.digits];
        
//        _stepPrice = [NSDecimalNumber decimalNumberWithString:
//                      [NSString stringWithFormat:@"%d",model.pipsRatio]];
        _stepPrice = [NSDecimalNumber decimalNumberWithString:@"1"];
        _stepPrice = [_stepPrice decimalNumberByMultiplyingByPowerOf10:-model.digits];
    }
    return self;
}

- (void)setOrderDir:(item_order_edirection)orderDir
{
    _orderDir = orderDir;
    [self caculateRangePrice];
}

- (void)setOrderType:(NSString *)orderType
{
    _orderType = orderType;
    [self caculateRangePrice];
}

- (void)setQuoteModel:(IXQuoteM *)quoteModel
{
    if (quoteModel) {
        _noQuoteInfo = NO;
        _quoteModel = quoteModel;
        [self caculateRangePrice];
    } else {
      _noQuoteInfo = YES;
    }
}

//设置价格范围
- (void)caculateRangePrice
{
    if (_priceRange && _quoteModel) {
        if ([_orderType isEqualToString:LIMITORDER]) {
            if (_orderDir == item_order_edirection_DirectionBuy) {
                [self caculateLimitBuy];
            } else {
                [self caculateLimitSell];
            }
        } else if ([_orderType isEqualToString:STOPORDER]){
            if (_orderDir == item_order_edirection_DirectionBuy) {
                [self caculateStopBuy];
            } else {
                [self caculateStopSell];
            }
        }
    }
}

//限价买
- (void)caculateLimitBuy
{
    NSDecimalNumber *topPrice = [NSDecimalNumber decimalNumberWithString:_quoteModel.BuyPrc[0]];
    self.minRequestPrice = [topPrice decimalNumberBySubtracting:_maxPrice withBehavior:[self handlerDigits]];;
    self.maxRequestPrice = [topPrice decimalNumberBySubtracting:_minPrice withBehavior:[self handlerDigits]];;

    if([self.minRequestPrice doubleValue] < 0){
        self.minRequestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
    }
    NSString *left = [NSString formatterPrice:[self.minRequestPrice stringValue] WithDigits:_symbolModel.digits];
    NSString *right = [NSString formatterPrice:[self.maxRequestPrice stringValue] WithDigits:_symbolModel.digits];
    _priceRange(left, right);
}

//限价卖
- (void)caculateLimitSell
{
    NSDecimalNumber *topPrice = [NSDecimalNumber decimalNumberWithString:_quoteModel.SellPrc[0]];
    self.minRequestPrice = [topPrice decimalNumberByAdding:_minPrice withBehavior:[self handlerDigits]];;
    self.maxRequestPrice = [topPrice decimalNumberByAdding:_maxPrice withBehavior:[self handlerDigits]];;

    if([self.minRequestPrice doubleValue] < 0){
        self.minRequestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
    }
    
    NSString *left = [NSString formatterPrice:[self.minRequestPrice stringValue] WithDigits:_symbolModel.digits];
    NSString *right = [NSString formatterPrice:[self.maxRequestPrice stringValue] WithDigits:_symbolModel.digits];
    _priceRange(left, right);
}

//停损买
- (void)caculateStopBuy
{
    NSDecimalNumber *topPrice = [NSDecimalNumber decimalNumberWithString:_quoteModel.BuyPrc[0]];
    self.minRequestPrice = [topPrice decimalNumberByAdding:_minPrice withBehavior:[self handlerDigits]];;
    self.maxRequestPrice = [topPrice decimalNumberByAdding:_maxPrice withBehavior:[self handlerDigits]];;

    if([self.minRequestPrice doubleValue] < 0){
        self.minRequestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
    }
    NSString *left = [NSString formatterPrice:[self.minRequestPrice stringValue] WithDigits:_symbolModel.digits];
    NSString *right = [NSString formatterPrice:[self.maxRequestPrice stringValue] WithDigits:_symbolModel.digits];
    _priceRange(left, right);
}

//停损卖
- (void)caculateStopSell
{
    NSDecimalNumber *topPrice = [NSDecimalNumber decimalNumberWithString:_quoteModel.SellPrc[0]];
    self.minRequestPrice = [topPrice decimalNumberBySubtracting:_maxPrice withBehavior:[self handlerDigits]];
    self.maxRequestPrice = [topPrice decimalNumberBySubtracting:_minPrice withBehavior:[self handlerDigits]];;
    if([self.minRequestPrice doubleValue] < 0){
        self.minRequestPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
    }
    NSString *left = [NSString formatterPrice:[self.minRequestPrice stringValue] WithDigits:_symbolModel.digits];
    NSString *right = [NSString formatterPrice:[self.maxRequestPrice stringValue] WithDigits:_symbolModel.digits];
    
    _priceRange(left, right);
}

//市价单根据深度价的顶层价计算止盈止损范围
- (void)setRequestPrice:(NSDecimalNumber *)requestPrice
{
    if (requestPrice) {
        _requestPrice = requestPrice;
        _refePrice = [NSDecimalNumber decimalNumberWithString:[_requestPrice stringValue]];
        
        [self caculateRangeTakeProfit];
        [self caculateRangeStopLoss];
    }
}

//限价单、停损单根据填入的价格计算止盈止损范围
- (void)setDealPrice:(NSDecimalNumber *)dealPrice
{
    if (dealPrice) {
        _dealPrice = dealPrice;
        _refePrice = [NSDecimalNumber decimalNumberWithString:[_dealPrice stringValue]];

        [self caculateRangeTakeProfit];
        [self caculateRangeStopLoss];
    }
}

//止盈
- (void)caculateRangeTakeProfit
{
    if (_profitRange) {
        if (_orderDir == item_order_edirection_DirectionBuy) {
            self.minProfit = [_refePrice decimalNumberByAdding:_minPrice withBehavior:[self handlerDigits]];
            self.maxProfit = [_refePrice decimalNumberByAdding:_maxPrice withBehavior:[self handlerDigits]];

        } else {
            self.minProfit = [_refePrice decimalNumberBySubtracting:_maxPrice withBehavior:[self handlerDigits]];
            self.maxProfit = [_refePrice decimalNumberBySubtracting:_minPrice withBehavior:[self handlerDigits]];
            
            if ([self.minProfit doubleValue] < 0) {
                self.minProfit = [NSDecimalNumber decimalNumberWithString:@"0"];
            }
            
            if ([self.maxProfit integerValue] < 0) {
                self.maxProfit = [NSDecimalNumber decimalNumberWithString:@"0"];
            }
        }
        
        NSString *left = [NSString formatterPrice:[self.minProfit stringValue] WithDigits:_symbolModel.digits];
        NSString *right = [NSString formatterPrice:[self.maxProfit stringValue] WithDigits:_symbolModel.digits];

        _profitRange(left, right);
    }
}

//止损
- (void)caculateRangeStopLoss
{
    if (_lossRange) {
        if (_orderDir == item_order_edirection_DirectionBuy) {
            self.minLoss = [_refePrice decimalNumberBySubtracting:_maxPrice withBehavior:[self handlerDigits]];
            self.maxLoss = [_refePrice decimalNumberBySubtracting:_minPrice withBehavior:[self handlerDigits]];
            
            if ([self.minLoss doubleValue] < 0) {
                self.minLoss = [NSDecimalNumber decimalNumberWithString:@"0"];
            }
            
            if ([self.maxLoss doubleValue] < 0) {
                self.maxLoss = [NSDecimalNumber decimalNumberWithString:@"0"];
            }
            
        } else {
            self.minLoss = [_refePrice decimalNumberByAdding:_minPrice withBehavior:[self handlerDigits]];
            self.maxLoss = [_refePrice decimalNumberByAdding:_maxPrice withBehavior:[self handlerDigits]];
        }
        
        NSString *left = [NSString formatterPrice:[self.minLoss stringValue] WithDigits:_symbolModel.digits];
        NSString *right = [NSString formatterPrice:[self.maxLoss stringValue] WithDigits:_symbolModel.digits];

        _lossRange(left, right);
    }
}

- (NSDecimalNumberHandler *)handlerDigits
{
    return  [NSDecimalNumberHandler
             decimalNumberHandlerWithRoundingMode:NSRoundPlain
             scale:_symbolModel.digits
             raiseOnExactness:NO
             raiseOnOverflow:NO
             raiseOnUnderflow:NO
             raiseOnDivideByZero:YES];
}


@end
