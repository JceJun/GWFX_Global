//
//  IXOpenTypeCell.h
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTradeTypeV.h"

typedef void(^chooseTrade)(TRADE_TYPE type,UIView *showView);


@interface IXOpenTypeCell : UITableViewCell

@property (nonatomic, copy) chooseTrade tradeCon;

@property (nonatomic, copy) NSString *kind;

@property (nonatomic, copy) NSString *expire;

- (void)resetExpireArrow;

- (void)resetKindArrow;

@end
