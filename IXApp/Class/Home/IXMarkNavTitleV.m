//
//  IXMarkNavTitleV.m
//  IXApp
//
//  Created by mac on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXMarkNavTitleV.h"

@interface IXMarkNavTitleV ()

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIImageView *arrowImg;
@property (nonatomic, assign) BOOL arrowDown;

@end

@implementation IXMarkNavTitleV

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.titleLbl];
        [self addSubview:self.arrowImg];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(responseToTapGes:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        CGRect frame = self.bounds;
        frame.origin.y = self.center.y - 11;
        frame.size.height = 22;
        _titleLbl = [[UILabel alloc] initWithFrame:frame];
        
        _titleLbl.font = PF_MEDI(15);
        _titleLbl.dk_textColorPicker = DKColorWithRGBs(0x232935, 0xe9e9ea);
        _titleLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLbl;
}

- (UIImageView *)arrowImg
{
    if (!_arrowImg) {
        CGRect frame = self.bounds;
        frame.origin.x = (frame.size.width - 8)/2;
        frame.size.width = 8;
        frame.origin.y = self.bounds.size.height - 12;
        frame.size.height = 4;
        _arrowImg = [[UIImageView alloc] initWithFrame:frame];
        _arrowImg.dk_imagePicker = DKImageNames(@"common_arrow_down", @"common_arrow_down");
    }
    return _arrowImg;
}

- (void)setTitleContent:(NSString *)title
{
    title = LocalizedString(title);
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName : PF_MEDI(15)}];
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    _titleLbl.text = title;
    _titleLbl.frame = CGRectMake((width - size.width - 18)/2, (height - 22)/2, size.width, size.height);
    
    _arrowImg.frame = CGRectMake(GetView_MaxX(_titleLbl) + 10, (height - 2)/2, 8, 4);
}

- (CGFloat)getWidthOfLblWithContent:(NSString *)str WithFont:(UIFont *)font
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    CGSize titleSize = [str sizeWithFont:font constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
#pragma clang diagnostic pop
    return titleSize.width;
}

- (void)responseToTapGes:(UITapGestureRecognizer *)ges
{
    if (self.tapContent) {
        self.tapContent();
    }
}

@end
