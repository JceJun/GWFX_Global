//
//  IXOpenSLCell.h
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXAccTextField.h"

typedef void(^addsl)();
typedef void(^substractsl)();
typedef void(^erase)();

@interface IXOpenSLCell : UITableViewCell

@property (nonatomic, strong) IXAccTextField *inputTF;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *inputContent;
@property (nonatomic, copy) NSString *inputExplain;

@property (nonatomic, copy) addsl addBlock;
@property (nonatomic, copy) substractsl substractBlock;
@property (nonatomic, copy) erase eraseBlock;

@property (nonatomic, assign) BOOL hiddenErase;

@end
