//
//  IXSymbolDetailBtomV.m
//  IXApp
//
//  Created by Seven on 2017/12/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSymbolDetailBtomV.h"
#import "CAGradientLayer+IX.h"

@implementation IXSymbolDetailBtomV

+ (CGSize)targetSize
{
    if (kBtomMargin == 0) {
        return CGSizeMake(kScreenWidth, 43);
    }
    return CGSizeMake(kScreenWidth, 43 + 9 + kBtomMargin);
}


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.dk_backgroundColorPicker = DKNavBarColor;
        
        if (kBtomMargin == 0) {
            [self createNormalV];
        } else {
            [self createIphoneXV];
        }
        
        if([IXUserInfoMgr shareInstance].settingRed){
            _selBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xe3515b, 0xff4d2d);
            _buyBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
        }else{
            _buyBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xe3515b, 0xff4d2d);
            _selBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
        }
        
        [self addSubview:_selBtn];
        [self addSubview:_buyBtn];
    }
    return self;
}

- (void)createNormalV
{
    _selBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth/2, 43)];
    [_selBtn setTitle:LocalizedString(@"卖  出") forState:UIControlStateNormal];
    
    
    _buyBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth/2, 0, kScreenWidth/2, 43)];
    [_buyBtn setTitle:LocalizedString(@"买  入") forState:UIControlStateNormal];
}

- (void)createIphoneXV
{
    _selBtn = [[UIButton alloc] initWithFrame:CGRectMake(13, 9, kScreenWidth/2 - 14, 43)];
    _selBtn.layer.cornerRadius = 2.f;
    _selBtn.layer.masksToBounds = YES;
    [_selBtn setTitle:LocalizedString(@"卖  出") forState:UIControlStateNormal];
    
    
    _buyBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth/2 + 1, 9, kScreenWidth/2 - 14, 43)];
    _buyBtn.layer.cornerRadius = 2.f;
    _buyBtn.layer.masksToBounds = YES;
    [_buyBtn setTitle:LocalizedString(@"买  入") forState:UIControlStateNormal];
    
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * lay = [CAGradientLayer topShadowWithFrame:CGRectMake(-5, 0, kScreenWidth, 5) dkcolors:topColors];
    [self.layer addSublayer:lay];
    self.layer.masksToBounds = NO;
}

@end
