//
//  IXOpenModel+CheckData.m
//  IXApp
//
//  Created by Bob on 2016/12/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOpenModel+CheckData.h"

@implementation IXOpenModel (CheckData)

- (BOOL)validPrice:(NSDecimalNumber *)price
{
    if (self.noQuoteInfo) {
        return NO;
    }
    
    NSComparisonResult min = [price compare:self.minRequestPrice];
    NSComparisonResult max = [price compare:self.maxRequestPrice];
    
    return (min != NSOrderedAscending && max != NSOrderedDescending);
}

- (BOOL)validLoss:(NSDecimalNumber *)loss
{
    if (  isnan([loss floatValue]) ) {
        return YES;
    }
    
    NSComparisonResult min = [loss compare:self.minLoss];
    NSComparisonResult max = [loss compare:self.maxLoss];
    return (min != NSOrderedAscending && max != NSOrderedDescending);
}

- (BOOL)validProfit:(NSDecimalNumber *)profit
{
    if (  isnan([profit floatValue]) ) {
        return YES;
    }
    
    NSComparisonResult min = [profit compare:self.minProfit];
    NSComparisonResult max = [profit compare:self.maxProfit];
    return (min != NSOrderedAscending && max != NSOrderedDescending);
}

- (BOOL)validVolume:(NSDecimalNumber *)volume
{
    if ([volume floatValue] <= 0) {
        return NO;
    }
    
    NSDecimalNumber *volNum = [volume decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];
    
    NSDecimalNumber *volMin = [[NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithDouble:self.symbolModel.volumesMin] decimalValue]] decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];
    
    NSDecimalNumber *volStep = [[NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithDouble:self.symbolModel.volumesStep] decimalValue]] decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];
    
    NSDecimalNumber *volMax = [[NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithDouble:self.symbolModel.volumesMax] decimalValue]] decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];

    if ([volNum compare:volMin] == NSOrderedAscending) {
        return NO;
    }
    if ([volMax compare:volNum] == NSOrderedAscending) {
        return NO;
    }
    
    NSString *value = [[[volNum decimalNumberBySubtracting:volMin] decimalNumberByDividingBy:volStep] stringValue];
    return ([value rangeOfString:@"."].location == NSNotFound);
}

- (BOOL)validPositionVolume:(NSDecimalNumber *)volume
{
    
    if ([volume doubleValue] <= 0) {
        return NO;
    }
    
    //处理修改步长导致、历史持仓数量少于步长
    if ([volume doubleValue] == self.positionVolumes) {
        return YES;
    }
    
    NSDecimalNumber *volNum = [volume decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];
    
    NSDecimalNumber *volMin = [[NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithDouble:self.symbolModel.volumesMin] decimalValue]]decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];
    
    NSDecimalNumber *volStep = [[NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithDouble:self.symbolModel.volumesStep] decimalValue]]decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];
    
    NSDecimalNumber *volMax = [[NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithDouble:self.positionVolumes] decimalValue]]decimalNumberByMultiplyingByPowerOf10:self.symbolModel.volDigits];
    
    if ([volNum compare:volMin] == NSOrderedAscending) {
        return NO;
    }
    if ([volMax compare:volNum] == NSOrderedAscending) {
        return NO;
    }
    
    NSString *value = [[[volNum decimalNumberBySubtracting:volMin] decimalNumberByDividingBy:volStep] stringValue];
    return ([value rangeOfString:@"."].location == NSNotFound);
}

@end
