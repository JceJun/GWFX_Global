//
//  IXQuoteContentVC.m
//  IXApp
//
//  Created by bob on 16/11/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteContentVC.h"
#import "IXQuoteDetailVC.h"
#import "IXDPSChannelVC.h"

#import "IXSymbolCataView.h"
#import "IXGuideView.h"

#import "IXAccountBalanceModel.h"
#import "IXUserMgr.h"
#import "IXQuoteDataCache.h"
#import "IXStaticsMgr.h"

#import "IXCpyConfig.h"
#import "UIViewExt.h"
#import "UIKit+Block.h"
#import "IXDataProcessTools.h"


@interface IXQuoteContentVC ()
<
UICollectionViewDataSource,UICollectionViewDelegate
>

@property (nonatomic, strong) IXSymbolCataView *cataView;

@property (nonatomic, strong) UICollectionView *switchView;   //装载控制器
//覆盖在collection view上，处理无法右滑打开抽屉问题（仅在当前index为0的时显示）
@property (nonatomic, strong) UIView    * gestureV;
@property(nonatomic,strong)UIView *userBottomV;
@property(nonatomic,strong)UIView *guestBottomV;
@property(nonatomic,strong)UILabel *lb_account;
@property(nonatomic,strong) UILabel *lb_freeMargin;
@property(nonatomic,strong) UILabel *lb_pl;

@property (nonatomic, assign) NSInteger currentShowPage;
@property (nonatomic, copy) currentPage chooseSymbolCata;   //选中回调


@end

@implementation IXQuoteContentVC

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetAccountType)
                                                 name:kRefreashProfit
                                               object:nil];
    
    
    [self.view addSubview:self.switchView];
    [self resetAccountType];
    if (SymbolCataStyle) {
//        [self.view addSubview:self.cataView];
        [self cataView];
    }
    [self.view addSubview:self.gestureV];

}

#pragma mark logic module
/**
 *  滑动切换
 *
 *  @param page 滑动到页数
 */
- (void)setOffsetPage:(NSInteger)page
{
    _currentShowPage = page;
    if (page < 0) {
        page = 0;
    }else if(page > self.vcAry.count - 1){
        page = self.vcAry.count - 1;
    }
    [self.switchView setContentOffset:CGPointMake(page *kScreenWidth, 0) animated:NO];
    [self refreashQuoteWithPage:page];
}

/**
 *  更新数据
 *
 *  @param page 需要更新的页面
 */
- (void)refreashQuoteWithPage:(NSInteger)page
{
    IXQuoteDetailVC *detailVC = self.vcAry[page];
    [detailVC subscribeDynamicPrice];
}

#pragma mark -
#pragma mark - others
- (id)initWithViewControllerAry:(NSArray *)vcAry ChoosePage:(currentPage)choose
{
    self = [super init];
    if (self) {
        self.vcAry = [NSMutableArray arrayWithArray:vcAry];
        self.chooseSymbolCata = choose;
    }
    return self;
}


- (void)reloadCataListWithRow:(NSInteger)row
{
    if (_currentShowPage < self.vcAry.count) {
        if (_currentShowPage != row) {
            IXQuoteDetailVC *preVC = self.vcAry[_currentShowPage];
            [preVC canceQuotePrice];
            [[IXQuoteDataCache shareInstance] saveDynQuote];
            [[IXQuoteDataCache shareInstance] saveLastClosePrice];
        }
    }
    
    if(self.vcAry.count > row){
        IXQuoteDetailVC *currVC = self.vcAry[row];
        [currVC subscribeDynamicPrice];

        _currentShowPage = row;
        [self.switchView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0
                                                                    inSection:row]
                                atScrollPosition:UICollectionViewScrollPositionNone
                                        animated:NO];
        //定位分类
        [_cataView setOffsetPage:row];
    }
}

- (void)reloadCataList:(NSArray *)vcAry WithRow:(NSInteger)row
{
    self.vcAry = [NSMutableArray arrayWithArray:vcAry];
    [self.switchView reloadData];
    [self reloadCataListWithRow:row];
    
    [_cataView resetDataSource:[self getCataArr]];
    [_cataView setOffsetPage:row];
}

- (void)reloadData
{
    [self.switchView reloadData];
}

#pragma mark --
#pragma mark 订阅和取消订阅
- (void)subscribeQuote
{
    [self resetQuotePrice];
}

- (void)cancelQuotePrice
{
    if ( _currentShowPage >= 0 && self.vcAry && self.vcAry.count > _currentShowPage ) {
        IXQuoteDetailVC *detail = self.vcAry[_currentShowPage];
        [detail cancelVisualQuote];
    }
    
}

- (void)resetQuotePrice
{
    if ( _currentShowPage >= 0 && self.vcAry && self.vcAry.count > _currentShowPage ) {
        IXQuoteDetailVC *detail = self.vcAry[_currentShowPage];
        [detail subscribeDynamicPrice];
    }
}


#pragma mark -
#pragma mark - UICollectionViewDataSource&&Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
   return self.vcAry.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
    // 移除subviews 避免重用内容显示错误
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( self.vcAry.count > indexPath.section ) {
        IXQuoteDetailVC *detailVC = self.vcAry[indexPath.section];
        [cell.contentView addSubview:detailVC.view];
        detailVC.view.frame = cell.contentView.bounds;
        if (![[self childViewControllers] containsObject:detailVC]) {
            [self addChildViewController:detailVC];
        }
        [detailVC didMoveToParentViewController:self];
        [[IXStaticsMgr shareInstance] changeSymbolPageWithCataID:detailVC.cataId];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.switchView.bounds.size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _currentShowPage = indexPath.section;
    if (self.chooseSymbolCata) {
        self.chooseSymbolCata(_currentShowPage);
    }
}

#pragma mark -
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _currentShowPage = floor(scrollView.contentOffset.x / kScreenWidth);
    if (self.chooseSymbolCata) {
        self.chooseSymbolCata(_currentShowPage);
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    _currentShowPage = floor(targetContentOffset->x / kScreenWidth);
    [_cataView setOffsetPage:_currentShowPage];
    
    self.gestureV.hidden = _currentShowPage != 0;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.titleBlock) {
            self.titleBlock(_currentShowPage);
        }
    });
}


#pragma mark -
#pragma mark - lazy load

- (UICollectionView *) switchView
{
    if ( !_switchView ) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        if (SymbolCataStyle) {
            CGFloat vHeight = kScreenHeight - kNavbarHeight - kTabbarHeight - 40;
            _switchView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 40,
                                                                             kScreenWidth,
                                                                             vHeight)
                                             collectionViewLayout:flowLayout];
        } else {
            CGFloat vHeight = kScreenHeight - kNavbarHeight - kTabbarHeight - 40;
            _switchView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0,
                                                                             kScreenWidth,
                                                                             vHeight
                                                                             )
                                             collectionViewLayout:flowLayout];
        }
        _switchView.pagingEnabled = YES;
        _switchView.backgroundColor = [UIColor clearColor];
        _switchView.bounces = NO;
        _switchView.delegate = self;
        _switchView.dataSource = self;
        [_switchView registerClass:[UICollectionViewCell class]
        forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        _switchView.showsHorizontalScrollIndicator = NO;
    }
    return _switchView;
}

- (UIView *)gestureV
{
    if (!_gestureV) {
        _gestureV = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                             30,
                                                             kScreenHeight - kNavbarHeight - kTabbarHeight-40)];
        _gestureV.backgroundColor = [UIColor clearColor];
    }
    return _gestureV;
}

- (NSMutableArray *)getCataArr
{
    NSMutableArray *arr = [NSMutableArray arrayWithObject:@{@"name":LocalizedString(@"热门")}];
    
    id cataArr = [IXDataProcessTools querySymbolCataByParentId:0];
    if ( cataArr && [cataArr isKindOfClass:[NSArray class]] ) {
        [arr addObjectsFromArray:cataArr];
    }
    //[arr addObjectsFromArray:[IXDBSymbolCataMgr querySymbolCataByParentId:0]];
    return  arr;
}


- (IXSymbolCataView *)cataView{
    if ( !_cataView ) {
        _cataView = [[IXSymbolCataView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 40) WithCataAry:[self getCataArr]];
        [self.view addSubview:_cataView];
        
        weakself;
        _cataView.selectedIndexPath = ^(NSIndexPath *indexPath){
            if (weakSelf.chooseSymbolCata) {
                weakSelf.chooseSymbolCata(indexPath.section);
            }
            
            [weakSelf refreashQuoteWithPage:indexPath.section];
            [weakSelf.switchView scrollToItemAtIndexPath:indexPath
                                        atScrollPosition:UICollectionViewScrollPositionNone
                                                animated:NO];
        };
    }
    return _cataView;
}

- (UIView *)userBottomV{
    if (!_userBottomV) {
        _userBottomV = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_switchView.frame), kScreenWidth, 40)];
        _userBottomV.dk_backgroundColorPicker = DKNavBarColor;
        [self.view addSubview:_userBottomV];
        
        NSString *accountType = [IXDataProcessTools showCurrentAccountType];
        _lb_account = [UILabel new];
        _lb_account.font = ROBOT_FONT(15);
        _lb_account.dk_textColorPicker = DKColorWithRGBs( 0x4c6072, 0xffffff);
        _lb_account.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x242a36);
        _lb_account.text = [@"  " stringByAppendingString:accountType];
        _lb_account.textAlignment = NSTextAlignmentLeft;
        [_userBottomV addSubview:_lb_account];

        [_lb_account makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.top.equalTo(0);
            make.bottom.equalTo(0);
            make.width.equalTo(70);
        }];
        weakself;
        [_lb_account block_whenTapped:^(UIView *aView) {
            [weakSelf expandView];
        }];
        UIImageView *imgV = [UIImageView new];
        imgV.userInteractionEnabled = NO;
        imgV.dk_imagePicker = DKImageNames(@"pullDown_D", @"pullDown");
        [_lb_account addSubview:imgV];
        [imgV makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(-8);
            make.centerY.equalTo(0);
        }];
        
        _lb_freeMargin = [UILabel new];
        _lb_freeMargin.font = ROBOT_FONT(15);
        _lb_freeMargin.dk_textColorPicker = DKCellTitleColor;
        _lb_freeMargin.text = @"319.49";
        [_userBottomV addSubview:_lb_freeMargin];
        [_lb_freeMargin makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(2);
            make.centerX.equalTo(-50);
        }];
        [UIView bondSupperObject:_userBottomV subObject:_lb_freeMargin byKey:@"_lb_freeMargin"];
        
        UILabel *lb_tip1 = [UILabel new];
        lb_tip1.font = ROBOT_FONT(12);
        lb_tip1.dk_textColorPicker = DKCellContentColor;
        lb_tip1.text = @"Free Margin";
        [_userBottomV addSubview:lb_tip1];
        [lb_tip1 makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(-2);
            make.centerX.equalTo(-50);
        }];
        
        _lb_pl  = [UILabel new];
        _lb_pl.font = ROBOT_FONT(15);
        _lb_pl.dk_textColorPicker = DKCellTitleColor;
        _lb_pl.textAlignment = NSTextAlignmentCenter;
        _lb_pl.text = @"-999.999";
        [_userBottomV addSubview:_lb_pl];
        [_lb_pl makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_lb_freeMargin);
            make.centerX.equalTo(30);
        }];
        
        UILabel *lb_tip2 = [UILabel new];
        lb_tip2.font = ROBOT_FONT(12);
        lb_tip2.dk_textColorPicker = DKCellContentColor;
        lb_tip2.text = @"Realtime P/L";
        [_userBottomV addSubview:lb_tip2];
        [lb_tip2  makeConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(-2);
             make.centerX.equalTo(30);
        }];
        
        UILabel *lb_deosiit = [UILabel new];
        lb_deosiit.font = ROBOT_FONT(15);
        lb_deosiit.dk_textColorPicker = DKColorWithRGBs( 0xffffff, 0xffffff);
        lb_deosiit.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
        lb_deosiit.textAlignment = NSTextAlignmentCenter;
        lb_deosiit.layer.cornerRadius = 3;
        lb_deosiit.layer.masksToBounds = YES;
        lb_deosiit.text = @"Deposit";
        [_userBottomV addSubview:lb_deosiit];
        [lb_deosiit makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(-8);
            make.centerY.equalTo(0);
            make.height.equalTo(30);
            make.width.equalTo(70);
        }];
        [lb_deosiit block_whenTapped:^(UIView *aView) {
            // 强制跳转入金
            NSString *accountType = [IXDataProcessTools showCurrentAccountType];
            if ([accountType isEqualToString:LocalizedString(@"模拟")]) {
                [[IXUserMgr sharedIXUserMgr] switchToRealAccount:4];
            }else if ([accountType isEqualToString:LocalizedString(@"真实")]){
                if (_depositBlock) {
                    _depositBlock();
                }
            }
        }];
    }
    return _userBottomV;
}

- (UIView *)guestBottomV{
    if (!_guestBottomV) {
        CGFloat originY = CGRectGetMaxY(_switchView.frame);
        _guestBottomV = [[UIView alloc] initWithFrame:CGRectMake(0, originY, kScreenWidth, 40)];
        _guestBottomV.dk_backgroundColorPicker = DKNavBarColor;
        [self.view addSubview:_guestBottomV];
        
        NSString *accountType = [IXDataProcessTools showCurrentAccountType];
        UILabel *lb_account = [UILabel new];
        lb_account.font = ROBOT_FONT(15);
        lb_account.dk_textColorPicker = DKColorWithRGBs( 0x242a36,  0xffffff);
        lb_account.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x242a36);
        lb_account.text = accountType;
        lb_account.textAlignment = NSTextAlignmentCenter;
        [_guestBottomV addSubview:lb_account];
        
        [lb_account makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.top.equalTo(0);
            make.bottom.equalTo(0);
            make.width.equalTo(70);
        }];
        
        UILabel *lb_login = [UILabel new];
        lb_login.font = ROBOT_FONT(15);
        lb_login.dk_textColorPicker = DKCellTitleColor;
        lb_login.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x99abba);
        lb_login.textAlignment = NSTextAlignmentCenter;
        lb_login.layer.cornerRadius = 3;
        lb_login.layer.masksToBounds = YES;
        lb_login.text = @"Sign in";
        [_guestBottomV addSubview:lb_login];
        [lb_login makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(0);
            make.height.equalTo(30);
            make.width.equalTo(100);
        }];
        [lb_login block_whenTapped:^(UIView *aView) {
            if (_needregistBlock) {
                _needregistBlock(NO);
            }
        }];
        
        UILabel *lb_regist = [UILabel new];
        lb_regist.font = ROBOT_FONT(15);
        lb_regist.dk_textColorPicker = DKColorWithRGBs( 0xffffff, 0xffffff);
        lb_regist.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
        lb_regist.textAlignment = NSTextAlignmentCenter;
        lb_regist.layer.cornerRadius = 3;
        lb_regist.layer.masksToBounds = YES;
        lb_regist.text = @"Register";
        [_guestBottomV addSubview:lb_regist];
        [lb_regist makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(-8);
            make.centerY.equalTo(0);
            make.height.equalTo(30);
            make.width.equalTo(70);
        }];
        [lb_regist block_whenTapped:^(UIView *aView) {
            if (_needregistBlock) {
                _needregistBlock(YES);
            }
        }];
    }
    return _guestBottomV;
}


- (void)resetAccountType{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *accountType = [IXDataProcessTools showCurrentAccountType];
        if (SameString(accountType, LocalizedString(@"游客"))) {
            self.guestBottomV.hidden = NO;
            self.userBottomV.hidden = YES;
        }else{
            self.userBottomV.hidden = NO;
            self.guestBottomV.hidden = YES;
            
            _lb_account.text = [@"  " stringByAppendingString:accountType];
            
            _lb_freeMargin.text = [IXDataProcessTools moneyFormatterComma:[[IXAccountBalanceModel shareInstance] getAvailabelFunds] positiveNumberSign:YES];
            
            [IXDataProcessTools resetTextColorLabel:_lb_pl value:[IXAccountBalanceModel shareInstance].totalProfit];
            _lb_pl.text = [IXDataProcessTools moneyFormatterComma:[IXAccountBalanceModel shareInstance].totalProfit positiveNumberSign:NO];
            
        }
    });
}


- (void)expandView{
    UIView *bgView = [[UIView alloc] initWithFrame:KEYWINDOW.bounds];
    [KEYWINDOW addSubview:bgView];
    UILabel *expandView = [[UILabel alloc] initWithFrame:CGRectMake(0, kScreenHeight-kTabbarHeight-40, 70, 0)];
    expandView.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xffffff);
    expandView.dk_backgroundColorPicker = DKColorWithRGBs(0x99abba, 0x4c6072);
    expandView.textAlignment = NSTextAlignmentCenter;
    expandView.font = _lb_account.font;
    [bgView addSubview:expandView];
    NSString *accountType = [IXDataProcessTools showCurrentAccountType];
    if ([accountType isEqualToString:@"Demo"]) {
        expandView.text = @"Real";
    }else{
        expandView.text = @"Demo";
    }
    
    [expandView block_whenTapped:^(UIView *aView) {
        [bgView removeFromSuperview];
        if ([accountType isEqualToString:@"Demo"]) {
            [[IXUserMgr sharedIXUserMgr] switchToRealAccount:3];
        }else{
            [[IXUserMgr sharedIXUserMgr] switchToDemoAccount:3];
        }
    }];
    
    [UIView animateWithDuration:0.1 animations:^{
        expandView._top = kScreenHeight-kTabbarHeight-40-40;
        expandView._height = 40;
    }];
    
    [bgView block_whenTapped:^(UIView *aView) {
        [UIView animateWithDuration:0.1 animations:^{
            expandView._top = kScreenHeight-kTabbarHeight-40;
            expandView._height = 0;
        } completion:^(BOOL finished) {
            [aView removeFromSuperview];
        }];
    }];
}



@end
