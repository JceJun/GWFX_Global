//
//  IXSymbolDetailVC.h
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXTradeMarketModel.h"

@interface IXSymbolDetailVC : IXDataBaseVC

@property (nonatomic, strong) IXTradeMarketModel *tradeModel;

@property (nonatomic, assign) double nLastClosePrice;

@property (nonatomic, assign) BOOL refreashFeeQuote;

- (void)refreashProfit;

@end
