//
//  IXHorizontalkVC.m
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXHorizontalkVC.h"
#import "IXHoriKTitleV.h"
#import "IXHoriKTypeV.h"
#import "IXHoriKIndexV.h"

#import "IXTSEngine.h"
#import "IXCSEngine.h"

#import "IXTradeMarketModel.h"
#import "IxItemTick.pbobjc.h"
#import "IXStaticsMgr.h"

@interface IXHorizontalkVC ()
@property (nonatomic,strong)IXHoriKTitleV *titleV;
@property (nonatomic,strong)IXHoriKTypeV  *ktypeV;
@property (nonatomic,strong)IXHoriKIndexV *indexV;

@property (nonatomic,strong)IXTSEngine *tsEngine;
@property (nonatomic,strong)IXCSEngine *csEngine;

@property (nonatomic,strong)IXTradeMarketModel *tradeModel;
@property (nonatomic,assign)float lastPrice;

@end

@interface IXHorizontalkVC ()
{
    BOOL isGetOneDayKData;
}

@end

@implementation IXHorizontalkVC

- (instancetype)initWithTradeModel:(IXTradeMarketModel *)trade
                         lastPrice:(float)price;
{
    self = [super init];
    if (self) {
        self.tradeModel = trade;
        self.lastPrice  = price;
        CATransform3D transform = CATransform3DMakeRotation(M_PI / 2, 0, 0, 1.0);
        self.view.layer.transform = transform;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    
    [self titleV];
    [self ktypeV];
    [self indexV];
    
    [self.view addSubview:self.csEngine.displayV];
    [self.view addSubview:self.tsEngine.displayV];
    CGFloat x = 0.f;
    if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
        x = [[UIApplication sharedApplication] statusBarFrame].size.height;
    }
    [self.csEngine.displayV setFrame:CGRectMake(x, 56 + 29, kScreenHeight - 60 - 8 * 2 - x - kBtomMargin, kScreenWidth - 56 - 29)];
    [self.tsEngine.displayV setFrame:CGRectMake(x, 56 + 29, kScreenHeight - x - kBtomMargin, kScreenWidth - 56 - 29)];
    self.tsEngine.displayV.alpha = 0;
    self.csEngine.displayV.alpha = 1;
    [self getOneDayKData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    _ktypeV.selectedType = _kLineType;
    self.ktypeV.ktypeBtnClicked(_kLineType);
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    if ([[UIApplication sharedApplication] statusBarFrame].size.height <= 20) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
        });
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if ([[UIApplication sharedApplication] statusBarFrame].size.height <= 20) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
    }
}

- (void)getOneDayKData
{
    isGetOneDayKData = YES;
    item_stk *stk = [[item_stk alloc] init];
    stk.marketid = self.tradeModel.marketId;
    stk.id_p = self.tradeModel.symbolModel.id_p;
    
    proto_quote_kdata_req *req = [[proto_quote_kdata_req alloc] init];
    req.stk = stk;
    req.ktype = 3;  //日k
    req.startTime = 0;
    req.endTime = 0;
    req.reqCount = 1;
    [[IXTCPRequest shareInstance] subscribeChartDataWithParam:req];
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    switch (cmd) {
        case CMD_QUOTE_KDATA:
            [self shouldProcessQuoteDistribute:arr];
            break;
        case CMD_QUOTE_PUB_DETAIL:{
            [self shouldProcessDynQuote:arr];
        }
        default:
            break;
    }
}

#pragma mark -
#pragma mark - TitileView

- (IXHoriKTitleV *)titleV
{
    if (!_titleV) {
        CGFloat x = 0.f;
        if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
            x = [[UIApplication sharedApplication] statusBarFrame].size.height;
        }
        _titleV = [[IXHoriKTitleV alloc] initWithFrame:CGRectMake(x, 0, kScreenHeight - x - kBtomMargin, 56)];
        _titleV.lastPrice = _lastPrice;
        [self.view addSubview:_titleV];
        
        weakself;
        _titleV.shutBtnClicked = ^(){
            if (weakSelf.hkvcWillDismiss) {
                weakSelf.hkvcWillDismiss();
            }

            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        [_titleV refreshUIWithTrade:_tradeModel];
    }
    return _titleV;
}

#pragma mark -
#pragma mark - KTypeView

- (IXHoriKTypeV *)ktypeV
{
    if (!_ktypeV) {
        CGFloat x = 0.f;
        if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
            x = [[UIApplication sharedApplication] statusBarFrame].size.height;
        }
        _ktypeV = [[IXHoriKTypeV alloc] initWithFrame:CGRectMake(x, 56, kScreenHeight - x - kBtomMargin, 29)];
        [self.view addSubview:_ktypeV];
        
        weakself;
        _ktypeV.ktypeBtnClicked = ^(NSInteger tag){
            weakSelf.kLineType = (IXKLineType)tag;
            if (tag==0) {
                [weakSelf.tsEngine.displayV setFrame:CGRectMake(x, 56 + 29,
                                                                kScreenHeight - x - kBtomMargin,
                                                                kScreenWidth - 56 - 29)];
                [weakSelf.tsEngine display];
                [weakSelf showIndxV:NO];
            } else {
                [weakSelf.csEngine.displayV setFrame:CGRectMake(x, 56 + 29,
                                                                kScreenHeight - 60 - 8 * 2 - x - kBtomMargin,
                                                                kScreenWidth - 56 - 29)];
                [weakSelf.csEngine showKLineWithType:(IXKLineType)tag];
                [weakSelf showIndxV:YES];
            }
            [weakSelf doChartAnimation];
            
            //埋点
            [[IXStaticsMgr shareInstance] updateKlineType:tag];
        };
    }
    return _ktypeV;
}

- (void)showIndxV:(BOOL)show
{
    if (show) {
        if (self.indexV.alpha == 1.f) {
            return;
        }
        [UIView animateWithDuration:.2 animations:^{
            self.indexV.alpha = 1.f;
        }];
    } else {
        [UIView animateWithDuration:.2 animations:^{
            self.indexV.alpha = 0.f;
        }];
    }
}

- (void)doChartAnimation
{
    if (_kLineType == IXKLineTypeTline) {
        [UIView animateWithDuration:.2 animations:^{
            self.tsEngine.displayV.alpha = 1;
            self.csEngine.displayV.alpha = 0;
        }];
    } else {
        [UIView animateWithDuration:.2 animations:^{
            self.tsEngine.displayV.alpha = 0;
            self.csEngine.displayV.alpha = 1;
        }];
    }
}

#pragma mark -
#pragma mark - IndexView

- (IXHoriKIndexV *)indexV
{
    if (!_indexV) {
        CGRect rect = CGRectMake(kScreenHeight - 60 - 8 - kBtomMargin, 56 + 29 + 22, 60, kScreenWidth - 56 - 29 - 22 - 17);
        _indexV = [[IXHoriKIndexV alloc] initWithFrame:rect];
        [self.view addSubview:_indexV];
        
        weakself;
        _indexV.idxBtnClicked = ^(IXHoriKIndexType type,NSInteger index){
            switch (type) {
                case IXHoriKIndexMain:
                    weakSelf.csEngine.displayV.mainIdxType = (IXCSMainIdxType)index;
                    break;
                case IXHoriKIndexScnd:
                    weakSelf.csEngine.displayV.scndIdxType = (IXCSScndIdxType)index;
                    break;
                default:
                    break;
            }
            
            //埋点
            [[IXStaticsMgr shareInstance] updateKlineIndexWithSection:type index:index];
        };
    }
    return _indexV;
}

- (IXTSEngine *)tsEngine
{
    if (!_tsEngine) {
        _tsEngine = [[IXTSEngine alloc] initWithDigit:_tradeModel.symbolModel.digits
                                       scheduleCateID:_tradeModel.symbolModel.scheduleCataId
                                                     lastPrice:self.lastPrice];
        _tsEngine.displayV.orientation = IXTSOrientationH;
        [_tsEngine setNightMode:[IXUserInfoMgr shareInstance].isNightMode];
        [_tsEngine setRedUpBlueDown:[IXUserInfoMgr shareInstance].settingRed];

        weakself;
        _tsEngine.request = ^(time_t endTime, int32_t cnt){
            item_stk *stk = [[item_stk alloc] init];
            stk.marketid = weakSelf.tradeModel.marketId;
            stk.id_p = weakSelf.tradeModel.symbolModel.id_p;
            proto_quote_kdata_req *req = [[proto_quote_kdata_req alloc] init];
            req.stk = stk;
            req.ktype = MIN_1;
            req.startTime = 0;
            req.endTime = endTime;
            req.reqCount = cnt;
            [[IXTCPRequest shareInstance] subscribeChartDataWithParam:req];
        };
        _tsEngine.status = ^(IXTSEngineReqStatus status){
            if (status == IXTSEngineeReqFree) {
                //DLog(@"========请求空闲");
            } else {
                //DLog(@"========请求忙");
            }
        };
        _tsEngine.clicked = ^(){
            if (weakSelf.hkvcWillDismiss) {
                weakSelf.hkvcWillDismiss();
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    return _tsEngine;
}

- (IXCSEngine *)csEngine
{
    if (!_csEngine) {
        _csEngine = [[IXCSEngine alloc] initWithDigit:_tradeModel.symbolModel.digits marketID:self.tradeModel.marketId];
        _csEngine.displayV.orientation = IXTSOrientationH;
        _csEngine.symbolId = self.tradeModel.symbolModel.id_p;
        [_csEngine setNightMode:[IXUserInfoMgr shareInstance].isNightMode];
        [_csEngine setRedUpBlueDown:[IXUserInfoMgr shareInstance].settingRed];
        
        weakself;
        _csEngine.request = ^(K_TYPE ktype, time_t endTime, int32_t cnt) {
            item_stk *stk = [[item_stk alloc] init];
            stk.marketid = weakSelf.tradeModel.marketId;
            stk.id_p = weakSelf.tradeModel.symbolModel.id_p;
            
            proto_quote_kdata_req *req = [[proto_quote_kdata_req alloc] init];
            req.stk = stk;
            req.ktype = ktype;
            req.startTime = 0;
            req.endTime = endTime;
            req.reqCount = cnt;
                        
            [[IXTCPRequest shareInstance] subscribeChartDataWithParam:req];
        };
        _csEngine.status = ^(IXCSEngineReqStatus status) {
            if (status == IXCSEngineReqFree) {
                //DLog(@"========请求空闲");
            } else {
                //DLog(@"========请求忙");
            }
        };
        _csEngine.clicked = ^(){
            if (weakSelf.hkvcWillDismiss) {
                weakSelf.hkvcWillDismiss();
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    return _csEngine;
}

#pragma mark -
#pragma mark - others
//k线数据响应
- (void)shouldProcessQuoteDistribute:(NSMutableArray *)arr
{
    if (isGetOneDayKData && arr.count == 1) {
        isGetOneDayKData = NO;
        [self.titleV syncFirstPriceWithArray:arr];
        return;
    }
    if (_kLineType == IXKLineTypeTline) {
        [self.tsEngine requestDataResponse:arr];
    } else {
        [self.csEngine klineDataResponse:arr];
    }
}

//动态行情响应
- (void)shouldProcessDynQuote:(NSMutableArray *)resultAry;
{
    for (IXQuoteM *quoteModel in resultAry) {
        if (quoteModel.symbolId == _tradeModel.symbolModel.id_p) {
            _tradeModel.quoteModel = quoteModel;
            
            IXMetaData *meta = [quoteModel transferToMetaData];
            
            [_tsEngine dynamicDataReseponse:meta];
            [_csEngine dynamicDataReseponse:meta];
            [self.titleV syncDynamicWithQuote:quoteModel];
            return;
        }
    }
}

#pragma mark -
#pragma mark - 隐藏状态栏

- (BOOL)prefersStatusBarHidden
{
    return self.hideStatusBar;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationSlide;
}

@end
