//
//  IXSymbolSearchCell.m
//  IXApp
//
//  Created by Evn on 16/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSymbolSearchCell.h"
#import "IXDataProcessTools.h"
#import "IXDBLanguageMgr.h"
#import "IXDBGlobal.h"

@implementation IXSymbolSearchCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self lineView];
    [self addBgBtn];
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = [IXCustomView createLable:CGRectMake(15,  11.5,kScreenWidth/2, 13)                                  title:@"" font:PF_MEDI(13)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

- (UIView *)topView
{
    if (!_topView) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(15, 36, 20, 7)];
        _topView.hidden = YES;
        [self.contentView addSubview:_topView];
    }
    return _topView;
}

- (UILabel *)labLbl1 {
    if (!_labLbl1) {
        _labLbl1 = [IXCustomView createLable:CGRectMake(15, 36, 20, 7)
                                       title:@""
                                        font:PF_MEDI(7)
                                  wTextColor:0xffffff
                                  dTextColor:0x262f3e
                               textAlignment:NSTextAlignmentCenter];
        _labLbl1.backgroundColor = UIColorHexFromRGB(0xebad62);
        [self.contentView addSubview:_labLbl1];
    }
    return _labLbl1;
}

- (UILabel *)labLbl2 {
    if (!_labLbl2) {
        _labLbl2 = [IXCustomView createLable:CGRectMake(15, 36, 20, 7)
                                       title:@""
                                        font:PF_MEDI(7)
                                  wTextColor:0xffffff
                                  dTextColor:0x262f3e
                               textAlignment:NSTextAlignmentCenter];
        _labLbl2.backgroundColor = UIColorHexFromRGB(0xebad62);
        [self.contentView addSubview:_labLbl2];
    }
    return _labLbl2;
}

- (UILabel *)contentLbl {
    if (!_contentLbl) {
        _contentLbl = [IXCustomView createLable:CGRectMake(15, 34, 120, 10)
                                          title:@""
                                           font:RO_REGU(10)
                                     wTextColor:0xa7adb5
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UIButton *)addBgBtn {
    if (!_addBgBtn) {
        _addBgBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth  - 75, (54.5 - 40)/2, 75, 40)];
        [self.contentView addSubview:_addBgBtn];
    }
    return _addBgBtn;
}

- (UIButton *)addBtn {
    if (!_addBtn) {
        _addBtn = [[UIButton alloc] initWithFrame:CGRectMake((75 - 26.5)/2, (40 - 26.5)/2, 26.5, 26.5)];
        [_addBtn addTarget:self action:@selector(addBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_addBtn dk_setTitleColorPicker:DKColorWithRGBs(0x99abba, 0x8395a4) forState:UIControlStateNormal];
        _addBtn.titleLabel.font = PF_MEDI(13);
        _addBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        [self.addBgBtn addSubview:_addBtn];
    }
    return _addBtn;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 54, kScreenWidth, kLineHeight)];
        _lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)reloadUIWithItem:(IXSymbolM *)model
{
    NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
    if (languageDic && languageDic.count) {
        if (languageDic[kValue] && [languageDic[kValue] length]) {
            model.languageName = languageDic[kValue];
        } else {
            model.languageName = model.name;
        }
    }
    else {
        model.languageName = model.name;
    }
    self.nameLbl.text = [IXDataProcessTools dealWithNil:model.languageName];
    self.labLbl1.text = @"";
    self.labLbl2.text = @"";
    if( model.labelArr && model.labelArr.count ){
        NSDictionary *label0;
        if ( model.labelArr.count >= 1 ) {
            
            label0 = model.labelArr[0];
            NSInteger labeIndex = [label0 integerForKey:@"colour"];
            self.labLbl1.backgroundColor = UIColorHexFromRGB([IXEntityFormatter getLabelColorValue:labeIndex]);
            self.labLbl2.backgroundColor = UIColorHexFromRGB([IXEntityFormatter getLabelColorValue:labeIndex]);
            
            if ( [[label0 stringForKey:@"name"] length] != 0 ) {
                self.labLbl1.text = [label0 stringForKey:@"name"];
            }
        }
        
        NSDictionary *label1;
        if ( model.labelArr.count >= 2 ) {
            
            label1 = model.labelArr[1];
            if ( [[label1 stringForKey:@"name"] length] != 0 ) {
                if ( 0 == self.labLbl1.text.length ) {
                    self.labLbl1.text = [label1 stringForKey:@"name"];
                }else{
                    self.labLbl2.text = [label1 stringForKey:@"name"];
                }
            }
        }
    }
    
    NSInteger width = 0;
    NSInteger width1 = (NSInteger)[IXEntityFormatter getContentWidth:self.labLbl1.text
                                                            WithFont:self.labLbl1.font] + 5;
    NSInteger width2 = 0;
    if ( model.labelArr.count > 1 ) {
        width2 = (NSInteger)[IXEntityFormatter getContentWidth:self.labLbl2.text
                                                      WithFont:self.labLbl2.font] + 5;
    }
    if ( width1 >= width2 ) {
        width = width1;
    }else{
        width = width2;
    }
    if (width1 == 5) {
        self.labLbl1.hidden = YES;
        self.labLbl2.hidden = YES;
    } else {
        if ( width2 == 5 || width2 == 0 ) {
            self.labLbl2.hidden = YES;
            self.topView.hidden = YES;
            CGRect frame = self.labLbl1.frame;
            frame.size.height = 10;
            frame.origin.y = 35;
            frame.size.width = width;
            self.labLbl1.frame = frame;
            self.labLbl1.hidden = NO;
        }else{
            self.labLbl2.hidden = NO;
            CGRect frame = self.labLbl1.frame;
            frame.origin.y = CGRectGetMaxY(self.nameLbl.frame) + 6;
            frame.size.width = width;
            frame.size.height = 7;
            self.labLbl1.frame = frame;
            self.labLbl1.hidden = NO;
            
            frame.origin.y = CGRectGetMaxY(self.nameLbl.frame) + 5;
            frame.size.height = 1;
            self.topView.frame = frame;
            self.topView.backgroundColor = self.labLbl1.backgroundColor;
            self.topView.hidden = NO;
            
            frame = self.labLbl2.frame;
            frame.origin.y = CGRectGetMaxY(self.labLbl1.frame);
            frame.size.width = width;
            frame.size.height = 10;
            self.labLbl2.frame = frame;
        }
    }
    CGRect frame = self.contentLbl.frame;
    if ( width1 != 5 ) {
        frame.origin.x = CGRectGetMaxX(self.labLbl1.frame) + 5;
    } else {
        frame.origin.x = 15;
    }
    self.contentLbl.frame = frame;
    
    if ( model.name && [model.name length] != 0 ) {
        self.contentLbl.text = [NSString stringWithFormat:@"%@:%@",model.name,model.marketName];
    }else{
        self.contentLbl.text = [NSString stringWithFormat:@"%@:%@",model.source,model.marketName];
    }
    
    if (model.isSub) {
        [self.addBtn setImage:nil forState:UIControlStateNormal];
        self.addBtn.frame = CGRectMake(10, 12, 60, 15);
        [self.addBtn setTitle:LocalizedString(@"已添加") forState:UIControlStateNormal];
    } else {
        self.addBtn.frame = CGRectMake((75 - 26.5)/2, (40 - 26.5)/2, 26.5, 26.5);
        [self.addBtn setTitle:nil forState:UIControlStateNormal];
        [self.addBtn dk_setImage:DKImageNames(@"search_add",@"search_add_D") forState:UIControlStateNormal];
    }
}

- (void)addBtn:(UIButton *)btn
{
    if (_delegate && [_delegate respondsToSelector:@selector(addOrDeleteSymbolSub:)]) {
        [_delegate addOrDeleteSymbolSub:btn];
    }
}

@end

