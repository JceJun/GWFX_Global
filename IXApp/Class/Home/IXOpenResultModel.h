//
//  IXOpenResultModel.h
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXOpenResultModel : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) item_order_edirection dir;
@property (nonatomic, assign) item_order_etype type;
@property (nonatomic, strong) NSString *cate;
@property (nonatomic, strong) NSString *num;
@property (nonatomic, strong) NSString *reqPrice;
@property (nonatomic, strong) NSString *exePrice;
@property (nonatomic, strong) NSString *reqDate;
@property (nonatomic, strong) NSString *exeDate;
@property (nonatomic, strong) NSString *margin;//保证金
@property (nonatomic, strong) NSString *commission;//佣金
@property (nonatomic, strong) NSString *unitLanName;//产品单位
@end
