//
//  IXOptionHeadV.h
//  IXApp
//
//  Created by Bob on 2016/12/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, FilterType){
    FilterType_All,
};

typedef void(^orderKind)(FilterType kind);
@interface IXOptionHeadV : UIView

@property (nonatomic, strong) orderKind orderKind;

@end
