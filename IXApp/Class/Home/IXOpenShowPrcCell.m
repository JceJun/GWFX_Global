//
//  IXOpenShowPrcCell.m
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenShowPrcCell.h"

@interface IXOpenShowPrcCell ()

@property (nonatomic, strong) UIButton *showDeepPrcBtn;

@end

@implementation IXOpenShowPrcCell

- (UIButton *)showDeepPrcBtn
{
    if ( !_showDeepPrcBtn ) {
        _showDeepPrcBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:self.showDeepPrcBtn];
        _showDeepPrcBtn.frame = CGRectMake( 0, 0, kScreenWidth, 40);
        _showDeepPrcBtn.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x242a36);
        [_showDeepPrcBtn addTarget:self
                            action:@selector(responseToBtn)
                  forControlEvents:UIControlEventTouchUpInside];
    }
    return _showDeepPrcBtn;
}

- (void)refreashUIWithArrowDir:(BOOL)arrowDown
{
    NSString *con = arrowDown ?  LocalizedString(@"展开市场深度") : LocalizedString(@"收起市场深度");
    
    NSInteger conWidth = (NSInteger)[IXEntityFormatter getContentWidth:con WithFont:PF_MEDI(12)] + 1;
    NSInteger width = conWidth + 5 + 8; //5表示文字和图片的间距，8表示图片的宽度
    
    [self.showDeepPrcBtn setTitle:con forState:UIControlStateNormal];
    _showDeepPrcBtn.titleLabel.font = PF_MEDI(12);
    [_showDeepPrcBtn setTitleColor:MarketGrayPriceColor forState:UIControlStateNormal];

    CGFloat left = (kScreenWidth - width )/2;
    [_showDeepPrcBtn setTitleEdgeInsets:UIEdgeInsetsMake( 0, left - 13, 0, left)];

    if ( arrowDown ) {
        [_showDeepPrcBtn dk_setImage:DKImageNames(@"positionModify_up", @"positionModify_up")
                            forState:UIControlStateNormal];
    }else{
        [_showDeepPrcBtn dk_setImage:DKImageNames(@"positionModify_down", @"positionModify_down")
                            forState:UIControlStateNormal];
    }
    [_showDeepPrcBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, left + conWidth + 5, 0, left)];
}

- (void)responseToBtn
{
    if( self.deepPrcBlock ){
        self.deepPrcBlock();
    }
}


@end
