//
//  IXDomPosCell.h
//  IXApp
//
//  Created by Bob on 2017/8/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXDomDealM.h"

@interface IXDomPosCell : UITableViewCell

@property (nonatomic, strong) IXDomDealM *model;

- (void)updateBuyVol;
- (void)updateSellVol;
- (void)updateProfit;

@end
