//
//  IXHorizontalkVC.h
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXKLineType.h"
@class IXTradeMarketModel;

@interface IXHorizontalkVC : UIViewController

@property (nonatomic,assign)BOOL hideStatusBar;
@property (nonatomic,copy)void(^hkvcWillDismiss)();

@property (nonatomic,assign)IXKLineType kLineType;

- (instancetype)initWithTradeModel:(IXTradeMarketModel *)trade
                         lastPrice:(float)price;


@end
