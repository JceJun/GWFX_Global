//
//  IXHoriKTitleV.m
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXHoriKTitleV.h"
#import "IXTradeMarketModel.h"
#import "IXMetaData.h"


@interface IXHoriKTitleV ()
{
    double priceTmp;
}

@property (nonatomic,assign)float dis;

@property (nonatomic,strong)UILabel *nameL;
@property (nonatomic,strong)UILabel *typeL;
@property (nonatomic,strong)UILabel *timeL;

@property (nonatomic,strong)UILabel *priceL;
@property (nonatomic,strong)UILabel *pipL;
@property (nonatomic,strong)UILabel *perL;

@property (nonatomic,strong)UILabel *nowL;
@property (nonatomic,strong)UILabel *lastL;
@property (nonatomic,strong)UILabel *nowPL;
@property (nonatomic,strong)UILabel *lastPL;

@property (nonatomic,strong)UILabel *highL;
@property (nonatomic,strong)UILabel *lowL;
@property (nonatomic,strong)UILabel *highPL;
@property (nonatomic,strong)UILabel *lowPL;

@property (nonatomic,strong)UIButton *shutBtn;

@property (nonatomic,strong)IXMetaData  * todayMeta;
@end

@interface IXHoriKTitleV ()
@property (nonatomic,assign)NSInteger digits;
@end

@implementation IXHoriKTitleV

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *seqV = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - .5f, self.frame.size.width, .5f)];
        seqV.dk_backgroundColorPicker = DKLineColor;
        self.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
        
        [self addSubview:seqV];
        
        CGFloat x = 0.f;
        if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
            x = [[UIApplication sharedApplication] statusBarFrame].size.height;
        }
        
        self.dis = (kScreenHeight - x - kBtomMargin - 10 - 95 - 200 - 7 - 60 - 25 - 60 - 25 - 60 - 50) / 5.f;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self nowL];
    [self nowPL];
    [self lastL];
    [self lastPL];
    
    [self highL];
    [self highPL];
    [self lowL];
    [self lowPL];
    
    [self shutBtn];
}

#pragma mark - Part 1

- (UILabel *)nameL
{
    if (!_nameL) {
        CGRect rect = CGRectMake(10, 10, 95 + _dis + 5, 15);
        _nameL = [[UILabel alloc] initWithFrame:rect];
        _nameL.dk_textColorPicker = DKCellTitleColor;
        _nameL.font = PF_MEDI(15.f);
        _nameL.text = @"--";
        [self addSubview:_nameL];
    }
    return _nameL;
}

- (UILabel *)typeL
{
    if (!_typeL) {
        CGRect rect = CGRectMake(10, GetView_MaxY(self.nameL) + 1.5, 105, 10);
        _typeL = [[UILabel alloc] initWithFrame:rect];
        _typeL.dk_textColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
        _typeL.font = RO_REGU(10.f);
        _typeL.text = @"--";
        [self addSubview:_typeL];
    }
    return _typeL;
}

- (UILabel *)timeL
{
    if (!_timeL) {
        CGRect rect = CGRectMake(10, GetView_MaxY(self.typeL) + 1.0, 105, 10);
        _timeL = [[UILabel alloc] initWithFrame:rect];
        _timeL.dk_textColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
        _timeL.font = RO_REGU(10.f);
        _timeL.text = @"--";
        [self addSubview:_timeL];
    }
    return _timeL;
}

#pragma mark - Part 2

- (UILabel *)priceL
{
    if (!_priceL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.typeL) - 10 + _dis, 7, 200, 40);
        _priceL = [[UILabel alloc] initWithFrame:rect];
        _priceL.textColor = UIColorHexFromRGB(0x4c6072);
        _priceL.textAlignment = NSTextAlignmentCenter;
        _priceL.font = RO_REGU(40.f);
        _priceL.text = @"--";
        [self addSubview:_priceL];
    }
    return _priceL;
}

- (UILabel *)pipL
{
    if (!_pipL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.priceL) + 7, GetView_MinY(self.nameL) + 2, 60, 12);
        _pipL = [[UILabel alloc] initWithFrame:rect];
        _pipL.textColor = UIColorHexFromRGB(0x4c6072);
        _pipL.font = RO_REGU(12.f);
        _pipL.text = @"--";
        [self addSubview:_pipL];
    }
    return _pipL;
}

- (UILabel *)perL
{
    if (!_perL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.priceL) + 7, GetView_MinY(self.timeL) - 2, 60, 12);
        _perL = [[UILabel alloc] initWithFrame:rect];
        _perL.textColor = UIColorHexFromRGB(0x4c6072);
        _perL.font = RO_REGU(12.f);
        _perL.text = @"--";
        _lastL.backgroundColor = [UIColor greenColor];
        [self addSubview:_perL];
    }
    return _perL;
}

#pragma mark - Part 3

- (UILabel *)nowL
{
    if (!_nowL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.pipL) + _dis, GetView_MinY(self.pipL), 35, 12);
        _nowL = [[UILabel alloc] initWithFrame:rect];
        _nowL.dk_textColorPicker = DKGrayTextColor;
        _nowL.font = RO_REGU(12.f);
        _nowL.text = LocalizedString(@"今开");
        [self addSubview:_nowL];
    }
    return _nowL;
}

- (UILabel *)lastL
{
    if (!_lastL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.pipL) + _dis, GetView_MinY(self.perL), 35, 12);
        _lastL = [[UILabel alloc] initWithFrame:rect];
        _lastL.dk_textColorPicker = DKGrayTextColor;
        _lastL.font = RO_REGU(12.f);
        _lastL.text = LocalizedString(@"昨收");
        [self addSubview:_lastL];
    }
    return _lastL;
}

#pragma mark - Part 4

- (UILabel *)nowPL
{
    if (!_nowPL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.nowL) + _dis, GetView_MinY(self.nowL), 60, 12);
        _nowPL = [[UILabel alloc] initWithFrame:rect];
        _nowPL.dk_textColorPicker = DKGrayTextColor;
        _nowPL.font = RO_REGU(12.f);
        _nowPL.text = @"--";
        [self addSubview:_nowPL];
    }
    return _nowPL;
}

- (UILabel *)lastPL
{
    if (!_lastPL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.lastL) + _dis, GetView_MinY(self.lastL), 60, 12);
        _lastPL = [[UILabel alloc] initWithFrame:rect];
        _lastPL.dk_textColorPicker = DKGrayTextColor;
        _lastPL.font = RO_REGU(12.f);
        _lastPL.text = @"--";
        [self addSubview:_lastPL];
    }
    return _lastPL;
}

#pragma mark - Part 5

- (UILabel *)highL
{
    if (!_highL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.nowPL) + _dis, GetView_MinY(self.nowPL), 35, 12);
        _highL = [[UILabel alloc] initWithFrame:rect];
        _highL.dk_textColorPicker = DKGrayTextColor;
        _highL.font = RO_REGU(12.f);
        _highL.text = LocalizedString(@"最高");
        [self addSubview:_highL];
    }
    return _highL;
}

- (UILabel *)lowL
{
    if (!_lowL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.lastPL) + _dis, GetView_MinY(self.lastPL), 35, 12);
        _lowL = [[UILabel alloc] initWithFrame:rect];
        _lowL.dk_textColorPicker = DKGrayTextColor;
        _lowL.font = RO_REGU(12.f);
        _lowL.text = LocalizedString(@"最低");
        [self addSubview:_lowL];
    }
    return _lowL;
}

#pragma mark - Part 6

- (UILabel *)highPL
{
    if (!_highPL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.highL) + _dis, GetView_MinY(self.highL), 60, 12);
        _highPL = [[UILabel alloc] initWithFrame:rect];
        _highPL.dk_textColorPicker = DKGrayTextColor;
        _highPL.font = RO_REGU(12.f);
        _highPL.text = @"--";
        [self addSubview:_highPL];
    }
    return _highPL;
}

- (UILabel *)lowPL
{
    if (!_lowPL) {
        CGRect rect = CGRectMake(GetView_MaxX(self.lowL) + _dis, GetView_MinY(self.lowL), 60, 12);
        _lowPL = [[UILabel alloc] initWithFrame:rect];
        _lowPL.textColor = MarketGrayPriceColor;
        _lowPL.font = RO_REGU(12.f);
        _lowPL.text = @"--";
        [self addSubview:_lowPL];
    }
    return _lowPL;
}

- (UIButton *)shutBtn
{
    if (!_shutBtn) {
        _shutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _shutBtn.frame = CGRectMake(self.frame.size.width - 50, 0, 50, 50);
        [_shutBtn setImageEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
        [_shutBtn setImage:GET_IMAGE_NAME(@"market_shutdown") forState:UIControlStateNormal];
        [_shutBtn addTarget:self action:@selector(btnClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_shutBtn];
    }
    return _shutBtn;
}

- (void)btnClicked
{
    if (self.shutBtnClicked) {
        self.shutBtnClicked();
    }
}

#pragma mark -
#pragma makr - refresh

- (void)refreshUIWithTrade:(IXTradeMarketModel *)tradeModel
{
    self.nameL.text = tradeModel.symbolModel.languageName;
    
    IXSymbolM *symbol = tradeModel.symbolModel;
    NSInteger marketID = tradeModel.marketId;
    self.digits = symbol.digits;
    self.nameL.text = symbol.languageName;
    
    NSString *content = [NSString stringWithFormat:@"%@:%@",symbol.name,[IXEntityFormatter getMarketNameWithMarketId:marketID]];
    self.typeL.text = content;
}

- (void)syncDynamicWithQuote:(IXQuoteM *)quote
{
    self.timeL.text = [NSString stringWithFormat:@"%@",[IXEntityFormatter timeIntervalToString:quote.n1970Time]];
    
    [self updateProfit:quote.nPrice];
    
    if (quote.nPrice != 0) {
        self.priceL.text = [self formatterPrice:[NSString stringWithFormat:@"%lf",quote.nPrice]];
    } else {
        self.priceL.text = @"--";
        self.priceL.textColor = MarketGrayPriceColor;
    }
    
    //动态刷新最高最低价
    if (_todayMeta) {
        if (quote.nPrice > _todayMeta.high) {
            _todayMeta.high = quote.nPrice;
            self.highPL.text = [self formatterPrice:[NSString stringWithFormat:@"%lf",quote.nPrice]];
        } else if (quote.nPrice < _todayMeta.low) {
            _todayMeta.low = quote.nPrice;
            self.lowPL.text = [self formatterPrice:[NSString stringWithFormat:@"%lf",quote.nPrice]];
        }
    }
}

- (void)updateProfit:(double)price
{
    if (_lastPrice == 0 || price == 0){
        self.pipL.text = @"--";
        self.perL.text = @"--";
    } else {
        float offsetPrice = price - _lastPrice;
        self.pipL.text = [NSString formatterPrice:[NSString formatterPriceSign:offsetPrice]
                                       WithDigits:_digits];
        if (offsetPrice < 0) {
            self.pipL.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            self.perL.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        } else if (offsetPrice == 0){
            self.pipL.dk_textColorPicker = DKCellContentColor;
            self.perL.dk_textColorPicker = DKCellContentColor;
        } else {
            self.pipL.dk_textColorPicker =  DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
            self.perL.dk_textColorPicker =  DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        }
        
        if (priceTmp == 0.f) {
            self.priceL.dk_textColorPicker = DKCellContentColor;
        } else if(price > priceTmp) {
            self.priceL.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        } else {
            self.priceL.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        }
        priceTmp = price;
        
        if (_lastPrice != 0) {
            NSString *profitSwapStr = [NSString formatterPrice:[NSString formatterPriceSign:(offsetPrice/_lastPrice * 100)]
                                                    WithDigits:2];
            self.perL.text = [NSString stringWithFormat:@"%@%%",profitSwapStr];
        }
    }
}

- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price WithDigits:_digits];
}

- (void)syncFirstPriceWithArray:(NSMutableArray *)array
{
    NSDictionary *dic = array.firstObject;
    if (!dic) {
        return;
    }
    
    _todayMeta = [[IXMetaData alloc] initWithDic:dic];
    
    self.lastPL.text = [NSString formatterPrice:[NSString stringWithFormat:@"%f",_lastPrice] WithDigits:self.digits];
    
    self.nowPL.text  = [NSString formatterPrice:[NSString stringWithFormat:@"%f",_todayMeta.open] WithDigits:self.digits];
    if (_todayMeta.open > _lastPrice) {
        self.nowPL.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    } else if(_todayMeta.open < _lastPrice) {
        self.nowPL.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
    
    self.highPL.text = [NSString formatterPrice:[NSString stringWithFormat:@"%f",_todayMeta.high] WithDigits:self.digits];
    if (_todayMeta.high > _lastPrice) {
        self.highPL.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    } else if(_todayMeta.high < _lastPrice) {
        self.highPL.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
    
    self.lowPL.text = [NSString formatterPrice:[NSString stringWithFormat:@"%f",_todayMeta.low] WithDigits:self.digits];
    if (_todayMeta.low > _lastPrice) {
        self.lowPL.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    } else if(_todayMeta.low < _lastPrice) {
        self.lowPL.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
}

@end

