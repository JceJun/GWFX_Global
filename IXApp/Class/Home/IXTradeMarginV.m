//
//  IXTradeMarginV.m
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeMarginV.h"
#import "IXCpyConfig.h"

@interface IXTradeMarginV ()

@property (nonatomic, strong) UILabel *commisionLbl;

@property (nonatomic, strong) UILabel *marginTitleLbl;

@end

@implementation IXTradeMarginV

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x242a36);
        self.backgroundColor = [UIColor clearColor];
        CGRect currentFrame = CGRectMake( 15, 0, kScreenWidth - 30, 25);
        _marginTitleLbl = [IXUtils createLblWithFrame:currentFrame
                                             WithFont:PF_MEDI(12)
                                            WithAlign:NSTextAlignmentLeft
                                           wTextColor:0x4c6072
                                           dTextColor:0x8395a4];
        [self addSubview:_marginTitleLbl];
        _marginTitleLbl.text = [NSString stringWithFormat:@"%@:--",LocalizedString(@"保证金")];
        
        
        _commisionLbl = [IXUtils createLblWithFrame:currentFrame
                                           WithFont:PF_MEDI(12)
                                          WithAlign:NSTextAlignmentRight
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4];
        [self addSubview:_commisionLbl];
        _commisionLbl.text = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"佣金"),
                              LocalizedString(@"免费")];
        
         _sellBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_sellBtn];
        
        if (kBtomMargin == 0) {
            _sellBtn.frame = CGRectMake( 0, 25, kScreenWidth, 43);
        } else {
            _sellBtn.frame = CGRectMake(13, 25, kScreenWidth - 26, 43);
            _sellBtn.layer.cornerRadius = 2.f;
            _sellBtn.layer.masksToBounds = YES;
        }
        
        [_sellBtn setTitle:LocalizedString(@"提  交") forState:UIControlStateNormal];
        [_sellBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_sellBtn.titleLabel setFont:PF_REGU(15)];
        _sellBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
        [_sellBtn addTarget:self
                     action:@selector(responseToBtn)
           forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)setMarginValue:(NSString *)margin
{
    dispatch_async( dispatch_get_main_queue(), ^{
        NSString *value = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"保证金"),margin];
        _marginTitleLbl.text = value;
        [IXDataProcessTools resetLabel:_marginTitleLbl leftContent:LocalizedString(@"保证金") leftFont:12 WithContent:value rightFont:12 fontType:NO];
    });
}

- (void)setCommissionValue:(NSString *)commission
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ( 0 == [commission floatValue] ) {
            _commisionLbl.text = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"佣金"),
                                  LocalizedString(@"免费")];
            _commisionLbl.font = PF_MEDI(12);
        }else{
            _commisionLbl.text = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"佣金"),
                                  commission];
            [IXDataProcessTools resetLabel:_commisionLbl leftContent:LocalizedString(@"佣金") leftFont:12 WithContent:[NSString stringWithFormat:@"%@:%@",LocalizedString(@"佣金"),commission] rightFont:12 fontType:NO];
        }
    });
}

- (void)setCommisionLblHidden:(BOOL)hidden
{
    _commisionLbl.hidden = hidden;
}
- (void)responseToBtn
{
    if (self.addOrder) {
        self.addOrder();
    }
}

@end
