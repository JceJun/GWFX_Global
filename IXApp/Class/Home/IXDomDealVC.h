//
//  IXDomDealVC.h
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXTradeMarketModel.h"
#import "IXSymbolDetailVC.h"

#define UPDATEPROFIT @"updateProfit"

@interface IXDomDealVC : IXDataBaseVC

@property (nonatomic, strong) IXTradeMarketModel *tradeModel;
@property (nonatomic, strong) IXSymbolDetailVC *detailVC;

@property (nonatomic, assign) double nLastClosePrice;
@property (nonatomic, assign) IXSymbolTradeState tradeState;


@end
