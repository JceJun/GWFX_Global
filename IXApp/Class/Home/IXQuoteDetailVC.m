//
//  IXQuoteDetailVC.m
//  IXApp
//
//  Created by bob on 16/11/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteDetailVC.h"

#import "IXQuoteCell.h"
#import "IXQuoteThumbCell.h"

#import "IXOptionHeadV.h"
#import "IXChooseSymbolCataView.h"

#import "IXOpenRootVC.h"
#import "IXSymbolDetailVC.h"

#import "IXCpyConfig.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDataProcessTools.h"

#import "IXLastQuoteM.h"
#import "IXQuoteDataCache.h"
#import "IXQuoteDistribute.h"

#import "IXAccountBalanceModel.h"
#import "IXADMgr.h"


#define MAXSUBCOUNT 25

#define OBJECT @"object"
#define STATUS @"status"

@interface IXQuoteDetailVC ()
<
UITableViewDelegate,UITableViewDataSource,
IXQuoteThumbCellDelegate
>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) IXQuoteDetailM *detailModel;
@property (nonatomic, strong) IXChooseSymbolCataView *chooseCata;

@end

@implementation IXQuoteDetailVC

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadView)
                                                     name:kSaveSubSymbolCellStyle
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadView)
                                                     name:DKNightVersionThemeChangingNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithSymbolCataId:(NSInteger)cataId
              WithMarketId:(NSInteger)marketId
{
    self = [self init];
    if (self) {
        
        _detailModel = [[IXQuoteDetailM alloc] initWithMarketId:marketId SymCataId:cataId];
        weakself;
        _detailModel.updatePosNum = ^{
            [weakSelf.tableV reloadData];
        };
        
        _detailModel.updateSym = ^{
            [weakSelf.detailModel setSymbolCataId:weakSelf.detailModel.symbolCataId];
            [weakSelf refreashState];
            [weakSelf subscribeDynamicPrice];
            [weakSelf.tableV reloadData];
        };
        
     
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!_detailModel.dataSource.count) {
        _detailModel.dataSource = [[_detailModel querySymbolList] mutableCopy];
    }
    [self refreashState];
    [self subscribeDynamicPrice];
    [self.tableV reloadData];
}

- (NSInteger)cataId{
    return _detailModel.symbolCataId;
}


#pragma mark --
#pragma mark 页面跳转
- (void)responseToTradeWithDirection:(item_order_edirection)direction WithTag:(NSInteger)tag
{
    if ( _detailModel.tradeStatusArr.count > tag ) {
        IXSymbolTradeState state = [_detailModel.tradeStatusArr[tag] integerValue];
        if (![IXDataProcessTools canTradeByState:state orderDir:direction]) {
            return;
        }
        IXOpenRootVC *openVC = [[IXOpenRootVC alloc] init];
        
        NSDictionary *dataDic = _detailModel.dataSource[tag];
        
        NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:[dataDic integerForKey:kID]];
        IXSymbolM *model = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
        if (!model.contractSizeNew) {
            model.contractSizeNew = 1;
        }
        IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
        tradeModel.symbolModel = model;
        tradeModel.marketId = [self getMarketIdWithSymbolId:model.id_p];
        
        tradeModel.direction = direction;
        
        IXQuoteM *dic;
        id value = [self dynQuoteDataModelWithSymbolId:model.id_p];
        if( [value isKindOfClass:[NSDictionary class]] ){
            dic = value[OBJECT];
        }else if( [value isKindOfClass:[IXQuoteM class]] ){
            dic = value;
        }
        if (dic) {
            tradeModel.quoteModel = dic;
        }
        openVC.tradeModel = tradeModel;
        
        IXLastQuoteM *lastPrice = [self lcModelWithSymId:[dataDic integerForKey:kID]];
        if (lastPrice) {
            openVC.openModel.nLastClosePrice = lastPrice.nLastClosePrice;
            tradeModel.nLastPrice = lastPrice.nLastClosePrice;
        }
        openVC.hidesBottomBarWhenPushed = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.parentVC.navigationController pushViewController:openVC animated:YES];
        });
    }
}

- (void)pushToTradeWithRow:(NSInteger)row WithDirection:(item_order_edirection)direction
{
    IXSymbolDetailVC *openVC = [[IXSymbolDetailVC alloc] init];
    
    IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
    
    NSDictionary *dataDic = _detailModel.dataSource[row];
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:[dataDic integerForKey:kID]];
    IXSymbolM *model = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
    if (!model.contractSizeNew) {
        model.contractSizeNew = 1;
    }
    tradeModel.symbolModel = model;
    tradeModel.marketId = [self getMarketIdWithSymbolId:model.id_p];
    
    IXQuoteM *dic;
    id value = [self dynQuoteDataModelWithSymbolId:model.id_p];
    if( [value isKindOfClass:[NSDictionary class]] ){
        dic = value[OBJECT];
    }else if( [value isKindOfClass:[IXQuoteM class]] ){
        dic = value;
    }
    if (dic) {
        tradeModel.quoteModel = dic;
    }
    IXLastQuoteM *lastPrice = [self lcModelWithSymId:[dataDic integerForKey:kID]];
    if ( lastPrice ) {
        openVC.nLastClosePrice =  lastPrice.nLastClosePrice;
    }
    openVC.tradeModel = tradeModel;
    openVC.hidesBottomBarWhenPushed = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.parentVC.navigationController pushViewController:openVC animated:YES];
    });
}


#pragma mark --
#pragma mark 订阅和取消订阅
- (void)subscribeDynamicPrice
{
    if (_detailModel.dataSource.count > 0) {
        [self combineQuoteArr];
    }else{
        [_detailModel setSymbolCataId:_detailModel.symbolCataId];
        if (_detailModel.dataSource.count > 0) {
            [self combineQuoteArr];
        }
    }
}

- (void)combineQuoteArr
{
    [self refreashState];
    if (_detailModel.subDynPriceArr.count > 0) {
        [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:_detailModel.subDynPriceArr];
        [self subscribeVisualQuote];
    }
}
 
- (void)canceQuotePrice
{
    [self cancelVisualQuote];
}

- (void)cancelVisualQuote
{
    NSArray *unSubcribeArr = [_detailModel.subDynPriceArr filteredArrayUsingPredicate:
                              [NSPredicate predicateWithFormat:@"NOT (SELF in %@)",
                               [IXAccountBalanceModel shareInstance].dynQuoteArr]];
    if (unSubcribeArr.count) {
        [[IXTCPRequest shareInstance] unSubQuotePriceWithSymbolAry:unSubcribeArr];
    }
}

- (void)subscribeVisualQuote
{
//    NSArray *subcribeArr = [_detailModel.subDynPriceArr filteredArrayUsingPredicate:
//                            [NSPredicate predicateWithFormat:@"NOT (SELF in %@)",
//                             [IXAccountBalanceModel shareInstance].dynQuoteArr]];
    if (_detailModel.subDynPriceArr.count) {
        [[IXTCPRequest shareInstance] subscribeDynPriceWithSymbolAry:_detailModel.subDynPriceArr];
    }
}

- (void)reloadView
{
    _detailModel.cellType = [IXEntityFormatter getCellStyle];
    [_tableV reloadData];
}

#pragma mark --
#pragma mark 查询缓存行情
/**
 查询昨收价

 @param symId  产品Id
 @return 昨收价（没有缓存记录则返回nil）
 */
- (IXLastQuoteM *)lcModelWithSymId:(NSInteger)symId
{
    NSString *symbolKey = [@(symId) stringValue];
    IXLastQuoteM *model = _detailModel.lastClosePriceDic[symbolKey];
    if ( !model ) {
        model = [IXDataProcessTools queryYesterdayPriceBySymbolId:symId];
        if ( model && 0 != model.nLastClosePrice ) {
            [_detailModel.lastClosePriceDic setValue:model forKey:symbolKey];
            return model;
        }else{
            return nil;
        }
    }
    return model;
}

/**
 查询动态行情

 @param symbolId id
 @return 动态行情或者nil
 */
- (id)dynQuoteDataModelWithSymbolId:(NSInteger)symbolId
{
    NSString *symbolKey = [@(symbolId) stringValue];
    IXQuoteM *model = _detailModel.priceDic[symbolKey];
    if (0 == model.symbolId || 0 == model.nPrice) {
        model = [IXDataProcessTools queryQuoteDataBySymbolId:symbolId];
        if (0 == model.symbolId || 0 == model.nPrice) {
            return nil;
        }else{
            [_detailModel.priceDic setValue:model forKey:symbolKey];
        }
    }
    return model;
}


/**
 查询市场Id

 @param symbolId 产品Id
 @return 整型marketId
 */
- (NSInteger)getMarketIdWithSymbolId:(uint64)symbolId
{
    NSDictionary *dic = _detailModel.marketInfoDic;
    NSString *key = [IXEntityFormatter integerToString:symbolId];
    return [[(NSDictionary *)[dic objectForKey:key]
             objectForKey:kMarketId] integerValue];
} 

#pragma mark -
#pragma mark - IXQuoteDistribute delegate
- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
     if(cmd == CMD_QUOTE_SUB ||
        cmd == CMD_QUOTE_PUB){
        [self refreashDynPrice:arr];
    }else if (cmd == CMD_QUOTE_CLOSE_PRICE){
        [self refreahLastClosePrice:arr];
    }else if (cmd == CMD_QUOTE_UNSUB){
        if(_detailModel.needReSubDynQuote){
            _detailModel.needReSubDynQuote = NO;
        }
    }
}

#pragma mark --
#pragma mark 行情刷新和昨收价
- (void)refreahLastClosePrice:(NSArray *)arr
{
    [_detailModel.subLastClosePriceArr removeAllObjects];
    for (IXLastQuoteM *model in arr) {
        [_detailModel.lastClosePriceDic setObject:[model mutableCopy]
                                           forKey:[@(model.symbolId) stringValue]];
    }
    //刷新可视区域的行情
    for (UITableViewCell *cell in [_tableV visibleCells]) {
        if ([cell isKindOfClass:[IXQuoteCell class]]) {
            NSDictionary *model = _detailModel.dataSource[cell.tag];
            IXLastQuoteM *qModel = [self lcModelWithSymId:[model integerForKey:kID]];
            if (qModel) {
                [(IXQuoteCell *)cell setLastClosePriceInfo:qModel];
            }
        }
    }
}

- (void)refreashDynPrice:(NSArray *)arr
{
    //只保存model.symbolId存在的行情
    NSMutableArray *resultAry = [NSMutableArray arrayWithArray:arr];
    [resultAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXQuoteM *model = (IXQuoteM *)obj;
        BOOL needRef = [_detailModel.marketInfoDic.allKeys containsObject:[@(model.symbolId) stringValue]];
        if (needRef) {
            [_detailModel.priceDic setValue:[model mutableCopy] forKey:[@(model.symbolId) stringValue]];
        }
    }];
   
    //刷新可视区域的行情
    for (UITableViewCell *cell in [_tableV visibleCells]) {
        if ([cell isKindOfClass:[IXQuoteCell class]]) {
            // 刷 XQuoteCell
            NSDictionary *dic = _detailModel.dataSource[cell.tag];
            id value = [self dynQuoteDataModelWithSymbolId:[dic integerForKey:kID]];
            if( [value isKindOfClass:[IXQuoteM class]] ){
                [(IXQuoteCell *)cell setQuotePriceInfo:value];
            }
        }else if([cell isKindOfClass:[IXQuoteThumbCell class]]){
            // 刷 QuoteThumbCell
            NSDictionary *dic = _detailModel.dataSource[cell.tag];
            id value = [self dynQuoteDataModelWithSymbolId:[dic integerForKey:kID]];
            if([value isKindOfClass:[IXQuoteM class]]){
                [(IXQuoteThumbCell *)cell setQuotePriceInfo:value];
            }
        }
    }
}



#pragma mark --
#pragma mark 刷新数据
//刷新状态
- (void)refreashState
{
    [_detailModel.tradeStatusArr removeAllObjects];
    [_detailModel.subDynPriceArr removeAllObjects];
    
    NSMutableArray *dataArr = [NSMutableArray array];
    for (int i = 0; i < _detailModel.dataSource.count; i++) {
        NSDictionary *dic = _detailModel.dataSource[i];
        [dataArr addObject:@{@"symbolId":@([dic integerForKey:kID])}];
        
        IXSymbolTradeState state = [IXDataProcessTools symbolIsCanTradeNew:[dic integerForKey:kID]];
        [_detailModel.tradeStatusArr addObject:@(state)];
    }
    
    _detailModel.subDynPriceArr = [[IXDBSymbolHotMgr queryBatchSymbolMarketIdsBySymbolIds:dataArr] mutableCopy];
    
    for (NSInteger count = 0; count < _detailModel.subDynPriceArr.count; count++) {
        NSDictionary *dic = _detailModel.subDynPriceArr[count];
        [_detailModel.marketInfoDic setValue:dic forKey:[dic objectForKey:kID]];
    }
}



- (NSString *)marketNameWithSymId:(NSInteger)symId
{
    NSString *result = @"";
    result = [IXEntityFormatter getMarketNameWithMarketId:[self getMarketIdWithSymbolId:symId]];
    return result;
}

#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    _detailModel.numberOfCount = [_detailModel.dataSource count];
    return _detailModel.numberOfCount;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == _detailModel.cellType) {
        
        IXQuoteCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXQuoteCell class])];
        cell.tag = indexPath.row;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            cell.backgroundColor = [UIColor clearColor];
        }
        [cell elemetData];
        
        NSDictionary *dic = _detailModel.dataSource[cell.tag];
        NSString *posNum = [[IXAccountBalanceModel shareInstance].cachePosNumDic stringForKey:
                             dic[kID]];
        
        IXSymbolTradeState state = ( _detailModel.tradeStatusArr.count > cell.tag ) ?
        [_detailModel.tradeStatusArr[cell.tag] integerValue] : IXSymbolTradeStateNormal;
        
        NSString *marketName = [self marketNameWithSymId:[dic[kID] integerValue]];
        [cell setSymbolModel:dic
             WithPositionNum:posNum
              WithMarketName:marketName
              WithTradeState:state];
        
        id value = [self dynQuoteDataModelWithSymbolId:[dic[kID] integerValue]];
        if( [value isKindOfClass:[NSDictionary class]] ){
            [(IXQuoteCell *)cell setQuotePriceInfo:value[OBJECT]];
        }else if( [value isKindOfClass:[IXQuoteM class]] ){
            [(IXQuoteCell *)cell setQuotePriceInfo:value];
        }else{
            [cell resetQuotePrice];
        }
        
        IXLastQuoteM *lastPrice = [self lcModelWithSymId:[dic integerForKey:kID]];
        if(lastPrice){
            [cell  setLastClosePriceInfo:lastPrice];
        }else{
            [cell resetLastClosePriceInfo];
        }
        return cell;
    }else{
        IXQuoteThumbCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                  NSStringFromClass([IXQuoteThumbCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
        cell.tag = indexPath.row;
        cell.thumbDelegate = self;
        NSDictionary *dic =  _detailModel.dataSource[cell.tag];
        
        IXSymbolTradeState state = ( _detailModel.tradeStatusArr.count > cell.tag ) ?
              [_detailModel.tradeStatusArr[cell.tag] integerValue] : IXSymbolTradeStateNormal;
        NSString *marketName = [self marketNameWithSymId:[dic[kID] integerValue]];

        [cell setSymbolModel:dic
              WithMarketName:marketName
              WithTradeState:state];
        
        id value = [self dynQuoteDataModelWithSymbolId:[dic[kID] integerValue]];
        if( [value isKindOfClass:[NSDictionary class]] ){
            [(IXQuoteThumbCell *)cell setQuotePriceInfo:value[OBJECT]];
        }else if( [value isKindOfClass:[IXQuoteM class]] ){
            [(IXQuoteThumbCell *)cell setQuotePriceInfo:value];
        }else{
            [cell resetQuotePrice];
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _detailModel.rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ( 0 == _detailModel.cellType ) {
        [self pushToTradeWithRow:indexPath.row WithDirection:item_order_edirection_DirectionBuy];
    }else{
        [self pushToTradeWithRow:indexPath.row WithDirection:item_order_edirection_DirectionBuy];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (0 == _detailModel.cellType) {
        return 33;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (0 == _detailModel.cellType) {
        UIImageView *sepLine = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 32.5, kScreenWidth, .5)];
        sepLine.dk_backgroundColorPicker = DKLineColor;
        IXOptionHeadV *headView = [self headView];
        [headView addSubview:sepLine];
        return headView;
    }
    return [[UIView alloc] initWithFrame:CGRectZero];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (![IXADMgr shareInstance].haveCloseHome && [IXADMgr shareInstance].homeAdArr.count) {
        return 94;
    } else {
        return 0.01;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (![IXADMgr shareInstance].haveCloseHome && [IXADMgr shareInstance].homeAdArr.count) {
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 94)];
    } else {
        return nil;
    }
}

#pragma mark ScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:sender afterDelay:0];
 
    if (!_detailModel.didLoadAllData) {
        UITableViewCell *cell = [_tableV.visibleCells lastObject];
        if (cell.tag == _detailModel.dataSource.count - 3) {
            NSArray *arr = [_detailModel querySymbolList];
            if ( arr && arr.count ) {
                [_detailModel.dataSource addObjectsFromArray:arr];
                [self refreashState];
                [_tableV reloadData];
            }
        }
    }
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
 
    if ( _detailModel.dataSource.count == 0 ) {
        return;
    }
    _detailModel.needReSubDynQuote = YES;
    //获取停止滑动后，可视区域的第一行的Symbol
    NSInteger startTag = 1;
    for (UITableViewCell *cell in _tableV.visibleCells) {
        if ([cell isKindOfClass:[IXQuoteCell class]]||
            [cell isKindOfClass:[IXQuoteThumbCell class]]) {
            startTag = cell.tag;
            break;
        }
    }
    
    if (_tableV.visibleCells.count == 0) {
        return;
    }
    //如果可视区域第一行的symbol在订阅列表，则不更新列表，
    //否则需要重新订阅
    //往上和往下均缓冲一页订阅
    if( _tableV.visibleCells[0].tag <= _detailModel.currentSubStartTag + 5
       || [_tableV.visibleCells lastObject].tag  >= _detailModel.currentSubStartTag + MAXSUBCOUNT - 5 ||
       (_tableV.visibleCells[0].tag <= 1 && _tableV.visibleCells[0].tag < _detailModel.currentSubStartTag) ||
       ([_tableV.visibleCells lastObject].tag > _detailModel.currentSubStartTag &&
        [_tableV.visibleCells lastObject].tag >= _detailModel.dataSource.count - 1) ){
           //取消订阅可视区域以外内容的订阅
           NSArray *unSubcribeArr = [_detailModel.subDynPriceArr filteredArrayUsingPredicate:
                                     [NSPredicate predicateWithFormat:@"NOT (SELF in %@)",
                                      [IXAccountBalanceModel shareInstance].dynQuoteArr]];
           [[IXTCPRequest shareInstance] unSubQuotePriceWithSymbolAry:unSubcribeArr];
           [_detailModel.subDynPriceArr removeAllObjects];
           
           _detailModel.currentSubStartTag = startTag;
           for ( NSInteger count = 0; count < MAXSUBCOUNT && count + startTag <_detailModel.dataSource.count; count++) {
               NSDictionary *model = _detailModel.dataSource[count+startTag];
               NSInteger symId = [model integerForKey:kID];
               NSInteger mId = [self getMarketIdWithSymbolId:symId];
               if (![self lcModelWithSymId:symId]) {
                   [_detailModel.subLastClosePriceArr addObject:@{kID:@(symId),
                                                                  kMarketId:@(mId)}];
               }
               [_detailModel.subDynPriceArr addObject:@{kID:@(symId),
                                                        kMarketId:@(mId)}];
           }
           [[IXTCPRequest shareInstance] subscribeDynPriceWithSymbolAry:_detailModel.subDynPriceArr];
           
           //可视区域的Symbol没有订阅昨日收盘价，则需要订阅
           if ( _detailModel.subLastClosePriceArr ) {
               [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:_detailModel.subLastClosePriceArr];
           }
       }
}


#pragma mark -
#pragma mark - lazy load
- (UITableView *)tableV
{
    if (!_tableV) {
        CGRect frame = self.view.bounds;
        if (SymbolCataStyle) {
            frame.size.height -= (kTabbarHeight + kNavbarHeight + 40+40);
        } else {
            frame.size.height -= (kTabbarHeight + kNavbarHeight+40);
        }
        _tableV = [[UITableView alloc] initWithFrame:frame];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        
        _tableV.showsVerticalScrollIndicator = NO;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorInset = UIEdgeInsetsMake( 0, -20, 0, 0);
        _tableV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [_tableV registerClass:[IXQuoteCell class]
        forCellReuseIdentifier:NSStringFromClass([IXQuoteCell class])];
        [_tableV registerClass:[IXQuoteThumbCell class]
        forCellReuseIdentifier:NSStringFromClass([IXQuoteThumbCell class])];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

- (IXOptionHeadV *)headView
{
    IXOptionHeadV *headView = [[IXOptionHeadV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 33)];
    weakself;
    headView.orderKind = ^(FilterType kind){
        switch (kind) {
            case FilterType_All:{
                weakSelf.chooseCata.dataSourceAry = [weakSelf.detailModel.chooseMarketArr mutableCopy];
                [weakSelf.chooseCata show];
            }
                break;
            default:
                break;
        }
    };
    return headView;
}

- (IXChooseSymbolCataView *)chooseCata
{
    if (!_chooseCata) {
        _chooseCata = [[IXChooseSymbolCataView alloc] initWithCataAry:@[]
                                                           WithChoose:nil];
                       
        _chooseCata.type = Symbol_HotAllType;
        _chooseCata.chooseSymbol = ^(NSInteger btnTag){
            //刷新行情
        };
    }
    return _chooseCata;
}


@end
