//
//  IXOpenDirectV.h
//  IXApp
//
//  Created by Bob on 2016/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^orderDirection)(item_order_edirection direction);

@interface IXOpenDirectV : UIView

- (id)initWithFrame:(CGRect)frame WithTitle:(NSString *)title WithDir:(orderDirection)dire;

- (void)resetUIWithTag:(item_order_edirection)dirction;

@end
