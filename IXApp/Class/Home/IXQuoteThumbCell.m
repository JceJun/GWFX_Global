//
//  IXQuoteThumbCell.m
//  IXApp
//
//  Created by Bob on 2016/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//  113

#import "IXQuoteThumbCell.h"
#import "UIImageView+SepLine.h"
#import "UILabel+LineSpace.h"
#import "IXDBGlobal.h"

@interface IXQuoteThumbCell ()
{
    BOOL canTrade;

    UIImageView *equalArrowImg;
    UIImageView *lessArrowImg;
    UIImageView *thanlArrowImg;
    UIImageView *disableArrowImg;

    UIImageView *equalBackImg;
    UIImageView *lessBackImg;
    UIImageView *thanlBackImg;
    UIImageView *disableBackImg;
    
    UILabel *symbolName;
    
    UILabel *symbolLabel1;
    UILabel *symbolLabel2;
    UIView  *backView;
    
    UILabel *symbolCode;
    UILabel *addTime;
    
    double prePrice;
 
    UILabel  *buyDirection;
    UILabel *buyPrcLbl;  //买入价
    
    UILabel  *sellDirection;
    UILabel *sellPrcLbl; //卖出价

    UILabel *pips;
    NSDictionary *symbolModel;
    IXQuoteM *quoteM;
}

@end

@implementation IXQuoteThumbCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        [self elementImg];
        [self elementLbl];
        [self addSepLine];
        
        [self setShowImage:0];
    }
    return self;
}

- (void)elementImg
{ 
    UIView *head = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 34)];
    head.dk_backgroundColorPicker = DKColorWithRGBs(0xfffffff, 0x242a36);
    [self.contentView addSubview:head];
    
    CGRect frame = CGRectMake( 0, 34, kScreenWidth, 78);
    
    equalBackImg = [self loadImage:@"quoteCell_equal"
                       WithDarkImg:@"quoteCell_equal_dark"
                         WithFrame:frame];
    
    NSString *day = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_less" : @"quoteCell_than";
    NSString *dark = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_less_dark" : @"quoteCell_than_dark";
    lessBackImg = [self loadImage:day
                      WithDarkImg:dark
                        WithFrame:frame];
    
    day = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_than" : @"quoteCell_less";
    dark = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_than_dark" : @"quoteCell_less_dark";
    thanlBackImg = [self loadImage:day
                       WithDarkImg:dark
                         WithFrame:frame];
    
    disableBackImg = [self loadImage:@"quoteCell_dis"
                         WithDarkImg:@"quoteCell_dis_dark"
                           WithFrame:frame];
    disableBackImg.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    
    frame = CGRectMake( 7, 12, 10, 10);
    disableArrowImg = [self loadImage:@"quoteCell_close"
                          WithDarkImg:@"quoteCell_close_dark"
                            WithFrame:frame];
    
    equalArrowImg = [self loadImage:@"quoteCell_arrow_equal"
                        WithDarkImg:@"quoteCell_arrow_equal_dark"
                          WithFrame:frame];
    
    day = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_greenDown" : @"quote_arrow_redDown";
    dark = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_greenDown_dark" : @"quote_arrow_redDown_dark";
    lessArrowImg = [self loadImage:day
                       WithDarkImg:dark
                         WithFrame:frame];
    
    day = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_redUp" : @"quote_arrow_greenUp";
    dark = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_redUp_dark" : @"quote_arrow_greenUp_dark";
    thanlArrowImg = [self loadImage:day
                        WithDarkImg:dark
                          WithFrame:frame];
}

- (void)elementLbl
{
    CGRect frame = CGRectMake( CGRectGetMaxX(equalArrowImg.frame) + 5, 9, 120, 17);
    symbolName = [IXUtils createLblWithFrame:frame
                                    WithFont:PF_MEDI(13)
                                   WithAlign:NSTextAlignmentLeft
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea];
    [self.contentView addSubview:symbolName];
    
    frame = CGRectMake(CGRectGetMaxX(equalArrowImg.frame) + 5, 14, 120, 10);
    backView = [[UIView alloc] initWithFrame:frame];
    [self.contentView addSubview:backView];
    backView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    
    frame.origin = CGPointZero;
    frame.size.height = 7;
    symbolLabel1 = [IXUtils createLblWithFrame:frame
                                      WithFont:PF_MEDI(7)
                                     WithAlign:NSTextAlignmentCenter
                                    wTextColor:0xffffff
                                    dTextColor:0x262f3e];
    [backView addSubview:symbolLabel1];
    symbolLabel1.text = @"--";
    
    symbolLabel2 = [IXUtils createLblWithFrame:frame
                                      WithFont:PF_MEDI(7)
                                     WithAlign:NSTextAlignmentCenter
                                    wTextColor:0xffffff
                                    dTextColor:0x262f3e];
    [backView addSubview:symbolLabel2];
    symbolLabel2.hidden = YES;
    
  
    
    frame.origin.x = CGRectGetMaxX(backView.frame) + 5;
    frame.origin.y = 15;
    frame.size.height = 10;
    symbolCode = [IXUtils createLblWithFrame:frame
                                    WithFont:RO_REGU(10)
                                   WithAlign:NSTextAlignmentLeft
                                  wTextColor:0xa7adb5
                                  dTextColor:0x8395a4];
    [self.contentView addSubview:symbolCode];
    
    frame = CGRectMake( kScreenWidth - 180, 13, 165, 12);
    addTime = [IXUtils createLblWithFrame:frame
                                 WithFont:RO_REGU(12)
                                WithAlign:NSTextAlignmentRight
                               wTextColor:0xa7adb5
                               dTextColor:0x8395a4];
    [self.contentView addSubview:addTime];
    
    
    UIView *leftOpe = [[UIView alloc] initWithFrame:CGRectMake( 0, 34, kScreenWidth/2, 78)];
    [self.contentView addSubview:leftOpe];
    
    UIView *rightOpe = [[UIView alloc] initWithFrame:CGRectMake( kScreenWidth/2, 34, kScreenWidth/2, 78)];
    [self.contentView addSubview:rightOpe];
    
    NSInteger width = [IXEntityFormatter getContentWidth:@"--" WithFont:PF_MEDI(10)] + 5;
    pips = [IXUtils createLblWithFrame:CGRectMake( (kScreenWidth - width )/2 , 97, (int)width, 15)
                              WithFont:PF_MEDI(10)
                             WithAlign:NSTextAlignmentCenter
                            wTextColor:0xffffff
                            dTextColor:0x8395a4];
    [self.contentView addSubview:pips];
    pips.dk_backgroundColorPicker = DKColorWithRGBs(0x99abba, 0x282a32);
    pips.text = @"--";
    
    frame = CGRectMake( 0, 20, kScreenWidth/2, 20);
    buyPrcLbl = [IXUtils createLblWithFrame:frame
                                   WithFont:RO_REGU(24)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea];
    [rightOpe addSubview:buyPrcLbl];
    
    sellPrcLbl = [IXUtils createLblWithFrame:frame
                                    WithFont:RO_REGU(24)
                                   WithAlign:NSTextAlignmentCenter
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea];
    [leftOpe addSubview:sellPrcLbl];

    //不根据红涨绿跌换，所以没有使用宏定义
    frame = CGRectMake( 0, CGRectGetMaxY(sellPrcLbl.frame) + 9, kScreenWidth/2, 12);
    sellDirection = [IXUtils createLblWithFrame:frame
                                       WithFont:PF_MEDI(12)
                                      WithAlign:NSTextAlignmentCenter
                                     wTextColor:mRedPriceColor
                                     dTextColor:mDRedPriceColor];
    [leftOpe addSubview:sellDirection];
    sellDirection.text = LocalizedString(@"卖出");
    
    buyDirection = [IXUtils createLblWithFrame:frame
                                                WithFont:PF_MEDI(12)
                                               WithAlign:NSTextAlignmentCenter
                                              wTextColor:mGreenPriceColor
                                              dTextColor:mDGreenPriceColor];
    [rightOpe addSubview:buyDirection];
    buyDirection.text = LocalizedString(@"买入");
    
    UITapGestureRecognizer *sellTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(responseSellTap:)];
    [leftOpe addGestureRecognizer:sellTapGes];
    
    UITapGestureRecognizer *buyTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(responseBuyTap:)];
    [rightOpe addGestureRecognizer:buyTapGes];
}

- (UIImageView *)loadImage:(NSString *)day WithDarkImg:(NSString *)dark WithFrame:(CGRect)frame
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.dk_imagePicker = DKImageNames(day,dark);
    [self.contentView addSubview:imageView];
    imageView.hidden = YES;
    
    return imageView;
}

- (void)setDisTradeImg
{
    disableArrowImg.hidden = NO;
    equalArrowImg.hidden = YES;
    lessArrowImg.hidden = YES;
    thanlArrowImg.hidden = YES;
    
    disableBackImg.hidden = NO;
    lessBackImg.hidden = YES;
    thanlBackImg.hidden = YES;
    equalBackImg.hidden = YES;
}

- (void)setShowImage:(NSInteger)showWhichImage
{
    disableBackImg.hidden = YES;
    [equalBackImg setHidden: !(showWhichImage == 0)];
    [lessBackImg setHidden: !(showWhichImage < 0)];
    [thanlBackImg setHidden: !(showWhichImage > 0)];
    
    [disableArrowImg setHidden:YES];
    [equalArrowImg setHidden: !(showWhichImage == 0)];
    [lessArrowImg setHidden: !(showWhichImage < 0)];
    [thanlArrowImg setHidden: !(showWhichImage > 0)];
}

- (void)refreashPipsWithSell:(double)sell WithBuy:(double)buy
{
    
    double points = pow(10, -[symbolModel integerForKey:kDigits]) * [symbolModel integerForKey:kPipsRatio];
    if (0 != points) {
        pips.text = [NSString stringWithFormat:@"%.1f",(buy - sell)/points];
    }else{
        pips.text = @"--";
    }
}

#pragma mark 赋值产品信息
- (void)setSymbolModel:(NSDictionary *)model
        WithMarketName:(NSString *)marketName
        WithTradeState:(IXSymbolTradeState)state

{
    symbolModel = model;

    canTrade = [IXDataProcessTools isNormalByState:state];
    [self resetColorWithState];
    
    symbolName.text = symbolModel[kLanguageName];
    symbolCode.text = [NSString stringWithFormat:@"%@:%@",symbolModel[kName],marketName];
    
    [self setLabel];
}


- (void)resetColorWithState
{
    if (canTrade) {
        symbolName.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        symbolCode.dk_textColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
        addTime.dk_textColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
        buyPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        sellPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        buyDirection.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        sellDirection.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        [self setShowImage:0];
    }else{
        symbolName.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x6c7fa0);
        symbolCode.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        addTime.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        buyPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x6c7fa0);
        sellPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x6c7fa0);
        buyDirection.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        sellDirection.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        [self setDisTradeImg];
    }
}


- (void)setLabel
{
    symbolLabel1.text = @"";
    symbolLabel2.text = @"";
    
    NSArray *labelArr = symbolModel[kLabel];
    if([labelArr isKindOfClass:[NSArray class]] && labelArr.count){
        NSDictionary *label0;
        if (labelArr.count >= 1) {
            
            label0 = labelArr[0];
            NSInteger labeIndex = [label0 integerForKey:@"colour"];
            backView.backgroundColor = UIColorHexFromRGB([IXEntityFormatter getLabelColorValue:labeIndex]);
            
            if ( [[label0 stringForKey:@"name"] length] != 0 ) {
                symbolLabel1.text = [label0 stringForKey:@"name"];
            }
        }
        
        NSDictionary *label1;
        if (labelArr.count >= 2) {
            
            label1 = labelArr[1];
            if ( [[label1 stringForKey:@"name"] length] != 0 ) {
                if (0 == symbolLabel1.text.length) {
                    symbolLabel1.text = [label1 stringForKey:@"name"];
                }else{
                    symbolLabel2.text = [label1 stringForKey:@"name"];
                }
            }
        }
    }
}

- (void)resetFrame
{
    CGRect frame = symbolName.frame;
    frame.size.width = [IXEntityFormatter getContentWidth:symbolName.text WithFont:symbolName.font] + 5;
    symbolName.frame = frame;
    
    NSInteger width = 0;
    NSInteger width1 = (NSInteger)[IXEntityFormatter getContentWidth:symbolLabel1.text
                                                            WithFont:symbolLabel1.font] + 5;
    
    NSInteger width2 = 0;
    NSArray *labelArr = symbolModel[kLabel];
    if ([labelArr isKindOfClass:[NSArray class]] && labelArr.count > 1) {
        width2 = (NSInteger)[IXEntityFormatter getContentWidth:symbolLabel2.text
                                                      WithFont:symbolLabel2.font] + 5;
    }
    
    if ( 0 == width1 - 5 ) {
        width1 = 0;
    }
    
    if ( 0 == width2 - 5 ) {
        width2 = 0;
    }
    
    if ( width1 >= width2 ) {
        width = width1;
    }else{
        width = width2;
    }
    
    if (width2 == 0) {
        symbolLabel2.hidden = YES;
        CGRect frame = symbolLabel1.frame;
        frame.origin.y = 2;
        frame.size.width = width;
        symbolLabel1.frame = frame;
        
        frame = backView.frame;
        frame.size.height = 10;
        frame.origin.x = CGRectGetMaxX(symbolName.frame) + 5;
        frame.origin.y = CGRectGetMinY(symbolCode.frame);
        frame.size.width = width;
        backView.frame = frame;
        
    }else{
        symbolLabel2.hidden = NO;
        CGRect frame = symbolLabel1.frame;
        frame.origin.y = 1;
        frame.size.width = width;
        symbolLabel1.frame = frame;
        
        frame = symbolLabel2.frame;
        frame.origin.y = CGRectGetMaxY(symbolLabel1.frame) + 1;
        frame.size.width = width;
        symbolLabel2.frame = frame;
        
        frame = backView.frame;
        frame.origin.x = CGRectGetMaxX(symbolName.frame) + 5;
        frame.origin.y = CGRectGetMinY(symbolName.frame) ;
        frame.size.height = 17;
        frame.size.width = width;
        backView.frame = frame;
    }
    
    frame = symbolCode.frame;
    frame.origin.x =  (width == 0 ) ? (CGRectGetMaxX(symbolName.frame) + 5) : CGRectGetMaxX(backView.frame) + 5;
    symbolCode.frame = frame;
    
    frame = pips.frame;
    frame.size.width = [self getPipsWidth];
    frame.origin.x = (kScreenWidth - frame.size.width)/2;
    pips.frame = frame;
}

#pragma mark 赋值行情信息
- (void)setQuotePriceInfo:(IXQuoteM *)priceInfo{

    quoteM = priceInfo;
    [self formatterLabel:buyPrcLbl WithContent:quoteM.BuyPrc];
    [self formatterLabel:sellPrcLbl WithContent:quoteM.SellPrc];
    
    if ( priceInfo.BuyPrc.count != 0 ) {
        double buyPrc = [priceInfo.BuyPrc[0] doubleValue];
        double sellPrc = [priceInfo.SellPrc[0] doubleValue];
        [self refreashPipsWithSell:sellPrc WithBuy:buyPrc];
 
        DKColorPicker picker;
        if (canTrade) {
            picker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
            if ( priceInfo.nPrice - prePrice > 0 ) {
                [self setShowImage:1];
            }else if ( priceInfo.nPrice - prePrice == 0 ){
                [self setShowImage:0];
            }else{
                [self setShowImage:-1];
            }
        }else{
            picker = DKColorWithRGBs(0x99abba, 0x6c7fa0);
            [self setDisTradeImg];
        }
        buyPrcLbl.dk_textColorPicker = picker;
        sellPrcLbl.dk_textColorPicker = picker;
        
        prePrice = priceInfo.nPrice;
        
        if ( priceInfo.n1970Time != 0 ) {
            addTime.text = [IXEntityFormatter timeIntervalToString:priceInfo.n1970Time];
        }else{
            addTime.text = @"";
        }
        [UILabel changeWordSpaceForLabel:addTime WithSpace:-0.21];
        addTime.textAlignment = NSTextAlignmentRight;
        
        [self setNeedsLayout];
    }else{
        [self resetQuotePrice];
    }
}

- (void)formatterLabel:(UILabel *)label WithContent:(NSArray *)arr
{
    if ( !arr || !arr.count ) {
        return;
    }
    
    NSString *content =  [NSString formatterPrice:arr[0] WithDigits:[symbolModel integerForKey:kDigits]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:content];
    
    //pipsratio位往上跳
    NSInteger length = content.length;
    NSInteger pipsratio = log10([symbolModel integerForKey:kPipsRatio]);
    NSInteger start = length - pipsratio;
    NSInteger stop = pipsratio;
    if ( start < 0 ) {
        start = 0;
        stop = length;
    }
    
    [str addAttribute:NSBaselineOffsetAttributeName value:@(4) range:NSMakeRange( start, stop)];
    [str addAttribute:NSFontAttributeName value:RO_REGU(18) range:NSMakeRange( start, stop)];
    
    //上跳位往前走两位字号变大
    if ( stop < length ) {
        NSRange range = [content rangeOfString:@"."];
        stop = start - 2;
        
        //如果跨小数点，则多往前走
        if ( range.location != NSNotFound && range.location > stop - 1 ) {
            stop -= 1;
        }
        
        if ( stop >= 0 ) {
            [str addAttribute:NSBaselineOffsetAttributeName value:@(0) range:NSMakeRange( 0, stop)];
            [str addAttribute:NSFontAttributeName value:RO_REGU(20) range:NSMakeRange( 0, stop)];
        }
    }
    
    label.attributedText = str;
}

#pragma mark 还原行情信息
- (void)resetQuotePrice
{
    buyPrcLbl.text = @"--";
    sellPrcLbl.text = @"--";
    buyPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
    sellPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
}

#pragma mark 刷新UI
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self resetFrame];
}

- (NSInteger)getPipsWidth
{
    NSInteger width = [IXEntityFormatter getContentWidth:pips.text WithFont:pips.font] + 5;
    width = ( width < 21 ) ? 21 : width;
    return width;
}

- (void)responseBuyTap:(UITapGestureRecognizer *)tapGes
{
    if ( self.thumbDelegate && [self.thumbDelegate respondsToSelector:@selector(responseToTradeWithDirection:WithTag:)] ) {
        [self.thumbDelegate responseToTradeWithDirection:item_order_edirection_DirectionBuy WithTag:self.tag];
    }
}

- (void)responseSellTap:(UITapGestureRecognizer *)tapGes
{
    if ( self.thumbDelegate && [self.thumbDelegate respondsToSelector:@selector(responseToTradeWithDirection:WithTag:)] ) {
        [self.thumbDelegate responseToTradeWithDirection:item_order_edirection_DirectionSell WithTag:self.tag];
    }
}

- (void)addSepLine
{
    UIImageView *topSepImg = [UIImageView addSepImageWithFrame:CGRectMake( 0, 34, kScreenWidth, kLineHeight)
                                                     WithColor:UIColorHexFromRGB(0x242a36)
                                                     WithAlpha:1.0];
    [self.contentView addSubview:topSepImg];
    
    UIImageView *verSepImg = [UIImageView addSepImageWithFrame:CGRectMake( kScreenWidth/2 - 0.25, 34, 0.5, 63)
                                                     WithColor:UIColorHexFromRGB(0x242a36)
                                                     WithAlpha:1.0];
    [self.contentView addSubview:verSepImg];
}
@end
