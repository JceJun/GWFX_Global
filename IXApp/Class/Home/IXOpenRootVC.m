////
//  IXOpenRootVC.m
//  IXApp
//
//  Created by bob on 16/11/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOpenRootVC.h"

#import "IXTradeMarginV.h"

#import "IXTradeVM.h"
#import "IXAccountBalanceModel.h"
#import "IXTradeMarketModel.h"
#import "IXOpenResultVC.h"
#import "IXOpenModel+CheckData.h"
#import "NSString+FormatterPrice.h"
#import "IXOpenTipV.h"
#import "IXAppUtil.h"

#import "IXQuoteDataCache.h"
#import "IXTouchTableV.h"

#import "IXOpenCellM.h"
#import "IXOpenQuoteM.h"
#import "IXOpenCalM.h"
#import "IXOpenLogicM.h"

#import "UIImageView+SepLine.h"
#import "IXPullDownMenu.h"
#import "IXUserDefaultM.h"

@interface IXOpenRootVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV *contentTV;
 
@property (nonatomic, assign) IXSymbolTradeState tradeState;

@property (nonatomic, strong) IXOpenTipV *tipView;

@property (nonatomic, strong) IXTradeMarginV *marginView;

@property (nonatomic, strong) IXPullDownMenu    * menu;

@property (nonatomic, strong) NSMutableArray *showVolArr;

@property (nonatomic, strong) NSMutableArray *volArr;

@property (nonatomic, strong) NSMutableArray *prcArr;


@property (nonatomic, strong) NSMutableArray *swapRelSym;

//YES：数量   NO：手数
@property (nonatomic, assign) BOOL isVolNum;

@end

@implementation IXOpenRootVC

#pragma mark life cycle
- (id)init
{
    self = [super init];
    if ( self ) {
        _isVolNum = ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount );
    }
    return self;
}

- (void)dealloc
{
    [IXOpenCalM removeTip:_tipView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];  
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    
    [self initDataSource];
    [self.contentTV reloadData];
    [self.view addSubview:self.marginView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
#if DEBUG
    [IXOpenCalM addPrcTipInfo:self];
#else
    self.title = LocalizedString(@"新交易");
#endif
    
    [self resetDeepPrice];
    NSMutableArray *arr = [IXOpenQuoteM subscribeDynamicPrice:self];
    if ( arr && arr.count ) {
        [self didResponseQuoteDistribute:arr cmd:CMD_QUOTE_PUB_DETAIL];
    }
}


- (void)resetSub
{
    if (_tradeModel.quoteModel) {
        [[IXQuoteDataCache shareInstance] saveOneDynQuote:_tradeModel.quoteModel];
    }

    [IXOpenQuoteM cancelDynamicPrice:self];
    
    [IXOpenCalM removeTip:_tipView];
    
#if DEBUG
    [IXOpenCalM removePrcTipInfo:self];
#endif
}

- (void)rightBtnItemClicked
{
    [self resetSub];
    
    NSArray *arr = @[@{@"marketId":@(_tradeModel.marketId),
                       @"id":@(_tradeModel.symbolModel.id_p)}];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyCancelDetailQuote object:arr];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)leftBtnItemClicked
{
    [self resetSub];

    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark dataSouce
- (void)initDataSource
{
    _tradeState = [IXDataProcessTools symbolIsCanTrade:_tradeModel.symbolModel];
    _marginView.sellBtn.userInteractionEnabled = ([IXDataProcessTools isNormalAddOptiosByState:_tradeState]);
    
    self.openModel.quoteModel = _tradeModel.quoteModel;
    self.openModel.symbolModel = _tradeModel.symbolModel;
    self.openModel.orderDir = _tradeModel.direction;
    self.openModel.orderType = MARKETORDER;
    self.tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                     [IXEntityFormatter doubleToString:_tradeModel.symbolModel.volumesMin]];
    self.tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:[IXDataProcessTools showCurrentVolume:_tradeModel.symbolModel.volumesMin contractSizeNew:_tradeModel.symbolModel.contractSizeNew volDigit:_tradeModel.symbolModel.volDigits]];
}

#pragma mark - UITextField代理
- (void)textFieldDidBeginEditing:(UITextField *)textField {
 
    // 获取到父类cell
    UITableViewCell *cell = (UITableViewCell *) [[textField superview] superview];
  
    CGFloat offsetY = CGRectGetMaxY(cell.frame);
    
    CGFloat animationY = CGRectGetHeight(self.contentTV.frame) - offsetY - 217;
    if (  animationY < 0 ) {
        
        if ( offsetY - CGRectGetHeight(self.contentTV.frame) > 0 ) {
            [self.contentTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_openModel.showSLCount inSection:2]
                                  atScrollPosition:UITableViewScrollPositionNone
                                          animated:NO];
        }
        // 执行动画(移动到输入的位置)
        [self.contentTV setContentOffset:CGPointMake(0, -animationY) animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField.tag == ROWNAMEVOLUME) {
        [self resetInputContent:textField];
    }
    
    [self dealValueWithTag:textField.tag WithValue:textField.text];
    
    [self.contentTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                          atScrollPosition:UITableViewScrollPositionNone
                                  animated:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag == ROWNAMEPROFIT ||
       textField.tag == ROWNAMELOSS){
        BOOL hidnErase = ([textField.text length] == 1 && [string length] == 0);
        IXOpenSLCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(textField.tag - 2) inSection:2]];
        cell.hiddenErase = hidnErase;
    }
    return YES;
}

- (void)resetInputContent:(UITextField *)textField
{
    NSString *str = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    if (_isVolNum) {
        textField.text  = [NSString thousandFormate:[NSString formatterPrice:str WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits];
    } else {
        textField.text  = [NSString thousandFormate:[NSString formatterPrice:str WithDigits:2] withDigits:2];
    }
   
}


#pragma mark uitableView delegate && dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section ) {
        case 0:{
            return 2 + _openModel.showDeepPrcCount;
        }
            break;
        case 1:{
            return 4;
        }
            break;
        default:{
            return 1 + _openModel.showSLCount;
        }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section ) {
        case 0:{
            return [self sectionFirstWithIndexPath:indexPath];
        }
            break;
        default:{
            return 44;
        }
            break;
    }
}

- (CGFloat)sectionFirstWithIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row == 0 ) {
        return 73;
    }else if ( indexPath.row == _openModel.showDeepPrcCount + 1){
        return 40;
    }else{
        return 22;
    }
}

#pragma 手动管理Cell
- (void)removeSepLine:(UITableViewCell *)cell
{
    UIImageView *sepLine = (UIImageView *)[cell viewWithTag:1000];
    if ( sepLine ) {
        [sepLine removeFromSuperview];
    }
}

- (void)addSepLine:(UITableViewCell *)cell WithFrame:(CGRect)frame
{
    UIImageView *sepLine = [UIImageView addSepImageWithFrame:frame
                                                   WithColor:CellSepLineColor
                                                   WithAlpha:1.f];
    sepLine.tag = 1000;
    [cell.contentView addSubview:sepLine];
}

- (UITableViewCell *)sectionFirst:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row == 0 ) {
        IXDetailSymbolCell *cell = [IXOpenCellM configSymbolDetailCellWithTableView:tableView
                                                                          WithIndex:indexPath];
        cell.tradeState = _tradeState;
        cell.nLastClosePrice = _openModel.nLastClosePrice;
        cell.symbolModel = _tradeModel.symbolModel;
        cell.quoteModel = _tradeModel.quoteModel;
        cell.marketId = _tradeModel.marketId;
        
        return cell;
    }else if ( indexPath.row == _openModel.showDeepPrcCount + 1){
        IXOpenShowPrcCell *cell = [IXOpenCellM configShowPrcCellWithTableView:tableView
                                                                    WithIndex:indexPath];
        cell.dk_backgroundColorPicker = DKColorWithRGBs( 0xfafcfe, 0x242a36);
        
        [cell refreashUIWithArrowDir:( _openModel.showDeepPrcCount == 1)];
        [self addSepLine:cell WithFrame:CGRectMake( 0, 0, kScreenWidth, kLineHeight)];
        [self addSepLine:cell WithFrame:CGRectMake( 0, 40 - kLineHeight, kScreenWidth, kLineHeight)];
        
        weakself;
        cell.deepPrcBlock = ^{
            NSInteger count = weakSelf.openModel.showDeepPrcCount;
            count = (count == 1) ? 5 : 1;
            weakSelf.openModel.showDeepPrcCount = count;
            
            [weakSelf.contentTV reloadData];
            [weakSelf.view endEditing:YES];
        };
        
        return cell;
    }else{
        IXSymbolDeepCell *cell = [IXOpenCellM configDeepPrcCellWithTableView:tableView
                                                                   WithIndex:indexPath];
        NSInteger tag = indexPath.row - 1;
        cell.tag = tag;
        
        if ( _tradeModel.quoteModel.BuyPrc.count > tag) {
            [cell setBuyPrc:[self formatterPrice:_tradeModel.quoteModel.BuyPrc[tag]]
                     BuyVol:[self formatterPrice:_tradeModel.quoteModel.BuyVol[tag]]
                    SellPrc:[self formatterPrice:_tradeModel.quoteModel.SellPrc[tag]]
                    SellVol:[self formatterPrice:_tradeModel.quoteModel.SellVol[tag]]];
        }
        
        return cell;
    }
}

- (UITableViewCell *)sectionSecond:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    weakself;
    switch ( indexPath.row ) {
        case 0:{
            IXOpenTypeCell *cell = [IXOpenCellM configOpenTypeCellWithTableView:tableView
                                                                      WithIndex:indexPath];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);

            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];

            cell.kind = self.tradeModel.requestType;
            cell.expire = self.tradeModel.requestExpire;
            
            cell.tradeCon = ^(TRADE_TYPE type,UIView *view) {
                if (type == Trade_kind ) {
                    [weakSelf chooseTradeType:view];
                } else {
                    [weakSelf chooseExpireType:view];
                }
            };
            return cell;
        }
            break;
        case 1:{
            IXOpenDirCell *cell = [IXOpenCellM configOpenDirCellWithTableView:tableView
                                                                      WithIndex:indexPath];
            cell.dk_backgroundColorPicker = DKColorWithRGBs( 0xfafcfe, 0x242a36);

            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.direction = _tradeModel.direction;
            cell.tradeState = _tradeState;
//            if (_tradeState == IXSymbolTradeStateNormal) {
            cell.userInteractionEnabled = YES;
            cell.orderDirBlock = ^(item_order_edirection dir) {
                [weakSelf updateDirection:dir];
            };
//            } else {
//                cell.userInteractionEnabled = NO;
//            }
            return cell;
        }
            break;
        case 2:{
            
            if ( !_tradeModel.requestType || [_tradeModel.requestType isEqualToString:MARKETORDER] ) {
                IXOpenEptPrcCell *cell = [IXOpenCellM configOpenEptPrcCellWithTableView:tableView
                                                                                WithIndex:indexPath];
                cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);

                [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
                cell.title = LocalizedString(@"预计成交价");
                if ( _openModel.dealPrice && !isnan([_openModel.dealPrice floatValue]) ) {
                    cell.expectPrice = [_openModel.dealPrice stringValue];
                }
                return cell;
            }else{
                IXOpenPrcCell *cell = [IXOpenCellM configOpenPrcCellWithTableView:tableView
                                                                          WithIndex:indexPath];
                cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);

                UIButton *stepBtn = (UIButton *)[cell.contentView viewWithTag:DASHBORADBTNTAG];
                [stepBtn dk_setImage:DKImageNames(@"openRoot_stepVol", @"openRoot_stepVol_D")
                            forState:UIControlStateNormal];
                [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];

                cell.title = LocalizedString(@"价格");
                cell.inputExplain = _openModel.priceRangeStr;
                CGRect frame = cell.inputTF.frame;
                frame.origin.y = 5;
                cell.inputTF.frame = frame;

                cell.inputTF.delegate = self;
                cell.inputTF.tag = ROWNAMEPRICE;
                
                if(_tradeModel.requestPrice) {
                    cell.inputContent = [NSString formatterPrice:[_tradeModel.requestPrice stringValue]
                                                      WithDigits:_tradeModel.symbolModel.digits];
                }
                
                cell.addBlock = ^{
                    [weakSelf updateTakeprofitWithTag:ROWNAMEPRICE];
                };
                
                cell.substractBlock = ^{
                    [weakSelf updateStoplossWithTag:ROWNAMEPRICE];
                };
                
                __block IXOpenPrcCell *blockCell = cell;
                cell.stepBlock = ^{
                    weakSelf.tradeModel.chooseType = Choose_PriceType;
                    [weakSelf showMenu:stepBtn WithItem:weakSelf.prcArr];
                };
                
                cell.inputTF.editBlock = ^{
                    [weakSelf dealValueWithTag:ROWNAMEPRICE WithValue:blockCell.inputTF.text];
                };
                
                return cell;
            }
            
        }
            break;
        default :{
            IXOpenPrcCell *cell = [IXOpenCellM configOpenPrcCellWithTableView:tableView
                                                                      WithIndex:indexPath];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            
            UIButton *stepBtn = (UIButton *)[cell.contentView viewWithTag:DASHBORADBTNTAG];
            [stepBtn dk_setImage:DKImageNames(@"openRoot_stepPrc", @"openRoot_stepPrc_D")
                        forState:UIControlStateNormal];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];

            if ( _isVolNum ) {
                cell.title = LocalizedString(@"数量");
            }else{
                cell.title = LocalizedString(@"手数");
            }
         
            CGRect frame = cell.inputTF.frame;
            if ( _tradeModel.symbolModel.unitLanName && [_tradeModel.symbolModel.unitLanName length] ) {
                frame.origin.y = 5;
                cell.inputExplain = [NSString stringWithFormat:@"(%@)",
                                     [IXDataProcessTools showCurrentUnitLanName:_tradeModel.symbolModel.unitLanName]];
            }else{
                frame.origin.y = 7;
                cell.inputExplain = @"";
            }
            cell.inputTF.frame = frame;

            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMEVOLUME;
            
            if( _tradeModel.showVolume && !isnan([_tradeModel.showVolume floatValue]) ){
                if (!_isVolNum && _tradeModel.symbolModel.contractSizeNew) {
                    cell.inputContent = [NSString thousandFormate:[NSString formatterPrice:[_tradeModel.showVolume stringValue] WithDigits:2] withDigits:2];
                }else{
                    cell.inputContent = [NSString thousandFormate:[NSString formatterPrice:[_tradeModel.showVolume stringValue] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits];
                }
            }
            
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMEVOLUME];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMEVOLUME];
            };
            
            __block IXOpenPrcCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMEVOLUME WithValue:blockCell.inputTF.text];
            };
            
            cell.stepBlock = ^{
                [weakSelf.contentTV endEditing:YES];
                weakSelf.tradeModel.chooseType = Choose_VolumeType;
                [weakSelf showMenu:stepBtn WithItem:weakSelf.showVolArr];
            };
            
            return cell;
        }
            break;
    }
}

- (UITableViewCell *)sectionThird:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    weakself;
    switch ( indexPath.row ) {
        case 0:{
            IXOpenShowSLCell *cell = [IXOpenCellM configShowSLCellWithTableView:tableView
                                                                        WithIndex:indexPath];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];

            cell.openSwi = ( _openModel.showSLCount == 2 );
            
            cell.slBlock = ^{
                NSInteger count = weakSelf.openModel.showSLCount;
                count = ( count == 2 ) ? 0 : 2;
                
                weakSelf.openModel.showSLCount = count;
                [weakSelf.contentTV endEditing:YES];
                [weakSelf.contentTV reloadData];
             };
            return cell;
        }
            break;
        case 1:{
            IXOpenSLCell *cell = [IXOpenCellM configOpenSLCellWithTableView:tableView
                                                                    WithIndex:indexPath];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];

            cell.title = LocalizedString(@"止盈");
            cell.inputExplain = _openModel.profitRangeStr;
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMEPROFIT;
            
            if(_tradeModel.takeprofit && !isnan([_tradeModel.takeprofit doubleValue])){
                cell.inputContent = [NSString formatterPrice:[_tradeModel.takeprofit stringValue]
                                                  WithDigits:_tradeModel.symbolModel.digits];
            }else{
                cell.inputContent = @"";
            }
            
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMEPROFIT];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMEPROFIT];
            };
            
            cell.eraseBlock = ^{
                [weakSelf updateEraseWithTag:ROWNAMEPROFIT];
            };
            
            __block IXOpenSLCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMEPROFIT WithValue:blockCell.inputTF.text];
            };
            return cell;
        }
            break;
        default:{
            IXOpenSLCell *cell = [IXOpenCellM configOpenSLCellWithTableView:tableView
                                                                    WithIndex:indexPath];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);

            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];

            cell.title = LocalizedString(@"止损");
            cell.inputExplain = _openModel.stopRangeStr;
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMELOSS;

            if(_tradeModel.stoploss && !isnan([_tradeModel.stoploss doubleValue])){
                cell.inputContent = [NSString formatterPrice:[_tradeModel.stoploss stringValue]
                                                  WithDigits:_tradeModel.symbolModel.digits];
            }else{
                cell.inputContent = @"";
            }
            
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMELOSS];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMELOSS];
            };
            
            cell.eraseBlock = ^{
                [weakSelf updateEraseWithTag:ROWNAMELOSS];
            };

            __block IXOpenSLCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMELOSS WithValue:blockCell.inputTF.text];
            };
            
            return cell;
        }
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section ) {
        case 0:{
            return [self sectionFirst:tableView WithIndexPath:indexPath];
        }
            break;
        case 1:{
            return [self sectionSecond:tableView WithIndexPath:indexPath];
        }
        default:{
            return [self sectionThird:tableView WithIndexPath:indexPath];
        }
            break;
    }
}


#pragma mark load view module
- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        CGRect frame = self.view.bounds;
        frame.size.height = kScreenHeight - 68 - kNavbarHeight - kBtomMargin;
        _contentTV = [[IXTouchTableV alloc] initWithFrame:frame];
        [self.view addSubview:_contentTV];
        _contentTV.showsVerticalScrollIndicator = NO;
        _contentTV.dk_backgroundColorPicker = DKTableColor;

        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_contentTV registerClass:[IXDetailSymbolCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        
        [_contentTV registerClass:[IXSymbolDeepCell class]
           forCellReuseIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
        
        [_contentTV registerClass:[IXOpenShowPrcCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenShowPrcCell class])];
        
        [_contentTV registerClass:[IXOpenTypeCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenTypeCell class])];
        
        [_contentTV registerClass:[IXOpenDirCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenDirCell class])];
        
        [_contentTV registerClass:[IXOpenPrcCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenPrcCell class])];
        
        [_contentTV registerClass:[IXOpenEptPrcCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenEptPrcCell class])];
        
        [_contentTV registerClass:[IXOpenShowSLCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenShowSLCell class])];
        
        [_contentTV registerClass:[IXOpenSLCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenSLCell class])];
        
    }
    return _contentTV;
}

- (IXOpenTipV *)tipView
{
    if ( !_tipView ) {
        _tipView = [[IXOpenTipV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 40)];
    }
    return _tipView;
}

- (IXTradeMarginV *)marginView
{
    if (!_marginView) {
        _marginView = [[IXTradeMarginV alloc] initWithFrame:CGRectMake( 0,
                                                                       kScreenHeight - 68 - kNavbarHeight - kBtomMargin,
                                                                       kScreenWidth,
                                                                       68 + kBtomMargin)];
        weakself;
        _marginView.addOrder = ^(){
            [weakSelf.view endEditing:YES];
            
            NSString *checkOrderInfo = [IXOpenCalM checkResult:weakSelf];
            if ( [checkOrderInfo length] == 0 ) {
                [IXOpenCalM removeTip:weakSelf.tipView];
                
                if (!weakSelf.openModel.showSLCount) {
                    weakSelf.tradeModel.takeprofit = [NSDecimalNumber decimalNumberWithString:@"0"];
                    weakSelf.tradeModel.stoploss = [NSDecimalNumber decimalNumberWithString:@"0"];
                }
                [weakSelf submitOrderInfo];
                
            }else{
                [IXOpenCalM showTip:checkOrderInfo WithView:weakSelf.tipView InView:weakSelf.view];
            }
        };
    }
    return _marginView;
}

- (IXTradeMarketModel *)tradeModel
{
    if (!_tradeModel) {
        _tradeModel = [[IXTradeMarketModel alloc] init];
    }
    return _tradeModel;
}

- (IXOpenModel *)openModel
{
    if (!_openModel) {
        weakself;
        _openModel = [[IXOpenModel alloc] initWithSymbolModel:_tradeModel.symbolModel
                                               WithPriceRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetPriceRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               } WithProfitRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetProfitRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               } WithLossRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetStopRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               }];
    }
    return _openModel;
}

- (void)chooseTradeType:(UIView *)view
{
    self.tradeModel.chooseType = Choose_TradeType;
    [self showMenu:view WithItem:self.tradeModel.tradeTypeArr];
}

- (void)chooseExpireType:(UIView *)view
{
    self.tradeModel.chooseType = Choose_ExpireType;
    [self showMenu:view WithItem:self.tradeModel.tradeExpireArr];
}

- (void)updatePriceRange
{
    [self.openModel caculateRangePrice];
    [self.openModel caculateRangeStopLoss];
    [self.openModel caculateRangeTakeProfit];
}

//刷新请求价
- (void)updateRequstPrice
{
    if (![_tradeModel.requestType isEqualToString:MARKETORDER]) {
        if ([_tradeModel.requestType isEqualToString:LIMITORDER]) {
            if (_tradeModel.direction == item_order_edirection_DirectionBuy) {
                self.tradeModel.requestPrice = self.openModel.maxRequestPrice;
            }else{
                self.tradeModel.requestPrice = self.openModel.minRequestPrice;
            }
        }else if ([_tradeModel.requestType isEqualToString:STOPORDER]){
            if (_tradeModel.direction == item_order_edirection_DirectionBuy) {
                self.tradeModel.requestPrice = self.openModel.minRequestPrice;
            }else{
                self.tradeModel.requestPrice = self.openModel.maxRequestPrice;
            }
        }
    }
    
    _openModel.requestPrice = self.tradeModel.requestPrice;
    [self updatePrcCell];
}

- (void)updatePrcCell
{
    if (![_tradeModel.requestType isEqualToString:MARKETORDER]) {
        UITableViewCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
        if([cell isKindOfClass:[IXOpenPrcCell class]]){
            if (_tradeModel.requestPrice) {
                [(IXOpenPrcCell *)cell setInputContent:[NSString formatterPrice:[_tradeModel.requestPrice stringValue]
                                                                     WithDigits:_tradeModel.symbolModel.digits]];
            }
        }
    }
}

- (void)updateDirection:(item_order_edirection)direction
{
    self.tradeModel.direction = direction;
    self.openModel.orderDir = direction;
  
    //根据方向刷新止盈止损，保证金和佣金,请求价
    [self resetEptPrice];
    [self resetMarginAssetWithVolume:[_tradeModel.requestVolume doubleValue]];
}

#pragma mark 点击按钮更新数据
- (void)updateStoplossWithTag:(ROWNAME)tag
{
    [IXOpenLogicM updateStoplossWithTag:tag WithRoot:self];
    if ( tag == ROWNAMEPRICE ) {
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }else if ( tag == ROWNAMEVOLUME ){
        [self updateShowVol];
        [self resetDeepPrice];
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }
  
    [IXOpenCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)updateEraseWithTag:(ROWNAME)tag
{
    if(tag == ROWNAMEPROFIT){
        _tradeModel.takeprofit = nil;
    }else if (tag == ROWNAMELOSS){
        _tradeModel.stoploss = nil;
    }
    [IXOpenCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)updateShowVol
{
    if ( !_isVolNum &&  _tradeModel.symbolModel.contractSizeNew ) {
        self.tradeModel.showVolume = [self.tradeModel.requestVolume decimalNumberByDividingBy:
                                      [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",
                                                                                self.tradeModel.symbolModel.contractSizeNew]]
                                                                                 withBehavior:[IXRateCaculate handlerPriceWithDigit:2]];
    }else{
        self.tradeModel.showVolume = [NSDecimalNumber decimalNumberWithDecimal:
                                      [self.tradeModel.requestVolume decimalValue]];
    }
}

- (void)updateTakeprofitWithTag:(ROWNAME)tag
{
    [IXOpenLogicM updateTakeprofitWithTag:tag WithRoot:self];
    if ( tag == ROWNAMEPRICE ) {
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }else if ( tag == ROWNAMEVOLUME ){
        [self updateShowVol];
        [self resetDeepPrice];
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }
    
    [IXOpenCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)reloadContentWithTag:(ROWNAME)tag
{
    NSIndexPath *indexPath;
 
    switch ( tag) {
        case ROWNAMEPRICE:{
            indexPath = [NSIndexPath indexPathForRow:2 inSection:1];
        }
            break;
        case ROWNAMEVOLUME:{
            indexPath = [NSIndexPath indexPathForRow:3 inSection:1];
        }
            break;
        case ROWNAMEPROFIT:{
            indexPath = [NSIndexPath indexPathForRow:1 inSection:2];
        }
            break;
        default:{
            indexPath = [NSIndexPath indexPathForRow:2 inSection:2];
        }
            break;
    }
    
    [self.contentTV reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)dealValueWithTag:(ROWNAME)tag WithValue:(NSString *)value
{
    switch (tag) {
        case ROWNAMEPRICE:{     //价格
            _tradeModel.requestPrice = [NSDecimalNumber decimalNumberWithString:value];
            _openModel.dealPrice = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        case ROWNAMEVOLUME:{    //手数
            value = [value stringByReplacingOccurrencesOfString:@"," withString:@""];
            if ( !_isVolNum && _tradeModel.symbolModel.contractSizeNew) {
                _tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:2]];
                _tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                             [NSString stringWithFormat:@"%.2f",[value doubleValue] * _tradeModel.symbolModel.contractSizeNew]];
            }else{
                _tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:_tradeModel.symbolModel.volDigits]];
                _tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:_tradeModel.symbolModel.volDigits]];
            }
            
            [self resetMarginAssetWithVolume:[_tradeModel.requestVolume doubleValue]];
            [self resetDeepPrice];
        }
            break;
        case ROWNAMEPROFIT:{    //止赢
            _tradeModel.takeprofit = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        case ROWNAMELOSS:{      //止损
            _tradeModel.stoploss = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        default:
            break;
    }
   
    [IXOpenCalM updateTipInfoWithView:self.tipView WithRoot:self];
}

- (void)submitOrderInfo
{
    [self resetSub];
    
    IXOpenResultVC *result = [[IXOpenResultVC alloc] init];
    result.model = _tradeModel;
    result.openModel = _openModel;
    proto_order_add *proto = [_tradeModel packageOrder];
    [[IXTCPRequest shareInstance] orderWithParam:proto];
    
    [self.navigationController pushViewController:result animated:YES];
}


- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price
                         WithDigits:self.tradeModel.symbolModel.digits];
}

#pragma mark 用户操作触发的UI刷新
- (void)resetPriceRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    self.openModel.priceRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    
    UITableViewCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
    if( [cell isKindOfClass:[IXOpenPrcCell class]] ){
        if (self.openModel.profitRangeStr) {
            [(IXOpenPrcCell *)cell setInputExplain:self.openModel.priceRangeStr];
        }
    }
}

- (void)resetProfitRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    self.openModel.profitRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    
    if ( _openModel.showSLCount > 0 ) {
        IXOpenPrcCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
        cell.inputExplain = _openModel.profitRangeStr;
    }
}

- (void)resetStopRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    _openModel.stopRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    
    if ( _openModel.showSLCount > 0 ) {
        IXOpenPrcCell *cell = [_contentTV cellForRowAtIndexPath:
                               [NSIndexPath indexPathForRow:2 inSection:2]];
        cell.inputExplain = _openModel.stopRangeStr;
    }
}

#pragma mark -
#pragma mark - 行情数据
- (void)needRefresh
{
    NSMutableArray *arr = [IXOpenQuoteM subscribeDynamicPrice:self];
    if ( arr ) {
        [self didResponseQuoteDistribute:arr cmd:CMD_QUOTE_PUB_DETAIL];
    }
}

- (NSMutableArray *)swapRelSym
{
    if ( !_swapRelSym ) {
        _swapRelSym = [NSMutableArray array];
        NSDictionary *value = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:
                               [NSString stringWithFormat:@"%llu",_tradeModel.symbolModel.id_p]];
        if ( value && [value isKindOfClass:[NSDictionary class]] ) {
            for ( NSString *key in value ) {
                NSDictionary *dic = [value objectForKey:key];
                if ( [dic objectForKey:SYMBOLID] ) {
                    [_swapRelSym addObject:[dic objectForKey:SYMBOLID]];
                }
            }
        }else if ( value && [value isKindOfClass:[NSNumber class]] ){
            [_swapRelSym addObject:value];
        }
    }
    return _swapRelSym;
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    
    if( cmd == CMD_QUOTE_PUB_DETAIL || cmd == CMD_QUOTE_SUB_DETAIL ){
        for ( IXQuoteM *model in arr ) {
            if (model.symbolId == _tradeModel.symbolModel.id_p) {
                _tradeModel.quoteModel = [model mutableCopy];
                _openModel.quoteModel = [model mutableCopy];
               
                if ( Choose_PriceType == _tradeModel.chooseType ) {
                    self.menu.menuItems = self.prcArr;
                }

                [self resetQuote];
                [self resetEptPrice];
                [self resetMarginAssetWithVolume:[_tradeModel.requestVolume doubleValue]];
            }else if ( [self.swapRelSym containsObject:@(model.symbolId)] ) {
                [self resetMarginAssetWithVolume:[_tradeModel.requestVolume doubleValue]];
            }
        }
    }
}

//刷新UI显示行情的cell
- (void)resetQuote
{
    for ( UITableViewCell *cell  in [_contentTV visibleCells] ) {
        if ( [cell isKindOfClass:[IXDetailSymbolCell class]] ) {
            [(IXDetailSymbolCell *)cell setQuoteModel:_tradeModel.quoteModel];
        }else if ( [cell isKindOfClass:[IXSymbolDeepCell class]] ){
            NSInteger tag = cell.tag;
            if ( _tradeModel.quoteModel.BuyPrc.count > tag ) {
                [(IXSymbolDeepCell *)cell setBuyPrc:[self formatterPrice:_tradeModel.quoteModel.BuyPrc[tag]]
                                             BuyVol:[self formatterPrice:_tradeModel.quoteModel.BuyVol[tag]]
                                            SellPrc:[self formatterPrice:_tradeModel.quoteModel.SellPrc[tag]]
                                            SellVol:[self formatterPrice:_tradeModel.quoteModel.SellVol[tag]]];
            }
        }
    }
}

//刷新价格范围
- (void)resetEptPrice
{
    if ( [_tradeModel.requestType isEqualToString:MARKETORDER] ) {
        [self resetDeepPrice];
    }else{
//        [self updateRequstPrice];
    }
    [IXOpenCalM updateTipInfoWithView:self.tipView WithRoot:self];
}

//刷新保证金和佣金,百万分之一
- (void)resetMarginAssetWithVolume:(double)volume
{
    NSArray *arr = [IXOpenCalM resetMarginAssetWithVolume:volume WithRoot:self];
    if (arr.count > 1) {
        _tradeModel.margin = [arr[0] doubleValue];
        _tradeModel.commission = [arr[1] doubleValue];
        
        [self.marginView setMarginValue:[IXDataProcessTools moneyFormatterComma:[arr[0] doubleValue] positiveNumberSign:YES]];
        [self.marginView setCommissionValue:[IXDataProcessTools moneyFormatterComma:[arr[1] doubleValue] positiveNumberSign:YES]];
    }
}

//刷新预计成交价
//市价单，预计成交价和手数和数量有关，根据深度价格做对应的运算
- (void)resetDeepPrice
{
    if ( [_tradeModel.requestType isEqualToString:MARKETORDER] ) {
        NSDecimalNumber *price = [IXRateCaculate caculateMayDealPriceWithVolume:[_tradeModel.requestVolume longValue]
                                                                        WithDir:_tradeModel.direction
                                                                      WithQuote:_tradeModel.quoteModel
                                                                     WithDigits:_tradeModel.symbolModel.digits];
        
        NSString *priceStr = [NSString formatterPrice:[price stringValue]
                                           WithDigits:_tradeModel.symbolModel.digits];
        _tradeModel.requestPrice = [NSDecimalNumber decimalNumberWithString:priceStr];
        _openModel.dealPrice =  [NSDecimalNumber decimalNumberWithString:priceStr];

        UITableViewCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
        if( [cell isKindOfClass:[IXOpenEptPrcCell class]] ){
            if ( self.openModel.dealPrice ) {
                [(IXOpenEptPrcCell *)cell setExpectPrice:
                    [NSString formatterPrice:[_openModel.dealPrice stringValue]
                                  WithDigits:_tradeModel.symbolModel.digits]];
            }
        }
     }
}

- (NSMutableArray *)showVolArr
{
    if ( !_showVolArr ) {
        _showVolArr = [[NSMutableArray array] mutableCopy];
        _volArr = [[NSMutableArray array] mutableCopy];
    }else{
        [_showVolArr removeAllObjects];
        [_volArr removeAllObjects];
    }
    
    double min = _tradeModel.symbolModel.volumesMin;
    double max = _tradeModel.symbolModel.volumesMax;
    
    if ( [_tradeModel.requestVolume doubleValue] != min ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",min]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew ) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",min] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else{
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",min/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }

    if ( max > min * 10 && [_tradeModel.requestVolume doubleValue] != (min * 10) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 10)]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 10)] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 10)/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    
    if ( max > min * 100  && [_tradeModel.requestVolume doubleValue] != (min * 100) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 100)]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 100)] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 100)/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    
    if ( max > min * 500  && [_tradeModel.requestVolume doubleValue] != (min * 500) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 500)]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 500)] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 500)/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    
    if ( [_tradeModel.requestVolume doubleValue] != max ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",max]];
        if ( _isVolNum || 0 == _tradeModel.symbolModel.contractSizeNew ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",max] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",max/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    
    return _showVolArr;
}

- (NSMutableArray *)prcArr
{
    if ( !_prcArr ) {
        _prcArr = [[NSMutableArray array] mutableCopy];
    }
    [_prcArr removeAllObjects];
    
    NSDecimalNumber *max = _openModel.maxRequestPrice;
    NSDecimalNumber *min = _openModel.minRequestPrice;
    
    NSInteger digit = _tradeModel.symbolModel.digits;
    [_prcArr addObject:[NSString formatterPrice:[min stringValue]
                                     WithDigits:digit]];
    
    NSDecimalNumber *step = [[max decimalNumberBySubtracting:min] decimalNumberByDividingBy:
                             [NSDecimalNumber decimalNumberWithString:@"3"]];
    [_prcArr addObject:[NSString formatterPrice:[[min decimalNumberByAdding:step] stringValue]
                                     WithDigits:digit]];
    
    [_prcArr addObject:[NSString formatterPrice:[[min decimalNumberByAdding:
                                                  [step decimalNumberByMultiplyingBy:
                                                   [NSDecimalNumber decimalNumberWithString:@"2"]]] stringValue]
                                     WithDigits:digit]];
    
    [_prcArr addObject:[NSString formatterPrice:[max stringValue]
                                     WithDigits:digit]];
    return _prcArr;
}

- (IXPullDownMenu *)menu
{
    if (!_menu) {
        _menu = [IXPullDownMenu menuWithMenuItems:@[]
                                        rowHeight:30
                                            width:100];
        
        weakself;
        _menu.itemClicked = ^(NSInteger index ,NSString * title) {
            [weakSelf menuItemClicked:index title:title];
        };
        _menu.endEditBlock = ^{
            IXOpenTypeCell *cell = [weakSelf.contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            if ( weakSelf.tradeModel.chooseType == Choose_TradeType ) {
                [cell resetKindArrow];
            }else if ( weakSelf.tradeModel.chooseType == Choose_ExpireType ){
                [cell resetExpireArrow];
            }
        };
    }
    
    return _menu;
}

#pragma mark -
#pragma mark - menu
- (void)showMenu:(UIView *)view WithItem:(NSArray *)arr
{ 
    [self.view endEditing:YES];
    
    CGFloat offsetY = -4;
    if ( self.tradeModel.chooseType == Choose_ExpireType || self.tradeModel.chooseType == Choose_TradeType ) {
        offsetY = 22;
    }
    [self.menu showMenuFrom:view items:arr offsetY:offsetY];
}

- (void)menuItemClicked:(NSInteger)index title:(NSString *)title
{
    switch ( _tradeModel.chooseType ) {
        case Choose_PriceType:{
            //更新价格
            IXOpenPrcCell *prc = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
            if ( [prc isKindOfClass:[IXOpenPrcCell class]] ) {
                prc.inputTF.text = _prcArr[index];
                [self dealValueWithTag:ROWNAMEPRICE WithValue:prc.inputTF.text];
            }
        }
            break;
        case Choose_VolumeType:{
            //更新手数
            IXOpenPrcCell *vol = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1]];
            if ( [vol isKindOfClass:[IXOpenPrcCell class]] ) {
                vol.inputTF.text = _showVolArr[index];
                [self dealValueWithTag:ROWNAMEVOLUME WithValue:_showVolArr[index]];
            }
        }
            break;
        case Choose_TradeType:{
            if ( index >= 0 && index <= self.tradeModel.tradeTypeArr.count ) {
                self.tradeModel.requestType = self.tradeModel.tradeTypeArr[index];
                [self.openModel setOrderType:self.tradeModel.requestType];
                [self resetEptPrice];
                [self.contentTV reloadData];
                [self updatePrcCell];
            }
        }
            break;
        default:{
            if ( index >= 0 &&  index <= self.tradeModel.tradeExpireArr.count ) {
                self.tradeModel.requestExpire = self.tradeModel.tradeExpireArr[index];
                [self.contentTV reloadData];
            }
        }
            break;
    }
}


@end
