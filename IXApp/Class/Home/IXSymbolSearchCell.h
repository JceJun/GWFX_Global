//
//  IXSymbolSearchCell.h
//  IXApp
//
//  Created by Evn on 16/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXSymbolM.h"

@protocol IXSymbolSearchCellDelegate <NSObject>

- (void)addOrDeleteSymbolSub:(UIButton *)btn;

@end

@interface IXSymbolSearchCell : UITableViewCell

@property (nonatomic, strong)UILabel *nameLbl;
@property (nonatomic,strong)UIView *topView;
@property (nonatomic, strong)UILabel *labLbl1;//标签
@property (nonatomic, strong)UILabel *labLbl2;
@property (nonatomic, strong)UILabel *contentLbl;//内容
@property (nonatomic, strong)UIButton *addBtn;
@property (nonatomic, strong)UIButton *addBgBtn;
@property (nonatomic, strong)UIView *lineView;
@property (nonatomic, assign)id <IXSymbolSearchCellDelegate> delegate;

- (void)reloadUIWithItem:(IXSymbolM *)model;

@end
