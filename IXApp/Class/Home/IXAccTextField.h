//
//  IXAccTextField.h
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^endEdit)();

@interface IXAccTextField : UITextField

@property (nonatomic, copy) endEdit editBlock;

@end
