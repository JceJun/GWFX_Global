//
//  IXOpenShowPrcCell.h
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^resetDeepPrc)();

@interface IXOpenShowPrcCell : UITableViewCell

@property (nonatomic, copy) resetDeepPrc deepPrcBlock;

- (void)refreashUIWithArrowDir:(BOOL)arrowDown;

@end
