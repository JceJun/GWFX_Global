//
//  IXTradeSymbolV.h
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^showDetailDeepPrice)(BOOL show);

@interface IXTradeSymbolV : UIView

@property (nonatomic, assign) double marketId;
@property (nonatomic, assign) double nLastClosePrice;

@property (nonatomic, copy) showDetailDeepPrice deepPrice;
@property (nonatomic, strong) IXQuoteM *quoteModel;
@property (nonatomic, strong) IXSymbolM *symbolModel;

@end
