//
//  IXSymbolDetailBtomV.h
//  IXApp
//
//  Created by Seven on 2017/12/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXSymbolDetailBtomV : UIView

@property (nonatomic, strong) UIButton  * selBtn;
@property (nonatomic, strong) UIButton  * buyBtn;

/**
 获取空间size

 @return size
 */
+ (CGSize)targetSize;

@end
