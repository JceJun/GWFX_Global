//
//  IXTradeMarginV.h
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^submitOrder)();

@interface IXTradeMarginV : UIView

@property (nonatomic, strong) UIButton *sellBtn;
@property (nonatomic, copy) submitOrder addOrder;

- (void)setMarginValue:(NSString *)margin;
- (void)setCommissionValue:(NSString *)commission;
- (void)setCommisionLblHidden:(BOOL)hidden;
@end
