//
//  IXSmybolDetailMgr.h
//  IXApp
//
//  Created by Larry on 2018/5/16.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXSingleton.h"
#import "IXTradeMarketModel.h"

@interface IXSmybolDetailMgr : NSObject
@property(nonatomic,copy)NSString *symbolId;
@property(nonatomic,strong) IXTradeMarketModel *tradeModel;
@property (nonatomic, assign) IXSymbolTradeState tradeState;
@property (nonatomic, copy) NSString *totalProfit;
@property (nonatomic, strong) NSMutableArray *symbolPosArr;
@property (nonatomic, assign) BOOL shouldHKVCResponse;

//是否自选
@property (nonatomic, assign) BOOL isSubSymbol;

//是否改变自选状态
@property (nonatomic, assign) BOOL isChangeSubStatus;

@property (nonatomic, assign) double nLastClosePrice;

@property (nonatomic, assign) BOOL refreashFeeQuote;

+ (instancetype)sharedIXSmybolDetailMgr;
@end
