//
//  IXKLineCell.m
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXKLineCell.h"
#import "IXChartTitleV.h"
@interface IXKLineCell ()
{
    IXChartTitleV *titleV;
    __weak UIView *tsView;
    __weak UIView *csView;
}

@end

@implementation IXKLineCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        titleV = [[IXChartTitleV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 30)];
        [self.contentView insertSubview:titleV atIndex:10];
        self.backgroundColor = [UIColor clearColor];

        weakself;
        titleV.btnClick = ^(NSInteger tag){
            if (weakSelf.btnClick) {
                weakSelf.btnClick((IXKLineType)tag);
            }
        };
    }
    return self;
}

- (void)showDisplayView:(UIView *)view
                   type:(IXKLineType)type
{
    if (type == IXKLineTypeTline) {
        tsView = view;
        if (!tsView.superview) {
            [self.contentView insertSubview:tsView belowSubview:titleV];
        }
        [UIView animateWithDuration:.2 animations:^{
            tsView.alpha = 1;
            csView.alpha = 0;
        }];
    } else {
        csView = view;
        if (!csView.superview) {
            [self.contentView insertSubview:csView belowSubview:titleV];
        }
        [UIView animateWithDuration:.2 animations:^{
            csView.alpha = 1;
            tsView.alpha = 0;
        }];
    }
    titleV.selectedTag = type;
}

@end

