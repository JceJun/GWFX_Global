//
//  IXQuoteContentVC.m
//  IXApp
//
//  Created by bob on 16/11/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

typedef void(^currentPage)(NSInteger page);
typedef void(^changeTitle)(NSInteger page);

@interface IXQuoteContentVC : UIViewController

/**
 *  控制器数组
 */
@property (nonatomic, strong) NSMutableArray *vcAry;
@property (nonatomic, copy) changeTitle titleBlock;
@property(nonatomic,copy) void(^depositBlock)(void);
@property(nonatomic,copy) void(^needregistBlock)(BOOL);

/**
 *  初始化控制器
 *
 *  @param vcAry 行情控制器数组
 *  @param page  选中某行回调
 *
 *  @return 初始化值
 */
- (id)initWithViewControllerAry:(NSArray *)vcAry ChoosePage:(currentPage)page;

/**
 *  设置当前页
 *
 *  @param page 页数
 */
- (void)setOffsetPage:(NSInteger)page;

//刷新行情
- (void)resetQuotePrice;

//取消行情
- (void)cancelQuotePrice;

//加载分类
- (void)reloadCataList:(NSArray *)vcAry
               WithRow:(NSInteger)row;

//定位到特定分类
- (void)reloadCataListWithRow:(NSInteger)row;

- (void)reloadData;

@end
