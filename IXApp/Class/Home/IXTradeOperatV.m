//
//  IXTradeOperatV.m
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeOperatV.h"
#import "IXOpenMetricV.h"
#import "UIImageView+SepLine.h"
#import "PHTextView.h"

@interface IXTradeOperatV ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *priceRangeLbl;

@property (nonatomic, strong) UIView *marketOrder;
@property (nonatomic, strong) UIView *otherOrder;

@property (nonatomic, assign) float resetOrginY;
@property (nonatomic, strong) NSString *stopLevel;

@end

@implementation IXTradeOperatV

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(validTextContent)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:nil];
        
        [self addSubview:self.titleLbl];
        [self addSubview:self.otherOrder];
        
        [self addSubview:self.marketOrder];
        [self.marketOrder setHidden:YES];
        
        [self addSepLine];
    }
    return self;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)removeFromSuperview
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)validTextContent
{
    //兼容控件，设置了digit的才判断
    if ( _digit > 0 ) {
        NSString *content = _contentTF.text;
        NSRange range = [content rangeOfString:@"."];
        if ( range.location != NSNotFound ) {
            if ( content.length - range.location  - 1 > _digit ) {
                _contentTF.text = [_contentTF.text substringToIndex:(range.location + _digit + 1)];
            }
        }
    }
}

- (void)addSepLine
{
    UIImageView *bottomSep = [UIImageView addSepImageWithFrame:CGRectMake( 0, 55, kScreenWidth, 1)
                                                     WithColor:MarketCellSepColor
                                                     WithAlpha:1];
    [self addSubview:bottomSep];
}

- (void)setTextFieldInputAccessoryView
{
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    [topView setBarStyle:UIBarStyleDefault];
    topView.backgroundColor = MarketCellSepColor;
    
    UIBarButtonItem * spaceBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:self
                                                                              action:nil];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:PF_MEDI(13)];
    [cancelBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(15, 9, 60, 25);
    [cancelBtn addTarget:self action:@selector(cancelKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBtnItem = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneBtn setTitle:LocalizedString(@"完成") forState:UIControlStateNormal];
    [doneBtn.titleLabel setFont:PF_MEDI(13)];
    [doneBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    doneBtn.frame = CGRectMake(kScreenWidth - 90, 9, 80, 25);
    [doneBtn addTarget:self action:@selector(dealKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneBtnItem = [[UIBarButtonItem alloc]initWithCustomView:doneBtn];
    NSArray * buttonsArray = [NSArray arrayWithObjects:cancelBtnItem,spaceBtn,doneBtnItem,nil];
    [topView setItems:buttonsArray];
    
    [_contentTF setInputAccessoryView:topView];
    [_contentTF setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_contentTF setAutocapitalizationType:UITextAutocapitalizationTypeNone];
}

- (void)dealKeyboardHidden
{
    [_contentTF resignFirstResponder];
}

- (void)cancelKeyboardHidden{
    [_contentTF resignFirstResponder];
}

- (void)responseToSubstract
{
    if(self.clickBlock){
        self.clickBlock(Click_Subtract,self.tag);
    }
}

- (void)responseToAdd
{
    if(self.clickBlock){
        self.clickBlock(Click_Add,self.tag);
    }
}

- (void)setTitleContent:(NSString *)content
{
    _titleLbl.text = content;
}

- (void)setDealPrice:(NSString *)price
{
    if ( !price || ![price isKindOfClass:[NSString class]] ) {
        return;
    }
    for ( UILabel *dealPrice in [_marketOrder subviews] ) {
        if ( [dealPrice isKindOfClass:[UILabel class]] ) {
            dealPrice.text = price;
            break;
        }
    }
}

- (void)updateWithLeftRange:(NSString *)left
             WithRightRange:(NSString *)right
{
    _priceRangeLbl.text = [NSString stringWithFormat:@"(%@-%@)",left,right];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_textContent && textField.text.length != 0 ) {
        _textContent(textField.text);
    }else if ( _textContent ){
        _textContent(@"");
    }
}

- (void)setStopLevel:(NSString *)stopLevel
{
    _stopLevel = stopLevel;
    if (_otherOrder) {
        for (IXOpenMetricV *view in _otherOrder.subviews) {
            if ([view isKindOfClass:[IXOpenMetricV class]]) {
                [view setStep:_stopLevel];
            }
        }
    }
}

- (void)setUnitName:(NSString *)unitName
{
    if ( unitName && [unitName isKindOfClass:[NSString class]] && [unitName length] ) {
        _priceRangeLbl.text = [NSString stringWithFormat:@"(%@)",unitName];
    }
}

- (void)updatePlaceOlderValue:(NSString *)value
{
    _contentTF.text = value;
}

- (void)setTitleColor:(UIColor *)color
{
    
    _titleLbl.textColor = color;
}

- (void)setTextFieldColor:(UIColor *)color isEnableEdit:(BOOL)enable
{
    _contentTF.textColor = color;
    _contentTF.enabled = enable;
}

- (void)updateUIWithOrderType:(NSString *)orderType
{
    if( [orderType isEqualToString:MARKETORDER] ){
        [_otherOrder setHidden:YES];
        [_marketOrder setHidden:NO];
        _titleLbl.text = LocalizedString(@"预计成交价");
    }else{
        [_otherOrder setHidden:NO];
        [_marketOrder setHidden:YES];
        _titleLbl.text = LocalizedString(@"价格");
    }
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 21, kScreenWidth - 15, 15)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x99abba
                                     dTextColor:0xff0000];
    }
    return _titleLbl;
}

- (UIView *)marketOrder
{
    if (!_marketOrder) {

        _marketOrder = [[UIView alloc] initWithFrame:CGRectMake( 125, 0, kScreenWidth - 183, 56)];
        _marketOrder.backgroundColor = [UIColor whiteColor];
        
        UILabel *dealPriceLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 19, kScreenWidth - 183, 18)
                                                   WithFont:RO_REGU(18)
                                                  WithAlign:NSTextAlignmentCenter
                                                 wTextColor:0x99abba
                                                 dTextColor:0xff0000];
        [_marketOrder addSubview:dealPriceLbl];
    }
    return _marketOrder;
}

- (UIView *)otherOrder
{
    if (!_otherOrder) {
        _otherOrder = [[UIView alloc] initWithFrame:CGRectMake( 70, 0, kScreenWidth - 70, 56)];
        _otherOrder.backgroundColor = [UIColor whiteColor];
        
        CGRect currentFrame = CGRectMake( 0, 0, 58, 56);
        IXOpenMetricV *substractBtn = [[IXOpenMetricV alloc] initWithFrame:currentFrame WithImgName:@"openRoot_btn_substract"];
        [_otherOrder addSubview:substractBtn];
        UITapGestureRecognizer *substractGes = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(responseToSubstract)];
        [substractBtn addGestureRecognizer:substractGes];
        
        currentFrame.origin.x = CGRectGetWidth(_otherOrder.frame) - 58;
        IXOpenMetricV *addBtn = [[IXOpenMetricV alloc] initWithFrame:currentFrame WithImgName:@"openRoot_btn_add"];
        [_otherOrder addSubview:addBtn];
        UITapGestureRecognizer *addGes = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(responseToAdd)];
        [addBtn addGestureRecognizer:addGes];

        currentFrame.origin.x = CGRectGetMaxX(substractBtn.frame);
        currentFrame.size.width = CGRectGetMinX(addBtn.frame) - CGRectGetMaxX(substractBtn.frame);
        
        currentFrame.origin.y = 5;
        currentFrame.size.height = 30;
        _contentTF = [[UITextField alloc] initWithFrame:currentFrame];
        [_otherOrder addSubview:_contentTF];
    
        _contentTF.borderStyle = UITextBorderStyleNone;
        _contentTF.textAlignment = NSTextAlignmentCenter;
        _contentTF.keyboardType = UIKeyboardTypeDecimalPad;
        _contentTF.font = RO_REGU(18);
        _contentTF.textColor = MarketSymbolNameColor;
        _contentTF.placeholder = LocalizedString(@"没有设置");
        _contentTF.delegate = self;
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSFontAttributeName] = PF_MEDI(15);
        dict[NSForegroundColorAttributeName] = MarketCellSepColor;

        NSAttributedString *attribute = [[NSAttributedString alloc] initWithString:_contentTF.placeholder
                                                                        attributes:dict];
        [_contentTF setAttributedPlaceholder:attribute];
        
        [self setTextFieldInputAccessoryView];
        
        currentFrame.origin.y = 35;
        currentFrame.size.height = 15;
        _priceRangeLbl = [IXUtils createLblWithFrame:currentFrame
                                            WithFont:RO_REGU(15)
                                           WithAlign:NSTextAlignmentCenter
                                          wTextColor:0x99abba
                                          dTextColor:0xff0000];
        [_otherOrder addSubview:_priceRangeLbl];
    }
    return _otherOrder;
}

@end
