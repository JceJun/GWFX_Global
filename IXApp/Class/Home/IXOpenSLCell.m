//
//  IXOpenSLCell.m
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenSLCell.h"

@interface IXOpenSLCell ()

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *inputExpLbl;

@property (nonatomic, strong) UIButton *eraseBtn;

@end

@implementation IXOpenSLCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        self.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x242a36);
        [self loadOpView];
        
        [self.contentView addSubview:self.inputTF];
    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    if ( title && title.length ) {
        self.titleLbl.text = title;
    }
}

- (void)setInputContent:(NSString *)inputContent
{
    if (inputContent.length) {
        self.inputTF.text = inputContent;
    }else{
        self.inputTF.text = @"";
    }
    [_eraseBtn setHidden:!inputContent.length];
}

- (void)setInputExplain:(NSString *)inputExplain
{
    if (inputExplain.length) {
        self.inputExpLbl.text = inputExplain;
    }
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 14, kScreenWidth - 15, 15)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x99abba
                                     dTextColor:0x99abba];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}


- (IXAccTextField *)inputTF
{
    if ( !_inputTF ) {
        
        CGFloat width = kScreenWidth - 222;
        _inputTF = [[IXAccTextField alloc] initWithFrame:CGRectMake( 119, 5, width, 30)];
    }
    return _inputTF;
}

- (UILabel *)inputExpLbl
{
    if ( !_inputExpLbl ) {
        CGFloat width = kScreenWidth - 222;
        _inputExpLbl = [IXUtils createLblWithFrame:CGRectMake( 119, 31, width, 9)
                                          WithFont:RO_REGU(9)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0x99abba
                                        dTextColor:0x99abba];
        
        [self.contentView addSubview:_inputExpLbl];
    }
    return _inputExpLbl;
}


- (void)setHiddenErase:(BOOL)hiddenErase
{
    _hiddenErase = hiddenErase;
    [_eraseBtn setHidden:_hiddenErase];
}

- (void)loadOpView
{
    UIButton *subBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:subBtn];
    
    subBtn.frame = CGRectMake( 70, 0, 30, 44);
    [subBtn dk_setImage:DKImageNames(@"openRoot_btn_substract", @"openRoot_btn_substract_dark")
               forState:UIControlStateNormal];
    [subBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 8, 0, 0)];
    [subBtn addTarget:self
               action:@selector(responseToSubstract)
     forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:addBtn];
    
    addBtn.frame = CGRectMake(kScreenWidth - 88, 0, 30, 44);
    [addBtn dk_setImage:DKImageNames(@"openRoot_btn_add", @"openRoot_btn_add_dark")
               forState:UIControlStateNormal];
    [addBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [addBtn addTarget:self
               action:@selector(responseToAdd)
     forControlEvents:UIControlEventTouchUpInside];
    
    if(!_eraseBtn){
        _eraseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_eraseBtn];
        _eraseBtn.frame = CGRectMake(kScreenWidth - 45 + 7, 0, 30, 44);
        [_eraseBtn dk_setImage:DKImageNames(@"erase", @"erase_D")
                      forState:UIControlStateNormal];
        _eraseBtn.hidden = YES;
        [_eraseBtn addTarget:self
                      action:@selector(responseToErase)
            forControlEvents:UIControlEventTouchUpInside];
    }
}


- (void)responseToAdd
{
    if (self.addBlock) {
        self.addBlock();
    }
}

- (void)responseToSubstract
{
    if(self.substractBlock){
        self.substractBlock();
    }
}

- (void)responseToErase
{
    if(self.eraseBlock){
        self.eraseBlock();
    }
}
@end
