//
//  IXOpenModel+CheckData.h
//  IXApp
//
//  Created by Bob on 2016/12/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOpenModel.h"

@interface IXOpenModel (CheckData)

//验证输入的手数是否合法
- (BOOL)validVolume:(NSDecimalNumber *)volume;

//验证输入的价格是否合法
- (BOOL)validPrice:(NSDecimalNumber *)price;

//验证输入的止盈是否合法
- (BOOL)validProfit:(NSDecimalNumber *)profit;

//验证输入的止损是否合法
- (BOOL)validLoss:(NSDecimalNumber *)loss;

//验证输入的持仓手数是否合法(特殊)
- (BOOL)validPositionVolume:(NSDecimalNumber *)volume;

@end
