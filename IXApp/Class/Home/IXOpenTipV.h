//
//  IXOpenTipV.h
//  IXApp
//
//  Created by Bob on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXOpenTipV : UIView

@property (nonatomic, strong) NSString *tipInfo;

@property (nonatomic, assign) BOOL isShow;

@property(nonatomic,copy) void(^textBlock)(NSString *text);

- (void)addEntranceArrow:(NSString *)msg;

+ (void)showInView:(UIView *)superView lText:(NSString *)lText rText:(NSString *)rText block:(void(^)(void))block;
+ (void)hideInView:(UIView *)superView;

@end
