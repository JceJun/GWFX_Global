//
//  IXSymSearchVC.m
//  IXApp
//
//  Created by Evn on 16/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSymSearchVC.h"
#import "IXSymbolSearchCell.h"
#import "IXDataProcessTools.h"
#import "IxItemSymbolSub.pbobjc.h"
#import "IXDataBase.h"
#import "IXDBGlobal.h"
#import "IXSymbolDetailVC.h"
#import "IXAlertVC.h"
#import "UINavigationController+FDFullscreenPopGesture.h"

typedef NS_ENUM(NSInteger,IXSearchOperateType) {
    IXSearchOperateTypeSearch,
    IXSearchOperateTypeHistory
};

@interface IXSymSearchVC ()
<
UITextFieldDelegate,
UITableViewDataSource,
UITableViewDelegate,
IXSymbolSearchCellDelegate,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong)UITextField *tField;
@property (nonatomic, strong)UIView *bgView;
@property (nonatomic, strong)UIView *browseView;
@property (nonatomic, strong)UIView *navBgView;
@property (nonatomic, strong)UITableView *tableV;
@property (nonatomic, strong)UIView *lineView;
@property (nonatomic, assign)IXSearchOperateType operType;
@property (nonatomic, strong)NSMutableArray *dataArr;//搜索数据
@property (nonatomic, strong)NSArray *sourceArr;
@property (nonatomic, assign)NSInteger currentIndex;//当前选择的产品索引

@end

@implementation IXSymSearchVC

- (id)init
{
    self = [super init];
    if ( self ) {

        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_ADD);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_DELETE);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_DELETE);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_interactivePopDisabled = YES;
    
    _dataArr = [[NSMutableArray alloc] init];
    _sourceArr = [[NSArray alloc] init];
    [self addSubView];
    [self addSearchBrowseData];
    [self updateNavBottomLine];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES];
    [self addNavigationView];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [_navBgView removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [self.navigationItem setHidesBackButton:NO];
}

#pragma mark 添加搜索浏览数据
- (void)addSearchBrowseData {
    
    _operType = IXSearchOperateTypeHistory;
    _sourceArr = [IXDBSearchBrowseMgr queryAllSearchBrowserHistoryInfoUserId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    _sourceArr = [self packageQuerySearchHistory:_sourceArr];
    _dataArr = [_sourceArr mutableCopy];
    [_tableV reloadData];
}

- (void)addSubView {
    
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                       kScreenWidth,
                                                       kScreenHeight - kNavbarHeight)];
    _bgView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
    [self.view addSubview:_bgView];
    
    [_bgView addSubview:self.tableV];
}

- (void)addNavigationView
{
    float cancelBtnWidth = 30;
    _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                          0,
                                                          kScreenWidth,
                                                          self.navigationController.navigationBar.frame.size.height)
                  ];
    _navBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    [self.navigationController.navigationBar addSubview:_navBgView];
    
    UIView *searchBgView = [[UIView alloc] initWithFrame:CGRectMake(15,7,
                                                                    kScreenWidth - (45 + 40),
                                                                    cancelBtnWidth)];
    searchBgView.layer.borderWidth = 1;
    searchBgView.layer.dk_borderColorPicker = DKColorWithRGBs(0xe2eaf2, 0x303b4d);
    searchBgView.layer.cornerRadius = 5;
    [_navBgView addSubview:searchBgView];
    
    UIImageView *iconImgV = [[UIImageView alloc] initWithFrame:CGRectMake(7,
                                                                          6.5,
                                                                          VIEW_H(searchBgView) - 2*6.5,
                                                                          VIEW_H(searchBgView) - 2*6.5)];
    iconImgV.dk_imagePicker = DKImageNames(@"symbolsearch_default", @"symbolsearch_default_D");
    [searchBgView addSubview:iconImgV];
    
    
    _tField = [[UITextField alloc] initWithFrame:CGRectMake(VIEW_X(iconImgV) + VIEW_W(iconImgV) + 9.5,7,      VIEW_W(searchBgView) - (VIEW_X(iconImgV) +VIEW_W(iconImgV) + 9.5*2),VIEW_H(searchBgView) - 14)];
    UIColor *pColor = nil;
    if ([IXUserInfoMgr shareInstance].isNightMode) {
        pColor = UIColorHexFromRGB(0x303b4d);
    } else {
        pColor = UIColorHexFromRGB(0xe2e9f1);
    }
    _tField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"股票名称/代码/全拼") attributes:@{NSForegroundColorAttributeName:pColor,NSFontAttributeName:ROBOT_FONT(13)}];
    _tField.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    _tField.delegate = self;
    [_tField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [searchBgView addSubview:_tField];
    [_tField becomeFirstResponder];
    
    CGFloat x = VIEW_X(searchBgView) + VIEW_W(searchBgView) + 10;
    UIButton *cancelBtn = [IXCustomView createButton:CGRectMake(x,
                                                                VIEW_Y(searchBgView),
                                                                50,
                                                                cancelBtnWidth)
                                               title:LocalizedString(@"取消")
                                                font:PF_MEDI(13)
                                           wTextColor:0x4c6072
                                          dTextColor:0x8395a4
                                       textAlignment:NSTextAlignmentCenter];
    [cancelBtn addTarget:self action:@selector(cancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_navBgView addSubview:cancelBtn];
}

- (UITableView *)tableV{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,
                                                                kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        _tableV.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kBtomMargin)];
        [_tableV  registerClass:[IXSymbolSearchCell class]
         forCellReuseIdentifier:NSStringFromClass([IXSymbolSearchCell class])];
        _tableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _tableV.tableFooterView = [[UIView alloc] initWithFrame:ktableFooterFrame];
    }
    return _tableV;
}

#pragma mark delegate && datasource method moduel
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 54.5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (_operType == IXSearchOperateTypeHistory) {
        return 33;
    } else {
        return 0;
    }
}

- (UIView*) tableView:(UITableView *)_tableView viewForHeaderInSection:(NSInteger)section {
    
    if (_operType == IXSearchOperateTypeSearch) {
        return nil;
    }
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 33)];
    headerView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
    
    UILabel *showLbl = [IXCustomView createLable:CGRectMake(15,9, kScreenWidth/2 - (20),VIEW_H(headerView) - 9*2) title:LocalizedString(@"以下是历史搜索记录")                                   font:PF_MEDI(13)
                                       wTextColor:0x99abba
                                      dTextColor:0x4c6072
                                   textAlignment:NSTextAlignmentLeft];
    [headerView addSubview:showLbl];
    
    UILabel *cleanBtn = [IXCustomView createLable:CGRectMake(kScreenWidth/2 + 5,
                                                             9,
                                                             kScreenWidth/2 - 20,
                                                             VIEW_H(headerView) - 9*2)
                                            title:LocalizedString(@"清除历史记录")
                                             font:PF_MEDI(13)
                                       wTextColor:0x4c6072
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentRight];
    cleanBtn.userInteractionEnabled = YES;
    UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cleanBtn:)];
    [cleanBtn addGestureRecognizer:tg];
    [headerView addSubview:cleanBtn];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(headerView) - kLineHeight, kScreenWidth, kLineHeight)];
    lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xcbcfd6, 0x242a36);
    [headerView addSubview:lineView];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IXSymbolSearchCell *tableCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSymbolSearchCell class])];
    tableCell.delegate = self;
    tableCell.addBtn.tag = indexPath.row;
    tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableCell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    [tableCell reloadUIWithItem:_dataArr[indexPath.row]];

    return tableCell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXSymbolDetailVC *openVC = [[IXSymbolDetailVC alloc] init];
    _currentIndex = indexPath.row;
    IXSymbolM *model = _dataArr[indexPath.row];
    model = [IXDataProcessTools querySymbolBySymbolId:model.id_p];
    if (!model.contractSizeNew) {
        model.contractSizeNew = 1;
    }
    IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
    tradeModel.symbolModel = model;
    NSDictionary *marketDic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:model.id_p];
    if (marketDic && marketDic.count > 0) {
        tradeModel.marketId = [marketDic[kMarketId] intValue];
    }
    openVC.tradeModel = tradeModel;
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

#pragma mark 选择或取消自选
- (void)refreshUIBySub:(BOOL)isSub
{
    IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
    tmpModel = _dataArr[_currentIndex];
    tmpModel.isSub = isSub;
    [_dataArr replaceObjectAtIndex:_currentIndex withObject:tmpModel];
    [_tableV reloadData];
}

#pragma mark 组装查询搜索历史model
- (NSArray *)packageQuerySearchHistory:(NSArray *)searchArr
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < searchArr.count; i++) {
        NSDictionary *tmpDic = [[NSDictionary alloc] init];
        tmpDic = searchArr[i];
        IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
        tmpModel.languageName = tmpDic[kLanguageName];
        tmpModel.id_p = [tmpDic[kID] doubleValue];
        tmpModel.marketName = tmpDic[kMarketName];
        tmpModel.isSub = [tmpDic[kEnable] boolValue];
        tmpModel.source = tmpDic[kSource];
        tmpModel.name = tmpDic[kName];
        NSArray *labelArr = [[NSArray alloc] init];
        labelArr = [IXDBSymbolLableMgr querySymbolLableBySymbolId:tmpModel.id_p];
        if (labelArr && labelArr.count > 0) {
            tmpModel.labelArr = labelArr;
        }
        [retArr addObject:tmpModel];
    }
    return retArr;
}

#pragma mark 组装搜索历史
- (NSDictionary *)packageSearchHistory:(IXSymbolM *)model
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[kID] = @(model.id_p);
    dic[kName] = model.name;
    dic[kEnable] = @(model.isSub);
    dic[kMarketName] = model.marketName;
    dic[kLanguageName] = model.languageName;
    dic[kSource] = model.source;
    dic[kSymbolSubId] = @(model.symbolsubId);
    return dic;
}

#pragma mark 处理产品model
- (NSMutableArray *)packageSymbol:(NSArray *)dataArr
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataArr.count; i++) {
        NSDictionary *dataDic = [[NSDictionary alloc] init];
        dataDic = dataArr[i];
        IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
        tmpModel.id_p = [dataDic[kID] doubleValue];
        tmpModel.name = dataDic[kName];
        tmpModel.cataId = [dataDic[kCataId] doubleValue];
        NSDictionary *subDic = [IXDBSymbolSubMgr querySymbolSubBySymbolId:tmpModel.id_p accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        DLog(@"subDic is:%@------------",subDic);
        if (subDic && subDic[kID]) {
            tmpModel.isSub = YES;
        } else {
            tmpModel.isSub = NO;
        }
        [retArr addObject:tmpModel];
    }
    return retArr;
}

#pragma mark 搜索产品
- (void)searchSymbol
{
    if (_tField.text.length == 0) {
        _operType = IXSearchOperateTypeHistory;
        _sourceArr = [IXDBSearchBrowseMgr queryAllSearchBrowserHistoryInfoUserId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        _sourceArr = [self packageQuerySearchHistory:_sourceArr];
        _dataArr = [_sourceArr mutableCopy];
    } else {
        _operType = IXSearchOperateTypeSearch;
        _sourceArr = [[IXDataProcessTools fuzzyQuerySymbolByContent:_tField.text cataId:0] mutableCopy];
        _sourceArr = [self packageMarketIdToSymbol:_sourceArr];
        _dataArr = [_sourceArr mutableCopy];
    }
    [_tableV reloadData];
}

#pragma mark 清除搜索记录
- (void)cleanBtn:(UIButton *)btn
{
    BOOL flag = [IXDBSearchBrowseMgr deleteAllSearchBrowserHistoryInfoUserId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    if (flag) {
        [_dataArr removeAllObjects];
        [_tableV reloadData];
    } else {
        [self addSearchBrowseData];
    }
}

- (void)addOrDeleteSymbolSub:(UIButton *)btn
{
    [_tField resignFirstResponder];
    if ([[IXUserInfoMgr shareInstance] isDemeLogin]) {
        [self showRegistLoginAlert];
        return;
    }
    _currentIndex = [btn tag];
    IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
    tmpModel = _dataArr[_currentIndex];
    
    if (!tmpModel.isSub) {
        [self responseToSubSymbol:tmpModel];
    } else {
//        [self delSubSymbol:tmpModel];
    }
}

#pragma mark 添加自选
- (void)responseToSubSymbol:(IXSymbolM *)model
{
    proto_symbol_sub_add *subAdd = [[proto_symbol_sub_add alloc] init];
    item_symbol_sub *itemSub = [[item_symbol_sub alloc] init];
    [itemSub setAccountid:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    [itemSub setSymbolid:model.id_p];
    subAdd.symbolSub = itemSub;
    [[IXTCPRequest shareInstance] addSubSymbolWithParam:subAdd];
}

#pragma mark 删除自选
- (void)delSubSymbol:(IXSymbolM *)model
{
    NSDictionary *subDic = [IXDBSymbolSubMgr querySymbolSubBySymbolId:model.id_p accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
    if (subDic && subDic[kID]) {
        proto_symbol_sub_delete *delSubSymbol = [[proto_symbol_sub_delete alloc] init];
        [delSubSymbol setId_p:[subDic[kID] doubleValue]];
        [delSubSymbol setAccountid:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        [[IXTCPRequest shareInstance] delSubSymbolWithParam:delSubSymbol];
    }
}

- (NSArray *)packageMarketIdToSymbol:(NSArray *)modelArr
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    NSArray *marketArr = [[NSArray alloc] init];
    marketArr = [self queryBatchSymbolMarketIdBySymbols:modelArr];
    for (int i = 0; i < modelArr.count; i++) {
        IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
        tmpModel = modelArr[i];
        for (int j = 0; j < marketArr.count; j++) {
            NSDictionary *tmpDic = [[NSDictionary alloc] init];
            tmpDic = marketArr[j];
            if (tmpModel.id_p == [tmpDic[kID] doubleValue]) {
                tmpModel.marketID = [tmpDic[kMarketId] intValue];
                tmpModel.marketName = [IXEntityFormatter getMarketNameWithMarketId:[tmpDic[kMarketId] doubleValue]];
                break;
            }
        }
        [retArr addObject:tmpModel];
    }
    return retArr;
}

- (NSArray *)queryBatchSymbolMarketIdBySymbols:(NSArray *)modelArr
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < modelArr.count; i++) {
        IXSymbolM *tmpModel = modelArr[i];
        [retArr addObject:@{@"symbolId":@(tmpModel.id_p)}];
    }
    retArr = [[IXDBSymbolHotMgr queryBatchSymbolMarketIdsBySymbolIds:retArr] mutableCopy];
    return retArr;
}

#pragma mark 取消
- (void)cancelBtn:(UIButton *)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self searchSymbol];
    return YES;
}

- (void)textFieldDidChange:(UITextField *) TextField
{
    [self searchSymbol];
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if( IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_SUB_ADD) ){
        proto_symbol_sub_add  *pb = ((IXTradeDataCache *)object).pb_symbol_sub_add;
        if (pb.result == 0) {
            if (pb.symbolSub) {
                BOOL flag = [IXDBSymbolSubMgr saveSymbolSubInfo:pb.symbolSub accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
                DLog(@"<添加自选:%@>",flag?@"成功":@"失败");
                if (flag) {
                    IXSymbolM *tmpModel = [[IXSymbolM alloc] init];
                    tmpModel = _dataArr[_currentIndex];
                    tmpModel.isSub = YES;
                    tmpModel.symbolsubId = pb.symbolSub.id_p;
                    NSDictionary *tmpDic = [self packageSearchHistory:tmpModel];
                    BOOL flag = [IXDBSearchBrowseMgr saveSearchBrowserHistoryInfoSymbolId:tmpDic userId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
                    DLog(@"%@",flag?@"搜索历史保存成功":@"搜索历史保存失败" );
                    [SVProgressHUD showSuccessWithStatus:LocalizedString(@"添加自选成功")];
                    [self refreshUIBySub:YES];
                    return;
                }
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"添加自选")
                                                     message:pb.comment
                                                 cancelTitle:nil
                                                  sureTitles:LocalizedString(@"确定")];
            VC.index = 0;
            VC.expendAbleAlartViewDelegate = self;
            [VC showView];
        }
    } else if (IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_SUB_DELETE)) {
        proto_symbol_sub_delete *pb = ((IXTradeDataCache *)object).pb_symbol_sub_delete;
        if (pb.result == 0) {
            [self refreshUIBySub:NO];
        } else {
            [SVProgressHUD dismiss];
            IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"删除自选")
                                                     message:pb.comment
                                                 cancelTitle:nil
                                                  sureTitles:LocalizedString(@"确定")];
            VC.index = 1;
            VC.expendAbleAlartViewDelegate = self;
            [VC showView];
        }
    }
}

@end
