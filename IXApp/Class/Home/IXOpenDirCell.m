//
//  IXOpenDirCell.m
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenDirCell.h"

@interface IXOpenDirCell ()

@property (nonatomic, strong) UILabel *markDir;
@property (nonatomic, strong) UIButton *sellBtn;
@property (nonatomic, strong) UIButton *buyBtn;

@end

@implementation IXOpenDirCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        [self loadBtn];
    }
    return self;
}

- (UILabel *)markDir
{
    if ( !_markDir ) {
        _markDir = [[UILabel alloc] initWithFrame:CGRectMake( 0, 42, kScreenWidth/2, 2)];
        _markDir.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        [self.contentView addSubview:_markDir];
    }
    return _markDir;
}

- (void)loadBtn
{
    _sellBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_sellBtn];

    _sellBtn.frame = CGRectMake( 0, 0, kScreenWidth/2, 44);
    [_sellBtn setTitle: LocalizedString(@"卖出") forState:UIControlStateNormal];
    [_sellBtn.titleLabel setFont:PF_MEDI(13)];
    [_sellBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4)
                           forState:UIControlStateNormal];
    [_sellBtn addTarget:self
                action:@selector(responseToChooseSell)
      forControlEvents:UIControlEventTouchUpInside];
    
    _buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_buyBtn];
    
    _buyBtn.frame = CGRectMake( kScreenWidth/2, 0, kScreenWidth/2, 44);
    [_buyBtn setTitle: LocalizedString(@"买入") forState:UIControlStateNormal];
    [_buyBtn dk_setTitleColorPicker:DKColorWithRGBs(0x99abba, 0x8395a4)
                           forState:UIControlStateNormal];
    [_buyBtn.titleLabel setFont:PF_MEDI(13)];
    [_buyBtn addTarget:self
                action:@selector(responseToChooseBuy)
      forControlEvents:UIControlEventTouchUpInside];
}

- (void)setDirection:(item_order_edirection)direction
{
    CGRect frame = self.markDir.frame;
    if ( direction == item_order_edirection_DirectionBuy ) {
        frame.origin.x = kScreenWidth/2;
        _markDir.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        [_sellBtn dk_setTitleColorPicker:DKColorWithRGBs(0x99abba, 0x8395a4)
                                forState:UIControlStateNormal];
        [_buyBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4)
                               forState:UIControlStateNormal];
    }else{
        frame.origin.x = 0;
        _markDir.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        [_sellBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4)
                                forState:UIControlStateNormal];
        [_buyBtn dk_setTitleColorPicker:DKColorWithRGBs(0x99abba, 0x8395a4)
                               forState:UIControlStateNormal];
    }
    self.markDir.frame = frame;
}

- (void)responseToChooseSell
{
    if (_tradeState == IXSymbolTradeStateBuyNormal) {
        [SVProgressHUD showMessage:LocalizedString(@"该行情现只能进行单边交易")];
        return;
    }
    [UIView animateWithDuration:.3f animations:^{
        
        CGRect frame = _markDir.frame;
        frame.origin.x = 0;
        _markDir.frame = frame;
        
    } completion:^(BOOL finished) {
        
        _markDir.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        [_sellBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4)
                                forState:UIControlStateNormal];
        [_buyBtn dk_setTitleColorPicker:DKColorWithRGBs(0x99abba, 0x8395a4)
                               forState:UIControlStateNormal];
        if( self.orderDirBlock ){
            self.orderDirBlock(item_order_edirection_DirectionSell);
        }
    }]; 
}

- (void)responseToChooseBuy
{
    if (_tradeState == IXSymbolTradeStateSellNormal) {
        [SVProgressHUD showMessage:LocalizedString(@"该行情现只能进行单边交易")];
        return;
    }
    [UIView animateWithDuration:.3f animations:^{
        
        CGRect frame = _markDir.frame;
        frame.origin.x = kScreenWidth/2;
        _markDir.frame = frame;
        
    }completion:^(BOOL finished) {
        
        _markDir.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        [_sellBtn dk_setTitleColorPicker:DKColorWithRGBs(0x99abba, 0x8395a4)
                                forState:UIControlStateNormal];
        [_buyBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4)
                               forState:UIControlStateNormal];
        if( self.orderDirBlock ){
            self.orderDirBlock(item_order_edirection_DirectionBuy);
        }
    }];
}
@end
