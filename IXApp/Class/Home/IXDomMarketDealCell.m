//
//  IXDomMarketDealCell.m
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDomMarketDealCell.h"
#import "UIImageView+SepLine.h"
#import "IXUserDefaultM.h"

@interface IXDomMarketDealCell ()<UITextFieldDelegate>

@property (nonatomic, strong) UIButton *sellbtn;

@property (nonatomic, strong) UIButton *buybtn;


@property (nonatomic, strong) UILabel *unitNameLbl;

@property (nonatomic, strong) UITextField *volTF;


@end


@implementation IXDomMarketDealCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {

        [self.contentView addSubview:self.backClickView];
        [self.contentView addSubview:self.unitNameLbl];
        
        [self.contentView addSubview:self.sellbtn];
        [self.contentView addSubview:self.buybtn];
     }
    return self;
}

- (void)setVol:(NSString *)vol
{
    if ( vol ) {
        _vol = vol;
        self.volTF.text = _vol;
    }
}

- (void)setModel:(IXDomDealM *)model
{
    _model = model;
    
    [self updateState];
}

- (void)updateState
{
    if (_model.showM.canBuy) {
        [_buybtn addTarget:self
                    action:@selector(responseToBuy)
          forControlEvents:UIControlEventTouchUpInside];
        _buybtn.userInteractionEnabled = YES;
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            
            UIColor *nColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0x21ce99) :UIColorHexFromRGB(0xff4d2d);
            UIColor *hColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0x149c72) :UIColorHexFromRGB(0xe35029);
            
            UIImage *nImg = [UIImageView switchToImageWithColor:nColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_buybtn setBackgroundImage:nImg forState:UIControlStateNormal];
            
            UIImage *hImg = [UIImageView switchToImageWithColor:hColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_buybtn setBackgroundImage:hImg forState:UIControlStateHighlighted];
        }else{
            
            UIColor *nColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0x11b873) :UIColorHexFromRGB(0xff4653);
            UIColor *hColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0x17a46a) :UIColorHexFromRGB(0xc9464f);
            
            UIImage *nImg = [UIImageView switchToImageWithColor:nColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_buybtn setBackgroundImage:nImg forState:UIControlStateNormal];
            
            UIImage *hImg = [UIImageView switchToImageWithColor:hColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_buybtn setBackgroundImage:hImg forState:UIControlStateHighlighted];
        }
    }else{
        _buybtn.userInteractionEnabled = NO;
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            UIImage *nImg = [UIImageView switchToImageWithColor:UIColorHexFromRGB(0x4c6072)
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_buybtn setBackgroundImage:nImg forState:UIControlStateNormal];
        }else{
            UIImage *nImg = [UIImageView switchToImageWithColor:UIColorHexFromRGB(0x99abba)
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_buybtn setBackgroundImage:nImg forState:UIControlStateNormal];
        }
    }

    if (_model.showM.canSell) {
        [_sellbtn addTarget:self
                     action:@selector(responseToSell)
           forControlEvents:UIControlEventTouchUpInside];
        _sellbtn.userInteractionEnabled = YES;
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            
            UIColor *nColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0xff4d2d) :UIColorHexFromRGB(0x21ce99);
            UIColor *hColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0xe35029) :UIColorHexFromRGB(0x149c72);

            UIImage *nImg = [UIImageView switchToImageWithColor:nColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_sellbtn setBackgroundImage:nImg forState:UIControlStateNormal];
            
            UIImage *hImg = [UIImageView switchToImageWithColor:hColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_sellbtn setBackgroundImage:hImg forState:UIControlStateHighlighted];
        }else{
            
            UIColor *nColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0xff4653) :UIColorHexFromRGB(0x11b873);
            UIColor *hColor = [IXUserInfoMgr shareInstance].settingRed ? UIColorHexFromRGB(0xc9464f) :UIColorHexFromRGB(0x17a46a);
            
            UIImage *nImg = [UIImageView switchToImageWithColor:nColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_sellbtn setBackgroundImage:nImg forState:UIControlStateNormal];
            
            UIImage *hImg = [UIImageView switchToImageWithColor:hColor
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_sellbtn setBackgroundImage:hImg forState:UIControlStateHighlighted];
        }

    }else{
        if ([IXUserInfoMgr shareInstance].isNightMode) {
            UIImage *nImg = [UIImageView switchToImageWithColor:UIColorHexFromRGB(0x4c6072)
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_sellbtn setBackgroundImage:nImg forState:UIControlStateNormal];
        }else{
            UIImage *nImg = [UIImageView switchToImageWithColor:UIColorHexFromRGB(0x99abba)
                                                           size:CGSizeMake((kScreenWidth - 30)/2 - 0.25, 44)];
            [_sellbtn setBackgroundImage:nImg forState:UIControlStateNormal];
        }
     
        _sellbtn.userInteractionEnabled = NO;
    }
}

- (void)responseToVol
{
    if ( _requstVol ) {
        _requstVol();
    }
}

- (void)responseToSell
{
    [_volTF resignFirstResponder];
    if ( _addOrder ) {
        _addOrder(item_order_edirection_DirectionSell);
    }
}

- (void)responseToBuy
{
    [_volTF resignFirstResponder];
    if( _addOrder ){
        _addOrder(item_order_edirection_DirectionBuy);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ( _inVol ) {
        NSString *text = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if (_model.isVolNum) {
            text = [NSString thousandFormate:[NSString formatterPrice:text WithDigits:_model.showM.symbolModel.volDigits] withDigits:_model.showM.symbolModel.volDigits];
        } else {
            text = [NSString thousandFormate:[NSString formatterPrice:text WithDigits:2] withDigits:2];
        }
        textField.text = text;
        _inVol(text);
    }
}

- (void)setUnitName:(NSString *)unitName
{
    if ( unitName ) {
        self.unitNameLbl.text = unitName;
    }
}

- (void)setCorner:(UIRectCorner)corner ForView:(UIView *)view
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                   byRoundingCorners:corner
                                                         cornerRadii:CGSizeMake(2.0, 2.0)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //首字母不能为'0'
    if (![textField.text length]) {
        if ([string isEqualToString:@"0"]) {
            return NO;
        }
    }
    //不能输入多个'.'
    if ([textField.text containsString:@"."]) {
        if ([string isEqualToString:@"."]) {
            return NO;
        }
    }
    return YES;
}

- (UIImageView *)backClickView
{
    if ( !_backClickView ) {
        _backClickView = [[UIImageView alloc] initWithFrame:CGRectMake( 15, 10, kScreenWidth - 30, 44)];
        _backClickView.userInteractionEnabled = YES;
        [self setCorner:UIRectCornerTopLeft|UIRectCornerTopRight ForView:_backClickView];
        if ( ![IXUserInfoMgr shareInstance].isNightMode ) {
            UIImage * wImg = [[UIImage imageNamed:@"dom_vol"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake( 5, 5, 5, 5)];
            [_backClickView setImage:wImg];
        }else{
            _backClickView.backgroundColor = UIColorHexFromRGB(0x303b4d);
        }
        
        UIButton *chooseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        chooseBtn.frame = CGRectMake( kScreenWidth - 58, 7, 28, 30);
        chooseBtn.tag = CHOOSETAG;
        
        [_backClickView addSubview:chooseBtn];
        [chooseBtn addTarget:self
                      action:@selector(responseToVol)
            forControlEvents:UIControlEventTouchUpInside];
        [chooseBtn dk_setImage:DKImageNames(@"common_arrow_down", @"common_arrow_down_D")
                      forState:UIControlStateNormal];
        [chooseBtn dk_setImage:DKImageNames(@"common_arrow_up_black", @"common_arrow_up_D")
                      forState:UIControlStateSelected];
        [chooseBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 5, 0, 15)];
    }
    return _backClickView;
}

- (UITextField *)volTF
{
    if ( !_volTF ) {
        _volTF = [[UITextField alloc] initWithFrame:CGRectMake( 75, 7,  kScreenWidth - 135, 30)];
        _volTF.font = RO_REGU(15);
        _volTF.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _volTF.textAlignment = NSTextAlignmentRight;
        _volTF.keyboardType = UIKeyboardTypeDecimalPad;
        _volTF.delegate = self;
        [self.backClickView addSubview:_volTF];
    }
    return _volTF;
}

- (UILabel *)unitNameLbl
{
    if ( !_unitNameLbl ) {
        _unitNameLbl = [IXUtils createLblWithFrame:CGRectMake( 30, 25, kScreenWidth - 60, 15)
                                          WithFont:RO_REGU(15)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x99abba
                                        dTextColor:0x8395a4];
        _unitNameLbl.text = @"数量";
    }
    return _unitNameLbl;
}


- (UIButton *)sellbtn
{
    if ( !_sellbtn ) {
        _sellbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sellbtn.frame = CGRectMake( 15, 54, (kScreenWidth - 30)/2 - 0.25, 44);
        [self setCorner:UIRectCornerBottomLeft|UIRectCornerBottomRight ForView:_sellbtn];
        [_sellbtn setTitle:LocalizedString(@"卖") forState:UIControlStateNormal];
    }
    return _sellbtn;
}

- (UIButton *)buybtn
{
    if ( !_buybtn ) {
        _buybtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _buybtn.frame = CGRectMake( 15 + (kScreenWidth - 30)/2 + 0.25, 54, (kScreenWidth - 30)/2 - 0.25, 44);
        [_buybtn setTitle:LocalizedString(@"买") forState:UIControlStateNormal];
        [self setCorner:UIRectCornerBottomLeft|UIRectCornerBottomRight ForView:_buybtn];
    }
 
    return _buybtn;
}

@end
