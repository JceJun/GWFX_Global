//
//  IXSymbolPosTitleM.h
//  IXApp
//
//  Created by Bob on 2017/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXSymbolPosTitleM : NSObject

@property (nonatomic, copy) NSString *nPrice;

@property (nonatomic, copy) NSString *profit;

@property (nonatomic, copy) NSString *positionNum;
@property (nonatomic, copy) NSString *showTotNum;

@property (nonatomic, copy) NSString *displayName;
@end
