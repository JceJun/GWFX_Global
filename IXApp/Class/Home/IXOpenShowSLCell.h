//
//  IXOpenShowSLCell.h
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^showSL)();

@interface IXOpenShowSLCell : UITableViewCell

@property (nonatomic, copy) showSL slBlock;

@property (nonatomic, assign) BOOL openSwi;

@end
