//
//  IXQuoteDetailM.m
//  IXApp
//
//  Created by Bob on 2016/12/21.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteDetailM.h"
#import "IXQuoteDataCache.h"
#import "IXLastQuoteM.h"

#import "IXAccountBalanceModel.h"
#import "IxProtoSymbol.pbobjc.h"

@implementation IXQuoteDetailM

- (IXQuoteDetailM *)initWithMarketId:(NSInteger)mId SymCataId:(NSInteger)sId
{
    self = [self init];
    if(self){
        _markId = mId;
        _symbolCataId = sId;
 
        _startOffset = 0;
        _currentPage = 0;
        _currentSearchIndex = 0;
 
        if ( 0 == _symbolCataId ){
//            _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsHot]];
            _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsHotNew]];
        }else{
            _childCataArr = [IXDataProcessTools querySymbolCataByParentId:_symbolCataId];
        }
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {

        [self dataInit];
        
        IXAccountBalanceModel_listen_regist(self, AB_CACHE_NUMBER_CHANGE);
        
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_ADD);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_UPDATE);
        
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_HOT_ADD);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_HOT_UPDATE);
    }
    return self;
}

- (void)dealloc
{
    IXAccountBalanceModel_listen_resign(self, AB_CACHE_NUMBER_CHANGE);
    
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_HOT_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_HOT_UPDATE);
}

- (void)dataInit
{
    _pageCount = 25;
    _currentPage = 0;
    _didLoadAllData = NO;
    _currentSearchIndex = 0;
    _cellType = [IXEntityFormatter getCellStyle];
    
    _priceDic = [NSMutableDictionary dictionary];
    _lastClosePriceDic = [NSMutableDictionary dictionary];
    
    _subDynPriceArr = [NSMutableArray array];
    _subLastClosePriceArr = [NSMutableArray array];
    _tradeStatusArr = [NSMutableArray array];
    
    _marketInfoDic = [NSMutableDictionary dictionary];
    
    _chooseMarketArr = @[@[LocalizedString(@"全部"),@"SH",@"SZ",@"HK"],
                         @[@"SF",@"SC",@"DC",@"ZC"],
                         @[@"GW",@"FX"]];
}

- (void)setSymbolCataId:(NSInteger)symbolCataId
{
    _symbolCataId = symbolCataId;
    if ( 0 == _symbolCataId ){
//        _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsHot]];
        _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsHotNew]];

    }else{
        _childCataArr = [IXDataProcessTools querySymbolCataByParentId:_symbolCataId];
    }
}

#pragma mark -
#pragma mari - IXTradeDataKVO
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXAccountBalanceModel_isSameKey( keyPath , AB_CACHE_NUMBER_CHANGE)) {
        if(self.updatePosNum){
            self.updatePosNum();
        }
    }else if(IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_ADD) ||
             IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_UPDATE)){
        proto_symbol_add *pb = ((IXTradeDataCache *)object).pb_symbol_add;
        if (pb.symbol.symbolCataid == self.symbolCataId) {
            if (self.updateSym) {
                self.updateSym();
            }
        }
    }else if(IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_HOT_ADD)||
             IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_HOT_UPDATE)){
        if (0 == _symbolCataId) {
            if (self.updateSym) {
                self.updateSym();
            }
        }
    }
}

//检索产品数据
//如果是二级分类，直接查询对应的数据
//如果是一级分类，则需要遍历二级分类，查询对应的数据
- (NSMutableArray *)querySymbolList
{
    if ( _didLoadAllData ) {
        return [@[] mutableCopy];
    }
    
    if ( 0 == self.childCataArr.count ) {
        _pageCount = 25;
        _currentSearchIndex = 0;
        NSMutableArray *result = [[self loadSymbolBySymbolCataId:_symbolCataId] mutableCopy];

        _startOffset += result.count;
        _didLoadAllData = ( result.count == 0 );
        return  result;
    }else{
        NSMutableArray *result = [NSMutableArray array];
        for ( ;_currentSearchIndex < self.childCataArr.count; _currentSearchIndex++ ) {
            
            NSDictionary *dic = self.childCataArr[_currentSearchIndex];
            [result addObjectsFromArray:[self loadSymbolBySymbolCataId:[dic[kID] integerValue]]];
            
            _startOffset += result.count;
            if ( [result count] && result.count == 25 ) {
                _pageCount = 25;
                break;
            }else{
                _startOffset = 0;
                _pageCount -= result.count;
            }
        }
        _didLoadAllData = ( result.count == 0 );
        return result;
    }
}

- (NSArray *)loadSymbolBySymbolCataId:(NSInteger)cataId
{
    NSArray *result =  [IXDataProcessTools querySymbolBySymbolNewCataId:cataId
                                                             startIndex:_startOffset
                                                                 offset:_pageCount];
    return result;
}

- (CGFloat)rowHeight
{
    if( 0 == _cellType )
        return 55;
    return 112;
}

@end
