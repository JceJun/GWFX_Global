//
//  IXQuoteDetailM.h
//  IXApp
//
//  Created by Bob on 2016/12/21.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXDBGlobal.h"
#import "IXDBSymbolCataMgr.h"

@interface IXQuoteDetailM : NSObject

@property (nonatomic, assign) NSInteger markId;             //市场Id
@property (nonatomic, assign) NSInteger symbolCataId;       //分类Id

@property (nonatomic, assign) NSInteger cellType;
@property (nonatomic, assign) NSInteger numberOfCount;

@property (nonatomic, assign) NSInteger pageCount;          //每页记录条数
@property (nonatomic, assign) NSInteger currentPage;        //当前页数

@property (nonatomic, assign) NSInteger currentSubStartTag;
@property (nonatomic, assign) NSInteger currentSearchIndex; //当前索引哪个分类
@property (nonatomic, assign) NSInteger startOffset;        //当前偏移
@property (nonatomic, assign) BOOL didLoadAllData;

@property (nonatomic, strong) NSMutableDictionary *priceDic;            //行情系统Price信息
@property (nonatomic, strong) NSMutableDictionary *lastClosePriceDic;   //行情系统Price信息

@property (nonatomic, strong) NSMutableArray *subDynPriceArr;           //当前订阅Price信息
@property (nonatomic, strong) NSMutableArray *subLastClosePriceArr;     //当前订阅Price信息
@property (nonatomic, strong) NSMutableArray *tradeStatusArr;           //是否可交易

@property (nonatomic, strong) NSMutableDictionary *marketInfoDic;

@property (nonatomic, strong) NSMutableArray *dataSource;               //交易系统Symbol信息
@property (nonatomic, strong) NSArray *childCataArr;                    //交易系统Symbol信息
@property (nonatomic, strong) NSArray *chooseMarketArr;

@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) BOOL needReSubDynQuote;

/**
 刷新所有产品：账号组信息更新，更新具体产品
 */
@property (nonatomic,copy)void(^updateSym)();


/**
 刷新持仓数
 */
@property (nonatomic,copy)void(^updatePosNum)();


//检索产品数据
- (NSMutableArray *)querySymbolList;


- (IXQuoteDetailM *)initWithMarketId:(NSInteger)mId SymCataId:(NSInteger)sId;

@end
