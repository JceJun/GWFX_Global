//
//  IXOpenChooseContentView.h
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selectRow)(NSInteger row);
typedef void(^endEdit)();

@interface IXOpenChooseContentView : UIView

@property (nonatomic, strong) NSMutableArray *dataSourceArr;

@property (nonatomic, strong) UIFont *cntFont;

@property (nonatomic, copy) endEdit endEditBlock;

- (id)initWithDataSource:(NSArray *)dataSource WithSelectedRow:(selectRow)chooseRow;

- (void)resetDataSourceWithData:(NSArray *)dataSource;

- (void)show;

- (void)dismiss;

@end
