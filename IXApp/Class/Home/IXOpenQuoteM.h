//
//  IXOpenQuoteM.h
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXOpenRootVC.h"

@interface IXOpenQuoteM : NSObject

//订阅数据
+ (NSMutableArray *)subscribeDynamicPrice:(IXOpenRootVC *)root;

+ (void)cancelDynamicPrice:(IXOpenRootVC *)root;

+ (void)loadDetailQuote:(IXOpenRootVC *)root;

+ (void)cancelDetailQuote:(IXOpenRootVC *)root;

@end
