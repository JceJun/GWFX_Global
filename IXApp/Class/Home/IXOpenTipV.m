//
//  IXOpenTipV.m
//  IXApp
//
//  Created by Bob on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenTipV.h"
#import "YYText.h"
#import "UIViewExt.h"
#import "UIKit+Block.h"
@interface IXOpenTipV ()

@property (nonatomic, strong) YYLabel *tipContent;

@property (nonatomic, strong) UIImageView *tipImg;

@end

@implementation IXOpenTipV

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if ( self ) {
        self.backgroundColor = AutoNightColor(0xfcf1f2, 0x2c2b35);
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = AutoNightColor(0xfcf1f2, 0x2c2b35);
        self.frame = CGRectMake( 0, 0, kScreenWidth, 40);
    }
    return self;
}

+ (void)showInView:(UIView *)superView lText:(NSString *)lText rText:(NSString *)rText block:(void(^)(void))block{
    IXOpenTipV *tip = [IXOpenTipV new];
    tip.tipInfo = lText;
    tip.tag = 111111;
    if (rText.length) {
        [tip addEntranceArrow:rText];
    }
    [superView addSubview:tip];
    
    [tip block_whenTapped:^(UIView *aView) {
        if (block) {
            block();
        }
    }];
}

+ (void)hideInView:(UIView *)superView{
    UIView *view = [superView viewWithTag:111111];
    if (view) {
        [view removeFromSuperview];
    };
}

- (YYLabel *)tipContent
{
    if ( !_tipContent ) {
//        _tipContent = [IXUtils createLblWithFrame:CGRectMake( 37, -10, kScreenWidth - 47, 60)
//                                         WithFont:PF_MEDI(13)
//                                        WithAlign:NSTextAlignmentLeft
//                                       wTextColor:0xff4653
//                                       dTextColor:0xff4d2d];
        
        _tipContent = [YYLabel new];
        _tipContent.frame = CGRectMake( 37, -10, kScreenWidth - 47, 60);
        _tipContent.font = PF_MEDI(13);
        _tipContent.textAlignment = NSTextAlignmentLeft;
        _tipContent.textColor = [UIColor redColor];
        
        _tipContent.numberOfLines = 0;
        [self addSubview:_tipContent];
    }
    return _tipContent;
}

- (UIImageView *)tipImg
{
    if ( !_tipImg ) {
        _tipImg = [[UIImageView alloc] initWithFrame:CGRectMake( 10, 10, 20, 20)];
        _tipImg.dk_imagePicker = DKImageNames(@"common_tip", @"common_tip_D");
        [self addSubview:_tipImg];
    }
    return _tipImg;
}

- (void)setTipInfo:(NSString *)tipInfo
{
    [self tipImg];
    [self tipContent];
   
    _tipInfo = tipInfo;
    if ( tipInfo && [tipInfo isKindOfClass:[NSString class]] ) {
        
        if ([_tipInfo isEqualToString:LocalizedString(@"支付遇到问题请联系客服")]) {
            [self textToYY:tipInfo];
        }else{
            _tipContent.text = tipInfo;
        }
        
        
    }
}


- (void)addEntranceArrow:(NSString *)msg{
    // 加右箭头 >
    UIImageView *imgArrow = [UIImageView new];
    imgArrow.image = [UIImage imageNamed:@"openAccount_arrow_right"];
    [_tipContent addSubview:imgArrow];
    imgArrow.frame = CGRectMake(_tipContent._width- 8, (_tipContent._height - 14)/2, 8, 14);
    
    [_tipContent addSubview:imgArrow];
    
    UILabel *lb_title = [UILabel new];
    lb_title.font = ROBOT_FONT(12);
    lb_title.dk_textColorPicker = DKCellTitleColor;
    lb_title.textAlignment = NSTextAlignmentRight;
    lb_title.text = msg;
    [_tipContent addSubview:lb_title];
    [lb_title sizeToFit];
    lb_title._centerY = imgArrow._centerY;
    lb_title._right = imgArrow._left - 10;
    
}



- (void)textToYY:(NSString *)title{
    NSString *textStr;
    NSMutableAttributedString *text;
    
    NSString *rangeStr0;
    NSString *rangeStr1;
    
    NSRange range0;
//    NSRange range1;
    
    if ([title isEqualToString:LocalizedString(@"支付遇到问题请联系客服")]) {
        rangeStr0 = LocalizedString(@"支付遇到问题请联系客服");
        rangeStr1 = @"Customer Services";
    }else{
        return;
//        rangeStr0 = @"The Skrill Payment Gateway is opening\n";
//        rangeStr1 = @"VISA,AE,JCB,Diners Club,etc";
    }
    
    textStr = title;
    text = [[NSMutableAttributedString alloc] initWithString:textStr];
    
    [text yy_setColor:[UIColor redColor] range:[[text string] rangeOfString:[text string] options:NSCaseInsensitiveSearch]];
    
    range0 = [[text string] rangeOfString:rangeStr1 options:NSCaseInsensitiveSearch];
    YYTextDecoration *decoration = [YYTextDecoration decorationWithStyle:YYTextLineStyleSingle
                                                                   width:@(1)
                                                                   color:[UIColor redColor]];
    //下划线
    [text yy_setTextUnderline:decoration range:range0];
    
    // 高亮
    weakself;
    [text yy_setTextHighlightRange:range0
                            color:_tipContent.textColor
                  backgroundColor:[UIColor clearColor]
                        tapAction:^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect){
                            //点击事件
                            if (weakSelf.textBlock) {
                                weakSelf.textBlock(rangeStr1);
                            }
                        }];
    _tipContent.attributedText = text;
}

@end
