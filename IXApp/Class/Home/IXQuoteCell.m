//
//  IXQuoteCell.m
//  IXApp
//
//  Created by bob on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteCell.h"
#import "IXDBGlobal.h"
#import "UILabel+LineSpace.h"
#import "NSString+FormatterPrice.h"

@interface IXQuoteCell ()
{
    UILabel *symbolLabel1;
    UILabel *symbolLabel2;
    UIView  *backView;

    UILabel *symbolName;
    
    UILabel *symbolCode;
    UILabel *positionMark;
    
    UILabel *currentPrice;//现价
    
    BOOL canAnimation;

    double prePrice;
    double nLastClosePrice;

    UILabel *profit;
    UILabel *profitSwap;

    UILabel *positionNumber;
    

    BOOL canTrade;
    
    UIImageView *equalArrowImg;
    UIImageView *lessArrowImg;
    UIImageView *thanlArrowImg;
    UIImageView *disableArrowImg;
    
    UIImageView *equalBackImg;
    UIImageView *lessBackImg;
    UIImageView *thanlBackImg;
    UIImageView *disableBackImg;
    
    NSDictionary *symbolModel;
}

@end

@implementation IXQuoteCell

#pragma mark --
#pragma mark life cycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self elementImg];
        [self elementLabel];
    }
    return self;
}

- (void)elemetData
{
    canAnimation = YES;
    prePrice = 0;
    nLastClosePrice = 0;
}

- (void)elementImg
{
    CGRect frame = CGRectMake( 0, 0, kScreenWidth, 55);

    equalBackImg = [self loadImage:@"quoteCell_equal_jb"
                       WithDarkImg:@"quoteCell_equal_jb_dark"
                         WithFrame:frame];
    
    NSString *day = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_less_jb" : @"quoteCell_than_jb";
    NSString *dark = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_less_jb_dark" : @"quoteCell_than_jb_dark";
    lessBackImg = [self loadImage:day
                      WithDarkImg:dark
                        WithFrame:frame];
 
    day = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_than_jb" : @"quoteCell_less_jb";
    dark = [IXEntityFormatter isRedUpGreenDown] ? @"quoteCell_than_jb_dark" : @"quoteCell_less_jb_dark";
    thanlBackImg = [self loadImage:day
                       WithDarkImg:dark
                         WithFrame:frame];
 
    disableBackImg = [self loadImage:@"quoteCell_dis"
                         WithDarkImg:@"quoteCell_dis_dark"
                           WithFrame:frame];
 
    frame = CGRectMake( kScreenWidth - 110, 22, 10, 10);
    disableArrowImg = [self loadImage:@"quoteCell_close"
                          WithDarkImg:@"quoteCell_close_dark"
                            WithFrame:frame];
    
    equalArrowImg = [self loadImage:@"quoteCell_arrow_equal"
                        WithDarkImg:@"quoteCell_arrow_equal_dark"
                          WithFrame:frame];
    
    day = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_greenDown" : @"quote_arrow_redDown";
    dark = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_greenDown_dark" : @"quote_arrow_redDown_dark";
    lessArrowImg = [self loadImage:day
                       WithDarkImg:dark
                         WithFrame:frame];
    
    day = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_redUp" : @"quote_arrow_greenUp";
    dark = [IXEntityFormatter isRedUpGreenDown] ? @"quote_arrow_redUp_dark" : @"quote_arrow_greenUp_dark";
    thanlArrowImg = [self loadImage:day
                        WithDarkImg:dark
                          WithFrame:frame];
}

- (void)elementLabel
{
    positionNumber = [IXUtils createLblWithFrame:CGRectZero
                                        WithFont:PF_MEDI(10)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0xffffff
                                      dTextColor:0xffffff];
    positionNumber.backgroundColor = MarketSymbolNameColor;
    [self.contentView addSubview:positionNumber];
    
    CGRect frame = CGRectMake(22, 13, 120, 15);
    symbolName = [IXUtils createLblWithFrame:frame
                                    WithFont:PF_MEDI(13)
                                   WithAlign:NSTextAlignmentLeft
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea];
    [self.contentView addSubview:symbolName];
    
    frame = CGRectMake(22, 32, 20, 7);
    backView = [[UIView alloc] initWithFrame:frame];
    [self.contentView addSubview:backView];
    backView.backgroundColor = UIColorHexFromRGB(0x4886C9);
    
    frame.origin = CGPointZero;
    symbolLabel1 = [IXUtils createLblWithFrame:frame
                                     WithFont:PF_MEDI(7)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0xffffff
                                   dTextColor:0x262f3e];
    [backView addSubview:symbolLabel1];
    
    symbolLabel2 = [IXUtils createLblWithFrame:frame
                                      WithFont:PF_MEDI(7)
                                     WithAlign:NSTextAlignmentCenter
                                    wTextColor:0xffffff
                                    dTextColor:0x262f3e];
    [backView addSubview:symbolLabel2];
    symbolLabel2.hidden = YES;
    
    frame = CGRectMake(CGRectGetMaxX(symbolLabel1.frame) + 5, 32, 120, 10);
    symbolCode = [IXUtils createLblWithFrame:frame
                                    WithFont:RO_REGU(10)
                                   WithAlign:NSTextAlignmentLeft
                                  wTextColor:0x99abba
                                  dTextColor:0x8395a4];
    [self.contentView addSubview:symbolCode];
    symbolCode.text = @"--";
    
    frame = CGRectMake( kScreenWidth - 60, 14, 65, 12);
    profitSwap = [IXUtils createLblWithFrame:frame
                                    WithFont:RO_REGU(12)
                                   WithAlign:NSTextAlignmentLeft
                                  wTextColor:0x99abba
                                  dTextColor:0xff0000];
    [self.contentView addSubview:profitSwap];
    profitSwap.text = @"--";
    
    frame.origin.y = CGRectGetMaxY(profitSwap.frame) + 5;
    profit = [IXUtils createLblWithFrame:frame
                                WithFont:RO_REGU(12)
                               WithAlign:NSTextAlignmentLeft
                              wTextColor:0x99abba
                              dTextColor:0xff0000];
    [self.contentView addSubview:profit];
    profit.text = @"--";
    
    CGFloat orginX = CGRectGetMinX(profitSwap.frame) - 100;
    if ( orginX > kScreenWidth/2 ) {
        orginX = kScreenWidth/2;
    }
    frame = CGRectMake( kScreenWidth - 265, 18, 150, 19);
    currentPrice = [IXUtils createLblWithFrame:frame
                                      WithFont:RO_REGU(24)
                                     WithAlign:NSTextAlignmentRight
                                    wTextColor:0x99abba
                                    dTextColor:0xff0000];
    [self.contentView addSubview:currentPrice];
}

- (UIImageView *)loadImage:(NSString *)day WithDarkImg:(NSString *)dark WithFrame:(CGRect)frame
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.dk_imagePicker = DKImageNames(day,dark);
    [self.contentView addSubview:imageView];
    imageView.hidden = YES;
    
    return imageView;
}



/**
 添加标签
 */
- (void)setLabel
{
    symbolLabel1.text = @"";
    symbolLabel2.text = @"";
    
    NSArray *labelArr = symbolModel[kLabel];
    
    if([labelArr isKindOfClass:[NSArray class]] && labelArr.count){
        NSDictionary *label0;
        if (labelArr.count >= 1) {
            
            label0 = labelArr[0];
            NSInteger labeIndex = [label0 integerForKey:@"colour"];
            backView.backgroundColor = UIColorHexFromRGB([IXEntityFormatter getLabelColorValue:labeIndex]);
            
            if ([[label0 stringForKey:@"name"] length] != 0) {
                symbolLabel1.text = [label0 stringForKey:@"name"];
            }
        }
        
        NSDictionary *label1;
        if (labelArr.count >= 2) {
            label1 = labelArr[1];
            if ( [[label1 stringForKey:@"name"] length] != 0 ) {
                if (0 == symbolLabel1.text.length) {
                    symbolLabel1.text = [label1 stringForKey:@"name"];
                }else{
                    symbolLabel2.text = [label1 stringForKey:@"name"];
                }
            }
        }
    }
}

#pragma mark --
#pragma mark 刷新数据
- (void)setSymbolModel:(NSDictionary *)model
       WithPositionNum:(NSString *)positionNum
        WithMarketName:(NSString *)marketName
        WithTradeState:(IXSymbolTradeState)state
{
    symbolModel = model;

    canTrade = [IXDataProcessTools isNormalByState:state];
    [self refreashState];
    
    symbolName.text = symbolModel[kLanguageName];
    symbolCode.text = [NSString stringWithFormat:@"%@:%@",symbolModel[kName],marketName];
    
    [self refreshPositionNumber:positionNum];
    
    [self setLabel];
}


/**
 更新产品状态
 */
- (void)refreashState
{
    if (canTrade) {
        symbolName.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        symbolCode.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
        currentPrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        profit.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
        profitSwap.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
        [self setShowImage:0];
    }else{
        symbolName.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x6c7fa0);
        symbolCode.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        currentPrice.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        profit.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        profitSwap.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
        [self setDisTradeImg];
    }
}


#pragma mark 刷新UI
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self resetFrame];
}

- (void)resetFrame
{
    CGRect frame = symbolName.frame;
    frame.size.width = [IXEntityFormatter getContentWidth:symbolName.text WithFont:symbolName.font] + 5;
    symbolName.frame = frame;
    
    NSInteger width = 0;
    NSInteger width1 = (NSInteger)[IXEntityFormatter getContentWidth:symbolLabel1.text
                                                            WithFont:symbolLabel1.font] + 5;
    
    NSInteger width2 = 0;
    NSArray *labelArr = symbolModel[kLabel];
    if ([labelArr isKindOfClass:[NSArray class]] && labelArr.count > 1) {
        width2 = (NSInteger)[IXEntityFormatter getContentWidth:symbolLabel2.text
                                                      WithFont:symbolLabel2.font] + 5;
    }
    
    if ( 0 == width1 - 5 ) {
        width1 = 0;
    }
    
    if ( 0 == width2 - 5 ) {
        width2 = 0;
    }
    if ( width1 >= width2 ) {
        width = width1;
    }else{
        width = width2;
    }
 
    
    if ( width2 == 0 ) {
        symbolLabel2.hidden = YES;
        CGRect frame = symbolLabel1.frame;
        frame.origin.y = 0;
        frame.size.height = 10;
        frame.size.width = width;
        symbolLabel1.frame = frame;
        
        frame = backView.frame;
        frame.size.height = 10;
        frame.origin.x = CGRectGetMinX(symbolName.frame);
        frame.origin.y = CGRectGetMinY(symbolCode.frame);
        frame.size.width = width;
        backView.frame = frame;
        
    }else{
        symbolLabel2.hidden = NO;
        CGRect frame = symbolLabel1.frame;
        frame.origin.y = 1;
        frame.size.height = 7;
        frame.size.width = width;
        symbolLabel1.frame = frame;
        
        frame = symbolLabel2.frame;
        frame.origin.y = CGRectGetMaxY(symbolLabel1.frame) + 1;
        frame.size.width = width;
        symbolLabel2.frame = frame;
        
        frame = backView.frame;
        frame.origin.x = CGRectGetMinX(symbolName.frame);
        frame.origin.y = CGRectGetMaxY(symbolName.frame) + 3;
        frame.size.height = 17;
        frame.size.width = width;
        backView.frame = frame;
    }
    
    frame = symbolCode.frame;
    frame.origin.x =  (width == 0 ) ? CGRectGetMinX(symbolName.frame) : CGRectGetMaxX(backView.frame) + 5;
    frame.origin.y = ( width2 == 0 ) ? 32 : 35;
    symbolCode.frame = frame;
}

- (void)refreshPositionNumber:(NSString *)positionNum
{
    CGRect frame = symbolName.frame;
    frame.size.width = [IXEntityFormatter getContentWidth:symbolName.text
                                                 WithFont:symbolName.font] + 3;
    if (!positionNum ||
        ![positionNum isKindOfClass:[NSString class]] ||
        [positionNum isEqualToString:@""] ||
        [positionNum isEqualToString:@"0"] ) {
        positionNumber.frame = CGRectZero;
    }else{
        positionNumber.text = positionNum;
        frame = CGRectMake( 0, 12, 14, 15);
        positionNumber.layer.masksToBounds = YES;
        positionNumber.layer.cornerRadius = 1;
        positionNumber.frame = frame;
    }
}

#pragma mark 设置图片和背景色
- (void)setShowImage:(NSInteger)showWhichImage
{
    disableArrowImg.hidden = YES;
    [equalArrowImg setHidden: !(showWhichImage == 0)];
    [lessArrowImg setHidden: !(showWhichImage < 0)];
    [thanlArrowImg setHidden: !(showWhichImage > 0)];
 
    disableBackImg.hidden = YES;
    [equalBackImg setHidden: !(showWhichImage == 0)];
    [lessBackImg setHidden: !(showWhichImage < 0)];
    [thanlBackImg setHidden: !(showWhichImage > 0)];
}

- (void)setDisTradeImg
{
    disableArrowImg.hidden = NO;
    equalArrowImg.hidden = YES;
    lessArrowImg.hidden = YES;
    thanlArrowImg.hidden = YES;
    
    disableBackImg.hidden = NO;
    lessBackImg.hidden = YES;
    thanlBackImg.hidden = YES;
    equalBackImg.hidden = YES;
}

#pragma mark 设置昨收价和行情
- (void)setQuotePriceInfo:(IXQuoteM *)priceInfo
{
    if (0 == priceInfo.nPrice) {
        currentPrice.text = @"--";
    }else{
        currentPrice.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",priceInfo.nPrice]
                                          WithDigits:[symbolModel integerForKey:kDigits]];
    }

    if (canTrade) {
        if (priceInfo.nPrice == 0) {
            currentPrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
            [self setShowImage:0];
        }else if ( priceInfo.nPrice - prePrice > 0) {
            currentPrice.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
            [self setShowImage:1];
        }else if (priceInfo.nPrice - prePrice == 0){
            currentPrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
            [self setShowImage:0];
        }else{
            currentPrice.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            [self setShowImage:-1];
        }
    }else{
        [self setDisTradeImg];
        currentPrice.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);
    }

    if (priceInfo.nPrice != 0) {
        prePrice = priceInfo.nPrice;
        [self resetProfit];
    }
}

- (void)setLastClosePriceInfo:(IXLastQuoteM *)model
{
    nLastClosePrice = model.nLastClosePrice;
    [self resetProfit];
}

- (void)resetProfit
{
    double offsetPrice = 0;
    if ( nLastClosePrice != 0 && prePrice != 0 ) {
        offsetPrice = prePrice - nLastClosePrice;
        profit.text = [NSString formatterPrice:[NSString formatterPriceSign:offsetPrice]
                                    WithDigits:[symbolModel integerForKey:kDigits]];
        NSString *profitSwapStr = [NSString formatterPrice:
                                   [NSString formatterPriceSign:(offsetPrice/nLastClosePrice * 100)]
                                             WithDigits:2];
        profitSwap.text = [NSString stringWithFormat:@"%@%%",profitSwapStr];
    }else{
        [self resetLastClosePriceInfo];
    }
    
    if (canTrade) {
        if (offsetPrice < 0) {
            profit.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            profitSwap.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        }else if (offsetPrice == 0){
            profit.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
            profitSwap.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        }else{
            profit.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
            profitSwap.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        }
    }else{
        profit.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);//0x4b5667
        profitSwap.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4d5c77);//0x4b5667
    }
}

#pragma mark 还原昨收价和行情
- (void)resetQuotePrice
{
    currentPrice.text = @"--";
}

- (void)resetLastClosePriceInfo
{
    profit.text = @"--";
    profitSwap.text = @"--";
}


#pragma mark 背景图切换加动效的尝试
- (void) fadeIn: (UIView *) view
{
    if ( canAnimation ) {
        [UIView animateWithDuration:1.f animations:^{
            canAnimation = NO;
            view.alpha = 1;
        } completion:^(BOOL finished) {
            canAnimation = YES;
            [self fadeOut:view];
        }];
    }
}

- (void) fadeOut: (UIView *) view

{
    if ( canAnimation ) {
        [UIView animateWithDuration:.3f animations:^{
            canAnimation = NO;
            view.alpha = 0;
        } completion:^(BOOL finished) {
            canAnimation = YES;
        }];
    }
    
}


@end
