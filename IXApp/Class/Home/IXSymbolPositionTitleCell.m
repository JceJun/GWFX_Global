//
//  IXSymbolPositionTitleCell.m
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSymbolPositionTitleCell.h"
#import "NSString+FormatterPrice.h"
#import "IXAppUtil.h"

@interface IXSymbolPositionTitleCell ()

@property (nonatomic, strong) UILabel *profitTitleLbl;
@property (nonatomic, strong) UILabel *profitContentLbl;

@property (nonatomic, strong) UILabel *openPriceTitleLbl;
@property (nonatomic, strong) UILabel *openPriceContentLbl;

@property (nonatomic, strong) UILabel *positionNumTitleLbl;
@property (nonatomic, strong) UILabel *positionNumContentLbl;

@property (nonatomic, strong) UIImageView *logoImg;
@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation IXSymbolPositionTitleCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self profitTitleLbl];
    [self openPriceTitleLbl];
    [self positionNumTitleLbl];
    [self addSepLineImg];
}

- (void)addSepLineImg
{
    UIImageView *sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 5, kScreenWidth, kLineHeight)];
    sepLineImg.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0,0x242a36);
    [self.contentView addSubview:sepLineImg];
}
//格式化2位小数，红涨绿跌
- (void)setModel:(IXSymbolPosTitleM *)model
{
    NSMutableString *title = [NSMutableString stringWithString:LocalizedString(@"我的持仓")];
    if( model.displayName &&
       [model.displayName isKindOfClass:[NSString class]] &&
       [model.displayName length] ){
        [title appendFormat:@"(%@)",model.displayName];
    }
    self.positionNumTitleLbl.text = title;
    
    if( model.showTotNum && 0 < [model.showTotNum doubleValue] ){
 
        if (self.logoImg) {
            [self.logoImg removeFromSuperview];
        }
        if (self.tipLbl) {
            [self.tipLbl removeFromSuperview];
        }
        self.positionNumContentLbl.text = model.showTotNum;
        self.openPriceContentLbl.text = model.nPrice;
        NSString *profitStr = [NSString formatterPrice:model.profit WithDigits:2];
        if ( [model.profit doubleValue] < 0 ) {
            self.profitContentLbl.text = [NSString stringWithFormat:@"-%@",[IXAppUtil amountToString:[profitStr stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
            self.profitContentLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        }else if( [model.profit doubleValue] == 0){
            self.profitContentLbl.text = profitStr;
            self.profitContentLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
        }else{
            self.profitContentLbl.text = [NSString stringWithFormat:@"+%@",[IXAppUtil amountToString:[profitStr stringByReplacingOccurrencesOfString:@"+" withString:@""]]];
            self.profitContentLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        }
        
    }else{
        [self.logoImg dk_setImagePicker:DKImageNames(@"detail_position_default", @"detail_position_default_D")];
        self.tipLbl.text = LocalizedString(@"暂无持仓");
    }
}

- (UIImageView *)logoImg
{
    if ( !_logoImg ) {
        CGFloat originX = (kScreenWidth - 62)/2;
        _logoImg = [[UIImageView alloc] initWithFrame:CGRectMake( originX, 35, 62, 60)];
        [self addSubview:_logoImg];
    }
    return _logoImg;
}

- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, GetView_MaxY(self.logoImg) + 7, kScreenWidth, 22)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x99abba
                                   dTextColor:0x4c6072
                   ];
        [self addSubview:_tipLbl];
    }
    return _tipLbl;
}

- (UILabel *)positionNumTitleLbl
{
    if ( !_positionNumTitleLbl ) {
        _positionNumTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 10, 120, 14)
                                                  WithFont:PF_MEDI(10)
                                                 WithAlign:NSTextAlignmentLeft
                                                wTextColor:0x99abba
                                                dTextColor:0x8395a4];
        [self.contentView addSubview:_positionNumTitleLbl];
    }
    return _positionNumTitleLbl;
}


- (UILabel *)positionNumContentLbl
{
    if ( !_positionNumContentLbl ) {
        _positionNumContentLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 30, 120, 18)
                                                    WithFont:RO_REGU(18)
                                                   WithAlign:NSTextAlignmentLeft
                                                  wTextColor:0x4c6072
                                                  dTextColor:0xe9e9ea];
        [self.contentView addSubview:_positionNumContentLbl];
        _positionNumContentLbl.text = @"--";
    }
    return _positionNumContentLbl;
}

- (UILabel *)openPriceTitleLbl
{
    if ( !_openPriceTitleLbl ) {
        _openPriceTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 117, 10, 100, 14)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x99abba
                                              dTextColor:0x8395a4];
        [self.contentView addSubview:_openPriceTitleLbl];
        _openPriceTitleLbl.text = LocalizedString(@"均价");
    }
    return _openPriceTitleLbl;
}


- (UILabel *)openPriceContentLbl
{
    if ( !_openPriceContentLbl ) {
        _openPriceContentLbl = [IXUtils createLblWithFrame:CGRectMake( 117, 30, 110, 18)
                                                  WithFont:RO_REGU(18)
                                                 WithAlign:NSTextAlignmentLeft
                                                wTextColor:0x4c6072
                                                dTextColor:0xe9e9ea];
        [self.contentView addSubview:_openPriceContentLbl];
        _openPriceContentLbl.text = @"--";
    }
    return _openPriceContentLbl;
}

- (UILabel *)profitTitleLbl
{
    if ( !_profitTitleLbl ) {
        _profitTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 104, 10, kScreenWidth - 114, 14)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentRight
                                           wTextColor:0x99abba
                                           dTextColor:0x8395a4];
        [self.contentView addSubview:_profitTitleLbl];
        _profitTitleLbl.text = LocalizedString(@"盈亏金额");
    }
    return _profitTitleLbl;
}

- (UILabel *)profitContentLbl
{
    if ( !_profitContentLbl ) {
        _profitContentLbl = [IXUtils createLblWithFrame:CGRectMake( 104, 30, kScreenWidth - 114, 18)
                                               WithFont:RO_REGU(18)
                                              WithAlign:NSTextAlignmentRight
                                             wTextColor:0x4c6072
                                             dTextColor:0xe9e9ea];
        [self.contentView addSubview:_profitContentLbl];
        _profitContentLbl.text = @"--";
    }
    return _profitContentLbl;
}
@end
