//
//  IXOpenPrcCell.h
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXAccTextField.h"

typedef void(^add)();
typedef void(^step)();
typedef void(^substract)();

#define DASHBORADBTNTAG 1000

@interface IXOpenPrcCell : UITableViewCell

@property (nonatomic, strong) IXAccTextField *inputTF;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *inputContent;
@property (nonatomic, copy) NSString *inputExplain;

@property (nonatomic, copy) add addBlock;
@property (nonatomic, copy) add stepBlock;
@property (nonatomic, copy) substract substractBlock;

@end
