//
//  IXHoriKIndexV.h
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    IXHoriKIndexMain,
    IXHoriKIndexScnd
}IXHoriKIndexType;

typedef enum {
    IXKIndexMainMA,
    IXKIndexMainBBI,
    IXKIndexMainBOLL,
    IXKIndexMainMIKE,
    IXKIndexMainPBX
}IXKIndexMainType;

typedef enum {
    IXKIndexScndMACD,
    IXKIndexScndARBR,
    IXKIndexScndATR,
    IXKIndexScndBIAS,
    IXKIndexScndCCI,
    IXKIndexScndDKBY,
    IXKIndexScndKD,
    IXKIndexScndKDJ,
    IXKIndexScndLWR,
    IXKIndexScndQHLSR,
    IXKIndexScndRSI,
    IXKIndexScndWR
}IXKIndexScndType;

@interface IXHoriKIndexV : UIView

@property (nonatomic,copy)void(^idxBtnClicked)(IXHoriKIndexType type,NSInteger index);
@end

////////////////////////////////////////////

@interface IXHoriKIndexVCell : UITableViewCell

@property (nonatomic,strong)UILabel *title;

- (void)hilighTitle:(BOOL)hilight;

@end
