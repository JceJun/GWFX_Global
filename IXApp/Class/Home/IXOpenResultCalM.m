//
//  IXOpenResultCalM.m
//  IXApp
//
//  Created by Evn on 2017/8/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenResultCalM.h"
#import "IXOpenResultVC.h"
#import "IXOpenModel.h"
#import "IXTradeMarketModel.h"
#import "IXAccountBalanceModel.h"
#import "IXComCalM.h"

@implementation IXOpenResultCalM

+ (NSArray *)resetMarginAssetWithVolume:(double)volume
                               WithRoot:(IXOpenResultVC *)root
{
    NSString *symbolKey = [NSString stringWithFormat:@"%llu",root.model.symbolModel.id_p];
    NSInteger direction = 1;
    id subDic = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symbolKey];
    if ( subDic && [subDic isKindOfClass:[NSNumber class]] ) {
        direction = [subDic integerValue];
    }else if( subDic && [subDic isKindOfClass:[NSDictionary class]] ){
        NSDictionary *dic = [(NSDictionary *)subDic objectForKey:POSITIONMODEL];
        direction = [dic integerForKey:SWAPDIRECRION];
    }
    double swap =  [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.model.symbolModel.id_p
                                                                   WithOffsetPrice:-1];
    
    double marginSwap = [[IXAccountBalanceModel shareInstance] caculateMarginSwapWithSymbolId:root.model.symbolModel.id_p
                                                                                WithDirection:direction
                                                                              WithOffsetPrice:-1];

    if ( swap != 0 ) {
        //没有查找过才查询
        if ( root.openModel.commissionRate == 0 ) {
            root.openModel.commissionRate =  [IXRateCaculate queryComissionWithSymbolId:root.model.symbolModel.id_p
                                                                       WithSymbolCataId:root.model.symbolModel.cataId];
            root.openModel.cataId = root.model.symbolModel.cataId;
        }
        
        double marginSet = [[IXAccountBalanceModel shareInstance] marginCurrentRateWithVolume:volume
                                                                                   WithSymbol:root.model.symbolModel];
        double margin = 0;
        double commission = 0;
        
        double marginPrice = root.marginPrice;
        double commissionPrice = root.marginPrice;
       
        
        //计算保证金
        if(root.model.symbolModel.cataId == FXCATAID){
            margin = marginSwap * marginSet;
        }else{
            margin = swap * marginPrice * marginSet;
        }
        
        NSDecimalNumber *decMargin = [NSDecimalNumber decimalNumberWithString:
                                      [NSString stringWithFormat:@"%lf",margin]];
        decMargin = [decMargin decimalNumberByRoundingAccordingToBehavior:
                     [IXOpenResultCalM handlerPriceWithDigit:2]];
        
        root.model.margin = margin;
        
        //计算佣金，佣金可能为负；如果是负对用户来说是赚，需要重新计算汇率
        if( root.openModel.commissionRate < 0 ){
            swap = [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.model.symbolModel.id_p
                                                                   WithOffsetPrice:0];
        }
        
        if([IXAccountBalanceModel shareInstance].commissionType){
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:volume
                                   conSize:root.openModel.symbolModel.contractSizeNew];
        }else{
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:volume
                                      swap:swap
                                     price:commissionPrice];
        }
        NSDecimalNumber *decCommission = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",commission]];
        decCommission = [decCommission decimalNumberByRoundingAccordingToBehavior:
                         [IXOpenResultCalM handlerPriceWithDigit:2]];
        
        root.model.commission = commission;
        return @[decMargin,decCommission];
        
    }
    return nil;
}
 

+ (NSDecimalNumberHandler *)handlerPriceWithDigit:(NSInteger)digit
{
    NSDecimalNumberHandler *roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:digit
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
    
    return roundingBehavior;
}


@end
