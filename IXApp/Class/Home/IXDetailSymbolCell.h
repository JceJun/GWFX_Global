//
//  IXDetailSymbolCell.h
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXSymbolM.h"
#import "IXQuoteM.h"

@interface IXDetailSymbolCell : UITableViewCell

@property (nonatomic, strong) IXSymbolM    *symbolModel;
@property (nonatomic, strong) IXQuoteM *quoteModel;

@property (nonatomic, assign) double marketId;
@property (nonatomic, assign) double nLastClosePrice;
@property (nonatomic, assign) IXSymbolTradeState tradeState;

@end
