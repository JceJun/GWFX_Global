//
//  IXOpenTypeCell.m
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenTypeCell.h"
#import "IXOpenChoiceV.h"
#import "UIImageView+SepLine.h"

@interface IXOpenTypeCell ()

@property (nonatomic, strong) IXOpenChoiceV *typeChoice;
@property (nonatomic, strong) IXOpenChoiceV *expireChoice;

@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation IXOpenTypeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        CGRect currentFrame = CGRectMake( 0, 0, kScreenWidth/2 - 0.25, 43);
        UIView *leftView = [[UIView alloc] initWithFrame:currentFrame];
        leftView.backgroundColor = [UIColor clearColor];
        [self addSubview:leftView];
        leftView.tag = 1;
        
        UIImageView *sepLine = [[UIImageView alloc] initWithFrame:CGRectMake( kScreenWidth/2 - 0.25, 0, 0.5, 43)];
        sepLine.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:sepLine];
        
        currentFrame = CGRectMake( kScreenWidth/2 + 0.25, 0, kScreenWidth/2 - 0.25, 43);
        UIView *rightView = [[UIView alloc] initWithFrame:currentFrame];
        rightView.backgroundColor = [UIColor clearColor];
        [self addSubview:rightView];
        
        CGRect  frame = CGRectMake( 60, 0, kScreenWidth/2 - 60, 43);
        
        weakself;
        _typeChoice = [[IXOpenChoiceV alloc] initWithFrame:frame
                                                 WithTitle:MARKETORDER
                                                 WithAlign:TapChoiceAlignRight
                                                   WithTap:^{
                                                       [weakSelf responseToTypeGes:nil];
                                                   }];
        [leftView addSubview:_typeChoice];
        
        
        _expireChoice = [[IXOpenChoiceV alloc] initWithFrame:frame
                                                   WithTitle:LocalizedString(@"当日")
                                                   WithAlign:TapChoiceAlignRight
                                                     WithTap:^{
                                                         [weakSelf responseToExpireGes:nil];
                                                     }];
        [rightView addSubview:_expireChoice];
        _expireChoice.hidden = YES;
        
        currentFrame = CGRectMake( 15, 14, kScreenWidth/2, 15);
        UILabel *leftTitle = [IXUtils createLblWithFrame:currentFrame
                                                WithFont:PF_MEDI(13)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x99abba
                                              dTextColor:0x8395a4];
        [leftView addSubview:leftTitle];
        leftTitle.text = LocalizedString(@"类型");
        
        UILabel *rightTitle = [IXUtils createLblWithFrame:currentFrame
                                                 WithFont:PF_MEDI(13)
                                                WithAlign:NSTextAlignmentLeft
                                               wTextColor:0x99abba
                                               dTextColor:0x8395a4];
        [rightView addSubview:rightTitle];
        rightTitle.text = LocalizedString(@"有效期");
        
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 14, kScreenWidth/2 - 14, 16)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentRight
                                   wTextColor:0x99abba
                                   dTextColor:0x8395a4];
        
        [rightView addSubview:_tipLbl];
        _tipLbl.text = LocalizedString(@"长期");
    }
    return self;
}

- (void)resetExpireArrow
{
    [self.expireChoice resetArrow];
}

- (void)resetKindArrow
{
    [self.typeChoice resetArrow];
}

- (void)responseToTypeGes:(UITapGestureRecognizer *)tapGes
{
    if (self.tradeCon) {
        self.tradeCon(Trade_kind,_typeChoice.arrowImg);
    }
}

- (void)responseToExpireGes:(UITapGestureRecognizer *)tapGes
{
    if (self.tradeCon) {
        self.tradeCon(Trade_expire,_expireChoice.arrowImg);
    }
}

- (void)setTradeCon:(chooseTrade)tradeCon
{
    _tradeCon = tradeCon;
}

- (void)setKind:(NSString *)kind
{
    _kind = kind;
    [self updateUIWithKind:kind];
    [_typeChoice resetTitle:kind];
}

- (void)setExpire:(NSString *)expire
{
    _expire = expire;
    [_expireChoice resetTitle:expire];
}

- (void)updateUIWithKind:(NSString *)kind
{
    _expireChoice.hidden = [kind isEqualToString:MARKETORDER];
    _tipLbl.hidden = ![kind isEqualToString:MARKETORDER];
}

@end
