//
//  IXOpenEptPrcCell.m
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenEptPrcCell.h"

@interface IXOpenEptPrcCell ()

@property (nonatomic, strong) UILabel *titleLbl;

@end

@implementation IXOpenEptPrcCell

- (void)setTitle:(NSString *)title
{
    if ( title && title.length ) {
        self.titleLbl.text = title;
    }
}

- (void)setExpectPrice:(NSString *)expectPrice
{
    if ( expectPrice && expectPrice ) {
        self.expectDealPrcLbl.text = expectPrice;
    }
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 14, kScreenWidth - 15, 15)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x99abba
                                     dTextColor:0x8395a4];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}


- (UILabel *)expectDealPrcLbl
{
    if ( !_expectDealPrcLbl ) {
        _expectDealPrcLbl = [IXUtils createLblWithFrame:CGRectMake( 104, 13, kScreenWidth - 192, 18)
                                               WithFont:RO_REGU(18)
                                              WithAlign:NSTextAlignmentCenter
                                             wTextColor:0x99abba
                                             dTextColor:0x8395a4];
        [self.contentView addSubview:_expectDealPrcLbl];
    }
    return _expectDealPrcLbl;
}
@end
