//
//  IXTradeDeepV.m
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeDeepV.h"

@interface IXTradeDeepV ()

@property (nonatomic, strong) UILabel *buyCountLbl;
@property (nonatomic, strong) UILabel *buyPrcLbl;
@property (nonatomic, strong) UILabel *buyVolLbl;
@property (nonatomic, strong) UILabel *sellCountLbl;
@property (nonatomic, strong) UILabel *sellPrcLbl;
@property (nonatomic, strong) UILabel *sellVolLbl;

@end

@implementation IXTradeDeepV

- (id)initWithFrame:(CGRect)frame WithCount:(NSInteger)count
{
    self = [super initWithFrame:frame];
    if (self) {
        
        frame.origin = CGPointZero;
        frame.size.width = kScreenWidth/2;
        
        UIView *buyBackView = [[UIView alloc] initWithFrame:frame];
        [self addSubview:buyBackView];
   
        NSString *buyImgName = @"";
        NSString *sellImgName = @"";
        
        if ( [IXUserInfoMgr shareInstance].isNightMode ) {
            buyImgName = [IXUserInfoMgr shareInstance].settingRed ? @"quoteCell_than_dark" : @"quoteCell_less_f_dark";
            sellImgName =  [IXUserInfoMgr shareInstance].settingRed ? @"quoteCell_less_dark" : @"quoteCell_than_f_dark";
        }else{
            buyImgName =  [IXUserInfoMgr shareInstance].settingRed ? @"deepPrc_gradientRed" : @"deepPrc_gradientGreen_f";
            sellImgName =  [IXUserInfoMgr shareInstance].settingRed ? @"deepPrc_gradientGreen" : @"deepPrc_gradientRed_f";
        }

        UIImageView *buyBackImg = [[UIImageView alloc] initWithFrame:buyBackView.bounds];
        buyBackImg.image = [UIImage imageNamed:buyImgName];
        [buyBackView addSubview:buyBackImg];
 
        UIView *sellBackView = [[UIView alloc] initWithFrame:CGRectMake( kScreenWidth/2, 0, kScreenWidth/2, CGRectGetHeight(frame))];
        [self addSubview:sellBackView];
    
        UIImageView *sellBackImg = [[UIImageView alloc] initWithFrame:buyBackView.bounds];
        sellBackImg.image = [UIImage imageNamed:sellImgName];
        [sellBackView addSubview:sellBackImg];
        
        CGFloat originY = (frame.size.height - 10)/2;
        CGRect countFrame = CGRectMake( 15, originY, 10, 10);
        _sellCountLbl = [IXUtils createLblWithFrame:countFrame
                                           WithFont:RO_REGU(8)
                                          WithAlign:NSTextAlignmentCenter
                                         wTextColor:0xffffff
                                         dTextColor:0x22262e];
        [buyBackView addSubview:_sellCountLbl];
        _sellCountLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
        _sellCountLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor,mDRedPriceColor);
        
        CGRect prFrame = CGRectMake( 30, 0, kScreenWidth/2 - 40, CGRectGetHeight(frame));
        _sellPrcLbl = [IXUtils createLblWithFrame:prFrame
                                         WithFont:RO_REGU(12)
                                        WithAlign:NSTextAlignmentLeft
                                       wTextColor:mRedPriceColor
                                       dTextColor:mDRedPriceColor];
        [buyBackView addSubview:_sellPrcLbl];
        
        _sellVolLbl = [IXUtils createLblWithFrame:prFrame
                                         WithFont:RO_REGU(12)
                                        WithAlign:NSTextAlignmentRight
                                       wTextColor:0x4c6072
                                       dTextColor:0x8395a4];
        [buyBackView addSubview:_sellVolLbl];
 
        _buyCountLbl = [IXUtils createLblWithFrame:countFrame
                                          WithFont:RO_REGU(8)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0xffffff
                                        dTextColor:0x22262e];
        [sellBackView addSubview:_buyCountLbl];
        _buyCountLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
        _buyCountLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor,mDGreenPriceColor);

        _buyPrcLbl = [IXUtils createLblWithFrame:prFrame
                                        WithFont:RO_REGU(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:mGreenPriceColor
                                      dTextColor:mDGreenPriceColor];
        [sellBackView addSubview:_buyPrcLbl];
 
        _buyVolLbl = [IXUtils createLblWithFrame:prFrame
                                        WithFont:RO_REGU(12)
                                       WithAlign:NSTextAlignmentRight
                                      wTextColor:0x4c6072
                                      dTextColor:0x8395a4];
        [sellBackView addSubview:_buyVolLbl];
    }
    return self;
}

- (void)setBuyPrc:(NSString *)buyPrc
           BuyVol:(NSString *)buyVol
          SellPrc:(NSString *)sellPrc
          SellVol:(NSString *)sellVol
{
    if ([buyVol floatValue] > 0 && [buyVol floatValue] < 1000) {
        buyVol = [NSString stringWithFormat:@"%.f",[buyVol floatValue]];
    } else if([buyVol floatValue]>=1000.f&&[buyVol floatValue]<1000000.f){
        buyVol = [NSString stringWithFormat:@"%.1fK",[buyVol floatValue]/1000.f];
    }
    else if([buyVol floatValue]>=1000000.f){
        buyVol = [NSString stringWithFormat:@"%.1fM",[buyVol floatValue]/1000000.f];
    }
    
    if ([sellVol floatValue] > 0 && [sellVol floatValue] < 1000) {
        sellVol = [NSString stringWithFormat:@"%.f",[sellVol floatValue]];
    } if([sellVol floatValue]>=1000.f&&[sellVol floatValue]<1000000.f){
        sellVol = [NSString stringWithFormat:@"%.1fK",[sellVol floatValue]/1000.f];
    }
    else if([sellVol floatValue]>=1000000.f){
        sellVol = [NSString stringWithFormat:@"%.1fM",[sellVol floatValue]/1000000.f];
    }
    
    _buyPrcLbl.text = buyPrc;
    _buyVolLbl.text = buyVol;
    _sellPrcLbl.text = sellPrc;
    _sellVolLbl.text = sellVol;
}

- (void)setDeepCount:(NSInteger)count
{
    _buyCountLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    _sellCountLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
}

@end
