//
//  IXDomResultCell.h
//  IXApp
//
//  Created by Bob on 2017/3/23.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDomResultCell : UITableViewCell


@property (nonatomic, strong) NSString *leftContent;

@property (nonatomic, strong) NSString *rightContent;

@property (nonatomic, strong) UIFont *rightFont;

@end
