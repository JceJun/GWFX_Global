//
//  IXHoriKTypeV.h
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXKLineType.h"

@interface IXHoriKTypeV : UIView

@property (nonatomic,copy)void(^ktypeBtnClicked)(NSInteger tag);
@property (nonatomic,assign)IXKLineType   selectedType;

@end
