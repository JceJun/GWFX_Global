//
//  IXOpenChoiceV.m
//  IXApp
//
//  Created by Bob on 2016/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOpenChoiceV.h"
#import "UIImageView+SepLine.h"

@interface IXOpenChoiceV ()

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) UILabel *contentLbl;

@property (nonatomic, assign) CGFloat contentWidth;

@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) tapOpenChoice tapChoose;

@property (nonatomic, assign) TapChoiceAlign choiceAlign;

@property (nonatomic, assign) BOOL arrowUp;
@end

@implementation IXOpenChoiceV

- (id)initWithFrame:(CGRect)frame
          WithTitle:(NSString *)title
          WithAlign:(TapChoiceAlign)align
            WithTap:(tapOpenChoice)choose
{
    self = [super initWithFrame:frame];
    if ( self ) {
        _backView = [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:_backView];
        
        _arrowUp = NO;
        _tapChoose = choose;
        _choiceAlign = align;
        
        _content = title;
        _contentWidth = [self getWidth];
        
        [self resetViewContent];
    }
    return self;
}

- (void)resetArrow
{
    [self resetArrowImg];
}

- (void)responseTapGes:(UITapGestureRecognizer *)tapGes
{
    [self resetContent];
    [self responseChain];
}

- (void)resetViewContent
{
    [_backView addSubview:self.contentLbl];
    [_backView addSubview:self.arrowImg];
    
    [self refreashFrame];
    
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                             action:@selector(responseTapGes:)];
    [self addGestureRecognizer:tapGes];
}

- (void)resetContent
{
    if ( _choiceAlign == TapChoiceAlignCenter ) {
        _contentLbl.dk_textColorPicker = DKCellTitleColor;
    }
}

- (void)responseChain
{
    [self resetArrowImg];
    
    if( self.tapChoose ){
        self.tapChoose();
    }
}

- (void)resetArrowImg
{
    _arrowUp = !_arrowUp;
    if ( _choiceAlign == TapChoiceAlignDefault || _choiceAlign == TapChoiceAlignRightOutImg ) {
        if( _arrowUp ){
            _arrowImg.dk_imagePicker = DKImageNames(@"positionModify_down", @"positionModify_down_dark");
        }else{
            _arrowImg.dk_imagePicker = DKImageNames(@"positionModify_up", @"positionModify_up_dark");
        }
        _contentLbl.text = ( _arrowUp ? LocalizedString(@"收起市场深度") :
                                        LocalizedString(@"展开市场深度") );
    }else if( _choiceAlign == TapChoiceAlignCenter ){
        _arrowImg.dk_imagePicker = DKImageNames(@"common_cell_choose", @"common_cell_choose_D");
    }else{
        if( _arrowUp ){
            _arrowImg.dk_imagePicker = DKImageNames(@"common_arrow_up", @"common_arrow_up_D");
        }else{
            _arrowImg.dk_imagePicker = DKImageNames(@"common_arrow_down", @"common_arrow_down_D");
        }
    }
}

- (void)hightedState:(BOOL)state
{
    _contentLbl.dk_textColorPicker = state ? DKCellTitleColor : DKCellContentColor;
    [_arrowImg setHidden:!state];
}

- (void)resetTitle:(NSString *)title
{
    _content = title;
    _contentWidth = [self getWidth];
    
    self.contentLbl.text = _content;
    [self refreashFrame];
}

- (void)refreashFrame
{
    CGRect frame = _backView.frame;
    if ( _choiceAlign == TapChoiceAlignDefault ) {
        frame.size.height = 12;
    }else{
        frame.size.height = 17;
    }
    frame.size.width = _contentWidth + 24;
    _backView.frame = frame;
    
    frame = _contentLbl.frame;
    frame.size.width = _contentWidth;
    _contentLbl.frame = frame;
    
    frame = _arrowImg.frame;
    frame.origin.x = CGRectGetMaxX(_contentLbl.frame) + 5;
    _arrowImg.frame = frame;
    
    CGPoint center = _backView.center;
    switch ( _choiceAlign ) {
        case TapChoiceAlignDefault:{
            center.x  = self.center.x;
            center.y = CGRectGetHeight(self.frame)/2;
            _arrowImg.dk_imagePicker = DKImageNames(@"positionModify_up", @"positionModify_up_dark");
        }
            break;
        case TapChoiceAlignRight:{
            center.y = CGRectGetHeight(self.frame)/2;
            center.x  = CGRectGetWidth(self.frame) - _contentWidth/2 - 15;
            _arrowImg.dk_imagePicker = DKImageNames(@"common_arrow_down", @"common_arrow_down_D");
        }
            break;
        case TapChoiceAlignCenter:{
            center.x = kScreenWidth/2 + 12;
            center.y = CGRectGetHeight(self.frame)/2;
            
            frame = _arrowImg.frame;
            frame.origin.y = -2;
            frame.size.width = 18;
            frame.size.height = 18;
            _arrowImg.frame = frame;
            _arrowImg.dk_imagePicker = DKImageNames(@"common_cell_choose", @"common_cell_choose_D");
        }
            break;
        case TapChoiceAlignRightOutImg:{
            center.x  = self.frame.size.width/2;
            center.y = CGRectGetHeight(self.bounds)/2;
            _arrowImg.dk_imagePicker = DKImageNames(@"positionModify_up", @"positionModify_up_dark");
        }
            break;
        default:
            break;
    }
    _backView.center = center;
}

- (UILabel *)contentLbl
{
    if ( !_contentLbl ) {
        if ( _choiceAlign == TapChoiceAlignDefault ) {
            _contentLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 0, _contentWidth, 12)
                                             WithFont:PF_MEDI(12)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0xa7adb5
                                           dTextColor:0xe9e9ea];
        }else{
            _contentLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 0, _contentWidth, 17)
                                             WithFont:PF_MEDI(13)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0x4c6072
                                          dTextColor:0xe9e9ea];
        }
        _contentLbl.text = _content;
    }
    return _contentLbl;
}


- (UIImageView *)arrowImg
{
    if ( !_arrowImg ) {
        CGFloat originY = (_choiceAlign == TapChoiceAlignDefault) ? 5 : 6.5;
        _arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake( _contentWidth + 6, originY, 8, 4)];
    }
    return _arrowImg;
}


- (CGFloat)getWidth
{
    if ( _choiceAlign == TapChoiceAlignDefault ) {
        return [IXEntityFormatter getContentWidth:_content WithFont:PF_MEDI(12)];
    }
    return [IXEntityFormatter getContentWidth:_content WithFont:PF_MEDI(13)];
}
@end
