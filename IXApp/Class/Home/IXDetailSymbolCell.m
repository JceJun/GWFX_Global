//
//  IXDetailSymbolCell.m
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDetailSymbolCell.h"
#import "UILabel+LineSpace.h"
#import "NSString+FormatterPrice.h"
#import "UIImageView+SepLine.h"

@interface IXDetailSymbolCell ()
{
    double prePrice;
}

@property (nonatomic, strong) UILabel *symbolNameLbl;

@property (nonatomic, strong) UILabel *symbolCodeLbl;

@property (nonatomic, strong) UILabel *tradeStatusLbl;

@property (nonatomic, strong) UILabel *tradeTimeLbl;

@property (nonatomic, strong) UILabel *currentPriceLbl;

@property (nonatomic, strong) UILabel *profitSwapLbl;

@property (nonatomic, strong) UILabel *profitLbl;

@property (nonatomic, strong) UIImageView  *dLine;

@property (nonatomic, assign) float digits;

@end

@implementation IXDetailSymbolCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x242a36);
        
        CGFloat orginX = 15;
        CGFloat width = [IXEntityFormatter getContentWidth:_symbolNameLbl.text WithFont:PF_MEDI(15)] + 5;
        _symbolNameLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, 16, 140, 22)
                                            WithFont:PF_MEDI(15)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0x4c6072
                                          dTextColor:0xe9e9ea];
        [self.contentView addSubview:_symbolNameLbl];
        
        orginX += width;
        CGFloat orginY = 23;
        _symbolCodeLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 140, 10)
                                            WithFont:RO_REGU(10)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0xa7adb5
                                          dTextColor:0x8395a4];
        [self.contentView addSubview:_symbolCodeLbl];
        
        orginX = 15;
        orginY = 41;
        width = [IXEntityFormatter getContentWidth:LocalizedString(@"休假中") WithFont:PF_MEDI(10)] + 3;
        _tradeStatusLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, (int)width, 15)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0xffffff
                                           dTextColor:0xe9e9ea];
        [self.contentView addSubview:_tradeStatusLbl];
        _tradeStatusLbl.text = LocalizedString(@"交易中");
        _tradeStatusLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xa3aebb, 0x4c6072);
        _tradeStatusLbl.layer.masksToBounds = YES;
        _tradeStatusLbl.layer.cornerRadius = 1;
        
        orginX = width + 20;
        orginY = 43;
        _tradeTimeLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 140, 10)
                                           WithFont:RO_REGU(10)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0xa7adb5
                                         dTextColor:0x8395a4];
        [self.contentView addSubview:_tradeTimeLbl];
        
        orginX = kScreenWidth - 58;
        orginY = 25;
        _profitSwapLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 58, 12)
                                            WithFont:RO_REGU(12)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0x99abba
                                          dTextColor:0x8395a4];
        [self.contentView addSubview:_profitSwapLbl];
        
        orginY += 15;
        _profitLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 58, 12)
                                        WithFont:RO_REGU(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_profitLbl];
        
        _currentPriceLbl = [IXUtils createLblWithFrame:CGRectZero
                                              WithFont:RO_REGU(40)
                                             WithAlign:NSTextAlignmentRight
                                            wTextColor:0x99abba
                                            dTextColor:0x8395a4];
        [self.contentView addSubview:_currentPriceLbl];
        
        _dLine = [UIImageView addSepImageWithFrame:CGRectMake(0, 73 - kLineHeight, kScreenWidth, kLineHeight)
                                                       WithColor:CellSepLineColor
                                                       WithAlpha:1.f];
        [self.contentView addSubview:_dLine];
    }
    return self;
}

- (void)setSymbolModel:(IXSymbolM *)symbolModel
{
    _symbolModel = symbolModel;
    _digits = symbolModel.digits;
    
    _symbolNameLbl.text = symbolModel.languageName;
    CGRect frame = _symbolNameLbl.frame;
    frame.size.width = [IXEntityFormatter getContentWidth:_symbolNameLbl.text WithFont:_symbolNameLbl.font];
    _symbolNameLbl.frame = frame;
}

- (void)setMarketId:(double)marketId
{
    _marketId = marketId;
    
    NSString *content = [NSString stringWithFormat:@"%@:%@",_symbolModel.name,[IXEntityFormatter getMarketNameWithMarketId:marketId]];
    CGRect frame = _symbolCodeLbl.frame;
    frame.origin.x = CGRectGetMaxX(_symbolNameLbl.frame) + 5;
    frame.size.width = [IXEntityFormatter getContentWidth:content WithFont:_symbolCodeLbl.font];
    _symbolCodeLbl.frame = frame;
    _symbolCodeLbl.text = content;
}

- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price WithDigits:_digits];
}

- (void)setQuoteModel:(IXQuoteM *)quoteModel
{
    _quoteModel = quoteModel;
    if ( [IXDataProcessTools isNormalByState:_tradeState] ) {
        if ( quoteModel.nPrice -  prePrice < 0 ) {
            _currentPriceLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        }else if (  quoteModel.nPrice - prePrice == 0 ){
            _currentPriceLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8894a3);
        }else{
            _currentPriceLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        }
    }else{
       _currentPriceLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4b5667);
    }
    prePrice = quoteModel.nPrice;
    _tradeTimeLbl.text = [NSString stringWithFormat:@"%@",
                          [IXEntityFormatter timeIntervalToString:quoteModel.n1970Time]];

    [self updateProfit:quoteModel.nPrice];
    
    if ( quoteModel.nPrice != 0 ) {
        _currentPriceLbl.text = [self formatterPrice:
                                 [NSString stringWithFormat:@"%lf",quoteModel.nPrice]];
    }else{
        _currentPriceLbl.text = @"--";
    }

    [self setNeedsLayout];
}

- (void)setNLastClosePrice:(double)nLastClosePrice
{
    _nLastClosePrice = nLastClosePrice;
    [self updateProfit:_quoteModel.nPrice];
}

- (void)setTradeState:(IXSymbolTradeState)tradeState
{
    _tradeState = tradeState;
    _tradeStatusLbl.text = [IXRateCaculate symbolTradeState:tradeState];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat originX = ( CGRectGetMaxX(_symbolCodeLbl.frame) > kScreenWidth/2) ?
                            CGRectGetMaxX(_symbolCodeLbl.frame) :
                            kScreenWidth/2;
    
    CGRect frame = _currentPriceLbl.frame;
    frame.origin.x = originX;
    frame.origin.y = 20;
    frame.size.width =  kScreenWidth - originX - 68;
    frame.size.height = 33;
    _currentPriceLbl.frame = frame;
    [_currentPriceLbl setAdjustsFontSizeToFitWidth:YES];
}


- (void)updateProfit:(double)price
{
    if ( _nLastClosePrice == 0 || price == 0 ) {
        _profitLbl.text = @"--";
        _profitSwapLbl.text = @"--";
        [self resetColor:0];
    }else{
        double offsetPrice = price - _nLastClosePrice;
        NSString *profitSwapStr = [NSString formatterPrice:[NSString formatterPriceSign:(offsetPrice/_nLastClosePrice * 100)]
                                                WithDigits:2];
        _profitSwapLbl.text = [NSString stringWithFormat:@"%@%%",profitSwapStr];
        [UILabel changeWordSpaceForLabel:_profitSwapLbl WithSpace:-0.21];
        
        _profitLbl.text = [NSString formatterPrice:[NSString formatterPriceSign:offsetPrice]
                                        WithDigits:_digits];
        [_profitLbl adjustsFontSizeToFitWidth];
        [UILabel changeWordSpaceForLabel:_profitLbl WithSpace:-0.21];
        
        [self resetColor:offsetPrice];
    }
}


- (void)resetColor:(double)offsetPrice
{
    if ( [IXDataProcessTools isNormalByState:_tradeState] ) {
        if ( offsetPrice < 0 ) {
            _profitLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            _profitSwapLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            
        }else if( offsetPrice == 0 ){
            _profitLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8894a3);
            _profitSwapLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8894a3);
        }else{
            _profitLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor,mDRedPriceColor);
            _profitSwapLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor,mDRedPriceColor);
        }
    }else{
        _profitLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4b5667);
        _profitSwapLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4b5667);
    }
}
@end
