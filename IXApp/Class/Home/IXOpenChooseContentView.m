//
//  IXOpenChooseContentView.m
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenChooseContentView.h"
#import "NSString+IX.h"
@interface IXOpenChooseContentView ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) UIView  *backImgView;


@property (nonatomic, copy) selectRow chooseContent;

@property (nonatomic, strong) UITapGestureRecognizer    * tapGes;

@end


@implementation IXOpenChooseContentView

- (id)initWithDataSource:(NSArray *)dataSource WithSelectedRow:(selectRow)chooseRow
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self) {
        _dataSourceArr = [NSMutableArray arrayWithArray:dataSource];
        _chooseContent = chooseRow;
        
        [self addSubview:self.backImgView];
        [self addSubview:self.tableV];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)resetDataSourceWithData:(NSArray *)dataSource
{
    self.dataSourceArr = [NSMutableArray arrayWithArray:dataSource];
    
    [self setNeedsLayout];
}

- (void)setDataSourceArr:(NSMutableArray *)dataSourceArr
{
    _dataSourceArr = [dataSourceArr mutableCopy];
    [self.tableV reloadData];
}

- (void)show
{
    CGSize  size = self.tableV.frame.size;
    CGRect  aimFrame = CGRectMake(0,
                                  kScreenHeight - size.height - kBtomMargin,
                                  size.width,
                                  size.height);
    CGRect  beginFrame = CGRectMake(0, kScreenHeight, aimFrame.size.width, aimFrame.size.height);
    self.tableV.frame = beginFrame;
    
    UIWindow    * w = [UIApplication sharedApplication].keyWindow;
    [w addSubview:self];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
        self.tableV.frame = aimFrame;
    } completion:nil];
}

- (void)dismiss
{
    CGRect  beginFrame = self.tableV.frame;
    CGRect  aimFrame = CGRectMake(0, kScreenHeight, beginFrame.size.width, beginFrame.size.height);
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.tableV.frame = aimFrame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat height = 45 * (_dataSourceArr.count + 1) + 5;
    CGRect frame = _tableV.frame;
    frame.origin.y = kScreenHeight - height - kBtomMargin;
    frame.size.height = height;
    _tableV.frame = frame;
    
    [_tableV reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return _dataSourceArr.count;
        default:
            return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return 45;
        default:
            return 47;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 5;
        default:
            return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ( section == 0 ) {
        UIView *footView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 5)];
        footView.dk_backgroundColorPicker = DKTableColor;
        return footView;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             NSStringFromClass([UITableViewCell class])];
    cell.contentView.dk_backgroundColorPicker = DKNavBarColor;
    cell.tag = indexPath.row;
    
    if ( 1 == indexPath.section ) {
        UIImageView *bottomImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 1)];
        bottomImg.dk_backgroundColorPicker = DKLineColor;
        [cell.contentView addSubview:bottomImg];
    }else if( 0 == indexPath.section ){
        UIImageView *sepLineImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 44, kScreenWidth, 1)];
        sepLineImg.dk_backgroundColorPicker = DKLineColor;
        [cell.contentView addSubview:sepLineImg];
    }
    
    cell.textLabel.font = PF_MEDI(13);
    cell.textLabel.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    switch (indexPath.section) {
        case 0:{
            cell.textLabel.text = self.dataSourceArr[indexPath.row];
            NSString *name = self.dataSourceArr[indexPath.row];
            NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:name];
            
            NSString *content =  [name subStringFrom:@"[" to:@"]"];
            UIFont *font = ROBOT_FONT(12);
            UIColor *color = HexRGB(0x99abba);
            
            NSRange range = [name rangeOfString:content];
            [AttributedStr addAttribute:NSFontAttributeName
                                  value:font
                                  range:range];
            
            [AttributedStr addAttribute:NSForegroundColorAttributeName
                                  value:color
                                  range:range];
            cell.textLabel.text = @"";
            cell.textLabel.attributedText = AttributedStr;
            
            if (_cntFont) {
                cell.textLabel.font = _cntFont;
            }
        }break;
        default:
            cell.textLabel.text = LocalizedString(@"取消");
            break;
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger result = -1;
    switch (indexPath.section) {
        case 0:
            result = indexPath.row;
            break;
        default:
            result = -1;
            break;
    }
    
    [self responseToClick:result];
}

- (UITableView *)tableV
{
    if (!_tableV) {
        CGFloat height = 45 * (_dataSourceArr.count + 1) + 5;
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake( 0, kScreenHeight - height - kBtomMargin, kScreenWidth, height)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV registerClass:[UITableViewCell class]
        forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        _tableV.scrollEnabled = NO;
    }
    return _tableV;
}

- (UIView *)backImgView
{
    if (!_backImgView) {
        _backImgView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight - kBtomMargin, kScreenWidth, kBtomMargin)];
        _backImgView.dk_backgroundColorPicker = DKTableColor;
    }
    return _backImgView;
}

- (void)responseToClick:(NSInteger)row
{
    if (self.chooseContent && row >= 0) {
        self.chooseContent(row);
    }
    [self dismiss];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if ( self.endEditBlock ) {
        self.endEditBlock();
    }
    [self dismiss];
}

@end

