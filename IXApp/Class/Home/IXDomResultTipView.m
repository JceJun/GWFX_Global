//
//  IXDomResultTipView.m
//  IXApp
//
//  Created by Bob on 2017/3/23.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDomResultTipView.h"
#import "IXDomResultCell.h"
#import "IXUserDefaultM.h"
#import "IXCpyConfig.h"

#define MARKBTNTAG 100

@interface IXDomResultTipView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *headView;

@property (nonatomic, strong) UIView *footView;

@property (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) UIImageView *statusImg;

@property (nonatomic, strong) UITableView *contentTV;


@property (nonatomic, assign) BOOL markStatus;

@property (nonatomic, strong) UIView *contentView;

@end

@implementation IXDomResultTipView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if ( self ) {

        _markStatus = NO;
        UIImageView *backImg = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:backImg];
        backImg.backgroundColor = [UIColor blackColor];
        backImg.alpha = 0.6f;
        
        [self.contentView addSubview:self.headView];
        [self.contentView addSubview:self.contentTV];
        [self.contentView addSubview:self.footView];

    }
    return self;
}


- (void)removeFromSuperview
{
    [super removeFromSuperview];
    
    if ( _markStatus ) {
        UIButton *markBtn = (UIButton *)[_footView viewWithTag:MARKBTNTAG];
        [self responseToMark:markBtn];
    }

}

- (void)setContentArr:(NSArray *)contentArr
{
    _contentArr = contentArr;
    [self.contentTV reloadData];
}

- (void)responseToMark:(UIButton *)sender
{
    _markStatus = !_markStatus;
    if ( _markStatus ) {
        [sender dk_setImage:DKImageNames(@"common_cell_choose", @"common_cell_choose_D")
                   forState:UIControlStateNormal];
    }else{
        [sender dk_setImage:DKImageNames(@"common_cell_unchoose", @"common_cell_unchoose_D")
                   forState:UIControlStateNormal];
    }
}

- (void)responseToSure
{
    [IXUserDefaultM saveDomResult:!_markStatus];
    [self removeFromSuperview];
    
    if ( self.submitBlock ) {
        self.submitBlock();
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row != self.titleArr.count - 1 ) {
        return 30;
    }
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDomResultCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDomResultCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.tag = indexPath.row;
    
    cell.leftContent = _titleArr[indexPath.row];
    
    if ( _contentArr && _contentArr.count > indexPath.row ) {
        cell.rightContent = _contentArr[indexPath.row];
    }
    if ([cell.leftContent isEqualToString:LocalizedString(@"请求价")] ||
        [cell.leftContent isEqualToString:LocalizedString(@"数量")]) {
        [cell setRightFont:RO_REGU(15)];
    } else {
        [cell setRightFont:PF_MEDI(13)];
    }
    return cell;
}


- (UIImageView *)statusImg
{
    if ( !_statusImg ) {
        _statusImg = [[UIImageView alloc] initWithFrame:CGRectMake( 116, 40, 80, 80)];
        _statusImg.dk_imagePicker = DKImageNames(@"openAccount_complete", @"openAccount_complete_D");
    }
    return _statusImg;
}

- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 140, 312, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea];
        _tipLbl.text = LocalizedString(@"订单提交");
    }
    return _tipLbl;
}



- (UIView *)contentView
{
    if ( !_contentView ) {
        
        double originX = (kScreenWidth - 312)/2;
        double originY = (kScreenHeight - 440)/2;
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(originX, originY, 312, 440)];
        _contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        [self addSubview:_contentView];
        _contentView.layer.masksToBounds = YES;
        _contentView.layer.cornerRadius = 3;
    }
    return _contentView;
}

- (UIView *)headView
{
    if ( !_headView ) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, 312, 175)];
        
        NSString *lineStr = ([IXUserInfoMgr shareInstance].isNightMode) ? @"domTip_line_dark" : @"domTip_line";
        UIImageView *tipImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, 312, 3)];
        tipImg.image = [[UIImage imageNamed:lineStr] resizableImageWithCapInsets:UIEdgeInsetsMake( 2, 5, 2, 5)];
        [_headView addSubview:tipImg];
        
        NSString *con =  LocalizedString(@"提醒");
        NSInteger width = [IXEntityFormatter getContentWidth:con WithFont:PF_MEDI(13)] + 21;
        
        NSString *conStr = ([IXUserInfoMgr shareInstance].isNightMode) ? @"domTip_con_dark" : @"domTip_con";
        UIImageView *conImg = [[UIImageView alloc] initWithFrame:CGRectMake( 18, 3, width, 22)];
        conImg.image = [[UIImage imageNamed:conStr] resizableImageWithCapInsets:UIEdgeInsetsMake( 5, 5, 5, 5)];
        [_headView addSubview:conImg];
        
        UILabel *tipContent = [IXUtils createLblWithFrame:CGRectMake( 18, 2, width, 24)
                                                 WithFont:PF_MEDI(13)
                                                WithAlign:NSTextAlignmentCenter
                                               wTextColor:0xffffff
                                               dTextColor:0x262f3e];
        [_headView addSubview:tipContent];
        tipContent.text = con;
        
        [_headView addSubview:self.statusImg];
        [_headView addSubview:self.tipLbl];
    }
    return _headView;
}

- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:CGRectMake( 22, 175, 272, 135)];
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _contentTV.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x303b4d);

        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.layer.masksToBounds = YES;
        _contentTV.layer.cornerRadius = 3;
        [_contentTV registerClass:[IXDomResultCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDomResultCell class])];
    }
    return _contentTV;
}


- (UIView *)footView
{
    if ( !_footView ) {
        _footView = [[UIView alloc] initWithFrame:CGRectMake( 0, 310, 312, 130)];
        
        UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sureBtn.frame = CGRectMake( 22, 20, 268, 40);
        sureBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);

        [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sureBtn setTitle:LocalizedString(@"确定")
                 forState:UIControlStateNormal];
        [_footView addSubview:sureBtn];
        sureBtn.layer.masksToBounds = YES;
        sureBtn.layer.cornerRadius = 3;
        [sureBtn addTarget:self
                    action:@selector(responseToSure)
          forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *markBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        markBtn.frame = CGRectMake( 13, 71, 34, 34);
        [_footView addSubview:markBtn];
        [markBtn dk_setImage:DKImagePickerWithNames(@"common_cell_unchoose",@"common_cell_unchoose_D")
                    forState:UIControlStateNormal];
        [markBtn addTarget:self
                    action:@selector(responseToMark:)
          forControlEvents:UIControlEventTouchUpInside];
        markBtn.tag = MARKBTNTAG;
        
        UILabel *markLbl = [IXUtils createLblWithFrame:CGRectMake( 47, 81,  265, 15)
                                              WithFont:PF_MEDI(13)
                                             WithAlign:NSTextAlignmentLeft
                                            wTextColor:0x99abba
                                            dTextColor:0x8395a4];
        [_footView addSubview:markLbl];
        markLbl.numberOfLines = 0;
        
        markLbl.text = LocalizedString(@"不再弹出此框，双击即提交订单");
        [markLbl sizeToFit];

        if ( markLbl.frame.size.height > 15 ) {
            CGRect frame = markLbl.frame;
            frame.origin.y -= 5;
            markLbl.frame = frame;
        }
    }
    return _footView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self removeFromSuperview];
}

@end
