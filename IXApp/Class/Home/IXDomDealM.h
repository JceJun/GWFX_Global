//
//  IXDomDealM.h
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXTradeMarketModel.h"

#define TIPVIEW    10000
#define RESULTVIEW 20000

typedef NS_ENUM(NSInteger, DOMSTATUS) {
    DOMSTATUSUNCHOOSE,
    DOMSTATUSCHOOSEBUY,
    DOMSTATUSCHOOSESELL,
};

@interface IXDomDealM : NSObject

@property (nonatomic, strong) NSMutableArray *volArr;
@property (nonatomic, strong) NSMutableArray *showVolArr;

@property (nonatomic, strong) NSMutableArray *posArr;
@property (nonatomic, strong) NSMutableArray *chooseStatusArr;

@property (nonatomic, strong) IXTradeMarketModel  *showM;

@property (nonatomic, strong) NSDecimalNumber *showVolume;
@property (nonatomic, strong) NSDecimalNumber *requestVolume;

@property (nonatomic, strong) NSDecimalNumber *requestPrice;
@property (nonatomic, assign) item_order_edirection requestDir;

@property (nonatomic, copy)   NSString *requestType;
@property (nonatomic, strong) NSMutableArray *prcRelPosBuyArr;
@property (nonatomic, strong) NSMutableArray *prcRelPosSellArr;


@property (nonatomic, assign) BOOL isVolNum;

//深度价格对应的最大手数
@property (nonatomic, assign) double maxVol;

@property (nonatomic,copy)void(^notiDomSuc)(NSString *prc,NSString *vol);  //下单成功
@property (nonatomic,copy)void(^notiDomFail)(NSString *errMsg);            //下单失败
@property (nonatomic,copy)void(^notiUpdate)();                             //刷新Cell
@property (nonatomic,copy)void(^notiPostOrder)();                          //刷新Cell

- (void)refreashRequestPrice;

//获取深度价格
- (NSString *)getDeepPrcWithRow:(NSInteger)deepRow;
//获取买入手数
- (NSString *)getBuyVolWithRow:(NSInteger)deepRow;
//获取卖出手数
- (NSString *)getSellVolWithRow:(NSInteger)deepRow;
//获取选中状态
- (DOMSTATUS)getChooseStatusWithRow:(NSInteger)deepRow;

- (void)setMaxVolWithQuote;

- (void)resetDeepPrice;

- (proto_order_add *)packageOrder;
- (void)chooseWithRow:(NSInteger)row WithDir:(item_order_edirection)dir;

@end
