//
//  IXAccSymCataM.h
//  IXApp
//
//  Created by Evn on 17/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXAccSymCataM : NSObject

@property (nonatomic, assign) uint64_t cataId;//一级分类
@property (nonatomic, strong) NSArray *cataIdArr;//二级分类集合
@property (nonatomic, assign) uint64_t symbolId;
@property (nonatomic, assign) uint64_t level;//0:分类 1:产品
@property (nonatomic, assign) uint64_t accGroupId;
@property (nonatomic, assign) uint32_t noDisplay;


@end
