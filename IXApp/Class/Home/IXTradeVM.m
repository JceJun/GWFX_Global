//
//  IXTradeVM.m
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeVM.h"

#import "IXOpenChoiceV.h"
#import "NSString+FormatterPrice.h"
#import "UIImageView+SepLine.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBGlobal.h"

@interface IXTradeVM ()

@property (nonatomic, assign) float currentTFHeight;

@property (nonatomic, assign) BOOL moreOperation;
@property (nonatomic, strong) UIView *moreOperationView;
@property (nonatomic, strong) IXOpenChoiceV *showMoreOperationBtn;

@property (nonatomic, strong) IXSymbolM *symbolModel;
@property (nonatomic, strong) IXQuoteM *quoteDataModel;

@end

@implementation IXTradeVM

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEdit:)
                                                     name:UITextFieldTextDidBeginEditingNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
   
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
   
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidBeginEditingNotification
                                                  object:nil];
}

- (void)didEdit:(NSNotification *)notification
{
    UITextField *contentTF = notification.object;
    UIView *result = contentTF.superview.superview;
    _currentTFHeight =  CGRectGetMaxY(result.frame);
    if ( result.tag == ROWNAMELOSS || result.tag == ROWNAMEPROFIT ) {
        _currentTFHeight += CGRectGetMaxY(_moreOperationView.frame) - 56;
    }
}

- (void)keyboardHidden:(NSNotification *)notify
{
    CGRect frame = _contentScroll.frame;
    frame.origin.y = 0;
    _contentScroll.frame = frame;
}

- (void)keyboardShow:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    float keyboardHeight = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    
    float height = kScreenHeight - keyboardHeight - _currentTFHeight - CGRectGetMinY(_contentScroll.frame);
    height -= 56;
    CGRect frame = _contentScroll.frame;
    if ( frame.origin.y < 0 || height < 0 ) {
        frame.origin.y += height;
    }
    _contentScroll.frame = frame;
}

- (void)setContentScroll:(UIScrollView *)contentScroll
{
    _contentScroll = contentScroll;
    
    [_contentScroll addSubview:self.symbolView];
     CGRect frame = CGRectMake( 0, 0, kScreenWidth, 148);
    self.symbolView.frame = frame;
    
    [_contentScroll addSubview:self.typeView];
    frame.origin.y = CGRectGetMaxY( self.symbolView.frame );// + 10;
    frame.size.height = 56;
    self.typeView.frame = frame;
    
    [_contentScroll addSubview:self.directionView];
    frame.origin.y = CGRectGetMaxY( self.typeView.frame );// + 10;
    self.directionView.frame = frame;
    
    [_contentScroll addSubview:self.dealPriceView];
    frame.origin.y = CGRectGetMaxY( self.directionView.frame );// + 10;
    self.dealPriceView.frame = frame;
    
    [_contentScroll addSubview:self.volumeView];
    frame.origin.y = CGRectGetMaxY( self.dealPriceView.frame );// + 10;
    self.volumeView.frame = frame;
    
    [_contentScroll addSubview:self.showMoreOperationBtn];
    frame.origin.y = CGRectGetMaxY( self.volumeView.frame );
    frame.size.height = 40;
    self.showMoreOperationBtn.frame = frame;
    [self.showMoreOperationBtn resetTitle:LocalizedString(@"更多高级操作")];
    [self resetContentSizeWithCompareView:self.showMoreOperationBtn];
}

- (void)resetContentSizeWithCompareView:(UIView *)view
{
    CGSize size =  _contentScroll.contentSize;
    if (CGRectGetMaxY(view.frame) < kScreenHeight + 44) {
        [_contentScroll setContentSize:CGSizeMake( kScreenWidth, kScreenHeight + 44)];
    }else if (size.height < CGRectGetMaxY(view.frame) + 106) {
        size.height = CGRectGetMaxY(view.frame) + 106;
        [_contentScroll setContentSize:size];
    }
}

- (void)showMoreDeep:(BOOL)show
{
    CGFloat offsetY = show ? (35 * 4) : (-35 * 4);
    
    CGRect frame  = _symbolView.frame;
    frame.size.height += offsetY;
    _symbolView.frame = frame;
    
    frame = _typeView.frame;
    frame.origin.y += offsetY;
    _typeView.frame = frame;
    
    frame = _directionView.frame;
    frame.origin.y += offsetY;
    _directionView.frame = frame;
    
    frame  = _dealPriceView.frame;
    frame.origin.y += offsetY;
    _dealPriceView.frame = frame;
    
    frame  = _volumeView.frame;
    frame.origin.y += offsetY;
    _volumeView.frame = frame;
    
    frame  = _showMoreOperationBtn.frame;
    frame.origin.y += offsetY;
    _showMoreOperationBtn.frame = frame;
    
    frame = _moreOperationView.frame;
    frame.origin.y += offsetY;
    _moreOperationView.frame = frame;
    
    [self resetContentSizeWithCompareView:_moreOperationView];
}

- (IXTradeSymbolV *)symbolView
{
    if (!_symbolView) {
        _symbolView = [[IXTradeSymbolV alloc] initWithFrame:CGRectZero];
        weakself;
        _symbolView.deepPrice = ^(BOOL show){
            [weakSelf showMoreDeep:show];
        };
    }
    return _symbolView;
}

- (IXTradeTypeV *)typeView
{
    if (!_typeView) {
//         _typeView = [[IXTradeTypeV alloc] initWithFrame:CGRectZero WithTadeType:^(TRADE_TYPE type) {
//             switch (type) {
//                 case Trade_kind:
//                     [weak_self.tradeVMDelegate chooseTradeType];
//                     break;
//                 case Trade_expire:
//                     [weak_self.tradeVMDelegate chooseExpireType];
//                     break;
//                 default:
//                     break;
//             }
//         }];
    }
    return _typeView;
}

- (IXOpenDirectV *)directionView
{
    if (!_directionView) {
        weakself;
        _directionView = [[IXOpenDirectV alloc] initWithFrame:CGRectZero
                                                          WithTitle:LocalizedString(@"方向")
                                                            WithDir:^(item_order_edirection direction) {
            if (weakSelf.tradeVMDelegate) {
                [weakSelf.tradeVMDelegate updateDirection:direction];
            }
        }];
    }
    return _directionView;
}

- (IXTradeOperatV *)dealPriceView
{
    if (!_dealPriceView) {
        _dealPriceView = [[IXTradeOperatV alloc] initWithFrame:CGRectZero];
        _dealPriceView.tag = ROWNAMEPRICE;
        [_dealPriceView setTitleContent:LocalizedString(@"价格")];
        [_dealPriceView setStopLevel:[self getPricePoint]];

        weakself;
        _dealPriceView.clickBlock = ^(Click_Type type,NSInteger tag){
            switch (type) {
                case Click_Subtract:
                    [weakSelf.tradeVMDelegate updateStoplossWithTag:tag];
                    break;
                case Click_Add:
                    [weakSelf.tradeVMDelegate updateTakeprofitWithTag:tag];
                    break;
                default:
                    break;
            }
        };
        _dealPriceView.textContent = ^(NSString *value){
            [weakSelf.tradeVMDelegate updateVolume:value WithTag:weakSelf.dealPriceView.tag];
        };
    }
    return _dealPriceView;
}

- (IXTradeOperatV *)volumeView
{
    if (!_volumeView) {
        _volumeView = [[IXTradeOperatV alloc] initWithFrame:CGRectZero];
        _volumeView.tag = ROWNAMEVOLUME;
        [_volumeView setTitleContent:LocalizedString(@"数量")];
        [_volumeView setStopLevel:[NSString stringWithFormat:@"%f",_symbolModel.volumesStep * _symbolModel.contractSize]];
        weakself;
        _volumeView.clickBlock = ^(Click_Type type,NSInteger tag){
            switch (type) {
                case Click_Subtract:
                    [weakSelf.tradeVMDelegate updateStoplossWithTag:tag];
                    break;
                case Click_Add:
                    [weakSelf.tradeVMDelegate updateTakeprofitWithTag:tag];
                    break;
                default:
                    break;
            }
        };
        _volumeView.textContent = ^(NSString *value){
            [weakSelf.tradeVMDelegate updateVolume:value WithTag:weakSelf.volumeView.tag];
        };
    }
    return _volumeView;
}

- (UIView *)moreOperationView
{
    if (!_moreOperationView) {
        CGRect frame = self.volumeView.frame;
        frame.origin.y = CGRectGetMaxY(self.volumeView.frame);// + 10;
        frame.size.height = 132;
        _moreOperationView = [[UIView alloc] initWithFrame:frame];
    }
    return _moreOperationView;
}

- (IXOpenChoiceV *)showMoreOperationBtn
{
    if (!_showMoreOperationBtn) {
        weakself;
        _showMoreOperationBtn = [[IXOpenChoiceV alloc] initWithFrame:CGRectZero
                                                              WithTitle:LocalizedString(@"更多高级操作")
                                                              WithAlign:TapChoiceAlignDefault
                                                                WithTap:^{
            [weakSelf moreTradeOperation];
        }];
        
        CGRect frame = CGRectMake(0, 39, kScreenWidth, 1);
        UIImageView *sepLine = [UIImageView addSepImageWithFrame:frame WithColor:MarketCellSepColor WithAlpha:1];
        [_showMoreOperationBtn addSubview:sepLine];
    }
    return _showMoreOperationBtn;
}

- (void)updatePlaceOlderContent:(NSString *)content WithTag:(ROWNAME)tag
{
    content = [NSString formatterPrice:content WithDigits:_symbolModel.digits];
    switch (tag) {
        case ROWNAMEPROFIT:{
            for (IXTradeOperatV *view in _moreOperationView.subviews) {
                if (view.tag == tag) {
                    [view updatePlaceOlderValue:content];
                }
            }
        }
            break;
        case ROWNAMELOSS:{
            for (IXTradeOperatV *view in _moreOperationView.subviews) {
                if (view.tag == tag) {
                    [view updatePlaceOlderValue:content];
                }
            }
        }
            break;
        case ROWNAMEVOLUME:
            [_volumeView updatePlaceOlderValue:content];
            break;
        case ROWNAMEPRICE:
            [_dealPriceView updatePlaceOlderValue:content];
            break;
        default:
            break;
    }
}

- (void)addMoreTradeOperation
{
    CGRect frame = _moreOperationView.frame;
    frame.origin.x = 0;
    frame.size.height = 56;
    for (NSInteger currentRow = 0; currentRow < 2; currentRow++) {
        frame.origin.y = 56 * currentRow;
        IXTradeOperatV *tradOperation = [[IXTradeOperatV alloc] initWithFrame:frame];
        tradOperation.tag = (currentRow == 0) ? ROWNAMEPROFIT : ROWNAMELOSS;
        tradOperation.digit = _symbolModel.digits;
        [tradOperation setTitleContent:@[LocalizedString(@"止盈"),LocalizedString(@"止损")][currentRow]];
        [tradOperation setStopLevel:[self getPricePoint]];
        
        weakself;
        tradOperation.clickBlock = ^(Click_Type type, NSInteger tag){
            switch (type) {
                case Click_Subtract:
                    [weakSelf.tradeVMDelegate updateStoplossWithTag:tag];
                    break;
                case Click_Add:
                    [weakSelf.tradeVMDelegate updateTakeprofitWithTag:tag];
                    break;
                default:
                    break;
            }
        };
        
        __block IXTradeOperatV *blockView = tradOperation;
        tradOperation.textContent = ^(NSString *value){
            [weakSelf.tradeVMDelegate updateVolume:value WithTag:blockView.tag];
        };
        [_moreOperationView addSubview:tradOperation];
    }
}

- (void)moreTradeOperation
{
    if (!_moreOperationView) {
        [_contentScroll addSubview:self.moreOperationView];
        [self addMoreTradeOperation];
        if ([self.tradeVMDelegate respondsToSelector:@selector(caculatePrice)]) {
            [self.tradeVMDelegate caculatePrice];
        }
    }
    
//  _moreOperation = !_moreOperation;
    _moreOperation = YES;
    _moreOperationView.hidden = !_moreOperation;
    
    CGRect frame = _showMoreOperationBtn.frame;
    frame.origin.y = _moreOperation
                ? ( CGRectGetMaxY(_moreOperationView.frame))
                : ( CGRectGetMaxY(_volumeView.frame) + 1);
    _showMoreOperationBtn.frame = frame;
    _showMoreOperationBtn.hidden = YES;
    
    [self resetContentSizeWithCompareView:_moreOperationView];
}

- (void)resetSybolModel:(IXSymbolM *)symbolModel
{
    _symbolModel = symbolModel;
    _symbolView.symbolModel = symbolModel;
    
    NSDictionary *dic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:symbolModel.id_p];
    _symbolView.marketId = [dic[kMarketId] integerValue];
    
    [_volumeView setStopLevel:[self getVolume:_symbolModel.volumesStep]];
    [_volumeView updatePlaceOlderValue:[self getVolume:_symbolModel.volumesMin]];
    
    _dealPriceView.digit = _symbolModel.digits;
    [_dealPriceView setStopLevel:[self getPricePoint]];
}

- (NSString *)getPricePoint
{
//    NSDecimalNumber *pricePoint = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",_symbolModel.pipsRatio]];
    NSDecimalNumber *pricePoint = [NSDecimalNumber decimalNumberWithString:@"1"];
    pricePoint = [pricePoint decimalNumberByMultiplyingByPowerOf10:-_symbolModel.digits];
    
    NSString *step = [NSString formatterPrice:[pricePoint stringValue] WithDigits:_symbolModel.digits];
    return step;
}

- (NSString *)getVolume:(double)value
{
    return [NSString stringWithFormat:@"%.lf",value];
}

- (void)setNLastClosePrice:(double)nLastClosePrice
{
    _symbolView.nLastClosePrice = nLastClosePrice;
}

- (void)resetQuoteModel:(IXQuoteM *)dataModel
{
    _quoteDataModel = dataModel;
    _symbolView.quoteModel = dataModel;
}

- (void)resetDirection:(item_order_edirection)direction
{
    [self.directionView resetUIWithTag:direction];
    switch (direction) {
        case item_order_edirection_DirectionBuy:{
            if ( _quoteDataModel.SellPrc.count != 0 ) {
                [self.dealPriceView updatePlaceOlderValue:[NSString formatterPrice:_quoteDataModel.SellPrc[0]
                                                                        WithDigits:_symbolModel.digits]];
            }
        }

            break;
        case item_order_edirection_DirectionSell:{
            if ( _quoteDataModel.BuyPrc.count != 0 ) {
                [self.dealPriceView updatePlaceOlderValue:[NSString formatterPrice:_quoteDataModel.BuyPrc[0]
                                                                        WithDigits:_symbolModel.digits]];
            }
        }
            break;
        default:
            break;
    }
}

//更新手数
- (void)resetVolUnitName:(NSString *)unitName
{
    [_volumeView setUnitName:unitName];
}

- (void)resetPriceWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange
{
    [_dealPriceView updateWithLeftRange:leftRange WithRightRange:rightRange];
}

- (void)resetVolumeWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange
{
    [_volumeView updateWithLeftRange:leftRange WithRightRange:rightRange];
}

- (void)resetProfitWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange
{
    for (IXTradeOperatV *view in _moreOperationView.subviews) {
        if ([view isKindOfClass:[IXTradeOperatV class]] && view.tag == ROWNAMEPROFIT) {
            [view updateWithLeftRange:leftRange WithRightRange:rightRange];
            break;
        }
    }
}

- (void)resetLossWithLeftRange:(NSString *)leftRange WithRightRange:(NSString *)rightRange
{
    for (IXTradeOperatV *view in _moreOperationView.subviews) {
        if ( [view isKindOfClass:[IXTradeOperatV class]] && view.tag == ROWNAMELOSS ) {
            [view updateWithLeftRange:leftRange WithRightRange:rightRange];
            break;
        }
    }
}

@end
