//
//  IXTradeSymbolV.m
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeSymbolV.h"
#import "IXTradeDeepV.h"
#import "UILabel+LineSpace.h"
#import "NSString+FormatterPrice.h"
#import "UIImageView+SepLine.h"
#import "IXOpenChoiceV.h"

@interface IXTradeSymbolV ()
{
    float prePrice;
}

@property (nonatomic, strong) UILabel *tradeTimeLbl;
@property (nonatomic, strong) UILabel *tradeStatusLbl;
@property (nonatomic, strong) UILabel *symbolNameLbl;
@property (nonatomic, strong) UILabel *symbolCodeLbl;
@property (nonatomic, strong) UILabel *currentPriceLbl;
@property (nonatomic, strong) UILabel *profitSwapLbl;
@property (nonatomic, strong) UILabel *profitLbl;

@property (nonatomic, strong) UIView *deepView;
@property (nonatomic, strong) UIView *moreDeepView;
@property (nonatomic, strong) IXOpenChoiceV  *showMoreDeepBtn;
@property (nonatomic, assign) BOOL showMoreDeep;

@property (nonatomic, assign) int digits;

@end

@implementation IXTradeSymbolV

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _showMoreDeep = NO;
        self.backgroundColor = [UIColor whiteColor];
        
        CGFloat orginX = 15;
        _symbolNameLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, 16, 140, 22)
                                            WithFont:PF_MEDI(15)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0x4c6072
                                          dTextColor:0xff0000];
        [self addSubview:_symbolNameLbl];
        
        CGFloat width = [IXEntityFormatter getContentWidth:_symbolNameLbl.text WithFont:PF_MEDI(15)] + 5;
        orginX += width;
        CGFloat orginY = 23;
        _symbolCodeLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 140, 10)
                                            WithFont:RO_REGU(10)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0xa7adb5
                                          dTextColor:0xff0000];
        [self addSubview:_symbolCodeLbl];
        
        orginX = 15;
        orginY = 41;
        width = [IXEntityFormatter getContentWidth:LocalizedString(@"休假中") WithFont:PF_MEDI(10)] + 3;
        _tradeStatusLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, (int)width, 15)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0xffffff
                                           dTextColor:0xff0000];
        [self addSubview:_tradeStatusLbl];
        _tradeStatusLbl.text = LocalizedString(@"交易中");
        _tradeStatusLbl.backgroundColor = MarketGrayShowColor;
        
        orginX = width + 20;
        orginY = 43;
        _tradeTimeLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 140, 10)
                                           WithFont:RO_REGU(10)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0xa7adb5
                                         dTextColor:0xff0000];
        [self addSubview:_tradeTimeLbl];
        
        
        
        orginX = kScreenWidth - 58;
        orginY = 25;
        _profitSwapLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 58, 12)
                                            WithFont:RO_REGU(12)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:mGreenPriceColor
                                          dTextColor:0xff0000];
        [self addSubview:_profitSwapLbl];
        
        orginY += 15;
        _profitLbl = [IXUtils createLblWithFrame:CGRectMake( orginX, orginY, 58, 12)
                                        WithFont:RO_REGU(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:mGreenPriceColor
                                      dTextColor:0xff0000];
        [self addSubview:_profitLbl];
        
        
        
        _currentPriceLbl = [IXUtils createLblWithFrame:CGRectZero
                                              WithFont:RO_REGU(40)
                                             WithAlign:NSTextAlignmentRight
                                            wTextColor:0x99abba
                                            dTextColor:0xff0000];
        [self addSubview:_currentPriceLbl];

        orginY = 73;
        _deepView = [[UIView alloc] initWithFrame:CGRectMake( 0, orginY, kScreenWidth, 35)];
        [self addSubview:_deepView];
        [self addDeepPriceParentView:_deepView WithCount:1];
        UIImageView *sepImageView = [UIImageView addSepImageWithFrame:CGRectMake( 0, 0, kScreenWidth, 1)
                                                            WithColor:UIColorHexFromRGB(0x0d0d0d)
                                                            WithAlpha:0.11];
        [_deepView addSubview:sepImageView];
        
        
        orginY += 35;
        _moreDeepView = [[UIView alloc] initWithFrame:CGRectMake(  0, orginY, kScreenWidth, 35 * 4)];
        _moreDeepView.hidden = YES;
        [self addSubview:_moreDeepView];
 
        frame = CGRectMake( 0, orginY, kScreenWidth, 40);
        weakself;
        _showMoreDeepBtn = [[IXOpenChoiceV alloc] initWithFrame:frame
                                                         WithTitle:LocalizedString(@"展开市场深度")
                                                         WithAlign:TapChoiceAlignDefault
                                                           WithTap:^{
            [weakSelf responseToShowMore];
        }];
        [self addSubview:_showMoreDeepBtn];
        
        frame.origin.y = 0;
        frame.size.height = 1;
        UIImageView *sepLine = [UIImageView addSepImageWithFrame:frame
                                                       WithColor:UIColorHexFromRGB(0x0d0d0d)
                                                       WithAlpha:0.11];
        [_showMoreDeepBtn addSubview:sepLine];
    }
    return self;
}

- (void)setSymbolModel:(IXSymbolM *)symbolModel
{
    _symbolModel = symbolModel;
    _digits = symbolModel.digits;
    
    _symbolNameLbl.text = symbolModel.languageName;
    CGRect frame = _symbolNameLbl.frame;
    frame.size.width = [IXEntityFormatter getContentWidth:_symbolNameLbl.text WithFont:_symbolNameLbl.font];
    _symbolNameLbl.frame = frame;
    
    _tradeStatusLbl.text = [IXRateCaculate symbolTradeState:
                            [IXDataProcessTools symbolIsCanTrade:symbolModel]];
}

- (void)setMarketId:(double)marketId
{
    _marketId = marketId;
    
    NSString *content = [NSString stringWithFormat:@"%@:%@",_symbolModel.name,[IXEntityFormatter getMarketNameWithMarketId:marketId]];
    CGRect frame = _symbolCodeLbl.frame;
    frame.origin.x = CGRectGetMaxX(_symbolNameLbl.frame) + 5;
    frame.size.width = [IXEntityFormatter getContentWidth:content WithFont:_symbolCodeLbl.font];
    _symbolCodeLbl.frame = frame;
    _symbolCodeLbl.text = content;
}

- (void)setQuoteModel:(IXQuoteM *)quoteModel
{
    _quoteModel = quoteModel;
    
    
    if ( prePrice - quoteModel.nPrice < 0 ) {
        
        _currentPriceLbl.textColor = MarketRedPriceColor;
        
    }else if ( prePrice - quoteModel.nPrice == 0 ){
        
        _currentPriceLbl.textColor = MarketSymbolNameColor;
        
    }else{
        
        _currentPriceLbl.textColor = MarketGreenPriceColor;
    }
    prePrice = quoteModel.nPrice;

    
    [self updateDeepPrice];
    [self updateProfit:quoteModel.nPrice];
    _tradeTimeLbl.text = [NSString stringWithFormat:@"%@",[IXEntityFormatter timeIntervalToString:quoteModel.n1970Time]];
    
    if ( quoteModel.nPrice != 0 ) {
        _currentPriceLbl.text = [self formatterPrice:[NSString stringWithFormat:@"%lf",quoteModel.nPrice]];
    }else{
        _currentPriceLbl.text = @"--";
        _currentPriceLbl.textColor = MarketGrayPriceColor;
    }
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat originX = ( CGRectGetMaxX(_symbolCodeLbl.frame) > kScreenWidth/2) ?  CGRectGetMaxX(_symbolCodeLbl.frame) : kScreenWidth/2;
    CGRect frame = _currentPriceLbl.frame;
    frame.origin.x = originX;
    frame.origin.y = 20;
    frame.size.width =  kScreenWidth - originX - 68;
    frame.size.height = 33;
    _currentPriceLbl.frame = frame;
    [_currentPriceLbl setAdjustsFontSizeToFitWidth:YES];
}

- (void)updateProfit:(double)price
{
    if ( _nLastClosePrice == 0  || price == 0 ) {
        _profitLbl.text = @"--";
        _profitSwapLbl.text = @"--";
    }else{
        float offsetPrice = price - _nLastClosePrice;
        _profitLbl.text = [NSString formatterPrice:[NSString formatterPriceSign:offsetPrice]
                                        WithDigits:_digits];
        
        if (offsetPrice < 0) {
            _profitLbl.textColor = MarketGreenPriceColor;
            _profitSwapLbl.textColor = MarketGreenPriceColor;
            
        } else if (offsetPrice == 0){
            _profitLbl.textColor = MarketGrayPriceColor;
            _profitSwapLbl.textColor = MarketGrayPriceColor;
        } else {
            _profitLbl.textColor = MarketRedPriceColor;
            _profitSwapLbl.textColor = MarketRedPriceColor;
        }
        
        if (_nLastClosePrice != 0) {
            
            NSString *profitSwapStr = [NSString formatterPrice:[NSString formatterPriceSign:(offsetPrice/_nLastClosePrice * 100)]
                                                    WithDigits:2];
            _profitSwapLbl.text = [NSString stringWithFormat:@"%@%%",profitSwapStr];
            [UILabel changeWordSpaceForLabel:_profitSwapLbl WithSpace:-0.21];
        }
        
        [_profitLbl adjustsFontSizeToFitWidth];
        [UILabel changeWordSpaceForLabel:_profitLbl WithSpace:-0.21];

    }
}

- (void)updateDeepPrice
{
    for (IXTradeDeepV *tradeView in [_deepView subviews]) {
        if ([tradeView isKindOfClass:[IXTradeDeepV class]]) {
            if ( _quoteModel.BuyPrc.count > 0 ) {
                [tradeView setBuyPrc:[self formatterPrice:_quoteModel.BuyPrc[0]]
                              BuyVol:[self formatterPrice:_quoteModel.BuyVol[0]]
                             SellPrc:[self formatterPrice:_quoteModel.SellPrc[0]]
                             SellVol:[self formatterPrice:_quoteModel.SellVol[0]]];
            }
        }
    }
    
    for (IXTradeDeepV *tradeView in [_moreDeepView subviews]) {
        if ([tradeView isKindOfClass:[IXTradeDeepV class]]) {
            if ( _quoteModel.BuyPrc.count > tradeView.tag ) {
                [tradeView setBuyPrc:[self formatterPrice:_quoteModel.BuyPrc[tradeView.tag]]
                              BuyVol:[self formatterPrice:_quoteModel.BuyVol[tradeView.tag]]
                             SellPrc:[self formatterPrice:_quoteModel.SellPrc[tradeView.tag]]
                             SellVol:[self formatterPrice:_quoteModel.SellVol[tradeView.tag]]];
            }
        }
    }
}

- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price WithDigits:_digits];
}

- (void)addDeepPriceParentView:(UIView *)view WithCount:(NSInteger)count
{
    CGRect frame = view.bounds;
    frame.size.height = 35;
    for (NSInteger currentRow = 1; currentRow <= count; currentRow++) {
        frame.origin.y = 35 * (currentRow - 1);
        NSInteger row = count > 1 ? currentRow+1 : currentRow;
        IXTradeDeepV *deepPrice = [[IXTradeDeepV alloc] initWithFrame:frame WithCount:row];
        deepPrice.tag = currentRow;
        [view addSubview:deepPrice];
    }
}

- (void)responseToShowMore
{
    _showMoreDeep = !_showMoreDeep;
    if (self.deepPrice) {
        _moreDeepView.hidden = !_showMoreDeep;
        
        if([_moreDeepView subviews].count == 0){
            [self addDeepPriceParentView:_moreDeepView WithCount:4];
            [self updateDeepPrice];
        }
        CGRect frame = _showMoreDeepBtn.frame;
        frame.origin.y = _showMoreDeep ? CGRectGetMaxY(_moreDeepView.frame) : CGRectGetMaxY(_deepView.frame);
        _showMoreDeepBtn.frame = frame;
        
        self.deepPrice(_showMoreDeep);
    }
}

@end
