//
//  IXTradeMarketModel.h
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IxProtoHeader.pbobjc.h"
#import "IxProtoOrder.pbobjc.h"


typedef NS_ENUM( NSInteger, CHOOSE_TYPE){
    Choose_TradeType,
    Choose_ExpireType,
    Choose_VolumeType,
    Choose_PriceType,
};

@interface IXTradeMarketModel : NSObject

@property (nonatomic, assign) BOOL canBuy;

@property (nonatomic, assign) BOOL canSell;

@property (nonatomic, strong) IXSymbolM *symbolModel;   //产品

@property (nonatomic, strong) IXQuoteM *quoteModel; //行情

@property (nonatomic, assign) NSInteger marketId;           //市场Id

@property (nonatomic, assign,readonly) NSInteger parentId;

@property (nonatomic, assign) NSTimeInterval createTime;

@property (nonatomic, strong) NSArray *tradeTypeArr;        //开仓类型

@property (nonatomic, strong) NSArray *tradeExpireArr;      //挂单有效期

@property (nonatomic, assign) CHOOSE_TYPE chooseType;       //标示弹出选择框对应的

@property (nonatomic, assign) BOOL receviedOrderAdd;        //下单后收到服务端返回

@property (nonatomic, assign) NSInteger repeatCount;        //重复次数

@property (nonatomic, copy) NSString *requestType;

@property (nonatomic, copy) NSString *requestExpire;

@property (nonatomic, strong) NSDecimalNumber *requestVolume;

@property (nonatomic, strong) NSDecimalNumber *showVolume;

@property (nonatomic, strong) NSDecimalNumber *requestPrice;

@property (nonatomic, assign) double nLastPrice;


@property (nonatomic, strong) NSDecimalNumber *stoploss;

@property (nonatomic, strong) NSDecimalNumber *takeprofit;

@property (nonatomic, assign) item_order_edirection direction;

@property (nonatomic, assign) uint64_t orderId;

@property (nonatomic, assign) double margin;//保证金

@property (nonatomic, assign) double commission;//佣金


- (void)resetInitInfo;

- (proto_order_add *)packageOrder;

- (proto_order_update *)packageUpdateOrder;

@end
