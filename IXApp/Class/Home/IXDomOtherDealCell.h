//
//  IXDomOtherDealCell.h
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IXDomDealM.h"

typedef void(^chooseOrder)(item_order_edirection dir);

@interface IXDomOtherDealCell : UITableViewCell

@property (nonatomic, strong) IXDomDealM *model;

 
@property (nonatomic, strong) chooseOrder order;

- (void)updateOrderCount;

- (void)updateStatus:(NSArray *)arr;
 
@end
