//
//  IXOpenPrcCell.m
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenPrcCell.h"

@interface IXOpenPrcCell ()

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *inputExpLbl;

@end

@implementation IXOpenPrcCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x242a36);
        [self loadOpView];
        [self.contentView addSubview:self.inputTF];
        
    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    if ( title && title.length ) {
        self.titleLbl.text = title;
    }
}

- (void)setInputContent:(NSString *)inputContent
{
    if ( inputContent ) {
        self.inputTF.text = inputContent;
    }
}


- (void)setInputExplain:(NSString *)inputExplain
{
    if ( inputExplain ) {
        self.inputExpLbl.text = inputExplain;
    }
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 14, kScreenWidth - 15, 15)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x99abba
                                     dTextColor:0x99abba];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}


- (IXAccTextField *)inputTF
{
    if ( !_inputTF ) {
        
        CGFloat width = kScreenWidth - 222;
        _inputTF = [[IXAccTextField alloc] initWithFrame:CGRectMake( 119, 5, width, 30)];
    }
    return _inputTF;
}

- (UILabel *)inputExpLbl
{
    if ( !_inputExpLbl ) {
        CGFloat width = kScreenWidth - 222;
        _inputExpLbl = [IXUtils createLblWithFrame:CGRectMake( 119, 31, width, 9)
                                          WithFont:RO_REGU(9)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0x99abba
                                        dTextColor:0x99abba];
        [self.contentView addSubview:_inputExpLbl];
    }
    return _inputExpLbl;
}

- (void)loadOpView
{
    UIButton *subBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:subBtn];
    
    subBtn.frame = CGRectMake( 70, 0, 30, 44);
    [subBtn dk_setImage:DKImageNames(@"openRoot_btn_substract", @"openRoot_btn_substract_dark")
               forState:UIControlStateNormal];
    [subBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 8, 0, 0)];
    [subBtn addTarget:self
               action:@selector(responseToSubstract)
     forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *stepBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:stepBtn];
    stepBtn.tag = DASHBORADBTNTAG;
    stepBtn.frame = CGRectMake( kScreenWidth - 45 + 7, 0, 30, 44);
    [stepBtn addTarget:self
                action:@selector(responseToStep)
      forControlEvents:UIControlEventTouchUpInside];
    //    [stepBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 12, 0, 0)];
    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:addBtn];
    
    addBtn.frame = CGRectMake( kScreenWidth - 88, 0, 30, 44);
    [addBtn dk_setImage:DKImageNames(@"openRoot_btn_add", @"openRoot_btn_add_dark")
               forState:UIControlStateNormal];
    [addBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 8, 0, 0)];
    [addBtn addTarget:self
               action:@selector(responseToAdd)
     forControlEvents:UIControlEventTouchUpInside];
}

- (void)responseToAdd
{
    if ( self.addBlock ) {
        self.addBlock();
    }
}


- (void)responseToStep
{
    if ( self.stepBlock ) {
        self.stepBlock();
    }
}


- (void)responseToSubstract
{
    if( self.substractBlock ){
        self.substractBlock();
    }
}

@end
