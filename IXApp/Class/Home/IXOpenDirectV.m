//
//  IXOpenDirectV.m
//  IXApp
//
//  Created by Bob on 2016/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOpenDirectV.h"
#import "UIImageView+SepLine.h"

@interface IXOpenDirectV ()

@property  (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) orderDirection orderDir;

@end

@implementation IXOpenDirectV

- (id)initWithFrame:(CGRect)frame WithTitle:(NSString *)title WithDir:(orderDirection)dire
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.orderDir = dire;
        
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.tipLbl];
        self.tipLbl.text = title;
        
        CGFloat orginX =  50 + [IXEntityFormatter getContentWidth:title WithFont:PF_MEDI(13)];
        CGFloat width = (kScreenWidth - orginX - 15)/2;
        frame.origin.x = orginX;
        frame.origin.y = 13;
        frame.size.width = width;
        frame.size.height = 30;
        UIButton *buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        buyBtn.frame = frame;
        [self addSubview:buyBtn];
        [buyBtn setTitle:LocalizedString(@"卖出") forState:UIControlStateNormal];
        [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [buyBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        [buyBtn.titleLabel setFont:PF_MEDI(13)];
        [buyBtn addTarget:self action:@selector(responseToBtn:) forControlEvents:UIControlEventTouchUpInside];
        buyBtn.tag = item_order_edirection_DirectionSell;

        UIImage *image = GET_IMAGE_NAME(@"openRoot_dir_left");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [buyBtn setBackgroundImage:image
                          forState:UIControlStateNormal];
        
        image = GET_IMAGE_NAME(@"openRoot_sell_left");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [buyBtn setBackgroundImage:image
                          forState:UIControlStateSelected];
        
        frame.origin.x += width;
        UIButton *sellBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sellBtn.frame = frame;
        [self addSubview:sellBtn];
        [sellBtn setTitle:LocalizedString(@"买入") forState:UIControlStateNormal];
        [sellBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [sellBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        [sellBtn.titleLabel setFont:PF_MEDI(13)];
        
        image = GET_IMAGE_NAME(@"openRoot_dir_right");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [sellBtn setBackgroundImage:image
                           forState:UIControlStateNormal];
        
        image = GET_IMAGE_NAME(@"openRoot_green_right");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [sellBtn setBackgroundImage:image
                           forState:UIControlStateSelected];
        
        [sellBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [sellBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        [sellBtn addTarget:self action:@selector(responseToBtn:) forControlEvents:UIControlEventTouchUpInside];
        sellBtn.tag = item_order_edirection_DirectionBuy;
        
        [self addSepLine];
    }
    return self;
}

- (UILabel *)tipLbl
{
    if (!_tipLbl) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 21, 100, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentLeft
                                   wTextColor:0x99abba
                                   dTextColor:0xff0000];
    }
    return _tipLbl;
}

- (void)addSepLine
{
    UIImageView *bottomSep = [UIImageView addSepImageWithFrame:CGRectMake(0, 55, kScreenWidth, 1)
                                                     WithColor:MarketCellSepColor
                                                     WithAlpha:1];
    [self addSubview:bottomSep];
}

- (void)resetUIWithTag:(item_order_edirection)dirction
{
    for (UIButton *btn in self.subviews) {
        if([btn isKindOfClass:[UIButton class]]){
            [btn setSelected:(btn.tag == dirction)];
        }
    }
}

- (void)responseToBtn:(UIButton *)send
{
    if (self.orderDir) {
        item_order_edirection dir = (send.tag == 1) ?
        item_order_edirection_DirectionBuy :
        item_order_edirection_DirectionSell;
        self.orderDir(dir);
    }
    
    [self resetUIWithTag:(item_order_edirection)send.tag];
}

@end
