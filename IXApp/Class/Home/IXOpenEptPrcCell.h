//
//  IXOpenEptPrcCell.h
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXOpenEptPrcCell : UITableViewCell

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *expectPrice;
@property (nonatomic, strong) UILabel *expectDealPrcLbl;

@end
