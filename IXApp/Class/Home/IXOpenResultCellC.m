//
//  IXOpenResultCellC.m
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenResultCellC.h"
#import "NSString+FormatterPrice.h"
#import "IXUserDefaultM.h"

@interface IXOpenResultCellC()

@property (nonatomic, strong) UIImageView  *bgView;
@property (nonatomic, strong) UILabel *showName;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *showDir;
@property (nonatomic, strong) UILabel *dir;
@property (nonatomic, strong) UILabel *showType;
@property (nonatomic, strong) UILabel *type;
@property (nonatomic, strong) UILabel *showNum;
@property (nonatomic, strong) UILabel *num;
@property (nonatomic, strong) UILabel *showReqPrice;
@property (nonatomic, strong) UILabel *reqPrice;
@property (nonatomic, strong) UILabel *showExePrice;
@property (nonatomic, strong) UILabel *exePrice;
@property (nonatomic, strong) UILabel *showReqDate;
@property (nonatomic, strong) UILabel *reqDate;
@property (nonatomic, strong) UILabel *showExeDate;
@property (nonatomic, strong) UILabel *exeDate;
@property (nonatomic, strong) UILabel *showMargin;
@property (nonatomic, strong) UILabel *margin;
@property (nonatomic, strong) UILabel *showCommission;
@property (nonatomic, strong) UILabel *commission;

@end
@implementation IXOpenResultCellC

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(9, 0, kScreenWidth - 18,198)];
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)showName
{
    if (!_showName) {
        CGRect rect = CGRectMake(14.5, 14,kScreenWidth - 34, 16);
        _showName = [[UILabel alloc] initWithFrame:rect];
        _showName.font = PF_MEDI(13);
        _showName.text = LocalizedString(@"商品");
        _showName.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showName.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showName];
    }
    return _showName;
}

- (UILabel *)name
{
    if (!_name) {
        CGRect rect = CGRectMake(0, 18,VIEW_W(_bgView) - 14.5, 15);
        _name = [[UILabel alloc] initWithFrame:rect];
        _name.font = PF_MEDI(13);
        _name.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _name.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_name];
    }
    return _name;
}

- (UILabel *)showDir
{
    if (!_showDir) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showName) + 14,kScreenWidth - 34, 16);
        _showDir = [[UILabel alloc] initWithFrame:rect];
        _showDir.font = PF_MEDI(13);
        _showDir.text = LocalizedString(@"方向");
        _showDir.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showDir.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showDir];
    }
    return _showDir;
}

- (UILabel *)dir
{
    if (!_dir) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showName) + 14,VIEW_W(_bgView) - 14.5, 15);
        _dir = [[UILabel alloc] initWithFrame:rect];
        _dir.font = PF_MEDI(13);
        _dir.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _dir.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_dir];
    }
    return _dir;
}

- (UILabel *)showType
{
    if (!_showType) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showDir) + 14,kScreenWidth - 34, 16);        _showType = [[UILabel alloc] initWithFrame:rect];
        _showType.font = PF_MEDI(13);
        _showType.text = LocalizedString(@"类型");
        _showType.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showType.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showType];
    }
    return _showType;
}

- (UILabel *)type
{
    if (!_type) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showDir) + 14,VIEW_W(_bgView) - 14.5, 15);
        _type = [[UILabel alloc] initWithFrame:rect];
        _type.font = PF_MEDI(13);
        _type.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _type.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_type];
    }
    return _type;
}


- (UILabel *)showNum
{
    if (!_showNum) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showType) + 14,kScreenWidth - 34, 16);
        _showNum = [[UILabel alloc] initWithFrame:rect];
        _showNum.font = PF_MEDI(13);
        _showNum.text = LocalizedString(@"数量");
        _showNum.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showNum.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showNum];
    }
    return _showNum;
}

- (UILabel *)num
{
    if (!_num) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showType) + 14,VIEW_W(_bgView) - 14.5, 15);
        _num = [[UILabel alloc] initWithFrame:rect];
        _num.font = RO_REGU(15);
        _num.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _num.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_num];
    }
    return _num;
}

- (UILabel *)showReqPrice
{
    if (!_showReqPrice) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showNum) + 14,kScreenWidth - 34, 16);
        _showReqPrice = [[UILabel alloc] initWithFrame:rect];
        _showReqPrice.font = PF_MEDI(13);
        _showReqPrice.text = LocalizedString(@"请求价格");
        _showReqPrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showReqPrice.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showReqPrice];
    }
    return _showReqPrice;
}

- (UILabel *)reqPrice
{
    if (!_reqPrice) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showNum) + 14,VIEW_W(_bgView) - 14.5, 15);
        _reqPrice = [[UILabel alloc] initWithFrame:rect];
        _reqPrice.font = RO_REGU(15);
        _reqPrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _reqPrice.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_reqPrice];
    }
    return _reqPrice;
}

- (UILabel *)showExePrice
{
    if (!_showExePrice) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showReqPrice) + 14,kScreenWidth - 34, 16);
        _showExePrice = [[UILabel alloc] initWithFrame:rect];
        _showExePrice.font = PF_MEDI(13);
        _showExePrice.text = LocalizedString(@"执行价格");
        _showExePrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showExePrice.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showExePrice];
    }
    return _showExePrice;
}

- (UILabel *)exePrice
{
    if (!_exePrice) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showReqPrice) + 14,VIEW_W(_bgView) - 14.5, 15);
        _exePrice = [[UILabel alloc] initWithFrame:rect];
        _exePrice.font = RO_REGU(15);
        _exePrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _exePrice.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_exePrice];
    }
    return _exePrice;
}

- (UILabel *)showMargin
{
    if (!_showMargin) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showExePrice) + 14,kScreenWidth - 34, 16);
        _showMargin = [[UILabel alloc] initWithFrame:rect];
        _showMargin.font = PF_MEDI(13);
        _showMargin.text = LocalizedString(@"占用保证金");
        _showMargin.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showMargin.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showMargin];
    }
    return _showMargin;
}

- (UILabel *)margin
{
    if (!_margin) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showExePrice) + 14,VIEW_W(_bgView) - 14.5, 15);
        _margin = [[UILabel alloc] initWithFrame:rect];
        _margin.font = RO_REGU(15);
        _margin.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _margin.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_margin];
    }
    return _margin;
}

- (UILabel *)showCommission
{
    if (!_showCommission) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showMargin) + 14,kScreenWidth - 34, 16);
        _showCommission = [[UILabel alloc] initWithFrame:rect];
        _showCommission.font = PF_MEDI(13);
        _showCommission.text = LocalizedString(@"佣金");
        _showCommission.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showCommission.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showCommission];
    }
    return _showCommission;
}

- (UILabel *)commission
{
    if (!_commission) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showMargin) + 14,VIEW_W(_bgView) - 14.5, 15);
        _commission = [[UILabel alloc] initWithFrame:rect];
        _commission.font = RO_REGU(15);
        _commission.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _commission.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_commission];
    }
    return _commission;
}

- (UILabel *)showReqDate
{
    if (!_showReqDate) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showCommission) + 14,kScreenWidth - 34, 16);
        _showReqDate = [[UILabel alloc] initWithFrame:rect];
        _showReqDate.font = PF_MEDI(13);
        _showReqDate.text = LocalizedString(@"请求日期");
        _showReqDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showReqDate.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showReqDate];
    }
    return _showReqDate;
}

- (UILabel *)reqDate
{
    if (!_reqDate) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showCommission) + 14,VIEW_W(_bgView) - 14.5, 15);
        _reqDate = [[UILabel alloc] initWithFrame:rect];
        _reqDate.font = RO_REGU(15);
        _reqDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _reqDate.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_reqDate];
    }
    return _reqDate;
}

- (UILabel *)showExeDate
{
    if (!_showExeDate) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showReqDate) + 14,kScreenWidth - 34, 16);
        _showExeDate = [[UILabel alloc] initWithFrame:rect];
        _showExeDate.font = PF_MEDI(13);
        _showExeDate.text = LocalizedString(@"执行日期");
        _showExeDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showExeDate.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showExeDate];
    }
    return _showExeDate;
}

- (UILabel *)exeDate
{
    if (!_exeDate) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showReqDate) + 14,VIEW_W(_bgView) - 14.5, 15);
        _exeDate = [[UILabel alloc] initWithFrame:rect];
        _exeDate.font = RO_REGU(15);
        _exeDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _exeDate.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_exeDate];
    }
    return _exeDate;
}

- (void)reloadUIData:(IXOpenResultModel *)model
{
    UIImage *wImage = GET_IMAGE_NAME(@"common_result_back");
    UIImage * wImg = [wImage stretchableImageWithLeftCapWidth:wImage.size.width/2 topCapHeight:wImage.size.height/2];
    UIImage *dImage = GET_IMAGE_NAME(@"common_result_back_D");
    UIImage * dImg = [dImage stretchableImageWithLeftCapWidth:dImage.size.width/2 topCapHeight:dImage.size.height/2];
    
    self.bgView.dk_imagePicker = DKImageWithImgs(wImg, dImg);
    
    self.showName.text = LocalizedString(@"商品");
    self.name.text = model.name;
    
    self.showDir.text = LocalizedString(@"方向");
    if (model.dir == item_order_edirection_DirectionBuy) {
        self.dir.text = LocalizedString(@"买入");
        self.dir.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    } else {
        self.dir.text = LocalizedString(@"卖出");
        self.dir.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    
    self.showType.text = LocalizedString(@"类型");
    self.type.text = LocalizedString(model.cate);
    
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
        self.showNum.text = LocalizedString(@"数量");
    } else {
        self.showNum.text = LocalizedString(@"手数");
    }
    NSString *numStr = [NSString stringWithFormat:@"%@%@",model.num,[IXDataProcessTools showCurrentUnitLanName:model.unitLanName]];
    self.num.text = numStr;
    if ([[IXDataProcessTools showCurrentUnitLanName:model.unitLanName] length]) {
        [IXDataProcessTools resetLabel:self.num leftContent:model.num leftFont:15 WithContent:numStr rightFont:15 fontType:YES];
    }
    
    self.showReqPrice.text = LocalizedString(@"请求价格");
    self.reqPrice.text = model.reqPrice;
    CGRect frame = self.showExePrice.frame;
    if (model.type == item_order_etype_TypeLimit || model.type == item_order_etype_TypeStop ) {
        self.showExePrice.hidden = YES;
        self.exePrice.hidden = YES;
        self.showMargin.hidden = YES;
        self.margin.hidden = YES;
        self.showCommission.hidden = YES;
        self.commission.hidden = YES;
        self.showExeDate.hidden  = YES;
        self.exeDate.hidden = YES;
        
        self.showReqDate.text = LocalizedString(@"请求日期");
        frame = self.showReqDate.frame;
        frame.origin.y = GetView_MaxY(self.showReqPrice) + 14;
        self.showReqDate.frame = frame;
        
        self.reqDate.text = model.reqDate;
        frame = self.reqDate.frame;
        frame.origin.y = VIEW_Y(self.showReqDate);
        self.reqDate.frame = frame;
        
        
        self.bgView.frame = CGRectMake(9, 0, kScreenWidth - 18,196);
    } else {
        self.showExePrice.text = LocalizedString(@"开仓价格");
        frame.origin.y = GetView_MaxY(self.showReqPrice) + 14;
        self.showExePrice.frame = frame;
        
        self.exePrice.text = model.exePrice;
        frame = self.exePrice.frame;
        frame.origin.y = VIEW_Y(self.showExePrice);
        self.exePrice.frame = frame;
        
        self.showMargin.text = LocalizedString(@"占用保证金");
        frame = self.showMargin.frame;
        frame.origin.y = GetView_MaxY(self.showExePrice) + 14;
        self.showMargin.frame = frame;
        
        self.margin.text = [NSString formatterPrice:model.margin WithDigits:2];
        frame = self.margin.frame;
        frame.origin.y = VIEW_Y(self.showMargin);
        self.margin.frame = frame;
        
        if ([model.commission doubleValue] == 0) {
            self.showCommission.hidden = YES;
            self.commission.hidden = YES;
            
            self.showReqDate.text = LocalizedString(@"请求日期");
            frame = self.showReqDate.frame;
            frame.origin.y = GetView_MaxY(self.showMargin) + 14;
            self.showReqDate.frame = frame;
            
            self.bgView.frame = CGRectMake(9, 0, kScreenWidth - 18,285);
        } else {
            self.showCommission.hidden = NO;
            self.commission.hidden = NO;
            self.commission.text = model.commission;
            
            self.showCommission.text = LocalizedString(@"佣金");
            frame = self.showCommission.frame;
            frame.origin.y = GetView_MaxY(self.showMargin) + 14;
            self.showCommission.frame = frame;
            
            self.commission.text = model.commission;
            frame = self.commission.frame;
            frame.origin.y = VIEW_Y(self.showCommission);
            self.commission.frame = frame;
            
            self.showReqDate.text = LocalizedString(@"请求日期");
            frame = self.showReqDate.frame;
            frame.origin.y = GetView_MaxY(self.showCommission) + 14;
            self.showReqDate.frame = frame;
            
            
            self.bgView.frame = CGRectMake(9, 0, kScreenWidth - 18,314);
        }
        
        self.reqDate.text = model.reqDate;
        frame = self.reqDate.frame;
        frame.origin.y = VIEW_Y(self.showReqDate);
        self.reqDate.frame = frame;
        
        self.showExeDate.text = LocalizedString(@"开仓日期");
        frame = self.showExeDate.frame;
        frame.origin.y = GetView_MaxY(self.showReqDate) + 14;
        self.showExeDate.frame = frame;
        
        self.exeDate.text = model.exeDate;
        frame = self.exeDate.frame;
        frame.origin.y = VIEW_Y(self.showExeDate);
        self.exeDate.frame = frame;
    }
    
}

@end

