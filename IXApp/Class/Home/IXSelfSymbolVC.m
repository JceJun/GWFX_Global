//
//  IXSelfSymbolVC.m
//  IXApp
//
//  Created by Bob on 2016/12/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSelfSymbolVC.h"

#import "IXQuoteCell.h"
#import "IXQuoteThumbCell.h"


#import "IXDBGlobal.h"
#import "IXDBSymbolSubMgr.h"
#import "IXDBSearchBrowseMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDataProcessTools.h"
#import "IxItemSymbolSub.pbobjc.h"

#import "IXTCPRequest.h"
#import "IXTradeDataCache.h"

#import "IXOpenRootVC.h"
#import "IXSymbolDetailVC.h"
#import "IXSymSearchVC.h"

#import "IXOptionHeadV.h"
#import "IXChooseSymbolCataView.h"

#import "IXLastQuoteM.h"
#import "IXQuoteDistribute.h"
#import "IXAccountBalanceModel.h"
#import "IXADMgr.h"

#define MAXSUBCOUNT 20
#define OBJECT @"object"
#define STATUS @"status"

@interface IXSelfSymbolVC ()
<UITableViewDelegate,
UITableViewDataSource,
IXQuoteThumbCellDelegate>
{
    NSInteger pageCount;                        //每页记录条数
    NSInteger currentPage;                      //当前页数
    NSInteger cellType;
//    NSInteger numberOfCount;
    
    NSInteger currentSubStartTag;
    NSInteger currentSearchIndex;               //当前索引哪个分类
    NSInteger startOffset;                      //当前偏移
    BOOL didLoadAllData;
    BOOL needReSubDynQuote;
    
    NSMutableDictionary *priceDic;              //行情系统Price信息
    NSMutableDictionary *lastClosePriceDic;     //行情系统Price信息
    
    NSMutableArray *subDynPriceArr;             //当前订阅Price信息
    NSMutableArray *subLastClosePriceArr;
    NSMutableArray *tradeStatusArr;
    
    NSMutableDictionary *marketInfoDic;

    NSMutableDictionary *positionMapSymbol;
    
    NSMutableDictionary *cacheRefreashQuote;
    
}

@property (nonatomic, strong) UITableView *contentTable;    //表单

@property (nonatomic, strong) UIView   *optionBgView;
@property (nonatomic, strong) UIButton *optionView;
@property (nonatomic, strong) UIView   *optionalDefView;      //缺省页
@property (nonatomic, strong) IXChooseSymbolCataView *chooseCata;


@property (nonatomic, strong) NSMutableArray *dataSource;   //交易系统Symbol信息
@property (nonatomic, strong) NSArray *childCataArr;        //交易系统Symbol信息

@property (nonatomic, strong) NSArray *chooseMarketArr;

@end

@implementation IXSelfSymbolVC

- (id)init
{
    self = [super init];
    if ( self ) {
        
        IXAccountBalanceModel_listen_regist(self, AB_CACHE_NUMBER_CHANGE);
        
        IXTradeData_listen_regist( self, PB_CMD_SYMBOL_SUB_ADD);
        IXTradeData_listen_regist( self, PB_CMD_SYMBOL_SUB_DELETE);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadView)
                                                     name:kSaveSubSymbolCellStyle
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadView)
                                                     name:DKNightVersionThemeChangingNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    IXAccountBalanceModel_listen_resign(self, AB_CACHE_NUMBER_CHANGE);

    IXTradeData_listen_resign( self, PB_CMD_SYMBOL_SUB_ADD);
    IXTradeData_listen_resign( self, PB_CMD_SYMBOL_SUB_DELETE);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self initData];
    [self.view addSubview:self.contentTable];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self subscribeDynamicPrice];
}

- (void)initData
{
    subDynPriceArr = [NSMutableArray array];
    subLastClosePriceArr = [NSMutableArray array];
    tradeStatusArr = [NSMutableArray array];
    
    lastClosePriceDic = [NSMutableDictionary dictionary];
    cellType = [IXEntityFormatter getCellStyle];
    priceDic = [NSMutableDictionary dictionary];
    marketInfoDic = [NSMutableDictionary dictionary];
    
    _chooseMarketArr = @[@[LocalizedString(@"全部"),@"SH",@"SZ",@"HK"],
                         @[@"SF",@"SC",@"DC",@"ZC"],
                         @[@"GW",@"FG"]];
}

#pragma mark -
#pragma mark - IXTradeDataKVO
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXAccountBalanceModel_isSameKey( keyPath , AB_CACHE_NUMBER_CHANGE)) {
        [_contentTable reloadData];
    }else if( IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_SUB_ADD) ){
        if ( subDynPriceArr && 0 != subDynPriceArr.count ) {
            [self canceQuotePrice];
        }
        [self refreashData];
        
    }else if( IXTradeData_isSameKey( keyPath, PB_CMD_SYMBOL_SUB_DELETE) ){

        proto_symbol_sub_delete  *pb = ((IXTradeDataCache *)object).pb_symbol_sub_delete;
        if (pb.result == 0) {
            if ( subDynPriceArr && 0 != subDynPriceArr.count ) {
                [self canceQuotePrice];
            }
            [self refreashData];
        }
    }
}

- (void)refreashData
{
//    _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsSub]];
    _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsSubNew]];

    [self refreashState];
    [self subscribeDynamicPrice];

    [_contentTable setScrollEnabled:!(_dataSource.count == 0)];
    [_contentTable reloadData];
}

//刷新状态
- (void)refreashState
{
    [tradeStatusArr removeAllObjects];
    [subDynPriceArr removeAllObjects];
    
    NSMutableArray *dataArr = [NSMutableArray array];
    for (int i = 0; i < _dataSource.count; i++) {
        NSDictionary *model = _dataSource[i];
        [dataArr addObject:@{@"symbolId":@([model integerForKey:kID])}];
        
        IXSymbolTradeState state = [IXDataProcessTools symbolIsCanTradeNew:[model integerForKey:kID]];
        [tradeStatusArr addObject:@(state)];
    }

#warning 先屏蔽交易状态的排序
//    NSInteger disableFlag = tradeStatusArr.count - 1;
//    for ( int i = 0; i < tradeStatusArr.count; i++ ) {
//        
//        if([tradeStatusArr[i] integerValue] != IXSymbolTradeStateNormal ){
//            
//            NSNumber *number = tradeStatusArr[disableFlag];
//            while ( [number integerValue] != IXSymbolTradeStateNormal ) {
//                disableFlag--;
//                if ( disableFlag <= 0 ) {
//                    break;
//                }
//            }
//            if ( disableFlag > i ) {
//                [_dataSource exchangeObjectAtIndex:i  withObjectAtIndex:disableFlag];
//                [tradeStatusArr exchangeObjectAtIndex:i  withObjectAtIndex:disableFlag];
//                i--;
//            }else{
//                break;
//            }
//        }
//    }
    
    subDynPriceArr = [[IXDBSymbolHotMgr queryBatchSymbolMarketIdsBySymbolIds:dataArr] mutableCopy];
    for (NSInteger count = 0; count < subDynPriceArr.count; count++) {
        NSDictionary *dic = subDynPriceArr[count];
        [marketInfoDic setValue:dic forKey:[dic objectForKey:kID]];
    }
}
#pragma mark 数据处理
/**
 *  行情回调
 *
 *  @param data 回调的数据
 *  @param cmd  对应的cmd
 */
- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if( cmd ==  CMD_QUOTE_SUB || cmd == CMD_QUOTE_PUB ){
        [self refreashVisualCellWithQuoteArr:arr WithCmd:cmd];
    }else if ( cmd == CMD_QUOTE_CLOSE_PRICE ){
        NSMutableArray *resultAry = [NSMutableArray arrayWithArray:arr];
        for ( IXLastQuoteM *model in resultAry ) {
            [lastClosePriceDic setObject:[model mutableCopy]
                                  forKey:[NSString stringWithFormat:@"%ld",(long)model.symbolId]];
        }
        
        //刷新可视区域的行情
        for ( UITableViewCell *cell in [_contentTable visibleCells] ) {
            if ( [cell isKindOfClass:[IXQuoteCell class]] ) {
                NSDictionary *model = _dataSource[cell.tag];
                IXLastQuoteM *qModel = [self lcModelWithSymId:[model integerForKey:kID]];
                if (qModel) {
                    [(IXQuoteCell *)cell setLastClosePriceInfo:qModel];
                }
            }
        }
        
    }else if (cmd == CMD_QUOTE_UNSUB){
        needReSubDynQuote = NO;
    }
}

- (void)cancelVisualQuote
{
    if ( subDynPriceArr && subDynPriceArr.count > 0 ) {
        NSArray *unSubcribeArr = [subDynPriceArr filteredArrayUsingPredicate:
                                  [NSPredicate predicateWithFormat:@"NOT (SELF in %@)",
                                   [IXAccountBalanceModel shareInstance].dynQuoteArr]];
        if ( unSubcribeArr && unSubcribeArr.count > 0 ) {
            [[IXTCPRequest shareInstance] unSubQuotePriceWithSymbolAry:unSubcribeArr];
        }
    }
}

- (void)subscribeVisualQuote
{
    if ( subDynPriceArr && subDynPriceArr.count > 0 ) {
        [[IXTCPRequest shareInstance] subscribeDynPriceWithSymbolAry:subDynPriceArr];
        [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:subDynPriceArr];
    }
}

#pragma mark 刷新UI
- (void)refreashVisualCellWithQuoteArr:(NSMutableArray *)arr WithCmd:(uint16)cmd
{
    //保存返回的行情
    for ( IXQuoteM *model in arr ) {
        [priceDic setObject:@{OBJECT:[model mutableCopy],
                              STATUS:@(1)}
                     forKey:[NSString stringWithFormat:@"%ld",(long)model.symbolId]];
    }
    
    //刷新可视区域的行情
    for ( UITableViewCell *cell in [_contentTable visibleCells] ) {
        if ( [cell isKindOfClass:[IXQuoteCell class]] ) {
//            //连续动态行情，需要检查行情是否需要付费，已经支付状态
//            if ( cmd == CMD_QUOTE_PUB ) {
//                if ( [self refreashFeeQuote:model] ) {
//                    continue;
//                }
//            }
            NSDictionary *model = _dataSource[cell.tag];
            NSInteger symId = [model integerForKey:kID];
            id value = [self dynQuoteDataModelWithSymbolId:symId];
            if ( [value isKindOfClass:[NSDictionary class]] ) {
                NSNumber *quoteStatus = value[STATUS];
                if ([quoteStatus integerValue] == 1) {
                    NSMutableDictionary *quote = [NSMutableDictionary dictionaryWithDictionary:value];
                    [quote setValue:@(0) forKey:STATUS];
                    [priceDic setValue:quote forKey:[self strSymbolWithId:symId]];
                    [(IXQuoteCell *)cell setQuotePriceInfo:value[OBJECT]];
                }
            }else if( [value isKindOfClass:[IXQuoteM class]] ){
                [(IXQuoteCell *)cell setQuotePriceInfo:[priceDic objectForKey:[NSString stringWithFormat:@"%ld",(long)symId]]];
            }
            
        }else if( [cell isKindOfClass:[IXQuoteThumbCell class]] ){
            
            NSDictionary *model = _dataSource[cell.tag];
            NSInteger symId = [model integerForKey:kID];

            id value = [self dynQuoteDataModelWithSymbolId:symId];
            if ( [value isKindOfClass:[NSDictionary class]] ) {
                NSNumber *quoteStatus = value[STATUS];
                if ([quoteStatus integerValue] == 1) {
                    NSMutableDictionary *quote = [NSMutableDictionary dictionaryWithDictionary:value];
                    [quote setValue:@(0) forKey:STATUS];
                    [priceDic setValue:quote forKey:[self strSymbolWithId:symId]];
                    [(IXQuoteThumbCell *)cell setQuotePriceInfo:value[OBJECT]];
                }
            }else if( [value isKindOfClass:[IXQuoteM class]] ){
                [(IXQuoteThumbCell *)cell setQuotePriceInfo:[priceDic objectForKey:[NSString stringWithFormat:@"%ld",(long)symId]]];
            }
        }
    }
}

- (void)reloadTableView
{
    [subDynPriceArr removeAllObjects];
    
    [_dataSource removeAllObjects];
    _dataSource = nil;
    
    [_contentTable reloadData];
    [self subscribeDynamicPrice];
}

- (void)reloadView
{
    cellType = [IXEntityFormatter getCellStyle];
    [_contentTable reloadData];
}

- (IXLastQuoteM *)lcModelWithSymId:(NSInteger)symId
{
    NSString *symbolKey = [self strSymbolWithId:symId];
    IXLastQuoteM *model = [lastClosePriceDic objectForKey:symbolKey];
    if (!model) {
        IXLastQuoteM *model = [IXDataProcessTools queryYesterdayPriceBySymbolId:symId];
        if (model && 0 != model.nLastClosePrice) {
            [lastClosePriceDic setValue:model forKey:symbolKey];
            return model;
        }else{
            return nil;
        }
    }
    return model;
}

- (id)dynQuoteDataModelWithSymbolId:(NSInteger)symbolId
{
    NSString *symbolKey = [self strSymbolWithId:symbolId];
    IXQuoteM *model = [priceDic objectForKey:symbolKey];
    if (!model) {
        model = [IXDataProcessTools queryQuoteDataBySymbolId:symbolId];
        if (!model || 0 == model.symbolId) {
            return nil;
        }else{
            [priceDic setValue:model forKey:symbolKey];
        }
    }
    return [priceDic objectForKey:symbolKey];
}

/**
 *  订阅数据
 */
- (void)subscribeDynamicPrice
{
    [self subscribeVisualQuote];
}

//取消订阅
- (void)canceQuotePrice{
    [self cancelVisualQuote];
}


#pragma mark tableView
#pragma mark - tableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( 0 == cellType ) {
        IXQuoteCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXQuoteCell class])];
        cell.tag = indexPath.row;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell elemetData];
        
        NSDictionary *dic = self.dataSource[cell.tag];
        NSString *posNum = [[IXAccountBalanceModel shareInstance].cachePosNumDic stringForKey:
                            [self strSymbolWithId:[dic integerForKey:kID]]];

        IXSymbolTradeState state = (tradeStatusArr.count > cell.tag) ?
                             [tradeStatusArr[cell.tag] integerValue] :
                             IXSymbolTradeStateNormal;
        NSString *mName = [IXEntityFormatter getMarketNameWithMarketId:
                           [self getMarketIdWithSymbolId:[dic integerForKey:kID]]];
        [cell setSymbolModel:dic
             WithPositionNum:posNum
              WithMarketName:mName
              WithTradeState:state];
        
        id value = [self dynQuoteDataModelWithSymbolId:[dic integerForKey:kID]];
        if( [value isKindOfClass:[NSDictionary class]] ){
            [(IXQuoteCell *)cell setQuotePriceInfo:value[OBJECT]];
        }else if( [value isKindOfClass:[IXQuoteM class]] ){
            [(IXQuoteCell *)cell setQuotePriceInfo:value];
        }else{
            [cell resetQuotePrice];
        }
       
        IXLastQuoteM *lastPrice = [self lcModelWithSymId:[dic integerForKey:kID]];
        if( lastPrice ){
            [cell  setLastClosePriceInfo:lastPrice];
        }else{
            [cell resetLastClosePriceInfo];
        }
        return cell;
        
    }else{
        IXQuoteThumbCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXQuoteThumbCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.tag = indexPath.row;
        cell.thumbDelegate = self;
        
        NSDictionary *dic = self.dataSource[cell.tag];
        IXSymbolTradeState state = (tradeStatusArr.count > cell.tag) ?
                             [tradeStatusArr[cell.tag] integerValue] :
                             IXSymbolTradeStateNormal;
        NSString *mName = [IXEntityFormatter getMarketNameWithMarketId:
                           [self getMarketIdWithSymbolId:[dic integerForKey:kID]]];
        [cell setSymbolModel:dic
              WithMarketName:mName
              WithTradeState:state];
        
        id value = [self dynQuoteDataModelWithSymbolId:[dic integerForKey:kID]];
        
        if( [value isKindOfClass:[NSDictionary class]] ){
            [(IXQuoteThumbCell *)cell setQuotePriceInfo:value[OBJECT]];
        }else if( [value isKindOfClass:[IXQuoteM class]] ){
            [(IXQuoteThumbCell *)cell setQuotePriceInfo:value];
        }else{
            [cell resetQuotePrice];
        }
        
        return cell;
    }    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( 0 == cellType )
        return 55;
    return 112;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.dataSource.count == 0) {
        return 284;
    } else {
        if (![IXADMgr shareInstance].haveCloseSelf && [IXADMgr shareInstance].selfAdArr.count) {
            return 44 + 94;//需加上广告的高度
        } else {
            return 44;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if( self.dataSource.count == 0 ){
        self.headView.hidden = YES;
        return self.optionalDefView;
    }else{
        self.headView.hidden = NO;
        [self.optionBgView addSubview:self.optionView];
        if (![IXADMgr shareInstance].haveCloseSelf && [IXADMgr shareInstance].selfAdArr.count) {
            self.optionBgView.frame = CGRectMake( 0, 0, kScreenWidth, 44 + 94);
            self.optionView.frame = CGRectMake( 0, 0, kScreenWidth, 44);
        } else {
            self.optionBgView.frame = CGRectMake( 0, 0, kScreenWidth, 44);
        }
        return self.optionBgView;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ( 0 == cellType ) {
        [self pushToTradeWithRow:indexPath.row WithDirection:item_order_edirection_DirectionSell];
    }else{
        [self pushToTradeWithRow:indexPath.row WithDirection:item_order_edirection_DirectionBuy];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return LocalizedString(@"删除");
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *model = _dataSource[indexPath.row];
    NSInteger accId = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    NSDictionary *subDic = [IXDBSymbolSubMgr querySymbolSubBySymbolId:[model integerForKey:kID]
                                                            accountId:accId];
    if (subDic && subDic[kID]) {
        [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
        proto_symbol_sub_delete *delSubSymbol = [[proto_symbol_sub_delete alloc] init];
        [delSubSymbol setId_p:[subDic[kID] doubleValue]];
        [delSubSymbol setAccountid:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        [[IXTCPRequest shareInstance] delSubSymbolWithParam:delSubSymbol];
    }
}


#pragma mark 组装搜索历史
- (NSDictionary *)packageSearchHistory:(IXSymbolM *)model {
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[kID] = @(model.id_p);
    dic[kName] = model.name;
    dic[kEnable] = @(model.isSub);
    dic[kMarketName] = model.marketName;
    dic[kLanguageName] = model.languageName;
    dic[kSource] = model.source;
    return dic;
}


//添加自选
- (void)responseToSubSymbol
{
    IXSymSearchVC *searchVC = [[IXSymSearchVC alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.parentVC.navigationController pushViewController:searchVC animated:YES];
}


#pragma mark 跳转到产品详情或者新交易
- (void)responseToTradeWithDirection:(item_order_edirection)direction WithTag:(NSInteger)tag
{
    if ( tradeStatusArr && tradeStatusArr.count > tag ) {
        IXSymbolTradeState state = [tradeStatusArr[tag] integerValue];
        if (![IXDataProcessTools canTradeByState:state orderDir:direction]) {
            return;
        }
        IXOpenRootVC *openVC = [[IXOpenRootVC alloc] init];
        NSDictionary *dataDic = _dataSource[tag];
        
        NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:[dataDic integerForKey:kID]];
        IXSymbolM *model = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
        if(!model.contractSizeNew){
            model.contractSizeNew = 1;
        }
        IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
        tradeModel.symbolModel = model;
        tradeModel.marketId = [self getMarketIdWithSymbolId:model.id_p];

        tradeModel.direction = direction;

        IXQuoteM *dic;
        id value = [self dynQuoteDataModelWithSymbolId:model.id_p];
        if( [value isKindOfClass:[NSDictionary class]] ){
            dic = value[OBJECT];
        }else if( [value isKindOfClass:[IXQuoteM class]] ){
            dic = value;
        }
        if ( dic ) {
            tradeModel.quoteModel = dic;
        }
        openVC.tradeModel = tradeModel;
         
        IXLastQuoteM *lastPrice = [self lcModelWithSymId:model.id_p];
        if (lastPrice) {
            openVC.openModel.nLastClosePrice = lastPrice.nLastClosePrice;
            tradeModel.nLastPrice = lastPrice.nLastClosePrice;
        }
        openVC.openModel.refreashFeeQuote = [self refreashFeeQuote:model];


        openVC.hidesBottomBarWhenPushed = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.parentVC.navigationController pushViewController:openVC animated:YES];
        });
    }
}

- (void)pushToTradeWithRow:(NSInteger)row WithDirection:(item_order_edirection)direction
{
    if ( _dataSource && _dataSource.count > row ) {
        
        IXSymbolDetailVC *openVC = [[IXSymbolDetailVC alloc] init];
        IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];

        NSDictionary *dataDic = _dataSource[row];
        NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:[dataDic integerForKey:kID]];
        IXSymbolM *model = [IXDataProcessTools appendLanguageNameAndLabelBySymbolModel:symDic];
        if (!model.contractSizeNew) {
            model.contractSizeNew = 1;
        }
        tradeModel.symbolModel = model;
        tradeModel.marketId = [self getMarketIdWithSymbolId:model.id_p];
        IXQuoteM *dic;
        id value = [self dynQuoteDataModelWithSymbolId:model.id_p];
        if( [value isKindOfClass:[NSDictionary class]] ){
            dic = value[OBJECT];
        }else if( [value isKindOfClass:[IXQuoteM class]] ){
            dic = value;
        }
        if (dic) {
            tradeModel.quoteModel = dic;
        }
        openVC.tradeModel = tradeModel;

        IXLastQuoteM *lastPrice = [self lcModelWithSymId:model.id_p];
        if (lastPrice) {
            openVC.nLastClosePrice =  lastPrice.nLastClosePrice;
        }
        openVC.refreashFeeQuote = [self refreashFeeQuote:model];
        
        openVC.hidesBottomBarWhenPushed = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.parentVC.navigationController pushViewController:openVC animated:YES];
        });

    }
   
}


#pragma mark ScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:sender afterDelay:0];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self resetSubArr];
}

- (void)resetSubArr
{
    if ( _dataSource.count == 0 || _contentTable.visibleCells.count == 0) {
        return;
    }
     needReSubDynQuote = YES;
    //获取停止滑动后，可视区域的第一行的Symbol
    NSInteger startTag = 1;
    for (UITableViewCell *cell in _contentTable.visibleCells) {
        if ([cell isKindOfClass:[IXQuoteCell class]]||
            [cell isKindOfClass:[IXQuoteThumbCell class]]) {
            startTag = cell.tag;
            break;
        }
    }
    //如果可视区域第一行的symbol在订阅列表，则不更新列表，
    //否则需要重新订阅
    //往上和往下均缓冲一页订阅
    if( _contentTable.visibleCells[0].tag <= currentSubStartTag + 5
       || [_contentTable.visibleCells lastObject].tag  >= currentSubStartTag + MAXSUBCOUNT - 5 ||
       (_contentTable.visibleCells[0].tag <= 1 && _contentTable.visibleCells[0].tag < currentSubStartTag) ||
       ([_contentTable.visibleCells lastObject].tag > currentSubStartTag &&
        [_contentTable.visibleCells lastObject].tag >= _dataSource.count - 1) ){
           //取消订阅可视区域以外内容的订阅
           NSArray *unSubcribeArr = [subDynPriceArr filteredArrayUsingPredicate:
                                     [NSPredicate predicateWithFormat:@"NOT (SELF in %@)",
                                      [IXAccountBalanceModel shareInstance].dynQuoteArr]];
           if ( unSubcribeArr && unSubcribeArr.count > 0 ) {
               [[IXTCPRequest shareInstance] unSubQuotePriceWithSymbolAry:unSubcribeArr];
           }
           [subDynPriceArr removeAllObjects];
           
           currentSubStartTag = startTag;
           for ( NSInteger count = 0; count < MAXSUBCOUNT && count + startTag <_dataSource.count; count++) {
               NSDictionary *model = _dataSource[count+startTag];
               NSInteger symId = [model integerForKey:kID];
               NSInteger mId = [self getMarketIdWithSymbolId:symId];
               if (![self lcModelWithSymId:symId]) {
                   [subLastClosePriceArr addObject:@{kID:@(symId),
                                                     kMarketId:@(mId)}];
               }
               [subDynPriceArr addObject:@{kID:@(symId),
                                           kMarketId:@(mId)}];
           }
           [[IXTCPRequest shareInstance] subscribeDynPriceWithSymbolAry:subDynPriceArr];
           
           //可视区域的Symbol没有订阅昨日收盘价，则需要订阅
           if ( subLastClosePriceArr ) {
               [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:subLastClosePriceArr];
           }
       }
}


#pragma mark load module
/**
 *  初始化表单
 */
- (UITableView *)contentTable
{
    if ( !_contentTable ) {
        
        CGRect frame = self.view.bounds;
        frame.size.height -= kNavbarHeight + kTabbarHeight;
        
        _contentTable = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _contentTable.showsVerticalScrollIndicator = NO;
        _contentTable.estimatedSectionHeaderHeight = 0;
        
        _contentTable.dk_separatorColorPicker = DKLineColor;
        _contentTable.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _contentTable.separatorInset = UIEdgeInsetsMake( 0, -20, 0, 0);
        
        _contentTable.delegate = self;
        _contentTable.dataSource = self;
        [_contentTable registerClass:[IXQuoteCell class] forCellReuseIdentifier:
                        NSStringFromClass([IXQuoteCell class])];
        [_contentTable registerClass:[IXQuoteThumbCell class] forCellReuseIdentifier:
                        NSStringFromClass([IXQuoteThumbCell class])];
    }
    return _contentTable;
}


- (IXOptionHeadV *)headView
{
    IXOptionHeadV *headView = [[IXOptionHeadV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 33)];
    weakself;
    headView.orderKind = ^(FilterType kind){
        switch (kind) {
            case FilterType_All:{
                weakSelf.chooseCata.dataSourceAry = [weakSelf.chooseMarketArr mutableCopy];
                [weakSelf.chooseCata show];
            }
                break;
            default:
                break;
        }
    };
    return headView;
}

- (UIButton *)optionView
{
    if ( !_optionView ) {
        _optionView = [UIButton buttonWithType:UIButtonTypeCustom];
        _optionView.frame = CGRectMake( 0, 0, kScreenWidth, 44);
        [_optionView setTitle:LocalizedString(@"添加自选")
                      forState:UIControlStateNormal];
        [_optionView setTitleColor:MarketSymbolNameColor
                           forState:UIControlStateNormal];
        [_optionView.titleLabel setFont:PF_MEDI(13)];
        [_optionView addTarget:self
                         action:@selector(responseToSubSymbol)
               forControlEvents:UIControlEventTouchUpInside];
        [_optionView setImage:[UIImage imageNamed:@"optional_add"]
                      forState:UIControlStateNormal];
        
        NSString *str = LocalizedString(@"添加自选");
        NSInteger width = [IXEntityFormatter getContentWidth:str WithFont:PF_MEDI(13)] + 2;
        [_optionView setImageEdgeInsets:UIEdgeInsetsMake( 15, (kScreenWidth - width)/2 - 13.5, 15, (kScreenWidth + width)/2)];
    }
    return  _optionView;
}

- (UIView *)optionBgView
{
    if (!_optionBgView) {
        _optionBgView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 44)];
    }
    return _optionBgView;
}

- (UIView *)optionalDefView
{
    if ( !_optionalDefView ) {
        _optionalDefView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 240)];
        
        CGFloat originX = (kScreenWidth - 104)/2;
        UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake( originX, 99, 104, 98)];
        logoImg.dk_imagePicker = DKImageNames(@"optional_noneMsg_avatar", @"optional_noneMsg_avatar_D");;
        [_optionalDefView addSubview:logoImg];
        
        UILabel *tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 217, kScreenWidth, 18)
                                             WithFont:PF_MEDI(15)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0x99abba
                                           dTextColor:0x4c6072];
        tipLbl.text = LocalizedString(@"暂无自选产品");
        [_optionalDefView addSubview:tipLbl];
        
        UIButton *subSymbolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_optionalDefView addSubview:subSymbolBtn];
        
        subSymbolBtn.frame = CGRectMake( 0, 240, kScreenWidth, 44);
        [subSymbolBtn setTitle:LocalizedString(@"添加自选")
                      forState:UIControlStateNormal];
        [subSymbolBtn dk_setTitleColorPicker:DKGrayTextColor forState:UIControlStateNormal];
        [subSymbolBtn.titleLabel setFont:PF_MEDI(13)];
        [subSymbolBtn addTarget:self
                         action:@selector(responseToSubSymbol)
               forControlEvents:UIControlEventTouchUpInside];
        [subSymbolBtn setImage:[UIImage imageNamed:@"optional_add"]
                      forState:UIControlStateNormal];
        
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"添加自选") WithFont:PF_MEDI(13)];
        CGFloat offsetX = (kScreenWidth - width)/2 - 13.5;
        [subSymbolBtn setImageEdgeInsets:UIEdgeInsetsMake( 15, offsetX, 15, (kScreenWidth + width)/2 )];
    }
    return _optionalDefView;
}

- (IXChooseSymbolCataView *)chooseCata
{
    if (!_chooseCata) {
        _chooseCata = [[IXChooseSymbolCataView alloc] initWithCataAry:@[]
                                                            WithChoose:nil];
                       
        _chooseCata.type = Symbol_AllType;
        _chooseCata.chooseSymbol = ^(NSInteger btnTag){
            //刷新行情
        };
    }
    return _chooseCata;
}


- (BOOL)refreashFeeQuote:(IXSymbolM *)model
{
    if ( !cacheRefreashQuote ) {
        cacheRefreashQuote = [NSMutableDictionary dictionary];
    }
    
    NSString *key = [self strSymbolWithId:model.cataId];
    if ( ![cacheRefreashQuote objectForKey:key] ) {
#warning 查询数据库，获取当前Symbol行情是否需要付费；若需要确认支付状态
        [cacheRefreashQuote setValue:@(NO) forKey:key];
        return NO;
    }
    return [[cacheRefreashQuote objectForKey:key] boolValue];

}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
//        _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsSub]];
        _dataSource = [NSMutableArray arrayWithArray:[IXDataProcessTools queryAllSymbolsSubNew]];
        [self refreashState];
    }
    return _dataSource;
}

- (NSInteger)getMarketIdWithSymbolId:(NSInteger)symbolId
{
    if ( [marketInfoDic objectForKey:[IXEntityFormatter integerToString:symbolId]] ) {
        return [[(NSDictionary *)[marketInfoDic objectForKey:[IXEntityFormatter integerToString:symbolId]] objectForKey:@"marketId"] integerValue];
    }else{
        NSDictionary *dic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:symbolId];
        NSInteger marketId = [dic integerForKey:kMarketId];
        [marketInfoDic setValue:@(marketId) forKey:[IXEntityFormatter integerToString:symbolId]];
        
        return marketId;
    }
}


- (NSString *)strSymbolWithId:(uint64)symbolId
{
    return [NSString stringWithFormat:@"%llu",symbolId];
}

@end
