//
//  IXOpenResultCalM.h
//  IXApp
//
//  Created by Evn on 2017/8/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IXOpenResultVC;
@interface IXOpenResultCalM : NSObject

+ (NSArray *)resetMarginAssetWithVolume:(double)volume
                               WithRoot:(IXOpenResultVC *)root;

@end
