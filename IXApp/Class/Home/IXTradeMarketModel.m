//
//  IXTradeMarketModel.m
//  IXApp
//
//  Created by Bob on 2016/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeMarketModel.h"
 
@implementation IXTradeMarketModel

- (id)init
{
    self = [super init];
    if (self) {
        [self resetInitInfo];
    }
    return self;
}

- (void)resetInitInfo
{
    _repeatCount = 1;
    _requestType = MARKETORDER;
    _requestExpire = LocalizedString(@"当日");
    _tradeExpireArr = @[LocalizedString(@"当日"),LocalizedString(@"当周")];
    _tradeTypeArr = @[MARKETORDER,LIMITORDER,STOPORDER];
    _direction = item_order_edirection_DirectionBuy;
}

- (void)updateTradeState
{
    IXSymbolTradeState state = [IXDataProcessTools symbolModelIsCanTrade:_symbolModel];
    _canBuy = (state == IXSymbolTradeStateNormal || state == IXSymbolTradeStateBuyNormal);
    _canSell = (state == IXSymbolTradeStateNormal || state == IXSymbolTradeStateSellNormal);
}

- (void)setSymbolModel:(IXSymbolM *)symbolModel
{
    _symbolModel = symbolModel;
    _parentId = [IXDataProcessTools queryParentIdBySymbolId:_symbolModel.id_p];
    [self updateTradeState];
}

- (void)setQuoteModel:(IXQuoteM *)quoteModel
{
    _quoteModel = quoteModel;
}

- (NSTimeInterval)getCurrentTimeRelationServerTime
{

    long serverTime = [[[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime] longValue];
    
    long savsDateTime = [[[NSUserDefaults standardUserDefaults] objectForKey:IXLocaleTime] longValue];
    long curDateTime = [[NSDate date] timeIntervalSince1970];
    
    return (curDateTime - savsDateTime + serverTime);
}


- (proto_order_add *)packageOrder
{
    _createTime = [self getCurrentTimeRelationServerTime];
    
    proto_order_add *proto = [[proto_order_add alloc] init];
    
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    
    item_order *order = [[item_order alloc] init];
    order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    order.symbol  =  _symbolModel.name;
    order.symbolid = _symbolModel.id_p;
    order.createTime = _createTime;
    order.clientType = [IXUserInfoMgr shareInstance].itemType;
    order.direction = _direction;
    order.requestVolume = [_requestVolume doubleValue];
    if ([_requestType isEqualToString:MARKETORDER]) {
        order.priceRange = 10000;
        
#warning 股票使用最新价，其它使用顶层价
        if (STOCKID == _parentId || IDXMARKETID == _marketId) {
            order.refPrice = _quoteModel.nPrice;
        }else{
            if (_direction == item_order_edirection_DirectionBuy){
                order.refPrice = [_quoteModel.BuyPrc[0] doubleValue];
            }else {
                order.refPrice = [_quoteModel.SellPrc[0] doubleValue];
            }
        }
        
        order.type = item_order_etype_TypeOpen;
        order.expireType = item_order_eexpire_ExpireDaily;
        
    } else {
        order.requestPrice =  [_requestPrice doubleValue];
        if ([_requestExpire isEqualToString:LocalizedString(@"当日")]) {
            order.expireType = item_order_eexpire_ExpireDaily;
        }else{
            order.expireType = item_order_eexpire_ExpireWeekly;
        }
        
        if ([_requestType isEqualToString:LIMITORDER]) {
            order.type = item_order_etype_TypeLimit;
        } else {
            order.type = item_order_etype_TypeStop;
        }
    }
    
    if ( !isnan([_takeprofit floatValue]) && 0 != [_takeprofit doubleValue] ) {
        order.takeProfit = [_takeprofit doubleValue];
    }
    
    if ( !isnan([_stoploss floatValue]) && 0 != [_takeprofit doubleValue] ) {
        order.stopLoss = [_stoploss doubleValue];
    }
    proto.order = order;
    return proto;
}

- (proto_order_update *)packageUpdateOrder {
    
    _createTime = [self getCurrentTimeRelationServerTime];
    
    proto_order_update *proto = [[proto_order_update alloc] init];
    
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    
    item_order *order = [[item_order alloc] init];
    order.id_p = _orderId;
    order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    order.symbol  =  _symbolModel.name;
    order.symbolid = _symbolModel.id_p;
    order.createTime = _createTime;
    order.clientType = [IXUserInfoMgr shareInstance].itemType;
    order.direction = _direction;//买入
    order.requestVolume = [_requestVolume doubleValue];
    
    if ([_requestType isEqualToString:MARKETORDER]) {
        order.priceRange = 10000;
        order.refPrice = _quoteModel.nPrice;
        order.requestPrice = order.refPrice;
        order.type = item_order_etype_TypeOpen;
        order.expireType = item_order_eexpire_ExpireDaily;
        
    } else {
        order.requestPrice =  [_requestPrice doubleValue];
        if ([_requestExpire isEqualToString:LocalizedString(@"当日")]) {
            order.expireType = item_order_eexpire_ExpireDaily;
        }else{
            order.expireType = item_order_eexpire_ExpireWeekly;
        }
        
        if ([_requestType isEqualToString:LIMITORDER]) {
            order.type = item_order_etype_TypeLimit;
        } else {
            order.type = item_order_etype_TypeStop;
        }
    }
    
    if ( !isnan([_takeprofit floatValue]) ) {
        order.takeProfit = [_takeprofit doubleValue];
    }
    
    if ( !isnan([_stoploss floatValue]) ) {
        order.stopLoss = [_stoploss doubleValue];
    }
    proto.order = order;
    return proto;
}

@end
