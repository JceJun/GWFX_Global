
//
//  IXDomOtherDealCell.m
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDomOtherDealCell.h"
#import "IXEntityFormatter.h"
#import "UIViewExt.h"

@interface IXDomOtherDealCell ()

@property (nonatomic, strong) UIImageView *buyImg;
@property (nonatomic, strong) UIImageView *sellImg;

@property (nonatomic, strong) UILabel *buyVolLbl;
@property (nonatomic, strong) UILabel *sellVolLbl;

@property (nonatomic, assign) double prePrice;
@property (nonatomic, strong) UILabel *deepPrcLbl;

@property (nonatomic, strong) CAShapeLayer *buyLayer;
@property (nonatomic, strong) CAShapeLayer *sellLayer;

@property (nonatomic, strong) UILabel *buyMarkLbl;
@property (nonatomic, strong) UILabel *sellMarkLbl;


@property (nonatomic, strong) UILabel *tBuyCnLbl;
@property (nonatomic, strong) UILabel *tSellCnLbl;

@property (nonatomic, strong) UILabel *cBuyCnLbl;
@property (nonatomic, strong) UILabel *cSellCnLbl;

@property (nonatomic, strong) UILabel *bBuyCnLbl;
@property (nonatomic, strong) UILabel *bSellCnLbl;

@end

@implementation IXDomOtherDealCell
- (void)responseBuyGes
{
    if( _order ){
        _order(item_order_edirection_DirectionBuy);
    }
}

- (void)rsponseSellGes
{
    if( _order ){
        _order(item_order_edirection_DirectionSell);
    }
}

- (void)updateStatus:(NSArray *)arr
{
    if (!arr || arr.count < 2) {
        [self updateChooseStatus:DOMSTATUSUNCHOOSE];
    }else{
        if ([arr[0] boolValue]) {
            [self updateChooseStatus:DOMSTATUSCHOOSEBUY];
        }else if ([arr[1] boolValue]){
            [self updateChooseStatus:DOMSTATUSCHOOSESELL];
        }else{
            [self updateChooseStatus:DOMSTATUSUNCHOOSE];
        }
    }
}

//默认买入
- (NSString *)getDBStr
{
    NSString *dbStr = [IXUserInfoMgr shareInstance].settingRed ? @"doom_default_buy" : @"doom_default_sell";
    return dbStr;
}

//选择买入
- (NSString *)getCBStr
{
    NSString *cbStr = [IXUserInfoMgr shareInstance].settingRed ? @"doom_choose_buy" : @"doom_choose_sell";
    return cbStr;
}

//选择买入夜间
- (NSString *)getCBDStr
{
    NSString *cbStr = [IXUserInfoMgr shareInstance].settingRed ? @"doom_choose_buy_dark" : @"doom_choose_sell_dark";
    return cbStr;
}

//默认卖出
- (NSString *)getDSStr
{
    NSString *dsStr = [IXUserInfoMgr shareInstance].settingRed ? @"doom_default_sell" : @"doom_default_buy";
    return dsStr;
}

//选择卖出
- (NSString *)getCSStr
{
    NSString *cbStr = [IXUserInfoMgr shareInstance].settingRed ? @"doom_choose_sell" : @"doom_choose_buy";
    return cbStr;
}

//选择卖出夜间
- (NSString *)getCSDStr
{
    NSString *cbStr = [IXUserInfoMgr shareInstance].settingRed ? @"doom_choose_sell_dark" : @"doom_choose_buy_dark";
    return cbStr;
}

- (void)imageUnchoose
{
    if (!_model || _model.showM.canBuy) {
        self.buyImg.dk_imagePicker = DKImageWithImgs([self imageWithName:[self getDBStr]],
                                                     [self imageWithName:@"doom_default_buy_dark"]);
    }else{
        self.buyImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
                                                     [self imageWithName:@"doom_default_buy_dark"]);
    }
    
    if (!_model || _model.showM.canSell) {
        self.sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:[self getDSStr]],
                                                      [self imageWithName:@"doom_default_buy_dark"]);
    }else{
        self.sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
                                                      [self imageWithName:@"doom_default_buy_dark"]);
    }
}


- (void)imageChooseBuy
{
    if (!_model || _model.showM.canBuy) {
        self.buyImg.dk_imagePicker =  DKImageWithImgs([self imageWithName:[self getCBStr]],
                                                      [self imageWithName:[self getCBDStr]]);
    }else{
        self.buyImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
                                                     [self imageWithName:@"doom_default_buy_dark"]);
    }

    if (!_model || _model.showM.canSell) {
        self.sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:[self getDSStr]],
                                                      [self imageWithName:@"doom_default_buy_dark"]);
    }else{
        self.sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
                                                      [self imageWithName:@"doom_default_buy_dark"]);
    }
}

- (void)imageChooseSell
{
    if (!_model || _model.showM.canBuy) {
        self.buyImg.dk_imagePicker = DKImageWithImgs([self imageWithName:[self getDBStr]],
                                                     [self imageWithName:@"doom_default_buy_dark"]);
    }else{
        self.buyImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
                                                     [self imageWithName:@"doom_default_buy_dark"]);
    }
 
    if (!_model || _model.showM.canSell) {
        self.sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:[self getCSStr]],
                                                      [self imageWithName:[self getCSDStr]]);
    }else{
        self.sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
                                                      [self imageWithName:@"doom_default_buy_dark"]);
    }
}

- (void)updateChooseStatus:(DOMSTATUS)status
{
    [self resetUI];
    
    switch ( status ) {
        case DOMSTATUSUNCHOOSE:{
            [self imageUnchoose];
        }
            break;
        case DOMSTATUSCHOOSEBUY:{
            [self imageChooseBuy];

            NSString *tipCon = (self.tag <= 3 ) ? LocalizedString(@"停损") :
            LocalizedString(@"限价");
            self.buyMarkLbl.text = tipCon;
            self.buyMarkLbl.hidden = NO;
            
            self.buyLayer.hidden = YES;
            self.buyVolLbl.hidden = YES;
        }
            break;
        default:{
            [self imageChooseSell];

            NSString *tipCon = (self.tag <= 5 ) ? LocalizedString(@"限价") :
            LocalizedString(@"停损");
            self.sellMarkLbl.text = tipCon;
            self.sellMarkLbl.hidden = NO;
            
            self.sellLayer.hidden = YES;
            self.sellVolLbl.hidden = YES;
        }
            break;
    }
}

- (void)resetUI
{
    _buyMarkLbl.hidden = YES;
    _sellMarkLbl.hidden = YES;
    
    _buyVolLbl.hidden = NO;
    _sellVolLbl.hidden = NO;
    
    _buyLayer.hidden = NO;
    _sellLayer.hidden = NO;
}

- (void)updateDeepPrc:(NSString *)deepPrc
{
    if ( [deepPrc doubleValue] == 0 || isnan([deepPrc doubleValue]) ) {
        self.deepPrcLbl.text = @"-";
        return;
    }
    
    if ( _prePrice < [deepPrc doubleValue] ) {
        self.deepPrcLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }else if( _prePrice == [deepPrc doubleValue] ){
        self.deepPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
    }else{
        self.deepPrcLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
    
    NSString *content =  [NSString formatterPrice:deepPrc WithDigits:_model.showM.symbolModel.digits];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:content];
    
    //pipsratio位往上跳
    NSInteger length = content.length;
    NSInteger pipsratio = log10(_model.showM.symbolModel.pipsRatio);
    NSInteger start = length - pipsratio;
    NSInteger stop = pipsratio;
    if ( start < 0 ) {
        start = 0;
        stop = length;
    }
    
    [str addAttribute:NSBaselineOffsetAttributeName value:@(3) range:NSMakeRange( start, stop)];
    [str addAttribute:NSFontAttributeName value:RO_REGU(14) range:NSMakeRange( start, stop)];
    
    //上跳位往前走两位字号变大
    if ( stop < length ) {
        NSRange range = [content rangeOfString:@"."];
        stop = start - 2;
        
        //如果跨小数点，则多往前走
        if ( range.location != NSNotFound && range.location > stop - 1 ) {
            stop -= 1;
        }
        
        if ( stop >= 0 ) {
            [str addAttribute:NSBaselineOffsetAttributeName value:@(0) range:NSMakeRange( 0, stop)];
            [str addAttribute:NSFontAttributeName value:RO_REGU(14) range:NSMakeRange( 0, stop)];
        }
    }
    
    self.deepPrcLbl.attributedText = str;
    
    _prePrice = [deepPrc doubleValue];
}

- (void)updateBuyVol:(NSString *)buyVol
{
    if ( [buyVol doubleValue] == 0 ) {
        buyVol = @"-";
    }else{
        if ([buyVol doubleValue] < 1000) {
            buyVol = [NSString stringWithFormat:@"%d",[buyVol integerValue]];
        }else if([buyVol doubleValue]>=1000.f&&[buyVol doubleValue]<1000000.f){
            buyVol = [NSString stringWithFormat:@"%.1fK",[buyVol doubleValue]/1000.f];
        }
        else if([buyVol doubleValue]>=1000000.f){
            buyVol = [NSString stringWithFormat:@"%.1fM",[buyVol doubleValue]/1000000.f];
        }
    }
    self.buyVolLbl.text = buyVol;
    
}

- (void)updateSellVol:(NSString *)sellVol
{
    if ( [sellVol doubleValue] == 0 ) {
        sellVol = @"-";
    }else{
        if ([sellVol doubleValue] < 1000) {
            sellVol = [NSString stringWithFormat:@"%d",[sellVol integerValue]];
        }else if([sellVol doubleValue]>=1000.f&&[sellVol doubleValue]<1000000.f){
            sellVol = [NSString stringWithFormat:@"%.1fK",[sellVol doubleValue]/1000.f];
        }
        else if([sellVol floatValue]>=1000000.f){
            sellVol = [NSString stringWithFormat:@"%.1fM",[sellVol doubleValue]/1000000.f];
        }
    }
    self.sellVolLbl.text = sellVol;
}

- (void)updateMaxVol
{
    if ( _model.maxVol != 0 ) {
        
        double buyFrameWidth = [[_model getBuyVolWithRow:self.tag] doubleValue]/_model.maxVol * 108;
        if ( buyFrameWidth > 108 ) {
            buyFrameWidth = 108;
        }else if ( buyFrameWidth < 0 ){
            buyFrameWidth = 0;
        }
        
        CGRect frame = self.buyLayer.frame;
        frame.size.width = buyFrameWidth;
        self.buyLayer.frame = frame;
        
        if ( ![IXUserInfoMgr shareInstance].isNightMode ) {
            double buyWidth = [IXEntityFormatter getContentWidth:self.buyVolLbl.text WithFont:RO_REGU(15)];
            if ( 54 + buyWidth/2 >= buyFrameWidth ) {
                self.buyVolLbl.textColor = MarketSymbolNameColor;
            }else{
                self.buyVolLbl.textColor = [UIColor whiteColor];
            }
        }else{
            self.buyVolLbl.textColor = [UIColor whiteColor];
        }
        _sellLayer.path = nil;
        
        double sellFrameWidth = [[_model getSellVolWithRow:self.tag] doubleValue]/_model.maxVol * 108;
        if ( sellFrameWidth > 108 ) {
            sellFrameWidth = 108;
        }else if( sellFrameWidth < 0 ){
            sellFrameWidth = 0;
        }
        
        frame = self.sellLayer.frame;
        frame.origin.x = 108 - sellFrameWidth;
        frame.size.width = sellFrameWidth;
        self.sellLayer.frame = frame;
        
        if ( ![IXUserInfoMgr shareInstance].isNightMode ) {
            double sellWidth = [IXEntityFormatter getContentWidth:self.sellVolLbl.text WithFont:RO_REGU(15)];
            if ( 54 - sellWidth/2 >=  108 - sellFrameWidth ) {
                self.sellVolLbl.textColor = [UIColor whiteColor];
            }else{
                self.sellVolLbl.textColor = MarketSymbolNameColor;
            }
            
        }else{
            self.sellVolLbl.textColor = [UIColor whiteColor];
        }
    }
}

- (void)setModel:(IXDomDealM *)model
{
    _model = model;

    [self updateOrderCount];
    [self updateMaxVol];
    [self updateBuyVol:[_model getBuyVolWithRow:self.tag]];
    [self updateSellVol:[_model getSellVolWithRow:self.tag]];
    [self updateDeepPrc:[_model getDeepPrcWithRow:self.tag]];
 
    [self updateColor];
}

- (void)updateColor
{
    if (!_model || _model.showM.canSell) {
//        _sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:[self getDSStr]],
//                                                  [self imageWithName:@"doom_default_buy_dark"]);
        
        _sellImg.userInteractionEnabled = YES;
    }else{
//        _sellImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
//                                                  [self imageWithName:@"doom_default_buy_dark"]);
        _sellImg.userInteractionEnabled = NO;
    }
    
    if (!_model || _model.showM.canBuy) {
//        _buyImg.dk_imagePicker = DKImageWithImgs([self imageWithName:[self getDBStr]],
//                                                 [self imageWithName:@"doom_default_buy_dark"]);
        
        _buyImg.userInteractionEnabled = YES;
        
    }else{
//        _buyImg.dk_imagePicker = DKImageWithImgs([self imageWithName:@"doom_unTrade"],
//                                                 [self imageWithName:@"doom_default_buy_dark"]);
        
        _buyImg.userInteractionEnabled = NO;
    }
}

- (void)updateOrderCount
{
    if ( !self.tag ) {
        _tBuyCnLbl.hidden = NO;
        _tSellCnLbl.hidden = NO;
    }else{
        _tBuyCnLbl.hidden = YES;
        _tSellCnLbl.hidden = YES;
    }
    
    if ( self.model.prcRelPosBuyArr.count > self.tag ) {
        NSArray *buyArr = self.model.prcRelPosBuyArr[self.tag];
        
        self.tBuyCnLbl.text   = [buyArr[0] stringValue];
        self.tBuyCnLbl.hidden = ![buyArr[0] boolValue];
        
        self.cBuyCnLbl.text   =  [buyArr[1] stringValue];
        self.cBuyCnLbl.hidden = ![buyArr[1] boolValue];
        
        self.bBuyCnLbl.text   =  [buyArr[2] stringValue];
        self.bBuyCnLbl.hidden = ![buyArr[2] boolValue];
    }
    
    if ( self.model.prcRelPosSellArr.count > self.tag ) {
        NSArray *sellArr = self.model.prcRelPosSellArr[self.tag];
        self.tSellCnLbl.text   =  [sellArr[0] stringValue];
        self.tSellCnLbl.hidden = ![sellArr[0] boolValue];
        
        self.cSellCnLbl.text   =  [sellArr[1] stringValue];
        self.cSellCnLbl.hidden = ![sellArr[1] boolValue];
        
        self.bSellCnLbl.text   =  [sellArr[2] stringValue];
        self.bSellCnLbl.hidden = ![sellArr[2] boolValue];
    }
}


- (UIImage *)imageWithName:(NSString *)name
{
    return [[UIImage imageNamed:name] resizableImageWithCapInsets:UIEdgeInsetsMake( 5, 20, 5, 20)];
}

- (UILabel *)tBuyCnLbl
{
    if ( !_tBuyCnLbl ) {
        _tBuyCnLbl = [IXUtils createLblWithFrame:CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 10, 12, 12)
                                        WithFont:PF_REGU(10)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0xffffff
                                      dTextColor:0x22262e];
        [self.contentView addSubview:_tBuyCnLbl];
        _tBuyCnLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        [_tBuyCnLbl addCornerRadius:6 viewSize:CGSizeMake(12, 12)];
     }
    if (0 == self.tag) {
        _tBuyCnLbl.frame = CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 10, 12, 12);
    }else{
        _tBuyCnLbl.frame = CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 0, 12, 12);
    }
    return _tBuyCnLbl;
}

- (UILabel *)tSellCnLbl
{
    if ( !_tSellCnLbl ) {
        _tSellCnLbl = [IXUtils createLblWithFrame:CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 10, 12, 12)
                                         WithFont:PF_REGU(10)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0xffffff
                                       dTextColor:0x22262e];
        [self.contentView addSubview:_tSellCnLbl];
        _tSellCnLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        [_tSellCnLbl addCornerRadius:6 viewSize:CGSizeMake(12, 12)];
     }
    if (0 == self.tag) {
        _tSellCnLbl.frame = CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 10, 12, 12);
    }else{
        _tSellCnLbl.frame = CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 0, 12, 12);
    }
    return _tSellCnLbl;
}


- (UILabel *)cBuyCnLbl
{
    if ( !_cBuyCnLbl ) {
        _cBuyCnLbl = [IXUtils createLblWithFrame:CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 14, 12, 12)
                                        WithFont:PF_REGU(10)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0xffffff
                                      dTextColor:0x22262e];
        [self.contentView addSubview:_cBuyCnLbl];
        _cBuyCnLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        [_cBuyCnLbl addCornerRadius:6 viewSize:CGSizeMake(12, 12)];
    }
    CGRect frame = CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 14, 12, 12);
    if(!self.tag){
        frame = CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 25, 12, 12);
    }
    _cBuyCnLbl.frame = frame;
    
    return _cBuyCnLbl;
}

- (UILabel *)cSellCnLbl
{
    if ( !_cSellCnLbl ) {
        _cSellCnLbl = [IXUtils createLblWithFrame:CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 14, 12, 12)
                                         WithFont:PF_REGU(10)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0xffffff
                                       dTextColor:0x22262e];
        [self.contentView addSubview:_cSellCnLbl];
        _cSellCnLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
        [_cSellCnLbl addCornerRadius:6 viewSize:CGSizeMake(12, 12)];
    }
    CGRect frame = CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 14, 12, 12);
    if(!self.tag){
        frame = CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 25, 12, 12);
    }
    _cSellCnLbl.frame = frame;
    
    return _cSellCnLbl;
}

- (UILabel *)bBuyCnLbl
{
    if ( !_bBuyCnLbl ) {
        _bBuyCnLbl = [IXUtils createLblWithFrame:CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 33, 12, 12)
                                        WithFont:PF_REGU(10)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0xffffff
                                      dTextColor:0x22262e];
        [self.contentView addSubview:_bBuyCnLbl];
        _bBuyCnLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
        [_bBuyCnLbl addCornerRadius:6 viewSize:CGSizeMake(12, 12)];
    }
    CGRect frame = CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 32, 12, 12);
    if(0==self.tag){
        frame = CGRectMake( CGRectGetMinX(self.buyImg.frame) - 30, 43, 12, 12);
    }
    _bBuyCnLbl.frame = frame;
    return _bBuyCnLbl;
}

- (UILabel *)bSellCnLbl
{
    if ( !_bSellCnLbl ) {
        _bSellCnLbl = [IXUtils createLblWithFrame:CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 33, 12, 12)
                                         WithFont:PF_REGU(10)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0xffffff
                                       dTextColor:0x22262e];
        [self.contentView addSubview:_bSellCnLbl];
        [_bSellCnLbl addCornerRadius:6 viewSize:CGSizeMake(12, 12)];
        _bSellCnLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    CGRect frame = CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 32, 12, 12);
    if(0==self.tag){
        frame = CGRectMake( CGRectGetMaxX(self.sellImg.frame) + 10, 43, 12, 12);
    }
    _bSellCnLbl.frame = frame;
    return _bSellCnLbl;
}


- (UILabel *)buyMarkLbl
{
    if ( !_buyMarkLbl ) {
        _buyMarkLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth - 123, 12, 108, 15)
                                         WithFont:PF_REGU(15)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0x11b873
                                       dTextColor:0x21ce99];
        [self.contentView addSubview:_buyMarkLbl];
    }
    CGRect frame = CGRectMake( kScreenWidth - 123, 12, 108, 15);
    if(0==self.tag){
        frame = CGRectMake( kScreenWidth - 123, 22, 108, 15);
    }
    _buyMarkLbl.frame = frame;
    return _buyMarkLbl;
}

- (UILabel *)sellMarkLbl
{
    if ( !_sellMarkLbl ) {
        _sellMarkLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 12, 108, 15)
                                          WithFont:PF_REGU(15)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0xff4653
                                        dTextColor:0xff4d2d];
        [self.contentView addSubview:_sellMarkLbl];
    }
    CGRect frame = CGRectMake( 15, 12, 108, 15);
    if(0==self.tag){
        frame = CGRectMake( 15, 22, 108, 15);
    }
    _sellMarkLbl.frame = frame;
    return _sellMarkLbl;
}


- (CAShapeLayer *)buyLayer
{
    if ( !_buyLayer ) {
        _buyLayer = [CAShapeLayer new];
        _buyLayer.frame = CGRectMake( 0, 0, 0, 28);
       
        if (!_model || _model.showM.canBuy) {
            if ([IXUserInfoMgr shareInstance].isNightMode) {
                if ([IXUserInfoMgr shareInstance].settingRed) {
                    _buyLayer.backgroundColor = UIColorHexFromRGB(0x21ce99).CGColor;
                }else{
                    _buyLayer.backgroundColor = UIColorHexFromRGB(0xff4d2d).CGColor;
                }
            }else{
                if ([IXUserInfoMgr shareInstance].settingRed) {
                    _buyLayer.backgroundColor = UIColorHexFromRGB(0x11b873).CGColor;
                }else{
                    _buyLayer.backgroundColor = UIColorHexFromRGB(0xff4653).CGColor;
                }
            }
        }else{
            if ([IXUserInfoMgr shareInstance].isNightMode) {
                _buyLayer.backgroundColor = UIColorHexFromRGB(0x4c6072).CGColor;
            }else{
                _buyLayer.backgroundColor = UIColorHexFromRGB(0x99abba).CGColor;
            }
        }
        
        [self.buyImg.layer addSublayer:_buyLayer];
    }
    return _buyLayer;
}

- (CAShapeLayer *)sellLayer
{
    if ( !_sellLayer ) {
        _sellLayer = [CAShapeLayer new];
        _sellLayer.frame = CGRectMake( 108, 0, 0, 28);
      
        if (!_model || _model.showM.canSell) {
            if ([IXUserInfoMgr shareInstance].isNightMode) {
                if ([IXUserInfoMgr shareInstance].settingRed) {
                    _sellLayer.backgroundColor = UIColorHexFromRGB(0xff4d2d).CGColor;
                }else{
                    _sellLayer.backgroundColor = UIColorHexFromRGB(0x21ce99).CGColor;
                }
            }else{
                if ([IXUserInfoMgr shareInstance].settingRed) {
                    _sellLayer.backgroundColor = UIColorHexFromRGB(0xff4653).CGColor;
                }else{
                    _sellLayer.backgroundColor = UIColorHexFromRGB(0x11b873).CGColor;
                }
            }
        }else{
            if ([IXUserInfoMgr shareInstance].isNightMode) {
                _sellLayer.backgroundColor = UIColorHexFromRGB(0x4c6072).CGColor;
            }else{
                _sellLayer.backgroundColor = UIColorHexFromRGB(0x99abba).CGColor;
            }
        }
        [self.sellImg.layer addSublayer:_sellLayer];
    }
    return _sellLayer;
}

- (UILabel *)deepPrcLbl
{
    if ( !_deepPrcLbl ) {
        _deepPrcLbl = [IXUtils createLblWithFrame:CGRectMake( 123, 10, kScreenWidth - 246, 18)
                                         WithFont:RO_REGU(18)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0x4c6072
                                       dTextColor:0x8395a4];
        [self.contentView addSubview:_deepPrcLbl];
    }
    if (self.tag) {
        _deepPrcLbl.frame = CGRectMake( 123, 10, kScreenWidth - 246, 18);
    }else{
        _deepPrcLbl.frame = CGRectMake( 123, 20, kScreenWidth - 246, 18);
    }
    return _deepPrcLbl;
}

- (UILabel *)buyVolLbl
{
    if ( !_buyVolLbl ) {
        _buyVolLbl = [IXUtils createLblWithFrame:CGRectMake(kScreenWidth - 123, 12, 108, 15)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0x4c6072
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_buyVolLbl];
    }
    if (self.tag) {
        _buyVolLbl.frame = CGRectMake(kScreenWidth - 123, 12, 108, 15);
    }else{
        _buyVolLbl.frame = CGRectMake(kScreenWidth - 123, 22, 108, 15);
    }
    return _buyVolLbl;
}

- (UIImageView *)buyImg
{
    if ( !_buyImg ) {
        _buyImg = [[UIImageView alloc] initWithFrame:CGRectMake( kScreenWidth - 123, 5, 108, 28)];
        [self.contentView addSubview:_buyImg];
        
        _buyImg.layer.masksToBounds = YES;
        _buyImg.layer.cornerRadius = 2;
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(responseBuyGes)];
        [_buyImg addGestureRecognizer:tapGes];
    }
    
    if (self.tag) {
        _buyImg.frame = CGRectMake( kScreenWidth - 123, 5, 108, 28);
    }else{
        _buyImg.frame = CGRectMake( kScreenWidth - 123, 15, 108, 28);
    }
    return _buyImg;
}

- (UILabel *)sellVolLbl
{
    if ( !_sellVolLbl ) {
        _sellVolLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 12, 108, 15)
                                         WithFont:RO_REGU(15)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0x4c6072
                                       dTextColor:0x8395a4];
        [self.contentView addSubview:_sellVolLbl];
    }
    if (self.tag) {
        _sellVolLbl.frame = CGRectMake( 15, 12, 108, 15);
    }else{
        _sellVolLbl.frame = CGRectMake( 15, 22, 108, 15);
    }
    return _sellVolLbl;
}


- (UIImageView *)sellImg
{
    if ( !_sellImg ) {
        _sellImg = [[UIImageView alloc] initWithFrame:CGRectMake( 15, 5, 108, 28)];
        [self.contentView addSubview:_sellImg];
        _sellImg.layer.masksToBounds = YES;
        _sellImg.layer.cornerRadius = 2;
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(rsponseSellGes)];
        [_sellImg addGestureRecognizer:tapGes];
    }

    if (self.tag) {
        _sellImg.frame = CGRectMake( 15, 5, 108, 28);
    }else{
        _sellImg.frame = CGRectMake( 15, 15, 108, 28);
    }
    return _sellImg;
}
@end
