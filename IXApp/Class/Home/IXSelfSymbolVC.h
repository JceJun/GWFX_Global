//
//  IXSelfSymbolVC.h
//  IXApp
//
//  Created by Bob on 2016/12/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IXQuoteDetailVC.h"

@interface IXSelfSymbolVC : UIViewController

@property (nonatomic, strong) UIViewController *parentVC;

- (void)subscribeDynamicPrice;

- (void)reloadTableView;

@end
