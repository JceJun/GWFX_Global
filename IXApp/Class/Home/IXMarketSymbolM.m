//
//  IXMarketSymbolM.m
//  IXApp
//
//  Created by Bob on 2016/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXMarketSymbolM.h"
#import "IXDBSymbolCataMgr.h"
#import "IXTCPRequest_net.h"
#import "IXBORequestMgr+BroadSide.h"

#import "IXUserInfoMgr.h"
#import "IXTradeDataCache.h"
#import "IxProtoGroupSymbol.pbobjc.h"

@interface IXMarketSymbolM ()

/**
 选中的分类Id
 */
@property (nonatomic, assign) NSInteger cataId;

@property (nonatomic, assign) BOOL needResNoti;
@end

@implementation IXMarketSymbolM

#pragma mark --
#pragma mark life cycle
- (id)init
{
    self = [super init];
    if (self) {
        _needResNoti = YES;
        IXTradeData_listen_regist(self, PB_CMD_POSITION_ADD);
        
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_ADD);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_DELETE);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_LIST);

        
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_ADD);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_DELETE);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_SUB_LIST);
        
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CHANGE);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_HOT_LIST);
        
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_LIST);
        
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_CATA_CHANGE);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_CATA_LIST);
        
        IXTradeData_listen_regist(self, PB_CMD_USERLOGIN_INFO);
        IXTradeData_listen_regist(self, PB_CMD_USER_LOGIN_DATA_TOTAL);
        

        IXNetStatus_listen_regist(self, IXTradeStatusKey);
        IXNetStatus_listen_regist(self, IXQuoteStatusKey);
        
        [[IXBORequestMgr shareInstance] addObserver:self
                                         forKeyPath:@"headUrl"
                                            options:NSKeyValueObservingOptionNew
                                            context:NULL];
 
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(unsubDetailQuote:)
                                                     name:kNotifyCancelDetailQuote
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_POSITION_ADD);
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_DELETE);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_SUB_LIST);
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_CHANGE);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_HOT_LIST);
    
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_LIST);
    
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_CATA_CHANGE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_CATA_LIST);
    
    IXTradeData_listen_resign(self, PB_CMD_USERLOGIN_INFO);
    IXTradeData_listen_resign(self, PB_CMD_USER_LOGIN_DATA_TOTAL);
    
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_ADD);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_DELETE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_LIST);

    IXNetStatus_listen_resign(self, IXTradeStatusKey);
    IXNetStatus_listen_resign(self, IXQuoteStatusKey);
    
    [[IXBORequestMgr shareInstance] removeObserver:self forKeyPath:@"headUrl"];
    [[IXBORequestMgr shareInstance] removeObserver:self forKeyPath:kNotifyCancelDetailQuote];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -
#pragma mark - IXTradeDataKVOs
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath, PB_CMD_ACCOUNT_GROUP_CHANGE)) {
        if (self.updateAccount) {
            self.updateAccount();
        }
    }
    
    if (IXTradeData_isSameKey(keyPath, PB_CMD_USERLOGIN_INFO)) {
        _needResNoti = NO;
    }else if(IXTradeData_isSameKey(keyPath, PB_CMD_USER_LOGIN_DATA_TOTAL)){
        _needResNoti = YES;
        
        //登录过程固定一次刷新，避免数据更新了，UI对应的数据没更新
        [self refreashCataArr];
        if (self.updateCata) {
            self.updateCata();
        }
        return;
    }
    
    /*
     *   登录过程中，指令是发顺序下发的；而PB_CMD_USER_LOGIN_DATA_TOTAL
     *   是登录过程中的最后一个指令，所以完全可以根据PB_CMD_USER_LOGIN_DATA_TOTAL刷新UI
     */
    if (!_needResNoti) {
        return;
    }
    
    if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_ADD) ||
        IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_DELETE)||
        IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_UPDATE)) {
        
        uint64 acgId = 0;
        if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_ADD)) {
            proto_group_symbol_add *pb = ((IXTradeDataCache *)object).pb_group_symbol_add;
            acgId = pb.accountGroupid;
        }else if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_DELETE)){
            proto_group_symbol_delete *pb = ((IXTradeDataCache *)object).pb_group_symbol_delete;
            acgId = pb.accountGroupid;
        }else if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_UPDATE)){
            proto_group_symbol_update *pb = ((IXTradeDataCache *)object).pb_group_symbol_update;
            acgId = pb.accountGroupid;
        }
        
        if (!acgId || acgId == [IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid) {
            [self refreashCataArr];
            if (self.updateCata) {
                self.updateCata();
            }
        }
        
    }else if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_CATA_CHANGE) ||
        IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_CATA_LIST)||
        IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_CHANGE)||
        IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_LIST)) {
        
        [self refreashCataArr];
        if(IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_CATA_CHANGE) ||
           IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_CATA_LIST)){
            if (self.updateCata) {
                self.updateCata();
            }
        }else{
            if (self.updateSym) {
                self.updateSym();
            }
        }
    }else if (IXTradeData_isSameKey( keyPath, PB_CMD_POSITION_ADD) ){
        proto_position_add *pb = ((IXTradeDataCache *)object).pb_position_add;
        if (self.updatePosTip) {
            self.updatePosTip(pb);
        }
    }else if (IXNetStatus_isSameKey(keyPath, IXTradeStatusKey)) {
        if (self.updateNetStatus) {
            self.updateNetStatus(((IXTCPRequest *)object).tradeStatus);
        }
    }else if(IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_HOT_LIST)){
        if (self.tableType == TABLE_HOTSYMBOL) {
            if (self.updateHotSym && self.tableType == TABLE_HOTSYMBOL) {
                self.updateHotSym();
            }
        }
    }else if(IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_SUB_LIST)||
             IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_SUB_ADD)||
             IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_SUB_DELETE)){
        if (self.updateSubSym && self.tableType == TABLE_SUBSYMBOL) {
            if (self.updateSubSym) {
                self.updateSubSym();
            }
        }
    }else if (IXTradeData_isSameKey(keyPath, PB_CMD_ACCOUNT_CHANGE)){
        if (self.updateAccount) {
            self.updateAccount();
        }
    }else if (SameString(keyPath, @"headUrl")) {
        if (self.updateUserInfo) {
            self.updateUserInfo();
        }
    }
}

- (void)unsubDetailQuote:(NSNotification *)notify
{
    id obj = notify.object;
    [[IXTCPRequest shareInstance] unSubscribeDetailPriceWithSymbolAry:obj];
}

#pragma mark --
#pragma mark logic method
//刷新分类
- (void)refreashCataArr
{
    _cataArr = [NSMutableArray arrayWithObjects:@{@"name":LocalizedString(@"我的自选")},
                @{@"name":LocalizedString(@"热门")}, nil];
    
    id cataArr = [IXDataProcessTools querySymbolCataByParentId:0];
    if ( cataArr && [cataArr isKindOfClass:[NSArray class]] ) {
        [_cataArr addObjectsFromArray:cataArr];
    }
}

//自选
- (void)setSubSymbol
{
    _tableType = TABLE_SUBSYMBOL;
}

//热门
- (void)setHotSymbol
{
    _tableType = TABLE_HOTSYMBOL;
}

#pragma mark --
#pragma mark lazy load
- (NSMutableArray *)cataArr
{
    if (!_cataArr) {
        [self refreashCataArr];
    }
    return  _cataArr;
}

@end

