//
//  IXOpenResultVC.h
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXOpenModel.h"
#import "IXTradeMarketModel.h"

typedef void(^popToParentVC)();

@interface IXOpenResultVC : IXDataBaseVC

@property (nonatomic,strong) IXTradeMarketModel *model;
@property (nonatomic, strong) IXOpenModel *openModel;
@property (nonatomic, assign) double marginPrice;
@property (nonatomic, strong) popToParentVC dismissCurrentVC;

@end
