//
//  IXOpenMetricV.h
//  IXApp
//
//  Created by Bob on 2016/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXOpenMetricV : UIView

- (id)initWithFrame:(CGRect)frame
        WithImgName:(NSString *)imageName;

- (void)setStep:(NSString *)step;

@end
