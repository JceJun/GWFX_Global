//
//  IXMarketHomeVC.h
//  IXApp
//
//  Created by bob on 16/11/21.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXMarketSymbolM.h"

@interface IXMarketHomeVC : IXDataBaseVC

//记住选择的分类
- (void)remChooseRow;

@end
