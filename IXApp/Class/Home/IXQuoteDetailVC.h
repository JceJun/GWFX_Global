//
//  IXQuoteDetailVC.h
//  IXApp
//
//  Created by bob on 16/11/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXQuoteDetailM.h"

@interface IXQuoteDetailVC : UIViewController

@property (nonatomic, assign) BOOL refreashFeeQuote;

@property (nonatomic, readonly) NSInteger   cataId;

@property (nonatomic, strong) UIViewController *parentVC;

- (id)initWithSymbolCataId:(NSInteger)cataId
              WithMarketId:(NSInteger)marketId;


- (void)cancelVisualQuote;

//取消订阅
- (void)canceQuotePrice;

//订阅行情
- (void)subscribeDynamicPrice;


@end
