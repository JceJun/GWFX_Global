//
//  IXQuoteLable.h
//  IXTest
//
//  Created by Evn on 16/12/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXSymbolM.h"

typedef NS_ENUM(NSInteger, QuoteLabelAlign)
{
    Quote_Left,
    Quote_Center,
    Quote_Right,
};

@interface IXQuoteLable : UIView

/**初始化视图*/
- (id)initWithFrame:(CGRect)frame
          WithAlign:(QuoteLabelAlign)align;

- (void)setEqualFont;

/**刷新*/
-(void)refreshQuoteLabelWithSymbolModel:(IXSymbolM *)model
                               andPrice:(CGFloat)price
                          andQuoteColor:(DKColorPicker)quoteColor;

/**得到label动态宽度*/
-(float)getLabelWidth;

/** 设置价格 */
- (void)setFPrice:(float)fPrice symbolModel:(IXSymbolM *)model;

@property (nonatomic, assign) double price;

@end
