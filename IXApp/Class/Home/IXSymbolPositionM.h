//
//  IXSymbolPositionM.h
//  IXApp
//
//  Created by Bob on 2017/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXSymbolPositionM : NSObject

@property (nonatomic, assign) uint64 positionId;

@property (nonatomic, copy) NSString *posDirection;

//持仓数量
@property (nonatomic, copy) NSString *posNum;

//显示数量/手数
@property (nonatomic, copy) NSString *showNum;

@property (nonatomic, copy) NSString *posPrice;

@property (nonatomic, copy) NSString *posProfit;

@end
