//
//  IXOpenCalM.m
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenCalM.h"
#import "IXUserDefaultM.h"

#import "IXTradeMarketModel.h"
#import "IXAccountBalanceModel.h"
#import "IXOpenModel+CheckData.h"
#import "IXComCalM.h"

#define BASELBLTAG 100
#define PROFITLBLTAG 200
#define POSITIONLBLTAG 300
#define MARGINLBLTAG 400

@implementation IXOpenCalM


#pragma mark view logic
+ (void)showTip:(NSString *)checkOrderInfo WithView:(IXOpenTipV *)view InView:(UIView *)rootView
{
    view.tipInfo = checkOrderInfo;
    if ( !view.isShow ) {
        view.isShow = YES;
        [rootView addSubview:view];
    }
}

+ (void)removeTip:(IXOpenTipV *)view
{
    if ( view ) {
        view.isShow = NO;
        [view removeFromSuperview];
    }
}

+ (void)updateTipInfoWithView:(IXOpenTipV *)view WithRoot:(IXOpenRootVC *)root
{
    [IXOpenCalM removeTip:view];
    NSString *checkOrderInfo = [IXOpenCalM checkResult:root];
    if ( checkOrderInfo.length != 0 ) {
        [IXOpenCalM showTip:[IXOpenCalM checkResult:root] WithView:view InView:root.view];
    }
}

+ (NSString *)checkResult:(IXOpenRootVC *)root
{
    if ( ![root.tradeModel.requestType isEqualToString:MARKETORDER] &&
        ![root.openModel validPrice:root.tradeModel.requestPrice]) {
        return LocalizedString(@"你输入的价格不在范围内，请重新输入");
    }else if (![root.openModel validVolume:root.tradeModel.requestVolume]) {
        if ( ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount ) ) {
            return LocalizedString(@"你输入的数量不在范围内，请重新输入");
        }
        return LocalizedString(@"你输入的手数不在范围内，请重新输入");
    }else if ( ![root.openModel validProfit:root.tradeModel.takeprofit]) {
        return LocalizedString(@"你输入的止盈不在范围内，请重新输入");
    }else if ( ![root.openModel validLoss:root.tradeModel.stoploss]) {
        return LocalizedString(@"你输入的止损不在范围内，请重新输入");
    }
    return @"";
}

//刷新保证金和佣金,百万分之一
//行情推回来的时候，是主线程
+ (NSArray *)resetMarginAssetWithVolume:(double)volume WithRoot:(IXOpenRootVC *)root
{
    NSString *symbolKey = [NSString stringWithFormat:@"%llu",root.tradeModel.symbolModel.id_p];
    NSInteger direction = 1;
    id subDic = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symbolKey];
    if ( subDic && [subDic isKindOfClass:[NSNumber class]] ) {
        direction = [subDic integerValue];
    }else if( subDic && [subDic isKindOfClass:[NSDictionary class]] ){
        NSDictionary *dic = [(NSDictionary *)subDic objectForKey:POSITIONMODEL];
        direction = [dic integerForKey:SWAPDIRECRION];
    }
    
    double swap =  [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                   WithOffsetPrice:-1];
    
    double marginSwap = [[IXAccountBalanceModel shareInstance] caculateMarginSwapWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                                WithDirection:direction
                                                                              WithOffsetPrice:-1];
#if DEBUG
    [IXOpenCalM showRefreashPrc:root];
#endif
    
    if ( swap != 0 ) {
        //没有查找过才查询
        if ( root.openModel.commissionRate == 0 ) {
            root.openModel.commissionRate =  [IXRateCaculate queryComissionWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                       WithSymbolCataId:root.tradeModel.symbolModel.cataId];
            root.openModel.cataId = root.tradeModel.symbolModel.cataId;
        }
        
        double marginSet = [[IXAccountBalanceModel shareInstance] marginCurrentRateWithVolume:volume
                                                                                   WithSymbol:root.tradeModel.symbolModel];
        double margin = 0;
        double commission = 0;
        
        double marginPrice;
        double commissionPrice;
        
        if ( root.tradeModel.direction == item_order_edirection_DirectionBuy ) {
            if (root.tradeModel.quoteModel.BuyPrc.count > 0) {
                marginPrice = [root.tradeModel.quoteModel.BuyPrc[0] doubleValue];
                commissionPrice = [root.tradeModel.quoteModel.BuyPrc[0] doubleValue];
            } else {
                return nil;
            }
            
        }else{
            if (root.tradeModel.quoteModel.SellPrc.count > 0) {
                marginPrice = [root.tradeModel.quoteModel.SellPrc[0] doubleValue];
                commissionPrice = [root.tradeModel.quoteModel.SellPrc[0] doubleValue];
            } else {
                return nil;
            }
        }
        
        //如果为股票，保证金需要使用最新价
        if (STOCKID == root.openModel.parentId ||
            IDXMARKETID == root.openModel.marketId) {
            marginPrice = root.tradeModel.quoteModel.nPrice;
        }
        
        //计算保证金
        if(root.tradeModel.symbolModel.cataId == FXCATAID){
            margin = marginSwap * marginSet;
        }else{
            margin = swap * marginPrice * marginSet;
        }
        NSDecimalNumber *decMargin = [NSDecimalNumber decimalNumberWithString:
                                      [NSString stringWithFormat:@"%lf",margin]];
        decMargin = [decMargin decimalNumberByRoundingAccordingToBehavior:
                     [IXOpenCalM handlerPriceWithDigit:2]];
        
        root.tradeModel.margin = margin;
        
        //计算佣金，佣金可能为负；如果是负对用户来说是赚，需要重新计算汇率
        if( root.openModel.commissionRate < 0 ){
            swap = [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                   WithOffsetPrice:0];
        }
        
 
        if([IXAccountBalanceModel shareInstance].commissionType){
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:volume
                                   conSize:root.openModel.symbolModel.contractSizeNew];
        }else{
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:volume
                                      swap:swap
                                     price:commissionPrice];
        }
        NSDecimalNumber *decCommission = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",commission]];
        decCommission = [decCommission decimalNumberByRoundingAccordingToBehavior:
                         [IXOpenCalM handlerPriceWithDigit:2]];

        root.tradeModel.commission = commission;
        return @[decMargin,decCommission];

    }
    return nil;
}

+ (UILabel *)getbaseLbl:(IXOpenRootVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:BASELBLTAG];
    return lbl;
}

+ (UILabel *)getProfitLbl:(IXOpenRootVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:PROFITLBLTAG];
    return lbl;
}

+ (UILabel *)getPositionLbl:(IXOpenRootVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:POSITIONLBLTAG];
    return lbl;
}

+ (UILabel *)getMarginLbl:(IXOpenRootVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:MARGINLBLTAG];
    return lbl;
}

+ (NSInteger)getParentId:(NSInteger)symId
{
    NSInteger parentId = [IXDataProcessTools queryParentIdBySymbolId:symId];
    return parentId;
}

//更新提示
+ (void)showRefreashPrc:(IXOpenRootVC *)root
{
    NSString *symId = [NSString stringWithFormat:@"%llu",root.tradeModel.symbolModel.id_p];
    id value = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symId];
    if ( value && [value isKindOfClass:[NSDictionary class]] ) {
        for ( NSString *key in [value allKeys] ) {
            
            NSDictionary *curDic = [(NSDictionary *)value objectForKey:key];
            
            if ( [curDic objectForKey:SYMBOLID] ) {
                
                if ( [key isEqualToString:BASEMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                               [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    [IXOpenCalM getbaseLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                         sModel.name,
                                                         quote.SellPrc[0],
                                                         quote.BuyPrc[0]];
                    
                }else if ( [key isEqualToString:PROFITMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    [IXOpenCalM getProfitLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                           sModel.name,
                                                           quote.SellPrc[0],
                                                           quote.BuyPrc[0]];
                    
                }else if ( [key isEqualToString:POSITIONMODEL] ) {
                    if(STOCKID == root.openModel.parentId ||
                       IDXMARKETID == root.openModel.marketId){
                        [IXOpenCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ lastPrice %lf",
                                                               root.tradeModel.symbolModel.name,
                                                               root.tradeModel.quoteModel.nPrice];
                    }else if ( root.tradeModel.quoteModel.BuyPrc.count > 0 && root.tradeModel.quoteModel.SellPrc.count > 0 ) {
                        [IXOpenCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                       root.tradeModel.symbolModel.name,
                                       root.tradeModel.quoteModel.SellPrc[0],
                                       root.tradeModel.quoteModel.BuyPrc[0]];
                    }
                    
                }else if ( [key isEqualToString:MAGNSYMMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    [IXOpenCalM getMarginLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                           sModel.name,
                                                           quote.SellPrc[0],
                                                           quote.BuyPrc[0]];
                }
            }
        }
    }else{
        if ( root.tradeModel.quoteModel.BuyPrc.count > 0 && root.tradeModel.quoteModel.SellPrc.count > 0 ) {
            
            if(STOCKID == root.openModel.parentId ||
               IDXMARKETID == root.openModel.marketId){
                [IXOpenCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ lastPrice %lf",
                                                         root.tradeModel.symbolModel.name,
                                                         root.tradeModel.quoteModel.nPrice];
            }else if ( root.tradeModel.quoteModel.BuyPrc.count > 0 && root.tradeModel.quoteModel.SellPrc.count > 0 ) {
                [IXOpenCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                         root.tradeModel.symbolModel.name,
                                                         root.tradeModel.quoteModel.SellPrc[0],
                                                         root.tradeModel.quoteModel.BuyPrc[0]];
            }
            
        }
    }
}


//添加提示
+ (void)addPrcTipInfo:(IXOpenRootVC *)root
{
    if ( ![IXOpenCalM getPositionLbl:root] ){
        UILabel *posLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 0, kScreenWidth, 10)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentLeft
                                           wTextColor:0x4c6072
                                           dTextColor:0x4c6072];
        posLbl.tag = POSITIONLBLTAG;
        [root.navigationController.navigationBar addSubview:posLbl];
    }
    
    if ( ![IXOpenCalM getProfitLbl:root] ) {
        UILabel *profitLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 11, kScreenWidth, 10)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x4c6072
                                              dTextColor:0x4c6072];
        profitLbl.tag = PROFITLBLTAG;
        [root.navigationController.navigationBar addSubview:profitLbl];
    }
    
    if (![IXOpenCalM getbaseLbl:root] ) {
        UILabel *baseLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 22, kScreenWidth, 10)
                                              WithFont:PF_MEDI(10)
                                             WithAlign:NSTextAlignmentLeft
                                            wTextColor:0x4c6072
                                            dTextColor:0x4c6072];
        baseLbl.tag = BASELBLTAG;
        [root.navigationController.navigationBar addSubview:baseLbl];
    }
    
    if (![IXOpenCalM getMarginLbl:root]) {
        UILabel *marginLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 33, kScreenWidth, 10)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x4c6072
                                              dTextColor:0x4c6072];
        marginLbl.tag = MARGINLBLTAG;
        [root.navigationController.navigationBar addSubview:marginLbl];
    }
}

+ (void)removePrcTipInfo:(IXOpenRootVC *)root
{
    UILabel *pos = [IXOpenCalM getPositionLbl:root];
    if (pos) {
        [pos removeFromSuperview];
    }
    
    UILabel *base = [IXOpenCalM getbaseLbl:root];
    if (base) {
        [base removeFromSuperview];
    }
    
    UILabel *profit = [IXOpenCalM getProfitLbl:root];
    if (profit) {
        [profit removeFromSuperview];
    }
    
    UILabel *margin = [IXOpenCalM getMarginLbl:root];
    if (margin) {
        [margin removeFromSuperview];
    }
}

+ (NSDecimalNumberHandler *)handlerPriceWithDigit:(NSInteger)digit
{
    NSDecimalNumberHandler *roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:digit
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
    
    return roundingBehavior;
}
@end
