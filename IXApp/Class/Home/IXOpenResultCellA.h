//
//  IXOpenResultCellA.h
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXOpenResultCellA : UITableViewCell

@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *labelU;
@property (nonatomic, strong) UILabel *labelD;

@end
