//
//  IXOpenChoiceV.h
//  IXApp
//
//  Created by Bob on 2016/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^tapOpenChoice)();

typedef NS_ENUM(NSInteger, TapChoiceAlign) {
    TapChoiceAlignDefault,
    TapChoiceAlignLeft,
    TapChoiceAlignCenter,
    TapChoiceAlignRight,
    TapChoiceAlignRightOutImg,
};

@interface IXOpenChoiceV : UIView

@property (nonatomic, strong) UIImageView *arrowImg;


- (id)initWithFrame:(CGRect)frame
          WithTitle:(NSString *)title
          WithAlign:(TapChoiceAlign)align
            WithTap:(tapOpenChoice)choose;

- (void)resetTitle:(NSString *)title;

- (void)hightedState:(BOOL)state;

- (void)resetArrow;

@end
