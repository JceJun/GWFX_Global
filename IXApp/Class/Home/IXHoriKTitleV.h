//
//  IXHoriKTitleV.h
//  IXApp
//
//  Created by Magee on 2017/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IXTradeMarketModel;
@interface IXHoriKTitleV : UIView

@property (nonatomic,copy)void(^shutBtnClicked)();
@property (nonatomic,assign)double lastPrice;

- (void)refreshUIWithTrade:(IXTradeMarketModel *)tradeModel;
- (void)syncDynamicWithQuote:(IXQuoteM *)quote;
- (void)syncFirstPriceWithArray:(NSMutableArray *)array;
@end
