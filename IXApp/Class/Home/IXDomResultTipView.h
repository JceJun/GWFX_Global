//
//  IXDomResultTipView.h
//  IXApp
//
//  Created by Bob on 2017/3/23.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^submitOrder)(void);
@interface IXDomResultTipView : UIView


@property (nonatomic, copy) submitOrder submitBlock;

@property (nonatomic, strong) NSArray *titleArr;

@property (nonatomic, strong) NSArray *contentArr;

@end
