//
//  IXTradeDeepV.h
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXTradeDeepV : UIView

- (id)initWithFrame:(CGRect)frame
          WithCount:(NSInteger)count;

- (void)setBuyPrc:(NSString *)buyPrc
           BuyVol:(NSString *)buyVol
          SellPrc:(NSString *)sellPrc
          SellVol:(NSString *)sellVol;

- (void)setDeepCount:(NSInteger)count;

@end

