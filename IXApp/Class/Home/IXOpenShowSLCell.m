//
//  IXOpenShowSLCell.m
//  IXApp
//
//  Created by Bob on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenShowSLCell.h"

@interface IXOpenShowSLCell ()

@property (nonatomic, strong) UISwitch *swi;

@end

@implementation IXOpenShowSLCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x242a36);
        
       UILabel *titlteLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 14, kScreenWidth - 15, 15)
                                               WithFont:PF_MEDI(13)
                                              WithAlign:NSTextAlignmentLeft
                                             wTextColor:0x99abba
                                             dTextColor:0x99abba];
        
        [self.contentView addSubview:titlteLbl];
        titlteLbl.text = LocalizedString(@"止盈止损");
    }
    return self;
}


- (void)setOpenSwi:(BOOL)openSwi
{
    [self.swi setOn:openSwi];
}

- (void)repsonseToSwitch
{
    if ( self.slBlock ) {
        self.slBlock();
    }
}


- (UISwitch *)swi
{
    if ( !_swi ) {
        _swi = [[UISwitch alloc] initWithFrame:CGRectMake( kScreenWidth - 65, 5, 50, 34)];
        _swi.dk_onTintColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
//        _swi.dk_thumbTintColorPicker = DKColorWithRGBs(0xffffff, 0x8395a4);
        _swi.dk_tintColorPicker = DKColorWithRGBs(0xe5e5e5, 0x4b5667);
        [self.contentView addSubview:_swi];
        [_swi addTarget:self
                 action:@selector(repsonseToSwitch)
       forControlEvents:UIControlEventValueChanged];
    }
    return _swi;
}


@end
