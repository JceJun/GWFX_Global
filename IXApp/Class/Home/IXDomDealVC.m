//
//  IXDomDealVC.m
//  IXApp
//
//  Created by Bob on 2017/3/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDomDealVC.h"
#import "IXSelfOrderListVC.h"
#import "IXOpenModel+CheckData.h"
#import "IXDomPosCell.h"
#import "IXDetailSymbolCell.h"
#import "IXDomOtherDealCell.h"
#import "IXDomMarketDealCell.h"

#import "NSArray+Expection.h"
#import "UIImageView+SepLine.h"

#import "IXDomDealM.h"
#import "IXAccountBalanceModel.h"
#import "IXOpenChooseContentView.h"

#import "IXDomResultTipView.h"
#import "IXDomResultView.h"

#import "IXUserDefaultM.h"
#import "IXCpyConfig.h"
#import "IXTouchTableV.h"
#import "IXNewGuideV.h"


@interface IXDomDealVC ()<UITableViewDelegate,UITableViewDataSource,IXNewGuideVDelegate>


@property (nonatomic, strong) IXDomDealM  *model;
@property (nonatomic, strong) proto_order_add *proto;

@property (nonatomic, strong) IXTouchTableV *contentTV;

@property (nonatomic, strong) IXOpenChooseContentView *chooseRequestVol;

@property (nonatomic, strong) IXDomResultTipView *tipView;

@property (nonatomic, strong) IXDomResultView *resultView;

@property (nonatomic, strong) IXNewGuideV *newGuideV;

@end

@implementation IXDomDealVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ORDER_LIST);
        
        [[IXAccountBalanceModel shareInstance] addObserver:self
                                                forKeyPath:KBUYVOL
                                                   options:NSKeyValueObservingOptionNew
                                                   context:nil];
        [[IXAccountBalanceModel shareInstance] addObserver:self
                                                forKeyPath:KSELLVOL
                                                   options:NSKeyValueObservingOptionNew
                                                   context:nil];
        [[IXAccountBalanceModel shareInstance] addObserver:self
                                                forKeyPath:KSYMPFT
                                                   options:NSKeyValueObservingOptionNew
                                                   context:nil];
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"--%s---",__func__);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_LIST);
    
    [[IXAccountBalanceModel shareInstance] removeObserver:self
                                               forKeyPath:KBUYVOL];
    [[IXAccountBalanceModel shareInstance] removeObserver:self
                                               forKeyPath:KSELLVOL];
    [[IXAccountBalanceModel shareInstance] removeObserver:self
                                               forKeyPath:KSYMPFT];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_LIST)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _model.posArr = [[IXTradeDataCache shareInstance].pb_cache_order_list mutableCopy];
            [self.contentTV reloadData];
        });
    }else if ([keyPath isEqualToString:KBUYVOL]){
        [self updateBuy];
    }else if ([keyPath isEqualToString:KSELLVOL]) {
       [self updateSell];
    }else if ([keyPath isEqualToString:KSYMPFT]){
        [self updateProfit];
    }
}

#warning 新加的UI和行情机制不匹配，导致改动好大／
- (void)updateBuy
{
    dispatch_async(dispatch_get_main_queue(), ^{
        IXDomPosCell *cell = [self.contentTV cellForRowAtIndexPath:
                              [NSIndexPath indexPathForRow:0 inSection:2]];
        [cell updateBuyVol];
    });
}

- (void)updateSell
{
    dispatch_async(dispatch_get_main_queue(), ^{
        IXDomPosCell *cell = [self.contentTV cellForRowAtIndexPath:
                              [NSIndexPath indexPathForRow:0 inSection:2]];
        [cell updateSellVol];
    });
}

- (void)updateProfit
{
    dispatch_async(dispatch_get_main_queue(), ^{
        IXDomPosCell *cell = [self.contentTV cellForRowAtIndexPath:
                              [NSIndexPath indexPathForRow:0 inSection:2]];
        [cell updateProfit];
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 75, 30)];
    [rightBtn setTitle:LocalizedString(@"我的挂单") forState:UIControlStateNormal];
    [rightBtn.titleLabel setFont:PF_MEDI(13)];
    [rightBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea) forState:UIControlStateNormal];
    [rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, -15)];
    [rightBtn addTarget:self action:@selector(rightBtnItemClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [rightBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MarketSymbolNameColor}
                                forState:UIControlStateNormal];
    [rightBarItem setTintColor:MarketSymbolNameColor];
    
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    self.title = LocalizedString(@"DOM交易");
    
    [self.contentTV reloadData];
    
    if (NewGuideEnable) {
        [self loadNewGuideV];
    }
}

#pragma mark 新手指引
- (void)loadNewGuideV
{
    if (![IXUserDefaultM getNewGuide:IXNewGuideDomMp]) {
        [self.view addSubview:self.newGuideV];
        [self.newGuideV showNewGuideType:IXNewGuideDomMp];
    } else {
        if (![IXUserDefaultM getNewGuide:IXNewGuideDomClick]) {
            [self.view addSubview:self.newGuideV];
            [self.newGuideV showNewGuideType:IXNewGuideDomClick];
        } else {
            if (![IXUserDefaultM getNewGuide:IXNewGuideDomLimit]) {
                [self.view addSubview:self.newGuideV];
                [self.newGuideV showNewGuideType:IXNewGuideDomLimit];
            }
        }
    }
}

- (IXNewGuideV *)newGuideV
{
    if ( !_newGuideV ) {
        _newGuideV = [[IXNewGuideV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, kScreenHeight)];
        _newGuideV.delegate = self;
    }
    return _newGuideV;
}

- (void)nextRemoveViewType:(IXNewGuide)type
{
    switch (type) {
        case IXNewGuideDomMp:{
            [self.view addSubview:self.newGuideV];
            [self.newGuideV showNewGuideType:IXNewGuideDomClick];
            [IXUserDefaultM saveNewGuide:IXNewGuideDomMp];
        }
            break;
        case IXNewGuideDomClick:{
            [self.view addSubview:self.newGuideV];
            [self.newGuideV showNewGuideType:IXNewGuideDomLimit];
            [IXUserDefaultM saveNewGuide:IXNewGuideDomClick];
        }
            break;
        case IXNewGuideDomLimit:{
            [IXUserDefaultM saveNewGuide:IXNewGuideDomLimit];
        }
            break;
            
        default:
            break;
    }
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    IXSelfOrderListVC *VC = [[IXSelfOrderListVC alloc] init];
    [self.navigationController pushViewController:VC animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self subscribeDynamicPrice];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self cancelDynamicPrice];
    
    [self removeRequstVol];
    [self removeResultView];
}

//订阅行情
- (void)subscribeDynamicPrice
{
    if ( _tradeModel && _tradeModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(_tradeModel.marketId),
                           @"id":@(_tradeModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] subscribeDetailPriceWithSymbolAry:arr];
        [self didResponseQuoteDistribute:[@[_tradeModel.quoteModel] mutableCopy]
                                     cmd:CMD_QUOTE_PUB_DETAIL];
        if (_nLastClosePrice <= 0) {
            [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:arr];
        }
    }
}

//取消行情
- (void)cancelDynamicPrice
{
    if ( _tradeModel && _tradeModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(_tradeModel.marketId),
                           @"id":@(_tradeModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] unSubscribeDetailPriceWithSymbolAry:arr];
    }
}

//显示结果：正常执行
- (void)showResultViewWithPrice:(NSString *)price WithVol:(NSString *)vol
{
    if( [self.navigationController.topViewController isKindOfClass:[self class]] ){
        
        [self removeResultView];
        
        NSString *volStr = LocalizedString(@"数量");
        if ( !_model.isVolNum ) {
            volStr = LocalizedString(@"手数");
        }
        self.resultView.tipTitle = LocalizedString(@"订单已执行");
        self.resultView.tipDes = [NSString stringWithFormat:@"%@%@，%@%@",
                                  LocalizedString(@"执行价格"),price,
                                  volStr,vol];
        [self.resultView setSuccessImg];
        [self.resultView show];
    }
}


- (void)showError:(NSString *)title Comment:(NSString *)comment
{
    if( [self.navigationController.topViewController isKindOfClass:[self class]] ){
        
        [self removeResultView];
        
        self.resultView.tipTitle = title;
        self.resultView.tipDes = comment;
        [self.resultView setErrorImg];

        [self.resultView show];
    }
}

//移除结果提示
- (void)removeResultView
{
    if ( self.resultView ) {
        [self.resultView removeFromSuperview];
    }
    
    if ( self.tipView ) {
        [self.tipView removeFromSuperview];
    }
}

#pragma mark -
#pragma mark - 行情数据
- (void)needRefresh
{
    [self subscribeDynamicPrice];
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if( cmd == CMD_QUOTE_PUB_DETAIL || cmd == CMD_QUOTE_SUB_DETAIL ){
        [self refreashQuote:arr];
    }
}

- (void)refreashQuote:(NSMutableArray *)arr
{
    for ( IXQuoteM *model in arr ) {
        if ( model.symbolId == _tradeModel.symbolModel.id_p ) {
            if ( _detailVC ) {
                [_detailVC refreashProfit];
            }

            _model.showM.quoteModel = [model mutableCopy];
            [_model setMaxVolWithQuote];
            for (UITableViewCell *cell in _contentTV.visibleCells) {
                if ([cell isKindOfClass:[IXDetailSymbolCell class]]) {
                    IXDetailSymbolCell *symCell = (IXDetailSymbolCell *)cell;
                    symCell.quoteModel = [model mutableCopy];
                }else if ([cell isKindOfClass:[IXDomOtherDealCell class]]) {
                    IXDomOtherDealCell *oCell = (IXDomOtherDealCell *)cell;
                    oCell.model = _model;
                }
            }
        }
    }
}


#pragma mark load moduel
- (void)setTradeModel:(IXTradeMarketModel *)tradeModel
{
    _tradeModel = tradeModel;
    _tradeState = [IXDataProcessTools symbolIsCanTrade:_tradeModel.symbolModel];
    
    _model = [[IXDomDealM alloc] init];
    _model.showM = _tradeModel;
    _model.posArr = [[IXTradeDataCache shareInstance].pb_cache_order_list mutableCopy];
    
    weakself;
    _model.notiDomSuc = ^(NSString *prc, NSString *vol) {
        if (![[UIApplication sharedApplication].keyWindow viewWithTag:TIPVIEW]) {
            [weakSelf showResultViewWithPrice:prc WithVol:vol];
        }
    };
    
    _model.notiDomFail = ^(NSString *errMsg) {
        if (![[UIApplication sharedApplication].keyWindow viewWithTag:TIPVIEW]) {
            [weakSelf showError:LocalizedString(@"下单失败")
                         Comment:errMsg];
        }
    };
    
    _model.notiUpdate = ^{
        if (weakSelf.model) {
            for (UITableViewCell *cell in weakSelf.contentTV.visibleCells) {
                if ([cell isKindOfClass:[IXDomOtherDealCell class]]) {
 
                    [(IXDomOtherDealCell *)cell updateOrderCount];
                    NSArray *arr = weakSelf.model.chooseStatusArr[cell.tag];
                    [(IXDomOtherDealCell *)cell updateStatus:arr];
                }
            }
        }
    };
    
    _model.notiPostOrder = ^{
        [weakSelf postOrder];
    };
    
    [self.contentTV reloadData];
}

- (void)showRequstVol
{
    [self.view endEditing:YES];
    [self resetVolArrow:YES];
    [self.chooseRequestVol resetDataSourceWithData:self.model.showVolArr];
    [self.chooseRequestVol show];
}

- (void)removeRequstVol
{
    [self resetVolArrow:NO];
    [self.chooseRequestVol dismiss];
}

- (void)resetVolArrow:(BOOL)down
{
    IXDomMarketDealCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UIButton *btn = (UIButton *)[cell.backClickView viewWithTag:CHOOSETAG];
    [btn setSelected:down];
}

//提交订单
- (void)postOrder
{
    [self.view endEditing:YES];
    
    [_model refreashRequestPrice];
 
    if ( _model.requestPrice &&
        !isnan([_model.requestPrice doubleValue]) &&
        [_model.requestPrice doubleValue]  ) {
        NSArray *arr = @[[_model.requestPrice stringValue],
                         @(_model.requestDir),
                         _model.requestVolume,
                         _model.requestType];
        _proto = [_model packageOrder];
        [self  showTip:arr];
    }else{
        ELog(@"深度价格异常");
    }

}


- (void)showTip:(NSArray *)arr
{
    if ( self.tipView ) {
        [self.tipView removeFromSuperview];
    }
    
    if ( self.resultView ) {
        [self.resultView removeFromSuperview];
    }
    
    if ( !_model.requestPrice ) {
        return;
    }
    
    
    if ( [IXUserDefaultM getDomResult] ) {
        
        NSString *direction = LocalizedString(@"买入");
        if( [arr[1] integerValue] == item_order_edirection_DirectionSell ){
            direction = LocalizedString(@"卖出");
        }
        
        NSString *type = arr[3];
        NSString *requestPrc = [NSString formatterPrice:arr[0]
                                             WithDigits:_tradeModel.symbolModel.digits];
 
        NSString *requestVol = arr[2];
        if (  [arr[2] isKindOfClass:[NSDecimalNumber class]] ) {
            requestVol = [arr[2] stringValue];
        }
        if ( !_model.isVolNum && _model.showM.symbolModel.contractSizeNew ) {
            requestVol = [NSString stringWithFormat:@"%@%@",[NSString thousandFormate:[NSString formatterPrice:requestVol WithDigits:2] withDigits:2],LocalizedString(@"手")];
        } else {
            requestVol = [NSString stringWithFormat:@"%@%@",[NSString thousandFormate:[NSString formatterPrice:requestVol WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits],_tradeModel.symbolModel.unitLanName];
        }
        NSArray *contentArr = @[
                                 direction,
                                 type,
                                 requestPrc,
                                 requestVol
                                ];
        
        self.tipView.contentArr = contentArr;
        [[UIApplication sharedApplication].keyWindow addSubview:self.tipView];
    }else{
        [[IXTCPRequest shareInstance] orderWithParam:self.proto];
    }
}

- (void)removeTip
{
    if ( self.tipView ) {
        [self.tipView removeFromSuperview];
    }
    
    if ( self.resultView ) {
        [self.resultView removeFromSuperview];
    }
}

#pragma mark tableView delegate && dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (0 == section) {
        return 2;
    }else if (1 == section){
        return 10;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (0 == section) {
        return 1;
    }else if (1 == section){
        return 1;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (0 == section || 1 == section) {
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    UIImageView *topLine = [UIImageView addSepImageWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)
                                                 WithColor:CellSepLineColor
                                                 WithAlpha:1.f];
    [headerView addSubview:topLine];
    
    UIImageView *btmLine = [UIImageView addSepImageWithFrame:CGRectMake(0, 10 - kLineHeight, kScreenWidth, kLineHeight)
                                                   WithColor:CellSepLineColor
                                                   WithAlpha:1.f];
    [headerView addSubview:btmLine];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == indexPath.section){
        if (0 == indexPath.row){
            return 74;
        }
        return  98;
    }else if(1 == indexPath.section){
        if ( !indexPath.row ){
            return 48;
        }
        return  38;
    }
    return 59;
}

- (UITableViewCell *)sectionFirst:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.row ) {
        case 0:{
            IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                        NSStringFromClass([IXDetailSymbolCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.nLastClosePrice = _nLastClosePrice;
            cell.symbolModel = _tradeModel.symbolModel;
            cell.quoteModel = _tradeModel.quoteModel;
            cell.marketId = _tradeModel.marketId;
            cell.tradeState = _tradeState;
            return cell;
        }
            break;
        default:{
            IXDomMarketDealCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                         NSStringFromClass([IXDomMarketDealCell class])];
            cell.contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.model = self.model;
            if (_model.isVolNum) {
                cell.vol = [NSString thousandFormate:[NSString formatterPrice:[self.model.showVolume stringValue] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits];
            } else {
                cell.vol = [NSString thousandFormate:[NSString formatterPrice:[self.model.showVolume stringValue] WithDigits:2] withDigits:2];
            }
            NSMutableString *unitName = _model.isVolNum ? [LocalizedString(@"数量") mutableCopy] :
            [LocalizedString(@"手数") mutableCopy];
            if ( [self.tradeModel.symbolModel.unitLanName length] ) {
                [unitName appendString:[NSString stringWithFormat:@"(%@)",
                                        [IXDataProcessTools showCurrentUnitLanName:
                                         self.tradeModel.symbolModel.unitLanName]]];
            }
            cell.unitName = unitName;
            
            
            weakself;
            cell.requstVol = ^(){
                [weakSelf showRequstVol];
            };
            
            cell.inVol = ^(NSString *vol){
                NSString *volStr = [vol stringByReplacingOccurrencesOfString:@"," withString:@""];
                weakSelf.tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:volStr];
                weakSelf.tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:volStr];
                weakSelf.model.requestVolume = [NSDecimalNumber decimalNumberWithString:volStr];
                weakSelf.model.showVolume = [NSDecimalNumber decimalNumberWithString:volStr];
            };
            
            cell.addOrder = ^(item_order_edirection dir){
                weakSelf.view.userInteractionEnabled = NO;
                weakSelf.model.requestType = MARKETORDER;
                weakSelf.model.requestDir = dir;
                weakSelf.tradeModel.requestType = MARKETORDER;
                weakSelf.tradeModel.direction = dir;
                [weakSelf.model resetDeepPrice];
                [weakSelf postOrder];
                weakSelf.view.userInteractionEnabled = YES;
            };
            return cell;
            
        }
            break;
    }
}

- (UITableViewCell *)sectionSecond:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDomOtherDealCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                NSStringFromClass([IXDomOtherDealCell class])];
    cell.contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.tag = indexPath.row;
    
    weakself;
    cell.order = ^(item_order_edirection dir){
        @synchronized (weakSelf.contentTV) {
            weakSelf.view.userInteractionEnabled = NO;
            [weakSelf.model chooseWithRow:indexPath.row WithDir:dir];
            weakSelf.view.userInteractionEnabled = YES;
            [weakSelf.contentTV reloadData];
        }
    };
    
    NSArray *arr = self.model.chooseStatusArr[indexPath.row];
    [cell updateStatus:arr];
  
    cell.model = _model;

    return cell;
}

- (UITableViewCell *)sectionThird:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDomPosCell *cell = [tableView dequeueReusableCellWithIdentifier:
                          NSStringFromClass([IXDomPosCell class])];
    cell.contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
    cell.model = _model;
    [cell updateProfit];
    [cell updateSellVol];
    [cell updateBuyVol];
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == indexPath.section) {
        return [self sectionFirst:tableView cellForRowAtIndexPath:indexPath];
    }else if (1 == indexPath.section){
        return [self sectionSecond:tableView cellForRowAtIndexPath:indexPath];
    }
    return [self sectionThird:tableView cellForRowAtIndexPath:indexPath];
}
 
#pragma mark init dataSource
- (IXOpenChooseContentView *)chooseRequestVol
{
    if (!_chooseRequestVol) {
        weakself;
        _chooseRequestVol = [[IXOpenChooseContentView alloc] initWithDataSource:self.model.showVolArr WithSelectedRow:^(NSInteger row) {
            [weakSelf updateVol:row];
        }];
        _chooseRequestVol.endEditBlock = ^{
            [weakSelf resetVolArrow:NO];
        };
        _chooseRequestVol.cntFont = RO_REGU(15);
    }
    return _chooseRequestVol;
}

- (void)updateVol:(NSInteger)row
{
    NSArray *arr = self.model.showVolArr;
    if (row >= 0 && row < arr.count) {
        NSString *volStr = [arr[row] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:
                                           volStr];
        self.model.showVolume = [NSDecimalNumber decimalNumberWithString:
                                      volStr];
        self.tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                         volStr];
        self.model.requestVolume =  [NSDecimalNumber decimalNumberWithString:
                                     volStr];

        [self.contentTV reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]]
                                   withRowAnimation:UITableViewRowAnimationNone];
    }
    [self resetVolArrow:NO];
}

- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight)];
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _contentTV.dk_backgroundColorPicker = DKViewColor;
        
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXDetailSymbolCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        [_contentTV registerClass:[IXDomOtherDealCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDomOtherDealCell class])];
        [_contentTV registerClass:[IXDomMarketDealCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDomMarketDealCell class])];
        [self.view addSubview:_contentTV];
        [_contentTV registerClass:[IXDomPosCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDomPosCell class])];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:ktableFooterFrame];
    }
    return _contentTV;
}

- (IXDomResultTipView *)tipView
{
    if ( !_tipView ) {
        _tipView = [[IXDomResultTipView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
        NSString *volStr = LocalizedString(@"数量");
        if ( !_model.isVolNum ) {
            volStr = LocalizedString(@"手数");
        }
        _tipView.titleArr = @[LocalizedString(@"方向"),
                              LocalizedString(@"类别"),
                              LocalizedString(@"请求价"),
                              volStr];
        _tipView.tag = TIPVIEW;
    }
    
    weakself;
    _tipView.submitBlock = ^{
        [[IXTCPRequest shareInstance] orderWithParam:weakSelf.proto];
    };
    
    return _tipView;
}

- (IXDomResultView *)resultView
{
    if ( !_resultView ) {
        _resultView = [[IXDomResultView alloc] initWithFrame:CGRectMake( 0, kNavbarHeight, kScreenWidth, 68)];
        _resultView.tag = RESULTVIEW;
        
        _resultView.layer.shadowOffset = CGSizeMake(5, 3);
        _resultView.layer.shadowOpacity = 0.6;
        _resultView.layer.shadowColor = [UIColor blackColor].CGColor;
    }
    return _resultView;
}
@end
