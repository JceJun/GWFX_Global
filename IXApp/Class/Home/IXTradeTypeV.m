//
//  IXTradeTypeV.m
//  IXApp
//
//  Created by Bob on 2016/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeTypeV.h"
#import "IXOpenChoiceV.h"
#import "UIImageView+SepLine.h"

@interface IXTradeTypeV ()

@property (nonatomic, strong) IXOpenChoiceV *typeChoice;
@property (nonatomic, strong) IXOpenChoiceV *expireChoice;

@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation IXTradeTypeV

- (id)initWithFrame:(CGRect)frame WithTadeType:(chooseTrade)trade
{
    self = [super initWithFrame:frame];
    if (self) {
        self.tradeCon = trade;
        CGRect currentFrame = CGRectMake( 0, 0, kScreenWidth/2, 55);
        UIView *leftView = [[UIView alloc] initWithFrame:currentFrame];
        [self addSubview:leftView];
        leftView.backgroundColor = [UIColor whiteColor];
        leftView.tag = 1;
        
        currentFrame = CGRectMake( kScreenWidth/2, 0, kScreenWidth/2, 55);
        UIView *rightView = [[UIView alloc] initWithFrame:currentFrame];
        [self addSubview:rightView];
        rightView.backgroundColor = [UIColor whiteColor];
        
        currentFrame.origin.x = 15;
        currentFrame.origin.y = 21;
        currentFrame.size.height = 15;

        frame = leftView.frame;
        frame.origin.x = 60;
        frame.size.width = kScreenWidth/2 - 60;
        weakself;
        _typeChoice = [[IXOpenChoiceV alloc] initWithFrame:frame
                                                    WithTitle:MARKETORDER
                                                    WithAlign:TapChoiceAlignRight
                                                      WithTap:^{
                                                          [weakSelf responseToTypeGes:nil];
                                                      }];
        [leftView addSubview:_typeChoice];
        
        
        _expireChoice = [[IXOpenChoiceV alloc] initWithFrame:frame
                                                      WithTitle:LocalizedString(@"当日")
                                                      WithAlign:TapChoiceAlignRight
                                                        WithTap:^{
                                                            [weakSelf responseToExpireGes:nil];
                                                        }];
        [rightView addSubview:_expireChoice];
        _expireChoice.hidden = YES;
        
        UILabel *leftTitle = [IXUtils createLblWithFrame:currentFrame
                                                WithFont:PF_MEDI(13)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x99abba
                                              dTextColor:0xff0000];
        [leftView addSubview:leftTitle];
        leftTitle.text = LocalizedString(@"类型");
        
        UILabel *rightTitle = [IXUtils createLblWithFrame:currentFrame
                                                 WithFont:PF_MEDI(13)
                                                WithAlign:NSTextAlignmentLeft
                                               wTextColor:0x99abba
                                               dTextColor:0xff0000];
        [rightView addSubview:rightTitle];
        rightTitle.text = LocalizedString(@"有效期");
        
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 13, kScreenWidth/2 - 20, 28)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentRight
                                   wTextColor:0x99abba
                                   dTextColor:0xff0000];
        [rightView addSubview:_tipLbl];
        _tipLbl.text = @"-";

        [self addSepLine];
    }
    return self;
}


- (void)addSepLine
{
    UIImageView *centerSep = [UIImageView addSepImageWithFrame:CGRectMake(  kScreenWidth/2 - 0.5, 0, 1, 56)
                                                     WithColor:MarketCellSepColor
                                                     WithAlpha:1];
    [self addSubview:centerSep];
    
    UIImageView *topSep = [UIImageView addSepImageWithFrame:CGRectMake( 0, 0, kScreenWidth, 1)
                                                  WithColor:MarketCellSepColor
                                                  WithAlpha:1];
    [self addSubview:topSep];
}

- (void)responseToTypeGes:(UITapGestureRecognizer *)tapGes
{
    if (self.tradeCon) {
        self.tradeCon(Trade_kind,_typeChoice.arrowImg);
    }
}

- (void)responseToExpireGes:(UITapGestureRecognizer *)tapGes
{
    if (self.tradeCon) {
        self.tradeCon(Trade_expire,_expireChoice.arrowImg);
    }
}

- (void)setKind:(NSString *)kind
{
    [self updateUIWithKind:kind];
    [_typeChoice resetTitle:kind];
}

- (void)setExpire:(NSString *)expire
{
    [_expireChoice resetTitle:expire];
}

- (void)updateUIWithKind:(NSString *)kind
{
    _expireChoice.hidden = [kind isEqualToString:MARKETORDER];
    _tipLbl.hidden = ![kind isEqualToString:MARKETORDER];
    
}

@end
