//
//  IXDomResultView.h
//  IXApp
//
//  Created by Bob on 2017/3/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDomResultView : UIView

@property (nonatomic, strong) NSString *tipTitle;

@property (nonatomic, strong) NSString *tipDes;

- (void)setErrorImg;
- (void)setSuccessImg;

- (void)show;

@end
