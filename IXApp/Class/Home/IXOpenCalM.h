//
//  IXOpenCalM.h
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXOpenRootVC.h"

#import "IXOpenTipV.h"

@interface IXOpenCalM : NSObject

//刷新保证金和佣金,百万分之一
//行情推回来的时候，是主线程
+ (NSArray *)resetMarginAssetWithVolume:(double)volume
                               WithRoot:(IXOpenRootVC *)root;

+ (void)addPrcTipInfo:(IXOpenRootVC *)root;
+ (void)removePrcTipInfo:(IXOpenRootVC *)root;

+ (void)showTip:(NSString *)checkOrderInfo
       WithView:(IXOpenTipV *)view
         InView:(UIView *)rootView;

+ (void)removeTip:(IXOpenTipV *)view;

+ (NSString *)checkResult:(IXOpenRootVC *)root;

+ (void)updateTipInfoWithView:(IXOpenTipV *)view
                     WithRoot:(IXOpenRootVC *)root;

@end
