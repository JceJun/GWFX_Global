//
//  IXAccTextField.m
//  IXApp
//
//  Created by Bob on 2017/6/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAccTextField.h"

@implementation IXAccTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if ( self ) {
        
        self.borderStyle = UITextBorderStyleNone;
        self.textAlignment = NSTextAlignmentCenter;
        self.keyboardType = UIKeyboardTypeDecimalPad;
        self.font = RO_REGU(15);
        self.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        self.placeholder = LocalizedString(@"没有设置");

        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSFontAttributeName] = PF_MEDI(13);
        if ( [IXUserInfoMgr shareInstance].isNightMode ) {
            dict[NSForegroundColorAttributeName] = UIColorHexFromRGB(0x303b4d);
        }else{
            dict[NSForegroundColorAttributeName] = MarketCellSepColor;
        }
        
        NSAttributedString *attribute = [[NSAttributedString alloc] initWithString:self.placeholder
                                                                        attributes:dict];
        [self setAttributedPlaceholder:attribute];
        
        [self setTextFieldInputAccessoryView];
    }
    return self;
}

- (void)setTextFieldInputAccessoryView
{
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    [topView setBarStyle:UIBarStyleDefault];
    topView.backgroundColor = MarketCellSepColor;
    
    UIBarButtonItem * spaceBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:self
                                                                              action:nil];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:PF_MEDI(13)];
    [cancelBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(15, 9, 60, 25);
    [cancelBtn addTarget:self action:@selector(cancelKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBtnItem = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneBtn setTitle:LocalizedString(@"完成") forState:UIControlStateNormal];
    [doneBtn.titleLabel setFont:PF_MEDI(13)];
    [doneBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    doneBtn.frame = CGRectMake(kScreenWidth - 90, 9, 80, 25);
    [doneBtn addTarget:self action:@selector(dealKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneBtnItem = [[UIBarButtonItem alloc]initWithCustomView:doneBtn];
    NSArray * buttonsArray = [NSArray arrayWithObjects:cancelBtnItem,spaceBtn,doneBtnItem,nil];
    [topView setItems:buttonsArray];
    [self setInputAccessoryView:topView];
    [self setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self setAutocapitalizationType:UITextAutocapitalizationTypeNone];
}

- (void)dealKeyboardHidden
{
    [self resignFirstResponder];
    if ( self.editBlock ) {
        self.editBlock();
    }
}

- (void)cancelKeyboardHidden{
    [self resignFirstResponder];
}

@end
