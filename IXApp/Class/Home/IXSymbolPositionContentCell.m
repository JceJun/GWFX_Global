//
//  IXSymbolPositionContentCell.m
//  IXApp
//
//  Created by Bob on 2017/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSymbolPositionContentCell.h"
#import "NSString+FormatterPrice.h"
#import "IXAppUtil.h"

@interface IXSymbolPositionContentCell ()

@property (nonatomic, strong) UILabel *posDiretionLbl;
@property (nonatomic, strong) UILabel *posNumLbl;
@property (nonatomic, strong) UILabel *nPriceLbl;
@property (nonatomic, strong) UILabel *profitLbl;

@end

@implementation IXSymbolPositionContentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
 
        [self.contentView addSubview:self.posDiretionLbl];
        [self.contentView addSubview:self.posNumLbl];
        [self.contentView addSubview:self.nPriceLbl];
        [self.contentView addSubview:self.profitLbl];
        
        UIImageView *contractImg = [[UIImageView alloc] initWithFrame:
                                    CGRectMake( kScreenWidth - 25, 9, 15, 15)];
        
        if ( [IXUserInfoMgr shareInstance].isNightMode ) {
            contractImg.image = [UIImage imageNamed:@"symbolDetail_contract_D"];
        }else{
            contractImg.image = [UIImage imageNamed:@"symbolDetail_contract"];
        }
        [self.contentView addSubview:contractImg];
    }
    return self;
}


- (UILabel *)posDiretionLbl
{
    if ( !_posDiretionLbl ) {
        _posDiretionLbl = [IXUtils createLblWithFrame:CGRectMake( 10, 9, 15, 15)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0xffffff
                                           dTextColor:0x22262e];
    }
    return _posDiretionLbl;
}


- (UILabel *)posNumLbl
{
    if ( !_posNumLbl ) {
        _posNumLbl = [IXUtils createLblWithFrame:CGRectMake( 30, 10, 80, 13)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea];
    }
    return _posNumLbl;
}

- (UILabel *)nPriceLbl
{
    if ( !_nPriceLbl ) {
        _nPriceLbl = [IXUtils createLblWithFrame:CGRectMake( 117, 10, 110, 13)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea];
    }
    return _nPriceLbl;
}

- (UILabel *)profitLbl
{
    if ( !_profitLbl ) {
        _profitLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth - 130, 10, 100, 13)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentRight
                                      wTextColor:0x4c6072
                                      dTextColor:0xff0000];
    }
    return _profitLbl;
}


- (void)setModel:(IXSymbolPositionM *)model
{
    _posDiretionLbl.text = model.posDirection;
    if( [_posDiretionLbl.text isEqualToString:LocalizedString(@"卖.")] ){
        _posDiretionLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor,mDRedPriceColor);
    }
    else{
        _posDiretionLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor,mDGreenPriceColor);
    }

    _posNumLbl.text = model.showNum;
    _nPriceLbl.text = model.posPrice;

    if ( !model.posProfit || model.posProfit.length == 0 ) {
        _profitLbl.text = @"-";
        _profitLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
    }else{
        if ( [model.posProfit doubleValue] < 0 ) {
            _profitLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            _profitLbl.text = [NSString stringWithFormat:@"-%@",[IXAppUtil amountToString:[model.posProfit stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
        } else if ( [model.posProfit doubleValue] == 0 ) {
            _profitLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
            _profitLbl.text = [NSString formatterProfit:model.posProfit];
        } else {
           _profitLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
            _profitLbl.text = [NSString stringWithFormat:@"+%@",[IXAppUtil amountToString:[model.posProfit stringByReplacingOccurrencesOfString:@"+" withString:@""]]];
        }
    }
}

@end
