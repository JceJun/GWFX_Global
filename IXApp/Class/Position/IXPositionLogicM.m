//
//  IXPositionLogicM.m
//  IXApp
//
//  Created by Evn on 2017/6/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionLogicM.h"
#import "IXOpenModel.h"
#import "IXOpenModel+CheckData.h"

@implementation IXPositionLogicM

+ (void)addPrice:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.requestPrice || isnan([root.positionModel.requestPrice floatValue]) ) {
        root.positionModel.requestPrice = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minRequestPrice.decimalValue];
    }else{
        root.positionModel.requestPrice = [root.positionModel.requestPrice decimalNumberByAdding:root.openModel.stepPrice];
        if (![root.openModel validPrice:root.positionModel.requestPrice] ) {
            root.positionModel.requestPrice = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minRequestPrice.decimalValue];
        }
    }
    
    root.openModel.requestPrice = root.positionModel.requestPrice;
}

+ (void)addVolume:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.requestVolume || isnan([root.positionModel.requestVolume floatValue]) ) {
        root.positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                            [NSString stringWithFormat:@"%f", [self getPositionVolumesMin:root]]];
    }else{
        root.positionModel.requestVolume = [root.positionModel.requestVolume decimalNumberByAdding:
                                            [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf", root.positionModel.symbolModel.volumesStep]]];
        
        if ( ![root.openModel validPositionVolume:root.positionModel.requestVolume] ) {
            root.positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                                [NSString stringWithFormat:@"%f", [self getPositionVolumesMin:root]]];
        }
    }
}

+ (void)addProfit:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.takeprofit || isnan([root.positionModel.takeprofit floatValue]) ) {
        root.positionModel.takeprofit = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minProfit.decimalValue];
    }else{
        root.positionModel.takeprofit = [root.positionModel.takeprofit decimalNumberByAdding:root.openModel.stepPrice];
        if( ![root.openModel validProfit:root.positionModel.takeprofit] ){
            root.positionModel.takeprofit = root.openModel.minProfit;
        }
    }
}

+ (void)addLoss:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.stoploss || isnan([root.positionModel.stoploss floatValue]) ) {
        root.positionModel.stoploss = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minLoss.decimalValue];
    }else{
        root.positionModel.stoploss = [root.positionModel.stoploss decimalNumberByAdding:root.openModel.stepPrice];
        if( ![root.openModel validLoss:root.positionModel.stoploss]  ){
            root.positionModel.stoploss = root.openModel.minLoss;
        }
    }
}

+ (void)updateTakeprofitWithTag:(ROWNAME)tag WithRoot:(IXPositionInfoVC *)root
{
    switch (tag) {
        case ROWNAMEPRICE:{
            [IXPositionLogicM addPrice:root];
        }
            break;
        case ROWNAMEVOLUME:{
            [IXPositionLogicM addVolume:root];
        }
            break;
        case ROWNAMEPROFIT:{
            [IXPositionLogicM addProfit:root];
        }
            break;
        case ROWNAMELOSS:{
            [IXPositionLogicM addLoss:root];
        }
            break;
        default:
            break;
    }
}

+ (void)substractPrice:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.requestPrice || isnan([root.positionModel.requestPrice floatValue]) ) {
        root.positionModel.requestPrice = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.maxRequestPrice.decimalValue];
    }else{
        root.positionModel.requestPrice = [root.positionModel.requestPrice decimalNumberBySubtracting:root.openModel.stepPrice];
        if (![root.openModel validPrice:root.positionModel.requestPrice] ) {
            root.positionModel.requestPrice = root.openModel.maxRequestPrice;
        }
    }
    root.openModel.requestPrice = root.positionModel.requestPrice;
}

+ (void)substractVolume:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.requestVolume || isnan([root.positionModel.requestVolume floatValue]) ) {
        root.positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", root.positionModel.position.volume]];
    }else{
        root.positionModel.requestVolume = [root.positionModel.requestVolume decimalNumberBySubtracting:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", root.positionModel.symbolModel.volumesStep]]];
        if ( ![root.openModel validPositionVolume:root.positionModel.requestVolume] ) {
            root.positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", root.positionModel.position.volume]];
        }
    }
}


+ (void)substractProfit:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.takeprofit || isnan([root.positionModel.takeprofit floatValue]) ) {
        root.positionModel.takeprofit = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.maxProfit.decimalValue];
    }else{
        root.positionModel.takeprofit = [root.positionModel.takeprofit decimalNumberBySubtracting:root.openModel.stepPrice];
        if ( ![root.openModel validProfit:root.positionModel.takeprofit] ) {
            root.positionModel.takeprofit = root.openModel.maxProfit;
        }
    }
}

+ (void)substractLoss:(IXPositionInfoVC *)root
{
    if ( !root.positionModel.stoploss || isnan([root.positionModel.stoploss floatValue]) ) {
        root.positionModel.stoploss = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.maxLoss.decimalValue];
    }else{
        root.positionModel.stoploss = [root.positionModel.stoploss decimalNumberBySubtracting:root.openModel.stepPrice];
        if ( ![root.openModel validLoss:root.positionModel.stoploss] ) {
            root.positionModel.stoploss = root.openModel.maxLoss;
        }
    }
}

+ (void)updateStoplossWithTag:(ROWNAME)tag WithRoot:(IXPositionInfoVC *)root
{
    switch (tag) {
        case ROWNAMEPRICE:{
            [IXPositionLogicM substractPrice:root];
        }
            break;
        case ROWNAMEVOLUME:{
            [IXPositionLogicM substractVolume:root];
        }
            break;
        case ROWNAMEPROFIT:{
            [IXPositionLogicM substractProfit:root];
        }
            break;
        case ROWNAMELOSS:{
            [IXPositionLogicM substractLoss:root];
        }
            break;
        default:
            break;
    }
}

+ (double)getPositionVolumesMin:(IXPositionInfoVC *)root
{
    double  volumesMin;
    if (root.positionModel.symbolModel.volumesMin > root.positionModel.position.volume) {
        volumesMin = root.positionModel.position.volume;
    } else {
        volumesMin = root.positionModel.symbolModel.volumesMin;
    }
    return volumesMin;
}
@end
