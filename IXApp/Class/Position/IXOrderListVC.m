//
//  IXOrderListVC.m
//  IXApp
//
//  Created by Magee on 16/11/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOrderListVC.h"
#import "IXOrderListCell.h"

#import "IXDBSymbolMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBGlobal.h"
#import "IXOrderListInfoVC.h"
#import "IXPosDefultView.h"
#import "IXPositionSymModel.h"

@interface IXOrderListVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) IXPosDefultView *posDefView;

@property (nonatomic, strong) NSMutableArray *orderAry;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation IXOrderListVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_CHANGE);
        
        IXTradeData_listen_regist(self, PB_CMD_ORDER_LIST);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_ADD);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_CANCEL);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOrderUnit) name:kNotificationUnit object:nil];
        
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_CHANGE);
    
    IXTradeData_listen_resign(self, PB_CMD_ORDER_LIST);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_CANCEL);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)refreshOrderUnit
{
    [self.tableV reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshData];
    [self tableV];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)refreshData {
    _orderAry = [IXTradeDataCache shareInstance].pb_cache_order_list;
    [self setOrderData];
}

#warning 排序可以优化
- (void)setOrderData
{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    } else {
        [_dataArr removeAllObjects];
    }
    if (_orderAry.count > 0) {
        for (int i = 0; i < _orderAry.count; i++) {
            IXOrderModel *model = [[IXOrderModel alloc] init];
            item_order *order = [[item_order alloc] init];
            order = _orderAry[i];
            model.order = order;
            
            if (order.type == item_order_etype_TypeLimit || order.type == item_order_etype_TypeStop) {
                IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:order.symbolid];
                if (symModel) {
                    model.symbolModel = symModel;
                    [_dataArr addObject:model];
                }
            }
        }
    }
    [self reloadTableViewData];
}

#pragma mark 刷新list列表
- (void)refreshListData:(NSArray *)listArr {
    
    if (!listArr || listArr.count == 0) {
        return;
    }
    for (int i = 0; i < listArr.count; i++) {
        IXOrderModel *model = [[IXOrderModel alloc] init];
        item_order *order = listArr[i];
        model.order = order;
        
        BOOL flag = NO;
        for (IXOrderModel *tmpModel in _dataArr) {
            if (order.id_p == tmpModel.order.id_p) {
                flag = YES;
                break;
            }
        }
        if (flag) {
            continue;
        }
        if (order.type == item_order_etype_TypeLimit || order.type == item_order_etype_TypeStop) {
            IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:order.symbolid];
            if (symModel) {
                model.symbolModel = symModel;
                [_dataArr insertObject:model atIndex:0];
            }
        }
    }
    [self reloadTableViewData];
}

#pragma mark 刷新add列表
- (void)refreshAddData {
    
    _orderAry = [IXTradeDataCache shareInstance].pb_cache_order_list;
    if (!_orderAry || _orderAry.count == 0) {
        
        return;
    }
    if ( !_dataArr ) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    IXOrderModel *model = [[IXOrderModel alloc] init];
    item_order *order = _orderAry[0];
    model.order = order;
    
    //去重、避免后台返回多次add
    BOOL flag = NO;
    for (IXOrderModel *tmpModel in _dataArr) {
        
        if (order.id_p == tmpModel.order.id_p) {
            flag = YES;
            break;
        }
    }
    if (flag) {
        
        return;
    }
    
    if (order.type == item_order_etype_TypeLimit || order.type == item_order_etype_TypeStop) {
        IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:order.symbolid];
        if (symModel) {
            model.symbolModel = symModel;
            [_dataArr insertObject:model atIndex:0];
            [self reloadTableViewData];
        }
    }
    
}

#pragma mark 刷新update列表
- (void)refreshUpdateData:(proto_order_update *)pb {
    
    
    if ( !_dataArr || _dataArr.count == 0) {
        
        return;
    }
    NSInteger index = -1;
    IXOrderModel *model;
    for (int i = 0; i < _dataArr.count; i++) {
        
        model = _dataArr[i];
        if (model.order.id_p == pb.order.id_p) {
            
            model.order = pb.order;
            index = i;
            break;
        }
    }
    if (index == -1) {
        
        return;
    }
    if (pb.order.type == item_order_etype_TypeLimit || pb.order.type == item_order_etype_TypeStop) {
        
        if (pb.order.status == item_order_estatus_StatusFilled ||
            pb.order.status == item_order_estatus_StatusRejected) {
            
            [_dataArr removeObjectAtIndex:index];
        } else if (pb.order.status == item_order_estatus_StatusPlaced) {
            
            [_dataArr replaceObjectAtIndex:index withObject:model];
        }
    }
    [self reloadTableViewData];
    
}

#pragma mark 刷新cancel列表
- (void)refreshCancelData:(proto_order_cancel *)pb {
    
    if ( !_dataArr || _dataArr.count == 0) {
        
        return;
    }
    NSInteger index = -1;
    IXOrderModel *model;
    for (int i = 0; i < _dataArr.count; i++) {
        
        model = _dataArr[i];
        if (model.order.id_p == pb.order.id_p) {
            
            model.order = pb.order;
            index = i;
            break;
        }
    }
    if (index == -1) {
        
        return;
    }
    if (pb.order.type == item_order_etype_TypeLimit || pb.order.type == item_order_etype_TypeStop) {
        
        [_dataArr removeObjectAtIndex:index];
    }
    [self reloadTableViewData];
    
}

#pragma mark UI刷新列表
- (void)reloadTableViewData {
    
    if (_dataArr.count == 0) {
        
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        
        self.posDefView.imageName = @"newList_noneMsg_avatar";
        self.posDefView.tipMsg = LocalizedString(@"暂无挂单");
        
        self.tableV.scrollEnabled = NO;
        [self.tableV reloadData];
        return;
    }else{
        
        [self.posDefView removeFromSuperview];
        self.tableV.scrollEnabled = YES;
        [self.tableV reloadData];
    }
}

#pragma mark -
#pragma mark - lazy load

- (UITableView *)tableV
{
    if(!_tableV)
    {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 5, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight - kTabbarHeight - 5)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        [_tableV registerClass:[IXOrderListCell class] forCellReuseIdentifier:NSStringFromClass([IXOrderListCell class])];
        [_tableV setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}


- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0, 80, kScreenWidth, 217)];
    }
    return _posDefView;
}

#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 114;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXOrderListCell class])];
    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell reloadUIWithOrderModel:_dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXOrderListInfoVC *VC = [[IXOrderListInfoVC alloc] init];
    VC.orderModel = _dataArr[indexPath.row];
    VC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_LIST)) {
        
        proto_order_list  *pb = ((IXTradeDataCache *)object).pb_order_list;
        [self refreshListData:pb.orderArray];
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_UPDATE)) {
        proto_order_update  *pb = ((IXTradeDataCache *)object).pb_order_update;
        if (pb.result == 0) {
            if (pb.order.type == item_order_etype_TypeLimit ||
                pb.order.type == item_order_etype_TypeStop) {
                [self refreshUpdateData:pb];
            }
        }
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_ADD)) {
        proto_order_add  *pb = ((IXTradeDataCache *)object).pb_order_add;
        if (pb.result == 0) {
            if (pb.order.type == item_order_etype_TypeLimit ||
                pb.order.type == item_order_etype_TypeStop) {
                [self refreshAddData];
            }
        }
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_ORDER_CANCEL)) {
        proto_order_cancel  *pb = ((IXTradeDataCache *)object).pb_order_cancel;
        if (pb.result == 0) {
            if (pb.order.type == item_order_etype_TypeLimit ||
                pb.order.type == item_order_etype_TypeStop) {
                [self refreshCancelData:pb];
            }
        }
    } else if ([keyPath isEqualToString:PB_CMD_ACCOUNT_CHANGE] ) {
        [_dataArr removeAllObjects];
        [self reloadTableViewData];
    }
}

@end

