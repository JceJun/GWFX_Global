//
//  IXPositionInfoVC.m
//  IXApp
//
//  Created by Evn on 16/12/26.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXPositionInfoVC.h"
#import "IXPositionInfoCell.h"
#import "IXDetailSymbolCell.h"
#import "UILabel+LineSpace.h"
#import "IXTradeVM.h"
#import "IXTouchTableV.h"
#import "IXBtomBtnV.h"
#import "IXDBGlobal.h"
#import "IxProtoHeader.pbobjc.h"
#import "NSString+FormatterPrice.h"
#import "IXSymbolDetailVC.h"
#import "IXPositionResultVC.h"
#import "IXLastQuoteM.h"
#import "IXPositionOpenCell.h"
#import "IXPositionInfoCellA.h"
#import "IXDBSymbolHotMgr.h"
#import "IXAccountBalanceModel.h"
#import "IXTradeMarginV.h"
#import "IXOpenTipV.h"
#import "IXAlertVC.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "IXCpyConfig.h"

#import "IXOpenCellM.h"
#import "IXPositionCalM.h"
#import "IXPositionLogicM.h"
#import "IXPositionQuoteM.h"

#import "UIImageView+SepLine.h"
#import "IXPullDownMenu.h"
#import "IXUserDefaultM.h"

@interface IXPositionInfoVC ()
<
UITableViewDataSource,
UITableViewDelegate,
ExpendableAlartViewDelegate,
UITextFieldDelegate
>
{
    UILabel *posLbl;
    UILabel *baseLbl;
    UILabel *profitLbl;
}

@property (nonatomic, strong) IXTouchTableV *tableV;
@property (nonatomic, strong) IXBtomBtnV    *btomV;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, assign) double nLastClosePrice;
@property (nonatomic, strong) IXOpenTipV *tipView;
@property (nonatomic, assign) BOOL isFold;//是否展开
@property (nonatomic, strong) IXPullDownMenu    *menu;
@property (nonatomic, strong) NSMutableArray *volArr;
@property (nonatomic, strong) NSMutableArray *showVolArr;
@property (nonatomic, assign) NSInteger quickChooseType;
//YES：数量   NO：手数
@property (nonatomic, assign) BOOL isVolNum;
@property (nonatomic, assign) NSInteger volDigits;//手数小数位

@end

@implementation IXPositionInfoVC

- (id)initWithPositionModel:(IXPositionM *)positionModel
{
    if (self = [self init]) {
        weakself;
        _positionModel = [[IXPositionM alloc] init];
        _positionModel.symbolModel = positionModel.symbolModel;
        _positionModel.position = positionModel.position;
        _openModel = [[IXOpenModel alloc] initWithSymbolModel:positionModel.symbolModel
                                               WithPriceRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetPriceRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               } WithProfitRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetProfitRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               } WithLossRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetStopRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               }];
        _openModel.orderDir = _positionModel.position.direction;
        _openModel.quoteModel = _positionModel.quoteModel;
        _openModel.orderType = MARKETORDER;
        _openModel.positionVolumes = _positionModel.position.volume;
        _positionModel.quoteModel = positionModel.quoteModel;
        if (!isnan([_positionModel.stoploss doubleValue]) && [positionModel.stoploss doubleValue]) {
            _positionModel.stoploss = positionModel.stoploss;
        }
        if (!isnan([_positionModel.takeprofit doubleValue]) && [positionModel.takeprofit doubleValue]) {
            _positionModel.takeprofit = positionModel.takeprofit;
        }
        _positionModel.symbolModel.marketID = positionModel.marketId;
        _positionModel.showVolume = [NSDecimalNumber decimalNumberWithString:[IXDataProcessTools showCurrentVolume:_positionModel.position.volume contractSizeNew:_positionModel.symbolModel.contractSizeNew
volDigit:_positionModel.symbolModel.volDigits]];
//        if (_isVolNum || 0 == _positionModel.symbolModel.contractSizeNew) {
//            _positionModel.showVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:[NSString stringWithFormat:@"%f",_positionModel.position.volume] WithDigits:_positionModel.symbolModel.volDigits]];
//        } else {
//            _positionModel.showVolume = [NSDecimalNumber decimalNumberWithString:
//                                         [NSString stringWithFormat:@"%.2lf",_positionModel.position.volume/_positionModel.symbolModel.contractSizeNew]];
//        }
    }
    return self;
}

- (id)init
{
    self = [super init];
    if ( self ) {
        IXTradeData_listen_regist(self, PB_CMD_POSITION_UPDATE);
        _isVolNum = ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount );
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_POSITION_UPDATE);
}

- (NSMutableArray *)showVolArr
{
    if ( !_showVolArr ) {
        _showVolArr = [[NSMutableArray array] mutableCopy];
        _volArr = [[NSMutableArray array] mutableCopy];
    }else{
        [_showVolArr removeAllObjects];
        [_volArr removeAllObjects];
    }
    
    double min = _positionModel.symbolModel.volumesMin;
    double max = _positionModel.position.volume;
    //如果持仓数少于最少步长
    if (min >= _positionModel.position.volume) {
        min = _positionModel.position.volume;
        if ([_positionModel.requestVolume doubleValue] != min) {
            [_volArr addObject:[NSString stringWithFormat:@"%.lf",min]];
            if ( _isVolNum || ( 0 == _positionModel.symbolModel.contractSizeNew ) ) {
                [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",min] WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits]];
            }else{
                [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",min/_positionModel.symbolModel.contractSizeNew] withDigits:2]];
            }
        }
        return _showVolArr;
    }
    if ([_positionModel.requestVolume doubleValue] != min) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",min]];
        if ( _isVolNum || ( 0 == _positionModel.symbolModel.contractSizeNew ) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",min] WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits]];
        }else{
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",min/_positionModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    if ( max > min * 10 && [_positionModel.requestVolume doubleValue] != (min * 10) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 10)]];
        if ( _isVolNum || ( 0 == _positionModel.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 10)] WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits]];
        }else if( 0 != _positionModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 10)/_positionModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    
    if ( max > min * 100 && [_positionModel.requestVolume doubleValue] != (min * 100) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 100)]];
        if ( _isVolNum || ( 0 == _positionModel.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 100)] WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits]];
        }else if( 0 != _positionModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 100)/_positionModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    
    if ( max > min * 500 && [_positionModel.requestVolume doubleValue] != (min * 500) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 500)]];
        if ( _isVolNum || ( 0 == _positionModel.symbolModel.contractSizeNew)  ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 500)] WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits]];
        }else if( 0 != _positionModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 500)/_positionModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    if ( [_positionModel.requestVolume doubleValue] != max ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",max]];
        if ( _isVolNum || 0 == _positionModel.symbolModel.contractSizeNew ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",max] WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits]];
        }else if( 0 != _positionModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",max/_positionModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    return _showVolArr;
}

- (IXPullDownMenu *)menu
{
    if (!_menu) {
        _menu = [IXPullDownMenu menuWithMenuItems:@[]
                                        rowHeight:30
                                            width:100];
        weakself;
        _menu.itemClicked = ^(NSInteger index ,NSString * title) {
            [weakSelf menuItemClicked:index title:title];
        };
    }
    
    return _menu;
}

#pragma mark -
#pragma mark - menu
- (void)showMenu:(UIView *)view WithItem:(NSArray *)arr
{
    [self.view endEditing:YES];
    //如果没有元素就不弹框
    if (arr.count == 0) {
        return;
    }
    [self.menu showMenuFrom:view items:arr offsetY:-4];
}

- (void)menuItemClicked:(NSInteger)index title:(NSString *)title
{
    if ( _quickChooseType == 0 ) {
        //更新价格
    }else{
        //更新手数
        IXPositionPrcCell *vol = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
        if ( [vol isKindOfClass:[IXPositionPrcCell class]] ) {
            vol.inputTF.text = _showVolArr[index];
            [self dealValueWithTag:ROWNAMEVOLUME WithValue:_showVolArr[index]];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _parentId = -1;
    
    self.fd_interactivePopDisabled = YES;
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    if (_isVolNum) {
        _titleArr = @[LocalizedString(@"方向"),
                      LocalizedString(@"数量"),
                      LocalizedString(@"开仓价格"),
                      LocalizedString(@"开仓日期"),
                      LocalizedString(@"交易号"),
                      LocalizedString(@"利息"),
                      LocalizedString(@"佣金"),
                      LocalizedString(@"占用保证金")];
    } else {
        _titleArr = @[LocalizedString(@"方向"),
                      LocalizedString(@"手数"),
                      LocalizedString(@"开仓价格"),
                      LocalizedString(@"开仓日期"),
                      LocalizedString(@"交易号"),
                      LocalizedString(@"利息"),
                      LocalizedString(@"佣金"),
                      LocalizedString(@"占用保证金")];
    }
    
    _isFold = YES;//默认展开仓位详情
    _positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:[NSString stringWithFormat:@"%f",_positionModel.position.volume] WithDigits:_positionModel.symbolModel.volDigits]];
    _positionModel.positionDir = _positionModel.position.direction;
    NSDictionary *marketDic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:_positionModel.symbolModel.id_p];
    if (marketDic && marketDic.count > 0) {
        _positionModel.marketId = [marketDic[kMarketId] intValue];
        _positionModel.symbolModel.marketID = [marketDic[kMarketId] intValue];
    }
    if ( _positionModel.symbolModel.contractSizeNew ) {
        _volDigits = [IXDataProcessTools volumeDigits:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",_positionModel.position.volume/_positionModel.symbolModel.contractSizeNew]]];
    }
    if (_volDigits < 2) {
        _volDigits = 2;
    }
    [self resetMarginAsset];
    [self.view addSubview:self.tableV];
    [self.view addSubview:self.btomV];
    [self updateNavBottomLine];
    
}

- (void)leftBtnItemClicked
{
    [self resetSub];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    [self resetSub];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self resetDeepPrice];
    [IXPositionQuoteM subscribeDynamicPrice:self];
#if DEBUG
    [IXPositionCalM addPrcTipInfo:self];
#else
    self.title = LocalizedString(@"仓位详情");
#endif
}

- (void)resetSub
{
    [IXPositionQuoteM cancelDynamicPrice:self];
    if (_openModel.quoteModel) {
        [[IXQuoteDataCache shareInstance] saveOneDynQuote:_openModel.quoteModel];
    }
    [IXPositionCalM removeTip:self.tipView];
#if DEBUG
    [IXPositionCalM removePrcTipInfo:self];
#endif
}

#pragma mark load view module
- (IXOpenTipV *)tipView
{
    if ( !_tipView ) {
        _tipView = [[IXOpenTipV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 40)];
    }
    return _tipView;
}

- (NSString *)getVolume:(double)value
{
    NSString *volume = [NSString stringWithFormat:@"%.0f",value];
    return volume;
    
}

- (NSString *)formatterPrice:(double)price
{
    return [NSString formatterPrice:[NSString stringWithFormat:@"%.0f",price]
                         WithDigits:_positionModel.symbolModel.digits];
}

- (NSString *)formatterStringPrice:(NSString *)price
{
    return [NSString formatterPrice:price
                         WithDigits:_positionModel.symbolModel.digits];
}

- (NSString *)getPricePoint
{
//    NSString *price = [NSString stringWithFormat:@"%lf",pow( 10, -_positionModel.symbolModel.digits) * _positionModel.symbolModel.pipsRatio];
    NSString *price = [NSString stringWithFormat:@"%lf",pow( 10, -_positionModel.symbolModel.digits)];
    return [NSString formatterPrice:price WithDigits:_positionModel.symbolModel.digits];
}

#pragma mark -
#pragma mark - lazy load

- (IXTouchTableV *)tableV{
    if (!_tableV){
        CGSize  size = [IXBtomBtnV targetSize];
        _tableV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  kScreenWidth,
                                                                  kScreenHeight - kNavbarHeight - size.height)];
        _tableV.showsVerticalScrollIndicator = NO;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV  registerClass:[IXPositionInfoCell class]
         forCellReuseIdentifier:NSStringFromClass([IXPositionInfoCell class])];
        [_tableV registerClass:[IXDetailSymbolCell class] forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        [_tableV registerClass:[IXPositionOpenCell class] forCellReuseIdentifier:NSStringFromClass([IXPositionOpenCell class])];
        [_tableV registerClass:[IXPositionInfoCellA class] forCellReuseIdentifier:NSStringFromClass([IXPositionInfoCellA class])];
        [_tableV registerClass:[IXPositionPrcCell class]
        forCellReuseIdentifier:NSStringFromClass([IXPositionPrcCell class])];
        [_tableV registerClass:[IXOpenEptPrcCell class]
        forCellReuseIdentifier:NSStringFromClass([IXOpenEptPrcCell class])];
        [_tableV registerClass:[IXOpenShowSLCell class]
        forCellReuseIdentifier:NSStringFromClass([IXOpenShowSLCell class])];
        [_tableV registerClass:[IXOpenSLCell class]
        forCellReuseIdentifier:NSStringFromClass([IXOpenSLCell class])];
    }
    return _tableV;
}

- (IXBtomBtnV *)btomV {
    if (!_btomV) {
        CGSize  size = [IXBtomBtnV targetSize];
        _btomV = [[IXBtomBtnV alloc] initWithFrame:CGRectMake(0, kScreenHeight - kNavbarHeight - size.height, size.width, size.height)];
        
        [_btomV.btn addTarget:self action:@selector(closePositionBtn) forControlEvents:UIControlEventTouchUpInside];
        [_btomV.btn setTitle:LocalizedString(@"平 仓") forState:UIControlStateNormal];
    }
    IXSymbolTradeState state = [IXDataProcessTools symbolModelIsCanTrade:_positionModel.symbolModel];

    if ([IXDataProcessTools isNormalAddOptiosByState:state]) {
        _btomV.btn.userInteractionEnabled = YES;
        [_btomV.btn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
    } else {
        _btomV.btn.userInteractionEnabled = NO;
        _btomV.btn.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
    }
    return _btomV;
}

- (NSInteger)checkParentId
{
    if ( _parentId <  0 ) {
        _parentId = [IXDataProcessTools queryParentIdBySymbolId:_positionModel.symbolModel.id_p];
    }
    return _parentId;
}

- (void)showRefreashPrc
{
    NSString *symId = [NSString stringWithFormat:@"%llu",_positionModel.symbolModel.id_p];
    id value = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symId];
    if ( value && [value isKindOfClass:[NSDictionary class]] ) {
        for ( NSString *key in [value allKeys] ) {
            
            NSDictionary *curDic = [(NSDictionary *)value objectForKey:key];
            
            if ( [curDic objectForKey:SYMBOLID] ) {
                
                if ( [key isEqualToString:BASEMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    if ( quote.SellPrc.count > 0 && quote.BuyPrc.count > 0 ) {
                        
                        baseLbl.text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                        sModel.languageName,
                                        quote.SellPrc[0],
                                        quote.BuyPrc[0]];
                    }
                    
                }else if ( [key isEqualToString:PROFITMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    if ( quote.SellPrc.count > 0 && quote.BuyPrc.count > 0 ) {
                        
                        profitLbl.text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                          sModel.languageName,
                                          quote.SellPrc[0],
                                          quote.BuyPrc[0]];
                    }
                    
                }else if ( [key isEqualToString:POSITIONMODEL] ) {
                    
                    if ( _positionModel.quoteModel.BuyPrc.count > 0 && _positionModel.quoteModel.SellPrc.count > 0 ) {
                        posLbl.text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                       _positionModel.symbolModel.name,
                                       _positionModel.quoteModel.SellPrc[0],
                                       _positionModel.quoteModel.BuyPrc[0]];
                    }
                    
                }
            }
        }
    }else{
        if ( _positionModel.quoteModel.BuyPrc.count > 0 && _positionModel.quoteModel.SellPrc.count > 0 ) {
            
            posLbl.text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                           _positionModel.symbolModel.name,
                           _positionModel.quoteModel.SellPrc[0],
                           _positionModel.quoteModel.BuyPrc[0]];
        }
    }
}

//刷新预计成交价
//市价单，预计成交价和手数和数量有关，根据深度价格做对应的运算
- (void)resetDeepPrice
{
    item_order_edirection dir;
    if (_positionModel.positionDir == item_position_edirection_DirectionBuy) {
        dir = item_order_edirection_DirectionSell;
    } else {
        dir = item_order_edirection_DirectionBuy;
    }
    NSDecimalNumber *price = [IXRateCaculate caculateMayDealPriceWithVolume:[_positionModel.requestVolume longValue]
                                                                    WithDir:dir
                                                                  WithQuote:_positionModel.quoteModel
                                                                 WithDigits:_positionModel.symbolModel.digits];
    
    NSString *priceStr = [NSString formatterPrice:[price stringValue]
                                       WithDigits:_positionModel.symbolModel.digits];
    _positionModel.requestPrice = [NSDecimalNumber decimalNumberWithString:priceStr];
    _openModel.dealPrice =  [NSDecimalNumber decimalNumberWithString:priceStr];
    
    UITableViewCell *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if( [cell isKindOfClass:[IXOpenEptPrcCell class]] ){
        if ( self.openModel.dealPrice ) {
            [(IXOpenEptPrcCell *)cell setExpectPrice:[_openModel.dealPrice stringValue]];
        }
    }
}

#pragma mark - UITextField代理
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // 获取到父类cell
    UITableViewCell *cell = (UITableViewCell *) [[textField superview] superview];
    CGFloat offsetY = CGRectGetMaxY(cell.frame);
    CGFloat animationY = CGRectGetHeight(self.tableV.frame) - offsetY - 257;
    if (  animationY < 0 ) {
        if ( offsetY - CGRectGetHeight(self.tableV.frame) > 0 ) {
            [self.tableV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_openModel.showSLCount inSection:2]
                               atScrollPosition:UITableViewScrollPositionNone
                                       animated:NO];
        }
        // 执行动画(移动到输入的位置)
        [self.tableV setContentOffset:CGPointMake(0, -animationY) animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == ROWNAMEVOLUME) {
        [self resetInputContent:textField];
    }
    [self dealValueWithTag:textField.tag WithValue:textField.text];
    [self.tableV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                       atScrollPosition:UITableViewScrollPositionNone
                               animated:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag == ROWNAMEPROFIT ||
       textField.tag == ROWNAMELOSS){
        BOOL hidnErase = ([textField.text length] == 1 && [string length] == 0);
        IXOpenSLCell *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(textField.tag - 2) inSection:2]];
        cell.hiddenErase = hidnErase;
    }
    return YES;
}

- (void)resetInputContent:(UITextField *)textField
{
    NSString *str = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    if (_isVolNum) {
        textField.text  = [NSString thousandFormate:[NSString formatterPrice:str WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits];
    } else {
        textField.text  = [NSString thousandFormate:[NSString formatterPrice:str WithDigits:2] withDigits:2];
    }
}

#pragma mark -
#pragma mark - UITableViewDelegate&&DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:{
            if (_isFold) {
                return self.titleArr.count + 3;
            } else {
                return 3;
            }
        }
            break;
        case 1:{
            return 2;
        }
            break;
        default:{
            return 1 + _openModel.showSLCount;
        }
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            if (indexPath.row == 0) {
                return  73;
            } else if (indexPath.row == 1) {
                return 33;
            } else {
                if (_isFold) {
                    if (indexPath.row == 10) {
                        return 30;
                    } else {
                        return 29;
                    }
                } else {
                    return 30;
                }
            }
        }
            break;
        default:{
            return 44;
        }
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            switch (indexPath.row) {
                case 0:{
                    IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.tradeState = [IXDataProcessTools symbolModelIsCanTrade:_positionModel.symbolModel];
                    cell.symbolModel = _positionModel.symbolModel;
                    cell.marketId = _positionModel.symbolModel.marketID;
                    cell.nLastClosePrice = _openModel.nLastClosePrice;
                    if ( _positionModel.quoteModel ) {
                        cell.quoteModel = _positionModel.quoteModel;
                    }
                    return cell;
                }
                    break;
                case 1:{
                    IXPositionInfoCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionInfoCellA class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell config:_positionModel];
                    [cell refreshProfit:_positionModel.nTakeProfit];
                    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
                    return cell;
                }
                    break;
                default: {
                    if (_isFold) {
                        if (indexPath.row == 10) {
                            IXPositionOpenCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionOpenCell class])];
                            cell.selectionStyle = UITableViewCellSelectionStyleNone;
                            [cell reloadPositionUI:_isFold];
                            return cell;
                        } else {
                            IXPositionInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionInfoCell class])];
                            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                            NSString *posId = [NSString stringWithFormat:@"%llu",_positionModel.position.id_p];
//                            NSNumber *posProfit = [[IXAccountBalanceModel shareInstance].profitDic objectForKey:posId][0];
                            PFTModel *model = [[IXAccountBalanceModel shareInstance] caculateProfit:_positionModel.position];
                            _positionModel.nTakeProfit = model.profit;
                            if (_positionModel.quoteModel) {
                                if (_positionModel.positionDir == item_position_edirection_DirectionBuy) {
                                    if (_positionModel.quoteModel.SellPrc.count > 0) {
                                        _positionModel.dealPrice = _positionModel.quoteModel.SellPrc[0];
                                    }
                                } else {
                                    if (_positionModel.quoteModel.BuyPrc.count > 0) {
                                        _positionModel.dealPrice = _positionModel.quoteModel.BuyPrc[0];
                                    }
                                }
                            }
                            [cell config:_positionModel title:_titleArr[(indexPath.row - 2)] indexPathRow:(indexPath.row - 2)];
                            cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
                            return cell;
                        }
                    } else {
                        IXPositionOpenCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionOpenCell class])];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        [cell reloadPositionUI:_isFold];
                        return cell;
                    }
                }
                    break;
            }
        }
            break;
        case 1:{
            return [self sectionSecond:tableView WithIndexPath:indexPath];
        }
            break;
        default:{
            return [self sectionThird:tableView WithIndexPath:indexPath];
        }
            break;
    }
}

- (UITableViewCell *)sectionSecond:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.row ) {
        case 0:{
            IXOpenEptPrcCell *cell = [IXOpenCellM configOpenEptPrcCellWithTableView:tableView
                                                                          WithIndex:indexPath];
            cell.contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.title = LocalizedString(@"预计成交价");
            if ( _openModel.dealPrice && !isnan([_openModel.dealPrice floatValue]) ) {
                cell.expectPrice = [_openModel.dealPrice stringValue];
            }
            cell.expectDealPrcLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4b5667);
            return cell;
        }
            break;
        default :{
            IXPositionPrcCell *cell = [IXOpenCellM configPositionPrcCellWithTableView:tableView
                                                                            WithIndex:indexPath];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            if ( _isVolNum ) {
                cell.title = LocalizedString(@"数量");
            }else{
                cell.title = LocalizedString(@"手数");
            }
            
            if (_positionModel.symbolModel.unitLanName && _positionModel.symbolModel.unitLanName.length) {
                cell.inputExplain = [NSString stringWithFormat:@"(%@)",[IXDataProcessTools showCurrentUnitLanName:_positionModel.symbolModel.unitLanName]];
            } else {
                CGRect frame = cell.inputTF.frame;
                frame.origin.y = 7;
                cell.inputTF.frame = frame;
            }
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMEVOLUME;
            [self refreshLotCell:cell];
            if (self.openModel.showSLCount == 2) {
                cell.inputTF.dk_textColorPicker = DKColorWithRGBs(0x8395a4, 0x4b5667);
            } else {
                cell.inputTF.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
            }
            
            if( _positionModel.showVolume && !isnan([_positionModel.showVolume floatValue]) ){
                if ( _isVolNum ) {
                    cell.inputContent = [NSString thousandFormate:[NSString formatterPrice:[_positionModel.showVolume stringValue] WithDigits:_positionModel.symbolModel.volDigits] withDigits:_positionModel.symbolModel.volDigits];
                }else{
                    cell.inputContent = [NSString thousandFormate:[NSString formatterPrice:[_positionModel.showVolume stringValue] WithDigits:_volDigits] withDigits:_volDigits];
                }
            }
            
            weakself;
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMEVOLUME];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMEVOLUME];
            };
            
            __block IXPositionPrcCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMEVOLUME WithValue:blockCell.inputTF.text];
            };
            
            cell.stepBlock = ^{
                [weakSelf.view endEditing:YES];
                weakSelf.quickChooseType = 1;
                [weakSelf showMenu:blockCell.stepBtn WithItem:weakSelf.showVolArr];
            };
            
            return cell;
        }
            break;
    }
}

#pragma mark 刷新手数UI
- (void)refreshLotCell:(IXPositionPrcCell *)cell
{
    if (self.openModel.showSLCount > 0) {
        cell.subBtn.userInteractionEnabled = NO;
        cell.stepBtn.userInteractionEnabled = NO;
        cell.addBtn.userInteractionEnabled = NO;
        cell.inputTF.userInteractionEnabled = NO;
        cell.inputTF.dk_textColorPicker = DKColorWithRGBs(0x8395a4, 0x4b5667);
        [cell.subBtn dk_setImage:DKImageNames(@"openRoot_btn_substract_n", @"openRoot_btn_substract_n_D") forState:UIControlStateNormal];
        [cell.stepBtn dk_setImage:DKImageNames(@"openRoot_stepPrc_n", @"openRoot_stepPrc_n_D") forState:UIControlStateNormal];
        [cell.addBtn dk_setImage:DKImageNames(@"openRoot_btn_add_n", @"openRoot_btn_add_n_D") forState:UIControlStateNormal];
    } else {
        cell.subBtn.userInteractionEnabled = YES;
        cell.stepBtn.userInteractionEnabled = YES;
        cell.addBtn.userInteractionEnabled = YES;
        cell.inputTF.userInteractionEnabled = YES;
        cell.inputTF.dk_textColorPicker = DKColorWithRGBs(0x8395a4, 0xe9e9ea);
        [cell.subBtn dk_setImage:DKImageNames(@"openRoot_btn_substract", @"openRoot_btn_substract_dark") forState:UIControlStateNormal];
        [cell.stepBtn dk_setImage:DKImageNames(@"openRoot_stepPrc", @"openRoot_stepPrc_D") forState:UIControlStateNormal];
        [cell.addBtn dk_setImage:DKImageNames(@"openRoot_btn_add", @"openRoot_btn_add_dark") forState:UIControlStateNormal];
    }
}

- (UITableViewCell *)sectionThird:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    weakself;
    switch ( indexPath.row ) {
        case 0:{
            IXOpenShowSLCell *cell = [IXOpenCellM configShowSLCellWithTableView:tableView
                                                                      WithIndex:indexPath];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            cell.openSwi = ( _openModel.showSLCount == 2 );
            
            cell.slBlock = ^{
                NSInteger count = weakSelf.openModel.showSLCount;
                count = ( count == 2 ) ? 0 : 2;
                weakSelf.openModel.showSLCount = count;
                weakSelf.positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:[NSString stringWithFormat:@"%f",weakSelf.positionModel.position.volume] WithDigits:weakSelf.positionModel.symbolModel.volDigits]] ;
                [weakSelf updateShowVol];
                [weakSelf.tableV reloadData];
                [weakSelf.view endEditing:YES];
                [weakSelf updateCloseBtn];
            };
            return cell;
        }
            break;
        case 1:{
            IXOpenSLCell *cell = [IXOpenCellM configOpenSLCellWithTableView:tableView
                                                                  WithIndex:indexPath];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            cell.title = LocalizedString(@"止盈");
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMEPROFIT;
            if(_positionModel.takeprofit && !isnan([_positionModel.takeprofit doubleValue]) ){
                cell.inputContent = [NSString formatterPrice:[_positionModel.takeprofit stringValue]
                                                  WithDigits:_positionModel.symbolModel.digits];
                cell.inputExplain = _openModel.profitRangeStr;
            }else{
                cell.inputContent = @"";
                cell.inputExplain = @"";
            }
            
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMEPROFIT];
            };
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMEPROFIT];
            };
            cell.eraseBlock = ^{
                [weakSelf updateEraseWithTag:ROWNAMEPROFIT];
            };
            
            __block IXOpenSLCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMEPROFIT WithValue:blockCell.inputTF.text];
            };
            return cell;
        }
            break;
        default:{
            IXOpenSLCell *cell = [IXOpenCellM configOpenSLCellWithTableView:tableView
                                                                  WithIndex:indexPath];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            cell.title = LocalizedString(@"止损");
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMELOSS;
            if(_positionModel.stoploss && !isnan([_positionModel.stoploss doubleValue])){
                cell.inputContent = [NSString formatterPrice:[_positionModel.stoploss stringValue]
                                                  WithDigits:_positionModel.symbolModel.digits];
                cell.inputExplain = _openModel.stopRangeStr;
            }else{
                cell.inputContent = @"";
                cell.inputExplain = @"";
            }
            
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMELOSS];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMELOSS];
            };
            
            cell.eraseBlock = ^{
                [weakSelf updateEraseWithTag:ROWNAMELOSS];
            };
            
            __block IXOpenSLCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMELOSS WithValue:blockCell.inputTF.text];
            };
            
            return cell;
        }
            break;
    }
}


#pragma 手动管理Cell
- (void)removeSepLine:(UITableViewCell *)cell
{
    UIImageView *sepLine = (UIImageView *)[cell viewWithTag:1000];
    if ( sepLine ) {
        [sepLine removeFromSuperview];
    }
}

- (void)addSepLine:(UITableViewCell *)cell WithFrame:(CGRect)frame
{
    UIImageView *sepLine = [UIImageView addSepImageWithFrame:frame
                                                   WithColor:CellSepLineColor
                                                   WithAlpha:1.f];
    sepLine.tag = 1000;
    [cell.contentView addSubview:sepLine];
}

#pragma mark 用户操作触发的UI刷新
- (void)resetPriceRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    self.openModel.priceRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    
    UITableViewCell *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
    if( [cell isKindOfClass:[IXOpenPrcCell class]] ){
        if ( self.openModel.profitRangeStr ) {
            [(IXOpenPrcCell *)cell setInputExplain:self.openModel.priceRangeStr];
        }
    }
}

- (void)resetProfitRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    self.openModel.profitRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    if ( _openModel.showSLCount > 0 ) {
        IXOpenSLCell *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
        if( _positionModel.takeprofit && !isnan([_positionModel.takeprofit doubleValue]) ){
            cell.inputExplain = _openModel.profitRangeStr;
        }else{
            cell.inputExplain = @"";
        }
    }
}

- (void)resetStopRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    _openModel.stopRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    if ( _openModel.showSLCount > 0 ) {
        IXOpenPrcCell *cell = [_tableV cellForRowAtIndexPath:
                               [NSIndexPath indexPathForRow:2 inSection:2]];
        if(  _positionModel.stoploss && !isnan([_positionModel.stoploss doubleValue]) ){
            cell.inputExplain = _openModel.stopRangeStr;
        }else{
            cell.inputExplain = @"";
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            if (indexPath.row == 0) {
//                IXSymbolDetailVC *openVC = [[IXSymbolDetailVC alloc] init];
//                IXSymbolM *model = _positionModel.symbolModel;
//                IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
//                tradeModel.symbolModel = model;
//                if (_positionModel.quoteModel) {
//                    tradeModel.quoteModel = _positionModel.quoteModel;
//                }
//                openVC.tradeModel = tradeModel;
//                openVC.nLastClosePrice = _openModel.nLastClosePrice;
//                openVC.hidesBottomBarWhenPushed = YES;
//                [self.navigationController pushViewController:openVC animated:YES];
            } else {
                if (!_isFold) {
                    if (indexPath.row == 2) {
                        _isFold = !_isFold;
                        [self.tableV reloadData];
                    }
                } else {
                    if (indexPath.row == 10) {
                        _isFold = !_isFold;
                        [self.tableV reloadData];
                    }
                }
            }
        }
            break;
        default:
            break;
    }
}

- (void)dealValueWithTag:(ROWNAME)tag WithValue:(NSString *)value
{
    switch (tag) {
        case ROWNAMEPRICE:{     //价格
            _positionModel.requestPrice = [NSDecimalNumber decimalNumberWithString:value];
            _openModel.dealPrice = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        case ROWNAMEVOLUME:{    //手数
            value = [value stringByReplacingOccurrencesOfString:@"," withString:@""];
            _positionModel.showVolume = [NSDecimalNumber decimalNumberWithString:value];
            if ( _isVolNum || 0 == _positionModel.symbolModel.contractSizeNew) {
                _positionModel.showVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:_positionModel.symbolModel.volDigits]];
                _positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:_positionModel.symbolModel.volDigits]];
            }else{
                _positionModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                                [NSString stringWithFormat:@"%f",[value doubleValue] * _positionModel.symbolModel.contractSizeNew]];
                _positionModel.showVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:_volDigits]];
            }
            [self resetDeepPrice];
        }
            break;
        case ROWNAMEPROFIT:{    //止赢
            _positionModel.takeprofit = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        case ROWNAMELOSS:{      //止损
            _positionModel.stoploss = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        default:
            break;
    }
    [IXPositionCalM updateTipInfoWithView:self.tipView WithRoot:self];
}

- (void)updateCloseBtn
{
    if (self.openModel.showSLCount != 0) {
        [_btomV.btn setTitle:LocalizedString(@"修改仓位") forState:UIControlStateNormal];
    } else {
        [_btomV.btn setTitle:LocalizedString(@"平 仓") forState:UIControlStateNormal];
    }
}

- (void)updateStoplossWithTag:(ROWNAME)tag
{
    [IXPositionLogicM updateStoplossWithTag:tag WithRoot:self];
    if ( tag == ROWNAMEPRICE ) {
    }else if ( tag == ROWNAMEVOLUME ){
        [self updateShowVol];
        [self resetDeepPrice];
    }
    [IXPositionCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)updateEraseWithTag:(ROWNAME)tag
{
    if(tag == ROWNAMEPROFIT){
        _positionModel.takeprofit = nil;
    }else if (tag == ROWNAMELOSS){
        _positionModel.stoploss = nil;
    }
    [IXPositionCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}


- (void)updateShowVol
{
    if ( _isVolNum || 0 == _positionModel.symbolModel.contractSizeNew ) {
        _positionModel.showVolume = [NSDecimalNumber decimalNumberWithDecimal:
                                     [_positionModel.requestVolume decimalValue]];
    }else{
        _positionModel.showVolume = [_positionModel.requestVolume decimalNumberByDividingBy:
                                     [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",
                                                                               _positionModel.symbolModel.contractSizeNew]]
                                                                               withBehavior:[IXRateCaculate handlerPriceWithDigit:_volDigits]];
    }
}

- (void)updateTakeprofitWithTag:(ROWNAME)tag
{
    [IXPositionLogicM updateTakeprofitWithTag:tag WithRoot:self];
    if ( tag == ROWNAMEPRICE ) {
    }else if ( tag == ROWNAMEVOLUME ){
        [self updateShowVol];
        [self resetDeepPrice];
    }
    [IXPositionCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)reloadContentWithTag:(ROWNAME)tag
{
    NSIndexPath *indexPath;
    switch ( tag) {
        case ROWNAMEPRICE:{
            indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        }
            break;
        case ROWNAMEVOLUME:{
            indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
        }
            break;
        case ROWNAMEPROFIT:{
            indexPath = [NSIndexPath indexPathForRow:1 inSection:2];
        }
            break;
        default:{
            indexPath = [NSIndexPath indexPathForRow:2 inSection:2];
        }
            break;
    }
    [self.tableV reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)showLoadStatus
{
    [self resetSub];
    IXPositionResultVC *VC = [[IXPositionResultVC alloc] init];
    VC.model = _positionModel;
    [self.navigationController pushViewController:VC animated:YES];
    
    proto_order_add *proto = [self packageOrder];
    if (!proto) {
        //暂时
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LocalizedString(@"平仓")
                                                            message:LocalizedString(@"平仓失败")
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        [alertView show];
    }
    [[IXTCPRequest shareInstance] orderWithParam:proto];
}

//平仓
- (proto_order_add *)packageOrder
{
    proto_order_add *proto = [[proto_order_add alloc] init];
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    
    item_order *order = [[item_order alloc] init];
    order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    order.symbol  =  _positionModel.symbolModel.name;
    order.symbolid = _positionModel.symbolModel.id_p;
    order.createTime = [IXEntityFormatter getCurrentTimeInterval];
    order.clientType = [IXUserInfoMgr shareInstance].itemType;
    order.positionid = _positionModel.position.id_p;
    
    if (_positionModel.position.direction == item_position_edirection_DirectionBuy) {
        order.direction = item_position_edirection_DirectionBuy;
        if (_positionModel.quoteModel.SellPrc.count > 0) {
            order.requestPrice = [_positionModel.quoteModel.SellPrc[0] doubleValue];
        } else {
            return nil;
        }
        
    } else {
        order.direction = item_position_edirection_DirectionSell;
        if (_positionModel.quoteModel.BuyPrc.count > 0) {
            order.requestPrice = [_positionModel.quoteModel.BuyPrc[0] doubleValue];
        } else {
            return nil;
        }
    }
    
    order.type = item_order_etype_TypeClose;
    order.requestVolume = [_positionModel.requestVolume doubleValue];
    if ([_positionModel.takeprofit doubleValue] != 0 && !isnan([_positionModel.takeprofit floatValue])) {
        order.takeProfit = [_positionModel.takeprofit doubleValue];
    }
    if ([_positionModel.stoploss doubleValue] != 0 && !isnan([_positionModel.stoploss floatValue])) {
        order.stopLoss = [_positionModel.stoploss doubleValue];
    }
    proto.order = order;
    return proto;
}

#pragma mark 平仓
- (void)closePositionBtn
{
    [self.view endEditing:YES];
    NSString *checkOrderInfo = [IXPositionCalM checkResult:self];;
    if (self.openModel.showSLCount > 0) {
        if ( [checkOrderInfo length] == 0 ) {
            [IXPositionCalM removeTip:self.tipView];
            [self submitOrderInfo];
        }else{
            [IXPositionCalM showTip:checkOrderInfo WithView:self.tipView InView:self.view];
        }
    } else {
        if (checkOrderInfo.length == 0) {
            IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"平仓提醒")
                                                     message:LocalizedString(@"点击\"确定\"将会进行该交易平仓")
                                                 cancelTitle:LocalizedString(@"取消")
                                                  sureTitles:LocalizedString(@"确定.")];
            VC.index = 0;
            VC.expendAbleAlartViewDelegate = self;
            [VC showView];
        } else {
            [IXPositionCalM showTip:checkOrderInfo WithView:self.tipView InView:self.view];
        }
    }
}

#pragma mark 修改仓位
- (void)submitOrderInfo
{
    proto_position_update *proto = [self packageUpdatePosition];
    [[IXTCPRequest shareInstance] updatePositionWithParam:proto];
}

- (proto_position_update *)packageUpdatePosition
{
    proto_position_update *proto = [[proto_position_update alloc] init];
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    item_position *position = [[item_position alloc] init];
    position.id_p = _positionModel.position.id_p;
    position.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    double takeprofit = [_positionModel.takeprofit doubleValue];
    double stoploss = [_positionModel.stoploss doubleValue];
    if (takeprofit != 0 && !isnan(takeprofit)) {
        position.takeProfit = takeprofit;
    }
    if (stoploss != 0 && !isnan(stoploss)) {
        position.stopLoss = stoploss;
    }
    proto.position = position;
    
    return proto;
}

#pragma mark -
#pragma mark - 行情数据
- (void)needRefresh
{
    NSMutableArray *arr = [IXPositionQuoteM subscribeDynamicPrice:self];
    if ( arr ) {
        [self didResponseQuoteDistribute:arr cmd:CMD_QUOTE_PUB_DETAIL];
    }
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if( cmd == CMD_QUOTE_PUB_DETAIL || cmd == CMD_QUOTE_SUB_DETAIL ){
        for ( IXQuoteM *model in arr ) {
            if (model.symbolId == _positionModel.symbolModel.id_p) {
                _positionModel.quoteModel = [model mutableCopy];
                _openModel.quoteModel = [model mutableCopy];
                
                [self resetQuote];
                [self resetEptPrice];
                [self resetMarginAsset];
                break;
            }
        }
    } else if ( cmd == CMD_QUOTE_CLOSE_PRICE ){
        for ( IXLastQuoteM *model in arr ) {
            if ( model.symbolId == _positionModel.symbolModel.id_p ) {
                _openModel.nLastClosePrice = model.nLastClosePrice;
                for ( UITableViewCell *cell in [_tableV visibleCells] ) {
                    
                    if ( [cell isKindOfClass:[IXDetailSymbolCell class]] ) {
                        
                        ((IXDetailSymbolCell *)cell).nLastClosePrice = _openModel.nLastClosePrice;
                        break;
                    }
                }
                break;
            }
        }
    }
}

//刷新UI显示行情的cell
- (void)resetQuote
{
    for ( UITableViewCell *cell  in [_tableV visibleCells] ) {
        if ( [cell isKindOfClass:[IXDetailSymbolCell class]] ) {
            [(IXDetailSymbolCell *)cell setQuoteModel:_positionModel.quoteModel];
        }else if ( [cell isKindOfClass:[IXSymbolDeepCell class]] ){
            NSInteger tag = cell.tag;
            [(IXSymbolDeepCell *)cell setBuyPrc:[self formatterStringPrice:_positionModel.quoteModel.BuyPrc[tag]]
                                         BuyVol:[self formatterStringPrice:_positionModel.quoteModel.BuyVol[tag]]
                                        SellPrc:[self formatterStringPrice:_positionModel.quoteModel.SellPrc[tag]]
                                        SellVol:[self formatterStringPrice:_positionModel.quoteModel.SellVol[tag]]];
        }else if ([cell isKindOfClass:[IXPositionInfoCellA class]]) {
//            NSString *posId = [NSString stringWithFormat:@"%llu",_positionModel.position.id_p];
//            NSNumber *posProfit = [[IXAccountBalanceModel shareInstance].profitDic objectForKey:posId][0];
            PFTModel *model = [[IXAccountBalanceModel shareInstance] caculateProfit:_positionModel.position];
            _positionModel.nTakeProfit = model.profit;
            [(IXPositionInfoCellA *)cell refreshProfit:_positionModel.nTakeProfit];
        }
    }
}

//刷新价格范围
- (void)resetEptPrice
{
    [self resetDeepPrice];
    [IXPositionCalM updateTipInfoWithView:self.tipView WithRoot:self];
}

//刷新保证金和佣金,百万分之一
- (void)resetMarginAsset
{
    NSArray *arr = [IXPositionCalM resetMarginAssetWithRoot:self];
    if ( arr && _isFold) {
        _positionModel.margin = [arr[0] doubleValue];
        IXPositionInfoCell *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        if ([cell isKindOfClass:[IXPositionInfoCell class]]) {
            [(IXPositionInfoCell *)cell refreshMargin:_positionModel.margin];
        }
    }
}

//设置计算止盈止损范围的价格
//市价单：止盈止损的参考价格和顶层价有关
//限价单：止盈止损的参考价格和用户输入的价格有关
- (void)resetRequestPrice
{
    _openModel.requestPrice = _positionModel.requestPrice;
}

#pragma mark 持仓更新的监听
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (_openModel.showSLCount > 0 ) {
        if ( [keyPath isEqualToString:PB_CMD_POSITION_UPDATE] ){
            proto_position_update *pb = ((IXTradeDataCache *)object).pb_position_update;
            if (pb.result == 0) {
                IXAlertVC *VC = nil;
                if (pb.position.status == item_position_estatus_StatusOpened) {
                    VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"修改仓位成功")
                                                  message:LocalizedString(@"您的仓位已修改成功")
                                              cancelTitle:nil
                                               sureTitles:LocalizedString(@"确定")];
                } else if (pb.position.status == item_position_estatus_StatusClosed) {
                    VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"平仓成功")
                                                  message:LocalizedString(@"您的仓位已平仓成功")
                                              cancelTitle:nil
                                               sureTitles:LocalizedString(@"确定")];
                } else {
                    return;
                }
                VC.index = 1;
                VC.expendAbleAlartViewDelegate = self;
                [VC showView];
            } else {
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@(%@)",LocalizedString(@"失败"),[IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%d",pb.result]]]];
            }
        }
    }
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (alertView.tag == 0) {
        if (btnIndex == 0) {
        } else if (btnIndex == 1) {
            [self showLoadStatus];
        }
    } else if (alertView.tag == 1) {
        [self resetSub];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    return YES;
}

- (NSDecimalNumberHandler *)handlerPriceWithDigit:(NSInteger)digit
{
    NSDecimalNumberHandler *roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:digit
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
    
    return roundingBehavior;
}

@end
