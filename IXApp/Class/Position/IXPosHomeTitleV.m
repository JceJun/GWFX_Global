//
//  IXPosHomeTitleV.m
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPosHomeTitleV.h"

@interface IXPosHomeTitleV ()

@property (nonatomic, strong)UIButton   *posBtn;
@property (nonatomic, strong)UIButton   *orderBtn;
@property (nonatomic, strong)UIButton   *dealBtn;
@property (nonatomic, strong)UIView     *posV;  //滑块

@end

@implementation IXPosHomeTitleV

- (instancetype)init
{
    if (self = [super init]) {
        [self createSubV];
    }
    return self;
}

- (void)createSubV
{
    NSString    * str = LocalizedString(@"挂单");
    CGSize  size = [str sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(12)}];
    CGFloat width = MIN(size.width + 20, (kScreenWidth - 170)/3);
    
    self.frame = CGRectMake(0, 0, width*3, 44);
    
    //持仓
    _posBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, 44)];
    _posBtn.selected = YES;
    _posBtn.titleLabel.font = PF_MEDI(12);
    [_posBtn setTitle:LocalizedString(@"持仓") forState:UIControlStateNormal];
    [_posBtn addTarget:self action:@selector(posAction:) forControlEvents:UIControlEventTouchUpInside];
    [_posBtn dk_setTitleColorPicker:DKColorWithRGBs(0x232935, 0xe9e9ea) forState:UIControlStateSelected];
    [_posBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    [self addSubview:_posBtn];
    
    //挂单
    _orderBtn = [[UIButton alloc] initWithFrame:CGRectMake(width+5, 0, width+10, 44)];
    _orderBtn.titleLabel.font = PF_MEDI(12);
    [_orderBtn setTitle:LocalizedString(@"挂单") forState:UIControlStateNormal];
    [_orderBtn addTarget:self action:@selector(orderAction:) forControlEvents:UIControlEventTouchUpInside];
    [_orderBtn dk_setTitleColorPicker:DKColorWithRGBs(0x232935, 0xe9e9ea) forState:UIControlStateSelected];
    [_orderBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    [self addSubview:_orderBtn];
    
    //历史
    _dealBtn = [[UIButton alloc] initWithFrame:CGRectMake(width*2+20, 0, width, 44)];
    _dealBtn.titleLabel.font = PF_MEDI(12);
    [_dealBtn setTitle:LocalizedString(@"历史") forState:UIControlStateNormal];
    [_dealBtn addTarget:self action:@selector(dealAction:) forControlEvents:UIControlEventTouchUpInside];
    [_dealBtn dk_setTitleColorPicker:DKColorWithRGBs(0x232935, 0xe9e9ea) forState:UIControlStateSelected];
    [_dealBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    [self addSubview:_dealBtn];
    
    _posV = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMidX(_posBtn.frame) - size.width/2 + 3,
                                                     40,
                                                     size.width - 6,
                                                     3)];
    _posV.dk_backgroundColorPicker = DKColorWithRGBs(0x232935, 0xe9e9ea);
    [self addSubview:_posV];
}

- (void)posAction:(UIButton *)btn
{
    btn.selected = YES;
    _orderBtn.selected = NO;
    _dealBtn.selected = NO;
    if (self.itemClicked) {
        self.itemClicked(0);
    }
    
    CGFloat width = _posV.frame.size.width;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _posV.frame = CGRectMake(CGRectGetMidX(btn.frame) - width/2, 40, width, 3);
    } completion:nil];
}

- (void)orderAction:(UIButton *)btn
{
    btn.selected = YES;
    _posBtn.selected =  NO;
    _dealBtn.selected = NO;
    if (self.itemClicked) {
        self.itemClicked(1);
    }
    
    CGFloat width = _posV.frame.size.width;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _posV.frame = CGRectMake(CGRectGetMidX(btn.frame) - width/2, 40, width, 3);
    } completion:nil];
}

- (void)dealAction:(UIButton *)btn
{
    btn.selected = YES;
    _posBtn.selected = NO;
    _orderBtn.selected = NO;
    if (self.itemClicked) {
        self.itemClicked(2);
    }
    
    CGFloat width = _posV.frame.size.width;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _posV.frame = CGRectMake(CGRectGetMidX(btn.frame) - width/2, 40, width, 3);
    } completion:nil];
}

@end
