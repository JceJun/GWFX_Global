//
//  IXDealListVC.m
//  IXApp
//
//  Created by Evn on 16/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDealListVC.h"
#import "IXDealListCell.h"
#import "IXDealM.h"
#import "IXDBSymbolMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBGlobal.h"
#import "IXDealListInfoVC.h"
#import "IXPosDefultView.h"
#import "IxProtoDeal.pbobjc.h"
#import "IXUserDefaultM.h"

#import "IXPositionSymModel.h"

@interface IXDealListVC ()<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic,retain)UITableView *tableV;
@property (nonatomic, strong) IXPosDefultView *posDefView;

@property (nonatomic, strong) NSMutableArray *dealAry;
@property (nonatomic, strong) NSMutableArray *dataArr;//订单model

@end

@implementation IXDealListVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_CHANGE);
        IXTradeData_listen_regist(self, CMD_TIMEZONE_UPDATE);
        
        IXTradeData_listen_regist(self, PB_CMD_DEAL_LIST);
        IXTradeData_listen_regist(self, PB_CMD_DEAL_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_DEAL_ADD);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDealUnit) name:kNotificationUnit object:nil];
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_CHANGE);
    IXTradeData_listen_resign(self, CMD_TIMEZONE_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_DEAL_LIST);
    IXTradeData_listen_resign(self, PB_CMD_DEAL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_DEAL_ADD);
}

- (void)refreshDealUnit
{
    [self.tableV reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refreshData];
    [self tableV];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)refreshData
{
    _dealAry = [IXTradeDataCache shareInstance].pb_cache_deal_list;
    [self filterIncomeData];
    [self setDealData];
}

//过滤出入金以及行情订阅历史
- (void)filterIncomeData {
    
    if (_dealAry && _dealAry.count > 0) {
        
        NSMutableArray *retArr = [[NSMutableArray alloc] init];
        for (int i = 0; i < _dealAry.count; i++) {
            
            item_deal *deal = _dealAry[i];
            if (deal && deal.proposalType == 0 && deal.symbolid > 0) {
                
                [retArr addObject:deal];
            }
        }
        _dealAry = [retArr mutableCopy];
    }
}
#warning 排序可以优化
- (void)setDealData
{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    } else {
        [_dataArr removeAllObjects];
    }
    if (_dealAry.count > 0) {
        for (int i = 0; i < _dealAry.count; i++) {
            IXDealM *model = [[IXDealM alloc] init];
            item_deal *deal = [[item_deal alloc] init];
            deal = _dealAry[i];
            model.deal = deal;
            IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:deal.symbolid];
            if (symModel) {
                model.symbolModel = symModel;
                [_dataArr addObject:model];
            }
        }
    }
    [self reloadTableViewData];
}

#pragma mark 刷新list列表
- (void)refreshListData:(NSArray *)listArr {
    if (!listArr || listArr.count == 0) {
        return;
    }
    for (int i = 0; i < listArr.count; i++) {
        IXDealM *model = [[IXDealM alloc] init];
        item_deal *deal = listArr[i];
        model.deal = deal;
        
        BOOL flag = NO;
        for (IXDealM *tmpModel in _dataArr) {
            if (deal.id_p == tmpModel.deal.id_p) {
                flag = YES;
                break;
            }
        }
        if (flag) {
            continue;
        }
        if (deal && deal.proposalType == 0 && deal.symbolid > 0) {
            IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:deal.symbolid];
            if (symModel) {
                model.symbolModel = symModel;
                [_dataArr insertObject:model atIndex:0];
            }
        }
    }
    [self reloadTableViewData];
}

#pragma mark 刷新add列表
- (void)refreshAddData {
    
    _dealAry = [[IXTradeDataCache shareInstance].pb_cache_deal_list mutableCopy];
    if (!_dealAry || _dealAry.count == 0) {
        return;
    }
    if ( !_dataArr ) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    IXDealM *model = [[IXDealM alloc] init];
    item_deal *deal = _dealAry[0];
    model.deal = deal;
    if (deal && deal.proposalType == 0 && deal.symbolid > 0) {
        IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:deal.symbolid];
        if (symModel) {
            model.symbolModel = symModel;
            [_dataArr insertObject:model atIndex:0];
            [self reloadTableViewData];
        }
    }
}

#pragma mark 刷新update列表
- (void)refreshUpdateData:(proto_deal_update *)pb
{
    if ( !_dataArr || _dataArr.count == 0) {
        return;
    }
    NSInteger index = -1;
    IXDealM *model;
    for (int i = 0; i < _dataArr.count; i++) {
        model = _dataArr[i];
        if (model.deal.id_p == pb.deal.id_p) {
            
            model.deal = pb.deal;
            index = i;
            break;
        }
    }
    if (index == -1) {
        return;
    }
    [_dataArr removeObjectAtIndex:index];
    [self reloadTableViewData];
    
}

#pragma UI刷新列表
- (void)reloadTableViewData {
    
    if (_dataArr.count == 0) {
        
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        
        self.posDefView.imageName = @"newList_noneMsg_avatar";
        self.posDefView.tipMsg = LocalizedString(@"暂无成交历史");
        
        self.tableV.scrollEnabled = NO;
        [self.tableV reloadData];
        return;
    }else{
        
        [self.posDefView removeFromSuperview];
        self.tableV.scrollEnabled = YES;
        [self.tableV reloadData];
    }
}


- (UITableView *)tableV
{
    if(!_tableV){
        CGRect rect = kScreenBound;
        rect.origin.y = 5;
        rect.size.height = kScreenHeight - kTabbarHeight - kNavbarHeight - 5;
        _tableV = [[UITableView alloc] initWithFrame:rect];
        [_tableV registerClass:[IXDealListCell class] forCellReuseIdentifier:NSStringFromClass([IXDealListCell class])];
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0, 80, kScreenWidth, 217)];
    }
    return _posDefView;
}

#pragma mark - UITableViewDataSource&&UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDealListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDealListCell class])];
    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell reloadUIWithDealModel:_dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDealListInfoVC *VC = [[IXDealListInfoVC alloc] init];
    VC.dealModel = _dataArr[indexPath.row];
    VC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath,PB_CMD_DEAL_LIST)) {
        
        proto_deal_list  *pb = ((IXTradeDataCache *)object).pb_deal_list;
        [self refreshListData:pb.dealArray];
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_DEAL_UPDATE)) {
        
        proto_deal_update  *pb = ((IXTradeDataCache *)object).pb_deal_update;
        if (pb.result == 0) {
            
            if (pb.deal && pb.deal.proposalType == 0 && pb.deal.symbolid > 0) {
                
                [self refreshUpdateData:pb];
            }
        }
        
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_DEAL_ADD)) {
        
        proto_deal_add  *pb = ((IXTradeDataCache *)object).pb_deal_add;
        if (pb.result == 0) {
            if (pb.deal && pb.deal.proposalType == 0 && pb.deal.symbolid > 0) {
                [self refreshAddData];
            }
        }
    } else if ([keyPath isEqualToString:CMD_TIMEZONE_UPDATE] ){
        [self reloadTableViewData];
    } else if ([keyPath isEqualToString:PB_CMD_ACCOUNT_CHANGE] ) {
        [_dataArr removeAllObjects];
        [self refreshData];
        [self reloadTableViewData];
    }
}

@end

