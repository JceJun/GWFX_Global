//
//  IXPositonTopV.m
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositonTopV.h"

@interface IXPositonTopV ()

@property (nonatomic, strong) UIButton  * filterBtn;
@property (nonatomic, strong) UIButton  * sortBtn;

@end

@implementation IXPositonTopV

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.dk_backgroundColorPicker = DKNavBarColor;
        [self createSubView];
    }
    return self;
}

- (void)createSubView
{
    NSString    * title = LocalizedString(@"筛选");
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    
    _filterBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 7, (kScreenWidth - 40)/2, 30)];
    _filterBtn.titleLabel.font = PF_MEDI(13);
    _filterBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    [_filterBtn setTitle:title forState:UIControlStateNormal];
    [_filterBtn setTitle:LocalizedString(@"取消筛选") forState:UIControlStateSelected];
    [_filterBtn addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
    [_filterBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateSelected];
    [_filterBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea) forState:UIControlStateNormal];
    [_filterBtn dk_setImage:DKImageNames(@"down_arrow_n", @"down_arrow_n_D") forState:UIControlStateSelected];
    [_filterBtn dk_setImage:DKImageNames(@"down_arrow_s", @"down_arrow_s_D") forState:UIControlStateNormal];
    [_filterBtn setImageEdgeInsets:UIEdgeInsetsMake(10, (kScreenWidth - 40)/4 + size.width/2 + 10,
                                                    10, (kScreenWidth - 40)/4 - size.width/2 - 20)];
    [self addSubview:_filterBtn];
    
    title = LocalizedString(@"排序");
    size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    _sortBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth/2 + 10, 7, (kScreenWidth - 40)/2, 30)];
    _sortBtn.titleLabel.font = PF_MEDI(13);
    [_sortBtn setTitle:title forState:UIControlStateNormal];
    [_sortBtn addTarget:self action:@selector(sortAction:) forControlEvents:UIControlEventTouchUpInside];
    [_sortBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea) forState:UIControlStateNormal];
    [_sortBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea) forState:UIControlStateNormal];
    [_sortBtn dk_setImage:DKImageNames(@"down_arrow_s", @"down_arrow_s_D") forState:UIControlStateNormal];
    [_sortBtn dk_setImage:DKImageNames(@"down_arrow_s", @"down_arrow_s_D") forState:UIControlStateNormal];
    [_sortBtn setImageEdgeInsets:UIEdgeInsetsMake(10, (kScreenWidth - 40)/4 + size.width/2 + 10,
                                                  10, (kScreenWidth - 40)/4 - size.width/2 - 20)];
    [self addSubview:_sortBtn];
    
    UIView  * verLine = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth/2, 7, 0.5, 30)];
    verLine.dk_backgroundColorPicker = DKLineColor;
    [self addSubview:verLine];
    
    UIView  * horiLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 0.5,
                                                                  kScreenWidth, 0.5)];
    horiLine.dk_backgroundColorPicker = DKLineColor;
    [self addSubview:horiLine];
}

#pragma mark -
#pragma mark - btn action

- (void)filterAction:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (btn.selected) {
        NSString    * title = LocalizedString(@"取消筛选");
        CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(10, (kScreenWidth - 40)/4 + size.width/2 + 10,
                                                 10, (kScreenWidth - 40)/4 - size.width/2 - 20)];
        
        if (self.actionB) {
            self.actionB(PosActionTypeFilter);
        }
    } else {
        NSString    * title = LocalizedString(@"筛选");
        CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(10, (kScreenWidth - 40)/4 + size.width/2 + 10,
                                                 10, (kScreenWidth - 40)/4 - size.width/2 - 20)];

        if (self.actionB) {
            self.actionB(PosActionTypeCancelFilter);
        }
    }
}

- (void)sortAction:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (btn.selected && self.actionB) {
        self.actionB(PosActionTypeSort);
    }
    else if (self.actionB){
        self.actionB(PosActionTypeCancelSort);
    }
}

/**
 重置筛选按钮状态为Normnal
 */
- (void)resetFilterBtnState
{
    _filterBtn.selected = NO;
    NSString    * title = LocalizedString(@"筛选");
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    [_filterBtn setImageEdgeInsets:UIEdgeInsetsMake(10, (kScreenWidth - 40)/4 + size.width/2 + 10,
                                                    10, (kScreenWidth - 40)/4 - size.width/2 - 20)];
}

/**
 重置排序按钮状态为Normal
 */
- (void)resetSortBtnState
{
    _sortBtn.selected = NO;
}


@end
