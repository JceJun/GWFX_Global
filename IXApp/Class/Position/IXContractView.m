//
//  IXContractView.m
//  IXApp
//
//  Created by Evn on 17/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXContractView.h"
#import "IXDBGlobal.h"
#import "IXDBScheduleMgr.h"
#import "IXDateUtils.h"

NSString * const DAY_OF_WEEK = @"day_of_week";
NSString * const TRADE_TIME = @"trade_time";
NSString * const START_HOUR = @"start_hour";
NSString * const START_MINUTE = @"start_minute";
NSString * const END_HOUR = @"end_hour";
NSString * const END_MINUTE = @"end_minute";

@interface IXContractView()

@property (nonatomic, strong) UIScrollView *sContractView;
@property (nonatomic, strong) IXSymbolM *symbolModel;
@property (nonatomic, strong) NSMutableArray *baseValueArr;
@property (nonatomic, strong) NSMutableArray *condValueArr;
@property (nonatomic, strong) NSMutableArray *stepArr;
@property (nonatomic, strong) NSMutableArray *timeArr;

@property (nonatomic, assign) int currentTimeZone;

@end

@implementation IXContractView

- (id)initWithFrame:(CGRect)frame
        symbolModel:(IXSymbolM *)model {
    self = [super initWithFrame:frame];
    if ( self ) {
        
        _symbolModel = model;
        [self initData];
        [self addContractPropertieView];
    }
    return self;
}

- (void)initData {
    
    if (!_symbolModel) {
        
        return;
    }
    _baseValueArr = [[NSMutableArray alloc] init];
    _condValueArr = [[NSMutableArray alloc] init];
    _stepArr = [[NSMutableArray alloc] init];
    _timeArr = [[NSMutableArray alloc] init];
    
    [self dealCurrentTimeZone];
    [self dealBaseData];
    [self dealConditionData];
    [self dealRequireData];
    [self dealTimeData];
    
}

- (void)dealCurrentTimeZone
{
    id value =  [[NSUserDefaults standardUserDefaults] objectForKey:IXnTimeZone];
    _currentTimeZone = [value intValue];
}

- (void)dealBaseData {
    
    [_baseValueArr addObject:[IXDataProcessTools dealWithNil:_symbolModel.languageName]];
    [_baseValueArr addObject:@(_symbolModel.contractSize)];
    [_baseValueArr addObject:[IXDataProcessTools dealWithNil:_symbolModel.baseCurrency]];
    [_baseValueArr addObject:[IXDataProcessTools dealWithNil:_symbolModel.profitCurrency]];
    [_baseValueArr addObject:[NSString stringWithFormat:@"%.2f%%(年利率)",_symbolModel.longSwap]];
    [_baseValueArr addObject:[NSString stringWithFormat:@"%.2f%%(年利率)",_symbolModel.shortSwap]];
     [_baseValueArr addObject:(([IXDataProcessTools symbolIsCanTrade:_symbolModel] == IXSymbolTradeStateNormal)?@"正常":@"停牌中")];
    [_baseValueArr addObject:@"06:00"];
//    [_baseValueArr addObject:((_symbolModel.status == 0)?@"正常":@"停牌中")];
}

- (void)dealConditionData {
    
    [_condValueArr addObject:@(_symbolModel.digits)];
    [_condValueArr addObject:[NSString stringWithFormat:@"%@ - %@",@(_symbolModel.volumesMin),@(_symbolModel.volumesMax)]];
//    [_condValueArr addObject:[NSString stringWithFormat:@"%@手",@(_symbolModel.positionVolumeMax)]];
    [_condValueArr addObject:[NSString stringWithFormat:@"%@",@(_symbolModel.volumesStep)]];
    [_condValueArr addObject:[NSString stringWithFormat:@"%@ - %@",@(_symbolModel.stopLevel),@(_symbolModel.maxStopLevel)]];
}

- (void)dealRequireData {
    
    NSArray *marginArr = [IXDataProcessTools getMarginSet:_symbolModel];
    if (marginArr && marginArr.count > 0) {
        
        _stepArr = [marginArr mutableCopy];
    }
}


- (void)dealTimeData {
    
    NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
    for (int i = 1; i < 8; i++) {
        
        NSArray *scheduleArr = [[IXDBScheduleMgr sharedInstance] queryScheduleByScheduleCataId:_symbolModel.scheduleCataId dayOfWeek:i];
        if (scheduleArr && scheduleArr.count > 0) {
            
            NSString *preDayStr,*dayStr,*nextDayStr;
            int startHour = 0,startMinute = 0,endHour = 0,endMinute = 0,preIndex = 0,dayIndex = 0,nextIndex = 0;
            int startPreHour = 0,startPreMinute = 0,endPreHour = 0,endPreMinute = 0;
            int startNextHour = 0,startNextMinute = 0,endNextHour = 0,endNextMinute = 0;
            for (int j = 0; j < scheduleArr.count; j++) {
                
                NSDictionary *tmpDic = [[NSDictionary alloc] init];
                tmpDic = scheduleArr[j];
                dayIndex = [tmpDic[kDayOfWeek] intValue];
                if ((dayIndex - 1) == 0 ) {
                    
                    preIndex = 7;
                } else {
                    
                    preIndex = dayIndex - 1;
                }
                preDayStr = [IXDataProcessTools weekdayToWeekString:preIndex];
                dayStr = [IXDataProcessTools weekdayToWeekString:dayIndex];
                if ((dayIndex + 1) == 8 ) {
                    
                    nextIndex = 1;
                } else {
                    
                    nextIndex = dayIndex + 1;
                }
                nextDayStr =  [IXDataProcessTools weekdayToWeekString:nextIndex];
                
                NSMutableArray *preArr = [self getSchedulePart:tmpArr weekDay:preDayStr];
                NSMutableArray *currentArr = [self getSchedulePart:tmpArr weekDay:dayStr];
                NSMutableArray *nextArr = [self getSchedulePart:tmpArr weekDay:nextDayStr];
                
                int start = [tmpDic[kStartTime] intValue] + 8 * 60;//GMT8
                int end = [tmpDic[kEndTime] intValue] + 8 * 60;//GMT8
                startHour = start/60;
                startMinute = start%60;
                if (start < 0) {
                    
                    if (startMinute != 0) {
                        
                        startPreHour = 23 - startHour;
                        startPreMinute = 60 - startMinute;
                        
                    } else {
                        
                        startPreHour = 24 - startHour;
                    }
                    startHour = 0;
                    startMinute = 0;
                }
                endHour = end/60;
                endMinute = end%60;
                if (end < 0) {
                    
                    if (endMinute != 0) {
                        
                        endPreHour = 23 - endHour;
                        endPreMinute = 60 - endMinute;
                    } else {
                        
                        endPreHour = 24 - endHour;
                    }
                    endHour = 0;
                    endMinute = 0;
                } else {
                    
                    endPreHour = 24;
                    endPreMinute = 0;
                }
                if (endHour > 24) {
                    
                    endNextHour = endHour - 24;
                    endNextMinute = endMinute;
                    endHour = 24;
                    endMinute = 0;
                    startNextHour = 0;
                    startNextMinute = 0;
                }
                
                //前一天
                if (startPreHour != 0 || startPreMinute != 0) {
                    
                    NSMutableDictionary *preDic = [[NSMutableDictionary alloc] init];
                    [preDic setObject:@(startPreHour) forKey:START_HOUR];
                    [preDic setObject:@(startPreMinute) forKey:START_MINUTE];
                    [preDic setObject:@(endPreHour) forKey:END_HOUR];
                    [preDic setObject:@(endPreMinute) forKey:END_MINUTE];
                    if (![self isRepeat:preArr currentTime:preDic]) {
                    
                        [preArr addObject:preDic];
                        tmpArr = [self setSchedulePart:tmpArr currentPart:preArr weekDay:preDayStr];
                    }
                }
                //当天
                NSMutableDictionary *currentDic = [[NSMutableDictionary alloc] init];
                [currentDic setObject:@(startHour) forKey:START_HOUR];
                [currentDic setObject:@(startMinute) forKey:START_MINUTE];
                [currentDic setObject:@(endHour) forKey:END_HOUR];
                [currentDic setObject:@(endMinute) forKey:END_MINUTE];
                if (![self isRepeat:currentArr currentTime:currentDic]) {
                
                    [currentArr addObject:currentDic];
                    tmpArr = [self setSchedulePart:tmpArr currentPart:currentArr weekDay:dayStr];
                }
                //后一天
                if (endNextHour != 0 || endNextMinute != 0) {
                    
                    NSMutableDictionary *nextDic = [[NSMutableDictionary alloc] init];
                    [nextDic setObject:@(startNextHour) forKey:START_HOUR];
                    [nextDic setObject:@(startNextMinute) forKey:START_MINUTE];
                    [nextDic setObject:@(endNextHour) forKey:END_HOUR];
                    [nextDic setObject:@(endNextMinute) forKey:END_MINUTE];
                    
                    if (![self isRepeat:nextArr currentTime:nextDic]) {
                    
                        [nextArr addObject:nextDic];
                        tmpArr = [self setSchedulePart:tmpArr currentPart:nextArr weekDay:nextDayStr];
                    }
                }
            }
        }
    }
    if (tmpArr && tmpArr.count > 0) {
        
        [self dealWithSchedule:tmpArr];
    }
}

- (NSMutableArray *)getSchedulePart:(NSArray *)arr weekDay:(NSString *)key {
    
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    if (arr && arr.count > 0) {
        
        for (int i = 0; i < arr.count; i++) {
            
            NSDictionary *tmpDic = arr[i];
            if (tmpDic && tmpDic.count > 0) {
                
                NSArray *tmpArr = tmpDic[key];
                if (tmpArr && tmpArr.count > 0) {
                    
                    retArr = [tmpArr mutableCopy];
                    break;
                }
            }
        }
    }
    
    return retArr;
}

- (BOOL)isRepeat:(NSArray *)dataArr currentTime:(NSDictionary *)currentDic {
    
    BOOL flag = NO;
    if (dataArr.count == 0) {
        
        flag = NO;
    } else {
        
        for (int i = 0; i < dataArr.count; i++) {
            
            NSDictionary *tmpDic = dataArr[i];
            if ([tmpDic isEqualToDictionary:currentDic]) {
                
                flag = YES;
                break;
            }
        }
    }
    
    return flag;
}

- (NSMutableArray *)setSchedulePart:(NSArray *)arr currentPart:(NSArray *)dataArr weekDay:(NSString *)key {
    
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    
    if (arr && arr.count > 0) {
        
        retArr = [arr mutableCopy];
    }
    if (!dataArr || dataArr.count == 0 || !key) {
        
        return retArr;
    }
    
    BOOL flag = NO;
    for (int i = 0; i < arr.count; i++) {
        
        NSDictionary *tmpDic = arr[i];
        if (tmpDic && tmpDic.count > 0) {
            
            NSArray *allKey = [tmpDic allKeys];
            if (allKey && allKey.count > 0) {
                
                if ([allKey[0] isEqualToString:key]) {
                    
                    NSMutableDictionary *dataDic = [[NSMutableDictionary alloc] init];
                    [dataDic setObject:dataArr forKey:key];
                    [retArr replaceObjectAtIndex:i withObject:dataDic];
                    flag = YES;
                    break;
                }
            }
        }
    }
    if (!flag) {
        
        NSMutableDictionary *dataDic = [[NSMutableDictionary alloc] init];
        [dataDic setObject:dataArr forKey:key];
        [retArr addObject:dataDic];
    }

    
    return retArr;
}

- (void)dealWithSchedule:(NSArray *)dataArr {
    
    if (dataArr && dataArr.count > 0) {
        
        
        for (int i = 0; i < dataArr.count; i++) {
            
            NSString *scheduleStr = [[NSString alloc] init];
            NSString *dayStr = [[NSString alloc] init];
            NSDictionary *tmpDic = dataArr[i];
            if (tmpDic && tmpDic.count > 0) {
                
                NSArray *allKey = [tmpDic allKeys];
                if (allKey && allKey.count > 0) {
                    
                    dayStr = allKey[0];
                    NSArray *tmpArr = tmpDic[dayStr];
                    if (tmpArr && tmpArr.count > 0) {
                        for (int j = 0; j < tmpArr.count; j++) {
                            
                            NSDictionary *showDic = tmpArr[j];
                            if (showDic && showDic.count > 0) {
                                
                                if (scheduleStr.length > 0) {
                                    
                                    scheduleStr = [scheduleStr stringByAppendingString:
                                                   [NSString stringWithFormat:@" %@:%@ - %@:%@",
                                                    [IXDataProcessTools formatterTimeDisplayHour:showDic[START_HOUR]],
                                                    [IXDataProcessTools formaterTimeDisplay:showDic[START_MINUTE]],
                                                    [IXDataProcessTools formatterTimeDisplayHour:showDic[END_HOUR]],
                                                    [IXDataProcessTools formaterTimeDisplay:showDic[END_MINUTE]]]];
                                } else {
                                    
                                    scheduleStr = [scheduleStr stringByAppendingString:
                                                   [NSString stringWithFormat:@"%@:%@ - %@:%@",
                                                    [IXDataProcessTools formatterTimeDisplayHour:showDic[START_HOUR]],
                                                    [IXDataProcessTools formaterTimeDisplay:showDic[START_MINUTE]],
                                                    [IXDataProcessTools formatterTimeDisplayHour:showDic[END_HOUR]],
                                                    [IXDataProcessTools formaterTimeDisplay:showDic[END_MINUTE]]]];
                                }
                            }
                            
                        }
                        NSMutableDictionary *scheduleDic = [[NSMutableDictionary alloc] init];
                        [scheduleDic setObject:[IXDataProcessTools dealWithNil:dayStr] forKey:DAY_OF_WEEK];
                        [scheduleDic setObject:[IXDataProcessTools dealWithNil:scheduleStr] forKey:TRADE_TIME];
                        [_timeArr addObject:scheduleDic];
                    }
                }
            }
            
        }
       
    }
}

- (void)addContractPropertieView {
    
    _sContractView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _sContractView.layer.cornerRadius = 6;
    _sContractView.layer.borderColor = [UIColor whiteColor].CGColor;
    _sContractView.layer.borderWidth = 1;
    _sContractView.showsVerticalScrollIndicator = NO;
    _sContractView.showsHorizontalScrollIndicator = NO;
    _sContractView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_sContractView];
    
    float offsetMaxY = 22;
    UILabel *baseLbl = [IXCustomView createLable:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView) - 41, 15) title:@"基本信息" font:PF_SC_15 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
    [_sContractView addSubview:baseLbl];
    
    offsetMaxY = GetView_MaxY(baseLbl) + 12;
    UIView *baseView = [[UIView alloc] initWithFrame:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView)  - 41, 0)];
    baseView.layer.cornerRadius = 6;
    baseView.layer.borderColor = CellSepLineColor.CGColor;
    baseView.layer.borderWidth = 1;
    baseView.backgroundColor = CellBackgroundColor;
    [_sContractView addSubview:baseView];
    
    NSArray *baseArr = @[@"产品名称",@"合约单位",@"基准货币",@"收益货币",@"隔夜利息(买)",@"隔夜利息(卖)",@"状态",@"结算时间"];
    float offsetY = 12;
    CGRect frame = CGRectMake(0, 0, 0, 0);
    for (int i = 0; i < baseArr.count; i++) {
        
        UILabel *lbl = [IXCustomView createLable:CGRectMake(12, offsetY, VIEW_W(baseView)/2 - 14, 12) title:baseArr[i] font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
        [baseView addSubview:lbl];
        
        UILabel *valueLbl = [IXCustomView createLable:CGRectMake(VIEW_W(baseView)/2 + 2, offsetY, VIEW_W(baseView)/2 - 14, 12) title:[NSString stringWithFormat:@"%@",_baseValueArr[i]] font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentRight];
        [baseView addSubview:valueLbl];
        offsetY = GetView_MaxY(lbl) + 11;
    }
    
    frame = baseView.frame;
    frame.size.height = offsetY;
    baseView.frame = frame;
    
    offsetMaxY = GetView_MaxY(baseView) + 15;
    
    UILabel *condLbl = [IXCustomView createLable:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView) - 41, 15) title:@"交易条件" font:PF_SC_15 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
    [_sContractView addSubview:condLbl];
    offsetMaxY = GetView_MaxY(condLbl) + 15;
    
    
    UIView *condView = [[UIView alloc] initWithFrame:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView)  - 41, 0)];
    condView.layer.cornerRadius = 6;
    condView.layer.borderColor = CellSepLineColor.CGColor;
    condView.layer.borderWidth = 1;
    condView.backgroundColor = CellBackgroundColor;
    [_sContractView addSubview:condView];
//    NSArray *condArr = @[@"小数位",@"单笔交易手数",@"挂单距离",@"持仓上限",@"手数差值"];
    NSArray *condArr = @[@"小数位",@"单笔交易数量",@"数量步差",@"挂单距离"];
    offsetY = 12;
    for (int i = 0; i < condArr.count; i++) {
        
        UILabel *lbl = [IXCustomView createLable:CGRectMake(12, offsetY, VIEW_W(baseView)/2 - 14, 12) title:condArr[i] font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
        [condView addSubview:lbl];
        
        UILabel *valueLbl = [IXCustomView createLable:CGRectMake(VIEW_W(baseView)/2 + 2, offsetY, VIEW_W(baseView)/2 - 14, 12) title:[NSString stringWithFormat:@"%@",_condValueArr[i]] font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentRight];
        [condView addSubview:valueLbl];
        offsetY = GetView_MaxY(lbl) + 11;
    }
    
    frame = condView.frame;
    frame.size.height = offsetY;
    condView.frame = frame;
    
    offsetMaxY = GetView_MaxY(condView) + 15;
    
    UILabel *requireLbl = [IXCustomView createLable:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView) - 41, 15) title:@"保证金层级" font:PF_SC_15 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
    [_sContractView addSubview:requireLbl];
    
    offsetMaxY = GetView_MaxY(requireLbl) + 12;
    
    UIView *requireView = [[UIView alloc] initWithFrame:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView)  - 41, 0)];
    requireView.layer.cornerRadius = 6;
    requireView.layer.borderColor = CellSepLineColor.CGColor;
    requireView.layer.borderWidth = 1;
    requireView.backgroundColor = CellBackgroundColor;
    [_sContractView addSubview:requireView];
    
    UIView *requireBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, VIEW_W(requireView), 27)];
    requireBgView.backgroundColor = CellSepLineColor;
    [requireView addSubview:requireBgView];
    
    UILabel *stepShowLbl = [IXCustomView createLable:CGRectMake(12, 7.5,VIEW_W(requireBgView)*7/32 - 16 ,12) title:@"阶梯" font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentCenter];
    [requireBgView addSubview:stepShowLbl];
    
    UILabel *positionShowLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*7/32 + 9.5, 7.5,VIEW_W(requireBgView)*16/32 - 2*9.5 ,12) title:@"仓位数量" font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
    [requireBgView addSubview:positionShowLbl];
    
    UILabel *marginShowLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*23/32 + 9.5, 7.5,VIEW_W(requireBgView)*9/32 - 2*9.5 ,12) title:@"Margin" font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentCenter];
    [requireBgView addSubview:marginShowLbl];
    
    for (int i = 0; i < 2; i++) {
        
        if (i == 0) {
            
            frame = CGRectMake(VIEW_W(requireBgView)*7/32, 0, 1, VIEW_H(requireBgView));
        } else {
            
            frame = CGRectMake(VIEW_W(requireBgView)*23/32, 0, 1, VIEW_H(requireBgView));
        }
        UIView *lineView = [[UIView alloc] initWithFrame:frame];
        lineView.backgroundColor = [UIColor whiteColor];
        [requireBgView addSubview:lineView];
    }
    
    offsetY = GetView_MaxY(requireBgView) + 12;
    NSString *rangStr = [[NSString alloc] init];
    NSString *marginStr = [[NSString alloc] init];
    for (int i = 0; i < _stepArr.count; i++) {
        
        NSDictionary *tmpDic = [[NSDictionary alloc] init];
        tmpDic = _stepArr[i];
        if (tmpDic && tmpDic.count > 0) {
            
            rangStr = [NSString stringWithFormat:@"%@ - %@",tmpDic[kRangeLeft],tmpDic[kRangeRight]];
            marginStr = [NSString stringWithFormat:@"%@%%",tmpDic[kPercent]];
        } else {
            
            rangStr = @"--";
            marginStr = @"--";
        }
        UILabel *preLbl = [IXCustomView createLable:CGRectMake(12, offsetY, VIEW_W(requireBgView)*7/32 - 12*2, 12) title:[NSString stringWithFormat:@"%d",(i + 1)] font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentCenter];
        [requireView addSubview:preLbl];
        
        UILabel *midLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*7/32 + 9.5, offsetY, VIEW_W(requireBgView)*16/32 - 2*9.5, 12) title:rangStr font:RO_M_14 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
        [requireView addSubview:midLbl];
        
        UILabel *endLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*23/32 + 9.5, offsetY, VIEW_W(requireBgView)*9/32 - 2*9.5, 12) title:marginStr font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentCenter];
        [requireView addSubview:endLbl];
        offsetY = GetView_MaxY(preLbl) + 11;
    }
    
    frame = requireView.frame;
    frame.size.height = offsetY;
    requireView.frame = frame;
    
    offsetMaxY = GetView_MaxY(requireView) + 15;
    
    UILabel *timeLbl = [IXCustomView createLable:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView) - 41, 15)
                                           title:[self settingTimeZone]
                                            font:PF_SC_15
                                       textColor:MarketSymbolNameColor
                                   textAlignment:NSTextAlignmentLeft];
    [_sContractView addSubview:timeLbl];
    
    offsetMaxY = GetView_MaxY(timeLbl) + 12;
    
    UIView *timeView = [[UIView alloc] initWithFrame:CGRectMake(20.5, offsetMaxY, VIEW_W(_sContractView)  - 41, 0)];
    timeView.layer.cornerRadius = 6;
    timeView.layer.borderColor = CellSepLineColor.CGColor;
    timeView.layer.borderWidth = 1;
    timeView.backgroundColor = CellBackgroundColor;
    [_sContractView addSubview:timeView];
    
    
    offsetY = 12;
    for (int i = 0; i < _timeArr.count; i++) {
        
        NSDictionary *tmpDic = [[NSDictionary alloc] init];
        tmpDic = _timeArr[i];
        
        UILabel *lbl = [IXCustomView createLable:CGRectMake(12, offsetY, 40, 12) title:[IXDataProcessTools dealWithNil:tmpDic[DAY_OF_WEEK]] font:PF_SC_12 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentLeft];
        [timeView addSubview:lbl];
        
        UILabel *valueLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(lbl) + 12.5, offsetY, VIEW_W(timeView) - (GetView_MaxX(lbl) + 12.5 + 12) , 12) title:[IXDataProcessTools dealWithNil:tmpDic[TRADE_TIME]] font:RO_M_14 textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentRight];
        [timeView addSubview:valueLbl];
        offsetY = GetView_MaxY(lbl) + 11;
    }
    
    frame = timeView.frame;
    frame.size.height = offsetY;
    timeView.frame = frame;
    
    offsetMaxY = GetView_MaxY(timeView) + 15;
    if (offsetMaxY > (kScreenHeight - 180)) {
        
        _sContractView.contentSize = CGSizeMake(VIEW_W(condView), offsetMaxY + 10);
    }
    
}

- (NSString *)settingTimeZone{
   NSMutableString *time = [NSMutableString  stringWithString:@"交易时间(GMT+8)"];
//    [time appendFormat:@"(%@)",[IXDateUtils getUserSaveTimeZone]];
    return time;
}

@end
