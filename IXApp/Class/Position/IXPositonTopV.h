//
//  IXPositonTopV.h
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(int, PosActionType) {
    PosActionTypeFilter,   //筛选
    PosActionTypeSort,     //排序
    PosActionTypeCancelFilter, //取消筛选
    PosActionTypeCancelSort,   //取消排序
};

typedef void(^filterAndSortBlock) (PosActionType rype);

@interface IXPositonTopV : UIView

@property (nonatomic, copy) filterAndSortBlock  actionB;

/**
 重置筛选按钮状态为Normnal
 */
- (void)resetFilterBtnState;

/**
 重置排序按钮状态为Normal
 */
- (void)resetSortBtnState;
@end
