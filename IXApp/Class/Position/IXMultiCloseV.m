//
//  IXMultiCloseV.m
//  IXApp
//
//  Created by Seven on 2017/11/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXMultiCloseV.h"

@interface IXMultiCloseV ()
@property (nonatomic, strong)UIButton   * chooseAllBtn;
@end

@implementation IXMultiCloseV

- (instancetype)init
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, kSize.width, kSize.height);
        self.dk_backgroundColorPicker = DKNavBarColor;
        
        [self createSubview];
    }
    return self;
}

- (void)createSubview
{
    NSString    * str = LocalizedString(@"全选");
    CGSize size = [str sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    
    _chooseAllBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, size.width + 60, kSize.height)];
    _chooseAllBtn.titleLabel.font = PF_MEDI(13);
    [_chooseAllBtn setTitle:str forState:UIControlStateNormal];
    [_chooseAllBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
    [_chooseAllBtn addTarget:self action:@selector(chooseAllAction) forControlEvents:UIControlEventTouchUpInside];
    [_chooseAllBtn setImage:[UIImage imageNamed:@"positionFilter_unchoose"] forState:UIControlStateNormal];
    [_chooseAllBtn dk_setImage:DKImageNames(@"cerFile_checked", @"cerFile_checked_D") forState:UIControlStateSelected];
    [_chooseAllBtn setImageEdgeInsets:UIEdgeInsetsMake((kSize.height - 20)/2, 22, (kSize.height - 20)/2, size.width + 18)];
    [_chooseAllBtn setTitleEdgeInsets:UIEdgeInsetsMake((kSize.height - 20)/2, 10, (kSize.height - 20)/2, 0)];
    [self addSubview:_chooseAllBtn];
    
    str = LocalizedString(@"取消");
    size = [str sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    
    UIButton    * cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 145 - size.width - 20,
                                                                         0,
                                                                         size.width + 20,
                                                                         kSize.height)];
    cancelBtn.titleLabel.font = PF_MEDI(13);
    [cancelBtn setTitle:str forState:UIControlStateNormal];
    [cancelBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    
    
    UIButton    * confirmBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 135, 0, 135, kSize.height)];
    confirmBtn.titleLabel.font = PF_MEDI(13);
    [confirmBtn setTitle:LocalizedString(@"批量平仓") forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x50a1e5);
    [self addSubview:confirmBtn];
}

- (void)chooseAllAction
{
    _chooseAllBtn.selected = !_chooseAllBtn.selected;
    if (self.chooseB) {
        if (_chooseAllBtn.selected) {
            self.chooseB(ChooseTypeChooseAll);
        } else {
            self.chooseB(ChooseTypeUnChooseAll);
        }
    }
}

- (void)cancelAction
{
    if (self.chooseB) {
        self.chooseB(ChooseTypeCancel);
    }
}

- (void)confirmAction
{
    if (self.chooseB) {
        self.chooseB(ChooseTypeClosePos);
    }
}

- (void)resetChooseBtnState
{
    _chooseAllBtn.selected = NO;
}

@end
