//
//  IXOrderCalM.m
//  IXApp
//
//  Created by Evn on 2017/6/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOrderCalM.h"
#import "IXTradeMarketModel.h"
#import "IXAccountBalanceModel.h"
#import "IXOpenModel+CheckData.h"
#import "IXUserDefaultM.h"
#import "IXComCalM.h"

#define BASELBLTAG 100
#define PROFITLBLTAG 200
#define POSITIONLBLTAG 300
#define MARGINLBLTAG 400
@implementation IXOrderCalM

#pragma mark view logic
+ (void)showTip:(NSString *)checkOrderInfo WithView:(IXOpenTipV *)view InView:(UIView *)rootView
{
    view.tipInfo = checkOrderInfo;
    if ( !view.isShow ) {
        view.isShow = YES;
        [rootView addSubview:view];
    }
}

+ (void)removeTip:(IXOpenTipV *)view
{
    if ( view ) {
        view.isShow = NO;
        [view removeFromSuperview];
    }
}

+ (void)updateTipInfoWithView:(IXOpenTipV *)view WithRoot:(IXUpdateOrderVC *)root
{
    [IXOrderCalM removeTip:view];
    NSString *checkOrderInfo = [IXOrderCalM checkResult:root];
    if ( checkOrderInfo.length != 0 ) {
        [IXOrderCalM showTip:[IXOrderCalM checkResult:root] WithView:view InView:root.view];
    }
}

+ (NSString *)checkResult:(IXUpdateOrderVC *)root
{
    if ( ![root.tradeModel.requestType isEqualToString:MARKETORDER] &&
        ![root.openModel validPrice:root.tradeModel.requestPrice]) {
        return LocalizedString(@"你输入的价格不在范围内，请重新输入");
    }else if (![root.openModel validVolume:root.tradeModel.requestVolume]) {
        if ( ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount ) ) {
            return LocalizedString(@"你输入的数量不在范围内，请重新输入");
        }
        return LocalizedString(@"你输入的手数不在范围内，请重新输入");
    }else if ( (![root.openModel validProfit:root.tradeModel.takeprofit]) && ([root.tradeModel.takeprofit doubleValue] != 0)) {
        return LocalizedString(@"你输入的止盈不在范围内，请重新输入");
    }else if ( (![root.openModel validLoss:root.tradeModel.stoploss]) && ([root.tradeModel.stoploss doubleValue] != 0)) {
        return LocalizedString(@"你输入的止损不在范围内，请重新输入");
    }
    return @"";
}

//刷新保证金和佣金,百万分之一
//行情推回来的时候，是主线程
+ (NSArray *)resetMarginAssetWithVolume:(double)volume WithRoot:(IXUpdateOrderVC *)root
{
    NSString *symbolKey = [NSString stringWithFormat:@"%llu",root.tradeModel.symbolModel.id_p];
    NSInteger direction = 1;
    id subDic = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symbolKey];
    if ( subDic && [subDic isKindOfClass:[NSNumber class]] ) {
        direction = [subDic integerValue];
    }else if( subDic && [subDic isKindOfClass:[NSDictionary class]] ){
        NSDictionary *dic = [(NSDictionary *)subDic objectForKey:POSITIONMODEL];
        direction = [dic integerForKey:SWAPDIRECRION];
    }
    double swap =  [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                   WithOffsetPrice:-1];
    
    double marginSwap = [[IXAccountBalanceModel shareInstance] caculateMarginSwapWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                                WithDirection:direction
                                                                              WithOffsetPrice:-1];

#if DEBUG
    [IXOrderCalM showRefreashPrc:root];
#endif
    
    if ( swap != 0 ) {
        //没有查找过才查询
        if ( root.openModel.commissionRate == 0 ) {
            root.openModel.commissionRate =  [IXRateCaculate queryComissionWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                       WithSymbolCataId:root.tradeModel.symbolModel.cataId];
            root.openModel.cataId = root.tradeModel.symbolModel.cataId;
        }
        
        double marginSet = [[IXAccountBalanceModel shareInstance] marginCurrentRateWithVolume:volume
                                                                                   WithSymbol:root.tradeModel.symbolModel];
        double margin = 0;
        double commission = 0;
        
        double marginPrice;
        double commissionPrice;
        
        if ( root.tradeModel.direction == item_order_edirection_DirectionBuy ) {
            if (root.tradeModel.quoteModel.BuyPrc.count > 0) {
                marginPrice = [root.tradeModel.quoteModel.BuyPrc[0] doubleValue];
                commissionPrice = [root.tradeModel.quoteModel.BuyPrc[0] doubleValue];
            } else {
                return nil;
            }
            
        }else{
            if (root.tradeModel.quoteModel.SellPrc.count > 0) {
                marginPrice = [root.tradeModel.quoteModel.SellPrc[0] doubleValue];
                commissionPrice = [root.tradeModel.quoteModel.SellPrc[0] doubleValue];
            } else {
                return nil;
            }
        }
        
        //如果为股票，保证金需要使用最新价
        if (STOCKID == root.openModel.parentId ||
            IDXMARKETID == root.openModel.marketId) {
            marginPrice = root.tradeModel.quoteModel.nPrice;
        }
        
        //计算保证金
        if(root.tradeModel.symbolModel.cataId == FXCATAID){
            margin = marginSwap * marginSet;
        }else{
            margin = swap * marginPrice * marginSet;
        }
        
        NSDecimalNumber *decMargin = [NSDecimalNumber decimalNumberWithString:
                                      [NSString stringWithFormat:@"%lf",margin]];
        decMargin = [decMargin decimalNumberByRoundingAccordingToBehavior:
                     [IXOrderCalM handlerPriceWithDigit:2]];
        
        root.tradeModel.margin = margin;
        
        //计算佣金，佣金可能为负；如果是负对用户来说是赚，需要重新计算汇率
        if( root.openModel.commissionRate < 0 ){
            swap = [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.tradeModel.symbolModel.id_p
                                                                   WithOffsetPrice:0];
        }
        
        if([IXAccountBalanceModel shareInstance].commissionType){
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:volume
                                   conSize:root.openModel.symbolModel.contractSizeNew];
        }else{
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:volume
                                      swap:swap
                                     price:commissionPrice];
        }
        
        NSDecimalNumber *decCommission = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",commission]];
        decCommission = [decCommission decimalNumberByRoundingAccordingToBehavior:
                         [IXOrderCalM handlerPriceWithDigit:2]];
        
        root.tradeModel.commission = commission;
        return @[decMargin,decCommission];
        
    }
    return nil;
}

+ (UILabel *)getbaseLbl:(IXUpdateOrderVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:BASELBLTAG];
    return lbl;
}

+ (UILabel *)getProfitLbl:(IXUpdateOrderVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:PROFITLBLTAG];
    return lbl;
}

+ (UILabel *)getPositionLbl:(IXUpdateOrderVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:POSITIONLBLTAG];
    return lbl;
}

+ (UILabel *)getMarginLbl:(IXUpdateOrderVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:MARGINLBLTAG];
    return lbl;
}

//更新提示
+ (void)showRefreashPrc:(IXUpdateOrderVC *)root
{
    NSString *symId = [NSString stringWithFormat:@"%llu",root.tradeModel.symbolModel.id_p];
    id value = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symId];
    if ( value && [value isKindOfClass:[NSDictionary class]] ) {
        for ( NSString *key in [value allKeys] ) {
            
            NSDictionary *curDic = [(NSDictionary *)value objectForKey:key];
            
            if ( [curDic objectForKey:SYMBOLID] ) {
                
                if ( [key isEqualToString:BASEMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                               [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    [IXOrderCalM getbaseLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                          sModel.name,
                                                          quote.SellPrc[0],
                                                          quote.BuyPrc[0]];
                }else if ( [key isEqualToString:PROFITMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];

                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    [IXOrderCalM getProfitLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                            sModel.name,
                                                            quote.SellPrc[0],
                                                            quote.BuyPrc[0]];
                    
                }else if ( [key isEqualToString:POSITIONMODEL] ) {
                    if(STOCKID == root.openModel.parentId ||
                       IDXMARKETID == root.openModel.marketId){
                        [IXOrderCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ lastPrice %lf",
                                                                 root.tradeModel.symbolModel.name,
                                                                 root.tradeModel.quoteModel.nPrice];
                    }else{
                        if ( root.tradeModel.quoteModel.BuyPrc.count > 0 && root.tradeModel.quoteModel.SellPrc.count > 0 ) {
                            [IXOrderCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                                      root.tradeModel.symbolModel.name,
                                                                      root.tradeModel.quoteModel.SellPrc[0],
                                                                      root.tradeModel.quoteModel.BuyPrc[0]];
                        }
                    }                    
                }else if ( [key isEqualToString:MAGNSYMMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    [IXOrderCalM getMarginLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                            sModel.name,
                                                            quote.SellPrc[0],
                                                            quote.BuyPrc[0]];
                }
            }
        }
    }else{
        if ( root.tradeModel.quoteModel.BuyPrc.count > 0 && root.tradeModel.quoteModel.SellPrc.count > 0 ) {
            
            [IXOrderCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                     root.tradeModel.symbolModel.name,
                                                     root.tradeModel.quoteModel.SellPrc[0],
                                                     root.tradeModel.quoteModel.BuyPrc[0]];
        }
    }
}


//添加提示
+ (void)addPrcTipInfo:(IXUpdateOrderVC *)root
{
    if ( ![IXOrderCalM getPositionLbl:root] ){
        UILabel *posLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 0, kScreenWidth, 10)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentLeft
                                           wTextColor:0x4c6072
                                           dTextColor:0x4c6072];
        posLbl.tag = POSITIONLBLTAG;
        [root.navigationController.navigationBar addSubview:posLbl];
    }
    
    if ( ![IXOrderCalM getProfitLbl:root] ) {
        UILabel *profitLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 11, kScreenWidth, 10)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x4c6072
                                              dTextColor:0x4c6072];
        profitLbl.tag = PROFITLBLTAG;
        [root.navigationController.navigationBar addSubview:profitLbl];
    }
    
    if (![IXOrderCalM getbaseLbl:root] ) {
        UILabel *baseLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 22, kScreenWidth, 10)
                                              WithFont:PF_MEDI(10)
                                             WithAlign:NSTextAlignmentLeft
                                            wTextColor:0x4c6072
                                            dTextColor:0x4c6072];
        baseLbl.tag = BASELBLTAG;
        [root.navigationController.navigationBar addSubview:baseLbl];
    }
    
    if (![IXOrderCalM getMarginLbl:root]) {
        UILabel *marginLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 33, kScreenWidth, 10)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x4c6072
                                              dTextColor:0x4c6072];
        marginLbl.tag = MARGINLBLTAG;
        [root.navigationController.navigationBar addSubview:marginLbl];
    }
}

+ (void)removePrcTipInfo:(IXUpdateOrderVC *)root
{
    UILabel *pos = [IXOrderCalM getPositionLbl:root];
    if ( pos ) {
        [pos removeFromSuperview];
    }
    
    UILabel *base = [IXOrderCalM getbaseLbl:root];
    if ( base ) {
        [base removeFromSuperview];
    }
    
    UILabel *profit = [IXOrderCalM getProfitLbl:root];
    if ( profit ) {
        [profit removeFromSuperview];
    }
    
    UILabel *margin = [IXOrderCalM getMarginLbl:root];
    if (margin) {
        [margin removeFromSuperview];
    }
}

+ (NSDecimalNumberHandler *)handlerPriceWithDigit:(NSInteger)digit
{
    NSDecimalNumberHandler *roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:digit
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
    
    return roundingBehavior;
}

@end
