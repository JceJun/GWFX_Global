//
//  IXHedgeReciveCell.m
//  IXApp
//
//  Created by Bob on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXHedgeReciveCell.h"

@interface IXHedgeReciveCell ()

@property (nonatomic, strong) UILabel *posDirLbl;

@property (nonatomic, strong) UILabel *posVolLbl;

@property (nonatomic, strong) UILabel *posIdLbl;

@property (nonatomic, strong) UILabel *prcTipLbl;

@property (nonatomic, strong) UILabel *openPrcLbl;

@property (nonatomic, strong) UILabel *proTipLbl;

@property (nonatomic, strong) UILabel *profitLbl;

@property (nonatomic, strong) UIButton *markBtn;

@end


@implementation IXHedgeReciveCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self prcTipLbl];
    [self proTipLbl];
    [self markBtn];
}


- (void)setProfit:(NSString *)profit
{
    self.profitLbl.text = [NSString formatterPriceSign:[profit doubleValue] WithDigits:2];
    if( [profit doubleValue] > 0 ){
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }else if ( [profit doubleValue] == 0 ){
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
    }else{
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
}

- (void)setModel:(IXHedgeM *)model
{
    self.posDirLbl.text = model.posDir;
    if ( [model.posDir isEqualToString:LocalizedString(@"买.")] ) {
        self.posDirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.posDirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }else{
        self.posDirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.posDirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    
    self.posVolLbl.text = model.showVol;
    self.openPrcLbl.text = model.posPrc;
    self.posIdLbl.text = model.posId;
}

- (void)responseToMark
{
    if ( self.hedgeBlock ) {
        self.hedgeBlock(self.tag);
    }
}


- (void)setChoosePos:(BOOL)choosePos
{
    self.markBtn.hidden = !choosePos;
}

- (UIButton *)markBtn
{
    if ( !_markBtn ) {
        _markBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _markBtn.frame = CGRectMake( 20, 20, 16, 16);
        [_markBtn dk_setImage:DKImageNames(@"common_cell_choose", @"common_cell_choose_D")
                     forState:UIControlStateNormal];
        [self.contentView addSubview:_markBtn];
        [_markBtn addTarget:self
                     action:@selector(responseToMark)
           forControlEvents:UIControlEventTouchUpInside];
    }
    return _markBtn;
}

- (UILabel *)posDirLbl
{
    if ( !_posDirLbl ) {
        _posDirLbl = [IXUtils createLblWithFrame:CGRectMake( 56, 12, 15, 15)
                                        WithFont:PF_MEDI(12)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0xffffff
                                      dTextColor:0x262f3e];
        [self.contentView addSubview:_posDirLbl];
    }
    return _posDirLbl;
}

- (UILabel *)posVolLbl
{
    if ( !_posVolLbl ) {
        _posVolLbl = [IXUtils createLblWithFrame:CGRectMake( 77, 12, kScreenWidth/2 - 57, 15)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea];
        [self.contentView addSubview:_posVolLbl];
    }
    return _posVolLbl;
}

- (UILabel *)posIdLbl
{
    if ( !_posIdLbl ) {
        _posIdLbl = [IXUtils createLblWithFrame:CGRectMake( 56, 39, kScreenWidth/2 - 77, 15)
                                        WithFont:RO_REGU(12)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x99abba
                                     dTextColor:0x8395a4];
        [self.contentView addSubview:_posIdLbl];
    }
    return _posIdLbl;
}


- (UILabel *)prcTipLbl
{
    if ( !_prcTipLbl ) {
        _prcTipLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth/2 + 27, 12, 60, 15)
                                        WithFont:PF_MEDI(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_prcTipLbl];
        _prcTipLbl.text = LocalizedString(@"开仓价.");
    }
    return _prcTipLbl;
}

- (UILabel *)openPrcLbl
{
    if ( !_openPrcLbl ) {
        _openPrcLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth/2  + 70, 12, 120, 15)
                                         WithFont:RO_REGU(15)
                                        WithAlign:NSTextAlignmentLeft
                                       wTextColor:0x4c6072
                                       dTextColor:0xe9e9ea];
        [self.contentView addSubview:_openPrcLbl];
    }
    return _openPrcLbl;
}

- (UILabel *)proTipLbl
{
    if ( !_proTipLbl ) {
        _proTipLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth/2 + 27, 39, 60, 15)
                                        WithFont:PF_MEDI(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_proTipLbl];
        _proTipLbl.text = LocalizedString(@"盈亏");
    }
    return _proTipLbl;
}

- (UILabel *)profitLbl
{
    if ( !_profitLbl ) {
        _profitLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth/2 + 70, 39, 150, 15)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x4c6072
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_profitLbl];
    }
    return _profitLbl;
}

@end
