//
//  IXPositionFilterV.m
//  IXApp
//
//  Created by Seven on 2017/11/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionFilterV.h"
#import "UIImage+IX.h"

#define sHeight (kScreenHeight - kNavbarHeight - 43.5 - kBtomMargin)
#define kHideFrame  CGRectMake(0, kScreenHeight, kScreenWidth, sHeight)
#define kShowFrame  CGRectMake(0, kNavbarHeight + 43.5, kScreenWidth, sHeight)
#define kHeaderFrame    CGRectMake(0, 0, kScreenWidth, 150)

#pragma mark -
#pragma mark - FilterHeaderV

@interface FilterHeaderV : UIView
@property (nonatomic, assign)PositionFilterType   dirOp;
@end

@implementation FilterHeaderV

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self  = [super initWithFrame:frame]) {
        self.dk_backgroundColorPicker = DKNavBarColor;
        [self creteSubview];
    }
    return self;
}

- (void)creteSubview
{
    //买卖方向
    UIView  * line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    line.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x262f3e);
    [self addSubview:line];
    
    UILabel * dirLab = [UILabel labelWithFrame:CGRectMake(15, 33, 200, 16)
                                          text:LocalizedString(@"买卖方向")
                                      textFont:PF_MEDI(12)
                                     textColor:DKColorWithRGBs(0x99abba, 0x3a4553)];
    [self addSubview:dirLab];
    
    CGFloat width = (kScreenWidth - 60) / 3;
    UIButton    * dirBtn1 = [self buttonWithFrame:CGRectMake(15, GetView_MaxY(dirLab) + 23, width, 30)
                                            title:LocalizedString(@"全部")
                                              tag:PositionFilterTypeAll];
    dirBtn1.selected = YES;
    UIButton    * dirBtn2 = [self buttonWithFrame:CGRectMake(width + 30, GetView_MinY(dirBtn1), width, 30)
                                            title:LocalizedString(@"买入")
                                              tag:PositionFilterTypebuy];
    UIButton    * dirBtn3 = [self buttonWithFrame:CGRectMake(width*2 + 45, GetView_MinY(dirBtn1), width, 30)
                                            title:LocalizedString(@"卖出")
                                              tag:PositionFilterTypeSell];
    [self addSubview:dirBtn1];
    [self addSubview:dirBtn2];
    [self addSubview:dirBtn3];
    
    //产品名称
    UILabel * productLab = [UILabel labelWithFrame:CGRectMake(15, GetView_MaxY(dirBtn1) + 23, 200, 16)
                                              text:LocalizedString(@"产品名称")
                                          textFont:PF_MEDI(12)
                                         textColor:DKColorWithRGBs(0x99abba, 0x3a4553)];
    [self addSubview:productLab];
}

- (void)setDirOp:(PositionFilterType)dirOp
{
    _dirOp = dirOp;
    UIButton * button = (UIButton *)[self viewWithTag:dirOp];
    [self btnAction:button];
}

- (void)btnAction:(UIButton *)btn
{
    for (int i = PositionFilterTypeAll; i <= PositionFilterTypeSell; i ++) {
        UIButton * button = (UIButton *)[self viewWithTag:i];
        button.selected = NO;
    }
    _dirOp = btn.tag;

    btn.selected = YES;
}

- (UIButton *)buttonWithFrame:(CGRect)frame title:(NSString *)title tag:(NSInteger)tag
{
    UIButton * btn = [[UIButton alloc] initWithFrame:frame];
    btn.titleLabel.font = PF_MEDI(13);
    btn.titleLabel.numberOfLines = 0;
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btn setTitle:title forState:UIControlStateNormal];
    UIImage * wnImg = [UIImage imageWithColor:UIColorHexFromRGB(0xffffff)];
    UIImage * wsImg = [UIImage imageWithColor:UIColorHexFromRGB(0x11b873)];
    UIImage * dnimg = [UIImage imageWithColor:UIColorHexFromRGB(0x242a36)];
    UIImage * dsimg = [UIImage imageWithColor:UIColorHexFromRGB(0x50a1e5)];
    [btn dk_setBackgroundImage:DKImageWithImgs(wnImg, dnimg) forState:UIControlStateNormal];
    [btn dk_setBackgroundImage:DKImageWithImgs(wsImg, dsimg) forState:UIControlStateSelected];
    [btn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0x8395a4) forState:UIControlStateNormal];
    [btn dk_setTitleColorPicker:DKColorWithRGBs(0xffffff, 0xe9e9ea) forState:UIControlStateSelected];
    btn.layer.cornerRadius = 4.f;
    btn.layer.borderWidth = .5f;
    btn.layer.masksToBounds = YES;
    btn.layer.dk_borderColorPicker = DKColorWithRGBs(0xe4e4e4, 0x242a36);
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = tag;

    return btn;
}

@end

#pragma mark -
#pragma mark - FilterVCell

@interface FilterVCell : UITableViewCell
@property (nonatomic, strong)UILabel    * titleLab;
@property (nonatomic, strong)UIImageView    * imgV;
@property (nonatomic, assign)BOOL choosed;
@end
@implementation FilterVCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.dk_backgroundColorPicker = DKNavBarColor;
        [self createSubV];
    }
    return self;
}

- (void)createSubV
{
    _titleLab = [UILabel labelWithFrame:CGRectMake(15,  15, 200, 15)
                                   text:@""
                               textFont:PF_MEDI(13)
                              textColor:DKCellTitleColor];
    [self.contentView addSubview:_titleLab];
    
    _imgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - 38, 12.5, 20, 20)];
    [self.contentView addSubview:_imgV];
}


-(void)setChoosed:(BOOL)choosed
{
    _choosed = choosed;
    if (choosed) {
        _imgV.image = AutoNightImageNamed(@"common_cell_choose");
    } else {
        _imgV.image = nil;
    }
}

@end


#pragma mark -
#pragma mark - IXPositionFilterV

@interface IXPositionFilterV ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (nonatomic, strong)UITableView    * tableV;
@property (nonatomic, strong)FilterHeaderV  * headerV;

@end

@implementation IXPositionFilterV

- (instancetype)init
{
    if (self = [super init]) {
        self.frame = kHideFrame;
        self.backgroundColor = [UIColor redColor];
        
        [self createSubview];
        
        self.dirOp = PositionFilterTypeAll;
        self.productArr = [@[] mutableCopy];
    }
    return self;
}

- (void)createSubview
{
    _headerV = [[FilterHeaderV alloc] initWithFrame:kHeaderFrame];
    
    _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, sHeight - 43)];
    _tableV.separatorStyle = UITableViewCellSelectionStyleNone;
    _tableV.dk_backgroundColorPicker = DKNavBarColor;
    _tableV.tableFooterView = [UIView new];
    _tableV.tableHeaderView = _headerV;
    _tableV.dataSource = self;
    _tableV.delegate = self;
    [self addSubview:_tableV];
    
    UIButton    * resetBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, GetView_MaxY(_tableV), 100, 43)];
    resetBtn.titleLabel.font = PF_MEDI(13);
    [resetBtn setTitle:LocalizedString(@"重置") forState:UIControlStateNormal];
    [resetBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    [resetBtn addTarget:self action:@selector(resetBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [resetBtn dk_setBackgroundColorPicker:DKNavBarColor];
    [self addSubview:resetBtn];

    UIButton * confirmBtn = [[UIButton alloc] initWithFrame:CGRectMake(100, GetView_MinY(resetBtn), kScreenWidth - 100, 43)];
    confirmBtn.titleLabel.font = PF_MEDI(13);
    [confirmBtn setTitle:LocalizedString(@"确定") forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [confirmBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x11b873, 0x50a1e5)];
    [confirmBtn dk_setTitleColorPicker:DKColorWithRGBs(0xffffff, 0xe9e9ea) forState:UIControlStateNormal];
    [self addSubview:confirmBtn];
    
    UIView  * line = [[UIView alloc] initWithFrame:CGRectMake(0, sHeight - 43, kScreenWidth, 0.5)];
    line.dk_backgroundColorPicker = DKLineColor;
    [self addSubview:line];
}

- (void)show
{
    [self.tableV reloadData];
    _isShow = YES;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView  animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = kShowFrame;
    } completion:nil];
}

- (void)resetAction
{
    [self resetBtnAction:nil];
}

- (void)resetBtnAction:(id)sender
{
    _dirOp = PositionFilterTypeAll;
    _headerV.dirOp = PositionFilterTypeAll;
    _productArr = [@[] mutableCopy];
    [_tableV reloadData];
    
    if (self.filterB && sender != nil) {
        self.filterB(_dirOp, _productArr, NO);
    }
}

- (void)confirmAction
{
    _dirOp = self.headerV.dirOp;
    if (self.filterB) {
        self.filterB(_dirOp, _productArr, YES);
    }
    
    [UIView  animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = kHideFrame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)dismiss
{
    _isShow = NO;
    [UIView  animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = kHideFrame;
    } completion:^(BOOL finished) {
        self.headerV.dirOp = _dirOp;
        [self removeFromSuperview];
    }];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _itemArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

static  NSString * cellIdent = @"xxxxcell";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterVCell * cell = (FilterVCell *)[tableView dequeueReusableCellWithIdentifier:cellIdent];
    if (!cell) {
        cell = [[FilterVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
    }
    NSDictionary * dic = _itemArr[indexPath.row];
    cell.titleLab.text = dic[@"name"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    __block BOOL choosed = NO;
    [_productArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([dic[@"symbolId"] integerValue] == [obj[@"symbolId"] integerValue]) {
            choosed = YES;
            *stop = YES;
        }
    }];
    
    cell.choosed = choosed;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterVCell  * cell = (FilterVCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.choosed = !cell.choosed;
    
    NSDictionary * dic = _itemArr[indexPath.row];
    if (cell.choosed) {
        [_productArr addObject:dic];
    } else {
        [_productArr enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj[@"symbolId"] integerValue] == [dic[@"symbolId"] integerValue]) {
                [_productArr removeObject:obj];
            }
        }];
    }
}

#pragma mark -
#pragma mark - setter

- (void)setDirOp:(PositionFilterType)dirOp
{
    _dirOp = dirOp;
    self.headerV.dirOp = dirOp;
}

@end
