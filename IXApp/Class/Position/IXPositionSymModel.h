//
//  IXPositionSymbolModel.h
//  IXApp
//
//  Created by Evn on 2017/7/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXSymModel.h"

@interface IXPositionSymModel : NSObject

@property (nonatomic, strong) NSMutableDictionary *symDic;

+ (IXPositionSymModel *)shareInstance;
/** 通过产品ID获取缓存产品信息 */
- (IXSymModel *)getCacheSymbolBySymbolId:(uint64_t)symbolId;
- (void)clearCache;

@end
