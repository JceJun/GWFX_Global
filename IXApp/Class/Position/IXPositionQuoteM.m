//
//  IXPositionQuoteM.m
//  IXApp
//
//  Created by Evn on 2017/6/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionQuoteM.h"
#import "IXAccountBalanceModel.h"
#import "IXTradeMarketModel.h"

@implementation IXPositionQuoteM

//订阅数据
+ (NSMutableArray *)subscribeDynamicPrice:(IXPositionInfoVC *)root
{
    [[IXAccountBalanceModel shareInstance] addSubSymbolWithSymbolId:root.positionModel.symbolModel.id_p];
    [IXPositionQuoteM loadDetailQuote:root];
    
    return  [IXPositionQuoteM loadCacheQuote:root];
}

+ (void)cancelDynamicPrice:(IXPositionInfoVC *)root
{
    [[IXAccountBalanceModel shareInstance] delSubSymbolWithSymbolId:root.positionModel.symbolModel.id_p];
    [IXPositionQuoteM cancelDetailQuote:root];
}

+ (NSMutableArray *)loadCacheQuote:(IXPositionInfoVC *)root
{
    if (!root.positionModel.quoteModel) {
        return nil;
    }
    NSMutableArray *_quote = [NSMutableArray arrayWithObject:root.positionModel.quoteModel];
    
    NSString *symbolKey = [NSString stringWithFormat:@"%llu",root.positionModel.symbolModel.id_p];
    id subDic = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symbolKey];
    if ( subDic ) {
        if ( [subDic isKindOfClass:[NSDictionary class]] ) {
            for ( NSString *curKey in [subDic allKeys] ) {
                
                NSDictionary *curVal = [(NSDictionary *)subDic objectForKey:curKey];
                if( [curVal integerForKey:@"symbolId"] != 0 ){
                    
                    NSString *curSymbolKey = [NSString stringWithFormat:@"%ld",(long)[curVal integerForKey:@"symbolId"]];
                    IXQuoteM *model = [[[IXQuoteDataCache shareInstance].dynQuoteDic objectForKey:curSymbolKey] mutableCopy];
                    if( !model ){
                       model = [IXDataProcessTools queryQuoteDataBySymbolId:[curVal integerForKey:@"symbolId"]];
                        if (model) {
                            [[IXQuoteDataCache shareInstance].dynQuoteDic setValue:model forKey:curSymbolKey];
                        }
                    }
                    if ( model && model.BuyPrc.count != 0 && model.SellPrc.count != 0 ) {
                        [_quote addObject:model];
                    }
                }
            }
            
        }else{
            
            IXQuoteM *model = [[[IXQuoteDataCache shareInstance].dynQuoteDic objectForKey:symbolKey] mutableCopy];
            if( !model ){
                model = [IXDataProcessTools queryQuoteDataBySymbolId:[symbolKey integerValue]];
                [[IXQuoteDataCache shareInstance].dynQuoteDic setValue:model forKey:symbolKey];
            }
            if ( model && model.BuyPrc.count != 0 && model.SellPrc.count != 0 ) {
                [_quote addObject:model];
            }
        }
    }
    
    [[IXAccountBalanceModel shareInstance] setQuoteDataArr:_quote];
    return _quote;
}


+ (void)loadDetailQuote:(IXPositionInfoVC *)root
{
    if ( root.positionModel && root.positionModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(root.positionModel.marketId),
                           @"id":@(root.positionModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] subscribeDetailPriceWithSymbolAry:arr];
        if (root.openModel.nLastClosePrice <= 0) {
            [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:arr];
        }
    }
}

+ (void)cancelDetailQuote:(IXPositionInfoVC *)root
{
    if ( root.positionModel && root.positionModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(root.positionModel.marketId),
                           @"id":@(root.positionModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] unSubscribeDetailPriceWithSymbolAry:arr];
    }
}


@end
