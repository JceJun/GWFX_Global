//
//  IXOrderListCell.m
//  IXApp
//
//  Created by Magee on 16/11/23.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXOrderListCell.h"
#import "NSString+FormatterPrice.h"
#import "IXUserDefaultM.h"
#import "CAGradientLayer+IX.h"

@interface IXOrderListCell ()

@property (nonatomic, strong)UIImageView *bgView;
@property (nonatomic, strong)UILabel *nameLbl;
@property (nonatomic, strong)UILabel *contentLbl;//内容
@property (nonatomic, strong)UILabel *stopLbl;//止损
@property (nonatomic, strong)UILabel *profitLbl;//获利
@property (nonatomic, strong)UIView  *lineView;
@property (nonatomic, strong)UILabel *dirLbl;//买卖方向
@property (nonatomic, strong)UILabel *numLbl;//数量
@property (nonatomic, strong)UILabel *showPriceLbl;
@property (nonatomic, strong)UILabel *priceLbl;//开盘价
@property (nonatomic, strong)UILabel *showOrderLbl;
@property (nonatomic, strong)UILabel *orderLbl;//订单号
@property (nonatomic, strong)UILabel *showTypeLbl;//类型
@property (nonatomic, strong)UILabel *typeLbl;//类型

@end

@implementation IXOrderListCell

#pragma mark -
#pragma mark - initialize

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addShadow];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, kScreenWidth, 104)];
        _bgView.dk_backgroundColorPicker  = DKNavBarColor;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [IXCustomView createLable:CGRectMake(15,15, 85, 15)
                                       title:@""
                                        font:PF_MEDI(13)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_nameLbl];
    }
    return _nameLbl;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.nameLbl) + 5,VIEW_Y(self.nameLbl) + 3, GetView_MinX(self.stopLbl) - GetView_MaxX(self.nameLbl), 12)
                                          title:@""
                                           font:RO_REGU(10)
                                     wTextColor:0xa7adb5
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UILabel *)profitLbl
{
    if (!_profitLbl) {
        _profitLbl = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView) - 40, VIEW_Y(self.nameLbl), 25, 15)
                                         title:LocalizedString(@"止盈")
                                          font:PF_MEDI(10)
                                    wTextColor:0xffffff
                                    dTextColor:0x3a4553
                                 textAlignment:NSTextAlignmentCenter];
        _profitLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xa3aebb,0x262f3e);
        [self.bgView addSubview:_profitLbl];
    }
    return _profitLbl;
}

- (UILabel *)stopLbl
{
    if (!_stopLbl) {
        _stopLbl = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView) - 70, VIEW_Y(self.nameLbl), 25, 15)
                                       title:LocalizedString(@"止损")
                                        font:PF_MEDI(10)
                                  wTextColor:0xffffff
                                  dTextColor:0x3a4553
                               textAlignment:NSTextAlignmentCenter];
        _stopLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xa3aebb,0x262f3e);
        [self.bgView addSubview:_stopLbl];
    }
    return _stopLbl;
}

- (UILabel *)dirLbl
{
    if (!_dirLbl) {
        _dirLbl = [IXCustomView createLable:CGRectMake(15,GetView_MaxY(self.nameLbl) + 25,15, 15)
                                      title:@""
                                       font:PF_MEDI(10)
                                 wTextColor:0xffffff
                                 dTextColor:0x262f3e
                              textAlignment:NSTextAlignmentCenter];
        _dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0xff4d2d);
        [self.bgView addSubview:_dirLbl];
    }
    return _dirLbl;
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.dirLbl) + 5, GetView_MinY(self.dirLbl), 100, 16)
                                      title:@""
                                       font:RO_REGU(15)
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                              textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_numLbl];
    }
    return _numLbl;
}

- (UILabel *)showOrderLbl
{
    if (!_showOrderLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"订单号") WithFont:PF_MEDI(12)] + 1;
        _showOrderLbl = [IXCustomView createLable:CGRectMake( 112, GetView_MaxY(self.nameLbl) + 15, width, 12)title:LocalizedString(@"订单号")
                                             font:PF_MEDI(12)
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showOrderLbl];
    }
    return _showOrderLbl;
}

- (UILabel *)orderLbl
{
    if (!_orderLbl) {
        _orderLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.showOrderLbl) + 5, VIEW_Y(self.showOrderLbl) - 2, 200, 15)
                                        title:@""
                                         font:RO_REGU(15)
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_orderLbl];
    }
    return _orderLbl;
}

- (UILabel *)showPriceLbl
{
    if (!_showPriceLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"请求价.") WithFont:PF_MEDI(12)] + 1;
        _showPriceLbl = [IXCustomView createLable:CGRectMake( 112, GetView_MaxY(self.showOrderLbl) + 15,width, 12)
                                            title:LocalizedString(@"请求价.")
                                             font:PF_MEDI(12)
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showPriceLbl];
    }
    return _showPriceLbl;
}

- (UILabel *)priceLbl
{
    if (!_priceLbl) {
        _priceLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.showPriceLbl) + 5, VIEW_Y(self.showPriceLbl) -2, 200, (15))
                                        title:@"--"
                                         font:RO_REGU(15)
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_priceLbl];
    }
    return _priceLbl;
}

- (UILabel *)showTypeLbl
{
    if (!_showTypeLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"类型") WithFont:PF_MEDI(12)] + 1;
        _showTypeLbl = [IXCustomView createLable:CGRectMake( VIEW_W(self.bgView) - 70, VIEW_Y(self.showPriceLbl),width,12)
                                           title:LocalizedString(@"类型")
                                            font:PF_MEDI(12)
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showTypeLbl];
    }
    return _showTypeLbl;
}

- (UILabel *)typeLbl
{
    if (!_typeLbl) {
        _typeLbl = [IXCustomView createLable:CGRectMake( VIEW_W(self.bgView) - 40,  VIEW_Y(self.showTypeLbl),30,12)
                                       title:@"--"
                                        font:PF_MEDI(12)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_typeLbl];
    }
    return _typeLbl;
}

- (void)addShadow
{
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * topL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 5)
                                                        dkcolors:topColors];
    [self.contentView.layer addSublayer:topL];
    
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 109, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [self.contentView.layer addSublayer:btomL];
    
    self.layer.masksToBounds = NO;
    self.contentView.layer.masksToBounds = NO;
}


#pragma mark -
#pragma mark - public
- (void)reloadUIWithOrderModel:(IXOrderModel *)model
{
    self.nameLbl.text = model.symbolModel.languageName;
    CGRect frame = self.nameLbl.frame;
    CGFloat width = [IXEntityFormatter getContentWidth:self.nameLbl.text WithFont:PF_MEDI(13)];
    frame.size.width = width;
    self.nameLbl.frame = frame;
    frame = self.contentLbl.frame;
    frame.origin.x = VIEW_X(self.nameLbl) + VIEW_W(self.nameLbl) + 4.5;
    self.contentLbl.frame = frame;
    
    self.contentLbl.text = [NSString stringWithFormat:@"%@:%@ %@",model.symbolModel.name,model.symbolModel.marketName,[IXEntityFormatter quoteTimeToString:model.order.requestTime]];
    if (model.order.takeProfit > 0) {
        self.profitLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x262f3e);
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x8395a4);
    } else {
        self.profitLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x3a4553);
    }
    
    if (model.order.stopLoss > 0) {
        self.stopLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x262f3e);
        self.stopLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x8395a4);
    } else {
        self.stopLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
        self.stopLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x3a4553);
    }
    if (model.order.direction == 1) {
        self.dirLbl.text = LocalizedString(@"买.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    } else {
        self.dirLbl.text = LocalizedString(@"卖.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    
    NSString *volume,*price;
    if (model.order.type == item_order_etype_TypeOpen) {
        volume = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.order.requestVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits];
        price = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.order.refPrice] WithDigits:model.symbolModel.digits];
    } else {
        volume = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.order.requestVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits];
        price = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.order.requestPrice] WithDigits:model.symbolModel.digits];
    }
    self.numLbl.text = volume;
    self.priceLbl.text = price;
    self.typeLbl.text = [NSString stringWithFormat:@"%@",[IXDataProcessTools orderTypeByType:model.order.type]];
    self.orderLbl.text = [NSString stringWithFormat:@"%lld",model.order.id_p];
}

@end

