//
//  IXOrderListInfoCell.m
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOrderListInfoCell.h"
#import "NSString+FormatterPrice.h"
#import "IXDateUtils.h"
#import "IXAppUtil.h"

@interface IXOrderListInfoCell()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *content;
@property (nonatomic, strong)UIView *lineView;

@end

@implementation IXOrderListInfoCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self lineView];
}

- (UILabel *)title
{
    if (!_title) {
        _title =  [IXCustomView createLable:CGRectMake(10,10, kScreenWidth/2 - 10, 12)
                                      title:@""
                                       font:PF_MEDI(12)
                                 wTextColor:0x99abba
                                 dTextColor:0x8395a4
                              textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)content
{
    if (!_content) {
        _content =  [IXCustomView createLable:CGRectMake(kScreenWidth/2,10, kScreenWidth/2 - 10, 12)
                                        title:@""
                                         font:PF_MEDI(12)
                                    wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_content];
    }
    return _content;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 29 - kLineHeight, kScreenWidth, kLineHeight)];
        _lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)config:(IXOrderModel *)model title:(NSString *)title indexPathRow:(NSInteger)row
{
    self.title.text = title;
    self.content.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    NSString *volume,*price;
    if (model.order.type == item_order_etype_TypeOpen) {
        price = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.order.refPrice] WithDigits:model.symbolModel.digits];
    } else {
        price = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.order.requestPrice] WithDigits:model.symbolModel.digits];
    }
    volume = [NSString stringWithFormat:@"%@%@",[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.order.requestVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits],[IXDataProcessTools showCurrentUnitLanName:model.symbolModel.unitLanName]];
    self.lineView.hidden = YES;
    switch (row) {
        case 0:{
            self.content.text = [NSString stringWithFormat:@"%@",[IXDataProcessTools orderTypeByType:model.order.type]];
            self.content.font = PF_MEDI(12);
        }
            break;
        case 1:{
            if (model.order.direction == 1) {
                self.content.text = LocalizedString(@"买入");
                self.content.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            } else {
                self.content.text = LocalizedString(@"卖出");
                self.content.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
            }
            self.content.font = PF_MEDI(12);
        }
            break;
        case 2:
            self.content.text = volume;
            self.content.font = RO_REGU(12);
            if ([[IXDataProcessTools showCurrentUnitLanName:model.symbolModel.unitLanName] length]) {
                [IXDataProcessTools resetLabel:self.content leftContent:[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.order.requestVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits] leftFont:12 WithContent:volume rightFont:12 fontType:YES];
            }
            break;
        case 3:
            self.content.text = price;
            self.content.font = RO_REGU(12);
            break;
        case 4:
            self.content.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.order.stopLoss] WithDigits:model.symbolModel.digits];
            self.content.font = RO_REGU(12);
            break;
        case 5:{
            self.content.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.order.takeProfit] WithDigits:model.symbolModel.digits];
            self.content.font = RO_REGU(12);
        }
            break;
        case 6:
            self.content.text = [IXEntityFormatter timeIntervalToString:model.order.requestTime];
            self.content.font = RO_REGU(12);
            break;
        case 7:
            self.content.text = [NSString stringWithFormat:@"%lld",model.order.id_p];
            self.content.font = RO_REGU(12);
            break;
        case 8:{
            
            if (model.order.expireType == item_order_eexpire_ExpireDaily) {
                
                self.content.text = LocalizedString(@"当日");
            } else if (model.order.expireType == item_order_eexpire_ExpireWeekly) {
                
                self.content.text = LocalizedString(@"当周");

            } else {
                self.content.text = @"--";
            }
            self.lineView.hidden = NO;
            self.content.font = RO_REGU(12);
        }
            break;
        default:
            break;
    }
}

@end
