//
//  IXPositionFilterV.h
//  IXApp
//
//  Created by Seven on 2017/11/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 筛选类型 */
typedef NS_ENUM(NSInteger, PositionFilterType) {
    PositionFilterTypeAll = 100,   //全部
    PositionFilterTypebuy,         //买入
    PositionFilterTypeSell,        //卖出
};

typedef void(^positionFilterBlock)(PositionFilterType type, NSArray * selectedArr, BOOL dismiss);

@interface IXPositionFilterV : UIView

@property (nonatomic, strong) NSArray       * itemArr;
@property (nonatomic, assign) PositionFilterType    dirOp;  //已选择的筛选条件 - 买卖方向
@property (nonatomic, strong) NSMutableArray        * productArr;   //已选择的筛选条件 - 产品id
@property (nonatomic, readonly) BOOL        isShow;
@property (nonatomic, copy) positionFilterBlock filterB;

- (void)show;
- (void)dismiss;
- (void)resetAction;

@end
