//
//  IXPositionResultVC.h
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXPositionM.h"


/** 开仓结果 */
@interface IXPositionResultVC : IXDataBaseVC

@property (nonatomic, strong)IXPositionM *model;

@end
