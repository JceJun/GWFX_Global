//
//  IXUpdateOrderResultCell.h
//  IXApp
//
//  Created by Evn on 17/3/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXOpenResultModel.h"

@interface IXUpdateOrderResultCell : UITableViewCell
- (void)reloadUIData:(IXOpenResultModel *)model;
@end
