//
//  IXPositionInfoCell.h
//  IXApp
//
//  Created by Evn on 16/12/26.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXPositionM.h"

@interface IXPositionInfoCell : UITableViewCell

- (void)config:(IXPositionM *)model title:(NSString *)title indexPathRow:(NSInteger)row;
- (void)refreshMargin:(double)margin;

@end
