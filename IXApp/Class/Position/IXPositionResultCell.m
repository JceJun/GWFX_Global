//
//  IXPositionResultCell.m
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionResultCell.h"

@implementation IXPositionResultCell

- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 80)/2, 45, 80, 80)];
        [self.contentView addSubview:_icon];
        [_icon makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(-70);
            make.centerY.equalTo(0);
        }];
    }
    return _icon;
}

- (UILabel *)labelU
{
    if (!_labelU) {
        _labelU = [IXUtils createLblWithFrame:CGRectMake( 0, 145, kScreenWidth, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x4c6072
                                   dTextColor:0xffffff
                   ];
        [self.contentView addSubview:_labelU];
        [_labelU makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.icon.right).offset(5);
            make.centerY.equalTo(-10);
        }];
    }
    return _labelU;
}

- (UILabel *)labelD
{
    if (!_labelD) {
        _labelD = [IXUtils createLblWithFrame:CGRectMake( 0, 170, kScreenWidth, 15)
                                     WithFont:PF_MEDI(12)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x99abba
                                   dTextColor:0x8395a4
                   ];
        [self.contentView addSubview:_labelD];
        [_labelD makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.icon.right).offset(20);
            make.centerY.equalTo(10);
        }];
    }
    return _labelD;
}


@end
