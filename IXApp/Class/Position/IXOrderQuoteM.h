//
//  IXOrderQuoteM.h
//  IXApp
//
//  Created by Evn on 2017/6/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXUpdateOrderVC.h"

@interface IXOrderQuoteM : NSObject

//订阅数据
+ (NSMutableArray *)subscribeDynamicPrice:(IXUpdateOrderVC *)root;
+ (void)cancelDynamicPrice:(IXUpdateOrderVC *)root;
+ (void)loadDetailQuote:(IXUpdateOrderVC *)root;
+ (void)cancelDetailQuote:(IXUpdateOrderVC *)root;

@end
