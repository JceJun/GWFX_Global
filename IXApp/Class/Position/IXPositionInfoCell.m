//
//  IXPositionInfoCell.m
//  IXApp
//
//  Created by Evn on 16/12/26.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXPositionInfoCell.h"
#import "IXDateUtils.h"
#import "NSString+FormatterPrice.h"
#import "IXAppUtil.h"

@interface IXPositionInfoCell()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *content;
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UIView *lineView;

@end

@implementation IXPositionInfoCell

- (UILabel *)title
{
    if (!_title) {
        _title =  [IXCustomView createLable:CGRectMake(10,10, kScreenWidth/2 - 10, 12)
                                      title:@""
                                       font:PF_MEDI(12)
                                 wTextColor:0x99abba
                                 dTextColor:0x8395a4
                              textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)content
{
    if (!_content) {
        _content =  [IXCustomView createLable:CGRectMake(kScreenWidth/2,10, kScreenWidth/2 - 10, 12)
                                        title:@""
                                         font:RO_REGU(12)
                                   wTextColor:0x4c6072
                                   dTextColor:0x8395a4
                                textAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_content];
    }
    return _content;
}

- (UIView *)uLine
{
    if (!_uLine) {
        _uLine = [[UIView alloc] initWithFrame:CGRectMake(10, 0, kScreenWidth  - 20, kLineHeight)];
        _uLine.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:_uLine];
    }
    return _uLine;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 28, kScreenWidth, kLineHeight)];
        _lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)config:(IXPositionM *)model title:(NSString *)title indexPathRow:(NSInteger)row
{
    self.title.text = title;
    self.content.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
    self.lineView.hidden = YES;
    self.uLine.hidden = YES;
    switch (row) {
        case 0:{
            if (model.position.direction == 1) {
                self.content.text = LocalizedString(@"买入");
                self.content.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
            } else {
                self.content.text = LocalizedString(@"卖出");
                self.content.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
            }
            self.content.font = PF_MEDI(12);
            self.uLine.hidden = NO;
        }
            break;
        case 1:{
            self.content.text = [NSString stringWithFormat:@"%@%@",[NSString thousandFormate:[IXDataProcessTools showCurrentVolume: model.position.volume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits],[IXDataProcessTools showCurrentUnitLanName:model.symbolModel.unitLanName]];
            self.content.font = RO_REGU(12);
        }
            break;
        case 2:{
            self.content.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.position.openPrice] WithDigits:model.symbolModel.digits];
            self.content.font = RO_REGU(12);
        }
            break;
        case 3:{
            self.content.text = [IXEntityFormatter timeIntervalToString:model.position.openTime];
            self.content.font = RO_REGU(12);
        }
            break;
        case 4:{
            self.content.text = [NSString stringWithFormat:@"%lld",model.position.id_p];
            self.content.font = RO_REGU(12);
        }
            break;
        case 5:{
            self.content.text = [IXDataProcessTools moneyFormatterComma:model.position.swap positiveNumberSign:YES];
            self.content.font = RO_REGU(12);
        }
            break;
        case 6:{
            if (model.position.commission == 0) {
                self.content.text = LocalizedString(@"免费");
                self.content.font = PF_MEDI(12);
            } else if (model.position.commission > 0){
                self.content.text = [IXDataProcessTools moneyFormatterComma:model.position.commission positiveNumberSign:YES];
                self.content.font = RO_REGU(12);
            } else {
                self.content.text = [IXDataProcessTools moneyFormatterComma:model.position.commission positiveNumberSign:NO];
                self.content.font = RO_REGU(12);
            }
        }
            break;
        case 7:{
            if ( model.margin == 0) {
               self.content.text = @"0.00";
            } else {
                self.content.text = [IXDataProcessTools moneyFormatterComma:model.margin positiveNumberSign:YES];
            }
            self.content.font = RO_REGU(12);
        }
            break;
        default:
            break;
    }
}

- (void)refreshMargin:(double)margin
{
    self.content.text = [IXDataProcessTools moneyFormatterComma:margin positiveNumberSign:YES];
}

@end
