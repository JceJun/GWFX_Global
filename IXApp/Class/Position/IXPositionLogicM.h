//
//  IXPositionLogicM.h
//  IXApp
//
//  Created by Evn on 2017/6/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXTradeVM.h"
#import "IXPositionInfoVC.h"

@interface IXPositionLogicM : NSObject

+ (void)updateTakeprofitWithTag:(ROWNAME)tag WithRoot:(IXPositionInfoVC *)root;

+ (void)updateStoplossWithTag:(ROWNAME)tag WithRoot:(IXPositionInfoVC *)root;

@end
