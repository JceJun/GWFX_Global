//
//  IXUpdateOrderResultVC.h
//  IXApp
//
//  Created by Evn on 17/2/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXTradeMarketModel;
@interface IXUpdateOrderResultVC : IXDataBaseVC
@property (nonatomic,strong) IXTradeMarketModel *model;
@end
