//
//  IXPositionResultVC.m
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionResultVC.h"
#import "IXPositionResultCell.h"
#import "IXPositionResultCellA.h"
#import "IXPositionResultCellB.h"
#import "IXTradeMarketModel.h"
#import "NSString+FormatterPrice.h"
#import "IxProtoDeal.pbobjc.h"
#import "IxItemDeal.pbobjc.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "IXBORequestMgr+BroadSide.h"
#import "FXWheellV.h"
#import "IXADMgr.h"
#import "NSObject+IX.h"

typedef enum {
    IXPositionResultWaitting,
    IXPositionResultSuccess,
    IXPositionResultFailed
}IXPositionResultStatus;
@interface IXPositionResultVC ()<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, assign) IXPositionResultStatus status;
@property (nonatomic, copy)   NSString *infoStr;
@property (nonatomic, copy)   NSString *orderID;
@property (nonatomic, strong) item_order *order;
@property (nonatomic, strong) IXPositionRstM *rModel;
@property (nonatomic, strong)FXWheellV  * wheelV;//轮播图
@property(nonatomic,assign,getter=isOrderUpdate)BOOL orderUpdate;
@end

@implementation IXPositionResultVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ORDER_ADD);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_DEAL_ADD);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ORDER_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_DEAL_ADD);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:YES];

    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(rightBtnItemClicked)];
    
    self.title = LocalizedString(@"新交易");
    self.fd_interactivePopDisabled = YES;
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    
    [self initData];
    [self.view addSubview:self.tableV];
    [self loadAd];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    for( UIView *subView in self.navigationItem.titleView.subviews ){
        if ( [subView isKindOfClass:[UILabel class]] ) {
            CGPoint center = subView.center;
            center.x = kScreenWidth/2 - 18;
            subView.center = center;
        }
    }
}

- (void)initData {
    
    _rModel = [[IXPositionRstM alloc] init];
    _rModel.name = _model.symbolModel.languageName;
    _rModel.dir = _model.position.direction;
    _rModel.openPrice = [NSString formatterPrice:[NSString stringWithFormat:@"%f",_model.position.openPrice]
                                      WithDigits:_model.symbolModel.digits];
    _rModel.openDate = [IXEntityFormatter timeIntervalToString:_model.position.openTime];
    _rModel.swap = [NSString stringWithFormat:@"%.2f",_model.position.swap];
    _rModel.cate = LocalizedString(@"平仓");
    if ( _model.closeBy ) {
        _rModel.cate = LocalizedString(@"对冲平仓");
    }
    _rModel.unitLanName = _model.symbolModel.unitLanName;
    if (self.status != IXPositionResultSuccess) {
        _rModel.num = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:[_model.requestVolume doubleValue] contractSizeNew:_model.symbolModel.contractSizeNew volDigit:_model.symbolModel.volDigits] withDigits:_model.symbolModel.volDigits];
        _rModel.closePrice =  [NSString formatterPrice:[NSString stringWithFormat:@"%@",_model.requestPrice]
                                       WithDigits:_model.symbolModel.digits];
        _rModel.closeDate = [IXEntityFormatter timeIntervalToString:_model.position.openTime];
    } else {
        _rModel.num = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:self.order.requestVolume contractSizeNew:_model.symbolModel.contractSizeNew volDigit:_model.symbolModel.volDigits] withDigits:_model.symbolModel.volDigits];
        _rModel.closePrice = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.order.executePrice]
                                      WithDigits:_model.symbolModel.digits];
        _rModel.closeDate = [IXEntityFormatter timeIntervalToString:self.order.requestTime];
    }
}

- (void)updateData
{
    _rModel.num = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:self.order.requestVolume contractSizeNew:_model.symbolModel.contractSizeNew volDigit:_model.symbolModel.volDigits] withDigits:_model.symbolModel.volDigits];
    _rModel.closePrice = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.order.executePrice]
                                  WithDigits:_model.symbolModel.digits];
    _rModel.closeDate = [IXEntityFormatter timeIntervalToString:self.order.requestTime];
}

- (void)rightBtnItemClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UITableView *)tableV
{
    if ( !_tableV ) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableV;
}

#pragma mark -
#pragma mark - UITableviewDelegate&&datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
            case 0:
            return 85;
            break;
        default:
            return 344;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (row == 0) {
        static NSString *identifier = @"IXPositionResultCell";
        IXPositionResultCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[IXPositionResultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.labelU.text = self.infoStr;
        cell.labelD.text = self.orderID;
        [IXDataProcessTools resetLabel:cell.labelD leftContent:LocalizedString(@"订单号：") leftFont:12 WithContent:self.orderID rightFont:12 fontType:NO];
        cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        switch (self.status) {
                case IXPositionResultWaitting:{
                    cell.icon.dk_imagePicker = DKImageNames(@"ps_close_progress-iphone", @"ps_close_progress-iphone");
                }
                break;
                case IXPositionResultSuccess:{
                    cell.icon.dk_imagePicker = DKImageNames(@"ps_close_success-iphone", @"ps_close_success-iphone");
                }
                break;
                case IXPositionResultFailed:{
                    cell.icon.dk_imagePicker = DKImageNames(@"ps_close_failure-iphone", @"ps_close_failure-iphone");
                }
                break;
            default:
                break;
        }
        return cell;
    } else {
        
        if (self.status != IXPositionResultSuccess) {
            
            static NSString *identifier = @"IXPositionResultCellA";
            IXPositionResultCellA *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                
                cell = [[IXPositionResultCellA alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            }
            cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell reloadUIData:_rModel];
            return cell;
        } else {
            static NSString *identifier = @"IXPositionResultCellB";
            IXPositionResultCellB *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                
                cell = [[IXPositionResultCellB alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            }
            cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell reloadUIData:_rModel];
            return cell;
        }
        
    }
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath, PB_CMD_ORDER_ADD)) {
        proto_order_add *pb = ((IXTradeDataCache *)object).pb_order_add;
        if (pb.order.symbolid != _model.position.symbolid) {
            return;
        }
        if (pb.result != 0) {
            self.status = IXPositionResultFailed;
            self.infoStr = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"平仓失败"),[IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%d",pb.result]]];
            
        } else {
            if (pb.order.status == item_order_estatus_StatusPlaced
                || pb.order.status == item_order_estatus_StatusDealing) {
                self.infoStr = LocalizedString(@"订单已提交");
                self.orderID = [NSString stringWithFormat:@"%@：%llu",LocalizedString(@"订单号"),pb.order.id_p];
                self.status = IXPositionResultWaitting;
            } else if (pb.order.status == item_order_estatus_StatusFilled) {
                self.infoStr = LocalizedString(@"订单已执行");
                self.status = IXPositionResultSuccess;
                self.order = pb.order;
                [self updateData];
            } else {
                self.infoStr = LocalizedString(@"订单提交失败");
                self.status = IXPositionResultFailed;
            }
        }
        [self.tableV reloadData];
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_ORDER_UPDATE)) {
        proto_order_update *pb = ((IXTradeDataCache *)object).pb_order_update;
        if (pb.order.symbolid != _model.position.symbolid) {
            return;
        }
        _rModel.num = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:pb.order.requestVolume contractSizeNew:_model.symbolModel.contractSizeNew volDigit:_model.symbolModel.volDigits] withDigits:_model.symbolModel.volDigits];
        if (pb.result != 0) {
            self.status = IXPositionResultFailed;
            self.infoStr = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"平仓失败"),[IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%d",pb.result]]];
        } else {
            if (pb.order.status == item_order_estatus_StatusFilled) {
                _orderUpdate = YES;
                self.infoStr = LocalizedString(@"订单已执行");
                self.status = IXPositionResultSuccess;
                self.order = pb.order;
                [self updateData];
            } else if (pb.order.status == item_order_estatus_StatusDealing) {
                self.infoStr = LocalizedString(@"订单已提交");
                self.orderID = [NSString stringWithFormat:@"%@：%llu",LocalizedString(@"订单号"),pb.order.id_p];
                self.status = IXPositionResultWaitting;
            }
        }
        [self.tableV reloadData];
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_DEAL_ADD)){
        proto_deal_add *pb = ((IXTradeDataCache *)object).pb_deal_add;
        if (pb.deal.symbolid != _model.symbolModel.id_p) {
            return;
        }
        if (pb.result == 0) {
            _rModel.num = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:pb.deal.execVolume contractSizeNew:_model.symbolModel.contractSizeNew volDigit:_model.symbolModel.volDigits] withDigits:_model.symbolModel.volDigits];
            _rModel.profit = pb.deal.profit;
            [self.tableV reloadData];
        }
    }
}

#pragma mark 加载广告数据
- (void)loadAd
{
    NSDictionary *paramDic = @{
                               @"pageNo":@(1),
                               @"pageSize":@(20),
                               @"infomationType":@"OFFSETSUCCESSPAGE"
                               };
    
    weakself;
    [IXBORequestMgr bs_bannerAdvertisementListWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
     {
         if (success && [obj ix_isDictionary] && obj[@"resultList"]) {
             [IXADMgr shareInstance].positionResArr = [obj[@"resultList"] mutableCopy];
             [IXADMgr shareInstance].positionResBannerClose = [obj[@"bannerClose"] boolValue];
             if ([IXADMgr shareInstance].positionResArr.count) {
                 _tableV.tableFooterView = weakSelf.wheelV;
             }
         }
     }];
}

- (FXWheellV *)wheelV
{
    if (!_wheelV) {
        _wheelV = [[FXWheellV alloc] initWithFrame:CGRectMake(0,
                                                              kScreenHeight
                                                              - 94,
                                                              kScreenWidth,
                                                              94)];
    }
    _wheelV.items = [IXADMgr shareInstance].positionResArr;
    _wheelV.bannerClose = [IXADMgr shareInstance].positionResBannerClose;
    return _wheelV;
}




@end
