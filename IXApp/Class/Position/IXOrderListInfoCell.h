//
//  IXOrderListInfoCell.h
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXOrderModel.h"

@interface IXOrderListInfoCell : UITableViewCell

- (void)config:(IXOrderModel *)model title:(NSString *)title indexPathRow:(NSInteger)row;

@end
