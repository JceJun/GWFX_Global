//
//  IXPositionRstM.h
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXPositionRstM : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) item_order_edirection dir;
@property (nonatomic, strong) NSString *cate;
@property (nonatomic, strong) NSString *num;
@property (nonatomic, strong) NSString *closePrice;
@property (nonatomic, strong) NSString *closeDate;
@property (nonatomic, strong) NSString *margin;//保证金
@property (nonatomic, strong) NSString *commission;//佣金
@property (nonatomic, strong) NSString *unitLanName;//产品单位
@property (nonatomic, strong) NSString *openPrice;
@property (nonatomic, strong) NSString *openDate;
@property (nonatomic, strong) NSString *swap;
@property (nonatomic, assign) double profit;

@end
