//
//  IXHedgePosVC.m
//  IXApp
//
//  Created by Bob on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXHedgePosVC.h"
#import "IXDetailSymbolCell.h"
#import "IXHedgeSendCell.h"
#import "IXHedgeReciveCell.h"
#import "UIImageView+SepLine.h"
#import "IXCpyConfig.h"
#import "IXAlertVC.h"
#import "IXPositionResultVC.h"
#import "IXUserInfoMgr.h"
#import "IxProtoHeader.pbobjc.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBGlobal.h"
#import "IXAccountBalanceModel.h"
#import "IXUserDefaultM.h"
#import "NSString+FormatterPrice.h"

@interface IXHedgePosVC ()<UITableViewDelegate,UITableViewDataSource,ExpendableAlartViewDelegate>

@property (nonatomic, strong) UITableView *contentTV;

@property (nonatomic, assign) IXSymbolTradeState tradeState;

@property (nonatomic, strong) NSMutableArray *dataSourceArr;

@property (nonatomic, strong) UIButton *closeBtn;

@property (nonatomic, assign) NSInteger marketId;

@property (nonatomic, assign) double nLastClosePrice;

@property (nonatomic, assign) BOOL isVolNum;

@property (nonatomic, assign) NSInteger chooseCloseIndex;

@end

@implementation IXHedgePosVC

#pragma mark life cycle
- (id)init
{
    self = [super init];
    if ( self ) {
        _marketId = -1;
        _isVolNum = ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount );

        IXTradeData_listen_regist(self, PB_CMD_POSITION_LIST);
        IXTradeData_listen_regist(self, PB_CMD_POSITION_UPDATE);

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(refreashPositionQuote:)
                                                     name:kRefreashProfit
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_POSITION_LIST);
    IXTradeData_listen_regist(self, PB_CMD_POSITION_UPDATE);

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    
    self.title = LocalizedString(@"对冲平仓");
    _chooseCloseIndex = 2;
}

- (void)rightBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark logic dataSource
- (void)setPosModel:(IXPositionM *)posModel
{
    _posModel = posModel;
    _tradeState = [IXDataProcessTools symbolIsCanTrade:_posModel.symbolModel];
    
    [self subscribeLastClosePrice];
    [self initDataSource];
}

- (NSInteger)getMarketId
{
    if (_marketId == -1) {
        _marketId = [[[IXDBSymbolHotMgr
                       querySymbolMarketIdsBySymbolId:_posModel.symbolModel.id_p] objectForKey:kMarketId] integerValue];
    }
    return _marketId;
}

- (void)initDataSource
{
    [self resetSendModel];
    
    for ( item_position *pos in [IXTradeDataCache shareInstance].pb_cache_position_list ) {
        if ( pos.symbolid == _posModel.symbolModel.id_p ) {
            if ( pos.direction != _posModel.position.direction ) {
                IXHedgeM *model = [self conModelWithPos:pos];
                [self.dataSourceArr addObject:model];
            }
        }
    }
    [self.contentTV reloadData];
    [self.view addSubview:self.closeBtn];
}

- (void)resetSendModel
{
    self.dataSourceArr = [NSMutableArray array];
    IXHedgeM *model = [self conModelWithPos:_posModel.position];
    [self.dataSourceArr addObject:model];
}

- (IXHedgeM *)conModelWithPos:(item_position *)pos
{
    IXHedgeM *model = [[IXHedgeM alloc] init];
    model.posDir = ( pos.direction == item_position_edirection_DirectionBuy) ?
    LocalizedString(@"买.") :
    LocalizedString(@"卖.");
    
    model.posId = [NSString stringWithFormat:@"%llu",pos.id_p];

    model.posVol = [NSString stringWithFormat:@"%.f",pos.volume];
    
    if ( _isVolNum || 0 == _posModel.symbolModel.contractSizeNew ) {
        model.showVol = [NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",pos.volume] WithDigits:_posModel.symbolModel.volDigits] withDigits:_posModel.symbolModel.volDigits];
    }else if( 0 != _posModel.symbolModel.contractSizeNew ){
        model.showVol = [NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",pos.volume/_posModel.symbolModel.contractSizeNew] withDigits:2];
    }
    
    model.posPrc = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",pos.openPrice]
                                 WithDigits:_posModel.symbolModel.digits];
    
    return model;
}


#pragma mark kvo
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if(IXTradeData_isSameKey(keyPath,PB_CMD_POSITION_LIST)) {
        proto_position_list  *pb = ((IXTradeDataCache *)object).pb_position_list;
        [self refreshListData:pb.positionArray];
    } else if(IXTradeData_isSameKey(keyPath,PB_CMD_POSITION_UPDATE)) {
        proto_position_update  *pb = ((IXTradeDataCache *)object).pb_position_update;
        if (pb.result == 0) {
            [self refreshUpdateData:pb];
        }
    }
}


#pragma mark 刷新UI
- (void)refreashPositionQuote:(NSNotification *)notify
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *obj = notify.object;
        if ( [obj isKindOfClass:[NSDictionary class]] &&
            [[obj allKeys] containsObject:NOTIFYITEM] ) {
            [self.contentTV reloadData];
        }
    });
}

- (void)refreshListData:(NSArray *)listArr
{
    if (!listArr || listArr.count == 0) {
        return;
    }
    
    if( !self.dataSourceArr.count ){
        self.dataSourceArr = [[NSMutableArray array] mutableCopy];
    }
    
    for ( item_position *pos in listArr ) {
        if ( pos.symbolid == _posModel.symbolModel.id_p ) {
            if ( pos.direction != _posModel.position.direction ) {
                IXHedgeM *model = [self conModelWithPos:_posModel.position];
                [self.dataSourceArr addObject:model];
            }
        }
    }
    [self.contentTV reloadData];
}

- (void)refreshUpdateData:(proto_position_update *)pb
{
    if ( !self.dataSourceArr.count || pb.position.symbolid != _posModel.position.id_p ) {
        return;
    }
    
    //如果发起对冲的持仓被平仓了
    if ( pb.position.id_p == _posModel.position.id_p ) {
        //全部平仓则返回上一页
        if (pb.position.status == item_position_estatus_StatusClosed){
            [self.navigationController popViewControllerAnimated:YES];
            //部分平仓则更新信息
        }else if(pb.position.status == item_position_estatus_StatusOpened){
            _posModel.position = [pb.position mutableCopy];
            [self.contentTV reloadData];
        }
        return;
    }
    
    //如果被请求对冲到持仓更新，则更新表单数据
    NSInteger index = -1;
    for ( item_position *pos in self.dataSourceArr ) {
        index++;
        if ( pos.id_p == pb.position.id_p ) {
            if (pb.position.status == item_position_estatus_StatusClosed) {//全平
                [self.dataSourceArr removeObjectAtIndex:index];
                if ( index == _chooseCloseIndex ) {
                    _chooseCloseIndex = -1;
                }
            } else if (pb.position.status == item_position_estatus_StatusOpened) {//部分
                IXHedgeM *model = [self conModelWithPos:pb.position];
                [_dataSourceArr replaceObjectAtIndex:index withObject:model];
            }
            break;
        }
    }
    [self.contentTV reloadData];
}

#pragma mark 订阅行情和行情回调
//订阅昨收价
- (void)subscribeLastClosePrice
{
    if (_posModel && _posModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@([self getMarketId]),
                           @"id":@(_posModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:arr];
    }
}

//行情回调
- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    switch (cmd) {
        case CMD_QUOTE_CLOSE_PRICE:
            [self processDynCloseQuote:arr];
            break;
        case CMD_QUOTE_SUB:{
            [self processDynQuote:arr];
        }
            break;
        default:
            break;
    }
}

//处理昨收价
- (void)processDynCloseQuote:(NSMutableArray *)resultAry
{
    for (IXLastQuoteM *model in resultAry) {
        if (model.symbolId == _posModel.symbolModel.id_p) {
            _nLastClosePrice = model.nLastClosePrice;
            for (UITableViewCell *cell in [_contentTV visibleCells]) {
                if ([cell isKindOfClass:[IXDetailSymbolCell class]]) {
                    ((IXDetailSymbolCell *)cell).nLastClosePrice = _nLastClosePrice;
                    break;
                }
            }
            break;
        }
    }
}

//处理动态行情
- (void)processDynQuote:(NSMutableArray *)resultAry
{
    for (IXQuoteM *quoteModel in resultAry) {
        if (quoteModel.symbolId == _posModel.symbolModel.id_p) {
            _posModel.quoteModel = quoteModel;
            for (UITableViewCell *cell in [_contentTV visibleCells]) {
                if ([cell isKindOfClass:[IXDetailSymbolCell class]]) {
                    ((IXDetailSymbolCell *)cell).quoteModel = _posModel.quoteModel;
                    break;
                }
            }
            break;
        }
    }
}



#pragma mark popOut View
- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (alertView.tag == 0) {
        if (btnIndex == 1) {
            [self showLoadStatus];
        }
    } else if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    return YES;
}

- (void)showLoadStatus
{
    IXPositionResultVC *VC = [[IXPositionResultVC alloc] init];
    _posModel.closeBy = YES;
    VC.model = _posModel;
    [self.navigationController pushViewController:VC animated:YES];
    
    proto_order_add *proto = [self packageOrder];
    if (!proto) {
        
        //暂时
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LocalizedString(@"平仓")
                                                            message:LocalizedString(@"平仓失败")
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        [alertView show];
    }
    [[IXTCPRequest shareInstance] orderWithParam:proto];
}

//平仓
- (proto_order_add *)packageOrder
{
    proto_order_add *proto = [[proto_order_add alloc] init];
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    
    item_order *order = [[item_order alloc] init];
    order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    order.symbol  =  _posModel.symbolModel.name;
    order.symbolid = _posModel.symbolModel.id_p;
    order.createTime = [IXEntityFormatter getCurrentTimeInterval];
    order.clientType = [IXUserInfoMgr shareInstance].itemType;
    order.positionid = _posModel.position.id_p;

    IXHedgeM *m = _dataSourceArr[_chooseCloseIndex - 1];
    order.closebyPosid = [m.posId integerValue];
    
    if (_posModel.position.direction == item_position_edirection_DirectionBuy) {
        
        order.direction = item_position_edirection_DirectionBuy;
        if (_posModel.quoteModel.SellPrc.count > 0) {
            
            order.requestPrice = [_posModel.quoteModel.SellPrc[0] doubleValue];
        } else {
            
            return nil;
        }
        
    }else{
        
        order.direction = item_position_edirection_DirectionSell;
        if (_posModel.quoteModel.BuyPrc.count > 0) {
            
            order.requestPrice = [_posModel.quoteModel.BuyPrc[0] doubleValue];
        } else {
            
            return nil;
        }
    }
    
    order.type = item_order_etype_TypeCloseBy;

    //手动控制最小值
    long vol = _posModel.position.volume;
    order.requestVolume = ( vol >= [m.posVol integerValue] ) ? [m.posVol integerValue]  : vol;
    proto.order = order;
    return proto;
}

#pragma mark tableView delegate method
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.row ) {
        case 0:
            return 73;
            break;
        case 1:
            return 45;
        default:
            return 60;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + self.dataSourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (indexPath.row) {
        case 0:{
            return [self configDetailSymbolCell:tableView];
        }
            break;
        case 1:{
            return [self configSendCell:tableView WithIndex:indexPath];
        }
            break;
        default:{
            return [self configRecCell:tableView WithIndex:indexPath];
        }
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row > 1 ) {
        _chooseCloseIndex = indexPath.row;
        [self.contentTV reloadData];
    }
}

#pragma mark 配置cell
- (IXDetailSymbolCell *)configDetailSymbolCell:(UITableView *)tableView
{
    IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.tradeState = _tradeState;
    cell.nLastClosePrice = _nLastClosePrice;
    cell.symbolModel = _posModel.symbolModel;
    cell.quoteModel = _posModel.quoteModel;
    cell.marketId = [self getMarketId];
    
    return cell;
}

- (IXHedgeSendCell *)configSendCell:(UITableView *)tableView WithIndex:(NSIndexPath *)indexPath
{
    IXHedgeSendCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXHedgeSendCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);

    UIImageView *bottom =  [UIImageView addSepImageWithFrame:CGRectMake( 0, 35 - kLineHeight, kScreenWidth, kLineHeight)
                                                   WithColor:AutoNightColor(0xe2eaf2 ,0x262f3e)
                                                   WithAlpha:1.0];
    [cell.contentView addSubview:bottom];
    
    IXHedgeM *model = self.dataSourceArr[indexPath.row - 1];
    cell.model = model;
    
    PFTModel *pftM = [[IXAccountBalanceModel shareInstance] caculateProfitPosId:[model.posId integerValue]];
    cell.profit = [NSString stringWithFormat:@"%.2lf",pftM.profit];
    
    return cell;
}

- (IXHedgeReciveCell *)configRecCell:(UITableView *)tableView WithIndex:(NSIndexPath *)indexPath
{
    IXHedgeReciveCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXHedgeReciveCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    
    IXHedgeM *model = self.dataSourceArr[indexPath.row - 1];
    cell.model = model;
    
    PFTModel *pftM = [[IXAccountBalanceModel shareInstance] caculateProfitPosId:[model.posId integerValue]];
    cell.profit = [NSString stringWithFormat:@"%.2lf",pftM.profit];

    UIImageView *top =  [UIImageView addSepImageWithFrame:CGRectMake( 0, 0, kScreenWidth, kLineHeight)
                                                WithColor:AutoNightColor(0xe2eaf2,0x242a36)
                                                WithAlpha:1.0];
    [cell.contentView addSubview:top];
    
    if ( indexPath.row == self.dataSourceArr.count ) {
        UIImageView *bottom =  [UIImageView addSepImageWithFrame:CGRectMake( 0, 60 - kLineHeight, kScreenWidth, kLineHeight)
                                                       WithColor:AutoNightColor(0xe2eaf2,0x242a36)
                                                       WithAlpha:1.0];
        [cell.contentView addSubview:bottom];
    }
    
    cell.choosePos = (indexPath.row == _chooseCloseIndex);

    
    weakself;
    cell.hedgeBlock = ^(NSInteger tag) {
        @synchronized ( self ) {
            weakSelf.chooseCloseIndex = tag;
            [weakSelf.contentTV reloadData];
        }
    };
    
    return cell;
}

#pragma mark init moduel
- (UIButton *)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeBtn.frame = CGRectMake(0, kScreenHeight - 44 - kNavbarHeight, kScreenWidth, 44);
        [_closeBtn addTarget:self action:@selector(closePositionBtn) forControlEvents:UIControlEventTouchUpInside];
        [_closeBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
        [_closeBtn setTitle:LocalizedString(@"平 仓") forState:UIControlStateNormal];
        
        _closeBtn.titleLabel.font = PF_REGU(15);
    }
    return _closeBtn;
}

- (void)closePositionBtn
{
    
    [self.view endEditing:YES];
    
    IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"平仓提醒")
                                             message:LocalizedString(@"点击\"确定\"将会进行该交易平仓")
                                         cancelTitle:LocalizedString(@"取消")
                                          sureTitles:LocalizedString(@"确定.")];
    VC.index = 0;
    VC.expendAbleAlartViewDelegate = self;
    [VC showView];
}

- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        
        CGRect frame = self.view.bounds;
        frame.size.height -= (44 + kNavbarHeight);
        _contentTV = [[UITableView alloc] initWithFrame:frame];
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_contentTV registerClass:[IXDetailSymbolCell class] forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        [_contentTV registerClass:[IXHedgeReciveCell class] forCellReuseIdentifier:NSStringFromClass([IXHedgeReciveCell class])];
        [_contentTV registerClass:[IXHedgeSendCell class] forCellReuseIdentifier:NSStringFromClass([IXHedgeSendCell class])];
        [self.view addSubview:_contentTV];
        _contentTV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
     }
    return _contentTV;
}
@end
