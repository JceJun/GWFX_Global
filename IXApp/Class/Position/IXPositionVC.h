//
//  IXPositionVC.h
//  IXApp
//
//  Created by Magee on 16/11/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXPositionVC : IXDataBaseVC
@property (nonatomic, readonly) BOOL  isEditingPosition;  //是否正在编辑持仓
/** 编辑持仓 */
- (void)editPosition;
- (void)endEditPosition;

- (void)dismissFolterView;

@end
