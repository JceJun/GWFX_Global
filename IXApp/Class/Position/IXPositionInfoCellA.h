//
//  IXPositionInfoCellA.h
//  IXApp
//
//  Created by Evn on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXPositionM.h"

@interface IXPositionInfoCellA : UITableViewCell

- (void)config:(IXPositionM *)model;
- (void)refreshProfit:(double)profit;

@end
