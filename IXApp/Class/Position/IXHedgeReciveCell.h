//
//  IXHedgeReciveCell.h
//  IXApp
//
//  Created by Bob on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXHedgeM.h"

typedef void(^chooseHedgePosition)(NSInteger tag);

@interface IXHedgeReciveCell : UITableViewCell

@property (nonatomic, strong) IXHedgeM *model;

@property (nonatomic, copy) NSString *profit;

@property (nonatomic, copy) chooseHedgePosition hedgeBlock;

@property (nonatomic, assign) BOOL choosePos;
@end
