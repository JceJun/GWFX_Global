//
//  IXMultiCloseV.h
//  IXApp
//
//  Created by Seven on 2017/11/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kSize CGSizeMake(kScreenWidth, 45)
typedef NS_ENUM(NSInteger, ChooseType) {
    ChooseTypeNone = 0,     //无操作
    ChooseTypeChooseAll,    //全选
    ChooseTypeUnChooseAll,  //取消全选
    ChooseTypeCancel,       //取消
    ChooseTypeClosePos,     //平仓
};

typedef void(^multiChooseBlock)(ChooseType type);

@interface IXMultiCloseV : UIView

@property (nonatomic, copy) multiChooseBlock    chooseB;

- (void)resetChooseBtnState;

@end
