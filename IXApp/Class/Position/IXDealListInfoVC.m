//
//  IXDealListInfoVC.m
//  IXApp
//
//  Created by Evn on 17/1/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDealListInfoVC.h"
#import "IXDealListInfoCell.h"
#import "IXPositionOpenCell.h"
#import "IXSymbolDeepCell.h"
#import "IXDetailSymbolCell.h"
#import "IXSymbolDetailVC.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBSymbolMgr.h"
#import "IXDBLanguageMgr.h"
#import "IXDBGlobal.h"
#import "NSString+FormatterPrice.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "UIImageView+SepLine.h"
#import "IXUserDefaultM.h"

@interface IXDealListInfoVC ()
<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, assign) double nLastClosePrice;
@property (nonatomic, assign) BOOL flag;//是否展开
@end

@implementation IXDealListInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(rightBtnItemClicked)];
    
    self.title = LocalizedString(@"订单详情");
    self.fd_interactivePopDisabled = YES;
    [self.navigationItem setHidesBackButton:YES];
    if (_dealModel.deal.reason == item_order_etype_TypeOpen || _dealModel.deal.reason == item_order_etype_TypeLimit || _dealModel.deal.reason == item_order_etype_TypeStop || _dealModel.deal.reason == item_order_etype_TypeReopen ) {
        if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
            _titleArr = @[LocalizedString(@"交易方向"),
                          LocalizedString(@"开仓时间"),
                          LocalizedString(@"开仓价"),
                          LocalizedString(@"佣金"),
                          LocalizedString(@"利息"),
                          LocalizedString(@"交易数量"),
                          LocalizedString(@"盈亏")];
        } else {
            _titleArr = @[LocalizedString(@"交易方向"),
                          LocalizedString(@"开仓时间"),
                          LocalizedString(@"开仓价"),
                          LocalizedString(@"佣金"),
                          LocalizedString(@"利息"),
                          LocalizedString(@"交易手数"),
                          LocalizedString(@"盈亏")];
        }
        
    } else {
        if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
            _titleArr = @[LocalizedString(@"交易方向"),
                          LocalizedString(@"开仓时间"),
                          LocalizedString(@"开仓价"),
                          LocalizedString(@"平仓时间"),
                          LocalizedString(@"平仓价"),
                          LocalizedString(@"佣金"),
                          LocalizedString(@"利息"),
                          LocalizedString(@"交易数量"),
                          LocalizedString(@"盈亏")];
        } else {
            _titleArr = @[LocalizedString(@"交易方向"),
                          LocalizedString(@"开仓时间"),
                          LocalizedString(@"开仓价"),
                          LocalizedString(@"平仓时间"),
                          LocalizedString(@"平仓价"),
                          LocalizedString(@"佣金"),
                          LocalizedString(@"利息"),
                          LocalizedString(@"交易手数"),
                          LocalizedString(@"盈亏")];
        }
    }
    
    NSDictionary *marketDic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:_dealModel.symbolModel.id_p];
    if (marketDic && marketDic.count > 0) {
        _dealModel.marketId = [marketDic[kMarketId] intValue];
    }
    
    [self tableV];
    [self updateNavBottomLine];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self subscribeDynamicPrice];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self cancelQuote];
    
}

//订阅数据
- (void)subscribeDynamicPrice
{
    if ( _dealModel && _dealModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(_dealModel.marketId),
                           @"id":@(_dealModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] subscribeDetailPriceWithSymbolAry:arr];
        [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:arr];
    }
    
}

//取消订阅
- (void)cancelQuote
{
    if ( _dealModel && _dealModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(_dealModel.marketId),
                           @"id":@(_dealModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] unSubscribeDetailPriceWithSymbolAry:arr];
    }
}

- (void)rightBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - lazy load

- (UITableView *)tableV{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                0,
                                                                kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)];
        _tableV.showsVerticalScrollIndicator = NO;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV registerClass:[IXDealListInfoCell class] forCellReuseIdentifier:NSStringFromClass([IXDealListInfoCell class])];
        [_tableV registerClass:[IXDetailSymbolCell class] forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        [_tableV registerClass:[IXPositionOpenCell class] forCellReuseIdentifier:NSStringFromClass([IXPositionOpenCell class])];
        [_tableV registerClass:[IXSymbolDeepCell class] forCellReuseIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_flag) {
        return _titleArr.count + 7;
    } else {
        return _titleArr.count + 3;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return  73;
    } else {
        if (_flag) {
            if (indexPath.row >= 1 && indexPath.row < 6) {
                return 22;
            } else if (indexPath.row == 6) {
                return 30;
            } else {
                return 29;
            }
        } else {
            if (indexPath.row >= 1 && indexPath.row < 2) {
                return 22;
            } else if (indexPath.row == 2) {
                return 30;
            } else {
                return 29;
            }
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_flag) {
        switch (indexPath.row) {
            case 0: {
                IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
#warning 添加产品缓存
                cell.symbolModel = _dealModel.symbolModel;
                cell.quoteModel = _dealModel.quoteModel;
                cell.marketId = _dealModel.symbolModel.marketID;
                cell.nLastClosePrice = _nLastClosePrice;
                return cell;
            }
                break;
            default: {
                if (indexPath.row >= 1 && indexPath.row <= 5) {
                    IXSymbolDeepCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
                    cell.cellContentHeight = 22;
                    NSInteger tag = (indexPath.row - 1);
                    cell.tag = tag;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    IXQuoteM *quoteModel = _dealModel.quoteModel;
                    if ( quoteModel.BuyPrc.count >= indexPath.row ) {
                        [cell setBuyPrc:[self formatterPrice:quoteModel.BuyPrc[tag]]
                                 BuyVol:[self formatterPrice:quoteModel.BuyVol[tag]]
                                SellPrc:[self formatterPrice:quoteModel.SellPrc[tag]]
                                SellVol:[self formatterPrice:quoteModel.SellVol[tag]]];
                    }
                    return cell;
                } else if (indexPath.row == 6) {
                    IXPositionOpenCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionOpenCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell reloadUI:_flag];
                    return cell;
                } else {
                    IXDealListInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDealListInfoCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell config:_dealModel title:_titleArr[(indexPath.row - 7)] indexPathRow:(indexPath.row - 7)];
                    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
                    return cell;
                }
            }
                break;
        }
    } else {
        switch (indexPath.row) {
            case 0: {
                IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
#warning 添加产品缓存
                cell.symbolModel = _dealModel.symbolModel;
                cell.quoteModel = _dealModel.quoteModel;
                cell.marketId = _dealModel.symbolModel.marketID;
                cell.nLastClosePrice = _nLastClosePrice;
                return cell;
            }
                break;
            default: {
                if (indexPath.row == 1) {
                    IXSymbolDeepCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
                    cell.cellContentHeight = 22;
                    NSInteger tag = (indexPath.row - 1);
                    cell.tag = tag;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    IXQuoteM *quoteModel = _dealModel.quoteModel;
                    if ( quoteModel.BuyPrc.count >= indexPath.row ) {
                        [cell setBuyPrc:[self formatterPrice:quoteModel.BuyPrc[tag]]
                                 BuyVol:[self formatterPrice:quoteModel.BuyVol[tag]]
                                SellPrc:[self formatterPrice:quoteModel.SellPrc[tag]]
                                SellVol:[self formatterPrice:quoteModel.SellVol[tag]]];
                    }
                    return cell;
                } else if (indexPath.row == 2) {
                    IXPositionOpenCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionOpenCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell reloadUI:_flag];
                    return cell;
                } else {
                    IXDealListInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDealListInfoCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell config:_dealModel title:_titleArr[(indexPath.row - 3)] indexPathRow:(indexPath.row - 3)];
                    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
                    return cell;
                }
            }
                break;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        IXSymbolDetailVC *openVC = [[IXSymbolDetailVC alloc] init];
        IXSymModel *model = _dealModel.symbolModel;
        IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
        tradeModel.symbolModel = model;
        if (_dealModel.quoteModel) {
            tradeModel.quoteModel = _dealModel.quoteModel;
        }
        openVC.tradeModel = tradeModel;
        openVC.nLastClosePrice = _nLastClosePrice;
        openVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:openVC animated:YES];
    } else {
        if (!_flag) {
            if (indexPath.row == 2) {
                _flag = !_flag;
                [self.tableV reloadData];
            }
        } else {
            if (indexPath.row == 6) {
                _flag = !_flag;
                [self.tableV reloadData];
            }
        }
    }
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if(cmd == CMD_QUOTE_PUB_DETAIL||
       cmd == CMD_QUOTE_SUB_DETAIL){
        NSMutableArray *resultAry = [NSMutableArray arrayWithArray:arr];
        for (IXQuoteM *model in resultAry) {
            if (model.symbolId == _dealModel.symbolModel.id_p) {
                _dealModel.quoteModel = [model mutableCopy]; 
                [self.tableV reloadData];
                break;
            }
        }
    } else if ( cmd == CMD_QUOTE_CLOSE_PRICE ){
        for ( IXLastQuoteM *model in arr ) {
            if ( model.symbolId == _dealModel.symbolModel.id_p ) {
                _nLastClosePrice = model.nLastClosePrice;
                for ( UITableViewCell *cell in [_tableV visibleCells] ) {
                    if ( [cell isKindOfClass:[IXDetailSymbolCell class]] ) {
                        ((IXDetailSymbolCell *)cell).nLastClosePrice = _nLastClosePrice;
                        break;
                    }
                }
                break;
            }
        }
    }
}

- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price WithDigits:_dealModel.symbolModel.digits];
}

@end
