//
//  IXPositionHeaderV.m
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionHeaderV.h"
#import "UILabel+IX.h"
#import "CAGradientLayer+IX.h"

@interface IXPositionHeaderV ()

@property (nonatomic, strong) UIView    * rateSubV; //进度条右侧白块
@property (nonatomic, assign) CGFloat   rate;

@end

@implementation IXPositionHeaderV

- (instancetype)init
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, kScreenWidth, 119);
        self.dk_backgroundColorPicker = DKNavBarColor;
        [self createSubview];
        [self addShadow];
    }
    return self;
}

- (void)createSubview
{
    NSString * title = LocalizedString(@"保证金比例");
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(13)}];
    _marginLab = [UILabel labelWithFrame:CGRectMake(21, 13, size.width, size.height)
                                    text:title
                                textFont:PF_MEDI(13)
                               textColor:DKCellContentColor];
    [self addSubview:_marginLab];
    
    _marginStateLab = [UILabel labelWithFrame:CGRectMake(CGRectGetMaxX(_marginLab.frame) + 5, 13, 100, size.height)
                                         text:@""
                                     textFont:PF_MEDI(13)
                                    textColor:DKCellTitleColor];
    [self addSubview:_marginStateLab];
    
    _marginRateLab = [UILabel labelWithFrame:CGRectMake(kScreenWidth - 154, 13, 130, size.height)
                                        text:@""
                                    textFont:RO_REGU(15)
                                   textColor:DKCellTitleColor
                               textAlignment:NSTextAlignmentRight];
    [self addSubview:_marginRateLab];
    
    UIView  * bgScrollV = [[UIView alloc] initWithFrame:CGRectMake(0, 44, kScreenWidth, 5)];
    bgScrollV.dk_backgroundColorPicker = DKColorWithRGBs(0xe2e9f1, 0x4c6072);
    [self addSubview:bgScrollV];
    
    _rateV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _rate*kScreenWidth, 5)];
    _rateV.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873,0x50a1e5);
    [bgScrollV addSubview:_rateV];
    
    _rateSubV = [[UIView alloc] initWithFrame:CGRectMake(_rate*kScreenWidth, 0, 3, 5)];
    _rateSubV.dk_backgroundColorPicker = DKNavBarColor;
    _rateSubV.hidden = YES;
    [bgScrollV addSubview:_rateSubV];
    
    //自由资金
    title = @"--";
    size = [title sizeWithAttributes:@{NSFontAttributeName:PF_REGU(15)}];
    _amountLab = [UILabel labelWithFrame:CGRectMake(21, GetView_MaxY(bgScrollV) + 12, 130, size.height)
                                    text:title
                                textFont:RO_REGU(18)
                               textColor:DKCellTitleColor];
    [self addSubview:_amountLab];
    
    //实时盈亏
    _profitLab = [UILabel labelWithFrame:CGRectMake(kScreenWidth/2 + 15, GetView_MinY(_amountLab), 130, size.height)
                                    text:@"--"
                                textFont:RO_REGU(18)
                               textColor:DKColorWithRGBs(mRedPriceColor, mDRedPriceColor)];
    [self addSubview:_profitLab];
    
    title = LocalizedString(@"自由资金");
    size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(12)}];
    UILabel * amtLab = [UILabel labelWithFrame:CGRectMake(21, GetView_MaxY(_amountLab) + 5, 100, size.height)
                                          text:title
                                      textFont:PF_MEDI(12)
                                     textColor:DKCellContentColor];
    [self addSubview:amtLab];
    
    UILabel * profLab = [UILabel labelWithFrame:CGRectMake(GetView_MinX(_profitLab), GetView_MinY(amtLab), 100, size.height)
                                           text:LocalizedString(@"实时盈亏")
                                       textFont:PF_MEDI(12)
                                      textColor:DKCellContentColor];
    [self addSubview:profLab];
}

- (void)addShadow
{
    UIView  * btomV = [[UIView alloc] initWithFrame:CGRectMake(0, 114, kScreenWidth, 5)];
    btomV.dk_backgroundColorPicker = DKTableColor;
    [self addSubview:btomV];
    
    NSArray * colors = @[
                         DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                         DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                         ];
    
    CAGradientLayer * btomL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 114, kScreenWidth, 10)
                                                           dkcolors:colors];
    [self.layer addSublayer:btomL];
    self.layer.masksToBounds = NO;
}

- (void)resetMarginRateWith:(CGFloat)rate dkColor:(DKColorPicker)color
{
    _rateSubV.hidden = rate == 0;
    _rateV.dk_backgroundColorPicker = color;
    
    if (_rate >= 1 && rate >= 1) {
        _rate = rate;
    } else {
        _rate = rate;
        CGFloat width = kScreenWidth * rate;
        
        [UIView animateWithDuration:0.1 animations:^{
            _rateV.frame = CGRectMake(0, 0, width, 5);
            _rateSubV.frame = CGRectMake(width, 0, 3, 5);
        }];
    }
}

@end
