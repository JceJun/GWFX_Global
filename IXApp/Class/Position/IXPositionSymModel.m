//
//  IXPositionSymbolModel.m
//  IXApp
//
//  Created by Evn on 2017/7/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionSymModel.h"
#import "IXDBSymbolMgr.h"
#import "IXDBLanguageMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBG_SMgr.h"
#import "IXDBGlobal.h"
#import "IxProtoSymbol.pbobjc.h"
#import "IxItemSymbol.pbobjc.h"
#import "IxProtoGroupSymbol.pbobjc.h"
#import "IxItemGroupSymbol.pbobjc.h"

@implementation IXPositionSymModel

static IXPositionSymModel *share = nil;

+ (IXPositionSymModel *)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ( !share ) {
            share = [[IXPositionSymModel alloc] init];
        }
    });
    return share;
}

- (id)init
{
    self = [super init];
    if ( self ){
        [self addObserverKey];
        _symDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserverKey];
}

- (void)addObserverKey
{
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_UPDATE);
    IXTradeData_listen_regist(self, PB_CMD_SYMBOL_DELETE);
    IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_UPDATE);
    IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_ADD);
    IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_DELETE);
}

- (void)removeObserverKey
{
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_DELETE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_ADD);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_DELETE);
}

- (void)clearCache
{
    [_symDic removeAllObjects];
}

- (IXSymModel *)getCacheSymbolBySymbolId:(uint64_t)symbolId
{
    if (symbolId == 0) {
        return nil;
    }
    //缓存有从缓存取
    IXSymModel *model = [_symDic objectForKey:[self symbolId:symbolId]];
    if (model) {
        return model;
    } else {
        model = [self querySymbolBySymbolId:symbolId];
        if (model) {
            [_symDic setObject:model forKey:[self symbolId:symbolId]];
        }
        return model;
    }
    return nil;
}

- (void)updateCacheSymbolBySymbolId:(uint64_t)symbolId
{
    if (symbolId == 0) {
        return;
    }
    IXSymModel *model = [self querySymbolBySymbolId:symbolId];
    if (model) {
        [_symDic setObject:model forKey:[self symbolId:symbolId]];
    }
}

- (void)deleteCacheSymbolBySymbolId:(uint64_t)symbolId
{
    [_symDic removeObjectForKey:[self symbolId:symbolId]];
}

- (IXSymModel *)querySymbolBySymbolId:(uint64_t)symbolId
{
    NSDictionary *symDic = [IXDBSymbolMgr querySymbolBySymbolId:symbolId];
    if (symDic && symDic.count > 0) {
        IXSymModel *model = [self symbolModelBySymbolInfo:symDic];
        NSDictionary *languageDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.name];
        if (languageDic && languageDic.count > 0) {
            if (languageDic && languageDic.count > 0) {
                if (languageDic[kValue] && [languageDic[kValue] length] > 0) {
                    model.languageName = languageDic[kValue];
                } else {
                    model.languageName = model.name;
                }
            } else {
                model.languageName = model.name;
            }
        } else {
            model.languageName = model.name;
        }
        
        NSDictionary *marketDic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:symbolId];
        if (symbolId == [marketDic[kID] doubleValue]) {
            model.marketID = [marketDic[kMarketId] intValue];
            model.marketName = [IXEntityFormatter getMarketNameWithMarketId:[marketDic[kMarketId] doubleValue]];
        }
        
        NSDictionary *unitLanDic = [IXDBLanguageMgr queryLanguageByCounrtry:[IXLocalizationModel currentCheckLanguage] nameSpace:model.displayName];
        if (unitLanDic && unitLanDic.count > 0) {
            if (unitLanDic && unitLanDic.count > 0) {
                if (unitLanDic[kValue] && [unitLanDic[kValue] length] > 0) {
                    model.unitLanName = unitLanDic[kValue];
                } else {
                    model.unitLanName = model.displayName;
                }
            } else {
                model.unitLanName = model.displayName;
            }
        } else {
            model.unitLanName = model.displayName;
        }
        
        return model;
    } else {
        return nil;
    }
}

- (IXSymModel *)symbolModelBySymbolInfo:(NSDictionary *)symDic
{
    IXSymModel *model = [[IXSymModel alloc] init];
    model.id_p = [[IXDataProcessTools dealWithNil:symDic[kID]] longLongValue];
    model.languageName = [IXDataProcessTools dealWithNil:symDic[kLanguageName]];
    model.source = [IXDataProcessTools dealWithNil:symDic[kSource]];
    model.name = [IXDataProcessTools dealWithNil:symDic[kName]];
    model.contractSizeNew = [[IXDataProcessTools dealWithNil:symDic[kContractSizeNew]] intValue];
    model.digits = [[IXDataProcessTools dealWithNil:symDic[kDigits]] intValue];
    model.displayName = [IXDataProcessTools dealWithNil:symDic[kDisplayName]];
    model.cataId = [[IXDataProcessTools dealWithNil:symDic[kCataId]] longLongValue];
    model.holidayCataId =[[IXDataProcessTools dealWithNil:symDic[kHolidayCataId]] longLongValue];
    model.scheduleCataId = [[IXDataProcessTools dealWithNil:symDic[kScheduleCataId]] longLongValue];
    model.startTime = [[IXDataProcessTools dealWithNil:symDic[kStartTime]] longLongValue];
    model.expiryTime = [[IXDataProcessTools dealWithNil:symDic[kExpiryTime]] longLongValue];
    model.tradable = [[IXDataProcessTools dealWithNil:symDic[KTradable]] intValue];
    model.scheduleDelayMinutes = [[IXDataProcessTools dealWithNil:symDic[kScheduleDelayMinutes]] intValue];
    model.volumesMin = [[IXDataProcessTools dealWithNil:symDic[kVolumesMin]] doubleValue];
    model.volumesMax = [[IXDataProcessTools dealWithNil:symDic[kVolumsMax]] doubleValue];
    model.volumesStep = [[IXDataProcessTools dealWithNil:symDic[kVolumesStep]] doubleValue];
    model.pipsRatio = [[IXDataProcessTools dealWithNil:symDic[kPipsRatio]] intValue];
    model.stopLevel = [[IXDataProcessTools dealWithNil:symDic[kStopLevel]] intValue];
    if (model.stopLevel == 0) {
        model.stopLevel = 1;
    }
    model.maxStopLevel = [[IXDataProcessTools dealWithNil:symDic[kMaxStopLevel]] intValue];
    model.volDigits = [[IXDataProcessTools dealWithNil:symDic[kVolDigits]] longLongValue];

    NSDictionary *groupSymDic = [IXDBG_SMgr queryG_SByAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid symbolId:model.id_p];
    if (groupSymDic && groupSymDic.count > 0) {
        model.symTradable = [[IXDataProcessTools dealWithNil:groupSymDic[KTradable]] integerValue];
        model.isSetGroupSym = YES;
        model.volumesMin = [[IXDataProcessTools dealWithNil:groupSymDic[kVolumesMin]] doubleValue];
        model.volumesMax = [[IXDataProcessTools dealWithNil:groupSymDic[kVolumsMax]] doubleValue];
        model.volumesStep = [[IXDataProcessTools dealWithNil:groupSymDic[kVolumesStep]] doubleValue];
    }
    return model;
}

- (NSString *)symbolId:(uint64)symbolId
{
    return [NSString stringWithFormat:@"%llu",symbolId];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    //symbol
    if (IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_UPDATE)) {
        proto_symbol_update *pb = ((IXTradeDataCache *)object).pb_symbol_update;
        if (pb && pb.result == 0) {
            [self updateCacheSymbolBySymbolId:((item_symbol *)pb.symbol).id_p];
        }
    }
    if (IXTradeData_isSameKey(keyPath, PB_CMD_SYMBOL_DELETE)) {
        proto_symbol_delete *pb = ((IXTradeDataCache *)object).pb_symbol_delete;
        if (pb && pb.result == 0) {
            [self deleteCacheSymbolBySymbolId:pb.id_p];
        }
    }
    //group_symbol
    if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_UPDATE)) {
        proto_group_symbol_update *pb = ((IXTradeDataCache *)object).pb_group_symbol_update;
        if (pb && pb.result == 0 && pb.groupSymbolArray.count) {
            for (item_group_symbol *obj in pb.groupSymbolArray) {
                [self updateCacheSymbolBySymbolId:obj.id_p];
            }
        }
    }
    if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_ADD)) {
        proto_group_symbol_add *pb = ((IXTradeDataCache *)object).pb_group_symbol_add;
        if (pb && pb.result == 0 && pb.groupSymbolArray.count) {
            for (item_group_symbol *obj in pb.groupSymbolArray) {
                [self updateCacheSymbolBySymbolId:obj.id_p];
            }
        }
    }
    if (IXTradeData_isSameKey(keyPath, PB_CMD_GROUP_SYMBOL_DELETE)) {
        proto_group_symbol_delete *pb = ((IXTradeDataCache *)object).pb_group_symbol_delete;
        if (pb && pb.result == 0) {
            [self deleteCacheSymbolBySymbolId:pb.id_p];
        }
    }
}

@end
