//
//  IXUpdateOrderResultVC.m
//  IXApp
//
//  Created by Evn on 17/2/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUpdateOrderResultVC.h"
#import "IXOpenResultCellA.h"
#import "IXOpenResultCellB.h"
#import "IXUpdateOrderResultCell.h"
#import "IXTradeMarketModel.h"
#import "NSString+FormatterPrice.h"
#import "IXOpenResultModel.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "IXOrderListInfoVC.h"

typedef enum {
    IXOpenResultWaitting,
    IXOpenResultSuccess,
    IXOpenResultFailed
}IXOpenResultStatus;

@interface IXUpdateOrderResultVC ()<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, assign) IXOpenResultStatus status;
@property (nonatomic, copy)   NSString *infoStr;
@property (nonatomic, copy)   NSString *orderID;
@property (nonatomic, strong) item_order *order;
@property (nonatomic, strong) IXOpenResultModel *rModel;
@end

@implementation IXUpdateOrderResultVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ORDER_ADD);
        IXTradeData_listen_regist(self, PB_CMD_ORDER_UPDATE);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ORDER_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ORDER_UPDATE);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(rightBtnItemClicked)];
    
    self.title = LocalizedString(@"修改订单");
    self.fd_interactivePopDisabled = YES;
    [self.navigationItem setHidesBackButton:YES];
    
    [self initData];
    [self.view addSubview:self.tableV];
}

- (void)initData {
    
    _rModel = [[IXOpenResultModel alloc] init];
    _rModel.name = _model.symbolModel.languageName;
    _rModel.dir = _model.direction;
    _rModel.cate = _model.requestType;
    _rModel.num = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:[_model.requestVolume doubleValue] contractSizeNew:_model.symbolModel.contractSizeNew volDigit:_model.symbolModel.volDigits] withDigits:_model.symbolModel.volDigits];
    _rModel.reqPrice =  [NSString formatterPrice:[NSString stringWithFormat:@"%@",_model.requestPrice]
                                   WithDigits:_model.symbolModel.digits];
    _rModel.reqDate =  [IXEntityFormatter timeIntervalToString:_model.createTime];
    _rModel.unitLanName = _model.symbolModel.unitLanName;
    self.status = IXOpenResultWaitting;
    self.infoStr = LocalizedString(@"订单已提交");
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:self.view.bounds];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    }
    return _tableV;
}

#pragma mark -
#pragma mark - UITableviewDelegate&&datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
            case 0:
            return 225;
            break;
        default:
            if (self.status != IXOpenResultSuccess) {
                
                return 198;
            } else {
                
                return 200;
            }
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (row == 0) {
        static NSString *identifier = @"IXOpenResultCellA";
        IXOpenResultCellA * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[IXOpenResultCellA alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        cell.labelU.text = self.infoStr;
        cell.labelD.text = self.orderID;
        [IXDataProcessTools resetLabel:cell.labelD leftContent:LocalizedString(@"订单号：") leftFont:12 WithContent:self.orderID rightFont:12 fontType:NO];
        switch (self.status) {
                case IXOpenResultWaitting:{
                    cell.icon.dk_imagePicker = DKImageNames(@"openAccount_wait", @"openAccount_wait_D");
                }
                break;
                case IXOpenResultSuccess:{
                    cell.icon.dk_imagePicker = DKImageNames(@"openAccount_complete", @"openAccount_complete_D");
                }
                break;
                case IXOpenResultFailed:{
                    cell.icon.dk_imagePicker = DKImageNames(@"openAccount_failure", @"openAccount_failure_D");
                }
                break;
            default:
                break;
        }
        
        return cell;
    } else {
        
        if (self.status != IXOpenResultSuccess) {
            static NSString *identifier = @"IXOpenResultCellB";
            IXOpenResultCellB *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[IXOpenResultCellB alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            }
            cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell reloadUIData:_rModel];
            return cell;
        } else {
            
            static NSString *identifier = @"IXUpdateOrderResultCell";
            IXUpdateOrderResultCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                
                cell = [[IXUpdateOrderResultCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            }
            cell.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell reloadUIData:_rModel];
            return cell;
        }
    }
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath, PB_CMD_ORDER_ADD)) {
        
        proto_order_add *pb = ((IXTradeDataCache *)object).pb_order_add;
        if (pb.order.symbolid != _model.symbolModel.id_p) {
            return;
        }
        _rModel.reqDate =  [IXEntityFormatter timeIntervalToString:pb.order.requestTime];
        if (pb.result != 0) {
            self.status = IXOpenResultFailed;
            self.infoStr = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"修改订单失败"),[IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%d",pb.result]]];
        } else {
            
            self.orderID = [NSString stringWithFormat:@"%@：%llu",LocalizedString(@"订单号"),pb.order.id_p];
            if (pb.order.status == item_order_estatus_StatusPlaced) {
                self.infoStr = LocalizedString(@"订单已提交");
            } else if (pb.order.status == item_order_estatus_StatusFilled) {
                self.infoStr = LocalizedString(@"订单已修改");
                self.status = IXOpenResultSuccess;
                self.order = pb.order;
            } else {
                self.infoStr = LocalizedString(@"订单提交失败");
                self.status = IXOpenResultFailed;
            }
        }
        [self.tableV reloadData];
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_ORDER_UPDATE)) {
        proto_order_update *pb = ((IXTradeDataCache *)object).pb_order_update;
        if (pb.order.symbolid != _model.symbolModel.id_p) {
            return;
        }
        if (0 == pb.order.executeTime) {
            
            _rModel.reqDate =  [IXEntityFormatter timeIntervalToString:pb.order.requestTime];
        } else {
            
            _rModel.reqDate =  [IXEntityFormatter timeIntervalToString:pb.order.executeTime];
        }
        if (pb.order.requestPrice) {
            _rModel.reqPrice = [NSString formatterPrice:[NSString stringWithFormat:@"%f",pb.order.requestPrice]
                                          WithDigits:_model.symbolModel.digits];
        }
        if (pb.result != 0) {
            self.status = IXOpenResultFailed;
            self.infoStr = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"修改订单失败"),[IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%d",pb.result]]];
        } else {
            
            self.orderID = [NSString stringWithFormat:@"%@：%llu",LocalizedString(@"订单号"),pb.order.id_p];
            if (pb.order.status == item_order_estatus_StatusFilled || pb.order.status == item_order_estatus_StatusPlaced) {
                self.infoStr = LocalizedString(@"订单已修改");
                _rModel.type = pb.order.type;
                self.status = IXOpenResultSuccess;
                self.order = pb.order;
            } else {
                self.infoStr = LocalizedString(@"订单执行失败");
                self.status = IXOpenResultFailed;
            }
        }
        [self.tableV reloadData];
    }
}


@end
