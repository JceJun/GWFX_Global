//
//  IXPositionCell.h
//  IXApp
//
//  Created by bob on 16/11/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IxItemPosition.pbobjc.h"
#import "IXLastQuoteM.h"
#import "IXPositionM.h"
@class IXPositionCell;

#define IOS_VERSION_11_OR_ABOVE (([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)? (YES):(NO))
#define kBeginDragingKey    @"positionCellBeginDraging"

typedef void(^positionCellEditingBlock)();
typedef void(^positionCellHedgeBlock)(IXPositionM *m); //对冲平仓
typedef void(^positionCellQuickBlock)(IXPositionM *m); //快速平仓
typedef void(^positionCellTapBlock)(IXPositionM * m, IXPositionCell * c);   //cell点击事件

@protocol  IXPositionCellDelegate <NSObject>

@end

@interface IXPositionCell : UITableViewCell

@property (nonatomic, assign) BOOL  hasHedge;
@property (nonatomic, assign) BOOL  multiSelection; //可选

@property (nonatomic, assign) IXSymbolTradeState tradeState;

@property (nonatomic, copy) positionCellEditingBlock    editB;
@property (nonatomic, copy) positionCellHedgeBlock  hedgeB; //对冲平仓
@property (nonatomic, copy) positionCellQuickBlock  quickB; //快速平仓
@property (nonatomic, copy) positionCellTapBlock    tapB;   //点击回调
@property (nonatomic, strong) IXPositionM   * model;

- (void)chooseAction;

- (void)setLastClosePriceInfo:(IXLastQuoteM *)model;

- (void)setCurrentPrice:(double)priceInfo
             WithProfit:(double)profit
          WithQuoteTime:(NSNumber *)time
             WithSymbol:(IXSymModel *)model;

@end
