//
//  IXPositionResultCellB.h
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXPositionRstM.h"

@interface IXPositionResultCellB : UITableViewCell
- (void)reloadUIData:(IXPositionRstM *)model;
@end
