//
//  IXDealListInfoCell.h
//  IXApp
//
//  Created by Evn on 17/1/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXDealM.h"

@interface IXDealListInfoCell : UITableViewCell

- (void)config:(IXDealM *)model title:(NSString *)title indexPathRow:(NSInteger)row;

@end
