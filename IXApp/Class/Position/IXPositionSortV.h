//
//  IXPositionSortV.h
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(int, PositionSortType) {
    PositionSortTypeDefault,    //开仓顺序
    PositionSortTypePriceLH,    //价格低到高
    PositionSortTypePriceHL,    //价格高到低
};

typedef void (^positionSortBlock)(PositionSortType type);
typedef void (^sortVDismissBlock)(void);

@interface IXPositionSortV : UIView

@property (nonatomic, readonly) BOOL    isShow;
@property (nonatomic, copy) positionSortBlock   sortB;
@property (nonatomic, copy) sortVDismissBlock   dismissB;

- (void)show;
- (void)dismiss;

@end
