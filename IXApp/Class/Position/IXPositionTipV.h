//
//  IXPositionTipV.h
//  IXApp
//
//  Created by Seven on 2017/11/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kTipSize    CGSizeMake(kScreenWidth, 32)
typedef void(^tipVCleanBlock)(void);

/** 筛选提示 */
@interface IXPositionTipV : UIView
@property (nonatomic, copy) NSString    * message;
@property (nonatomic, copy) tipVCleanBlock  cleanB;

@end
