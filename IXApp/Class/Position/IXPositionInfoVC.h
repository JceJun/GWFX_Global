//
//  IXPositionInfoVC.h
//  IXApp
//
//  Created by Evn on 16/12/26.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXPositionM.h"
#import "IXOpenModel.h"

/** 仓位详情 */
@interface IXPositionInfoVC : IXDataBaseVC

@property (nonatomic, strong) IXPositionM   *positionModel;
@property (nonatomic, strong) IXOpenModel   *openModel;
@property (nonatomic, assign) NSInteger parentId;

- (id)initWithPositionModel:(IXPositionM *)positionModel;

@end
