//
//  IXPositionPrcCell.m
//  IXApp
//
//  Created by Evn on 2017/6/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionPrcCell.h"

@interface IXPositionPrcCell()

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *inputExpLbl;

@end

@implementation IXPositionPrcCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xfafcfe, 0x242a36);
        [self loadOpView];
        [self.contentView addSubview:self.inputTF];
    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    if ( title && title.length ) {
        self.titleLbl.text = title;
    }
}

- (void)setInputContent:(NSString *)inputContent
{
    if ( inputContent && inputContent.length ) {
        self.inputTF.text = inputContent;
    }
}

- (void)setInputExplain:(NSString *)inputExplain
{
    if ( inputExplain && inputExplain.length ) {
        self.inputExpLbl.text = inputExplain;
    }
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 14, kScreenWidth - 15, 15)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x99abba
                                     dTextColor:0x99abba];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}


- (IXAccTextField *)inputTF
{
    if ( !_inputTF ) {
        
        CGFloat width = kScreenWidth - 222;
        _inputTF = [[IXAccTextField alloc] initWithFrame:CGRectMake( 119, 5, width, 30)];
    }
    return _inputTF;
}

- (UILabel *)inputExpLbl
{
    if ( !_inputExpLbl ) {
        CGFloat width = kScreenWidth - 222;
        _inputExpLbl = [IXUtils createLblWithFrame:CGRectMake( 119, 31, width, 9)
                                          WithFont:RO_REGU(9)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0x99abba
                                        dTextColor:0x99abba];
        [self.contentView addSubview:_inputExpLbl];
    }
    return _inputExpLbl;
}

- (void)loadOpView
{
    _subBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_subBtn];
    
    _subBtn.frame = CGRectMake( 70, 0, 30, 44);
    [_subBtn dk_setImage:DKImageNames(@"openRoot_btn_substract", @"openRoot_btn_substract_dark")
                forState:UIControlStateNormal];
    [_subBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 8, 0, 0)];
    [_subBtn addTarget:self
                action:@selector(responseToSubstract)
      forControlEvents:UIControlEventTouchUpInside];
    
    _stepBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_stepBtn];
    
    _stepBtn.frame = CGRectMake( kScreenWidth - 45 + 7, 0, 30, 44);
    [_stepBtn addTarget:self
                 action:@selector(responseToStep)
       forControlEvents:UIControlEventTouchUpInside];
    [_stepBtn dk_setImage:DKImageNames(@"openRoot_stepPrc", @"openRoot_stepPrc_D")
                 forState:UIControlStateNormal];
    //    [_stepBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 12, 0, 0)];
    
    _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_addBtn];
    
    _addBtn.frame = CGRectMake( kScreenWidth - 88, 0, 30, 44);
    [_addBtn dk_setImage:DKImageNames(@"openRoot_btn_add", @"openRoot_btn_add_dark")
                forState:UIControlStateNormal];
    [_addBtn setImageEdgeInsets:UIEdgeInsetsMake( 0, 8, 0, 0)];
    [_addBtn addTarget:self
                action:@selector(responseToAdd)
      forControlEvents:UIControlEventTouchUpInside];
}

- (void)responseToAdd
{
    if ( self.addBlock ) {
        self.addBlock();
    }
}

- (void)responseToStep
{
    if ( self.stepBlock ) {
        self.stepBlock();
    }
}

- (void)responseToSubstract
{
    if( self.substractBlock ){
        self.substractBlock();
    }
}

@end
