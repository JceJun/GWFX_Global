//
//  IXHedgeM.h
//  IXApp
//
//  Created by Bob on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXHedgeM : NSObject


@property (nonatomic, copy) NSString *posPrc;

@property (nonatomic, copy) NSString *posDir;

@property (nonatomic, copy) NSString *posVol;

@property (nonatomic, copy) NSString *showVol;

@property (nonatomic, copy) NSString *posId;

@end
