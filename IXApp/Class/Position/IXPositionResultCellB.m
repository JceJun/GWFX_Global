//
//  IXPositionResultCellB.m
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionResultCellB.h"
#import "IXUserDefaultM.h"

@interface IXPositionResultCellB()

@property (nonatomic, strong) UIImageView  *bgView;
@property (nonatomic, strong) UILabel *showName;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *showDir;
@property (nonatomic, strong) UILabel *dir;
@property (nonatomic, strong) UILabel *showType;
@property (nonatomic, strong) UILabel *type;
@property (nonatomic, strong) UILabel *showNum;
@property (nonatomic, strong) UILabel *num;
@property (nonatomic, strong) UILabel *showPrice;
@property (nonatomic, strong) UILabel *price;
@property (nonatomic, strong) UILabel *showClosePrice;
@property (nonatomic, strong) UILabel *closePrice;
@property (nonatomic, strong) UILabel *showDate;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, strong) UILabel *showCloseDate;
@property (nonatomic, strong) UILabel *closeDate;
@property (nonatomic, strong) UILabel *showSwap;
@property (nonatomic, strong) UILabel *swap;
@property (nonatomic, strong) UILabel *showProfit;
@property (nonatomic, strong) UILabel *profit;
@property (nonatomic, strong) UILabel *showCommission;
@property (nonatomic, strong) UILabel *commission;

@end
@implementation IXPositionResultCellB

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(9, 0, kScreenWidth - 18,344)];
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)showName
{
    if (!_showName) {
        CGRect rect = CGRectMake(14.5, 14,kScreenWidth - 34, 16);
        _showName = [[UILabel alloc] initWithFrame:rect];
        _showName.font = PF_MEDI(13);
        _showName.text = LocalizedString(@"商品");
        _showName.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showName.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showName];
    }
    return _showName;
}

- (UILabel *)name
{
    if (!_name) {
        CGRect rect = CGRectMake(0, 14,VIEW_W(_bgView) - 14.5, 16);
        _name = [[UILabel alloc] initWithFrame:rect];
        _name.font = PF_MEDI(13);
        _name.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _name.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_name];
    }
    return _name;
}

- (UILabel *)showDir
{
    if (!_showDir) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showName) + 14,kScreenWidth - 34, 16);
        _showDir = [[UILabel alloc] initWithFrame:rect];
        _showDir.font = PF_MEDI(13);
        _showDir.text = LocalizedString(@"方向");
        _showDir.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showDir.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showDir];
    }
    return _showDir;
}

- (UILabel *)dir
{
    if (!_dir) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showName) + 14,VIEW_W(_bgView) - 14.5, 16);
        _dir = [[UILabel alloc] initWithFrame:rect];
        _dir.font = PF_MEDI(13);
        _dir.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _dir.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_dir];
    }
    return _dir;
}

- (UILabel *)showType
{
    if (!_showType) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showDir) + 14,kScreenWidth - 34, 16);
        _showType = [[UILabel alloc] initWithFrame:rect];
        _showType.font = PF_MEDI(13);
        _showType.text = LocalizedString(@"类型");
        _showType.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showType.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showType];
    }
    return _showType;
}

- (UILabel *)type
{
    if (!_type) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showDir) + 14,VIEW_W(_bgView) - 14.5, 16);
        _type = [[UILabel alloc] initWithFrame:rect];
        _type.font = PF_MEDI(13);
        _type.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _type.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_type];
    }
    return _type;
}

- (UILabel *)showNum
{
    if (!_showNum) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showType) + 14,kScreenWidth - 34, 16);
        _showNum = [[UILabel alloc] initWithFrame:rect];
        _showNum.font = PF_MEDI(13);
        _showNum.text = LocalizedString(@"数量");
        _showNum.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showNum.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showNum];
    }
    return _showNum;
}

- (UILabel *)num
{
    if (!_num) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showType) + 14,VIEW_W(_bgView) - 14.5, 16);
        _num = [[UILabel alloc] initWithFrame:rect];
        _num.font = RO_REGU(15);
        _num.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _num.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_num];
    }
    return _num;
}

- (UILabel *)showPrice
{
    if (!_showPrice) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showNum) + 14,kScreenWidth - 34, 16);        _showPrice = [[UILabel alloc] initWithFrame:rect];
        _showPrice.font = PF_MEDI(13);
        _showPrice.text = LocalizedString(@"开仓价格");
        _showPrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showPrice.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showPrice];
    }
    return _showPrice;
}

- (UILabel *)price
{
    if (!_price) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showNum) + 14,VIEW_W(_bgView) - 14.5, 16);
        _price = [[UILabel alloc] initWithFrame:rect];
        _price.font = RO_REGU(15);
        _price.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _price.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_price];
    }
    return _price;
}

- (UILabel *)showClosePrice
{
    if (!_showClosePrice) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showPrice) + 14,kScreenWidth - 34, 16);        _showClosePrice = [[UILabel alloc] initWithFrame:rect];
        _showClosePrice.font = PF_MEDI(13);
        _showClosePrice.text = LocalizedString(@"平仓价格");
        _showClosePrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showClosePrice.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showClosePrice];
    }
    return _showClosePrice;
}

- (UILabel *)closePrice
{
    if (!_closePrice) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showPrice) + 14,VIEW_W(_bgView) - 14.5, 16);
        _closePrice = [[UILabel alloc] initWithFrame:rect];
        _closePrice.font = RO_REGU(15);
        _closePrice.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _closePrice.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_closePrice];
    }
    return _closePrice;
}

- (UILabel *)showCommission
{
    if (!_showCommission) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showClosePrice) + 14,kScreenWidth - 34, 16);
        _showCommission = [[UILabel alloc] initWithFrame:rect];
        _showCommission.font = PF_MEDI(13);
        _showCommission.text = LocalizedString(@"佣金");
        _showCommission.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showCommission.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showCommission];
    }
    return _showCommission;
}

- (UILabel *)commission
{
    if (!_commission) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showClosePrice) + 14,VIEW_W(_bgView) - 14.5, 16);
        _commission = [[UILabel alloc] initWithFrame:rect];
        _commission.font = RO_REGU(15);
        _commission.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _commission.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_commission];
    }
    return _commission;
}

- (UILabel *)showProfit
{
    if (!_showProfit) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showCommission) + 14,kScreenWidth - 34, 16);
        _showProfit = [[UILabel alloc] initWithFrame:rect];
        _showProfit.font = PF_MEDI(13);
        _showProfit.text = LocalizedString(@"盈亏");
        _showProfit.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showProfit.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showProfit];
    }
    return _showProfit;
}

- (UILabel *)profit
{
    if (!_profit) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showCommission) + 14,VIEW_W(_bgView) - 14.5, 16);
        _profit = [[UILabel alloc] initWithFrame:rect];
        _profit.font = RO_REGU(15);
        _profit.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _profit.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_profit];
    }
    return _profit;
}

- (UILabel *)showSwap
{
    if (!_showSwap) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showProfit) + 14,kScreenWidth - 34, 16);        _showSwap = [[UILabel alloc] initWithFrame:rect];
        _showSwap.font = PF_MEDI(13);
        _showSwap.text = LocalizedString(@"利息");
        _showSwap.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showSwap.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showSwap];
    }
    return _showSwap;
}

- (UILabel *)swap
{
    if (!_swap) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showProfit) + 14,VIEW_W(_bgView) - 14.5, 16);
        _swap = [[UILabel alloc] initWithFrame:rect];
        _swap.font = RO_REGU(15);
        _swap.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _swap.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_swap];
    }
    return _swap;
}

- (UILabel *)showDate
{
    if (!_showDate) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showSwap) + 14,kScreenWidth - 34, 16);
        _showDate = [[UILabel alloc] initWithFrame:rect];
        _showDate.font = PF_MEDI(13);
        _showDate.text = LocalizedString(@"开仓日期");
        _showDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showDate.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showDate];
    }
    return _showDate;
}

- (UILabel *)date
{
    if (!_date) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showSwap) + 14,VIEW_W(_bgView) - 14.5, 16);
        _date = [[UILabel alloc] initWithFrame:rect];
        _date.font = RO_REGU(15);
        _date.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _date.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_date];
    }
    return _date;
}

- (UILabel *)showCloseDate
{
    if (!_showCloseDate) {
        CGRect rect = CGRectMake(14.5, GetView_MaxY(_showDate) + 14,kScreenWidth - 34, 16);
        _showCloseDate = [[UILabel alloc] initWithFrame:rect];
        _showCloseDate.font = PF_MEDI(13);
        _showCloseDate.text = LocalizedString(@"平仓日期");
        _showCloseDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        _showCloseDate.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_showCloseDate];
    }
    return _showCloseDate;
}

- (UILabel *)closeDate
{
    if (!_closeDate) {
        CGRect rect = CGRectMake(0, GetView_MaxY(_showDate) + 14,VIEW_W(_bgView) - 14.5, 16);
        _closeDate = [[UILabel alloc] initWithFrame:rect];
        _closeDate.font = RO_REGU(15);
        _closeDate.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        _closeDate.textAlignment = NSTextAlignmentRight;
        [_bgView addSubview:_closeDate];
    }
    return _closeDate;
}

- (void)reloadUIData:(IXPositionRstM *)model
{
    UIImage *wImage = GET_IMAGE_NAME(@"common_result_back");
    UIImage * wImg = [wImage stretchableImageWithLeftCapWidth:wImage.size.width/2 topCapHeight:wImage.size.height/2];
    UIImage *dImage = GET_IMAGE_NAME(@"common_result_back_D");
    UIImage * dImg = [dImage stretchableImageWithLeftCapWidth:dImage.size.width/2 topCapHeight:dImage.size.height/2];
    self.bgView.dk_imagePicker = DKImageWithImgs(wImg, dImg);
    
    self.showName.text = LocalizedString(@"商品");
     self.name.text = model.name;
    
    self.showDir.text = LocalizedString(@"方向");
    if (model.dir == item_order_edirection_DirectionBuy) {
        self.dir.text = LocalizedString(@"买入");
        self.dir.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    } else {
        self.dir.text = LocalizedString(@"卖出");
        self.dir.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    
    self.showType.text = LocalizedString(@"类型");
    self.type.text = LocalizedString(model.cate);
    
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
        self.showNum.text = LocalizedString(@"数量");
    } else {
        self.showNum.text = LocalizedString(@"手数");
    }
    self.num.text = [NSString stringWithFormat:@"%@%@",model.num,[IXDataProcessTools showCurrentUnitLanName:model.unitLanName]];
    
    self.showPrice.text = LocalizedString(@"开仓价格");
    self.price.text = model.openPrice;
    
    self.showClosePrice.text = LocalizedString(@"平仓价格");
    self.closePrice.text = model.closePrice;
    
    self.showCommission.text = LocalizedString(@"佣金");
    if (model.commission == 0) {
        self.commission.text = LocalizedString(@"免费");
    } else {
        self.commission.text = [IXDataProcessTools moneyFormatterComma:[model.commission doubleValue] positiveNumberSign:YES];
    }
    
    self.showProfit.text = LocalizedString(@"盈亏");
    [IXDataProcessTools resetTextColorLabel:self.profit value:model.profit];
    self.profit.text = [IXDataProcessTools moneyFormatterComma:model.profit positiveNumberSign:NO];
    
    self.showSwap.text = LocalizedString(@"利息");
    self.swap.text = model.swap;
    
    self.showDate.text = LocalizedString(@"开仓日期");
    self.date.text = model.openDate;
    
    self.showCloseDate.text = LocalizedString(@"平仓日期");
    self.closeDate.text = model.closeDate;
    
    

}

@end
