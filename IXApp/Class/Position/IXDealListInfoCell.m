//
//  IXDealListInfoCell.m
//  IXApp
//
//  Created by Evn on 17/1/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDealListInfoCell.h"
#import "IXDateUtils.h"
#import "NSString+FormatterPrice.h"
#import "IXAppUtil.h"

@interface IXDealListInfoCell()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *content;
@property (nonatomic, strong)UIView *lineView;

@end

@implementation IXDealListInfoCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self lineView];
}

- (UILabel *)title
{
    if (!_title) {
        _title =  [IXCustomView createLable:CGRectMake(10,
                                                       10,
                                                       kScreenWidth/2 - 10,
                                                       12)
                                      title:@""
                                       font:PF_MEDI(12)
                                 wTextColor:0x99abba
                                 dTextColor:0x8395a4
                              textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)content
{
    if (!_content) {
        _content =  [IXCustomView createLable:CGRectMake(kScreenWidth/2,10, kScreenWidth/2 - 10, 12)
                                        title:@""
                                         font:PF_MEDI(12)
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_content];
    }
    return _content;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 29 - kLineHeight, kScreenWidth, kLineHeight)];
        _lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)config:(IXDealM *)model title:(NSString *)title indexPathRow:(NSInteger)row
{
    self.title.text = title;
    self.content.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
     if (model.deal.reason == item_order_etype_TypeOpen ||
         model.deal.reason == item_order_etype_TypeLimit ||
         model.deal.reason == item_order_etype_TypeStop ) {
         if (row == 0 || row == 4 || row == 6) {
             self.lineView.hidden = NO;
         } else {
             self.lineView.hidden = YES;
         }
         switch (row) {
             case 0:{
                 if (model.deal.direction == 1) {
                     self.content.text = LocalizedString(@"买入");
                     self.content.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
                 } else {
                     self.content.text = LocalizedString(@"卖出");
                     self.content.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
                 }
                 self.content.font = PF_MEDI(12);
             }
                 break;
             case 1:{
                 self.content.text = [IXEntityFormatter timeIntervalToString:model.deal.execTime];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 2:{
                 self.content.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.deal.execPrice] WithDigits:model.symbolModel.digits];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 3:{
                 if (model.deal.commission == 0) {
                     self.content.text = LocalizedString(@"免费");
                     self.content.font = PF_MEDI(12);
                 } else {
                     self.content.text = [IXDataProcessTools moneyFormatterComma:model.deal.commission positiveNumberSign:YES];
                     self.content.font = RO_REGU(12);
                 }
             }
                 break;
             case 4:{
                 self.content.text = [NSString stringWithFormat:@"%.2f",model.deal.swap];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 5:{
                 NSString *numStr = [NSString stringWithFormat:@"%@%@",[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.deal.execVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits],[IXDataProcessTools showCurrentUnitLanName:model.symbolModel.unitLanName]];
                 self.content.text = numStr;
                 self.content.font = RO_REGU(12);
                 if ([[IXDataProcessTools showCurrentUnitLanName:model.symbolModel.unitLanName] length]) {
                     [IXDataProcessTools resetLabel:self.content leftContent:[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.deal.execVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits] leftFont:12 WithContent:numStr rightFont:12 fontType:YES];
                 }
                 
             }
                 break;
             case 6:{
                 [IXDataProcessTools resetTextColorLabel:self.content value:model.deal.profit];
                 self.content.text = [IXDataProcessTools moneyFormatterComma:model.deal.profit positiveNumberSign:NO];
                 self.content.font = RO_REGU(12);
             }
                 break;
             default:
                 break;
         }
     } else {
         if (row == 0 || row == 6 || row == 8) {
             self.lineView.hidden = NO;
         } else {
             self.lineView.hidden = YES;
         }
         
         switch (row) {
             case 0:{
                 if (model.deal.direction == 1) {
                     self.content.text = LocalizedString(@"买入");
                     self.content.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
                 } else {
                     self.content.text = LocalizedString(@"卖出");
                     self.content.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
                 }
                 self.content.font = PF_MEDI(12);
             }
                 break;
             case 1:{
                 self.content.text = [IXEntityFormatter timeIntervalToString:model.deal.srcTime];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 2:{
                 self.content.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.deal.srcPrice] WithDigits:model.symbolModel.digits];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 3:{
                 self.content.text = [IXEntityFormatter timeIntervalToString:model.deal.execTime];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 4:{
                 self.content.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.deal.execPrice] WithDigits:model.symbolModel.digits];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 5:{
                 if (model.deal.commission == 0) {
                    self.content.text = LocalizedString(@"免费");
                     self.content.font = PF_MEDI(12);
                 } else if (model.deal.commission > 0) {
                     self.content.text = [IXDataProcessTools moneyFormatterComma:model.deal.commission positiveNumberSign:YES];
                     self.content.font = RO_REGU(12);
                 } else {
                     self.content.text = [IXDataProcessTools moneyFormatterComma:model.deal.commission positiveNumberSign:YES];
                     self.content.font = RO_REGU(12);
                 }
             }
                 break;
             case 6:{
                 self.content.text = [IXDataProcessTools moneyFormatterComma:model.deal.swap positiveNumberSign:YES];
                 self.content.font = RO_REGU(12);
             }
                 break;
             case 7:{
                 NSString *numStr = [NSString stringWithFormat:@"%@%@",[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.deal.execVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits],[IXDataProcessTools showCurrentUnitLanName:model.symbolModel.unitLanName]];
                 self.content.text = numStr;
                 self.content.font = RO_REGU(12);
                 if ([[IXDataProcessTools showCurrentUnitLanName:model.symbolModel.unitLanName] length]) {
                    [IXDataProcessTools resetLabel:self.content leftContent:[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.deal.execVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits] leftFont:12 WithContent:numStr rightFont:12 fontType:YES];
                 }
             }
                 break;
             case 8:{
                 [IXDataProcessTools resetTextColorLabel:self.content value:model.deal.profit];
                 self.content.text = [IXDataProcessTools moneyFormatterComma:model.deal.profit positiveNumberSign:NO];
                 self.content.font = RO_REGU(12);
             }
                 break;
                 
             default:
                 break;
         }
     }
   
}

@end
