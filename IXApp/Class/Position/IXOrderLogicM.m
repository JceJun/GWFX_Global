//
//  IXOrderLogicM.m
//  IXApp
//
//  Created by Evn on 2017/6/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOrderLogicM.h"
#import "IXOpenModel.h"
#import "IXOpenModel+CheckData.h"
#import "IXTradeMarketModel.h"

@implementation IXOrderLogicM

+ (void)addPrice:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.requestPrice || isnan([root.tradeModel.requestPrice floatValue]) ) {
        root.tradeModel.requestPrice = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minRequestPrice.decimalValue];
    }else{
        root.tradeModel.requestPrice = [root.tradeModel.requestPrice decimalNumberByAdding:root.openModel.stepPrice];
        if (![root.openModel validPrice:root.tradeModel.requestPrice] ) {
            root.tradeModel.requestPrice = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minRequestPrice.decimalValue];
        }
    }
    
    root.openModel.requestPrice = root.tradeModel.requestPrice;
}

+ (void)addVolume:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.requestVolume || isnan([root.tradeModel.requestVolume floatValue]) ) {
        root.tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                         [NSString stringWithFormat:@"%lf", root.tradeModel.symbolModel.volumesMin]];
    }else{
        root.tradeModel.requestVolume = [root.tradeModel.requestVolume decimalNumberByAdding:
                                         [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf", root.tradeModel.symbolModel.volumesStep]]];
        
        if ( ![root.openModel validVolume:root.tradeModel.requestVolume] ) {
            root.tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                             [NSString stringWithFormat:@"%lf", root.tradeModel.symbolModel.volumesMin]];
        }
    }
}

+ (void)addProfit:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.takeprofit || isnan([root.tradeModel.takeprofit floatValue]) ) {
        root.tradeModel.takeprofit = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minProfit.decimalValue];
    }else{
        root.tradeModel.takeprofit = [root.tradeModel.takeprofit decimalNumberByAdding:root.openModel.stepPrice];
        if( ![root.openModel validProfit:root.tradeModel.takeprofit] ){
            root.tradeModel.takeprofit = root.openModel.minProfit;
        }
    }
}

+ (void)addLoss:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.stoploss || isnan([root.tradeModel.stoploss floatValue]) ) {
        root.tradeModel.stoploss = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.minLoss.decimalValue];
    }else{
        root.tradeModel.stoploss = [root.tradeModel.stoploss decimalNumberByAdding:root.openModel.stepPrice];
        if( ![root.openModel validLoss:root.tradeModel.stoploss]  ){
            root.tradeModel.stoploss = root.openModel.minLoss;
        }
    }
}

+ (void)updateTakeprofitWithTag:(ROWNAME)tag WithRoot:(IXUpdateOrderVC *)root
{
    switch (tag) {
        case ROWNAMEPRICE:{
            [IXOrderLogicM addPrice:root];
        }
            break;
        case ROWNAMEVOLUME:{
            [IXOrderLogicM addVolume:root];
        }
            break;
        case ROWNAMEPROFIT:{
            [IXOrderLogicM addProfit:root];
        }
            break;
        case ROWNAMELOSS:{
            [IXOrderLogicM addLoss:root];
        }
            break;
        default:
            break;
    }
}

+ (void)substractPrice:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.requestPrice || isnan([root.tradeModel.requestPrice floatValue]) ) {
        root.tradeModel.requestPrice = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.maxRequestPrice.decimalValue];
    }else{
        root.tradeModel.requestPrice = [root.tradeModel.requestPrice decimalNumberBySubtracting:root.openModel.stepPrice];
        if (![root.openModel validPrice:root.tradeModel.requestPrice] ) {
            root.tradeModel.requestPrice = root.openModel.maxRequestPrice;
        }
    }
    root.openModel.requestPrice = root.tradeModel.requestPrice;
}

+ (void)substractVolume:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.requestVolume || isnan([root.tradeModel.requestVolume floatValue]) ) {
        root.tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", root.tradeModel.symbolModel.volumesMax]];
    }else{
        root.tradeModel.requestVolume = [root.tradeModel.requestVolume decimalNumberBySubtracting:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", root.tradeModel.symbolModel.volumesStep]]];
        if ( ![root.openModel validVolume:root.tradeModel.requestVolume] ) {
            root.tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", root.tradeModel.symbolModel.volumesMax]];
        }
    }
}


+ (void)substractProfit:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.takeprofit || isnan([root.tradeModel.takeprofit floatValue]) ) {
        root.tradeModel.takeprofit = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.maxProfit.decimalValue];
    }else{
        root.tradeModel.takeprofit = [root.tradeModel.takeprofit decimalNumberBySubtracting:root.openModel.stepPrice];
        if ( ![root.openModel validProfit:root.tradeModel.takeprofit] ) {
            root.tradeModel.takeprofit = root.openModel.maxProfit;
        }
    }
}

+ (void)substractLoss:(IXUpdateOrderVC *)root
{
    if ( !root.tradeModel.stoploss || isnan([root.tradeModel.stoploss floatValue]) ) {
        root.tradeModel.stoploss = [NSDecimalNumber decimalNumberWithDecimal:root.openModel.maxLoss.decimalValue];
    }else{
        root.tradeModel.stoploss = [root.tradeModel.stoploss decimalNumberBySubtracting:root.openModel.stepPrice];
        if ( ![root.openModel validLoss:root.tradeModel.stoploss] ) {
            root.tradeModel.stoploss = root.openModel.maxLoss;
        }
    }
}

+ (void)updateStoplossWithTag:(ROWNAME)tag WithRoot:(IXUpdateOrderVC *)root
{
    switch (tag) {
        case ROWNAMEPRICE:{
            [IXOrderLogicM substractPrice:root];
        }
            break;
        case ROWNAMEVOLUME:{
            [IXOrderLogicM substractVolume:root];
        }
            break;
        case ROWNAMEPROFIT:{
            [IXOrderLogicM substractProfit:root];
        }
            break;
        case ROWNAMELOSS:{
            [IXOrderLogicM substractLoss:root];
        }
            break;
        default:
            break;
    }
}

@end
