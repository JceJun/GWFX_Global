//
//  IXOrderListInfoVC.m
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOrderListInfoVC.h"
#import "IXOrderListInfoCell.h"
#import "IXUpdateOrderVC.h"
#import "IXTradeMarketModel.h"
#import "IXDBGlobal.h"
#import "IXDetailSymbolCell.h"
#import "IXLastQuoteM.h"
#import "IXAccountBalanceModel.h"
#import "IXPositionOpenCell.h"
#import "IXSymbolDeepCell.h"
#import "IXSymbolDetailVC.h"
#import "NSString+FormatterPrice.h"
#import "IXAlertVC.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "UIImageView+SepLine.h"
#import "IXCpyConfig.h"
#import "IXUserDefaultM.h"

@interface IXOrderListInfoVC ()
<
UITableViewDataSource,
UITableViewDelegate,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong) UITableView   *tableV;
@property (nonatomic, strong) NSArray       *titleArr;
@property (nonatomic, strong) UIButton      *cancelBtn;
@property (nonatomic, strong) UIButton      *updateBtn;
@property (nonatomic, strong) UIView        *lineView;

@property (nonatomic, strong) IXQuoteM      *quoteModel;

@property (nonatomic, assign) BOOL          receviedOrderAdd;
@property (nonatomic, assign) double        nLastClosePrice;
@property (nonatomic, assign) BOOL          flag;//是否展开

@end

@implementation IXOrderListInfoVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ORDER_CANCEL);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ORDER_CANCEL);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:YES];

    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(rightBtnItemClicked)];
    
    self.title = LocalizedString(@"挂单详情");
    self.fd_interactivePopDisabled = YES;
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
        _titleArr = @[LocalizedString(@"类型"),
                      LocalizedString(@"方向"),
                      LocalizedString(@"数量"),
                      LocalizedString(@"价格"),
                      LocalizedString(@"止损"),
                      LocalizedString(@"止盈"),
                      LocalizedString(@"建立日期"),
                      LocalizedString(@"订单号"),
                      LocalizedString(@"有效期")];
    } else {
        _titleArr = @[LocalizedString(@"类型"),
                      LocalizedString(@"方向"),
                      LocalizedString(@"手数"),
                      LocalizedString(@"价格"),
                      LocalizedString(@"止损"),
                      LocalizedString(@"止盈"),
                      LocalizedString(@"建立日期"),
                      LocalizedString(@"订单号"),
                      LocalizedString(@"有效期")];
    }
    
    NSDictionary *marketDic = [IXDBSymbolHotMgr querySymbolMarketIdsBySymbolId:_orderModel.symbolModel.id_p];
    if (marketDic && marketDic.count > 0) {
        _orderModel.marketId = [marketDic[kMarketId] intValue];
    }
    [self.view addSubview:self.tableV];
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.updateBtn];
    [self.view addSubview:self.lineView];
    [self updateNavBottomLine];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self subscribeDynamicPrice];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self cancelQuote];
}

//订阅数据
- (void)subscribeDynamicPrice
{
    if ( _orderModel && _orderModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(_orderModel.marketId),
                           @"id":@(_orderModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] subscribeDetailPriceWithSymbolAry:arr];
        [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:arr];
    }
    
}

//取消订阅
- (void)cancelQuote
{
    if ( _orderModel && _orderModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(_orderModel.marketId),
                           @"id":@(_orderModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] unSubscribeDetailPriceWithSymbolAry:arr];
    }
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - lazy load

- (UITableView *)tableV{
    if (!_tableV){
        
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,
                                                                kScreenWidth,
                                                                kScreenHeight - 44 - kNavbarHeight)];
        _tableV.showsVerticalScrollIndicator = NO;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV registerClass:[IXOrderListInfoCell class] forCellReuseIdentifier:NSStringFromClass([IXOrderListInfoCell class])];
        [_tableV registerClass:[IXDetailSymbolCell class] forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        [_tableV registerClass:[IXPositionOpenCell class] forCellReuseIdentifier:NSStringFromClass([IXPositionOpenCell class])];
        [_tableV registerClass:[IXSymbolDeepCell class] forCellReuseIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
    }
    return _tableV;
}

- (UIButton *)updateBtn {
    if (!_updateBtn) {
        _updateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _updateBtn.frame = CGRectMake(kScreenWidth/2,
                                      kScreenHeight - (44 + kNavbarHeight),
                                      kScreenWidth/2,
                                      44);
        [_updateBtn addTarget:self action:@selector(updateOrderBtn) forControlEvents:UIControlEventTouchUpInside];
        [_updateBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
        [_updateBtn setTitle:LocalizedString(@"修改订单") forState:UIControlStateNormal];
        _updateBtn.titleLabel.font = PF_REGU(15);
    }
    IXSymbolTradeState state = [IXDataProcessTools symbolModelIsCanTrade:_orderModel.symbolModel];
    if ([IXDataProcessTools canTradeByState:state orderDir:_orderModel.order.direction]) {
        _updateBtn.userInteractionEnabled = YES;
        [_updateBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
    } else {
        _updateBtn.userInteractionEnabled = NO;
        _updateBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
    }
    return _updateBtn;
}



- (void)cancelOrderBtn {
    IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"取消订单提醒")
                                             message:LocalizedString(@"点击\"确定\"将会取消订单")
                                         cancelTitle:LocalizedString(@"取消")
                                          sureTitles:LocalizedString(@"确定.")];
    VC.index = 0;
    VC.expendAbleAlartViewDelegate = self;
    [VC showView];
}

- (void)sendOrderCancel {
    
    proto_order_cancel *proto = [self packageOrder];
    [[IXTCPRequest shareInstance] orderCancelWithParam:proto];
    
    if( [SVProgressHUD isVisible] ){
        [SVProgressHUD dismiss];
    }
    _receviedOrderAdd = NO;
}

#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_flag) {
        return _titleArr.count + 7;
    } else {
        return _titleArr.count + 3;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return  73;
    } else {
        if (_flag) {
            if (indexPath.row >= 1 && indexPath.row < 6) {
                return 22;
            } else if (indexPath.row == 6) {
                return 30;
            } else {
                return 29;
            }
        } else {
            if (indexPath.row >= 1 && indexPath.row < 2) {
                return 22;
            } else if (indexPath.row == 2) {
                return 30;
            } else {
                return 29;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_flag) {
        
        switch (indexPath.row) {
            case 0: {
                IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.symbolModel = _orderModel.symbolModel;
                cell.quoteModel = _quoteModel;
                cell.marketId = _orderModel.symbolModel.marketID;
                cell.nLastClosePrice = _nLastClosePrice;
                cell.tradeState = [IXDataProcessTools symbolModelIsCanTrade:_orderModel.symbolModel];
                return cell;
            }
                break;
            default: {
                
                if (indexPath.row >= 1 && indexPath.row <= 5) {
                    
                    IXSymbolDeepCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    NSInteger tag = (indexPath.row - 1);
                    cell.cellContentHeight = 22;
                    cell.tag = tag;
                    IXQuoteM *quoteModel = _quoteModel;
                    if ( quoteModel.BuyPrc.count >= indexPath.row ) {
                        [cell setBuyPrc:[self formatterPrice:quoteModel.BuyPrc[tag]]
                                 BuyVol:[self formatterPrice:quoteModel.BuyVol[tag]]
                                SellPrc:[self formatterPrice:quoteModel.SellPrc[tag]]
                                SellVol:[self formatterPrice:quoteModel.SellVol[tag]]];
                    }
                    
                    return cell;
                } else if (indexPath.row == 6) {
                    
                    IXPositionOpenCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionOpenCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell reloadUI:_flag];
                    return cell;
                } else {
                    
                    IXOrderListInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXOrderListInfoCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell config:_orderModel title:_titleArr[(indexPath.row - 7)] indexPathRow:(indexPath.row - 7)];
                    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
                    return cell;
                }
                
            }
                break;
        }
    } else {
        
        switch (indexPath.row) {
            case 0: {
                
                IXDetailSymbolCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.symbolModel = _orderModel.symbolModel;
                cell.quoteModel = _quoteModel;
                cell.marketId = _orderModel.symbolModel.marketID;
                cell.nLastClosePrice = _nLastClosePrice;
                cell.tradeState = [IXDataProcessTools symbolModelIsCanTrade:_orderModel.symbolModel];
                return cell;
            }
                break;
            default: {
                if (indexPath.row == 1) {
                    
                    IXSymbolDeepCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    NSInteger tag = (indexPath.row - 1);
                    cell.cellContentHeight = 22;
                    cell.tag = tag;
                    IXQuoteM *quoteModel = _quoteModel;
                    if ( quoteModel.BuyPrc.count >= indexPath.row ) {
                        [cell setBuyPrc:[self formatterPrice:quoteModel.BuyPrc[tag]]
                                 BuyVol:[self formatterPrice:quoteModel.BuyVol[tag]]
                                SellPrc:[self formatterPrice:quoteModel.SellPrc[tag]]
                                SellVol:[self formatterPrice:quoteModel.SellVol[tag]]];
                    }
                    
                    return cell;
                } else if (indexPath.row == 2) {
                    
                    IXPositionOpenCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXPositionOpenCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell reloadUI:_flag];
                    return cell;
                } else {
                    
                    IXOrderListInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXOrderListInfoCell class])];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell config:_orderModel title:_titleArr[(indexPath.row - 3)] indexPathRow:(indexPath.row - 3)];
                    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
                    return cell;
                }
            }
                break;
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        IXSymbolDetailVC *openVC = [[IXSymbolDetailVC alloc] init];
        IXSymbolM *model = _orderModel.symbolModel;
        IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
        tradeModel.symbolModel = model;
        if (_quoteModel) {
            
            tradeModel.quoteModel = _quoteModel;
        }
        
        openVC.tradeModel = tradeModel;
        openVC.nLastClosePrice = _nLastClosePrice;
        openVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:openVC animated:YES];
    } else {
        
        if (!_flag) {
            
            if (indexPath.row == 2) {
                
                _flag = !_flag;
                [self.tableV reloadData];
            }
        } else {
            
            if (indexPath.row == 6) {
                
                _flag = !_flag;
                [self.tableV reloadData];
            }
        }
        
    }
    
}


- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelBtn.frame = CGRectMake(0,
                                      kScreenHeight - (44 + kNavbarHeight),
                                      kScreenWidth/2,
                                      44);
        [_cancelBtn addTarget:self action:@selector(cancelOrderBtn) forControlEvents:UIControlEventTouchUpInside];
        [_cancelBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
        [_cancelBtn setTitle:LocalizedString(@"取消订单") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = PF_REGU(15);
    }
    IXSymbolTradeState state = [IXDataProcessTools symbolModelIsCanTrade:_orderModel.symbolModel];
    if ([IXDataProcessTools canTradeByState:state orderDir:_orderModel.order.direction]) {
        _cancelBtn.userInteractionEnabled = YES;
        [_cancelBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
    } else {
        _cancelBtn.userInteractionEnabled = NO;
        _cancelBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
    }
    return _cancelBtn;
}

- (UIView *)lineView {
    
    if (!_lineView) {
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth/2, kScreenHeight - 44 - kNavbarHeight, 1, 44)];
        _lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        [self.view addSubview:_lineView];
    }
    return _lineView;
}

- (proto_order_cancel *)packageOrder
{
    proto_order_cancel *proto = [[proto_order_cancel alloc] init];
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    
    item_order *order = [[item_order alloc] init];
    order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    order.id_p = _orderModel.order.id_p;
    proto.order = order;
    
    return proto;
}

- (void)updateOrderBtn {
    
    if (!_quoteModel) {
        
        return;
    }
    IXUpdateOrderVC *openVC = [[IXUpdateOrderVC alloc] init];
    IXTradeMarketModel *tradeModel = [[IXTradeMarketModel alloc] init];
    tradeModel.symbolModel = _orderModel.symbolModel;
    tradeModel.quoteModel = _quoteModel;
    if (_orderModel.order.type == item_order_etype_TypeOpen) {
        
        tradeModel.requestType = MARKETORDER;
    } else if (_orderModel.order.type == item_order_etype_TypeLimit) {
        
        tradeModel.requestType = LIMITORDER;
    } else if (_orderModel.order.type == item_order_etype_TypeStop) {
        
        tradeModel.requestType = STOPORDER;
    }
    
    if (_orderModel.order.expireType == item_order_eexpire_ExpireDaily) {
        
        tradeModel.requestExpire = LocalizedString(@"当日");
    } else if (_orderModel.order.expireType == item_order_eexpire_ExpireWeekly) {
        
        tradeModel.requestExpire = LocalizedString(@"当周");
    }
    tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",_orderModel.order.requestVolume]];
    tradeModel.orderId = _orderModel.order.id_p;
    tradeModel.stoploss = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",_orderModel.order.stopLoss]];
    tradeModel.takeprofit = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",_orderModel.order.takeProfit]];
    tradeModel.marketId = _orderModel.marketId;
    tradeModel.requestPrice = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",_orderModel.order.requestPrice]];
    openVC.tradeModel = tradeModel;
    tradeModel.direction = _orderModel.order.direction;
    
    
    openVC.direction = _orderModel.order.direction;
    openVC.nLastClosePrice = _nLastClosePrice;
    
    [self.navigationController pushViewController:openVC animated:YES];
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath, PB_CMD_ORDER_CANCEL)) {
        if (!_receviedOrderAdd) {
            [SVProgressHUD dismiss];
            NSInteger result = -1;
            NSString *comment = @"";
            proto_order_cancel *pb = ((IXTradeDataCache *)object).pb_order_cancel;
            result = pb.result;
            comment = pb.comment;
            
            if (result != 0) {
                comment = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"取消订单失败"),[IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%d",pb.result]]];
            } else {
                comment = LocalizedString(@"取消订单成功");
            }
            _receviedOrderAdd = YES;
            
            IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"提示")
                                                     message:comment
                                                 cancelTitle:nil
                                                  sureTitles:LocalizedString(@"确定")];
            VC.index = 1;
            VC.expendAbleAlartViewDelegate = self;
            [VC showView];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - IXQuoteDistribute
- (void)needRefresh
{
    [self subscribeDynamicPrice];
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if( cmd == CMD_QUOTE_PUB_DETAIL||
       cmd == CMD_QUOTE_SUB_DETAIL){
        for ( IXQuoteM *model in arr ) {
            if ( model.symbolId == _orderModel.symbolModel.id_p ) {
                
                _quoteModel = model;
                [self.tableV reloadData];
                break;
            }
        }
        
    } else if ( cmd == CMD_QUOTE_CLOSE_PRICE ){
        for ( IXLastQuoteM *model in arr ) {
            if ( model.symbolId == _orderModel.symbolModel.id_p ) {
                
                _nLastClosePrice = model.nLastClosePrice;
                for ( UITableViewCell *cell in [_tableV visibleCells] ) {
                    
                    if ( [cell isKindOfClass:[IXDetailSymbolCell class]] ) {
                        
                        ((IXDetailSymbolCell *)cell).nLastClosePrice = _nLastClosePrice;
                        break;
                    }
                }
                break;
            }
        }
    }
}

- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price WithDigits:_orderModel.symbolModel.digits];
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (alertView.tag == 0) {
        
        if (btnIndex == 0) {
            
        } else {
            
            [self sendOrderCancel];
        }
    } else if (alertView.tag == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    return YES;
}

@end
