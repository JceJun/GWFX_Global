//
//  IXContractPropertiesVC.m
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXContractPropertiesVC.h"
#import "IXDBGlobal.h"
#import "IXDBScheduleCataMgr.h"
#import "IXDBScheduleMgr.h"
#import "IXDBSymbolCataMgr.h"
#import "IXDBEodTimeMgr.h"
#import "IXDateUtils.h"
#import "IxProtoSymbol.pbobjc.h"
#import "IxProtoSchedule.pbobjc.h"
#import "IxProtoScheduleMargin.pbobjc.h"
#import "IxProtoSymbolMarginSet.pbobjc.h"
#import "IxProtoHolidayCata.pbobjc.h"
#import "IxProtoHoliday.pbobjc.h"
#import "IxProtoScheduleCata.pbobjc.h"
#import "IxProtoSchedule.pbobjc.h"
#import "IxProtoSymbolCata.pbobjc.h"
#import "IxProtoEodTime.pbobjc.h"
#import "IxProtoGroupSymbol.pbobjc.h"
#import "IxItemGroupSymbol.pbobjc.h"
#import "IxProtoGroupSymbolCata.pbobjc.h"
#import "IxItemGroupSymbolCata.pbobjc.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "IXUserDefaultM.h"

NSString * const DAY_OF_WEEK = @"day_of_week";
NSString * const TRADE_TIME = @"trade_time";
NSString * const START_HOUR = @"start_hour";
NSString * const START_MINUTE = @"start_minute";
NSString * const END_HOUR = @"end_hour";
NSString * const END_MINUTE = @"end_minute";

@interface IXContractPropertiesVC ()

@property (nonatomic, strong) UIScrollView *sContractView;
@property (nonatomic, strong) NSMutableArray *baseValueArr;
@property (nonatomic, strong) NSMutableArray *condValueArr;
@property (nonatomic, strong) NSMutableArray *stepArr;
@property (nonatomic, strong) NSMutableArray *timeArr;

@property (nonatomic, assign) int currentTimeZone;

@end

@implementation IXContractPropertiesVC

- (id)init{
    self = [super init];
    if(self){
        
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_CATA_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_CATA_ADD);
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_CATA_LIST);
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_CATA_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_ADD);
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_LIST);
        IXTradeData_listen_regist(self, PB_CMD_HOLIDAY_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_CATA_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_CATA_ADD);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_CATA_LIST);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_CATA_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CATA_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CATA_ADD);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_CATA_LIST);
        
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_UPDATE);
        
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_ADD);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_CATA_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_CATA_ADD);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_CATA_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_ADD);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_LIST);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_MARGIN_LIST);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_MARGIN_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_SCHEDULE_MARGIN_ADD);
        
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_MARGIN_SET_LIST);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_MARGIN_SET_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_SYMBOL_MARGIN_SET_ADD);
        
        IXTradeData_listen_regist(self, PB_CMD_EOD_TIME_LIST);
        IXTradeData_listen_regist(self, PB_CMD_EOD_TIME_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_EOD_TIME_ADD);
        IXTradeData_listen_regist(self, PB_CMD_EOD_TIME_DELETE);
    }
    return self;
}

- (void)dealloc{
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_CATA_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_CATA_LIST);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_CATA_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_ADD);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_LIST);
    IXTradeData_listen_resign(self, PB_CMD_HOLIDAY_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_CATA_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_CATA_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_CATA_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_CATA_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_CATA_LIST);
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_ADD);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_CATA_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_CATA_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_ADD);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_MARGIN_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_MARGIN_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_SCHEDULE_MARGIN_ADD);
    
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_MARGIN_SET_LIST);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_MARGIN_SET_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_SYMBOL_MARGIN_SET_ADD);
    
    IXTradeData_listen_resign(self, PB_CMD_EOD_TIME_LIST);
    IXTradeData_listen_resign(self, PB_CMD_EOD_TIME_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_EOD_TIME_ADD);
    IXTradeData_listen_resign(self, PB_CMD_EOD_TIME_DELETE);
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.fd_interactivePopDisabled = YES;
    [self.navigationItem setHidesBackButton:YES];
    [self addBackItem];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    self.title = LocalizedString(@"合约属性");
    
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    [self refreshData];
    
}

- (void)rightBtnItemClicked{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshData{
    _symbolModel = [IXDataProcessTools querySymbolBySymbolId:_symbolModel.id_p];
    if (!_symbolModel) {
        return;
    }
    if (!_baseValueArr) {
        _baseValueArr = [[NSMutableArray alloc] init];
    } else {
        [_baseValueArr removeAllObjects];
    }
    if (!_condValueArr) {
        _condValueArr = [[NSMutableArray alloc] init];
    } else {
        [_condValueArr removeAllObjects];
    }
    if (!_stepArr) {
        _stepArr = [[NSMutableArray alloc] init];
    } else {
        [_stepArr removeAllObjects];
    }if (!_timeArr) {
        _timeArr = [[NSMutableArray alloc] init];
    } else {
        [_timeArr removeAllObjects];
    }
    
    [self dealCurrentTimeZone];
    [self dealBaseData];
    [self dealConditionData];
    [self dealRequireData];
    [self dealTimeData];
    [self addContractPropertieView];
    
}

- (void)dealCurrentTimeZone{
    id value =  [[NSUserDefaults standardUserDefaults] objectForKey:IXnTimeZone];
    _currentTimeZone = [value intValue];
}

- (void)dealBaseData {
    
    [_baseValueArr addObject:[IXDataProcessTools dealWithNil:_symbolModel.languageName]];
    [_baseValueArr addObject:@(_symbolModel.contractSizeNew)];
    [_baseValueArr addObject:[IXDataProcessTools dealWithNil:_symbolModel.baseCurrency]];
    [_baseValueArr addObject:[IXDataProcessTools dealWithNil:_symbolModel.profitCurrency]];
    [_baseValueArr addObject:[NSString stringWithFormat:@"%.2f%%(%@)",_symbolModel.longSwap,LocalizedString(@"年利率")]];
    [_baseValueArr addObject:[NSString stringWithFormat:@"%.2f%%(%@)",_symbolModel.shortSwap,LocalizedString(@"年利率")]];
    [_baseValueArr addObject:[IXRateCaculate symbolTradeState:[IXDataProcessTools symbolIsCanTrade:_symbolModel]]];
    [_baseValueArr addObject:[self dealEodTime]];
    NSString *expiryTime = [self expiryTime];
    if (expiryTime) {
        [_baseValueArr addObject:expiryTime];
    }
}

- (NSString *)dealEodTime{
    NSDateComponents *result = [IXDateUtils currentTimeComponents];
    NSInteger weekday = (result.weekday + 7)%8;
    //特殊设置
    NSDictionary *sDic = [IXDBEodTimeMgr queryEodTimeInfoByWeekDay:weekday type:1];
    NSString *zone = [IXDateUtils getUserSaveTimeZone];
    zone = [zone stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    if (sDic) {
        int64_t startTime = [sDic[kStartTime] longLongValue] + [zone intValue] * 60;
        return [self dealShowEodTime:startTime];
    } else {
        //通用设置
        NSDictionary *uDic =[IXDBEodTimeMgr queryEodTimeInfoByWeekDay:weekday type:0];
        if (uDic) {
            int64_t startTime = [uDic[kStartTime] longLongValue] + [zone intValue] * 60;
            return [self dealShowEodTime:startTime];
        } else {
            return @"06:00";
        }
    }
    return nil;
}

- (NSString *)expiryTime{
    if (_symbolModel.expiryTime <= 0) {
        return nil;
    }
    int64_t expiryTime = _symbolModel.expiryTime - [[[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime] longLongValue];
    if (expiryTime < 365*24*3600) {
        return [IXEntityFormatter timeIntervalToString:_symbolModel.expiryTime];
    }
    return nil;
    
}

- (NSString *)dealShowEodTime:(int64_t)startTime{
    if (startTime < 0) {
        startTime = startTime + 24*60;
    }
    int32 hour = startTime/60;
    if (hour > 24) {
        hour = hour - 24;
    }
    int32 minute = startTime%60;
    NSString *hourStr = [NSString stringWithFormat:@"%d",hour];
    if (hour < 10) {
        hourStr = [NSString stringWithFormat:@"0%d",hour];
    }
    NSString *minuteStr = [NSString stringWithFormat:@"%d",minute];
    if (minute < 10) {
        minuteStr = [NSString stringWithFormat:@"0%d",minute];
    }
    return [NSString stringWithFormat:@"%@:%@",hourStr,minuteStr];
}

- (void)dealConditionData {
    [_condValueArr addObject:@(_symbolModel.digits)];
//    NSString *volumesMin = [@(_symbolModel.volumesMin) stringValue];
//    NSString *contractSizeNew = [@(_symbolModel.contractSizeNew) stringValue];
//    NSString *volDigits = [@(_symbolModel.volDigits) stringValue];
//    NSString *volumesMax = [@(_symbolModel.volumesMax) stringValue];
//    NSString *unitLanName = _symbolModel.unitLanName;
    [_condValueArr addObject:[NSString stringWithFormat:@"%@ - %@%@",[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:_symbolModel.volumesMin contractSizeNew:_symbolModel.contractSizeNew volDigit:_symbolModel.volDigits] withDigits:_symbolModel.volDigits],[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:_symbolModel.volumesMax contractSizeNew:_symbolModel.contractSizeNew volDigit:_symbolModel.volDigits] withDigits:_symbolModel.volDigits],[IXDataProcessTools showCurrentUnitLanName:_symbolModel.unitLanName]]];
    [_condValueArr addObject:[NSString thousandFormate:[IXDataProcessTools showCurrentVolume:_symbolModel.volumesStep contractSizeNew:_symbolModel.contractSizeNew volDigit:_symbolModel.volDigits] withDigits:_symbolModel.volDigits]];
    [_condValueArr addObject:[NSString stringWithFormat:@"%@ - %@",@(_symbolModel.stopLevel),@(_symbolModel.maxStopLevel)]];
}

- (void)dealRequireData {
    
    NSArray *marginArr = [IXDataProcessTools getMarginSet:_symbolModel];
    if (marginArr && marginArr.count > 0) {
        
        _stepArr = [marginArr mutableCopy];
    }
}

- (void)dealTimeData {
    
    NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
    //查询scheduleCata是否有效
    NSDictionary *scheduleCataDic = [IXDBScheduleCataMgr queryScheduleCataByScheduleCataId:_symbolModel.scheduleCataId];
    if (!scheduleCataDic) {
        return;
    }
    //冬令时延迟交易时间(优先查询symbol表、然后再找symbolCata表)
    int scheduleDelayMinutes = _symbolModel.scheduleDelayMinutes;
    if (scheduleDelayMinutes == 0) {
        NSDictionary *symCataDic = [IXDBSymbolCataMgr querySymbolCataBySymbolCataId:_symbolModel.cataId];
        if (symCataDic) {
            scheduleDelayMinutes = [symCataDic[kScheduleDelayMinutes] integerValue];
        }
    }
    for (int i = 0; i < 7; i++) {
        NSArray *scheduleArr = [IXDBScheduleMgr queryScheduleByScheduleCataId:_symbolModel.scheduleCataId dayOfWeek:i];
        if (scheduleArr && scheduleArr.count > 0) {
            
            NSString *preDayStr,*dayStr,*nextDayStr;
           
            for (int j = 0; j < scheduleArr.count; j++) {
                
                int startHour = 0,startMinute = 0,endHour = 0,endMinute = 0,preIndex = 0,dayIndex = 0,nextIndex = 0;
                int startPreHour = 0,startPreMinute = 0,endPreHour = 0,endPreMinute = 0;
                int startNextHour = 0,startNextMinute = 0,endNextHour = 0,endNextMinute = 0;
                NSDictionary *tmpDic = [[NSDictionary alloc] init];
                tmpDic = scheduleArr[j];
                dayIndex = [tmpDic[kDayOfWeek] intValue];
                if ((dayIndex - 1) == -1 ) {
                    
                    preIndex = 6;
                } else {
                    
                    preIndex = dayIndex - 1;
                }
                preDayStr = [IXDataProcessTools weekdayToWeekString:preIndex];
                dayStr = [IXDataProcessTools weekdayToWeekString:dayIndex];
                if ((dayIndex + 1) == 7 ) {
                    
                    nextIndex = 0;
                } else {
                    
                    nextIndex = dayIndex + 1;
                }
                nextDayStr =  [IXDataProcessTools weekdayToWeekString:nextIndex];
                
                NSMutableArray *preArr = [self getSchedulePart:tmpArr weekDay:preDayStr];
                NSMutableArray *currentArr = [self getSchedulePart:tmpArr weekDay:dayStr];
                NSMutableArray *nextArr = [self getSchedulePart:tmpArr weekDay:nextDayStr];
                
                NSString *zone = [IXDateUtils getUserSaveTimeZone];
                zone = [zone stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
                int start = [tmpDic[kStartTime] intValue] + [zone intValue] * 60 + scheduleDelayMinutes;//GMT8
                int end = [tmpDic[kEndTime] intValue] + [zone intValue] * 60 + scheduleDelayMinutes;//GMT8
                startHour = start/60;
                startMinute = start%60;
                if (start < 0) {
                    
                    if (startMinute != 0) {
                        
                        startPreHour = 23 + startHour;
                        startPreMinute = 60 + startMinute;
                        
                    } else {
                        
                        startPreHour = 24 + startHour;
                    }
                    startHour = 0;
                    startMinute = 0;
                }
                
                if (startHour > 24) {
                    
                    startNextHour = startHour - 24;
                    startNextMinute = startMinute;
                    startHour = 0;
                    startMinute = 0;
                    endHour = 0;
                    endMinute = 0;
                    
                }
                endHour = end/60;
                endMinute = end%60;
                if (end < 0) {
                    
                    if (endMinute != 0) {
                        
                        endPreHour = 23 + endHour;
                        endPreMinute = 60 + endMinute;
                    } else {
                        
                        endPreHour = 24 + endHour;
                    }
                    endHour = 0;
                    endMinute = 0;
                } else {
                    
                    if ((endHour > 24) || (endHour == 24 && endMinute > 0)) {
                        
                        if (startNextHour != 0 || startNextMinute != 0) {
                            
                            endNextHour = endHour - 24;
                            endNextMinute = endMinute;
                            endHour = 0;
                            endMinute = 0;
                        } else  {
                            
                            endNextHour = endHour - 24;
                            endNextMinute = endMinute;
                            endHour = 24;
                            endMinute = 0;
                            startNextHour = 0;
                            startNextMinute = 0;
                        }
                        
                    } else {
                        
                        endPreHour = 24;
                        endPreMinute = 0;

                    }
                    
                }
                
                //前一天
                if ((startPreHour != 0 || startPreMinute != 0) && ![self startTimeIsEqualEndTime:startPreHour startMinute:startPreMinute endHour:endPreHour endMinute:endPreMinute]) {
                    
                    NSMutableDictionary *preDic = [[NSMutableDictionary alloc] init];
                    [preDic setObject:@(startPreHour) forKey:START_HOUR];
                    [preDic setObject:@(startPreMinute) forKey:START_MINUTE];
                    [preDic setObject:@(endPreHour) forKey:END_HOUR];
                    [preDic setObject:@(endPreMinute) forKey:END_MINUTE];
                    if (![self isRepeat:preArr currentTime:preDic]) {
                        
                        [preArr addObject:preDic];
                        preArr = [[self sortScheduel:preArr] mutableCopy];
                        preArr = [self removeTimeInclude:preArr];
                        tmpArr = [self setSchedulePart:tmpArr currentPart:preArr weekDay:preDayStr];
                    }
                }
                
                if (((startHour > 0 || startMinute > 0 || endHour > 0 || endMinute > 0) && (startNextHour == 0 && startNextMinute == 0)) && ![self startTimeIsEqualEndTime:startHour startMinute:startMinute endHour:endHour endMinute:endMinute]) {
                    
                    //当天
                    NSMutableDictionary *currentDic = [[NSMutableDictionary alloc] init];
                    [currentDic setObject:@(startHour) forKey:START_HOUR];
                    [currentDic setObject:@(startMinute) forKey:START_MINUTE];
                    [currentDic setObject:@(endHour) forKey:END_HOUR];
                    [currentDic setObject:@(endMinute) forKey:END_MINUTE];
                    if (![self isRepeat:currentArr currentTime:currentDic]) {
                        
                        [currentArr addObject:currentDic];
                        currentArr = [[self sortScheduel:currentArr] mutableCopy];
                        currentArr = [self removeTimeInclude:currentArr];
                        tmpArr = [self setSchedulePart:tmpArr currentPart:currentArr weekDay:dayStr];
                    }
                }
                
                //后一天
                if ((endNextHour != 0 || endNextMinute != 0) && ![self startTimeIsEqualEndTime:startNextHour startMinute:startNextMinute endHour:endNextHour endMinute:endNextMinute]) {
                    
                    NSMutableDictionary *nextDic = [[NSMutableDictionary alloc] init];
                    [nextDic setObject:@(startNextHour) forKey:START_HOUR];
                    [nextDic setObject:@(startNextMinute) forKey:START_MINUTE];
                    [nextDic setObject:@(endNextHour) forKey:END_HOUR];
                    [nextDic setObject:@(endNextMinute) forKey:END_MINUTE];
                    
                    if (![self isRepeat:nextArr currentTime:nextDic]) {
                        
                        [nextArr addObject:nextDic];
                        nextArr = [[self sortScheduel:nextArr] mutableCopy];
                        nextArr = [self removeTimeInclude:nextArr];
                        tmpArr = [self setSchedulePart:tmpArr currentPart:nextArr weekDay:nextDayStr];
                    }
                }
            }
        }
    }
    if (tmpArr && tmpArr.count > 0) {
        [self dealWithSchedule:tmpArr];
    }
}

- (BOOL)startTimeIsEqualEndTime:(int)startHour
                    startMinute:(int)startMinute
                        endHour:(int)endHour
                      endMinute:(int)endMinute{
    if ((
         startHour == startMinute &&
         startMinute == endHour &&
         endHour == endMinute
         ) ||
        (
         startHour == 24
         && endHour == 24)
        ) {
        return YES;
    } else {
        return NO;
    }
}

- (NSArray *)sortScheduel:(NSArray *)dataArr {
    
    if (!dataArr || dataArr.count == 0) {
        
        return dataArr;
    } else {
        
        NSStringCompareOptions comparisonOptions = NSCaseInsensitiveSearch|NSNumericSearch|
        NSWidthInsensitiveSearch|NSForcedOrderingSearch;
        NSComparator sort = ^(NSDictionary *dic1,NSDictionary *dic2){
            
            id parm1 = dic1[START_HOUR];
            id parm2 = dic2[START_HOUR];
            NSString *obj1 = [NSString stringWithFormat:@"%@",parm1];
            NSString *obj2 = [NSString stringWithFormat:@"%@",parm2];
            NSRange range = NSMakeRange(0,obj1.length);
            return [obj1 compare:obj2 options:comparisonOptions range:range];
        };
        
        return  [dataArr sortedArrayUsingComparator:sort];
    }
}

#pragma mark 去除时间段包含操作
- (NSMutableArray *)removeTimeInclude:(NSArray *)arr{
    if (arr.count <= 1) {
        return [arr mutableCopy];
    }
    NSMutableArray *retArr = [[NSMutableArray alloc] initWithArray:arr];
    NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
    NSInteger start_hour,start_minute,end_hour,end_minute,startMinute,endMinute;
    NSInteger next_start_hour,next_start_minute,next_end_hour,next_end_minute,nextStartMinute,nextEndMinute;
    for (int i = 0; i < (arr.count - 1); i++) {
        NSDictionary *dic = arr[i];
        start_hour = [dic[START_HOUR] integerValue];
        start_minute = [dic[START_MINUTE] integerValue];
        startMinute = start_hour*60 + start_minute;
        
        end_hour = [dic[END_HOUR] integerValue];
        end_minute = [dic[END_MINUTE] integerValue];
        endMinute = end_hour*60 + end_minute;
        
        NSDictionary *nextDic = arr[i+1];
        next_start_hour = [nextDic[START_HOUR] integerValue];
        next_start_minute = [nextDic[START_MINUTE] integerValue];
        nextStartMinute = next_start_hour*60 + next_start_minute;
        
        next_end_hour = [nextDic[END_HOUR] integerValue];
        next_end_minute = [nextDic[END_MINUTE] integerValue];
        nextEndMinute = next_end_hour*60 + next_end_minute;
        
        if (nextStartMinute <= endMinute) {
            [tmpDic setObject:dic[START_HOUR] forKey:START_HOUR];
            [tmpDic setObject:dic[START_MINUTE] forKey:START_MINUTE];
            if (nextEndMinute <= endMinute) {
                [retArr removeObjectAtIndex:(i+1)];
            } else {
                [tmpDic setObject:nextDic[END_HOUR] forKey:END_HOUR];
                [tmpDic setObject:nextDic[END_MINUTE] forKey:END_MINUTE];
                [retArr removeObjectsInRange:NSMakeRange(i, 2)];
                [retArr insertObject:tmpDic atIndex:i];
            }
            break;
        }
    }
    return retArr;
}

- (NSMutableArray *)getSchedulePart:(NSArray *)arr weekDay:(NSString *)key {
    
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    if (arr && arr.count > 0) {
        
        for (int i = 0; i < arr.count; i++) {
            
            NSDictionary *tmpDic = arr[i];
            if (tmpDic && tmpDic.count > 0) {
                
                NSArray *tmpArr = tmpDic[key];
                if (tmpArr && tmpArr.count > 0) {
                    
                    retArr = [tmpArr mutableCopy];
                    break;
                }
            }
        }
    }
    
    return retArr;
}

- (BOOL)isRepeat:(NSArray *)dataArr currentTime:(NSDictionary *)currentDic {
    
    BOOL flag = NO;
    if (dataArr.count == 0) {
        
        flag = NO;
    } else {
        
        for (int i = 0; i < dataArr.count; i++) {
            
            NSDictionary *tmpDic = dataArr[i];
            if ([tmpDic isEqualToDictionary:currentDic]) {
                
                flag = YES;
                break;
            }
        }
    }
    
    return flag;
}

- (NSMutableArray *)setSchedulePart:(NSArray *)arr currentPart:(NSArray *)dataArr weekDay:(NSString *)key {
    
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    
    if (arr && arr.count > 0) {
        
        retArr = [arr mutableCopy];
    }
    if (!dataArr || dataArr.count == 0 || !key) {
        
        return retArr;
    }
    
    BOOL flag = NO;
    for (int i = 0; i < arr.count; i++) {
        
        NSDictionary *tmpDic = arr[i];
        if (tmpDic && tmpDic.count > 0) {
            
            NSArray *allKey = [tmpDic allKeys];
            if (allKey && allKey.count > 0) {
                
                if ([allKey[0] isEqualToString:key]) {
                    
                    NSMutableDictionary *dataDic = [[NSMutableDictionary alloc] init];
                    [dataDic setObject:dataArr forKey:key];
                    [retArr replaceObjectAtIndex:i withObject:dataDic];
                    flag = YES;
                    break;
                }
            }
        }
    }
    if (!flag) {
        
        NSMutableDictionary *dataDic = [[NSMutableDictionary alloc] init];
        [dataDic setObject:dataArr forKey:key];
        [retArr addObject:dataDic];
    }
    
    
    return retArr;
}

- (NSArray *)sortScheduleResult:(NSArray *)dataArr{
    if (dataArr && dataArr.count) {
        NSArray *retArr = [dataArr sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            if ([IXDataProcessTools weekStringToWeekday:[[obj1 allKeys] objectAtIndex:0]] > [IXDataProcessTools weekStringToWeekday:[[obj2 allKeys] objectAtIndex:0]]) {
                return NSOrderedDescending;
            } else {
                return NSOrderedAscending;
            }
        }];
        return retArr;
    } else {
        return dataArr;
    }
}

- (void)dealWithSchedule:(NSArray *)dataArr {
    
    if (dataArr && dataArr.count > 0) {
        dataArr = [self sortScheduleResult:dataArr];
        for (int i = 0; i < dataArr.count; i++) {
            NSMutableArray *scheduleArr = [[NSMutableArray alloc] init];
            NSString *dayStr = [[NSString alloc] init];
            NSDictionary *tmpDic = dataArr[i];
            if (tmpDic && tmpDic.count > 0) {
                NSArray *allKey = [tmpDic allKeys];
                if (allKey && allKey.count > 0) {
                    
                    dayStr = allKey[0];
                    NSArray *tmpArr = tmpDic[dayStr];
                    if (tmpArr && tmpArr.count > 0) {
                        for (int j = 0; j < tmpArr.count; j++) {
                            
                            NSDictionary *showDic = tmpArr[j];
                            if (showDic && showDic.count > 0) {
                                
                                [scheduleArr addObject:[NSString stringWithFormat:@" %@:%@ - %@:%@",
                                                        [IXDataProcessTools formatterTimeDisplayHour:showDic[START_HOUR]],
                                                        [IXDataProcessTools formaterTimeDisplay:showDic[START_MINUTE]],
                                                        [IXDataProcessTools formatterTimeDisplayHour:showDic[END_HOUR]],
                                                        [IXDataProcessTools formaterTimeDisplay:showDic[END_MINUTE]]]];
                            }
                            
                        }
                        NSMutableDictionary *scheduleDic = [[NSMutableDictionary alloc] init];
                        [scheduleDic setObject:[IXDataProcessTools dealWithNil:dayStr] forKey:DAY_OF_WEEK];
                        [scheduleDic setObject:scheduleArr forKey:TRADE_TIME];
                        [_timeArr addObject:scheduleDic];
                    }
                }
            }
            
        }
    }
}

- (void)addContractPropertieView{
    if (_sContractView) {
        [_sContractView removeFromSuperview];
    }
    _sContractView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,
                                                                    kScreenWidth,
                                                                    self.view.bounds.size.height - kNavbarHeight)];
    _sContractView.showsVerticalScrollIndicator = NO;
    _sContractView.showsHorizontalScrollIndicator = NO;
    _sContractView.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    [self.view addSubview:_sContractView];
    
    
    float offsetMaxY = 10;
    UIView *baseView = [[UIView alloc] initWithFrame:CGRectMake(10, offsetMaxY, VIEW_W(_sContractView)  - 20, 0)];
    baseView.layer.cornerRadius = 6;
    baseView.layer.masksToBounds = YES;
    baseView.layer.dk_borderColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    baseView.layer.borderWidth = 1;
    baseView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    [_sContractView addSubview:baseView];
    
    UILabel *baseLbl = [IXCustomView createLable:CGRectMake(12, 15, VIEW_W(baseView) - 24, 15)
                                           title:LocalizedString(@"基本信息")
                                            font:PF_MEDI(13)
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea
                                   textAlignment:NSTextAlignmentLeft];
    baseLbl.backgroundColor = [UIColor clearColor];
    [baseView addSubview:baseLbl];
    
    UIView *lineBaseV = [[UIView alloc] initWithFrame:CGRectMake(0,  GetView_MaxY(baseLbl) + 14, VIEW_W(baseView), 1)];
    lineBaseV.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
    [baseView addSubview:lineBaseV];
    
    
    float offsetY = GetView_MaxY(lineBaseV);
    UIView *bgBaseView = [[UIView alloc] initWithFrame:CGRectMake(0, offsetY, VIEW_W(baseView), 0)];
    bgBaseView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x303b4d);
    [baseView addSubview:bgBaseView];
    
    offsetY = 15;
    NSArray *baseArr = (_baseValueArr.count == 8)? @[LocalizedString(@"产品名称"),
                                                     LocalizedString(@"合约单位"),
                                                     LocalizedString(@"基准货币"),
                                                     LocalizedString(@"收益货币"),
                                                     LocalizedString(@"隔夜利息(买)"),
                                                     LocalizedString(@"隔夜利息(卖)"),
                                                     LocalizedString(@"状态"),
                                                     LocalizedString(@"结算时间")]
    : @[LocalizedString(@"产品名称"),
        LocalizedString(@"合约单位"),
        LocalizedString(@"基准货币"),
        LocalizedString(@"收益货币"),
        LocalizedString(@"隔夜利息(买)"),
        LocalizedString(@"隔夜利息(卖)"),
        LocalizedString(@"状态"),
        LocalizedString(@"结算时间"),
        LocalizedString(@"到期时间")];
    CGRect frame = CGRectMake(0, 0, 0, 0);
    UIFont *valueFont = nil;
    for (int i = 0; i < baseArr.count; i++) {
        NSString *title = baseArr[i];
        if ([title isEqualToString:LocalizedString(@"合约单位")]
            || [title isEqualToString:LocalizedString(@"结算时间")]) {
            valueFont = RO_REGU(15);
        } else {
            valueFont = PF_MEDI(13);
        }
        UILabel *lbl = [IXCustomView createLable:CGRectMake(12,
                                                            offsetY,
                                                            VIEW_W(bgBaseView)/2 - 14,
                                                            15)
                                           title:baseArr[i]
                                            font:PF_MEDI(13)
                                       wTextColor:0x4c6072
                                      dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentLeft];
        [bgBaseView addSubview:lbl];
        
        UILabel *valueLbl = [IXCustomView createLable:CGRectMake(0,
                                                                 offsetY,
                                                                 VIEW_W(bgBaseView) - 14,
                                                                 15)
                                                title:[NSString stringWithFormat:@"%@",_baseValueArr[i]]
                                                 font:valueFont
                                           wTextColor:0x4c6072
                                           dTextColor:0x8395a4
                                        textAlignment:NSTextAlignmentRight];
        if ([title isEqualToString:LocalizedString(@"隔夜利息(买)")]) {
            [IXDataProcessTools resetLabel:valueLbl leftContent:[NSString stringWithFormat:@"%.2f",_symbolModel.longSwap] leftFont:15 WithContent:[NSString stringWithFormat:@"%@",_baseValueArr[i]] rightFont:15 fontType:YES];
        } else if ([title isEqualToString:LocalizedString(@"隔夜利息(卖)")]) {
            [IXDataProcessTools resetLabel:valueLbl leftContent:[NSString stringWithFormat:@"%.2f",_symbolModel.shortSwap] leftFont:15 WithContent:[NSString stringWithFormat:@"%@",_baseValueArr[i]] rightFont:15 fontType:YES];
        }
        [bgBaseView addSubview:valueLbl];
        
        offsetY = GetView_MaxY(lbl) + 15;
    }
    
    frame = bgBaseView.frame;
    frame.size.height = offsetY;
    bgBaseView.frame = frame;
    
    frame = baseView.frame;
    frame.size.height = GetView_MaxY(bgBaseView);
    baseView.frame = frame;
    
    offsetMaxY = GetView_MaxY(baseView) + 10;
    
    
    UIView *condView = [[UIView alloc] initWithFrame:CGRectMake(10, offsetMaxY, VIEW_W(_sContractView)  - 20, 0)];
    condView.layer.cornerRadius = 6;
    condView.layer.masksToBounds = YES;
    condView.layer.dk_borderColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    condView.layer.borderWidth = 1;
    condView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    [_sContractView addSubview:condView];
    
    
    UILabel *condLbl = [IXCustomView createLable:CGRectMake(12, 15, VIEW_W(condView) - 24, 15)
                                           title:LocalizedString(@"交易条件")
                                            font:PF_MEDI(13)
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea
                                   textAlignment:NSTextAlignmentLeft];
    condLbl.backgroundColor = [UIColor clearColor];
    [condView addSubview:condLbl];
    
    UIView *lineCondV = [[UIView alloc] initWithFrame:CGRectMake(0,  GetView_MaxY(condLbl) + 14, VIEW_W(condView), 1)];
    lineCondV.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
    [condView addSubview:lineCondV];
    
    offsetY = GetView_MaxY(lineCondV);
    UIView *bgCondView = [[UIView alloc] initWithFrame:CGRectMake(0, offsetY, VIEW_W(condView), 0)];
    bgCondView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x303b4d);
    [condView addSubview:bgCondView];
    
    offsetY = 15;
    NSArray *condArr = nil;
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
        condArr = @[LocalizedString(@"小数位"),
                      LocalizedString(@"单笔交易数量"),
                      LocalizedString(@"数量步差"),
                      LocalizedString(@"挂单距离")];
    } else {
        condArr = @[LocalizedString(@"小数位"),
                    LocalizedString(@"单笔交易手数"),
                    LocalizedString(@"手数步差"),
                    LocalizedString(@"挂单距离")];
    }
    for (int i = 0; i < condArr.count; i++) {
        
        UILabel *lbl = [IXCustomView createLable:CGRectMake(12, offsetY, VIEW_W(bgCondView)/2 + 10, 15)
                                           title:condArr[i]
                                            font:PF_MEDI(13)
                                      wTextColor:0x4c6072
                                      dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentLeft];
        [bgCondView addSubview:lbl];
        
        UILabel *valueLbl = [IXCustomView createLable:CGRectMake(12, offsetY, VIEW_W(bgCondView) - 24, 15)
                                                title:[NSString stringWithFormat:@"%@",_condValueArr[i]]
                                                 font:RO_REGU(15)
                                           wTextColor:0x4c6072
                                           dTextColor:0x8395a4
                                        textAlignment:NSTextAlignmentRight];
        [bgCondView addSubview:valueLbl];
        offsetY = GetView_MaxY(lbl) + 15;
    }
    
    frame = bgCondView.frame;
    frame.size.height = offsetY;
    bgCondView.frame = frame;
    
    frame = condView.frame;
    frame.size.height =  GetView_MaxY(bgCondView);
    condView.frame = frame;
    
    offsetMaxY = GetView_MaxY(condView) + 10;
    
    
    UIView *requireView = [[UIView alloc] initWithFrame:CGRectMake(10, offsetMaxY, VIEW_W(_sContractView)  - 20, 0)];
    requireView.layer.cornerRadius = 6;
    requireView.layer.masksToBounds = YES;
    requireView.layer.dk_borderColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    requireView.layer.borderWidth = 1;
    requireView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    [_sContractView addSubview:requireView];
    
    UILabel *requireLbl = [IXCustomView createLable:CGRectMake(12, 12, VIEW_W(requireView) - 24, 21)
                                              title:LocalizedString(@"保证金层级")
                                               font:PF_MEDI(13)
                                         wTextColor:0x4c6072
                                         dTextColor:0xe9e9ea
                                      textAlignment:NSTextAlignmentLeft];
    requireLbl.backgroundColor = [UIColor clearColor];
    [requireView addSubview:requireLbl];
    
    UIView *lineRequireV = [[UIView alloc] initWithFrame:CGRectMake(0,  GetView_MaxY(requireLbl) + 14, VIEW_W(requireView), 1)];
    lineRequireV.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x303b4d);
    [requireView addSubview:lineRequireV];
    
    offsetY = GetView_MaxY(lineRequireV);
    UIView *bgRequireView = [[UIView alloc] initWithFrame:CGRectMake(0, offsetY, VIEW_W(requireView), 0)];
    bgRequireView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x262f3e);
    [requireView addSubview:bgRequireView];
    
    
    UIView *requireBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, VIEW_W(bgRequireView), 33)];
    requireBgView.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x303b4d);
    [bgRequireView addSubview:requireBgView];
    
    UILabel *stepShowLbl = [IXCustomView createLable:CGRectMake(12, 9,VIEW_W(requireBgView)*7/32 - 16 ,15)
                                               title:LocalizedString(@"阶梯")
                                                font:PF_MEDI(13)
                                           wTextColor:0x4c6072
                                          dTextColor:0x8395a4
                                       textAlignment:NSTextAlignmentCenter];
    [requireBgView addSubview:stepShowLbl];
    
    NSString    *positionStr = nil;
    if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
        positionStr = [NSString stringWithFormat:@"%@%@",
                       LocalizedString(@"仓位数量"),
                       _symbolModel.unitLanName.length?
                       [NSString stringWithFormat:@"(%@)",_symbolModel.unitLanName]:@""];
    } else {
        positionStr = [NSString stringWithFormat:@"%@%@",
                       LocalizedString(@"仓位手数"),
                       _symbolModel.unitLanName.length?
                       [NSString stringWithFormat:@"(%@)",[IXDataProcessTools showCurrentUnitLanName:_symbolModel.unitLanName]]:@""];
    }
    
    UILabel *positionShowLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*7/32 + 9.5,
                                                                    9,
                                                                    VIEW_W(requireBgView)*16/32 - 2*9.5 ,
                                                                    15)
                                                   title:positionStr
                                                    font:PF_MEDI(13)
                                              wTextColor:0x4c6072
                                              dTextColor:0x8395a4
                                           textAlignment:NSTextAlignmentLeft];
    [requireBgView addSubview:positionShowLbl];
    
    UILabel *marginShowLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*23/32 + 9.5,
                                                                  7,
                                                                  VIEW_W(requireBgView)*9/32 - 2*9.5 ,
                                                                  19)
                                                 title:@"Margin"
                                                  font:PF_MEDI(13)
                                            wTextColor:0x4c6072
                                            dTextColor:0x8395a4
                                         textAlignment:NSTextAlignmentCenter];
    [requireBgView addSubview:marginShowLbl];
    
    for (int i = 0; i < 2; i++) {
        
        if (i == 0) {
            
            frame = CGRectMake(VIEW_W(requireBgView)*7/32, 0, 1, VIEW_H(requireBgView));
        } else {
            
            frame = CGRectMake(VIEW_W(requireBgView)*23/32, 0, 1, VIEW_H(requireBgView));
        }
        UIView *lineView = [[UIView alloc] initWithFrame:frame];
        lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
        [requireBgView addSubview:lineView];
    }
    
    offsetY = GetView_MaxY(requireBgView) + 15;
    
    NSString *rangStr = [[NSString alloc] init];
    NSString *marginStr = [[NSString alloc] init];
    for (int i = 0; i < _stepArr.count; i++) {
        
        NSDictionary *tmpDic = [[NSDictionary alloc] init];
        tmpDic = _stepArr[i];
        if (tmpDic && tmpDic.count > 0) {
            if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
               rangStr = [NSString stringWithFormat:@"%@ - %@",tmpDic[kRangeLeft],tmpDic[kRangeRight]];
            } else {
                if (_symbolModel.contractSizeNew) {
                    rangStr = [NSString stringWithFormat:@"%@ - %@",[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",[tmpDic[kRangeLeft] doubleValue]/_symbolModel.contractSizeNew]],[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",[tmpDic[kRangeRight] doubleValue]/_symbolModel.contractSizeNew]]];                } else {
                        rangStr = [NSString stringWithFormat:@"%@ - %@",tmpDic[kRangeLeft],tmpDic[kRangeRight]];
                    }
            }
            marginStr = [NSString stringWithFormat:@"%.2f%%",([tmpDic[kPercent] intValue])/100.0];
        } else {
            rangStr = @"--";
            marginStr = @"--";
        }
        UILabel *preLbl = [IXCustomView createLable:CGRectMake(12,
                                                               offsetY,
                                                               VIEW_W(requireBgView)*7/32 - 12*2,
                                                               15)
                                              title:[NSString stringWithFormat:@"%d",(i + 1)]
                                               font:RO_REGU(15)
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4
                                      textAlignment:NSTextAlignmentCenter];
        [bgRequireView addSubview:preLbl];
        
        UILabel *midLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*7/32 + 9.5,
                                                               offsetY,
                                                               VIEW_W(requireBgView)*16/32 - 2*9.5,
                                                               15)
                                              title:rangStr
                                               font:RO_REGU(15)
                                          wTextColor:0x4c6072
                                         dTextColor:0x8395a4
                                      textAlignment:NSTextAlignmentLeft];
        [bgRequireView addSubview:midLbl];
        
        UILabel *endLbl = [IXCustomView createLable:CGRectMake(VIEW_W(requireBgView)*23/32 + 9.5,
                                                               offsetY,
                                                               VIEW_W(requireBgView)*9/32 - 2*9.5,
                                                               15)
                                              title:marginStr
                                               font:RO_REGU(15)
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4
                                      textAlignment:NSTextAlignmentCenter];
        [bgRequireView addSubview:endLbl];
        offsetY = GetView_MaxY(preLbl) + 11;
    }
    
    frame = bgRequireView.frame;
    frame.size.height = offsetY;
    bgRequireView.frame = frame;
    
    frame = requireView.frame;
    frame.size.height = GetView_MaxY(bgRequireView);
    requireView.frame = frame;
    
    offsetMaxY = GetView_MaxY(requireView) + 10;
    
    UIView *timeView = [[UIView alloc] initWithFrame:CGRectMake(10, offsetMaxY, VIEW_W(_sContractView)  - 20, 0)];
    timeView.layer.cornerRadius = 6;
    timeView.layer.masksToBounds = YES;
    timeView.layer.dk_borderColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    timeView.layer.borderWidth = 1;
    timeView.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    [_sContractView addSubview:timeView];
    
    UILabel *timeLbl = [IXCustomView createLable:CGRectMake(12, 12, VIEW_W(timeView) - 24, 21)
                                           title:[self settingTimeZone]
                                            font:PF_MEDI(13)
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea
                                   textAlignment:NSTextAlignmentLeft];
    timeLbl.backgroundColor = [UIColor clearColor];
    [IXDataProcessTools resetLabel:timeLbl leftContent:LocalizedString(@"交易时间") leftFont:15 WithContent:[self settingTimeZone] rightFont:15 fontType:NO];
    [timeView addSubview:timeLbl];
    
    UIView *lineTimeV = [[UIView alloc] initWithFrame:CGRectMake(0,  GetView_MaxY(timeLbl) + 14, VIEW_W(timeView), 1)];
    lineTimeV.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
    [timeView addSubview:lineTimeV];
    
    offsetY = GetView_MaxY(lineTimeV);
    
    UIView *bgTimeView = [[UIView alloc] initWithFrame:CGRectMake(0, offsetY, VIEW_W(timeView), 0)];
    bgTimeView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x303b4d);
    [timeView addSubview:bgTimeView];
    
    offsetY = 15;
    for (int i = 0; i < _timeArr.count; i++) {
        
        NSDictionary *tmpDic = [[NSDictionary alloc] init];
        tmpDic = _timeArr[i];
        
        UILabel *lbl = [IXCustomView createLable:CGRectMake(12, offsetY, 40, 15)
                                           title:[IXDataProcessTools dealWithNil:tmpDic[DAY_OF_WEEK]]
                                            font:PF_MEDI(13)
                                       wTextColor:0x4c6072
                                      dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentLeft];
        [bgTimeView addSubview:lbl];
        
        NSMutableArray *timeArr = [tmpDic[TRADE_TIME] mutableCopy];
        int index = timeArr.count/2 + timeArr.count%2;
        for (int i = 0; i < index; i++) {
            
            int  j = (i+1)*2;
            NSString *tmpStr;
            if (timeArr.count%2 == 0 || j < timeArr.count) {
                
                tmpStr = [[NSString alloc] initWithFormat:@"%@ %@",timeArr[j-2],timeArr[j-1]];
            } else {
                
                if (j == timeArr.count/2) {
                    
                    tmpStr = [[NSString alloc] initWithFormat:@"%@ %@",timeArr[j-2],timeArr[j-1]];
                } else {
                    
                    tmpStr = [[NSString alloc] initWithFormat:@"%@",timeArr[j-2]];
                }
            }
            
            CGFloat width = VIEW_W(timeView) - (GetView_MaxX(lbl) + 12.5 + 12);
            UILabel *valueLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(lbl) + 12.5,
                                                                     offsetY,
                                                                     width,
                                                                     15)
                                                    title:tmpStr
                                                     font:RO_REGU(15)
                                               wTextColor:0x4c6072
                                               dTextColor:0x8395a4
                                            textAlignment:NSTextAlignmentRight];
            [bgTimeView addSubview:valueLbl];
            offsetY = GetView_MaxY(valueLbl) + 15;
        }
        
    }
    
    frame = bgTimeView.frame;
    frame.size.height = offsetY;
    bgTimeView.frame = frame;
    
    frame = timeView.frame;
    frame.size.height = GetView_MaxY(bgTimeView);
    timeView.frame = frame;
    
    offsetMaxY = GetView_MaxY(timeView) + 10;
    
    if (offsetMaxY > (kScreenHeight - 180)) {
        _sContractView.contentSize = CGSizeMake(VIEW_W(condView), offsetMaxY + 5 + kBtomMargin);
    }
}

- (NSString *)settingTimeZone{
    NSMutableString *time = [NSMutableString stringWithFormat:@"%@(%@)",LocalizedString(@"交易时间"),
                                                                        [IXDateUtils getUserSaveTimeZone]];
    return time;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context{
    //symbol
    if ([keyPath isEqualToString:PB_CMD_SYMBOL_UPDATE] ) {
        proto_symbol_update *pb = ((IXTradeDataCache *)object).pb_symbol_update;
        if (pb && pb.result == 0) {
            if (pb.id_p == _symbolModel.id_p) {
                [self refreshData];
            }
        }
    }
    
    //group_symbol
    if ([keyPath isEqualToString:PB_CMD_GROUP_SYMBOL_UPDATE] ) {
        proto_group_symbol_update *pb = ((IXTradeDataCache *)object).pb_group_symbol_update;
        if (pb && pb.result == 0) {
            for (item_group_symbol *groupSym in pb.groupSymbolArray) {
                if (groupSym.symbolid == _symbolModel.id_p) {
                    [self refreshData];
                    break;
                }
            }
        }
    }
    if ([keyPath isEqualToString:PB_CMD_GROUP_SYMBOL_ADD] ) {
        proto_group_symbol_add *pb = ((IXTradeDataCache *)object).pb_group_symbol_add;
        if (pb && pb.result == 0) {
            for (item_group_symbol *groupSym in pb.groupSymbolArray) {
                if (groupSym.symbolid == _symbolModel.id_p) {
                    [self refreshData];
                    break;
                }
            }
        }
    }
    if ([keyPath isEqualToString:PB_CMD_GROUP_SYMBOL_DELETE] ) {
        proto_group_symbol_delete *pb = ((IXTradeDataCache *)object).pb_group_symbol_delete;
        if (pb && pb.result == 0 && pb.idsArray.count) {
            [self refreshData];
        }
    }
    
    //group_symbol_cata
    if ([keyPath isEqualToString:PB_CMD_GROUP_SYMBOL_CATA_UPDATE] ) {
        proto_group_symbol_cata_update *pb = ((IXTradeDataCache *)object).pb_group_symbol_cata_update;
        if (pb && pb.result == 0) {
            for (item_group_symbol_cata *groupSym in pb.groupSymbolCataArray) {
                if ([IXDataProcessTools compareCataId0:_symbolModel.cataId cataId1:groupSym.symbolCataid]) {
                    [self refreshData];
                    break;
                }
            }
        }
    }
    if ([keyPath isEqualToString:PB_CMD_GROUP_SYMBOL_CATA_ADD] ) {
        proto_group_symbol_cata_add *pb = ((IXTradeDataCache *)object).pb_group_symbol_cata_add;
        if (pb && pb.result == 0) {
            for (item_group_symbol_cata *groupSym in pb.groupSymbolCataArray) {
                if ([IXDataProcessTools compareCataId0:_symbolModel.cataId cataId1:groupSym.symbolCataid]) {
                    [self refreshData];
                    break;
                }
            }
        }
    }
    if ([keyPath isEqualToString:PB_CMD_GROUP_SYMBOL_CATA_DELETE] ) {
        proto_group_symbol_cata_delete *pb = ((IXTradeDataCache *)object).pb_group_symbol_cata_delete;
        if (pb && pb.result == 0 && pb.idsArray.count) {
            [self refreshData];
        }
    }
    
    //schedule
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_UPDATE]) {
        proto_schedule_update *pb = ((IXTradeDataCache *)object).pb_schedule_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_ADD]) {
        proto_schedule_add *pb = ((IXTradeDataCache *)object).pb_schedule_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_LIST]) {
        proto_schedule_list *pb = ((IXTradeDataCache *)object).pb_schedule_list;
        if (pb) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_DELETE]) {
        proto_schedule_delete *pb = ((IXTradeDataCache *)object).pb_schedule_delete;
        if (pb.result == 0) {
            [self refreshData];
        }
    }
    
    //schedule_margin
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_MARGIN_UPDATE]) {
        proto_schedule_margin_update *pb = ((IXTradeDataCache *)object).pb_schedule_margin_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_MARGIN_ADD]) {
        proto_schedule_margin_add *pb = ((IXTradeDataCache *)object).pb_schedule_margin_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_MARGIN_LIST]) {
        proto_schedule_margin_list *pb = ((IXTradeDataCache *)object).pb_schedule_margin_list;
        if (pb) {
            [self refreshData];
        }
    }
    
    //symbol_margin_set
    if ([keyPath isEqualToString:PB_CMD_SYMBOL_MARGIN_SET_UPDATE]) {
        proto_symbol_margin_set_update *pb = ((IXTradeDataCache *)object).pb_symbol_margin_set_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_SYMBOL_MARGIN_SET_ADD]) {
        proto_symbol_margin_set_add *pb = ((IXTradeDataCache *)object).pb_symbol_margin_set_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_SYMBOL_MARGIN_SET_LIST]) {
        proto_symbol_margin_set_list *pb = ((IXTradeDataCache *)object).pb_symbol_margin_set_list;
        if (pb) {
            [self refreshData];
        }
    }
    
    //holiday_cata
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_CATA_UPDATE]) {
        proto_holiday_cata_update *pb = ((IXTradeDataCache *)object).pb_holiday_cata_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_CATA_ADD]) {
        proto_holiday_cata_add *pb = ((IXTradeDataCache *)object).pb_holiday_cata_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_CATA_LIST]) {
        proto_holiday_cata_list *pb = ((IXTradeDataCache *)object).pb_holiday_cata_list;
        if (pb) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_CATA_DELETE]) {
        proto_holiday_cata_delete *pb = ((IXTradeDataCache *)object).pb_holiday_cata_delete;
        if (pb.result == 0) {
            [self refreshData];
        }
    }
    
    //holiday
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_UPDATE]) {
        proto_holiday_update *pb = ((IXTradeDataCache *)object).pb_holiday_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_ADD]) {
        proto_holiday_add *pb = ((IXTradeDataCache *)object).pb_holiday_add;
        if (pb && pb.result == 0) {
           [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_LIST]) {
        proto_holiday_list *pb = ((IXTradeDataCache *)object).pb_holiday_list;
        if (pb) {
            [self refreshData];
        }
    }
    if ([keyPath isEqualToString:PB_CMD_HOLIDAY_DELETE]) {
        proto_holiday_delete *pb = ((IXTradeDataCache *)object).pb_holiday_delete;
        if (pb.result == 0) {
            [self refreshData];
        }
    }
    
    //schedule_cata
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_CATA_UPDATE]) {
        proto_schedule_cata_update *pb = ((IXTradeDataCache *)object).pb_schedule_cata_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_CATA_ADD]) {
        proto_schedule_cata_add *pb = ((IXTradeDataCache *)object).pb_schedule_cata_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_CATA_LIST]) {
        proto_schedule_cata_list *pb = ((IXTradeDataCache *)object).pb_schedule_cata_list;
        if (pb) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_SCHEDULE_CATA_DELETE]) {
        proto_schedule_cata_delete *pb = ((IXTradeDataCache *)object).pb_schedule_cata_delete;
        if (pb.result == 0) {
            [self refreshData];
        }
    }
    
    //symbol_cata
    if ([keyPath isEqualToString:PB_CMD_SYMBOL_CATA_UPDATE]) {
        proto_symbol_cata_update *pb = ((IXTradeDataCache *)object).pb_symbol_cata_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_SYMBOL_CATA_ADD]) {
        proto_symbol_cata_add *pb = ((IXTradeDataCache *)object).pb_symbol_cata_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_SYMBOL_CATA_LIST]) {
        proto_symbol_cata_list *pb = ((IXTradeDataCache *)object).pb_symbol_cata_list;
        if (pb) {
            [self refreshData];
        }
    }
    
    //eod_time
    if ([keyPath isEqualToString:PB_CMD_EOD_TIME_UPDATE]) {
        proto_eod_time_update *pb = ((IXTradeDataCache *)object).pb_eod_time_update;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_EOD_TIME_ADD]) {
        proto_eod_time_add *pb = ((IXTradeDataCache *)object).pb_eod_time_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_EOD_TIME_LIST]) {
        proto_eod_time_list *pb = ((IXTradeDataCache *)object).pb_eod_time_list;
        if (pb) {
            [self refreshData];
        }
    }
    
    if ([keyPath isEqualToString:PB_CMD_EOD_TIME_DELETE]) {
        proto_eod_time_add *pb = ((IXTradeDataCache *)object).pb_eod_time_add;
        if (pb && pb.result == 0) {
            [self refreshData];
        }
    }
}


@end
