//
//  IXDealListCell.m
//  IXApp
//
//  Created by Evn on 16/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDealListCell.h"
#import "IXDateUtils.h"
#import "NSString+FormatterPrice.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"
#import "CAGradientLayer+IX.h"

@interface IXDealListCell()

@property (nonatomic, strong)UIImageView *bgView;
@property (nonatomic, strong)UILabel *nameLbl;
@property (nonatomic, strong)UILabel *contentLbl;//内容
@property (nonatomic, strong)UILabel *showProfitLbl;
@property (nonatomic, strong)UILabel *profitLbl;//获利
@property (nonatomic, strong)UILabel *dirLbl;//买卖方向
@property (nonatomic, strong)UILabel *numLbl;//数量
@property (nonatomic, strong)UILabel *showOpenLbl;
@property (nonatomic, strong)UILabel *openPriceLbl;//开盘价
@property (nonatomic, strong)UILabel *openTimeLbl;//开盘时间
@property (nonatomic, strong)UILabel *showCloseLbl;
@property (nonatomic, strong)UILabel *closePriceLbl;//平仓价
@property (nonatomic, strong)UILabel *closeTimeLbl;//平仓时间

@end
@implementation IXDealListCell

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 104)];
        _bgView.dk_backgroundColorPicker  = DKNavBarColor;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = [IXCustomView createLable:CGRectMake(15,15, 85, 15)
                                       title:@""
                                        font:PF_MEDI(13)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_nameLbl];
    }
    return _nameLbl;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.nameLbl) + 5,VIEW_Y(self.nameLbl) + 3, GetView_MinX(self.showProfitLbl) - GetView_MaxX(self.nameLbl), 12)
                                          title:@""
                                           font:RO_REGU(10)
                                     wTextColor:0xa7adb5
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UILabel *)showProfitLbl
{
    if (!_showProfitLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"盈亏") WithFont:PF_MEDI(13)] + 1;
        _showProfitLbl = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView) - 80 - 15 - width,VIEW_Y(self.nameLbl) + 3, width, 12)
                                             title:LocalizedString(@"盈亏")
                                              font:PF_MEDI(12)
                                        wTextColor:0x99abba
                                        dTextColor:0x8395a4
                                     textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showProfitLbl];
    }
    return _showProfitLbl;
}

- (UILabel *)profitLbl
{
    if (!_profitLbl) {
        _profitLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.showProfitLbl),VIEW_Y(self.nameLbl), 80, 15)
                                         title:@""
                                          font:RO_REGU(15)
                                    wTextColor:0xa7adb5
                                    dTextColor:0xff0000
                                 textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_profitLbl];
    }
    return _profitLbl;
}

- (UILabel *)dirLbl
{
    if (!_dirLbl) {
        _dirLbl = [IXCustomView createLable:CGRectMake(15,GetView_MaxY(self.nameLbl) + 25,15, 15)
                                      title:@""
                                       font:PF_MEDI(10)
                                 wTextColor:0xffffff
                                 dTextColor:0x262f3e
                              textAlignment:NSTextAlignmentCenter];
        _dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0xff4d2d);
        [self.bgView addSubview:_dirLbl];
    }
    return _dirLbl;
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.dirLbl) + 5, GetView_MinY(self.dirLbl), 100, 16)
                                      title:@""
                                       font:RO_REGU(15)
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                              textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_numLbl];
    }
    return _numLbl;
}

- (UILabel *)showOpenLbl
{
    if (!_showOpenLbl){
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"开仓价.") WithFont:PF_MEDI(12)] + 1;
        _showOpenLbl = [IXCustomView createLable:CGRectMake(112, GetView_MaxY(self.nameLbl) + 15,width, 12)
                                           title:LocalizedString(@"开仓价.")
                                            font:PF_MEDI(12)
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showOpenLbl];
    }
    return _showOpenLbl;
}

- (UILabel *)openPriceLbl
{
    if (!_openPriceLbl) {
        _openPriceLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(_showOpenLbl) + 5, VIEW_Y(_showOpenLbl) - 2,200, 15) title:@""
                                             font:RO_REGU(15)
                                       wTextColor:0x4c6072
                                       dTextColor:0xe9e9ea
                                    textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_openPriceLbl];
    }
    return _openPriceLbl;
}

- (UILabel *)openTimeLbl
{
    if (!_openTimeLbl) {
        _openTimeLbl = [IXCustomView createLable:CGRectMake(kScreenWidth/2, VIEW_Y(_showOpenLbl),kScreenWidth/2 - 15,12)
                                           title:@"--" font:RO_REGU(12)
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4
                                   textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_openTimeLbl];
    }
    return _openTimeLbl;
}

- (UILabel *)showCloseLbl
{
    if (!_showCloseLbl){
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"平仓价.") WithFont:PF_MEDI(12)] + 1;
        _showCloseLbl = [IXCustomView createLable:CGRectMake(112, GetView_MaxY(_showOpenLbl) + 15,width, 12)
                                            title:LocalizedString(@"平仓价.")
                                             font:PF_MEDI(12)
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentLeft];
        _showCloseLbl.hidden = YES;
        [self.bgView addSubview:_showCloseLbl];
    }
    return _showCloseLbl;
}

- (UILabel *)closePriceLbl
{
    if (!_closePriceLbl) {
        _closePriceLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(_showCloseLbl) + 5, VIEW_Y(_showCloseLbl) - 2,200, 15)
                                             title:@"" font:RO_REGU(15)
                                        wTextColor:0x4c6072
                                        dTextColor:0xe9e9ea
                                     textAlignment:NSTextAlignmentLeft];
        _closePriceLbl.hidden = YES;
        [self.bgView addSubview:_closePriceLbl];
    }
    return _closePriceLbl;
}

- (UILabel *)closeTimeLbl
{
    if (!_closeTimeLbl) {
        _closeTimeLbl = [IXCustomView createLable:CGRectMake(kScreenWidth/2, VIEW_Y(_showCloseLbl),kScreenWidth/2 - 15,12)
                                            title:@"--" font:RO_REGU(12)
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                    textAlignment:NSTextAlignmentRight];
        _closeTimeLbl.hidden = YES;
        [self.bgView addSubview:_closeTimeLbl];
    }
    return _closeTimeLbl;
}

- (void)addShadow:(CGFloat)offsetY
{
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * topL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 5)
                                                        dkcolors:topColors];
    [self.contentView.layer addSublayer:topL];
    
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, offsetY, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [self.contentView.layer addSublayer:btomL];
    
    self.layer.masksToBounds = NO;
    self.contentView.layer.masksToBounds = NO;
}

- (void)reloadUIWithDealModel:(IXDealM *)model
{
    self.nameLbl.text = model.symbolModel.languageName;
    CGRect frame = self.nameLbl.frame;
    CGFloat width = [IXEntityFormatter getContentWidth:self.nameLbl.text WithFont:PF_MEDI(13)];
    frame.size.width = width;
    self.nameLbl.frame = frame;
    frame = self.contentLbl.frame;
    frame.origin.x = VIEW_X(self.nameLbl) + VIEW_W(self.nameLbl) + 4.5;
    self.contentLbl.frame = frame;
    self.contentLbl.text = [NSString stringWithFormat:@"%@:%@",model.symbolModel.name,model.symbolModel.marketName];
    [IXDataProcessTools resetTextColorLabel:self.profitLbl value:model.deal.profit];
    self.profitLbl.text = [IXDataProcessTools moneyFormatterComma:model.deal.profit positiveNumberSign:NO];
    width = [IXEntityFormatter getContentWidth:self.profitLbl.text WithFont:RO_REGU(15)];
    frame = self.profitLbl.frame;
    frame.size.width = width;
    frame.origin.x = VIEW_W(self.bgView) - width - 15;
    self.profitLbl.frame = frame;
    
    width = [IXEntityFormatter getContentWidth:self.showProfitLbl.text WithFont:PF_MEDI(12)];
    frame = self.showProfitLbl.frame;
    frame.size.width = width;
    frame.origin.x = VIEW_W(self.bgView) - VIEW_W(self.profitLbl) - width - 20;
    self.showProfitLbl.frame = frame;
    if ( model.deal.direction == item_deal_edirection_DirectionBuy) {
        self.dirLbl.text = LocalizedString(@"买.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }else{
        self.dirLbl.text = LocalizedString(@"卖.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    if (model.deal.reason == item_order_etype_TypeOpen || model.deal.reason == item_order_etype_TypeLimit || model.deal.reason == item_order_etype_TypeStop || model.deal.reason == item_order_etype_TypeReopen ) {
        [self addShadow:82];
        self.showOpenLbl.text = LocalizedString(@"开仓价.");
        width = [IXEntityFormatter getContentWidth:LocalizedString(@"开仓价.") WithFont:PF_MEDI(12)] + 1;
        CGRect rect = self.showOpenLbl.frame;
        rect.size.width = width;
        self.showOpenLbl.frame = rect;
        rect = self.openPriceLbl.frame;
        rect.origin.x = VIEW_X(self.showOpenLbl) + VIEW_W(self.showOpenLbl) + 5;
        self.openPriceLbl.frame = rect;
        self.showCloseLbl.hidden = YES;
        self.closePriceLbl.hidden = YES;
        self.closeTimeLbl.hidden = YES;
        frame.size.height = 82;
        self.frame = frame;
        self.bgView.frame = CGRectMake(0, 5, kScreenWidth, 77);
        
        frame = self.dirLbl.frame;
        frame.origin.y = GetView_MaxY(self.nameLbl) + 11;
        self.dirLbl.frame = frame;
        
        frame = self.numLbl.frame;
        frame.origin.y = GetView_MinY(self.dirLbl);
        self.numLbl.frame = frame;
        
        self.numLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.deal.execVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits];
        self.openPriceLbl.text =  [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.deal.execPrice] WithDigits:model.symbolModel.digits];
        self.openTimeLbl.text = [IXEntityFormatter quoteTimeToString:model.deal.execTime];
        frame = self.openTimeLbl.frame;
        frame.origin.y = VIEW_Y(self.showOpenLbl);
        self.openTimeLbl.frame = frame;
        
    } else if (model.deal.reason == item_order_etype_TypeClose || model.deal.reason == item_order_etype_TypeStopLoss || model.deal.reason == item_order_etype_TypeTakeProfit || model.deal.reason == item_order_etype_TypeStopOut || model.deal.reason == item_order_etype_TypeCloseOneAccount ||
               model.deal.reason == item_order_etype_TypeSettlement || model.deal.reason == item_order_etype_TypeCloseBy
               ) {
        [self addShadow:109];
        self.showOpenLbl.text = LocalizedString(@"开仓价.");
        width = [IXEntityFormatter getContentWidth:LocalizedString(@"开仓价.") WithFont:PF_MEDI(12)] + 1;
        CGRect rect = self.showOpenLbl.frame;
        
        rect.size.width = width;
        self.showOpenLbl.frame = rect;
        rect = self.openPriceLbl.frame;
        rect.origin.x = VIEW_X(self.showOpenLbl) + VIEW_W(self.showOpenLbl) + 5;
        self.openPriceLbl.frame = rect;
        frame = self.frame;
        frame.size.height = 109;
        self.frame = frame;
        self.bgView.frame = CGRectMake(0, 5, kScreenWidth, 104);
        
        self.showCloseLbl.hidden = NO;
        self.closePriceLbl.hidden = NO;
        self.closeTimeLbl.hidden = NO;
        
        frame = self.dirLbl.frame;
        frame.origin.y = GetView_MaxY(self.nameLbl) + 25;
        self.dirLbl.frame = frame;
        
        frame = self.numLbl.frame;
        frame.origin.y = GetView_MinY(self.dirLbl);
        self.numLbl.frame = frame;
        
        self.numLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.deal.execVolume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits];
        self.openPriceLbl.text =  [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.deal.srcPrice] WithDigits:model.symbolModel.digits];
        self.openTimeLbl.text = [IXEntityFormatter quoteTimeToString:model.deal.srcTime];
        
        self.showCloseLbl.text = LocalizedString(@"平仓价.");
        width = [IXEntityFormatter getContentWidth:LocalizedString(@"平仓价.") WithFont:PF_MEDI(12)] + 1;
        rect = self.showCloseLbl.frame;
        rect.size.width = width;
        self.showCloseLbl.frame = rect;
        rect = self.closePriceLbl.frame;
        rect.origin.x = VIEW_X(self.showCloseLbl) + VIEW_W(self.showCloseLbl) + 5;
        self.closePriceLbl.frame = rect;
        self.closePriceLbl.text =  [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.deal.execPrice] WithDigits:model.symbolModel.digits];
        self.closeTimeLbl.text = [IXEntityFormatter quoteTimeToString:model.deal.execTime];
    }
}

@end

