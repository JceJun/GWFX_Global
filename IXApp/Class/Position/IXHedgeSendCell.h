//
//  IXHedgeSendCell.h
//  IXApp
//
//  Created by Bob on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXHedgeM.h"

@interface IXHedgeSendCell : UITableViewCell

@property (nonatomic, strong) IXHedgeM *model;

@property (nonatomic, strong) NSString *profit;

@end
