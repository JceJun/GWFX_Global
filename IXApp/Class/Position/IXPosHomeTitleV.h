//
//  IXPosHomeTitleV.h
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 持仓Home页title view
 */
@interface IXPosHomeTitleV : UIView

@property (nonatomic, copy)void (^itemClicked)(int idx);

@end
