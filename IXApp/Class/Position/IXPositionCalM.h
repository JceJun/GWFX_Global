//
//  IXPositionCalM.h
//  IXApp
//
//  Created by Evn on 2017/6/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXPositionInfoVC.h"
#import "IXOpenTipV.h"

@interface IXPositionCalM : NSObject

//刷新保证金和佣金,百万分之一
//行情推回来的时候，是主线程
//仓位算保证金与数量无关
+ (NSArray *)resetMarginAssetWithRoot:(IXPositionInfoVC *)root;
+ (void)addPrcTipInfo:(IXPositionInfoVC *)root;
+ (void)removePrcTipInfo:(IXPositionInfoVC *)root;
+ (void)showTip:(NSString *)checkOrderInfo
       WithView:(IXOpenTipV *)view
         InView:(UIView *)rootView;
+ (void)removeTip:(IXOpenTipV *)view;
+ (NSString *)checkResult:(IXPositionInfoVC *)root;
+ (void)updateTipInfoWithView:(IXOpenTipV *)view
                     WithRoot:(IXPositionInfoVC *)root;

@end
