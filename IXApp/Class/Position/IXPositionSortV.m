//
//  IXPositionSortV.m
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionSortV.h"

#define kHideFrame  CGRectMake(0, - 150, kScreenWidth, 150)
#define kShowFrame  CGRectMake(0, 0, kScreenWidth, 150)

@interface  IXPositionSortV ()

@property (nonatomic, strong)UIView     * mainV;
@property (nonatomic, strong)UIButton   * dismissBtn;
@property (nonatomic, strong)UIButton   * btn1; //排序选项
@property (nonatomic, strong)UIButton   * btn2; //排序选项
@property (nonatomic, strong)UIButton   * btn3; //排序选项

@end
@implementation IXPositionSortV

- (instancetype)init
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, kNavbarHeight + 44, kScreenWidth,  kScreenHeight - kNavbarHeight - 44);
        self.clipsToBounds = YES;
        [self createSubView];
    }
    return self;
}

- (void)createSubView
{
    _dismissBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, self.frame.size.height)];
    [_dismissBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_dismissBtn];
    
    _mainV = [[UIView alloc] initWithFrame:CGRectMake(0, -150, kScreenWidth, 150)];
    _mainV.dk_backgroundColorPicker = DKNavBarColor;
    [self addSubview:_mainV];
    
    // -------------
    
    UILabel * lab1 = [UILabel labelWithFrame:CGRectMake(20, 0, kScreenWidth - 40, 50)
                                        text:LocalizedString(@"最近开仓时间")
                                    textFont:PF_MEDI(13)
                                   textColor:DKCellTitleColor];
    [_mainV addSubview:lab1];
    
    _btn1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    _btn1.selected = YES;
    [_btn1 addTarget:self action:@selector(btn1Action:) forControlEvents:UIControlEventTouchUpInside];
    [_btn1 setImageEdgeInsets:UIEdgeInsetsMake(17, kScreenWidth - 35, 17, 18)];
    [_btn1 dk_setImage:DKImageNames(@"common_cell_choose", @"common_cell_choose_D") forState:UIControlStateSelected];
    [_mainV addSubview:_btn1];
    
    // -------------
    
    UILabel * lab2 = [UILabel labelWithFrame:CGRectMake(20, 50, kScreenWidth - 40, 50)
                                        text:LocalizedString(@"开仓价从高到低")
                                    textFont:PF_MEDI(13)
                                   textColor:DKCellTitleColor];
    [_mainV addSubview:lab2];
    
    _btn2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 50, kScreenWidth, 50)];
    [_btn2 addTarget:self action:@selector(btn2Action:) forControlEvents:UIControlEventTouchUpInside];
    [_btn2 setImageEdgeInsets:UIEdgeInsetsMake(17, kScreenWidth - 35, 17, 18)];
    [_btn2 dk_setImage:DKImageNames(@"common_cell_choose", @"common_cell_choose_D") forState:UIControlStateSelected];
    [_mainV addSubview:_btn2];
    
     // -------------
    
    UILabel * lab3 = [UILabel labelWithFrame:CGRectMake(20, 100, kScreenWidth - 40, 50)
                                        text:LocalizedString(@"开仓价从低到高")
                                    textFont:PF_MEDI(13)
                                   textColor:DKCellTitleColor];
    [_mainV addSubview:lab3];
    
    _btn3 = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, kScreenWidth, 50)];
    [_btn3 addTarget:self action:@selector(btn3Action:) forControlEvents:UIControlEventTouchUpInside];
    [_btn3 setImageEdgeInsets:UIEdgeInsetsMake(17, kScreenWidth - 35, 17, 18)];
    [_btn3 dk_setImage:DKImageNames(@"common_cell_choose", @"common_cell_choose_D") forState:UIControlStateSelected];
    [_mainV addSubview:_btn3];
}

- (void)btn1Action:(UIButton *)btn
{
    if (self.sortB) {
        self.sortB(PositionSortTypeDefault);
    }
    
    btn.selected = YES;
    _btn2.selected = NO;
    _btn3.selected = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}

- (void)btn2Action:(UIButton *)btn
{
    if (self.sortB) {
        self.sortB(PositionSortTypePriceHL);
    }
    
    btn.selected = YES;
    _btn1.selected = NO;
    _btn3.selected = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}

- (void)btn3Action:(UIButton *)btn
{
    if (self.sortB) {
        self.sortB(PositionSortTypePriceLH);
    }

    btn.selected = YES;
    _btn1.selected = NO;
    _btn2.selected = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}

- (void)show
{
    _isShow = YES;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _mainV.frame = CGRectMake(0, 0, kScreenWidth, 150);
        _dismissBtn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    } completion:nil];
}

- (void)dismiss
{
    if (self.dismissB) {
        self.dismissB();
    }
    _isShow = NO;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _mainV.frame = CGRectMake(0, -150, kScreenWidth, 150);
        _dismissBtn.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
