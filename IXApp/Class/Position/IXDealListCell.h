//
//  IXDealListCell.h
//  IXApp
//
//  Created by Evn on 16/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXDealM.h"

@interface IXDealListCell : UITableViewCell

- (void)reloadUIWithDealModel:(IXDealM *)model;

@end
