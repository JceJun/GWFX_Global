//
//  IXContractPropertiesVC.h
//  IXApp
//
//  Created by Evn on 17/2/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXSymbolM.h"

/** 合约属性 */
@interface IXContractPropertiesVC : IXDataBaseVC

@property (nonatomic, strong)IXSymbolM *symbolModel;
@end
