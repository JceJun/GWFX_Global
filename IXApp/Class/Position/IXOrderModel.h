//
//  IXOrderModel.h
//  IXApp
//
//  Created by Evn on 16/12/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IxItemOrder.pbobjc.h"
#import "IXSymModel.h"

@interface IXOrderModel : NSObject

@property (nonatomic, strong) item_order *order;
@property (nonatomic, strong) IXSymModel *symbolModel;
@property (nonatomic, assign) NSInteger marketId;           //市场Id

@end
