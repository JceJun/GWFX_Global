//
//  IXHedgeSendCell.m
//  IXApp
//
//  Created by Bob on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXHedgeSendCell.h"


@interface IXHedgeSendCell ()

@property (nonatomic, strong) UIImageView *bgImage;

@property (nonatomic, strong) UILabel *posDirLbl;

@property (nonatomic, strong) UILabel *posVolLbl;

@property (nonatomic, strong) UILabel *prcTipLbl;

@property (nonatomic, strong) UILabel *openPrcLbl;

@property (nonatomic, strong) UILabel *proTipLbl;

@property (nonatomic, strong) UILabel *profitLbl;

@end

@implementation IXHedgeSendCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        [self bgImage];

    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self prcTipLbl];
    [self proTipLbl];
}

- (void)setModel:(IXHedgeM *)model
{
    self.posDirLbl.text = model.posDir;
    if ( [model.posDir isEqualToString:LocalizedString(@"买.")] ) {
        self.posDirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.posDirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }else{
        self.posDirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.posDirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    
    self.posVolLbl.text = model.showVol;
    self.openPrcLbl.text = model.posPrc;
    
}

- (void)setProfit:(NSString *)profit
{
    self.profitLbl.text = [NSString formatterPriceSign:[profit doubleValue] WithDigits:2];
    if( [profit doubleValue] > 0 ){
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }else if ( [profit doubleValue] == 0 ){
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
    }else{
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
}

- (UIImageView *)bgImage
{
    if ( !_bgImage ) {
        _bgImage = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 35)];
        _bgImage.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        [self.contentView addSubview:_bgImage];
    }
    return _bgImage;
}

- (UILabel *)posDirLbl
{
    if ( !_posDirLbl ) {
        _posDirLbl = [IXUtils createLblWithFrame:CGRectMake( 12, 10, 15, 15)
                                        WithFont:PF_MEDI(12)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0xffffff
                                      dTextColor:0x262f3e];
        [self.contentView addSubview:_posDirLbl];
    }
    return _posDirLbl;
}

- (UILabel *)posVolLbl
{
    if ( !_posVolLbl ) {
        _posVolLbl = [IXUtils createLblWithFrame:CGRectMake( 33, 10, 100, 15)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea];
        [self.contentView addSubview:_posVolLbl];
    }
    return _posVolLbl;
}


- (UILabel *)prcTipLbl
{
    if ( !_prcTipLbl ) {
        _prcTipLbl = [IXUtils createLblWithFrame:CGRectMake( 133, 10, 38, 15)
                                        WithFont:PF_MEDI(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_prcTipLbl];
        _prcTipLbl.text = LocalizedString(@"开仓价.");
    }
    return _prcTipLbl;
}

- (UILabel *)openPrcLbl
{
    if ( !_openPrcLbl ) {
        _openPrcLbl = [IXUtils createLblWithFrame:CGRectMake( 171, 10, kScreenWidth - 237, 15)
                                         WithFont:RO_REGU(15)
                                        WithAlign:NSTextAlignmentLeft
                                       wTextColor:0x4c6072
                                       dTextColor:0xe9e9ea];
        [self.contentView addSubview:_openPrcLbl];
    }
    return _openPrcLbl;
}

- (UILabel *)proTipLbl
{
    if ( !_proTipLbl ) {
        _proTipLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth - 110, 10, 60, 15)
                                        WithFont:PF_MEDI(12)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x99abba
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_proTipLbl];
        _proTipLbl.text = LocalizedString(@"盈亏");
    }
    return _proTipLbl;
}

- (UILabel *)profitLbl
{
    if ( !_profitLbl ) {
        _profitLbl = [IXUtils createLblWithFrame:CGRectMake( kScreenWidth - 80, 10, 80, 15)
                                        WithFont:RO_REGU(15)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x4c6072
                                      dTextColor:0x8395a4];
        [self.contentView addSubview:_profitLbl];
    }
    return _profitLbl;
}

@end
