//
//  IXPositionM.m
//  IXApp
//
//  Created by Bob on 2016/12/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXPositionM.h"

@interface IXPositionM ()


@property (nonatomic, strong) getPriceRange priceRange;

@property (nonatomic, strong) getProfitRange profitRange;

@property (nonatomic, strong) getLossRange lossRange;


@end

@implementation IXPositionM


- (id)initWithSymbolModel:(IXSymModel *)model
              withPositon:(item_position *)position
           WithPriceRange:(getPriceRange)price
          WithProfitRange:(getProfitRange)profit
            WithLossRange:(getLossRange)loss
{
    self = [self init];
    if ( self ) {

        _profitRange = profit;
        _lossRange = loss;
        _symbolModel = model;
        _position = position;
        
//        _minPrice = [NSDecimalNumber decimalNumberWithString:
//                     [NSString stringWithFormat:@"%d",model.stopLevel * model.pipsRatio]];
        _minPrice = [NSDecimalNumber decimalNumberWithString:
                     [NSString stringWithFormat:@"%d",model.stopLevel]];
        _minPrice = [_minPrice decimalNumberByMultiplyingByPowerOf10:-model.digits];
        
//        _maxPrice = [NSDecimalNumber decimalNumberWithString:
//                     [NSString stringWithFormat:@"%d",model.maxStopLevel * model.pipsRatio]];
        _maxPrice = [NSDecimalNumber decimalNumberWithString:
                     [NSString stringWithFormat:@"%d",model.maxStopLevel]];
        _maxPrice =  [_maxPrice decimalNumberByMultiplyingByPowerOf10:-model.digits];
        
//        _stepPrice = [NSDecimalNumber decimalNumberWithString:
//                      [NSString stringWithFormat:@"%d",model.pipsRatio]];
        _stepPrice = [NSDecimalNumber decimalNumberWithString:@"1"];
        _stepPrice = [_stepPrice decimalNumberByMultiplyingByPowerOf10:-model.digits];
        
    }
    return self;
}

@end
