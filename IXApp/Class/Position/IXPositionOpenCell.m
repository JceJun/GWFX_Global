//
//  IXPositionOpenCell.m
//  IXApp
//
//  Created by Evn on 17/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionOpenCell.h"

@interface IXPositionOpenCell()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UIImageView *icon;
@property (nonatomic, strong)UIView *uLineV;
@property (nonatomic, strong)UIView *dLineV;
@end

@implementation IXPositionOpenCell

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (UILabel *)title
{
    if (!_title) {
        _title = [IXCustomView createLable:CGRectMake(0,7, kScreenWidth, 15)
                                     title:@""
                                      font:PF_MEDI(12)
                                 wTextColor:0x99abba
                                dTextColor:0x8395a4
                             textAlignment:NSTextAlignmentCenter];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UIImageView *)icon
{
    if (!_icon)
    {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(GetView_MaxX(_title) + 4, 12, 9, 5)];
        [_icon setImage:GET_IMAGE_NAME(@"positionModify_down")];
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (UIView *)uLineV
{
    if (!_uLineV) {
        _uLineV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
        [self.contentView addSubview:_uLineV];
    }
    return _uLineV;
}

- (UIView *)dLineV
{
    if (!_dLineV) {
        _dLineV = [[UIView alloc] initWithFrame:CGRectMake(0, 30 - kLineHeight, kScreenWidth, kLineHeight)];
        [self.contentView addSubview:_dLineV];
    }
    return _dLineV;
}

- (void)reloadUI:(BOOL)flag
{
    self.uLineV.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    self.dLineV.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    self.contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
    if (flag) {
        self.title.text = LocalizedString(@"收起市场深度");
        [self.icon setImage:GET_IMAGE_NAME(@"positionModify_down")];
    } else {
        self.title.text = LocalizedString(@"展开市场深度");
        [self.icon setImage:GET_IMAGE_NAME(@"positionModify_up")];
    }
    CGFloat width = [IXEntityFormatter getContentWidth:self.title.text WithFont:PF_MEDI(12)];
    CGRect frame = self.icon.frame;
    frame.origin.x = kScreenWidth/2 + width/2 + 3;
    self.icon.frame = frame;
}

- (void)reloadPositionUI:(BOOL)flag
{
    self.uLineV.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    self.dLineV.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    self.contentView.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x242a36);
    if (flag) {
        self.title.text = LocalizedString(@"收起详情");
        [self.icon setImage:GET_IMAGE_NAME(@"positionModify_down")];
    } else {
        self.title.text = LocalizedString(@"展开详情");
        [self.icon setImage:GET_IMAGE_NAME(@"positionModify_up")];
    }
    CGFloat width = [IXEntityFormatter getContentWidth:self.title.text WithFont:PF_MEDI(12)];
    CGRect frame = self.icon.frame;
    frame.origin.x = kScreenWidth/2 + width/2 + 3;
    self.icon.frame = frame;
}

@end
