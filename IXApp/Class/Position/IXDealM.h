//
//  IXDealM.h
//  IXApp
//
//  Created by Evn on 16/12/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IxItemDeal.pbobjc.h"
#import "IXSymModel.h"

@interface IXDealM : NSObject

@property (nonatomic, strong) item_deal *deal;
@property (nonatomic, strong) IXSymModel *symbolModel;
@property (nonatomic, strong) IXQuoteM *quoteModel;
@property (nonatomic, assign) NSInteger marketId;           //市场Id

@end
