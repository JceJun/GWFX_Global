//
//  IXPositionQuoteM.h
//  IXApp
//
//  Created by Evn on 2017/6/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXPositionInfoVC.h"

@interface IXPositionQuoteM : NSObject

//订阅数据
+ (NSMutableArray *)subscribeDynamicPrice:(IXPositionInfoVC *)root;
+ (void)cancelDynamicPrice:(IXPositionInfoVC *)root;
+ (void)loadDetailQuote:(IXPositionInfoVC *)root;
+ (void)cancelDetailQuote:(IXPositionInfoVC *)root;

@end
