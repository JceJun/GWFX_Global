//
//  IXPositionCalM.m
//  IXApp
//
//  Created by Evn on 2017/6/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionCalM.h"
#import "IXAccountBalanceModel.h"
#import "IXOpenModel+CheckData.h"
#import "IXUserDefaultM.h"
#import "IXComCalM.h"

#define BASELBLTAG 100
#define PROFITLBLTAG 200
#define POSITIONLBLTAG 300
#define MARGINLBLTAG 400

@implementation IXPositionCalM

#pragma mark view logic
+ (void)showTip:(NSString *)checkOrderInfo WithView:(IXOpenTipV *)view InView:(UIView *)rootView
{
    view.tipInfo = checkOrderInfo;
    if ( !view.isShow ) {
        view.isShow = YES;
        [rootView addSubview:view];
    }
}

+ (void)removeTip:(IXOpenTipV *)view
{
    if ( view ) {
        view.isShow = NO;
        [view removeFromSuperview];
    }
}

+ (void)updateTipInfoWithView:(IXOpenTipV *)view WithRoot:(IXPositionInfoVC *)root
{
    [IXPositionCalM removeTip:view];
    NSString *checkOrderInfo = [IXPositionCalM checkResult:root];
    if ( checkOrderInfo.length != 0 ) {
        [IXPositionCalM showTip:[IXPositionCalM checkResult:root] WithView:view InView:root.view];
    }
}

+ (NSString *)checkResult:(IXPositionInfoVC *)root
{
    if (![root.openModel validPositionVolume:root.positionModel.requestVolume]) {
        if ( ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount ) ) {
            return LocalizedString(@"你输入的数量不在范围内，请重新输入");
        }
        return LocalizedString(@"你输入的手数不在范围内，请重新输入");
    }else if ( (![root.openModel validProfit:root.positionModel.takeprofit]) && ([root.positionModel.takeprofit doubleValue] != 0) && root.openModel.showSLCount > 0) {
        return LocalizedString(@"你输入的止盈不在范围内，请重新输入");
    }else if ( (![root.openModel validLoss:root.positionModel.stoploss]) && ([root.positionModel.stoploss doubleValue] != 0) && root.openModel.showSLCount > 0) {
        return LocalizedString(@"你输入的止损不在范围内，请重新输入");
    }
    return @"";
}

//刷新保证金和佣金,百万分之一
//行情推回来的时候，是主线程
+ (NSArray *)resetMarginAssetWithRoot:(IXPositionInfoVC *)root
{
    NSString *symbolKey = [NSString stringWithFormat:@"%llu",root.positionModel.symbolModel.id_p];
    NSInteger direction = 1;
    id subDic = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symbolKey];
    if ( subDic && [subDic isKindOfClass:[NSNumber class]] ) {
        direction = [subDic integerValue];
    }else if( subDic && [subDic isKindOfClass:[NSDictionary class]] ){
        NSDictionary *dic = [(NSDictionary *)subDic objectForKey:POSITIONMODEL];
        direction = [dic integerForKey:SWAPDIRECRION];
    }
    
    double swap =  [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.positionModel.symbolModel.id_p
                                                                   WithOffsetPrice:-1];
    
    double marginSwap = [[IXAccountBalanceModel shareInstance] caculateMarginSwapWithSymbolId:root.positionModel.symbolModel.id_p
                                                                                WithDirection:direction
                                                                              WithOffsetPrice:-1];
    
#if DEBUG
    [IXPositionCalM showRefreashPrc:root];
#endif
    
    if ( swap != 0 ) {
        //没有查找过才查询
        if ( root.openModel.commissionRate == 0 ) {
            root.openModel.commissionRate =  [IXRateCaculate queryComissionWithSymbolId:root.positionModel.symbolModel.id_p
                                                                       WithSymbolCataId:root.positionModel.symbolModel.cataId];
            root.openModel.cataId = root.positionModel.symbolModel.cataId;
        }
        
        double marginSet = [[IXAccountBalanceModel shareInstance] marginCurrentRateWithVolume:root.positionModel.position.volume
                                                                                   WithSymbol:root.positionModel.symbolModel];
        double margin = 0;
        double commission = 0;
        
        double marginPrice;
        double commissionPrice;
        
        if ( root.positionModel.positionDir == item_order_edirection_DirectionBuy ) {
            if (root.positionModel.quoteModel.BuyPrc.count > 0) {
                marginPrice = [root.positionModel.quoteModel.BuyPrc[0] doubleValue];
                commissionPrice = [root.positionModel.quoteModel.BuyPrc[0] doubleValue];
            } else {
                return nil;
            }
            
        }else{
            if (root.positionModel.quoteModel.SellPrc.count > 0) {
                marginPrice = [root.positionModel.quoteModel.SellPrc[0] doubleValue];
                commissionPrice = [root.positionModel.quoteModel.SellPrc[0] doubleValue];
            } else {
                return nil;
            }
        }
        
        //如果为股票，保证金需要使用最新价
        if(STOCKID == root.openModel.parentId ||
           IDXMARKETID == root.openModel.marketId){
            marginPrice = root.positionModel.quoteModel.nPrice;
        }
        
        //计算保证金
        if(root.positionModel.symbolModel.cataId == FXCATAID){
            margin = marginSwap * marginSet;
        }else{
            margin = swap * marginPrice * marginSet;
        }
        
        NSDecimalNumber *decMargin = [NSDecimalNumber decimalNumberWithString:
                                      [NSString stringWithFormat:@"%lf",margin]];
        decMargin = [decMargin decimalNumberByRoundingAccordingToBehavior:
                     [IXPositionCalM handlerPriceWithDigit:2]];
        
        root.positionModel.margin = margin;
        
        //计算佣金，佣金可能为负；如果是负对用户来说是赚，需要重新计算汇率
        if( root.openModel.commissionRate < 0 ){
            swap = [[IXAccountBalanceModel shareInstance] caculateSwapWithSymbolId:root.positionModel.symbolModel.id_p
                                                                   WithOffsetPrice:0];
        }
        
        if([IXAccountBalanceModel shareInstance].commissionType){
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:root.positionModel.position.volume
                                   conSize:root.openModel.symbolModel.contractSizeNew];
        }else{
            commission = [IXComCalM cacCom:root.openModel.commissionRate
                                       vol:root.positionModel.position.volume
                                      swap:swap
                                     price:commissionPrice];
        }
        
        NSDecimalNumber *decCommission = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",commission]];
        decCommission = [decCommission decimalNumberByRoundingAccordingToBehavior:
                         [IXPositionCalM handlerPriceWithDigit:2]];
        return @[decMargin];
        
    }
    return nil;
}

+ (UILabel *)getbaseLbl:(IXPositionInfoVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:BASELBLTAG];
    return lbl;
}

+ (UILabel *)getProfitLbl:(IXPositionInfoVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:PROFITLBLTAG];
    return lbl;
}

+ (UILabel *)getPositionLbl:(IXPositionInfoVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:POSITIONLBLTAG];
    return lbl;
}

+ (UILabel *)getMarginLbl:(IXPositionInfoVC *)root
{
    UILabel *lbl = (UILabel *)[root.navigationController.navigationBar viewWithTag:MARGINLBLTAG];
    return lbl;
}


//更新提示
+ (void)showRefreashPrc:(IXPositionInfoVC *)root
{
    NSString *symId = [NSString stringWithFormat:@"%llu",root.positionModel.symbolModel.id_p];
    id value = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symId];
    if ( value && [value isKindOfClass:[NSDictionary class]] ) {
        for ( NSString *key in [value allKeys] ) {
            
            NSDictionary *curDic = [(NSDictionary *)value objectForKey:key];
            
            if ( [curDic objectForKey:SYMBOLID] ) {
                
                if ( [key isEqualToString:BASEMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    if ( quote.SellPrc.count > 0 && quote.BuyPrc.count > 0 ) {
                        [IXPositionCalM getbaseLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                                 sModel.name,
                                                                 quote.SellPrc[0],
                                                                 quote.BuyPrc[0]];
                    }
                }else if ( [key isEqualToString:PROFITMODEL] ) {
                    
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];

                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
              
                    if ( quote.SellPrc.count > 0 && quote.BuyPrc.count > 0 ) {
                        [IXPositionCalM getProfitLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                                   sModel.name,
                                                                   quote.SellPrc[0],
                                                                   quote.BuyPrc[0]];
                    }
                    
                }else if ( [key isEqualToString:POSITIONMODEL] ) {
                    if(STOCKID == root.openModel.parentId ||
                       IDXMARKETID == root.openModel.marketId){
                        [IXPositionCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ lastPrice %lf",
                                                                 root.positionModel.symbolModel.name,
                                                                 root.positionModel.quoteModel.nPrice];
                    } else {
                        if ( root.positionModel.quoteModel.BuyPrc.count > 0 && root.positionModel.quoteModel.SellPrc.count > 0 ) {
                            [IXPositionCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                                         root.positionModel.symbolModel.name,
                                                                         root.positionModel.quoteModel.SellPrc[0],
                                                                         root.positionModel.quoteModel.BuyPrc[0]];
                        }
                    }
                }else if ( [key isEqualToString:MAGNSYMMODEL] ) {
                    IXSymbolM *sModel = [[IXAccountBalanceModel shareInstance] symModelWithId:
                                         [[curDic stringForKey:SYMBOLID] longLongValue]];
                    IXQuoteM *quote = [[IXAccountBalanceModel shareInstance] getQuoteDataBySymbolId:
                                       [[curDic stringForKey:SYMBOLID] longLongValue]];
                    
                    [IXPositionCalM getMarginLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                               sModel.name,
                                                               quote.SellPrc[0],
                                                               quote.BuyPrc[0]];
                }
            }
        }
    }else{
        if (root.positionModel.quoteModel.BuyPrc.count > 0 && root.positionModel.quoteModel.SellPrc.count > 0 ) {
            if(STOCKID == root.openModel.parentId ||
               IDXMARKETID == root.openModel.marketId){
                [IXPositionCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ lastPrice %lf",
                                                         root.positionModel.symbolModel.name,
                                                         root.positionModel.quoteModel.nPrice];
            } else {
                [IXPositionCalM getPositionLbl:root].text = [NSString stringWithFormat:@"%@ sellTopPrice %@ buyTopPrice %@",
                                                             root.positionModel.symbolModel.name,
                                                             root.positionModel.quoteModel.SellPrc[0],
                                                             root.positionModel.quoteModel.BuyPrc[0]];
            }
        }
    }
}


//添加提示
+ (void)addPrcTipInfo:(IXPositionInfoVC *)root
{
    if ( ![IXPositionCalM getPositionLbl:root] ){
        UILabel *posLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 0, kScreenWidth, 10)
                                             WithFont:PF_MEDI(10)
                                            WithAlign:NSTextAlignmentLeft
                                           wTextColor:0x4c6072
                                           dTextColor:0x4c6072];
        posLbl.tag = POSITIONLBLTAG;
        [root.navigationController.navigationBar addSubview:posLbl];
    }
    
    if ( ![IXPositionCalM getProfitLbl:root] ) {
        UILabel *profitLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 11, kScreenWidth, 10)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x4c6072
                                              dTextColor:0x4c6072];
        profitLbl.tag = PROFITLBLTAG;
        [root.navigationController.navigationBar addSubview:profitLbl];
    }
    
    if (![IXPositionCalM getbaseLbl:root] ) {
        UILabel *baseLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 22, kScreenWidth, 10)
                                              WithFont:PF_MEDI(10)
                                             WithAlign:NSTextAlignmentLeft
                                            wTextColor:0x4c6072
                                            dTextColor:0x4c6072];
        baseLbl.tag = BASELBLTAG;
        [root.navigationController.navigationBar addSubview:baseLbl];
    }
    
    if (![IXPositionCalM getMarginLbl:root]) {
        UILabel *marginLbl = [IXUtils createLblWithFrame:CGRectMake( 50, 33, kScreenWidth, 10)
                                                WithFont:PF_MEDI(10)
                                               WithAlign:NSTextAlignmentLeft
                                              wTextColor:0x4c6072
                                              dTextColor:0x4c6072];
        marginLbl.tag = MARGINLBLTAG;
        [root.navigationController.navigationBar addSubview:marginLbl];
    }
}

+ (void)removePrcTipInfo:(IXPositionInfoVC *)root
{
    UILabel *pos = [IXPositionCalM getPositionLbl:root];
    if ( pos ) {
        [pos removeFromSuperview];
    }
    
    UILabel *base = [IXPositionCalM getbaseLbl:root];
    if ( base ) {
        [base removeFromSuperview];
    }
    
    UILabel *profit = [IXPositionCalM getProfitLbl:root];
    if ( profit ) {
        [profit removeFromSuperview];
    }
    
    UILabel *margin = [IXPositionCalM getMarginLbl:root];
    if ( margin ) {
        [margin removeFromSuperview];
    }
    
    
}

+ (NSDecimalNumberHandler *)handlerPriceWithDigit:(NSInteger)digit
{
    NSDecimalNumberHandler *roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:digit
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
    
    return roundingBehavior;
}


@end
