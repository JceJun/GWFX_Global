//
//  IIXPositionHomeVC.m
//  IXApp
//
//  Created by bob on 16/11/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXPositionHomeVC.h"
#import "IXPositionVC.h"
#import "IXOrderListVC.h"
#import "IXDealListVC.h"
#import "IXSymSearchVC.h"

#import "IXLeftNavView.h"
#import "IXPullDownMenu.h"
#import "IXPosHomeTitleV.h"

#import "IXAccountBalanceModel.h"
#import "IXQuoteDistribute.h"
#import "IXTCPRequest_net.h"
#import "UIImageView+AFNetworking.h"
#import "IXCpyConfig.h"
#import "IXUserDefaultM.h"
#import "UIImageView+WebCache.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"

@interface IXPositionHomeVC ()

@property (nonatomic, strong) UIViewController  * curVC;
@property (nonatomic, strong) IXPositionVC  * positionVC;
@property (nonatomic, strong) IXOrderListVC * orderListVC;
@property (nonatomic, strong) IXDealListVC  * dealListVC;
@property (nonatomic, strong) UIView        * lineView;
@property (nonatomic, strong) IXLeftNavView * leftNavItem;
//@property (nonatomic, copy) NSString        * curTitle;

@end

@implementation IXPositionHomeVC

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(night_updateColor) name:DKNightVersionThemeChangingNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(drawWillOpen)
                                                     name:@"drawWillOpenNotify"
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self resetHeadImage];
}

#pragma mark -
#pragma mark - IXTradeDataKVOs
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ( IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_CHANGE) ){
        [self resetAccountType];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavItem];

    [self addRightItemWithImageNamed:@"position_edit" target:self action:@selector(rightPosItemClicked:)];
   
    IXPosHomeTitleV * titleV = [IXPosHomeTitleV new];
    weakself;
    titleV.itemClicked = ^(int idx) {
        [weakSelf changeSegWithIdx:idx];
    };
    
    self.navigationItem.titleView = titleV;
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    
    [self.view addSubview:self.lineView];
    [self changeSegWithIdx:0];
}

//切换日夜间模式
- (void)night_updateColor
{
    [self resetHeadImage];
}

- (void)drawWillOpen
{
    [_positionVC dismissFolterView];
}

- (void)leftBtnItemClicked
{
    [_positionVC dismissFolterView];
    if ([IXUserInfoMgr shareInstance].isDemeLogin) {
        [self showRegistLoginAlert];
    } else {
        [[AppDelegate getRootVC] toggleLeftView];
    }
}

- (void)rightPosItemClicked:(id)sender
{
    if ([IXUserInfoMgr shareInstance].isDemeLogin) {
        [self showRegistLoginAlert];
        return;
    }
    [_positionVC dismissFolterView];
    
    if (_positionVC.isEditingPosition) {
        [_positionVC endEditPosition];
    } else {
        [_positionVC editPosition];
    }
}

- (void)rightBtnItemClicked:(id)sender
{
    IXSymSearchVC *searchVC = [[IXSymSearchVC alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)resetAccountType
{
    self.leftNavItem.typeLab.text = [IXDataProcessTools showCurrentAccountType];
}

//重设头像
- (void)resetHeadImage
{
    [self.leftNavItem.iconImgV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                                 placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                                          options:SDWebImageAllowInvalidSSLCertificates];
}

#pragma mark -
#pragma mark - lazy load

 - (UIImage*)createImageWithColor:(UIColor*) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 0.5, 30.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, kScreenWidth, 0.5)];
        _lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    }
    return _lineView;
}

- (IXPositionVC *)positionVC
{
    if(!_positionVC){
        _positionVC = [[IXPositionVC alloc] init];
        _positionVC.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        [self addChildViewController:_positionVC];
    }
    return _positionVC;
}

- (IXOrderListVC *)orderListVC
{
    if(!_orderListVC){
        _orderListVC = [[IXOrderListVC alloc] init];
        _orderListVC.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight );
        [self addChildViewController:_orderListVC];
    }
    return _orderListVC;
}

- (IXDealListVC *)dealListVC
{
    if(!_dealListVC){
        _dealListVC = [[IXDealListVC alloc] init];
        _dealListVC.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight );
        [self addChildViewController:_dealListVC];
    }
    return _dealListVC;
}

- (IXLeftNavView *)leftNavItem
{
    if (!_leftNavItem) {
        _leftNavItem = [[IXLeftNavView alloc] initWithTarget:self action:@selector(leftBtnItemClicked)];
    }
    return _leftNavItem;
}

#pragma mark -
#pragma mark - private 

- (void)changeSegWithIdx:(int)idx
{
    switch (idx) {
        case 0:
        {
            //仓位
            [self.view addSubview:self.positionVC.view];
            [self.orderListVC.view removeFromSuperview];
            [self.dealListVC.view removeFromSuperview];
            self.curVC = self.positionVC;
            [self addRightItemWithDayImgNamed:@"position_edit"
                                nightImgNamed:@"position_edit"
                                       target:self action:@selector(rightPosItemClicked:)];
        }
            break;
        case 1:
        {
            //挂单
            [_positionVC dismissFolterView];
            [self.view addSubview:self.orderListVC.view];
            [self.positionVC.view removeFromSuperview];
            [self.dealListVC.view removeFromSuperview];
            self.curVC = self.orderListVC;
            [self addRightItemWithDayImgNamed:@"common_search_avatar"
                                nightImgNamed:@"common_search_avatar"
                                       target:self action:@selector(rightBtnItemClicked:)];
        }
            break;
        case 2:
        {
            //成交
            [_positionVC dismissFolterView];
            [self.view addSubview:self.dealListVC.view];
            [self.positionVC.view removeFromSuperview];
            [self.orderListVC.view removeFromSuperview];
            self.curVC = self.dealListVC;
            [self addRightItemWithDayImgNamed:@"common_search_avatar"
                                nightImgNamed:@"common_search_avatar"
                                       target:self action:@selector(rightBtnItemClicked:)];
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - IXQuoteDistribute
- (void)needRefresh
{
    id obj = self.navigationController.topViewController;
    if ( ![obj isKindOfClass:[self class]] ) {
        
        if ([obj respondsToSelector:@selector(needRefresh)]) {
            [obj needRefresh];
        }
    }
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if ([self.navigationController.topViewController isKindOfClass:[self class]]) {
        id<IXQuoteDistribute> obj = (id<IXQuoteDistribute>)self.curVC;
    
        if ([obj respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)]) {
            [obj didResponseQuoteDistribute:arr cmd:cmd];
        }
    }else{
        id<IXQuoteDistribute> obj = (id<IXQuoteDistribute>)self.navigationController.topViewController;
        if ([obj respondsToSelector:@selector(didResponseQuoteDistribute:cmd:)]) {
            [obj didResponseQuoteDistribute:arr cmd:cmd];
        }
    }
}

@end
