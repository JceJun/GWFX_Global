//
//  IXPositionHeaderV.h
//  IXApp
//
//  Created by Seven on 2017/11/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXPositionHeaderV : UIView

/** 保证金 */
@property (nonatomic, strong) UILabel   * marginLab;
/** 保证金状态 */
@property (nonatomic, strong) UILabel   * marginStateLab;
/** 保证金比例 */
@property (nonatomic, strong) UILabel   * marginRateLab;

/** 自由资金 */
@property (nonatomic, strong) UILabel   * amountLab;
/** 盈亏 */
@property (nonatomic, strong) UILabel   * profitLab;

/** 保证金比例进度 */
@property (nonatomic, strong) UIView    * rateV;



/**
 刷新保证金比例进度条

 @param rate 保证金比例
 */
- (void)resetMarginRateWith:(CGFloat)rate dkColor:(DKColorPicker)color;

@end
