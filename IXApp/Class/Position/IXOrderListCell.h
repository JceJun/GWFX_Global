//
//  IXOrderListCell.h
//  IXApp
//
//  Created by Magee on 16/11/23.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXOrderModel.h"

@interface IXOrderListCell : UITableViewCell

- (void)reloadUIWithOrderModel:(IXOrderModel *)model;

@end
