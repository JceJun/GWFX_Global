//
//  IXUpdateOrderVC.h
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXOpenModel.h"

@class IXTradeMarketModel;
@interface IXUpdateOrderVC : IXDataBaseVC

@property (nonatomic, strong) IXTradeMarketModel *tradeModel;
@property (nonatomic, strong) IXOpenModel *openModel;
@property (nonatomic, assign) item_order_edirection direction;
@property (nonatomic, assign) double nLastClosePrice;

@end
