//
//  IXPositionM.h
//  IXApp
//
//  Created by Bob on 2016/12/2.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IXQuoteM.h"
#import "IxItemPosition.pbobjc.h"
#import "IXSymModel.h"

typedef void(^getPriceRange)(NSString *minPrice,NSString *maxPrice);
typedef void(^getProfitRange)(NSString *minPrice,NSString *maxPrice);
typedef void(^getLossRange)(NSString *minPrice,NSString *maxPrice);


@interface IXPositionM : NSObject

@property (nonatomic, strong) NSDecimalNumber *minRequestPrice;
@property (nonatomic, strong) NSDecimalNumber *maxRequestPrice;

@property (nonatomic, strong) NSDecimalNumber *minProfit;
@property (nonatomic, strong) NSDecimalNumber *maxProfit;

@property (nonatomic, strong) NSDecimalNumber *minLoss;
@property (nonatomic, strong) NSDecimalNumber *maxLoss;

@property (nonatomic, assign) double minLimitBuyPrice;
@property (nonatomic, assign) double maxLimitBuyPrice;

@property (nonatomic, assign) double minLimitSellPrice;
@property (nonatomic, assign) double maxLimitSellPrice;

@property (nonatomic, assign) double minStopBuyPrice;
@property (nonatomic, assign) double maxStopBuyPrice;

@property (nonatomic, assign) double minStopSellPrice;
@property (nonatomic, assign) double maxStopSellPrice;


//基点
@property (nonatomic, strong) NSDecimalNumber *minPrice;
@property (nonatomic, strong) NSDecimalNumber *maxPrice;
@property (nonatomic, strong) NSDecimalNumber *stepPrice;

//输入的价格
@property (nonatomic, strong) NSDecimalNumber *dealPrice;

@property (nonatomic, strong) NSDecimalNumber *requestVolume;
@property (nonatomic, strong) NSDecimalNumber *showVolume;

@property (nonatomic, strong) NSDecimalNumber *requestPrice;
@property (nonatomic, strong) NSDecimalNumber *stoploss;

@property (nonatomic, strong) NSDecimalNumber *takeprofit;

@property (nonatomic, assign) double nTakeProfit;
@property (nonatomic, assign) double  margin;//占用保证金

@property (nonatomic, strong) NSString *orderType;
@property (nonatomic, assign) item_position_edirection positionDir;

@property (nonatomic, assign) float profit;

@property (nonatomic, strong) item_position *position;

@property (nonatomic, strong) IXQuoteM *quoteModel;
@property (nonatomic, strong) IXSymModel *symbolModel;
@property (nonatomic, assign) NSInteger marketId;           //市场Id
@property (nonatomic, copy)   NSString *requestType;

@property (nonatomic, assign) BOOL  choosed;   //批量平仓选中状态

@property (nonatomic, assign) BOOL closeBy;

- (id)initWithSymbolModel:(IXSymModel *)model
              withPositon:(item_position *)position
           WithPriceRange:(getPriceRange)price
          WithProfitRange:(getProfitRange)profit
            WithLossRange:(getLossRange)loss;

@end
