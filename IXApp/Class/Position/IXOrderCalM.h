//
//  IXOrderCalM.h
//  IXApp
//
//  Created by Evn on 2017/6/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXUpdateOrderVC.h"
#import "IXOpenTipV.h"

@interface IXOrderCalM : NSObject

//刷新保证金和佣金,百万分之一
//行情推回来的时候，是主线程
+ (NSArray *)resetMarginAssetWithVolume:(double)volume
                               WithRoot:(IXUpdateOrderVC *)root;
+ (void)addPrcTipInfo:(IXUpdateOrderVC *)root;
+ (void)removePrcTipInfo:(IXUpdateOrderVC *)root;
+ (void)showTip:(NSString *)checkOrderInfo
       WithView:(IXOpenTipV *)view
         InView:(UIView *)rootView;
+ (void)removeTip:(IXOpenTipV *)view;
+ (NSString *)checkResult:(IXUpdateOrderVC *)root;
+ (void)updateTipInfoWithView:(IXOpenTipV *)view
                     WithRoot:(IXUpdateOrderVC *)root;

@end
