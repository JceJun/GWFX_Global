//
//  IXPositionCell.m
//  JDTest
//
//  Created by ixiOSDev on 16/11/9.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#import "IXPositionCell.h"
#import "IXCustomView.h"
#import "IXDataProcessTools.h"
#import "IXPositionM.h"
#import "NSString+FormatterPrice.h"
#import "IXUserDefaultM.h"

#import "CAGradientLayer+IX.h"

#define kLeftMargin 60

@interface IXPositionCell()
<UIScrollViewDelegate>
{
    float prePrice;
    float nLastClosePrice;
    float currentPrice;//当前价
}

@property (nonatomic, strong)UIImageView *bgView;
@property (nonatomic, strong)UILabel *nameLbl;
@property (nonatomic, strong)UILabel *contentLbl;   //内容
@property (nonatomic, strong)UILabel *stopLbl;      //止损
@property (nonatomic, strong)UILabel *profitLbl;    //获利
@property (nonatomic, strong)UILabel *dirLbl;   //买卖方向
@property (nonatomic, strong)UILabel *numLbl;   //数量
@property (nonatomic, strong)UILabel *priceLbl; //开盘价
@property (nonatomic, strong)UILabel *showNowPriceLbl;
@property (nonatomic, strong)UILabel *nowPriceLbl;
@property (nonatomic, strong)UILabel *showResultLbl;
@property (nonatomic, strong)UILabel *resultLbl;

@property (nonatomic, strong) UILabel *openPrcTitleLbl;
@property (nonatomic, strong) UILabel *curPrcTitleLbl;

@property (nonatomic, strong) UIScrollView  * bgScrollV;
@property (nonatomic, strong) UIButton      * selectBtn; //选择按钮
@property (nonatomic, strong) UIButton  * quickBtn; //快速平仓按钮
@property (nonatomic, strong) UIButton  * hedgeBtn; //对冲平仓按钮

@property (nonatomic, assign) CGFloat   rightMargin;    //scroll view contentSize.width超出屏幕宽度的部分

@end


@implementation IXPositionCell

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _rightMargin = 70;
        self.dk_backgroundColorPicker = DKTableColor;
        [self selectBtn];
        [self quickBtn];
        [self hedgeBtn];
        [self bgScrollV];
        [self bgView];
        
        [self nameLbl];
        [self contentLbl];
        
        [self openPrcTitleLbl];
        [self priceLbl];
        
        [self showNowPriceLbl];
        [self nowPriceLbl];
        
        [self showResultLbl];
        [self resultLbl];
        [self addShadow];
        
        [self.contentView bringSubviewToFront:self.bgScrollV];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(beginDragingNoti:)
                                                     name:kBeginDragingKey
                                                   object:nil];
    }
    return self;
}

- (void)addShadow
{
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * topL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 5)
                                                        dkcolors:topColors];
    [self.bgScrollV.layer addSublayer:topL];
 
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];

    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 104, kScreenWidth, 10)
                                                            dkcolors:btomColors];
    [self.bgScrollV.layer addSublayer:btomL];
    
    self.layer.masksToBounds = NO;
    self.bgScrollV.layer.masksToBounds = NO;
}

- (void)beginDragingNoti:(NSNotification*)noti
{

    if (noti.object != self) {
        if ([self ixIsEditing] || self.bgScrollV.decelerating) {
            if (self.bgScrollV.decelerating) {
                [self.bgScrollV setContentOffset:self.bgScrollV.contentOffset animated:NO];
            }
            
            [self.bgScrollV setContentOffset:CGPointMake(0, 0) animated:YES];
            [self.contentView bringSubviewToFront:self.bgScrollV];
            self.userInteractionEnabled = NO;
        }
    }
}

#pragma mark -
#pragma mark - btn action & gesture

- (void)chooseAction
{
    _selectBtn.selected = !_selectBtn.selected;
    _model.choosed = _selectBtn.selected;
}

- (void)quickCloseAction
{
    if (self.quickB) {
        self.quickB(_model);
    }
    
    [self.bgScrollV setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.contentView bringSubviewToFront:self.bgScrollV];
}

- (void)hedgeCloseAction
{
    if (self.hedgeB) {
        self.hedgeB(_model);
    }

    [self.bgScrollV setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.contentView bringSubviewToFront:self.bgScrollV];
}

- (void)tapAction
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kBeginDragingKey object:self];
    if (![self ixIsEditing]) {
        if (self.tapB) {
            self.tapB(_model,self);
        }
    } else {
        [self endEditing];
    }
}

#pragma mark -
#pragma mark - setter

- (void)setHasHedge:(BOOL)hasHedge
{
    _hasHedge = hasHedge;
    self.hedgeBtn.hidden = !hasHedge;
    _rightMargin = hasHedge ? 140 : 70;
    
    if (hasHedge) {
        _rightMargin = 140;
    } else {
        _hasHedge = 70;
    }
    
    self.bgScrollV.scrollEnabled = YES;
    self.bgScrollV.userInteractionEnabled =YES;
    self.bgScrollV.contentSize = CGSizeMake(kScreenWidth + _rightMargin, 109);
}

- (void)setMultiSelection:(BOOL)multiSelection
{
    if (_multiSelection != multiSelection) {
        _multiSelection = multiSelection;
        if (multiSelection) {
            [self.contentView bringSubviewToFront:self.bgScrollV];
            [self.bgScrollV setContentOffset:CGPointMake(-kLeftMargin, 0) animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.selectBtn.alpha = 1;
            });
        } else {
            [self.bgScrollV setContentOffset:CGPointMake(0, 0) animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.selectBtn.alpha = 1;
            });
        }
    }
    self.bgScrollV.scrollEnabled = !multiSelection;
}

- (void)setModel:(IXPositionM *)model
{
    _model = model;
    [self config:model];
    _selectBtn.selected = model.choosed;
}

- (void)config:(IXPositionM *)model
{
    self.nameLbl.text = model.symbolModel.languageName;
    CGRect frame = self.nameLbl.frame;
    CGFloat width = [IXEntityFormatter getContentWidth:self.nameLbl.text WithFont:PF_MEDI(13)];
    frame.size.width = width;
    self.nameLbl.frame = frame;
    frame = self.contentLbl.frame;
    frame.origin.x = VIEW_X(self.nameLbl) + VIEW_W(self.nameLbl) + 5;
    self.contentLbl.frame = frame;
    self.contentLbl.text = [NSString stringWithFormat:@"%@:%@",model.symbolModel.name,model.symbolModel.marketName];
    if (model.position.takeProfit > 0) {
        self.profitLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x262f3e);
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x8395a4);
    } else {
        self.profitLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
        self.profitLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x3a4553);
    }
    
    if (model.position.stopLoss > 0) {
        self.stopLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x262f3e);
        self.stopLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x8395a4);
    } else {
        self.stopLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x262f3e);
        self.stopLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x3a4553);
    }
    if (model.position.direction == 1) {
        self.dirLbl.text = LocalizedString(@"买.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    } else {
        self.dirLbl.text = LocalizedString(@"卖.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    self.numLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume: model.position.volume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits];
    self.priceLbl.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.position.openPrice] WithDigits:model.symbolModel.digits];
}

- (void)setLastClosePriceInfo:(IXLastQuoteM *)model
{
    nLastClosePrice = model.nLastClosePrice;
}

- (void)setCurrentPrice:(double)priceInfo
             WithProfit:(double)profit
          WithQuoteTime:(NSNumber *)time
             WithSymbol:(IXSymModel *)model
{
    if ( priceInfo - prePrice > 0 ) {
        self.nowPriceLbl.dk_textColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }else if ( priceInfo - prePrice == 0 ){
        self.nowPriceLbl.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
    }else{
        self.nowPriceLbl.dk_textColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    }
    self.nowPriceLbl.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",priceInfo] WithDigits:model.digits];
    prePrice = priceInfo;
    currentPrice = priceInfo;
    
    [IXDataProcessTools resetTextColorLabel:self.resultLbl value:profit];
    self.resultLbl.text = [IXDataProcessTools moneyFormatterComma:profit positiveNumberSign:NO];
    NSString *displayName = [NSString stringWithFormat:@"%@:%@ %@",model.name,model.marketName,[IXEntityFormatter quoteTimeToString:[time longLongValue]]];
    self.contentLbl.text = displayName;
}


#pragma mark -
#pragma mark - scroll view

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (!scrollView.tracking) {
        if (scrollView.contentOffset.x > _rightMargin/2) {
            [self beginEditing];
        } else {
            [self endEditing];
        }
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (targetContentOffset->x > _rightMargin/2) {
        [self beginEditing];
    } else {
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kBeginDragingKey object:self];
    if (scrollView.contentOffset.x > 1) {
        [self.contentView bringSubviewToFront:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x <= 0) {
        self.selectBtn.alpha = -(scrollView.contentOffset.x/kLeftMargin);
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if ([self ixIsEditing]) {
        [self.contentView bringSubviewToFront:self.quickBtn];
        [self.contentView bringSubviewToFront:self.hedgeBtn];
    }
    self.userInteractionEnabled = YES;
}

- (void)beginEditing
{
    self.quickBtn.enabled = [IXDataProcessTools isNormalByState:_tradeState];
    self.hedgeBtn.enabled = [IXDataProcessTools isNormalByState:_tradeState];
    
    if (self.quickBtn.enabled) {
        [self.contentView bringSubviewToFront:self.quickBtn];
        [self.contentView bringSubviewToFront:self.hedgeBtn];
        _quickBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
        _hedgeBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
    } else {
        _quickBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
        _hedgeBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
    }
    
    [self.bgScrollV setContentOffset:CGPointMake(_rightMargin, 0) animated:YES];
}

- (void)endEditing
{
    [self.bgScrollV setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.contentView bringSubviewToFront:self.bgScrollV];
}

#pragma mark -
#pragma mark - getter

- (BOOL)ixIsEditing
{
    return self.bgScrollV.contentOffset.x > _rightMargin/2;
}

- (UIScrollView *)bgScrollV
{
    if (!_bgScrollV) {
        _bgScrollV = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 109)];
        _bgScrollV.contentSize = CGSizeMake(kScreenWidth + _rightMargin, 109);
        _bgScrollV.showsHorizontalScrollIndicator = NO;
        _bgScrollV.backgroundColor = [UIColor clearColor];
        _bgScrollV.delegate = self;
        _bgScrollV.bounces = NO;
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(tapAction)];
        [_bgScrollV addGestureRecognizer:tap];
        
        [self.contentView addSubview:_bgScrollV];
    }
    return _bgScrollV;
}

- (UIButton *)selectBtn
{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc] initWithFrame:CGRectMake(12, 5, 40, 99)];
        [_selectBtn addTarget:self action:@selector(chooseAction) forControlEvents:UIControlEventTouchUpInside];
        [_selectBtn setImage:[UIImage imageNamed:@"positionFilter_unchoose"] forState:UIControlStateNormal];
        [_selectBtn dk_setImage:DKImageNames(@"cerFile_checked", @"cerFile_checked_D") forState:UIControlStateSelected];
        [_selectBtn setImageEdgeInsets:UIEdgeInsetsMake(39.5, 10, 39.5, 10)];
        _selectBtn.alpha = 0;
        _selectBtn.userInteractionEnabled = NO;
        [self.contentView addSubview:_selectBtn];
    }
    return _selectBtn;
}

- (UIButton *)quickBtn
{
    if (!_quickBtn) {
        _quickBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 70, 5, 70, 99)];
        _quickBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
        _quickBtn.titleLabel.font = PF_MEDI(13);
        _quickBtn.titleLabel.numberOfLines = 0;
        [_quickBtn setTitle:@"Quick\nClose" forState:UIControlStateNormal];
        [_quickBtn dk_setTitleColorPicker:DKColorWithRGBs(0xffffff, 0xe9e9ea) forState:UIControlStateNormal];
        [_quickBtn addTarget:self action:@selector(quickCloseAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_quickBtn];
    }
    return _quickBtn;
}

- (UIButton *)hedgeBtn
{
    if (!_hedgeBtn) {
        _hedgeBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 140, 5, 70, 99)];
        _hedgeBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
        _hedgeBtn.titleLabel.font = PF_MEDI(13);
        _hedgeBtn.titleLabel.numberOfLines = 0;
        _hedgeBtn.hidden = YES;
        [_hedgeBtn setTitle:LocalizedString(@"对冲平仓") forState:UIControlStateNormal];
        [_hedgeBtn dk_setTitleColorPicker:DKColorWithRGBs(0xffffff, 0xe9e9ea) forState:UIControlStateNormal];
        [_hedgeBtn addTarget:self action:@selector(hedgeCloseAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_hedgeBtn];
    }
    return _hedgeBtn;
}

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, kScreenWidth, 99)];
        _bgView.dk_backgroundColorPicker = DKNavBarColor;
        [self.bgScrollV addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [IXCustomView createLable:CGRectMake(15,15, 85, 15)
                                       title:@""
                                        font:PF_MEDI(13)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentLeft];
        
        [self.bgView addSubview:_nameLbl];
    }
    return _nameLbl;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.nameLbl) + 5,VIEW_Y(self.nameLbl) + 3, GetView_MinX(self.stopLbl) - GetView_MaxX(self.nameLbl), 12)
                                          title:@""
                                           font:RO_REGU(10)
                                     wTextColor:0xa7adb5
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UILabel *)profitLbl
{
    if (!_profitLbl) {
        _profitLbl = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView) - 40, VIEW_Y(self.nameLbl), 25, 15)
                                         title:LocalizedString(@"止盈")
                                          font:PF_MEDI(10)
                                    wTextColor:0xffffff
                                    dTextColor:0x3a4553
                                 textAlignment:NSTextAlignmentCenter];
        _profitLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xa3aebb,0x262f3e);
        [self.bgView addSubview:_profitLbl];
    }
    return _profitLbl;
}

- (UILabel *)stopLbl
{
    if (!_stopLbl) {
        _stopLbl = [IXCustomView createLable:CGRectMake(VIEW_W(self.bgView) - 70, VIEW_Y(self.nameLbl), 25, 15)
                                       title:LocalizedString(@"止损")
                                        font:PF_MEDI(10)
                                  wTextColor:0xffffff
                                  dTextColor:0x3a4553
                               textAlignment:NSTextAlignmentCenter];
        _stopLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xa3aebb,0x262f3e);
        
        [self.bgView addSubview:_stopLbl];
    }
    return _stopLbl;
}

- (UILabel *)dirLbl
{
    if (!_dirLbl) {
        _dirLbl = [IXCustomView createLable:CGRectMake(15,GetView_MaxY(self.nameLbl) + 25,15, 15)
                                      title:@""
                                       font:PF_MEDI(10)
                                 wTextColor:0xffffff
                                 dTextColor:0x262f3e
                              textAlignment:NSTextAlignmentCenter];
        _dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0xff4d2d);
        [self.bgView addSubview:_dirLbl];
    }
    return _dirLbl;
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.dirLbl) + 5,
                                                       GetView_MinY(self.dirLbl),
                                                       100,
                                                       16)
                                      title:@""
                                       font:RO_REGU(15)
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                              textAlignment:NSTextAlignmentLeft];
        
        [self.bgView addSubview:_numLbl];
    }
    return _numLbl;
}

- (UILabel *)openPrcTitleLbl
{
    if (!_openPrcTitleLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"开仓价.") WithFont:PF_MEDI(12)] + 1;
        _openPrcTitleLbl = [IXCustomView createLable:CGRectMake(132,
                                                                GetView_MaxY(self.nameLbl) + 15,
                                                                width,
                                                                12)
                                               title:LocalizedString(@"开仓价.")
                                                font:PF_MEDI(12)
                                          wTextColor:0x99abba
                                          dTextColor:0x8395a4
                                       textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_openPrcTitleLbl];
    }
    
    return _openPrcTitleLbl;
}

- (UILabel *)priceLbl
{
    if (!_priceLbl) {
        _priceLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.openPrcTitleLbl) + 5,
                                                         VIEW_Y(self.openPrcTitleLbl) - 2, 200, 15)
                                        title:@""
                                         font:RO_REGU(15)
                                   wTextColor:0x4c6072
                                   dTextColor:0xffffff
                                textAlignment:NSTextAlignmentLeft];
        
        [self.bgView addSubview:_priceLbl];
    }
    
    return _priceLbl;
}

- (UILabel *)showNowPriceLbl
{
    if (!_showNowPriceLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"现时价")
                                                    WithFont:PF_MEDI(12)] + 1;
        _showNowPriceLbl = [IXCustomView createLable:CGRectMake(132, GetView_MaxY(self.openPrcTitleLbl) + 15,width, 12)
                                               title:LocalizedString(@"现时价")
                                                font:PF_MEDI(12)
                                          wTextColor:0x99abba
                                          dTextColor:0x8395a4
                                       textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_showNowPriceLbl];
    }
    return _showNowPriceLbl;
}

- (UILabel *)nowPriceLbl
{
    if (!_nowPriceLbl) {
        _nowPriceLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.showNowPriceLbl) + 5, VIEW_Y(self.showNowPriceLbl) -2, 200, (15))
                                           title:@"--"
                                            font:RO_REGU(15)
                                      wTextColor:0x4c6072
                                      dTextColor:0xff0000
                                   textAlignment:NSTextAlignmentLeft];
        [self.bgView addSubview:_nowPriceLbl];
    }
    return _nowPriceLbl;
}

- (UILabel *)showResultLbl
{
    if (!_showResultLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"盈亏") WithFont:PF_MEDI(13)] + 1;
        _showResultLbl = [IXCustomView createLable:CGRectMake( VIEW_W(self.bgView) - width - 15, GetView_MaxY(self.stopLbl) + 15,width,12)
                                             title:LocalizedString(@"盈亏")
                                              font:PF_MEDI(12)
                                        wTextColor:0x99abba
                                        dTextColor:0x8395a4
                                     textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_showResultLbl];
    }
    return _showResultLbl;
}

- (UILabel *)resultLbl
{
    if (!_resultLbl) {
        _resultLbl = [IXCustomView createLable:CGRectMake( VIEW_W(self.bgView) - 100,  VIEW_Y(self.nowPriceLbl),85,16)
                                         title:@"--"
                                          font:RO_REGU(15)
                                    wTextColor:0x4c6072
                                    dTextColor:0xff0000
                                 textAlignment:NSTextAlignmentRight];
        [self.bgView addSubview:_resultLbl];
    }
    return _resultLbl;
}


@end
