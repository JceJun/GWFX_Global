//
//  IXDealListInfoVC.h
//  IXApp
//
//  Created by Evn on 17/1/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXDealM.h"

@interface IXDealListInfoVC : IXDataBaseVC

@property (nonatomic, strong) IXDealM *dealModel;

@end
