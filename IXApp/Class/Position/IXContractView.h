//
//  IXContractView.h
//  IXApp
//
//  Created by Evn on 17/1/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXSymbolM.h"

@interface IXContractView : UIView

- (id)initWithFrame:(CGRect)frame
        symbolModel:(IXSymbolM *)model;
@end
