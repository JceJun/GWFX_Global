//
//  IXOrderListInfoVC.h
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXOrderModel.h"

@interface IXOrderListInfoVC : IXDataBaseVC

@property (nonatomic, strong) IXOrderModel *orderModel;

@end
