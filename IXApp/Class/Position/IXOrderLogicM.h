//
//  IXOrderLogicM.h
//  IXApp
//
//  Created by Evn on 2017/6/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXTradeVM.h"
#import "IXUpdateOrderVC.h"

@interface IXOrderLogicM : NSObject

+ (void)updateTakeprofitWithTag:(ROWNAME)tag WithRoot:(IXUpdateOrderVC *)root;
+ (void)updateStoplossWithTag:(ROWNAME)tag WithRoot:(IXUpdateOrderVC *)root;

@end
