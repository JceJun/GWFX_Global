//
//  IXUpdateOrderVC.m
//  IXApp
//
//  Created by Evn on 17/1/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUpdateOrderVC.h"

#import "IXTradeMarginV.h"

#import "IXTradeVM.h"
#import "IXAccountBalanceModel.h"
#import "IXTradeMarketModel.h"
#import "IXOpenModel+CheckData.h"
#import "NSString+FormatterPrice.h"
#import "IXOpenTipV.h"
#import "IXAppUtil.h"
#import "IXLastQuoteM.h"
#import "IXTouchTableV.h"

#import "IXUpdateOrderResultVC.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "IXOpenCellM.h"
#import "IXOrderCalM.h"
#import "IXOrderLogicM.h"
#import "IXOrderQuoteM.h"
#import "UIImageView+SepLine.h"
#import "IXPullDownMenu.h"
#import "IXUserDefaultM.h"

@interface IXUpdateOrderVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV *contentTV;

@property (nonatomic, assign) IXSymbolTradeState tradeState;
@property (nonatomic, strong) IXTradeMarginV *marginView;
@property (nonatomic, strong) IXOpenTipV *tipView;
@property (nonatomic, strong) IXPullDownMenu    *menu;

@property (nonatomic, strong) NSMutableArray *showVolArr;
@property (nonatomic, strong) NSMutableArray *volArr;
@property (nonatomic, strong) NSMutableArray *prcArr;

//YES：数量   NO：手数
@property (nonatomic, assign) BOOL isVolNum;

@end

@implementation IXUpdateOrderVC

- (id)init
{
    self = [super init];
    if ( self ) {
        _isVolNum = ( [IXUserDefaultM unitSetting] == UnitSettingTypeCount );
    }
    return self;
}

- (NSMutableArray *)showVolArr
{
    if ( !_showVolArr ) {
        _showVolArr = [[NSMutableArray array] mutableCopy];
        _volArr = [[NSMutableArray array] mutableCopy];
    }else{
        [_showVolArr removeAllObjects];
        [_volArr removeAllObjects];
    }
    double min = _tradeModel.symbolModel.volumesMin;
    double max = _tradeModel.symbolModel.volumesMax;
    
    if ( [_tradeModel.requestVolume doubleValue] != min ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",min]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew ) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",min] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else{
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",min/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    if ( max > min * 10 && [_tradeModel.requestVolume doubleValue] != (min * 10) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 10)]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 10)] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 10)/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    if ( max > min * 100 && [_tradeModel.requestVolume doubleValue] != (min * 100) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 100)]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew) ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 100)] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 100)/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    
    if ( max > min * 500 && [_tradeModel.requestVolume doubleValue] != (min * 500) ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",(min * 500)]];
        if ( _isVolNum || ( 0 == _tradeModel.symbolModel.contractSizeNew)  ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",(min * 500)] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",(min * 500)/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    if ( [_tradeModel.requestVolume doubleValue] != max ) {
        [_volArr addObject:[NSString stringWithFormat:@"%.lf",max]];
        if ( _isVolNum || 0 == _tradeModel.symbolModel.contractSizeNew ) {
            [_showVolArr addObject:[NSString thousandFormate:[NSString formatterPrice:[NSString stringWithFormat:@"%f",max] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits]];
        }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
            [_showVolArr addObject:[NSString thousandFormate:[NSString stringWithFormat:@"%.2lf",max/_tradeModel.symbolModel.contractSizeNew] withDigits:2]];
        }
    }
    return _showVolArr;
}

- (NSMutableArray *)prcArr
{
    if ( !_prcArr ) {
        _prcArr = [[NSMutableArray array] mutableCopy];
    }
    [_prcArr removeAllObjects];
    
    NSDecimalNumber *max = _openModel.maxRequestPrice;
    NSDecimalNumber *min = _openModel.minRequestPrice;
    
    NSInteger digit = _tradeModel.symbolModel.digits;
    [_prcArr addObject:[NSString formatterPrice:[min stringValue]
                                     WithDigits:digit]];
    
    NSDecimalNumber *step = [[max decimalNumberBySubtracting:min] decimalNumberByDividingBy:
                             [NSDecimalNumber decimalNumberWithString:@"3"]];
    [_prcArr addObject:[NSString formatterPrice:[[min decimalNumberByAdding:step] stringValue]
                                     WithDigits:digit]];
    
    [_prcArr addObject:[NSString formatterPrice:[[min decimalNumberByAdding:
                                                  [step decimalNumberByMultiplyingBy:
                                                   [NSDecimalNumber decimalNumberWithString:@"2"]]] stringValue]
                                     WithDigits:digit]];
    
    [_prcArr addObject:[NSString formatterPrice:[max stringValue]
                                     WithDigits:digit]];
    return _prcArr;
}

- (IXPullDownMenu *)menu
{
    if (!_menu) {
        _menu = [IXPullDownMenu menuWithMenuItems:@[]
                                        rowHeight:30
                                            width:100];
        
        weakself;
        _menu.itemClicked = ^(NSInteger index ,NSString * title) {
            [weakSelf menuItemClicked:index title:title];
        };
    }
    
    return _menu;
}

#pragma mark -
#pragma mark - menu
- (void)showMenu:(UIView *)view WithItem:(NSArray *)arr
{
    [self.view endEditing:YES];
    //如果没有元素就不弹框
    if (arr.count == 0) {
        return;
    }
    CGFloat offsetY = -4;
    if ( self.tradeModel.chooseType == Choose_ExpireType || self.tradeModel.chooseType == Choose_TradeType ) {
        offsetY = 22;
    }
    [self.menu showMenuFrom:view items:arr offsetY:offsetY];
}

- (void)menuItemClicked:(NSInteger)index title:(NSString *)title
{
    switch (_tradeModel.chooseType) {
        case Choose_PriceType:{
            //更新价格
            IXOpenPrcCell *prc = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
            if ( [prc isKindOfClass:[IXOpenPrcCell class]] ) {
                prc.inputTF.text = _prcArr[index];
                [self dealValueWithTag:ROWNAMEPRICE WithValue:prc.inputTF.text];
            }
        }
            break;
        case Choose_VolumeType:{
            //更新手数
            IXOpenPrcCell *vol = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
            if ( [vol isKindOfClass:[IXOpenPrcCell class]] ) {
                vol.inputTF.text = _showVolArr[index];
                [self dealValueWithTag:ROWNAMEVOLUME WithValue:_showVolArr[index]];
            }
        }
            break;
        default:{
            if ( index >= 0 &&  index <= self.tradeModel.tradeExpireArr.count ) {
                self.tradeModel.requestExpire = self.tradeModel.tradeExpireArr[index];
                [self.contentTV reloadData];
            }
        }
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    
    self.fd_interactivePopDisabled = YES;
    [self.navigationItem setHidesBackButton:YES];
    [self initDataSource];
    [self.contentTV reloadData];
    [self.view addSubview:self.marginView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [IXOrderQuoteM subscribeDynamicPrice:self];
    
#if DEBUG
    [IXOrderCalM addPrcTipInfo:self];
#else
    self.title = LocalizedString(@"修改订单");
#endif
}

- (void)resetSub
{
    [IXOrderQuoteM cancelDynamicPrice:self];
    [IXOrderCalM removeTip:self.tipView];
#if DEBUG
    [IXOrderCalM removePrcTipInfo:self];
#endif
}

- (void)rightBtnItemClicked
{
    [self resetSub];
    NSArray *arr = @[@{@"marketId":@(_tradeModel.marketId),
                       @"id":@(_tradeModel.symbolModel.id_p)}];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyCancelDetailQuote object:arr];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)leftBtnItemClicked
{
    [self resetSub];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark dataSouce
- (void)initDataSource
{
    _tradeState = [IXDataProcessTools symbolIsCanTrade:_tradeModel.symbolModel];
    _marginView.sellBtn.userInteractionEnabled = ([IXDataProcessTools isNormalAddOptiosByState:_tradeState]);
    
    self.openModel.quoteModel = _tradeModel.quoteModel;
    self.openModel.symbolModel = _tradeModel.symbolModel;
    self.openModel.orderDir = _tradeModel.direction;
    self.openModel.orderType = _tradeModel.requestType;

    if ( _isVolNum || 0 == _tradeModel.symbolModel.contractSizeNew ) {
        self.tradeModel.showVolume = self.tradeModel.requestVolume;
    }else if( 0 != _tradeModel.symbolModel.contractSizeNew ){
        self.tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:
                                      [NSString stringWithFormat:@"%.2lf",[self.tradeModel.requestVolume doubleValue]/_tradeModel.symbolModel.contractSizeNew]];
    }
    _openModel.showSLCount = 2;//默认展开
}

#pragma mark - UITextField代理
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // 获取到父类cell
    UITableViewCell *cell = (UITableViewCell *) [[textField superview] superview];
    CGFloat offsetY = CGRectGetMaxY(cell.frame);
    CGFloat animationY = CGRectGetHeight(self.contentTV.frame) - offsetY - 217;
    if (  animationY < 0 ) {
        if ( offsetY - CGRectGetHeight(self.contentTV.frame) > 0 ) {
            [self.contentTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_openModel.showSLCount inSection:2]
                                  atScrollPosition:UITableViewScrollPositionNone
                                          animated:NO];
        }
        // 执行动画(移动到输入的位置)
        [self.contentTV setContentOffset:CGPointMake(0, -animationY) animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == ROWNAMEVOLUME) {
        [self resetInputContent:textField];
    }
    [self dealValueWithTag:textField.tag WithValue:textField.text];
    [self.contentTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                          atScrollPosition:UITableViewScrollPositionNone
                                  animated:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag == ROWNAMEPROFIT ||
       textField.tag == ROWNAMELOSS){
        BOOL hidnErase = ([textField.text length] == 1 && [string length] == 0);
        IXOpenSLCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(textField.tag - 3) inSection:2]];
        cell.hiddenErase = hidnErase;
    }
    return YES;
}


- (void)resetInputContent:(UITextField *)textField
{
    NSString *str = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    if (_isVolNum) {
        textField.text  = [NSString thousandFormate:[NSString formatterPrice:str WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits];
    } else {
        textField.text  = [NSString thousandFormate:[NSString formatterPrice:str WithDigits:2] withDigits:2];
    }
}

#pragma mark uitableView delegate && dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section ) {
        case 0:{
            return 2 + _openModel.showDeepPrcCount;
        }
            break;
        case 1:{
            return 3;
        }
            break;
        default:{
            return _openModel.showSLCount;
        }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section ) {
        case 0:{
            return [self sectionFirstWithIndexPath:indexPath];
        }
            break;
        default:{
            return 44;
        }
            break;
    }
}

- (CGFloat)sectionFirstWithIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row == 0 ) {
        return 73;
    }else if ( indexPath.row == _openModel.showDeepPrcCount + 1){
        return 40;
    }else{
        return 22;
    }
}

#pragma 手动管理Cell
- (void)removeSepLine:(UITableViewCell *)cell
{
    UIImageView *sepLine = (UIImageView *)[cell viewWithTag:1000];
    if ( sepLine ) {
        [sepLine removeFromSuperview];
    }
}

- (void)addSepLine:(UITableViewCell *)cell WithFrame:(CGRect)frame
{
    UIImageView *sepLine = [UIImageView addSepImageWithFrame:frame
                                                   WithColor:CellSepLineColor
                                                   WithAlpha:1.f];
    sepLine.tag = 1000;
    [cell.contentView addSubview:sepLine];
}

- (UITableViewCell *)sectionFirst:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row == 0 ) {
        IXDetailSymbolCell *cell = [IXOpenCellM configSymbolDetailCellWithTableView:tableView
                                                                          WithIndex:indexPath];
        cell.tradeState = _tradeState;
        cell.nLastClosePrice = _openModel.nLastClosePrice;
        cell.symbolModel = _tradeModel.symbolModel;
        cell.quoteModel = _tradeModel.quoteModel;
        cell.marketId = _tradeModel.marketId;
        
        return cell;
    }else if ( indexPath.row == _openModel.showDeepPrcCount + 1){
        IXOpenShowPrcCell *cell = [IXOpenCellM configShowPrcCellWithTableView:tableView
                                                                    WithIndex:indexPath];
       
        [cell refreashUIWithArrowDir:( _openModel.showDeepPrcCount == 1)];
        [self addSepLine:cell WithFrame:CGRectMake( 0, 0.5, kScreenWidth, kLineHeight)];
        [self addSepLine:cell WithFrame:CGRectMake( 0, 40 - kLineHeight, kScreenWidth, kLineHeight)];
        
        weakself;
        cell.deepPrcBlock = ^{
            NSInteger count = weakSelf.openModel.showDeepPrcCount;
            count = (count == 1) ? 5 : 1;
            weakSelf.openModel.showDeepPrcCount = count;
            [weakSelf.contentTV reloadData];
            [weakSelf.view endEditing:YES];
        };
        
        return cell;
    }else{
        IXSymbolDeepCell *cell = [IXOpenCellM configDeepPrcCellWithTableView:tableView
                                                                   WithIndex:indexPath];
        NSInteger tag = indexPath.row - 1;
        cell.tag = tag;
        if ( _tradeModel.quoteModel.BuyPrc.count > tag) {
            [cell setBuyPrc:[self formatterPrice:_tradeModel.quoteModel.BuyPrc[tag]]
                     BuyVol:[self formatterPrice:_tradeModel.quoteModel.BuyVol[tag]]
                    SellPrc:[self formatterPrice:_tradeModel.quoteModel.SellPrc[tag]]
                    SellVol:[self formatterPrice:_tradeModel.quoteModel.SellVol[tag]]];
        }
        return cell;
    }
}

- (UITableViewCell *)sectionSecond:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.row ) {
        case 0:{
            IXOpenTypeCell *cell = [IXOpenCellM configOpenTypeCellWithTableView:tableView
                                                                      WithIndex:indexPath];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            
            cell.kind = self.tradeModel.requestType;
            cell.expire = self.tradeModel.requestExpire;
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x242a36);
            
            weakself;
            cell.tradeCon = ^(TRADE_TYPE type,UIView *view) {
                if ( type == Trade_kind ) {
                }else{
                    weakSelf.tradeModel.chooseType = Choose_ExpireType;
                    [weakSelf chooseExpireType:view];
                }
            };
            return cell;
        }
            break;
        case 1:{
            IXOpenPrcCell *cell = [IXOpenCellM configOpenPrcCellWithTableView:tableView
                                                                    WithIndex:indexPath];
            UIButton *stepBtn = (UIButton *)[cell.contentView viewWithTag:DASHBORADBTNTAG];
            [stepBtn dk_setImage:DKImageNames(@"openRoot_stepVol", @"openRoot_stepVol_D")
                                                                   forState:UIControlStateNormal];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            cell.title = LocalizedString(@"价格");
            cell.inputExplain = _openModel.priceRangeStr;
            CGRect frame = cell.inputTF.frame;
            frame.origin.y = 5;
            cell.inputTF.frame = frame;
            
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMEPRICE;
            
            if( _tradeModel.requestPrice ){
                cell.inputContent = [_tradeModel.requestPrice stringValue];
                cell.inputContent = [NSString formatterPrice:[_tradeModel.requestPrice stringValue]
                                                  WithDigits:_tradeModel.symbolModel.digits];
            }
            
            weakself;
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMEPRICE];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMEPRICE];
            };
            
            __block IXOpenPrcCell *blockCell = cell;
            cell.stepBlock = ^{
                weakSelf.tradeModel.chooseType = Choose_PriceType;
                [weakSelf showMenu:stepBtn WithItem:weakSelf.prcArr];
            };
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMEPRICE WithValue:blockCell.inputTF.text];
            };
            
            return cell;
        }
            break;
        default :{
            IXOpenPrcCell *cell = [IXOpenCellM configOpenPrcCellWithTableView:tableView
                                                                    WithIndex:indexPath];
            UIButton *stepBtn = (UIButton *)[cell.contentView viewWithTag:DASHBORADBTNTAG];
            [stepBtn dk_setImage:DKImageNames(@"openRoot_stepPrc", @"openRoot_stepPrc_D")
                        forState:UIControlStateNormal];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            if ( _isVolNum ) {
                cell.title = LocalizedString(@"数量");
            }else{
                cell.title = LocalizedString(@"手数");
            }
            
            if ( _tradeModel.symbolModel.unitLanName && _tradeModel.symbolModel.unitLanName.length) {
                cell.inputExplain = [NSString stringWithFormat:@"(%@)",[IXDataProcessTools showCurrentUnitLanName:_tradeModel.symbolModel.unitLanName]];
            } else {
                CGRect frame = cell.inputTF.frame;
                frame.origin.y = 7;
                cell.inputTF.frame = frame;
                
                cell.inputExplain = @"";
            }
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMEVOLUME;
            
            if( _tradeModel.showVolume && !isnan([_tradeModel.showVolume floatValue]) ){
                if (!_isVolNum && _tradeModel.symbolModel.contractSizeNew) {
                    cell.inputContent = [NSString thousandFormate:[NSString formatterPrice:[_tradeModel.showVolume stringValue] WithDigits:2] withDigits:2];
                }else{
                    cell.inputContent = [NSString thousandFormate:[NSString formatterPrice:[_tradeModel.showVolume stringValue] WithDigits:_tradeModel.symbolModel.volDigits] withDigits:_tradeModel.symbolModel.volDigits];
                }
            }
            
            weakself;
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMEVOLUME];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMEVOLUME];
            };
            
            __block IXOpenPrcCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMEVOLUME WithValue:blockCell.inputTF.text];
            };
            
            cell.stepBlock = ^{
                [weakSelf.contentTV endEditing:YES];
                weakSelf.tradeModel.chooseType = Choose_VolumeType;
                [weakSelf showMenu:stepBtn WithItem:weakSelf.showVolArr];
            };
            
            return cell;
        }
            break;
    }
}

- (UITableViewCell *)sectionThird:(UITableView *)tableView WithIndexPath:(NSIndexPath *)indexPath
{
    weakself;
    switch ( indexPath.row ) {
        case 0:{
            IXOpenSLCell *cell = [IXOpenCellM configOpenSLCellWithTableView:tableView
                                                                  WithIndex:indexPath];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
           cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            cell.title = LocalizedString(@"止盈");
            cell.inputExplain = _openModel.profitRangeStr;
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMEPROFIT;
            if( _tradeModel.takeprofit && !isnan([_tradeModel.takeprofit doubleValue]) && [_tradeModel.takeprofit doubleValue] > 0){
                cell.inputContent = [NSString formatterPrice:[_tradeModel.takeprofit stringValue]
                                                  WithDigits:_tradeModel.symbolModel.digits];
                cell.inputExplain = _openModel.profitRangeStr;
            }else{
                cell.inputContent = @"";
                cell.inputExplain = @"";
            }
            
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMEPROFIT];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMEPROFIT];
            };
            
            cell.eraseBlock = ^{
                [weakSelf updateEraseWithTag:ROWNAMEPROFIT];
            };
            
            __block IXOpenSLCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMEPROFIT WithValue:blockCell.inputTF.text];
            };
            return cell;
        }
            break;
        default:{
            IXOpenSLCell *cell = [IXOpenCellM configOpenSLCellWithTableView:tableView
                                                                  WithIndex:indexPath];
            [self addSepLine:cell WithFrame:CGRectMake( 0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
            cell.contentView.dk_backgroundColorPicker =  DKColorWithRGBs(0xffffff, 0x262f3e);
            cell.title = LocalizedString(@"止损");
            cell.inputExplain = _openModel.stopRangeStr;
            cell.inputTF.delegate = self;
            cell.inputTF.tag = ROWNAMELOSS;
            if(  _tradeModel.stoploss && !isnan([_tradeModel.stoploss doubleValue])&& [_tradeModel.stoploss doubleValue] > 0 ){
                cell.inputContent = [NSString formatterPrice:[_tradeModel.stoploss stringValue]
                                                  WithDigits:_tradeModel.symbolModel.digits];
                
                cell.inputExplain = _openModel.stopRangeStr;
            }else{
                cell.inputContent = @"";
                cell.inputExplain = @"";
            }
            
            cell.addBlock = ^{
                [weakSelf updateTakeprofitWithTag:ROWNAMELOSS];
            };
            
            cell.substractBlock = ^{
                [weakSelf updateStoplossWithTag:ROWNAMELOSS];
            };
            
            
            cell.eraseBlock = ^{
                [weakSelf updateEraseWithTag:ROWNAMELOSS];
            };
            
            __block IXOpenSLCell *blockCell = cell;
            cell.inputTF.editBlock = ^{
                [weakSelf dealValueWithTag:ROWNAMELOSS WithValue:blockCell.inputTF.text];
            };
            
            return cell;
        }
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section ) {
        case 0:{
            return [self sectionFirst:tableView WithIndexPath:indexPath];
        }
            break;
        case 1:{
            return [self sectionSecond:tableView WithIndexPath:indexPath];
        }
        default:{
            return [self sectionThird:tableView WithIndexPath:indexPath];
        }
            break;
    }
}


#pragma mark load view module
- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        CGRect frame = self.view.bounds;
        frame.size.height = kScreenHeight - 68 - kNavbarHeight;
        _contentTV = [[IXTouchTableV alloc] initWithFrame:frame];
        [self.view addSubview:_contentTV];
        _contentTV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _contentTV.showsVerticalScrollIndicator = NO;
        
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_contentTV registerClass:[IXDetailSymbolCell class]
           forCellReuseIdentifier:NSStringFromClass([IXDetailSymbolCell class])];
        
        [_contentTV registerClass:[IXSymbolDeepCell class]
           forCellReuseIdentifier:NSStringFromClass([IXSymbolDeepCell class])];
        
        [_contentTV registerClass:[IXOpenShowPrcCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenShowPrcCell class])];
        
        [_contentTV registerClass:[IXOpenTypeCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenTypeCell class])];
        
        [_contentTV registerClass:[IXOpenDirCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenDirCell class])];
        
        [_contentTV registerClass:[IXOpenPrcCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenPrcCell class])];
        
        [_contentTV registerClass:[IXOpenEptPrcCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenEptPrcCell class])];
        
        [_contentTV registerClass:[IXOpenShowSLCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenShowSLCell class])];
        
        [_contentTV registerClass:[IXOpenSLCell class]
           forCellReuseIdentifier:NSStringFromClass([IXOpenSLCell class])];
        
    }
    return _contentTV;
}

- (IXOpenTipV *)tipView
{
    if ( !_tipView ) {
        _tipView = [[IXOpenTipV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 40)];
    }
    return _tipView;
}

- (IXTradeMarginV *)marginView
{
    if (!_marginView) {
        _marginView = [[IXTradeMarginV alloc] initWithFrame:CGRectMake( 0,
                                                                       kScreenHeight - 68 - kNavbarHeight,
                                                                       kScreenWidth,
                                                                       68)];
        weakself;
        _marginView.addOrder = ^(){
            [weakSelf.view endEditing:YES];
            
            NSString *checkOrderInfo = [IXOrderCalM checkResult:weakSelf];
            if ( [checkOrderInfo length] == 0 ) {
                [IXOrderCalM removeTip:weakSelf.tipView];
                [weakSelf submitOrderInfo];
            }else{
                [IXOrderCalM showTip:checkOrderInfo WithView:weakSelf.tipView InView:weakSelf.view];
            }
        };
    }
    return _marginView;
}

- (IXTradeMarketModel *)tradeModel
{
    if (!_tradeModel) {
        _tradeModel = [[IXTradeMarketModel alloc] init];
    }
    return _tradeModel;
}

- (IXOpenModel *)openModel
{
    if (!_openModel) {
        weakself;
        
        _openModel = [[IXOpenModel alloc] initWithSymbolModel:_tradeModel.symbolModel
                                               WithPriceRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetPriceRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               } WithProfitRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetProfitRangeMinPrc:minPrice MaxPrc:maxPrice];
                                               } WithLossRange:^(NSString *minPrice, NSString *maxPrice) {
                                                   [weakSelf resetStopRangeMinPrc:minPrice MaxPrc:maxPrice];
                                                   
                                               }];
        _openModel.nLastClosePrice = _nLastClosePrice;
    }
    return _openModel;
}

- (void)chooseExpireType:(UIView *)view
{
    self.tradeModel.chooseType = Choose_ExpireType;
    [self showMenu:view WithItem:self.tradeModel.tradeExpireArr];
}

- (void)updatePriceRange
{
    [self.openModel caculateRangePrice];
    [self.openModel caculateRangeStopLoss];
    [self.openModel caculateRangeTakeProfit];
}

- (void)updateDirection:(item_order_edirection)direction
{
    self.tradeModel.direction = direction;
    self.openModel.orderDir = direction;
    
    //根据方向刷新止盈止损，保证金和佣金
    [self resetEptPrice];
    [self updatePriceRange];
    [self resetMarginAssetWithVolume:[_tradeModel.requestVolume doubleValue]];
}

#pragma mark 点击按钮更新数据
- (void)updateStoplossWithTag:(ROWNAME)tag
{
    [IXOrderLogicM updateStoplossWithTag:tag WithRoot:self];
    if ( tag == ROWNAMEPRICE ) {
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }else if ( tag == ROWNAMEVOLUME ){
        [self updateShowVol];
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }
    
    [IXOrderCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)updateEraseWithTag:(ROWNAME)tag
{
    if(tag == ROWNAMEPROFIT){
        _tradeModel.takeprofit = nil;
    }else if (tag == ROWNAMELOSS){
        _tradeModel.stoploss = nil;
    }
    [IXOrderCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)updateShowVol
{
    if ( _isVolNum || 0 == _tradeModel.symbolModel.contractSizeNew ) {
        self.tradeModel.showVolume = [NSDecimalNumber decimalNumberWithDecimal:
                                      [self.tradeModel.requestVolume decimalValue]];
    }else{
        self.tradeModel.showVolume = [self.tradeModel.requestVolume decimalNumberByDividingBy:
                                      [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",
                                                                                self.tradeModel.symbolModel.contractSizeNew]]
                                                                                 withBehavior:[IXRateCaculate handlerPriceWithDigit:2]];
    }
}

- (void)updateTakeprofitWithTag:(ROWNAME)tag
{
    [IXOrderLogicM updateTakeprofitWithTag:tag WithRoot:self];
    if ( tag == ROWNAMEPRICE ) {
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }else if ( tag == ROWNAMEVOLUME ){
        [self updateShowVol];
        [self resetMarginAssetWithVolume:[self.tradeModel.requestVolume doubleValue]];
    }
    
    [IXOrderCalM updateTipInfoWithView:self.tipView WithRoot:self];
    [self reloadContentWithTag:tag];
}

- (void)reloadContentWithTag:(ROWNAME)tag
{
    NSIndexPath *indexPath;
    switch ( tag) {
        case ROWNAMEPRICE:{
            indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
        }
            break;
        case ROWNAMEVOLUME:{
            indexPath = [NSIndexPath indexPathForRow:2 inSection:1];
        }
            break;
        case ROWNAMEPROFIT:{
            indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        }
            break;
        default:{
            indexPath = [NSIndexPath indexPathForRow:1 inSection:2];
        }
            break;
    }
    
    [self.contentTV reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)dealValueWithTag:(ROWNAME)tag WithValue:(NSString *)value
{
    switch (tag) {
        case ROWNAMEPRICE:{     //价格
            _tradeModel.requestPrice = [NSDecimalNumber decimalNumberWithString:value];
            _openModel.dealPrice = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        case ROWNAMEVOLUME:{    //手数
            value = [value stringByReplacingOccurrencesOfString:@"," withString:@""];
            if ( _isVolNum || 0 == _tradeModel.symbolModel.contractSizeNew) {
                _tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:_tradeModel.symbolModel.volDigits]];
                _tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:_tradeModel.symbolModel.volDigits]];
            }else{
                _tradeModel.showVolume = [NSDecimalNumber decimalNumberWithString:[NSString formatterPrice:value WithDigits:2]];
                _tradeModel.requestVolume = [NSDecimalNumber decimalNumberWithString:
                                             [NSString stringWithFormat:@"%.2lf",[value doubleValue] * _tradeModel.symbolModel.contractSizeNew]];
            }
            [self resetMarginAssetWithVolume:[_tradeModel.requestVolume doubleValue]];
        }
            break;
        case ROWNAMEPROFIT:{    //止赢
            _tradeModel.takeprofit = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        case ROWNAMELOSS:{      //止损
            _tradeModel.stoploss = [NSDecimalNumber decimalNumberWithString:value];
        }
            break;
        default:
            break;
    }
    
    [IXOrderCalM updateTipInfoWithView:self.tipView WithRoot:self];
}

- (void)submitOrderInfo
{
    [self resetSub];
    IXUpdateOrderResultVC *result = [[IXUpdateOrderResultVC alloc] init];
    result.model = _tradeModel;
    [self.navigationController pushViewController:result animated:YES];
    
    
    proto_order_update *proto = [_tradeModel packageUpdateOrder];
    [[IXTCPRequest shareInstance] updateOrderWithParam:proto];
}


- (NSString *)formatterPrice:(NSString *)price
{
    return [NSString formatterPrice:price
                         WithDigits:self.tradeModel.symbolModel.digits];
}

#pragma mark 用户操作触发的UI刷新
- (void)resetPriceRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    self.openModel.priceRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    
    UITableViewCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
    if( [cell isKindOfClass:[IXOpenPrcCell class]] ){
        if ( self.openModel.profitRangeStr ) {
            [(IXOpenPrcCell *)cell setInputExplain:self.openModel.priceRangeStr];
        }
    }
}

- (void)resetProfitRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    self.openModel.profitRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    
    if ( _openModel.showSLCount > 0 ) {
        IXOpenSLCell *cell = [_contentTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        if( _tradeModel.takeprofit && !isnan([_tradeModel.takeprofit doubleValue]) && [_tradeModel.takeprofit doubleValue] > 0 ){
            cell.inputExplain = _openModel.profitRangeStr;
        }else{
            cell.inputExplain = @"";
        }
    }
}

- (void)resetStopRangeMinPrc:(NSString *)minPrice MaxPrc:(NSString *)maxPrice
{
    _openModel.stopRangeStr = [NSString stringWithFormat:@"(%@ - %@)",minPrice,maxPrice];
    
    if ( _openModel.showSLCount > 0 ) {
        IXOpenPrcCell *cell = [_contentTV cellForRowAtIndexPath:
                               [NSIndexPath indexPathForRow:1 inSection:2]];
        if(  _tradeModel.stoploss && !isnan([_tradeModel.stoploss doubleValue]) && [_tradeModel.stoploss doubleValue] > 0 ){
            cell.inputExplain = _openModel.stopRangeStr;
        }else{
            cell.inputExplain = @"";
        }
    }
}

#pragma mark -
#pragma mark - 行情数据
- (void)needRefresh
{
    NSMutableArray *arr = [IXOrderQuoteM subscribeDynamicPrice:self];
    if ( arr ) {
        [self didResponseQuoteDistribute:arr cmd:CMD_QUOTE_PUB_DETAIL];
    }
}

- (void)didResponseQuoteDistribute:(NSMutableArray *)arr cmd:(uint16)cmd
{
    if( cmd == CMD_QUOTE_PUB_DETAIL || cmd == CMD_QUOTE_SUB_DETAIL ){
        for ( IXQuoteM *model in arr ) {
            if (model.symbolId == _tradeModel.symbolModel.id_p) {
                _tradeModel.quoteModel = [model mutableCopy];
                _openModel.quoteModel = [model mutableCopy];
                if ( Choose_PriceType == _tradeModel.chooseType  ) {
                    self.menu.menuItems = self.prcArr;
                }
                
                [self resetQuote];
                [self resetEptPrice];
                [self resetMarginAssetWithVolume:[_tradeModel.requestVolume doubleValue]];
                break;
            }
        }
    }
}

//刷新UI显示行情的cell
- (void)resetQuote
{
    for ( UITableViewCell *cell  in [_contentTV visibleCells] ) {
        if ( [cell isKindOfClass:[IXDetailSymbolCell class]] ) {
            [(IXDetailSymbolCell *)cell setQuoteModel:_tradeModel.quoteModel];
        }else if ( [cell isKindOfClass:[IXSymbolDeepCell class]] ){
            NSInteger tag = cell.tag;
            [(IXSymbolDeepCell *)cell setBuyPrc:[self formatterPrice:_tradeModel.quoteModel.BuyPrc[tag]]
                                         BuyVol:[self formatterPrice:_tradeModel.quoteModel.BuyVol[tag]]
                                        SellPrc:[self formatterPrice:_tradeModel.quoteModel.SellPrc[tag]]
                                        SellVol:[self formatterPrice:_tradeModel.quoteModel.SellVol[tag]]];
        }
    }
}

//刷新价格范围
- (void)resetEptPrice
{
    [self resetRequestPrice];
    [IXOrderCalM updateTipInfoWithView:self.tipView WithRoot:self];
}

//刷新保证金和佣金,百万分之一
- (void)resetMarginAssetWithVolume:(double)volume
{
    NSArray *arr = [IXOrderCalM resetMarginAssetWithVolume:volume WithRoot:self];
    if ( arr ) {
        _tradeModel.margin = [arr[0] doubleValue];
        _tradeModel.commission = [arr[1] doubleValue];
        
        [self.marginView setMarginValue:[IXDataProcessTools moneyFormatterComma:[arr[0] doubleValue] positiveNumberSign:YES]];
        [self.marginView setCommissionValue:[IXDataProcessTools moneyFormatterComma:[arr[1] doubleValue] positiveNumberSign:YES]];        
    }
}

//设置计算止盈止损范围的价格
//市价单：止盈止损的参考价格和顶层价有关
//限价单：止盈止损的参考价格和用户输入的价格有关
- (void)resetRequestPrice
{
    _openModel.requestPrice = _tradeModel.requestPrice;
}

@end
