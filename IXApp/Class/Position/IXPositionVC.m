//
//  IXPositionVC.m
//  IXApp
//
//  Created by Magee on 16/11/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXPositionVC.h"

#import "IXMultiCloseV.h"
#import "IXPositionCell.h"
#import "IXPositionInfoVC.h"
#import "IXDealListInfoVC.h"
#import "IXPosDefultView.h"
#import "IXPositionResultVC.h"
#import "IXHedgePosVC.h"
#import "IXPositionFilterV.h"
#import "IXPositionSortV.h"
#import "IXPositonTopV.h"
#import "IXPositionHeaderV.h"
#import "IXPositionTipV.h"

#import "IXDBAccountGroupMgr.h"
#import "IXAccountBalanceModel.h"
#import "IXAccountGroupModel.h"
#import "IXQuoteDistribute.h"
#import "IXPositionSymModel.h"
#import "IxProtoCompany.pbobjc.h"
#import "IxProtoAccountGroup.pbobjc.h"
#import "IXUserInfoMgr.h"
#import "IxProtoOrder.pbobjc.h"
#import "IxProtoHeader.pbobjc.h"
#import "IXDBCompanyMgr.h"
#import "IXCpyConfig.h"
#import "IxItemCompany.pbobjc.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"
#import "IXNewGuideV.h"

@interface IXPositionVC ()
<
UITableViewDataSource,
UITableViewDelegate,
IXNewGuideVDelegate
>

@property (nonatomic, strong) UITableView       * tableV;
@property (nonatomic, strong) IXPosDefultView   * posDefView;
@property (nonatomic, assign) BOOL endScroll;

@property (nonatomic, strong) NSMutableArray    *positionAry;
@property (nonatomic, strong) NSMutableArray    * dataArr;//持仓model
@property (nonatomic, strong) NSDictionary      * accGroupDic;
@property (nonatomic, strong) NSMutableDictionary   * lastCloseDic;
@property (nonatomic, strong) NSMutableArray    *showArr;

@property (nonatomic, assign) item_company_emode    companyMode;

//positionId对应的持仓是否可以对冲
@property (nonatomic, strong) NSMutableDictionary   * markCloseByDic;
//symbolId对应的持仓方向列表
@property (nonatomic, strong) NSMutableDictionary   * symDirDic;
//positionId映射的持仓是否可以交易
@property (nonatomic, strong) NSMutableDictionary   * tradeStateDic;


@property (nonatomic, strong) IXNewGuideV   * newGuideV;
@property (nonatomic, strong) IXMultiCloseV * closeV;
@property (nonatomic, strong) IXPositionFilterV     * filterV;
@property (nonatomic, strong) IXPositionSortV       * sortV;
@property (nonatomic, strong) IXPositonTopV         * topV;
@property (nonatomic, strong) IXPositionHeaderV     * headerV;
@property (nonatomic, strong) IXPositionTipV        * tipV;
@property (nonatomic, assign) ChooseType    chooseType;

@property (nonatomic, strong) NSMutableArray    * closePosArr;  //批量平仓仓位元素数组
@property (nonatomic, assign) PositionSortType  sortOp;


@property (nonatomic, assign) BOOL needResNoti;


@end

@implementation IXPositionVC

- (id)init
{
    self = [super init];
    if(self){
        _endScroll = YES;
        
        IXTradeData_listen_regist(self, PB_CMD_COMPANY_UPDATE);
        
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_CHANGE);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_UPDATE);
        
        IXTradeData_listen_regist(self, PB_CMD_POSITION_LIST);
        IXTradeData_listen_regist(self, PB_CMD_POSITION_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_POSITION_ADD);
        
        IXTradeData_listen_regist(self, PB_CMD_ORDER_ADD_BATCH);
        
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
        
        IXTradeData_listen_regist(self, PB_CMD_USERLOGIN_INFO);
        IXTradeData_listen_regist(self, PB_CMD_USER_LOGIN_DATA_TOTAL);

        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(refreashPositionQuote:)
                                                     name:kRefreashProfit
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(refreshPositionUnit)
                                                     name:kNotificationUnit
                                                   object:nil];
    }
    return self;
}

- (void)refreshPositionUnit
{
    [self.tableV reloadData];
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_COMPANY_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_CHANGE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_UPDATE);
    
    IXTradeData_listen_resign(self, PB_CMD_POSITION_LIST);
    IXTradeData_listen_resign(self, PB_CMD_POSITION_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_POSITION_ADD);
    
    IXTradeData_listen_resign(self, PB_CMD_ORDER_ADD_BATCH);
    
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
    
    IXTradeData_listen_resign(self, PB_CMD_USERLOGIN_INFO);
    IXTradeData_listen_resign(self, PB_CMD_USER_LOGIN_DATA_TOTAL);

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa,0x242a36);
    
    _companyMode = -1;
    [self queryCompanyMode];
    
    [self.view addSubview:self.topV];
    [self.view addSubview:self.tipV];
    [self.view addSubview:self.tableV];
    
    [self refreshData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    if (!_tableV.tableFooterView) {
        _tableV.frame = CGRectMake(0, 44, kScreenWidth,
                                   kScreenHeight - kTabbarHeight - kNavbarHeight - 44);
    }
    [_tableV reloadData];
    if (NewGuideEnable) {
        [self loadNewGuideV];
    }
    [self dealWithPositionFilterType:_filterV.dirOp selectSymbol:_filterV.productArr];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self subscribeDynamicPrice];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self endEditPosition];
}

#pragma mark - 新手指引
- (void)loadNewGuideV
{
    if (![IXUserDefaultM getNewGuide:IXNewGuidePosition] && [IXTradeDataCache shareInstance].pb_cache_position_list.count) {
        [self.view addSubview:self.newGuideV];
        [self.newGuideV showNewGuideType:IXNewGuidePosition];
    }
}

- (IXNewGuideV *)newGuideV
{
    if ( !_newGuideV ) {
        _newGuideV = [[IXNewGuideV alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, kScreenHeight)];
        _newGuideV.delegate = self;
    }
    return _newGuideV;
}

- (void)nextRemoveViewType:(IXNewGuide)type
{
    switch (type) {
        case IXNewGuidePosition:{
            [IXUserDefaultM saveNewGuide:type];
            [IXUserDefaultM saveNewGuide:IXNewGuidePosition];
        }
            break;
        default:
            break;
    }
}

- (void)subscribeDynamicPrice
{
    if ( [IXAccountBalanceModel shareInstance].dynQuoteArr ) {
        [[IXTCPRequest shareInstance] subscribeDynPriceWithSymbolAry:
         [IXAccountBalanceModel shareInstance].dynQuoteArr];
    }
}

- (void)loadCacheQuote{
    NSMutableArray *_quote = [NSMutableArray array];
    for ( NSDictionary *dic in [IXAccountBalanceModel shareInstance].dynQuoteArr ) {
        IXQuoteM *model = [IXDataProcessTools queryQuoteDataBySymbolId:[dic[kID] integerValue]];
        if ( model ) {
            [_quote addObject:model];
        }
    }
    [[IXAccountBalanceModel shareInstance] setQuoteDataArr:_quote];
}

#pragma mark -
#pragma mark - private methods
- (void)resetLabel:(UILabel *)label WithContent:(NSString *)str
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSRange range = [str rangeOfString:@"."];
    
    [attrStr addAttribute:NSFontAttributeName
                    value:RO_REGU(18)
                    range:NSMakeRange(0, range.location)];
    
    [attrStr addAttribute:NSFontAttributeName
                    value:RO_REGU(12)
                    range:NSMakeRange(range.location + 1, str.length - range.location - 1)];
    label.attributedText = attrStr;
}

- (void)refreshPositionHeadData
{
    [IXDataProcessTools resetTextColorLabel:self.headerV.profitLab value:[IXAccountBalanceModel shareInstance].totalProfit];
    [self resetLabel:self.headerV.profitLab WithContent:[IXDataProcessTools moneyFormatterComma:[IXAccountBalanceModel shareInstance].totalProfit positiveNumberSign:NO]];
    [self resetLabel:self.headerV.amountLab WithContent:[IXDataProcessTools moneyFormatterComma:[[IXAccountBalanceModel shareInstance] getAvailabelFunds] positiveNumberSign:YES]];
    [self resetFontWithLabel:self.headerV.amountLab WithContent:self.headerV.amountLab.text WithFont:self.headerV.amountLab.font];
    
    if ( [IXAccountBalanceModel shareInstance].totalMargin != 0 ) {
        NSDecimalNumber *total = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2lf", [[IXAccountBalanceModel shareInstance] getNet]]];
        NSDecimalNumber *margin = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2lf", [IXAccountBalanceModel shareInstance].totalMargin]];
        
        if ( [margin doubleValue] != 0 ) {
            double marginRate = [[[total decimalNumberByDividingBy:margin] decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithString:@"100"]] doubleValue];
            self.headerV.marginRateLab.text = [NSString stringWithFormat:@"%.2f%%",marginRate];
        }else{
            self.headerV.marginRateLab.text = @"0.00%";
        }
    }else{
        self.headerV.marginRateLab.text = @"0.00%";
    }
    
    [self refreshMarginView];
}

- (void)refreshMarginView
{
    double marginRate = [[IXAccountBalanceModel shareInstance] marginRate];
    double small = 0,middle = 0;
    
    if ( self.accGroupDic && self.accGroupDic.count > 0) {
        small = [self.accGroupDic[kStopOutLevel] doubleValue];
        middle = [self.accGroupDic[kMarginCallLevel] doubleValue];
    }
    
    float widthRate = marginRate;
    marginRate = marginRate*100;
    widthRate = MIN(1, widthRate);

    if ( [IXAccountBalanceModel shareInstance].totalMargin != 0 ) {
        NSDecimalNumber *total = [NSDecimalNumber decimalNumberWithString:
                                  [NSString stringWithFormat:@"%.2lf", [[IXAccountBalanceModel shareInstance] getNet]]];
        NSDecimalNumber *margin = [NSDecimalNumber decimalNumberWithString:
                                   [NSString stringWithFormat:@"%.2lf", [IXAccountBalanceModel shareInstance].totalMargin]];
        
        if ( [margin doubleValue] != 0 ) {
            double marginRate = [[[total decimalNumberByDividingBy:margin] decimalNumberByMultiplyingBy:
                                  [NSDecimalNumber decimalNumberWithString:@"100"]] doubleValue];
            self.headerV.marginRateLab.text = [NSString stringWithFormat:@"%.2f%%",marginRate];
        }else{
            self.headerV.marginRateLab.text = @"0.00%";
        }
    }else{
        self.headerV.marginRateLab.text = @"0.00%";
    }
    
    if (marginRate == 0) {
        self.headerV.marginLab.text = LocalizedString(@"保证金比例");
        [self.headerV resetMarginRateWith:0.f dkColor:DKColorWithRGBs(0xe2e9f1, 0x4c6072)];
    } else if ((marginRate > 0 && marginRate < small)) {
        self.headerV.marginLab.text = LocalizedString(@"保证金比例");
        self.headerV.marginStateLab.text = LocalizedString(@"高风险");
        self.headerV.marginStateLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        [self.headerV resetMarginRateWith:widthRate dkColor:DKColorWithRGBs(0xff4653, 0xff4653)];
    } else if (small < marginRate && marginRate < middle ) {
        self.headerV.marginLab.text = LocalizedString(@"保证金比例");
        self.headerV.marginStateLab.text = LocalizedString(@"低风险");
        self.headerV.marginStateLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        [self.headerV resetMarginRateWith:widthRate dkColor: DKColorWithRGBs(0xEBAD62, 0xEBAD62)];
    } else {
        self.headerV.marginLab.text = LocalizedString(@"保证金比例");
        self.headerV.marginStateLab.text = LocalizedString(@"正常");
        self.headerV.marginStateLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        [self.headerV resetMarginRateWith:widthRate dkColor:DKColorWithRGBs(0x11b873, 0x50a1e5)];
    }
}

- (void)refreshData
{
    //首次加载(读取自由资金和盈亏)
    [self refreshPositionHeadData];
    
    _positionAry = [IXTradeDataCache shareInstance].pb_cache_position_list;
    _sortOp = PositionSortTypeDefault;
    if ( !_dataArr ) {
        _dataArr = [[NSMutableArray alloc] init];
    } else {
        [_dataArr removeAllObjects];
    }
    
    if (_positionAry.count == 0) {
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        self.posDefView.imageName = @"newList_noneMsg_avatar";
        self.posDefView.tipMsg = LocalizedString(@"暂无持仓");
        self.tableV.scrollEnabled = NO;
        [self.tableV reloadData];
        return;
    }else{
        [self.posDefView removeFromSuperview];
        self.tableV.scrollEnabled = YES;
        
        [self setPositioData];
        [self loadCacheQuote];
    }
}

- (void)setPositioData
{
    for (int i = 0; i < _positionAry.count; i++) {
        IXPositionM *model = [[IXPositionM alloc] init];
        item_position *position = [[item_position alloc] init];
        position = _positionAry[i];
        model.position = position;
        
        IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:position.symbolid];
        if (symModel) {
            model.symbolModel = symModel;
            [_dataArr addObject:model];
            [self markCloseByPos:position WithIndex:i];
        }
    }
    [self dealWithPositionByCondition];
    [self.tableV reloadData];
}

- (void)removeCloseByPos:(item_position *)position WithIndex:(NSInteger)index
{
    NSString *key = [NSString stringWithFormat:@"%llu",position.symbolid];
    NSMutableDictionary *value = [[self.symDirDic objectForKey:key] mutableCopy];
    
    if ( value ) {
        NSInteger sellNum = [[value objectForKey:@"sellNum"] integerValue];
        NSInteger buyNum = [[value objectForKey:@"buyNum"] integerValue];
        
        if ( position.direction == item_position_edirection_DirectionBuy ) {
            buyNum--;
            [value setValue:@(buyNum) forKey:@"buyNum"];
        }else{
            sellNum--;
            [value setValue:@(sellNum) forKey:@"sellNum"];
        }
        
        if ( !buyNum || !sellNum ) {
            NSInteger removeIndex = 0;
            NSMutableArray *list = [[value objectForKey:@"list"] mutableCopy];
            for ( NSInteger count = 0; count < list.count; count++ ) {
                NSMutableDictionary *rDic = [list[count] mutableCopy];
                if ( [rDic integerForKey:@"posId"] == position.id_p ) {
                    removeIndex = count;
                    [self.markCloseByDic removeObjectForKey:[IXEntityFormatter integerToString:
                                                             [[rDic objectForKey:@"posId"] integerValue]]];
                }else{
                    [self.markCloseByDic setValue:@(NO)
                                           forKey:[IXEntityFormatter integerToString:
                                                   [[rDic objectForKey:@"posId"] integerValue]]];
                }
            }
            
            if( removeIndex >= 0 ){
                [list removeObjectAtIndex:removeIndex];
                [value setValue:list forKey:@"list"];
                [self.symDirDic setValue:value forKey:key];
            }
        }else{
            NSMutableArray *list = [[value objectForKey:@"list"] mutableCopy];
            for ( NSInteger count = 0; count < list.count; count++ ) {
                NSMutableDictionary *rDic = [list[count] mutableCopy];
                if ( [rDic integerForKey:@"posId"] == position.id_p ) {
                    [list removeObject:rDic];
                    [self.markCloseByDic removeObjectForKey:[IXEntityFormatter integerToString:
                                                             [[rDic objectForKey:@"posId"] integerValue]]];
                    break;
                }
            }
            [value setValue:list forKey:@"list"];
            
            if( buyNum || sellNum ){
                [self.symDirDic setValue:value forKey:key];
            }else{
                [self.symDirDic removeObjectForKey:key];
            }
        }
    }
}

- (void)markCloseByPos:(item_position *)position WithIndex:(NSInteger)index
{
    NSString *key = [NSString stringWithFormat:@"%llu",position.symbolid];
    NSMutableDictionary *value = [[self.symDirDic objectForKey:key] mutableCopy];
    if ( value ) {
        //买入和卖出均不为0
        NSInteger sellNum = [[value objectForKey:@"sellNum"] integerValue];
        NSInteger buyNum = [[value objectForKey:@"buyNum"] integerValue];
        
        if ( sellNum && buyNum ) {
            NSMutableArray *list = [[value objectForKey:@"list"] mutableCopy];
            [list addObject:@{
                              @"posId":@(position.id_p),
                              @"dir":@(position.direction),
                              }];
            [value setValue:list forKey:@"list"];
            if ( position.direction == item_position_edirection_DirectionBuy ) {
                buyNum++;
                [value setValue:@(buyNum) forKey:@"buyNum"];
            }else{
                sellNum++;
                [value setValue:@(sellNum) forKey:@"sellNum"];
            }
            
            [self.symDirDic setValue:value forKey:key];
            [self.markCloseByDic setValue:@(YES)
                                   forKey:[IXEntityFormatter integerToString:position.id_p]];
            return;
        }
        
        BOOL closeBy = NO;
        if ( position.direction == item_position_edirection_DirectionBuy ) {
            closeBy = (sellNum > 0);
            buyNum++;
            [value setValue:@(buyNum) forKey:@"buyNum"];
        }else{
            closeBy = (buyNum > 0);
            sellNum++;
            [value setValue:@(sellNum) forKey:@"sellNum"];
        }
        
        if ( closeBy ) {
            NSMutableArray *list = [[value objectForKey:@"list"] mutableCopy];
            [list addObject:@{
                              @"posId":@(position.id_p),
                              @"dir":@(position.direction),
                              }];
            
            for ( NSInteger count = 0; count < list.count; count++ ) {
                NSMutableDictionary *rDic = [list[count] mutableCopy];
                [self.markCloseByDic setValue:@(YES)
                                       forKey:[IXEntityFormatter integerToString:
                                               [[rDic objectForKey:@"posId"] integerValue]]];
            }
            
            [value setValue:list forKey:@"list"];
            [self.symDirDic setValue:value forKey:key];
        }else{
            NSMutableArray *list ;
            NSArray *arr = [value objectForKey:@"list"];
            if ( arr ) {
                list = [arr mutableCopy];
            }else{
                list = [[NSArray array] mutableCopy];
            }
            
            [list addObject:@{
                              @"posId":@(position.id_p),
                              @"dir":@(position.direction),
                              }];
            [value setValue:list forKey:@"list"];
            
            [self.symDirDic setValue:value forKey:key];
            [self.markCloseByDic setValue:@(NO)
                                   forKey:[IXEntityFormatter integerToString:position.id_p]];
            
        }
        
    }else{
        NSInteger sellNum = 0;
        NSInteger buyNum = 0;
        if ( position.direction == item_position_edirection_DirectionSell ) {
            sellNum = 1;
        }else{
            buyNum = 1;
        }
        
        [self.symDirDic setValue: @{
                                    @"sellNum":@(sellNum),
                                    @"buyNum":@(buyNum),
                                    @"list":@[
                                            @{
                                                @"posId":@(position.id_p),
                                                @"dir":@(position.direction),
                                                }
                                            ]
                                    }
                          forKey:key];
        [self.markCloseByDic setValue:@(NO) forKey:[IXEntityFormatter integerToString:position.id_p]];
    }
}

#pragma mark 刷新list列表
- (void)refreshListData:(NSArray *)listArr
{
    if (!listArr || listArr.count == 0) {
        return;
    }
    if ( !_dataArr ) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    BOOL flag = NO;
    
    for (int i = 0; i < listArr.count; i++) {
        IXPositionM *model = [[IXPositionM alloc] init];
        item_position *position = listArr[i];
        model.position = position;
        
        for (IXPositionM *tmpModel in _dataArr) {
            if (position.id_p == tmpModel.position.id_p) {
                flag = YES;
                break;
            }
        }
        if (flag) {
            continue;
        }
        IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:position.symbolid];
        if (symModel) {
            model.symbolModel = symModel;
            [_dataArr insertObject:model atIndex:0];
        }
    }
    
    [self reloadTableViewData];
}

#pragma mark 刷新add列表
- (void)refreshAddData
{
    _positionAry = [IXTradeDataCache shareInstance].pb_cache_position_list;
    if (!_positionAry || _positionAry.count == 0) {
        return;
    }
    if ( !_dataArr ) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    IXPositionM *model = [[IXPositionM alloc] init];
    item_position *position = _positionAry[0];
    model.position = position;
    
    IXSymModel *symModel = [[IXPositionSymModel shareInstance] getCacheSymbolBySymbolId:position.symbolid];
    if (symModel) {
        model.symbolModel = symModel;
        [_dataArr insertObject:model atIndex:0];
        [self markCloseByPos:position WithIndex:0];
        [self reloadTableViewData];
    }
}

#pragma mark 刷新update列表
- (void)refreshUpdateData:(proto_position_update *)pb
{
    if ( !_dataArr || _dataArr.count == 0) {
        return;
    }
    NSInteger index = -1;
    IXPositionM *model;
    for (int i = 0; i < _dataArr.count; i++) {
        model = _dataArr[i];
        if (model.position.id_p == pb.position.id_p) {
            index = i;
            break;
        }
    }
    
    if (index == -1) {
        return;
    }
    
    if (pb.position.status == item_position_estatus_StatusClosed) {//全平
        [_dataArr removeObjectAtIndex:index];
        [self removeCloseByPos:model.position WithIndex:index];
        [self reloadTableViewData];
    } else if (pb.position.status == item_position_estatus_StatusOpened) {//部分
        model.position = pb.position;
        [_dataArr replaceObjectAtIndex:index withObject:model];
        [self reloadTableViewData];
    }
}

- (void)dealWithPositionByCondition
{
    if (!_filterV) {
        _showArr = [_dataArr mutableCopy];
        return;
    }
    NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
    if (_filterV.productArr.count) {
        [_filterV.productArr enumerateObjectsUsingBlock:^(NSDictionary  *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            uint64_t symbolId = [obj[kSymbolId] integerValue];
            [_dataArr enumerateObjectsUsingBlock:^(IXPositionM *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (symbolId == obj.position.symbolid) {
                    [tmpArr addObject:obj];
                }
            }];
        }];
    } else {
        tmpArr = [_dataArr mutableCopy];
    }
    if (_showArr) {
        [_showArr removeAllObjects];
    } else {
        _showArr = [NSMutableArray new];
    }
    switch (_filterV.dirOp) {
        case PositionFilterTypebuy:{
            [tmpArr enumerateObjectsUsingBlock:^(IXPositionM  *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.position.direction == item_position_edirection_DirectionBuy) {
                    [_showArr addObject:obj];
                }
            }];
        }
            break;
        case PositionFilterTypeSell:{
            [tmpArr enumerateObjectsUsingBlock:^(IXPositionM  *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.position.direction == item_position_edirection_DirectionSell) {
                    [_showArr addObject:obj];
                }
            }];
        }
            break;
        case PositionFilterTypeAll:{
            _showArr = tmpArr;
        }
            break;
        default:
            break;
    }
    NSSortDescriptor *sortDesc = nil;
    switch (_sortOp) {
        case PositionSortTypeDefault:{
            sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"position.openTime"
                                                     ascending:NO];
        }
            break;
        case PositionSortTypePriceLH:{
            sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"position.openPrice"
                                                     ascending:YES];
        }
            break;
        case PositionSortTypePriceHL:{
            sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"position.openPrice"
                                                     ascending:NO];
        }
            break;
        default:
            break;
    }
    _showArr = [[_showArr sortedArrayUsingDescriptors:@[sortDesc]] mutableCopy];
}

#pragma mark UI刷新列表
- (void)reloadTableViewData
{
    [self dealWithPositionByCondition];
    if (_showArr.count == 0) {
        [self.posDefView removeFromSuperview];
        [self.tableV addSubview:self.posDefView];
        self.posDefView.imageName = @"newList_noneMsg_avatar";
        self.posDefView.tipMsg = LocalizedString(@"暂无持仓");
        self.tableV.scrollEnabled = NO;
        [self.tableV reloadData];
    }else{
        [self.posDefView removeFromSuperview];
        self.tableV.scrollEnabled = YES;
        [self.tableV reloadData];
    }
}

- (IXSymbolTradeState)checkSymbolTradeWithSym:(IXSymModel *)model
{
    NSString *key = [IXEntityFormatter integerToString:model.id_p];
    
    NSNumber *result = [self.tradeStateDic objectForKey:key];
    if ( result ) {
        return [result integerValue];
    }else{
        IXSymbolTradeState state = [IXDataProcessTools symbolModelIsCanTrade:model];
        [self.tradeStateDic setValue:@(state) forKey:key];
        return state;
    }
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _showArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 109;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"IXPositionCell";
    IXPositionCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXPositionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.tag = indexPath.row;
    
    cell.quickB = [self cellQuickBlock];
    cell.hedgeB = [self cellHedgeBlock];
    cell.editB = [self cellEditBlock];
    cell.tapB = [self cellTapBlock];
    
    IXPositionM *tmpModel = _showArr[indexPath.row];
    cell.model = tmpModel;
    cell.tradeState = [self checkSymbolTradeWithSym:tmpModel.symbolModel];
    
    NSNumber *edge = [self.markCloseByDic objectForKey:
                      [IXEntityFormatter integerToString:tmpModel.position.id_p]];
    if ([edge boolValue] && _companyMode == item_company_emode_ModeCloseby) {
        cell.hasHedge = YES;
    }else{
        cell.hasHedge = NO;
        if ( _companyMode != item_company_emode_ModeCloseby) {
            ELog(@"公司不支持closeBy");
        }
    }
 
    
    IXAccountBalanceModel *abModel = [IXAccountBalanceModel shareInstance];
    PFTModel *model = [abModel caculateProfit:tmpModel.position];
    [cell setCurrentPrice:model.nPrice
               WithProfit:model.profit
            WithQuoteTime:@(model.qModel.n1970Time)
               WithSymbol:tmpModel.symbolModel];
    
    [cell setLastClosePriceInfo:[_lastCloseDic objectForKey:[NSString stringWithFormat:@"%llu",tmpModel.symbolModel.id_p]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [(IXPositionCell *)cell setMultiSelection:_isEditingPosition];
}

//普通平仓
- (void)pushToNormal:(IXPositionM *)pModel
{
    IXQuoteM *model = [[IXAccountBalanceModel shareInstance].quoteDataDic objectForKey:[self modelWithSymbolId:pModel.symbolModel.id_p]];
    pModel.quoteModel = model;
    pModel.stoploss = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",pModel.position.stopLoss]];
    pModel.takeprofit = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",pModel.position.takeProfit]];
    
    IXPositionInfoVC *VC = [[IXPositionInfoVC alloc] initWithPositionModel:pModel];
    
    VC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:VC animated:YES];
}

//对冲平仓
- (void)pushToHedge:(IXPositionM *)pModel
{
    IXQuoteM *model = [[IXAccountBalanceModel shareInstance].quoteDataDic objectForKey:[self modelWithSymbolId:pModel.symbolModel.id_p]];
    pModel.quoteModel = model;
    pModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.f",pModel.position.volume]];
    
    PFTModel *pftM = [[IXAccountBalanceModel shareInstance] caculateProfit:pModel.position];
    pModel.requestPrice = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",pftM.nPrice]];

    IXHedgePosVC *posVC = [[IXHedgePosVC alloc] init];
    posVC.posModel = pModel;
    posVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:posVC animated:YES];
}

//快速平仓
- (void)closePosition:(IXPositionM *)pModel
{
    proto_order_add *proto = [[proto_order_add alloc] init];
    proto.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    
    item_order *order = [[item_order alloc] init];
    order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    order.symbol  =  pModel.symbolModel.name;
    order.symbolid = pModel.symbolModel.id_p;
    order.createTime = [IXEntityFormatter getCurrentTimeInterval];
    order.clientType = [IXUserInfoMgr shareInstance].itemType;
    order.positionid = pModel.position.id_p;
    order.direction = pModel.position.direction ;
    order.type = item_order_etype_TypeClose;
    
    NSString *nPrc = [[IXAccountBalanceModel shareInstance] getNprc:pModel.position
                                                              Quote:pModel.quoteModel];
    if (!nPrc) {
        return;
    }
    order.requestPrice = [nPrc doubleValue];
    order.requestVolume = pModel.position.volume;
    proto.order = order;
    pModel.requestPrice = [NSDecimalNumber decimalNumberWithString:nPrc];
    pModel.requestVolume = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.f",pModel.position.volume]];
   
    [[IXTCPRequest shareInstance] orderWithParam:proto];
}

//批量平仓
- (void)batchClosePosition
{
    proto_order_add_batch *proto_batch = [[proto_order_add_batch alloc] init];
    proto_batch.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    proto_batch.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    
    NSMutableArray *orderArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.closePosArr.count; i++) {
        IXPositionM *pModel = self.closePosArr[i];
        item_order *order = [[item_order alloc] init];
        order.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
        order.positionid = pModel.position.id_p;
        order.direction = pModel.position.direction ;
        order.requestVolume = pModel.position.volume;
        order.type = item_order_etype_TypeClose;
        [orderArr addObject:order];
    }
    proto_batch.orderArray = orderArr;
    [[IXTCPRequest shareInstance] batchOrderWithParam:proto_batch];
    
}

//编辑持仓
- (void)editPosition
{
    _isEditingPosition = YES;
    CGFloat height = self.view.frame.size.height;
    [self.tableV reloadData];
    [self.view addSubview:self.closeV];
    self.closeV.frame = CGRectMake(-kScreenWidth, height - kSize.height, kSize.width, kSize.height);

    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.closeV.frame = CGRectMake(0, height - kSize.height, kSize.width, kSize.height);
    } completion:^(BOOL finished) {
        UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kSize.height)];
        v.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        self.tableV.tableFooterView = v;
    }];
}

- (void)endEditPosition
{
    _isEditingPosition = NO;
    [self.closePosArr removeAllObjects];
    [self.dataArr enumerateObjectsUsingBlock:^(IXPositionM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.choosed = NO;
    }];
    
    CGFloat height = self.view.frame.size.height;
    self.tableV.tableFooterView = nil;
    [_tableV reloadData];
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.closeV.frame = CGRectMake(-kScreenWidth, height - kSize.height, kSize.width, kSize.height);
    } completion:^(BOOL finished) {
        [self.closeV resetChooseBtnState];
        [self.closeV removeFromSuperview];
    }];
}

- (void)dismissFolterView
{
    if (self.filterV.isShow) {    
        [_topV resetFilterBtnState];
        [self.filterV dismiss];
    }
    if (self.sortV.isShow) {
        [_topV resetSortBtnState];
        [self.sortV dismiss];
    }
}

- (NSArray <NSDictionary *>*)symbolDic
{
    __block NSMutableArray  * arr = [@[] mutableCopy];
    NSMutableDictionary * dic = [@{} mutableCopy];
    
    for (int i = 0; i < _dataArr.count; i ++) {
        IXPositionM *tmpModel = _dataArr[i];
        [dic setObject:@(tmpModel.symbolModel.id_p) forKey:tmpModel.symbolModel.languageName];
    }
    
    [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [arr addObject:@{@"name":key,
                         @"symbolId":obj
                         }];
    }];
    return arr.copy;
}

#pragma mark -
#pragma mark - IXTradeDataKVO
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath, PB_CMD_USERLOGIN_INFO)) {
        _needResNoti = NO;
    }else if(IXTradeData_isSameKey(keyPath, PB_CMD_USER_LOGIN_DATA_TOTAL)){
        _needResNoti = YES;
        [self.tableV reloadData];
    }
    
    if(IXTradeData_isSameKey(keyPath,PB_CMD_POSITION_LIST)) {
        proto_position_list  *pb = ((IXTradeDataCache *)object).pb_position_list;
        [self refreshListData:pb.positionArray];
    } else if(IXTradeData_isSameKey(keyPath,PB_CMD_POSITION_UPDATE)) {
        proto_position_update  *pb = ((IXTradeDataCache *)object).pb_position_update;
        if (pb.result == 0) {
            [self refreshUpdateData:pb];
        }
    } else if(IXTradeData_isSameKey(keyPath,PB_CMD_POSITION_ADD)) {
        proto_position_add  *pb = ((IXTradeDataCache *)object).pb_position_add;
        if (pb.result == 0) {
            [self refreshAddData];
        }
    } else if ([keyPath isEqualToString:PB_CMD_ACCOUNT_CHANGE]) {
        [self refreashPositionQuote:nil];
        [_dataArr removeAllObjects];
        [self reloadTableViewData];
    } else if(IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_UPDATE)) {
        proto_account_update  *pb = ((IXTradeDataCache *)object).pb_account_update;
        if (pb.result == 0) {
            [self refreashPositionQuote:nil];
        }
    } else if(IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_CHANGE)) {
        [self refreshMarginView];
    } else if ( IXTradeData_isSameKey(keyPath, PB_CMD_COMPANY_UPDATE)){
        proto_company_update *pb = ((IXTradeDataCache *)object).pb_company_update;
        if ( pb.result == 0 ) {
            _companyMode = -1;
            [self queryCompanyMode];
            [self.tableV reloadData];
        }
    } else if ( IXTradeData_isSameKey(keyPath, PB_CMD_ORDER_ADD_BATCH) ) {
        proto_order_add_batch *pb = ((IXTradeDataCache *)object).pb_order_add_batch;
        if (pb.result == 0) {
            [self.tableV reloadData];
        } else {
            [SVProgressHUD showErrorWithStatus:[IXEntityFormatter getErrorInfomation:[NSString stringWithFormat:@"%u",pb.result]]];
        }
    }
}


#pragma mark -
#pragma mark -  IXQuoteDistribute
- (void)needRefresh
{
    id obj = self.navigationController.topViewController;
    if ( ![obj isKindOfClass:[self class]] ) {
        if ([obj respondsToSelector:@selector(needRefresh)]) {
            [obj needRefresh];
        }
    }
}

- (void)subscribeVisualQuote
{
    [self subscribeDynamicPrice];
}

- (void)queryCompanyMode
{
    if ( _companyMode == -1 ) {
        _companyMode = [[[IXDBCompanyMgr  queryCompanyInfoByCompanyId:CompanyID] objectForKey:kMode] integerValue];
    }
}

#pragma mark 刷新UI
- (void)refreashPositionQuote:(NSNotification *)notify
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self refreshPositionHeadData];
        if ( !notify ) {
            [_tableV reloadData];
        }else{
            //行情更新推动的刷新，只需要刷新特定一行的值
            for ( IXPositionCell *cell in [_tableV visibleCells] ) {
                if ( _showArr.count <= cell.tag ) {
                    return;
                }
                
                IXPositionM *tmpModel = _showArr[cell.tag];
                IXAccountBalanceModel *abModel = [IXAccountBalanceModel shareInstance];
                PFTModel *model = [abModel caculateProfit:tmpModel.position];
                
                [cell setCurrentPrice:model.nPrice
                           WithProfit:model.profit
                        WithQuoteTime:@(model.qModel.n1970Time)
                           WithSymbol:tmpModel.symbolModel];                
            }
        }
    });
}

-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    _endScroll = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:sender afterDelay:.1f];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    _endScroll = YES;
}


- (void)refreahLastClosePrice:(NSArray *)arr
{
    [_lastCloseDic removeAllObjects];
    for (IXLastQuoteM *model in arr) {
        [_lastCloseDic setObject:model forKey:[NSString stringWithFormat:@"%ld",(long)model.symbolId]];
    }
}


- (NSString *)modelWithSymbolId:(uint64_t)symbolId
{
    return [NSString stringWithFormat:@"%ld",(long)symbolId];
}

- (void)resetFontWithLabel:(UILabel *)lbl WithContent:(NSString *)content WithFont:(UIFont *)font
{
    if ( !content || !lbl || !font ) {
        return;
    }
    lbl.adjustsFontSizeToFitWidth = YES;
}


/** 弹出删选提示 */
- (void)showFilterTip
{
    self.tipV.hidden = NO;
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.tableV.frame = CGRectMake(0, 44 + kTipSize.height, kScreenWidth, self.view.bounds.size.height - 44);
    } completion:^(BOOL finished) {
        UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kTipSize.height)];
        v.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        self.tableV.tableFooterView = v;
    }];
}

- (void)dismissFolterTip
{
    self.tableV.tableFooterView = nil;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.tableV.frame = CGRectMake(0, 44, kScreenWidth, self.view.bounds.size.height - 44);
    } completion:^(BOOL finished) {
        self.tipV.hidden = YES;
    }];
}

#pragma mark -
#pragma mark - lazy load

- (IXPositonTopV *)topV
{
    if (!_topV) {
        _topV = [[IXPositonTopV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
        _topV.actionB = [self filterAndSortBlock];
    }
    return _topV;
}

- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, kScreenWidth,
                                                                kScreenHeight - kTabbarHeight - kNavbarHeight - 44)
                                               style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.tableFooterView = nil;
        _tableV.tableHeaderView = self.headerV;
    }
    return _tableV;
}

- (IXPositionHeaderV *)headerV
{
    if (!_headerV) {
        _headerV = [IXPositionHeaderV new];
        [_headerV resetMarginRateWith:1.f dkColor:DKColorWithRGBs(0x11b873,0x50a1e5)];
    }
    return _headerV;
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0,130, kScreenWidth, 217)];
    }
    return _posDefView;
}

- (IXMultiCloseV *)closeV
{
    if (!_closeV) {
        _closeV = [IXMultiCloseV new];
        _closeV.chooseB = [self chooseBlock];
    }
    return _closeV;
}

- (IXPositionFilterV *)filterV
{
    if (!_filterV) {
        _filterV = [IXPositionFilterV new];
        _filterV.filterB = [self filterBlock];
    }
    return _filterV;
}

- (IXPositionSortV *)sortV
{
    if (!_sortV) {
        _sortV = [IXPositionSortV new];
        _sortV.sortB = [self sortBlock];
        _sortV.dismissB = [self sortVDismissBlock];
    }
    return _sortV;
}

- (IXPositionTipV *)tipV
{
    if (!_tipV) {
        _tipV = [[IXPositionTipV alloc] initWithFrame:CGRectMake(0, 44, kTipSize.width, kTipSize.height)];
        _tipV.cleanB = [self tipVCleanBlock];
        _tipV.hidden = YES;
    }
    return _tipV;
}

//////////////////////////////////////

- (NSDictionary *)accGroupDic
{
    _accGroupDic = [IXAccountGroupModel shareInstance].accGroupDic;
    return _accGroupDic;
}

- (NSMutableDictionary *)markCloseByDic
{
    if ( !_markCloseByDic ) {
        _markCloseByDic = [[NSMutableDictionary dictionary] mutableCopy];
    }
    return _markCloseByDic;
}

- (NSMutableDictionary *)symDirDic
{
    if ( !_symDirDic ) {
        _symDirDic = [[NSMutableDictionary dictionary] mutableCopy];
    }
    return _symDirDic;
}

- (NSMutableDictionary *)tradeStateDic
{
    if ( !_tradeStateDic ) {
        _tradeStateDic = [[NSMutableDictionary dictionary] mutableCopy];
    }
    return _tradeStateDic;
}

- (NSMutableArray *)closePosArr
{
    if (!_closePosArr) {
        _closePosArr = [@[] mutableCopy];
    }
    return _closePosArr;
}

#pragma mark -
#pragma mark - block

- (multiChooseBlock)chooseBlock
{
    weakself;
    multiChooseBlock chooseB = ^(ChooseType type) {
        switch (type) {
            case ChooseTypeChooseAll:{
                [weakSelf.showArr enumerateObjectsUsingBlock:^(IXPositionM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    obj.choosed = YES;
                }];
                [weakSelf.tableV reloadData];
                //全选
                break;
            }
            case ChooseTypeUnChooseAll:{
                [weakSelf.showArr enumerateObjectsUsingBlock:^(IXPositionM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    obj.choosed = NO;
                }];
                [weakSelf.tableV reloadData];
                //取消全选
                break;
            }
            case ChooseTypeCancel:{
                //取消
                [weakSelf endEditPosition];
                break;
            }
            case ChooseTypeClosePos:{
                //批量平仓
                [weakSelf.closePosArr removeAllObjects];
                [weakSelf.showArr enumerateObjectsUsingBlock:^(IXPositionM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.choosed) {
                        [weakSelf.closePosArr addObject:obj];
                        obj.choosed = NO;
                    }
                }];
                
                //对weakSelf.closePosArr中保存的持仓model进行平仓操作
                NSString * log = [NSString stringWithFormat:@"########### 处理批量平仓操作 数量 %ld #############",weakSelf.closePosArr.count];
                DLog(@"%@", log);
                if (weakSelf.closePosArr.count == 0) {
                    return;
                }
                [weakSelf batchClosePosition];
                [weakSelf endEditPosition];
                break;
            }
            default:
                break;
        }
    };
    return chooseB;
}

- (filterAndSortBlock)filterAndSortBlock
{
    weakself;
    filterAndSortBlock  block = ^(PosActionType type){
        if (weakSelf.isEditingPosition) {
            //去除编辑状态
            [weakSelf endEditPosition];
        }
        
        switch (type) {
            case PosActionTypeFilter: {
                //筛选
                weakSelf.filterV.itemArr = [weakSelf symbolDic];
                [weakSelf.filterV show];
                //隐藏快速平仓按钮
                [[NSNotificationCenter defaultCenter] postNotificationName:kBeginDragingKey object:nil];
                if (weakSelf.sortV.isShow) {
                    [weakSelf.sortV dismiss];
                    [weakSelf.topV resetSortBtnState];
                }
                break;
            }
            case PosActionTypeCancelFilter: {
                //取消筛选
                [weakSelf.filterV dismiss];
                break;
            }
            case PosActionTypeSort: {
                //排序
                [weakSelf.sortV show];
                
                //隐藏快速平仓按钮
                [[NSNotificationCenter defaultCenter] postNotificationName:kBeginDragingKey object:nil];
                if (weakSelf.filterV.isShow) {
                    [weakSelf.filterV dismiss];
                    [weakSelf.topV resetFilterBtnState];
                }
                break;
            }
            case PosActionTypeCancelSort: {
                //排序
                [weakSelf .sortV dismiss];
                break;
            }
            default:
                break;
        }
    };
    return block;
}

- (positionFilterBlock)filterBlock
{
    weakself;
    positionFilterBlock block = ^(PositionFilterType type, NSArray * selectedArr, BOOL dismiss){
        DLog(@"----- 根据以上条件进行筛选 -----");
        if (dismiss) {        
            [weakSelf.topV resetFilterBtnState];
        }
        [weakSelf dealWithPositionFilterType:type selectSymbol:selectedArr];
        [weakSelf reloadTableViewData];
    };
    return block;
}

- (void)dealWithPositionFilterType:(PositionFilterType)type
                      selectSymbol:(NSArray *)selectArr
{
    if (type > PositionFilterTypeAll || selectArr.count) {
        //处理tip
        __block NSString    * str = @"";
        for (int i = 0; i < selectArr.count; i ++) {
            NSDictionary    * dic = selectArr[i];
            str = [str stringByAppendingString:dic[@"name"]];
            if (i < selectArr.count - 1) {
                str = [str stringByAppendingString:@"、"];
            }
        }
        
        NSString    * dir = @"";
        if (type == PositionFilterTypebuy) {
            dir = LocalizedString(@"方向为:买入");
        }else if (type == PositionFilterTypeSell) {
            dir = LocalizedString(@"方向为:卖出");
        }
        if (dir.length && str.length) {
            str = [str stringByAppendingString:@"，"];
        }
        
        str = [str stringByAppendingString:dir];
        self.tipV.message = str;
        [self showFilterTip];
    } else {
        //清除筛选提示
        [self dismissFolterTip];
        [self.filterV resetAction];
    }
}

- (positionSortBlock)sortBlock
{
    weakself;
    positionSortBlock block = ^(PositionSortType type) {
        weakSelf.sortOp = type;
        [weakSelf reloadTableViewData];
    };
    return block;
}

- (sortVDismissBlock)sortVDismissBlock
{
    weakself;
    sortVDismissBlock block = ^{
        [weakSelf.topV resetSortBtnState];
    };
    return block;
}

- (tipVCleanBlock)tipVCleanBlock
{
    weakself;
    tipVCleanBlock block = ^{
        DLog(@"清除筛选操作");
        [weakSelf dismissFolterTip];
        [weakSelf.filterV resetAction];
        [weakSelf reloadTableViewData];
    };
    return block;
}

- (positionCellQuickBlock)cellQuickBlock
{
    weakself;
    positionCellQuickBlock block = ^(IXPositionM *m){
        [weakSelf setEditing:false animated:true];
        [weakSelf closePosition:m];
    };
    return block;
}

- (positionCellHedgeBlock)cellHedgeBlock
{
    weakself;
    positionCellHedgeBlock block = ^(IXPositionM *m){
        [weakSelf setEditing:false animated:true];
        [weakSelf pushToHedge:m];
    };
    return block;
}

- (positionCellTapBlock)cellTapBlock
{
    weakself;
    positionCellTapBlock block = ^(IXPositionM * m, IXPositionCell * c){
        if (_isEditingPosition) {
            [c chooseAction];
            [weakSelf.closeV resetChooseBtnState];
        } else {
            [weakSelf pushToNormal:m];
        }
    };
    return block;
}

- (positionCellEditingBlock)cellEditBlock
{
    weakself;
    positionCellEditingBlock block = ^{
        [weakSelf editPosition];
    };
    return block;
}

@end

