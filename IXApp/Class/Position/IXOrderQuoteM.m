//
//  IXOrderQuoteM.m
//  IXApp
//
//  Created by Evn on 2017/6/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOrderQuoteM.h"
#import "IXAccountBalanceModel.h"
#import "IXTradeMarketModel.h"

@implementation IXOrderQuoteM

//订阅数据
+ (NSMutableArray *)subscribeDynamicPrice:(IXUpdateOrderVC *)root
{
    [[IXAccountBalanceModel shareInstance] addSubSymbolWithSymbolId:root.tradeModel.symbolModel.id_p];
    [IXOrderQuoteM loadDetailQuote:root];
    
    return  [IXOrderQuoteM loadCacheQuote:root];
}

+ (void)cancelDynamicPrice:(IXUpdateOrderVC *)root
{
    [[IXAccountBalanceModel shareInstance] delSubSymbolWithSymbolId:root.tradeModel.symbolModel.id_p];
    [IXOrderQuoteM cancelDetailQuote:root];
}

+ (NSMutableArray *)loadCacheQuote:(IXUpdateOrderVC *)root
{
    if (!root.tradeModel.quoteModel) {
        return nil;
    }
    NSMutableArray *_quote = [NSMutableArray arrayWithObject:root.tradeModel.quoteModel];
    
    NSString *symbolKey = [NSString stringWithFormat:@"%llu",root.tradeModel.symbolModel.id_p];
    id subDic = [[IXAccountBalanceModel shareInstance].subSymbolDic objectForKey:symbolKey];
    if (subDic) {
        if ([subDic isKindOfClass:[NSDictionary class]]) {
            for (NSString *curKey in [subDic allKeys]) {
                
                NSDictionary *curVal = [(NSDictionary *)subDic objectForKey:curKey];
                if( [curVal integerForKey:@"symbolId"] != 0 ){
                    
                    NSString *curSymbolKey = [NSString stringWithFormat:@"%ld",(long)[curVal integerForKey:@"symbolId"]];
                    IXQuoteM *model = [[IXQuoteDataCache shareInstance].dynQuoteDic objectForKey:curSymbolKey];
                    if( !model ){
                        model = [IXDataProcessTools queryQuoteDataBySymbolId:[curVal integerForKey:@"symbolId"]];
                        if (model) {
                            [[IXQuoteDataCache shareInstance].dynQuoteDic setValue:model forKey:curSymbolKey];
                        }
                    }
                    if ( model && model.BuyPrc.count != 0 && model.SellPrc.count != 0 ) {
                        [_quote addObject:model];
                    }
                }
            }
            
        }else{
            
            IXQuoteM *model = [[[IXQuoteDataCache shareInstance].dynQuoteDic objectForKey:symbolKey] mutableCopy];
            if(!model){
                model = [IXDataProcessTools queryQuoteDataBySymbolId:[symbolKey integerValue]];
                if (model) {
                    [[IXQuoteDataCache shareInstance].dynQuoteDic setValue:model forKey:symbolKey];
                }
            }
            if (model && model.BuyPrc.count != 0 && model.SellPrc.count != 0) {
                [_quote addObject:model];
            }
        }
    }
    [[IXAccountBalanceModel shareInstance] setQuoteDataArr:_quote];
    return _quote;
}


+ (void)loadDetailQuote:(IXUpdateOrderVC *)root
{
    if ( root.tradeModel && root.tradeModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(root.tradeModel.marketId),
                           @"id":@(root.tradeModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] subscribeDetailPriceWithSymbolAry:arr];
        if (root.openModel.nLastClosePrice <= 0) {
            [[IXTCPRequest shareInstance] getLastClostPriceWithSymbolAry:arr];
        }
    }
}

+ (void)cancelDetailQuote:(IXUpdateOrderVC *)root
{
    if ( root.tradeModel && root.tradeModel.symbolModel) {
        NSArray *arr = @[@{@"marketId":@(root.tradeModel.marketId),
                           @"id":@(root.tradeModel.symbolModel.id_p)}];
        [[IXTCPRequest shareInstance] unSubscribeDetailPriceWithSymbolAry:arr];
    }
}

@end
