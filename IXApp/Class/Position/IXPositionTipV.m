//
//  IXPositionTipV.m
//  IXApp
//
//  Created by Seven on 2017/11/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionTipV.h"
#import "UILabel+IX.h"

@interface IXPositionTipV ()
@property (nonatomic, strong)UILabel    * messageLab;
@end
@implementation IXPositionTipV

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createSubview];
    }
    return self;
}

- (void)createSubview
{
    UIImageView * bgImgV = [[UIImageView alloc] initWithFrame:self.bounds];
    bgImgV.image = [UIImage imageNamed:@""];
    bgImgV.contentMode = UIViewContentModeScaleToFill;
    [self addSubview:bgImgV];
    
    UIImageView * imgV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8.5, 15, 15)];
    imgV.dk_imagePicker = DKImageNames(@"position_tip", @"position_tip_D");
    [self addSubview:imgV];
    
    _messageLab = [UILabel labelWithFrame:CGRectMake(33, 5, kScreenWidth - 90, 22)
                                     text:@""
                                 textFont:PF_MEDI(12)
                                textColor:DKColorWithRGBs(0x11b873, 0x21ce99)];
    [self addSubview:_messageLab];
    
    UILabel * cleanL = [UILabel labelWithFrame:CGRectMake( kScreenWidth - 58, 5, 50, 22)
                                          text:LocalizedString(@"清除")
                                      textFont:PF_MEDI(12)
                                     textColor:DKCellTitleColor
                                 textAlignment:NSTextAlignmentRight];
    [self addSubview:cleanL];
    
    UIButton    * cleanBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 48, 8, 48, 32)];
    [cleanBtn addTarget:self action:@selector(cleanAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cleanBtn];
}

- (void)cleanAction
{
    if (self.cleanB) {
        self.cleanB();
    }
}

- (void)setMessage:(NSString *)message
{
    _message = message;
    _messageLab.text = message;
}

@end
