//
//  IXPositionPrcCell.h
//  IXApp
//
//  Created by Evn on 2017/6/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXAccTextField.h"

typedef void(^add)();
typedef void(^step)();
typedef void(^substract)();

@interface IXPositionPrcCell : UITableViewCell

@property (nonatomic, strong) IXAccTextField *inputTF;
@property (nonatomic, strong) UIButton *subBtn;
@property (nonatomic, strong) UIButton *stepBtn;
@property (nonatomic, strong) UIButton *addBtn;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *inputContent;
@property (nonatomic, copy) NSString *inputExplain;

@property (nonatomic, copy) add addBlock;
@property (nonatomic, copy) add stepBlock;
@property (nonatomic, copy) substract substractBlock;

@end
