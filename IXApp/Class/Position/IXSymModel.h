//
//  IXSymModel.h
//  IXApp
//
//  Created by Evn on 2017/7/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXSymModel : NSObject

/**唯一标示符，item的基本属性*/
@property (nonatomic,assign) uint64_t id_p;
/** 语言名称 */
@property (nonatomic,strong) NSString *languageName;
/**报价源*/
@property (nonatomic,copy) NSString *source;
/**产品名称*/
@property (nonatomic,copy) NSString *name;
/** marketID */
@property (nonatomic,assign) uint32_t marketID;
/** market名称 */
@property (nonatomic,strong) NSString *marketName;
/**合约大小新*/
@property (nonatomic,assign) uint32_t contractSizeNew;
/**小数位*/
@property (nonatomic,assign) uint8_t digits;
/** 显示名称（目前为产品单位） */
@property (nonatomic,strong) NSString *displayName;
/** 产品数量单位多语言名称 */
@property (nonatomic,strong) NSString *unitLanName;
/** 产品分类ID */
@property (nonatomic, assign) uint64_t cataId;
/** 假期分类ID */
@property (nonatomic, assign) uint64_t holidayCataId;
/** 交易时间分类ID */
@property (nonatomic, assign) uint64_t scheduleCataId;
/** 开始时间 */
@property (nonatomic, assign) uint64_t startTime;
/** 过期时间 */
@property (nonatomic, assign) uint64_t expiryTime;
/* 是否可交易 */
@property (nonatomic, assign) uint64_t tradable;
/** 延迟交易时间 */
@property (nonatomic,assign) uint32_t scheduleDelayMinutes;
/**最小手数*/
@property (nonatomic,assign) double volumesMin;
/**手數間隔*/
@property (nonatomic,assign) double volumesStep;
/**最大手數*/
@property (nonatomic,assign) double volumesMax;
/**大点比率*/
@property (nonatomic,assign) uint8_t pipsRatio;
/**最小手动审批手数*/
@property (nonatomic,assign) uint16_t stopLevel;
/**限价/止损订单最高间隔点*/
@property (nonatomic,assign) uint16_t maxStopLevel;
/**是否设置group_symbol*/
@property (nonatomic, assign) BOOL isSetGroupSym;
/** group_symbol是否可交易 */
@property (nonatomic, assign) uint64_t symTradable;
/**  是否仅平仓 */
@property (nonatomic, assign) BOOL  closeOnly;
/* 数量小数位 */
@property(nonatomic, readwrite) uint64_t volDigits;

@end
