//
//  IXHedgePosVC.h
//  IXApp
//
//  Created by Bob on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXPositionM.h"

@interface IXHedgePosVC : IXDataBaseVC

@property (nonatomic, strong) IXPositionM *posModel;

@end
