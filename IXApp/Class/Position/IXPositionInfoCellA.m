//
//  IXPositionInfoCellA.m
//  IXApp
//
//  Created by Evn on 2017/6/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPositionInfoCellA.h"

@interface IXPositionInfoCellA()

@property (nonatomic, strong)UILabel *dirLbl;//买卖方向
@property (nonatomic, strong)UILabel *numLbl;//数量
@property (nonatomic, strong)UILabel *showPriceLbl;
@property (nonatomic, strong)UILabel *priceLbl;//开盘价
@property (nonatomic, strong)UILabel *showProfitLbl;
@property (nonatomic, strong)UILabel *profitLbl;//盈亏
@end
@implementation IXPositionInfoCellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self showPriceLbl];
    [self showProfitLbl];
}

- (UILabel *)dirLbl
{
    if (!_dirLbl) {
        _dirLbl = [IXCustomView createLable:CGRectMake(12,9,15, 15)
                                      title:@""
                                       font:PF_MEDI(10)
                                 wTextColor:0xffffff
                                 dTextColor:0xffffff
                              textAlignment:NSTextAlignmentCenter];
        _dirLbl.backgroundColor = MarketGreenPriceColor;
        [self.contentView addSubview:_dirLbl];
    }
    return _dirLbl;
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.dirLbl) + 5, GetView_MinY(self.dirLbl), 100, 16)
                                      title:@""
                                       font:RO_REGU(15)
                                  wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                              textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_numLbl];
    }
    return _numLbl;
}

- (UILabel *)showPriceLbl
{
    if (!_showPriceLbl) {
        NSInteger width = [IXEntityFormatter getContentWidth:LocalizedString(@"开仓价") WithFont:PF_MEDI(12)] + 1;
        _showPriceLbl = [IXCustomView createLable:CGRectMake( 132, 11, width, 12)
                                               title:LocalizedString(@"开仓价")
                                                font:PF_MEDI(12)
                                           wTextColor:0x99abba
                                       dTextColor:0x8395a4
                                       textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_showPriceLbl];
    }
    return _showPriceLbl;
}

- (UILabel *)priceLbl
{
    if (!_priceLbl) {
        _priceLbl = [IXCustomView createLable:CGRectMake(GetView_MaxX(self.showPriceLbl) + 5, VIEW_Y(self.showPriceLbl) - 2, 200, 15)
                                        title:@""
                                         font:RO_REGU(15)
                                    wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_priceLbl];
    }
    return _priceLbl;
}

- (UILabel *)showProfitLbl
{
    if (!_showProfitLbl) {
        _showProfitLbl = [IXCustomView createLable:CGRectMake( 0, VIEW_Y(self.showPriceLbl),kScreenWidth - 94, 12)
                                             title:LocalizedString(@"盈亏")
                                              font:PF_MEDI(12)
                                        wTextColor:0x99abba
                                        dTextColor:0x8395a4
                                     textAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_showProfitLbl];
    }
    return _showProfitLbl;
}

- (UILabel *)profitLbl
{
    if (!_profitLbl) {
        _profitLbl = [IXCustomView createLable:CGRectMake(kScreenWidth - 90, VIEW_Y(self.showProfitLbl) -2, 80, (15))
                                         title:@"--"
                                          font:RO_REGU(15)
                                    wTextColor:0x4c6072
                                    dTextColor:0xffffff
                                 textAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_profitLbl];
    }
    return _profitLbl;
}

- (void)config:(IXPositionM *)model
{
    if (model.position.direction == 1) {
        self.dirLbl.text = LocalizedString(@"买.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mGreenPriceColor, mDGreenPriceColor);
    } else {
        self.dirLbl.text = LocalizedString(@"卖.");
        self.dirLbl.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        self.dirLbl.dk_backgroundColorPicker = DKColorWithRGBs(mRedPriceColor, mDRedPriceColor);
    }
    self.numLbl.text = [NSString thousandFormate:[IXDataProcessTools showCurrentVolume:model.position.volume contractSizeNew:model.symbolModel.contractSizeNew volDigit:model.symbolModel.volDigits] withDigits:model.symbolModel.volDigits];
    self.priceLbl.text = [NSString formatterPrice:[NSString stringWithFormat:@"%lf",model.position.openPrice] WithDigits:model.symbolModel.digits];
}

- (void)refreshProfit:(double)profit
{
    [IXDataProcessTools resetTextColorLabel:self.profitLbl value:profit];
    self.profitLbl.text = [IXDataProcessTools moneyFormatterComma:profit positiveNumberSign:NO];
    CGSize size = [self.profitLbl.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 15) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:RO_REGU(15)} context:nil].size;
    CGRect frame = self.profitLbl.frame;
    frame.origin.x = kScreenWidth - (size.width + 10);
    frame.size.width = size.width;
    self.profitLbl.frame = frame;
    
    frame = self.showProfitLbl.frame;
    frame.size.width = kScreenWidth - (size.width + 14);
    self.showProfitLbl.frame = frame;
    
}

@end
