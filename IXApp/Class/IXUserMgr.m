//
//  IXUserMgr.m
//  IXApp
//
//  Created by mac on 2018/8/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXUserMgr.h"
#import "IXCpyConfig.h"
#import "IXAccountBalanceModel.h"
#import "IXDataBase.h"

@implementation IXUserMgr
SingletonM(IXUserMgr)
- (void)switchToRealAccount:(NSInteger)swithType{
    
    NSString *accountType = [IXDataProcessTools showCurrentAccountType];
    if ([accountType isEqualToString:LocalizedString(@"模拟")]) {
        accountType = LocalizedString(@"真实");
    }else{
        accountType = LocalizedString(@"模拟");
    }
    NSString *tipMsg = [NSString stringWithFormat:@"Switch to %@ acoount",accountType];
    
    switch (swithType) {
        case 2:{
            tipMsg = [tipMsg stringByAppendingString:@",then go to deposit page"];
        }break;
        case 3:{
            // 纯切换账户
        }break;
        case 4:{
            tipMsg = [tipMsg stringByAppendingString:@",then go to deposit page"];;
        }break;
        default:
            break;
    }
    [SVProgressHUD showWithStatus:tipMsg];
    
    //ix平台账户
    NSArray *ixArr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                      [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:0];
    
    if (ixArr.count < 2) {
        [SVProgressHUD showInfoWithStatus:@"Tolong buka akun nyata dulu."];
        return;
    }
    NSDictionary *realDic = ixArr[1];
    NSInteger realAccId = [realDic[@"id"] integerValue];
    NSInteger realAccGroupId = [realDic[@"accountGroupId"] integerValue];
    
    [(AppDelegate *)[UIApplication sharedApplication].delegate setSwitchAccount:swithType];
    
    [[IXAccountBalanceModel shareInstance] clearCache];
    
    proto_user_login_account *pb = [[proto_user_login_account alloc] init];
    pb.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    pb.userid = [IXUserInfoMgr shareInstance].userLogInfo.account.userid;
    pb.previousAccountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    pb.accountid = realAccId;
    pb.name = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
    pb.companyToken = CompanyToken;
    pb.companyid = CompanyID;
    pb.sessionType = [IXUserInfoMgr shareInstance].itemType;
    pb.type = proto_user_login_elogintype_ByPhone;
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    pb.version =  [version intValue];
    pb.logintime = [IXEntityFormatter getCurrentTimeInterval];
    pb.dataVersion = [self dataVersionWithAccountId:realAccId accountGroupId:realAccGroupId];
    
    [[IXTCPRequest shareInstance] swiAccountWithParam:pb];
}


- (void)switchToDemoAccount:(NSInteger)swithType{
    
    //ix平台账户
    NSArray *ixArr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                      [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:0];
    
    if (ixArr.count < 2) {
        return;
    }
    NSDictionary *realDic = ixArr[0];
    NSInteger demoAccId = [realDic[@"id"] integerValue];
    NSInteger democcGroupId = [realDic[@"accountGroupId"] integerValue];
    
    [(AppDelegate *)[UIApplication sharedApplication].delegate setSwitchAccount:swithType];
    
    [[IXAccountBalanceModel shareInstance] clearCache];
    
    proto_user_login_account *pb = [[proto_user_login_account alloc] init];
    pb.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    pb.userid = [IXUserInfoMgr shareInstance].userLogInfo.account.userid;
    pb.previousAccountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    pb.accountid = demoAccId;
    pb.name = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
    pb.companyToken = CompanyToken;
    pb.companyid = CompanyID;
    pb.sessionType = [IXUserInfoMgr shareInstance].itemType;
    pb.type = proto_user_login_elogintype_ByPhone;
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    pb.version =  [version intValue];
    pb.logintime = [IXEntityFormatter getCurrentTimeInterval];
    pb.dataVersion = [self dataVersionWithAccountId:demoAccId accountGroupId:democcGroupId];
    [SVProgressHUD showWithStatus:LocalizedString(@"Switch to demo acoount...")];
    [[IXTCPRequest shareInstance] swiAccountWithParam:pb];
}


- (item_data_version *)dataVersionWithAccountId:(uint64_t)curAccountId  accountGroupId:(uint64_t)curAccGroupId
{
    item_data_version *dataVersion = [[item_data_version alloc] init];
    
    //产品
    NSDictionary *symbolUUID = [IXDBSymbolMgr querySymbolUUIDMax];
    dataVersion.verSym = [symbolUUID[kUUID] longLongValue];
    
    //产品分类
    NSDictionary *symbolCataUUID = [IXDBSymbolCataMgr querySymbolCatasUUIDMax];
    dataVersion.verSymcata = [symbolCataUUID[kUUID] longLongValue];
    
    //自选产品
    //    NSDictionary *symbolSubUUID = [IXDBSymbolSubMgr querySymbolSubUUIDMaxAccountId:[IXKeepAliveCache shareInstance].accountId];
    //    dataVersion.verSymsub = [symbolSubUUID[kUUID] longLongValue];
    
    NSDictionary *symSubDic = @{
                                kAccountId:@(curAccountId)
                                };
    NSDictionary *symbolSubUUID = [IXDBSymbolSubMgr querySymbolSubUUIDMaxByAddInfo:symSubDic];
    dataVersion.verSymsub = [symbolSubUUID[kUUID] longLongValue];
    
    //公司
    NSDictionary *companyUUID = [IXDBCompanyMgr queryCompanyUUIDMax];
    dataVersion.verCompany = [companyUUID[kUUID] longLongValue];
    
    //热门产品
    NSDictionary *symbolHotUUID = [IXDBSymbolHotMgr querySymbolHotUUIDMax];
    dataVersion.verSymhot = [symbolHotUUID[kUUID] longLongValue];
    
    //假期分类
    NSDictionary *holidayCataUUID = [IXDBHolidayCataMgr queryHolidayCatasUUIDMax];
    dataVersion.verHolidayCata = [holidayCataUUID[kUUID] longLongValue];
    
    //假期
    NSDictionary *holidayUUID = [IXDBHolidayMgr queryHolidayUUIDMax];
    dataVersion.verHoliday = [holidayUUID[kUUID] longLongValue];
    
    //交易时间分类
    NSDictionary *scheduleCataUUID = [IXDBScheduleCataMgr queryScheduleCataUUIDMax];
    dataVersion.verScheduleCata = [scheduleCataUUID[kUUID] longLongValue];
    
    //交易时间
    NSDictionary *scheduleUUID = [IXDBScheduleMgr queryScheduleUUIDMax];
    dataVersion.verSchedule = [scheduleUUID[kUUID] longLongValue];
    
    //产品标签
    NSDictionary *symLableUUID = [IXDBSymbolLableMgr querySymbolLableUUIDMax];
    dataVersion.verSymLabel = [symLableUUID[kUUID] longLongValue];
    
    //marginSet
    NSDictionary *marginSetUUID = [IXDBMarginSetMgr queryMarginSetUUIDMax];
    dataVersion.verMarginSet = [marginSetUUID[kUUID] longLongValue];
    
    //语言
    NSDictionary *languageUUID = [IXDBLanguageMgr queryLanguageUUIDMax];
    dataVersion.verLanguage = [languageUUID[kUUID] longLongValue];
    
    //schedule_Margin
    NSDictionary *scheduleMarginUUID = [IXDBScheduleMarginMgr queryScheduleMarginUUIDMax];
    dataVersion.verScheduleMargin = [scheduleMarginUUID[kUUID] longLongValue];
    
    //holiday_Margin
    NSDictionary *holidayMarginUUID = [IXDBHolidayMarginMgr queryHolidayMarginUUIDMax];
    dataVersion.verHolidayMargin = [holidayMarginUUID[kUUID] longLongValue];
    
    //    //账户组
    if (curAccGroupId == 0) {
        //新开账户还未下发对应的账户账户组、此时全量下发
        dataVersion.verAccgroupSymcata = 0;
    } else {
        NSDictionary *agscDic = @{kAccGroupId : @(curAccGroupId)};
        NSDictionary *agscUUID = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataUUIDMaxByAddInfo:agscDic];
        dataVersion.verAccgroupSymcata = [agscUUID[kUUID] longLongValue];
    }
    
    //结算时间
    NSDictionary *eodTimeUUID = [IXDBEodTimeMgr queryEodTimeUUIDMax];
    dataVersion.verEodTime = [eodTimeUUID[kUUID] longLongValue];
    
    //账户组产品
    NSDictionary *groupSymUUID = [IXDBG_SMgr queryG_SUUIDMax];
    dataVersion.verGrpsym = [groupSymUUID[kUUID] longLongValue];
    
    //账户组产品分类
    NSDictionary *groupSymCataUUID = [IXDBG_S_CataMgr queryG_S_CataUUIDMax];
    dataVersion.verGrpsymcata = [groupSymCataUUID[kUUID] longLongValue];
    
    //lp_channel_account
    NSDictionary *lpchaccUUID = [IXDBLpchaccMgr queryLpchaccUUIDMax];
    dataVersion.verLpchacc = [lpchaccUUID[kUUID] longLongValue];
    
    //lp_channel_account_symbol
    NSDictionary *lpchaccSymUUID = [IXDBLpchaccSymbolMgr queryLpchaccSymbolUUIDMax];
    dataVersion.verLpchaccSym = [lpchaccSymUUID[kUUID] longLongValue];
    
    //lp_channel
    NSDictionary *lpchannelUUID = [IXDBLpchannelMgr queryLpchannelUUIDMax];
    dataVersion.verLpchannel = [lpchannelUUID[kUUID] longLongValue];
    
    //lp_channel_symbol
    NSDictionary *lpchannelSymUUID = [IXDBLpchannelSymbolMgr queryLpchannelSymbolUUIDMax];
    dataVersion.verLpchannelSym = [lpchannelSymUUID[kUUID] longLongValue];
    
    //lp_IbBind
    NSDictionary *lpIbBindUUID = [IXDBLpibBindMgr queryLpibBindUUIDMax];
    dataVersion.verLpibBind = [lpIbBindUUID[kUUID] longLongValue];
    
    return dataVersion;
}

@end
