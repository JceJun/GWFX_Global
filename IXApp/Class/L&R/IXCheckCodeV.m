//
//  IXCheckCodeV.m
//  IXApp
//
//  Created by Evn on 2018/3/13.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCheckCodeV.h"
#import "IXCodeTF.h"

#import "UIImageView+WebCache.h"

@interface IXCheckCodeV()

@property (nonatomic, strong)UIView *alertV;
@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UIImageView *imgV;
@property (nonatomic, strong)UIButton *refreshBtn;
@property (nonatomic, strong)IXCodeTF *codeTF;
@property (nonatomic, strong)UIButton *cancelBtn;
@property (nonatomic, strong)UIButton *sureBtn;
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UIView *cutLine;

@property (nonatomic, copy)NSString *imgUrl;

@end

@implementation IXCheckCodeV

- (id)initWithCodeImageUrl:(NSString *)imgUrl
{
    if (self = [super initWithFrame:kScreenBound]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75f];
        UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenKeyboard)];
        [self addGestureRecognizer:tg];
        self.imgUrl = imgUrl;
        [self loadUI];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)loadUI
{
    [self alertV];
    [self title];
    [self imgV];
    [self refreshBtn];
    [self codeTF];
    [self uLine];
    [self cancelBtn];
    [self sureBtn];
    [self cutLine];
}

- (void)show
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.center = keyWindow.center;
    [keyWindow addSubview:self];
    CGPoint point = keyWindow.center;
    point.y -= 30;
    self.alertV.center = point;
    [keyWindow addSubview:self.alertV];
}

- (UIView *)alertV
{
    if (!_alertV) {
        _alertV = [[UIView alloc]init];
        _alertV.frame = CGRectMake(0, 0, 295, 250);
        _alertV.dk_backgroundColorPicker = DKViewColor;
        _alertV.layer.cornerRadius = 10;
        _alertV.layer.masksToBounds = YES;
        _alertV.userInteractionEnabled = YES;
    }
    return _alertV;
}

- (UILabel *)title
{
    if ( !_title ) {
        _title = [IXUtils createLblWithFrame:CGRectMake( 0, 29,VIEW_W(self.alertV), 20)
                                   WithFont:PF_MEDI(15)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        _title.text = LocalizedString(@"请输入图片验证码");
        [self.alertV addSubview:_title];
    }
    return _title;
}

- (UIImageView *)imgV
{
    if (!_imgV) {
        _imgV = [[UIImageView alloc] initWithFrame:CGRectMake(56, GetView_MaxY(self.title) + 29, 132, 48)];
        [_imgV sd_setImageWithURL:[NSURL URLWithString:self.imgUrl] placeholderImage:GET_IMAGE_NAME(@"regist_code_default") options:SDWebImageAllowInvalidSSLCertificates];
        [self.alertV addSubview:_imgV];
    }
    return _imgV;
}

- (UIButton *)refreshBtn
{
    if (!_refreshBtn) {
        _refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _refreshBtn.frame = CGRectMake(GetView_MaxX(self.imgV), VIEW_Y(self.imgV), 48, 48);
        _refreshBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        [_refreshBtn setBackgroundImage:AutoNightImageNamed(@"regist_code_refresh") forState:UIControlStateNormal];
        [_refreshBtn addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
        [self.alertV addSubview:_refreshBtn];
    }
    return _refreshBtn;
}

- (IXCodeTF *)codeTF
{
    weakself;
    if (!_codeTF) {
        _codeTF = [IXCodeTF instanceWidth:40 count:4 activeBlock:^(BOOL isActive) {
            if (isActive) {
                weakSelf.sureBtn.userInteractionEnabled = YES;
                [weakSelf.sureBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
            } else {
                weakSelf.sureBtn.userInteractionEnabled = NO;
                [weakSelf.sureBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
            }
        }];
        _codeTF.frame = CGRectMake(56, GetView_MaxY(self.imgV) + 19, 180, 24);
        [self.alertV addSubview:_codeTF];
    }
    return _codeTF;
}

- (UIButton *)cancelBtn
{
    if (!_cancelBtn) {
        _cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, VIEW_H(self.alertV) - 46, (VIEW_W(self.alertV) - 1)/2, 46)];
        [_cancelBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = PF_MEDI(15);
        [_cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
        [_cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [self.alertV addSubview:_cancelBtn];
    }
    return _cancelBtn;
}

- (UIButton *)sureBtn
{
    if (!_sureBtn) {
        _sureBtn = [[UIButton alloc] initWithFrame:CGRectMake((VIEW_W(self.alertV) - 1)/2, VIEW_H(self.alertV) - 46, (VIEW_W(self.alertV) - 1)/2, 46)];
        [_sureBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
        _sureBtn.titleLabel.font = PF_MEDI(15);
        _sureBtn.userInteractionEnabled = NO;
        [_sureBtn setTitle:LocalizedString(@"确定.") forState:UIControlStateNormal];
        [_sureBtn addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
        [self.alertV addSubview:_sureBtn];
    }
    return _sureBtn;
}

- (UIView *)uLine
{
    if (!_uLine) {
        _uLine = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(self.alertV) - 46, VIEW_W(self.alertV), 1)];
        _uLine.dk_backgroundColorPicker = DKLineColor;
        [self.alertV addSubview:_uLine];
    }
    return _uLine;
}

- (UIView *)cutLine
{
    if (!_cutLine) {
        _cutLine = [[UIView alloc] initWithFrame:CGRectMake((VIEW_W(self.alertV) - 1)/2, GetView_MaxY(self.uLine), 1 ,VIEW_H(self.sureBtn))];
        _cutLine.dk_backgroundColorPicker = DKLineColor;
        [self.alertV addSubview:_cutLine];
    }
    return _cutLine;
}

#pragma mark -
#pragma mark - keyboard notify
- (void)keyboardWillShow:(NSNotification *)notify
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    CGPoint point = keyWindow.center;
    if (kScreenWidth <= 320) {
        point.y -= 90;
    } else {
        point.y -= 70;
    }
    self.alertV.center = point;
}

- (void)keyboardWillHide:(NSNotification *)notify
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    CGPoint point = keyWindow.center;
    point.y -= 30;
    self.alertV.center = point;
}

- (void)dismiss
{
    if (self.alertV) {
        [self.alertV removeFromSuperview];
        self.alertV = nil;
    }
    if (self) {
        [self removeFromSuperview];
    }
    if (self.dismissB) {
        self.dismissB();
    }
}

- (void)hiddenKeyboard
{
    [self dismiss];
    [self.codeTF resignFirstResponder];
}

- (void)cancelAction
{
    [self dismiss];
}

- (void)sureAction
{
    [self.codeTF resignFirstResponder];
    if (self.checkB) {
        self.checkB(self.codeTF.text);
    }
    [self dismiss];
}

- (void)refreshAction
{
    if (self.refreshB) {
        self.refreshB();
    }
}

- (void)refreshImageUrl:(NSString *)imgUrl
{
    [self.imgV sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:GET_IMAGE_NAME(@"regist_code_default") options:SDWebImageAllowInvalidSSLCertificates];
    self.codeTF.text = @"";
    self.sureBtn.userInteractionEnabled = NO;
    [self.sureBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    [self.codeTF reloadText:@""];
    [self.codeTF becomeFirstResponder];
}

@end
