//
//  IXRegistStep4VC.h
//  IXApp
//
//  Created by Bob on 2017/3/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXRegistStep4VC : IXDataBaseVC

@property (nonatomic, strong) UIViewController *parentVC;

@end
