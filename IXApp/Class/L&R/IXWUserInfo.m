//
//  FXUserInfo.m
//  SDKSample
//
//  Created by Bob on 2017/4/24.
//
//

#import "IXWUserInfo.h"

@implementation IXWUserInfo

+ (void)clear
{
    //删除微信绑定手机记录
    if ([[self getUnionId] length]) {
       [[NSUserDefaults standardUserDefaults] removeObjectForKey:[self getUnionId]];
    }
    [IXWUserInfo delAccessToken];
    [IXWUserInfo delRefeashToken];
    [IXWUserInfo delOpenId];
    [IXWUserInfo delUnionId];
}

+ (void)saveRefreashToken:(NSString *)refreashToken
{
    [IXWUserInfo save:REFREASHTOKEN data:refreashToken];
}

+ (NSString *)getRefreashToken
{
    return [IXWUserInfo load:REFREASHTOKEN];
}

+ (void)delRefeashToken
{
    [IXWUserInfo delete:REFREASHTOKEN];
}


+ (void)saveAccessToken:(NSString *)accessToken
{
    [IXWUserInfo save:ACCESSTOKEN data:accessToken];
}

+ (NSString *)getAccessToken
{
   return [IXWUserInfo load:ACCESSTOKEN];
}

+ (void)delAccessToken
{
    [IXWUserInfo delete:ACCESSTOKEN];
}

+ (void)saveOpenId:(NSString *)openId
{
    [IXWUserInfo save:OPENID data:openId];
}

+ (NSString *)getOpenId
{
    return [IXWUserInfo load:OPENID];
}

+ (void)delOpenId
{
    [IXWUserInfo delete:OPENID];
}

+ (void)saveUnionId:(NSString *)unionId
{
    [IXWUserInfo save:UNIONID data:unionId];

}

+ (NSString *)getUnionId
{
    return [IXWUserInfo load:UNIONID];
}

+ (void)delUnionId
{
    [IXWUserInfo delete:UNIONID];
}

+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service {
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (id)kSecClassGenericPassword,(id)kSecClass,
            service, (id)kSecAttrService,
            service, (id)kSecAttrAccount,
            (id)kSecAttrAccessibleAfterFirstUnlock,(id)kSecAttrAccessible,
            nil];
}

//保存
+ (void)save:(NSString *)service data:(id)data {
    //Get search dictionary
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    //Delete old item before add new item
    SecItemDelete((CFDictionaryRef)keychainQuery);
    //Add new object to search dictionary(Attention:the data format)
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(id)kSecValueData];
    //Add item to keychain with the search dictionary
    SecItemAdd((CFDictionaryRef)keychainQuery, NULL);
}

//读
+ (id)load:(NSString *)service {
    id ret = nil;
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    //Configure the search setting
    //Since in our simple case we are expecting only a single attribute to be returned (the password) we can set the attribute kSecReturnData to kCFBooleanTrue
    [keychainQuery setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
    [keychainQuery setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    CFDataRef keyData = NULL;
    if (SecItemCopyMatching((CFDictionaryRef)keychainQuery, (CFTypeRef *)&keyData) == noErr) {
        @try {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)keyData];
        } @catch (NSException *e) {
            NSLog(@"Unarchive of %@ failed: %@", service, e);
        } @finally {
        }
    }
    if (keyData)
        CFRelease(keyData);
    return ret;
}
//删除
+ (void)delete:(NSString *)service {
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    SecItemDelete((CFDictionaryRef)keychainQuery);
}

@end
