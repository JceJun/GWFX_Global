//
//  IXCodeTF.m
//  IXApp
//
//  Created by Evn on 2018/3/13.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCodeTF.h"

static int labelTag = 100,bottomVTag = 1000;
@interface IXCodeTF()<UITextFieldDelegate>

@property(nonatomic,assign)CGFloat width;
@property(nonatomic,assign)int count;
@property(nonatomic,strong)void(^activeBlock)(BOOL isActive);

@end

@implementation IXCodeTF

+ (instancetype)instanceWidth:(CGFloat)width
                        count:(int)count
                  activeBlock:(void(^)(BOOL isActive))activeBlock
{
    IXCodeTF *codeTF = [IXCodeTF new];
    codeTF.width = width;
    codeTF.count = count;
    [codeTF initSubview];
    codeTF.activeBlock = activeBlock;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [codeTF becomeFirstResponder];
    });
    return codeTF;
}

- (void)initSubview
{
    CGFloat width = 18,space = 21;
    for (int i = 0; i < _count; i ++) {
        UILabel *lbl = [UILabel new];
        lbl.tag = labelTag + i;
        lbl.font = RO_REGU(20);
        lbl.dk_textColorPicker  = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        lbl.frame = CGRectMake(space + (width + space)*i, 0, width, 24);
        [self addSubview:lbl];
        
        
        UIView *bgV = [UIView new];
        bgV.tag = bottomVTag + i;
        bgV.dk_backgroundColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
        bgV.frame = CGRectMake(VIEW_X(lbl), 23, width, 1);
        [self addSubview:bgV];
        
    }
    self.delegate = self;
    self.tintColor = [UIColor clearColor];
    self.keyboardType = UIKeyboardTypeAlphabet;
    self.textColor = [UIColor clearColor];
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

- (void)addNumber:(NSString *)num AtIndex:(NSInteger)index
{
    UIView *bgView = [self viewWithTag:bottomVTag + index];
    UILabel *lb = [self viewWithTag:labelTag + index];
    lb.text = num;
    if (num.length) {
        bgView.hidden = YES;
        lb.hidden = NO;
        lb.backgroundColor = [UIColor clearColor];
    }else{
        lb.hidden = YES;
        bgView.hidden = NO;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //空字符
    if (string.length) {
        NSString *regex = @"^[0-9A-Za-z]";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        if (![predicate evaluateWithObject:string]) {
            return NO;
        }
    }
    if (textField.text.length >= _count && string.length) {
        return NO;
    }
    if (textField.text.length == _count-1){
        if (string.length) {
            // 进
            _activeBlock(YES);
        }
    }else if (textField.text.length == _count){
        if (!string.length) {
            // 退
            _activeBlock(NO);
        }
    }
    NSString    *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self reloadText:text];
    
    return YES;
}

 - (void)reloadText:(NSString *)text
{
    [self resetSubView];
    for (int i = 0; i < text.length; i++) {
        UILabel *lb = [self viewWithTag:labelTag + i];
        lb.text = [text substringWithRange:NSMakeRange(i, 1)];
        lb.backgroundColor = [UIColor clearColor];
        
        UIView *bgView = [self viewWithTag:bottomVTag + i];
        lb.hidden = NO;
        bgView.hidden = YES;
    }
}

- (void)resetSubView
{
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView *view = obj;
        if (view.tag >= labelTag && view.tag < bottomVTag) {
            view.hidden = YES;
        }else{
            view.hidden = NO;
        }
    }];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:))//禁止粘贴
        return NO;
    if (action == @selector(select:))// 禁止选择
        return NO;
    if (action == @selector(selectAll:))// 禁止全选
        return NO;
    return [super canPerformAction:action withSender:sender];
}


@end
