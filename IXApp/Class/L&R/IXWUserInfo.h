//
//  FXUserInfo.h
//  SDKSample
//
//  Created by Bob on 2017/4/24.
//
//

#import <Foundation/Foundation.h>

#define TIMESTAMP      @"timeStamp"
#define REFREASHTOKEN  @"refreashToken"
#define ACCESSTOKEN    @"accessToken"
#define OPENID         @"openId1234"
#define UNIONID        @"unionId1234"

#define WURL @"https://api.weixin.qq.com/sns"

@interface IXWUserInfo : NSObject

+ (void)clear;

//保存refreashToken
+ (void)saveRefreashToken:(NSString *)refreashToken;

//获取accessToken
+ (NSString *)getRefreashToken;

//保存refreashToken
+ (void)saveAccessToken:(NSString *)accessToken;

//获取accessToken
+ (NSString *)getAccessToken;

+ (void)saveOpenId:(NSString *)openId;

+ (NSString *)getOpenId;


+ (void)saveUnionId:(NSString *)unionId;

+ (NSString *)getUnionId;
@end
