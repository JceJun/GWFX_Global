//
//  IXRegisterStep3M.h
//  IXApp
//
//  Created by Bob on 2017/2/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "jsonModel.h"
@interface IXRegisterStep3M : JSONModel

//公司Id
@property (nonatomic, assign) NSInteger companyId;

//邮箱
@property (nonatomic, strong) NSString *email;

//身份证Id
@property (nonatomic, strong) NSString *idDocumentNumber;

//身份证Id对应MD5校验值
@property (nonatomic, strong) NSString *idDocumentNumberMd5;

//信息来源
@property (nonatomic, strong) NSString *informationFrom;

//自动审批
@property (nonatomic, assign) BOOL isAutoApprove;

//注册来源
@property (nonatomic, strong) NSString *openFrom;

//手机号码
@property (nonatomic, strong) NSString *mobilePhone;

//手机号前缀
@property (nonatomic, assign) NSString *mobilePhonePrefix;

//密码
@property (nonatomic, strong) NSString *passwordRaw;

//消息/模版语种
@property (nonatomic, strong) NSString *messageLang;

//国家
@property (nonatomic, strong) NSString *nationality;

// 姓名
@property(nonatomic,strong) NSString *chineseName;

// 注册来源
@property(nonatomic,strong)NSString *appMarket;

@end
