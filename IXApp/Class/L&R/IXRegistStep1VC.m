//
//  IXRegistStep1VC.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRegistStep1VC.h"
#import "IXRegistStep2VC.h"
#import "IXRegistStep3VC.h"
#import "IXEmailRegStep1VC.h"
#import "IXLoginMainVC.h"
#import "IXWChatRegVC.h"
#import "IXAboutVC.h"
#import "IXCountryListVC.h"
#import "IXAlertVC.h"

#import "IXRegistStep1CellB.h"
#import "IXRegistStep1CellC.h"
#import "IXRegisterFooterV.h"
#import "IXTouchTableV.h"
#import "FXWheellV.h"

#import <UMSocialCore/UMSocialCore.h>
#import "IXBORequestMgr+BroadSide.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Login.h"
#import "IXCpyConfig.h"
#import "NSObject+IX.h"
#import "IXUserDefaultM.h"
#import "IXADMgr.h"
#import "AppDelegate+UI.h"
#import "IXDataCacheMgr.h"

#define kHeaderHeight (187.5 + kTopMargin)

@interface IXRegistStep1VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong)IXTouchTableV  * tableV;
@property (nonatomic, strong)UIButton   * registBtn;
@property (nonatomic, strong)FXWheellV  * wheelV;//轮播图

@property (nonatomic, copy) NSString    * phoneNum;
@property (nonatomic, strong)IXCountryM    *countryInfo;
@property (nonatomic, assign)float curTFPositionY;

@property (nonatomic, strong)UIButton *closeBtn;

@end

@implementation IXRegistStep1VC

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEdit:)
                                                     name:UITextFieldTextDidBeginEditingNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    self.title = LocalizedString(@"注册用户");
    
    [self loadAd];
}

- (void)viewWillAppear:(BOOL)animated
{
    //处理选择不能国家返回刷新
    [self setCountry:[IXUserDefaultM nationalityByCode:[IXUserDefaultM getCode]]];
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.view addSubview:self.closeBtn];
}


#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        static NSString *identifier = @"IXRegistStep1CellC";
        IXRegistStep1CellC *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[IXRegistStep1CellC alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title.dk_textColorPicker = DKGrayTextColor;
        cell.desc.dk_textColorPicker = DKCellTitleColor;
        
        [cell loadUIWithDesc:[self.countryInfo localizedName]];
        return cell;
    } else {
        IXRegistStep1CellB *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
        cell.textF.delegate = self;
        [cell loadUIWithTFText:_phoneNum withDesc:[NSString stringWithFormat:@"+ %ld",(long)self.countryInfo.countryCode] withTag:indexPath.row];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self selectCountry];
    }
}

- (void)selectCountry
{
    weakself;
    IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
        [weakSelf setCountry:country];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setCountry:(IXCountryM *)country
{
    if (!self.countryInfo) {
        self.countryInfo = [[IXCountryM alloc] init];
    }
    //默认中国
    if (!country) {
        NSString *nationalCode = [IXUserDefaultM getCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityByCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        [IXUserDefaultM saveCountryCode:countryM.nationalCode];
//        if ([nationalCode isEqualToString:@"CN"]) {
//            self.countryInfo.countryCode = 86;
//        }
    } else {
        self.countryInfo.nameCN = country.nameCN;
        self.countryInfo.nameEN = country.nameEN;
        self.countryInfo.nameTW = country.nameTW;
        self.countryInfo.countryCode = country.countryCode;
        [IXUserDefaultM saveCountryCode:country.nationalCode];
        [self.tableV reloadData];
        _tableV.tableFooterView = [self tableFooterV];
//        if (self.countryInfo.countryCode == 86) {
//            [self.tableV reloadData];
//            _tableV.tableFooterView = [self tableFooterV];
//        } else {
//            [self.tableV reloadData];
//            _tableV.tableFooterView = [self tableFooterV];
//        }
        
    }
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (aimStr.length >= 5) {
        [self registerBtnEnable:YES];
        _phoneNum = aimStr;
    }else{
        [self registerBtnEnable:NO];
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self registerBtnEnable:NO];
    return YES;
}

- (void)registerBtnEnable:(BOOL)enable
{
    if (enable) {
        _registBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        _registBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

- (void)keyboardHidden:(NSNotification *)notify
{
    CGRect  frame = _tableV.frame;
    frame.origin.y = 0;
    _tableV.frame = frame;
}

- (void)didEdit:(NSNotification *)notification
{
    UIView      * contentTF = notification.object;
    UIWindow    * window = [[[UIApplication sharedApplication] delegate] window];
    CGRect      rect = [contentTF convertRect: contentTF.bounds toView:window];
    _curTFPositionY = rect.origin.y + contentTF.superview.frame.size.height;
}

- (void)keyboardShow:(NSNotification *)notification
{
    NSDictionary    * userInfo = [notification userInfo];
    CGRect  keyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    CGRect  frame = _tableV.frame;
    if (_curTFPositionY + keyboardFrame.size.height > kScreenHeight) {
        frame.origin.y = frame.origin.y - (_curTFPositionY + keyboardFrame.size.height - kScreenHeight);
        [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _tableV.frame = frame;
        } completion:nil];
    }
}

#pragma mark 加载广告数据
- (void)loadAd
{
    NSDictionary *paramDic = @{
                               @"pageNo":@(1),
                               @"pageSize":@(20),
                               @"infomationType":@"REGISTER_ADVERTISEMENT"
                               };
    
    weakself;
    [IXBORequestMgr bs_bannerAdvertisementListWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
    {
        if (success && [obj ix_isDictionary] && obj[@"resultList"]) {
            [IXADMgr shareInstance].registAdArr = [obj[@"resultList"] mutableCopy];
            [IXADMgr shareInstance].registBannerClose = [obj[@"bannerClose"] boolValue];
            if ([IXADMgr shareInstance].registAdArr.count) {
                [weakSelf.view addSubview:weakSelf.wheelV];
            }
        }
    }];
}


#pragma mark -
#pragma mark - create UI


- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[IXRegistStep1CellB class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
        [_tableV registerClass:[IXRegistStep1CellC class] forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellC class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kHeaderHeight)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 100)/2, (kHeaderHeight - 115.5)/2, 100, 115.5)];
    [logo dk_setImagePicker:DKImageNames(@"common_logo", @"common_logo_D")];
    float height = 116*kScreenWidth/375;
    float width = height;
    logo.frame = CGRectMake((kScreenWidth - width)/2, (kHeaderHeight - height)/2, width, height);
    [v addSubview:logo];
    
    return v;
}

- (UIView *)tableFooterV
{
    CGRect  rect = CGRectMake(0, 0, kScreenWidth, kScreenHeight - kHeaderHeight - 44*2);
    
    IXRegisterFooterV   * v = [[IXRegisterFooterV alloc] initWithFrame:rect
                                                           topBtnTitle:LocalizedString(@"注 册")
                                                          leftBtnTitle:LocalizedString(@"关于我们")
                                                        centerBtnTtile:LocalizedString(@"登录账号")
                                                         rightBtnTitle:LocalizedString(@"邮箱注册")];
    
    weakself;
    v.btnBlock = ^(NSString *title) {
        [weakSelf dealWithFooterBtn:title];
    };
    
    return v;
}

- (UIButton *)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeBtn.frame = CGRectMake(kScreenWidth - (40 + 15), 20, 40, 40);
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_closeBtn setImage:GET_IMAGE_NAME(@"ad_close") forState:UIControlStateNormal];
        _closeBtn.titleLabel.font = PF_REGU(15);
    }
    return _closeBtn;
}

- (void)closeBtnAction
{
    [IXDataCacheMgr clearAccountInfo];
    [IXUserInfoMgr shareInstance].isCloseDemo = NO;
    [AppDelegate showLogin];
}

- (void)dealWithFooterBtn:(NSString *)title
{
    if (SameString(title, LocalizedString(@"注 册"))) {
        [self.view endEditing:YES];
        IXRegistStep1CellB *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
        //手机号合法位数最少为5
        if ( cell.textF.text.length < PHONEMINLENGTH ) {
            [SVProgressHUD showErrorWithStatus:PHONEMINCHAR];
            return;
        }
        
        [self validPhone:cell.textF.text];
    }
    else if (SameString(title, LocalizedString(@"关于我们"))) {
        [self.navigationController pushViewController:[IXAboutVC new] animated:YES];
    }
    else if (SameString(title, LocalizedString(@"登录账号"))) {
        IXLoginMainVC   * vc = nil;
        for (UIViewController * v in self.navigationController.viewControllers) {
            if ([v isKindOfClass:[IXLoginMainVC class]]) {
               [IXUserInfoMgr shareInstance].isCloseDemo = YES;
                vc = (IXLoginMainVC *)v;
                break;
            }
        }
        
        if (vc) {
            [self.navigationController popToViewController:vc animated:NO];
        } else {
            vc = [IXLoginMainVC new];
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
    else if (SameString(title, LocalizedString(@"邮箱注册"))) {
        [self.navigationController pushViewController:[IXEmailRegStep1VC new] animated:NO];
    }
}

#pragma mark -
#pragma mark - request

- (void)validPhone:(NSString *)phoneNum
{
    IXEmailPhoneModel *model = [[IXEmailPhoneModel alloc] init];
    model.index = 1;
    model.mobilePhone = phoneNum;
    model.mobilePhonePrefix = [NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode];
    
    [SVProgressHUD showWithStatus:LocalizedString(@"校验手机号...")];
    
    weakself;
    [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model complete:^(BOOL exist, NSString *errStr) {
        if (exist) {
             [SVProgressHUD showErrorWithStatus:errStr];
        }else{
            [SVProgressHUD dismiss];
            
            BOOL k = VerifyCodeEnable;
            if (k) {
                [weakSelf popToValidCode];
            }else{
                [weakSelf popToStep3VC];
            }
        }
    }];
}

- (void)popToValidCode
{
    IXRegistStep1CellB *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
  
    IXRegistStep2VC *vc = [[IXRegistStep2VC alloc] init];
    vc.phoneNum = cell.textF.text;
    vc.phoneId = [NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)popToStep3VC {
    
    IXRegistStep1CellB *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    IXRegistStep3VC *vc = [[IXRegistStep3VC alloc] init];
    vc.phoneNum = cell.textF.text;
    vc.phoneId = [NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode];
    vc.token = @"";
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark -
#pragma mark - lazy laoding

- (FXWheellV *)wheelV
{
    if (!_wheelV) {
        _wheelV = [[FXWheellV alloc] initWithFrame:CGRectMake(0,
                                                              kScreenHeight
                                                              - 94,
                                                              kScreenWidth,
                                                              94)];
    }
    _wheelV.items = [IXADMgr shareInstance].registAdArr;
    _wheelV.bannerClose = [IXADMgr shareInstance].registBannerClose;
    return _wheelV;
}


- (void)closeAd
{
    if (_wheelV) {
        [_wheelV removeFromSuperview];
        _wheelV = nil;
    }
}

@end
