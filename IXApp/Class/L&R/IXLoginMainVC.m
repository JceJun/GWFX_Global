
//
//  IXLoginMainVC.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXLoginMainVC.h"
#import "IXEmailRegStep1VC.h"
#import "IXDeviceVerifyVC.h"
#import "IXFindPwdStep1VC.h"
#import "IXRegistStep1VC.h"
#import "IXLoginAboutVC.h"
#import "IXWChatRegVC.h"

#import "IXOpenChooseContentView.h"
#import "IXLoginMainCellA.h"
#import "IXLoginMainCell.h"
#import "IXLoginFooterV.h"
#import "IXTouchTableV.h"
#import "FXWheellV.h"
#import "IXCountryListVC.h"
#import "AppDelegate+UI.h"


#import <UMMobClick/MobClick.h>
#import "IXBORequestMgr+BroadSide.h"
#import "IXBORequestMgr+Login.h"
#import "NSObject+IX.h"
#import "SFHFKeychainUtils.h"
#import "IXCpyConfig.h"
#import "IXDataBase.h"
#import "IXWUserInfo.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"
#import "IXADMgr.h"
#import "IXPhoneRegistVC.h"

#define kHeaderHeight (187.5 + kTopMargin)

@interface IXLoginMainVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>


@property (nonatomic, strong)IXTouchTableV  * tableV;
@property (nonatomic, strong)FXWheellV      * wheelV;//轮播图
@property (nonatomic, strong)UIImageView    * logoImgV;
@property (nonatomic, strong)UIActivityIndicatorView    *idctr;

@property (nonatomic, strong)IXCountryM    *countryInfo;

@end

@implementation IXLoginMainVC

- (id)init
{
    self = [super init];
    if(self) {
        IXTradeData_listen_regist(self, PB_CMD_USERLOGIN_INFO);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_USERLOGIN_INFO);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    if (UMAppKey.length) {
        [MobClick event:UMEnterLoginPage];  //友盟统计
    }
   
    self.view.dk_backgroundColorPicker = DKTableColor;
    _userName = [[NSString alloc] init];
    _passWord = [[NSString alloc] init];

    [self setAutoLoginData];
    if (_isAutoLogin) {
        NSString *countryCode = [[NSUserDefaults standardUserDefaults] objectForKey:kIXPhoneCode];
        if (countryCode.length) {
            //有保存手机区号
            [self setCountry:[IXUserDefaultM nationalityByCountryCode:countryCode]];
        } else {
            [self setCountry:[IXUserDefaultM nationalityByCode:[IXUserDefaultM getCode]]];
        }
        BOOL    login = NO;
        if (_userName.length && _passWord.length){
            [self loginAction];
            login = YES;
        }
        
        if (login) {
            _logoImgV = [[UIImageView alloc] initWithFrame:kScreenBound];
            [_logoImgV setImage:[IXDataProcessTools getLaunchImage]];
            [self.view addSubview:_logoImgV];
            [self.view addSubview:self.idctr];
            [self.idctr startAnimating];
        } else {
            _isAutoLogin = NO;
            [self addLoginView];
            [self loadAd];
        }
    } else {
        [self setCountry:[IXUserDefaultM nationalityByCode:[IXUserDefaultM getCode]]];
        [self addLoginView];
        [self loadAd];
    }
}

- (void)addLoginView
{
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if(IXTradeData_isSameKey(keyPath, PB_CMD_USERLOGIN_INFO)) {
        proto_user_login_info *info = ((IXTradeDataCache *)object).pb_user_login_info;
        if (info.result==0) {
            if (!_isAutoLogin) {
                [self saveUserInfo];
            }
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(reloadUI) object:nil];
        } else if (info.result == RET_USER_LOGIN_UNSECURE_DEV) {
            [SVProgressHUD dismiss];
            IXDeviceVerifyVC *VC = [[IXDeviceVerifyVC alloc] init];
            VC.userName = _userName;
            VC.passWord = _passWord;
            VC.phoneId = [NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode];
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            [self reloadUI];
        }
    }
}

- (void)setAutoLoginData
{
    _userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    if (_userName && _userName.length > 0) {
        _passWord = [SFHFKeychainUtils getPasswordForUsername:_userName andServiceName:kServiceName error:nil];
    }
    if ( (_userName.length > 0 && _passWord.length > 0) ||
        [IXWUserInfo getRefreashToken] ) {
        _isAutoLogin = YES;
    } else {
        if (![IXUserInfoMgr shareInstance].isCloseDemo) {
            //demo账号登录
            _userName = [[IXUserInfoMgr shareInstance] demoLoginAccount];
            _passWord = [[IXUserInfoMgr shareInstance] demoLoginPassword];
            _isAutoLogin = YES;
        }
    }
}

//保存用户信息
- (void)saveUserInfo
{
    //自动登录没必要保存用户名密码
    if (!_isAutoLogin) {
        if (![[IXUserInfoMgr shareInstance] isDemeLogin]) {
            if ( [_userName length] && [_passWord length] ) {
                [[NSUserDefaults standardUserDefaults] setObject:_userName forKey:kIXLoginName];
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode] forKey:kIXPhoneCode];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSError *error;
                [SFHFKeychainUtils storeUsername:_userName
                                     andPassword:_passWord
                                  forServiceName:kServiceName
                                  updateExisting:YES
                                           error:&error];
                if (!error) {
                    DLog(@"用户保存成功");
                } else {
                    ELog([NSString stringWithFormat:@"用户保存失败:%@",error.localizedDescription]);
                }
            }
        }
    }else{
        DLog(@"当前页不是登录页，不调用保存用户名");
    }
}

- (void)reloadUI
{
    if (SameString(_userName, [[IXUserInfoMgr shareInstance] demoLoginAccount])) {
        _userName = @"";
        _passWord = @"";
        [IXUserInfoMgr shareInstance].isCloseDemo = YES;
        [AppDelegate showLogin];
        return;
    }
    if (_logoImgV) {
        [_logoImgV removeFromSuperview];
        _logoImgV = nil;
    }
    if (_idctr) {
        [_idctr stopAnimating];
        [_idctr removeFromSuperview];
        _idctr = nil;
    }
    _isAutoLogin = NO;
    [self deleteLoginInfo];
    [self addLoginView];
    
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD showInfoWithStatus:@"Login Time Out"];
    }
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:{
            IXLoginMainCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title.dk_textColorPicker = DKGrayTextColor;
            cell.desc.dk_textColorPicker = DKCellTitleColor;
            [cell loadUIWithName:[self.countryInfo localizedName] countryCode:[NSString stringWithFormat:@" +%ld",(long)self.countryInfo.countryCode]];
            return cell;
        }
            break;
        case 1:{
            IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell loadUIWithDesc:LocalizedString(@"账号")
                            text:_userName
                     placeHolder:LocalizedString(@"请输入手机号码或邮箱")
                    keyboardType:UIKeyboardTypeEmailAddress
                 secureTextEntry:NO];
            cell.textF.textFont = RO_REGU(13);
            cell.textF.placeHolderFont = PF_MEDI(13);
            return cell;
        }
            break;
        default:{
            IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell loadUIWithDesc:LocalizedString(@"密码")
                            text:_passWord
                     placeHolder:LocalizedString(@"请填写密码")
                    keyboardType:UIKeyboardTypeDefault
                 secureTextEntry:YES];
            cell.textF.textFont = RO_REGU(13);
            cell.textF.placeHolderFont = PF_MEDI(13);
            cell.textF.delegate = self;
            cell.textF.returnKeyType = UIReturnKeyDone;
            return cell;
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self selectCountry];
    }
}

- (void)selectCountry
{
    weakself;
    IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
        [weakSelf setCountry:country];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setCountry:(IXCountryM *)country
{
    if (!self.countryInfo) {
        self.countryInfo = [[IXCountryM alloc] init];
    }
    //默认中国
    if (!country) {
        NSString *nationalCode = [IXUserDefaultM getCountryCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityModelByNationalCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        [IXUserDefaultM saveCountryCode:countryM.nationalCode];
    } else {
        self.countryInfo.nameCN = country.nameCN;
        self.countryInfo.nameEN = country.nameEN;
        self.countryInfo.nameTW = country.nameTW;
        self.countryInfo.countryCode = country.countryCode;
        [IXUserDefaultM saveCountryCode:country.nationalCode];
        [self.tableV reloadData];
        _tableV.tableFooterView = [self tableFooterV];
    }
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [self loginAction];
        return NO;
    }
    
    return YES;
}

- (FXWheellV *)wheelV
{
    if (!_wheelV) {
        _wheelV = [[FXWheellV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kHeaderHeight)];
    }
    _wheelV.items = [IXADMgr shareInstance].loginAdArr;
    _wheelV.bannerClose = [IXADMgr shareInstance].loginBannerClose;
    return _wheelV;
}


#pragma mark -
#pragma mark - create UI

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 20, kScreenWidth, kScreenHeight-20) style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableHeaderColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXLoginMainCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        [_tableV registerClass:[IXLoginMainCell class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCell class])];
    }
    return _tableV;
}

- (UIActivityIndicatorView *)idctr
{
    if (!_idctr) {
        _idctr = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGPoint center = self.view.center;
        center.y = center.y + 50;
        _idctr.center = center;
        _idctr.hidesWhenStopped = YES;
        [self.view addSubview:_idctr];
    }
    return _idctr;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kHeaderHeight)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 100)/2, (kHeaderHeight - 115.5)/2, 100, 115.5)];
    [logo dk_setImagePicker:DKImageNames(@"common_logo", @"common_logo_D")];
    float height = 116*kScreenWidth/375;
    float width = height;
    logo.frame = CGRectMake((kScreenWidth - width)/2, (kHeaderHeight - height)/2, width, height);
    [v addSubview:logo];
    
    return v;
}

- (UIView *)tableFooterV
{
    IXLoginFooterV  * footerV = [[IXLoginFooterV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth , kScreenHeight - kHeaderHeight - 44*2)];
    
    weakself;
    footerV.actionBlock = ^(LoginFooterActionType type){
        switch (type) {
            case LoginFooterActionTypeLogin:{
                [weakSelf loginAction];
                break;
            }
            case LoginFooterActionTypeForgetPwd:{
                [weakSelf findPsdAction];
                break;
            }
            case LoginFooterActionTypeRegist:{
                [weakSelf registAction];
                break;
            }
            case LoginFooterActionTypeContactUs:{
                [weakSelf aboutAction];
                break;
            }
            default:
                break;
        }
    };
    
    return footerV;
}

#pragma mark 加载广告数据
- (void)loadAd
{
    NSDictionary *paramDic = @{
                               @"pageNo":@(1),
                               @"pageSize":@(20),
                               @"infomationType":@"BANNER_ADVERTISEMENT"
                               };
    
    weakself;
    [IXBORequestMgr bs_bannerAdvertisementListWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
     {
         if (success && [obj  ix_isDictionary] && obj[@"resultList"]) {
             [IXADMgr shareInstance].loginAdArr = [obj[@"resultList"] mutableCopy];
             [IXADMgr shareInstance].loginBannerClose = [obj[@"bannerClose"] boolValue];
             if ([IXADMgr shareInstance].loginAdArr.count) {
                 [weakSelf.view addSubview:self.wheelV];
             }
         }
     }];
}

#pragma mark -
#pragma mark - btn action

- (void)findPsdAction
{
    NSArray * arr = @[
                      LocalizedString(@"通过手机找回"),
                      LocalizedString(@"通过邮箱找回")
                      ];
    
    weakself;
    IXOpenChooseContentView * chooseV = [[IXOpenChooseContentView alloc] initWithDataSource:arr
                                                                            WithSelectedRow:^(NSInteger row)
    {
        if (row == 0){
            [weakSelf findPwdByPhone];
        }else{
            [weakSelf findPwdByEmail];
        }
    }];
    [chooseV show];
}

- (void)registAction{
    IXPhoneRegistVC *vc = nil;
    for (UIViewController * v in self.navigationController.viewControllers) {
        if ([v isKindOfClass:[IXPhoneRegistVC class]]) {
            vc = (IXPhoneRegistVC *)v;
        }
    }
    
    
    if (vc) {
        [self.navigationController popToViewController:vc animated:NO];
    } else {
        [self.navigationController pushViewController:[IXPhoneRegistVC new] animated:NO];
    }
}

- (void)aboutAction{
    IXLoginAboutVC *about = [[IXLoginAboutVC alloc] init];
    [self.navigationController pushViewController:about animated:YES];
}

- (void)loginAction
{
    [self.view endEditing:YES];
    IXLoginMainCellA *cellName = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    IXLoginMainCellA *cellPwd  = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    
    if (!_isAutoLogin) {
        _userName = cellName.textF.text;
        _passWord = cellPwd.textF.text;
        if (SameString(_userName, [[IXUserInfoMgr shareInstance] demoLoginAccount])) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"用户名错误")];
            return;
        }
    }
    
    if (_userName.length == 0) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"手机号／邮箱不能为空")];
        return;
    }
    
    if (_passWord.length == 0) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入密码")];
        return;
    }

    if (_userName.length < 5) {
        [SVProgressHUD showErrorWithStatus:PHONEMINCHAR];
        return;
    }

    
    if (!_isAutoLogin) {
        [SVProgressHUD showWithStatus:LocalizedString(@"正在登录...")];
    }
    
    [self performSelector:@selector(reloadUI) withObject:nil afterDelay:10];
    
    [IXBORequestMgr lg_loginWithAccount:_userName pwd:_passWord phoneCode:[NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode]];
}


- (void)findPwdByPhone
{
    NSIndexPath * idxPath = [NSIndexPath indexPathForRow:1 inSection:0];
    IXLoginMainCellA    * cell = (IXLoginMainCellA *)[self.tableV cellForRowAtIndexPath:idxPath];
    
    IXFindPwdStep1VC   * vc = [[IXFindPwdStep1VC alloc] init];
    if (![_userName containsString:@"@"]) {
        //不是邮箱
        vc.userName = cell.textF.text;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)findPwdByEmail
{
    NSIndexPath * idxPath = [NSIndexPath indexPathForRow:1 inSection:0];
    IXLoginMainCellA    * cell = (IXLoginMainCellA *)[self.tableV cellForRowAtIndexPath:idxPath];
    
    IXFindPwdStep1VC   * vc = [[IXFindPwdStep1VC alloc] init];
    vc.type = pageTypeEmailFind;
    if ([_userName containsString:@"@"]) {
        //不是邮箱
        vc.userName = cell.textF.text;
    }
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark -
#pragma mark - other

- (void)deleteLoginInfo
{
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    [SFHFKeychainUtils deleteItemForUsername:userName andServiceName:kServiceName error:nil];
    
    if (_userName && _userName.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:_userName forKey:kIXLoginName];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

@end
