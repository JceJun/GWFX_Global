//
//  IXEmailRegStep3VC.h
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXEmailRegStep3VC : IXDataBaseVC

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *fName;
@property (nonatomic, copy) NSString *lName;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *phoneId;

- (void)loginAfterRegister;

@end
