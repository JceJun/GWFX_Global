//
//  IXEmailRegStep3VC.m
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXEmailRegStep3VC.h"
#import "IXTouchTableV.h"
#import "IXLoginMainCellA.h"
#import "IXEmailRegStep3CellA.h"
#import "IXRegistStep4VC.h"
#import "IXTraceWebV.h"

#import "IXBORequestMgr+Account.h"
#import "SFHFKeychainUtils.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"
#import "AppDelegate.h"


@interface IXEmailRegStep3VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV * tableV;
@property (nonatomic, strong) UITextField   * pwdTf;
@property (nonatomic, strong) UITextField   * confirmPwdTf;
@property (nonatomic, strong) IXTraceWebV   * traceWebV;
@property (nonatomic, assign) BOOL sendReg;

@end

@implementation IXEmailRegStep3VC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self
                                      sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"注册用户");
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    self.view.dk_backgroundColorPicker = DKTableColor;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

- (UIView *)tableHeaderV
{
    UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel * lab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 64-18-5, kScreenWidth, 18)];
    lab1.text = LocalizedString(@"请设置账户密码");
    lab1.font = PF_MEDI(15);
    lab1.numberOfLines = 2;
    lab1.dk_textColorPicker = DKCellTitleColor;
    lab1.textAlignment = NSTextAlignmentCenter;
    [v addSubview:lab1];
    
    UILabel * lab2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 64+5, kScreenWidth, 18)];
    lab2.text = LocalizedString(@"之后你可以使用邮箱登录");
    lab2.font = PF_MEDI(15);
    lab2.dk_textColorPicker = DKCellTitleColor;
    lab2.textAlignment = NSTextAlignmentCenter;
    [v addSubview:lab2];
    
    return v;
}

- (UIView *)tableFooterV
{
    CGFloat height = kScreenHeight - 132 - 44*2 - kNavbarHeight;
    UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, height)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage     * image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                       topCapHeight:image.size.height/2];
    
    UIButton    * cmpltBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cmpltBtn.tag = 100;
    cmpltBtn.enabled = NO;
    cmpltBtn.titleLabel.font = PF_REGU(15);
    cmpltBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    [cmpltBtn setTitle:LocalizedString(@"完成注册") forState:UIControlStateNormal];
    [cmpltBtn addTarget:self action:@selector(cmpltBtnClk)
       forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:cmpltBtn];
   
    return v;
}


#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)cmpltBtnClk
{
    [self.view endEditing:YES];
#warning
    if(!_sendReg){
        _sendReg = YES;
        
        [self regisOutter];
    }
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:{
            NSString    * ident = NSStringFromClass([IXEmailRegStep3CellA class]);
            IXEmailRegStep3CellA    * cell = [tableView dequeueReusableCellWithIdentifier:ident];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell reloadUIWithEmail:self.email];
        
            return cell;
        }
        case 1:{
            NSString    * ident = NSStringFromClass([IXLoginMainCellA class]);
            IXLoginMainCellA    * cell = [tableView dequeueReusableCellWithIdentifier:ident];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textF.delegate = self;
            _pwdTf = cell.textF;
            [cell.textF becomeFirstResponder];
            [cell loadUIWithDesc:LocalizedString(@"密码")
                            text:@""
                     placeHolder:LocalizedString(@"6至18位字母或数字")
                    keyboardType:UIKeyboardTypeDefault
                 secureTextEntry:YES];
            cell.textF.textFont = RO_REGU(15);
            cell.textF.placeHolderFont = PF_MEDI(13);
            return cell;
        }
        case 2:{
            NSString    * ident = NSStringFromClass([IXLoginMainCellA class]);
            IXLoginMainCellA    * cell = [tableView dequeueReusableCellWithIdentifier:ident];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textF.delegate = self;
            _confirmPwdTf = cell.textF;
            [cell loadUIWithDesc:LocalizedString(@"确认密码")
                            text:@""
                 placeHolder:LocalizedString(@"请再次输入")
                    keyboardType:UIKeyboardTypeDefault
                 secureTextEntry:YES];
            cell.textF.textFont = RO_REGU(15);
            cell.textF.placeHolderFont = PF_MEDI(13);
            return cell;
        }
    }
    
    return nil;
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (aimStr.length &&
        ((textField == _pwdTf && aimStr.length == _confirmPwdTf.text.length) ||
         (textField == _confirmPwdTf && aimStr.length == _pwdTf.text.length))) {
         [self rgisterBtnEnable:YES];
    }else{
         [self rgisterBtnEnable:NO];
    }
    if (aimStr.length > 18) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self rgisterBtnEnable:NO];
    return YES;
}

- (void)rgisterBtnEnable:(BOOL)enable
{
    UIButton *btn = (UIButton *)[_tableV.tableFooterView viewWithTag:100];
    btn.enabled = enable;
    
    UIImage     * image = GET_IMAGE_NAME(@"regist_btn_unable");
    if (enable) {
        image = AutoNightImageNamed(@"regist_btn_enable");
    }
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                       topCapHeight:image.size.height/2];
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    
}


#pragma mark -
#pragma mark - other

- (BOOL)checkSettingPwd
{
    NSString *errTip = @"";
    //两次密码一致
    if ([_pwdTf.text isEqualToString:_confirmPwdTf.text]) {
        //密码长度6-18位
        if (_pwdTf.text.length >= 6 && _pwdTf.text.length <= 18) {
            //只有数字或字母
            if ([IXEntityFormatter validPasswd:_pwdTf.text]) {
                return YES;
            }
        }
        errTip = PWDVALIDCHAR;
        
    }else{
        errTip = PWDDIFF;
    }
    _sendReg = NO;
    [SVProgressHUD showErrorWithStatus:errTip];
    return NO;
}

- (void)regisOutter
{
    if ([self checkSettingPwd]) {
        [SVProgressHUD showWithStatus:LocalizedString(@"正在注册")];
        
        //嵌套webview地址
        if (!_traceWebV) {
            _traceWebV = [IXTraceWebV initWithWebUrlType:TraceWebUrlTypeTouchRegist];
            [self.view addSubview:_traceWebV];
        } else {
            [_traceWebV refreshWebUrlType:TraceWebUrlTypeTouchRegist];
        }
        
        IXRegisterStep3M    * model = [[IXRegisterStep3M alloc] init];
        model.companyId = CompanyID;
        model.email = self.email;
        model.isAutoApprove = YES;
        model.passwordRaw = [IXDataProcessTools md5StringByString:_pwdTf.text];
        model.nationality = [IXUserDefaultM getCode];
        model.chineseName = [_fName stringByAppendingFormat:@" %@",_lName];
        weakself;
        [IXBORequestMgr acc_registWith:model token:_token
                              complete:^(BOOL registerSucess, NSString *errStr)
        {
            if (registerSucess) {
                //注册成功
                [weakSelf saveUserInfo];
                
//                IXRegistStep4VC *step4 = [[IXRegistStep4VC alloc] init];
//                step4.parentVC = weakSelf;
//                [weakSelf.navigationController pushViewController:step4 animated:YES];
                [SVProgressHUD dismiss];
                //嵌套webview地址
                if (!weakSelf.traceWebV) {
                    weakSelf.traceWebV = [IXTraceWebV initWithWebUrlType:TraceWebUrlTypeRegistSuccess];
                    [weakSelf.view addSubview:weakSelf.traceWebV];
                } else {
                    [weakSelf.traceWebV refreshWebUrlType:TraceWebUrlTypeRegistSuccess];
                }
                [weakSelf loginAfterRegister];
            }else{
                [SVProgressHUD showErrorWithStatus:errStr];
            }
        }];
    }
}

- (void)loginAfterRegister
{
    [SVProgressHUD showWithStatus:LocalizedString(@"加载中...")];
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    IXLoginMainCellA    * cell  = [_tableV cellForRowAtIndexPath:indexPath];

    NSString    * version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];

    proto_user_login    * proto = [[proto_user_login alloc] init];
    [proto setEmail:self.email];
    [proto setPwd:[IXDataProcessTools md5StringByString:cell.textF.text]];
    [proto setLogintime:[IXEntityFormatter getCurrentTimeInterval]];
    [proto setLoginType:proto_user_login_elogintype_ByEmail];
    [proto setType:proto_user_login_elogintype_ByEmail];
    [proto setCompanyToken:CompanyToken];
    [proto setCompanyid:CompanyID];
    [proto setSessionType:[IXUserInfoMgr shareInstance].itemType];
    [proto setVersion:[version intValue]];
    [proto setSecureDev:[IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]]];
    [IXUserInfoMgr shareInstance].loginType = proto_user_login_elogintype_ByEmail;
    [IXUserInfoMgr shareInstance].loginName = self.email;
    [[IXTCPRequest shareInstance] loginWithParam:proto];
    
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.loginAferRegist = YES;
}

- (void)saveUserInfo
{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    IXLoginMainCellA    * cell = [_tableV cellForRowAtIndexPath:indexPath];
    
    NSError *error = nil;
    [SFHFKeychainUtils storeUsername:self.email
                         andPassword:cell.textF.text
                      forServiceName:kServiceName
                      updateExisting:YES error:&error];
    if (!error) {
        DLog(@"账户保存成功");
        [[NSUserDefaults standardUserDefaults] setObject:self.email forKey:kIXLoginName];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        ELog(@"账户保存失败");
    }
    return;
}

#pragma mark -
#pragma mark - lazy loading

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[IXLoginMainCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        [_tableV registerClass:[IXEmailRegStep3CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXEmailRegStep3CellA class])];
    }
    return _tableV;
}

@end
