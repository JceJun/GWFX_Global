//
//  IXLoginMainCellA.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXLoginMainCellA.h"

@interface IXLoginMainCellA ()<UITextFieldDelegate>

@property (nonatomic, strong)UILabel *desc;

@end

@implementation IXLoginMainCellA

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 10, 87.5-20, 24)];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKGrayTextColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (IXTextField *)textF
{
    if (!_textF) {
        _textF = [[IXTextField alloc] init];
        _textF.frame = CGRectMake(110-20, 8, kScreenWidth-102-10+30, 30);
        _textF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textF.dk_textColorPicker = DKCellTitleColor;
        _textF.tintColor = MarketSymbolNameColor;
        _textF.font = RO_REGU(13);
        _textF.delegate = self;
        [self.contentView addSubview:_textF];
    }
    return _textF;
}

- (void)loadUIWithDesc:(NSString *)desc
                  text:(NSString *)title
           placeHolder:(NSString *)holder
          keyboardType:(UIKeyboardType)type
       secureTextEntry:(BOOL)Entry
{
    self.desc.text = desc;
    self.textF.text = title;
    self.textF.placeholder = holder;
    //    UIColor * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
    //    self.textF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:holder attributes:@{NSForegroundColorAttributeName:phColor}];
    self.textF.keyboardType = type;
    self.textF.secureTextEntry = Entry;
    
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (void)setTextFieldTag:(NSUInteger)tag {
    
    self.textF.tag = tag;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textValue:tag:)]) {
        
        [self.delegate textValue:textField.text tag:textField.tag];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //不接受输入空格
    if( [string isEqualToString:@" "] ){
        if ( self.delegate && [self.delegate respondsToSelector:@selector(errMsg:)] ) {
            [self.delegate errMsg:NONESPACECHAR];
        }
        return NO;
    }
    
    //如果是密码输入项，则最多只能输入18位字符
    if ( textField.secureTextEntry ) {
        if ( range.location > PWDMAXLENGTH ) {
            if ( self.delegate && [self.delegate respondsToSelector:@selector(errMsg:)] ) {
                [self.delegate errMsg:PWDMAXCHAR];
            }
            return NO;
        }
    }
    
    return YES;
}
@end
