//
//  IXCheckCodeV.h
//  IXApp
//
//  Created by Evn on 2018/3/13.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^checkCodeBlock)(NSString *code);
typedef void (^refreshCodeBlock)(void);
typedef void (^dismissCodeView)(void);
@interface IXCheckCodeV : UIView

@property (nonatomic, strong)checkCodeBlock checkB;
@property (nonatomic, strong)refreshCodeBlock refreshB;
@property (nonatomic, strong)dismissCodeView dismissB;

- (id)initWithCodeImageUrl:(NSString *)imgUrl;
- (void)refreshImageUrl:(NSString *)imgUrl;
- (void)show;


@end
