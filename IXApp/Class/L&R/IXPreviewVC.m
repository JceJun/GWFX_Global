//
//  IXPreviewVC.m
//  IXApp
//
//  Created by Magee on 2016/12/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXPreviewVC.h"
#import "AppDelegate+UI.h"
#import "IXCpyConfig.h"
#import "IXSysConfig.h"
#import "IXUserDefaultM.h"
#import "UIViewExt.h"
#import "IXAppUtil.h"
@interface IXPreviewVC ()<UIScrollViewDelegate>
{
    UIScrollView *scrollView_;
    UIPageControl *page;
    UIView *alphatView_;//透明层
}
@end

@implementation IXPreviewVC

-(void)loadView{
    
    [super loadView];
    scrollView_ = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    BOOL flag = NO;
    NSString *imgName = nil;
    for(NSInteger i = 0; i < GuidePageNumber; i++){
        UIView *view;
        NSString *btnTitle;
        if (i == (GuidePageNumber - 4) || i == (GuidePageNumber - 3) || i == (GuidePageNumber - 2)) {
            flag = NO;
        } else {
            flag = NO;
        }
        if(i == (GuidePageNumber - 1)) {
            if ([CompanyToken isEqualToString:@"MCtrader"]) {
                btnTitle = LocalizedString(@"Learn More");
            } else {
                btnTitle = LocalizedString(@"立即体验");
            }
        }else{
            btnTitle = @"Continue";
        }
        if (i == GuidePageNumber) {
            view  = [self creatViewWithFrame:CGRectMake(i * self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height) WithImage:@"LaunchImage" WithBtn:btnTitle isHidden:flag];
        } else {
            if (Is_iPhoneX) {
                imgName = @"ad4x_00";
            } else {
                imgName = @"ad4_00";
            }
            view  = [self creatViewWithFrame:CGRectMake(i * self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height) WithImage:[NSString stringWithFormat:@"%@%ld",imgName,(long)i+1] WithBtn:btnTitle isHidden:flag];
        }
        [scrollView_ addSubview:view];
    }
    
    scrollView_.contentSize = CGSizeMake(self.view.frame.size.width * GuidePageNumber, 0);
    scrollView_.pagingEnabled = YES;
    scrollView_.showsHorizontalScrollIndicator = NO;
    scrollView_.bounces = NO;
    scrollView_.delegate =self;
    [self.view addSubview:scrollView_];
    
    CGRect  frame = CGRectMake(0, self.view.frame.size.height - 80, self.view.frame.size.width, 20);
    if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
        frame.origin.y -= 34;
    }
    
    page = [[UIPageControl alloc]initWithFrame:frame];
    page.numberOfPages = GuidePageNumber;
    page.currentPage = 0;
    if ([CompanyToken isEqualToString:@"firfx"]) {
        page.pageIndicatorTintColor = CurrentPageIndicatorTintColor;
        page.currentPageIndicatorTintColor = PageIndicatorTintColor;
    } else {
        page.pageIndicatorTintColor = PageIndicatorTintColor;
        page.currentPageIndicatorTintColor = CurrentPageIndicatorTintColor;
    }
    [page addTarget:self action:@selector(valuechange) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:page];
}

-(void)handleClick:(id) handle
{    
    alphatView_.hidden = YES;
}

-(void)btnclick
{
    if (page.currentPage == GuidePageNumber -1) {
        [IXUserDefaultM setNightMode:DayNightMode];
        [[NSUserDefaults standardUserDefaults] setObject:[IXAppUtil appVersion] forKey:kLoadStartFlag];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [AppDelegate showLogin];
    }else{
        page.currentPage ++;
        [self valuechange];
    }
}

- (UIView *)creatViewWithFrame:(CGRect)rect WithImage:(NSString *)image WithBtn:(NSString *)btnTitle isHidden:(BOOL)isHidden {
    UIView *view = [[UIView alloc]initWithFrame:rect];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    imageView.image = [UIImage imageNamed:image];
    imageView.userInteractionEnabled = YES;
    [view addSubview:imageView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, kScreenHeight - 44, kScreenWidth, 44);
    if ([[UIApplication sharedApplication] statusBarFrame].size.height > 20) {
        btn.frame = CGRectMake(13, kScreenHeight - 44 - 34, kScreenWidth - 26, 43);
        btn.layer.cornerRadius = 2.f;
        btn.layer.masksToBounds = YES;
    }
//    btn._top -= 150;
//    btn._height += 150;
    btn.backgroundColor = CurrentPageIndicatorTintColor;
    [btn.titleLabel setFont:PF_REGU(15)];
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnclick) forControlEvents:UIControlEventTouchUpInside];
    btn.hidden = isHidden;
    [view addSubview:btn];
    
    return view;
}

-(void)valuechange{
    [scrollView_ setContentOffset:CGPointMake(kScreenWidth * page.currentPage, 0) animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSInteger i = scrollView.contentOffset.x / self.view.frame.size.width;
    page.currentPage = i;
    if (i == GuidePageNumber) {
        
        [page removeFromSuperview];
        [self btnclick];
    }
}

@end
