//
//  IXRegisterFooterV.h
//  IXApp
//
//  Created by Seven on 2017/9/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface IXRegisterFooterV : UIView

- (instancetype)initWithFrame:(CGRect)frame
                  topBtnTitle:(NSString *)title0
                 leftBtnTitle:(NSString *)titleL
               centerBtnTtile:(NSString *)titleC
                rightBtnTitle:(NSString *)titleR;

@property (nonatomic, copy) void((^btnBlock)(NSString * title));

@end
