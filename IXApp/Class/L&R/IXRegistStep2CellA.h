//
//  IXRegistStep2CellA.h
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SendSMS_Interval 60

@protocol IXRegistStep2CellADelegate <NSObject>

- (void)sendBtnClicked;

@end

@interface IXRegistStep2CellA : UITableViewCell

@property (nonatomic, strong)UIButton   * sendBtn;
@property (nonatomic, copy) NSString    * title;

@property (nonatomic, assign)id<IXRegistStep2CellADelegate>delegate;

- (void)reloadUIWithPhoneNum:(NSString *)phoneNum
                     phoneId:(NSString *)phoneId;

- (void)sendBtnTitleChanged:(NSInteger)cnt;

@end
