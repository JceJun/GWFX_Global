//
//  IXGuideView.h
//  IXApp
//
//  Created by Larry on 2018/8/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#define GUIDE_MARKET @"GUIDE_MARKET" // marketVC
#define GUIDE_ASSET  @"GUIDE_ASSET" // AssetVC
#define GUIDE_HELP2PAY_ADDBANKCARD @"GUIDE_HELP2PAY_ADDBANKCARD" // IXAddHelp2PayBankVC
@interface IXGuideView : UIView
+ (void)makeInstance:(NSString *)flag;
- (void)dismiss;
@end
