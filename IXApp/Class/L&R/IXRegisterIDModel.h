//
//  IXRegisterIDModel.h
//  IXApp
//
//  Created by Bob on 2017/2/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IXRegisterIDModel : JSONModel

@property (nonatomic, strong) NSString *value;

@end
