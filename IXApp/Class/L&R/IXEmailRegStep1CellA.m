//
//  IXEmailRegStep1CellA.m
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXEmailRegStep1CellA.h"

@interface IXEmailRegStep1CellA()

@property (nonatomic, strong)UILabel *desc;

@end

@implementation IXEmailRegStep1CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self desc];
    [self textF];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 12, 87.5, 20)];
        _desc.font = PF_MEDI(13);
        _desc.textAlignment = NSTextAlignmentLeft;
        _desc.dk_textColorPicker = DKGrayTextColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (IXTextField *)textF
{
    if (!_textF) {
        _textF = [[IXTextField alloc] initWithFrame:CGRectMake(99, 8, kScreenWidth-102-10, 30)];
        _textF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textF.dk_textColorPicker = DKCellTitleColor;
        _textF.tintColor = MarketSymbolNameColor;
        _textF.font = RO_REGU(13);
        _textF.keyboardType = UIKeyboardTypeEmailAddress;
        
        UIColor * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
        _textF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"请输入电子邮箱地址")
                                                                       attributes:@{NSForegroundColorAttributeName:phColor}];
        [self.contentView addSubview:_textF];
    }
    return _textF;
}

- (void)loadUIWithTFText:(NSString *)text withDesc:(NSString *)desc withTag:(NSUInteger)tag
{
    self.textF.text = text;
    self.textF.tag = tag;
    self.desc.text = [NSString stringWithFormat:@"%@",desc];
}

@end
