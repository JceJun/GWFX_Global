//
//  IXGuideView.m
//  IXApp
//
//  Created by Larry on 2018/8/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXGuideView.h"
#import "IXAppUtil.h"
#import "UIKit+Block.h"
@interface IXGuideView()
@property(nonatomic,strong)NSString *flag;
@property(nonatomic,strong)UIImageView *imgView;
@end

@implementation IXGuideView

+ (void)makeInstance:(NSString *)flag{
    if ([IXGuideView checkShow:flag]) {
        IXGuideView *instanceV = [IXGuideView new];
        instanceV.flag = flag;
        [KEYWINDOW addSubview:instanceV];
        [instanceV makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(0);
        }];
        [[NSUserDefaults standardUserDefaults] setObject:[IXAppUtil appVersion] forKey:flag];
        
        [instanceV block_whenTapped:^(UIView *aView) {
            [instanceV dismiss];
        }];
    }
}

+ (BOOL)checkShow:(NSString *)flag{
    
    // 特定页面重复出现
    if ([flag isEqualToString:GUIDE_HELP2PAY_ADDBANKCARD]) {
        return YES;
    }
    
    
    NSString *showPreviewYet = @"";
    @try{
        showPreviewYet = [[NSUserDefaults standardUserDefaults] objectForKey:flag];
    }@catch(NSException *exception) {
        showPreviewYet = @"";
    }
    // 随版本出现引导
    if ([showPreviewYet isEqualToString:[IXAppUtil appVersion]]) {
        return NO;
    }

    return YES;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        [self imgView];
    }
    return self;
}

- (UIImageView *)imgView{
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imgView];
        weakself;
        [_imgView block_whenTapped:^(UIView *aView) {
            [weakSelf dismiss];
        }];
    }
    return _imgView;
}


- (void)setFlag:(NSString *)flag{
    _flag = flag;
    if ([_flag isEqualToString:GUIDE_MARKET]) {
        _imgView.image = [UIImage imageNamed:@"guide_marketVC"];
        [_imgView makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(0);
            make.bottom.equalTo(-kTabbarHeight+25);
            make.left.equalTo(0);
            make.right.equalTo(0);
        }];
    }else if ([_flag isEqualToString:GUIDE_ASSET]){
        _imgView.image = [UIImage imageNamed:@"guide_assetVC"];
        [_imgView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(kNavbarHeight+30);
            make.centerX.equalTo(0);
        }];
    }else if ([_flag isEqualToString:GUIDE_HELP2PAY_ADDBANKCARD]){
        _imgView.image = [UIImage imageNamed:@"guide_AddHelp2PayBankVC"];
        [_imgView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(8);
            make.right.equalTo(-8);
            make.center.equalTo(0);
        }];
    }
}

- (void)dismiss{
    [self removeFromSuperview];
}

@end
