//
//  IXLoginFooterV.h
//  IXApp
//
//  Created by Seven on 2017/5/10.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,LoginFooterActionType) {
    LoginFooterActionTypeLogin, //登录
    LoginFooterActionTypeForgetPwd, //忘记密码
    LoginFooterActionTypeRegist,    //注册用户
    LoginFooterActionTypeContactUs, //联系我们
};

typedef void(^footerActionBlock)(LoginFooterActionType type);


/** 登录页table footer view */
@interface IXLoginFooterV : UIView

@property (nonatomic, copy) footerActionBlock   actionBlock;

@end
