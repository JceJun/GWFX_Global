//
//  IXFindPwdStep2VC.m
//  IXApp
//
//  Created by Evn on 17/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXFindPwdStep2VC.h"
#import "IXTouchTableV.h"
#import "IXRegistStep2CellA.h"
#import "IXLoginMainCellA.h"
#import "IXFindPwdStep3VC.h"
#import "IXCheckCodeV.h"

#import "IXBORequestMgr+Account.h"
#import "IXCpyConfig.h"
#import "IXCodeM.h"


static NSInteger sendCounter = SendSMS_Interval;
@interface IXFindPwdStep2VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
IXRegistStep2CellADelegate
>

@property (nonatomic, strong) IXTouchTableV * tableV;
@property (nonatomic, strong) UIButton      * submitBtn;
@property (nonatomic, strong) IXCheckCodeV *codeV;

@property (nonatomic, copy) NSString        * phoneId;
@property (nonatomic, strong) NSTimer       * timer;
@property (nonatomic, strong)IXCodeM        * codeM;

@end

@implementation IXFindPwdStep2VC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.title = LocalizedString(@"找回密码");
    _codeM = [[IXCodeM alloc] init];

    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    
    if (sendCounter < SendSMS_Interval) {
        [self enableTimer];
    }
    
    [self sendBtnClicked];
    self.view.dk_backgroundColorPicker = DKTableColor;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self disableTimer];
}


#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sendBtnClicked
{
    [self sendVerifyCode];
}

- (void)submitBtnClk
{
    [self submitVerifyCode];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        IXRegistStep2CellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep2CellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell sendBtnTitleChanged:sendCounter];
        cell.delegate = self;
        
        if (_type == pageTypePhoneFind) {
            [cell reloadUIWithPhoneNum:_psdModel.mobilePhone phoneId:_psdModel.mobilePhonePrefix];
        }else{
            [cell reloadUIWithPhoneNum:_psdModel.email phoneId:@""];
            cell.title = LocalizedString(@"电子邮箱");
        }
        return cell;
    } else {
        IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell loadUIWithDesc:LocalizedString(@"验证码")
                        text:@""
                 placeHolder:LocalizedString(@"请填写验证码")
                keyboardType:UIKeyboardTypeNumberPad
             secureTextEntry:NO];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        cell.textF.delegate = self;
        [cell.textF becomeFirstResponder];
        return cell;
    }
    return nil;
}



#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self submitBtnEnable:aimStr.length >= 4];
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self submitBtnEnable:NO];
    return YES;
}

- (void)submitBtnEnable:(BOOL)enable
{
    if (enable) {
        _submitBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
        
    }else{
        _submitBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark - create UI

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[IXLoginMainCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        [_tableV registerClass:[IXRegistStep2CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep2CellA class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth - 30, 132)];
    if (_type == pageTypePhoneFind) {
        label.text = LocalizedString(@"短信验证码已发送，请填写验证码");
    }else{
        label.text = LocalizedString(@"邮箱验证码已发送，请填写验证码");
    }
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKCellTitleColor;
    label.font = PF_MEDI(15);
    [v addSubview:label];

    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*2-kNavbarHeight)];
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
   
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.userInteractionEnabled = NO;
    _submitBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_submitBtn addTarget:self action:@selector(submitBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_submitBtn setTitle:LocalizedString(@"提 交") forState:UIControlStateNormal];
    _submitBtn.titleLabel.font = PF_REGU(15);
    [v addSubview:_submitBtn];
    
    return v;
}


#pragma mark -
#pragma mark - request

- (void)submitVerifyCode
{
    IXLoginMainCellA *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (_phoneId && [cell.textF.text length] >= 4) {
        weakself;
        [IXBORequestMgr acc_checkVerifyCodeWithCode:cell.textF.text
                                             codeId:_phoneId
                                           complete:^(BOOL success,
                                                      NSString *token,
                                                      NSString *errStr)
        {
            if (success) {
                [SVProgressHUD showSuccessWithStatus:LocalizedString(@"验证码正确")];
                
                [weakSelf disableTimer];
                
                IXFindPwdStep3VC *vc = [[IXFindPwdStep3VC alloc] init];
                vc.psdModel = weakSelf.psdModel;
                vc.type = _type;
                [weakSelf.navigationController pushViewController:vc animated:YES];

            }else{
                [SVProgressHUD showErrorWithStatus:errStr];
            }
        }];
    } else {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入验证码(最少四位数字)")];
    }
}

- (void)sendVerifyCode
{
    [SVProgressHUD showWithStatus:LocalizedString(@"验证码获取中...")];
    
    weakself;
    if (_type == pageTypePhoneFind) {
        weakself;
        [IXBORequestMgr acc_getPhoneVerifyCodeWithLimitedWithNumber:_psdModel.mobilePhone areaCode:_psdModel.mobilePhonePrefix imgVerifiCodeCode:_codeM.imgVerifiCodeCode imgVerifiCodeId:_codeM.imgVerifiCodeId result:^(NSString *errCode, NSString *errStr, NSString *codeId, NSString *imageVerifiCodeUrl, NSString *imageVerifiCodeId) {
            if (!errCode) {
                if (codeId.length) {
                    weakSelf.phoneId = codeId;
                    [weakSelf enableTimer];
                    [SVProgressHUD showSuccessWithStatus:LocalizedString(@"验证码发送成功")];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
                }
            } else {
                //需要图形验证
                if ([errCode integerValue] == 2203) {
                    [SVProgressHUD dismiss];
                    weakSelf.codeM.imgVerifiCodeId = imageVerifiCodeId;
                    weakSelf.codeM.imageVerifiCodeUrl = imageVerifiCodeUrl;
                    if (!weakSelf.codeV) {
                        weakSelf.codeV = [[IXCheckCodeV alloc] initWithCodeImageUrl:weakSelf.codeM.imageVerifiCodeUrl];
                        weakSelf.codeV.checkB = ^(NSString *code) {
                            weakSelf.codeM.imgVerifiCodeCode = code;
                            [weakSelf sendBtnClicked];
                        };
                        weakSelf.codeV.refreshB = ^{
                            [weakSelf sendBtnClicked];
                        };
                        weakSelf.codeV.dismissB = ^{
                            weakSelf.codeV = nil;
                        };
                        [weakSelf.codeV show];
                    } else {
                        [weakSelf.codeV refreshImageUrl:weakSelf.codeM.imageVerifiCodeUrl];
                    }
                } else {
                    if (errStr.length) {
                        [SVProgressHUD showErrorWithStatus:errStr];
                    } else {
                        [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
                    }
                }
            }
        }];
    }else{
        [IXBORequestMgr acc_getEmailVerifyCodeWithEmail:_psdModel.email
                                               complete:^(NSString *codeId, NSString *errStr)
        {
            if (codeId.length) {
                [weakSelf enableTimer];
                weakSelf.phoneId = codeId;
                [SVProgressHUD showSuccessWithStatus:LocalizedString(@"邮箱验证码成功发送")];
            }else{
                [SVProgressHUD showErrorWithStatus:errStr];
            }
        }];
    }
}

#pragma mark -
#pragma mark - timer

- (void)enableTimer
{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                selector:@selector(checkCounter)
                                                userInfo:nil
                                                 repeats:YES];
    }
    [_timer setFireDate:[NSDate distantPast]];
}

- (void)disableTimer
{
    if (_timer) {
        if ([_timer isValid]) {
            [_timer invalidate];
        }
        _timer = nil;
    }
}

- (void)checkCounter
{
    sendCounter --;
    if (sendCounter <= 0) {
        sendCounter = SendSMS_Interval;
        [_timer setFireDate:[NSDate distantFuture]];
    }
    IXRegistStep2CellA *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell sendBtnTitleChanged:sendCounter];
}


@end
