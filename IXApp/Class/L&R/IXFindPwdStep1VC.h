//
//  IXFindPwdStep1VC.h
//  IXApp
//
//  Created by Evn on 17/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXBORequestMgr+Account.h"

@interface IXFindPwdStep1VC : IXDataBaseVC

@property (nonatomic, copy) NSString    * userName;

/**
 页面类型，默认为通过手机找回密码
 */
@property (nonatomic, assign) pageType  type;

@end
