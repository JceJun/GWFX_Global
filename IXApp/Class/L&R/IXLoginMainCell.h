//
//  IXLoginMainCell.h
//  IXApp
//
//  Created by Evn on 2018/3/20.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXLoginMainCell : UITableViewCell

@property (nonatomic, strong)UILabel   *title;
@property (nonatomic, strong)UILabel   *desc;

- (void)loadUIWithName:(NSString *)name
           countryCode:(NSString *)countryCode;

@end
