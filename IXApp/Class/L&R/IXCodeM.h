//
//  IXCodeM.h
//  IXApp
//
//  Created by Evn on 2018/3/19.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXCodeM : NSObject

@property (nonatomic, copy)NSString  *verifyCodeId;
@property (nonatomic, copy)NSString  *imgVerifiCodeCode;
@property (nonatomic, copy)NSString  *imgVerifiCodeId;
@property (nonatomic, copy)NSString  *imageVerifiCodeUrl;

@end
