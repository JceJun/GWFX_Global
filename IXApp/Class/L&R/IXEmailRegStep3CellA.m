//
//  IXEmailRegStep3CellA.m
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXEmailRegStep3CellA.h"

@interface IXEmailRegStep3CellA()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *desc;

@end

@implementation IXEmailRegStep3CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 87.5, 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKGrayTextColor;
        _title.text = LocalizedString(@"电子邮箱");
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(110,
                                                          14,
                                                          kScreenWidth - GetView_MaxX(self.title),
                                                          16)];
        _desc.font = RO_REGU(15);
        _desc.dk_textColorPicker = DKCellContentColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (void)reloadUIWithEmail:(NSString *)email
{
    self.desc.text = email;
}

@end
