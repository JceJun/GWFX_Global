//
//  IXEmailRegStep1VC.m
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXEmailRegStep1VC.h"
#import "IXEmailRegStep2VC.h"
#import "IXEmailRegStep3VC.h"
#import "IXRegistStep1VC.h"
#import "IXLoginMainVC.h"
#import "IXWChatRegVC.h"
#import "IXAboutVC.h"

#import "IXEmailRegStep1CellA.h"
#import "IXRegisterFooterV.h"
#import "IXTouchTableV.h"
#import "FXWheellV.h"
#import "AppDelegate+UI.h"

#import <UMSocialCore/UMSocialCore.h>
#import "IXBORequestMgr+BroadSide.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Login.h"
#import "NSObject+IX.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXCountryListVC.h"
#import "IXUserDefaultM.h"
#import "IXADMgr.h"
#import "IXDataCacheMgr.h"

#define kHeaderHeight (187.5 + kTopMargin)

@interface IXEmailRegStep1VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV * tableV;
@property (nonatomic, strong)FXWheellV     *wheelV;//轮播图
@property (nonatomic, strong) UIButton  * registBtn;
@property (nonatomic, copy) NSString    * email;

@property (nonatomic, strong)UIButton *closeBtn;

@end

@implementation IXEmailRegStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(leftBtnItemClicked)];
//    self.navigationItem.rightBarButtonItem =[IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
//                                                                           target:self
//                                                                              sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"注册用户");
    
    [self.view addSubview:self.tableV];
    self.view.dk_backgroundColorPicker = DKTableColor;
    [self.view addSubview:self.closeBtn];
    [self loadAd];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //不实现或调用super会导致导航条显示，此处空实现以防止导航条出现
}

#pragma mark -
#pragma mark - ben action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}



#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString    * ident = NSStringFromClass([IXEmailRegStep1CellA class]);
    IXEmailRegStep1CellA *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    [cell loadUIWithTFText:self.email
                  withDesc:[IXLocalizationModel getLocalizationWordByKey:LocalizedString(@"电子邮箱")]
                   withTag:indexPath.row];
    cell.textF.textFont = RO_REGU(15);
    cell.textF.placeHolderFont = PF_MEDI(13);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textF.delegate = self;
    
    return cell;
   
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    //不接受输入空格
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:NONESPACECHAR];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self registerBtnEnable:[IXAppUtil isValidateEmail:aimStr]];
    _email = aimStr;
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self registerBtnEnable:NO];
    return YES;
}

- (void)registerBtnEnable:(BOOL)enable
{
    if (enable){
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                           topCapHeight:image.size.height/2];
        
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        UIImage * image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                           topCapHeight:image.size.height/2];
        
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
    _registBtn.userInteractionEnabled = enable;
}


#pragma mark -
#pragma mark - other

- (void)checkEmail
{
    if ([IXAppUtil isValidateEmail:self.email]) {
        if (SameString(self.email, [[IXUserInfoMgr shareInstance] demoLoginAccount])) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"用户名错误")];
            return;
        }
        IXEmailPhoneModel   * model = [[IXEmailPhoneModel alloc] init];
        model.email = self.email;
        model.index = 0;
        
        [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
        
        weakself;
        [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model
                                              complete:^(BOOL exist, NSString *errStr)
         {
             if (exist) {
                 [SVProgressHUD showErrorWithStatus:errStr];
             }else{
                 [SVProgressHUD dismiss];
                 
                 BOOL k = VerifyCodeEnable;
                 if (k) {
                     [weakSelf popToValidCode];
                 }else{
                     [weakSelf popToStep3VC];
                 }
             }
         }];
    }else{
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入正确的邮箱号")];
    }
}

- (void)popToValidCode
{
    IXEmailRegStep2VC * vc = [[IXEmailRegStep2VC alloc] init];
    vc.email = _email;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)popToStep3VC
{
    IXEmailRegStep3VC *vc = [[IXEmailRegStep3VC alloc] init];
    vc.email = _email;
    vc.token = @"";
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark -
#pragma mark - UI

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kHeaderHeight)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 100)/2, (kHeaderHeight - 115.5)/2, 100, 115.5)];
    [logo dk_setImagePicker:DKImageNames(@"common_logo", @"common_logo_D")];
    float height = 116*kScreenWidth/375;
    float width = height;
    logo.frame = CGRectMake((kScreenWidth - width)/2, (kHeaderHeight - height)/2, width, height);
    [v addSubview:logo];
    
    return v;
}

- (UIView *)tableFooterV
{
    CGRect rect = CGRectMake(0, 0, 0, kScreenHeight - kHeaderHeight - 44);
    IXRegisterFooterV   * v = [[IXRegisterFooterV alloc] initWithFrame:rect
                                                           topBtnTitle:LocalizedString(@"注 册")
                                                          leftBtnTitle:LocalizedString(@"关于我们")
                                                        centerBtnTtile:LocalizedString(@"登录账号")
                                                         rightBtnTitle:LocalizedString(@"手机注册")];
    
    weakself;
    v.btnBlock = ^(NSString *title) {
        [weakSelf dealWithFooterBtn:title];
    };
    
    return v;
}

- (UIButton *)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeBtn.frame = CGRectMake(kScreenWidth - (40 + 15), 20 , 40, 40);
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_closeBtn setImage:GET_IMAGE_NAME(@"ad_close") forState:UIControlStateNormal];
        _closeBtn.titleLabel.font = PF_REGU(15);
    }
    return _closeBtn;
}

- (void)closeBtnAction
{
    [IXDataCacheMgr clearAccountInfo];
    [IXUserInfoMgr shareInstance].isCloseDemo = NO;
    [AppDelegate showLogin];
}

- (void)dealWithFooterBtn:(NSString *)title
{
    if (SameString(title, LocalizedString(@"注 册"))) {
        [self.view endEditing:YES];
        [self checkEmail];
    }
    else if (SameString(title, LocalizedString(@"关于我们"))) {
        [self.navigationController pushViewController:[IXAboutVC new] animated:YES];
    }
    else if (SameString(title, LocalizedString(@"登录账号"))) {
        IXLoginMainVC * vc = nil;
        for (UIViewController * v in self.navigationController.viewControllers) {
            if ([v isKindOfClass:[IXLoginMainVC class]]) {
                [IXUserInfoMgr shareInstance].isCloseDemo = YES;
                vc = (IXLoginMainVC *)v;
                break;
            }
        }
        if (vc) {
            [self.navigationController popToViewController:vc animated:NO];
        } else {
            [self.navigationController pushViewController:[IXLoginMainVC new] animated:NO];
        }
    }
    else if (SameString(title, LocalizedString(@"手机注册"))) {
        [self.navigationController pushViewController:[IXRegistStep1VC new] animated:NO];
    }
}

#pragma mark 加载广告数据
- (void)loadAd
{
    NSDictionary *paramDic = @{
                               @"pageNo":@(1),
                               @"pageSize":@(20),
                               @"infomationType":@"REGISTER_ADVERTISEMENT"
                               };
    
    weakself;
    [IXBORequestMgr bs_bannerAdvertisementListWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
     {
         if (success && [obj ix_isDictionary] && obj[@"resultList"]) {
             [IXADMgr shareInstance].registAdArr = [obj[@"resultList"] mutableCopy];
             [IXADMgr shareInstance].registBannerClose = [obj[@"bannerClose"] boolValue];
             if ([IXADMgr shareInstance].registAdArr.count) {
                 [weakSelf.view addSubview:weakSelf.wheelV];
             }
         }
     }];
}

- (FXWheellV *)wheelV
{
    if (!_wheelV) {
        _wheelV = [[FXWheellV alloc] initWithFrame:CGRectMake(0, kScreenHeight - 94, kScreenWidth, 94)];
    }
    _wheelV.items = [IXADMgr shareInstance].registAdArr;
    _wheelV.bannerClose = [IXADMgr shareInstance].registBannerClose;
    return _wheelV;
}


#pragma mark -
#pragma mark - lazy laoding

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.tableFooterView = [self tableFooterV];
        _tableV.tableHeaderView = [self tableHeaderV];
        [_tableV registerClass:[IXEmailRegStep1CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXEmailRegStep1CellA class])];
    }
    return _tableV;
}

@end
