//
//  IXEmailRegStep2VC.h
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXEmailRegStep2VC : IXDataBaseVC

@property (nonatomic, strong)NSString *email;
@property (nonatomic, strong)NSString *fName;
@property (nonatomic, strong)NSString *lName;

@end
