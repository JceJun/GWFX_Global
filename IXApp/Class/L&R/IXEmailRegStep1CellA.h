//
//  IXEmailRegStep1CellA.h
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

@interface IXEmailRegStep1CellA : UITableViewCell

@property (nonatomic, strong)IXTextField *textF;

- (void)loadUIWithTFText:(NSString *)text withDesc:(NSString *)desc withTag:(NSUInteger)tag;

@end
