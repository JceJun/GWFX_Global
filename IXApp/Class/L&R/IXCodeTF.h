//
//  IXCodeTF.h
//  IXApp
//
//  Created by Evn on 2018/3/13.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXCodeTF : UITextField

+ (instancetype)instanceWidth:(CGFloat)width
                        count:(int)count
                  activeBlock:(void(^)(BOOL isActive))activeBlock;

- (void)reloadText:(NSString *)text;

@end
