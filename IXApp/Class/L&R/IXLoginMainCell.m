//
//  IXLoginMainCell.m
//  IXApp
//
//  Created by Evn on 2018/3/20.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXLoginMainCell.h"

@interface IXLoginMainCell()

@property (nonatomic, strong)UIImageView *icon;

@end

@implementation IXLoginMainCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    [self desc];
    [self icon];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(15, 12, 87.5-20, 20)];
        _title.font = PF_MEDI(13);
        _title.text = LocalizedString(@"国家/地区");
        _title.textAlignment = NSTextAlignmentLeft;
        _title.dk_textColorPicker = DKCellTitleColor;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(110-20, 12, kScreenWidth - (7.5 + 20 + 110), 20)];
        _desc.font = PF_MEDI(13);
        _desc.text = LocalizedString(@"国家信息");
        _desc.textAlignment = NSTextAlignmentLeft;
        _desc.dk_textColorPicker = DKCellContentColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (7.5 + 16), 15, 7.5, 13.5)];
        _icon.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (void)loadUIWithName:(NSString *)name
           countryCode:(NSString *)countryCode
{
    if (!name.length) {
        self.desc.text = LocalizedString(@"国家信息");
    } else {
        [IXDataProcessTools resetLabel:self.desc leftContent:name leftFont:13 WithContent:[NSString stringWithFormat:@"%@%@",name,countryCode] rightFont:13 fontType:NO];
    }
}

@end
