//
//  IXLoginAboutVC.m
//  IXApp
//
//  Created by Bob on 2017/3/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXLoginAboutVC.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Comp.h"
#import "NSObject+IX.h"

@interface IXLoginAboutVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
{
    NSMutableArray *_titleArr;
}

@property (nonatomic, strong) UITableView *contentTV;
@property (nonatomic, strong) UIView *headView;

@property (nonatomic, copy) NSString *cpyPhone;
@property (nonatomic, copy) NSString *cpyEmail;

@end

@implementation IXLoginAboutVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"联系我们");
    [self loadData];
}

- (void)loadData
{
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    weakself;
    [IXBORequestMgr comp_requestCompanyConfigInfoWithId:CompanyID complete:^(NSString *errStr, NSString *errCode, id obj) {
        [SVProgressHUD dismiss];
        if (!errCode && [obj ix_isDictionary]) {
            weakSelf.cpyPhone = [(NSDictionary *)obj objectForKey:@"companyTelephoneNo"];
            weakSelf.cpyEmail = [(NSDictionary *)obj objectForKey:@"companyEmail"];
            if (weakSelf.cpyPhone.length || weakSelf.cpyEmail.length) {
                [weakSelf initData];
                [weakSelf.view addSubview:weakSelf.contentTV];
            }
        }
    }];
}

- (void)initData
{
    if ([CompanyToken isEqualToString:@"firfx"]
        || [CompanyToken isEqualToString:@"ep_global"]
        || [CompanyToken isEqualToString:@"SD"]) {
        if (self.cpyEmail.length) {
            _titleArr = [@[[NSString stringWithFormat:@"%@：%@",
                            LocalizedString(@"电子邮箱"),
                            [IXDataProcessTools dealWithNil:self.cpyEmail]]] mutableCopy];
        }
    } else {
        _titleArr = [NSMutableArray new];
        if (self.cpyPhone.length) {
            [_titleArr addObject:[NSString stringWithFormat:@"%@：%@",LocalizedString(@"联系电话"),
                                  [IXDataProcessTools dealWithNil:self.cpyPhone]]];
        }
        if (self.cpyEmail.length) {
            [_titleArr addObject:[NSString stringWithFormat:@"%@：%@",
                                  LocalizedString(@"电子邮箱"),
                                  [IXDataProcessTools dealWithNil:self.cpyEmail]]];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString    * ident = NSStringFromClass([UITableViewCell class]);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = _titleArr[indexPath.row];
    cell.textLabel.font = PF_MEDI(13);
    cell.textLabel.dk_textColorPicker = DKCellTitleColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *sepImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 43, kScreenWidth, 1)];
    sepImg.dk_backgroundColorPicker = DKLineColor;
    [cell.contentView addSubview:sepImg];
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([((NSString *)_titleArr[indexPath.row]) containsString:LocalizedString(@"联系电话")]) {
        //拨打电话
        NSString    * tel = [NSString stringWithFormat:@"tel://%@",[IXDataProcessTools dealWithNil:self.cpyPhone]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
    }else{
        //发送邮件
        UIAlertControllerStyle style = kIsiPad ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert;
        
        UIAlertController   * alert = [UIAlertController alertControllerWithTitle:nil
                                                                          message:[IXDataProcessTools dealWithNil:self.cpyEmail]
                                                                   preferredStyle:style];
        [alert addAction:[UIAlertAction actionWithTitle:LocalizedString(@"取消")
                                                  style:UIAlertActionStyleCancel
                                                handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:LocalizedString(@"发送邮件")
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    [self sendEmail];
                                                }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)sendEmail
{
    NSString    * eml = [NSString stringWithFormat:@"mailto://%@",[IXDataProcessTools dealWithNil:self.cpyEmail]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:eml]];
}

#pragma mark -
#pragma mark - lazy loading

- (UIView *)headView
{
    if (!_headView) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 217.5)];
        _headView.dk_backgroundColorPicker = DKTableHeaderColor;
        
        UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 100)/2,
                                                                          (207.5 - 115.5)/2,
                                                                          100,
                                                                          115.5)];
        [_headView addSubview:logo];
        float height = 116*kScreenWidth/375;
        float width = height;
        [logo setImage:[UIImage imageNamed:@"common_logo"]];
        logo.frame = CGRectMake((kScreenWidth - width)/2, (207.5 - height)/2, width, height);
        
        UILabel *tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 172, kScreenWidth, 18)
                                             WithFont:PF_MEDI(15)
                                            WithAlign:NSTextAlignmentCenter
                                           wTextColor:0x4c6072
                                           dTextColor:0xe9e9ea];
        tipLbl.text = LocalizedString(CompanyName);
        
        UIView *sepV = [[UIView alloc] initWithFrame:CGRectMake(0, 216.5, kScreenWidth, 1)];
        sepV.dk_backgroundColorPicker = DKLineColor;
        [_headView addSubview:sepV];
    }
    return _headView;
}


- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:self.view.bounds];
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.tableHeaderView = self.headView;
        _contentTV.dk_separatorColorPicker = DKLineColor;
        _contentTV.dk_backgroundColorPicker = DKTableColor;
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_contentTV registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    }
    return _contentTV;
}
@end

