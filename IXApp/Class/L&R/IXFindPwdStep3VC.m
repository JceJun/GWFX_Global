//
//  IXFindPwdStep3VC.m
//  IXApp
//
//  Created by Evn on 17/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXFindPwdStep3VC.h"
#import "IXTouchTableV.h"
#import "IXLoginMainCellA.h"
#import "IXRegistStep3CellA.h"
#import "IXFindPwdStep4VC.h"
#import "IXCpyConfig.h"
#import "SFHFKeychainUtils.h"

@interface IXFindPwdStep3VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV * tableV;
@property (nonatomic, strong) UIButton      * cmpltBtn;
@property (nonatomic, assign) BOOL          flag;

@property (nonatomic, strong) UITextField   * pwdTf;
@property (nonatomic, strong) UITextField   * confirmPwdTf;

@end

@implementation IXFindPwdStep3VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.title = LocalizedString(@"找回密码");
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cmpltBtnClk
{
    [self.view endEditing:YES];
    
    if( [self checkPasswdValid] ){
        [self sendFindPsd];
    }
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textF.delegate = self;
        _pwdTf = cell.textF;
        [cell.textF becomeFirstResponder];
        [cell loadUIWithDesc:LocalizedString(@"新密码")
                        text:_psdModel.psd
                 placeHolder:LocalizedString(@"6至18位字母或数字")
                keyboardType:UIKeyboardTypeDefault
             secureTextEntry:YES];
        [cell setTextFieldTag:indexPath.row];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        return cell;
    } else {
        IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textF.delegate = self;
        _confirmPwdTf = cell.textF;
        [cell setTextFieldTag:indexPath.row];
        [cell loadUIWithDesc:LocalizedString(@"确认密码")
                        text:_psdModel.psdPre
                 placeHolder:LocalizedString(@"请重复输入")
                keyboardType:UIKeyboardTypeDefault
             secureTextEntry:YES];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        return cell;
    }
    return nil;
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (aimStr.length &&
        ((textField == _pwdTf && aimStr.length == _confirmPwdTf.text.length) ||
        (textField == _confirmPwdTf && aimStr.length == _pwdTf.text.length))){
        [self cmpltBtnEnable:YES];
    }else{
        [self cmpltBtnEnable:NO];
    }
    
    if (textField == _pwdTf) {
        _psdModel.psd = aimStr;
    }else{
        _psdModel.psdPre = aimStr;
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self cmpltBtnEnable:NO];
    return YES;
}

#pragma mark -
#pragma mark - create UI

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.dk_separatorColorPicker = DKLineColor;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[IXLoginMainCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        [_tableV registerClass:[IXRegistStep3CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep3CellA class])];
    }
    return _tableV;
}


- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(15, (132 - 18)/2, kScreenWidth-30, 18)];
    label1.numberOfLines = 0;
    label1.text = LocalizedString(@"请设置新的账户密码");
    label1.textAlignment = NSTextAlignmentCenter;
    label1.dk_textColorPicker = DKCellTitleColor;
    label1.font = PF_MEDI(15);
    [v addSubview:label1];
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*2-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    _cmpltBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _cmpltBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_cmpltBtn addTarget:self action:@selector(cmpltBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_cmpltBtn setTitle:LocalizedString(@"重置密码") forState:UIControlStateNormal];
    _cmpltBtn.titleLabel.font = PF_REGU(15);
    _cmpltBtn.userInteractionEnabled = NO;
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    
    [v addSubview:_cmpltBtn];
    
    
    return v;
}


//检查密码是否完整设置，以及新密码是否合法
//合法密码5-18位字母数字
- (BOOL)checkPasswdValid
{
    BOOL result = YES;
    if ( ![_psdModel.psd length] || ![_psdModel.psdPre length] ) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请完整输入新密码和确认密码")];
        result = NO;
    }
    
    if ( ![_psdModel.psd isEqualToString:_psdModel.psdPre] ) {
        [SVProgressHUD showErrorWithStatus:PWDDIFF];
        result = NO;
    }
    
    if ( ![IXEntityFormatter validPasswd:_psdModel.psd]  ) {
        [SVProgressHUD showErrorWithStatus:PWDVALIDCHAR];
        result = NO;
    }
    
    return result;
}


- (void)cmpltBtnEnable:(BOOL)enable
{
    if (enable) {
        _cmpltBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        _cmpltBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark - request

- (void)sendFindPsd
{
    weakself;
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    [IXBORequestMgr acc_findPasswordWith:_psdModel
                                pageType:_type
                                complete:^(BOOL findSuccess,
                                           NSString *errStr)
    {
        if (findSuccess) {
            [SVProgressHUD dismiss];
                        
            IXFindPwdStep4VC *VC = [[IXFindPwdStep4VC alloc] init];
            IXBaseNavVC * nc = [[IXBaseNavVC alloc] initWithRootViewController:VC];
            nc.navigationBar.dk_barTintColorPicker = DKNavBarColor;
            nc.navigationBar.translucent = NO;
            [weakSelf.navigationController presentViewController:nc animated:YES completion:nil];
        }else{
            [SVProgressHUD showErrorWithStatus:errStr];
        }
    }];
}




#pragma mark -
#pragma mark - cell delegate

- (void)textValue:(NSString *)value tag:(NSInteger)tag
{
    switch (tag) {
        case 0:
            _psdModel.psd = value;
            break;
        case 1:
            _psdModel.psdPre = value;
            break;
        default:
            break;
    }
    
    if (_psdModel.psd.length > 0 && _psdModel.psdPre.length > 0) {
        _flag = YES;
    } else {
        _flag = NO;
    }
    if (_flag) {
        
        _cmpltBtn.userInteractionEnabled = YES;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    } else {
        
        _cmpltBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

- (void)errMsg:(NSString *)msg
{
    [SVProgressHUD showErrorWithStatus:msg];
}
@end
