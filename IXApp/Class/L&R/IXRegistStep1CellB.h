//
//  IXRegistStep1CellB.h
//  IXApp
//
//  Created by Magee on 2016/12/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

@protocol IXRegistStep1CellBDelegate <NSObject>

- (void)textValue:(NSString *)value tag:(NSInteger)tag;

- (void)errMsg:(NSString *)msg;

@end


@interface IXRegistStep1CellB : UITableViewCell

@property (nonatomic, strong) IXTextField   *textF;

@property (nonatomic, assign)id <IXRegistStep1CellBDelegate> delegate;

- (void)loadUIWithTFText:(NSString *)text withDesc:(NSString *)desc withTag:(NSUInteger)tag;

- (void)setDescLabFont:(UIFont *)font;

@end
