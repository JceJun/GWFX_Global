//
//  IXRegistStep3CellA.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRegistStep3CellA.h"

@interface IXRegistStep3CellA ()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *desc;

@end

@implementation IXRegistStep3CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 87.5, 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKGrayTextColor;
        _title.text = LocalizedString(@"手机号码");
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(110, 14, 120, 16)];
        _desc.font = RO_REGU(15);
        _desc.dk_textColorPicker = DKCellContentColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (void)reloadUIWithPhoneNum:(NSString *)phoneNum
{
    self.desc.text = phoneNum;
}

@end
