//
//  IXFindPwdStep4VC.m
//  IXApp
//
//  Created by Evn on 17/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXFindPwdStep4VC.h"
#import "AppDelegate+UI.h"
#import "UINavigationController+FDFullscreenPopGesture.h"

@interface IXFindPwdStep4VC ()

@property (nonatomic, strong)UILabel *desc;
@property (nonatomic, strong)UIImageView *iconImg;
@end

@implementation IXFindPwdStep4VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_interactivePopDisabled = YES;
    [self.navigationItem setHidesBackButton:YES];

    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(rightBtnItemClicked)];

    self.title = LocalizedString(@"找回密码");
    [self.view addSubview:self.iconImg];
    [self.view addSubview:self.desc];

}

- (void)rightBtnItemClicked
{
    [AppDelegate showLoginMain];
}

- (UIImageView *)iconImg {
    
    if (!_iconImg) {
        
        _iconImg = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 80)/2, 45 + kNavbarHeight, 80, 80)];
        [_iconImg setImage:GET_IMAGE_NAME(@"common_complete")];
    }
    
    return _iconImg;
}

- (UILabel *)desc {
    
    if (!_desc) {        
        _desc = [IXCustomView createLable:CGRectMake(0, GetView_MaxY(_iconImg) + 20, kScreenWidth, 15)
                                    title:LocalizedString(@"密码重置成功")
                                     font:PF_MEDI(13)
                               wTextColor:0x4c6072
                               dTextColor:0xe9e9ea
                            textAlignment:NSTextAlignmentCenter];
    }
    return _desc;
}

@end
