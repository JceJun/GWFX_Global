//
//  IXRegistStep1CellB.m
//  IXApp
//
//  Created by Magee on 2016/12/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRegistStep1CellB.h"

@interface IXRegistStep1CellB ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel       * descLab;

@end

@implementation IXRegistStep1CellB

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self descLab];
    [self textF];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)descLab
{
    if (!_descLab) {
        _descLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 14, 70, 16)];
        _descLab.font = RO_REGU(15);
        _descLab.textAlignment = NSTextAlignmentLeft;
        _descLab.dk_textColorPicker = DKGrayTextColor;
        [self.contentView addSubview:_descLab];
    }
    return _descLab;
}

- (IXTextField *)textF
{
    if (!_textF) {
        _textF = [[IXTextField alloc] initWithFrame:CGRectMake(99, 8, kScreenWidth-102-10, 30)];
        _textF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textF.tintColor = MarketSymbolNameColor;
        _textF.dk_textColorPicker = DKCellTitleColor;
        _textF.font = RO_REGU(15);
        _textF.delegate = self;
        _textF.keyboardType = UIKeyboardTypeNumberPad;
         UIColor * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
        _textF.attributedPlaceholder = [[NSAttributedString alloc]
                                        initWithString:LocalizedString(@"请输入手机号码")
                                        attributes:@{NSForegroundColorAttributeName:phColor}];
        [self.contentView addSubview:_textF];
    }
    return _textF;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textValue:tag:)]) {
        
        [self.delegate textValue:textField.text tag:textField.tag];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //不接受输入空格
    if([string isEqualToString:@" "]){
        if (self.delegate && [self.delegate respondsToSelector:@selector(errMsg:)]) {
            [self.delegate errMsg:NONESPACECHAR];
        }
        return NO;
    }
    
    
    return YES;
}

- (void)loadUIWithTFText:(NSString *)text withDesc:(NSString *)desc withTag:(NSUInteger)tag
{
    self.textF.text = text;
    self.textF.tag = tag;
    self.descLab.text = desc;
}

- (void)setDescLabFont:(UIFont *)font
{
    self.descLab.font = font;
}

@end
