//
//  IXRegistStep3VC.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRegistStep3VC.h"
#import "IXRegistStep4VC.h"
#import "IXRegistStep3CellA.h"
#import "IXLoginMainCellA.h"
#import "IXTouchTableV.h"
#import "IXTraceWebV.h"

#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Login.h"
#import "SFHFKeychainUtils.h"
#import "IXCpyConfig.h"
#import "IXWUserInfo.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"

@interface IXRegistStep3VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV * tableV;
@property (nonatomic, strong) UIButton      * cmpltBtn;
@property (nonatomic, strong) UITextField   * pwdTf;
@property (nonatomic, strong) UITextField   * confirmPwdTf;
@property (nonatomic, strong) IXTraceWebV   * traceWebV;

@property (nonatomic, copy) NSString    * pwd;

@end

@implementation IXRegistStep3VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"注册用户");
   
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [SVProgressHUD dismiss];
}


#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)cmpltBtnClk
{
    [self.view endEditing:YES];
    [self regisOutter];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        IXRegistStep3CellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep3CellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell reloadUIWithPhoneNum:_phoneNum];
        return cell;
    } else if (indexPath.row == 1){
        IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _pwdTf = cell.textF;
        cell.textF.delegate = self;
        [cell.textF becomeFirstResponder];
        [cell loadUIWithDesc:LocalizedString(@"密码")
                        text:@""
                 placeHolder:LocalizedString(@"6至18位字母或数字")
                keyboardType:UIKeyboardTypeDefault
             secureTextEntry:YES];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        return cell;
    } else {
        IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _confirmPwdTf = cell.textF;
        cell.textF.delegate = self;
        [cell loadUIWithDesc:LocalizedString(@"确认密码")
                        text:@""
                 placeHolder:LocalizedString(@"请重复输入")
                keyboardType:UIKeyboardTypeDefault
             secureTextEntry:YES];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        return cell;
    }
    return nil;
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (aimStr.length &&
        ((textField == _pwdTf && aimStr.length == _confirmPwdTf.text.length) ||
        (textField == _confirmPwdTf && aimStr.length == _pwdTf.text.length))){
            [self cmpltBtnEnable:YES];
    }else{
        [self cmpltBtnEnable:NO];
    }
    if (aimStr.length > 18) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self cmpltBtnEnable:NO];
    return YES;
}

- (void)cmpltBtnEnable:(BOOL)enable
{
    if (enable) {
        _cmpltBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        _cmpltBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

- (BOOL)checkSettingPwd
{
    NSString *errTip = @"";
    //两次密码一致
    if ( [_pwdTf.text isEqualToString:_confirmPwdTf.text] ) {
        
        _pwd = _pwdTf.text;
        //密码长度6-18位
        if (_pwd.length >= 6 && _pwd.length <= 18) {
            //只有数字或字母
            if ([IXEntityFormatter validPasswd:_pwd]) {
                return YES;
            }
        }
        
        errTip = PWDVALIDCHAR;
    }else{
        errTip = PWDDIFF;
    }
    
    [SVProgressHUD showErrorWithStatus:errTip];
    
    return NO;
}

#pragma mark -
#pragma mark - create UI

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXLoginMainCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        [_tableV registerClass:[IXRegistStep3CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep3CellA class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 64-18-5, kScreenWidth, 18)];
    label1.text = LocalizedString(@"请设置账户密码");
    label1.textAlignment = NSTextAlignmentCenter;
    label1.dk_textColorPicker = DKCellTitleColor;
    label1.font = PF_MEDI(15);
    [v addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 64+5, kScreenWidth, 18)];
    label2.text = LocalizedString(@"之后你可以使用手机号登录");
    label2.textAlignment = NSTextAlignmentCenter;
    label2.dk_textColorPicker = DKCellTitleColor;
    label2.font = PF_MEDI(15);
    [v addSubview:label2];
    
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*2-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
   
    _cmpltBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _cmpltBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_cmpltBtn addTarget:self action:@selector(cmpltBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_cmpltBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_cmpltBtn setTitle:LocalizedString(@"完成注册") forState:UIControlStateNormal];
    _cmpltBtn.titleLabel.font = PF_REGU(15);
    _cmpltBtn.userInteractionEnabled = NO;
    
    [v addSubview:_cmpltBtn];
    return v;
}


#pragma mark -
#pragma mark - tequest

- (void)regisOutter
{
    if ([self checkSettingPwd]) {
        [self cmpltBtnEnable:NO];
        [SVProgressHUD showWithStatus:LocalizedString(@"正在注册")];
        
        //嵌套webview地址
        if (!_traceWebV) {
            _traceWebV = [IXTraceWebV initWithWebUrlType:TraceWebUrlTypeTouchRegist];
            [self.view addSubview:_traceWebV];
        } else {
            [_traceWebV refreshWebUrlType:TraceWebUrlTypeTouchRegist];
        }
        
        IXRegisterStep3M *model = [[IXRegisterStep3M alloc] init];
        model.companyId = CompanyID;
        model.mobilePhone = _phoneNum;
        model.mobilePhonePrefix = _phoneId;
        model.isAutoApprove = YES;
        model.passwordRaw = [IXDataProcessTools md5StringByString:_pwd];
        model.nationality = [IXUserDefaultM getCode];
        model.chineseName = [_fName stringByAppendingFormat:@" %@",_lName];
        model.email = _email;
        [self rByPhone:model];
    }
}



- (void)rByPhone:(IXRegisterStep3M *)model
{
    
    weakself;
    [IXBORequestMgr acc_registWith:model
                       token:_token
                    complete:^(BOOL registerSucess, NSString *errStr)
    {
        if (registerSucess) {
            [weakSelf saveUserInfo];
            [SVProgressHUD dismiss];
            
            //嵌套webview地址
            if (!weakSelf.traceWebV) {
                weakSelf.traceWebV = [IXTraceWebV initWithWebUrlType:TraceWebUrlTypeRegistSuccess];
                [weakSelf.view addSubview:weakSelf.traceWebV];
            } else {
                [weakSelf.traceWebV refreshWebUrlType:TraceWebUrlTypeRegistSuccess];
            }
            
            [self loginAfterRegister];
//            [weakSelf popReg4];
        }else{
            [SVProgressHUD showErrorWithStatus:errStr];
        }
        
        [weakSelf cmpltBtnEnable:YES];
    }];
}

- (void)popReg4
{
    IXRegistStep4VC *step4 = [[IXRegistStep4VC alloc] init];
    step4.parentVC = self;
    [self.navigationController pushViewController:step4 animated:YES];
}



- (void)loginAfterRegister
{
    [SVProgressHUD showWithStatus:LocalizedString(@"加载中...")];
    IXLoginMainCellA *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    proto_user_login *proto = [[proto_user_login alloc] init];
    [proto setPhone:_phoneNum];
    [proto setPwd:[IXDataProcessTools md5StringByString:cell.textF.text]];
    
    [proto setLogintime:[IXEntityFormatter getCurrentTimeInterval]];
    [proto setLoginType:proto_user_login_elogintype_ByPhone];
    [proto setType:proto_user_login_elogintype_ByPhone];
    [proto setCompanyToken:CompanyToken];
    [proto setCompanyid:CompanyID];
    [proto setSessionType:[IXUserInfoMgr shareInstance].itemType];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    [proto setVersion:[version intValue]];
    [proto setSecureDev:[IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]]];
    if (!_phoneId.length) {
        _phoneId = @"62";
    }
    [proto setPhoneCode:_phoneId];
    [IXUserInfoMgr shareInstance].loginType = proto_user_login_elogintype_ByPhone;
    [IXUserInfoMgr shareInstance].loginName = nil;
    [[IXTCPRequest shareInstance] loginWithParam:proto];
    
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.loginAferRegist = YES;
}



- (void)saveUserInfo
{
    IXLoginMainCellA *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    NSError *error;
    
    [SFHFKeychainUtils storeUsername:_phoneNum
                         andPassword:cell.textF.text
                      forServiceName:kServiceName
                      updateExisting:YES error:&error];
    if (!error) {
        DLog(@"账户保存成功");
        [[NSUserDefaults standardUserDefaults] setObject:_phoneNum forKey:kIXLoginName];
        [[NSUserDefaults standardUserDefaults] setObject:_phoneId forKey:kIXPhoneCode];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        
        ELog(@"账户保存失败");
    }
    return;
}

@end
