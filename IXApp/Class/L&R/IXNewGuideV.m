//
//  IXNewGuideV.m
//  IXApp
//
//  Created by Evn on 2017/8/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXNewGuideV.h"

@interface IXNewGuideV()

@property (nonatomic, strong)UIImageView *imgView;
@property (nonatomic, strong)UIButton *sureBtn;
@property (nonatomic, assign)CGRect imgFrame;
@property (nonatomic, assign)CGRect btnFrame;
@property (nonatomic, copy)NSString *imgName;
@property (nonatomic, assign)IXNewGuide type;

@end

@implementation IXNewGuideV

- (void)showNewGuideType:(IXNewGuide)type
{
    self.backgroundColor = [UIColor blackColor];
    self.alpha = 0.75;
    [self dealWithDataType:type];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [[UIApplication sharedApplication].keyWindow addSubview:self.imgView];
    [[UIApplication sharedApplication].keyWindow addSubview:self.sureBtn];
}

- (void)dealWithDataType:(IXNewGuide)type
{
    NSString *lan = [IXLocalizationModel currentCheckLanguage];
    _type = type;
    CGFloat height = 0.f,offseY = 0.f;
    if (Is_iPhoneX) {
        height = 812;
        offseY = 24;
    } else {
        height = 667;
    }
    switch (type) {
        case IXNewGuideBroadside:{
            _imgFrame = CGRectMake(8, 20 + offseY, 254*kScreenWidth/375, 135*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 15 + _imgFrame.origin.y + _imgFrame.size.height + 20, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_broadside_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_broadside_en";
            } else {
                _imgName = @"newGuide_broadside_cn";
            }
        }
            break;
        case IXNewGuideCata:{
            _imgFrame = CGRectMake((kScreenWidth - (147*kScreenWidth/375))/2, 25 + offseY, 193*kScreenWidth/375, 147*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 15 + _imgFrame.origin.y + _imgFrame.size.height + 25, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_cata_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_cata_en";
            } else {
                _imgName = @"newGuide_cata_cn";
            }
        }
            break;
        case IXNewGuidePosition:{
            _imgFrame = CGRectMake(5, kNavbarHeight + 128 + 50 + offseY, kScreenWidth - 10, ((kScreenWidth - 10)/374)*192*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 15 + _imgFrame.origin.y + _imgFrame.size.height + 20, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_position_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_position_en";
            } else {
                _imgName = @"newGuide_position_cn";
            }
        }
            break;
        case IXNewGuideKLineDetail:{
            _imgFrame = CGRectMake(20, 73 + kNavbarHeight + 15 + offseY, 280*kScreenWidth/375, 180*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 15 + _imgFrame.origin.y + _imgFrame.size.height + 20, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_kline_detail_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_kline_detail_en";
            } else {
                _imgName = @"newGuide_kline_detail_cn";
            }
        }
            break;
        case IXNewGuideKlineLandscape:{
            _imgFrame = CGRectMake((kScreenWidth - 180*kScreenWidth/375)/2, 73 + kNavbarHeight + 60 + offseY, 180*kScreenWidth/375, 152*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 15 + _imgFrame.origin.y + _imgFrame.size.height + 20, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_kline_ls_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_kline_ls_en";
            } else {
                _imgName = @"newGuide_kline_ls_cn";
            }
        }
            break;
        case IXNewGuideDomIn:{

            _imgFrame = CGRectMake(5, 73 + kNavbarHeight + 286 + offseY, kScreenWidth - 10, ((kScreenWidth - 10)/370)*165*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 73 + kNavbarHeight + 286 - (40*kScreenHeight/height + 30), 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_dom_in_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_dom_in_en";
            } else {
                _imgName = @"newGuide_dom_in_cn";
            }
        }
            break;
        case IXNewGuideDomMp:{
            _imgFrame = CGRectMake(5, 73 + kNavbarHeight + 50 + offseY, kScreenWidth - 10, ((kScreenWidth - 10)/372)*145*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 15 + _imgFrame.origin.y + _imgFrame.size.height + 20, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_dom_mp_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_dom_mp_en";
            } else {
                _imgName = @"newGuide_dom_mp_cn";
            }
        }
            break;
        case IXNewGuideDomClick:{
            _imgFrame = CGRectMake(kScreenWidth - (10 + 234*kScreenWidth/375), 73 + kNavbarHeight + 175 + offseY, 234*kScreenWidth/375, 95*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, 15 + _imgFrame.origin.y + _imgFrame.size.height + 20, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_dom_click_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_dom_click_en";
            } else {
                _imgName = @"newGuide_dom_click_cn";
            }
        }
            break;
        case IXNewGuideDomLimit:{
            _imgFrame = CGRectMake(5, 73 + kNavbarHeight + 100 + offseY, kScreenWidth - 10, ((kScreenWidth - 10)/364)*400*kScreenHeight/height);
            _btnFrame = CGRectMake((kScreenWidth - 100*kScreenWidth/375)/2, _imgFrame.origin.y + _imgFrame.size.height/2 + 30, 100*kScreenWidth/375, 40*kScreenHeight/height);
            if ([lan isEqualToString:LANTW]) {
                _imgName = @"newGuide_dom_limit_tw";
            } else if ([lan isEqualToString:LANEN]) {
                _imgName = @"newGuide_dom_limit_en";
            } else {
                _imgName = @"newGuide_dom_limit_cn";
            }
        }
            break;
            
        default:
            break;
    }
}

- (UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:_imgFrame];
    }
    _imgView.tag = 10000;
    [_imgView setImage:GET_IMAGE_NAME(_imgName)];
    return _imgView;
}

- (UIButton *)sureBtn
{
    if (!_sureBtn) {
        _sureBtn = [[UIButton alloc] initWithFrame:_btnFrame];
        [_sureBtn addTarget:self action:@selector(sureBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    if ([[IXLocalizationModel currentCheckLanguage] isEqualToString:LANEN]) {
        [_sureBtn setImage:GET_IMAGE_NAME(@"newGuide_btn_en") forState:UIControlStateNormal];
    } else {
        [_sureBtn setImage:GET_IMAGE_NAME(@"newGuide_btn") forState:UIControlStateNormal];
    }
    _sureBtn.tag = 10001;
    return _sureBtn;
}

- (void)sureBtnAction
{
    if (self) {
        [self removeFromSuperview];
    }
    if (_imgView) {
        [_imgView removeFromSuperview];
        _imgView = nil;
    }
    if (_sureBtn) {
        [_sureBtn removeFromSuperview];
        _sureBtn = nil;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(nextRemoveViewType:)]) {
        [self.delegate nextRemoveViewType:_type];
    }
}

@end
