//
//  IXRegistStep4VC.m
//  IXApp
//
//  Created by Bob on 2017/3/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRegistStep4VC.h"
#import "IXRegistStep3VC.h"
#import "IXCpyConfig.h"
#import <UMMobClick/MobClick.h>

@interface IXRegistStep4VC ()

@property (nonatomic, strong) UIImageView *backImg;

@property (nonatomic, strong) UIImageView *logoImg;

@end

@implementation IXRegistStep4VC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.backImg.image = [UIImage imageNamed:@"regGuideCon"];

    if (UMAppKey.length) {
        [MobClick event:UMRegisterSuccess];     //友盟统计
    }
    
    UIButton *openRealBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:openRealBtn];
    openRealBtn.backgroundColor = [UIColor whiteColor];
    [openRealBtn setTitle:LocalizedString(@"开设真实资金账户") forState:UIControlStateNormal];
    [openRealBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    [openRealBtn.titleLabel setFont:PF_REGU(15)];
    
    CGFloat originY = kScreenHeight - 93;
    openRealBtn.frame = CGRectMake( 15, originY, kScreenWidth - 30, 43);
    openRealBtn.layer.masksToBounds = YES;
    openRealBtn.layer.cornerRadius = 3;

    [openRealBtn addTarget:self action:@selector(openRealAccount) forControlEvents:UIControlEventTouchUpInside];
    //印尼项目隐藏
    openRealBtn.hidden = YES;
    
    originY -= 58;
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:loginBtn];
    
    loginBtn.frame = CGRectMake( 15, originY, kScreenWidth - 30, 43);
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginBtn.titleLabel setFont:PF_REGU(15)];
    [loginBtn setTitle:LocalizedString(@"以虚拟10 000美元开始") forState:UIControlStateNormal];

    loginBtn.layer.masksToBounds = YES;
    loginBtn.layer.cornerRadius = 3;
    loginBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    loginBtn.layer.borderWidth = 1;
    
    [loginBtn addTarget:self action:@selector(startDeal) forControlEvents:UIControlEventTouchUpInside];

    
    originY -= 56;
    UILabel *tip1Lbl = [IXUtils createLblWithFrame:CGRectMake( 0, originY, kScreenWidth, 18)
                                          WithFont:PF_MEDI(15)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0xbfd2e2
                                        dTextColor:0xbfd2e2];
    [self.view addSubview:tip1Lbl];
    tip1Lbl.text = LocalizedString(@"交易一模一样");
    
    originY -= 31;
    UILabel *tip2Lbl = [IXUtils createLblWithFrame:CGRectMake( 0, originY, kScreenWidth, 18)
                                          WithFont:PF_MEDI(15)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0xbfd2e2
                                        dTextColor:0xbfd2e2];
    [self.view addSubview:tip2Lbl];
    tip2Lbl.text = LocalizedString(@"模拟交易除了使用虚拟资金外跟真实资金");
    
    originY -= 31;
    UILabel *tip3Lbl = [IXUtils createLblWithFrame:CGRectMake( 0, originY, kScreenWidth, 18)
                                          WithFont:PF_MEDI(15)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0xbfd2e2
                                        dTextColor:0xbfd2e2];
    [self.view addSubview:tip3Lbl];
    tip3Lbl.text = LocalizedString(@"您可以使用10000美元资金来模拟交易");
    
    
    originY -= 134;
    self.logoImg.image = [UIImage imageNamed:[IXUtils tranImageName:@"register_mark_img"]];
    CGRect frame = self.logoImg.frame;
    frame.origin.y = originY;
    self.logoImg.frame = frame;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.navigationController  setNavigationBarHidden:NO];
}

- (void)startDeal
{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.startWithReg = RegStartDeal;
    
    [self login];
}

- (void)openRealAccount
{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.startWithReg = RegOpenRealAccount;

    [self login];
}


- (void)login
{
    if (self.parentVC) {
        if ([self.parentVC respondsToSelector:@selector(loginAfterRegister)]) {
            [self.parentVC performSelector:@selector(loginAfterRegister)];
        }
    }
}

- (UIImageView *)backImg
{
    if ( !_backImg ) {
        _backImg = [[UIImageView alloc] initWithFrame:self.view.bounds];
        [self.view addSubview:_backImg];
    }
    return _backImg;
}



- (UIImageView *)logoImg
{
    if ( !_logoImg ) {
        CGFloat originX = (kScreenWidth - 273)/2;
        _logoImg = [[UIImageView alloc] initWithFrame:CGRectMake( originX, 0, 273, 99)];
        [self.view addSubview:_logoImg];
    }
    return _logoImg;
}
@end
