//
//  IXRegisterStep3M.m
//  IXApp
//
//  Created by Bob on 2017/2/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRegisterStep3M.h"
#import "IXRegisterIDModel.h"
#import "IXCpyConfig.h"

@implementation IXRegisterStep3M

- (id)init
{
    self = [super init];
    if ( self ) {
        _isAutoApprove = YES;
        IXRegisterIDModel *model = [[IXRegisterIDModel alloc] init];
        _idDocumentNumber = [model toJSONString];
        _idDocumentNumberMd5 = [IXDataProcessTools md5StringByString:_idDocumentNumber];
        _openFrom = @"WEBSITE_IOS";
        _messageLang = [IXLocalizationModel currentCheckLanguage];
        _appMarket = AppMarket;
    }
    return self;
}

- (void)setMobileNumber:(NSString *)mobileNumber
{
    _mobilePhone = mobileNumber;
}

- (void)setIdNumber:(NSString *)idNumber
{
    _idDocumentNumber = idNumber;
    _idDocumentNumberMd5 = [IXDataProcessTools md5StringByString:idNumber];
}

@end
