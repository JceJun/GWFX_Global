//
//  FXWheellV.m
//  FXApp
//
//  Created by Seven on 2017/7/17.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "FXWheellV.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSObject+IX.h"
#import "IXCommonWebVC.h"
#import "AppDelegate+UI.h"
#import "IXUserMgr.h"
static  NSString    * cellIdent = @"collection_cell";

@interface FXWheellV ()
<
UICollectionViewDelegate,
UICollectionViewDataSource
>

@property (nonatomic, strong) UICollectionView  * collectionV;
@property (nonatomic, strong) NSTimer   * wheelTimer;
@property (nonatomic, strong) UIPageControl * pageControl;
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) NSMutableArray    * operationArr;

@end

@implementation FXWheellV

- (void)dealloc
{
    [_wheelTimer invalidate];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.collectionV];
        [self addSubview:self.pageControl];
        self.collectionV.contentOffset = CGPointMake(frame.size.width, 0);
        _timeSpace = 5.f;
    }
    return self;
}

- (void)setBannerClose:(BOOL)bannerClose
{
    _bannerClose = bannerClose;
    if (_bannerClose) {
        [self addSubview:self.closeBtn];
    }
}

- (void)setItems:(NSArray *)items
{
    if (items.count > 1) {
        NSMutableArray  * arr = [items mutableCopy];
        [arr addObject:[[items firstObject] copy]];
        [arr insertObject:[items lastObject] atIndex:0];
        _items = arr;
        
        self.pageControl.numberOfPages = items.count;
        [self.collectionV reloadData];
        [self delayFireTimer];
    } else {
        _items = [items copy];
        [self.collectionV reloadData];
    }
}

- (void)setTimeSpace:(CGFloat)timeSpace
{
    _timeSpace = timeSpace;
    if (_wheelTimer) {
        [_wheelTimer invalidate];
        _wheelTimer = nil;
    }
    
    [self delayFireTimer];
}

- (void)delayFireTimer
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_timeSpace * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.wheelTimer fire];
    });
}

- (void)fireTimer
{
    [self.wheelTimer fire];
}

- (void)changePage
{
    CGFloat x = self.collectionV.contentOffset.x;
    NSInteger   aimIndex = ((NSInteger)(x/self.bounds.size.width) + 1)% self.items.count;
    x = aimIndex * self.bounds.size.width;
    
    [self.collectionV scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:aimIndex inSection:0]
                             atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                     animated:YES];
    self.pageControl.currentPage = (aimIndex-1)%(self.items.count - 2);
    if (aimIndex == self.items.count-1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.collectionV setContentOffset:CGPointMake(self.bounds.size.width, 0)];
        });
    } else if (aimIndex == 0) {
        [self.collectionV setContentOffset:CGPointMake(self.bounds.size.width*(_items.count-1), 0)];
    }
}

#pragma mark -
#pragma mark - collection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FXWheellCell    * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdent
                                                                       forIndexPath:indexPath];
    
    cell.imgPathDic = self.items[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.items[indexPath.row];
    NSString *urlStr = dic[@"url"];
    if ([urlStr ix_isString] && urlStr.length) {
        if ([[urlStr lowercaseString] containsString:@"http"]) {
            [AppDelegate gotoCommonWeb:urlStr];
        }else{
            [self gotoNative:urlStr];
        }
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset
{
    CGFloat w = self.bounds.size.width;
    
    //page control
    if (targetContentOffset->x <= 0.f) {
        self.pageControl.currentPage = _items.count - 2;
    }else if (targetContentOffset->x >= w*(_items.count - 1)) {
        self.pageControl.currentPage = 0;
    }else {
        self.pageControl.currentPage = targetContentOffset->x / self.frame.size.width - 1;
    }
    
    self.userInteractionEnabled = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.userInteractionEnabled = YES;
    CGFloat w = self.bounds.size.width;
    if (scrollView.contentOffset.x <= 0.f) {
        CGPoint p = CGPointMake(w*(_items.count - 2), 0);
        [scrollView setContentOffset:p animated:NO];
    }else if (scrollView.contentOffset.x >= w*(_items.count - 1)) {
        CGPoint p = CGPointMake(w, 0);
        [scrollView setContentOffset:p animated:NO];
    }
    
    for (NSInvocationOperation * op in self.operationArr) {
        [op cancel];
    }
    NSInvocationOperation   * op = [[NSInvocationOperation alloc] initWithTarget:self
                                                                        selector:@selector(fireTimer)
                                                                          object:nil];
    [self.operationArr addObject:op];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_timeSpace * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (!op.isCancelled) {
            [op start];
            [self.operationArr removeAllObjects];
        }
    });
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_wheelTimer invalidate];
    _wheelTimer = nil;
}


#pragma mark -
#pragma mark - lazy loading

- (UICollectionView *)collectionV
{
    if (!_collectionV) {
        UICollectionViewFlowLayout  * layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 0;
        layout.itemSize = self.bounds.size;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        _collectionV = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
        _collectionV.delegate = self;
        _collectionV.dataSource = self;
        _collectionV.pagingEnabled = YES;
        _collectionV.bounces = NO;
        _collectionV.showsHorizontalScrollIndicator = NO;
        [_collectionV registerClass:[FXWheellCell class] forCellWithReuseIdentifier:cellIdent];
    }
    return _collectionV;
}

- (NSTimer *)wheelTimer
{
    if (!_wheelTimer) {
        _wheelTimer = [NSTimer scheduledTimerWithTimeInterval:_timeSpace
                                                       target:self
                                                     selector:@selector(changePage)
                                                     userInfo:nil
                                                      repeats:YES];
    }
    return _wheelTimer;
}

-(UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 15,
                                                                       self.bounds.size.width,
                                                                       10)];
        _pageControl.pageIndicatorTintColor = MarketGrayPriceColor;
        _pageControl.currentPageIndicatorTintColor = MarketSymbolNameColor;
    }
    return _pageControl;
}

- (UIButton *)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 28, 8, 20, 20)];
        [_closeBtn dk_setImage:DKImagePickerWithNames(@"openAccount_remove_btn",@"openAccount_remove_btn_D") forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}

- (void)closeBtnAction
{
    [self removeFromSuperview];
}

- (NSMutableArray *)operationArr
{
    if (!_operationArr) {
        _operationArr = [@[] mutableCopy];
    }
    return _operationArr;
}

- (void)gotoNative:(NSString *)url{
    if ([url isEqualToString:@"app:deposit"]) {
        // 强制跳转入金
        NSString *accountType = [IXDataProcessTools showCurrentAccountType];
        if ([accountType isEqualToString:LocalizedString(@"模拟")]) {
            [[IXUserMgr sharedIXUserMgr] switchToRealAccount:4];
        }else if ([accountType isEqualToString:LocalizedString(@"真实")]){
            [AppDelegate gotoNativeVC:@"IXDPSChannelVC" isPresent:NO param:nil];
        }
    }else if ([url isEqualToString:@""]){
        
    }
}


@end


@interface FXWheellCell ()

@property (nonatomic, strong) UIImageView   * imgV;

@end

@implementation FXWheellCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        _imgV = [[UIImageView alloc] initWithFrame:self.bounds];
        _imgV.contentMode = UIViewContentModeScaleAspectFill;
        _imgV.clipsToBounds = YES;
        _imgV.dk_imagePicker = DKImageNames(@"ad_default", @"ad_default_D");
        [self.contentView addSubview:_imgV];
    }
    return self;
}

- (void)setImgPathDic:(NSDictionary *)imgPathDic
{
    _imgPathDic = imgPathDic;
    NSString *urlStr = _imgPathDic[@"titleImg"];
    if ([urlStr containsString:@"http"]) {
        
        NSURL * url = [NSURL URLWithString:urlStr];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:urlStr] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image) {
                [[SDWebImageManager sharedManager] saveImageToCache:image forURL:url];
                self.imgV.image = image;
            }
        }];
    } else {
        _imgV.dk_imagePicker = DKImageNames(@"ad_default", @"ad_default_D");
    }
}



@end
