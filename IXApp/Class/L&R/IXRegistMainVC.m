//
//  IXRegistMainVC.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRegistMainVC.h"
#import "IXRegistStep1VC.h"
#import "IXEmailRegStep1VC.h"
#import "IXLoginMainVC.h"
#import "IXRegistStep3VC.h"
#import "AppDelegate+UI.h"
#import "IXCpyConfig.h"
#import <UMMobClick/MobClick.h>
#import "IXPhoneRegistVC.h"
#import "IXEmailRegistVC.h"
@implementation IXRegistMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutSubviews];
    
    if (UMAppKey.length) {
        [MobClick event:UMEnterRegisterPage];   //友盟统计
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)layoutSubviews
{
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    //logo
    UIImageView * icon = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-117)/2,
                                                                       170,
                                                                       117,
                                                                       117)];
    [icon dk_setImagePicker:DKImageNames(@"common_logo", @"common_logo_D")];
    [self.view addSubview:icon];
    
    //提示
    UILabel * btLb = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                               kScreenHeight-30-13,
                                                               kScreenWidth,
                                                               13)];
    [btLb setFont:PF_MEDI(12)];
    [btLb setTextColor:MarketSymbolCodeColor];
    btLb.dk_textColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
    [btLb setTextAlignment:NSTextAlignmentCenter];
    [btLb setText:LocalizedString(@"差价合约（CFD）交易涉及到高度的亏损风险")];
    [self.view addSubview:btLb];
    
    //使用现有账户登录按钮
    UIButton    * accBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [accBtn setTitle:LocalizedString(@"使用现有用户登录") forState:UIControlStateNormal];
    [accBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
    [accBtn.titleLabel setFont:PF_MEDI(12)];
    [accBtn addTarget:self action:@selector(accBtnClk)
     forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:accBtn];
    accBtn.frame = CGRectMake(15,
                              GetView_MinY(btLb) - 46 - 13,
                              kScreenWidth - 2 * 15,
                              13);

    UIImage * image = AutoNightImageNamed(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                       topCapHeight:image.size.height/2];
    
    CGFloat btnWidth = (kScreenWidth - 15*3)/2;
    //注册按钮
    UIButton    * pRegistBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    pRegistBtn.titleLabel.font = PF_REGU(15);
    [pRegistBtn addTarget:self action:@selector(pRegistBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [pRegistBtn setBackgroundImage:image forState:UIControlStateNormal];
    [pRegistBtn setTitle:LocalizedString(@"手机注册") forState:UIControlStateNormal];
    [self.view addSubview:pRegistBtn];
    pRegistBtn.frame = CGRectMake(15,
                                  GetView_MinY(accBtn) - 20 - 43,
                                  btnWidth,
                                  43);
    
    UIButton    * eRegistBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    eRegistBtn.titleLabel.font = PF_REGU(15);
    [eRegistBtn addTarget:self action:@selector(eRegistBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [eRegistBtn setBackgroundImage:image forState:UIControlStateNormal];
    [eRegistBtn setTitle:LocalizedString(@"邮箱注册") forState:UIControlStateNormal];
    [self.view addSubview:eRegistBtn];
    eRegistBtn.frame = CGRectMake(GetView_MaxX(pRegistBtn) + 15,
                                  GetView_MinY(pRegistBtn),
                                  btnWidth,
                                  43);
}

- (void)pRegistBtnClk
{
//    IXRegistStep1VC *vc = [[IXRegistStep1VC alloc] init];
    IXPhoneRegistVC *vc = [IXPhoneRegistVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)eRegistBtnClk
{
//    [self.navigationController pushViewController:[IXEmailRegStep1VC new] animated:YES];
    IXEmailRegistVC *vc = [IXEmailRegistVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)accBtnClk
{
    NSArray * arr = self.navigationController.viewControllers;
    __block UINavigationController  * loginNc = nil;
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[IXLoginMainVC class]]) {
            loginNc = obj;
            *stop = YES;
        }
    }];
    
    if (loginNc) {
        [self.navigationController popToViewController:loginNc animated:YES];
    }else{
        [AppDelegate showLoginMain];
    }
}

@end
