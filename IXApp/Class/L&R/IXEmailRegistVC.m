//
//  IXEmailRegistVC.m
//  IXApp
//
//  Created by Larry on 2018/5/24.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXEmailRegistVC.h"
#import "IXComInputView.h"
#import "UIKit+Block.h"
#import "NSObject+IX.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"
#import "IXADMgr.h"
#import "IXDataCacheMgr.h"
#import "IXBORequestMgr+BroadSide.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Login.h"
#import "IXEmailRegStep2VC.h"
#import "IXEmailRegStep3VC.h"
#import "IXAboutVC.h"
#import "IXLoginMainVC.h"
#import "IXPhoneRegistVC.h"
#import "AppDelegate+UI.h"
#import "FXWheellV.h"

@interface IXEmailRegistVC ()<UITextFieldDelegate>
@property(nonatomic,strong)UIView *bottomV;
@property (nonatomic, strong)UIButton *closeBtn;
@property (nonatomic, strong)FXWheellV  * wheelV;//轮播图

@property(nonatomic,strong)IXComInputView *input_email;
@property(nonatomic,strong)IXComInputView *input_fName;
@property(nonatomic,strong)IXComInputView *input_lName;

@property(nonatomic,copy)NSString *email;
@property(nonatomic,copy)NSString *fName;
@property(nonatomic,copy)NSString *lName;

@end

@implementation IXEmailRegistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[AppsFlyerTracker sharedTracker] trackEvent:@"邮箱注册-填资料页" withValues:nil];
    
    [self.view addSubview:self.closeBtn];
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    
    UIImageView *imgV = [UIImageView new];
    imgV.image = [UIImage imageNamed:@"common_logo"];
    imgV.layer.cornerRadius = 3;
    imgV.layer.masksToBounds = YES;
    [self.view addSubview:imgV];
    [imgV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(30);
        make.size.equalTo(CGSizeMake(90, 90));
        make.centerX.equalTo(0);
    }];
    

    IXComInputView *input_email =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Email" rPlaceHolder:@"Please enter your email address" topLineStyle:TopLineStyleFull bottomLineStyle:BottomStyleSeparate];
    [self.view addSubview:input_email];
    input_email.tf_right.delegate = self;
    
    [input_email makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgV.bottom).offset(20);
    }];
    _input_email = input_email;
    
    
    IXComInputView *input_fName =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"First Name" rPlaceHolder:@"Please enter your first name" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleSeparate];
    [self.view addSubview:input_fName];
    input_fName.tf_right.delegate = self;
    
    [input_fName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_email.bottom);
    }];
    _input_fName = input_fName;
    
    IXComInputView *input_lName =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Last Name" rPlaceHolder:@"Please enter your last name" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleFull];
    [self.view addSubview:input_lName];
    input_lName.tf_right.delegate = self;
    
    [input_lName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_fName.bottom);
    }];
    _input_lName = input_lName;
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:LocalizedString(@"注册") forState:UIControlStateNormal];
    [self.view addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.top.equalTo(input_lName.bottom).offset(20);
        make.height.equalTo(44);
    }];
    weakself;
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        [[AppsFlyerTracker sharedTracker] trackEvent:@"邮箱注册-填资料-提交" withValues:nil];
        [weakSelf gotoNext:LocalizedString(@"注册")];
    }];
    
    [self.bottomV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nextBtn.bottom).offset(20);
    }];

    // 广告
    [self loadAd];
}

- (UIView *)bottomV{
    if (!_bottomV) {
        _bottomV = [UIView new];
        _bottomV.clipsToBounds = YES;
        [self.view addSubview:_bottomV];
        
        [_bottomV makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.height.equalTo(20);
        }];
        
        UILabel *lb_center = [UILabel new];
        lb_center.font = ROBOT_FONT(13);
        lb_center.textColor = HexRGB(0x4c6072);
        lb_center.textAlignment = NSTextAlignmentCenter;
        lb_center.numberOfLines = 0;
        lb_center.layer.borderColor = CellSepLineColor.CGColor;
        lb_center.layer.borderWidth = 1;
        lb_center.text = LocalizedString(@"登录");
        [_bottomV addSubview:lb_center];
        
        [lb_center makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(-2);
            make.width.equalTo(80);
            make.bottom.equalTo(2);
            make.centerX.equalTo(0);
        }];
        
        weakself;
        [lb_center block_whenTapped:^(UIView *aView) {
            // 登录
            [weakSelf gotoNext:LocalizedString(@"登录")];
        }];
        
        UILabel *lb_left = [UILabel new];
        lb_left.font = ROBOT_FONT(13);
        lb_left.textColor = HexRGB(0x4c6072);
        lb_left.textAlignment = NSTextAlignmentCenter;
        lb_left.numberOfLines = 0;
        lb_left.text = LocalizedString(@"关于我们");
        [_bottomV addSubview:lb_left];
        
        [lb_left makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(lb_center.left).equalTo(-20);
            make.centerY.equalTo(0);
        }];
        
        [lb_left block_whenTapped:^(UIView *aView) {
            // 关于
            [weakSelf gotoNext:LocalizedString(@"关于我们")];
        }];
        
        
        UILabel *lb_right = [UILabel new];
        lb_right.font = ROBOT_FONT(12);
        lb_right.textColor = HexRGB(0x4c6072);
        lb_right.textAlignment = NSTextAlignmentCenter;
        lb_right.numberOfLines = 0;
        lb_right.text = LocalizedString(@"手机注册");
        [_bottomV addSubview:lb_right];
        
        [lb_right makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lb_center.right).equalTo(20);
            make.centerY.equalTo(0);
        }];
        
        [lb_right block_whenTapped:^(UIView *aView) {
            // 关于
            [weakSelf gotoNext:LocalizedString(@"手机注册")];
        }];
        
        
        
    }
    return _bottomV;
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    //不接受输入空格
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:NONESPACECHAR];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
//    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    [self registerBtnEnable:[IXAppUtil isValidateEmail:aimStr]];
//    _email = aimStr;
    
    return YES;
}

- (BOOL)checkInput{
    [self.view endEditing:YES];
    
    self.email = _input_email.tf_right.text;
    self.fName = _input_fName.tf_right.text;
    self.lName = _input_lName.tf_right.text;
    
    if ([IXAppUtil isValidateEmail:self.email]) {
        if (SameString(self.email, [[IXUserInfoMgr shareInstance] demoLoginAccount])) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"用户名错误")];
            return NO;
        }
    }else{
        [SVProgressHUD showInfoWithStatus:LocalizedString(@"请输入正确的邮箱号")];
        return NO;
    }
    if (!self.fName.length) {
        [SVProgressHUD showInfoWithStatus:_input_fName.tf_right.placeholder];
        return NO;
    }
    if (!self.lName.length) {
        [SVProgressHUD showInfoWithStatus:_input_lName.tf_right.placeholder];
        return NO;
    }
    
    return YES;
}


- (void)gotoNext:(NSString *)title{
    if (SameString(title, LocalizedString(@"注册"))) {
        if ([self checkInput]) {
            [self requset];
        }
    }
    else if (SameString(title, LocalizedString(@"关于我们"))) {
        [self.navigationController pushViewController:[IXAboutVC new] animated:YES];
        
    }else if (SameString(title, LocalizedString(@"登录"))) {
        IXLoginMainVC * vc = nil;
        for (UIViewController * v in self.navigationController.viewControllers) {
            if ([v isKindOfClass:[IXLoginMainVC class]]) {
                [IXUserInfoMgr shareInstance].isCloseDemo = YES;
                vc = (IXLoginMainVC *)v;
                break;
            }
        }
        if (vc) {
            [self.navigationController popToViewController:vc animated:NO];
        } else {
            [self.navigationController pushViewController:[IXLoginMainVC new] animated:NO];
        }
    }
    else if (SameString(title, LocalizedString(@"手机注册"))) {
        IXPhoneRegistVC *vc = [IXPhoneRegistVC new];
        [self.navigationController pushViewController:vc animated:NO];
    }else if([title isEqualToString:@"步骤二"]) {
        IXEmailRegStep2VC * vc = [[IXEmailRegStep2VC alloc] init];
        vc.email = _email;
        vc.fName = _fName;
        vc.lName = _lName;
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"步骤三"]){
        IXEmailRegStep3VC *vc = [[IXEmailRegStep3VC alloc] init];
        vc.email = _email;
        vc.fName = _fName;
        vc.lName = _lName;
        vc.token = @"";
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)requset{
    IXEmailPhoneModel   * model = [[IXEmailPhoneModel alloc] init];
    model.email = self.email;
    model.index = 0;
    
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    
    weakself;
    [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model
                                          complete:^(BOOL exist, NSString *errStr)
     {
         if (exist) {
             [IXEntityFormatter showErrorInformation:errStr];
         }else{
             [SVProgressHUD dismiss];
             
             BOOL k = VerifyCodeEnable;
             if (k) {
                 [weakSelf gotoNext:@"步骤二"];
             }else{
                 [weakSelf gotoNext:@"步骤三"];
             }
         }
     }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (UIButton *)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeBtn.frame = CGRectMake(kScreenWidth - (40 + 15), 20 , 40, 40);
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_closeBtn setImage:GET_IMAGE_NAME(@"ad_close") forState:UIControlStateNormal];
        _closeBtn.titleLabel.font = PF_REGU(15);
    }
    return _closeBtn;
}

- (void)closeBtnAction
{
    [IXDataCacheMgr clearAccountInfo];
    [IXUserInfoMgr shareInstance].isCloseDemo = NO;
    [AppDelegate showLogin];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //不实现或调用super会导致导航条显示，此处空实现以防止导航条出现
}



#pragma mark 加载广告数据
- (void)loadAd
{
    NSDictionary *paramDic = @{
                               @"pageNo":@(1),
                               @"pageSize":@(20),
                               @"infomationType":@"REGISTER_ADVERTISEMENT"
                               };
    
    weakself;
    [IXBORequestMgr bs_bannerAdvertisementListWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
     {
         if (success && [obj ix_isDictionary] && obj[@"resultList"]) {
             [IXADMgr shareInstance].registAdArr = [obj[@"resultList"] mutableCopy];
             [IXADMgr shareInstance].registBannerClose = [obj[@"bannerClose"] boolValue];
             if ([IXADMgr shareInstance].registAdArr.count) {
                 [weakSelf.view addSubview:weakSelf.wheelV];
             }
         }
     }];
}

- (FXWheellV *)wheelV
{
    if (!_wheelV) {
        _wheelV = [[FXWheellV alloc] initWithFrame:CGRectMake(0,
                                                              kScreenHeight
                                                              - 94,
                                                              kScreenWidth,
                                                              94)];
    }
    _wheelV.items = [IXADMgr shareInstance].registAdArr;
    _wheelV.bannerClose = [IXADMgr shareInstance].registBannerClose;
    return _wheelV;
}


- (void)closeAd
{
    if (_wheelV) {
        [_wheelV removeFromSuperview];
        _wheelV = nil;
    }
}

@end
