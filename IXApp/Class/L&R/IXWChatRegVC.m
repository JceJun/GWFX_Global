//
//  IXWChatRegVC.m
//  IXApp
//
//  Created by Bob on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXWChatRegVC.h"
#import "IXTouchTableV.h"
#import "IXRegistStep1CellA.h"
#import "IXRegistStep1CellB.h"
#import "IXRegistStep2VC.h"
#import "IXRegistStep3VC.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Login.h"

#import "IXWUserInfo.h"
#import "NSDictionary+Type.h"
#import "IXLoginMainVC.h"


@interface IXWChatRegVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong)IXTouchTableV  * tableV;
@property (nonatomic, strong)UIButton   * registBtn;

@property (nonatomic, copy) NSString    * phoneNum;
@property (nonatomic, copy) NSString    * phoneId;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;
@property (nonatomic, strong)NSDictionary * countryDic;

@end

@implementation IXWChatRegVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    self.title = LocalizedString(@"注册用户");
    
#warning 默认中国
    _phoneId = @"62";
    _nameCN = LocalizedString(@"中国");
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}


#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)registBtnClk
{
    [self.view endEditing:YES];
    IXRegistStep1CellB *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    //手机号合法位数最少为5
    if ( cell.textF.text.length < PHONEMINLENGTH ) {
        [SVProgressHUD showErrorWithStatus:PHONEMINCHAR];
        return;
    }
    [self validPhone:cell.textF.text];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        IXRegistStep1CellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep1CellA class])];
        [cell loadUIWithDesc:_nameCN];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        IXRegistStep1CellB *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
        cell.textF.delegate = self;
        [cell.textF becomeFirstResponder];
        [cell loadUIWithTFText:_phoneNum withDesc:[NSString stringWithFormat:@"+ %@",_phoneId] withTag:indexPath.row];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (aimStr.length >= 5) {
        [self registerBtnEnable:YES];
        _phoneNum = aimStr;
    }else{
        [self registerBtnEnable:NO];
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self registerBtnEnable:NO];
    return YES;
}

- (void)registerBtnEnable:(BOOL)enable
{
    if (enable) {
        _registBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        _registBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}


#pragma mark -
#pragma mark - create UI


- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[IXRegistStep1CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellA class])];
        [_tableV registerClass:[IXRegistStep1CellB class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18)/2, kScreenWidth, 18)];
    label.text = LocalizedString(@"请输入手机号码");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKCellTitleColor;
    label.font = PF_MEDI(15);
    [v addSubview:label];
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*2-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    _registBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _registBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_registBtn addTarget:self
                   action:@selector(registBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [_registBtn setTitle:LocalizedString(@"注 册")
                forState:UIControlStateNormal];
    _registBtn.titleLabel.font = PF_REGU(15);
    _registBtn.userInteractionEnabled = NO;
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    [v addSubview:_registBtn];
    
    return v;
}


#pragma mark -
#pragma mark - request

- (void)validPhone:(NSString *)phoneNum
{
    IXEmailPhoneModel *model = [[IXEmailPhoneModel alloc] init];
    model.index = 1;
    model.mobilePhone = phoneNum;
    model.mobilePhonePrefix = _phoneId;
    
    [SVProgressHUD showWithStatus:LocalizedString(@"校验手机号...")];
    
    weakself;
    [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model complete:^(BOOL exist, NSString *errStr) {
        [SVProgressHUD dismiss];
        BOOL k = VerifyCodeEnable;
        if (k) {
            IXRegistStep2VC *vc = [[IXRegistStep2VC alloc] init];
            vc.phoneNum = phoneNum;
            vc.phoneId = _phoneId;
            vc.phoneExist = exist;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else{
            ELog(@"没有配置验证码");
        }
    }];
}

@end
