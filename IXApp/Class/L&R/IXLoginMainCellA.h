//
//  IXLoginMainCellA.h
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

@protocol IXLoginMainCellADelegate <NSObject>

@optional

- (void)textValue:(NSString *)value tag:(NSInteger)tag;
- (void)errMsg:(NSString *)msg;

@end

@interface IXLoginMainCellA : UITableViewCell

@property (nonatomic, strong)IXTextField *textF;

@property (nonatomic, assign)id <IXLoginMainCellADelegate> delegate;

- (void)loadUIWithDesc:(NSString *)desc
                  text:(NSString *)title
           placeHolder:(NSString *)holder
          keyboardType:(UIKeyboardType)type
       secureTextEntry:(BOOL)Entry;
- (void)setTextFieldTag:(NSUInteger)tag;

@end
