//
//  IXRegistStep1CellA.h
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXRegistStep1CellA : UITableViewCell
- (void)loadUIWithDesc:(NSString *)text;
@end

