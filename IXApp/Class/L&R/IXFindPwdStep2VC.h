//
//  IXFindPwdStep2VC.h
//  IXApp
//
//  Created by Evn on 17/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXBORequestMgr+Account.h"

@interface IXFindPwdStep2VC : IXDataBaseVC

@property (nonatomic, strong) IXFindPwdM *psdModel;

@property (nonatomic, assign) pageType  type;

@end
