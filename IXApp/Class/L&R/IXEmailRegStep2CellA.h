//
//  IXEmailRegStep2CellA.h
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SendSMS_Interval 60

@interface IXEmailRegStep2CellA : UITableViewCell

@property (nonatomic, strong) UIButton   *sendBtn;


- (void)reloadUIWithEmail:(NSString *)email;
- (void)sendBtnTitleChanged:(NSInteger)cnt;

@end
