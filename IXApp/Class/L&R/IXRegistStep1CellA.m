//
//  IXRegistStep1CellA.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRegistStep1CellA.h"

@interface IXRegistStep1CellA ()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *desc;

@end
@implementation IXRegistStep1CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    [self desc];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 87.5, 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKGrayTextColor;
        _title.text = LocalizedString(@"国家/地区");
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(110, 14, kScreenWidth - 110, 16)];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKGrayTextColor;
        _desc.text = LocalizedString(@"中国");
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (void)loadUIWithDesc:(NSString *)text
{
    self.desc.text = text;
}

@end
