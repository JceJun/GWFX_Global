//
//  IXPhoneRegistVC.m
//  IXApp
//
//  Created by Larry on 2018/5/24.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXPhoneRegistVC.h"
#import "NSObject+IX.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "IXUserDefaultM.h"
#import "IXADMgr.h"
#import "IXDataCacheMgr.h"
#import "IXBORequestMgr+BroadSide.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Login.h"
#import "IXComInputView.h"
#import "UIKit+Block.h"
#import "IXEmailRegistVC.h"
#import "IXLoginMainVC.h"
#import "IXAboutVC.h"
#import "IXEmailRegistVC.h"
#import "IXRegistStep2VC.h"
#import "IXRegistStep3VC.h"
#import "IXCountryListVC.h"
#import "AppDelegate+UI.h"
#import "FXWheellV.h"
#import "IXBORequestMgr+Comp.h"

@interface IXPhoneRegistVC ()<UITextFieldDelegate>
@property(nonatomic,strong)IXComInputView *input_Country;
@property(nonatomic,strong)IXComInputView *input_areaCode;
@property(nonatomic,strong)IXComInputView *input_fName;
@property(nonatomic,strong)IXComInputView *input_lName;
@property(nonatomic,strong)IXComInputView *input_email;
@property(nonatomic,strong)UIView *bottomV;
@property (nonatomic, strong)UIButton *closeBtn;
@property (nonatomic, strong)FXWheellV  * wheelV;//轮播图

@property(nonatomic,copy)NSString *areaCode;
@property(nonatomic,copy)NSString *phone;
@property(nonatomic,copy)NSString *fName;
@property(nonatomic,copy)NSString *lName;
@property(nonatomic,copy)NSString *email;
@property (nonatomic, strong)IXCountryM    *countryInfo;
@end

@implementation IXPhoneRegistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[AppsFlyerTracker sharedTracker] trackEvent:@"手机注册-填资料页" withValues:nil];
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    [self.view addSubview:self.closeBtn];
    
    UIImageView *imgV = [UIImageView new];
    imgV.image = [UIImage imageNamed:@"common_logo"];
    imgV.layer.cornerRadius = 3;
    imgV.layer.masksToBounds = YES;
    [self.view addSubview:imgV];
    [imgV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(30);
        make.size.equalTo(CGSizeMake(70, 70));
        make.centerX.equalTo(0);
    }];
    
    UILabel *lb_tip = [UILabel new];
    lb_tip.font = ROBOT_FONT(13);
    lb_tip.dk_textColorPicker = DKCellTitleColor;
    lb_tip.textAlignment = NSTextAlignmentCenter;
    lb_tip.textColor = [UIColor redColor];
    lb_tip.text = @"";
    [self.view addSubview:lb_tip];
    [lb_tip makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(0);
        make.top.equalTo(imgV.bottom).offset(15);
    }];
    
    weakself;
    IXComInputView *input_Country =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Country" rPlaceHolder:@"Country Information" topLineStyle:TopLineStyleFull bottomLineStyle:BottomStyleSeparate];
    [input_Country addEnteranceArrow];
    [self.view addSubview:input_Country];
    
    [input_Country block_whenTapped:^(UIView *aView) {
        [weakSelf gotoNext:@"选择国家"];
    }];
    
    [input_Country makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgV.bottom).offset(40);
    }];
    _input_Country = input_Country;
    
    IXComInputView *input_areaCode =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"+86" rPlaceHolder:@"Enter phone number" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleSeparate];
    input_areaCode.tf_right.keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:input_areaCode];
    
    [input_areaCode makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_Country.bottom);
    }];
    _input_areaCode = input_areaCode;
    
    IXComInputView *input_fName =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"First Name" rPlaceHolder:@"Please enter your first name" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleSeparate];
    [self.view addSubview:input_fName];
    
    [input_fName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_areaCode.bottom);
    }];
    _input_fName = input_fName;
    
    IXComInputView *input_lName =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Last Name" rPlaceHolder:@"Please enter your last name" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleFull];
    [self.view addSubview:input_lName];
    
    [input_lName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_fName.bottom);
    }];
    _input_lName = input_lName;
    
    IXComInputView *input_email =  [IXComInputView makeViewInSuperView:self.view lPlaceHolder:@"Email" rPlaceHolder:@"Please enter your email address" topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleFull];
    [self.view addSubview:input_email];
    
    [input_email makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_lName.bottom);
    }];
    _input_email = input_email;
    
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:LocalizedString(@"注册") forState:UIControlStateNormal];
    [self.view addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.top.equalTo(input_email.bottom).offset(20);
        make.height.equalTo(44);
    }];
    
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        [[AppsFlyerTracker sharedTracker] trackEvent:@"手机注册-填资料-提交" withValues:nil];
        [weakSelf gotoNext:LocalizedString(@"注册")];
    }];
    
    [self.bottomV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nextBtn.bottom).offset(15);
    }];
    
    // 广告
    [self loadAd];
    
    // 获取配置信息，更新提示信息
    [IXBORequestMgr comp_requestCompanyConfigInfoWithId:CompanyID complete:^(NSString *errStr, NSString *errCode, id obj) {
        if ([IXBORequestMgr shareInstance].mobileOnlineConfig) {
            NSString *registerTips = [IXBORequestMgr shareInstance].mobileOnlineConfig[@"registerTips"];
            lb_tip.text= registerTips;
        }
    }];
}

- (UIView *)bottomV{
    if (!_bottomV) {
        _bottomV = [UIView new];
        _bottomV.clipsToBounds = YES;
        [self.view addSubview:_bottomV];
        
        [_bottomV makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.height.equalTo(20);
        }];
        
        UILabel *lb_center = [UILabel new];
        lb_center.font = ROBOT_FONT(13);
        lb_center.textColor = HexRGB(0x4c6072);
        lb_center.textAlignment = NSTextAlignmentCenter;
        lb_center.numberOfLines = 0;
        lb_center.layer.borderColor = CellSepLineColor.CGColor;
        lb_center.layer.borderWidth = 1;
        lb_center.text = LocalizedString(@"登录");
        [_bottomV addSubview:lb_center];
        
        [lb_center makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(-2);
            make.width.equalTo(80);
            make.bottom.equalTo(2);
            make.centerX.equalTo(0);
        }];
        
        weakself;
        [lb_center block_whenTapped:^(UIView *aView) {
            // 登录
            [weakSelf gotoNext:LocalizedString(@"登录")];
        }];
        
        UILabel *lb_left = [UILabel new];
        lb_left.font = ROBOT_FONT(13);
        lb_left.textColor = HexRGB(0x4c6072);
        lb_left.textAlignment = NSTextAlignmentCenter;
        lb_left.numberOfLines = 0;
        lb_left.text = LocalizedString(@"关于我们");
        [_bottomV addSubview:lb_left];
        
        [lb_left makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(lb_center.left).equalTo(-20);
            make.centerY.equalTo(0);
        }];
        
        [lb_left block_whenTapped:^(UIView *aView) {
            // 关于
            [weakSelf gotoNext:LocalizedString(@"关于我们")];
        }];
        
        
        UILabel *lb_right = [UILabel new];
        lb_right.font = ROBOT_FONT(13);
        lb_right.textColor = HexRGB(0x4c6072);
        lb_right.textAlignment = NSTextAlignmentCenter;
        lb_right.numberOfLines = 0;
        lb_right.text = LocalizedString(@"邮箱注册");
        [_bottomV addSubview:lb_right];
        
        [lb_right makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lb_center.right).equalTo(20);
            make.centerY.equalTo(0);
        }];
        
        [lb_right block_whenTapped:^(UIView *aView) {
            // 关于
            [weakSelf gotoNext:LocalizedString(@"邮箱注册")];
        }];
        
        
        
    }
    return _bottomV;
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string{
    //不接受输入空格
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:NONESPACECHAR];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    //    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //    [self registerBtnEnable:[IXAppUtil isValidateEmail:aimStr]];
    //    _email = aimStr;
    
    return YES;
}

- (BOOL)checkInput{
    [self.view endEditing:YES];
    
    self.areaCode = [self.input_areaCode.lb_left.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    self.phone = self.input_areaCode.tf_right.text;
    self.fName = self.input_fName.tf_right.text;
    self.lName = self.input_lName.tf_right.text;
    self.email = self.input_email.tf_right.text;
    
    if (!self.input_Country.tf_right.text.length) {
        [SVProgressHUD showInfoWithStatus:self.input_Country.tf_right.placeholder];
        return NO;
    }else if ( self.phone.length < PHONEMINLENGTH ) {
        //手机号合法位数最少为5
        [SVProgressHUD showErrorWithStatus:PHONEMINCHAR];
        return NO;
    }
    if (!self.fName.length) {
        [SVProgressHUD showInfoWithStatus:_input_fName.tf_right.placeholder];
        return NO;
    }
    if (!self.lName.length) {
        [SVProgressHUD showInfoWithStatus:_input_lName.tf_right.placeholder];
        return NO;
    }if (!self.email.length) {
        [SVProgressHUD showInfoWithStatus:_input_email.tf_right.placeholder];
        return NO;
    }
    if (![IXAppUtil isValidateEmail:_email]) {
        [SVProgressHUD showInfoWithStatus:LocalizedString(@"请输入正确的邮箱号")];
        return NO;
    }
    
    return YES;
}

- (void)gotoNext:(NSString *)title{
    if (SameString(title, LocalizedString(@"注册"))) {
        if ([self checkInput]) {
            [self requestForCheckEmail];
        }
    }
    else if (SameString(title, LocalizedString(@"关于我们"))) {
        [self.navigationController pushViewController:[IXAboutVC new] animated:YES];
    }
    else if (SameString(title, LocalizedString(@"登录"))) {
        IXLoginMainVC   * vc = nil;
        for (UIViewController * v in self.navigationController.viewControllers) {
            if ([v isKindOfClass:[IXLoginMainVC class]]) {
                [IXUserInfoMgr shareInstance].isCloseDemo = YES;
                vc = (IXLoginMainVC *)v;
                break;
            }
        }
        
        if (vc) {
            [self.navigationController popToViewController:vc animated:NO];
        } else {
            vc = [IXLoginMainVC new];
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
    else if (SameString(title, LocalizedString(@"邮箱注册"))) {
        [self.navigationController pushViewController:[IXEmailRegistVC new] animated:NO];
    }else if (SameString(title, LocalizedString(@"步骤二"))) {
        IXRegistStep2VC *vc = [[IXRegistStep2VC alloc] init];
        vc.lName = _lName;
        vc.fName = _fName;
        vc.phoneNum = _phone;
        vc.phoneId = _areaCode;
        vc.email = _email;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (SameString(title, LocalizedString(@"步骤三"))) {
        IXRegistStep3VC *vc = [[IXRegistStep3VC alloc] init];
        vc.lName = _lName;
        vc.fName = _fName;
        vc.phoneNum = _phone;
        vc.phoneId = _areaCode;
        vc.email = _email;
        vc.token = @"";
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"选择国家"]){
        weakself;
        IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
            [weakSelf setCountry:country];
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)setCountry:(IXCountryM *)country{
    if (!self.countryInfo) {
        self.countryInfo = [[IXCountryM alloc] init];
    }
    //默认中国
    if (!country) {
        NSString *nationalCode = [IXUserDefaultM getCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityByCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        [IXUserDefaultM saveCountryCode:countryM.nationalCode];

    } else {
        self.countryInfo.nameCN = country.nameCN;
        self.countryInfo.nameEN = country.nameEN;
        self.countryInfo.nameTW = country.nameTW;
        self.countryInfo.countryCode = country.countryCode;
        [IXUserDefaultM saveCountryCode:country.nationalCode];
    }
    
    self.input_Country.tf_right.text = self.countryInfo.nameEN;
    self.input_areaCode.lb_left.text = [@"+" stringByAppendingString:[@(country.countryCode) stringValue]];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)requestForCheckPhone{
    IXEmailPhoneModel *model = [[IXEmailPhoneModel alloc] init];
    model.index = 1;
    model.mobilePhone = _phone;
    model.mobilePhonePrefix = [NSString stringWithFormat:@"%@",_areaCode];
    
    weakself;
    [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model complete:^(BOOL exist, NSString *errStr) {
        if (exist) {
            [SVProgressHUD showErrorWithStatus:errStr];
        }else{
            [SVProgressHUD dismiss];
            
            BOOL k = VerifyCodeEnable;
            if (k) {
                [weakSelf gotoNext:@"步骤二"];
            }else{
                [weakSelf gotoNext:@"步骤三"];
            }
        }
    }];
}

- (void)requestForCheckEmail{
    IXEmailPhoneModel *model = [[IXEmailPhoneModel alloc] init];
    model.index = 0;
    model.email = _email;
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    weakself;
    [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model complete:^(BOOL exist, NSString *errStr) {
        if (exist) {
            [SVProgressHUD showErrorWithStatus:errStr];
        }else{
            [weakSelf requestForCheckPhone];
        }
    }];
}

- (UIButton *)closeBtn{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeBtn.frame = CGRectMake(kScreenWidth - (40 + 15), 20, 40, 40);
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_closeBtn setImage:GET_IMAGE_NAME(@"ad_close") forState:UIControlStateNormal];
        _closeBtn.titleLabel.font = PF_REGU(15);
    }
    return _closeBtn;
}

- (void)closeBtnAction{
//    [IXDataCacheMgr clearAccountInfo];
//    [IXUserInfoMgr shareInstance].isCloseDemo = NO;
//    [AppDelegate showLogin];
    [AppDelegate showRoot];
}

- (void)viewWillAppear:(BOOL)animated{
    //处理选择不能国家返回刷新
    [self setCountry:[IXUserDefaultM nationalityByCode:[IXUserDefaultM getCode]]];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark 加载广告数据
- (void)loadAd{
    NSDictionary *paramDic = @{
                               @"pageNo":@(1),
                               @"pageSize":@(20),
                               @"infomationType":@"REGISTER_ADVERTISEMENT"
                               };
    
    weakself;
    [IXBORequestMgr bs_bannerAdvertisementListWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
     {
         if (success && [obj ix_isDictionary] && obj[@"resultList"]) {
             [IXADMgr shareInstance].registAdArr = [obj[@"resultList"] mutableCopy];
             [IXADMgr shareInstance].registBannerClose = [obj[@"bannerClose"] boolValue];
             if ([IXADMgr shareInstance].registAdArr.count) {
                 [weakSelf.view addSubview:weakSelf.wheelV];
             }
         }
     }];
}

- (FXWheellV *)wheelV{
    if (!_wheelV) {
        _wheelV = [[FXWheellV alloc] initWithFrame:CGRectMake(0,
                                                              kScreenHeight
                                                              - 94,
                                                              kScreenWidth,
                                                              94)];
    }
    _wheelV.items = [IXADMgr shareInstance].registAdArr;
    _wheelV.bannerClose = [IXADMgr shareInstance].registBannerClose;
    return _wheelV;
}

@end
