//
//  IXRegistStep3CellA.h
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXRegistStep3CellA : UITableViewCell

- (void)reloadUIWithPhoneNum:(NSString *)phoneNum;

@end
