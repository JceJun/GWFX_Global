//
//  IXEmailRegStep2CellA.m
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXEmailRegStep2CellA.h"

@interface IXEmailRegStep2CellA ()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *desc;

@end

@implementation IXEmailRegStep2CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 12, 87.5, 20)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKGrayTextColor;
        _title.text = LocalizedString(@"电子邮箱");
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(110-35,
                                                          12,
                                                          kScreenWidth - GetView_MaxX(self.title) - 110+35,
                                                          20)];
        _desc.font = RO_REGU(15);
        _desc.dk_textColorPicker = DKCellContentColor;
        [self.contentView addSubview:_desc];
        
        UIView *div = [[UIView alloc] initWithFrame:CGRectMake(GetView_MaxX(_desc),
                                                               0,
                                                               .5f,
                                                               44)];
        div.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:div];
    }
    
    return _desc;
}

- (UIButton *)sendBtn
{
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sendBtn.frame = CGRectMake(kScreenWidth - 110, 0, 110, 44);
        [_sendBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        [_sendBtn.titleLabel setFont:PF_MEDI(13)];
        [self.contentView addSubview:_sendBtn];
    }
    return _sendBtn;
}

- (void)reloadUIWithEmail:(NSString *)email
{
    self.desc.text = email;
    self.desc.userInteractionEnabled = NO;
}

- (void)sendBtnTitleChanged:(NSInteger)cnt
{
    if( cnt == SendSMS_Interval ){
        _sendBtn.enabled = YES;
        [self.sendBtn setTitle:LocalizedString(@"重新发送") forState:UIControlStateNormal];
        [_sendBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
    } else{
        _sendBtn.enabled = NO;
        [self.sendBtn setTitle:[NSString stringWithFormat:@"%@(%lu)",
                                LocalizedString(@"重新发送"),
                                (unsigned long)cnt]
                      forState:UIControlStateNormal];
        [_sendBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    }
}


@end
