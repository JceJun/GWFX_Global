//
//  IXProtocolVC.m
//  IXApp
//
//  Created by Bob on 2017/5/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXProtocolVC.h"
#import "IXTouchTableV.h"
#import "IXBORequestMgr+Account.h"

#import "IXAcntStep5AVC.h"
#import "IXAcntStep4CellA.h"
#import "IXCpyConfig.h"

@interface IXProtocolVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) IXTouchTableV *tableV;

@end

@implementation IXProtocolVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"条款协议");

    [self.tableV reloadData];
    [self loadProtocolList];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

//请求数据
- (void)loadProtocolList
{
    [SVProgressHUD showWithStatus:LocalizedString(@"数据加载中...")];
    
    [IXBORequestMgr acc_protocolListWithParam:nil
                                       result:^(BOOL success,
                                                NSString *errCode,
                                                NSString *errStr,
                                                id obj)
    {
        if (success) {
            if ([obj isKindOfClass:[NSDictionary class]] && [[obj allKeys] containsObject:@"titleList"]) {
                [SVProgressHUD dismiss];
                self.titleArr = (NSArray *)(obj[@"titleList"]);
                [self.tableV reloadData];
            } else {
                [SVProgressHUD dismiss];
            }
        } else {
            if ([errCode length]) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        }
    }];
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntStep4CellA *cell = [tableView dequeueReusableCellWithIdentifier:
                              NSStringFromClass([IXAcntStep4CellA class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dic = _titleArr[indexPath.row];
    [cell loadUIWithText:dic[@"title"]
               logoImage:GET_IMAGE_NAME(@"openAccount_customProfile")
               iconImage:nil
            isHiddenIcon:NO];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntStep5AVC *VC = [[IXAcntStep5AVC alloc] init];
    VC.idStr = [(NSDictionary *)_titleArr[indexPath.row] objectForKey:@"id"];
    VC.acntTitle = [(NSDictionary *)_titleArr[indexPath.row] objectForKey:@"title"];
    if (!VC.idStr) {
        return;
    }
    [self.navigationController pushViewController:VC animated:YES];
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight - kBtomMargin)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXAcntStep4CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXAcntStep4CellA class])];
    }
    return _tableV;
}

@end
