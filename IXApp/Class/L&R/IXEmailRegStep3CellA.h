//
//  IXEmailRegStep3CellA.h
//  IXApp
//
//  Created by Evn on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXEmailRegStep3CellA : UITableViewCell

- (void)reloadUIWithEmail:(NSString *)email;

@end
