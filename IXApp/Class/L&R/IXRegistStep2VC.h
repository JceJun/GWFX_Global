//
//  IXRegistStep2VC.h
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"


@interface IXRegistStep2VC : IXDataBaseVC

@property (nonatomic, assign) BOOL phoneExist;

@property (nonatomic, copy) NSString *phoneNum;

@property (nonatomic, strong) NSString *phoneId;

@property(nonatomic,copy)NSString *fName;
@property(nonatomic,copy)NSString *lName;
@property(nonatomic,copy)NSString *email;

@end
