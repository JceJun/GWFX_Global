//
//  IXNewGuideV.h
//  IXApp
//
//  Created by Evn on 2017/8/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,IXNewGuide) {
    IXNewGuideBroadside,
    IXNewGuideCata,
    IXNewGuidePosition,
    IXNewGuideKLineDetail,
    IXNewGuideKlineLandscape,
    IXNewGuideDomIn,
    IXNewGuideDomMp,
    IXNewGuideDomLimit,
    IXNewGuideDomClick
};

@protocol IXNewGuideVDelegate <NSObject>

- (void)nextRemoveViewType:(IXNewGuide)type;

@end

@interface IXNewGuideV : UIView

@property (nonatomic, assign) id <IXNewGuideVDelegate> delegate;

- (void)showNewGuideType:(IXNewGuide)type;

@end
