//
//  IXRegistStep3VC.h
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXRegistStep3VC : IXDataBaseVC
@property(nonatomic,strong)NSString *fName;
@property(nonatomic,strong)NSString *lName;
@property (nonatomic, copy) NSString    * phoneNum;
@property (nonatomic, copy) NSString    * token;
@property (nonatomic, copy) NSString    * phoneId;
@property(nonatomic,copy)NSString *email;

- (void)loginAfterRegister;

@end
