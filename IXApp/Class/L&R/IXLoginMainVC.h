//
//  IXLoginMainVC.h
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import <UMSocialCore/UMSocialCore.h>

@interface IXLoginMainVC : IXDataBaseVC

@property (nonatomic, strong)   NSString *userName;
@property (nonatomic, strong)   NSString *passWord;
@property (nonatomic, assign)   BOOL isAutoLogin;//是否自动登录

- (void)registAction;

@end
