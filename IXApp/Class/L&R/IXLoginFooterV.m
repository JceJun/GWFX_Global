
//
//  IXLoginFooterV.m
//  IXApp
//
//  Created by Seven on 2017/5/10.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXLoginFooterV.h"
#import <UMSocialCore/UMSocialCore.h>
#import "IXCpyConfig.h"

@implementation IXLoginFooterV

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createSubview];

        self.dk_backgroundColorPicker = DKViewColor;
    }
    
    return self;
}

- (void)createSubview
{
    UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    //登录按钮
    UIButton    * loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(14.5, 40, kScreenWidth - 29, 44);
    [loginBtn addTarget:self
                 action:@selector(loginAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [loginBtn setBackgroundImage:image
                        forState:UIControlStateNormal];
    [loginBtn setTitle:LocalizedString(@"登 录") forState:UIControlStateNormal];
    loginBtn.titleLabel.font = PF_REGU(15);
    [self addSubview:loginBtn];
    
    CGFloat width   = [IXEntityFormatter getContentWidth:LocalizedString(@"注册用户")
                                                WithFont:PF_MEDI(12)];
    CGFloat originX =  kScreenWidth/2 - width/2;
    CGFloat height  = 25;
    CGFloat originY = 40 + 44 + 15; //self.frame.size.height - 30 - height
    //注册用户
    UILabel * rLb = [[UILabel alloc] initWithFrame:CGRectMake(originX,
                                                              originY,
                                                              width,
                                                              height)];
    rLb.userInteractionEnabled = YES;
    rLb.dk_textColorPicker = DKCellTitleColor;
    rLb.textAlignment = NSTextAlignmentCenter;
    rLb.font = PF_MEDI(12);
    rLb.text = LocalizedString(@"注册用户");
    UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                         action:@selector(registAction)];
    [rLb addGestureRecognizer:tg];
    [self addSubview:rLb];
    
    //分割线
    UIView  *lineL = [[UIView alloc] initWithFrame:CGRectMake( originX - 16, VIEW_Y(rLb)+5, 1, 15)];
    lineL.dk_backgroundColorPicker = DKLineColor;
    [self addSubview:lineL];
    
    width = kScreenWidth/2 - (VIEW_W(rLb)/2 + 32);
    //忘记密码
    UILabel * qLb = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                              GetView_MinY(rLb),
                                                              width,
                                                              height)];
    qLb.dk_textColorPicker = DKCellTitleColor;
    qLb.textAlignment = NSTextAlignmentRight;
    qLb.userInteractionEnabled = YES;
    qLb.font = PF_MEDI(12);
    qLb.text = LocalizedString(@"忘记密码");
    UITapGestureRecognizer  * qtg = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(findPsdAction)];
    [qLb addGestureRecognizer:qtg];
    [self addSubview:qLb];
    
    originX = kScreenWidth/2 + VIEW_W(rLb)/2 + 16;
    
    //分割线
    UIView  * lineR = [[UIView alloc] initWithFrame:CGRectMake(originX, VIEW_Y(lineL), 1, 15)];
    lineR.dk_backgroundColorPicker = DKLineColor;
    [self addSubview:lineR];
    
    width = kScreenWidth - originX;
    //联系我们
    UILabel * rLb1 = [[UILabel alloc] initWithFrame:CGRectMake(VIEW_X(lineR) + height,
                                                               GetView_MinY(rLb),
                                                               width,
                                                               height)];
    rLb1.userInteractionEnabled = YES;
    rLb1.dk_textColorPicker = DKCellTitleColor;
    rLb1.textAlignment = NSTextAlignmentLeft;
    rLb1.font = PF_MEDI(12);
    rLb1.text = LocalizedString(@"联系我们");
    UITapGestureRecognizer *tg1 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(contactUs)];
    [rLb1 addGestureRecognizer:tg1];
    [self addSubview:rLb1];
}


#pragma mark -
#pragma mark - action

- (void)loginAction:(id)sender
{
    if (self.actionBlock) {
        self.actionBlock(LoginFooterActionTypeLogin);
    }
}

- (void)findPsdAction
{
    if (self.actionBlock) {
        self.actionBlock(LoginFooterActionTypeForgetPwd);
    }
}

- (void)registAction
{
    if (self.actionBlock) {
        self.actionBlock(LoginFooterActionTypeRegist);
    }
}

- (void)contactUs
{
    if (self.actionBlock) {
        self.actionBlock(LoginFooterActionTypeContactUs);
    }
}



@end
