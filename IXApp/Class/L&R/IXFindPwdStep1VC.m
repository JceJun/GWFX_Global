//
//  IXFindPwdStep1VC.m
//  IXApp
//
//  Created by Evn on 17/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXFindPwdStep1VC.h"
#import "IXTouchTableV.h"
#import "IXRegistStep1CellB.h"
#import "IXRegistStep1CellC.h"
#import "IXFindPwdStep2VC.h"
#import "IXFindPwdStep3VC.h"
#import "IXPickerChooseView.h"
#import "IXCountryListVC.h"

#import "IXAppUtil.h"
#import "IXBORequestMgr+Account.h"
#import "IXCpyConfig.h"
#import "IXUserDefaultM.h"


@interface IXFindPwdStep1VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV     * tableV;
@property (nonatomic, strong) UIButton          * submitBtn;

@property (nonatomic, strong)IXCountryM    *countryInfo;
@property (nonatomic, strong)IXFindPwdM    *psdModel;

@end

@implementation IXFindPwdStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.title = LocalizedString(@"找回密码");
    [self setCountry:[IXUserDefaultM nationalityByCode:[IXUserDefaultM getCode]]];
    
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    self.view.dk_backgroundColorPicker = DKTableColor;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}


#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submitBtnClk
{
    [self.view endEditing:YES];
    IXRegistStep1CellB  * cell = nil;
    if (_type == pageTypePhoneFind) {
        cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        _psdModel.mobilePhone = cell.textF.text;
    }else{
        cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        _psdModel.email = cell.textF.text;
    }
    
    [cell.textF resignFirstResponder];
    [self verifyPhoneOrEmail:cell.textF.text];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _type == pageTypePhoneFind ? 2 : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_type == pageTypePhoneFind && indexPath.row == 0) {
        static NSString *identifier = @"IXRegistStep1CellC";
        IXRegistStep1CellC *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[IXRegistStep1CellC alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title.dk_textColorPicker = DKGrayTextColor;
        cell.desc.dk_textColorPicker = DKCellTitleColor;
        
        [cell loadUIWithDesc:[self.countryInfo localizedName]];
        return cell;
    }else{
        IXRegistStep1CellB *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSString    * mobilePrefix = [NSString stringWithFormat:@"+ %ld",(long)self.countryInfo.countryCode];
        NSString    * desc = _type == pageTypePhoneFind ? mobilePrefix : LocalizedString(@"电子邮箱");
        [cell loadUIWithTFText:self.psdModel.mobilePhone withDesc:desc withTag:indexPath.row];

        if (_userName.length) {
            cell.textF.text = _userName;
        }
        
        if (_type == pageTypeEmailFind) {
            cell.textF.placeholder = LocalizedString(@"请输入电子邮箱地址");
            cell.textF.keyboardType = UIKeyboardTypeEmailAddress;
            [cell setDescLabFont:PF_MEDI(13)];
        }else{
            cell.textF.keyboardType = UIKeyboardTypeNumberPad;
            [cell setDescLabFont:RO_REGU(15)];
        }
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        cell.textF.delegate = self;
        
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_type == pageTypePhoneFind &&
        indexPath.row == 0) {
        [self selectCountry];
    }
}

- (void)selectCountry
{
    weakself;
    IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
        [weakSelf setCountry:country];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setCountry:(IXCountryM *)country
{
    if (!self.countryInfo) {
        self.countryInfo = [[IXCountryM alloc] init];
    }
    //默认中国
    if (!country) {
        NSString *nationalCode = [IXUserDefaultM getCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityByCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        [IXUserDefaultM saveCountryCode:countryM.nationalCode];
//        if ([nationalCode isEqualToString:@"CN"]) {
//            self.countryInfo.countryCode = 86;
//        }
    } else {
        self.countryInfo.nameCN = country.nameCN;
        self.countryInfo.nameEN = country.nameEN;
        self.countryInfo.nameTW = country.nameTW;
        self.countryInfo.countryCode = country.countryCode;
        [IXUserDefaultM saveCountryCode:country.nationalCode];
        [self.tableV reloadData];
        _tableV.tableFooterView = [self tableFooterV];
//        if (self.countryInfo.countryCode == 86) {
//            [self.tableV reloadData];
//            _tableV.tableFooterView = [self tableFooterV];
//        } else {
//            [self.tableV reloadData];
//            _tableV.tableFooterView = [self tableFooterV];
//        }
        
    }
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ((_type == pageTypePhoneFind && aimStr.length >= 5) ||
        (_type == pageTypeEmailFind && [IXAppUtil isValidateEmail:aimStr])) {
        [self submitBtnEnable:YES];
    }else{
        [self submitBtnEnable:NO];
    }
    
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self submitBtnEnable:NO];
    return YES;
}

- (void)submitBtnEnable:(BOOL)enable
{
    if (enable) {
        _submitBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        _submitBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}


#pragma mark -
#pragma mark - getter

- (IXFindPwdM *)psdModel
{
    if (!_psdModel) {
        _psdModel = [[IXFindPwdM alloc] init];
    }
    return _psdModel;
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[IXRegistStep1CellC class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellC class])];
        [_tableV registerClass:[IXRegistStep1CellB class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18)/2, kScreenWidth, 18)];
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKCellTitleColor;
    label.font = PF_MEDI(15);
    [v addSubview:label];
    
    if (_type == pageTypePhoneFind){
        label.text = LocalizedString(@"请输入手机号码");
    }else{
        label.text = LocalizedString(@"请输入电子邮箱地址");
    }

    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*2-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_submitBtn addTarget:self action:@selector(submitBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_submitBtn setTitle:LocalizedString(@"提 交") forState:UIControlStateNormal];
    _submitBtn.titleLabel.font = PF_REGU(15);
    _submitBtn.userInteractionEnabled = _userName.length > 0;
    
    UIImage *image = nil;
    if (_userName.length) {
        image = AutoNightImageNamed(@"regist_btn_enable");
    }else{
        image = GET_IMAGE_NAME(@"regist_btn_unable");
    }
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    [v addSubview:_submitBtn];
    
    return v;
}


#pragma mark -
#pragma mark - navigation

- (void)popToValidCode
{
    IXFindPwdStep2VC *vc = [[IXFindPwdStep2VC alloc] init];
    vc.psdModel = _psdModel;
    vc.type = _type;
    [self.navigationController pushViewController:vc animated:YES];
}

//跳过验证码
- (void)jumpCode {
    IXFindPwdStep3VC *vc = [[IXFindPwdStep3VC alloc] init];
    vc.psdModel = _psdModel;
    vc.type = _type;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark -
#pragma mark - request

- (void)verifyPhoneOrEmail:(NSString *)string
{
    _psdModel.mobilePhonePrefix = [NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode];
    IXEmailPhoneModel *model = [[IXEmailPhoneModel alloc] init];
    if (_type == pageTypePhoneFind) {
        model.index = 1;
        model.mobilePhone = string;
        model.mobilePhonePrefix = _psdModel.mobilePhonePrefix;
    }else{
        model.index = 0;
        model.email = string;
        if (SameString(string, [[IXUserInfoMgr shareInstance] demoLoginAccount])) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"用户名错误")];
            return;
        }
    }
    
    NSString    * toastStr = _type == pageTypePhoneFind ?
    LocalizedString(@"校验手机号...") : LocalizedString(@"校验邮箱...");
    [SVProgressHUD showWithStatus:toastStr];
    
    weakself;
    [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model complete:^(BOOL exist, NSString *errStr) {
        if (!exist) {
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            }else{
                NSString    * toastStr = _type == pageTypePhoneFind ?
                LocalizedString(@"手机号不存在") : LocalizedString(@"邮箱不存在");
                [SVProgressHUD showErrorWithStatus:toastStr];
            }
        }else{
            //邮箱(ErrorOA09)或手机号(ErrorOA08)已被注册
            BOOL k = VerifyCodeEnable;
            if (k) {
                [weakSelf popToValidCode];
            }else{
                [weakSelf jumpCode];
            }
        }
    }];
}

@end
