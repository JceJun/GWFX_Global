//
//  IXRegistStep2CellA.m
//  IXApp
//
//  Created by Magee on 2016/12/14.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXRegistStep2CellA.h"

@interface IXRegistStep2CellA ()

@property (nonatomic, strong) UILabel   * titleLab;
@property (nonatomic, strong) UILabel   * desc;

@end

@implementation IXRegistStep2CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 87.5, 16)];
        _titleLab.font = RO_REGU(15);
        _titleLab.dk_textColorPicker = DKGrayTextColor;
        _titleLab.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_titleLab];
    }
    return _titleLab;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(110,
                                                          14,
                                                          kScreenWidth-GetView_MaxX(self.titleLab)-110,
                                                          16)];
        _desc.font = RO_REGU(15);
        _desc.dk_textColorPicker = DKGrayTextColor;
        [self.contentView addSubview:_desc];
        
        UIView *div = [[UIView alloc] initWithFrame:CGRectMake(GetView_MaxX(_desc), 0, .5f, 44)];
        div.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:div];
    }
    return _desc;
}

- (UIButton *)sendBtn
{
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sendBtn.frame = CGRectMake(kScreenWidth-110, 0, 110, 44);
        [_sendBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        [_sendBtn addTarget:self action:@selector(sendBtnClk) forControlEvents:UIControlEventTouchUpInside];
        [_sendBtn.titleLabel setFont:PF_MEDI(13)];
        [self.contentView addSubview:_sendBtn];
    }
    return _sendBtn;
}

- (void)reloadUIWithPhoneNum:(NSString *)phoneNum
                     phoneId:(NSString *)phoneId
{
    self.titleLab.text = [NSString stringWithFormat:@"+ %@",phoneId];
    self.desc.text = phoneNum;
    self.desc.userInteractionEnabled = NO;
}

- (void)sendBtnTitleChanged:(NSInteger)cnt
{
    if( cnt == SendSMS_Interval ){
        _sendBtn.enabled = YES;
        [self.sendBtn setTitle:LocalizedString(@"重新发送") forState:UIControlStateNormal];
        [_sendBtn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
    }else{
        _sendBtn.enabled = NO;
        [self.sendBtn setTitle:[NSString stringWithFormat:@"%@(%lu)",LocalizedString(@"重新发送"),(unsigned long)cnt]
                      forState:UIControlStateNormal];
        [_sendBtn dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    }
}

- (void)sendBtnClk
{
    if ([self.delegate respondsToSelector:@selector(sendBtnClicked)]) {
        [self.delegate sendBtnClicked];
    }
}

#pragma mark -
#pragma mark - setter

- (void)setTitle:(NSString *)title
{
    _title = title;
    _titleLab.text = title;
}

@end
