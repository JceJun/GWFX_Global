//
//  IXRegistStep1CellC.h
//  IXApp
//
//  Created by Evn on 2017/11/29.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

@interface IXRegistStep1CellC : UITableViewCell

@property (nonatomic, strong) UILabel   * desc;
@property (nonatomic, strong) UILabel   * title;
- (void)loadUIWithDesc:(NSString *)desc;

@end
