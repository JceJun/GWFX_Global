//
//  IXBORequestMgr+Record.m
//  IXApp
//
//  Created by Seven on 2017/8/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Record.h"
#import "RSA.h"
#import "NSDictionary+json.h"
#import "IXCpyConfig.h"
#import "NSArray+IX.h"
#import "IXUserInfoMgr.h"
#import "NSObject+IX.h"
#import "IXUserInfoM.h"

#define kAppDepositProposal     @"/app/appGetDepositProposalDealPage" //入金
#define kAppWithdrawProposal    @"/app/appGetWithdrawProposalPage" //出金
#define kAppTradeReport         @"/app/appGetTradeReportPage"   //交易记录
#define kAppDailyStatementList  @"/app/getDailyStatementList"   //结单列表

#define kIncashInfo         [NSString stringWithFormat:@"inCashRecord_%llu",\
[IXUserInfoMgr shareInstance].userLogInfo.account.id_p]
#define kWithDrawInfo       [NSString stringWithFormat:@"withDrawRecord_%llu",\
[IXUserInfoMgr shareInstance].userLogInfo.account.id_p]
#define kTradeRecordInfo    [NSString stringWithFormat:@"tradeRecord_%llu",\
[IXUserInfoMgr shareInstance].userLogInfo.account.id_p]
#define kStatementList      [NSString stringWithFormat:@"statement_%llu",\
[IXUserInfoMgr shareInstance].userLogInfo.account.id_p]

@implementation IXBORequestMgr (Record)


#pragma mark -
#pragma mark - request

// 入金明细列表
+(void)record_incomeListWithParam:(NSDictionary *)param result:(respResult)result
{
    NSDictionary    * gts2AccDic = @{
                                     @"tradeAccountId":[NSString stringWithFormat:@"%llu",
                                                        [IXUserInfoMgr shareInstance].userLogInfo.account.id_p]
                                     };
    NSString    * url = [NSString formatterUTF8:kAppDepositProposal];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"pageNo=%@&pageSize=%@&param=%@&_url=%@&_timestamp=%@",
                               param[@"pageNo"],
                               param[@"pageSize"],
                               [IXDataProcessTools dictionaryToJson:gts2AccDic],
                               url,
                               timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest postIncomeListWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (result) {
                result(success1,errCode,errStr,obj1);
            }
            if ([obj1 ix_isDictionary] && [obj1[@"data"] ix_isArray]) {
                [self cacheCashRecord:obj1[@"data"] fileName:kIncashInfo];
            }
        }];
    }];
}

// 出金明细列表
+(void)record_drawCashListWithParam:(NSDictionary *)param result:(respResult)result
{
    NSDictionary *gts2AccDic = @{
                                 @"tradeAccountId":[NSString stringWithFormat:@"%llu",
                                                    [IXUserInfoMgr shareInstance].userLogInfo.account.id_p]
                                 };
    NSString    * url = [NSString formatterUTF8:kAppWithdrawProposal];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"pageNo=%@&pageSize=%@&param=%@&_url=%@&_timestamp=%@",
                               param[@"pageNo"],
                               param[@"pageSize"],
                               [gts2AccDic jsonString],
                               url,
                               timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest postDrawCashListWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (result) {
                result(success1,errCode,errStr,obj1);
            }
            
            if ([obj1 ix_isDictionary] && [obj1[@"data"] ix_isArray]) {
                [self cacheCashRecord:obj1[@"data"] fileName:kWithDrawInfo];
            }
        }];
    }];
}

// 交易记录列表
+(void)record_tradeRecordListWithParam:(NSDictionary *)param result:(respResult)result
{
    NSDictionary    * tradeAccDic = @{
                                      @"tradeAccountId":[NSString stringWithFormat:@"%llu",
                                                         [IXUserInfoMgr shareInstance].userLogInfo.account.id_p]
                                      };
    NSString    * url = [NSString formatterUTF8:kAppTradeReport];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"pageNo=%@&pageSize=%@&param=%@&_url=%@&_timestamp=%@",
                               param[@"pageNo"],
                               param[@"pageSize"],
                               [IXDataProcessTools dictionaryToJson:tradeAccDic],
                               url,
                               timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary * paramDic = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign
                                };
    
    [IXAFRequest postTradeRecordListWithParam:paramDic
                                       result:^(BOOL respont, BOOL success, id obj)
    {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             if (result) {
                 result(success1,errCode,errStr,obj1);
             }
             
             if ([obj1 ix_isDictionary] && [obj1[@"data"] ix_isArray]) {
                 [self cacheTradeRecord:obj1[@"data"]];
             }
         }];
    }];
}

/**
 获取结单列表
 
 @param pageNo 页码
 @param size 页面大小
 @param complete 请求完成回调
 */
+ (void)record_statementListWithPageNo:(NSInteger)pageNo
                              pageSize:(NSInteger)size complete:(void(^)(NSArray * list,
                                                                         NSString * errStr))complete
{
    NSString    * url = [NSString formatterUTF8:kAppDailyStatementList];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"pageNo=%ld&pageSize=%ld&gts2CustomerId=%ld&_url=%@&_timestamp=%@",
                               pageNo,
                               size,
                               (long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId,
                               url,
                               timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary * paramDic = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign
                                };
    [IXAFRequest dailyStatementListWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             __block NSMutableArray * statementList = [@[] mutableCopy];
             if ([obj1 ix_isArray]) {
                 [obj1 enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull dic, NSUInteger idx, BOOL * _Nonnull stop) {
                     if ([dic ix_isDictionary]) {
                         [statementList addObject:[IXDailyStatement statementWithDic:dic]];
                     }
                 }];
             }
             
             if (complete) {
                 complete (statementList, errStr);
             }
         }];
    }];
}


#pragma mark -
#pragma mark - 缓存相关

+ (NSMutableArray *)getCachedInCashRecord
{
    return [self getCashRecord:kIncashInfo];
}
+ (NSMutableArray *)getCachedWithDrawRecord
{
    return [self getCashRecord:kWithDrawInfo];
}

//入金记录
+ (void)cacheCashRecord:(NSArray *)jsonArr fileName:(NSString *)fileName
{
    if (!jsonArr || !jsonArr.count) {
        return;
    }
    
    NSMutableArray * arr = [self getCashRecord:fileName];
    if (!arr) {
        arr = [@[] mutableCopy];
    }
    
    if (arr.count) {
        //去重，使用新数据替代就数据
        [jsonArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj,
                                              NSUInteger idx,
                                              BOOL * _Nonnull stop)
        {
            [arr enumerateObjectsWithOptions:NSEnumerationReverse
                                  usingBlock:^(NSDictionary * _Nonnull obj1,
                                               NSUInteger idx1,
                                               BOOL * _Nonnull stop1)
            {
                if ([obj[@"pno"] isEqualToString:obj[@"pno"]]) {
                    [arr removeObject:obj1];
                    *stop1 = YES;
                }
            }];
        }];
    }
    
    [arr addObjectsFromArray:jsonArr];
    
    [arr sortUsingComparator:^NSComparisonResult(NSDictionary * _Nonnull obj1,
                                                 NSDictionary * _Nonnull obj2)
    {
        return ![obj1[@"pno"] compare:obj2[@"pno"]];
    }];
    
    NSString    * path = [[self cachePath] stringByAppendingString:fileName];
    arr = [arr ix_dealWithNullValue];
    BOOL    suc = [arr writeToFile:path atomically:YES];
    if (!suc) {
        ELog(@"出入金记录写入本地失败");
    }
}

+ (NSMutableArray *)getCashRecord:(NSString *)fileName
{
    NSString    * path = [[self cachePath] stringByAppendingString:fileName];
    return [NSMutableArray arrayWithContentsOfFile:path];
}

//成交记录
+ (void)cacheTradeRecord:(NSArray *)jsonArr
{
    if (!jsonArr || !jsonArr.count) {
        return;
    }
    
    NSMutableArray  * arr = [self getCachedTradeRecord];
    if (!arr) {
        arr = [NSMutableArray array];
    }
    
    if (arr.count) {        
        [jsonArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj,
                                              NSUInteger idx,
                                              BOOL * _Nonnull stop)
        {
            [arr enumerateObjectsWithOptions:NSEnumerationReverse
                                  usingBlock:^(NSDictionary * _Nonnull obj1,
                                               NSUInteger idx1,
                                               BOOL * _Nonnull stop1)
            {
                if ([obj[@"dealId"] integerValue] == [obj1[@"dealId"] integerValue]) {
                    [arr removeObject:obj1];
                    *stop1 = YES;
                }
            }];
        }];
    }
    
    [arr addObjectsFromArray:jsonArr];
    
    [arr sortUsingComparator:^NSComparisonResult(NSDictionary * _Nonnull obj1,
                                                 NSDictionary * _Nonnull obj2)
    {
        return [obj1[@"dealId"] integerValue] < [obj2[@"dealId"] integerValue];
    }];
    
    NSString    * path = [[self cachePath] stringByAppendingString:kTradeRecordInfo];
    
    arr = [arr ix_dealWithNullValue];
    BOOL suc = [arr writeToFile:path atomically:YES];
    if (!suc) {
        ELog(@"交易记录写入本地失败");
    }
}

+ (NSMutableArray *)getCachedTradeRecord
{
    NSString    * path = [[self cachePath] stringByAppendingString:kTradeRecordInfo];
    return [NSMutableArray arrayWithContentsOfFile:path];
}

//日结单记录
+ (void)cacheDailyStatement:(NSArray *)jsonArr
{
    if (!jsonArr || !jsonArr.count) {
        return;
    }
    
    NSMutableArray  * arr = [self getCachedStatementRecord];
    if (!arr) {
        arr = [@[] mutableCopy];
    }
    
    if (arr.count) {
        [jsonArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj,
                                              NSUInteger idx,
                                              BOOL * _Nonnull stop)
        {
            [arr enumerateObjectsWithOptions:NSEnumerationReverse
                                  usingBlock:^(NSDictionary * _Nonnull obj1, NSUInteger idx1, BOOL * _Nonnull stop1) {
                if ([obj[@"id"] integerValue] ==  [obj1[@"id"] integerValue]) {
                    [arr removeObject:obj1];
                    *stop1 = YES;
                }
            }];
        }];
    }
    
    [arr addObjectsFromArray:jsonArr];
    arr = [arr ix_dealWithNullValue];
    
    [arr sortUsingComparator:^NSComparisonResult(NSDictionary * _Nonnull obj1, NSDictionary * _Nonnull obj2) {
        return [obj1[@"id"] integerValue] < [obj2[@"id"] integerValue];
    }];
    
    NSString    * path = [[self cachePath] stringByAppendingString:kStatementList];
    BOOL suc = [arr writeToFile:path atomically:YES];
    
    if (!suc) {
        ELog(@"日结单列表写入本地失败");
    }
}

+ (NSMutableArray *)getCachedStatementRecord
{
    NSString    * path = [[self cachePath] stringByAppendingString:kStatementList];
    return [NSMutableArray arrayWithContentsOfFile:path];
}

@end
