//
//  NSDictionary+json.m
//  IXApp
//
//  Created by Seven on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "NSDictionary+json.h"

@implementation NSDictionary (json)

- (NSString *)jsonString
{
    if (!self) {
        return nil;
    }
    
    NSString    * jsonStr = @"";
    NSError * err = nil;
    NSData  * data = [NSJSONSerialization dataWithJSONObject:self
                                                     options:NSJSONWritingPrettyPrinted
                                                       error:&err];
    
    if (!err) {
        jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    
    return jsonStr;
}
- (NSString *)toJSONString {
    //  NSJSONReadingMutableLeaves
    NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                   options:NSJSONReadingAllowFragments
                                                     error:nil];
    
    if (data == nil) {
        return nil;
    }
    
    NSString *string = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
    return string;
}

- (NSString *)toReadableJSONString {
    NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:nil];
    
    if (data == nil) {
        return nil;
    }
    
    NSString *string = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
    return string;
}

- (NSData *)toJSONData {
    NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:nil];
    
    return data;
}

@end
