//
//  IXBORequestMgr+MsgCenter.m
//  IXApp
//
//  Created by Seven on 2017/8/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+MsgCenter.h"
#import "IXCpyConfig.h"
#import "IXAppUtil.h"
#import "NSArray+IX.h"
#import "NSObject+IX.h"
#import "RSA.h"
#import "IXUserInfoM.h"

static  NSString    * kMsgTypeSourceVersion = @"ixMsgTypeSourceVersion";
static  NSString    * kFileName = @"ixMsgTypes";

#define kGetMessageTypeList         @"/app/appGetMessageTypeList" //获取消息类型列表
#define kMessageByParentTypePage    @"/app/appGetMessageByParentTypePage" //获取消息列表
#define kMessageDetail              @"/app/appGetMessageDetail" //查看消息详情
#define kUpdateAllMsgStatusToRead   @"/app/appUpdateAllMsgStatusToRead" //將消息状态全部更新为已读
#define kUnreadCountByParentType    @"/app/appGetUnreadCountByParentType" //查询未读消息数量
#define kUpdateMessageLang          @"/app/appUpdateMessageLang"  //用户消息/邮件语种修改

@implementation IXBORequestMgr (MsgCenter)

/** 获取全部未读消息条数 */
+ (void)msg_obtainAllNoReadCount:(void(^)(BOOL success, NSInteger count))complete
{
    NSString    * url       = [NSString formatterUTF8:@"/app/getAppUnreadMessageCount"];
    NSString    * paramStr  = [NSString stringWithFormat:@"_url=%@&tradeUserId=%@",
                               url,
                               @([IXUserInfoMgr shareInstance].userLogInfo.account.userid)];
    NSString *paramSign     = [RSA encryptString:paramStr
                                       publicKey:CompanyKey];
    
    NSDictionary *param = @{
                            @"loginName":CompanyLoginName,
                            @"param":paramSign,
                            };
    
    [IXAFRequest getUnreadMsgCountWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        if ([obj ix_isDictionary] && [obj[@"newRet"] isEqualToString:@"OK"]) {
            if (complete) {
                complete(YES, [obj[@"result"] integerValue]);
            }
        } else {
            NSString    * err = [NSString stringWithFormat:@"消息中心未读数量获取失败 -- %@ --",obj];
            ELog(err);
            if (complete) {
                complete (NO, 0);
            }
        }
    }];
}

/**
 获取未读条数
 
 @param type    消息类型，使用服务器返回的字符串 (IXMsgCenterTypeM code)
 该参数为nil或@""时，则请求全部消息未读数
 @param complete 请求结束
 */
+ (void)msg_obtainNoReadCountWithType:(NSString *)type complete:(void(^)(NSInteger count))complete
{
    if (!type)  type = @"";
    
    //未读条数
    NSString    * url       = [NSString formatterUTF8:kUnreadCountByParentType];
    NSString    * paramStr  = [NSString stringWithFormat:@"_url=%@&tradeUserId=%@&lang=%@&parentMsgType=%@",
                               url,
                               @([IXUserInfoMgr shareInstance].userLogInfo.account.userid),
                               [IXLocalizationModel currentCheckLanguage],
                               type
                               ];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign,
                                };
    
    [IXAFRequest getUnreadMsgCountWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        NSInteger   count = 0;
        if ([obj ix_isDictionary] && [obj[@"newRet"] isEqualToString:@"OK"]) {
            NSDictionary    * context = obj[@"context"];
            if ([context ix_isDictionary]) {
                id data = context[@"data"];
                if ([data ix_isString]) {
                    count = [data integerValue];
                } else {
                    ELog([NSString stringWithFormat:@"消息中心未读数量获取失败 -- %@ --",obj]);
                }
            } else {
                ELog([NSString stringWithFormat:@"消息中心未读数量获取失败 -- %@ --",obj]);
            }
        } else {
            ELog([NSString stringWithFormat:@"消息中心未读数量获取失败 -- %@ --",obj]);
        }
        
        if (complete) {
            complete(count);
        }
    }];
}


+ (void)b_appGetUnreadCountByParentType:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString  *url = @"/app/appGetUnreadCountByParentType";
    
    // 参数节点 _principal_
    NSDictionary *_principal_ = @{@"loginName":@"",
                                  @"remoteIpAddress":@"",
                                  @"invoke":@"",
                                  @"companyId":@"",
                                  };
    
    // 汇总总参数
    NSDictionary *param = @{@"tradeUserId":@"",
                            @"tradeAccountId":@"",
                            @"parentMsgType":@"",
                            @"lang":@"",
                            @"args":@"",
                            @"_principal_":[IXDataProcessTools dictionaryToJson:_principal_],
                            @"_signature_":@"3aa63aef1fb34840ee946b4f025c7650",
                            @"_resultMap_":@"false",
                            };
    
//    [IXDataProcessTools dictionaryToJson:dic]
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        rsp(success1,errCode1,errStr1,obj1);
    }];
}



/**
 刷新消息类型
 
 @param complete 网络请求成功后刷新
 @return 缓存消息类型
 */
+ (NSArray *)msg_refreshMsgTypes:(void(^)(NSArray <NSString *>* types,NSString * errorStr))complete
{
    __block NSMutableArray  * typeArr = [self cachedMsgTypes];
    
    id  sourceVersion = [[NSUserDefaults standardUserDefaults] objectForKey:kMsgTypeSourceVersion];
    if (!sourceVersion) {
        sourceVersion = @0;
    }
    
    NSString    * url = [NSString formatterUTF8:kGetMessageTypeList];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&sourceVersion=%@",
                              url,
                              sourceVersion];
    
    [IXAFRequest getMsgTypesWithParam:[self requestParamWith:paramStr] resule:^(BOOL respont, BOOL success, id obj) {
        NSString    * errStr = @"";
        
        if ([obj ix_isDictionary] &&[obj[@"newRet"] isEqualToString:@"OK"]) {
            NSDictionary    * context = obj[@"context"];
            if ([context ix_isDictionary]) {
                NSDictionary    * data = context[@"data"];
                if ([data ix_isDictionary]) {
                    if (data[@"version"]) {
                        [[NSUserDefaults standardUserDefaults] setObject:obj[@"newComment"]
                                                                  forKey:kMsgTypeSourceVersion];
                    }
                    NSArray     * retArr = data[@"data"];
                    if ([retArr ix_isArray] && retArr.count) {
                        [self saveMsgTypes:retArr];
                        [typeArr removeAllObjects];
                        [retArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            if ([obj ix_isDictionary]) {
                                [typeArr addObject:[IXMsgCenterTypeM msgTypeWithDic:obj]];
                            }
                        }];
                    }
                }
            }
            if (complete) {
                complete(typeArr,errStr);
            }
        }else if ([obj ix_isString]){
            if (complete) {
                complete(typeArr,obj);
            }
        } else {
            errStr = LocalizedString(@"请求失败");
            if (complete) {
                complete(typeArr,errStr);
            }
        }
    }];
    
    return typeArr;
}

/**
 获取消息列表
 
 @param type    消息类型，使用服务器返回的字符串 (IXMsgCenterTypeM code)
 该参数为nil或@""时，则请求全部消息未读数
 @param complete 请求结束
 */
+ (void)msg_obtainMsgListWithType:(NSString *)type
                             page:(NSInteger)page
                         complete:(void(^)(NSArray <IXMsgCenterM *>*list))complete
{
    NSString        * url = [NSString formatterUTF8:kMessageByParentTypePage];
    NSDictionary    * dic = @{
                              @"tradeUserId":@([IXUserInfoMgr shareInstance].userLogInfo.account.userid)
                              };
    
    NSString    * paramStr  = [NSString stringWithFormat:@"_url=%@&pageNo=%ld&pageSize=20&timezone=%@&param=%@&lang=%@&parentMsgType=%@",
                               url,
                               (long)page,
                               [IXAppUtil appTimeZone],
                               [IXDataProcessTools dictionaryToJson:dic],
                               [IXLocalizationModel currentCheckLanguage],
                               type
                               ];
    
    [IXAFRequest getMsgListWithParam:[self requestParamWith:paramStr]
                              result:^(BOOL respont, BOOL success, NSDictionary * obj)
     {
         __block NSMutableArray  * result = [@[] mutableCopy];
         
         if ([obj ix_isDictionary] && [obj[@"newRet"] isEqualToString:@"OK"]) {
             NSDictionary    * context = obj[@"context"];
             if ([context ix_isDictionary]) {
                 NSDictionary    * data = context[@"data"];
                 if ([data ix_isDictionary]) {
                     NSArray     * retArr = data[@"data"];
                     if ([retArr ix_isArray] && retArr.count) {
                         [retArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj,
                                                              NSUInteger idx,
                                                              BOOL * _Nonnull stop)
                          {
                              [result addObject:[IXMsgCenterM msgCenterModelWithDic:obj]];
                          }];
                     } else {
                         ELog([NSString stringWithFormat:@"获取未读消息失败 -- %@ --",obj]);
                     }
                 } else {
                     ELog([NSString stringWithFormat:@"获取未读消息失败 -- %@ --",obj]);
                 }
             } else {
                 ELog([NSString stringWithFormat:@"获取未读消息失败 -- %@ --",obj]);
             }
         }else{
             ELog([NSString stringWithFormat:@"获取未读消息失败 -- %@ --",obj]);
         }
         
         if (complete) {
             complete(result);
         }
     }];
}

/**
 获取消息列表和未读数
 
 @param type    消息类型，使用服务器返回的字符串 (IXMsgCenterTypeM code)
 该参数为nil或@""时，则请求全部消息未读数
 @param page    当前消息页码
 @param complete 请求结束回调
 */
+ (void)msg_obtainMsgListAndNoReadCountWithType:(NSString *)type
                                           page:(NSInteger)page
                                       complete:(void(^)(NSArray <IXMsgCenterM *>*lst,
                                                         NSInteger noReadCount))complete
{
    __block NSInteger   noReadCount = 0;
    __block NSArray     * msgList = nil;
    
    dispatch_group_t group = dispatch_group_create();
    
    //获取未读数
    dispatch_group_enter(group);
    [self msg_obtainNoReadCountWithType:type complete:^(NSInteger count) {
        noReadCount = count;
        dispatch_group_leave(group);
    }];
    
    //获取消息列表
    dispatch_group_enter(group);
    [self msg_obtainMsgListWithType:type page:page complete:^(NSArray<IXMsgCenterM *> *list) {
        msgList = list;
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (complete) {
            complete(msgList,noReadCount);
        }
    });
}


/**
 全部消息标为已读
 
 @param complrete 请求结束回调
 */
+ (void)msg_readAllMsg:(void(^)(BOOL success, NSString * errStr))complete
{
    NSString    * url = [NSString formatterUTF8:kUpdateAllMsgStatusToRead];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&tradeUserId=%@",
                              url,
                              @([IXUserInfoMgr shareInstance].userLogInfo.account.userid)];
    
    [IXAFRequest readAllMessageWithParam:[self requestParamWith:paramStr]
                                  result:^(BOOL respont, BOOL success, id obj)
     {
         NSDictionary    * dic = obj;
         if ([dic ix_isDictionary] && [dic[@"newRet"] isEqualToString:@"OK"]) {
             if (complete) {
                 complete(YES,nil);
             }
         } else if ([obj ix_isString]) {
             if (complete) {
                 complete (NO, obj);
             }
         } else {
             ELog([NSString stringWithFormat:@"消息中心全部标记已读失败 -- %@ -- ",obj]);
             if (complete) {
                 complete(NO,LocalizedString(@"请求失败"));
             }
         }
     }];
}


/**
 获取消息详情
 
 @param msgId 消息id
 @param complete 请求完成回调
 */
+ (void)msg_obtainMsgDetailWithId:(NSInteger)msgId
                         complete:(void(^)(IXMsgCenterM * m,NSString * errStr))complete
{
    NSString    * url       = [NSString formatterUTF8:kMessageDetail];
    NSString    * paramStr  = [NSString stringWithFormat:@"_url=%@&msgId=%ld&timezone=%@&lang=%@",
                               url,
                               (long)msgId,
                               [IXAppUtil appTimeZone],
                               [IXLocalizationModel currentCheckLanguage]
                               ];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign,
                                };
    
    [IXAFRequest getMsgDetailWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        NSDictionary    * dic = obj;
        NSString        * errStr = @"";
        IXMsgCenterM    * m = nil;
        
        if ([dic isKindOfClass:[NSDictionary class]] && [dic[@"newRet"] isEqualToString:@"OK"]) {
            NSDictionary    * context = dic[@"context"];
            if ([context ix_isDictionary]) {
                NSDictionary    * data = context[@"data"];
                if ([data ix_isDictionary]) {
                    m = [IXMsgCenterM msgCenterModelWithDic:data];
                } else {
                    errStr = LocalizedString(@"获取信息失败");
                }
            } else {
                errStr = LocalizedString(@"获取信息失败");
            }
        } else {
            errStr = LocalizedString(@"获取信息失败");
        }
        
        if (complete) {
            complete(m,errStr);
        }
    }];
}

/**
 用户消息/邮件语种修改
 
 @param messageLang 消息id
 @param result 请求完成回调
 */
+ (void)msg_updateMessageLangWithMessageLang:(NSString *)messageLang result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kUpdateMessageLang];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&gts2CustomerId=%ld&messageLang=%@",
                              url,(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId,
                              messageLang];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest updateMessageLangWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}


#pragma mark -
#pragma mark - cache

+ (BOOL)saveMsgTypes:(NSArray <NSDictionary *>*)types
{
    if (!types) {
        return NO;
    }
    
    //处理NULL
    NSMutableArray  * aimArr = [types ix_dealWithNullValue];
    
    NSString    * path = [[self cachePath] stringByAppendingString:kFileName];
    BOOL    success = [aimArr writeToFile:path atomically:YES];
    return  success;
}

+ (NSMutableArray *)cachedMsgTypes
{
    [self configDirectoryChange];
    
    NSString    * path = [[self cachePath] stringByAppendingString:kFileName];
    NSArray     * types = [NSArray arrayWithContentsOfFile:path];
    
    __block NSMutableArray  * typeArr = [@[] mutableCopy];
    
    [types enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj,
                                        NSUInteger idx,
                                        BOOL * _Nonnull stop)
     {
         if ([obj ix_isDictionary]) {
             [typeArr addObject:[IXMsgCenterTypeM msgTypeWithDic:obj]];
         }
     }];
    
    return typeArr;
}

+ (void)configDirectoryChange
{
    NSString    * originPath = [NSHomeDirectory() stringByAppendingString:@"/*Library/IXCacheInfo"];
    NSString    * filePath = [originPath stringByAppendingString:kFileName];
    NSArray * originArr = [NSArray arrayWithContentsOfFile:filePath];

    if (originArr) {
        BOOL    suc = [self saveMsgTypes:originArr];
        if (!suc) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kMsgTypeSourceVersion];
    }
}

+ (id)requestParamWith:(NSString *)paramStr
{
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign,
                                };
    
    return param;
}


@end
