//
//  NSArray+IX.m
//  IXApp
//
//  Created by Seven on 2017/8/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "NSArray+IX.h"

@implementation NSArray (IX)

- (NSMutableArray *)ix_dealWithNullValue
{
    __block NSMutableArray  * aimArr = [NSMutableArray array];
    
    //过滤value为<null>的键值对，value为null将导致数据写入本地失败
    [self enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSMutableDictionary * dic = [obj mutableCopy];
            [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj1, BOOL * _Nonnull stop) {
                if ([obj1 isKindOfClass:[NSNull class]]
                    || ([obj1 isKindOfClass:[NSString class]]
                        &&( [obj1 isEqualToString:@"<null>"]
                           || [obj1 isEqualToString:@"<NULL>"]))) {
                            [dic setObject:@"" forKey:key];
                        }
                
                //递归处理
                if ([obj1 isKindOfClass:[NSArray class]]
                    && [[obj1 firstObject] isKindOfClass:[NSDictionary class]]) {
                    NSArray * arr = [obj1 ix_dealWithNullValue];
                    [dic setObject:arr forKey:key];
                }
            }];
            [aimArr addObject:dic];
        }
    }];
    
    return aimArr;
}

@end
