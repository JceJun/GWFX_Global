//
//  IXBORequestMgr+Comp.h
//  IXApp
//
//  Created by Seven on 2017/11/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"

@interface IXBORequestMgr (Comp)

@property(nonatomic,strong)NSDictionary *mobileOnlineConfig;
@property(nonatomic,strong)NSArray *depositSort;
@property(nonatomic,strong)NSArray *depositAmount;


/** 获取公司列表 */
+ (void)comp_requestCompanyList:(void(^)(NSString * errStr, NSString * errCode, id obj))complete;


/**
 获取公司信息

 @param compId 公司id
 */
+ (void)comp_requestCompanyConfigInfoWithId:(NSInteger)compId complete:(void(^)(NSString * errStr, NSString * errCode, id obj))complete;


/**
 根据用户输入搜索公司

 @param compName 用户输入关键字
 @param complete 完成回调
 */
+ (void)comp_requestCompanyWith:(NSString *)compName complete:(void(^)(NSString * errStr, NSString * errCode, id obj))complete;

/**
 请求顶层公司信息

 @param complete 完成回调
 */
+ (void)comp_requestCompanySympleInfoWith:(NSInteger)companyId complete:(void(^)(NSString * errStr, NSString  * errCode, id obj))complete;


@end
