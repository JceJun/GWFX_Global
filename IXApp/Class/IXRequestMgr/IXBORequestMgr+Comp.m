//
//  IXBORequestMgr+Comp.m
//  IXApp
//
//  Created by Seven on 2017/11/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Comp.h"
#import "NSObject+IX.h"
#import "IXCpyConfig.h"
#import "RSA.h"
#import "IXDataProcessTools.h"
#import "UIViewExt.h"

#define kApiPublicKey @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCB0p2/PBIX2YAY5c0y/17qxuNNAk0tdzo1x6PZLxku5vhZEBg0G3kea7miA+19Ykx4NMw4fn61DDKeWeMLLZYFQ8X1OfsTlomQszL1m9Q1fAch7VbE3dK8UkhHKA+qK0sLHyklg+tfsfAmk+NsUMmK++9hMR367SFhqpLTUvaw3QIDAQAB"

@implementation IXBORequestMgr (Comp)

DYSYNTH_DYNAMIC_PROPERTY_OBJECT(mobileOnlineConfig, setMobileOnlineConfig, RETAIN, NSDictionary *)
DYSYNTH_DYNAMIC_PROPERTY_OBJECT(depositSort, setDepositSort, RETAIN, NSArray *)
DYSYNTH_DYNAMIC_PROPERTY_OBJECT(depositAmount, setDepositAmount, RETAIN, NSArray *)


+ (void)comp_requestCompanyList:(void(^)(NSString * errStr, NSString * errCode, id obj))complete
{
    NSString    * url = [NSString formatterUTF8:@"/app/getCompanyList"];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@",url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:kApiPublicKey];
    NSDictionary    * param = @{
                                @"loginName":@"testApi",
                                @"param":paramSign
                                };
    
    [IXAFRequest companyListWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (complete) {
                complete(errStr,errCode,obj1);
            }
        }];
    }];
}

+ (void)comp_requestCompanyConfigInfoWithId:(NSInteger)compId complete:(void(^)(NSString * errStr, NSString * errCode, id obj))complete
{   // /app/getCompanyProperties4Package
    // /app/getCompanyProperties
    NSString    * url = [NSString formatterUTF8:@"/app/getCompanyProperties4Package"];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&companyId=%ld",url,compId];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:kApiPublicKey];
    NSDictionary    * param = @{
                                @"loginName":@"testApi",
                                @"param":paramSign
                                };
    [IXAFRequest companyListWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             if ([obj1 isKindOfClass:[NSDictionary class]]) {
                 NSString *mobileOnlineConfig = obj1[@"mobileOnlineConfig"];
                 if (mobileOnlineConfig) {
                     NSDictionary *dic = [IXDataProcessTools dictionaryWithJsonString:mobileOnlineConfig];
                     [IXBORequestMgr shareInstance].mobileOnlineConfig = dic;
                     [IXBORequestMgr shareInstance].depositSort =  dic[@"depositSort"];
                     [IXBORequestMgr shareInstance].depositAmount = dic[@"depositAmount"];
                     NSMutableArray *arr = [NSMutableArray array];
                     [[IXBORequestMgr shareInstance].depositSort enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                         if (obj[@"appSort"] && obj[@"getewayName"]) {
                             [arr addObject:obj];
                         }
                     }];
                     [IXBORequestMgr shareInstance].depositSort = [arr mutableCopy];
                     
                     NSArray *tempArr = [[IXBORequestMgr shareInstance].depositSort sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                         if ([obj1[@"appSort"] integerValue] < [obj2[@"appSort"] integerValue]) {
                             return NSOrderedAscending;
                         }else if ([obj1[@"appSort"] integerValue] > [obj2[@"appSort"] integerValue]){
                             return NSOrderedDescending;
                         }else{
                             return NSOrderedSame;
                         }
                     }];
                     [IXBORequestMgr shareInstance].depositSort = [tempArr mutableCopy];
                 }
             }
             if (complete) {
                 complete(errStr,errCode,obj1);
             }
         }];
    }];
}


+ (void)comp_requestCompanyWith:(NSString *)compName complete:(void(^)(NSString * errStr, NSString * errCode, id obj))complete
{
    NSString    * url = [NSString formatterUTF8:@"/app/getCompanyList"];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&param={\"appName\"=\"%@\"}",url,compName];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:kApiPublicKey];
    NSDictionary    * param = @{
                                @"loginName":@"testApi",
                                @"param":paramSign
                                };
    
    [IXAFRequest companyListWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             if (complete) {
                 complete(errStr,errCode,obj1);
             }
         }];
    }];
}

+ (void)comp_requestCompanySympleInfoWith:(NSInteger)companyId complete:(void(^)(NSString * errStr, NSString  * errCode, id obj))complete
{
    NSString    * url = [NSString formatterUTF8:@"/app/getCompanyList"];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&param={\"companyId\"=\"%ld\"}",url,companyId];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:kApiPublicKey];
    NSDictionary    * param = @{
                                @"loginName":@"testApi",
                                @"param":paramSign
                                };
    
    [IXAFRequest companyListWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             if (complete) {
                 complete(errStr,errCode,obj1);
             }
         }];
    }];
}

@end
