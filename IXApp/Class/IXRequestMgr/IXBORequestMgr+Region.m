//
//  IXBORequestMgr+Region.m
//  IXApp
//
//  Created by Seven on 2017/8/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Region.h"
#import "RSA.h"
#import "IXCpyConfig.h"
#import "IXBOModel.h"
#import "NSDictionary+json.h"
#import "NSArray+IX.h"
#import "NSObject+IX.h"

#define kCountryPath @"countryInfo"

static NSMutableDictionary  * countryInfo;
static NSMutableDictionary  * provinceInfo;

static  NSString    * kFileName = @"ixRegionInfo";
static  NSString    * kProvinceHeader = @"ixProvince";

//key
static  NSString    * kOperationState = @"operationState";
static  NSString    * kCityList = @"subCountryDictParamList";
static  NSString    * kCurrentVersion = @"currentVersion";
static  NSString    * kProvinceList = @"countryList";
#define kCountryListUpdateByVersionNo   @"/app/appGetAreaListByVersionNo" //地区版本更新
#define kCountryInterface   @"/app/appGetAllCountryList"    //获取国家列表


@implementation IXBORequestMgr (Region)


+ (NSArray <IXCountryM *>*)region_refreshCountryList:(void(^)(NSArray <IXCountryM *>* list,
                                                              NSString * errStr,
                                                              BOOL hasNewData))complete
{
    [self configCountryInfo];
    
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kCountryInterface];
    
    NSString    * paramStr = [NSString stringWithFormat:@"_timestamp=%ld&_url=%@",
                              ([timeStamp longValue] * 1000),
                              url];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
    [IXAFRequest obtainCountryListUpdateWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        NSString    * errStr = @"";
        BOOL    hasNewData = NO;
        
        if ([obj ix_isDictionary]) {
            NSDictionary    * context = obj[@"context"];
            if ([context ix_isDictionary]) {
                NSArray    * data = context[@"data"];
                if ([data ix_isArray] && data.count) {
                    //保存国家列表信息
                    [self saveContryList:[context mutableCopy]];
                }
            }
            countryInfo = [context mutableCopy];
        } else {
            errStr = LocalizedString(@"请求失败");
        }
        
        if (complete) {
            complete([self countryModelArrWith:countryInfo[@"data"]],errStr,hasNewData);
        }
    }];
    /*
    if ([countryInfo[@"data"] ix_isArray]) {
        return [self countryModelArrWith:countryInfo[@"data"]];
    } else {
        [IXAFRequest obtainCountryListUpdateWithParam:params result:^(BOOL respont, BOOL success, id obj) {
            NSString    * errStr = @"";
            BOOL    hasNewData = NO;
            
            if ([obj ix_isDictionary]) {
                NSDictionary    * context = obj[@"context"];
                if ([context ix_isDictionary]) {
                    NSArray    * data = context[@"data"];
                    if ([data ix_isArray] && data.count) {
                        //保存国家列表信息
                        [self saveContryList:[context mutableCopy]];
                    }
                }
                countryInfo = [context mutableCopy];
            } else {
                errStr = LocalizedString(@"请求失败");
            }
            
            if (complete) {
                complete([self countryModelArrWith:countryInfo[@"data"]],errStr,hasNewData);
            }
        }];
    }*/
    return [self countryModelArrWith:countryInfo[@"data"]];
}


/**
 刷新省份列表（调用增量更新接口）
 
 @param countryCode  国家代码，从IXCountryM中取值
 @param complete 刷新完成
 @return 缓存内容
 */
+ (NSArray <IXProvinceM *>*)region_refreshProvinceWithNationalCode:(NSString *)nationalCode
                                                       countryCode:(NSString *)countryCode
                                                          complete:(void(^)(NSArray <IXProvinceM *>* list,
                                                                            NSString * errStr,
                                                                            BOOL hasNewData))complete
{
    if (!nationalCode) {
        ELog(@"nationalCode is nil");
        return nil;
    }
    
    NSDictionary    * cachedInfo = [self cachedProvinceListWith:nationalCode];
    
    NSString    * countryVersion = @"-1";
    NSString    * totalVersion = @"0";
    
    //本地无省份列表缓存，则请求原接口获取列表
    //若存在，则请求增量更新接口获取增量
    if (cachedInfo){
        countryVersion = [self provinceVersionWithNationalCode:nationalCode];
        if ([countryVersion containsString:@"null"]) {
            countryVersion = @"-1";
        }
    }
    
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kCountryListUpdateByVersionNo];
    
    NSDictionary    * map = @{nationalCode:countryVersion};
    NSString    * paramStr = [NSString stringWithFormat:@"_timestamp=%ld&lang=%@&cpid=%d&_url=%@&totalVersion=%@&codeAndVersionMap=%@",
                              ([timeStamp longValue] * 1000),
                              [IXLocalizationModel currentCheckLanguage],
                              CompanyID,
                              url,
                              totalVersion,
                              [map jsonString]];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest obtainCountryListUpdateWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        NSString    * errStr = @"";
        if ([obj ix_isDictionary]){
            NSDictionary * context = obj[@"context"];
            if ([context ix_isDictionary]) {
                NSDictionary * data = context[@"data"];
                if ([data ix_isDictionary]) {
                    NSArray * arr = data[@"provinceAndCityList"];
                    if (!cachedInfo) {
                        NSMutableDictionary * aimDic = [@{} mutableCopy];
                        [aimDic setValue:arr forKey:kProvinceList];
                        [aimDic setValue:data[kCurrentVersion] forKey:kCurrentVersion];
                        [self saveProvinceInfo:aimDic nationalCode:nationalCode];
                        
                        if (complete) {
                            complete ([self provinceModelArrWith:arr], errStr, YES);
                        }
                    }
                    //增量数据
                    else if ([arr ix_isArray] && arr.count > 0){
                        NSMutableDictionary * aimDic = [self dealWithNewProvinceWith:[cachedInfo mutableCopy]
                                                                             newData:arr
                                                                         countryCode:countryCode];
                        NSString    * verson = [NSString stringWithFormat:@"%@",data[kCurrentVersion]];
                        [aimDic setValue:verson forKey:kCurrentVersion];
                        
                        //缓存操作
                        [self saveProvinceInfo:aimDic nationalCode:nationalCode];
                        if (complete) {
                            complete ([self provinceModelArrWith:aimDic[kProvinceList]], errStr, YES);
                        }
                    }
                } else {
                    errStr = LocalizedString(@"请求失败");
                }
            } else {
                errStr = LocalizedString(@"请求失败");
            }
        }else{
            //参考标准json
            errStr = LocalizedString(@"请求失败");
        }
        
        if (complete) {
            NSArray * list = [self provinceModelArrWith:cachedInfo[kProvinceList]];
            if (list.count) {
                [provinceInfo setValue:list forKey:nationalCode];
            }
            
            complete (list, errStr, NO);
        }
        
    }];
    
    //若内存中包含
    if (provinceInfo && provinceInfo[nationalCode]) {
        return provinceInfo[nationalCode];
    }else{
        provinceInfo = [NSMutableDictionary dictionary];
        NSArray * list = [self provinceModelArrWith:cachedInfo[kProvinceList]];
        
        if (list.count) {
            [provinceInfo setValue:list forKey:nationalCode];
        }
        
        return list;
    }
}

/**
 处理省市增量，将增量添加进缓存数据
 
 @param originInfo 原属缓存数据
 @param newList 增量省市数据
 @param code 国家code，可据此参数判断新增数据元是否为省份
 @return 处理后的新数据
 */
+ (NSMutableDictionary *)dealWithNewProvinceWith:(NSMutableDictionary *)originInfo
                                         newData:(NSArray <NSDictionary *>*)newList
                                     countryCode:(NSString *)code
{
    // ### 服务器返回的省份列表保存在countyrList字段中 ###
    
    //优先处理key-value中，value为null的情况
    __block NSMutableArray  * newCityList = [newList ix_dealWithNullValue];
    
    //筛选出新增省份，block处理之后，cityList中只保存有新增的省份管辖之下的市
    //若有新增省，则肯定会有新增市，但这种情况很少见，为减少程序复杂度，不单独对newList中的数据进行归类处理
    __block NSMutableArray  * newProvinceArr = [@[] mutableCopy];
    __block NSMutableArray  * originProvinceArr = [originInfo[kProvinceList] mutableCopy];
    
    [newCityList enumerateObjectsWithOptions:NSEnumerationReverse
                                  usingBlock:^(NSDictionary * _Nonnull obj,
                                               NSUInteger idx,
                                               BOOL * _Nonnull stop)
     {
         if (obj[@"parentCode"]) {
             //code是国家code，[obj[@"parentCode"] isEqualToString:code]判断为真说明obj为“省”
             if ([obj[@"parentCode"] isEqualToString:code]) {
                 //obj 为省级行政单位
                 [newProvinceArr addObject:obj];
                 
                 //匹配到重复的省份，则在原数组中删除
                 [originProvinceArr enumerateObjectsWithOptions:NSEnumerationReverse
                                                     usingBlock:^(NSDictionary *  _Nonnull obj1,
                                                                  NSUInteger idx1,
                                                                  BOOL * _Nonnull stop1)
                  {
                      if ([obj[@"code"] isEqualToString:obj1[@"code"]]) {
                          [originProvinceArr removeObject:obj1];
                          
                          if ([obj[kOperationState] isEqualToString:@"del"]) {
                              //删除省份
                              [newProvinceArr removeObject:obj];
                          } else {
                              [obj setValue:obj1[kCityList] forKey:kCityList];
                          }
                          *stop1 = YES;
                      }
                  }];
             }
         }
     }];
    
    [newCityList removeObjectsInArray:newProvinceArr];
    [originProvinceArr addObjectsFromArray:newProvinceArr];
    
    //将省份转字典转成可变字典
    [originProvinceArr enumerateObjectsWithOptions:NSEnumerationReverse
                                        usingBlock:^(NSDictionary *  _Nonnull obj,
                                                     NSUInteger idx,
                                                     BOOL * _Nonnull stop)
     {
         [originProvinceArr removeObject:obj];
         [originProvinceArr addObject:[obj mutableCopy]];
     }];
    
    //将新增省份辖区下的市添加进缓存数据中
    //新增市是随机的，新增省会放在列表最后，为减少计算量，故省份列表逆序遍历
    [originProvinceArr enumerateObjectsWithOptions:NSEnumerationReverse
                                        usingBlock:^(NSMutableDictionary *  _Nonnull obj,
                                                     NSUInteger idx, BOOL * _Nonnull stop)
     {
         //obj为省级行政单位
         __block NSMutableArray * originCityList = [obj[kCityList] mutableCopy];
         if (!originCityList) {
             originCityList = [@[] mutableCopy];
         }
         [newCityList enumerateObjectsWithOptions:NSEnumerationReverse
                                       usingBlock:^(NSDictionary *  _Nonnull newCityInfo,
                                                    NSUInteger idx1,
                                                    BOOL * _Nonnull stop1)
          {
              //obj1为市级行政单位
              if ([newCityInfo[@"parentCode"] isEqualToString:obj[@"code"]]){
                  //城市去重
                  __block BOOL    exist = NO;
                  [originCityList enumerateObjectsWithOptions:NSEnumerationReverse
                                                   usingBlock:^(NSDictionary *  _Nonnull cityInfo,
                                                                NSUInteger idx,
                                                                BOOL * _Nonnull stop)
                   {
                       if ([newCityInfo[@"code"] isEqualToString:cityInfo[@"code"]]) {
                           exist = YES;
                           [originCityList removeObject:cityInfo];
                           if (![newCityInfo[kOperationState] isEqualToString:@"del"]) {
                               //如果更新信息不是删除，则插入原数据
                               [originCityList addObject:newCityInfo];
                           }
                           *stop = YES;
                       }
                   }];
                  
                  if (!exist) {
                      [originCityList addObject:newCityInfo];
                  }
              }
          }];
         [obj setValue:originCityList forKey:kCityList];
     }];
    
    [originInfo setValue:originProvinceArr forKey:kProvinceList];
    return originInfo;
}

#pragma mark -
#pragma mark - other

+ (NSString *)provinceVersionWithNationalCode:(NSString *)code
{
    NSDictionary    * provinceDic = [self cachedProvinceListWith:code];
    NSString    * version = @"-1";
    
    if ([provinceDic ix_isDictionary]
        && provinceDic[kCurrentVersion]) {
        version = [NSString stringWithFormat:@"%@",provinceDic[@"currentVersion"]];
    }
    
    return version;
}


+ (void)configCountryInfo
{
    countryInfo = [[NSDictionary dictionaryWithContentsOfFile:[self countryCachedPath]] mutableCopy];
}

+ (NSDictionary *)cachedProvinceListWith:(NSString *)nationalCode
{
    return [NSDictionary dictionaryWithContentsOfFile:[self provinceCachedPathWith:nationalCode]];
}

+ (NSArray <IXCountryM *>*)countryModelArrWith:(NSArray <NSDictionary *>*)arr
{
    if (!arr || ![arr isKindOfClass:[NSArray class]]) {
        return @[];
    }
    
    __block NSMutableArray  * list = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj ix_isDictionary]) {
            [list addObject:[IXCountryM countryMWithDic:obj]];
        }
    }];
    
    return list;
}

+ (NSArray <IXProvinceM *>*)provinceModelArrWith:(NSArray <NSDictionary *>*)arr
{
    if (!arr || [arr isKindOfClass:[NSNull class]]) {
        return @[];
    }
    
    __block NSMutableArray  * list = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj ix_isDictionary] && ![obj[kOperationState] isEqualToString:@"del"]) {
            [list addObject:[IXProvinceM provinceWithDic:obj]];
        }
    }];
    
    [list sortUsingComparator:^NSComparisonResult(IXProvinceM * _Nonnull obj1, IXProvinceM * _Nonnull obj2) {
        return [obj1.nameEN compare:obj2.nameEN];
    }];
    
    return list;
}

#pragma mark -
#pragma mark - 缓存相关

//------------------------------ 缓存路径 ------------------------------
/**
 获取国家列表缓存路径
 
 @return 缓存路径
 */
+ (NSString *)countryCachedPath
{
    NSString    * path = [self cachePath];
    return [path stringByAppendingString:kFileName];
}

/**
 根据国家code获取省份列表缓存路径
 
 @param nationalCode 国家code 从IXCountryM中取值
 @return    缓存路径
 */
+ (NSString *)provinceCachedPathWith:(NSString *)nationalCode
{
    NSString    * fileName = [kProvinceHeader stringByAppendingFormat:@"_%@",nationalCode];
    NSString    * path = [self cachePath];
    
    return [path stringByAppendingString:fileName];
}

//------------------------------ 存取 ------------------------------

+ (void)saveContryList:(NSMutableDictionary *)countryListInfo
{
    NSArray * arr = (NSArray *)countryListInfo[@"data"];
    NSMutableArray  * aimArr = [arr ix_dealWithNullValue];
    
    NSMutableDictionary * aimDic = [countryListInfo mutableCopy];
    [aimDic setValue:aimArr forKey:@"data"];
    
    NSString    * filePath = [self countryCachedPath];
    BOOL success = [aimDic writeToFile:filePath atomically:YES];
    
    if (!success) {
        ELog(@" -- save country info failure --");
    }
}

+ (void)saveProvinceInfo:(NSDictionary *)info nationalCode:(NSString *)code
{
    if (!info || ![info isKindOfClass:[NSDictionary class]] || !info[kProvinceList]) {
        return;
    }
    
    NSMutableDictionary * dic = [info mutableCopy];
    @try{
        NSArray     * arr = [info[kProvinceList] ix_dealWithNullValue];
        [dic setValue:arr forKey:kProvinceList];
    }@catch(NSException *exception) {
        
    }
    
    
    NSString    * filePath = [self provinceCachedPathWith:code];
    
    BOOL success = [dic writeToFile:filePath atomically:YES];
    
    if (!success) {
        DLog(@" -- save province info failure --");
    }
}

//66获取地区以及根据版本更新
+ (void)b_getAreaListByNationalCode:(NSString *)nationalCode rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString    * url = @"/app/appGetAreaListByVersionNo";
    
    NSDictionary *map = @{nationalCode:@"-1"};
    NSString *param = [NSString stringWithFormat:@"totalVersion=0&cpid=%@&lang=%@&codeAndVersionMap=%@",@(CompanyID),[IXLocalizationModel currentCheckLanguage],[map toJSONString]];
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
    
}


@end

