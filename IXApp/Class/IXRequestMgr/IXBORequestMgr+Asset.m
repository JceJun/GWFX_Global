//
//  IXBORequestMgr+Asset.m
//  IXApp
//
//  Created by Seven on 2017/8/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Asset.h"
#import "IXWUserInfo.h"
#import "SFHFKeychainUtils.h"
#import "IXKeyConfig.h"
#import "IXCpyConfig.h"
#import "IXIncomeValidateModel.h"
#import "NSString+FormatterPrice.h"
#import "IXAccountBalanceModel.h"
#import "NSDictionary+json.h"
#import "NSObject+IX.h"
#import "RSA.h"
#import "AppDelegate+UI.h"
#import "NSArray+JSON.h"
#import "IXBORequestMgr+Comp.h"

#define kCacheBankPath          @"bankInfo"    //银行缓存文件名
#define kGetDepositUrl          @"/app/appGetDepositUrl"    //入金基础url
#define kDepositDataValidate    @"/app/appIsDepositDataValidate"   //入金校验
#define kCustomerAccountViewNew @"/app/appGetCustomerAccountViewNew" //1.取款实时账户信息查询
#define kAddWithdraw            @"/app/appAddWithdraw" //取款
#define kSignGrants             @"/app/appSignGrants" //签到
#define kCheckSignGrants        @"/app/appCheckIsSignGrants"//检查签到
#define kCancelCustomerBanks    @"/app/appCancelCustomerBanks" //用户银行卡解绑
#define kCashOutFee             @"/app/appGetCashOutFee" //获取出金手续费
#define kCashOutFeeConfig       @"/app/appGetCashOutFeeConfig" //获取出金手续费规则
#define kCustomerWouldCashOut   @"/app/getCustomerWouldCashOutByWitCard" //用户能否取款

#import "UIViewExt.h"

@implementation IXBORequestMgr (Asset)
DYSYNTH_DYNAMIC_PROPERTY_OBJECT(channelList, setChannelList, RETAIN, NSArray *)
DYSYNTH_DYNAMIC_PROPERTY_OBJECT(channelDic, setChannelDic, RETAIN, NSDictionary *)



/**
 签到
 
 @param complete 请求完成回调
 */
+ (void)signIn:(void(^)(NSInteger amount,signInType type, NSString * errStr))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kSignGrants];
    NSString    * paramStr = [NSString stringWithFormat:@"_timestamp=%ld&_url=%@&gts2CustomerId=%ld",
                              ([timeStamp longValue] * 1000),
                              url,
                              (long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest signInWith:params result:^(BOOL respont, BOOL success, id obj) {
        signInType  type;
        NSString    * errStr = @"";
        NSInteger   amount = 0;
        
        if ([obj ix_isDictionary] && [obj[@"context"] ix_isDictionary]){
            NSDictionary    * ctx = obj[@"context"];
            if ([ctx ix_isDictionary]) {
                NSDictionary    * data = ctx[@"data"];
                if ([data ix_isDictionary] && [data[@"isSign"] boolValue]) {
                    type = signInTypeComplete;
                    amount = [data[@"baseAmount"] integerValue] + [data[@"expireAmount"] integerValue];
                } else {
                    type = signInTypeAlreadySign;
                    errStr = LocalizedString(@"今天已经签到");
                }
            } else {
                type = signInTypeAlreadySign;
                errStr = LocalizedString(@"今天已经签到");
            }
        }else{
            type = signInTypeFailure;
            errStr = LocalizedString(@"签到失败");
            ELog([NSString stringWithFormat:@"签到失败 -- %@",obj]);
        }
        
        if (complete) {
            complete(amount, type, errStr);
        }
    }];
}

/**
 当天是否完成签到
 
 @param complete 请求完成回调
 */
+ (void)alreadySignIn:(void(^)(BOOL alreadySignIn, NSString * errStr))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kCheckSignGrants];
    NSString    * paramStr = [NSString stringWithFormat:@"_timestamp=%ld&_url=%@&gts2CustomerId=%ld",
                              ([timeStamp longValue] * 1000),
                              url,
                              (long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest alreadySignInWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        NSString    * errStr = @"";
        BOOL signd = NO;
        
        if ([obj ix_isDictionary]) {
            NSDictionary * ctx = obj[@"context"];
            if ([ctx ix_isDictionary]) {
                signd = [ctx[@"isSign"] boolValue];
            }
        }else{
            errStr = LocalizedString(@"请求失败");
            ELog([NSString stringWithFormat:@"查询签到结果失败 == %@",obj]);
        }
        
        if (complete) {
            complete(signd, errStr);
        }
    }];
}


#pragma mark -
#pragma mark - request

/** 获取入金基础url */
+ (void)incash_requstInCashBaseUrl:(void(^)(IXBaseUrlM * urlM))complete
{
    NSString    * url = [NSString formatterUTF8:kGetDepositUrl];
    NSString    * timeStamp = [NSString stringWithFormat:@"%ld",
                               (long)([[NSDate date] timeIntervalSince1970] * 1000)];
    NSString    * paramStr  = [NSString stringWithFormat:@"_url=%@&_timestamp=%@",
                               url,timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign
                                };
    
    [IXAFRequest postUrlListWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        IXBaseUrlM  * m = nil;
        if (respont && success) {
            if ([obj ix_isDictionary]
                && [obj[@"newRet"] isEqualToString:@"OK"]) {
                NSDictionary    * context = obj[@"context"];
                if ([context ix_isDictionary]) {
                    NSDictionary * result = context[@"data"];
                    m = [IXBaseUrlM baseUrlWithDic:result];
                }
            }
        }
        if (complete) {
            complete(m);
        }
    }];
}

/** 获取可添加的银行列表 */
+ (NSArray <IXBank *>*)incash_requestEnableBankList:(void(^)(NSArray * bankList))complete
{
    [self incash_requstInCashBaseUrl:^(IXBaseUrlM *urlM) {
        if (!urlM.depositSubmitUrl.length) {
            if (complete) {
                complete(nil);
            }
            ELog(@"base url有问题，拉取支持的银行卡列表失败");
            return;
        }
        NSString *url = [NSString stringWithFormat:@"%@/appGetEnableBankList.do?cpid=%d",
                         urlM.depositSubmitUrl,
                         CompanyID];
        [IXAFRequest getBankListWithParam:url result:^(BOOL respont, BOOL success, id obj) {
            __block NSMutableArray  * arr = [@[] mutableCopy];
            if (respont && success) {
                if ([obj ix_isDictionary]
                    && [obj[@"newRet"] isEqualToString:@"OK"]) {
                    NSDictionary * context = obj[@"context"];
                    if ([context ix_isDictionary]) {
                        NSArray * data = context[@"data"];
                        if ([data ix_isArray]) {
                            [self cacheBankList:data];
                            [data enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj,
                                                               NSUInteger idx,
                                                               BOOL * _Nonnull stop)
                            {
                                if ([obj ix_isDictionary]) {
                                    [arr addObject:[IXBank bankWithDic:obj]];
                                }
                            }];
                        }
                    }
                }
            }
            if (complete) {
                complete(arr);
            }
        }];
    }];
    return [self getCachedBankList];
}

/** 获取支付方式 */
+ (void)incash_requestPayMethodWithBank:(NSString *)code
                           complete:(void(^)(NSArray <IXPayMethod *>* methodArr,
                                             IXBaseUrlM * baseUrl))complete
{
    [self incash_requstInCashBaseUrl:^(IXBaseUrlM *urlM) {
        if (!urlM || !urlM.depositSubmitUrl.length) {
            ELog(@" --- 获取支付方式失败 ---");
            if (complete) {
                complete (nil, nil);
            }
            return ;
        }
        
        NSString    * url = [NSString stringWithFormat:@"%@/appGetPayMethodByBank.do?bankAccount=%@&cpid=%d&accid=%ld",
                             urlM.depositSubmitUrl,
                             code,
                             CompanyID,
                             (long)[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
        
        [IXAFRequest getPayMethodWithParam:url result:^(BOOL respont, BOOL success, id obj) {
            __block NSMutableArray  * arr = [@[] mutableCopy];
            if (respont && success && [obj ix_isDictionary]) {
                if ([obj[@"newRet"] isEqualToString:@"OK"]) {
                    NSDictionary    * context = obj[@"context"];
                    NSArray * data = context[@"data"];
                    if ([data ix_isArray]) {
                        [data enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj,
                                                           NSUInteger idx,
                                                           BOOL * _Nonnull stop)
                        {
                            if ([obj ix_isDictionary]) {
                                [arr addObject:[IXPayMethod payMethodWithDic:obj]];
                            }
                        }];
                    }
                }
            }
            
            if (complete) {
                complete ([arr copy], urlM);
            }
            
        }];
    }];
}

/** 获取入金token */
+ (void)incash_requestToken:(void(^)(BOOL success, NSString * token))complete
{
    NSString    * urlStr = [NSString formatterUTF8:[NSString stringWithFormat:@"%@:%d/frontoffice_web/",
                                                    BOSeverIP,
                                                    BOSeverPort]];
    NSDictionary    * paramDic = @{
                                   @"loginname":@([IXBORequestMgr shareInstance].userInfo.gts2CustomerId),
                                   @"_sid":[IXBORequestMgr shareInstance].userInfo.sid,
                                   @"weblinkId":@"PCUI_CUSTOMER_CENTER",
                                   @"fromURL":@"PCUI",
                                   @"toURL":urlStr
                                   };
    
    [IXAFRequest postTokenWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        if (respont && success) {
            if ([obj ix_isString] && [obj length]) {
                complete(YES, obj);
            } else {
                complete(NO, obj);
            }
        } else {
            complete(NO, obj);
        }
    }];
}

+ (void)incash_verifyPaymethodWith:(id)param complete:(void(^)(BOOL success, NSString * token))complete
{
    IXIncomeValidateModel *model = [[IXIncomeValidateModel alloc] init];
    model.accountNo = [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo;
    model.tradeAccountId = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    model.payType = @"mobileEgpay";
    model.depositCurrency = param[@"depositCurrency"];
    model.platform = PLATFORM;
    model.bank = param[@"bank"];
    model.bankAccountNumber = param[@"bankAccountNumber"];
    model.payMethod = param[@"payMethod"];
    model.amount = param[@"amount"];

    NSString    * url = [NSString formatterUTF8:kDepositDataValidate];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"cashInPropertiesVO=%@&_url=%@&_timestamp=%@",
                          [model toJSONString],url,timeStamp];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    
    NSDictionary *paramDic = @{
                               @"loginName":CompanyLoginName,
                               @"param":paramSign
                               };
    [IXAFRequest postPayMethodWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (complete) {
                if (errStr.length) {
                    complete (success1, errStr);
                } else {
                    complete (success1, obj1);
                }
            }
        }];
    }];
}

#define kPayModeUrl @"/app/appGetAllPayMethodByCompany"
+ (void)incash_requestPayModeComplete:(void(^)(NSArray * arr, NSString * errStr))complete
{
    NSString    * url = [NSString formatterUTF8:kPayModeUrl];
    NSString    * paramStr = [NSString stringWithFormat:@"accid=%llu&from=app&_url=%@",
                              [IXUserInfoMgr shareInstance].userLogInfo.account.id_p,
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest postIncashModeWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, NSArray * obj1)
        {
            NSMutableArray  * arr = [@[] mutableCopy];
            if ([obj1 ix_isArray]) {
                for (int i = 0; i < obj1.count; i++) {
                    if ([obj1[i] ix_isDictionary]) {
                        IXIncashPayModeM    * m = [IXIncashPayModeM payModeWithDic:obj1[i]];
                        if ([m.gatewayType containsString:@"cardpay"]) {
                            // 屏蔽微信、支付宝
                            [arr addObject:[IXIncashPayModeM payModeWithDic:obj1[i]]];
                        }
                    }
                }
            }
            
            if (complete) {
                complete (arr, errStr);
            }
            
        }];
    }];
}

#define kBestPayModeUrl @"/app/appGetBestPayMethodByBank"
+ (void)incash_requestBestModeWithBankCode:(NSString *)code complete:(void(^)(IXIncashPayModeM * mode, NSString * errStr, NSString * errCode))complete
{
    NSString    * url = [NSString formatterUTF8:kBestPayModeUrl];
    NSString    * paramStr = [NSString stringWithFormat:@"bankAccount=%@&accid=%llu&from=app&_url=%@",
                              code,
                              [IXUserInfoMgr shareInstance].userLogInfo.account.id_p,
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest postBestPayModeWithParam:paramDic reslt:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, NSDictionary * obj1)
        {
            if ([obj1 ix_isDictionary]) {
                if (complete) {
                    complete ([IXIncashPayModeM payModeWithDic:obj1], errStr,errCode);
                }
            } else {
                if (complete) {
                    complete (nil, errStr, errCode);
                }
            }
        }];
    }];
}

#define kApplyPaymentUrl    @"/app/appEgpayApplyPay"
/** 快捷支付 - 申请支付，验证码由银行发送 */
+ (void)incash_requestApplyPayment:(IXIncashApplyM *)param
                          complete:(void(^)(IXIncashResendMsmM * applyResult, NSString * errStr))complete
{
    param.payType = @"mobileEgpay";
    param.platform = PLATFORM;
    param.lang = [IXLocalizationModel currentCheckLanguage];
    param.createIp = BOSeverIP;

    NSString    * url = [NSString formatterUTF8:kApplyPaymentUrl];
    NSString    * paramStr = [NSString stringWithFormat:@"param=%@&_url=%@",
                              [param toJSONString],
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest incash_applyPayment:paramDic reslt:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (!success1) {
                ELog(@"申请快捷支付出错");
            }
            IXIncashResendMsmM  * result = [IXIncashResendMsmM new];
            if ([obj1 ix_isDictionary] && [[obj1 allKeys] containsObject:@"merchantId"]) {
                result.merchantId = obj1[@"merchantId"];
                result.encryptedText = obj1[@"encryptedText"];
            }
            if (complete) {
                complete (result, errStr);
            }
        }];
    }];
}

#define kConfirmPaymentUrl  @"/app/appEgpayConfirmPay"
/**
 快捷支付 - 确认支付
 @param verifyCode 验证码
 */
+ (void)incash_requestConfirmPayment:(IXIncashConfirmPayM *)confirmParam
                            complete:(void(^)(BOOL paymentSuc, NSString * errStr,  NSString * errCode))complete
{
    NSString    * url = [NSString formatterUTF8:kConfirmPaymentUrl];
    NSString    * paramStr = [NSString stringWithFormat:@"param=%@&_url=%@",
                              [confirmParam toJSONString],
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest incash_confirmPayment:paramDic reslt:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (complete) {
                if ([obj1 ix_isDictionary] && [[obj1 allKeys] containsObject:@"newRet"]) {
                    complete (success1, errStr, obj1[@"newRet"]);
                } else {
                    if ([obj1 ix_isString]) {
                        complete (success1, errStr, obj1);
                    } else {
                        complete (success1, errStr, LocalizedString(@"请求失败"));
                    }
                }
            }
        }];
    }];
}

#define kNewVerifyCodeUrl   @"/app/appEgpayResendSms"
/**
 快捷支付 - 重发短信
 @param applyResult 申请支付方法中得到的支付申请结果
 */
+ (void)incash_requestNewVerifyCode:(IXIncashResendMsmM *)resendParam
                           complete:(void(^)(BOOL success, NSString * errStr))complete
{
    NSString    * url = [NSString formatterUTF8:kNewVerifyCodeUrl];
    NSString    * paramStr = [NSString stringWithFormat:@"param=%@&_url=%@",
                              [resendParam toJSONString],
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest incash_resendVerifyCode:paramDic reslt:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             if (complete) {
                 complete (success1, errStr);
             }
         }];
    }];
}

// 获取支付通道列表
+ (void)b_getPayMethodListByCond:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString  *url = @"/app/appGetPayMethodListByCond";
    NSDictionary *param = @{@"tradeAccountId":@([IXUserInfoMgr shareInstance].userLogInfo.account.id_p),
                            @"type":@"M",
                            };
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
//        obj1 = @[@{
//            @"cardpayType" : @"other",
//            @"code" : @"247",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"help2pay",
//            @"gatewayType" : @"cardpay",
//            @"guideUrl" : @"http://m.gwfxglobal.com/app/en/manual/guideline-help2pay.html",
//            @"maxPayamount" : @"10000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"help2pay",
//            @"nameEN" : @"help2pay",
//            @"nameTW" : @"help2pay",
//            @"payCurrency" : @"IDR",
//            @"sort" : @"0",
//        },
//        @{
//             @"cardpayType" : @"other",
//             @"code" : @"280",
//             @"customerServiceUrl" : @"",
//             @"gatewayCode" : @"fasapay__usd",
//             @"gatewayType" : @"nocard@",
//             @"guideUrl" : @"http://m.gwfxglobal.com/app/en/manual/guideline-Fasapay.html",
//             @"maxPayamount" : @"100000",
//             @"minPayamount" : @"50",
//             @"nameCN" : @"FasaPay[USD]",
//             @"nameEN" : @"fasapay__usd",
//             @"nameTW" : @"FasaPay[USD]",
//             @"payCurrency" : @"USD",
//             @"sort" : @"0",
//        },
//        @{
//            @"cardpayType" : @"other",
//            @"code" : @"269",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"help2payvnd",
//            @"gatewayType" : @"cardpay",
//            @"guideUrl" : @"http://m.gwfxglobal.com/app/en/manual/guideline-help2pay.html",
//            @"maxPayamount" : @"100000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"help2payvnd",
//            @"nameEN" : @"help2payvnd",
//            @"nameTW" : @"help2payvnd",
//            @"payCurrency" : @"VND",
//            @"sort" : @"0",
//        },
//        @{
//            @"cardpayType" : @"other",
//            @"code" : @"267",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"help2paythb",
//            @"gatewayType" : @"cardpay",
//            @"guideUrl" : @"http://m.gwfxglobal.com/app/en/manual/guideline-help2pay.html",
//            @"maxPayamount" : @"100000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"help2paythb",
//            @"nameEN" : @"help2paythb",
//            @"nameTW" : @"help2paythb",
//            @"payCurrency" : @"THB",
//            @"sort" : @"0",
//        },
//        @{
//            @"cardpayType" : @"other",
//            @"code" : @"265",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"help2paymyr",
//            @"gatewayType" : @"cardpay",
//            @"guideUrl" : @"http://m.gwfxglobal.com/app/en/manual/guideline-help2pay.html",
//            @"maxPayamount" : @"100000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"help2paymyr",
//            @"nameEN" : @"help2paymyr",
//            @"nameTW" : @"help2paymyr",
//            @"payCurrency" : @"MYR",
//            @"sort" : @"0",
//        },
//        @{
//            @"cardpayType" : @"other",
//            @"code" : @"249",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"fasapay",
//            @"gatewayType" : @"nocard",
//            @"guideUrl" : @"http://m.gwfxglobal.com/app/en/manual/guideline-Fasapay.html",
//            @"maxPayamount" : @"10000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"FasaPay",
//            @"nameEN" : @"fasapay",
//            @"nameTW" : @"FasaPay",
//            @"payCurrency" : @"IDR",
//            @"sort" : @"0",
//        },
//        @{
//            @"cardpayType" : @"credit",
//            @"code" : @"322",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"asiapay",
//            @"gatewayType" : @"cardpay",
//            @"guideUrl" : @"",
//            @"maxPayamount" : @"5000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"Credit / Debit cards",
//            @"nameEN" : @"asiapay",
//            @"nameTW" : @"Credit / Debit cards",
//            @"payCurrency" : @"USD",
//            @"sort" : @"0",
//        },
//        @{
//            @"cardpayType" : @"other",
//            @"code" : @"334",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"skrill",
//            @"gatewayType" : @"nocard",
//            @"guideUrl" : @"",
//            @"maxPayamount" : @"100000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"skrill",
//            @"nameEN" : @"skrill",
//            @"nameTW" : @"skrill",
//            @"payCurrency" : @"USD",
//            @"sort" : @"0",
//        },
//        @{
//            @"cardpayType" : @"other",
//            @"code" : @"336",
//            @"customerServiceUrl" : @"",
//            @"gatewayCode" : @"sticpay",
//            @"gatewayType" : @"nocard",
//            @"guideUrl" : @"",
//            @"maxPayamount" : @"100000",
//            @"minPayamount" : @"50",
//            @"nameCN" : @"sticpay",
//            @"nameEN" : @"sticpay",
//            @"nameTW" : @"sticpay",
//            @"payCurrency" : @"USD",
//            @"sort" : @"0",
//        }];
        
        if ([obj1 isKindOfClass:[NSArray class]]) {
            [IXBORequestMgr shareInstance].channelList = obj1;
             /* ------------ 特殊处理： gatewayCode 剔除尾部小写currency 并追加 大写 _CURRENCY ----------- */
            if ([obj1 isKindOfClass:[NSArray class]]) {
                NSMutableArray *tempArr = [NSMutableArray array];
                [obj1 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSMutableDictionary *dataDic = [obj mutableCopy];
                    NSString *gatewayCode = dataDic[@"gatewayCode"];
                    dataDic[@"gatewayCode_org"] = gatewayCode;
                    NSString *curency = dataDic[@"payCurrency"];
                   
                    if ([gatewayCode containsString:[curency lowercaseString]]) {
                        gatewayCode = [gatewayCode stringByReplacingOccurrencesOfString:[curency lowercaseString] withString:@""];
                    }
                    if (![[gatewayCode lowercaseString] containsString:@"skrill"]) {
                        gatewayCode = [gatewayCode stringByAppendingFormat:@"_%@",[curency uppercaseString]];
                    }
                    dataDic[@"gatewayCode"] = gatewayCode;
                    
                    
                    if ([[dataDic[@"gatewayCode"] lowercaseString] containsString:@"asiapay"]){
                        // asiapay静态数据
                        if (![IXBORequestMgr shareInstance].depositSort.count) {
                            [IXBORequestMgr shareInstance].depositSort = @[@{
                                                                                      @"appSort" : @"1",
                                                                                      @"currencyRang" : @"MasterCard#JCB#Diners#AE#Discover",
                                                                                      @"getewayName" : @"asiapay",
                                                                                      }];
                        }
                        
                        [[IXBORequestMgr shareInstance].depositSort enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            if ([[obj[@"getewayName"] lowercaseString] containsString:@"asiapay"]) {
                                 NSString *str = obj[@"currencyRang"];
                                 NSArray *currencyArr = [str componentsSeparatedByString:@"#"];
                                [currencyArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    dataDic[@"gatewayCode"] = [@"asiapay" stringByAppendingFormat:@"_%@",obj];
                                    [tempArr addObject:[dataDic mutableCopy]];
                                }];
                            }
                        }];
                        
                    }else if ([[dataDic[@"cardpayType"] lowercaseString] containsString:@"credit"]) {
                        // 信用卡静态数据
                        dataDic[@"gatewayCode"] = [gatewayCode stringByAppendingFormat:@"_%@",@"VISA"];
                        [tempArr addObject:[dataDic mutableCopy]];

                        dataDic[@"gatewayCode"] = [gatewayCode stringByAppendingFormat:@"_%@",@"MasterCard"];
                        [tempArr addObject:[dataDic mutableCopy]];

                        dataDic[@"gatewayCode"] = [gatewayCode stringByAppendingFormat:@"_%@",@"JCB"];
                        [tempArr addObject:[dataDic mutableCopy]];
                        
                    }else if ([[dataDic[@"gatewayCode"] lowercaseString] containsString:@"skrill"]){
                        // skrill静态数据
                        dataDic[@"gatewayCode"] = [gatewayCode stringByAppendingFormat:@"_%@",@"Skrill Wallet"];
                        [tempArr addObject:[dataDic mutableCopy]];
                        
                        dataDic[@"gatewayCode"] = [gatewayCode stringByAppendingFormat:@"_%@",@"Skrill to skrill"];
                        [tempArr addObject:[dataDic mutableCopy]];
                        
                        dataDic[@"gatewayCode"] = [gatewayCode stringByAppendingFormat:@"_%@",@"Visa by skrill"];
                        [tempArr addObject:[dataDic mutableCopy]];
                    }else{
                        [tempArr addObject:dataDic];
                    }
                }];
                [IXBORequestMgr shareInstance].channelList = tempArr;
            }
            /* ------------ 特殊处理： gatewayCode 剔除尾部小写currency 并追加 大写 _CURRENCY ----------- */
            if ([IXBORequestMgr shareInstance].channelList.count) {
//                [IXBORequestMgr shareInstance].channelDic = [IXBORequestMgr shareInstance].channelList[0];
                
            }
        }
        rsp(success1,errCode1,errStr1,obj1);
    }];
}


/**
 根据 网关编码 获取银行列表
 @param gateWayCode 网关编码, 根据接口 [获取支付通道列表] 所获取的 gatewayCode
 */
+ (void)b_getEnableBankListByGateWayCodeType:(NSString *)gateWayCode rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString    * url = @"/app/getEnableBankListByGateWayCodeType";
    NSDictionary *param = @{@"gateWayCode":gateWayCode,
                            @"type":@"P",
                            };
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
}

// 60.banner广告-列表
+ (void)b_getBannerAdvertisementList:(NSString *)infomationType rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString    * url = @"/app/appGetBannerAdvertisementList";
    NSDictionary *param = @{@"pageNo":@"1",
                            @"pageSize":@"1",
                            @"companyId":[@(CompanyID) stringValue],
                            @"infomationType":infomationType,
                            @"lang":@"en_US",
                            };
    if ([IXBORequestMgr shareInstance].launchHappenDic[infomationType]) {
        if ([[IXBORequestMgr shareInstance].launchHappenDic[infomationType] boolValue] == YES) {
            return;
        }
    }
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        if (success1) {
            if ([obj1 isKindOfClass:[NSDictionary class]]) {
                if (obj1[@"resultList"]) {
                    NSArray *resultList = obj1[@"resultList"];
                    if (resultList.count) {
                        NSDictionary *info = resultList[0];
                        if (info[@"titleImg"]) {
                            [IXBORequestMgr shareInstance].launchHappenDic[infomationType] = @(YES);
                            [AppDelegate showAdContainer:info[@"titleImg"] link:info[@"url"]];
                        }
                    }
                }
            }
        }
        rsp(success1,errCode1,errStr1,obj1);
    }];
}







#pragma mark -
#pragma mark - 出金


/** 取款实时账户信息查询 */
+ (void)drawcash_checkBankInfo:(void(^)(BOOL success, NSString * errStr, NSDictionary * dic))complete
{
    NSString    * url = [NSString formatterUTF8:kCustomerAccountViewNew];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"accountNo=%@&tradeAccountId=%llu&platform=%@&isGetCredit=true&_url=%@&_timestamp=%ld&currency=%@",
                              [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                              [IXUserInfoMgr shareInstance].userLogInfo.account.id_p,
                              PLATFORM,
                              url,
                              [timeStamp longValue] * 1000,
                              [IXAccountBalanceModel shareInstance].currency
                              ];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign
                                };
    
    [IXAFRequest bankInfoWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (complete) {
                complete (success1, errStr, obj1);
            }
        }];
    }];

}

+ (void)drawCashWith:(NSDictionary *)param complete:(void(^)(BOOL success, NSString * errStr))complete
{
    NSString    * url = [NSString formatterUTF8:kAddWithdraw];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"cashout=%@&_url=%@&_timestamp=%ld&freezeAmount=0",
                               param[@"cashout"],
                               url,
                               ([timeStamp longValue] * 1000)
                               ];
    paramStr = [paramStr stringByAppendingString:@"&isNewWithdraw=YES"];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest drawCashWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            if (complete) {
                complete (success1, errStr);
            }
        }];
    }];
}

+ (void)drawCashFeeWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kCashOutFee];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"gts2CustomerId=%ld&tradeAccountId=%llu@&withdrawAmount=%@&_url=%@&_timestamp=%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId,[IXUserInfoMgr shareInstance].userLogInfo.account.id_p,
                               param[@"withdrawAmount"],
                               url,
                               ([timeStamp longValue] * 1000)
                               ];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest drawCashFeeWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

+ (void)drawCashFeeRuleWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kCashOutFeeConfig];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"companyId=%d&_url=%@&language=%@&_timestamp=%ld",CompanyID,
                               url,
                               [IXLocalizationModel currentCheckLanguage],
                               ([timeStamp longValue] * 1000)
                               ];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest drawCashFeeRuleWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

+ (void)drawCashStateWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kCustomerWouldCashOut];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"customerNo=%@&bankOrder=%@&_url=%@&_timestamp=%ld",[IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                               param[@"bankOrder"],
                               url,
                               ([timeStamp longValue] * 1000)
                               ];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest drawCashStateWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

+ (void)unbindBankWithParam:(NSDictionary *)param result:(respResult)result
{
    if (!param || ![param ix_isDictionary]) {
        return;
    }
    
    NSArray * bankArr = (NSArray *)param[@"banks"];
    NSError * parseError = nil;
    NSData  * jsonData = [NSJSONSerialization dataWithJSONObject:bankArr options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString    * bankStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kCancelCustomerBanks];
    NSString    * paramStr = [NSString stringWithFormat:@"gts2CustomerId=%@&banks=%@&isAutoApprove=true&_timestamp=%ld&_url=%@",
                              param[@"gts2CustomerId"],
                              bankStr,
                              ([timeStamp longValue] * 1000),
                              url
                              ];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign
                                 };
    
    [IXAFRequest unbindBankWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

// 137.出金银行卡解绑
+ (void)b_cancelCustomerWithdrawBanks:(NSArray *)banks rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString *url = @"/app/AppCancelCustomerWithdrawBanks";
    NSMutableArray *tempArr = [NSMutableArray array];
    [banks enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dic = obj;
        [tempArr addObject:@{@"id":dic[@"id"]}];
    }];
    NSString *bankStr = [[tempArr mutableCopy] toJSONString];
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:bankStr result:rsp];
}


//71.判断用户能否取款1
+ (void)b_getCustomerWouldCashOut:(NSInteger)bankOrder rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString *url = @"/app/getCustomerWouldCashOut";
    NSDictionary *param = @{@"customerNo":@([IXBORequestMgr shareInstance].userInfo.detailInfo.customerNumber),
                            @"bankOrder":@(bankOrder),
                            };
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
}

//138.判断用户能否取款2
+ (void)b_getCustomerWouldCashOutByWitCard:(NSInteger)bankOrder rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString *url = @"/app/getCustomerWouldCashOutByWitCard";
    NSDictionary *param = @{@"customerNo":@([IXBORequestMgr shareInstance].userInfo.detailInfo.customerNumber),
                            @"bankOrder":@(bankOrder),
                            };
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
}



#pragma mark -
#pragma mark - private method

/** 缓存银行列表 */
+ (void)cacheBankList:(NSArray *)bankList
{
    if (!bankList) {
        ELog(@"可选银行卡信息为空，不能进行缓存操作");
        return ;
    }
    
    NSString    * path  = [[self cachePath] stringByAppendingString:kCacheBankPath];
    BOOL    success = [bankList writeToFile:path atomically:YES];
    
    if (!success) {
        ELog([NSString stringWithFormat:@"缓存可选银行卡信息失败，缓存内容：%@ 路径：%@",bankList,path]);
    } else {
        [self mgr_configBankCardList];
    }
}

+ (NSArray <IXBank *>*)getCachedBankList
{
    NSString    * path = [[self cachePath] stringByAppendingString:kCacheBankPath];
    NSArray     * bankList = [NSArray arrayWithContentsOfFile:path];
    __block NSMutableArray  * aimList = [@[] mutableCopy];
    
    [bankList enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj,
                                           NSUInteger idx,
                                           BOOL * _Nonnull stop)
    {
        if ([obj ix_isDictionary]) {
            [aimList addObject:[IXBank bankWithDic:obj]];
        }
    }];
    
    return aimList;
}

/** 请求后用户银行卡详细参数配置 */
+ (void)mgr_configBankCardList
{
    NSArray * arr = [self getCachedBankList];
    [[IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams
     enumerateObjectsUsingBlock:^(IXUserBankM * _Nonnull obj1,
                                  NSUInteger idx1,
                                  BOOL * _Nonnull stop1)
     {
         if (obj1.bank.length) {
             __block BOOL    exist = NO;
             [arr enumerateObjectsUsingBlock:^(IXBank * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                 if ([obj.code isEqualToString:obj1.bank]) {
                     obj1.nameCN = obj.nameCN;
                     obj1.nameEN = obj.nameEN;
                     obj1.nameTW = obj.nameTW;
                     exist = YES;
                     *stop = YES;
                 }
             }];
             
             if (!exist) {
                 obj1.bank = nil;
                 obj1.bankAccountNumber = nil;
             }
         }
     }];
}

@end
