//
//  UIImage+IX.h
//  IXApp
//
//  Created by Seven on 2017/9/29.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (IX)

/** 压缩尺寸（裁剪） */
- (UIImage *)scaledToSize:(CGSize)newSize;

/**
 压缩图片质量

 @param width 宽高最大值
 @param length 目标大小
 @param accuracy 误差范围
 */
- (NSData *)compressWidthWidth:(CGFloat)width aimLength:(NSInteger)length accuracyOfLength:(NSInteger)accuracy;

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
