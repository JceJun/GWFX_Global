//
//  IXBORequestMgr+Login.m
//  IXApp
//
//  Created by Seven on 2017/8/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Login.h"
#import <UMSocialCore/UMSocialCore.h>
#import "IxProtoHeader.pbobjc.h"
#import "IXCpyConfig.h"
#import "IXWUserInfo.h"
#import "IXAppUtil.h"
#import "IXBOModel.h"
#import "RSA.h"

#import "NSDictionary+json.h"
#import "NSArray+IX.h"
#import "NSObject+IX.h"




@implementation IXBORequestMgr (Login)
/**
 用户名密码登录
 
 @param acc 账号
 @param password 密码
 @param phoneCode 手机前缀
 */
+ (void)lg_loginWithAccount:(NSString *)acc
                        pwd:(NSString *)password
                  phoneCode:(NSString *)phoneCode
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    proto_user_login *proto = [[proto_user_login alloc] init];
    [proto setVersion:[version intValue]];
    [proto setPwd:[IXDataProcessTools md5StringByString:password]];
    [proto setLogintime:[IXEntityFormatter getCurrentTimeInterval]];
    [proto setCompanyToken:CompanyToken];
    [proto setCompanyid:CompanyID];
    [proto setSessionType:[IXUserInfoMgr shareInstance].itemType];
    
    if ([acc containsString:@"@"]) {
        [proto setEmail:acc];
        [proto setLoginType:proto_user_login_elogintype_ByEmail];
        [proto setType:proto_user_login_elogintype_ByEmail];
        [IXUserInfoMgr shareInstance].loginType = proto_user_login_elogintype_ByEmail;
        [IXUserInfoMgr shareInstance].loginName = acc;
    } else {
        [proto setPhone:acc];
        [proto setLoginType:proto_user_login_elogintype_ByPhone];
        [proto setType:proto_user_login_elogintype_ByPhone];
        if (!phoneCode.length) {
            phoneCode = @"62";//没有，默认62
        }
        [proto setPhoneCode:phoneCode];
        [IXUserInfoMgr shareInstance].loginType = proto_user_login_elogintype_ByPhone;
        [IXUserInfoMgr shareInstance].loginName = nil;
    }
    [[IXTCPRequest shareInstance] loginWithParam:proto];
}







@end
