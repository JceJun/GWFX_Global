//
//  IXBORequestMgr+Record.h
//  IXApp
//
//  Created by Seven on 2017/8/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"

/** 记录 */
@interface IXBORequestMgr (Record)

/**
 入金明细列表
 */
+ (void)record_incomeListWithParam:(NSDictionary *)param result:(respResult)result;

/**
 出金明细列表
 */
+ (void)record_drawCashListWithParam:(NSDictionary *)param result:(respResult)result;

/**
 交易记录列表
 */
+ (void)record_tradeRecordListWithParam:(NSDictionary *)param result:(respResult)result;

/**
 获取结单列表

 @param pageNo 页码
 @param size 页面大小
 @param complete 请求完成回调
 */
+ (void)record_statementListWithPageNo:(NSInteger)pageNo pageSize:(NSInteger)size complete:(void(^)(NSArray * list, NSString * errStr))complete;

//入金记录
+ (NSMutableArray *)getCachedInCashRecord;
//出金记录
+ (NSMutableArray *)getCachedWithDrawRecord;
//交易记录
+ (NSMutableArray *)getCachedTradeRecord;
//日结单
+ (NSMutableArray *)getCachedStatementRecord;

@end
