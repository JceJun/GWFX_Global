//
//  IXRegionMgr.m
//  IXApp
//
//  Created by Seven on 2017/6/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXRegionMgr.h"
#import "NSDictionary+json.h"
#import "IXCpyConfig.h"
#import "IXProvinceM.h"
#import "RSA.h"



static NSMutableDictionary  * countryInfo;
static NSMutableDictionary  * provinceInfo;
// --------------------------

static  NSString    * kCachePath = @"/Library/IXCacheInfo/";
static  NSString    * kFileName = @"ixRegionInfo";
static  NSString    * kProvinceHeader = @"ixProvince";

//key
static  NSString    * kNewTotalVersion = @"newTotalVersion";
static  NSString    * kCityList = @"subCountryDictParamList";
static  NSString    * kCurrentVersion = @"currentVersion";
//由于服务器返回字段单词拼写错误，故使用此count’yr’List错误字段
static  NSString    * kCountyrList = @"countyrList";
static  NSString    * kProvinceList = @"countryList";
#define kCityAndProvinceList            @"/app/appGetCityAndProvinceList"//获取城市列表
#define kCountryListUpdateByVersionNo   @"/app/appGetCountryListUpdateByVersionNo" //地区版本更新


// ---------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark - IXCountryM

@implementation IXCountryM

+ (instancetype)countryMWithDic:(NSDictionary *)dic
{
    return [[IXCountryM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]){
        [self setValuesForKeysWithDictionary:dic] ;
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"countryId";
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    //model中找不到对应的key时会走改方法，若删除改方法，找不到对应的key时会导致程序crash
    //因此给予此方法空实现
}

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end


// ---------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark - IXRegionMgr


@interface IXRegionMgr()


/** 缓存的国家列表 */
@property (nonatomic, strong) NSDictionary  * countryInfo;

@end

@implementation IXRegionMgr

+ (NSArray <IXCountryM *>*)refreshCountryList:(void(^)(NSArray <IXCountryM *>* list,
                                                       NSString * errStr,
                                                       BOOL hasNewData))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kCountryListUpdateByVersionNo];
    
    [self configCountryInfo];
    NSString    * totalVersion = @"null";
    if (countryInfo && countryInfo[kNewTotalVersion]) {
        totalVersion = [NSString stringWithFormat:@"%@",countryInfo[kNewTotalVersion]];
    }
    
    NSString    * paramStr = [NSString stringWithFormat:@"_timestamp=%ld&_url=%@&totalVersion=%@",
                              ([timeStamp longValue] * 1000),
                              url,
                              totalVersion];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest obtainCountryListUpdateWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        NSString    * errStr = @"";
        BOOL    hasNewData = NO;
        
        if ([obj isKindOfClass:[NSDictionary class]]) {
            if ([obj[kCountyrList] isKindOfClass:[NSArray class]]
                && [(NSArray *)obj[kCountyrList] count]){
                hasNewData = YES;
                //数据有更新或第一次请求到数据
                if (countryInfo && countryInfo[kNewTotalVersion]) {
                    NSArray * list = [self dealWithCountryListWith:[countryInfo[kNewTotalVersion] mutableCopy]
                                                          newArray:obj[kCountyrList]];
                    [countryInfo setValue:list forKey:kCountyrList];
                    [countryInfo setValue:obj[kNewTotalVersion] forKey:kNewTotalVersion];
                }else{
                    countryInfo = [(NSDictionary *)obj mutableCopy];
                }
                //保存国家列表信息
                [self saveContryList:countryInfo];
            }else{
                errStr = obj[@"newComment"];
            }
        }else{
            errStr = LocalizedString(@"请求失败");
        }
        
        if (complete) {
            complete([self countryModelArrWith:countryInfo[kCountyrList]],errStr,hasNewData);
        }
    }];
    
    return [self countryModelArrWith:countryInfo[kCountyrList]];
}


/**
 刷新省份列表（调用增量更新接口）
 
 @param countryCode  国家代码，从IXCountryM中取值
 @param complete 刷新完成
 @return 缓存内容
 */
+ (NSArray <IXProvinceM *>*)refreshProvinceWithNationalCode:(NSString *)nationalCode
                                                countryCode:(NSString *)countryCode
                                                   complete:(void(^)(NSArray <IXProvinceM *>* list,
                                                                     NSString * errStr,
                                                                     BOOL hasNewData))complete
{
    if (!nationalCode) {
        ELog(@"nationalCode is nil");
        return nil;
    }
    
    NSDictionary    * cachedInfo = [self cachedProvinceListWith:nationalCode];
    //本地无省份列表缓存，则请求原接口获取列表
    //若存在，则请求增量更新接口获取增量
    if (!cachedInfo) {
        [self requestProvinceListWith:countryCode complete:^(NSDictionary *dic, NSString *errStr) {
            //获取省份列表
            // ### 服务器返回的省份列表保存在countryList字段中 ###
            __block NSMutableArray  * provinceList = nil;
            if (dic) {
                provinceList = [@[] mutableCopy];
                NSArray * originList = dic[kProvinceList];
                
                [originList enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [provinceList addObject:[IXProvinceM provinceWithDic:obj]];
                }];
                
                
                //缓存操作
                [self saveProvinceInfo:dic nationalCode:nationalCode];
            }
            
            if (complete) {
                complete(provinceList, errStr, YES);
            }
        }];
    }else{
        NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
        NSString    * url = [NSString formatterUTF8:kCountryListUpdateByVersionNo];
        
        NSDictionary    * map = @{nationalCode:[self provinceVersionWithNationalCode:nationalCode]};
        
        NSString    * paramStr = [NSString stringWithFormat:@"_timestamp=%ld&_url=%@&totalVersion=0&codeAndVersionMap=%@",
                                  ([timeStamp longValue] * 1000),
                                  url,
                                  [map jsonString]];
        
        NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
        NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
        
        [IXAFRequest obtainCountryListUpdateWithParam:params result:^(BOOL respont, BOOL success, id obj) {
            NSString    * errStr = @"";
            if ([obj isKindOfClass:[NSDictionary class]]
                && [[obj allKeys] containsObject:@"context"]){
                
                id context = obj[@"context"];
                if ([context isKindOfClass:[NSDictionary class]] && [[context allKeys] containsObject:@"data"]) {
                    id data = context[@"data"];
                    if ([data isKindOfClass:[NSDictionary class]] && [[data allKeys] containsObject:@"provinceAndCityList"]) {
                        NSArray * arr = data[@"provinceAndCityList"];
                        //增量数据
                        if (arr.count > 0){
                            NSMutableDictionary * aimDic = [self dealWithNewProvinceWith:[cachedInfo mutableCopy]
                                                                                 newData:arr
                                                                             countryCode:countryCode];
                            NSString    * verson = data[kNewTotalVersion];
                            [aimDic setValue:verson forKey:kCurrentVersion];
                            
                            //缓存操作
                            [self saveProvinceInfo:aimDic nationalCode:nationalCode];
                            if (complete) {
                                complete ([self provinceModelArrWith:aimDic[kProvinceList]], errStr, YES);
                            }
                        }
                    } else {
                        if (complete) {
                            complete ([self provinceModelArrWith:cachedInfo[kProvinceList]], errStr, YES);
                        }
                    }
                } else {
                   errStr = LocalizedString(@"请求失败");
                }
            }else{
                //参考标准json
                errStr = LocalizedString(@"请求失败");
            }
            
            if (complete) {
                complete ([self provinceModelArrWith:cachedInfo[kProvinceList]], errStr, NO);
            }
            
        }];
    }
    
    //若内存中包含
    if (provinceInfo && provinceInfo[nationalCode]) {
        return provinceInfo[nationalCode];
    }else{
        provinceInfo = [NSMutableDictionary dictionary];
        
        NSArray * list = [self provinceModelArrWith:cachedInfo[kProvinceList]];
        
        if (list.count) {
            [provinceInfo setValue:list forKey:nationalCode];
        }
        
        return list;
    }
}


+ (NSArray *)dealWithCountryListWith:(NSMutableArray *)originArr newArray:(NSArray *)newList
{
    //先从旧列表中删除重复国家
    [originArr enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSDictionary *  _Nonnull oldInfo, NSUInteger idx, BOOL * _Nonnull stop) {
        [newList enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull newInfo, NSUInteger idx1, BOOL * _Nonnull stop1) {
            if ([oldInfo[@"code"] isEqualToString:newInfo[@"code"]]) {
                [originArr removeObject:oldInfo];
                *stop1 = YES;
            }
        }];
    }];
    
    [originArr addObjectsFromArray:newList];
    return originArr;
}

/**
 处理省市增量，将增量添加进缓存数据
 
 @param originInfo 原属缓存数据
 @param newList 增量省市数据
 @param code 国家code，可据此参数判断新增数据元是否为省份
 @return 处理后的新数据
 */
+ (NSMutableDictionary *)dealWithNewProvinceWith:(NSMutableDictionary *)originInfo
                                         newData:(NSArray <NSDictionary *>*)newList
                                     countryCode:(NSString *)code
{
    // ### 服务器返回的省份列表保存在countyrList字段中 ###
    
    //优先处理key-value中，value为null的情况
    NSMutableArray  * newCityList = [self dealWithSaveData:newList];
    
    //筛选出新增省份，block处理之后，cityList中只保存有新增的省份管辖之下的市
    //若有新增省，则肯定会有新增市，但这种情况很少见，为减少程序复杂度，不单独对newList中的数据进行归类处理
    NSMutableArray  * newProvinceArr = [@[] mutableCopy];
    __block NSMutableArray * originProvinceArr = [originInfo[kProvinceList] mutableCopy];
    if (!originProvinceArr) {
        originProvinceArr = [@[] mutableCopy];
    }
    
    [newCityList enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj[@"parentCode"]){
            //code是国家code，[obj[@"parentCode"] isEqualToString:code]判断为真说明obj为“省”
            if ([obj[@"parentCode"] isEqualToString:code]) {
                //obj 为省级行政单位
                [newCityList removeObject:obj];
                [newProvinceArr addObject:obj];
                
                //匹配到重复的省份，则在原数组中删除
                [originProvinceArr enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSDictionary *  _Nonnull obj1, NSUInteger idx1, BOOL * _Nonnull stop1) {
                    if ([obj[@"code"] isEqualToString:obj1[@"code"]]) {
                        [originProvinceArr removeObject:obj1];
                        [obj setValue:obj1[kCityList] forKey:kCityList];
                        *stop1 = YES;
                    }
                }];
            }
        }
    }];
    
    [originProvinceArr addObjectsFromArray:newProvinceArr];
    
    //将省份转字典转成可变字典
    [originProvinceArr enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [originProvinceArr removeObject:obj];
        [originProvinceArr addObject:[obj mutableCopy]];
    }];
    
    //将新增省份辖区下的市添加进缓存数据中
    //新增市是随机的，新增省会放在列表最后，为减少计算量，故省份列表逆序遍历
    [originProvinceArr enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSMutableDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //obj为省级行政单位
        __block NSMutableArray * originCityList = [obj[kCityList] mutableCopy];
        if (!originCityList) {
            originCityList = [@[] mutableCopy];
        }
        [newCityList enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull newCityInfo, NSUInteger idx1, BOOL * _Nonnull stop1) {
            //obj1为市级行政单位
            if ([newCityInfo[@"parentCode"] isEqualToString:obj[@"code"]]){
                //城市去重
                __block BOOL    exist = NO;
                [originCityList enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSDictionary *  _Nonnull cityInfo, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([newCityInfo[@"code"] isEqualToString:cityInfo[@"code"]]) {
                        exist = YES;
                        [originCityList removeObject:cityInfo];
                        [originCityList addObject:newCityInfo];
                        *stop = YES;
                    }
                }];
                
                if (!exist) {
                    [originCityList addObject:newCityInfo];
                }
            }
        }];
        [obj setValue:originCityList forKey:kCityList];
    }];
    
    [originInfo setValue:originProvinceArr forKey:kProvinceList];
    return originInfo;
}

/**
 获取国家包含的省份列表

 @param parentCode 从IXCountryM中获取
 */
+ (void)requestProvinceListWith:(NSString *)parentCode complete:(void(^)(NSDictionary * info, NSString * errStr))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kCityAndProvinceList];
    
    NSString    * paramStr = [NSString stringWithFormat:@"_timestamp=%ld&_url=%@&parentCode=%@",
                              ([timeStamp longValue] * 1000),
                              url,
                              parentCode];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest checkProvinceListWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        NSString    * errStr = @"";
        
        __block NSMutableDictionary  * dic = nil;
        if ( [obj isKindOfClass:[NSDictionary class]] && [[obj allKeys] containsObject:@"newRet"]
            && [obj[@"newRet"] isEqualToString:@"OK"] && [[obj allKeys] containsObject:@"context"]) {
            id context = obj[@"context"];
            if ([context isKindOfClass:[NSDictionary class]] && [[context allKeys] containsObject:@"data"]) {
                id data = context[@"data"];
                if ([context isKindOfClass:[NSDictionary class]]) {
                    dic = [[NSMutableDictionary alloc] init];
                    [dic setValue:data forKey:kProvinceList];
                    [dic setValue:@"0" forKey:kCurrentVersion];
                } else {
                   errStr = LocalizedString(@"请求失败");
                }
            } else {
               errStr = LocalizedString(@"请求失败");
            }
        } else {
            if ([obj isKindOfClass:[NSString class]]){
                errStr = obj;
            }else{
                errStr = LocalizedString(@"请求失败");
            }
        }
        if (complete) complete([dic copy], errStr);
    }];
}


#pragma mark -
#pragma mark - other

+ (NSString *)provinceVersionWithNationalCode:(NSString *)code
{
    NSDictionary    * provinceDic = [self cachedProvinceListWith:code];
    NSString    * version = @"0";
    
    if ([provinceDic isKindOfClass:[NSDictionary class]]
        && provinceDic[kCurrentVersion]) {
        version = provinceDic[kCurrentVersion];
    }
    
    return [NSString stringWithFormat:@"%@",version];
}


+ (void)configCountryInfo
{
    if (!countryInfo) {
        countryInfo = [[NSDictionary dictionaryWithContentsOfFile:[self countryCachedPath]] mutableCopy];
    }
}

+ (NSDictionary *)cachedProvinceListWith:(NSString *)nationalCode
{
    NSDictionary    * dic = [NSDictionary dictionaryWithContentsOfFile:[self provinceCachedPathWith:nationalCode]];
    if (!dic) {
        dic = @{};
    }
    return dic;
}

+ (NSArray <IXCountryM *>*)countryModelArrWith:(NSArray <NSDictionary *>*)arr
{
    if (!arr) {
        return @[];
    }
    
    __block NSMutableArray  * list = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [list addObject:[IXCountryM countryMWithDic:obj]];
        }
    }];
    
    return list;
}

+ (NSArray <IXProvinceM *>*)provinceModelArrWith:(NSArray <NSDictionary *>*)arr
{
    if (!arr) {
        return @[];
    }
    
    __block NSMutableArray  * list = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [list addObject:[IXProvinceM provinceWithDic:obj]];
        }
    }];
    
    [list sortUsingComparator:^NSComparisonResult(IXProvinceM * _Nonnull obj1, IXProvinceM * _Nonnull obj2) {
        return [obj1.nameEN compare:obj2.nameEN];
    }];
    
    return list;
}

#pragma mark -
#pragma mark - 缓存相关

//------------------------------ 缓存路径 ------------------------------
/**
 获取国家列表缓存路径
 
 @return 缓存路径
 */
+ (NSString *)countryCachedPath
{
    NSString    * path = [self cacheDirectory];
    return [path stringByAppendingString:kFileName];
}

/**
 根据国家code获取省份列表缓存路径

 @param nationalCode 国家code 从IXCountryM中取值
 @return    缓存路径
 */
+ (NSString *)provinceCachedPathWith:(NSString *)nationalCode
{
    NSString    * fileName = [kProvinceHeader stringByAppendingFormat:@"_%@",nationalCode];
    NSString    * path = [self cacheDirectory];
    
    return [path stringByAppendingString:fileName];
}


/** 获取文件缓存路径 */
+ (NSString *)cacheDirectory
{
    NSString    * path = [NSHomeDirectory() stringByAppendingString:kCachePath];
    
    BOOL    isDirectory = YES;
    BOOL    exist = [[NSFileManager defaultManager] fileExistsAtPath:path
                                                         isDirectory:&isDirectory];
    
    if (!exist) {
        NSError * error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&error];
    }
    
    return path;
}

//------------------------------ 存取 ------------------------------

+ (void)saveContryList:(NSMutableDictionary *)countryListInfo
{
    NSArray * arr = (NSArray *)countryListInfo[kCountyrList];
    NSMutableArray  * aimArr = [self dealWithSaveData:arr];
    
    NSMutableDictionary * aimDic = [countryListInfo mutableCopy];
    [aimDic setValue:aimArr forKey:kCountyrList];
    
    NSString    * filePath = [self countryCachedPath];
    BOOL success = [aimDic writeToFile:filePath atomically:YES];
    
    if (success) {
        DLog(@" -- save country info success --");
    }else{
        DLog(@" -- save country info failure --");
    }
}

+ (void)saveProvinceInfo:(NSDictionary *)info nationalCode:(NSString *)code
{
    if (!info || ![info isKindOfClass:[NSDictionary class]] || !info[kProvinceList]) {
        return;
    }
    
    NSMutableDictionary * dic = [info mutableCopy];
    
    NSArray     * arr = [self dealWithSaveData:info[kProvinceList]];
    [dic setValue:arr forKey:kProvinceList];
    
    NSString    * filePath = [self provinceCachedPathWith:code];
    
    BOOL success = [dic writeToFile:filePath atomically:YES];
    
    if (success) {
        DLog(@" -- save province info success --");
    }else{
        DLog(@" -- save province info failure --");
    }
}

+ (NSMutableArray *)dealWithSaveData:(NSArray <NSDictionary *>*)arr
{
    NSMutableArray  * aimArr = [NSMutableArray array];
    
    //过滤value为<null>的键值对，value为null将导致数据写入本地失败
    [arr enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary * dic = [obj mutableCopy];
        [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj1, BOOL * _Nonnull stop) {
            if ([obj1 isKindOfClass:[NSNull class]]
                || ([obj1 isKindOfClass:[NSString class]]
                    &&( [obj1 isEqualToString:@"<null>"]
                       || [obj1 isEqualToString:@"<NULL>"]))) {
                        [dic setValue:@"" forKey:key];
                    }
            
            //递归处理
            if ([obj1 isKindOfClass:[NSArray class]]
                && [[obj1 firstObject] isKindOfClass:[NSDictionary class]]) {
                NSArray * arr = [self dealWithSaveData:obj1];
                [dic setValue:arr forKey:key];
            }
        }];
        [aimArr addObject:dic];
    }];
    
    return aimArr;
}

@end

