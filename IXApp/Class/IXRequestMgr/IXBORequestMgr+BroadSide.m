//
//  IXBORequestMgr+BroadSide.m
//  IXApp
//
//  Created by Seven on 2017/8/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+BroadSide.h"
#import "IXCpyConfig.h"
#import "NSObject+IX.h"
#import "RSA.h"

#define kAskedAndQuestionsList      @"/app/appGetAskedAndQuestionsList" //常见问答查询-列表
#define kAskedAndQuestionsDetail    @"/app/appGetAskedAndQuestionsDetail" //常见问答查询-详情
#define kBannerAdvertisementList    @"/app/appGetBannerAdvertisementList" //广告-列表
#define kAppJpushMessageDetail      @"/app/getAppJpushMessageDetail" //Jpush消息-详情
#define kAppVersionControl          @"/app/appGetLatestVersionNew"  //版本控制信息

@implementation IXBORequestMgr (BroadSide)

#pragma mark 常见问答查询-列表
+ (void)bs_askedAndQuestionsListWithParam:(id)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kAskedAndQuestionsList];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"pageNo=%@&pageSize=%@&companyId=%d&infomationType=ASK_QUESTION&lang=%@&_url=%@&_timestamp=%@",
                              param[@"pageNo"],
                              param[@"pageSize"],
                              CompanyID,
                              [IXLocalizationModel currentCheckLanguage],
                              url,
                              timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest askedAndQuestionsListWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

#pragma mark 常见问答查询-详情
+ (void)bs_askedAndQuestionsDetailWithParam:(id)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kAskedAndQuestionsDetail];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&id=%@",url,param[@"id"]];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest askedAndQuestionsDetailWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

#pragma mark 广告-列表
+ (void)bs_bannerAdvertisementListWithParam:(id)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kBannerAdvertisementList];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"pageNo=%@&pageSize=%@&companyId=%d&infomationType=%@&lang=%@&_url=%@&_timestamp=%@",
                              param[@"pageNo"],
                              param[@"pageSize"],
                              CompanyID,
                              param[@"infomationType"],
                              [IXLocalizationModel currentCheckLanguage],
                              url,
                              timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest bannerAdvertisementListWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

#pragma mark Jpush消息-详情
+ (void)bs_jpushMessageDetailWithParam:(id)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kAppJpushMessageDetail];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"msgId=%@&timeZone=%@&lang=%@&_url=%@&_timestamp=%@",
                              param[@"msgId"],
                              param[@"timeZone"],
                              [IXLocalizationModel currentCheckLanguage],
                              url,
                              timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest jpushMessageDetailWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

+ (void)bs_getAppVersionControlInfo:(void(^)(NSDictionary * result))complete
{
    NSString    * url = [NSString formatterUTF8:kAppVersionControl];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"companyId=%d&deviceType=IOS&_url=%@&_timestamp=%@",
                              CompanyID,
                              url,
                              timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest versionControlWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            NSDictionary    * result = nil;
            if ([obj1 ix_isDictionary]) {
                result = obj1[@"latestVersion"];
            }
            if (complete) {
                complete(result);
            }
        }];
        
    }];
}

@end
