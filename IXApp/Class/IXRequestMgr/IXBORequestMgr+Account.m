//
//  IXBORequestMgr+Account.m
//  IXApp
//
//  Created by Seven on 2017/8/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Account.h"
#import "IxProtoHeader.pbobjc.h"
#import "IXBORequestMgr+Login.h"
#import "IXSupplementFileM.h"
#import "IXSupplementBankM.h"
#import "IXCpyConfig.h"
#import "IXWUserInfo.h"
#import "NSObject+IX.h"
#import "IXAppUtil.h"
#import "RSA.h"
#import "IXUserInfoM.h"
#import "NSDictionary+json.h"

@implementation IXEmailPhoneModel
@end
@implementation IXEmailModel
@end
@implementation IXPhoneModel
@end
@implementation IXFindPwdM
@end

#define kUpdateCustomerAndNewAccount @"/app/appUpdateCustomerAndNewAccount" //开设真实账户
#define kValidatePhoneAndEmail  @"/app/appValidatePhoneAndEmail" //手机号/邮箱/微信校验
#define kUpdateCustomerFiles    @"/app/appUpdateCustomerFiles" //用户文件修改提交
#define kUpdateCustomerBanks    @"/app/appUpdateCustomerBanks" //用户银行资料修改提交
#define kBindDetailCustomerInfo @"/app/appBindDetailCustomerInfo" //邮箱或者手机号绑定
#define kGetLatestVersion   @"/app/appGetLatestVersion" //获取最新版本
#define kProtocolList       @"/app/appGetProtocolList" //客户协议信息列表
#define kProtocolDetail     @"/app/appGetProtocolDetail" //客户协议信息详情
#define kChangeCustomerPwd  @"/app/appChangeCustomerPwd" //找回密码
#define kUpdateCustomerPwd  @"/app/appUpdateCustomerPwd" //修改密码
#define kAddUserDevice      @"/app/appAddUserDevice" //用户设备绑定
#define kDeleteUserDevice   @"/app/appDeleteUserDevice" //移除绑定的设备
#define kUpdateCustomer     @"/app/appUpdateCustomer" //用户资料修改
#define kNewCustomer        @"/app/appNewCustomer" //手机/邮箱注册
#define kCustomerFiles      @"/app/appGetCustomerFiles" //获取客户文件信息
#define kDictChildListNew   @"/app/appGetDictChildListNew" //获取数据字典
#define kUploadFile         @"/app/appUploadFile" //上传文件
#define kCheckUserType      @"/app/appCheckUserType" //查询当前用户的类型

@implementation IXBORequestMgr (Account)


+ (void)acc_bindEmailOrPhoneWithParam:(NSDictionary *)param result:(respResult)result
{
    if (!param || ![param ix_isDictionary]) {
        return;
    }
    
    NSMutableDictionary * infoDic = [NSMutableDictionary dictionary];
    [infoDic setValue:[IXDataProcessTools dealWithNil:param[@"gts2CustomerId"]] forKey:@"gts2CustomerId"];
    [infoDic setValue:[IXDataProcessTools dealWithNil:param[@"mobilePhonePrefix"]] forKey:@"mobilePhonePrefix"];
    [infoDic setValue:[IXDataProcessTools dealWithNil:param[@"mobilePhone"]] forKey:@"mobilePhone"];
    [infoDic setValue:[IXDataProcessTools dealWithNil:param[@"email"]] forKey:@"email"];
    [infoDic setValue:[IXDataProcessTools dealWithNil:param[@"companyId"]] forKey:@"companyId"];
    
    NSError * parseError = nil;
    NSData  * jsonData = [NSJSONSerialization dataWithJSONObject:infoDic options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString    * infoStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kBindDetailCustomerInfo];
    NSString    * paramStr = [NSString stringWithFormat:@"customer=%@&flag=%@&_timestamp=%ld&_url=%@",
                              infoStr,param[@"flag"],
                              ([timeStamp longValue] * 1000),
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign
                                 };
    
    [IXAFRequest bindEmailOrPhoneWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}


#pragma mark -
#pragma mark - 账号相关（校验手机邮箱／获取校验验证码／注册／找回密码）

//手机／邮箱是否已经注册
+ (void)acc_checkEmailOrPhoneExistWith:(IXEmailPhoneModel *)model
                          complete:(void(^)(BOOL exist, NSString * errStr))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kValidatePhoneAndEmail];
    
    NSString    * paramStr = @"";
    
    if (model.index == 0) {
        IXEmailModel    * emailModel = [[IXEmailModel alloc] init];
        emailModel.email = model.email;
        emailModel.gts2CustomerId = model.gts2CustomerId;
        paramStr = [NSString stringWithFormat:@"customer=%@&_timestamp=%ld&_url=%@",
                    [emailModel toJSONString],
                    ([timeStamp longValue] * 1000),
                    url];
    } else {
        IXPhoneModel    * phoneModel = [[IXPhoneModel alloc] init];
        phoneModel.mobilePhone = model.mobilePhone;
        phoneModel.mobilePhonePrefix = model.mobilePhonePrefix;
        paramStr = [NSString stringWithFormat:@"customer=%@&_timestamp=%ld&_url=%@",
                    [phoneModel toJSONString],
                    ([timeStamp longValue] * 1000),
                    url];
    }
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign
                                 };
    
    [IXAFRequest checkEmailOrPhoneExsitWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        BOOL    esxist = NO;
        NSString    * errStr = @"";
        
        if (obj && [obj ix_isDictionary]) {
            if (![obj[@"newRet"] isEqualToString:@"OK"]) {
                //已注册(错误码为0x500106)
                esxist = YES;
                errStr =  [IXEntityFormatter getErrorInfomation:[(NSDictionary *)obj stringForKey:@"newRet"]];
            }
        }else if (obj && [obj ix_isString]) {
            errStr = [(NSString *)obj length] ? obj : nil;
        }
        
        if (complete) {
            complete(esxist, errStr);
        }
    }];
}

//获取短信验证码
+ (void)acc_getPhoneVerifyCodeWithNumber:(NSString *)phoneNum
                            areaCode:(NSString *)code
                            complete:(void(^)(NSString * codeId, NSString * errStr))complete
{
    NSString    * paramStr = [NSString stringWithFormat:@"_mobileNumber=%@&_url=%@&_areaCode=%@&_messageLang=%@",
                              phoneNum,
                              [NSString formatterUTF8:kNewCustomer],
                              code,
                              [self appLang]];
    NSString    * signStr = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"_apiUser":CompanyLoginName,
                                @"param":signStr
                                };
    
    [IXAFRequest getPhoneVerifyCodeWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        DLog(@"验证码=== %@ ===",obj);
        [self acc_dealVerifyCodeWith:obj complete:complete];
    }];
}

//获取短信验证码防刷接口
+ (void)acc_getPhoneVerifyCodeWithLimitedWithNumber:(NSString *)phoneNum
                                           areaCode:(NSString *)code
                                  imgVerifiCodeCode:(NSString *)imgVerifiCodeCode
                                    imgVerifiCodeId:(NSString *)imgVerifiCodeId
                                             result:(verifiCodeResult)result
{
    NSString    * paramStr = [NSString stringWithFormat:@"_mobileNumber=%@&_url=%@&_areaCode=%@&_messageLang=%@&_imgVerifiCodeCode=%@&_imgVerifiCodeId=%@",
                              phoneNum,
                              [NSString formatterUTF8:kNewCustomer],
                              code,
                              [self appLang],
                              [IXDataProcessTools md5StringByString:[imgVerifiCodeCode lowercaseString]],
                              imgVerifiCodeId];
    NSString    * signStr = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"_apiUser":CompanyLoginName,
                                @"param":signStr
                                };
    [IXAFRequest getPhoneVerifyCodeWithLimitedWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        DLog(@"验证码：\n%@",obj);
        if (respont && success && [obj ix_isDictionary]) {
            NSDictionary *status = [(NSDictionary *)obj objectForKey:@"status"];
            if (status && [status ix_isDictionary]) {
                id code = [status objectForKey:@"code"];
                if (code) {
                    if ([code integerValue] == 200) {
                        result(nil,nil,obj[@"_id"],obj[@"_imgVerifiCodeUrl"],obj[@"_imgVerifiCodeId"]);
                    } else if ([code integerValue] == 203
                               || [code integerValue] == 2203) {
                        result(code,[IXEntityFormatter getErrorInfomation:code],obj[@"_id"],obj[@"_imgVerifiCodeUrl"],obj[@"_imgVerifiCodeId"]);
                    } else {
                        result(nil,LocalizedString(@"数据异常"),nil,nil,nil);
                    }
                } else {
                    result(nil,LocalizedString(@"数据异常"),nil,nil,nil);
                }
            } else {
                result(nil,LocalizedString(@"数据异常"),nil,nil,nil);
            }
        } else {
            if ([obj isKindOfClass:[NSDictionary class]] && obj[@"newComment"]) {
                result(nil,obj[@"newComment"],nil,nil,nil);
            } else if ([obj isKindOfClass:[NSString class]] && [obj length]) {
                result(nil,obj,nil,nil,nil);
            } else {
                result(nil,LocalizedString(@"请求失败"),nil,nil,nil);
            }
        }
    }];
}

//获取邮箱验证码
+ (void)acc_getEmailVerifyCodeWithEmail:(NSString *)email
                           complete:(void(^)(NSString * codeId, NSString * errStr))complete
{
    NSString    * paramStr  = [NSString stringWithFormat:@"_email=%@&_url=%@&_messageLang=%@",
                               email,
                               [NSString formatterUTF8:kNewCustomer],
                               [self appLang]];
    NSString    * signStr   = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"_apiUser":CompanyLoginName,
                                @"param":signStr
                                };
    
    [IXAFRequest getEmailVerifyCodeWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        [self acc_dealVerifyCodeWith:obj complete:complete];
    }];
}

/**
 短信／邮箱验证码接口返回结果处理方法
 
 @param obj 返回结果
 @param complete 处理回调
 */
+ (void)acc_dealVerifyCodeWith:(id)obj complete:(void(^)(NSString * codeId, NSString * errStr))complete
{
    DLog(@"验证码：\n%@",obj);
    NSString    * verifyCodeId = @"";
    NSString    * errStr = @"";
    
    if (obj && [obj ix_isDictionary]) {
        NSDictionary    * status = [(NSDictionary *)obj objectForKey:@"status"];
        if (status && [status ix_isDictionary]) {
            id code = [status objectForKey:@"code"];
            if (code && ([code integerValue] == 200)) {
                verifyCodeId = [(NSDictionary *)obj stringForKey:@"_id"];
            }else{
                errStr =  [IXEntityFormatter getErrorInfomation:[(NSDictionary *)status stringForKey:@"code"]];
            }
        }
    }
    
    if (!verifyCodeId.length && !errStr.length){
        errStr = LocalizedString(@"数据异常");
    }
    
    if (complete) {
        complete(verifyCodeId, errStr);
    }
}

/**
 获取短信验证码防刷接口
 
 @param obj 返回结果
 @param complete 处理回调
 */
+ (void)acc_dealVerifyCodeWithLimitedWith:(id)obj complete:(void(^)(NSString * codeId, NSString * errStr))complete
{
    DLog(@"验证码：\n%@",obj);
    NSString    * verifyCodeId = @"";
    NSString    * errStr = @"";
    
    if (obj && [obj ix_isDictionary]) {
        NSDictionary    * status = [(NSDictionary *)obj objectForKey:@"status"];
        if (status && [status ix_isDictionary]) {
            id code = [status objectForKey:@"code"];
            if (code && ([code integerValue] == 200)) {
                verifyCodeId = [obj stringForKey:@"_id"];
            }else{
                errStr =  [IXEntityFormatter getErrorInfomation:[(NSDictionary *)status stringForKey:@"code"]];
            }
        }
    }
    
    if (!verifyCodeId.length && !errStr.length){
        errStr = LocalizedString(@"数据异常");
    }
    
    if (complete) {
        complete(verifyCodeId, errStr);
    }
}

//验证码正确性
+ (void)acc_checkVerifyCodeWithCode:(NSString *)code
                         codeId:(NSString *)idStr
                       complete:(void(^)(BOOL success, NSString * token, NSString * errStr))complete
{
    if (idStr.length && code.length >= 4) {
        NSDictionary    * param = @{
                                    @"_id":idStr,
                                    @"_verifiCode":code
                                    };

        [IXAFRequest checkVerifyCodeWithParam:param result:^(BOOL respont, BOOL success, id obj) {
            NSString    * token = @"";
            NSString    * errStr = @"";
            BOOL    confirmSuccess = NO;
            
            if (obj && [obj ix_isDictionary]) {
                NSDictionary    * status = [(NSDictionary *)obj objectForKey:@"status"];
                if (status && [status ix_isDictionary]) {
                    id code = [status objectForKey:@"code"];
                    if (code && [code integerValue] == 200) {
                        token = [obj stringForKey:@"token"];
                        confirmSuccess = YES;
                    }else{
                        errStr =  [IXEntityFormatter getErrorInfomation:[(NSDictionary *)status stringForKey:@"code"]];
                    }
                }
            }
            
            if (!confirmSuccess && !errStr.length){
                errStr = LocalizedString(@"数据异常");
            }
            
            if (complete) {
                complete(confirmSuccess, token, errStr);
            }
        }];
    }else{
        if (complete) {
            complete(NO, @"", LocalizedString(@"请输入验证码(最少四位数字)"));
        }
    }
}

//注册账号(wChat)
+ (void)acc_registWithP:(IXRegisterStep3M *)model
              token:(NSString *)token
           complete:(void(^)(BOOL registerSucess, NSString * errStr,NSInteger gId))complete
{
    NSString    * modelStr = [model toJSONString];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"customer=%@&_url=%@&_timesetamp=%ld&informationFrom=%@&openFrom=%@&devToken=%@&devName=%@&_messageLang=%@",
                               modelStr,
                               [NSString formatterUTF8:kNewCustomer],
                               ([timeStamp longValue] *1000),
                               @"chestbox",
                               @"WEBSITE_IOS",
                               [IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]],
                               [IXDataProcessTools dealWithDeviceName:[[UIDevice currentDevice] name]],
                               [self appLang]];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param  = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign,
                                 @"_token":token
                                 };
    
    [IXAFRequest registWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        NSString    * errString = @"";
        NSInteger   gId = 0;
        BOOL    requestSuccess = NO;
        
        if ([obj ix_isDictionary]) {
            if (obj[@"newRet"] && [obj[@"newRet"] isEqualToString:@"OK"]) {
                requestSuccess = YES;
                
                NSDictionary    * context = obj[@"context"];
                if ( context && [context ix_isDictionary] ) {
                    gId = [context integerForKey:@"gts2CustomerId"];
                }
            }else if (obj[@"newComment"] && [obj[@"newComment"] ix_isString]){
                errString =  [IXEntityFormatter getErrorInfomation:[(NSDictionary *)obj stringForKey:@"code"]];
            }else if([(NSDictionary *)obj objectForKey:@"error"] && [[(NSDictionary *)obj objectForKey:@"error"] ix_isDictionary]){
                errString = [(NSDictionary *)[(NSDictionary *)obj objectForKey:@"error"] objectForKey:@"message"];
            }
        }else if ([obj ix_isString]){
            errString = obj;
        }
        
        if (!errString.length && !requestSuccess) {
            errString = LocalizedString(@"注册失败");
        }
        
        if (complete) {
            complete(requestSuccess, errString,gId);
        }
    }];
}

//注册账号
+ (void)acc_registWith:(IXRegisterStep3M *)model
             token:(NSString *)token
          complete:(void(^)(BOOL registerSucess, NSString * errStr))complete
{
    NSString    * modelStr  = [model toJSONString];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"customer=%@&_url=%@&_timesetamp=%ld&informationFrom=%@&openFrom=%@&devToken=%@&devName=%@&_messageLang=%@",
                               modelStr,
                               [NSString formatterUTF8:kNewCustomer],
                               ([timeStamp longValue] *1000),
                               @"chestbox",
                               @"WEBSITE_IOS",
                               [IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]],
                               [IXDataProcessTools dealWithDeviceName:[[UIDevice currentDevice] name]],
                               [self appLang]];
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param  = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign,
                                 @"_token":token
                                 };
    
    [IXAFRequest registWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        @try{
            [self acc_dealWithResponse:obj
                             aimErrStr:obj[@"newComment"]
                                result:complete];
        }@catch(NSException *exception) {
            [self acc_dealWithResponse:obj
                             aimErrStr:@"Failed"
                                result:complete];
        }
        
    }];
}

#pragma mark -找回密码
+ (void)acc_findPasswordWith:(IXFindPwdM *)model
                pageType:(pageType)type
                complete:(void(^)(BOOL findSuccess, NSString * errStr))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kChangeCustomerPwd];
    
    NSString    * str = nil;
    if (type == pageTypeEmailFind) {
        str = [NSString stringWithFormat:@"email=%@",model.email];
    }else{
        str = [NSString stringWithFormat:@"phoneNo=%@&mobilePhonePrefix=%@",model.mobilePhone,model.mobilePhonePrefix];
    }
    NSString    * paramStr = [NSString stringWithFormat:@"platform=%@&%@&newPassword=%@&encryptPassword=%@&companyId=%d&_token=""&_timestamp=%ld&_url=%@",
                              PLATFORM,
                              str,
                              [IXDataProcessTools md5StringByString:model.psd],
                              [NSString formatterUTF8:[RSA encryptString:model.psd publicKey:EncryptPassword]],
                              CompanyID,
                              ([timeStamp longValue] * 1000),
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign
                                 };
    
    [IXAFRequest findPasswdWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        [self acc_dealWithResponse:obj
                     aimErrStr:LocalizedString(@"找回密码失败")
                        result:complete];
    }];
}

#pragma mark - 修改密码
+ (void)acc_modifyPasswdWithParam:(NSDictionary *)param result:(respResult)result
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kUpdateCustomerPwd];
    NSString    * paramStr = [NSString stringWithFormat:@"platform=%@&updateType=FO_UPDATE_CUSTOMER&gts2CustomerId=%@&_timestamp=%ld&_url=%@&oldPassword=%@&newPassword=%@&encryptPassword=%@",
                              PLATFORM,
                              param[@"gts2CustomerId"],
                              ([timeStamp longValue] * 1000),
                              url,
                              param[@"oldPassword"],
                              param[@"newPassword"],
                              [NSString formatterUTF8:[RSA encryptString:param[@"encryptPassword"] publicKey:EncryptPassword]]
                              ];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest modifyPasswdWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

#pragma mark -
#pragma mark - 用户信息相关

//更新用户头像
+ (void)acc_updateUserAvatar:(UIImage *)img
                     sid:(NSString *)sid
                complete:(void(^)(BOOL updateSuccess, NSString * errStr))complete
{
    if ( !img){
        ELog(@"头像上传失败 -- image为nil");
        if (complete) {
            complete (NO,LocalizedString(@"头像上传失败"));
        }
        return;
    }
    
    if (!sid || !sid.length) {
        ELog(@"头像上传失败 -- sid 为空");
        if (complete) {
            complete (NO,LocalizedString(@"头像上传失败"));
        }
        return;
    }
    
    NSData      * imageData = UIImageJPEGRepresentation(img,0.1);
    NSString    * base64Str = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    NSDictionary    * dic = @{
                              @"_sid":sid,
                              @"imgData":base64Str
                              };
    
    [IXAFRequest uploadHeadPhotoWithParam:dic result:^(BOOL respont, BOOL success, id obj) {
        if (success) {
            if ([obj ix_isDictionary] && (([obj[@"newRet"] ix_isNumber]
                     && [obj[@"newRet"] isEqual:@(0)])
                    || ([obj[@"newRet"] isKindOfClass:[NSString class]]
                        && [obj[@"newRet"] isEqual:@"OK"]))) {
                    }
            if ([obj ix_isDictionary]) {
                id num = obj[@"newRet"];
                if (([num ix_isNumber] && [num integerValue] == 0)
                    || ([num ix_isString] && [num isEqualToString:@"OK"])) {
                    if (complete) {
                        complete(YES, @"");
                    }
                    return;
                }
            }
        }
        
        if (complete) {
            complete(NO, LocalizedString(@"头像上传失败"));
        }
    }];
}

#pragma mark - 更新用户信息
+ (void)acc_modifyUserInfoWithParam:(NSDictionary *)param result:(respResult)result
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url   = [NSString formatterUTF8:kUpdateCustomer];
    NSString    * paramStr = [NSString stringWithFormat:@"customer=%@&_timestamp=%ld&_url=%@&gts2CustomerId=%@&isAutoApprove=true",
                              param[@"customer"],
                              ([timeStamp longValue] * 1000),
                              url,
                              param[@"gts2CustomerId"]];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest modifyUserInfoWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

#pragma mark - 获取客户文件信息
+ (void)acc_customerFilesWithParam:(NSDictionary *)param result:(respResult)result
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url   = [NSString formatterUTF8:kCustomerFiles];
    NSString    * paramStr = [NSString stringWithFormat:@"customerNumber=%@&_timestamp=%ld&_url=%@",
                              param[@"customerNumber"],
                              ([timeStamp longValue] * 1000),
                              url
                              ];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest customerFilesWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}


+ (void)acc_dealWithResponse:(id)obj
               aimErrStr:(NSString *)string
                  result:(void(^)(BOOL requestSuccess,NSString * errStr))result
{
    NSString    * errString = @"";
    BOOL    requestSuccess = NO;
    
    if ([obj ix_isDictionary]) {
        if (obj[@"newRet"] && [obj[@"newRet"] isEqualToString:@"OK"]) {
            requestSuccess = YES;
        }else if (obj[@"newComment"] && [obj[@"newComment"] ix_isString]){
            errString =  [IXEntityFormatter getErrorInfomation:[(NSDictionary *)obj stringForKey:@"code"]];
        }else if([(NSDictionary *)obj objectForKey:@"error"] && [[(NSDictionary *)obj objectForKey:@"error"] ix_isDictionary]){
            errString = [(NSDictionary *)[(NSDictionary *)obj objectForKey:@"error"] objectForKey:@"message"];
        }
    }else if ([obj ix_isString]){
        errString = obj;
    }
    
    if (!errString.length && !requestSuccess) {
        errString = string;
    }
    
    if (result) {
        result(requestSuccess, errString);
    }
}

#pragma mark - 客户协议列表
+ (void)acc_protocolListWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kProtocolList];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&companyId=%@&infomationType=%@&lang=%@",
                              url,
                              [NSString stringWithFormat:@"%d",
                               CompanyID],
                              @"CUSTOMER_PROTOCOL",
                              [IXLocalizationModel currentCheckLanguage]];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest postProtocolListWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
         {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

#pragma mark - 客户协议详情
+ (void)acc_protocolDetailWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kProtocolDetail];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&id=%@",url,param[@"id"]];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest postProtocolDetailWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

#pragma mark - 开设真实账户
+ (void)acc_openAccountWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kUpdateCustomerAndNewAccount];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"customer=%@&accountList=%@&finance=%@&_url=%@&_timestamp=%@",
                              param[@"customer"],
                              param[@"accountList"],
                              param[@"finance"],
                              url,
                              timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign,
                                   @"_token":@""
                                   };
    
    [IXAFRequest postOpenAccountWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

#pragma mark - 用户设备绑定
+ (void)acc_addUserDeviceWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kAddUserDevice];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&devToken=%@&devName=%@&loginName=%@&companyId=%d&phonePrefix=%@",
                              url,
                              [IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]],
                              [IXDataProcessTools dealWithDeviceName:[[UIDevice currentDevice] name]],
                              param[@"loginName"],
                              CompanyID,
                              param[@"phonePrefix"]
                              ];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary *paramDic = @{
                               @"loginName":CompanyLoginName,
                               @"param":paramSign
                               };
    
    [IXAFRequest addUserDeviceWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

#pragma mark -  移除绑定的设备
+ (void)acc_deleteUserDeviceWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kDeleteUserDevice];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&deviceId=%@&tradeIxCustomerId=%@",
                              url,
                              param[@"deviceId"],
                              param[@"tradeIxCustomerId"]
                              ];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest deleteUserDeviceWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}

//上传附件
+ (void)acc_uploadBankPhotoWithParam:(id)param WithResult:(respResult)result
{
    if (param && param[@"imageData"]) {
        NSData      * imageData = [(NSDictionary *)param objectForKey:@"imageData"];
        NSString    * base64Str = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
        base64Str = [NSString formatterUTF8:base64Str];
        
        NSString    * url = [NSString formatterUTF8:kUploadFile];
        NSNumber    * timeStamp = [(NSDictionary *)param objectForKey:@"timeStamp"];
        NSString    * fileName = [(NSDictionary *)param objectForKey:@"fileName"];
        NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&_timestamp=%ld&fileName=%@&fileStr=%@",
                                  url,
                                  [timeStamp longValue] * 1000,
                                  fileName,
                                  base64Str];
        NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
        
        NSDictionary    * params = @{
                                     @"loginName":CompanyLoginName,
                                     @"param":paramSign
                                     };
        
        [IXAFRequest uploadBankPhotoWithParam:params result:^(BOOL respont, BOOL success, id obj) {
            [IXAFRequest dealWithDataRespont:respont
                                     success:success
                                      result:obj
                                  respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
            {
                result(success1,errCode,errStr,obj1);
            }];
        }];
    }
}


//提交附件
+ (void)acc_uploadAttachWithParam:(id)param WithResult:(respResult)result
{
    IXSupplementFileM   * model = [(NSDictionary *)param objectForKey:NSStringFromClass([IXSupplementFileM class])];
    NSString    * modelJson = [model toJSONString];
    modelJson = [modelJson substringFromIndex:9];
    modelJson = [modelJson substringToIndex:(modelJson.length - 1)];

    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kUpdateCustomerFiles];
    NSString    * cusId = [param stringForKey:@"gts2CustomerId"];
    NSString    * paramStr = [NSString stringWithFormat:@"gts2CustomerId=%@&files=%@&_timestamp=%ld&_url=%@&isAutoApprove=true",
                              cusId,
                              modelJson,
                              ([timeStamp longValue] * 1000),
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign
                                 };
    
    [IXAFRequest uploadBankFileInfoWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}


//提交修改
+ (void)acc_modifyBankInfoWithParam:(id)param WithResult:(respResult)result
{
    IXSupplementBankM   * model = [(NSDictionary *)param objectForKey:NSStringFromClass([IXSupplementBankM class])];
    NSString    * modelJson = [model toJSONString];
    modelJson = [modelJson substringFromIndex:9];
    modelJson = [modelJson substringToIndex:(modelJson.length - 1)];
    
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kUpdateCustomerBanks];
    NSString    * cusId = [param stringForKey:@"gts2CustomerId"];
    NSString    * paramStr = [NSString stringWithFormat:@"gts2CustomerId=%@&banks=%@&cards=[]&_timestamp=%ld&_url=%@&isAutoApprove=true",
                              cusId,
                              modelJson,
                              ([timeStamp longValue] * 1000),
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign
                                 };
    
    [IXAFRequest submitBankFileInfoWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}



+ (void)acc_dictChildListWithParam:(NSString *)param WithResult:(respResult)result
{
    if (!param || ![param ix_isString] || !param.length) {
        return;
    }
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kDictChildListNew];
    NSString    * paramStr = [NSString stringWithFormat:@"code=%@&_timestamp=%ld&_url=%@",
                              param,
                              ([timeStamp longValue] * 1000),
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest dictChildListWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
        {
            result(success1,errCode,errStr,obj1);
        }];
    }];
}


+ (void)acc_checkUserTypeWithResult:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kCheckUserType];
    NSString    * paramStr = [NSString stringWithFormat:@"_url=%@&gts2CustomerId=%ld",
                              url,
                              (long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId
                              ];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    
    [IXAFRequest deleteUserDeviceWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

+ (NSString *)appLang
{
    NSString * lan = [IXLocalizationModel currentShowLanguage];
#warning 印尼默认英语
//    if (SameString(lan, @"English")) {
        lan = @"en_US";
//    } else {
//        lan = @"zh_CN";
//    }
    
    return lan;
}





@end
