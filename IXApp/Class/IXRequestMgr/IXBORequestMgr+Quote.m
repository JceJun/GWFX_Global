//
//  IXBORequestMgr+Quote.m
//  IXApp
//
//  Created by Seven on 2017/8/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Quote.h"
#import "IXCpyConfig.h"
#import "NSObject+IX.h"
#import "RSA.h"

@implementation IXBORequestMgr (Quote)

/**
 订阅行情
 
 @param cataId 产品分类id
 @param autoR 自动续费
 @param complete 完成回调
 */
+ (void)quote_subQuoteWithCataId:(NSInteger)cataId
                       autoRenew:(BOOL)autoR
                        complete:(void(^)(BOOL success, long limitDate))complete
{
    NSString    * url = [NSString formatterUTF8:@"/cash/subscribe"];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"accountId=%llu&platform=IX&customerNumber=%@&_url=%@&_timestamp=%ld&symbolCataId=%ld&_token=%@&type=%d",
                              [IXUserInfoMgr shareInstance].userLogInfo.account.id_p,
                              [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                              url,[timeStamp longValue] * 1000,(long)cataId,
                              [IXUserInfoMgr shareInstance].userLogInfo.token,
                              autoR ? 0 : 1];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest subQuoteWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        if (respont && success && [obj ix_isDictionary]) {
            if ([obj[@"newRet"] isEqualToString:@"OK"] ) {
                NSDictionary * context = [(NSDictionary *)obj objectForKey:@"context"];
                if (context && [context ix_isDictionary]) {
                    long limitDate = [context int64ForKey:@"limitDate"];

                    if (complete) {
                        complete (YES, limitDate);
                    }
                    return ;
                }
            }
        }
        if (complete) {
            complete (NO, 0);
        }
        
    }];

}

/**
 取消订阅行情
 
 @param cataId  产品分类id
 @param complete 完成回调
 */
+ (void)quote_unsubQuoteWithCataId:(NSInteger)cataId complete:(void(^)(BOOL success))complete
{
    NSString    * url = [NSString formatterUTF8:@"/cash/unsubscribe"];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr = [NSString stringWithFormat:@"accountId=%llu&platform=IX&customerNumber=%@&_url=%@&_timestamp=%ld&symbolCataId=%ld&_token=%@",
                              [IXUserInfoMgr shareInstance].userLogInfo.account.id_p,
                              [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                              url,[timeStamp longValue] * 1000,
                              (long)cataId,
                              [IXUserInfoMgr shareInstance].userLogInfo.token];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{@"loginName":CompanyLoginName,@"param":paramSign};
    
    [IXAFRequest unsubQuoteWithParam:param result:^(BOOL respont, BOOL success, id obj) {
        if ([obj ix_isDictionary]) {
            if (complete) {
                complete ([obj[@"newRet"] isEqualToString:@"OK"]);
            }
        }
    }];
}

@end
