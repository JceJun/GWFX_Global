//
//  IXBORequestMgr+BroadSide.h
//  IXApp
//
//  Created by Seven on 2017/8/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"

@interface IXBORequestMgr (BroadSide)

//常见问答查询-列表
+ (void)bs_askedAndQuestionsListWithParam:(id)param result:(respResult)result;

//常见问答查询-详情
+ (void)bs_askedAndQuestionsDetailWithParam:(id)param result:(respResult)result;

//广告-列表
+ (void)bs_bannerAdvertisementListWithParam:(id)param result:(respResult)result;

//JPush消息-详情
+ (void)bs_jpushMessageDetailWithParam:(id)param result:(respResult)result;

//获取版本控制信息
+ (void)bs_getAppVersionControlInfo:(void(^)(NSDictionary * result))complete;

@end
