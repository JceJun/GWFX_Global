//
//  IXBORequestMgr+Asset.h
//  IXApp
//
//  Created by Seven on 2017/8/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"
#import "IXBOModel.h"
#import "IXUserInfoM.h"
@class IXUserInfoM;
@class IXIncashApplyM;
@class IXIncashResendMsmM;
@class IXIncashConfirmPayM;

typedef NS_ENUM(NSInteger, signInType) {
    signInTypeFailure,  //签到失败
    signInTypeComplete, //签到成功
    signInTypeAlreadySign,  //已经完成当天签到
};


/** 出入金相关接口 */
@interface IXBORequestMgr (Asset)

@property(nonatomic,strong)NSArray *channelList; // 支付渠道列表
@property(nonatomic,strong)NSDictionary *channelDic; // 选中的支付渠道





// ----------------------------------------------------------------
// ------------------------------- 签到 ----------------------------
// ----------------------------------------------------------------

/** 签到 */
+ (void)signIn:(void(^)(NSInteger amount,signInType type, NSString * errStr))complete;

/** 当天是否完成签到 */
+ (void)alreadySignIn:(void(^)(BOOL alreadySignIn, NSString * errStr))complete;




// ----------------------------------------------------------------
// ----------------------------- 出入金相关 -------------------------
// ----------------------------------------------------------------

#pragma mark - 入金

/** 获取入金基础url */
+ (void)incash_requstInCashBaseUrl:(void(^)(IXBaseUrlM * urlM))complete;

/** 获取可添加的银行列表 */
+ (NSArray <IXBank *>*)incash_requestEnableBankList:(void(^)(NSArray * bankList))complete;

/** 获取支付方式 */
+ (void)incash_requestPayMethodWithBank:(NSString *)code
                           complete:(void(^)(NSArray <IXPayMethod *>* methodArr, IXBaseUrlM * baseUrl))complete __deprecated_msg("2.4已弃用");

/** 获取入金token */
+ (void)incash_requestToken:(void(^)(BOOL success, NSString * token))complete;

/** 校验支付方式 */
+ (void)incash_verifyPaymethodWith:(id)param
                          complete:(void(^)(BOOL success, NSString * token))complete;


/** 获取支付方式类型 */
+ (void)incash_requestPayModeComplete:(void(^)(NSArray * arr, NSString * errStr))complete;

/**
 根据银行卡获取最优支付通道
 @param acc 银行代码 如：工商银行ICBKCNBJXXX
 */
+ (void)incash_requestBestModeWithBankCode:(NSString *)code
                                  complete:(void(^)(IXIncashPayModeM * mode, NSString * errStr, NSString * errCode))complete;

/** 快捷支付 - 申请支付，验证码由银行发送 */
+ (void)incash_requestApplyPayment:(IXIncashApplyM *)param
                          complete:(void(^)(IXIncashResendMsmM * applyResult, NSString * errStr))complete;

/**
 快捷支付 - 确认支付
 @param verifyCode 验证码
 */
+ (void)incash_requestConfirmPayment:(IXIncashConfirmPayM *)confirmParam
                            complete:(void(^)(BOOL paymentSuc, NSString * errStr, NSString * errCode))complete;

/**
 快捷支付 - 重发短信
 @param applyResult 申请支付方法中得到的支付申请结果
 */
+ (void)incash_requestNewVerifyCode:(IXIncashResendMsmM *)resendParam
                           complete:(void(^)(BOOL success, NSString * errStr))complete;


// 获取支付通道列表
+ (void)b_getPayMethodListByCond:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

/**
 根据 网关编码 获取银行列表
 @param gateWayCode 网关编码, 根据接口 [获取支付通道列表] 所获取的 gatewayCode
 */
+ (void)b_getEnableBankListByGateWayCodeType:(NSString *)gateWayCode rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;


#pragma mark - 出金


/** 取款实时账户信息查询 */
+ (void)drawcash_checkBankInfo:(void(^)(BOOL success, NSString * errStr, NSDictionary * dic))complete;

/** 出金 */
+ (void)drawCashWith:(NSDictionary *)param complete:(void(^)(BOOL success, NSString * errStr))complete;

/** 提现手续费 */
+ (void)drawCashFeeWithParam:(NSDictionary *)param result:(respResult)result;

/** 提现手续费规则 */
+ (void)drawCashFeeRuleWithParam:(NSDictionary *)param result:(respResult)result;

/** 用户能否取款 */
+ (void)drawCashStateWithParam:(NSDictionary *)param result:(respResult)result;

//解绑银行卡
+ (void)unbindBankWithParam:(NSDictionary *)param result:(respResult)result;

/** 请求后用户银行卡详细参数配置 */
+ (void)mgr_configBankCardList;

// 60.banner广告-列表
+ (void)b_getBannerAdvertisementList:(NSString *)infomationType rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

// 137.出金银行卡解绑
+ (void)b_cancelCustomerWithdrawBanks:(NSArray *)banks rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

//71.判断用户能否取款1
+ (void)b_getCustomerWouldCashOut:(NSInteger)bankOrder rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

//138.判断用户能否取款
+ (void)b_getCustomerWouldCashOutByWitCard:(NSInteger)bankOrder rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

@end
