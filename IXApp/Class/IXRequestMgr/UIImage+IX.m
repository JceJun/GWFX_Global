//
//  UIImage+IX.m
//  IXApp
//
//  Created by Seven on 2017/9/29.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "UIImage+IX.h"

@implementation UIImage (IX)

/** 压缩尺寸（裁剪） */
- (UIImage *)scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 压缩图片质量
 
 @param width 宽高最大值
 @param length 目标大小
 @param accuracy 误差范围
 */
- (NSData *)compressWidthWidth:(CGFloat)width aimLength:(NSInteger)length accuracyOfLength:(NSInteger)accuracy
{
    CGFloat imgWidth = self.size.width;
    CGFloat imgHeight = self.size.height;
    CGSize  aimSize;
    if (width >= imgWidth || width == 0) {
        aimSize = self.size;
    }else{
        aimSize = CGSizeMake(width, width*imgHeight/imgWidth);
    }
    UIImage * newImage = [self scaledToSize:aimSize];
    
    NSData  * data = UIImageJPEGRepresentation(newImage, 1);
    NSInteger imageDataLen = [data length];
    
    if (imageDataLen <= length + accuracy) {
        return data;
    }else{
        NSData * imageData = UIImageJPEGRepresentation( newImage, 0.99);
        if (imageData.length < length + accuracy) {
            return imageData;
        }
        
        CGFloat maxQuality = 1.0;
        CGFloat minQuality = 0.0;
        int flag = 0;
        
        while (1) {
            @autoreleasepool {
                CGFloat midQuality = (maxQuality + minQuality)/2;
                
                if (flag >= 6) {
                    NSData * data = UIImageJPEGRepresentation(newImage, minQuality);
                    return data;
                }
                flag ++;
                
                NSData * imageData = UIImageJPEGRepresentation(newImage, midQuality);
                NSInteger len = imageData.length;
                
                if (len > length+accuracy) {
                    maxQuality = midQuality;
                    continue;
                }else if (len < length-accuracy){
                    minQuality = midQuality;
                    continue;
                }else{
                    return imageData;
                    break;
                }
            }
        }
    }
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f); //宽高 1.0只要有值就够了
    UIGraphicsBeginImageContext(rect.size); //在这个范围内开启一段上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);//在这段上下文中获取到颜色UIColor
    CGContextFillRect(context, rect);//用这个颜色填充这个上下文
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();//从这段上下文中获取Image属性,,,结束
    UIGraphicsEndImageContext();
    
    return image;
}

@end
