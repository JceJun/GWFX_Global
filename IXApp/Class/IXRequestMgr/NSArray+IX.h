//
//  NSArray+IX.h
//  IXApp
//
//  Created by Seven on 2017/8/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (IX)


/** self必须为json数组 */
- (NSMutableArray *)ix_dealWithNullValue;

@end
