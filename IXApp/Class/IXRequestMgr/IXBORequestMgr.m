//
//  IXBORequestMgr.m
//  IXApp
//
//  Created by Seven on 2017/8/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"
#import "IXBORequestMgr+Asset.h"
#import "IXBORequestMgr+Region.h"
#import "IXBORequestMgr+Account.h"
#import "IXWUserInfo.h"
#import "SFHFKeychainUtils.h"
#import "IXKeyConfig.h"
#import "IXCpyConfig.h"
#import "NSArray+IX.h"
#import "RSA.h"
#import "NSObject+IX.h"
#import "IXUserInfoM.h"
#import "IXUserDefaultM.h"
#import "NSDictionary+json.h"
#import "NSArray+JSON.h"
#import "IXAccountBalanceModel.h"
#import "IXAppUtil.h"

NSString * const kCachePath = @"/Library/BoCache/";//缓存路径
#define kDictChildListNew   @"/app/appGetDictChildListNew" //获取数据字典

@implementation IXBORequestMgr

static IXBORequestMgr * instance;
+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [IXBORequestMgr new];
        [IXBORequestMgr refreshUserInfo:nil];
        //可添加银行卡的银行
        [IXBORequestMgr incash_requestEnableBankList:nil];
        //刷新地区信息
        [IXBORequestMgr region_refreshCountryList:nil];
        [IXBORequestMgr region_refreshProvinceWithNationalCode:kChinaNationalCode
                                                   countryCode:kChinaCountryCoiide
                                                      complete:nil];
        
        NSString    * url = [[NSUserDefaults standardUserDefaults] valueForKey:kIXHeadIcon];
        instance.headUrl = url.length ? url : @"";
    });
    return instance;
}

- (NSMutableDictionary *)launchHappenDic{
    if (!_launchHappenDic) {
        _launchHappenDic = [NSMutableDictionary dictionary];
    }
    return _launchHappenDic;
}

- (NSMutableDictionary *)paramDic{
    if (!_paramDic) {
        _paramDic = [NSMutableDictionary dictionary];
    }
    return _paramDic;
}


- (void)clearInstance
{
    _userInfo = nil;
    _cerArr = @[];
    _headUrl = @"";
    _fileArr = @[];
    _uploadedCustomerFile = NO;
    _launchHappenDic = nil;
    _bank  =nil;
    _bankFile = nil;
    _withDraw_bank = nil;
    _withdraw_bankFileArr = nil;
    _withDraw_bankAddressFile = nil;
}



+ (void)refreshUserInfo:(void(^)(BOOL success))complete
{
    __block BOOL suc = NO;
    __block BOOL suc1 = NO;
    
    dispatch_group_t    group = dispatch_group_create();
    dispatch_group_enter(group);
    [self loginBo:^(BOOL response, BOOL success, NSString * errStr) {
        dispatch_group_leave(group);
        suc = success;
    }];
    
    dispatch_group_enter(group);
    [self refreshCredentialInfo:^(BOOL success) {
        dispatch_group_leave(group);
        suc1 = success;
    }];
    
    if (complete) {        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            complete (suc && suc1);
        });
    }
}

/** 登录bo */
+ (void)loginBo:(void(^)(BOOL response, BOOL success, NSString * errStr))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    timeStamp = [NSNumber numberWithLong:[IXEntityFormatter getCurrentTimeInterval] * 1000];
    
    NSString    * paramStr = [NSString stringWithFormat:@"userId=%@&password=%@&lang=zh_TW&userType=real&companyId=%d&platTypeKey=webui&timeStamp=%lld",
                              [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                              [self getPassword],
                              CompanyID,
                              ([timeStamp longLongValue] * 1000)];
    //如果是微信登录
    if ([IXWUserInfo getUnionId]) {
        paramStr = [NSString stringWithFormat:@"userId=%@&unionId=%@&lang=zh_TW&userType=real&companyId=%d&platTypeKey=webui&timeStamp=%lld",
                    [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                    [IXWUserInfo getUnionId],
                    CompanyID,
                    ([timeStamp longLongValue] * 1000)];
    }
    
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * param = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign
                                };
    
    [IXAFRequest postLoginBOWithParam:param result:^(BOOL respont, BOOL success, id obj) {
//        NSLog(@"%@",[obj jsonString]);
        BOOL    suc = NO;
        __block NSString    * errStr = @"";
        if ([obj ix_isDictionary]) {
            if (obj[@"user"]) {
                if (obj[@"user"][@"result"]) {
                    if (obj[@"user"][@"result"][@"result"]) {
                        /*---------------------------------登录: 信息----------------------------------*/
                        [IXBORequestMgr shareInstance].boLoginDic = obj[@"user"][@"result"][@"result"];

                        /*---------------------------------入金:信息---------------------------------------*/
                        [IXBORequestMgr shareInstance].bankArr = [IXBORequestMgr shareInstance].boLoginDic[@"customerInfoBankParams"];
                        
                        /*---------------------------------出金:信息---------------------------------------*/
                        [IXBORequestMgr shareInstance].withdraw_bankArr = [IXBORequestMgr shareInstance].boLoginDic[@"customerInfoBankWithdrawParams"];
                    }
                }
            }
            IXUserInfoM   * userInfo = [IXUserInfoM userInfoWithDic:obj];
            if (userInfo.gts2CustomerId) {
                [IXBORequestMgr shareInstance].gts2CustomerId = [@(userInfo.gts2CustomerId) stringValue];
                [IXBORequestMgr shareInstance].userInfo = userInfo;
                [IXBORequestMgr shareInstance].headUrl = userInfo.headImgUrl;
                NSString *nationalCode = [IXUserDefaultM nationalCodeByCode:userInfo.detailInfo.nationality];
                [IXUserDefaultM saveCountryCode:nationalCode];
                
                NSString    * nickName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
                if (!nickName.length) {
                    if (userInfo.detailInfo.mobilePhone.length) {
                        [[NSUserDefaults standardUserDefaults] setObject:userInfo.detailInfo.mobilePhone forKey:kIXLoginName];
                    } else if (userInfo.detailInfo.email.length) {
                        [[NSUserDefaults standardUserDefaults] setObject:userInfo.detailInfo.email forKey:kIXLoginName];
                    }
                }
                [self mgr_configBankCardList];
            }
            
            if (userInfo.detailInfo) {
                suc = YES;
            } else {
                errStr = LocalizedString(@"获取用户信息失败");
            }
        
        } else if ([obj ix_isString]) {
            errStr = obj;
        }
        
        if (complete) {
            complete (respont, suc, errStr);
        }
    }];
}


+ (void)refreshCredentialInfo:(void(^)(BOOL success))complete
{
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * url = [NSString formatterUTF8:kDictChildListNew];
    NSString    * paramStr = [NSString stringWithFormat:@"code=IdDocument&_timestamp=%ld&_url=%@",
                              ([timeStamp longValue] * 1000),
                              url];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * params = @{
                                 @"loginName":CompanyLoginName,
                                 @"param":paramSign
                                 };
    
    [IXAFRequest dictChildListWithParam:params result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1,
                                           NSString *errCode,
                                           NSString *errStr,
                                           id obj1)
        {
            if (success1) {
                if (obj1 && [obj1 ix_isArray]) {
                    [self processCredentialInfo:obj1];
                }
                if (complete) {
                    complete(YES);
                }
            } else {
                complete(NO);
            }
        }];
    }];
}

+ (void)processCredentialInfo:(id)obj
{
    if ([obj ix_isArray]) {
        NSMutableArray  * cerList = [@[] mutableCopy];
        NSArray * cerArr = (NSArray *)obj;
        
        for (int i = 0; i < cerArr.count; i++) {
            id cerObj = cerArr[i];
            if ([cerObj ix_isDictionary]) {
                IXCredentialM   * model = [[IXCredentialM alloc] initWithObj:cerObj];
                [cerList addObject:model];
            }
        }
        [IXBORequestMgr shareInstance].cerArr = cerList;
    }
}

/** 用户解绑银行卡时调用 */
+ (void)removeBankCard:(IXUserBankM *)bank
{
    [[IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams
     enumerateObjectsWithOptions:NSEnumerationReverse
     usingBlock:^(IXUserBankM * _Nonnull obj,
                  NSUInteger idx,
                  BOOL * _Nonnull stop)
    {
        if ([obj.bankAccountName isEqualToString:bank.bankAccountName]
            && [obj.bankAccountNumber isEqualToString:bank.bankAccountNumber]) {
            [[IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams removeObject:obj];
        }
    }];
}

/** 添加银行卡时调用 */
+ (void)addBankCard:(IXUserBankM *)bank
{
    __block BOOL exist = NO;
    [[IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams
     enumerateObjectsUsingBlock:^(IXUserBankM * _Nonnull obj,
                                  NSUInteger idx,
                                  BOOL * _Nonnull stop)
    {
        if ([obj.bankAccountName isEqualToString:bank.bankAccountName]
            && [obj.bankAccountNumber isEqualToString:bank.bankAccountNumber]) {
            exist = YES;
        }
    }];
    
    if (!exist) {
        [[IXBORequestMgr shareInstance].userInfo.detailInfo.customerInfoBankParams addObject:bank];
    }
}

#pragma mark -
#pragma mark - private method

/** 获取用户密码 */
+ (NSString *)getPassword
{
    NSString    * userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    NSString    * passWord = [[NSString alloc] init];
    
    if (userName && userName.length > 0) {
        passWord = [SFHFKeychainUtils getPasswordForUsername:userName
                                              andServiceName:kServiceName
                                                       error:nil];
        
        if (!passWord) {
            NSString *errMsg = [NSString stringWithFormat:@"%s at line %d called on secondary thread",
                                __FUNCTION__, __LINE__];
            ELog(errMsg);
            return @"";
        } else {
            DLog(@"password information is:%@========",passWord);
        }
        
        passWord = [IXDataProcessTools md5StringByString:passWord];
    }
    return passWord;
}

/** 获取信息缓存位置 */
+ (NSString *)cachePath{
    NSString    * path = [NSHomeDirectory() stringByAppendingString:kCachePath];
    
    BOOL    isDirectory = YES;
    BOOL    exist = [[NSFileManager defaultManager] fileExistsAtPath:path
                                                         isDirectory:&isDirectory];
    
    if (!exist) {
        NSError * error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&error];
    }
    
    return path;
}

//21.入金-获取银行列表(从FO获取)
+ (void)b_allBankList:(void(^)(NSArray *bankList))hd{
    [IXBORequestMgr incash_requestEnableBankList:^(NSArray *bankList) {
        if (hd) {
            [IXBORequestMgr shareInstance].allBankList = bankList;
            hd(bankList);
        }
    }];
}

// 初始化新增入金银行卡信息
+ (void)resetAddDepostiBankContainer{
    [IXBORequestMgr shareInstance].bank = nil;
    [IXBORequestMgr shareInstance].bankFile = nil;
    [IXBORequestMgr shareInstance].withDraw_bankAddressFile = nil;
    
//    [[IXBORequestMgr shareInstance].fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSDictionary *info = obj;
//        NSString *ftpFilePath = info[@"ftpFilePath"];
//        if (ftpFilePath && [ftpFilePath length]) {
//            // 有文件
//        }else{
//            // 添加缺少位置的文件初始参数
//            if ([info[@"fileType"] containsString:@"FILE_TYPE_BANK"]) {
//                [IXBORequestMgr shareInstance].deposit_bankFile = info;
//                [[IXBORequestMgr shareInstance].deposit_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    if ([info[@"fileType"] containsString:[obj[@"bankOrder"] stringValue]]) {
//                        [IXBORequestMgr shareInstance].deposit_bank = obj;
//                        *stop = YES;
//                    }
//                }];
//
//                *stop = YES;
//            }
//        }
//    }];
    NSArray *deposit_bankArr = [IXBORequestMgr shareInstance].bankArr;
    [deposit_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj[@"bank"] length]) {
            [IXBORequestMgr shareInstance].bank = obj;
            
            *stop = YES;
        }
    }];
    
    [IXBORequestMgr filterDepositBankFile:[IXBORequestMgr shareInstance].bank];
}

+ (NSDictionary *)filterDepositBankFile:(NSDictionary *)deposit_bank{
    __block NSInteger bankOrder = 1;
    NSDictionary *info = deposit_bank;
    if (info[@"bankOrder"]) {
        bankOrder = [info[@"bankOrder"] integerValue];
    }
    
    [[IXBORequestMgr shareInstance].fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj[@"fileType"] containsString:[NSString stringWithFormat:@"FILE_TYPE_BANK_%@",@(bankOrder)]]){
            [IXBORequestMgr shareInstance].bankFile = obj;
            bankOrder = [info[@"bankOrder"] integerValue];
            *stop = YES;
        }
    }];
    
    [[IXBORequestMgr shareInstance].fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *addressFile = [NSString stringWithFormat:@"FILE_TYPE_WITHDRAW_BANK_ADDRESS_%@",@(bankOrder)];
        if ([obj[@"fileType"] containsString:addressFile]) {
            [IXBORequestMgr shareInstance].withDraw_bankAddressFile = obj;
            *stop = YES;
        }
    }];

    return [IXBORequestMgr shareInstance].bankFile;
}

+ (NSDictionary *)filterWithDrawBankFile:(NSDictionary *)withDraw_bank{
    __block NSInteger bankOrder = 1;
    NSDictionary *info = withDraw_bank;
    if (info[@"bankOrder"]) {
        bankOrder = [info[@"bankOrder"] integerValue];
    }

    [[IXBORequestMgr shareInstance].fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj[@"fileType"] containsString:[NSString stringWithFormat:@"FILE_TYPE_WITHDRAW_BANK_%@",@(bankOrder)]]){
            [IXBORequestMgr shareInstance].withDraw_bankFile = obj;
            bankOrder = [info[@"bankOrder"] integerValue];
            *stop = YES;
        }
    }];
    
    [[IXBORequestMgr shareInstance].fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *addressFile = [NSString stringWithFormat:@"FILE_TYPE_WITHDRAW_BANK_ADDRESS_%@",@(bankOrder)];
        if ([obj[@"fileType"] containsString:addressFile]) {
            [IXBORequestMgr shareInstance].withDraw_bankAddressFile = obj;
            *stop = YES;
        }
    }];
    return [IXBORequestMgr shareInstance].withDraw_bankFile;
}



// 初始化新增出金银行卡信息
+ (void)resetAddWithDrawBankContainer{
    [IXBORequestMgr shareInstance].withDraw_bank = nil;
    [IXBORequestMgr shareInstance].withDraw_bankFile = nil;
    [IXBORequestMgr shareInstance].withDraw_bankAddressFile = nil;
    
//    [[IXBORequestMgr shareInstance].fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSDictionary *info = obj;
//        NSString *ftpFilePath = info[@"ftpFilePath"];
//        if (ftpFilePath && [ftpFilePath length]) {
//            // 有文件
//        }else{
//            // 添加缺少位置的文件初始参数
//            if ([info[@"fileType"] containsString:@"FILE_TYPE_WITHDRAW_BANK"]) {
//                [IXBORequestMgr shareInstance].withDraw_bankFile = info;
//                [[IXBORequestMgr shareInstance].withdraw_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    if ([info[@"fileType"] containsString:[obj[@"bankOrder"] stringValue]]) {
//                        [IXBORequestMgr shareInstance].withDraw_bank = obj;
//                        *stop = YES;
//                    }
//                }];
//                *stop = YES;
//            }
//        }
//    }];
    
    NSArray *deposit_bankArr = [IXBORequestMgr shareInstance].withdraw_bankArr;
    [deposit_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj[@"bank"] length]) {
            [IXBORequestMgr shareInstance].withDraw_bank = obj;
            *stop = YES;
        }
    }];
    
    [IXBORequestMgr filterWithDrawBankFile:[IXBORequestMgr shareInstance].withDraw_bank];
}

// 合并有效的入金银行卡信息（含附件审核状态）
+ (NSMutableArray *)mergedBankList{
    NSMutableArray *mergedBankList = [NSMutableArray array];
    NSArray *deposit_bankArr = [IXBORequestMgr shareInstance].bankArr;
    [deposit_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *bankInfo = [obj mutableCopy];
        if ([obj[@"bank"] length]) {
            NSDictionary *bankFile = [IXBORequestMgr filterDepositBankFile:bankInfo];
            bankInfo[@"proposalStatus"] = bankFile[@"proposalStatus"];
            bankInfo[@"type"] = @"Help2pay";
            IXUserBankM * bank = [IXUserBankM userBankWithDic:bankInfo];
            [mergedBankList addObject:bank];
        }
    }];
    
    return mergedBankList;
}

// 合并有效的出金银行卡信息（含附件审核状态）
+ (NSMutableArray *)mergedWithDrawBankList{
    NSMutableArray *mergedBankList = [NSMutableArray array];
    NSArray *withdraw_bankArr = [IXBORequestMgr shareInstance].withdraw_bankArr;
    [withdraw_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *bankInfo = [obj mutableCopy];
        if ([obj[@"bank"] length]) {
            NSDictionary *bankFile = [IXBORequestMgr filterWithDrawBankFile:bankInfo];
            if ([IXBORequestMgr shareInstance].withDraw_bankAddressFile) {
                bankInfo[@"proposalStatus"] = [IXBORequestMgr mergeFileStatus:@[[IXBORequestMgr shareInstance].withDraw_bankAddressFile[@"proposalStatus"],bankFile[@"proposalStatus"]] isBackNumber:YES];
            }else{
                bankInfo[@"proposalStatus"] = @"0";
            }
            bankInfo[@"type"] = @"Transfer Money";
            IXUserBankM * bank = [IXUserBankM userBankWithDic:bankInfo];
            [mergedBankList addObject:bank];
        }
    }];
    return mergedBankList;
}

// 合并文件优先显示状态
+ (NSString *)mergeFileStatus:(NSArray *)statusArr isBackNumber:(BOOL)number{
    NSString *status = @"";
    if ([statusArr containsObject:@"Not Uploaded"] || [statusArr containsObject:@"0"]) {
        status = @"Not Uploaded";
        if (number) {
            status = @"0";
        }
    }else if ([@[status,status] containsObject:@"Not Approved"] || [statusArr containsObject:@"-1"]){
        status = @"Not Approved";
        if (number) {
            status = @"-1";
        }
    }else if ([@[status,status] containsObject:@"Checking"] || [statusArr containsObject:@"1"]){
        status = @"Checking";
        if (number) {
            status = @"1";
        }
    }else {
        status = @"Approved";
        if (number) {
            status = @"2";
        }
    }
    return status;
}

// 63.获取客户文件信息
+ (void)b_getCustomerFiles:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString    * url = @"/app/appGetCustomerFiles";
    NSDictionary *param = @{@"customerNumber":@([IXBORequestMgr shareInstance].userInfo.detailInfo.customerNumber)
                            };
    [IXBORequestMgr shareInstance].fileArr = [NSMutableArray array];
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        if ([obj1 ix_isArray]) {
            NSArray *obj  = obj1;
            if (!obj || obj.count == 0) {
                rsp(success1,errCode1,errStr1,obj1);
                return;
            }
            NSMutableArray *tmpArr = [obj mutableCopy];
            if (tmpArr.count) {
                [IXBORequestMgr shareInstance].fileArr = [tmpArr mutableCopy];
                
                NSArray *fileArr = [IXBORequestMgr shareInstance].fileArr;
                NSMutableArray *id_docs = [NSMutableArray array];
                NSMutableArray *address_doc = [NSMutableArray array];
                NSMutableArray *bankArr = [NSMutableArray array];
                NSMutableArray *withdraw_bankArr = [NSMutableArray array];
                NSMutableArray *withdraw_bankAddressArrr = [NSMutableArray array];
                
                [fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *info = obj;
                    NSString *ftpFilePath = info[@"ftpFilePath"];
                    if (ftpFilePath) {
                        if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_IDCARD_FRONT"]) {
                            // 身份证正面图
                            [id_docs addObject:info];
                        }else if([info[@"fileType"] isEqualToString:@"FILE_TYPE_IDCARD_BACK"]){
                            // 身份证反面图
                            [id_docs addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_ADDRESS"]){
                            // 地址证明图
                            [address_doc addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_BANK_1"]){
                            // 入金银行卡1
                            [bankArr addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_BANK_2"]){
                            // 入金银行卡2
                            [bankArr addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_BANK_3"]){
                            // 入金银行卡3
                            [bankArr addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_WITHDRAW_BANK_1"]){
                            // 出金银行卡1
                            [withdraw_bankArr addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_WITHDRAW_BANK_2"]){
                            // 出金银行卡2
                            [withdraw_bankArr addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_WITHDRAW_BANK_3"]){
                            // 出金银行卡3
                            [withdraw_bankArr addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_WITHDRAW_BANK_ADDRESS_1"]){
                            // 出金银行卡地址1
                            [withdraw_bankAddressArrr addObject:info];
                        }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_WITHDRAW_BANK_ADDRESS_2"]){
                            // 出金银行卡地址2
                            [withdraw_bankAddressArrr addObject:info];
                        }
                        else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_WITHDRAW_BANK_ADDRESS_3"]){
                            // 出金银行卡地址3
                            [withdraw_bankAddressArrr addObject:info];
                        }
                    }else{
                        // 添加缺少位置的文件初始参数
                        if ([info[@"fileType"] containsString:@"FILE_TYPE_WITHDRAW_BANK"]) {
                            if (![IXBORequestMgr shareInstance].bankFile) {
                                [IXBORequestMgr shareInstance].bankFile = info;
                            }
                        }
                        if ([info[@"fileType"] containsString:@"FILE_TYPE_WITHDRAW_BANK_ADDRESS"]) {
                            if (![IXBORequestMgr shareInstance].withDraw_bankAddressFile) {
                                [IXBORequestMgr shareInstance].withDraw_bankAddressFile = info;
                            }
                        }
                        if ([info[@"fileType"] containsString:@"FILE_TYPE_WITHDRAW_BANK"]) {
                            if (![IXBORequestMgr shareInstance].withDraw_bankFile) {
                                [IXBORequestMgr shareInstance].withDraw_bankFile = info ;
                            }
                        }
                    }
                }];
                [IXBORequestMgr shareInstance].id_docs = id_docs;  // 身份证文件
                [IXBORequestMgr shareInstance].address_doc = address_doc; // 地址证明文件
                [IXBORequestMgr shareInstance].bankFileArr = bankArr; // 入金文件
                [IXBORequestMgr shareInstance].withdraw_bankFileArr = withdraw_bankArr; // 出金文件
                [IXBORequestMgr shareInstance].withdraw_bankAddressFileArr = withdraw_bankAddressArrr; // 出金地址簿文件
            }
            if ([IXBORequestMgr shareInstance].id_docs.count +
                [IXBORequestMgr shareInstance].address_doc.count == 3) {
                [IXBORequestMgr shareInstance].uploadedCustomerFile = YES;  // 文件上传提示标志
            } else {
                [IXBORequestMgr shareInstance].uploadedCustomerFile = NO;
            }
            rsp(success1,errCode1,errStr1,obj1);
        }else{
            rsp(success1,errCode1,errStr1,obj1);
        }
    }];
}



//12.用户银行资料修改提交 (入金银行卡新增、修改)
+ (void)b_appUpdateCustomerBanks:(NSArray *)banks rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp
{
    NSString *url = @"/app/appUpdateCustomerBanks";
    NSDictionary *dic = nil;// [IXBORequestMgr shareInstance].bankAdditionInfo;
    
    NSMutableDictionary *bankInfo = [banks[0] mutableCopy];
    
    [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        bankInfo[key] = obj;
    }];
    
    NSString *bankStr = [@[bankInfo] toJSONString];
    NSString *param = [NSString stringWithFormat:@"gts2CustomerId=%@&banks=%@&isAutoApprove=true",[IXBORequestMgr shareInstance].gts2CustomerId,bankStr];
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
}

// 136. 出金银行卡资料修改
+ (void)b_updateCustomerWithdrawBanks:(NSArray *)banks rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString *url = @"/app/appUpdateCustomerWithdrawBanks";
    
    NSString *bankStr = [banks toJSONString];
    NSString *param = [NSString stringWithFormat:@"gts2CustomerId=%@&banks=%@&isAutoApprove=true",[IXBORequestMgr shareInstance].gts2CustomerId,bankStr];
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
}

/*====================================  接口重构 ========================================*/
// 10.上传文件
+ (void)b_appUploadFile:(UIImage *)image rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    CGFloat idx = 0.5;
    NSData *data = UIImageJPEGRepresentation(image, idx);
    
    while (data.length > 400000 && idx > 0.1) { // 文件不大于400KB
        idx = 0.1;
        data = UIImageJPEGRepresentation(image, idx);
    }
    
    NSString *url = @"/app/appUploadFile";
    if (data) {
        NSString *base64Str = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
        base64Str = [NSString formatterUTF8:base64Str];
        
        NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
        NSString *fileName = [NSString stringWithFormat:@"%@.png",[timeStamp stringValue]];
        NSDictionary *param = @{@"fileName":fileName, // 文件名
                                @"fileStr":base64Str, // 文件转base64字符串,字符串中不包涵换行符，转base64后经过urlEncode转码
                                };
        // 方式3
        [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
    }
}



// 11.用户文件修改提交
+ (void)b_updateCustomerFiles:(NSArray *)files  rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString *url = @"/app/appUpdateCustomerFiles";
    NSString *gts2CustomerId =  [@([IXBORequestMgr shareInstance].userInfo.gts2CustomerId) stringValue];
    
    
    //    NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    //    NSString *fileName = [NSString stringWithFormat:@"%@.png",[timeStamp stringValue]];
    //
    //    files =    @[@{
    //                @"gts2CustomerId":gts2CustomerId,
    //                @"fileName":fileName,
    //                @"fileType":@"",
    //                @"id":@"", // 列编号,登录接口用户信息中返回
    //
    //                @"filePath":@"", // 上传接口返回的：fileStorePath
    //                @"ftpFilePath":@"", // 上传接口返回的：webFilePath
    //                },
    //                @{}
    //                 ];
    
    
    
    NSString *filesStr = [files toJSONString];
    NSString *param = [NSString stringWithFormat:@"gts2CustomerId=%@&files=%@&isAutoApprove=true",gts2CustomerId,filesStr];
    ;
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
}

//127.取款[出金法币]
+ (void)b_addWithdrawNew:(NSString *)transAmount
        withdrewBankName:(NSString *)withdrewBankName
     withdrewBankAccount:(NSString *)withdrewBankAccount
 withdrewBankAccountName:(NSString *)withdrewBankAccountName
                     rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString *url = @"/app/appAddWithdrawNew";
    
    NSDictionary *cashout = @{@"gts2AccountId":[IXBORequestMgr shareInstance].gts2CustomerId,
                              @"transAmount":transAmount,
                              @"withdrewBankName":withdrewBankName,
                              @"withdrewBankAccount":withdrewBankAccount,
                              @"withdrewBankAccountName":withdrewBankAccountName,
                              @"transCurrency":[IXAccountBalanceModel shareInstance].currency,
                              @"payCurrency":[IXAccountBalanceModel shareInstance].currency,
                              };

    NSString *cashOutStr = [cashout toJSONString];
    NSString *param = [NSString stringWithFormat:@"cashout=%@",cashOutStr];
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:rsp];
}

//39.埋点
+ (void)b_appAddEventTrack:(NSString *)mark  rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    NSString *url = @"/app/appAddEventTrack";

    NSMutableDictionary * info = [@{} mutableCopy];
    if (mark.length) {
        [info setValue:mark forKey:@"key"];
    }else{
        [info setValue:@"" forKey:@"Key"];
    }
    [info setValue:@"" forKey:@"Option"];
    [info setValue:@"iOS" forKey:@"AppType"];
    [info setValue:[IXAppUtil deviceType] forKey:@"PhoneType"];
    [info setValue:@(CompanyID) forKey:@"CompanyId"];
    [info setValue:@([IXUserInfoMgr shareInstance].userLogInfo.account.userid) forKey:@"UserId"];
    [info setValue:@([IXUserInfoMgr shareInstance].userLogInfo.account.id_p) forKey:@"AccountId"];
    [info setValue:@((NSUInteger)[[NSDate date] timeIntervalSince1970]/1000) forKey:@"Time"];
    
    NSString *paramStr = [NSString stringWithFormat:@"jsonStr=\"%@\"",[info toJSONString]];
    
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:paramStr result:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        if (rsp) {
            rsp(success1,errCode1,errStr1,obj1);
        }
    }];
    
}

//142. 余额变动状态修改接口
+ (void)b_customerActiviyBanlanceChange:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    if (![IXBORequestMgr shareInstance].customerActivityBalanceId.length) {
        return;
    }
    
    NSString *url = @"/app/customerActivityBalanceChange"; 
    if (![IXBORequestMgr shareInstance].customerActivityBalanceId.length) {
        return;
    }
    
    NSDictionary *param = @{@"id":[IXBORequestMgr shareInstance].customerActivityBalanceId,
                            };
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        if (rsp) {
            rsp(success1,errCode1,errStr1,obj1);
        }
    }];
}

// 143.余额变动状态查询
+ (void)b_customerActivityBalanceQuery:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp{
    if (![IXBORequestMgr shareInstance].gts2CustomerId.length) {
        return;
    }
    
    NSString *url = @"/app/appCustomerActivityBalanceQuery";
    NSDictionary *param = @{@"customerNo":[IXBORequestMgr shareInstance].gts2CustomerId,
                            };
    
    
    // 方式3
    [IXAFRequest doReqMethod3Url:url pram:param result:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        if (success1) {
            [IXBORequestMgr shareInstance].activityBalanceDic = obj1;
            [IXBORequestMgr shareInstance].customerActivityBalanceId = [obj1[@"id"] stringValue];
        }
        if (rsp) {
            rsp(success1,errCode1,errStr1,obj1);
        }
    }];
}


- (NSString *)gts2CustomerId{
    _gts2CustomerId = [@([IXBORequestMgr shareInstance].userInfo.gts2CustomerId) stringValue];
    return _gts2CustomerId;
}

- (NSString *)customerNo{
    _customerNo = [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo;
    return _customerNo;
}

- (NSString *)accountId{
    _accountId = [@([IXUserInfoMgr shareInstance].userLogInfo.account.id_p) stringValue];
    return _accountId;
}

- (NSString *)customerActivityBalanceId{
    if (!_customerActivityBalanceId) {
        _customerActivityBalanceId = @"";
    }
    return _customerActivityBalanceId;
    
}


@end
