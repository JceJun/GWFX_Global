//
//  IXBORequestMgr+Survey.m
//  IXApp
//
//  Created by Evn on 2017/10/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr+Survey.h"
#import "IXUserInfoM.h"
#import "IXCpyConfig.h"
#import "RSA.h"

#define kAppGetSurveyQuestionList   @"/app/appGetSurveyQuestionList"
#define kAppUpdateSurveyPaper       @"/app/appUpdateSurveyPaper"

@implementation IXSurInfoM
@end
@implementation IXSurRecordM
@end
@implementation IXSurRecords
@end

@implementation IXBORequestMgr (Survey)

// 查询问题列表
+ (void)survey_questionListWith:(IXSurveyReqType)type result:(respResult)result
{

    NSString    * url = [NSString formatterUTF8:kAppGetSurveyQuestionList];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSDictionary *dic = @{
                          @"companyId":[NSString stringWithFormat:@"%d",CompanyID],
                          @"actorId":[NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId],
                          @"lang":[IXLocalizationModel currentCheckLanguage],
                          @"type":@(type)
                          };
    NSString    * paramStr  = [NSString stringWithFormat:@"param=%@&_url=%@&_timestamp=%@",dic,
                               url,
                               timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary    * paramDic = @{
                                   @"loginName":CompanyLoginName,
                                   @"param":paramSign
                                   };
    [IXAFRequest postDrawCashListWithParam:paramDic result:^(BOOL respont, BOOL success, id obj) {
        [IXAFRequest dealWithDataRespont:respont
                                 success:success
                                  result:obj
                              respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
         {
             result(success1,errCode,errStr,obj1);
         }];
    }];
}

// 提交问卷调查
+ (void)survey_submitPaperWithParam:(NSDictionary *)param result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kAppUpdateSurveyPaper];
    NSError * parseError = nil;
    NSData  * jsonData = [NSJSONSerialization dataWithJSONObject:param[@"records"] options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString    * jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSString    * paramStr  = [NSString stringWithFormat:@"resultParam=%@&records=%@&_url=%@&_timestamp=%@",
                               param[@"resultParam"],
                              jsonStr,
                               url,
                               timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary * paramDic = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign
                                };
    
    [IXAFRequest postTradeRecordListWithParam:paramDic
                                       result:^(BOOL respont, BOOL success, id obj)
     {
         [IXAFRequest dealWithDataRespont:respont
                                  success:success
                                   result:obj
                               respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
          {
              result(success1,errCode,errStr,obj1);
          }];
     }];
}


+ (void)survey_submitPaperWithInfo:(IXSurInfoM *)info records:(IXSurRecords*)recordM result:(respResult)result
{
    NSString    * url = [NSString formatterUTF8:kAppUpdateSurveyPaper];
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    NSMutableString * recordStr = [@"[" mutableCopy];
    for (int i = 0; i < recordM.records.count; i ++) {
        IXSurRecordM    * m = recordM.records[i];
        [recordStr appendString:[m toJSONString]];
        if (i < recordM.records.count-1) {
            [recordStr appendString:@","];
        } else {
            [recordStr appendString:@"]"];
        }
    }

    NSString    * paramStr  = [NSString stringWithFormat:@"resultParam=%@&records=%@&_url=%@&_timestamp=%@",
                               [info toJSONString],
                               recordStr,
                               url,
                               timeStamp];
    NSString    * paramSign = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary * paramDic = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramSign
                                };
    
    [IXAFRequest postTradeRecordListWithParam:paramDic
                                       result:^(BOOL respont, BOOL success, id obj)
     {
         [IXAFRequest dealWithDataRespont:respont
                                  success:success
                                   result:obj
                               respResult:^(BOOL success1, NSString *errCode, NSString *errStr, id obj1)
          {
              if (result) {
                  result(success1,errCode,errStr,obj1);
              }
          }];
     }];
}


@end
