//
//  IXBORequestMgr+MsgCenter.h
//  IXApp
//
//  Created by Seven on 2017/8/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"

@interface IXBORequestMgr (MsgCenter)


/** 获取全部未读消息条数 */
+ (void)msg_obtainAllNoReadCount:(void(^)(BOOL success, NSInteger count))complete;

/**
 获取未读条数
 
 @param type    消息类型，使用服务器返回的字符串 (IXMsgCenterTypeM code)
 该参数为nil或@""时，则请求全部消息未读数
 @param complete 请求结束
 */
+ (void)msg_obtainNoReadCountWithType:(NSString *)type
                             complete:(void(^)(NSInteger count))complete;

/**
 刷新消息类型
 
 @param complete 网络请求成功后刷新
 @return 缓存消息类型
 */
+ (NSArray *)msg_refreshMsgTypes:(void(^)(NSArray <NSString *>* types,
                                          NSString * errorStr))complete;


/**
 获取消息列表
 
 @param type    消息类型，使用服务器返回的字符串 (IXMsgCenterTypeM code)
 该参数为nil或@""时，则请求全部消息未读数
 @param page    当前消息页码
 @param complete 请求结束
 */
+ (void)msg_obtainMsgListWithType:(NSString *)type
                             page:(NSInteger)page
                         complete:(void(^)(NSArray <IXMsgCenterM *>*list))complete;


/**
 获取消息列表和未读数
 
 @param type    消息类型，使用服务器返回的字符串 (IXMsgCenterTypeM code)
 该参数为nil或@""时，则请求全部消息未读数
 @param page    当前消息页码
 @param complete 请求结束回调
 */
+ (void)msg_obtainMsgListAndNoReadCountWithType:(NSString *)type
                                           page:(NSInteger)page
                                       complete:(void(^)(NSArray <IXMsgCenterM *>*lst,
                                                         NSInteger noReadCount))complete;


/**
 全部消息标为已读
 
 @param complrete 请求结束回调
 */
+ (void)msg_readAllMsg:(void(^)(BOOL success, NSString * errStr))complete;


/**
 获取消息详情
 
 @param msgId 消息id
 @param complete 请求完成回调
 */
+ (void)msg_obtainMsgDetailWithId:(NSInteger)msgId
                         complete:(void(^)(IXMsgCenterM * m,
                                           NSString * errStr))complete;

/**
 用户消息/邮件语种修改
 
 @param messageLang 消息id
 @param result 用户消息/邮件语种
 */
+ (void)msg_updateMessageLangWithMessageLang:(NSString *)messageLang result:(respResult)result;

@end
