//
//  IXBOModel.m
//  IXApp
//
//  Created by Seven on 2017/8/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBOModel.h"
#import "IXAppUtil.h"
#import "NSObject+IX.h"

#pragma mark - 入金基础url

@implementation IXBaseUrlM 

+ (instancetype)baseUrlWithDic:(NSDictionary *)dic
{
    return [[IXBaseUrlM alloc] initWithDic:dic];
}
- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{};

@end

#pragma mark - 入金支付类型

@implementation IXIncashPayModeM

+ (instancetype)payModeWithDic:(NSDictionary *)dic
{
    return [[IXIncashPayModeM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key{};

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end


#pragma mark - 银行

@implementation IXBank

+ (instancetype)bankWithDic:(NSDictionary *)dic
{
    return [[IXBank alloc] initWithDic:dic];
}
- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{};

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end

#pragma mark - 支付方式

@implementation IXPayMethod

+ (instancetype)payMethodWithDic:(NSDictionary *)dic
{
    return [[IXPayMethod alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{};

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end

#pragma mark -
#pragma mark -  ###### 地区 ######
#pragma mark -

#pragma mark - 国家

@implementation IXCountryM

+ (instancetype)countryMWithDic:(NSDictionary *)dic
{
    return [[IXCountryM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]){
        [self setValuesForKeysWithDictionary:dic] ;
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"countryId";
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    //model中找不到对应的key时会走改方法，若删除改方法，找不到对应的key时会导致程序crash
    //因此给予此方法空实现
}

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end

#pragma mark - 省份

@implementation IXProvinceM

+ (instancetype)provinceWithDic:(NSDictionary *)dic
{
    return [[IXProvinceM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"id_p";
    }
    else if ([key isEqualToString:@"subCountryDictParamList"]) {
        if ([value isKindOfClass:[NSArray class]]) {
            __block NSMutableArray  * arr = [@[] mutableCopy];
            [value enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj ix_isDictionary] && ![obj[@"operationState"] isEqualToString:@"del"]) {
                    [arr addObject:[IXCityM cityWithDic:obj]];
                }
            }];
            
            [arr sortUsingComparator:^NSComparisonResult(IXCityM * _Nonnull obj1, IXCityM * _Nonnull obj2) {
                return [obj1.nameEN compare:obj2.nameEN];
            }];
            
            value = arr;
        }
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}


// -----------------------------------------------------------

- (BOOL)isSameProvence:(NSString *)provenceCode
{
    return [provenceCode isEqualToString:_code];
}

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end

#pragma mark - 城市

@implementation IXCityM

+ (instancetype)cityWithDic:(NSDictionary *)dic
{
    return [[IXCityM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"id_p";
    }
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

- (BOOL)isSameCity:(NSString *)cityCode
{
    return [cityCode isEqualToString:_code];
}

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end

#pragma mark - 消息中心 - 消息类型

@implementation IXMsgCenterTypeM

+ (instancetype)msgTypeWithDic:(NSDictionary *)dic
{
    return [[IXMsgCenterTypeM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        if ([dic isKindOfClass:[NSDictionary class]]) {
            [self setValuesForKeysWithDictionary:dic];
        }
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"idx";
//        _idx = [value integerValue];
    }
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

- (NSString *)localizedName
{
    NSString    * lang = [IXLocalizationModel currentCheckLanguage];
    if ([lang isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([lang isEqualToString:LANEN]){
        return _nameEN;
    }
    else if ([lang isEqualToString:LANTW]){
        return _nameTW;
    }
    
    return _nameTW;
}

@end

#pragma mark - 消息中心 - 消息

@implementation IXMsgCenterM


+ (IXMsgCenterM *)msgCenterModelWithDic:(NSDictionary *)dic
{
    IXMsgCenterM    * m = [[IXMsgCenterM alloc] initWithDic:dic];
    return m;
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (NSString *)timeString
{
    if (self.timeInterval > 0) {
        NSDateFormatter * formater = [NSDateFormatter new];
        [formater setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
        
        NSTimeZone  * sysTimeZone = [NSTimeZone systemTimeZone];
        
        NSInteger   timeZone = [[IXAppUtil appTimeZone] integerValue];
        NSInteger   timeInterval = self.timeInterval/1000 - sysTimeZone.secondsFromGMT + timeZone*3600;
        return [formater stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeInterval]];
    }else{
        return @" -- ";
    }
}

- (void)setValue:(id)value forKey:(NSString *)key{
    if ([key isEqualToString:@"createDate"]) {
        key = @"timeInterval";
        value = value[@"time"];
    }
    if ([key isEqualToString:@"readStatus"]) {
        if ([value isEqualToString:@"NO"]) {
            _readStatus = NO;
        } else {
            _readStatus = YES;
        }
    } else {
        [super setValue:value forKey:key];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

@end


#pragma mark - 结单信息

@implementation IXDailyStatement

+ (instancetype)statementWithDic:(NSDictionary *)dic
{
    return [[IXDailyStatement alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

- (NSString *)timeString
{
    NSString    * timeStr = @"----/--/--";
    if (_reportDate.length) {
        timeStr = [_reportDate stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
    }
    return timeStr;
}

@end

