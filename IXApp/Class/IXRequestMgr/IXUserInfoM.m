//
//  IXUserInfoM.m
//  IXApp
//
//  Created by Seven on 2017/8/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUserInfoM.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#pragma mark - 用户信息

@implementation IXUserInfoM

+ (instancetype)userInfoWithDic:(NSDictionary *)dic
{
    return [[IXUserInfoM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"user"] && [value isKindOfClass:[NSDictionary class]]) {
        key = @"detailInfo";
        NSDictionary * dic = [(NSDictionary *)value objectForKey:@"result"];
        if (dic) {
            dic = [dic objectForKey:@"result"];
            if ([dic isKindOfClass:[NSDictionary class]]) {
                value = [IXUserDetailInfoM detailInfoWithDic:dic];
            }
        }
    }
    else if ([key isEqualToString:@"headImgUrl"]) {
        value = [NSString stringWithFormat:@"%@:%d%@",BOSeverIP,BOSeverPort,value];
        [[NSUserDefaults standardUserDefaults] setValue:value forKey:kIXHeadIcon];
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

- (BOOL)enable
{
    return (self.gts2CustomerId > 0
            && self.sid.length
            && self.token.length);
}

@end

#pragma mark - 用户详细信息

@implementation IXUserDetailInfoM

+ (instancetype)detailInfoWithDic:(NSDictionary *)dic
{
    return [[IXUserDetailInfoM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
        [self configBankFile];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"id_p";
    }
    else if ([key isEqualToString:@"idDocumentNumber"]) {
        if ([value isKindOfClass:[NSDictionary class]]) {
            value = value[@"value"];
        }
    }
    //银行卡
    else if ([key isEqualToString:@"customerInfoBankParams"]) {
        if ([value isKindOfClass:[NSArray class]]) {
            __block NSMutableArray  * arr = [@[] mutableCopy];
            [value enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isKindOfClass:[NSDictionary class]]) {
                    IXUserBankM * bank = [IXUserBankM userBankWithDic:obj];
                    [arr addObject:bank];
                }
            }];
            value = arr;
        }
    }
    //证明文件
    else if ([key isEqualToString:@"customerInfoFileParams"]) {
        if ([value isKindOfClass:[NSArray class]]) {
            __block NSMutableArray  * arr = [@[] mutableCopy];
            [value enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                IXUserFileM * m = [IXUserFileM userFileWithDic:obj];
                [arr addObject:m];
            }];
            value = arr;
        }
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{};

- (void)configBankFile
{
    [self.customerInfoBankParams enumerateObjectsUsingBlock:^(IXUserBankM * _Nonnull bank, NSUInteger idx, BOOL * _Nonnull stop) {
        IXUserFileM * m = [self fileWithBankOrder:bank.bankOrder];
        bank.imagePath = m.ftpFilePath;
    }];
}

- (IXUserFileM *)fileWithBankOrder:(int)num
{
    __block IXUserFileM * m = nil;
    NSString    * fileType = [NSString stringWithFormat:@"FILE_TYPE_BANK_%d",num];
    [self.customerInfoFileParams enumerateObjectsWithOptions:NSEnumerationReverse
                                                  usingBlock:^(IXUserFileM * _Nonnull file, NSUInteger idx, BOOL * _Nonnull stop)
    {
        
        if (SameString(file.fileType, fileType)) {
            m = file;
            *stop = YES;
        }
    }];
    return m;
}

//已经添加的有效银行卡数量
- (NSInteger)addedBankCount
{
    NSInteger   count = 0;
    for (int i = 0; i < self.customerInfoBankParams.count; i++) {
        IXUserBankM * bank = self.customerInfoBankParams[i];
        if (bank.bankAccountNumber.length) {
            count ++;
        }
    }
    return count;
}

//已经添加的有效银行卡
- (NSMutableArray *)addedBankList
{
    NSMutableArray      * array = [@[] mutableCopy];
    for (int i = 0; i < self.customerInfoBankParams.count; i++) {
        IXUserBankM * bank = self.customerInfoBankParams[i];
        if (bank.bankAccountName.length) {
            [array addObject:bank];
        }
    }
    return array;
}

/** 当前添加的银行卡序号*/
- (NSUInteger)currentAddBankSequence
{
    BOOL bankOrder1 = NO,bankOrder2 = NO,bankOrder3 = NO;
    for (int i = 0; i < self.customerInfoBankParams.count; i++) {
        IXUserBankM * bank = self.customerInfoBankParams[i];
        if (bank.bankAccountNumber.length) {
            switch (bank.bankOrder) {
                case 1:
                    bankOrder1 = YES;
                    break;
                case 2:
                    bankOrder2 = YES;
                    break;
                case 3:
                    bankOrder3 = YES;
                    break;
                default:
                    break;
            }
        }
    }
    
    NSUInteger index = 1;
    if (!bankOrder1) {
        return index;
    } else if (!bankOrder2) {
        index = 2;
    } else if (!bankOrder3) {
        index = 3;
    }
    return index;
}

/**
 获取可用的文件容器
 @param type fileType
 */
- (IXUserFileM *)fileWithType:(NSString *)type
{
    for (int i = 0; i < self.customerInfoFileParams.count; i++) {
        IXUserFileM * m = self.customerInfoFileParams[i];
        if ([m.fileType isEqualToString:type]) {
            return m;
        }
    }
    return nil;
}

@end

#pragma mark - 用户银行卡信息

@implementation IXUserBankM

+ (instancetype)userBankWithDic:(NSDictionary *)dic
{
    return [[IXUserBankM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"id_p";
    }
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{};

- (NSString *)localizedName
{
    __block NSString *bankName = _bank;
    [[IXBORequestMgr shareInstance].allBankList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXBank *bank = obj;
        if ([_bank isEqualToString:bank.code]) {
            bankName = bank.nameEN;
            *stop = YES;
        }
    }];
    return bankName;
    
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end

#pragma mark - 用户证明文件

@implementation IXUserFileM

+ (instancetype)userFileWithDic:(NSDictionary *)dic
{
    return [[IXUserFileM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        key = @"id_p";
    }
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{};

@end


#pragma mark -
#pragma mark - 可添加银行卡信息

@implementation IXEnableBankM

+ (instancetype)bankModelWithDic:(NSDictionary *)bankInfo
{
    IXEnableBankM   * m = [IXEnableBankM new];
    
    if (bankInfo){
        [m setValuesForKeysWithDictionary:bankInfo];
    }
    
    return m;
}

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key
{
    //当找不到对应的key时可在此处进行必要的操作
    //此处若调用super方法，在找不到对应的key时会导致crash
}

#warning 若添加其它语言，此处需要增加判断，返回对应的语言
- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end

