//
//  IXBORequestMgr+Survey.h
//  IXApp
//
//  Created by Evn on 2017/10/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"
#import "JSONModel.h"

typedef NS_ENUM(int, IXSurveyReqType) {
    IXSurveyReqTypeQuest = 0,   //问卷调查
    IXSurveyReqTypeReskEval = 1,    //风险测评
};


@interface  IXSurInfoM : JSONModel

#pragma mark - required
@property (nonatomic, assign) NSInteger actorId;    //【必填参数】参与者id
@property (nonatomic, assign) NSInteger companyId;  //【必填参数】公司id
@property (nonatomic, assign) NSInteger paperId;    //【必填参数】问卷id

#pragma mark - optional
@property (nonatomic, copy) NSString    * paperTitle;   //问卷标题
@property (nonatomic, copy) NSString    * submitDateStr;//提交时间（yyyy-MM-dd HH:mm:ss）

@end

/** 答案 */
@interface IXSurRecordM: JSONModel
#pragma mark - required
@property (nonatomic, assign) NSInteger questionId; //【必填选项】问题id
@property (nonatomic, copy) NSString    * answer;   //【必填选项】，选项答案；单选eg.@"A"；多选eg.@"A,C"
@property (nonatomic, copy) NSString    * lastEditableOption;//【必填选项】，用户输入答案,eg.@"没有建议"
#pragma mark - optional
@property (nonatomic, assign) int       questionType;
@end

@interface IXSurRecords: JSONModel
//IXSurRecordM 的jsonString
@property (nonatomic, strong)NSArray    * records;
@end

@interface IXBORequestMgr (Survey)

/**
 查询问题列表
 */
+ (void)survey_questionListWith:(IXSurveyReqType)type result:(respResult)result;

/**
 提交问卷调查
 */
+ (void)survey_submitPaperWithParam:(NSDictionary *)param result:(respResult)result;

+ (void)survey_submitPaperWithInfo:(IXSurInfoM *)info records:(IXSurRecords*)recordM result:(respResult)result;

@end
