//
//  IXBOModel.h
//  IXApp
//
//  Created by Seven on 2017/8/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
@class IXCityM;

#define kIncashModeCardPay  @"cardpay"
#define kIncashModeAlipay   @"alipay"

typedef NS_ENUM(NSInteger, IXMsgCenterMType) {
    IXMsgCenterMTypeAll,    //全部消息
    IXMsgCenterMTypeUpdatePassword, //更新密码
};

#pragma mark - 入金基础url
/** 入金基础url */
@interface IXBaseUrlM : NSObject

@property (nonatomic, copy) NSString    * depositSubmitUrl;
@property (nonatomic, copy) NSString    * depositValidateUrl;
@property (nonatomic, copy) NSString    * getBankUrl;
@property (nonatomic, copy) NSString    * getBankUrlFromFo;
@property (nonatomic, copy) NSString    * getPayMethodUrl;
@property (nonatomic, copy) NSString    * getPayMethodUrlFromFo;

+ (instancetype)baseUrlWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

@end

#pragma mark - 入金支付类型

@interface IXIncashPayModeM : NSObject

@property (nonatomic, copy) NSString    * code; //支付方式id
@property (nonatomic, copy) NSString    * gatewayType;  //支付方式
@property (nonatomic, copy) NSString    * payCurrency;  //货币类型
@property (nonatomic, copy) NSString    * cardpayType;  //swift：快捷支付；other：同支付宝一致的支付逻辑
@property (nonatomic, strong) NSNumber  * maxPayamount;
@property (nonatomic, strong) NSNumber  * minPayamount;

@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;

+ (instancetype)payModeWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)localizedName;

@end


#pragma mark - 银行

/** 可添加银行卡的银行列表 */
@interface IXBank : NSObject

@property (nonatomic, copy) NSString    * code;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;

+ (instancetype)bankWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)localizedName;

@end

#pragma mark - 支付方式

/** 入金支付方式 */
@interface IXPayMethod : NSObject

@property (nonatomic, copy) NSString    * code;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameTW;
@property (nonatomic, copy) NSString    * nameEN;

@property (nonatomic, strong) NSNumber  * minPayamount;
@property (nonatomic, strong) NSNumber  * maxPayamount;

+ (instancetype)payMethodWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)localizedName;

@end

#pragma mark -
#pragma mark -  地区
#pragma mark -

#pragma mark - 国家

/** 国家model */
@interface IXCountryM : NSObject

@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;
@property (nonatomic, copy) NSString    * code;
@property (nonatomic, copy) NSString    * nationalCode;
@property (nonatomic, copy) NSString    * parentCode;
@property (nonatomic, strong) NSArray   * subCountryDictParamList;
@property (nonatomic, assign) NSInteger versionNo;
@property (nonatomic, assign) NSInteger countryCode;
@property (nonatomic, assign) NSInteger countryId;
@property (nonatomic, assign) BOOL      valid;

+ (instancetype)countryMWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)localizedName;

@end

#pragma mark - 省份
@interface IXProvinceM : NSObject

@property (nonatomic, strong) NSArray<IXCityM *> *subCountryDictParamList;
@property (nonatomic, copy) NSString    * code;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;
@property (nonatomic, copy) NSString    * id_p;
@property (nonatomic, copy) NSString    * parentCode;

+ (instancetype)provinceWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (BOOL)isSameProvence:(NSString *)provenceName;

- (NSString *)localizedName;

@end

#pragma mark - 城市

@interface IXCityM : NSObject

@property (nonatomic, copy) NSString    * code;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;
@property (nonatomic, copy) NSString    * id_p;
@property (nonatomic, copy) NSString    * parentCode;

+ (instancetype)cityWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (BOOL)isSameCity:(NSString *)cityCode;

- (NSString *)localizedName;

@end

#pragma mark - 消息中心 - 消息类型
/** 消息中心 - 消息类型 */
@interface IXMsgCenterTypeM : NSObject

@property (nonatomic, assign) NSInteger codeSort;
@property (nonatomic, assign) NSInteger codeVersion;
@property (nonatomic, assign) NSInteger idx;    //索引
@property (nonatomic, copy) NSString    * code;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;

+ (instancetype)msgTypeWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)localizedName;

@end

#pragma mark - 消息中心 - 消息
/** 消息中心 - 消息 */
@interface IXMsgCenterM : NSObject

@property (nonatomic, copy) NSString    * msgTitle;
@property (nonatomic, copy) NSString    * msgContent;
@property (nonatomic, copy) NSString    * msgType;
@property (nonatomic, assign) NSInteger msgId;
@property (nonatomic, assign) double timeInterval;
@property (nonatomic, assign) BOOL      readStatus;
@property (nonatomic, assign) IXMsgCenterMType  type;

+ (IXMsgCenterM *)msgCenterModelWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)timeString;

@end

#pragma mark - 结单信息

@interface IXDailyStatement : NSObject

@property (nonatomic, copy) NSString    * filePath;
@property (nonatomic, copy) NSString    * reportDate;

+ (instancetype)statementWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)timeString;

@end
