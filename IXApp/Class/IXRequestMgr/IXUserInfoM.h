//
//  IXUserInfoM.h
//  IXApp
//
//  Created by Seven on 2017/8/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
@class IXUserDetailInfoM;
@class IXUserFileM;

#pragma mark - 用户信息
/**
 用户信息
 */
@interface IXUserInfoM : NSObject

@property (nonatomic, assign) int       companyId;
@property (nonatomic, assign) NSInteger gts2CustomerId;
@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString    * lang;
@property (nonatomic, copy) NSString    * headImgUrl;
@property (nonatomic, copy) NSString    * sid;
@property (nonatomic, copy) NSString    * token;

@property (nonatomic, strong) IXUserDetailInfoM * detailInfo;

+ (instancetype)userInfoWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

/** 登录信息是否正确 */
- (BOOL)enable;

@end

#pragma mark - 用户详细信息

/** 用户详细信息 */
@interface IXUserDetailInfoM : NSObject

@property (nonatomic, copy) NSString    * chineseName;
@property (nonatomic, copy) NSString    * address;
@property (nonatomic, copy) NSString    * country;
@property (nonatomic, copy) NSString    * city;
@property (nonatomic, copy) NSString    * province;
@property (nonatomic, copy) NSString    * dateOfBirth;
@property (nonatomic, copy) NSString    * email;
@property (nonatomic, copy) NSString    * idDocument;
@property (nonatomic, copy) NSString    * idDocumentNumber;
@property (nonatomic, copy) NSString    * mobilePhone;
@property (nonatomic, copy) NSString    * mobilePhonePrefix;
@property (nonatomic, copy) NSString    * password; //加密后的密码
@property (nonatomic, copy) NSString    * postalCode;   //邮编
@property (nonatomic, copy) NSString    * nationality;
@property (nonatomic, copy) NSString    * unionId;
@property (nonatomic, copy) NSString    * messageLang;//消息/模板语种

@property (nonatomic, assign) NSInteger customerNumber;
@property (nonatomic, assign) NSInteger gts2CustomerId;
@property (nonatomic, assign) NSInteger firstDepositGts2AccountId;
@property (nonatomic, assign) NSInteger id_p;
@property (nonatomic, assign) NSInteger tradeIxCustomerId;

@property (nonatomic, assign) int   companyId;
@property (nonatomic, assign) int   customerGroupId;

@property (nonatomic, strong) NSMutableArray    * customerInfoBankParams;
@property (nonatomic, strong) NSMutableArray    * customerInfoFileParams;

+ (instancetype)detailInfoWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

/** 已添加银行卡数量 */
- (NSInteger)addedBankCount;

/** 已添加的银行卡 */
- (NSMutableArray *)addedBankList;

/** 当前添加的银行卡序号*/
- (NSUInteger)currentAddBankSequence;

/**
 获取可用的文件容器
 @param type fileType
 */
- (IXUserFileM *)fileWithType:(NSString *)type;

/** 根据银行卡bankOrder获取对应文件 */
- (IXUserFileM *)fileWithBankOrder:(int)num;
@end

#pragma mark - 用户银行卡信息
/**
 用户银行卡信息
 */
@interface IXUserBankM : NSObject

@property (nonatomic, copy) NSString    * bank;
@property (nonatomic, copy) NSString    * bankName;
@property (nonatomic, copy) NSString    * bankAccountName;
@property (nonatomic, copy) NSString    * bankAccountNumber;
@property (nonatomic, copy) NSString    * bankAccountType;
@property (nonatomic, copy) NSString    * bankAddress;
@property (nonatomic, copy) NSString    * bankBranch;
@property (nonatomic, copy) NSString    * bankCity;
@property (nonatomic, copy) NSString    * bankCountry;
@property (nonatomic, copy) NSString    * bankCurrency;
@property (nonatomic, copy) NSString    * bankProvince;
@property (nonatomic, copy) NSString    * bankOther;
@property (nonatomic, copy) NSString    * internationalRemittanceCode;
@property (nonatomic, assign) int       bankOrder;
@property (nonatomic, copy) NSString    * imagePath;
@property (nonatomic, copy) NSString    * proposalStatus;
@property(nonatomic,copy) NSString *bankAccountNumberMd5;
@property (nonatomic, assign) int   companyId;
@property (nonatomic, assign) NSInteger customerNumber;
@property (nonatomic, assign) NSInteger id_p;

@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameTW;
@property (nonatomic, copy) NSString    * nameEN;

@property (nonatomic, assign) BOOL  mark;

@property(nonatomic,copy)NSString *type;

+ (instancetype)userBankWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

- (NSString *)localizedName;

@end

#pragma mark - 用户证明文件
/**
 用户证明文件
 */
@interface IXUserFileM : NSObject

@property (nonatomic, assign) int   companyId;
@property (nonatomic, assign) int   id_p;
@property (nonatomic, copy) NSString    * fileName;
@property (nonatomic, copy) NSString    * filePath;
@property (nonatomic, copy) NSString    * fileType;
@property (nonatomic, copy) NSString    * ftpFilePath;

@property (nonatomic, assign) NSInteger customerNumber;

+ (instancetype)userFileWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

@end

//可添加银行卡信息
@interface IXEnableBankM : NSObject

@property (nonatomic, copy) NSString    * code;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;

+ (instancetype)bankModelWithDic:(NSDictionary *)bankInfo;

- (NSString *)localizedName;

@end
