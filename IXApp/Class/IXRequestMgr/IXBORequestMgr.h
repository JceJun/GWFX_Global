//
//  IXBORequestMgr.h
//  IXApp
//
//  Created by Seven on 2017/8/14.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
@class IXUserInfoM;
@class IXUserBankM;

extern NSString * const kCachePath;


@interface IXBORequestMgr : NSObject
// 启动时触发一次的事件集合
@property(nonatomic,strong)NSMutableDictionary *launchHappenDic;
@property(nonatomic,strong)NSMutableDictionary *paramDic;
@property (nonatomic, strong) IXUserInfoM       * userInfo;
@property (nonatomic, strong) NSArray     * cerArr;   //证件类型
@property (nonatomic, copy)   NSString    * headUrl;  //用户头像

@property (nonatomic, assign)  BOOL       uploadedCustomerFile;   //是否已提交证明文件
@property(nonatomic,strong)NSString *gts2CustomerId; // 100005159
@property(nonatomic,copy)NSString *customerNo;  // 60001871
@property(nonatomic,copy)NSString *accountId; // 452922

@property(nonatomic,strong)NSArray *allBankList; // 所有银行卡列表

@property(nonatomic,strong)NSDictionary *activityBalanceDic; // 活动积分查询
@property(nonatomic,copy)NSString *customerActivityBalanceId; // 活动id

/*---------------------------------登录: 信息&文件----------------------------------*/
@property(nonatomic,strong)NSDictionary *boLoginDic; // 登录下发信息
@property (nonatomic, strong)NSArray     * fileArr;  //所有文件信息 (提交的时候获取列编号id)
/*---------------------------------登录: 信息&文件----------------------------------*/

/*----------------------------------- 身份&地址文件--------------------------------------------*/
@property(nonatomic,strong)NSMutableArray *id_docs; // 身份证文件
@property(nonatomic,strong)NSMutableArray *address_doc; // 地址证明文件
/*----------------------------------- 身份&地址文件--------------------------------------------*/

/*---------------------------------出金&地址簿:信息&文件---------------------------------------*/
@property(nonatomic,strong)NSDictionary *bankAdditionInfo;
@property(nonatomic,strong)NSMutableArray *bankArr; // 所有出金银行卡信息
@property(nonatomic,strong)NSMutableArray *bankFileArr; // 所有出金银行卡文件

@property(nonatomic,strong)NSDictionary *bank; // 某个出金银行卡信息
@property(nonatomic,strong)NSDictionary *bankFile; // 某个出金银行卡文件
/*---------------------------------出金&地址簿:信息&文件---------------------------------------*/


/*---------------------------------出金&地址簿:信息&文件---------------------------------------*/
@property(nonatomic,strong)NSMutableArray *withdraw_bankArr; // 所有出金银行卡信息
@property(nonatomic,strong)NSMutableArray *withdraw_bankFileArr; // 所有出金银行卡文件
@property(nonatomic,strong)NSMutableArray *withdraw_bankAddressFileArr; // 所有出金银行卡地址信息

@property(nonatomic,strong)NSDictionary *withDraw_bank; // 某个出金银行卡信息
@property(nonatomic,strong)NSDictionary *withDraw_bankFile; // 某个出金银行卡文件
@property(nonatomic,strong)NSDictionary *withDraw_bankAddressFile;  // 某个出金银行卡地址文件
/*---------------------------------出金&地址簿:信息&文件---------------------------------------*/


+ (instancetype)shareInstance;

/** 清理缓存信息 */
- (void) clearInstance;

/** 登录bo */
+ (void)loginBo:(void(^)(BOOL response, BOOL success, NSString * errStr))complete;

/** 刷新用户信息 */
+ (void)refreshUserInfo:(void(^)(BOOL success))complete;

/** 获取信息缓存位置 */
+ (NSString *)cachePath;

/** 用户解绑银行卡时调用 */
+ (void)removeBankCard:(IXUserBankM *)bank;
/** 添加银行卡时调用 */
+ (void)addBankCard:(IXUserBankM *)bank;

+ (void)processCredentialInfo:(id)obj;

//21.入金-获取银行列表(从FO获取)
+ (void)b_allBankList:(void(^)(NSArray *bankList))hd;

// 初始化新增入金银行卡信息
+ (void)resetAddDepostiBankContainer;
// 根据入金银行卡信息对应找到入金银行卡文件
+ (NSDictionary *)filterDepositBankFile:(NSDictionary *)deposit_bank;
// 根据出金银行卡信息对应找到出金银行卡文件
+ (NSDictionary *)filterWithDrawBankFile:(NSDictionary *)withDraw_bank;

// 初始化新增出金银行卡信息
+ (void)resetAddWithDrawBankContainer;

// 合并有效的入金银行卡信息（含附件审核状态）
+ (NSMutableArray *)mergedBankList;
// 合并有效的出金银行卡信息（含附件审核状态）
+ (NSMutableArray *)mergedWithDrawBankList;




// 合并文件优先显示状态
+ (NSString *)mergeFileStatus:(NSArray *)statusArr isBackNumber:(BOOL)number;

// 63.获取客户文件信息
+ (void)b_getCustomerFiles:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

// 136. 出金银行卡资料修改
+ (void)b_updateCustomerWithdrawBanks:(NSArray *)banks rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

/*====================================  接口重构 ========================================*/
// 10.上传文件
+ (void)b_appUploadFile:(UIImage *)image rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

// 11.用户文件修改提交
+ (void)b_updateCustomerFiles:(NSArray *)files  rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

//12.用户银行资料修改提交 (入金银行卡新增、修改)
+ (void)b_appUpdateCustomerBanks:(NSArray *)banks rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

//39.埋点
+ (void)b_appAddEventTrack:(NSString *)mark  rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

//127.取款[出金法币]
+ (void)b_addWithdrawNew:(NSString *)transAmount
        withdrewBankName:(NSString *)withdrewBankName
     withdrewBankAccount:(NSString *)withdrewBankAccount
 withdrewBankAccountName:(NSString *)withdrewBankAccountName
                     rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

//142. 余额变动状态修改接口
+ (void)b_customerActiviyBanlanceChange:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

// 143.余额变动状态查询
+ (void)b_customerActivityBalanceQuery:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

@end
