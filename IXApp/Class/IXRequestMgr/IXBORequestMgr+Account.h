//
//  IXBORequestMgr+Account.h
//  IXApp
//
//  Created by Seven on 2017/8/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"
#import "IXRegisterStep3M.h"
#import "IXOpenAcntModel.h"

typedef NS_ENUM(NSInteger, pageType) {
    pageTypePhoneFind = 0,  //通过手机找回密码
    pageTypeEmailFind = 1,  //通过邮箱找回密码
};

typedef void(^verifiCodeResult)(NSString *errCode,NSString *errStr,NSString *codeId,NSString *imageVerifiCodeUrl,NSString *imageVerifiCodeId);
@interface IXEmailPhoneModel : NSObject

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gts2CustomerId;
@property (nonatomic, strong) NSString *mobilePhonePrefix;
@property (nonatomic, strong) NSString *mobilePhone;
@property (nonatomic, assign) NSUInteger index; //0:邮箱 1:手机号

@end

@interface IXEmailModel : JSONModel

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gts2CustomerId;

@end

@interface IXPhoneModel : JSONModel

@property (nonatomic, strong) NSString *mobilePhonePrefix;
@property (nonatomic, strong) NSString *mobilePhone;

@end

@interface IXFindPwdM : NSObject

@property (nonatomic, copy) NSString    * nationality;
@property (nonatomic, copy) NSString    * mobilePhonePrefix;
@property (nonatomic, copy) NSString    * mobilePhone;
@property (nonatomic, copy) NSString    * email;
@property (nonatomic, copy) NSString    * psd;
@property (nonatomic, copy) NSString    * psdPre;
@property (nonatomic, copy) NSString    * accountId;
@property (nonatomic, copy) NSString    * nameCN;
@property (nonatomic, copy) NSString    * nameEN;
@property (nonatomic, copy) NSString    * nameTW;

@end

/** 用户信息，登录注册找回密码等相关请求处理 */
@interface IXBORequestMgr (Account)

#pragma mark - 账号相关（校验微信／微信注册／绑定微信）


//手机号或邮箱绑定
+ (void)acc_bindEmailOrPhoneWithParam:(NSDictionary *)param result:(respResult)result;

#pragma mark - 账号相关（校验手机邮箱／获取校验验证码／注册／找回密码）

/** 检查邮箱或手机号是否已注册 */
+ (void)acc_checkEmailOrPhoneExistWith:(IXEmailPhoneModel *)model
                          complete:(void(^)(BOOL exist, NSString * errStr))complete;


/**
 获取短信验证码
 
 @param phoneNum 电话号码
 @param code code
 @param complete 完成回调
 */
+ (void)acc_getPhoneVerifyCodeWithNumber:(NSString *)phoneNum
                            areaCode:(NSString *)code
                            complete:(void(^)(NSString * codeId, NSString * errStr))complete;

/**
 获取短信验证码防刷接口
 
 @param phoneNum 电话号码
 @param code 地区码、国家手机号前缀
 @param imgVerifiCodeCode 上一次验证码MD5加密数据
 @param imgVerifiCodeId 图形验证码ID
 @param result 完成回调
 */
+ (void)acc_getPhoneVerifyCodeWithLimitedWithNumber:(NSString *)phoneNum
                                areaCode:(NSString *)code
                       imgVerifiCodeCode:(NSString *)imgVerifiCodeCode
                         imgVerifiCodeId:(NSString *)imgVerifiCodeId
                                  result:(verifiCodeResult)result;


/**
 获取邮箱验证码
 
 @param email 合法的邮箱地址
 @param complete 完成回调
 */
+ (void)acc_getEmailVerifyCodeWithEmail:(NSString *)email
                           complete:(void(^)(NSString * codeId, NSString * errStr))complete;


/**
 验证验证码正确性
 
 @param code 验证码
 @param idStr 验证码id
 @param complete 完成回调
 */
+ (void)acc_checkVerifyCodeWithCode:(NSString *)code
                         codeId:(NSString *)idStr
                       complete:(void(^)(BOOL success, NSString * token, NSString * errStr))complete;


/**
 注册账号
 
 @param model IXRegisterStep3M obj
 @param token 验证码token
 @param complete 完成回调
 */
+ (void)acc_registWith:(IXRegisterStep3M *)model
             token:(NSString *)token
          complete:(void(^)(BOOL registerSucess, NSString * errStr))complete;


//注册账号(wChat)
+ (void)acc_registWithP:(IXRegisterStep3M *)model
              token:(NSString *)token
           complete:(void(^)(BOOL registerSucess, NSString * errStr,NSInteger gId))complete;

/**
 找回密码
 
 @param model IXFindPwdM obj
 @param type 找回密码途径
 @param complete 完成回调
 */
+ (void)acc_findPasswordWith:(IXFindPwdM *)model
                pageType:(pageType)type
                complete:(void(^)(BOOL findSuccess, NSString * errStr))complete;

/**
 修改密码
 */
+ (void)acc_modifyPasswdWithParam:(NSDictionary *)param result:(respResult)result;


#pragma mark - 用户信息相关


/**
 更新用户头像
 
 @param img 图片
 @param sid 用户id
 @param complete 完成回调
 */
+ (void)acc_updateUserAvatar:(UIImage *)img
                     sid:(NSString *)sid
                complete:(void(^)(BOOL updateSuccess, NSString * errStr))complete;

/** 更新用户信息 */
+ (void)acc_modifyUserInfoWithParam:(NSDictionary *)param result:(respResult)result;

/** 获取客户文件信息 */
+ (void)acc_customerFilesWithParam:(NSDictionary *)param result:(respResult)result;

/** 客户协议列表 */
+ (void)acc_protocolListWithParam:(NSDictionary *)param result:(respResult)result;

/** 客户协议详情 */
+ (void)acc_protocolDetailWithParam:(NSDictionary *)param result:(respResult)result;

/** 开设真实账户 */
+ (void)acc_openAccountWithParam:(NSDictionary *)param result:(respResult)result;

/** 用户设备绑定 */
+ (void)acc_addUserDeviceWithParam:(NSDictionary *)param result:(respResult)result;

/** 移除绑定的设备 */
+ (void)acc_deleteUserDeviceWithParam:(NSDictionary *)param result:(respResult)result;

/** 上传附件 */
+ (void)acc_uploadBankPhotoWithParam:(id)param WithResult:(respResult)result;

/** 提交附件 */
+ (void)acc_uploadAttachWithParam:(id)param WithResult:(respResult)result;

/** 提交修改 */
+ (void)acc_modifyBankInfoWithParam:(id)param WithResult:(respResult)result;

/** 获取数据字典 */
+ (void)acc_dictChildListWithParam:(NSString *)param WithResult:(respResult)result;

/** 查询当前用户的类型 */
+ (void)acc_checkUserTypeWithResult:(respResult)result;




@end
