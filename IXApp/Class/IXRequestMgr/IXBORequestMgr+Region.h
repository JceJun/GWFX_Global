//
//  IXBORequestMgr+Region.h
//  IXApp
//
//  Created by Seven on 2017/8/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"

#define kChinaNationalCode @"CN"
#define kChinaCountryCoiide @"ISO_3166_156"

/** 地区信息 */
@interface IXBORequestMgr (Region)

/**
 刷新国家列表（调用增量更新接口）
 
 @param complete 刷新完成
 @return 缓存内容
 */
+ (NSArray <IXCountryM *>*)region_refreshCountryList:(void(^)(NSArray <IXCountryM *>* list,
                                                       NSString * errStr,
                                                       BOOL hasNewData))complete;



/**
 刷新省份列表（调用增量更新接口）
 
 @param nationalCode    国家代码，从IXCountryM中取值
 @param countryCode     国家代码，从IXCountryM中取值(code)
 @param complete 刷新完成
 @return 缓存内容
 */
+ (NSArray <IXProvinceM *>*)region_refreshProvinceWithNationalCode:(NSString *)nationalCode
                                                countryCode:(NSString *)countryCode
                                                   complete:(void(^)(NSArray <IXProvinceM *>* list,
                                                                     NSString * errStr,
                                                                     BOOL hasNewData))complete;

//66获取地区以及根据版本更新
+ (void)b_getAreaListByNationalCode:(NSString *)nationalCode rsp:(void(^)(BOOL success, NSString *errCode, NSString *errStr, id obj))rsp;

@end
