//
//  IXBORequestMgr+Login.h
//  IXApp
//
//  Created by Seven on 2017/8/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"
#import "JSONModel.h"

/** 登录 */
@interface IXBORequestMgr (Login)

/**
 用户名密码登录

 @param acc 账号
 @param password 密码
 @param phoneCode 手机前缀
 */
+ (void)lg_loginWithAccount:(NSString *)acc
                        pwd:(NSString *)password
                  phoneCode:(NSString *)phoneCode;




@end
