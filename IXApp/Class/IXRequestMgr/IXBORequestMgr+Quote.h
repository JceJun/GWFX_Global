//
//  IXBORequestMgr+Quote.h
//  IXApp
//
//  Created by Seven on 2017/8/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBORequestMgr.h"

@interface IXBORequestMgr (Quote)


/**
 订阅行情

 @param cataId 产品分类id
 @param autoR 自动续费
 @param complete 完成回调
 */
+ (void)quote_subQuoteWithCataId:(NSInteger)cataId
                       autoRenew:(BOOL)autoR
                        complete:(void(^)(BOOL success, long limitDate))complete;


/**
 取消订阅行情
 
 @param cataId  产品分类id
 @param complete 完成回调
 */
+ (void)quote_unsubQuoteWithCataId:(NSInteger)cataId complete:(void(^)(BOOL success))complete;

@end
