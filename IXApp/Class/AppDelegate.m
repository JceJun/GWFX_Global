//
//  AppDelegate.m
//  IXApp
//
//  Created by bob on 16/11/1.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "AppDelegate+UI.h"
#import "SFHFKeychainUtils.h"
#import <UMMobClick/MobClick.h>
#import "IXAlertVC.h"
#import "IXJPushDetailVC.h"
#import "IXBORequestMgr+BroadSide.h"
#import "IXUserInfoM.h"
#import "IxProtoDeal.pbobjc.h"
#import "IxItemDeal.pbobjc.h"
#import "IXAccountGroupModel.h"
#import "IXBORequestMgr+Region.h"

#import "IXStaticsMgr.h"
#import "IxProtoHeader.pbobjc.h"
#import "IXUserDefaultM.h"

#import "IXAccountBalanceModel.h"
#import "IXLoginMainVC.h"

#import "IXKLineDBMgr.h"
#import "IXCpyConfig.h"

#import "NSObject+IX.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
#import "IXBORequestMgr+Comp.h"
#import "Firebase.h"//goole统计

@import UserNotifications;

@interface AppDelegate()
<
UIAlertViewDelegate,
ExpendableAlartViewDelegate,
AppsFlyerTrackerDelegate,
UNUserNotificationCenterDelegate,
FIRMessagingDelegate
>

@property (nonatomic, assign) BOOL  forceUpdate;

@end

@implementation AppDelegate

- (id)init
{
    self = [super init];
    if ( self ) {
        IXTradeData_listen_regist(self, PB_CMD_USERLOGIN_INFO);
        IXTradeData_listen_regist(self, PB_CMD_USER_KICT_OUT);
        IXTradeData_listen_regist(self, PB_CMD_USER_LOGIN_DATA);
        IXTradeData_listen_regist(self, PB_CMD_USER_LOGIN_DATA_TOTAL);
        IXTradeData_listen_regist(self, PB_CMD_DEAL_ADD);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_USERLOGIN_INFO);
    IXTradeData_listen_resign(self, PB_CMD_USER_KICT_OUT);
    IXTradeData_listen_resign(self, PB_CMD_USER_LOGIN_DATA);
    IXTradeData_listen_resign(self, PB_CMD_USER_LOGIN_DATA_TOTAL);
    IXTradeData_listen_resign(self, PB_CMD_DEAL_ADD);
}

- (void)notify{
    // 快速重登陆
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doRelogin)
                                                 name:@"needRelogin"
                                               object:nil];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"%@",NSHomeDirectory());
    
    // 友盟
    [self UMSetting];
    
    //Google FireBase: 统计 + 推送
    [self fireBase:application];
    
    //AppsFlyer统计
    [self appsFlyer];
    
    // 外观
    if (@available(iOS 11.0, *)){
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
        [[UITableView appearance] setEstimatedSectionFooterHeight:0.f];
        [[UITableView appearance] setEstimatedSectionHeaderHeight:0.f];
        [[UITableView appearance] setEstimatedRowHeight:0.f];
    }
    
    // 统计
    [[IXStaticsMgr shareInstance] activateDevice];
    
    // Init Model
    [IXKLineDBMgr shareInstance];
    [IXDataProcessTools initDataBase];
    [IXAccountBalanceModel shareInstance];
    
    // UI展示
    [self showUI];

    // 通知
    [self notify];
    
    // Setting
    [IXUserDefaultM dealScreenNormalLight];
    [IXUserDefaultM dealDayNightMode];
    
    // 请求配置信息
    [IXBORequestMgr region_refreshCountryList:nil];
    
    // 检查版本更新
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self getLatestVersion];
    });
    
    return YES;
}

- (void)appsFlyer{
    if ([CompanyName containsString:@"UAT"]) {
        return;
    }
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = AppsFlyerDevKey;
    [AppsFlyerTracker sharedTracker].appleAppID = AppleAppID;
    [AppsFlyerTracker sharedTracker].delegate = self;
#ifdef DEBUG
    [AppsFlyerTracker sharedTracker].isDebug = YES;
#endif
}

- (void)fireBase:(UIApplication *)application{
    [FIRApp configure];
    [FIRMessaging messaging].shouldEstablishDirectChannel = YES;
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    _bgStamp = time(NULL);
}

-  (void)applicationDidBecomeActive:(UIApplication *)application{
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    [application setApplicationIconBadgeNumber:0];

    //若在后台用户信息被清理，则再此刷新用户信息
    if (![[IXBORequestMgr shareInstance].userInfo enable]) {
        [IXBORequestMgr refreshUserInfo:nil];
        ELog(@"用户信息已更新");
    }
    
    time_t timeNow = time(NULL);
    //登录成功，或者从非激活状态到激活状态超过10s
    if ((_bgStamp > 0 && (timeNow - _bgStamp) > 10)) {
        _bgStamp = timeNow;
    }

}

- (BOOL)application:(UIApplication *)application handleOpenURL:(nonnull NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [AppDelegate saveUserBehavoir];
    [[IXQuoteDataCache shareInstance] saveDynQuote];
    [[IXQuoteDataCache shareInstance] saveLastClosePrice];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    DLog(@"========%@", [NSString stringWithFormat:@"Device Token: %@", deviceToken]);

//     With swizzling disabled you must set the APNs device token here.
     [FIRMessaging messaging].APNSToken = deviceToken;
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    //Optional
    DLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
     [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    _pushInfo = userInfo;
    [self dealPushData:_pushInfo];
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    // Print full message.
    NSLog(@"%@", userInfo);
    _pushInfo = userInfo;
    [self dealPushData:_pushInfo];
    
    completionHandler();
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
}

- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"Received data message: %@", remoteMessage.appData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DLog(@"iOS6及以下系统，收到通知:%@",userInfo);
    
     [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    DLog(@"iOS7及以上系统，收到通知:%@",userInfo);
    
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

    completionHandler(UIBackgroundFetchResultNewData);
    
}

+ (IXBroadsideVC *)getBroadSideVC
{
    IIViewDeckController *rootVC = [AppDelegate getRootVC];
    IXBroadsideVC *result = (IXBroadsideVC *)rootVC.leftController;
    return result;
}

+ (IIViewDeckController *)getRootVC
{
    IIViewDeckController *rootVC = (IIViewDeckController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    return rootVC;
}

+ (IXRootTabBarVC *)getTabBarVC
{
    IXRootTabBarVC  * tabBar = (IXRootTabBarVC *)[AppDelegate getRootVC].centerController;
    return tabBar;
}

#pragma mark -
#pragma mark - IXTradeDataKVO
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if(IXTradeData_isSameKey(keyPath, PB_CMD_USERLOGIN_INFO))
    {
        self.hasLogin = NO;
        proto_user_login_info *info = ((IXTradeDataCache *)object).pb_user_login_info;
        // 登录信息
        if (info.result==0) {
            // 登录成功
            [[IXTradeDataCache shareInstance] clearInstance];
            [[IXQuoteDataCache shareInstance] clearInstance];
            
            if( self.switchAccount <= 0 ){
                [self refreashDataVersion];
            }else{
                if (self.switchAccount == 2) {
                    // 切换账户
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_GOTO_DPSChannelVC object:nil];
                }else if (self.switchAccount == 4){
                    // MarketVC下边栏跳转入金
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_ROOT_GOTO_Deposit object:nil];
                }else if (self.switchAccount == 5){
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_ROOT_GOTO_MARKET object:nil];
                }
                self.switchAccount = 0;
            }
            // 刷新MarketVC下边栏
            [[NSNotificationCenter defaultCenter] postNotificationName:kRefreashProfit object:nil];
        }  else {
            if (info.result != RET_USER_LOGIN_UNSECURE_DEV) {
                //登录失败
                //埋点
                [IXEntityFormatter showErrorInformation:[NSString stringWithFormat:@"%d",info.result]];
                [[IXStaticsMgr shareInstance] endLoginTrade:NO errorCode:info.result];
            }
        }
    }
    else if (IXTradeData_isSameKey(keyPath, PB_CMD_USER_KICT_OUT))
    {
        proto_user_kickout *info = ((IXTradeDataCache *)object).pb_user_kick_out;
        if (((info.accountid == [IXUserInfoMgr shareInstance].userLogInfo.account.id_p &&
            info.type == [IXUserInfoMgr shareInstance].itemType) || info.kickoutType == 2)) {
            NSString *msg = nil;
            if (info.kickoutType == 2) {
                msg = LocalizedString(@"你已被销户");
            } else {
                msg = LocalizedString(@"账户在其它设备登录");
            }
            //被挤下线
            [self clearLoginInfo];
            IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"提示")
                                                     message:msg
                                                 cancelTitle:nil
                                                  sureTitles:LocalizedString(@"确定")];
            VC.index = 0;
            VC.emptyDismiss = NO;
            VC.expendAbleAlartViewDelegate = self;
            [VC showView];
        }else if ( info.kickoutType == 1 ){
            //修改密码或者忘记密码
            [self clearLoginInfo];
            [AppDelegate showLoginMain];
        }
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_USER_LOGIN_DATA_TOTAL)) {
        [SVProgressHUD dismiss];
        self.hasLogin = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotiRefreashCata object:nil];
        //登录成功埋点
        [[IXStaticsMgr shareInstance] endLoginTrade:YES errorCode:0];
        [IXBORequestMgr shareInstance];
        [IXBORequestMgr refreshUserInfo:nil];
        //友盟埋点
        if (UMAppKey.length) {
            [MobClick event:UMLoginSuccess];
        }
        [IXBORequestMgr comp_requestCompanyConfigInfoWithId:CompanyID complete:^(NSString *errStr, NSString *errCode, id obj) {
            NSLog(@"%@",obj);
        }];
#warning 这里的判断需要优化；这个时候早就已经登录成功了。
        id rootVC = self.window.rootViewController;
        if ( ![rootVC isKindOfClass:[IIViewDeckController class]] ) {
            
            BOOL autoLogin = NO;
            if ( [rootVC isKindOfClass:[IXBaseNavVC class]] ) {
                IXBaseNavVC *nav = (IXBaseNavVC *)rootVC;
                nav.navigationBar.dk_barTintColorPicker = DKNavBarColor;
                UIViewController *loginVC = nav.topViewController;
                if( [loginVC isKindOfClass:[IXLoginMainVC class]] ){
                    autoLogin = [(IXLoginMainVC *)loginVC isAutoLogin];
                }
            }
            
            [AppDelegate showRoot];
            [IXBORequestMgr b_allBankList:^(NSArray *bankList) {
                if (!bankList.count) {
                    [SVProgressHUD showInfoWithStatus:@"Get Bank List Failed"];
                }
            }];

        }
    } else if (IXTradeData_isSameKey(keyPath, PB_CMD_USER_LOGIN_DATA)) {
        proto_user_login_data *pb = ((IXTradeDataCache *)object).pb_user_login_data;
        if (pb.result != 0) {
            
        }
    } else if (IXTradeData_isSameKey(keyPath,PB_CMD_DEAL_ADD)) {
        //挂单成功提示
        proto_deal_add  *pb = ((IXTradeDataCache *)object).pb_deal_add;
        if (pb.result == 0) {
            if (pb.deal && pb.deal.proposalType == 0 && pb.deal.symbolid > 0) {
                if (pb.deal.reason == item_order_etype_TypeLimit ||
                    pb.deal.reason == item_order_etype_TypeStop) {
                    [IXUserDefaultM dealAlert];
                }
            }
        }
    }
}

//刷新基础数据需要换PB
- (void)refreashDataVersion
{
    [[IXAccountBalanceModel shareInstance] clearCache];
    [[IXAccountGroupModel shareInstance] clearInstance];
    
    proto_user_login_data *loginData = [[proto_user_login_data alloc] init];
    loginData.header.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    loginData.userid = [IXUserInfoMgr shareInstance].userLogInfo.account.userid;
    loginData.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    loginData.companyid = CompanyID;
    loginData.dataVersion = [AppDelegate dataVersion];
    loginData.version = 1;
    [[IXTCPRequest shareInstance] userDataVersion:loginData];
}

- (void)clearLoginInfo
{
    self.hasLogin = NO;
    [[IXTCPRequest shareInstance] disConnect];
    [[IXBORequestMgr shareInstance] clearInstance];
    [[IXTradeDataCache shareInstance] clearInstance];
    [[IXQuoteDataCache shareInstance] clearInstance];
    [[IXAccountBalanceModel shareInstance] clearCache];
    [[IXAccountGroupModel shareInstance] clearInstance];
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    [SFHFKeychainUtils deleteItemForUsername:userName andServiceName:kServiceName error:nil];
}

- (void)doRelogin
{
    if (self.hasLogin) {
        [self dealWithReLogin];
    }
}

- (void)UMSetting{
    if (!UMAppKey.length) {
        return;
    }
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    UMConfigInstance.appKey = UMAppKey;
    UMConfigInstance.channelId = AppMarket;
    [MobClick setAppVersion:version];
    [MobClick setEncryptEnabled:YES];   //对日志信息加密
    [MobClick setLogEnabled:YES];   //集成测试模式
    [MobClick setCrashReportEnabled:YES]; // 错误报告
    [MobClick startWithConfigure:UMConfigInstance];
}

- (void)dealPushData:(NSDictionary *)userInfo{
//    content1 = "\U7b2c\U4e00\U884c123456";
//    content2 = "\U7b2c\U4e8c\U884c123456";
//    content3 = "\U7b2c\U4e09\U884c123456";
//    content4 = "\U7b2c\U56db\U884c123456";
//    content5 = "\U7b2c\U4e94\U884c123456";
//    title = "Say Hello From Google Cloud Message";
    NSString *title = userInfo[@"title"];
    __block NSString *content = @"";
    
    for (int i = 0; i < 5; i++) {
        NSString *key = [NSString stringWithFormat:@"content%d",i+1];
        NSString *contentN = userInfo[key];
        if (contentN) {
            content = [content stringByAppendingString:contentN];
        }
    }
    
    IXJPushDetailVC *vc = [[IXJPushDetailVC alloc] init];
    IXMsgCenterM *message = [[IXMsgCenterM alloc] init];
    message.msgTitle = title;
    message.msgContent = content;
    vc.message = message;
    
    vc.hidesBottomBarWhenPushed = YES;
    IIViewDeckController *VDC = [AppDelegate getRootVC];
    if ([VDC isKindOfClass:[IIViewDeckController class]]) {
        [VDC closeLeftViewAnimated:YES];
        IXRootTabBarVC  * tabBar = (IXRootTabBarVC *)[AppDelegate getRootVC].centerController;
        IXBaseNavVC     * nav = tabBar.selectedViewController;
        [nav pushViewController:vc animated:YES];
        _pushInfo = nil;
    }else{
        // 由于点击推送banner进来不是RootVC，需要加载时间,但具体加载时间不确定，故用此方法让用户看到消息
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dealPushData:_pushInfo];
        });
    }
}

- (void)getLatestVersion
{
    if (!UpdateURL.length) {
        return ;
    }
    
    [IXBORequestMgr bs_getAppVersionControlInfo:^(NSDictionary *result) {
        if ([result ix_isDictionary] && [result[@"deviceType"] isEqualToString:@"IOS"]) {
            NSString    * type = result[@"type"];
            NSString    * latestStr = [NSString stringWithFormat:@"%@",result[@"version"]];
            int availabV = [result[@"minAvailableVersion"] integerValue];
            int latestV = [result[@"version"] integerValue];
            
            _forceUpdate= NO; 
            NSString * currentV = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            currentV = [currentV stringByReplacingOccurrencesOfString:@"." withString:@""];
            if (currentV.length > latestStr.length) {
                currentV = [currentV substringToIndex:[NSString stringWithFormat:@"%d",latestV].length];
            }
            
            NSInteger currV = [currentV integerValue];
            
            if (currV >= latestV) {
                return ;
            }
            
            if (SameString(type, @"S")) {
                //强制升级
                _forceUpdate = YES;
            }
            else if (SameString(type, @"V")) {
                if (availabV > currV) {
                    //强制升级
                    _forceUpdate = YES;
                }
            }
            
            IXAlertVC *vc = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"当前版本过低，请升级到最新版本")
                                                     message:nil
                                                 cancelTitle:LocalizedString(@"以后再说")   //_forceUpdate ? nil : LocalizedString(@"以后再说")
                                                  sureTitles:LocalizedString(@"立即更新")];
            vc.emptyDismiss = NO;
            vc.index = 1;
            vc.expendAbleAlartViewDelegate = self;
            [vc showView];
        }
    }];
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (alertView.tag == 0) {
        //        [AppDelegate showLoginMain];
    } else if (alertView.tag == 1 && btnIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UpdateURL]];
        return NO;
    }
    return YES;
}

@end
