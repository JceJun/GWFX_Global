//
//  IXOpenAcntModel.h
//  IXApp
//
//  Created by Evn on 17/2/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <JSONModel/JSONModel.h>
@class IXAcntModel;
@class IXCustomerModel;
@class IXIdDocumentNumberModel;
@class IXFinanceModel;
@protocol IXAcntModel

@end
@interface IXOpenAcntModel : JSONModel

@property (nonatomic, strong)NSArray <IXAcntModel>*accountList;
@end

@interface IXAcntModel : JSONModel
@property (nonatomic, strong)NSString *platform;
@property (nonatomic, strong)NSString *accountLevel;
@property (nonatomic, strong)NSString *currency;
@property (nonatomic, strong)NSString *gts2CustomerId;
@property (nonatomic, strong)NSString *isAutoApprove;
@property (nonatomic, assign)NSInteger clientType;
@property (nonatomic, assign)NSInteger groupType;

@end

@interface IXAcntMt4Model: IXAcntModel
@property (nonatomic, strong)NSString *encryptPassword;
@end

@interface IXCustomerModel : JSONModel
@property (nonatomic, strong)IXIdDocumentNumberModel *idDocumentNumber;
@property (nonatomic, strong)NSString *platform;
@property (nonatomic, strong)NSString *gts2CustomerId;
@property (nonatomic, strong)NSString *idDocumentNumberMd5;
@property (nonatomic, strong)NSString *chineseName;
@property (nonatomic, strong)NSString *idDocument;
@property (nonatomic, strong)NSString *email;
@property (nonatomic, strong)NSString *address;
@property (nonatomic, strong)NSString *postalCode;
@property (nonatomic, strong)NSString *nationality;
@property (nonatomic, strong)NSString *province;
@property (nonatomic, strong)NSString *city;
@end

@interface IXIdDocumentNumberModel : JSONModel
@property (nonatomic, strong)NSString *value;
@end

@interface IXFinanceModel : JSONModel
@property (nonatomic, strong)NSString *financeType;
@end

@interface IXAcntInfoM : JSONModel

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *chineseName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong)IXIdDocumentNumberModel *idDocumentNumber;
@property (nonatomic, strong) NSString *idDocumentNumberMd5;
@property (nonatomic, strong) NSString *idDocument;
@property (nonatomic, strong) NSString *nationality;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *platform;
@property (nonatomic, strong) NSString *gts2CustomerId;

@end
