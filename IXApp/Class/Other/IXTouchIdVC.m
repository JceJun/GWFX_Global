//
//  IXTouchIdVC.m
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXTouchIdVC.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "UIImageView+WebCache.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"

@interface IXTouchIdVC ()

@property (nonatomic, strong) UIImageView *photoImg;
@property (nonatomic, strong) UILabel *nickNameLbl;
@property (nonatomic, strong) UILabel *accountNumLbl;
@property (nonatomic, strong) UIButton *touchBtn;
@property (nonatomic, strong) UIButton *reloginBtn;
@property (nonatomic, strong) UILabel *tipDesLbl;

@end

@implementation IXTouchIdVC

#pragma mark life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    [self initData];
}

- (void)initData
{
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                     placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                              options:SDWebImageAllowInvalidSSLCertificates];
    [self.touchBtn dk_setImage:DKImageNames(@"lock_fingerPrint", @"lock_fingerPrint_D") forState:UIControlStateNormal];
    [self.view addSubview:self.reloginBtn];

    self.nickNameLbl.text = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
    
    NSString    * accountStr = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"账户"),
                                [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo];
    
    self.accountNumLbl.text = accountStr;
    
    self.tipDesLbl.text = LocalizedString(@"点击Home键进行指纹解锁");
}


//判断是否开启指纹解锁
- (BOOL)lockAppWithTouchId
{
    id thumbLock = [[NSUserDefaults standardUserDefaults] objectForKey:KSaveThumbLock];
    if ( !thumbLock || [thumbLock boolValue] == NO ) {
        return NO;
    }
    LAContext *lol = [[LAContext alloc] init];
    NSError *err;
    weakself;
    //TouchId可用
    if ([lol canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&err]) {
        [lol evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
            localizedReason:LocalizedString(@"点击Home进行指纹验证") reply:^(BOOL success, NSError * _Nullable error) {
            if (success) {
                [weakSelf validSuccess:weakSelf];
            }else{
                switch (error.code)
                {
                    //输入错误次数超过5次，touchId被锁定；iOS9需要手动调用输入密码的窗口
                    case LAErrorTouchIDLockout:
                    {
                        [weakSelf callpassLock];
                    }
                        break;
                }
            }
        }];
    }else{
        //TouchId不可用
        if(err.code == kLAErrorTouchIDLockout) {
            [self callpassLock];
        }else{
            return NO;
        }
    }
    return YES;
}


//唤醒密码解锁
- (void)callpassLock
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        LAContext *lol1 = [[LAContext alloc] init];
        lol1.localizedCancelTitle = @"";
        
        weakself;
        [lol1 evaluatePolicy:LAPolicyDeviceOwnerAuthentication
             localizedReason:LocalizedString(@"密码验证")
                       reply:^(BOOL success, NSError * _Nullable error)
         {
             if ( success ) {
                 [weakSelf validSuccess:weakSelf];
             }
         }];
    }
}

//验证成功跳转进App
- (void)validSuccess:(id)obj
{
    dispatch_async( dispatch_get_main_queue(), ^{
        [obj dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)reloginAction:(UIButton *)btn
{
    if (_relogin) {
        _relogin();
    }
}

#pragma mark init moduel
- (UIImageView *)photoImg
{
    if ( !_photoImg ) {
        _photoImg = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 54)/2, 50, 54, 54)];
        _photoImg.layer.masksToBounds = YES;
        _photoImg.layer.cornerRadius = 27;
        [self.view addSubview:_photoImg];
    }
    return _photoImg;
}

- (UIButton *)touchBtn
{
    if ( !_touchBtn ) {
        _touchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _touchBtn.frame = CGRectMake( (kScreenWidth - 62)/2, 270, 60, 62);
        [self.view addSubview:_touchBtn];
        [_touchBtn addTarget:self action:@selector(lockAppWithTouchId) forControlEvents:UIControlEventTouchUpInside];
    }
    return _touchBtn;
}

- (UIButton *)reloginBtn
{
    if ( !_reloginBtn ) {
        _reloginBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth/2-70,
                                                                 kScreenHeight - 60,
                                                                 140,
                                                                 35)];
        [_reloginBtn addTarget:self
                        action:@selector(reloginAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [_reloginBtn setTitle:LocalizedString(@"重新登录")
                     forState:UIControlStateNormal];
        [_reloginBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xe9e9ea)
                                   forState:UIControlStateNormal];
        _reloginBtn.titleLabel.font = PF_MEDI(13);
    }
    return _reloginBtn;
}

- (UILabel *)nickNameLbl
{
    if ( !_nickNameLbl ) {
        _nickNameLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 117, kScreenWidth, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0x4c6072
                                        dTextColor:0xe9e9ea];
        [self.view addSubview:_nickNameLbl];
    }
    return _nickNameLbl;
}

- (UILabel *)accountNumLbl
{
    if ( !_accountNumLbl ) {
        _accountNumLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 150, kScreenWidth, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentCenter
                                          wTextColor:0x99abba
                                          dTextColor:0x99abba];
        [self.view addSubview:_accountNumLbl];
    }
    return _accountNumLbl;
}

- (UILabel *)tipDesLbl
{
    if ( !_tipDesLbl ) {
        _tipDesLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 341, kScreenWidth, 15)
                                        WithFont:PF_MEDI(13)
                                       WithAlign:NSTextAlignmentCenter
                                      wTextColor:0x99abba
                                      dTextColor:0x99abba];
        [self.view addSubview:_tipDesLbl];
    }
    return _tipDesLbl;
}

@end
