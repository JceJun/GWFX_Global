//
//  IXBroadsideVC.h
//  IXApp
//
//  Created by Evn on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXBroadsideVC : IXDataBaseVC

//切换账户
- (void)popToSwitchAccountWithAccountId:(uint64_t)curAccountId withAccountGroupId:(uint64_t)curAccGroupId;

@end
