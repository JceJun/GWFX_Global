//
//  IXAnswersInfoCell.h
//  IXApp
//
//  Created by Evn on 2017/7/26.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^getWebHeight)(CGFloat height);
@interface IXAnswersInfoCell : UITableViewCell

@property (nonatomic, strong) getWebHeight refreashHeight;

@property (nonatomic, assign)  CGFloat cellHeight;

@property (nonatomic, strong) NSString *webUrl;

@property (nonatomic, strong) NSString *content;

@end
