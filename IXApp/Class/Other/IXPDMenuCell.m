//
//  IXPDMenuCell.m
//  IXApp
//
//  Created by Seven on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPDMenuCell.h"

@interface IXPDMenuCell()

@property (nonatomic, strong) UILabel   * titleLab;

@end

@implementation IXPDMenuCell

- (UILabel *)titleLab
{
    if (!_titleLab){
        _titleLab = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLab.backgroundColor = [UIColor clearColor];
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font = PF_MEDI(13);
        _titleLab.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0xe9e9ea);
        [self.contentView addSubview:_titleLab];
    }
    return _titleLab;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.titleLab.text = title;
    
    CGRect rect = self.frame;
    self.titleLab.frame = CGRectMake(0,
                                     (rect.size.height - 30) / 2,
                                     rect.size.width,
                                     30);
}

- (void)setAttributeTitle:(NSAttributedString *)attributeTitle
{
    _attributeTitle = attributeTitle;
    self.titleLab.attributedText = attributeTitle;
    
    CGRect rect = self.frame;
    self.titleLab.frame = CGRectMake(0,
                                     (rect.size.height - 30) / 2,
                                     rect.size.width,
                                     30);
}

- (void)setCommonColor:(UIColor *)commonColor
{
    _commonColor = commonColor;
    self.backgroundColor = commonColor;
}

- (void)setBtomLineColor:(UIColor *)btomLineColor
{
    _btomLineColor = btomLineColor;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    if (!_btomLineColor) {
        _btomLineColor = [UIColor lightGrayColor];
    }
    
    //绘制虚线
    CGPoint leftP = CGPointMake(rect.origin.x, rect.origin.y + rect.size.height);
    CGPoint rightP = CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height);
    CGFloat lengths[] = {3,2};
    
    CGContextRef    context = UIGraphicsGetCurrentContext();
    CGContextMoveToPoint(context, leftP.x, leftP.y);
    CGContextAddLineToPoint(context, rightP.x, rightP.y);
    
    CGContextSetStrokeColorWithColor(context, _btomLineColor.CGColor);
    CGContextSetLineDash(context, 2, lengths, 2);
    CGContextStrokePath(context);
}


#pragma mark -
#pragma mark - touch action

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    self.backgroundColor = _seletedColor;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    self.backgroundColor = _commonColor;
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    self.backgroundColor = _commonColor;
}

@end
