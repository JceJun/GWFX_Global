//
//  IXCountryCell.m
//  IXApp
//
//  Created by Evn on 2018/3/19.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCountryCell.h"

@interface IXCountryCell()

@property (nonatomic, strong)UILabel *name;
@property (nonatomic, strong)UILabel *code;

@end

@implementation IXCountryCell

- (UILabel *)name
{
    if ( !_name ) {
        _name = [IXUtils createLblWithFrame:CGRectMake( 15, 13,kScreenWidth - 15, 19)
                                    WithFont:PF_MEDI(13)
                                   WithAlign:NSTextAlignmentLeft
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                  ];
        [self.contentView addSubview:_name];
    }
    return _name;
}

- (UILabel *)code
{
    if ( !_code ) {
        _code = [IXUtils createLblWithFrame:CGRectMake(0, 15,kScreenWidth - 15, 15)
                                   WithFont:PF_MEDI(13)
                                  WithAlign:NSTextAlignmentRight
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        [self.contentView addSubview:_code];
    }
    return _code;
}

- (void)reloadData:(IXCountryM *)countryM
{
    self.name.text = [countryM localizedName];
    self.code.text = [NSString stringWithFormat:@"+%ld",(long)countryM.countryCode];
}

@end
