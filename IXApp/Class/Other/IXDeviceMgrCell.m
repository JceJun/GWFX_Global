//
//  IXDeviceMgrCell.m
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDeviceMgrCell.h"

@interface IXDeviceMgrCell()

@property (nonatomic, strong)UILabel *nameLbl;
@property (nonatomic, strong)UILabel *contentLbl;//内容
@property (nonatomic, strong)UIImageView *rIcon;
@property (nonatomic, strong)UIView *line;

@end

@implementation IXDeviceMgrCell

- (void)layoutSubviews {
    [super layoutSubviews];
    [self rIcon];
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [IXCustomView createLable:CGRectMake(15,9, kScreenWidth - (15 + 24), 17)
                                       title:@""
                                        font:PF_MEDI(13)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [IXCustomView createLable:CGRectMake(15,GetView_MaxY(self.nameLbl) + 5, VIEW_W(self.nameLbl), 12)
                                          title:@""
                                           font:PF_MEDI(12)
                                     wTextColor:0x99abba
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UIImageView *)rIcon
{
    if (!_rIcon) {
        _rIcon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (8 + 16), 20, 8, 14)];
        _rIcon.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
        [self.contentView addSubview:_rIcon];
    }
    return _rIcon;
}

- (UIView *)line
{
    if (!_line) {
        CGRect rect = CGRectMake(15, 55 - kLineHeight, kScreenWidth - 15, kLineHeight);
        _line = [[UIImageView alloc] initWithFrame:rect];
        _line.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_line];
    }
    return _line;
}

- (void)reloadUIWithData:(NSDictionary *)data
{
    self.nameLbl.text = [IXDataProcessTools dealWithNil:data[@"devName"]];
    self.contentLbl.text = [IXEntityFormatter timeIntervalToString:[data[@"lastLoginTime"] doubleValue]];
}

@end
