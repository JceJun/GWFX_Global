//
//  IXEvaluationCell.h
//  IXApp
//
//  Created by Evn on 2018/1/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"
@class IXEvaQuestionM;

typedef void(^selectEvaluateBlock)(NSInteger row,NSInteger index);
@interface IXEvaluationCell : UITableViewCell

@property (nonatomic, copy)selectEvaluateBlock selectEvaluateB;
@property (nonatomic, copy)void(^selectWordBlock)(NSString * key,NSString * desp);
@property (nonatomic, strong)IXTextField *tField;

- (void)refreshData:(IXEvaQuestionM *)evaQuestionM;

@end
