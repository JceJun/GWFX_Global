//
//  IXSysLanCell.h
//  IXApp
//
//  Created by Bob on 16/12/01.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@class IXSysLanCell;
@class IXSysLanModel;

/**系统语言*/
@interface IXSysLanCell : IXClickTableVCell

@property (nonatomic,strong) IXSysLanModel *model;

@end

@interface IXSysLanModel : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,assign)BOOL isSelect;

@end
