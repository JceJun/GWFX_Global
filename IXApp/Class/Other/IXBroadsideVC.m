//
//  IXBroadsideVC.m
//  IXApp
//
//  Created by Evn on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXBroadsideVC.h"
#import "IXBroadsideCell.h"
#import "IXDBLanguageMgr.h"
#import "IXDBSymbolLableMgr.h"
#import "IXTradeDataCache.h"
#import "AppDelegate+UI.h"
#import "IxProtoUser.pbobjc.h"
#import "SFHFKeychainUtils.h"
#import "IXAcntStep1VC.h"
#import "IXSettingVC.h"
#import "IXDBAccountMgr.h"
#import "AppDelegate.h"
#import "IXDBGlobal.h"
#import "IXTradeDataCache.h"
#import "IXDataBase.h"
#import "IXLeftNavView.h"
#import "IXSurveyMainVC.h"

#import "IXChooseAccountView.h"
#import "IXDBAccountGroupMgr.h"
#import "IXAccountGroupModel.h"
#import "IXAcntSafetyVC.h"
#import "ChatTableViewController.h"
#import "IXRootTabBarVC.h"
#import "IXAboutVC.h"
#import "IXMsgCenterVC.h"
#import "IXBORequestMgr+MsgCenter.h"
#import "IXUserInfoM.h"
#import "IXZoomScrollView.h"
#import "IXAnswersVC.h"

#import "IXAccountBalanceModel.h"
#import "IXAcntInfoVC.h"
#import "IXAlertVC.h"
#import "IXUserDefaultM.h"
#import "IXCpyConfig.h"
#import "IXWUserInfo.h"
#import "IXEvaluationM.h"

#import "IXSubQuoteVC.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+FormatterPrice.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "NSString+IX.h"
#import "IXEvaluationM.h"
#import "IXSurveyM.h"
#import "NSObject+IX.h"
#import "IXBORequestMgr+Account.h"
#import "IXADMgr.h"
#import "IXDataCacheMgr.h"
#import "IXWebCustomerSeviceVC.h"
#import "IXUserInfoVC.h"



@interface IXBroadsideVC ()
<
UITableViewDelegate,
UITableViewDataSource,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong) UITableView       * tableV;
@property (nonatomic, strong) NSMutableArray    * dataArr;
@property (nonatomic, strong) IXChooseAccountView   * accountView;
@property (nonatomic, strong) UIImageView   * logoImgV;
@property (nonatomic, strong) NSString  * sid;

@property (nonatomic, strong) UIView    * footerView;
@property (nonatomic, strong) UIView    * headerView;

@property (nonatomic, strong) IXBroadsideCell   * msgCenterCell;
@property (nonatomic, strong) IXEvaluationM *evaluationM;
@property (nonatomic, strong) NSTimer   * msgCenterTimer;   //消息中心请求timer
@property (nonatomic, assign) BOOL isExsitMt4;//是否存在MT4账户
@property (nonatomic, copy)  NSString *type;//type:"agent"、"user"

@end

@implementation IXBroadsideVC

- (id)init
{
    if (self = [super init]) {
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_LIST);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_CHANGE);
        IXTradeData_listen_regist(self, CMD_UPLOAD_HEAD_UPDATE);
    }
    
    return self;
}

- (void)dealloc
{
    // 退出后此VC不会被释放
    [self clear];
}

- (void)clear{
    [_msgCenterTimer invalidate];
    _msgCenterTimer = nil;
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_LIST);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_CHANGE);
    IXTradeData_listen_resign(self, CMD_UPLOAD_HEAD_UPDATE);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    _dataArr = [[NSMutableArray alloc] init];
    
     self.type = [NSString new];
    [self setContentData];
    [self.view addSubview:self.tableV];
    [self.view addSubview:self.footerView];
    [self msgCenterTimer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_tableV reloadData];
    [self resetHeadImage];
    [self resetFootContent];
    [self msgReq];
}

- (void)setContentData
{
    
    NSArray * iconArr = nil;
    NSArray * titleArr = nil;
    
    __block IXBroadsideCellM    * msgCenterM = nil;
    
    if (_dataArr.count > 0) {
        [_dataArr enumerateObjectsUsingBlock:^(IXBroadsideCellM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.title isEqualToString:LocalizedString(@"消息中心")]) {
                msgCenterM = obj;
                *stop = YES;
            }
        }];
        
        [_dataArr removeAllObjects];
    }
    
    if ([IXDataProcessTools getCurrentAccountType] == item_account_group_etype_Virtuals) {
        //        iconArr = @[@[@"more_msgCenter",@"more_msgCenter_D"],
        //                    @[@"more_accountSafe",@"more_accountSafe_D"],
        //                    @[@"more_customService",@"more_customService_D"],
        //                    @[@"more_about",@"more_about_D"],
        //                    @[@"more_qa",@"more_qa_D"],
        ////                    @[@"more_survey",@"more_survey_D"],
        //                    @[@"more_setting",@"more_setting_D"]];
        //        titleArr = @[LocalizedString(@"消息中心"),
        //                     LocalizedString(@"账户与安全"),
        //                     LocalizedString(@"在线客服"),
        //                     LocalizedString(@"关于我们"),
        //                     LocalizedString(@"常见问答"),
        ////                     LocalizedString(@"调查问卷"),
        //                     LocalizedString(@"设置")];
        iconArr = @[@[@"more_msgCenter",@"more_msgCenter_D"],
                    @[@"more_accountSafe",@"more_accountSafe_D"],
                    @[@"more_customService",@"more_customService_D"]];
        titleArr = @[LocalizedString(@"消息中心"),
                     LocalizedString(@"账户与安全"),
                     LocalizedString(@"在线客服")];
    } else {
        iconArr = @[@[@"more_msgCenter",@"more_msgCenter_D"],
                    @[@"more_accountInfo",@"more_accountInfo_D"],
                    @[@"more_accountSafe",@"more_accountSafe_D"],
                    @[@"more_customService",@"more_customService_D"]];
        titleArr = @[LocalizedString(@"消息中心"),
                     LocalizedString(@"用户资料"),
                     LocalizedString(@"账户与安全"),
                     LocalizedString(@"在线客服")];
    }
    
    for (int i = 0; i < iconArr.count; i++) {
        IXBroadsideCellM *tmpModel = [[IXBroadsideCellM alloc] init];
        tmpModel.imgStr = [iconArr[i] firstObject];
        tmpModel.dImgStr = [iconArr[i] lastObject];
        tmpModel.title = titleArr[i];
        
        if (i == 0 || i == 2 || i == 6) {
            tmpModel.isLine = YES;
            if (i == 0) tmpModel.number = @"";
        }else{
            tmpModel.isLine = NO;
        }
        
        if ([tmpModel.title isEqualToString:LocalizedString(@"消息中心")]) {
            tmpModel.number = msgCenterM.number;
        }
        
        [_dataArr addObject:tmpModel];
    }
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54.5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 215;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXBroadsideCell *tableCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXBroadsideCell class])];
    tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableCell.dk_backgroundColorPicker = DKNavBarColor;
    [tableCell config:_dataArr[indexPath.row]];
    if (indexPath.row == 0) {
        _msgCenterCell = tableCell;
    }
    
    return tableCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[AppDelegate getRootVC] closeLeftViewAnimated:YES];
    
    IXBroadsideCellM    * model = _dataArr[indexPath.row];
    UIViewController    * vc = nil;
    
    if ([model.title isEqualToString:LocalizedString(@"消息中心")]) {
        IXMsgCenterVC * v = [[IXMsgCenterVC alloc] init];
        [v setNoReadCount:[model.number integerValue]];
        weakself;
//        v.backBlock = ^{
//            [weakSelf msgReq];
//        };
        v.readAllBlock = ^{
            [weakSelf.dataArr enumerateObjectsUsingBlock:^(IXBroadsideCellM *  _Nonnull obj1, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj1 isKindOfClass:[IXBroadsideCellM class]]
                    && [obj1.title isEqualToString:LocalizedString(@"消息中心")]) {
                    obj1.number = @"";
                    [[NSNotificationCenter defaultCenter] postNotificationName:IXLeftNavViewMarkEnableNotify
                                                                        object:@0];
                    *stop = YES;
                }
            }];
            [weakSelf.tableV reloadData];
        };
        vc = v;
    }else if ([model.title isEqualToString:LocalizedString(@"账户与安全")]) {
        vc = [[IXAcntSafetyVC alloc] init];
    }else if([model.title isEqualToString:LocalizedString(@"设置")]){
        IXSettingVC * settingVC = [[IXSettingVC alloc] init];
        weakself;
        settingVC.logoutBlock = ^{
            [weakSelf logoutAction];
        };
        vc = settingVC;
    }else if ([model.title isEqualToString:LocalizedString(@"订阅行情")]){
        vc = [[IXSubQuoteVC alloc] init];
    }else if ([model.title isEqualToString:LocalizedString(@"在线客服")]){
//        vc = [[ChatTableViewController alloc] initWithPid:ServicePid
//                                                      key:ServiceKey
//                                                      url:kServiceUrl];
        
        vc = [IXWebCustomerSeviceVC new];
    }else if ([model.title isEqualToString:LocalizedString(@"用户资料")]){
//        vc = [[IXAcntInfoVC alloc] init];
        vc = [IXUserInfoVC new];
    }else if ([model.title isEqualToString:LocalizedString(@"关于我们")]){
        vc = [[IXAboutVC alloc] init];
    }else if ([model.title isEqualToString:LocalizedString(@"常见问答")]){
        vc = [[IXAnswersVC alloc] init];
    }else if ([model.title isEqualToString:LocalizedString(@"调查问卷")]) {
        vc = [[IXSurveyMainVC alloc] init];
    }
    
    if (vc) {
        vc.hidesBottomBarWhenPushed = YES;
        
        IXRootTabBarVC  * tabBar = (IXRootTabBarVC *)[AppDelegate getRootVC].centerController;
        IXBaseNavVC     * nav = tabBar.selectedViewController;
        [nav pushViewController:vc animated:YES];
    }
}

- (UIView*) tableView:(UITableView *)_tableView viewForHeaderInSection:(NSInteger)section
{
    [self refreshTableHeaderView];
    return self.headerView;
}


#pragma mark -
#pragma mark - button action

- (void)openAccBtnClick
{
    //查询当前用户类型
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    weakself;
    [IXBORequestMgr acc_checkUserTypeWithResult:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        if (success1 && [obj1 ix_isDictionary]) {
            [SVProgressHUD dismiss];
            weakSelf.type = [(NSDictionary *)obj1 objectForKey:@"type"];
            [weakSelf dealWithData];
        } else {
            [SVProgressHUD showErrorWithStatus:errStr1];
        }
    }];
}

- (void)dealWithData
{
    BOOL isMaxAccount = [self resetAccountInfo];
    if ( self.accountView.accountArr && self.accountView.accountArr.count > 0 ) {
        if (!isMaxAccount && self.accountView.accountArr && self.accountView.accountArr.count == 2) {
            [self.accountView removeFromSuperview];
            [self popToOpenRealAccount];
        }else{
            [self addChooseAccountView];
            
            weakself;
            self.accountView.choose = ^(NSIndexPath *index){
                if ([weakSelf.accountView.accountArr[index.row] isKindOfClass:[NSString class]]) {
                    [weakSelf popToOpenRealAccount];
                }else{
                    //切换账号
                    uint64_t curAccountId = [[(NSDictionary *)weakSelf.accountView.accountArr[index.row] objectForKey:@"id"] longLongValue];
                    uint64_t curAccGroupId = [[(NSDictionary *)weakSelf.accountView.accountArr[index.row] objectForKey:kAccGroupId] longLongValue];
                    uint32_t options = [[(NSDictionary *)weakSelf.accountView.accountArr[index.row] objectForKey:kOptions] intValue];
                    if (options == 3) {
#warning 提示不能登录
                    } else {
                        [weakSelf popToSwitchAccountWithAccountId:curAccountId withAccountGroupId:curAccGroupId];
                    }
                }
            };
        }
    }
}

- (void)setBtnClick
{
    [[AppDelegate getRootVC] closeLeftViewAnimated:YES];
    IXSettingVC *vc = [[IXSettingVC alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    IXRootTabBarVC  * tabBar = (IXRootTabBarVC *)[AppDelegate getRootVC].centerController;
    IXBaseNavVC     * nav = tabBar.selectedViewController;
    [nav pushViewController:vc animated:YES];
    
    weakself;
    vc.logoutBlock = ^{
        [weakSelf logoutAction];
    };
}

#pragma mark -
#pragma mark - reset

- (void)resetFootContent
{
    UIButton *openAccBtn = (UIButton *)[_footerView viewWithTag:1];
    NSString *openTitle = @"";
    NSArray *accArr = [IXDBAccountMgr queryAllAccountInfoByUserId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid];
    //处理已开户账户组被禁用
    int count = 0;
    for (int i = 0; i < accArr.count; i++) {
        NSDictionary *dic = accArr[i];
        uint64_t    groupId = [dic[kAccGroupId] longLongValue];
        NSDictionary *accGroupDic = [IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:groupId];
        //有数据代表状态正常
        if (accGroupDic.count) {
            count++;
        }
    }
    if ( count > 1 ) {
        openTitle = LocalizedString(@"切换账户");
    }else{
        openTitle = LocalizedString(@"开立真实账户");
    }
    [openAccBtn setTitle:openTitle forState:UIControlStateNormal];
}

- (BOOL)resetAccountInfo
{
    self.isExsitMt4 = NO;
    BOOL isMaxAccount = YES;
    NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
    
    //ix平台账户
    NSArray *ixArr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                      [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:0];
    if (ixArr.count) {
        [tmpArr addObjectsFromArray:ixArr];
    }
    //MT4平台账户(大账户启用才有效:clientType=1为大账户)
    NSArray *mt4LicArr = [IXDBAccountMgr queryAllMt4LicAccountInfoByClientType:1];
    if (mt4LicArr.count) {
        NSArray *mt4Arr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                           [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:2];
        if (mt4Arr.count) {
            [tmpArr addObjectsFromArray:mt4Arr];
        }
    }
    
    NSMutableArray *accountArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < tmpArr.count; i++) {
        NSMutableDictionary *dataDic = [tmpArr[i] mutableCopy];
        uint64_t    groupId = [dataDic[kAccGroupId] longLongValue];
        NSDictionary    * d = [IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:groupId];
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:d];
        
        if (dic) {
            NSString *curStr = [dic stringForKey:kCurrency];
            NSString *typeStr = [dic stringForKey:kType];
            NSString *option = [dic stringForKey:kOptions];
            
            if (curStr.length) {
                [dataDic setValue:curStr forKey:kCurrency];
                if (typeStr.length) {
                    [dataDic setObject:typeStr forKey:kType];
                }else{
                    [dataDic setObject:@"0" forKey:kType];
                }
                if (option.length) {
                    [dataDic setObject:option forKey:kOptions];
                } else {
                    [dataDic setObject:@"0" forKey:kOptions];
                }
                [accountArr addObject:dataDic];
            }
        }
    }
    
    if (![self isMaxAccount]) {
        [accountArr addObject:LocalizedString(@"添加真实账户")];
        isMaxAccount = NO;
    }
    [self.accountView setIsExsitMt4:self.isExsitMt4];
    [self.accountView setAccountArr:accountArr];
    return isMaxAccount;
}

//重设头像
- (void)resetHeadImage
{
    [_logoImgV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                 placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                          options:SDWebImageAllowInvalidSSLCertificates];
}


#pragma mark -
#pragma mark - other

- (BOOL)isMaxAccount
{
    BOOL ixMax = YES,mt4Max = YES;
    //0:IX平台
    NSArray *ixArr = nil;
    if (SameString(self.type, @"agent")) {
        ixArr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:0 type:8];
    } else {
       ixArr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:0 wipeOffType:8];
    }
    ixArr = [IXDataProcessTools uniqData:ixArr];
    ixMax = [self dealWithAccountInfo:ixArr clientType:0];
    
    NSArray *accountArr = [IXDBAccountMgr queryAllMt4LicAccountInfoByClientType:1];
    if (!accountArr.count) {
        return ixMax;
    }  else {
        self.isExsitMt4 = YES;//存在MT4账户
    }
    //2:MT4平台同步
    NSArray *mt4Arr = nil;
    if (SameString(self.type, @"agent")) {
        mt4Arr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:2 type:8];
    } else {
        mt4Arr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:2 wipeOffType:8];
    }
    //去重
    mt4Arr = [IXDataProcessTools uniqData:mt4Arr];
    
    if (mt4Arr.count) {
        mt4Max = [self dealWithAccountInfo:mt4Arr clientType:2];
    }
    return ixMax && mt4Max;
}

- (BOOL)dealWithAccountInfo:(NSArray *)accArr
                 clientType:(int32_t)clientType
{
    if (!accArr.count) {
        return YES;
    }
    NSArray *accountArr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                           [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:clientType];
    int count = 0;
    for (int j = 0; j < accArr.count; j++) {
        NSDictionary *tmpDic = accArr[j];
        uint32_t tmpType = [tmpDic[kType] intValue];
        NSString *tmpCurrency = tmpDic[kCurrency];
        for (int i = 0; i < accountArr.count; i++) {
            NSDictionary *dic = accountArr[i];
            uint64_t    groupId = [dic[kAccGroupId] longLongValue];
            NSDictionary    *acDic = [IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:groupId];
            uint32_t type = [acDic[kType] intValue];
            NSString *currency = acDic[kCurrency];
            //根据货币和类型区分是否开立账户
            if (tmpType == type && SameString(tmpCurrency, currency)) {
                count++;
            }
        }
    }
    return (accArr.count == count);
}

//开设真实账户
- (void)popToOpenRealAccount
{
    [[AppDelegate getRootVC] closeLeftViewAnimated:YES];
    IXAcntStep1VC *account = [[IXAcntStep1VC alloc] init];
    account.hidesBottomBarWhenPushed = YES;
    IXRootTabBarVC *tabBar = (IXRootTabBarVC *)[AppDelegate getRootVC].centerController;
    IXBaseNavVC *nav = tabBar.selectedViewController;
    [nav pushViewController:account animated:YES];
}

//切换账户
- (void)popToSwitchAccountWithAccountId:(uint64_t)curAccountId withAccountGroupId:(uint64_t)curAccGroupId
{
    [(AppDelegate *)[UIApplication sharedApplication].delegate setSwitchAccount:1];
    
    [[IXAccountBalanceModel shareInstance] clearCache];
    
    proto_user_login_account *pb = [[proto_user_login_account alloc] init];
    pb.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
    pb.userid = [IXUserInfoMgr shareInstance].userLogInfo.account.userid;
    pb.previousAccountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    pb.accountid = curAccountId;
    pb.name = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
    pb.companyToken = CompanyToken;
    pb.companyid = CompanyID;
    pb.sessionType = [IXUserInfoMgr shareInstance].itemType;
    pb.type = proto_user_login_elogintype_ByPhone;
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    pb.version =  [version intValue];
    pb.logintime = [IXEntityFormatter getCurrentTimeInterval];
    pb.dataVersion = [self dataVersionWithAccountId:curAccountId accountGroupId:curAccGroupId];
    [SVProgressHUD showWithStatus:LocalizedString(@"加载中...")];
    [[IXTCPRequest shareInstance] swiAccountWithParam:pb];
}

- (item_data_version *)dataVersionWithAccountId:(uint64_t)curAccountId  accountGroupId:(uint64_t)curAccGroupId
{
    item_data_version *dataVersion = [[item_data_version alloc] init];
    
    //产品
    NSDictionary *symbolUUID = [IXDBSymbolMgr querySymbolUUIDMax];
    dataVersion.verSym = [symbolUUID[kUUID] longLongValue];
    
    //产品分类
    NSDictionary *symbolCataUUID = [IXDBSymbolCataMgr querySymbolCatasUUIDMax];
    dataVersion.verSymcata = [symbolCataUUID[kUUID] longLongValue];
    
    //自选产品
    //    NSDictionary *symbolSubUUID = [IXDBSymbolSubMgr querySymbolSubUUIDMaxAccountId:[IXKeepAliveCache shareInstance].accountId];
    //    dataVersion.verSymsub = [symbolSubUUID[kUUID] longLongValue];
    
    NSDictionary *symSubDic = @{
                                kAccountId:@(curAccountId)
                                };
    NSDictionary *symbolSubUUID = [IXDBSymbolSubMgr querySymbolSubUUIDMaxByAddInfo:symSubDic];
    dataVersion.verSymsub = [symbolSubUUID[kUUID] longLongValue];
    
    //公司
    NSDictionary *companyUUID = [IXDBCompanyMgr queryCompanyUUIDMax];
    dataVersion.verCompany = [companyUUID[kUUID] longLongValue];
    
    //热门产品
    NSDictionary *symbolHotUUID = [IXDBSymbolHotMgr querySymbolHotUUIDMax];
    dataVersion.verSymhot = [symbolHotUUID[kUUID] longLongValue];
    
    //假期分类
    NSDictionary *holidayCataUUID = [IXDBHolidayCataMgr queryHolidayCatasUUIDMax];
    dataVersion.verHolidayCata = [holidayCataUUID[kUUID] longLongValue];
    
    //假期
    NSDictionary *holidayUUID = [IXDBHolidayMgr queryHolidayUUIDMax];
    dataVersion.verHoliday = [holidayUUID[kUUID] longLongValue];
    
    //交易时间分类
    NSDictionary *scheduleCataUUID = [IXDBScheduleCataMgr queryScheduleCataUUIDMax];
    dataVersion.verScheduleCata = [scheduleCataUUID[kUUID] longLongValue];
    
    //交易时间
    NSDictionary *scheduleUUID = [IXDBScheduleMgr queryScheduleUUIDMax];
    dataVersion.verSchedule = [scheduleUUID[kUUID] longLongValue];
    
    //产品标签
    NSDictionary *symLableUUID = [IXDBSymbolLableMgr querySymbolLableUUIDMax];
    dataVersion.verSymLabel = [symLableUUID[kUUID] longLongValue];
    
    //marginSet
    NSDictionary *marginSetUUID = [IXDBMarginSetMgr queryMarginSetUUIDMax];
    dataVersion.verMarginSet = [marginSetUUID[kUUID] longLongValue];
    
    //语言
    NSDictionary *languageUUID = [IXDBLanguageMgr queryLanguageUUIDMax];
    dataVersion.verLanguage = [languageUUID[kUUID] longLongValue];
    
    //schedule_Margin
    NSDictionary *scheduleMarginUUID = [IXDBScheduleMarginMgr queryScheduleMarginUUIDMax];
    dataVersion.verScheduleMargin = [scheduleMarginUUID[kUUID] longLongValue];
    
    //holiday_Margin
    NSDictionary *holidayMarginUUID = [IXDBHolidayMarginMgr queryHolidayMarginUUIDMax];
    dataVersion.verHolidayMargin = [holidayMarginUUID[kUUID] longLongValue];
    
    //    //账户组
    if (curAccGroupId == 0) {
        //新开账户还未下发对应的账户账户组、此时全量下发
        dataVersion.verAccgroupSymcata = 0;
    } else {
        NSDictionary *agscDic = @{kAccGroupId : @(curAccGroupId)};
        NSDictionary *agscUUID = [IXDBAccountGroupSymCataMgr queryAccountGroupSymCataUUIDMaxByAddInfo:agscDic];
        dataVersion.verAccgroupSymcata = [agscUUID[kUUID] longLongValue];
    }
    
    //结算时间
    NSDictionary *eodTimeUUID = [IXDBEodTimeMgr queryEodTimeUUIDMax];
    dataVersion.verEodTime = [eodTimeUUID[kUUID] longLongValue];
    
    //账户组产品
    NSDictionary *groupSymUUID = [IXDBG_SMgr queryG_SUUIDMax];
    dataVersion.verGrpsym = [groupSymUUID[kUUID] longLongValue];
    
    //账户组产品分类
    NSDictionary *groupSymCataUUID = [IXDBG_S_CataMgr queryG_S_CataUUIDMax];
    dataVersion.verGrpsymcata = [groupSymCataUUID[kUUID] longLongValue];
    
    //lp_channel_account
    NSDictionary *lpchaccUUID = [IXDBLpchaccMgr queryLpchaccUUIDMax];
    dataVersion.verLpchacc = [lpchaccUUID[kUUID] longLongValue];
    
    //lp_channel_account_symbol
    NSDictionary *lpchaccSymUUID = [IXDBLpchaccSymbolMgr queryLpchaccSymbolUUIDMax];
    dataVersion.verLpchaccSym = [lpchaccSymUUID[kUUID] longLongValue];
    
    //lp_channel
    NSDictionary *lpchannelUUID = [IXDBLpchannelMgr queryLpchannelUUIDMax];
    dataVersion.verLpchannel = [lpchannelUUID[kUUID] longLongValue];
    
    //lp_channel_symbol
    NSDictionary *lpchannelSymUUID = [IXDBLpchannelSymbolMgr queryLpchannelSymbolUUIDMax];
    dataVersion.verLpchannelSym = [lpchannelSymUUID[kUUID] longLongValue];
    
    //lp_IbBind
    NSDictionary *lpIbBindUUID = [IXDBLpibBindMgr queryLpibBindUUIDMax];
    dataVersion.verLpibBind = [lpIbBindUUID[kUUID] longLongValue];
    
    return dataVersion;
}

#pragma mark 退出登录
- (void)logoutAction
{
    [IXUserInfoMgr shareInstance].isCloseDemo = YES;
    [AppDelegate saveUserBehavoir];
    
    proto_user_logout *pb = [[proto_user_logout alloc] init];
    pb.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
    [[IXTCPRequest shareInstance] logoutWithParam:pb];
    [IXDataCacheMgr clearLoginInfo];
    
    [AppDelegate showLoginMain];
    
    [_msgCenterTimer invalidate];
    _msgCenterTimer = nil;
    
    
    // 退出后此VC不会被释放
    [self clear];
}

- (void)addChooseAccountView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self.accountView];
}

- (void)removeChooseAccountView
{
    [self.accountView removeFromSuperview];
}

- (void)refreshTableHeaderView
{
    if (_logoImgV) {
        [_logoImgV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                     placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                              options:SDWebImageAllowInvalidSSLCertificates];
        
    }
    
    if ([self.headerView viewWithTag:kNameLabTag]) {
        NSString    * title = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
        [(UILabel *)[_headerView viewWithTag:kNameLabTag] setText:title];
    }
    
    if ([self.headerView viewWithTag:kAccountLabTag]) {
        //        NSString    * accountStr = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"用户"),
        //                                    [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo];
        //        NSAttributedString      * attrTitle = [accountStr attributeStringWithFontSize:15
        //                                                                            textColor:AutoNightColor(0x99abba, 0x8395a4)];
        //        [(UILabel *)[_headerView viewWithTag:kAccountLabTag] setAttributedText:attrTitle];
        [(UILabel *)[_headerView viewWithTag:kAccountLabTag] setText:[IXUserInfoMgr shareInstance].userLogInfo.user.customerNo];
    }
    
    if ([self.headerView viewWithTag:kTypeLabTag]) {
        NSString    * typeStr = [IXDataProcessTools showCurrentAccountType];
        [(UILabel *)[_headerView viewWithTag:kTypeLabTag] setText:typeStr];
        
        //        NSInteger width = [IXEntityFormatter getContentWidth:typeStr WithFont:PF_MEDI(10)] + 3;
        //        UILabel     * typeLbl = (UILabel *)[_headerView viewWithTag:kTypeLabTag];
        //        typeLbl.frame = CGRectMake(kScreenWidth*3/4 - (15 + 25),GetView_MinY(_logoImgV) + 8, width, 14);
    }
}

#pragma mark -
#pragma mark - action sheet

- (void)responseToPhoto:(UITapGestureRecognizer *)tap
{
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [IXZoomScrollView showFrom:clickedImageView image:clickedImageView.image];
}

#pragma mark -
#pragma mark - IXTradeDataKVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_CHANGE)){
        //更新本地数据
        [[IXAccountBalanceModel shareInstance] refreashCurrency];
        [self setContentData];
        [self.tableV reloadData];
        
    }else if (IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_LIST)){
        [self.tableV reloadData];
        [self resetFootContent];
    }
    
    if (IXTradeData_isSameKey(keyPath, CMD_UPLOAD_HEAD_UPDATE)) {
        [self resetHeadImage];
    }
}

#pragma mark -
#pragma mark - getter

- (UITableView *)tableV
{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                kBtomMargin,
                                                                kScreenWidth - 80,
                                                                kScreenHeight - 76 - kBtomMargin)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKNavBarColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV  registerClass:[IXBroadsideCell class]
         forCellReuseIdentifier:NSStringFromClass([IXBroadsideCell class])];
    }
    
    return _tableV;
}

NSInteger   const kNameLabTag       = 200;
NSInteger   const kAccountLabTag    = 201;
NSInteger   const kTypeLabTag       = 202;

- (UIView *)headerView
{
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 215)];
        _headerView.dk_backgroundColorPicker = DKNavBarColor;
        
        NSString    * typeStr = [IXDataProcessTools showCurrentAccountType];
        NSInteger   width = [IXEntityFormatter getContentWidth:typeStr WithFont:PF_MEDI(10)] + 10;
        UILabel     * typeLbl = [IXCustomView createLable:CGRectMake(kScreenWidth*3/4 - (15 + 25),33, width, 14)
                                                    title:LocalizedString(typeStr)
                                                     font:PF_MEDI(10)
                                               wTextColor:0xffffff
                                               dTextColor:0x242a36
                                            textAlignment:NSTextAlignmentCenter];
        typeLbl.layer.cornerRadius = 2.f;
        typeLbl.layer.masksToBounds = YES;
        typeLbl.backgroundColor = MarketGrayPriceColor;
        typeLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x99abba, 0x50a1e5);
        typeLbl.tag = kTypeLabTag;
        [_headerView addSubview:typeLbl];
        
        CGRect  imgRect = CGRectMake(25, 13 + [[UIApplication sharedApplication] statusBarFrame].size.height, 52, 52);
        
        imgRect = CGRectMake((kScreenWidth*3/4 - 55)/2, 20 + GetView_MaxY(typeLbl), 55, 55);
        
        _logoImgV = [[UIImageView alloc] initWithFrame:imgRect];
        _logoImgV.layer.cornerRadius = 27;
        _logoImgV.clipsToBounds = YES;
        _logoImgV.userInteractionEnabled = YES;
        _logoImgV.contentMode =  UIViewContentModeScaleAspectFill;
        
        UITapGestureRecognizer  * tg = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(responseToPhoto:)];
        [_logoImgV addGestureRecognizer:tg];
        [_headerView addSubview:_logoImgV];
        
        CGRect  labRect = CGRectMake(0, GetView_MaxY(_logoImgV) + 15,
                                     kScreenWidth*3/4, 18);
        NSString    * title = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
        UILabel     * nameLbl = [IXCustomView createLable:labRect
                                                    title:title
                                                     font:PF_MEDI(13)
                                               wTextColor:0x4c6072
                                               dTextColor:0xe9e9ea
                                            textAlignment:NSTextAlignmentCenter];
        
        [_headerView addSubview:nameLbl];
        nameLbl.tag = kNameLabTag;
        
        labRect = CGRectMake(CGRectGetMinX(labRect),
                             CGRectGetMaxY(labRect) + 7,
                             CGRectGetWidth(labRect),
                             CGRectGetHeight(labRect));
        
        title = [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo;
        labRect = CGRectMake(0, GetView_MaxY(nameLbl) + 10,
                             kScreenWidth*3/4, 18);
        NSAttributedString      * attrTitle = [title attributeStringWithFontSize:15
                                                                       textColor:AutoNightColor(0x99abba, 0x8395a4)];
        
        UILabel     * accountLbl = [IXCustomView createLable:labRect
                                                       title:title
                                                        font:PF_MEDI(13)
                                                  wTextColor:0x99abba
                                                  dTextColor:0x8395a4
                                               textAlignment:NSTextAlignmentCenter];
        accountLbl.attributedText = attrTitle;
        
        [_headerView addSubview:accountLbl];
        accountLbl.tag = kAccountLabTag;
    }
    
    return _headerView;
}

- (UIView *)footerView
{
    if (!_footerView) {
        _footerView =  [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight - 76, kScreenWidth, 76)];
        _footerView.dk_backgroundColorPicker = DKNavBarColor;
        
        UIButton *openAccBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 10, kScreenWidth*3/8 + 30 , 24)];
        openAccBtn.dk_backgroundColorPicker = DKNavBarColor;
        openAccBtn.titleLabel.font = PF_MEDI(12);
        openAccBtn.tag = 1;
        openAccBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 25, 0,0);
        openAccBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 35, 0, 0);
        [openAccBtn dk_setImage:DKImageNames(@"more_switch", @"more_switch_D") forState:UIControlStateNormal];
        [openAccBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xd4d5dc) forState:UIControlStateNormal];
        [openAccBtn addTarget:self action:@selector(openAccBtnClick) forControlEvents:UIControlEventTouchUpInside];
        openAccBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_footerView addSubview:openAccBtn];
        
        UIButton *setBtn = [[UIButton alloc]initWithFrame:CGRectMake(kScreenWidth*3/8 + 30, 10, kScreenWidth*3/8 - 30, 24)];
        setBtn.dk_backgroundColorPicker = DKNavBarColor;
        setBtn.titleLabel.font = PF_MEDI(12);
        setBtn.tag = 2;
        setBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
        setBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        [setBtn dk_setImage:DKImageNames(@"more_setting", @"more_setting_D") forState:UIControlStateNormal];
        [setBtn setTitle:LocalizedString(@"设置") forState:UIControlStateNormal];
        [setBtn dk_setTitleColorPicker:DKColorWithRGBs(0x4c6072, 0xd4d5dc) forState:UIControlStateNormal];
        [setBtn addTarget:self action:@selector(setBtnClick) forControlEvents:UIControlEventTouchUpInside];
        setBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_footerView addSubview:setBtn];
    }
    
    return _footerView;
}

- (IXChooseAccountView *)accountView
{
    if (!_accountView) {
        _accountView = [[IXChooseAccountView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }
    
    return _accountView;
}

- (NSTimer *)msgCenterTimer
{
    if (!_msgCenterTimer) {
        _msgCenterTimer = [NSTimer scheduledTimerWithTimeInterval:30
                                                           target:self
                                                         selector:@selector(msgReq)
                                                         userInfo:nil repeats:YES];
        [_msgCenterTimer fire];
    }
    return _msgCenterTimer;
}


#pragma mark -
#pragma mark - request

- (void)msgReq
{
    [IXBORequestMgr msg_obtainAllNoReadCount:^(BOOL success, NSInteger count) {
        if (success) {
            [_dataArr enumerateObjectsUsingBlock:^(IXBroadsideCellM *  _Nonnull obj1, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj1 isKindOfClass:[IXBroadsideCellM class]]
                    && [obj1.title isEqualToString:LocalizedString(@"消息中心")]) {
                    obj1.number = [NSString stringWithFormat:@"%ld",count];
                    *stop = YES;
                }
            }];
            [IXLeftNavView setMsgNoReadCount:count];
            [[NSNotificationCenter defaultCenter] postNotificationName:IXLeftNavViewMarkEnableNotify
                                                                object:@(count)];
            [_tableV reloadData];
        }
    }];
}

- (id)unreadMsgRequestParam{
    NSString    * param = [NSString stringWithFormat:@"tradeUserId:%@",
                           @([IXUserInfoMgr shareInstance].userLogInfo.account.userid)];
    
    return param;
}

@end

