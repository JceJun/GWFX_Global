//
//  IXAcntAddressVC.m
//  IXApp
//
//  Created by Seven on 2017/12/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntAddressVC.h"
#import "IXAcntInfoAreaCellA.h"
#import "IXAcntStep2CellB.h"
#import "IXAcntStep1CellB.h"
#import "IXPickerChooseView.h"

#import "IXBORequestMgr+Region.h"
#import "IXBORequestMgr+Account.h"
#import "IXOpenAcntModel.h"
#import "IXUserDefaultM.h"

#import "IXUserInfoM.h"

@interface IXAcntAddressVC ()
<
UITableViewDelegate,
UITableViewDataSource,
IXAcntStep1CellBDelegate,
IXAcntStep2CellBDelegate
>
@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) IXPickerChooseView    * pickChoose;
@property (nonatomic, strong) NSArray       * dataArr;
@property (nonatomic, assign) BOOL          isEdit;
@property (nonatomic, assign) BOOL          infoChanged;

@property (nonatomic, strong) NSMutableArray    * countryArr;   //国家数组
@property (nonatomic, strong) NSMutableArray    * provinceArr;  //省份数组
@property (nonatomic, strong) NSMutableArray    * cityArr;      //城市数组

@property (nonatomic, strong) NSDictionary      * countryDic;   //国家
@property (nonatomic, strong) NSDictionary      * provinceDic;  //省份

@property (nonatomic, copy) NSString    * regionStr;    //原始地区
@property (nonatomic, copy) NSString    * detailAddr;   //原始详细地址
@property (nonatomic, copy) NSString    * postCode;     //原始邮编

@end

@implementation IXAcntAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LocalizedString(@"居住信息");
    [self addBackItem];
    [self.view addSubview:self.tableV];
    
    [self addDismissItemWithTitle:LocalizedString(@"编辑") target:self action:@selector(editAction)];
    
    [self refreashSectionInfo];
    _regionStr = self.accountInfo.regionStr;
    _postCode  = self.accountInfo.postCode;
    _detailAddr = self.accountInfo.address;
}

- (void)editAction
{
    _isEdit = !_isEdit;
    
    if (_isEdit) {
        [self addDismissItemWithTitle:LocalizedString(@"完成") target:self action:@selector(editAction)];
    } else {
        [self addDismissItemWithTitle:LocalizedString(@"编辑") target:self action:@selector(editAction)];
        [self.tableV endEditing:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self postModifyUserInfo];
        });
    }
    
    [self.tableV reloadData];
}

- (void)refreashSectionInfo
{
    weakself;
    NSArray * provinceArr = [IXBORequestMgr region_refreshProvinceWithNationalCode:kChinaNationalCode
                                                                       countryCode:kChinaCountryCoiide
                                                                          complete:^(NSArray<IXProvinceM *> *list, NSString *errStr, BOOL hasNewData)
                             {
                                 [weakSelf dealWithProvinceList:list
                                                         errStr:errStr
                                                     hasNewData:hasNewData];
                             }];
    if (provinceArr.count) {
        [self dealWithProvinceList:provinceArr errStr:@"" hasNewData:YES];
    }else{
        [SVProgressHUD showWithStatus:LocalizedString(@"获取城市信息中...")];
    }
}

- (void)dealWithProvinceList:(NSArray *)list errStr:(NSString *)str hasNewData:(BOOL)has
{
    [SVProgressHUD dismiss];
    
    if (list.count && has) {
        [self setProvinceArr:[list mutableCopy]];
        
        NSMutableArray  * arr = [@[] mutableCopy];
        if (self.accountInfo.province) {
            [arr addObject:self.accountInfo.province];
        }
        if (self.accountInfo.city) {
            [arr addObject:self.accountInfo.city];
        }
        
        self.pickChoose.dataArr = [list mutableCopy];
        self.pickChoose.seletedTitles = arr;
    }else if (str.length && !list.count){
        [SVProgressHUD showMessage:str];
    }
    
    if (str.length) {
        ELog(@"刷新地区信息接口请求失败");
    }
}

- (void)setProvinceArr:(NSMutableArray *)provinceArr
{
    _provinceArr = provinceArr;
    
    NSString    * pCode = self.accountInfo.province;
    NSString    * cCode = self.accountInfo.city;
    
    if (pCode.length) {
        [self.provinceArr enumerateObjectsUsingBlock:^(IXProvinceM * _Nonnull province, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([province.code isEqualToString:pCode]) {
                self.accountInfo.provinceName = [province localizedName];
                
                [province.subCountryDictParamList enumerateObjectsUsingBlock:^(IXCityM * _Nonnull city, NSUInteger idx1, BOOL * _Nonnull stop1) {
                    if ([city.code isEqualToString:cCode]) {
                        self.accountInfo.cityName = [city localizedName];
                        self.accountInfo.regionStr = [NSString stringWithFormat:@"%@ %@",[province localizedName],[city localizedName]];
                        *stop1 = YES;
                    }
                }];
                *stop = YES;
            }
        }];
    }
}

- (void)postModifyUserInfo
{
    if (!self.accountInfo.gts2CustomerId || !_infoChanged) {
        return;
    }
    
    IXIdDocumentNumberModel *numberModel = [[IXIdDocumentNumberModel alloc] init];
    //    numberModel.value = [[self.contentArr objectAtIndex:1] objectAtIndex:1];
    
    IXAcntInfoM * model = [[IXAcntInfoM alloc] init];
    model.address = self.accountInfo.address;
    model.chineseName = self.accountInfo.nickName;
    if ([self.accountInfo.email containsString:@"@"]) {
        model.email = self.accountInfo.email;
    } else {
        model.email = @"";
    }
    model.idDocumentNumber = numberModel;
    //    model.idDocumentNumberMd5 = [[self.contentArr objectAtIndex:1] objectAtIndex:1];
    if ([IXUserDefaultM isChina]) {
        model.nationality = @"ISO_3166_156";
    }
    model.province = self.accountInfo.province;
    model.city = self.accountInfo.city;
    model.postalCode = self.accountInfo.postCode;
    model.platform = PLATFORM;
    model.gts2CustomerId = self.accountInfo.gts2CustomerId;
    
    NSDictionary *paramDic = @{
                               @"customer":[model toJSONString],
                               @"gts2CustomerId":self.accountInfo.gts2CustomerId
                               };
    [SVProgressHUD showWithStatus:LocalizedString(@"数据提交中...")];
    
    [IXBORequestMgr acc_modifyUserInfoWithParam:paramDic
                                         result:^(BOOL success,
                                                  NSString *errCode,
                                                  NSString *errStr, id obj)
     {
         if (success) {
             [SVProgressHUD showSuccessWithStatus:LocalizedString(@"用户信息修改成功")];
             [IXBORequestMgr loginBo:nil];
         } else {
             if ([errCode length]) {
                 [SVProgressHUD showErrorWithStatus:errStr];
             } else {
                 [SVProgressHUD showErrorWithStatus:LocalizedString(@"用户信息修改失败")];
             }
         }
     }];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString    * option = self.dataArr[indexPath.row];
    if (SameString(option, LocalizedString(@"详细地址"))) {
        return 75;
    }
    return 44;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView  * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    NSArray * topColors = @[
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                            DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0))
                            ];
    CAGradientLayer * topL = [CAGradientLayer topShadowWithFrame:CGRectMake(0, 5, kScreenWidth, 5)
                                                        dkcolors:topColors];
    [view.layer addSublayer:topL];
    
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [view.layer addSublayer:btomL];
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView  * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    NSArray * btomColors = @[
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.15),UIColorWithHex(0xb5babe, 0.0)),
                             DKColorPickerWithColors(UIColorWithHex(0xb5babe, 0.0),UIColorWithHex(0xb5babe, 0.0)),
                             ];
    
    CAGradientLayer * btomL = [CAGradientLayer btomShadowWithFrame:CGRectMake(0, 0, kScreenWidth, 10)
                                                          dkcolors:btomColors];
    [view.layer addSublayer:btomL];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString    * option = self.dataArr[indexPath.row];

    if (SameString(option, LocalizedString(@"所在地区"))) {
        IXAcntInfoAreaCellA *cell =[tableView dequeueReusableCellWithIdentifier:
                                    NSStringFromClass([IXAcntInfoAreaCellA class])];
        
        if (self.accountInfo.regionStr) {
            [cell loadUIWithDesc:self.accountInfo.regionStr];
        }else{
            [cell loadUIWithDesc:@""];
        }
        
        UIView *sepImg = [[UIView alloc] initWithFrame:CGRectMake( 15, 44 - kLineHeight, kScreenWidth - 15, kLineHeight)];
        sepImg.dk_backgroundColorPicker = DKLineColor;
        [cell.contentView addSubview:sepImg];
        
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (_isEdit) {
            cell.desc.dk_textColorPicker = DKCellTitleColor;
        } else {
            cell.desc.dk_textColorPicker = DKCellContentColor;
        }
        
        return cell;
    }
    else if (SameString(option, LocalizedString(@"详细地址"))) {
        IXAcntStep2CellB *cell = [tableView dequeueReusableCellWithIdentifier:
                                  NSStringFromClass([IXAcntStep2CellB class])];
        cell.textV.textAlignment = NSTextAlignmentLeft;
//        cell.textV.frame = CGRectMake(81.5, 5, kScreenWidth - 15 - 78,70);
        cell.delegate = self;
        cell.tag = indexPath.row;
        
        UIImageView *sepImg = [[UIImageView alloc] initWithFrame:CGRectMake( 15, 75 - kLineHeight, kScreenWidth - 15, kLineHeight)];
        sepImg.dk_backgroundColorPicker = DKLineColor;
        [cell.contentView addSubview:sepImg];
        [cell loadUIWithTextView:self.accountInfo.address];
        [cell setTextViewEnable:_isEdit];
        
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_isEdit) {
            cell.textV.dk_textColorPicker = DKCellTitleColor;
        } else {
            cell.textV.dk_textColorPicker = DKCellContentColor;
        }
        return cell;
    }
    else if (SameString(option, LocalizedString(@"邮编号码"))) {
        IXAcntStep1CellB *cell =[tableView dequeueReusableCellWithIdentifier:
                                 NSStringFromClass([IXAcntStep1CellB class])];
        cell.delegate = self;
        cell.textF.textAlignment = NSTextAlignmentLeft;
        cell.textF.frame =CGRectMake(84.5, 14, kScreenWidth - 15 - 81,16);
        [cell setTextFieldEnable:_isEdit];
        [cell loadUIWithDesc:LocalizedString(@"邮编号码")
                        text:self.accountInfo.postCode
                 placeHolder:LocalizedString(@"邮编信息")
                keyboardType:UIKeyboardTypeNumberPad
             secureTextEntry:NO
                   iconImage:nil
                isHiddenIcon:YES
                     iconTag:indexPath.row];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        cell.dk_backgroundColorPicker = DKNavBarColor;
        if (_isEdit) {
            cell.textF.dk_textColorPicker = DKCellTitleColor;
        } else {
            cell.textF.dk_textColorPicker = DKCellContentColor;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && _isEdit) {
        [self.pickChoose show];
    }
}

#pragma mark -
#pragma mark - cell delegate

- (void)textValue:(NSString *)value tag:(NSInteger)tag
{
    if (![self.postCode isEqualToString:value]){
        _infoChanged = YES;
        self.accountInfo.postCode = value;
    }
}

- (void)textViewValue:(NSString *)value tag:(NSInteger)tag
{
    if (![self.detailAddr isEqualToString:value]) {
        _infoChanged = YES;
        self.accountInfo.address = value;
    }
}

#pragma mark -
#pragma mark - other


#pragma mark -
#pragma mark - lazy loading

static  NSString    * cellIdent = @"cell";
- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)
                                               style:UITableViewStyleGrouped];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
        [_tableV registerClass:[IXAcntStep2CellB class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntStep2CellB class])];
        [_tableV registerClass:[IXAcntStep1CellB class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntStep1CellB class])];
        [_tableV registerClass:[IXAcntInfoAreaCellA class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntInfoAreaCellA class])];
    }
    return _tableV;
}

- (IXPickerChooseView *)pickChoose
{
    if (!_pickChoose) {
        weakself;
        _pickChoose = [[IXPickerChooseView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)
                                                 WithChooseInfo:nil
                                                 WithResultInfo:^(NSInteger section, NSInteger row)
                       {
                           NSMutableString *regionStr = [@"" mutableCopy];
                           IXProvinceM *model = weakSelf.provinceArr[section];
                           
                           weakSelf.accountInfo.provinceName = [model localizedName];
                           weakSelf.accountInfo.province = model.code;
                           if (weakSelf.accountInfo.provinceName) {
                               [regionStr appendString:weakSelf.accountInfo.provinceName];
                           }
                           
                           if (model.subCountryDictParamList.count > row) {
                               IXCityM *cityM = model.subCountryDictParamList[row];
                               weakSelf.accountInfo.cityName = [cityM localizedName];
                               weakSelf.accountInfo.city = cityM.code;
                               
                               if (weakSelf.accountInfo.cityName) {
                                   [regionStr appendString:@" "];
                                   [regionStr appendString:weakSelf.accountInfo.cityName];
                               }
                           } else {
                               weakSelf.accountInfo.cityName = @"";
                               weakSelf.accountInfo.city = @"";
                           }
                           
                           weakSelf.accountInfo.regionStr = regionStr;
                           [weakSelf.tableV reloadData];
                           weakSelf.infoChanged = [weakSelf.regionStr isEqualToString:regionStr];
                       }];
    }
    return _pickChoose;
}


- (NSArray *)dataArr
{
    if (!_dataArr) {
        if ([IXUserDefaultM isChina]) {
            _dataArr = @[LocalizedString(@"所在地区"),
                         LocalizedString(@"详细地址"),
                         LocalizedString(@"邮编号码")];
        } else {
            _dataArr = @[LocalizedString(@"详细地址"),
                         LocalizedString(@"邮编号码")];
        }
    }
    return _dataArr;
}

- (AccountInfoM *)accountInfo
{
    if (!_accountInfo) {
        IXUserInfoM * boM = [IXBORequestMgr shareInstance].userInfo;
        
        _accountInfo = [AccountInfoM new];
        _accountInfo.gts2CustomerId = [NSString stringWithFormat:@"%lu",boM.gts2CustomerId];
        _accountInfo.provinceName   = boM.detailInfo.province;
        _accountInfo.cityName       = boM.detailInfo.city;
        _accountInfo.regionStr  = @"";
        _accountInfo.photoUrl   = boM.headImgUrl;
        _accountInfo.nickName   = boM.detailInfo.chineseName;
        _accountInfo.idCard     = boM.detailInfo.idDocumentNumber;
        _accountInfo.phoneNum   = boM.detailInfo.mobilePhone;
        _accountInfo.email      = boM.detailInfo.email;
        _accountInfo.province   = boM.detailInfo.province;
        _accountInfo.city       = boM.detailInfo.city;
        _accountInfo.postCode   = boM.detailInfo.postalCode;
        _accountInfo.address    = boM.detailInfo.address;
        _accountInfo.idDocument = boM.detailInfo.idDocument;
        if (!_accountInfo.idDocument.length) {
            _accountInfo.idDocument = @"0111";
        }
        _accountInfo.sid        = boM.sid;
    }
    
    return _accountInfo;
}

@end



@implementation AccountInfoM

- (NSString *)sid
{
    return _sid ? _sid : @"";
}

- (NSString *)nickName
{
    return _nickName ? _nickName : @"";
}

- (NSString *)idCard
{
    return _idCard ? _idCard : @"";
}

- (NSString *)phoneNum
{
    if (_phoneNum.length) {
        return _phoneNum;
    }
    return LocalizedString(@"未绑定");
}

- (NSString *)email
{
    if (_email.length) {
        return _email;
    }
    return LocalizedString(@"未绑定");
}

- (NSString *)regionStr
{
    if (_regionStr.length) {
        return _regionStr;
    }
    else if (_province && _city) {
        return [NSString stringWithFormat:@"%@ %@",_provinceName,_cityName];
    }
    else if (_province){
        return _province;
    }
    else if (_city)
    {
        return _city;
    }
    return @"";
}

- (NSString *)postCode
{
    return _postCode ? _postCode : @"";
}

- (NSString *)gts2CustomerId
{
    return _gts2CustomerId ? _gts2CustomerId : @"";
}

- (NSString *)provinceName
{
    return _provinceName ? _provinceName : @"";
}

- (NSString *)address
{
    return _address ? _address : @"";
}

- (NSString *)idDocument
{
    return _idDocument ? _idDocument : @"";
}

- (NSString *)photoUrl
{
    return _photoUrl ? _photoUrl : @"";
}




@end
