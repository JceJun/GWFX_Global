//
//  IXZoomScrollView.m
//  PhotoBrowserDemo
//
//  Created by Seven on 2017/6/29.
//  Copyright © 2017年 Seven. All rights reserved.
//

#import "IXZoomScrollView.h"

#pragma mark -
#pragma mark -
#pragma mark - IXTapDetectingV

@implementation IXTapDetectingV

- (id)init {
    if ((self = [super init])) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    NSUInteger tapCount = touch.tapCount;
    switch (tapCount) {
        case 1:
            [self handleSingleTap:touch];
            break;
        case 2:
            [self handleDoubleTap:touch];
            break;
        case 3:
            [self handleTripleTap:touch];
            break;
        default:
            break;
    }
    [[self nextResponder] touchesEnded:touches withEvent:event];
}

- (void)handleSingleTap:(UITouch *)touch {
    if (_tapDelegate && [_tapDelegate respondsToSelector:@selector(view:singleTapDetected:)])
        [_tapDelegate view:self singleTapDetected:touch];
}

- (void)handleDoubleTap:(UITouch *)touch {
    if (_tapDelegate && [_tapDelegate respondsToSelector:@selector(view:doubleTapDetected:)])
        [_tapDelegate view:self doubleTapDetected:touch];
}

- (void)handleTripleTap:(UITouch *)touch {
    if (_tapDelegate && [_tapDelegate respondsToSelector:@selector(view:tripleTapDetected:)])
        [_tapDelegate view:self tripleTapDetected:touch];
}

@end

// --------------------------------------------------------------------------

#pragma mark -
#pragma mark -
#pragma mark - IXTapDetectingImgV

@implementation IXTapDetectingImgV

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (id)initWithImage:(UIImage *)image {
    if ((self = [super initWithImage:image])) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage {
    if ((self = [super initWithImage:image highlightedImage:highlightedImage])) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    NSUInteger tapCount = touch.tapCount;
    switch (tapCount) {
        case 1:
            [self handleSingleTap:touch];
            break;
        case 2:
            [self handleDoubleTap:touch];
            break;
        case 3:
            [self handleTripleTap:touch];
            break;
        default:
            break;
    }
    [[self nextResponder] touchesEnded:touches withEvent:event];
}

- (void)handleSingleTap:(UITouch *)touch {
    if (_tapDelegate && [_tapDelegate respondsToSelector:@selector(imageView:singleTapDetected:)])
        [_tapDelegate imageView:self singleTapDetected:touch];
}

- (void)handleDoubleTap:(UITouch *)touch {
    if (_tapDelegate && [_tapDelegate respondsToSelector:@selector(imageView:doubleTapDetected:)])
        [_tapDelegate imageView:self doubleTapDetected:touch];
}

- (void)handleTripleTap:(UITouch *)touch {
    if (_tapDelegate && [_tapDelegate respondsToSelector:@selector(imageView:tripleTapDetected:)])
        [_tapDelegate imageView:self tripleTapDetected:touch];
}


@end

// --------------------------------------------------------------------------
#pragma mark -
#pragma mark -
#pragma mark - IXZoomScrollView

@interface IXZoomScrollView ()
<
IXTapDetectingVDelegate,
IXTapDetectingImgVDelegate,
UIScrollViewDelegate
>

@property (nonatomic, readwrite) IXTapDetectingImgV * photoImageView;
@property (nonatomic, readwrite) IXTapDetectingV    * tapView;
@property (nonatomic, assign) BOOL      needLayoutSubviews;

/**
 autoDismiss为YES时作为可自动隐藏的视图使用；
 为NO时，作为普通科缩放的image view使用
 */
@property (nonatomic, assign) BOOL      autoDismiss;
/** 提供图片的image view */
@property (nonatomic, strong) UIView    * targetV;

@end

@implementation IXZoomScrollView

IXZoomScrollView    * zoomView;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        // Tap view for background
        _tapView = [[IXTapDetectingV alloc] initWithFrame:self.bounds];
        _tapView.tapDelegate = self;
        _tapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tapView.backgroundColor = [UIColor clearColor];
        [self addSubview:_tapView];
        
        // Image view
        _photoImageView = [[IXTapDetectingImgV alloc] initWithFrame:CGRectZero];
        _photoImageView.tapDelegate = self;
        _photoImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_photoImageView];
        
        UIPanGestureRecognizer  * pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panAction:)];
        [_photoImageView addGestureRecognizer:pan];
        
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenBound.size.width;
        CGFloat screenHeight = screenBound.size.height;
        
        if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft
            || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight) {
            screenWidth = screenBound.size.height;
            screenHeight = screenBound.size.width;
        }
        
        // Setup
        self.backgroundColor = [UIColor clearColor];
        self.delegate = self;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        _needLayoutSubviews = YES;
    }
    
    return self;
}

+ (void)showFrom:(UIView *)view image:(UIImage *)img
{
    if (!zoomView) {
        zoomView = [[IXZoomScrollView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [[UIApplication sharedApplication].keyWindow addSubview:zoomView];
        [zoomView showFrom:view image:img];
    }
}

- (void)showFrom:(UIView *)view image:(UIImage *)img
{
    _autoDismiss = YES;
    self.image = img;
    _targetV = view;
    view.hidden = YES;
    
    CGRect  targetRect = [view.superview convertRect:view.frame toView:nil];
    _photoImageView.frame = targetRect;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
    [UIView animateWithDuration:0.3 animations:^{
        [self displayImage];
        _targetV.alpha = 0;
        self.backgroundColor = [UIColor blackColor];
    }];
}

- (void)dismiss
{
    if (!_autoDismiss) {
        return;
    }
    _needLayoutSubviews = NO;
    _targetV.hidden = NO;
    CGRect  targetRect = [_targetV.superview convertRect:_targetV.frame toView:nil];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.backgroundColor = [UIColor clearColor];
        _photoImageView.frame = targetRect;
    } completion:^(BOOL finished) {
        _targetV.alpha = 1;
        [self removeFromSuperview];
        zoomView = nil;
    }];
}

CGPoint originCenter;
CGRect  originR;
CGPoint originPoint;
- (void)panAction:(UIPanGestureRecognizer *)recognizer
{
    if (!_autoDismiss) {
        return;
    }
    
    _needLayoutSubviews = NO;
    CGPoint     p = [recognizer locationInView:self];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        originR = _photoImageView.frame;
        originCenter = _photoImageView.center;
        originPoint = [recognizer locationInView:self];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGRect  rect = CGRectMake(originR.origin.x,
                                  originR.origin.y + p.y - originPoint.y,
                                  originR.size.width,
                                  originR.size.height);
        _photoImageView.frame = rect;
        CGFloat alpha = 1 - fabs(_photoImageView.center.y - originCenter.y)/originCenter.y;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:alpha];
        [[UIApplication sharedApplication] setStatusBarHidden:alpha > 0.85 withAnimation:YES];
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded){
        _needLayoutSubviews = YES;
        if (fabs(p.y - originPoint.y) < 110) {
            [self resetImageViewPosition];
        }else{
            [self dismiss];
        }
    }
}

- (void)layoutSubviews
{
    if (!_needLayoutSubviews) return;
    // Update tap view frame
    _tapView.frame = self.bounds;
    
    // Super
    [super layoutSubviews];
    
    // Center the image as it becomes smaller than the size of the screen
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = _photoImageView.frame;
    
    // Horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = floorf((boundsSize.width - frameToCenter.size.width) / 2.0);
    } else {
        frameToCenter.origin.x = 0;
    }
    
    // Vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = floorf((boundsSize.height - frameToCenter.size.height) / 2.0);
    } else {
        frameToCenter.origin.y = 0;
    }
    
    // Center
    if (!CGRectEqualToRect(_photoImageView.frame, frameToCenter))
        _photoImageView.frame = frameToCenter;
}


#pragma mark -
#pragma mark - other

- (void)setImage:(UIImage *)image
{
    _photoImageView.image = image; // Release image
    _image = image;
    [self displayImage];
}

- (void)displayImage
{
    if (_image) {
        // Reset
        self.maximumZoomScale = 1;
        self.minimumZoomScale = 1;
        self.zoomScale = 1;
        
        // Set image
        _photoImageView.image = _image;
        _photoImageView.hidden = NO;
        
        // Setup photo frame
        CGRect photoImageViewFrame;
        photoImageViewFrame.origin = CGPointZero;
        photoImageViewFrame.size = _image.size;
        
        CGSize  size = CGSizeZero;
        if (photoImageViewFrame.size.height/photoImageViewFrame.size.width > kScreenHeight/kScreenWidth) {
            size = CGSizeMake(kScreenHeight*photoImageViewFrame.size.width/photoImageViewFrame.size.height, kScreenHeight);
        }else{
            size = CGSizeMake(kScreenWidth, kScreenWidth*photoImageViewFrame.size.height/photoImageViewFrame.size.width);
        }
        //处理图片过大
        if (size.height >= kScreenHeight) {
            size.height = kScreenHeight - 60;
        }
        _photoImageView.frame = CGRectMake((kScreenWidth - size.width)/2,
                                           (kScreenHeight - size.height)/2,
                                           size.width, size.height);
        self.contentSize = photoImageViewFrame.size;
        
        // Set zoom to minimum zoom
        [self setMaxMinZoomScalesForCurrentBounds];
        
        [self setNeedsLayout];
    }else {
        // Hide image view
        _photoImageView.hidden = YES;
    }
}

- (void)resetImageViewPosition
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
    [UIView animateWithDuration:0.4 animations:^{
        CGFloat y = (kScreenHeight - _image.size.height * kScreenWidth / _image.size.width) * 0.5;
        CGFloat height = _image.size.height * kScreenWidth / _image.size.width;
        [_photoImageView setFrame:CGRectMake(0, y, kScreenWidth, height)];
        [self setAlpha:1];
        self.backgroundColor = [UIColor blackColor];
    } completion:nil];
}


- (void)setMaxMinZoomScalesForCurrentBounds
{
    // Reset
    self.maximumZoomScale = 1;
    self.minimumZoomScale = 1;
    self.zoomScale = 1;
    
    // Bail
    if (_photoImageView.image == nil) return;
    
    // Sizes
    CGSize boundsSize = self.bounds.size;
    boundsSize.width -= 0.1;
    boundsSize.height -= 0.1;
    
    CGSize imageSize = _photoImageView.frame.size;
    
    // Calculate Min
    CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
    CGFloat minScale = MIN(xScale, yScale);                 // use minimum of these to allow the image to become fully visible
    
    // Calculate Max
    CGFloat maxScale = 4.0; // Allow double scale
    // on high resolution screens we have double the pixel density, so we will be seeing every pixel if we limit the
    // maximum zoom scale to 0.5.
    if ([UIScreen instancesRespondToSelector:@selector(scale)]) {
        maxScale = maxScale / [[UIScreen mainScreen] scale];
        if (maxScale < minScale) {
            maxScale = minScale * 2;
        }
    }
    
    // Calculate Max Scale Of Double Tap
    CGFloat maxDoubleTapZoomScale = 4.0 * minScale; // Allow double scale
    // on high resolution screens we have double the pixel density, so we will be seeing every pixel if we limit the
    // maximum zoom scale to 0.5.
    if ([UIScreen instancesRespondToSelector:@selector(scale)]) {
        maxDoubleTapZoomScale = maxDoubleTapZoomScale / [[UIScreen mainScreen] scale];
        if (maxDoubleTapZoomScale < minScale) {
            maxDoubleTapZoomScale = minScale * 2;
        }
    }
    
    // Make sure maxDoubleTapZoomScale isn't larger than maxScale
    maxDoubleTapZoomScale = MIN(maxDoubleTapZoomScale, maxScale);
    
    // Set
    self.maximumZoomScale = maxScale;
    self.minimumZoomScale = minScale;
    self.zoomScale = minScale;
    self.maximumDoubleTapZoomScale = maxDoubleTapZoomScale;
    
    [self setNeedsLayout];
}

#pragma mark -
#pragma mark - scroll view

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _photoImageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark -
#pragma mark - delegate method

// Image View
- (void)imageView:(UIImageView *)imageView singleTapDetected:(UITouch *)touch
{
    [self dismiss];
}

- (void)imageView:(UIImageView *)imageView doubleTapDetected:(UITouch *)touch
{
//    [self handleDoubleTap:[touch locationInView:imageView]];
}

// Background View
- (void)view:(UIView *)view singleTapDetected:(UITouch *)touch
{
    [self dismiss];

}

- (void)view:(UIView *)view doubleTapDetected:(UITouch *)touch
{
    [self handleDoubleTap:[touch locationInView:view]];
}

- (void)handleDoubleTap:(CGPoint)touchPoint
{
    // Zoom
    if (self.zoomScale == self.maximumDoubleTapZoomScale) {
        // Zoom out
        [self setZoomScale:self.minimumZoomScale animated:YES];
        self.contentSize = CGSizeMake(self.contentSize.width, [UIScreen mainScreen].bounds.size.height+1);
    } else {
        // Zoom in
        CGSize targetSize = CGSizeMake(self.frame.size.width / self.maximumDoubleTapZoomScale, self.frame.size.height / self.maximumDoubleTapZoomScale);
        CGPoint targetPoint = CGPointMake(touchPoint.x - targetSize.width / 2, touchPoint.y - targetSize.height / 2);
        
        [self zoomToRect:CGRectMake(targetPoint.x, targetPoint.y, targetSize.width, targetSize.height) animated:YES];
    }
}


@end
