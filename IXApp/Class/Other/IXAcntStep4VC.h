//
//  IXAcntStep4VC.h
//  IXApp
//
//  Created by Magee on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXOpenAcntDataModel.h"
#import "IXEvaluationM.h"

@interface IXAcntStep4VC : IXDataBaseVC
@property (nonatomic, strong)IXOpenAcntDataModel *acntModel;
@property (nonatomic, strong)IXEvaluationM *evaluationM;
@end
