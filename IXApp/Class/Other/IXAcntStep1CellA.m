//
//  IXAcntStep1CellA.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep1CellA.h"

@interface IXAcntStep1CellA()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *desc;
@property (nonatomic, strong)UIImageView *iconImgV;

@end

@implementation IXAcntStep1CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 87.5, 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKGrayTextColor;
        _title.text = LocalizedString(@"国家/地区");
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(14.5+87.5, 14, 87.5, 16)];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKCellTitleColor;
        _desc.text = LocalizedString(@"中国");
        _desc.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (UIImageView *)iconImgV
{
    if (!_iconImgV) {
        _iconImgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (7.5 + 16), 15, 7.5, 13.5)];
        _iconImgV.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
        [self.contentView addSubview:_iconImgV];
    }
    
    return _iconImgV;
}

- (void)loadUIWithDesc:(NSString *)desc
                  text:(NSString *)title
             iconImage:(UIImage *)image
         isHiddenIcon:(BOOL)isHidden {
    
    self.desc.text = desc;
    self.title.text = title;
    self.iconImgV.hidden = isHidden;
    
    if (!isHidden && image) {
        self.iconImgV.image = image;
    }
    
    self.dk_backgroundColorPicker = DKNavBarColor;
}

@end
