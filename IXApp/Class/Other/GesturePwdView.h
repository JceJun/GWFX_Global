//
//  GesturePwdView.h
//  AliPayDemo
//
//  Created by pg on 15/7/9.
//  Copyright (c) 2015年 pg. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    GesturePWDModeSetPwdMode,      //设置密码（无论存不存老密码都一并删除，在重新设置密码）
    GesturePWDModeConfirmPwdMode,  //验证密码 (输入一遍，进行验证)，提示文本top约束更新为与self.top对齐
    NoneMode
}GesturePWDMode;

typedef void (^PasswordBlock) (NSString *pswString);
typedef void (^ConfirmFailBlock)();

@interface GesturePwdView : UIView

@property (nonatomic, assign) GesturePWDMode    gestureMode;
@property (nonatomic, copy) PasswordBlock       block;
@property (nonatomic, copy) ConfirmFailBlock    confirmFailBlock;

- (instancetype)initWithModel:(GesturePWDMode)model;

@end
