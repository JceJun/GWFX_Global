//
//  IXAcntAddressVC.h
//  IXApp
//
//  Created by Seven on 2017/12/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXBaseNavVC.h"
@class AccountInfoM;

@interface IXAcntAddressVC : IXDataBaseVC

@property (nonatomic, strong) AccountInfoM  * accountInfo;  //存储修改的用户信息

@end


@interface AccountInfoM : NSObject

@property (nonatomic, copy) NSString    * gts2CustomerId;
@property (nonatomic, copy) NSString    * provinceName;     //省份
@property (nonatomic, copy) NSString    * cityName;         //城市
@property (nonatomic, copy) NSString    * regionStr;        //xx省xx市
@property (nonatomic, copy) NSString    * photoUrl;         //头像
@property (nonatomic, copy) NSString    * nickName;         //昵称
@property (nonatomic, copy) NSString    * idCard;           //身份证号码
@property (nonatomic, copy) NSString    * phoneNum;         //电话
@property (nonatomic, copy) NSString    * email;            //邮箱
@property (nonatomic, copy) NSString    * province;         //省份code
@property (nonatomic, copy) NSString    * city;             //城市code
@property (nonatomic, copy) NSString    * postCode;         //邮编
@property (nonatomic, copy) NSString    * address;          //地址
@property (nonatomic, copy) NSString    * idDocument;       //证件号
@property (nonatomic, copy) NSString    * cerType;          //证件类型
@property (nonatomic, copy) NSString    * sid;

@end
