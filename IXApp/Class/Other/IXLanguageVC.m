//
//  IXLanguageVC.m
//  IXApp
//
//  Created by Evn on 17/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXLanguageVC.h"
#import "IXLanguageCell.h"
#import "IXLocalizationModel.h"
#import "AppDelegate+UI.h"
#import "IXPositionSymModel.h"

@interface IXLanguageVC ()<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) NSArray       * titleArr;
@property (nonatomic, strong) NSString      * languageStr;
@end

@implementation IXLanguageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    [self.view addSubview:self.tableV];
    [self initData];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initData
{
    switch ( _type ) {
        case SettingTypeLanguage:{
            self.title = LocalizedString(@"系统语言");
            _titleArr = @[@"简体中文",@"English"];//LocalizedString(@"繁体中文"),
            _languageStr = [IXLocalizationModel currentShowLanguage];
        }
            break;
        case SettingTypeTradeType:{
            self.title = LocalizedString(@"市场模式");
            _titleArr = @[LocalizedString(@"普通交易"),
                          LocalizedString(@"快速交易")];
            _languageStr =  ([IXEntityFormatter getCellStyle] == 0) ?
           LocalizedString(@"普通交易") :
           LocalizedString(@"快速交易");
        }
            break;
        case SettingTypeColor:{
            self.title = LocalizedString(@"升跌颜色");
            _titleArr = @[LocalizedString(@"红涨绿跌"),
                          LocalizedString(@"绿涨红跌")];
            
            id value = [[NSUserDefaults standardUserDefaults] objectForKey:@"COLORSETTING"] ;
            _languageStr = ( !value || [value isEqualToString:@"RED"] )  ?
                LocalizedString(@"红涨绿跌") :
                LocalizedString(@"绿涨红跌");
        }
            break;
        default:
            break;
    }
    [_tableV reloadData];
}

- (UITableView *)tableV{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, kScreenHeight - 10)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV  registerClass:[IXLanguageCell class]
         forCellReuseIdentifier:NSStringFromClass([IXLanguageCell class])];
    }
    return _tableV;
}

#pragma mark delegate && datasource method moduel
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_titleArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IXLanguageCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!cell) {
        cell = [[IXLanguageCell alloc] initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:NSStringFromClass([UITableViewCell class])];
    }
    
    [cell showBototmLineWithOffsetX:15];
    if( 0 == indexPath.row ){
        [cell showTopLineWithOffsetX:0];
    }
    else if (indexPath.row == _titleArr.count-1){
        [cell showBototmLineWithOffsetX:0];
    }
    
    BOOL flag = NO;
    if ([_titleArr[indexPath.row] isEqualToString:_languageStr]) {
        flag = YES;
    }
    [cell configTitle:_titleArr[indexPath.row] selectState:flag];
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([_languageStr isEqualToString:_titleArr[indexPath.row]]) {
        
        return;
    }
    
    _languageStr = _titleArr[indexPath.row];
    switch ( _type ) {
        case SettingTypeLanguage:{
            [IXLocalizationModel setLanguage:_titleArr[indexPath.row]];
            [AppDelegate showRoot];
#warning 临时优化
            [[IXPositionSymModel shareInstance] clearCache];
        }
            break;
        case SettingTypeTradeType:{
            [[NSUserDefaults standardUserDefaults] setValue:@(indexPath.row) forKey:kSaveSubSymbolCellStyle];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:kSaveSubSymbolCellStyle object:nil];
        }
            break;
        case SettingTypeColor:{
            NSString *value = (indexPath.row == 0) ? @"RED" : @"GREEN";
            [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"COLORSETTING"];
            [IXUserInfoMgr shareInstance].settingRed = [value isEqualToString:@"RED"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [AppDelegate showRoot];
        }
            break;
        default:
            break;
    }
   
    [self leftBtnItemClicked];
}

@end
