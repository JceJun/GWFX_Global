//
//  IXMsgCenterDetailVC.m
//  IXApp
//
//  Created by Seven on 2017/4/26.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXMsgCenterDetailVC.h"
#import "IXBORequestMgr+MsgCenter.h"
#import "IXAppUtil.h"
#import "IXCpyConfig.h"
#import "UIViewExt.h"
#define kNavHeight  64

@interface IXMsgCenterDetailVC ()

@property (nonatomic, strong) UILabel   * msgTitleLab;
@property (nonatomic, strong) UILabel   * timeLab;
@property (nonatomic, strong) UITextView   * detailLab;

@property (nonatomic, strong) UIView    * btomLine;

@property (nonatomic, copy) NSString    * msgContent;

@end

@implementation IXMsgCenterDetailVC

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!_message.msgContent.length && !_msgContent.length){
        [SVProgressHUD show];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"消息详情");
    [self layoutSubview];
    [self requestData];
    [self setvalues];
}

- (void)layoutSubview
{
    _msgTitleLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 17, kScreenWidth-30, 20)];
    _msgTitleLab.font = PF_MEDI(13);
    _msgTitleLab.dk_textColorPicker = DKCellTitleColor;
    _msgTitleLab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_msgTitleLab];
    
    _detailLab = [[UITextView alloc] initWithFrame:CGRectMake(0, 54, kScreenWidth, 100)];
    _detailLab.editable = NO;
    _detailLab.selectable = NO;
    _detailLab.font = PF_MEDI(13);
    _detailLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
    _detailLab.dk_backgroundColorPicker = DKNavBarColor;
    _detailLab.textContainerInset = UIEdgeInsetsMake(19, 10, 19, 10);
    [self.view addSubview:_detailLab];
    
    _timeLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, kScreenWidth - 30, 14)];
    _timeLab.font = RO_REGU(12);
    _timeLab.textColor = MarketGrayPriceColor;
    _timeLab.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0xe9e9ea);
    _timeLab.backgroundColor = [UIColor clearColor];
    _timeLab.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:_timeLab];
    
    UIView  * line = [[UIView alloc] initWithFrame:CGRectMake(0, 53, kScreenWidth, 1)];
    line.dk_backgroundColorPicker = DKLineColor;
    [self.view addSubview:line];

    _btomLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_detailLab.frame), kScreenWidth, 1)];
    _btomLine.dk_backgroundColorPicker = DKLineColor;
    [self.view addSubview:_btomLine];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setvalues
{
    _msgTitleLab.text  = _message.msgTitle;
    _timeLab.text   = [_message timeString];
    
    if (_msgContent.length) {
        _detailLab.text = _msgContent;
    }else{
        _detailLab.text = _message.msgContent;
    }
    
    CGSize  size = _detailLab.contentSize;
    CGPoint p = self.detailLab.frame.origin;
    
    if (size.height > kScreenHeight - p.y) {
        size.height = kScreenHeight - p.y;
    }else{
        _detailLab.scrollEnabled = NO;
    }
    
    _detailLab.frame = CGRectMake(p.x, p.y, size.width, size.height);
    _timeLab._top = _detailLab._bottom -20;
    _btomLine.frame = CGRectMake(0, CGRectGetMaxY(_timeLab.frame)+5, kScreenWidth, 1);
    
}

- (void)requestData{
    [IXBORequestMgr msg_obtainMsgDetailWithId:_message.msgId
                                     complete:^(IXMsgCenterM *m, NSString *errStr)
    {
        if (m) {
            _msgContent = m.msgContent;
            [self setvalues];
        }else{
            [SVProgressHUD showErrorWithStatus:errStr];
        }
    }];
}

@end
