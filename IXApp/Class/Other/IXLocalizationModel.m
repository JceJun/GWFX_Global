//
//  IXLocalizationModel.m
//  IXApp
//
//  Created by Bob on 2016/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXLocalizationModel.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"


@implementation IXLocalizationModel

+ (NSString *)getBoLanuageKey
{
    return NAMEEN;
    NSString *checkLanguage = [IXLocalizationModel getLanguage];
    if( [checkLanguage rangeOfString:@"en"].location != NSNotFound )
        return NAMEEN;
    else if( [checkLanguage rangeOfString:@"zh-Hans"].location != NSNotFound )
        return NAMECN;
    else
        return NAMETW;
}

// * @param langugae 语言类型 //@"en" @"zh-Hans" @"zh-Hant"  数据库保存类型 //zh-cn zh-tw en-us
+ (void)setLanguage:(NSString *)langugae
{
    if ( !langugae || ![langugae isKindOfClass:[NSString class]] ) {
        return;
    }
    
    [IXLocalizationModel saveLanguage:langugae];
}

+ (void)saveLanguage:(NSString *)langugae
{
    NSString *saveLan = @"en";
    if( [langugae isEqualToString:@"English"] )
        saveLan = @"en";
    else if( [langugae isEqualToString:@"简体中文"] )
        saveLan = @"zh-Hans";
    else
        saveLan = @"zh-Hant";
 
    [[NSUserDefaults standardUserDefaults] setValue:saveLan forKey:LANGUAGE];
}


+ (NSString *)currentShowLanguage
{
    NSString *showLanguage = [IXLocalizationModel getLanguage];
    if( [showLanguage rangeOfString:@"en"].location != NSNotFound )
        return @"English";
    else if( [showLanguage rangeOfString:@"zh-Hans"].location != NSNotFound )
        return @"简体中文";
    else
        return @"简体中文";
//        return LocalizedString(@"繁体中文");
}

+ (NSString *)currentMessageShowLanguage
{
    NSString *showLanguage = [IXBORequestMgr shareInstance].userInfo.detailInfo.messageLang;
    if (SameString(showLanguage,LANCN)) {
        return @"简体中文";
    } else if (SameString(showLanguage,LANEN)) {
        return @"English";
    } else {
        return @"简体中文";
    }
}

+ (NSString *)currentCheckLanguage
{
    NSString *checkLanguage = [IXLocalizationModel getLanguage];
    if( [checkLanguage rangeOfString:@"en"].location != NSNotFound )
        return LANEN;
    else if( [checkLanguage rangeOfString:@"zh-Hans"].location != NSNotFound )
        return LANCN;
    else
        return LANTW;
}

+ (NSString *)getLanguage
{
    NSString *currentLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGE];
    if (!currentLanguage) {
        currentLanguage = [[NSLocale preferredLanguages] firstObject];
        NSArray * arr = [currentLanguage componentsSeparatedByString:@"-"];
        if (arr.count > 2) {
            currentLanguage = [NSString stringWithFormat:@"%@-%@",arr[0],arr[1]];
        }
        
        //部分公司屏蔽语言设置、默认显示中文
        if (LanguageMode) {
            if (DefaultLanguage == 1) {
                return @"en";
            }
            if ([currentLanguage isEqualToString:@"zh-Hans"]) {
                return @"zh-Hans";
            } else if ([currentLanguage isEqualToString:@"zh-Hant"]
                       || [currentLanguage isEqualToString:@"zh-HK"]){
                return @"zh-Hants";//香港、澳门、台湾默认显示简体中文
            } else{
                return @"en";
            }
        } else {
            switch (DefaultLanguage) {
                case 0:{
                    return @"zh-Hans";
                }
                    break;
                case 1:{
                    return @"en";
                }
                    break;
                case 2:{
                    return @"zh-Hant";
                }
                    break;
                    
                default:
                    break;
            }
        }
        return currentLanguage;
    }
    return currentLanguage;
}

    
+ (NSString *)getLocalizationWordByKey:(NSString *)key
{
    NSString *path = [[NSBundle mainBundle] pathForResource:[IXLocalizationModel getLanguage] ofType:@"lproj"];
    if ( !path ) {
        return key;
    }
    NSString *word =  [[NSBundle bundleWithPath:path] localizedStringForKey:key value:nil table:@"localizations"];
    if ( !word ) {
        return key;
    }
    return word;
}

+ (NSString *)getLanguageKeyByLanguage:(NSString *)language
{
    if (SameString(language,@"English")) {
        return LANEN;
    } else {
        return LANCN;
    }
}
    
@end
