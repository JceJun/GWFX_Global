//
//  IXEvaluationHeaderV.h
//  IXApp
//
//  Created by Evn on 2018/1/30.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IXEvaluationM;

@interface IXEvaluationHeaderV : UIView

- (id)initWithData:(IXEvaluationM *)evaluationM
         pageIndex:(NSInteger)pageIndex;


@end
