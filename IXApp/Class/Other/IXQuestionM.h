//
//  IXQuestionM.h
//  IXApp
//
//  Created by Evn on 2017/11/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXQuestionM : NSObject
@property (nonatomic, copy) NSString *answer;
@property (nonatomic, copy) NSString *optionAppend;
@property (nonatomic, strong) NSArray *appendArr;
@property (nonatomic, copy) NSNumber *orderNumber;
@property (nonatomic, copy) NSNumber *paperId;
@property (nonatomic, copy) NSString *rightAnswer;
@property (nonatomic, copy) NSNumber *score;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSNumber *type;
@property (nonatomic, copy) NSString *lastAnswer;
@property (nonatomic, strong) NSMutableArray *answerArr;
@property (nonatomic, strong) NSNumber *questionId;
@property (nonatomic, strong) NSNumber *isRequired;

@end
