//
//  IXChangeEmailStep2VC.h
//  IXApp
//
//  Created by Evn on 2017/6/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXChangeEmailStep2VC : IXDataBaseVC

@property (nonatomic, strong)NSString *email;

@end
