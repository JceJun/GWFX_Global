//
//  IXCredentialM.m
//  IXApp
//
//  Created by Evn on 2017/5/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCredentialM.h"

@implementation IXCredentialM

- (id)initWithObj:(id)obj
{
    self = [super init];
    if (self) {
        id code = obj[@"code"];
        if ([code isKindOfClass:[NSString class]]) {
            self.code = code;
        }
        
        id name = obj[@"name"];
        if ([name isKindOfClass:[NSString class]]) {
            self.name = name;
        }
        
        id nameCN = obj[@"nameCN"];
        if ([nameCN isKindOfClass:[NSString class]]) {
            self.nameCN = nameCN;
        }
        
        id nameEN = obj[@"nameEN"];
        if ([nameEN isKindOfClass:[NSString class]]) {
            self.nameEN = nameEN;
        }
        
        id nameTW = obj[@"nameTW"];
        if ([nameTW isKindOfClass:[NSString class]]) {
            self.nameTW = nameTW;
        }
    }
    return self;
}

- (NSString *)localizedName
{
    NSString    * str = [IXLocalizationModel currentCheckLanguage];
    if ([str isEqualToString:LANCN]) {
        return _nameCN;
    }
    else if ([str isEqualToString:LANTW]){
        return _nameTW;
    }
    else if ([str isEqualToString:LANEN]){
        return _nameEN;
    }
    
    return _nameCN;
}

@end
