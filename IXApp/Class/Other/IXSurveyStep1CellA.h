//
//  IXSurveyStep1CellA.h
//  IXApp
//
//  Created by Evn on 2017/10/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXSurveyStep1CellA : UITableViewCell

- (void)loadUIWithText:(NSString *)text
           numberLines:(NSInteger)numberLines;

@end
