//
//  IXSurveyStep1VC.h
//  IXApp
//
//  Created by Evn on 2017/10/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXSurveyM.h"

@interface IXSurveyMainVC : IXDataBaseVC

@property (nonatomic, strong) IXSurveyM *surveyM;

@end
