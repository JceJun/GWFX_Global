//
//  IXModifyPwdVC.m
//  IXApp
//
//  Created by Bob on 2017/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXModifyPwdVC.h"
#import "IXModifyPwdCell.h"
#import "IXModifyPwdResultVC.h"
#import "IXUserDefaultM.h"

#import "IXAFRequest.h"
#import "NSString+FormatterPrice.h"
#import "SFHFKeychainUtils.h"
#import "IXTouchTableV.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXBORequestMgr+Account.h"

#define MODIFYBTNTAG 1

@interface IXModifyPwdVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) IXTouchTableV *contentTV;

@property (nonatomic, copy)   UIView *footView;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, copy)   NSString *passwd;

@property (nonatomic, copy)   NSString *passwdNew;

@property (nonatomic, copy)   NSString *passwdReNew;

@end

@implementation IXModifyPwdVC

#pragma mark life cycle
- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"修改登录密码");
    [self.contentTV reloadData];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark tableView delegate&&dataSource
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXModifyPwdCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             NSStringFromClass([IXModifyPwdCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tipName = self.dataArr[indexPath.row];
    
    weakself;
    cell.textContent = ^(NSString *value){
        switch (indexPath.row) {
            case 0:
                weakSelf.passwd = value;
                break;
            case 1:
                weakSelf.passwdNew = value;
                break;
            case 2:
                weakSelf.passwdReNew = value;
                break;
            default:
                break;
        }
        
        UIButton *modifyBtn = [weakSelf.footView viewWithTag:MODIFYBTNTAG];
        
        NSString *imageName = @"";
        if ([weakSelf.passwd length] &&
            [weakSelf.passwdNew length] &&
            [weakSelf.passwdReNew length]) {
            modifyBtn.userInteractionEnabled = YES;
            imageName = [IXUserDefaultM isNightMode] ? @"regist_btn_enable_D" : @"regist_btn_enable";
        } else {
            modifyBtn.userInteractionEnabled = NO;
            imageName = [IXUserDefaultM isNightMode] ? @"regist_btn_unable" : @"regist_btn_unable";
        }
        
        UIImage *image = GET_IMAGE_NAME(imageName);
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [modifyBtn setBackgroundImage:image forState:UIControlStateNormal];
    };
    
    cell.errorMsg = ^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    };
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}


#pragma mark btn Action
//检查密码是否完整设置
//合法密码不能与原密码一致
//合法密码6-18位字母数字
- (BOOL)checkPasswdValid
{
    if ( ![_passwd length] || ![_passwdNew length] || ![_passwdReNew length] ) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请完整输入原密码和新密码")];
        return NO;
    }
    

    if ( ![_passwdNew isEqualToString:_passwdReNew] ) {
        [SVProgressHUD showErrorWithStatus:PWDDIFF];
        return NO;
    }
    
    if ( [_passwd isEqualToString:_passwdNew] ) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"新密码不能与原密码一致")];
        return NO;
    }
    
    //老账户没有密码位数限制；兼容老用户只判断新密码是否满足要求
    if ( ![IXEntityFormatter validPasswd:_passwdReNew]  ) {
        [SVProgressHUD showErrorWithStatus:PWDVALIDCHAR];
        return NO;
    }
    
    return YES;
}

//修改密码
- (void)responseToModifyPwd
{
    [self.view endEditing:YES];
    if ( [self checkPasswdValid] ) {
 
        if ([IXBORequestMgr shareInstance].userInfo.gts2CustomerId) {
            
            [self dealWithModifyPasswd];
        } else {
            [IXBORequestMgr refreshUserInfo:^(BOOL success) {
                if (success && [IXBORequestMgr shareInstance].userInfo.gts2CustomerId) {
                    [self dealWithModifyPasswd];
                } else {
                   [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
                }
            }];
        }
    }
}

- (void)dealWithModifyPasswd
{
    NSDictionary *paramDic = @{
                               @"gts2CustomerId":[NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId],
                               @"oldPassword":[IXDataProcessTools md5StringByString:_passwd],
                               @"newPassword":[IXDataProcessTools md5StringByString:_passwdNew],
                               @"encryptPassword":_passwdNew
                               };
    [SVProgressHUD showWithStatus:LocalizedString(@"数据提交中...")];
    
    [IXBORequestMgr acc_modifyPasswdWithParam:paramDic
                                       result:^(BOOL success,
                                                NSString *errCode,
                                                NSString *errStr,
                                                id obj)
    {
        if (success) {
            [SVProgressHUD dismiss];
//            NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];;
//            [SFHFKeychainUtils storeUsername:userName
//                                 andPassword:_passwdNew
//                              forServiceName:kServiceName
//                              updateExisting:YES
//                                       error:nil];
            
            IXModifyPwdResultVC *resultVC = [[IXModifyPwdResultVC alloc] init];
            [self.navigationController pushViewController:resultVC animated:YES];
        } else {
            if ([errCode length]) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        }
    }];
}

#pragma init moduel
- (NSArray *)dataArr
{
    if ( !_dataArr ) {
         _dataArr = @[LocalizedString(@"原密码"),
                      LocalizedString(@"新密码"),
                      LocalizedString(@"确认新密码")];
    }
    return _dataArr;
}

- (UIView *)footView
{
    if ( !_footView ) {
         _footView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 83)];
        
        UIButton *modifyPwdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        modifyPwdBtn.frame = CGRectMake( 15, 40, kScreenWidth - 30, 43);
        modifyPwdBtn.tag = MODIFYBTNTAG;
        
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [modifyPwdBtn setBackgroundImage:image forState:UIControlStateNormal];
        modifyPwdBtn.userInteractionEnabled = NO;
        [_footView addSubview:modifyPwdBtn];
        [modifyPwdBtn setTitle:LocalizedString(@"修改密码") forState:UIControlStateNormal];
        [modifyPwdBtn.titleLabel setFont:PF_REGU(15)];
        [modifyPwdBtn addTarget:self action:@selector(responseToModifyPwd) forControlEvents:UIControlEventTouchUpInside];
    }
    return _footView;
}

- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds];
        _contentTV.dk_backgroundColorPicker = DKTableColor;
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXModifyPwdCell class]
           forCellReuseIdentifier:NSStringFromClass([IXModifyPwdCell class])];
        [self.view addSubview:_contentTV];
        _contentTV.tableFooterView = self.footView;
    }
    return _contentTV;
}

@end
