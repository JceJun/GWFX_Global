//
//  IXAlertSettingVC.m
//  IXApp
//
//  Created by Seven on 2017/6/2.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAlertSettingVC.h"
#import "IXAcntSafeCell.h"
#import "IXUserDefaultM.h"

@interface IXAlertSettingVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) NSArray       * titleArr;

@end

@implementation IXAlertSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.title = LocalizedString(@"声音设置");
    [self.view addSubview:self.tableV];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntSafeCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntSafeCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tipName = self.titleArr[indexPath.row];
    [cell.swiBtn addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    cell.swiBtn.tag = indexPath.row;
    
    if (indexPath.row == 0) {
        [cell showTopLineWithOffsetX:0];
        [cell.swiBtn setOn:[IXUserDefaultM soundAlertEnable]];
    }else{
        [cell showTopLineWithOffsetX:15];
        [cell showBototmLineWithOffsetX:0];
        [cell.swiBtn setOn:[IXUserDefaultM shakeAlertEnable]];
    }
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    return cell;
}


- (void)switchAction:(UISwitch *)swi
{
    if (swi.tag == 0) {
        //声音提醒
        BOOL sound = [IXUserDefaultM soundAlertEnable];
        swi.on = !sound;
        [IXUserDefaultM setSoundAlertEnable:!sound];
    }else{
        //震动提醒
        BOOL shake = [IXUserDefaultM shakeAlertEnable];
        swi.on = !shake;
        [IXUserDefaultM setShakeAlertEnable:!shake];
    }
}

#pragma mark -
#pragma mark - lazy loading

- (NSArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = @[
                      LocalizedString(@"订单成交声音提醒"),
                      LocalizedString(@"订单成交声音震动")
                      ];
    }
    return _titleArr;
}


- (UITableView *)tableV
{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,
                                                                kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)
                                               style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV  registerClass:[IXAcntSafeCell class]
         forCellReuseIdentifier:NSStringFromClass([IXAcntSafeCell class])];
    }
    return _tableV;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
