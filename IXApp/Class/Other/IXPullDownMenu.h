//
//  IXPullDownMenu.h
//  IXApp
//
//  Created by Seven on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^menuItemClicked)(NSInteger index, NSString * title);
typedef void(^endEdit)();

/** 下拉菜单 */
@interface IXPullDownMenu : UIView

@property (nonatomic, copy) menuItemClicked itemClicked;
@property (nonatomic, copy) endEdit endEditBlock;

@property (nonatomic, strong) NSArray    <NSString *>* menuItems;

@property (nonatomic, readonly) CGFloat  rowHeight;
@property (nonatomic, readonly) CGFloat  width;
@property (nonatomic, readonly) BOOL    isShowing;
@property (nonatomic, assign) CGFloat   offsetX;    //尖角x轴偏移量,默认为0

/** item以富文本形式显示 */
@property (nonatomic, assign)   BOOL    showAttributeText;

+ (IXPullDownMenu *)menuWithMenuItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width;

- (instancetype)initWithMenuItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width;

/**
 针对items可能有变动的情况
 
 @param view 显示在某个view上
 @param items title数组
 @param offset  显示在目标视图下方的偏移量，默认为0
 */
- (void)showMenuFrom:(UIView *)view items:(NSArray <NSString *>*)items offsetY:(CGFloat)offset;

/**
 针对items固定不变的情况
 
 @param view 显示在某个view上
 @param offset  显示在目标视图下方的偏移量，默认为0
 */
- (void)showMenuFrom:(UIView *)view offsetY:(CGFloat)offset;



- (void)dismiss;

@end
