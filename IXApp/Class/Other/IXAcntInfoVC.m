//
//  IXAcntInfoVC.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntInfoVC.h"
#import "IXAcntStep7VC.h"
#import "IXAcntBankListVC.h"
#import "IXAcntCerFileVC.h"
#import "IXAcntAddressVC.h"

#import "IXAcntAvatarCell.h"
#import "IXAcntInfoAreaCell.h"
#import "IXAcntInfoBankCell.h"
#import "IXSupplementStyle2Cell.h"
#import "IXAcntIdCell.h"

#import "IXTouchTableV.h"
#import "IXOpenChooseContentView.h"

#import "IXOpenAcntDataModel.h"
#import "IXOpenAcntModel.h"
#import "IXUserDefaultM.h"
#import "IXBOModel.h"
#import "IXUserInfoM.h"
#import "IXCredentialM.h"
#import "IXCpyConfig.h"

#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Region.h"
#import "NSString+FormatterPrice.h"
#import "UIImageView+WebCache.h"
#import "IXAppUtil.h"
#import "NSArray+IX.h"
#import "NSObject+IX.h"
#import "IXBORequestMgr+Asset.h"

@interface IXAcntInfoVC ()
<
UITableViewDataSource,
UITableViewDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
UITextFieldDelegate
>

// view
@property (nonatomic, strong) IXTouchTableV * contentTV;
@property (nonatomic, weak)   IXTextField     * nameTf;
@property (nonatomic, weak)   IXTextField     * cardTF;
@property (nonatomic, strong) UIButton      * tipBtn;
@property (nonatomic, strong) NSArray       * titleArr;

@property (nonatomic, strong) IXOpenChooseContentView *chooseCata;
@property (nonatomic, strong) IXOpenChooseContentView *chooseCer;
@property (nonatomic, strong) NSMutableArray *titleItemArr;
@property (nonatomic, strong) NSMutableArray *cerItemArr;

@property (nonatomic, strong) AccountInfoM  * accountInfo;  //存储修改的用户信息

@property (nonatomic, assign) BOOL          isEdit;

@end

@implementation IXAcntInfoVC

- (void)dealloc
{
    NSLog(@" -- %s --",__func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableHeaderColor;
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self
                                      sel:@selector(onGoback)];
    self.title = LocalizedString(@"用户资料");
    
    [self loadData];
}

- (void)loadData
{
    if ([[IXBORequestMgr shareInstance].userInfo enable]) {
        [self.contentTV reloadData];
        [self loadCustomerFileInfo];
    } else {
        [SVProgressHUD showWithStatus:LocalizedString(@"获取用户信息中...")];
        
        weakself;
        [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString * errStr) {
            if (response && success) {
                [SVProgressHUD dismiss];
                [weakSelf.contentTV reloadData];
                [weakSelf resetHeadImage];
                [weakSelf loadCustomerFileInfo];
            } else if (errStr.length){
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
            }
        }];
    }
}

- (void)loadRightBar
{
    [self addDismissItemWithTitle:LocalizedString(@"编辑") target:self action:@selector(editAction)];
}

- (void)editAction
{
    if (!_isEdit) {
        _isEdit = !_isEdit;
        [self addDismissItemWithTitle:LocalizedString(@"完成") target:self action:@selector(editAction)];
        [self.contentTV reloadData];
    } else {
        [self.contentTV endEditing:YES];
        if ( !self.accountInfo.nickName.length) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入姓名")];
        } else {
            [self postModifyUserInfo];
        }
    }
}

- (void)loadCustomerFileInfo
{
    if (![IXBORequestMgr shareInstance].fileArr.count) {
        NSDictionary *paramDic = @{
                                   @"customerNumber":@([IXBORequestMgr shareInstance].userInfo.detailInfo.customerNumber)
                                   };
         weakself;
        [IXBORequestMgr acc_customerFilesWithParam:paramDic
                                            result:^(BOOL success,
                                                     NSString *errCode,
                                                     NSString *errStr,
                                                     id obj)
         {
             if (success) {
                 if (obj && [obj ix_isArray]) {
                     [weakSelf processCustomerFileInfo:obj];
                 } else {
                     [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取客户文件信息失败")];
                 }
             } else {
                 [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取客户文件信息失败")];
             }
             self.accountInfo.nickName = [IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName;
             self.accountInfo.idCard = [IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber;
             if (!self.accountInfo.nickName.length || !self.accountInfo.idCard.length) {
                 [self loadRightBar];
             }
             [weakSelf fillCustomerFile];
             [weakSelf.contentTV reloadData];
         }];
    } else {
         [self fillCustomerFile];
    }
}

- (void)processCustomerFileInfo:(NSArray *)obj
{
    if ([obj ix_isArray]) {
        if (!obj || obj.count == 0) {
            return;
        }
        NSMutableArray *tmpArr = [NSMutableArray array];
        NSInteger count = 0;
        for (int i = 0; i < obj.count; i++) {
            id file = obj[i];
            if (file && [file isKindOfClass:[NSDictionary class]]) {
                id fileType = [(NSDictionary *)file stringForKey:@"fileType"];
                if (fileType && [fileType isKindOfClass:[NSString class]]) {
                    if ([(NSString *)fileType isEqualToString:@"FILE_TYPE_IDCARD_FRONT"] || [(NSString *)fileType isEqualToString:@"FILE_TYPE_IDCARD_BACK"] || [(NSString *)fileType isEqualToString:@"FILE_TYPE_ADDRESS"]) {
                        id filePath = [(NSDictionary *)file stringForKey:@"filePath"];
                        if (filePath && [filePath isKindOfClass:[NSString class]] && [(NSString *)filePath length] > 0) {
                            [tmpArr addObject:(NSDictionary *)file];
                            id ftpFilePath = [(NSDictionary *)file stringForKey:@"ftpFilePath"];
                            if ([ftpFilePath ix_isString] && [(NSString *)ftpFilePath length]) {
                                count++;
                            }
                        }
                    }
                }
            }
        }
        if (tmpArr.count) {
            [IXBORequestMgr shareInstance].fileArr = [tmpArr mutableCopy];
        }
        if (count == 3) {
            [IXBORequestMgr shareInstance].uploadedCustomerFile = YES;
        } else {
            [IXBORequestMgr shareInstance].uploadedCustomerFile = NO;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.contentTV reloadData];
    [self fillCustomerFile];//刷新提示信息
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray *)self.titleArr[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * option = self.titleArr[indexPath.section][indexPath.row];
    if (SameString(option, LocalizedString(@"头像"))) {
        return kAvatarH;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == self.titleArr.count - 1) {
        return 10.f;
    }
    return 0.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [tableView sectionHeaderShadow];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == self.titleArr.count -1) {
        return [tableView bottomShadow];
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString    * option = self.titleArr[indexPath.section][indexPath.row];
    if (SameString(option, LocalizedString(@"头像"))) {
        IXAcntAvatarCell    * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntAvatarCell class])];
        cell.titleLab.text = option;
        
        if ([IXBORequestMgr shareInstance].headUrl.length) {
            [cell.avatarV sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                            placeholderImage:cell.avatarV.image];
        }
        
        return cell;
    }
    else if (SameString(option, LocalizedString(@"姓名")) ||
             SameString(option, LocalizedString(@"手机")) ||
             SameString(option, LocalizedString(@"邮箱"))) {
        
        IXSupplementStyle2Cell *cell = [tableView dequeueReusableCellWithIdentifier:
                                        NSStringFromClass([IXSupplementStyle2Cell class])];
        cell.tipTitle = option;
        cell.bankAccountTF.tag = indexPath.row;
        cell.bankAccountTF.font = PF_MEDI(13);
        cell.tipTitleLbl.dk_textColorPicker = DKCellTitleColor;
        cell.bankAccountTF.userInteractionEnabled = NO;
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (SameString(option, LocalizedString(@"姓名"))) {
            if (_isEdit) {
                if (![IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName.length) {
                    cell.bankAccountTF.userInteractionEnabled = YES;
                    cell.bankAccountTF.placeholder = LocalizedString(@"未填写");
                    cell.bankAccountTF.delegate = self;
                    _nameTf = cell.bankAccountTF;
                } else {
                    cell.bankAccountTF.userInteractionEnabled = NO;
                }
            }
            cell.bankAccountTF.text = [IXDataProcessTools dealWithNil:self.accountInfo.nickName];
        }
        else if (SameString(option, LocalizedString(@"手机"))) {
            NSString    * phone = [IXBORequestMgr shareInstance].userInfo.detailInfo.mobilePhone;
            if (!phone.length) {
                phone = LocalizedString(@"未绑定");
            } else {
                if (phone.length > 4) {
                    NSString    * subA = [phone substringToIndex:3];
                    NSString    * subB = [phone substringFromIndex:phone.length - 4];
                    phone = [NSString stringWithFormat:@"%@****%@",subA,subB];
                }
                cell.bankAccountTF.font = RO_REGU(15);
            }
            cell.bankAccountTF.text = phone;
        }
        else if (SameString(option, LocalizedString(@"邮箱"))) {
            NSString    * email = [IXBORequestMgr shareInstance].userInfo.detailInfo.email;
            if (!email.length) {
                email = LocalizedString(@"未绑定");
            } else {
                NSRange range = [email rangeOfString:@"@"];
                NSString    * subA = [email substringToIndex:MAX(range.location - 1, 0)];
                if (subA.length > 3) {
                    subA = [subA substringToIndex:3];
                }
                NSString    * subB = [email substringFromIndex:range.location];
                email = [NSString stringWithFormat:@"%@***%@",subA,subB];
            }
            cell.bankAccountTF.text = email;
            cell.bankAccountTF.keyboardType = UIKeyboardTypeEmailAddress;
        }
        
        return cell;
    }
    else if (SameString(option, [self getCerType])) {
        IXAcntIdCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                        NSStringFromClass([IXAcntIdCell class])];
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //证件信息
        NSString    * idStr = self.accountInfo.idCard;
        if (idStr.length > 10) {
            idStr = [NSString stringWithFormat:@"%@********%@",
                     [idStr substringToIndex:8],
                     [idStr substringFromIndex:idStr.length - 2]];
        }
        if (_isEdit && ![IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber.length) {
            weakself;
            cell.acntIdB = ^{
                if (![IXBORequestMgr shareInstance].cerArr || [IXBORequestMgr shareInstance].cerArr.count == 0) {
                    [weakSelf idDocumentList];
                } else {
                    if (!self.cerItemArr.count) {
                        [weakSelf dealWithIdDocumentData];
                    }
                    [weakSelf.view endEditing:YES];
                    [weakSelf.chooseCer show];
                }
            };
            cell.cardTF.placeholder = LocalizedString(@"未填写");
            cell.cardTF.userInteractionEnabled = YES;
            cell.title.userInteractionEnabled = YES;
            cell.cardTF.delegate = self;
            _cardTF = cell.cardTF;
        } else {
            cell.cardTF.userInteractionEnabled = NO;
            cell.title.userInteractionEnabled = NO;
        }
        [cell loadData:[self getCerType] content:idStr];
        return cell;
    }
    else if ([option isEqualToString:LocalizedString(@"居住信息")]) {
        IXAcntInfoAreaCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                    NSStringFromClass([IXAcntInfoAreaCell class])];
        [cell reloadUIWithTitle:option
                           icon:@"acntInfo_arrow_right" isHiddenLine:YES];
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (SameString(option, LocalizedString(@"我的银行卡")) || SameString(option, LocalizedString(@"证明文件"))) {
        IXAcntInfoBankCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                    NSStringFromClass([IXAcntInfoBankCell class])];
        [cell reloadUIWithTitle:self.titleArr[indexPath.section][indexPath.row]
                           icon:@"acntInfo_arrow_right" isHiddenLine:NO];
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.line.hidden = YES;
        return cell;
    }
    
    return [UITableViewCell new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString    * option = self.titleArr[indexPath.section][indexPath.row];
    
    if (SameString(option, LocalizedString(@"头像"))) {
        [self.chooseCata show];
    }
    else if (SameString(option, LocalizedString(@"居住信息"))) {
        IXAcntAddressVC * vc = [[IXAcntAddressVC alloc] init];
        vc.accountInfo = self.accountInfo;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (SameString(option, LocalizedString(@"我的银行卡"))) {
        IXAcntBankListVC *vc = [[IXAcntBankListVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (SameString(option, LocalizedString(@"证明文件"))) {
        IXAcntCerFileVC *vc = [[IXAcntCerFileVC alloc] init];
        vc.isUpload = [IXBORequestMgr shareInstance].uploadedCustomerFile;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _nameTf) {
        self.accountInfo.nickName = textField.text;
    }
    if (textField == _cardTF) {
        self.accountInfo.idCard = textField.text;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return  [textField resignFirstResponder];
}

#pragma mark -
#pragma mark - btn action

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)selectCerFile
{
    IXAcntStep7VC *VC = [[IXAcntStep7VC alloc] init];
    [self.navigationController pushViewController:VC animated:YES];
}

- (void)responseToPhoto
{
    [self.chooseCata show];
}

#pragma mark -
#pragma mark - image picker

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    CGSize imagesize = image.size;
//    if (imagesize.width > 750) {
//        imagesize = CGSizeMake(750, 750);
//    }
//    image = [self imageWithImage:image scaledToSize:imagesize];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self sumbmitHeadPhoto:image];
    
    //调用摄像头后会导致常亮失效，再此处处理屏幕常亮问题
    [IXUserDefaultM dealScreenNormalLight];
    [IXUserDefaultM dealStatusBarWithAlbumState:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //调用摄像头后会导致常亮失效，再此处处理屏幕常亮问题
    [IXUserDefaultM dealScreenNormalLight];
    [IXUserDefaultM dealStatusBarWithAlbumState:NO];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//对图片尺寸进行压缩--
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}

//重设头像
- (void)resetHeadImage
{
    [self.contentTV reloadData];
}

- (void)idDocumentList
{
    [SVProgressHUD showWithStatus:@"处理中..."];
    weakself;
    [IXBORequestMgr acc_dictChildListWithParam:@"IdDocument"
                                    WithResult:^(BOOL success,
                                                 NSString *errCode,
                                                 NSString *errStr,
                                                 id obj)
     {
         [SVProgressHUD dismiss];
         if (success) {
             if (obj && [obj ix_isArray]) {
                 [IXBORequestMgr processCredentialInfo:obj];
                 if ([IXBORequestMgr shareInstance].cerArr.count > 0) {
                     [weakSelf dealWithIdDocumentData];
                     [weakSelf.view endEditing:YES];
                     [weakSelf.chooseCer show];
//                     if (self.acntModel.flag) {
//                         [self dealWithCerType];
//                     } else {
//                         [self.chooseCata show];
//                     }
                 }
             }
         }
     }];
}

- (void)postModifyUserInfo
{
    
    IXIdDocumentNumberModel *numberModel = [[IXIdDocumentNumberModel alloc] init];
    numberModel.value = self.accountInfo.idCard;
    
    IXAcntInfoM * model = [[IXAcntInfoM alloc] init];
    model.address = self.accountInfo.address;
    model.chineseName = self.accountInfo.nickName;
    if ([self.accountInfo.email containsString:@"@"]) {
        model.email = self.accountInfo.email;
    } else {
        model.email = @"";
    }
    model.idDocumentNumber = numberModel;
    model.idDocumentNumberMd5 = self.accountInfo.idCard;
    if ([IXUserDefaultM isChina]) {
        model.nationality = @"ISO_3166_156";
    }
    model.idDocument = self.accountInfo.idDocument;
    model.province = self.accountInfo.province;
    model.city = self.accountInfo.city;
    model.postalCode = self.accountInfo.postCode;
    model.platform = PLATFORM;
    model.gts2CustomerId = self.accountInfo.gts2CustomerId;
    
    NSDictionary *paramDic = @{
                               @"customer":[model toJSONString],
                               @"gts2CustomerId":self.accountInfo.gts2CustomerId
                               };
    [SVProgressHUD showWithStatus:LocalizedString(@"数据提交中...")];
    weakself;
    [IXBORequestMgr acc_modifyUserInfoWithParam:paramDic
                                         result:^(BOOL success,
                                                  NSString *errCode,
                                                  NSString *errStr, id obj)
     {
         if (success) {
             [SVProgressHUD showSuccessWithStatus:LocalizedString(@"用户信息修改成功")];
             [IXBORequestMgr loginBo:nil];
             weakSelf.isEdit = !weakSelf.isEdit;
             if (self.accountInfo.idDocument.length && self.accountInfo.idCard.length) {
                 self.navigationItem.rightBarButtonItem = nil;
             } else {
                 [weakSelf addDismissItemWithTitle:LocalizedString(@"编辑") target:self action:@selector(editAction)];
             }
             [weakSelf.contentTV reloadData];
         } else {
             if ([errCode length]) {
                 [SVProgressHUD showErrorWithStatus:errStr];
             } else {
                 [SVProgressHUD showErrorWithStatus:LocalizedString(@"用户信息修改失败")];
             }
         }
     }];
}


#pragma mark -
#pragma mark - request

//提交头像图片
- (void)sumbmitHeadPhoto:(UIImage *)image {
    if (image) {
        [SVProgressHUD showWithStatus:LocalizedString(@"上传头像图片中...")];
        
        [IXBORequestMgr acc_updateUserAvatar:image sid:self.accountInfo.sid
                                    complete:^(BOOL updateSuccess, NSString *errStr)
        {
            if (updateSuccess) {
                [SVProgressHUD dismiss];
                [IXBORequestMgr loginBo:nil];
            }else{
                [SVProgressHUD showErrorWithStatus:errStr];
            }
        }];
    }
}

- (void)fillCustomerFile
{
    if (![IXBORequestMgr shareInstance].uploadedCustomerFile) {
        if (!self.tipBtn) {
            self.tipBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
            self.tipBtn.backgroundColor = AutoNightColor(0xfcf1f2, 0x2c2b35);
            [self.tipBtn addTarget:self action:@selector(selectCerFile) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.tipBtn];
            
            UIImageView *tipImg = [[UIImageView alloc] initWithFrame:CGRectMake( 10, 10, 20, 20)];
            [tipImg setImage:AutoNightImageNamed(@"common_tip")];
            [self.tipBtn addSubview:tipImg];
            
            UILabel *tipContent = [IXUtils createLblWithFrame:CGRectMake( 37, 0, kScreenWidth - 47, 40)
                                                     WithFont:PF_MEDI(13)
                                                    WithAlign:NSTextAlignmentLeft
                                                   wTextColor:0xff4653
                                                   dTextColor:0xff4d2d];
            tipContent.text = LocalizedString(@"请提交个人证明文件");
            [self.tipBtn addSubview:tipContent];
            
            UIImageView *accImg = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - 10 - 20, 10, 20, 20)];
            [accImg setImage:GET_IMAGE_NAME(@"broadSide_arrow_right")];
            [self.tipBtn addSubview:accImg];
            
            UILabel *tipName = [IXUtils createLblWithFrame:CGRectMake( 0, 0, kScreenWidth - 10 - 20, 40)
                                                  WithFont:PF_MEDI(13)
                                                 WithAlign:NSTextAlignmentRight
                                                wTextColor:0x4c6072
                                                dTextColor:0xe9e9ea];
            tipName.text = LocalizedString(@"立即提交");
            [self.tipBtn addSubview:tipName];
        }
        
        [self.view addSubview:self.tipBtn];
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.contentTV.frame = CGRectMake(0, 30, kScreenWidth, kScreenHeight - kNavbarHeight - 40);
        } completion:nil];
    } else {
        //上传文件后返回清空提示
        if (self.tipBtn) {
            [self.tipBtn removeFromSuperview];
            self.tipBtn = nil;
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.contentTV.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight);
            } completion:nil];
        }
    }
}

#pragma mark -
#pragma mark - lazy loading

- (NSArray *)titleArr
{
    _titleArr = @[@[LocalizedString(@"头像"),
                    LocalizedString(@"姓名"),
                    [self getCerType],
                    LocalizedString(@"手机"),
                    LocalizedString(@"邮箱"),
                    LocalizedString(@"居住信息")],
                  @[
                    LocalizedString(@"我的银行卡"),
                    LocalizedString(@"证明文件")]];
    return _titleArr;
}

- (void)dealWithIdDocumentData
{
    for (int i = 0; i < [IXBORequestMgr shareInstance].cerArr.count; i++) {
        IXCredentialM   * model = (IXCredentialM *)([IXBORequestMgr shareInstance].cerArr[i]);
        NSString *name = nil;
        if ( [BoLanKey isEqualToString:NAMEEN] ) {
            name = model.nameEN;
        } else if ( [BoLanKey isEqualToString:NAMECN] ) {
            name = model.nameCN;
        } else {
            name =  model.nameTW;
        }
        if (name.length) {
            [self.cerItemArr addObject:name];
        } else {
            [self.cerItemArr addObject:@""];
        }
    }
}

- (NSString *)getCerType
{
    NSArray * cerArr = [IXBORequestMgr shareInstance].cerArr;
    
    for (int i = 0; i < cerArr.count; i++) {
        IXCredentialM   * m = cerArr[i];
        if (m && [m.code isEqualToString:self.accountInfo.idDocument]) {
            return [m localizedName];
        }
    }
    if (self.accountInfo.idCard.length) {
        return LocalizedString(@"身份证");
    }
    return LocalizedString(@"其他");
}

- (NSMutableArray *)titleItemArr
{
    if (!_titleItemArr) {
        _titleItemArr = [@[LocalizedString(@"选择相册"),LocalizedString(@"选择相机")] mutableCopy];
    }
    return _titleItemArr;
}

- (NSMutableArray *)cerItemArr {
    if (!_cerItemArr) {
        _cerItemArr = [[NSMutableArray alloc] init];
    }
    return _cerItemArr;
}

- (IXOpenChooseContentView *)chooseCata
{
    if ( !_chooseCata ) {
        _chooseCata = [[IXOpenChooseContentView alloc] initWithDataSource:self.titleItemArr WithSelectedRow:^(NSInteger row) {
            UIImagePickerController * picker = [[UIImagePickerController alloc] init];
            switch (row) {
                case 0:{
                    if ([IXAppUtil albumEnable]) {
                        [IXUserDefaultM dealStatusBarWithAlbumState:YES];
                        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                        picker.delegate = self;
                        picker.allowsEditing = NO;
                        picker.navigationBar.translucent = NO;
                        [self presentViewController:picker animated:YES completion:nil];
                    }
                    break;
                }
                case 1:{
                    if ([IXAppUtil cameraEnable]) {
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        picker.delegate = self;
                        picker.allowsEditing = NO;
                        [self presentViewController:picker animated:YES completion:nil];
                    }
                    break;
                }
                default:
                    break;
            }
        }];
    }
    return _chooseCata;
}

- (IXOpenChooseContentView *)chooseCer
{
    if ( !_chooseCer ) {
        weakself;
        _chooseCer = [[IXOpenChooseContentView alloc] initWithDataSource:self.cerItemArr WithSelectedRow:^(NSInteger row) {
            if ( row >= 0 && row < [IXBORequestMgr shareInstance].cerArr.count ) {
                if ( [BoLanKey isEqualToString:NAMEEN] ) {
                    weakSelf.accountInfo.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).nameEN;
                } else if ( [BoLanKey isEqualToString:NAMECN] ) {
                    weakSelf.accountInfo.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).nameCN;
                } else {
                    weakSelf.accountInfo.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).nameTW;
                }
                weakSelf.accountInfo.idDocument = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).code;
                [weakSelf.contentTV reloadData];
            }
        }];
    }
    return _chooseCer;
}

- (IXTouchTableV *)contentTV
{
    if (!_contentTV) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 0,
                                                                     kScreenWidth,
                                                                     kScreenHeight - kNavbarHeight)
                                                    style:UITableViewStyleGrouped];
        _contentTV.dk_backgroundColorPicker = DKTableHeaderColor;
        _contentTV.separatorStyle = UITableViewCellSelectionStyleNone;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.tableFooterView = [UIView new];
        _contentTV.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        _contentTV.sectionFooterHeight = 0.f;
        [self.view addSubview:_contentTV];
        
        [_contentTV registerClass:[IXSupplementStyle2Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSupplementStyle2Cell class])];
        [_contentTV registerClass:[IXAcntInfoBankCell class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntInfoBankCell class])];
        [_contentTV registerClass:[IXAcntInfoAreaCell class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntInfoAreaCell class])];
        [_contentTV registerClass:[IXAcntAvatarCell class] forCellReuseIdentifier:NSStringFromClass([IXAcntAvatarCell class])];
        [_contentTV registerClass:[IXAcntIdCell class] forCellReuseIdentifier:NSStringFromClass([IXAcntIdCell class])];
    }
    
    return _contentTV;
}

- (AccountInfoM *)accountInfo
{
    if (!_accountInfo) {
        IXUserInfoM * boM = [IXBORequestMgr shareInstance].userInfo;
        
        _accountInfo = [AccountInfoM new];
        _accountInfo.gts2CustomerId = [NSString stringWithFormat:@"%lu",boM.gts2CustomerId];
        _accountInfo.provinceName   = boM.detailInfo.province;
        _accountInfo.cityName       = boM.detailInfo.city;
        _accountInfo.regionStr  = @"";
        _accountInfo.photoUrl   = boM.headImgUrl;
        _accountInfo.nickName   = boM.detailInfo.chineseName;
        _accountInfo.idCard     = boM.detailInfo.idDocumentNumber;
        _accountInfo.phoneNum   = boM.detailInfo.mobilePhone;
        _accountInfo.email      = boM.detailInfo.email;
        _accountInfo.province   = boM.detailInfo.province;
        _accountInfo.city       = boM.detailInfo.city;
        _accountInfo.postCode   = boM.detailInfo.postalCode;
        _accountInfo.address    = boM.detailInfo.address;
        _accountInfo.idDocument = boM.detailInfo.idDocument;
        if (!_accountInfo.idDocument.length) {
            _accountInfo.idDocument = @"0111";
        }
        _accountInfo.sid        = boM.sid;
    }
    
    return _accountInfo;
}

@end

