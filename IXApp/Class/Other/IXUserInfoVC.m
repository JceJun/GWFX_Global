//
//  IXUserInfoVC.m
//  IXApp
//
//  Created by Larry on 2018/6/27.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXUserInfoVC.h"
#import "IXCommonItemView.h"
#import "IXBORequestMgr+Asset.h"
#import "UIImageView+WebCache.h"
#import "IXOpenChooseContentView.h"
#import "IXOpenTipV.h"
#import "IXDocumentsVC.h"
#import "IXAddressDocVC.h"
#import "UIKit+Block.h"
#import "IXAcntAddressVC.h"
#import "IXAcntBankListVC.h"
#import "IXBankListVC.h"
@interface IXUserInfoVC ()

@end

@implementation IXUserInfoVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self checkFile];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LocalizedString(@"用户资料");
    [self addBackItem];
    weakself;
    
    IXCommonItemView *item_0 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Avatar" rText:nil topLineStyle:TopLineStyleShadow bottomLineStyle:BottomStyleNone];
    [item_0 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
    }];
    [item_0 addEnteranceArrow];
    
    UIImageView *img_head = [UIImageView new];
    img_head.dk_imagePicker = DKImageNames(@"openAccount_avatar", @"openAccount_avatar_D");
    if ([IXBORequestMgr shareInstance].headUrl.length) {
        [img_head sd_setImageWithURL:[NSURL URLWithString:[IXBORequestMgr shareInstance].headUrl]
                        placeholderImage:img_head.image];
    }
    [item_0 addSubview:img_head];
    [img_head makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-30);
        make.centerY.equalTo(0);
        make.size.equalTo(CGSizeMake(30, 30));
    }];
    
    NSString *userName = [IXDataProcessTools dealWithNil:[IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName];
    IXCommonItemView *item_1 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Name" rText:userName topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleNone];
    [item_1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_0.bottom);
    }];
    
    NSString *idCard = [IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber;
    if (idCard.length > 10) {
        idCard = [NSString stringWithFormat:@"%@********%@",
                 [idCard substringToIndex:8],
                 [idCard substringFromIndex:idCard.length - 2]];
    }
    IXCommonItemView *item_2 = [IXCommonItemView makeViewInSuperView:self.view lText:@"ID Card" rText:idCard topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleNone];
    [item_2 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_1.bottom);
    }];
    
    
    NSString *phone = [IXBORequestMgr shareInstance].userInfo.detailInfo.mobilePhone;
    if (!phone.length) {
        phone = LocalizedString(@"未绑定");
    }
    IXCommonItemView *item_3 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Phone" rText:phone topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleNone];
    [item_3 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_2.bottom);
    }];
    
    NSString *email = [IXBORequestMgr shareInstance].userInfo.detailInfo.email;
    if (!email.length) {
        email = LocalizedString(@"未绑定");
    } else {
        if ([email containsString:@"@"]) {
            NSRange range = [email rangeOfString:@"@"];
            NSString    * subA = [email substringToIndex:MAX(range.location - 1, 0)];
            if (subA.length > 3) {
                subA = [subA substringToIndex:3];
            }
            NSString    * subB = [email substringFromIndex:range.location];
            email = [NSString stringWithFormat:@"%@***%@",subA,subB];
        }
    }
    IXCommonItemView *item_4 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Email" rText:email topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleNone];
    [item_4 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_3.bottom);
    }];
    
    IXCommonItemView *item_5 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Address Information" rText:nil topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleShadow];
    [item_5 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_4.bottom);
    }];
    [item_5 addEnteranceArrow];
    [item_5 block_whenTapped:^(UIView *aView) {
        [self gotoNext:@"Address Information"];
    }];
    
//    IXCommonItemView *item_6 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Bank Information for Deposit" rText:nil topLineStyle:TopLineStyleShadow bottomLineStyle:BottomStyleNone];
//    [item_6 makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(item_5.bottom).offset(10);
//    }];
//    [item_6 addEnteranceArrow];
//    [item_6 block_whenTapped:^(UIView *aView) {
//        [weakSelf gotoNext:@"Bank Information for Deposit"];
//    }];
    
    IXCommonItemView *item_7 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Documents" rText:nil topLineStyle:TopLineStyleNone bottomLineStyle:BottomStyleShadow];
    [item_7 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_5.bottom);
    }];
    [item_7 addEnteranceArrow];
    [item_7 block_whenTapped:^(UIView *aView) {
        [weakSelf gotoNext:@"Documents"];
    }];
    
    [self request];
}

- (void)request{
    [SVProgressHUD show];
    
    // 更新银行卡信息&文件
    [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString *errStr) {
        if (success) {
            [IXBORequestMgr b_getCustomerFiles:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
                if (success) {
                    [SVProgressHUD dismiss];
                    [self checkFile];
                }else{
                    [SVProgressHUD showInfoWithStatus:LocalizedString(@"获取客户文件信息失败")];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }else if (errStr.length){
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
        }
    }];
}

- (void)checkFile{
    if (![IXBORequestMgr shareInstance].uploadedCustomerFile) {
        // 显示提交文件提示
        [IXOpenTipV showInView:self.view lText:LocalizedString(@"请提交个人证明文件") rText:LocalizedString(@"立即提交") block:^{
            [self gotoNext:@"Documents"];
        }];
    }else{
         // 隐藏提交文件提示
        [IXOpenTipV hideInView:self.view];
    }

    
    
}

- (void)gotoNext:(NSString *)title{
    if ([title isEqualToString:@"Address Information"]) {
        IXAcntAddressVC * vc = [[IXAcntAddressVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"Bank Information for Deposit"]){
        IXAcntBankListVC *vc = [[IXAcntBankListVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"Documents"]){
        IXDocumentsVC *vc = [IXDocumentsVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


/*
(
{
    companyId = 1;
    createDate =     {
        date = 8;
        day = 2;
        hours = 13;
        minutes = 59;
        month = 4;
        seconds = 11;
        time = 1525759151491;
        timezoneOffset = "-480";
        year = 118;
    };
    createIp = "192.168.35.108";
    createUser = testApi;
    customerNumber = 10002559;
    fileInstruction = "";
    fileName = "1525762190.png";
    filePath = "1/20180508/20180508065215_20180508_a3cb00ae";
    fileType = "FILE_TYPE_IDCARD_FRONT";
    ftpFilePath = "http://192.168.35.108:10850/public/ftp_upload/fx_sit/event/1525762335770a41a00af.png";
    id = 1097156;
    proposalStatus = 2;
    updateDate =     {
        date = 15;
        day = 2;
        hours = 16;
        minutes = 56;
        month = 4;
        seconds = 2;
        time = 1526374562691;
        timezoneOffset = "-480";
        year = 118;
    };
    updateIp = "192.168.35.108";
    updateUser = ixadmin;
    versionNo = 4;
},
{
    companyId = 1;
    createDate =     {
        date = 8;
        day = 2;
        hours = 13;
        minutes = 59;
        month = 4;
        seconds = 11;
        time = 1525759151699;
        timezoneOffset = "-480";
        year = 118;
    };
    createIp = "192.168.35.108";
    createUser = testApi;
    customerNumber = 10002559;
    fileInstruction = "";
    fileName = "1525762191.png";
    filePath = "1/20180508/20180508065234_20180508_ecd300b0";
    fileType = "FILE_TYPE_IDCARD_BACK";
    ftpFilePath = "http://192.168.35.108:10850/public/ftp_upload/fx_sit/event/1525762354452ed1400b1.png";
    id = 1097157;
    proposalStatus = 2;
    updateDate =     {
        date = 15;
        day = 2;
        hours = 16;
        minutes = 56;
        month = 4;
        seconds = 2;
        time = 1526374562691;
        timezoneOffset = "-480";
        year = 118;
    };
    updateIp = "192.168.35.108";
    updateUser = ixadmin;
    versionNo = 4;
},
{
    companyId = 1;
    createDate =     {
        date = 8;
        day = 2;
        hours = 13;
        minutes = 59;
        month = 4;
        seconds = 11;
        time = 1525759151794;
        timezoneOffset = "-480";
        year = 118;
    };
    createIp = "192.168.35.108";
    createUser = testApi;
    customerNumber = 10002559;
    fileInstruction = "";
    fileName = "1525762192.png";
    filePath = "1/20180508/20180508065243_20180508_10fe00b2";
    fileType = "FILE_TYPE_ADDRESS";
    ftpFilePath = "http://192.168.35.108:10850/public/ftp_upload/fx_sit/event/1525762363672111800b3.png";
    id = 1097158;
    proposalStatus = 2;
    updateDate =     {
        date = 15;
        day = 2;
        hours = 16;
        minutes = 56;
        month = 4;
        seconds = 2;
        time = 1526374562691;
        timezoneOffset = "-480";
        year = 118;
    };
    updateIp = "192.168.35.108";
    updateUser = ixadmin;
    versionNo = 4;
},
{
    companyId = 1;
    createDate =     {
        date = 8;
        day = 2;
        hours = 13;
        minutes = 59;
        month = 4;
        seconds = 11;
        time = 1525759151803;
        timezoneOffset = "-480";
        year = 118;
    };
    createIp = "192.168.35.108";
    createUser = testApi;
    customerNumber = 10002559;
    fileInstruction = "";
    fileName = "";
    filePath = "";
    fileType = "FILE_TYPE_BANK_1";
    ftpFilePath = "";
    id = 1097159;
    proposalStatus = 0;
    updateDate =     {
        date = 22;
        day = 5;
        hours = 17;
        minutes = 34;
        month = 5;
        seconds = 5;
        time = 1529660045636;
        timezoneOffset = "-480";
        year = 118;
    };
    updateIp = "192.168.35.108";
    updateUser = testApi;
    versionNo = 7;
},
{
    companyId = 1;
    createDate =     {
        date = 8;
        day = 2;
        hours = 13;
        minutes = 59;
        month = 4;
        seconds = 11;
        time = 1525759151814;
        timezoneOffset = "-480";
        year = 118;
    };
    createIp = "192.168.35.108";
    createUser = testApi;
    customerNumber = 10002559;
    fileInstruction = "";
    fileName = "";
    filePath = "";
    fileType = "FILE_TYPE_BANK_2";
    ftpFilePath = "";
    id = 1097160;
    proposalStatus = 0;
    updateDate =     {
        date = 22;
        day = 5;
        hours = 17;
        minutes = 34;
        month = 5;
        seconds = 2;
        time = 1529660042273;
        timezoneOffset = "-480";
        year = 118;
    };
    updateIp = "192.168.35.108";
    updateUser = testApi;
    versionNo = 5;
},
{
    companyId = 1;
    createDate =     {
        date = 8;
        day = 2;
        hours = 13;
        minutes = 59;
        month = 4;
        seconds = 11;
        time = 1525759151826;
        timezoneOffset = "-480";
        year = 118;
    };
    createIp = "192.168.35.108";
    createUser = testApi;
    customerNumber = 10002559;
    fileInstruction = "";
    fileName = "";
    filePath = "";
    fileType = "FILE_TYPE_BANK_3";
    ftpFilePath = "";
    id = 1097161;
    proposalStatus = 0;
    updateDate =     {
        date = 15;
        day = 2;
        hours = 16;
        minutes = 56;
        month = 4;
        seconds = 2;
        time = 1526374562691;
        timezoneOffset = "-480";
        year = 118;
    };
    updateIp = "192.168.35.108";
    updateUser = ixadmin;
    versionNo = 4;
}
 )
*/




/*
{
    "expireTime": 1530156567630,
    "headImgUrl": "",
    "user": {
        "result": {
            "errors": [
            
            ],
            "multiResult": false,
            "newRet": "",
            "ok": true,
            "code": "OK",
            "context": {
                "FO_LAST_LOGIN_TIME": {
                    "year": 118,
                    "time": 1530156261913,
                    "day": 4,
                    "date": 28,
                    "hours": 11,
                    "month": 5,
                    "nanos": 913000000,
                    "seconds": 21,
                    "minutes": 24,
                    "timezoneOffset": -480
                }
            },
            "result": {
                "firstDepositDate": {
                    "time": 1525760584461,
                    "day": 2,
                    "date": 8,
                    "hours": 14,
                    "month": 4,
                    "seconds": 4,
                    "timezoneOffset": -480,
                    "minutes": 23,
                    "year": 118
                },
                "updateDate": {
                    "time": 1528438074456,
                    "day": 5,
                    "date": 8,
                    "hours": 14,
                    "month": 5,
                    "seconds": 54,
                    "timezoneOffset": -480,
                    "minutes": 7,
                    "year": 118
                },
                "tradeIxCustomerId": 441713,
                "migrateType": "",
                "firstActiveAccountNo": "",
                "autoUnlockTimes": 0,
                "firstActivePlatform": "",
                "iamUsCitizen": false,
                "id": 51036,
                "accounts": "",
                "address": "思考的机会的话",
                "loginname": "",
                "mobilePhone": "",
                "successLoginCount": 0,
                "isDeal": false,
                "lastLoginTime": {
                    "time": 1530156261913,
                    "day": 4,
                    "date": 28,
                    "hours": 11,
                    "month": 5,
                    "seconds": 21,
                    "timezoneOffset": -480,
                    "minutes": 24,
                    "year": 118
                },
                "customerInfoBankWithdrawParams": [
                
                ],
                "stOpenAccountDate ": {
                    "time": 1525760458506,
                    "day": 2,
                    "date": 8,
                    "hours": 14,
                    "month": 4,
                    "seconds": 58,
                    "timezoneOffset": -480,
                    "minutes": 20,
                    "year": 118
                },
                "createDate": {
                    "time": 1525759150771,
                    "day": 2,
                    "date": 8,
                    "hours": 13,
                    "month": 4,
                    "seconds": 10,
                    "timezoneOffset": -480,
                    "minutes": 59,
                    "year": 118
                },
                "customerInfoServiceParams": [
                
                ],
                "firstWithdrawGts2AccountId": 0,
                "platforms": "",
                "goldenComment": 0,
                "addressConsistent": false,
                "homePhonePrefix": "",
                "firstDepositGts2AccountId": 100007493,
                "openId": "",
                "isAgreement": 0,
                "homePhone": "",
                "remark": "",
                "errorCount": 0,
                "chineseName": "陈俊",
                "clientStatus": "",
                "englishFirstName": "",
                "firstWithdrawDate": null,
                "isCashOut": false,
                "countryOther": "",
                "isLogin": false,
                "isOpenAccount": false,
                "maxCreditCount": 0,
                "customerCategory": "",
                "nationalityOther": "",
                "createIp": "192.168.35.108",
                "tagInfoParams": [
                
                ],
                "updateUser": "testApi",
                "accountCurrencys": "",
                "lastFailLoginTime": null,
                "deleted": false,
                "province": "",
                "isCashIn": false,
                "idDocumentCountryOther": "",
                "openCustomerDate": {
                    "time": 1525759150715,
                    "day": 2,
                    "date": 8,
                    "hours": 13,
                    "month": 4,
                    "seconds": 10,
                    "timezoneOffset": -480,
                    "minutes": 59,
                    "year": 118
                },
                "failLoginCount": 0,
                "firstWithdrawPlatform": "",
                "customerGroupId": 0,
                "messageLang": "en_US",
                "companyId": 1,
                "createUser": "testApi",
                "encodePasswordType": "GTS2",
                "idDocumentNumber": {
                    "mask": "",
                    "value": "360403199006190073",
                    "encrypted": "07mV6ArDNqR\/vp4IYeZ\/nIhcshVl+3S0L4zCru8Lx9I="
                },
                "idDocument": "0111",
                "customerInfoFileParams": [{
                    "id": 1097156,
                    "versionNo": 4,
                    "fileName": "1525762190.png",
                    "createDate": {
                        "time": 1525759151491,
                        "day": 2,
                        "date": 8,
                        "hours": 13,
                        "month": 4,
                        "seconds": 11,
                        "timezoneOffset": -480,
                        "minutes": 59,
                        "year": 118
                    },
                    "createIp": "192.168.35.108",
                    "fileInstruction": "",
                    "ftpFilePath": "http:\/\/192.168.35.108:10850\/public\/ftp_upload\/fx_sit\/event\/1525762335770a41a00af.png",
                    "createUser": "testApi",
                    "updateIp": "192.168.35.108",
                    "customerNumber": 10002559,
                    "updateDate": {
                        "time": 1526374562691,
                        "day": 2,
                        "date": 15,
                        "hours": 16,
                        "month": 4,
                        "seconds": 2,
                        "timezoneOffset": -480,
                        "minutes": 56,
                        "year": 118
                    },
                    "filePath": "1\/20180508\/20180508065215_20180508_a3cb00ae",
                    "fileType": "FILE_TYPE_IDCARD_FRONT",
                    "proposalStatus": "",
                    "updateUser": "ixadmin",
                    "companyId": 1
                },
                                           {
                                               "id": 1097157,
                                               "versionNo": 4,
                                               "fileName": "1525762191.png",
                                               "createDate": {
                                                   "time": 1525759151699,
                                                   "day": 2,
                                                   "date": 8,
                                                   "hours": 13,
                                                   "month": 4,
                                                   "seconds": 11,
                                                   "timezoneOffset": -480,
                                                   "minutes": 59,
                                                   "year": 118
                                               },
                                               "createIp": "192.168.35.108",
                                               "fileInstruction": "",
                                               "ftpFilePath": "http:\/\/192.168.35.108:10850\/public\/ftp_upload\/fx_sit\/event\/1525762354452ed1400b1.png",
                                               "createUser": "testApi",
                                               "updateIp": "192.168.35.108",
                                               "customerNumber": 10002559,
                                               "updateDate": {
                                                   "time": 1526374562691,
                                                   "day": 2,
                                                   "date": 15,
                                                   "hours": 16,
                                                   "month": 4,
                                                   "seconds": 2,
                                                   "timezoneOffset": -480,
                                                   "minutes": 56,
                                                   "year": 118
                                               },
                                               "filePath": "1\/20180508\/20180508065234_20180508_ecd300b0",
                                               "fileType": "FILE_TYPE_IDCARD_BACK",
                                               "proposalStatus": "",
                                               "updateUser": "ixadmin",
                                               "companyId": 1
                                           },
                                           {
                                               "id": 1097158,
                                               "versionNo": 4,
                                               "fileName": "1525762192.png",
                                               "createDate": {
                                                   "time": 1525759151794,
                                                   "day": 2,
                                                   "date": 8,
                                                   "hours": 13,
                                                   "month": 4,
                                                   "seconds": 11,
                                                   "timezoneOffset": -480,
                                                   "minutes": 59,
                                                   "year": 118
                                               },
                                               "createIp": "192.168.35.108",
                                               "fileInstruction": "",
                                               "ftpFilePath": "http:\/\/192.168.35.108:10850\/public\/ftp_upload\/fx_sit\/event\/1525762363672111800b3.png",
                                               "createUser": "testApi",
                                               "updateIp": "192.168.35.108",
                                               "customerNumber": 10002559,
                                               "updateDate": {
                                                   "time": 1526374562691,
                                                   "day": 2,
                                                   "date": 15,
                                                   "hours": 16,
                                                   "month": 4,
                                                   "seconds": 2,
                                                   "timezoneOffset": -480,
                                                   "minutes": 56,
                                                   "year": 118
                                               },
                                               "filePath": "1\/20180508\/20180508065243_20180508_10fe00b2",
                                               "fileType": "FILE_TYPE_ADDRESS",
                                               "proposalStatus": "",
                                               "updateUser": "ixadmin",
                                               "companyId": 1
                                           },
                                           {
                                               "id": 1097159,
                                               "versionNo": 7,
                                               "fileName": "",
                                               "createDate": {
                                                   "time": 1525759151803,
                                                   "day": 2,
                                                   "date": 8,
                                                   "hours": 13,
                                                   "month": 4,
                                                   "seconds": 11,
                                                   "timezoneOffset": -480,
                                                   "minutes": 59,
                                                   "year": 118
                                               },
                                               "createIp": "192.168.35.108",
                                               "fileInstruction": "",
                                               "ftpFilePath": "",
                                               "createUser": "testApi",
                                               "updateIp": "192.168.35.108",
                                               "customerNumber": 10002559,
                                               "updateDate": {
                                                   "time": 1529660045636,
                                                   "day": 5,
                                                   "date": 22,
                                                   "hours": 17,
                                                   "month": 5,
                                                   "seconds": 5,
                                                   "timezoneOffset": -480,
                                                   "minutes": 34,
                                                   "year": 118
                                               },
                                               "filePath": "",
                                               "fileType": "FILE_TYPE_BANK_1",
                                               "proposalStatus": "",
                                               "updateUser": "testApi",
                                               "companyId": 1
                                           },
                                           {
                                               "id": 1097160,
                                               "versionNo": 5,
                                               "fileName": "",
                                               "createDate": {
                                                   "time": 1525759151814,
                                                   "day": 2,
                                                   "date": 8,
                                                   "hours": 13,
                                                   "month": 4,
                                                   "seconds": 11,
                                                   "timezoneOffset": -480,
                                                   "minutes": 59,
                                                   "year": 118
                                               },
                                               "createIp": "192.168.35.108",
                                               "fileInstruction": "",
                                               "ftpFilePath": "",
                                               "createUser": "testApi",
                                               "updateIp": "192.168.35.108",
                                               "customerNumber": 10002559,
                                               "updateDate": {
                                                   "time": 1529660042273,
                                                   "day": 5,
                                                   "date": 22,
                                                   "hours": 17,
                                                   "month": 5,
                                                   "seconds": 2,
                                                   "timezoneOffset": -480,
                                                   "minutes": 34,
                                                   "year": 118
                                               },
                                               "filePath": "",
                                               "fileType": "FILE_TYPE_BANK_2",
                                               "proposalStatus": "",
                                               "updateUser": "testApi",
                                               "companyId": 1
                                           },
                                           {
                                               "id": 1097161,
                                               "versionNo": 4,
                                               "fileName": "",
                                               "createDate": {
                                                   "time": 1525759151826,
                                                   "day": 2,
                                                   "date": 8,
                                                   "hours": 13,
                                                   "month": 4,
                                                   "seconds": 11,
                                                   "timezoneOffset": -480,
                                                   "minutes": 59,
                                                   "year": 118
                                               },
                                               "createIp": "192.168.35.108",
                                               "fileInstruction": "",
                                               "ftpFilePath": "",
                                               "createUser": "testApi",
                                               "updateIp": "192.168.35.108",
                                               "customerNumber": 10002559,
                                               "updateDate": {
                                                   "time": 1526374562691,
                                                   "day": 2,
                                                   "date": 15,
                                                   "hours": 16,
                                                   "month": 4,
                                                   "seconds": 2,
                                                   "timezoneOffset": -480,
                                                   "minutes": 56,
                                                   "year": 118
                                               },
                                               "filePath": "",
                                               "fileType": "FILE_TYPE_BANK_3",
                                               "proposalStatus": "",
                                               "updateUser": "ixadmin",
                                               "companyId": 1
                                           }
                                           ],
                "idDocumentCountry": "",
                "dateOfBirth": null,
                "accountInfoParams": [
                
                ],
                "englishLastName": "",
                "openSource": "",
                "accessToken": "",
                "customerInfoBankParams": [{
                    "createDate": {
                        "time": 1525759151040,
                        "day": 2,
                        "date": 8,
                        "hours": 13,
                        "month": 4,
                        "seconds": 11,
                        "timezoneOffset": -480,
                        "minutes": 59,
                        "year": 118
                    },
                    "createIp": "192.168.35.108",
                    "bankPayMethod": "",
                    "createUser": "testApi",
                    "internationalRemittanceCode": "",
                    "bankAddress": "",
                    "updateIp": "192.168.35.108",
                    "versionNo": 10,
                    "updateDate": {
                        "time": 1529660045651,
                        "day": 5,
                        "date": 22,
                        "hours": 17,
                        "month": 5,
                        "seconds": 5,
                        "timezoneOffset": -480,
                        "minutes": 34,
                        "year": 118
                    },
                    "updateUser": "testApi",
                    "bankAccountName": "",
                    "bankBranch": "",
                    "bankCountryOther": "",
                    "bankProvince": "",
                    "companyId": 1,
                    "bankCity": "",
                    "bankAccountType": "",
                    "bankOrder": 1,
                    "customerNumber": 10002559,
                    "id": 152895,
                    "bankAccountNumberX": {
                        "mask": "",
                        "value": "",
                        "encrypted": ""
                    },
                    "bankCountry": "",
                    "bankAccountNumber": "",
                    "bankCityCode": "",
                    "bankCountryCode": "",
                    "bankCurrency": "USD",
                    "bankName": "",
                    "bankOther": "",
                    "bankProvinceCode": "",
                    "bank": "",
                    "bankAccountNumberMd5": ""
                },
                                           {
                                               "createDate": {
                                                   "time": 1525759151462,
                                                   "day": 2,
                                                   "date": 8,
                                                   "hours": 13,
                                                   "month": 4,
                                                   "seconds": 11,
                                                   "timezoneOffset": -480,
                                                   "minutes": 59,
                                                   "year": 118
                                               },
                                               "createIp": "192.168.35.108",
                                               "bankPayMethod": "",
                                               "createUser": "testApi",
                                               "internationalRemittanceCode": "",
                                               "bankAddress": "",
                                               "updateIp": "192.168.35.108",
                                               "versionNo": 10,
                                               "updateDate": {
                                                   "time": 1529660045651,
                                                   "day": 5,
                                                   "date": 22,
                                                   "hours": 17,
                                                   "month": 5,
                                                   "seconds": 5,
                                                   "timezoneOffset": -480,
                                                   "minutes": 34,
                                                   "year": 118
                                               },
                                               "updateUser": "testApi",
                                               "bankAccountName": "",
                                               "bankBranch": "",
                                               "bankCountryOther": "",
                                               "bankProvince": "",
                                               "companyId": 1,
                                               "bankCity": "",
                                               "bankAccountType": "",
                                               "bankOrder": 2,
                                               "customerNumber": 10002559,
                                               "id": 152896,
                                               "bankAccountNumberX": {
                                                   "mask": "",
                                                   "value": "",
                                                   "encrypted": ""
                                               },
                                               "bankCountry": "",
                                               "bankAccountNumber": "",
                                               "bankCityCode": "",
                                               "bankCountryCode": "",
                                               "bankCurrency": "",
                                               "bankName": "",
                                               "bankOther": "",
                                               "bankProvinceCode": "",
                                               "bank": "",
                                               "bankAccountNumberMd5": ""
                                           },
                                           {
                                               "createDate": {
                                                   "time": 1525759151475,
                                                   "day": 2,
                                                   "date": 8,
                                                   "hours": 13,
                                                   "month": 4,
                                                   "seconds": 11,
                                                   "timezoneOffset": -480,
                                                   "minutes": 59,
                                                   "year": 118
                                               },
                                               "createIp": "192.168.35.108",
                                               "bankPayMethod": "",
                                               "createUser": "testApi",
                                               "internationalRemittanceCode": "",
                                               "bankAddress": "",
                                               "updateIp": "192.168.35.108",
                                               "versionNo": 10,
                                               "updateDate": {
                                                   "time": 1529660045651,
                                                   "day": 5,
                                                   "date": 22,
                                                   "hours": 17,
                                                   "month": 5,
                                                   "seconds": 5,
                                                   "timezoneOffset": -480,
                                                   "minutes": 34,
                                                   "year": 118
                                               },
                                               "updateUser": "testApi",
                                               "bankAccountName": "",
                                               "bankBranch": "",
                                               "bankCountryOther": "",
                                               "bankProvince": "",
                                               "companyId": 1,
                                               "bankCity": "",
                                               "bankAccountType": "",
                                               "bankOrder": 3,
                                               "customerNumber": 10002559,
                                               "id": 152897,
                                               "bankAccountNumberX": {
                                                   "mask": "",
                                                   "value": "",
                                                   "encrypted": ""
                                               },
                                               "bankCountry": "",
                                               "bankAccountNumber": "",
                                               "bankCityCode": "",
                                               "bankCountryCode": "",
                                               "bankCurrency": "",
                                               "bankName": "",
                                               "bankOther": "",
                                               "bankProvinceCode": "",
                                               "bank": "",
                                               "bankAccountNumberMd5": ""
                                           }
                                           ],
                "idDocumentOther": "",
                "openFrom": "WEBSITE_IOS",
                "informationFrom": "other",
                "versionNo": 6,
                "email": "271200229@qq.com",
                "agentCode": "",
                "iamNotAmerican": false,
                "postalCode": "518000",
                "updateIp": "192.168.35.108",
                "recommenderId": "",
                "isDemo": false,
                "nationality": "ISO_3166_360",
                "openPlatform": "",
                "firstActiveDate": null,
                "idDocumentNumberMd5": "md5(a9c77070fb43255fc1f6f3584c4cad59)",
                "lastLoginIp": "",
                "password": "md5(65cd7c13440eb5415da563d461e3c6d5)",
                "verifyMethod": "",
                "city": "",
                "ibornInUs": false,
                "title": "",
                "appMarket": "",
                "country": "",
                "enable": 1,
                "firstDepositPlatform": "IX",
                "createTime": null,
                "gts2CustomerId": 100005149,
                "unionId": "",
                "isMigrateData": false,
                "mobilePhonePrefix": "",
                "customerNumber": 10002559,
                "emailService": ""
            },
            "newComment": "",
            "results": [
            
            ],
            "error": null,
            "fieldErrors": {
                
            }
        },
        "code": "SUCCESS"
    },
    "userType": "real",
    "code": "success",
    "token": "49786facf9b1fab6acb01003f5cbf137980f94ee80371fc90dc6d3fb86be4987acd90c23fca99fe4cc4eb037c9ebee21",
    "sid": "bsMdZlgERqyVa0FvlebsgDD6U0Cdde17bX31BH96YVs=",
    "userId": "10002559",
    "createTime": "2018-06-28T03:24:27.617Z",
    "lang": "zh_TW",
    "gts2CustomerId": 100005149,
    "platTypeKey": "webui",
    "companyId": "1",
    "systemTime": 1530156267638
}
*/

@end
