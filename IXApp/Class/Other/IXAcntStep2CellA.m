//
//  IXAcntStep2CellA.m
//  IXApp
//
//  Created by Magee on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep2CellA.h"

@interface IXAcntStep2CellA ()

@property (nonatomic,strong)UIImageView *icon;

@end

@implementation IXAcntStep2CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    [self desc];
    [self icon];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(15, 12, 70, 20)];
        _title.font = PF_MEDI(13);
        _title.text = LocalizedString(@"所在地区");
        _title.textAlignment = NSTextAlignmentLeft;
        _title.dk_textColorPicker = DKCellTitleColor;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(99, 12, kScreenWidth - (7.5 + 20 + 99), 20)];
        _desc.font = PF_MEDI(13);
        _desc.text = LocalizedString(@"地区信息");
        _desc.textAlignment = NSTextAlignmentLeft;
        _desc.dk_textColorPicker = DKCellContentColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (7.5 + 16), 15, 7.5, 13.5)];
        _icon.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (void)loadUIWithDesc:(NSString *)desc
{
    if (!desc || desc.length == 0) {
        self.desc.text = LocalizedString(@"地区信息");
    } else {
        self.desc.text = desc;
    }
}

- (void)setHiddenIcon:(BOOL)hidden
{
    self.icon.hidden = hidden;
}
@end
