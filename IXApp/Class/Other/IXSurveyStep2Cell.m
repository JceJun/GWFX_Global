//
//  IXSurveyStep2Cell.m
//  IXApp
//
//  Created by Evn on 2017/10/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyStep2Cell.h"

@interface IXSurveyStep2Cell()<UITextViewDelegate>

@end

@implementation IXSurveyStep2Cell

- (PHTextView *)textV
{
    if (!_textV) {
        CGRect rect = CGRectMake(14.5, 12, kScreenWidth - 29, 20);
        _textV = [[PHTextView alloc] initWithFrame:rect];
        _textV.font = PF_MEDI(13);
        _textV.delegate = self;
        _textV.contentOffset = CGPointMake(0, 5);
        _textV.backgroundColor = [UIColor clearColor];
        _textV.dk_textColorPicker = DKCellTitleColor;
        _textV.placeholderColor = AutoNightColor(0xe2e9f1, 0x303b4d);
        [self.contentView addSubview:_textV];
    }
    return _textV;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewValue:tag:)]) {
        [self.delegate textViewValue:textView.text tag:textView.tag];
    }
}

- (void)loadUIWithTextView:(NSString *)text
               numberLines:(NSInteger)numberLines
{
    [self bringSubviewToFront:self.textV];
    self.textV.text = text;
    self.textV.frame = CGRectMake(14.5, 12, kScreenWidth - 29, 20*numberLines);
}

@end
