//
//  GesturePwdVC.m
//  IXApp
//
//  Created by Seven on 2017/3/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "GesturePwdVC.h"
#import "GesturePWdData.h"
#import "GesturePwdView.h"
#import "GesturePwdHeader.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "IXAlertVC.h"
#import <LocalAuthentication/LocalAuthentication.h>

#define kImgViewHW  52
#define KimgViewBorderColor     UIColorFromHex(0xd4d4d4,1).CGColor

@interface GesturePwdVC ()<ExpendableAlartViewDelegate>

@property (nonatomic, copy) NSString    * titleText;
@property (nonatomic, copy) NSString    * oldPwd;

@end

@implementation GesturePwdVC


/**
 初始化方法
 
 @param state 页面类型
 @return obj
 */
- (instancetype)initWithState:(GesturePwdVCState)state{
    if (self = [super init]) {
        _state = state;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x262f3e);
    
    //验证手势密码是否已经设置
    if (![GesturePWdData hasSeted]) {
        if (_state == GesturePwdVCStateConfirm && self.reloginBlock) {
            self.reloginBlock();
        }
    }else{
        _oldPwd = [GesturePWdData currentPwd];
    }
    
    switch (_state) {
        case GesturePwdVCStateSetting:{
            [self createSettingPage];
            self.titleText = [IXLocalizationModel getLocalizationWordByKey:@"设置手势密码"];
            break;
        }
        case GesturePwdVCStateConfirm:{
            [self createConfirmPage];
            self.titleText = nil;
            break;
        }
    }
    
    if (self.navigationController) {
        self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                                  sel:@selector(onGoback)];
    }else{
        [self createTitleBar];
    }
}


#pragma mark -
#pragma mark - UI

//设置手势密码
- (void)createSettingPage
{
    GesturePwdView  * gesrureV = [self createGestureViewWith:_state];
    gesrureV.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [self.view addSubview:gesrureV];
}

//手势登录验证
- (void)createConfirmPage
{
    //头像
    UIImageView * imgView = [self createImgView];
    imgView.frame = CGRectMake((kScreenWidth - 52)/2, 71, 52, 52);
    imgView.image = AutoNightImageNamed(@"openAccount_avatar");
    [self.view addSubview:imgView];
    
    NSString    * iconUrl = [[NSUserDefaults standardUserDefaults] objectForKey:kIXHeadIcon];
    
    [imgView sd_setImageWithURL:[NSURL URLWithString:iconUrl]
               placeholderImage:AutoNightImageNamed(@"openAccount_avatar")
                        options:SDWebImageAllowInvalidSSLCertificates];
    
    //昵称
    UILabel * nickNameLab = [IXUtils createLblWithFrame:CGRectMake(0, 71+52+9, kScreenWidth, 20)
                                               WithFont:RO_REGU(16)
                                              WithAlign:NSTextAlignmentCenter
                                             wTextColor:0xffffff
                                             dTextColor:0xffffff];
    nickNameLab.text = [IXLocalizationModel getLocalizationWordByKey:[NSString stringWithFormat:@"%@",
                                                                      [IXUserInfoMgr shareInstance].userLogInfo.user.name]];
    [self.view addSubview:nickNameLab];
    
    //账号
    UILabel * accountLab = [IXUtils createLblWithFrame:CGRectMake(0, 71+52+9+20+7, kScreenWidth, 15)
                                              WithFont:RO_REGU(15)
                                             WithAlign:NSTextAlignmentCenter
                                            wTextColor:0x99abba
                                            dTextColor:0x99abba];
    accountLab.text = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"账户"),
                       [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo];;
    [self.view addSubview:accountLab];
    
    //手势视图
    GesturePwdView  * gestureV = [self createGestureViewWith:_state];
    gestureV.frame = CGRectMake(0, 71+52+9+20+15+36, kScreenWidth, kScreenHeight);
    gestureV.gestureMode = GesturePWDModeConfirmPwdMode;
    [self.view addSubview:gestureV];
    
    UIButton * reLoginBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth/2-80,
                                                                       71+52+9+20+7+15+40+15+36+70*3+40*2+60,
                                                                       160,
                                                                       35)];
    [reLoginBtn addTarget:self action:@selector(reloginAction:) forControlEvents:UIControlEventTouchUpInside];
    [reLoginBtn setTitle:LocalizedString(@"忘记手势密码") forState:UIControlStateNormal];
    reLoginBtn.backgroundColor = [UIColor clearColor];
    reLoginBtn.titleLabel.font = PF_MEDI(13);
    [self.view addSubview:reLoginBtn];
}


- (void)createTitleBar
{
    if (self.titleText.length) {
        UILabel * titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, kScreenWidth, 40)];
        titleLab.textAlignment = NSTextAlignmentCenter;
        titleLab.text = self.titleText;
        
        titleLab.backgroundColor = [UIColor clearColor];
        titleLab.textColor = AutoNightColor(0xffffff, 0xe9e9ea);
        titleLab.font = PF_MEDI(15);
        [self.view addSubview:titleLab];
    }
    
    if (_state != GesturePwdVCStateConfirm){
        UIButton    * cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, 25, 50, 35)];
        [cancelBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [cancelBtn setTitle:[IXLocalizationModel getLocalizationWordByKey:@"取消"] forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = PF_MEDI(13);
        [cancelBtn setTitleColor:AutoNightColor(0xffffff, 0xe9e9ea) forState:UIControlStateNormal];
        [self.view addSubview:cancelBtn];
    }
}


#pragma mark -
#pragma mark - btn Action

- (void)reloginAction:(UIButton *)btn
{
    if (self.reloginBlock) {
        self.reloginBlock();
    }
}

- (void)cancelBtnAction{
    if (self.cancelSetting) {
        self.cancelSetting();
    }
    if (_oldPwd.length) {
        [GesturePWdData setPwd:_oldPwd];
    }
    [self onGoback];
}

- (void)onGoback
{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark -
#pragma mark - other
//手势视图
- (GesturePwdView *)createGestureViewWith:(GesturePwdVCState)state
{
    GesturePwdView  * v = nil;
    
    switch (state) {
        case GesturePwdVCStateSetting:
            v = [[GesturePwdView alloc] initWithModel:GesturePWDModeSetPwdMode];
            break;
        case GesturePwdVCStateConfirm:
            v = [[GesturePwdView alloc] initWithModel:GesturePWDModeConfirmPwdMode];
            break;
    }
    
    weakself;
    v.block = ^(NSString *pswString){
        if (weakSelf.state != GesturePwdVCStateConfirm) {
            IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"手势密码设置成功")
                                                     message:nil
                                                 cancelTitle:nil
                                                  sureTitles:LocalizedString(@"好")];
            VC.index = 0;
            VC.expendAbleAlartViewDelegate = self;
            [VC showView];
        }else{
            [weakSelf onGoback];
        }
    };
    v.confirmFailBlock = ^(){
        IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"手势密码登录失败")
                                                 message:LocalizedString(@"您的手势输入有误，请重新登录。")
                                             cancelTitle:nil
                                              sureTitles:LocalizedString(@"重新登录")];
        VC.index = 0;
        VC.expendAbleAlartViewDelegate = self;
        [VC showView];
    };
    
    return v;
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (self.state == GesturePwdVCStateConfirm){
        if (self.reloginBlock) {
            self.reloginBlock();
        }
    }else{
        [self onGoback];
    }
    
    return YES;
}

- (UIImageView *)createImgView
{
    UIImageView * v = [[UIImageView alloc] initWithFrame:CGRectZero];
    v.contentMode = UIViewContentModeScaleAspectFill;
    v.backgroundColor = [UIColor clearColor];
    v.layer.cornerRadius = kImgViewHW / 2;
    v.clipsToBounds = YES;
    v.layer.borderWidth = 1.f;
    v.layer.borderColor = KimgViewBorderColor;
    return v;
}

@end
