//
//  IXAddressDocVC.m
//  IXApp
//
//  Created by Larry on 2018/6/28.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXAddressDocVC.h"
#import "YYText.h"
#import "IXDocumentView.h"
#import "UIKit+Block.h"
#import "IXOpenChooseContentView.h"
#import "UIImageView+WebCache.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Asset.h"

@interface IXAddressDocVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)UIView *mainView;
@property(nonatomic,strong)UILabel *lb_title;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)IXDocumentView * imgV1;
@property(nonatomic,strong)NSDictionary *imgInfo1;
@property(nonatomic,strong)NSDictionary *uploadInfo1;
@property(nonatomic,strong)UIButton * nextBtn;
@end

@implementation IXAddressDocVC

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.bounces = NO;
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
            make.width.equalTo(kScreenWidth);
        }];
    }
    return _scrollView;
}

- (UILabel *)lb_title{
    if (!_lb_title) {
        _lb_title = [UILabel new];
        _lb_title.font = ROBOT_FONT(11);
        _lb_title.dk_textColorPicker = DKCellTitleColor;
        _lb_title.textAlignment = NSTextAlignmentCenter;
        _lb_title.numberOfLines = 0;
        [self.scrollView addSubview:_lb_title];
        [_lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.right.equalTo(-15);
            make.top.equalTo(20);
            make.centerX.equalTo(0);
        }];
        _lb_title.text = @"Utility bills(water,electricity and gas bills),bank account (debit,credit) monthly statements in three month and ID address will be accepted. Name on the address documents is supposed to consist with your real name and address must be your current address.";
    }
    return _lb_title;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Address Document";
    [self showProgressWithAimStep:3 totalStep:4 originY:0];
    [self addBackItem];
    [self addCancelItem];
    
    [self scrollView];
    [self lb_title];
    [self mainView];
    
    [self loadUI];
    [self reloadSubmitUI];
}

- (void)loadUI{
    weakself;
    NSArray *fileArr = [IXBORequestMgr shareInstance].fileArr;
    [fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *info = obj;
        if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_ADDRESS"]) {
            // 前图
            weakSelf.imgInfo1 = info;
        }
    }];
    
    if (_imgInfo1) {
        if ([_imgInfo1[@"ftpFilePath"] length]) {
            [_imgV1.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo1[@"ftpFilePath"]]];
            _imgV1.imageV.hidden = NO;
        }
        _imgV1.status = _imgInfo1[@"proposalStatus"];
    }
    
}

- (UIView *)mainView{
    if (!_mainView) {
        _mainView = [UIView new];
        _mainView.dk_backgroundColorPicker = DKNavBarColor;
        [self.scrollView addSubview:_mainView];
        [_mainView makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(kScreenWidth);
            make.top.equalTo(_lb_title.bottom).offset(20);
            make.bottom.equalTo(0);
            make.centerX.equalTo(0);
        }];
        
        UILabel *lb_tip = [UILabel new];
        lb_tip.font = ROBOT_FONT(13);
        lb_tip.dk_textColorPicker = DKCellTitleColor;
        lb_tip.text = @"Please upload your address documents photo";
        [_mainView addSubview:lb_tip];
        [lb_tip makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(15);
        }];
        
        weakself;
        IXDocumentView * imgV1 = [IXDocumentView makeDocument:@"Upload Address Documents" superView:_mainView clearUploadInfo:^{
            weakSelf.uploadInfo1 = nil;
            [weakSelf reloadSubmitUI];
        }] ;
        
        [imgV1 makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lb_tip.bottom).offset(15);
        }];
        [imgV1 block_whenTapped:^(UIView *aView) {
            [weakSelf pickOne];
        }];
        _imgV1 = imgV1;
        
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        
        UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
        dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
        
        UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.titleLabel.font = PF_REGU(15);
        [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
        [nextBtn setTitle:@"Submission" forState:UIControlStateNormal];
        [_mainView addSubview:nextBtn];
        
        [nextBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.right.equalTo(-15);
            make.top.equalTo(imgV1.bottom).offset(15);
            make.height.equalTo(44);
            make.bottom.equalTo(-30);
        }];
        [nextBtn block_touchUpInside:^(UIButton *aButton) {
            [weakSelf submitInfo];
        }];
        _nextBtn = nextBtn;
        _nextBtn.enabled = NO;
    }
    return _mainView;
}


- (void)gotoNext:(NSString *)title{
    
}


- (void)pickOne
{
    NSArray * arr =   @[
                        LocalizedString(@"选择相机"),
                        LocalizedString(@"选择相册"),
                        ];
    
    weakself;
    IXOpenChooseContentView * chooseV = [[IXOpenChooseContentView alloc] initWithDataSource:arr
                                                                            WithSelectedRow:^(NSInteger row)
                                         {
                                             if (row == 0){
                                                 [weakSelf takePhoto];
                                             }else{
                                                 [weakSelf joinAlbum];
                                             }
                                         }];
    [chooseV show];
}

- (void)takePhoto{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    // 调用照相机
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)joinAlbum{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    // 访问相册
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = NO;
        imagePicker.navigationBar.translucent = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

// 代理方法（当照完相或者选择完照片会调用这个代理）
#pragma mark UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    UIImage *image = nil;
    NSString *imageId;
    NSString *imageType;
    
    if ([mediaType isEqualToString:@"public.image"])
    {
        image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        NSURL *url = [info objectForKey: @"UIImagePickerControllerReferenceURL"];
        imageId = [url absoluteString];
        NSArray *tmpArray = [imageId componentsSeparatedByString: @"="];
        imageId = [tmpArray objectAtIndex:1];
        imageType = [tmpArray objectAtIndex:2];
        NSArray *tmpArray2 = [imageId componentsSeparatedByString: @"&"];
        imageId = [tmpArray2 objectAtIndex:0];
    }
    
    // 把file和fileName赋值
    if (imageId == nil) {
        double s = [NSDate timeIntervalSinceReferenceDate];
        imageId = [NSString stringWithFormat:@"%f", s];
        imageType = @"PNG";
    }
    //    NSString *fileName = [[NSString alloc] initWithFormat: @"%@.%@", imageId, imageType];
    //    NSData *img_data = UIImageJPEGRepresentation(image, 0.5);
    
    weakself;
    [picker dismissViewControllerAnimated:YES completion:^{
        weakSelf.imgV1.imageV.image = image;
        weakSelf.imgV1.imageV.hidden = NO;
        weakSelf.imgV1.lb_status.hidden = YES;
        [weakSelf submitFile:image];
    }];
}

- (void)submitFile:(UIImage *)image{
    [SVProgressHUD showWithStatus:@"Uploading.."];
    [IXBORequestMgr b_appUploadFile:image rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            _uploadInfo1 = obj;
            _imgV1.dismissV.hidden = NO;
            [self reloadSubmitUI];
            [SVProgressHUD dismiss];
        }else{
            [_imgV1.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo1[@"ftpFilePath"]]];
            _imgV1.imageV.hidden = NO;
            _imgV1.status = _imgInfo1[@"proposalStatus"];
            _imgV1.lb_status.hidden = NO;
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}

- (void)submitInfo{
    NSString *gts2CustomerId =  [@([IXBORequestMgr shareInstance].userInfo.gts2CustomerId) stringValue];
    NSMutableArray *files = [NSMutableArray array];
    if (_uploadInfo1) {
        NSDictionary *dic1 = @{
                               @"gts2CustomerId":gts2CustomerId,
                               @"fileType":_imgInfo1[@"fileType"],
                               @"id":_imgInfo1[@"id"], // 列编号,登录接口用户信息中返回
                               
                               @"fileName":_uploadInfo1[@"fileName"],
                               @"filePath":_uploadInfo1[@"fileStorePath"], // 上传接口返回的：fileStorePath
                               @"ftpFilePath":_uploadInfo1[@"webFilePath"], // 上传接口返回的：webFilePath
                               };
        [files addObject:dic1];
    }
    [IXBORequestMgr b_updateCustomerFiles:files rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [SVProgressHUD showSuccessWithStatus:@"Success"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}

- (void)reloadSubmitUI{
    if (_uploadInfo1) {
        _nextBtn.enabled = YES;
    }else{
        _nextBtn.enabled = NO;
    }
}


@end
