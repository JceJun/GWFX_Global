//
//  IXAcntStep1CellB.h
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

@protocol IXAcntStep1CellBDelegate <NSObject>

@optional
- (void)clickInfo:(UIButton *)btn;
- (void)textValue:(NSString *)value tag:(NSInteger)tag;

@end
@interface IXAcntStep1CellB : UITableViewCell
@property (nonatomic, strong) UILabel       * desc;
@property (nonatomic, strong) IXTextField   * textF;
@property (nonatomic, assign) id <IXAcntStep1CellBDelegate> delegate;

- (void)loadUIWithDesc:(NSString *)desc
                  text:(NSString *)title
           placeHolder:(NSString *)holder
          keyboardType:(UIKeyboardType)type
       secureTextEntry:(BOOL)Entry
             iconImage:(UIImage *)image
          isHiddenIcon:(BOOL)isHidden
               iconTag:(NSInteger)tag;

- (void)setTextFieldEnable:(BOOL)enable;
@end
