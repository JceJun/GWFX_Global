//
//  IXModifyPwdCell.m
//  IXApp
//
//  Created by Bob on 2017/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXModifyPwdCell.h"
#import "IXUserDefaultM.h"

@interface IXModifyPwdCell ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) UITextField *contentTF;

@end

@implementation IXModifyPwdCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        //手动添加分割线
        UIImageView *sepImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 43, kScreenWidth, 1)];
        sepImg.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:sepImg];
    }
    return self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //不接受输入空格
    if ( [string isEqualToString:@" "] ) {
        if ( self.errorMsg ) {
            self.errorMsg(NONESPACECHAR);
        }
        return NO;
    }
    
    //密码长度不能大于18
    if ( range.location > PWDMAXLENGTH  ) {
        if ( self.errorMsg ) {
            self.errorMsg(PWDMAXCHAR);
        }
        return NO;
    }
 
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self dealKeyboardHidden];
}

- (void)dealKeyboardHidden
{
    [_contentTF resignFirstResponder];
    if ( _textContent ) {
        _textContent(_contentTF.text);
    }
}

- (void)cancelKeyboardHidden{
    [_contentTF resignFirstResponder];
}

- (void)setTipName:(NSString *)tipName
{
    if ( tipName ) {
        _tipName = tipName;
        self.tipLbl.text = tipName;

        if ( [tipName isEqualToString:LocalizedString(@"原密码")] ) {
            self.contentTF.placeholder = LocalizedString(@"请输入原密码");
        }else if ( [tipName isEqualToString:LocalizedString(@"新密码")] ){
            self.contentTF.placeholder = LocalizedString(@"请输入新密码");
        }else if ( [tipName isEqualToString:LocalizedString(@"确认新密码")] ){
            self.contentTF.placeholder = LocalizedString(@"请重复输入");
        }
        
        UIColor * placeHolderColor = nil;
        if ([IXUserDefaultM isNightMode]){
            placeHolderColor = UIColorHexFromRGB(0x303b4d);
        }else{
            placeHolderColor = SearchPlaceholderColor;
        }
        [self.contentTF setValue:placeHolderColor forKeyPath:@"_placeholderLabel.textColor"];
    }
}


- (void)setTextFieldInputAccessoryView
{
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    [topView setBarStyle:UIBarStyleDefault];
    topView.backgroundColor = MarketCellSepColor;
    
    UIBarButtonItem * spaceBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:self
                                                                              action:nil];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:PF_MEDI(13)];
    [cancelBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(15, 9, 60, 25);
    [cancelBtn addTarget:self action:@selector(cancelKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBtnItem = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneBtn setTitle:LocalizedString(@"完成") forState:UIControlStateNormal];
    [doneBtn.titleLabel setFont:PF_MEDI(13)];
    [doneBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    doneBtn.frame = CGRectMake(kScreenWidth - 90, 9, 80, 25);
    [doneBtn addTarget:self action:@selector(dealKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneBtnItem = [[UIBarButtonItem alloc]initWithCustomView:doneBtn];
    NSArray * buttonsArray = [NSArray arrayWithObjects:cancelBtnItem,spaceBtn,doneBtnItem,nil];
    [topView setItems:buttonsArray];
    [_contentTF setInputAccessoryView:topView];
    [_contentTF setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_contentTF setAutocapitalizationType:UITextAutocapitalizationTypeNone];
}


- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, 84, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentLeft
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea];
        [self.contentView addSubview:_tipLbl];
    }
    return _tipLbl;
}

- (UITextField *)contentTF
{
    if ( !_contentTF ) {
        _contentTF = [[UITextField alloc] initWithFrame:CGRectMake( 99, 7, kScreenWidth - 89, 30)];
        _contentTF.font = PF_MEDI(13);
        _contentTF.delegate = self;
        _contentTF.secureTextEntry = YES;
        _contentTF.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
        [self.contentView addSubview:_contentTF];
        [self setTextFieldInputAccessoryView];
    }
    return _contentTF;
}

@end
