//
//  IXAboutVC.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAboutVC.h"
#import "IXAboutCell.h"
#import "IXProductInfoVC.h"
#import "IXProInfoVC.h"
#import "IXProtocolVC.h"
#import "IXAboutVC+SetServerTime.h"

@interface IXAboutVC ()<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) NSArray *titleArr;

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) UIView *headView;

@property (nonatomic, strong) UITextView *cntT;
@property (nonatomic, strong) UIView *lineView;
@end


@implementation IXAboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
        [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"关于我们");
    _titleArr = @[LocalizedString(@"产品介绍"),
                  LocalizedString(@"条款协议")];

    self.view.dk_backgroundColorPicker = DKTableColor;
    [self.view addSubview:self.tableV];

//    if( [CompanyToken isEqualToString:@"IX"] ){
//        [self.view addSubview:self.tableV];
//    }else if ([CompanyToken isEqualToString:@"CFIX"]){
//        [self footerView];
//        [self.view addSubview:self.cntT];
//        [self.view addSubview:self.lineView];
//    }
#if DEBUG
#warning 修改服务器时间供测试使用
    [self addLongTap];
#else
#endif
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UITextView *)cntT
{
    if (!_cntT) {
        _cntT = [[UITextView alloc] initWithFrame:CGRectMake(15, 116*kScreenWidth/375 + 60 + kNavbarHeight, kScreenWidth - 15*2, 165)];
        _cntT.text = LocalizedString(@"      创富金融有限公司作为创富集团 (CF GROUPLTD) 旗下子公司。\n      自2008年成立，创富金融获取新西兰 FSP 金融监管牌照，为新西兰政府认可的金融服务商 (FSP牌照号码：FSP507506)，提供专业网上股指期货、外汇、贵金属和商品等多元化交易。");
        _cntT.font = PF_MEDI(13);
        _cntT.textColor = MarketSymbolNameColor;
        _cntT.backgroundColor = [UIColor clearColor];
        _cntT.editable = NO;
        _cntT.scrollEnabled = NO;
    }
    
    return _cntT;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, GetView_MaxY(self.cntT), kScreenWidth, 1)];
        _lineView.backgroundColor = CellSepLineColor;
    }
    return _lineView;
}

#pragma mark - lazy load
- (UITableView *)tableV
{
    if (!_tableV){
        CGRect frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-kNavbarHeight);
        _tableV = [[UITableView alloc] initWithFrame:frame];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.backgroundColor = [UIColor clearColor];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.tableHeaderView = self.headView;
    }
    return _tableV;
}

- (UIView *)headView
{
    if ( !_headView ) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, 0, 130)];
        _headView.dk_backgroundColorPicker = DKTableColor;
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 60)/2, 30, 60, 70)];
        float height = 116*kScreenWidth/375;
        float width = height;
        [imgV dk_setImagePicker:DKImageNames(@"common_logo", @"common_logo_D")];
        imgV.frame = CGRectMake((kScreenWidth - width)/2, 30, width, height);
        [_headView addSubview:imgV];
        
        _headView.frame = CGRectMake(0, kNavbarHeight, 0, height + 60);
       
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(_headView) - kLineHeight, kScreenWidth, kLineHeight)];
        lineView.dk_backgroundColorPicker = DKLineColor;
        [_headView addSubview:lineView];
    }
    return _headView;
}

- (void)footerView
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, 0, 130)];
    v.backgroundColor = UIColorHexFromRGB(0xfafcfe);
    
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 60)/2, 30, 60, 70)];
    float height = 116*kScreenWidth/375;
    float width = height;
    [imgV dk_setImagePicker:DKImageNames(@"common_logo", @"common_logo_D")];
    imgV.frame = CGRectMake((kScreenWidth - width)/2, 30, width, height);
    [v addSubview:imgV];
    
    v.frame = CGRectMake(0, kNavbarHeight, 0, height + 60);
   
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(v) - kLineHeight, kScreenWidth, kLineHeight)];
    lineView.backgroundColor = MarketCellSepColor;
    [v addSubview:lineView];
   
    [self.view addSubview:v];
}


#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"IXAboutCell";
    IXAboutCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXAboutCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    [cell reloadUIData:_titleArr[indexPath.row]];
    cell.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController    * vc = nil;
    if (indexPath.row == 0) {
//        vc = [[IXProductInfoVC alloc] init];
        vc = [[IXProInfoVC alloc] init];
    }else{
        vc = [[IXProtocolVC alloc] init];
    }
    [self.navigationController pushViewController:vc animated:YES];
}


@end
