//
//  IXAcntInfoBankCell.h
//  IXApp
//
//  Created by Evn on 2017/4/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@interface IXAcntInfoBankCell : IXClickTableVCell

@property (nonatomic,strong)UIView *line;

- (void)reloadUIWithTitle:(NSString *)title
                     icon:(NSString *)icon
             isHiddenLine:(BOOL)hidden;

@end
