//
//  IXEvaluationM.h
//  IXApp
//
//  Created by Evn on 2017/11/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXQuestionM.h"

typedef NS_ENUM(int, IXQuestionType) {
    IXQuestionTypeBlankFill = 0,//填空
    IXQuestionTypeSingleSel,    //单选
    IXQuestionTypeMutableSel,   //多选
};

#pragma mark - IXEvaluationM

@interface IXEvaluationM : NSObject

@property (nonatomic, assign) BOOL answered __deprecated_msg("弃用");
@property (nonatomic, copy) NSString    * statement;
@property (nonatomic, copy) NSString    * summary;
@property (nonatomic, copy) NSString    * title;
@property (nonatomic, assign) int       type;
@property (nonatomic, assign) int       status;
@property (nonatomic, assign) NSInteger id_p;

@property (nonatomic, strong) NSMutableArray    * pageList;  //问题分页数组，每一页一个数组
@property (nonatomic, readonly) NSMutableArray    * questionList; //问题数组，未分类

+ (instancetype)evaluateWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

@end

#pragma mark - IXEvaSectionM

/** 风险测评分类 */
@interface IXEvaSectionM : NSObject

@property (nonatomic, copy) NSString    * title;
@property (nonatomic, copy) NSString    * descriptionStr;
@property (nonatomic, assign) int       id_p;
@property (nonatomic, assign) int       orderNumber;

+ (instancetype)evaSectionWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

@end

#pragma mark - IXQueOptionM

/** 问题答案选项 */
@interface IXQueOptionM : NSObject

@property (nonatomic, copy) NSString    * content;      //选项描述
@property (nonatomic, assign) int       gotoSerialNo;   //目标题号（为0则根据serialNumber跳转）

+ (instancetype)queOptionWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

@end

#pragma mark - IXEvaQuestionM

/** 风险测评model */
@interface IXEvaQuestionM : NSObject

@property (nonatomic, assign) int       id_p;

/**  */
@property (nonatomic, assign) int       sectionId;      //所属分类id
@property (nonatomic, copy) NSString    * sectionTitle; //分类title，只有分类的第一题此property才有值
@property (nonatomic, copy) NSString    * title;        //问题描述
@property (nonatomic, copy) NSString    * lastAnswer;   //上一次答案
@property (nonatomic, copy) NSString    * answer;       //答案
@property (nonatomic, copy) NSString    * userInput;    //用户手动输入答案
@property (nonatomic, strong) NSMutableArray *answerArr;
@property (nonatomic, strong) NSMutableArray   <IXQueOptionM *>* optionList;   //选择题选项
@property (nonatomic, assign) BOOL      allowMobilePaging;      //是否分页
@property (nonatomic, assign) BOOL      isRequired;             //是否必做题
@property (nonatomic, assign) BOOL      lastOptionEditable;     //支持其他选项
@property (nonatomic, assign) int       serialNumber;       //下一题序号
@property (nonatomic, assign) IXQuestionType    type;       //题目类型
@property (nonatomic, strong) NSDictionary  * paraphrase;   //名词解释
@property (nonatomic, assign) BOOL      enable;         //是否启用（第一题默认启用）
@property (nonatomic, assign) BOOL      isOptionEditable; //其它选项是否已经选中（默认没选中）

+ (instancetype)evaQuestionWithDic:(NSDictionary *)dic;
- (instancetype)initWithDic:(NSDictionary *)dic;

@end
