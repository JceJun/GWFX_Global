//
//  IXChooseAccountView.m
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXChooseAccountView.h"
#import "IXChooseAccountCell.h"
#import "IXChooseAccount1Cell.h"
#import "IXDBAccountMgr.h"


@interface IXChooseAccountView ()
<
UITableViewDelegate,
UITableViewDataSource,
UIGestureRecognizerDelegate
>


@property (nonatomic, strong) UIImageView *backImg;



@property (nonatomic, strong) UIView *headView;

@end

@implementation IXChooseAccountView

- (id)initWithFrame:(CGRect)frame
{
    self  = [super initWithFrame:frame];
    if ( self ) {
        [self addSubview:self.backImg];
    }
    return self;
}

- (void)responseToGes
{
    [self removeFromSuperview];
}

- (void)setAccountArr:(NSArray *)accountArr
{
    if ( accountArr && accountArr.count != 0 ) {
        _accountArr = [NSArray arrayWithArray:accountArr];
        
        CGRect frame = self.contentTV.frame;
        frame.origin.x = 40;
        frame.size.width = kScreenWidth - 80;
        frame.origin.y = (kScreenHeight - (accountArr.count * 55) - 57)/2;
        frame.size.height = accountArr.count * 55 + 57;
        self.contentTV.frame = frame;
        
        [self.contentTV reloadData];
        
    }
}


#pragma mark uitableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.accountArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id value = _accountArr[indexPath.row];
    if ( [value isKindOfClass:[NSString class]] ) {
        IXChooseAccount1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXChooseAccount1Cell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = nil;
        
        return cell;
    }
    else if ( [value integerForKey:@"type"] == 0 ) {
        IXChooseAccount1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXChooseAccount1Cell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = _accountArr[indexPath.row];
        
        return cell;
    }else{
        IXChooseAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXChooseAccountCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.isExsitMt4 = self.isExsitMt4;
        cell.model = _accountArr[indexPath.row];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self responseToGes];
    if ( _choose ) {
        _choose(indexPath);
    }
}

- (UIImageView *)backImg
{
    if ( !_backImg ) {
        _backImg = [[UIImageView alloc] initWithFrame:self.bounds];
        _backImg.alpha = 0.75f;
        _backImg.backgroundColor = [UIColor blackColor];
    }
    return _backImg;
}


- (UIView *)headView
{
    if ( !_headView ) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth - 80, 57)];
        
        UILabel *tipLbl = [IXUtils createLblWithFrame:CGRectMake( 20, 20, 200, 18)
                                             WithFont:PF_MEDI(15)
                                            WithAlign:NSTextAlignmentLeft
                                           wTextColor:0x4c6072
                                           dTextColor:0xe9e9ea];
        tipLbl.text = LocalizedString(@"切换账户");
        [_headView addSubview:tipLbl];
        
        UIImageView *sepImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 56, kScreenWidth - 80, 1)];
        sepImg.dk_backgroundColorPicker = DKLineColor;
        [_headView addSubview:sepImg];
    }
    return _headView;
}

- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:CGRectZero];
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.scrollEnabled = NO;
        _contentTV.layer.masksToBounds = YES;
        _contentTV.layer.cornerRadius = 8;
        _contentTV.tableHeaderView = self.headView;
        _contentTV.dk_backgroundColorPicker = DKNavBarColor;
        _contentTV.dk_separatorColorPicker = DKLineColor;
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(responseToGes)];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        
        [_contentTV registerClass:[IXChooseAccountCell class]
           forCellReuseIdentifier:NSStringFromClass([IXChooseAccountCell class])];
        [_contentTV registerClass:[IXChooseAccount1Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXChooseAccount1Cell class])];
        [self addSubview:_contentTV];
    }
    return _contentTV;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ( [touch.view isKindOfClass:[IXChooseAccountView class]] ) {
        return YES;
    }
    return NO;
}
@end

