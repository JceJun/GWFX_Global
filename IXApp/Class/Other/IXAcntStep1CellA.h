//
//  IXAcntStep1CellA.h
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXAcntStep1CellA : UITableViewCell

- (void)loadUIWithDesc:(NSString *)desc
                  text:(NSString *)title
             iconImage:(UIImage *)image
          isHiddenIcon:(BOOL)isHidden;

@end
