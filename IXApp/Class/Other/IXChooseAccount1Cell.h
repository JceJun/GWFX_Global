//
//  IXChooseAccount1Cell.h
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXChooseAccount1Cell : UITableViewCell

@property (nonatomic, strong) NSDictionary *model;

@property (nonatomic, assign) BOOL isCurrentAccount;

@end
