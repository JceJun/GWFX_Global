//
//  IXCreditCardInfoVC.m
//  IXApp
//
//  Created by Larry on 2018/7/27.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXCreditCardContactVC.h"
#import "UIKit+Block.h"
#import "IXSysConfig.h"
#import "IXCpyConfig.h"
#import <UMMobClick/MobClick.h>
#import "IXAppUtil.h"
#import "IXCommonTextView.h"
#import "IXUserDefaultM.h"
#import "IXCountryListVC.h"
#import "IXIncomeModel.h"
#import "IXDPSAmountVC.h"
#import "IXBORequestMgr+Asset.h"
#import "IXBORequestMgr+Region.h"
#import "IXPickerChooseView.h"

@interface IXCreditCardContactVC ()<UITextViewDelegate,UIScrollViewDelegate>
@property(nonatomic,strong)UIView *headerV;
@property(nonatomic,strong)UIButton *nextBtn;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)IXCommonTextView *input_fName;
@property(nonatomic,strong)IXCommonTextView *input_lName;
@property(nonatomic,strong)IXCommonTextView *input_phone;
@property(nonatomic,strong)IXCommonTextView *input_email;
@property(nonatomic,strong)IXCommonTextView *input_address;
@property(nonatomic,strong)IXCommonTextView *input_city;
@property(nonatomic,strong)IXCommonTextView *input_country;
@property(nonatomic,strong)IXCommonTextView *input_postCode;
@property(nonatomic,strong)IXCommonTextView *input_province;

@property(nonatomic,copy)NSString *fName;
@property(nonatomic,copy)NSString *lName;
@property(nonatomic,copy)NSString *phone;
@property(nonatomic,copy)NSString *email;
@property(nonatomic,copy)NSString *address;
@property(nonatomic,copy)NSString *city;
@property(nonatomic,copy)NSString *country;
@property(nonatomic,copy)NSString *postCode;
@property(nonatomic,copy)NSString *province;

@property (nonatomic, strong)IXCountryM    *countryInfo;
@property(nonatomic,strong)IXProvinceM *provinceM;
@property (nonatomic, strong) IXPickerChooseView    * pickChoose;

@end

@implementation IXCreditCardContactVC

- (UIView *)headerV{
    if (!_headerV) {
        _headerV = [UIView new];
        _headerV.dk_backgroundColorPicker = DKTableHeaderColor;
        [self.scrollView addSubview:_headerV];
        
        [_headerV makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(0);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(80);
        }];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = PF_MEDI(15);
        lb_title.dk_textColorPicker = DKCellTitleColor;
        lb_title.textAlignment = NSTextAlignmentCenter;
        lb_title.numberOfLines = 0;
        lb_title.text = @"Bank Reserved Information";
        [_headerV addSubview:lb_title];
        
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(0);
        }];
    }
    return _headerV;
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        _scrollView.delegate = self;
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
            make.width.equalTo(kScreenWidth);
        }];
    }
    return _scrollView;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    
//    [[AppsFlyerTracker sharedTracker] trackEvent:@"入金-信用卡支付通道-" withValues:<#(NSDictionary *)#>]
    
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    self.title = LocalizedString(@"Add Bank Account");
    [self addBackItem];
    [self scrollView];
    self.automaticallyAdjustsScrollViewInsets= NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self headerV];
    
    weakself;
    IXCommonTextView *input_fName = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"First Name" rPlaceHoler:@"Please enter first name"];
    input_fName.tv_content.delegate = self;
    [input_fName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headerV.bottom);
    }];
    _input_fName = input_fName;
    
    IXCommonTextView *input_lName = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Last Name" rPlaceHoler:@"Please enter last name"];
    input_lName.tv_content.delegate = self;
    [input_lName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_fName.bottom);
    }];
    _input_lName = input_lName;
    
    IXCommonTextView *input_address = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Address" rPlaceHoler:@"Please enter address"];
    input_address.tv_content.delegate = self;
    [input_address makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_lName.bottom);
    }];
    _input_address = input_address;
    
    IXCommonTextView *input_city = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"City/Town" rPlaceHoler:@"Please enter city/town"];
    input_city.tv_content.delegate = self;
    [input_city makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_address.bottom);
    }];
    _input_city = input_city;
    
    IXCommonTextView *input_county = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Country/Region" rPlaceHoler:@" "];
    input_county.tv_content.delegate = self;
    [input_county addEnteranceArrow:^{
        [weakSelf selectCountry];
    }];
    [input_county makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_city.bottom);
    }];
    _input_country = input_county;
    
    IXCommonTextView *input_province = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"State/Province" rPlaceHoler:@" "];
    input_province.tv_content.delegate = self;
    [input_province makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_county.bottom);
    }];
    _input_province = input_province;
    
    IXCommonTextView *input_postCode = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Postcode/Zip" rPlaceHoler:@"Please enter postcode or zip"];
    input_postCode.tv_content.delegate = self;
    [input_postCode makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_province.bottom);
    }];
    _input_postCode = input_postCode;
    
    IXCommonTextView *input_email = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Email" rPlaceHoler:@"Please enter email"];
    input_email.tv_content.delegate = self;
    [input_email makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_postCode.bottom);
    }];
    _input_email = input_email;
    
    IXCommonTextView *input_phone = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Telephone" rPlaceHoler:@"Please enter telephone"];
    input_phone.tv_content.keyboardType = UIKeyboardTypeNumberPad;
    input_phone.tv_content.delegate = self;
    [input_phone makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(input_email.bottom);
    }];
    _input_phone = input_phone;
    
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"Next Step" forState:UIControlStateNormal];
    [self.scrollView addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.width.equalTo(kScreenWidth - 30);
        make.top.equalTo(input_phone.bottom).offset(30);
        make.height.equalTo(44);
    }];
    
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        [weakSelf gotoNext:@"Next Step"];
    }];
    _nextBtn = nextBtn;
    
    UILabel *lb_tip1 = [UILabel new];
    lb_tip1.font = ROBOT_FONT(12);
    lb_tip1.dk_textColorPicker = DKCellContentColor;
    lb_tip1.numberOfLines = 0;
    lb_tip1.text = @"Faild to pay?";
    lb_tip1.textAlignment = NSTextAlignmentCenter;
    lb_tip1.numberOfLines = 0;
    [self.scrollView addSubview:lb_tip1];
    
    [lb_tip1 makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.width.equalTo(kScreenWidth - 30);
        make.top.equalTo(nextBtn.bottom).offset(30);
    }];
    
    UILabel *lb_tip2 = [UILabel new];
    lb_tip2.font = ROBOT_FONT(12);
    lb_tip2.dk_textColorPicker = DKCellTitleColor;
    lb_tip2.numberOfLines = 0;
    lb_tip2.text = @"Please call your bank to authorize this payment";
    lb_tip2.textAlignment = NSTextAlignmentCenter;
    lb_tip2.numberOfLines = 0;
    [self.scrollView addSubview:lb_tip2];
    
    [lb_tip2 makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.width.equalTo(kScreenWidth - 30);
        make.top.equalTo(lb_tip1.bottom).offset(3);
        make.bottom.equalTo(-30);
    }];
    
    
    [self.view block_whenTapped:^(UIView *aView) {
        [self.view endEditing:YES];
    }];
    
    [self registKeyboard];
    
    [self activeButton];
    
    [self showProvince:NO];
}

#pragma mark - Keyboard
- (void)registKeyboard{
    //监听键盘出现和消失
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘出现
-(void)keyboardWillShow:(NSNotification *)note
{
    CGRect keyBoardRect= [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyBoardRect.size.height, 0);
}
#pragma mark 键盘消失
-(void)keyboardWillHide:(NSNotification *)note
{
    self.scrollView.contentInset = UIEdgeInsetsZero;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [IXBORequestMgr shareInstance].paramDic = [NSMutableDictionary dictionary];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


- (void)gotoNext:(NSString *)title{
    [self.view endEditing:YES];
    
    if ([self checkInput]) {
        IXIncomeModel *model = [IXIncomeModel new];
        model.firstName = _fName;
        model.lastName = _lName;
        model.mobileNumber = _phone;
        model.email = _email;
        model.mobileNumberPrefix = [@(_countryInfo.countryCode) stringValue];
        
        IXDPSAmountVC *vc = [IXDPSAmountVC new];
        vc.incashModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    //不接受输入空格
    if ([text isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:NONESPACECHAR];
        return NO;
    }
    
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self activeButton];
    });
    
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    [self activeButton];
}

- (void)activeButton{
    _fName = _input_fName.tv_content.text;
    _lName = _input_lName.tv_content.text;
    _phone = _input_phone.tv_content.text;
    _email = _input_email.tv_content.text;
    _address = _input_address.tv_content.text;
    _city = _input_city.tv_content.text;
    _country = _input_country.tv_content.text;
    _postCode = _input_postCode.tv_content.text;
    if (_input_province.hidden) {
        
    }
    _province = _input_province.tv_content.text;
    if (_fName.length && _lName.length && _phone.length && _email.length && _address.length && _city.length && _country.length && _postCode.length) {
        
        self.nextBtn.enabled = YES;
        
        NSString *str = _fName;
        str = [str stringByAppendingFormat:@";%@",_lName];
        str = [str stringByAppendingFormat:@";%@",_address];
        str = [str stringByAppendingFormat:@";%@",_city];
        str = [str stringByAppendingFormat:@";%@",_countryInfo.nationalCode];
        str = [str stringByAppendingFormat:@";%@",_postCode];
        str = [str stringByAppendingFormat:@";%@",_email];
        str = [str stringByAppendingFormat:@";%@",_phone];
        if (!_input_province.hidden) {
            if (_province.length) {
                if (_provinceM) {
                    str = [str stringByAppendingFormat:@";%@",_provinceM.code];
                }else{
                    str =[str stringByAppendingFormat:@";%@",_province] ;
                }
            }else{
                self.nextBtn.enabled = NO;
                return;
            }
        }else{
            str = [str stringByAppendingFormat:@";%@",@""];
        }
        str = [str stringByAppendingFormat:@";%@",@""];
        str = [str stringByAppendingFormat:@";%@",@"0"];
        
        [IXBORequestMgr shareInstance].paramDic = [NSMutableDictionary dictionary];
        [IXBORequestMgr shareInstance].paramDic[@"remark"] = str;

    }else{
        self.nextBtn.enabled = NO;
    }
}




- (BOOL)checkInput{
    if (![IXAppUtil isValidateEmail:_email]) {
        [SVProgressHUD showInfoWithStatus:LocalizedString(@"请输入正确的邮箱号")];
        return NO;
    }
    return YES;
}

- (void)selectCountry
{
    weakself;
    [weakSelf.view endEditing:YES];
    IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
        weakSelf.input_country.tv_content.text = country.nameEN;
        weakSelf.countryInfo = country;
        if ([country.nameEN isEqualToString:@"Canada"]) {
            [weakSelf showProvince:YES];
        }else{
            [weakSelf showProvince:NO];
        }
        [weakSelf activeButton];
//        [weakSelf request_provice:country.nationalCode];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)request_provice:(NSString *)nationalCode{
    [IXBORequestMgr b_getAreaListByNationalCode:nationalCode rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        NSLog(@"%@",obj);
    }];
}

- (NSArray *)getCanadaProvince{
    return @[@{
                @"stateName": @"Alberta",
                @"code": @"AB"
            },
             @{
                 @"stateName": @"British Columbia",
                 @"code": @"BC"
             },
             @{
                 @"stateName": @"Manitoba",
                 @"code": @"MB"
             },
             @{
                 @"stateName": @"New Brunswick",
                 @"code": @"NB"
             },
             @{
                 @"stateName": @"Newfoundland and Labrador",
                 @"code": @"NL"
             },
             @{
                 @"stateName": @"Northwest Territories",
                 @"code": @"NT"
             },
             @{
                 @"stateName": @"Nova Scotia",
                 @"code": @"NS"
             },
             @{
                 @"stateName": @"Nunavut",
                 @"code": @"NU"
             },
             @{
                 @"stateName": @"Ontario",
                 @"code": @"ON"
             },
             @{
                 @"stateName": @"Prince Edward Island",
                 @"code": @"PE"
             },
             @{
                 @"stateName": @"Quebec",
                 @"code": @"QC"
             },
             @{
                 @"stateName": @"Saskatchewan",
                 @"code": @"SK"
             },
             @{
                 @"stateName": @"Yukon",
                 @"code": @"YT"
             }
    ];
}

- (void)dealWithProvinceList:(NSArray *)list{
    NSMutableArray *tempArr = [NSMutableArray array];
    NSMutableArray *titleArr = [NSMutableArray array];
    [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        IXProvinceM *model =  [IXProvinceM new];
        model.code = obj[@"code"];
        model.nameEN = obj[@"stateName"];
        model.nameCN = obj[@"stateName"];
        [tempArr addObject:model];
        [titleArr addObject:obj[@"stateName"]];
    }];

    self.pickChoose.dataArr = [tempArr mutableCopy];
    self.pickChoose.seletedTitles = titleArr;
}


- (IXPickerChooseView *)pickChoose
{
    if (!_pickChoose) {
        weakself;
        _pickChoose = [[IXPickerChooseView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)
                                                 WithChooseInfo:nil
                                                 WithResultInfo:^(NSInteger section, NSInteger row)
                       {
                           weakSelf.provinceM = weakSelf.pickChoose.dataArr[section];
                           weakSelf.input_province.tv_content.text = weakSelf.provinceM.nameEN;
                           [weakSelf activeButton];
//                           NSMutableString *regionStr = [@"" mutableCopy];
//                           IXProvinceM *model = weakSelf.provinceArr[section];
//
//                           weakSelf.accountInfo.provinceName = [model localizedName];
//                           weakSelf.accountInfo.province = model.code;
//                           if (weakSelf.accountInfo.provinceName) {
//                               [regionStr appendString:weakSelf.accountInfo.provinceName];
//                           }
//
//                           if (model.subCountryDictParamList.count > row) {
//                               IXCityM *cityM = model.subCountryDictParamList[row];
//                               weakSelf.accountInfo.cityName = [cityM localizedName];
//                               weakSelf.accountInfo.city = cityM.code;
//
//                               if (weakSelf.accountInfo.cityName) {
//                                   [regionStr appendString:@" "];
//                                   [regionStr appendString:weakSelf.accountInfo.cityName];
//                               }
//                           } else {
//                               weakSelf.accountInfo.cityName = @"";
//                               weakSelf.accountInfo.city = @"";
//                           }
//
//                           weakSelf.accountInfo.regionStr = regionStr;
//                           [weakSelf.tableV reloadData];
//                           weakSelf.infoChanged = [weakSelf.regionStr isEqualToString:regionStr];
                       }];
    }
    _pickChoose.onlyProvince = YES;
    return _pickChoose;
}

- (void)showProvince:(BOOL)flag{
    [self activeButton];
    _input_province.tv_content.text = @"";
    _province = @"";
    _provinceM = nil;
    if (flag) {
//        _input_province.hidden = NO;
//        [_input_postCode updateConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(_input_country.bottom).offset(35);
//        }];
        weakself;
        [_input_province addEnteranceArrow:^{
            [weakSelf activeButton];
            if ([weakSelf.countryInfo.nameEN isEqualToString:@"Canada"]){
                [weakSelf dealWithProvinceList:[weakSelf getCanadaProvince]];
                [weakSelf.pickChoose show];
            }
        }];
    }else{
//        _input_province.hidden = YES;
//        [_input_postCode updateConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(_input_country.bottom);
//        }];
        [_input_province removeEnteranceArrow];
    }
}

@end
