//
//  IXAcntStep5AVC.h
//  IXApp
//
//  Created by Evn on 17/3/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXAcntStep5AVC : IXDataBaseVC
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) NSString *acntTitle;
@end
