//
//  IXSubQuoteStyle2Cell.m
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubQuoteStyle2Cell.h"

 

@implementation IXSubQuoteStyle2Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        [self.contentView addSubview:self.hkQuoteBtn];
         [self.contentView addSubview:self.usQuoteBtn];
 
    }
    return self;
}

- (void)responseToBtnAction:(UIButton *)sender
{
    if ( sender.tag == CHOOSETYPEHK ) {
        _hkQuoteBtn.layer.borderColor = MarketSymbolNameColor.CGColor;
        _usQuoteBtn.layer.borderColor = MarketCellSepColor.CGColor;
    }else{
        _hkQuoteBtn.layer.borderColor = MarketCellSepColor.CGColor;
        _usQuoteBtn.layer.borderColor = MarketSymbolNameColor.CGColor;
    }
    
    if ( _openType ) {
        _openType(sender.tag);
    }
}

- (UIButton *)hkQuoteBtn
{
    if ( !_hkQuoteBtn ) {
        _hkQuoteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _hkQuoteBtn.frame = CGRectMake( 15, 0, (kScreenWidth - 48)/2, 84);
        
        _hkQuoteBtn.layer.cornerRadius = 3;
        _hkQuoteBtn.layer.borderWidth = 1;
        _hkQuoteBtn.layer.borderColor = MarketSymbolNameColor.CGColor;
        
        _hkQuoteBtn.tag = CHOOSETYPEHK;
        [_hkQuoteBtn addTarget:self
                        action:@selector(responseToBtnAction:)
              forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 15, 49, 30)];
        CGPoint logoCenter = logo.center;
        logoCenter.x  = (kScreenWidth - 48)/4;
        logo.center = logoCenter;
        
        [logo setImage:[UIImage imageNamed:@"quoteFee_ChooseHK"]];
        [_hkQuoteBtn addSubview:logo];
        
        
        UILabel *tip = [IXUtils createLblWithFrame:CGRectMake( 0, 57, (kScreenWidth - 48)/2, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0x4c6072
                                        dTextColor:0xff0000];
        tip.text = @"港股实时行情";
        [_hkQuoteBtn addSubview:tip];
    }
    return _hkQuoteBtn;
}


- (UIButton *)usQuoteBtn
{
    if ( !_usQuoteBtn ) {
        _usQuoteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _usQuoteBtn.frame = CGRectMake( kScreenWidth/2 + 9, 0, (kScreenWidth - 48)/2, 84);
        
        _usQuoteBtn.layer.cornerRadius = 3;
        _usQuoteBtn.layer.borderWidth = CHOOSETYPEUS;
        _usQuoteBtn.layer.borderColor = MarketCellSepColor.CGColor;
        
        _usQuoteBtn.tag = 1;
        [_usQuoteBtn addTarget:self
                        action:@selector(responseToBtnAction:)
              forControlEvents:UIControlEventTouchUpInside];
        
        
        UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 15, 49, 30)];
        CGPoint logoCenter = logo.center;
        logoCenter.x  = (kScreenWidth - 48)/4;
        logo.center = logoCenter;
        
        [logo setImage:[UIImage imageNamed:@"quoteFee_ChooseUS"]];
        [_usQuoteBtn addSubview:logo];
                
        UILabel *tip = [IXUtils createLblWithFrame:CGRectMake( 0, 57, (kScreenWidth - 48)/2, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentCenter
                                        wTextColor:0x4c6072
                                        dTextColor:0xff0000];
        tip.text = @"美股实时行情";
        [_usQuoteBtn addSubview:tip];
    }
    return _usQuoteBtn;
}


@end
