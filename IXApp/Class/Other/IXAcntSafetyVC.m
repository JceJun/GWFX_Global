//
//  IXAcntSafetyVC.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntSafetyVC.h"
#import "IXAcntBindStep2AVC.h"
#import "IXAcntBindStep2BVC.h"
#import "IXDeviceMgrListVC.h"
#import "IXAcntSafeCell.h"
#import "IXModifyPwdVC.h"
#import "GesturePwdVC.h"
#import "IXWChatRegVC.h"

#import <LocalAuthentication/LocalAuthentication.h>
#import <UMSocialCore/UMSocialCore.h>

#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Login.h"
#import "GesturePWdData.h"
#import "IXWUserInfo.h"
#import "IXUserInfoM.h"
#import "IXCpyConfig.h"
#import "IXUserDefaultM.h"

@interface IXAcntSafetyVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView   * contentTV;
@property (nonatomic, strong) NSMutableArray    *chooseArr;
@property (nonatomic, strong) NSArray   * dataArr;

@end

@implementation IXAcntSafetyVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"账户与安全");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //如果手动删除系统的指纹密码；则置位指纹设置
    LAContext *lol = [[LAContext alloc] init];
    NSError *err;
    BOOL valid = [lol canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&err];
    if (!valid) {
        if (err.code == LAErrorTouchIDNotEnrolled) {
            [self.chooseArr replaceObjectAtIndex:0 withObject:@(NO)];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KSaveThumbLock];
        }
    }
    [self.contentTV reloadData];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL)supportThumb
{
    BOOL valid = NO;
    LAContext *lol = [[LAContext alloc] init];
    NSError *err;
    valid = [lol canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&err];
    NSString *errMsg ;
    
    if( !valid ){
        errMsg = LocalizedString(@"无法开启指纹解锁功能");
        switch ( err.code ) {
            case LAErrorTouchIDNotAvailable:
                errMsg = LocalizedString(@"设备不支持指纹识别功能");
                break;
            case LAErrorTouchIDNotEnrolled:
                errMsg = LocalizedString(@"还没有设置指纹");
                break;
            case LAErrorTouchIDLockout:
                errMsg = LocalizedString(@"指纹被锁定");
            default:
                break;
        }
        [SVProgressHUD showErrorWithStatus:errMsg];
    }
    
    return valid;
}


- (void)responseToChooseLockType:(UISwitch *)swi
{
    NSInteger swiTag = swi.tag;
    
    //打开指纹解锁的设置
    if (swiTag == 0) {
        if (![self supportThumb]) {
            [swi setOn:NO];
            return;
        }
    }
    
    //只有打开手势密码才提供修改手势密码入口
    if (swiTag == 1 && swi.isOn) {
        [self gesturePwdEnable:YES];
        
        if (![GesturePWdData hasSeted]) {
            [self enterGesturePwdPage];
        }
    } else {
        [self gesturePwdEnable:NO];
    }
    
    //手势密码和指纹验证只能同时只能一个有效
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    if (swi.isOn) {
        if (swiTag == 0) {
            [_chooseArr replaceObjectAtIndex:0 withObject:@(YES)];
            [_chooseArr replaceObjectAtIndex:1 withObject:@(NO)];
            
            [stand setValue:@(YES) forKey:KSaveThumbLock];
            [stand setValue:@(NO) forKey:KSaveGesLock];
        } else {
            [_chooseArr replaceObjectAtIndex:0 withObject:@(NO)];
            [_chooseArr replaceObjectAtIndex:1 withObject:@(YES)];
            
            [stand setValue:@(NO) forKey:KSaveThumbLock];
            [stand setValue:@(YES) forKey:KSaveGesLock];
        }
    } else {
        [_chooseArr replaceObjectAtIndex:swiTag withObject:@(NO)];
        NSString *key = (swiTag == 0) ? KSaveThumbLock : KSaveGesLock;
        [stand setValue:@(NO) forKey:key];
    }
    
    [stand synchronize];
    [self.contentTV reloadData];
}

- (void)gesturePwdEnable:(BOOL)enable{
    NSMutableArray  * sectionA = [@[LocalizedString(@"微信"),
                                    LocalizedString(@"手机号码"),
                                    LocalizedString(@"电子邮箱"),
                                    LocalizedString(@"登录设备管理"),
                                    LocalizedString(@"修改登录密码")] mutableCopy];
    NSMutableArray  * sectionB = [@[LocalizedString(@"指纹验证"),
                                    LocalizedString(@"手势密码"),
                                    LocalizedString(@"修改手势密码")] mutableCopy];


    [sectionA removeObjectAtIndex:0];
    

    if (!enable) {
        [sectionB removeObject:[sectionB lastObject]];
    }
    
    _dataArr = @[sectionA,sectionB];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataArr count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray *)self.dataArr[section] count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == self.dataArr.count - 1) {
        return 10.f;
    }
    return 0.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [tableView sectionHeaderShadow];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == self.dataArr.count -1) {
        return [tableView bottomShadow];
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntSafeCell *cell = [tableView dequeueReusableCellWithIdentifier:
                            NSStringFromClass([IXAcntSafeCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.arrImg.hidden = NO;
    cell.showAttributeText = NO;
    cell.arrImg.dk_imagePicker = DKImageNames(@"openAccount_arrow_right",
                                              @"openAccount_arrow_right_D");
    
    NSString    * option = self.dataArr[indexPath.section][indexPath.row];
    cell.tipName = option;
    
    if (SameString(option, LocalizedString(@"微信"))) {
        NSString *unionId = [IXBORequestMgr shareInstance].userInfo.detailInfo.unionId;
        if (unionId && unionId.length) {
            cell.showAttributeText = YES;
            NSMutableString *str = [LocalizedString(@"已绑定") mutableCopy];
            
            if (unionId.length >= 7) {
                NSString *first = [unionId substringToIndex:3];
                NSString *second = [unionId substringFromIndex:(unionId.length - 4)];
                [str appendFormat:@"%@ **** %@",first,second];
            }
            cell.content = str;
            cell.arrImg.hidden = YES;
        } else {
            cell.content = LocalizedString(@"未绑定");
            cell.contentLbl.dk_textColorPicker = DKCellTitleColor;
        }
    }
    else if (SameString(option, LocalizedString(@"电子邮箱"))) {
        NSString    * content = LocalizedString(@"未绑定");
        if ([IXUserInfoMgr shareInstance].userLogInfo.user.email.length) {
            cell.showAttributeText = YES;
            cell.arrImg.hidden = YES;
            content = [NSString stringWithFormat:@"%@ %@",LocalizedString(@"已绑定"),
                       [IXDataProcessTools formatterEmailDisplay:[IXUserInfoMgr shareInstance].userLogInfo.user.email]];
        } else {
            cell.contentLbl.dk_textColorPicker = DKCellTitleColor;
        }
        cell.content = content;
    }
    else if (SameString(option, LocalizedString(@"手机号码"))){
        NSString    * content = LocalizedString(@"未绑定");
        if ([IXUserInfoMgr shareInstance].userLogInfo.user.phone.length) {
            cell.showAttributeText = YES;
            cell.arrImg.hidden = YES;
            content = [NSString stringWithFormat:@"%@ %@",LocalizedString(@"已绑定"),
                       [IXDataProcessTools formatterPhoneDisplay:
                        [IXUserInfoMgr shareInstance].userLogInfo.user.phone]];
        } else {
            cell.contentLbl.dk_textColorPicker = DKCellTitleColor;
        }
        cell.content = content;
    }
    else if (SameString(option, LocalizedString(@"手势密码")) || SameString(option, LocalizedString(@"指纹验证"))) {
        cell.arrImg.hidden = YES;
        [cell.swiBtn addTarget:self
                        action:@selector(responseToChooseLockType:)
              forControlEvents:UIControlEventValueChanged];
        cell.swiBtn.tag = indexPath.row;
        [cell.swiBtn setOn:[self.chooseArr[indexPath.row] boolValue]];
    }
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray * arr = _dataArr[indexPath.section];
    NSString    * option = [arr objectAtIndex:indexPath.row];
    
    if (SameString(option, LocalizedString(@"电子邮箱"))) {
        if (![IXUserInfoMgr shareInstance].userLogInfo.user.email ||[IXUserInfoMgr shareInstance].userLogInfo.user.email.length == 0) {
            IXAcntBindStep2AVC *VC = [[IXAcntBindStep2AVC alloc] init];
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
    else if (SameString(option, LocalizedString(@"手机号码"))) {
        if (![IXUserInfoMgr shareInstance].userLogInfo.user.phone ||[IXUserInfoMgr shareInstance].userLogInfo.user.phone.length == 0) {
            IXAcntBindStep2BVC *VC = [[IXAcntBindStep2BVC alloc] init];
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
    else if (SameString(option, LocalizedString(@"登录设备管理"))) {
        IXDeviceMgrListVC *VC = [[IXDeviceMgrListVC alloc] init];
        [self.navigationController pushViewController:VC animated:YES];
    }
    else if (SameString(option, LocalizedString(@"修改登录密码"))) {
        IXModifyPwdVC *rootVC = [[IXModifyPwdVC alloc] init];
        [self.navigationController pushViewController:rootVC animated:YES];
    }
    else if (SameString(option, LocalizedString(@"修改手势密码"))) {
         [self enterGesturePwdPage];
    }
}




- (void)enterGesturePwdPage{
    GesturePwdVCState   state = GesturePwdVCStateSetting;
    
    GesturePwdVC    * vc = [[GesturePwdVC alloc] initWithState:state];
    [self presentViewController:vc animated:YES completion:nil];
    
    weakself;
    vc.cancelSetting = ^{
        if (state == GesturePwdVCStateSetting) {
            if (![GesturePWdData hasSeted]) {
                weakSelf.chooseArr = nil;
                [weakSelf gesturePwdEnable:NO];
                [weakSelf.contentTV reloadData];
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:KSaveGesLock];
            }
        }
    };
}

- (NSMutableArray *)chooseArr
{
    if (!_chooseArr) {
        _chooseArr = [NSMutableArray array];
        
        id value = [[NSUserDefaults standardUserDefaults] objectForKey:KSaveThumbLock];
        if (value) {
            [_chooseArr addObject:value];
        } else {
            [_chooseArr addObject:@(NO)];
        }
        
        value = [[NSUserDefaults standardUserDefaults] objectForKey:KSaveGesLock];
        if (value) {
            [_chooseArr addObject:value];
        } else {
            [_chooseArr addObject:@(NO)];
        }
    }
    return _chooseArr;
}

- (NSArray *)dataArr
{
    if (!_dataArr) {
        NSMutableArray  * sectionA = [@[LocalizedString(@"微信"),
                                        LocalizedString(@"手机号码"),
                                        LocalizedString(@"电子邮箱"),
                                        LocalizedString(@"登录设备管理"),
                                        LocalizedString(@"修改登录密码")] mutableCopy];
        NSMutableArray  * sectionB = [@[LocalizedString(@"指纹验证"),
                                        LocalizedString(@"手势密码"),
                                        LocalizedString(@"修改手势密码")] mutableCopy];
        

        [sectionA removeObjectAtIndex:0];
        
        NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
        if (![stand objectForKey:KSaveGesLock] || ![[stand objectForKey:KSaveGesLock] boolValue]) {
            [sectionB removeObject:[sectionB lastObject]];
        }
        _dataArr = @[sectionA,sectionB];
    }
    return _dataArr;
}

- (UITableView *)contentTV
{
    if (!_contentTV) {
        _contentTV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                   kScreenHeight - kNavbarHeight)
                                                  style:UITableViewStyleGrouped];
        _contentTV.sectionFooterHeight = 0.f;
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _contentTV.dk_backgroundColorPicker = DKTableColor;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXAcntSafeCell class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntSafeCell class])];
        [self.view addSubview:_contentTV];
    }
    return _contentTV;
}
@end
