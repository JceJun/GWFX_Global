//
//  GesturePwdView.m
//  AliPayDemo
//
//  Created by pg on 15/7/9.
//  Copyright (c) 2015年 pg. All rights reserved.
//

#import "GesturePwdView.h"
#import "GesturePwdItem.h"
#import "GesturePwdHeader.h"
#import "GesturePWdData.h"
#import "GesturePwdSubItem.h"

#define KscreenHeight [UIScreen mainScreen].bounds.size.height
#define KscreenWidth [UIScreen mainScreen].bounds.size.width

#define ITEMTAG 122

@interface GesturePwdView()

@property(nonatomic , strong) NSMutableArray    * btnArray;
@property(nonatomic , strong) GesturePwdSubItem * subItemsss;
@property(nonatomic , strong) UILabel   * tfLabel;

@property(nonatomic , assign) CGPoint   movePoint;
@property(nonatomic , assign) CGPoint   lastPoint;
//验证次数
@property (nonatomic, assign) int confirmCount;

@property (nonatomic, copy) NSString    * firstInput;   //保存设置密码时最近一次设置的有效密码

@end

@implementation GesturePwdView

#pragma mark -
#pragma mark - init

- (instancetype)initWithModel:(GesturePWDMode)model
{
    if (self = [super init]) {
        
        self.gestureMode = model;
        _confirmCount = 5;
        [self initViews];
    }
    return self;
}

- (void)initViews
{
    self.backgroundColor = [UIColor clearColor];
    //上面的9个小点
    self.subItemsss.backgroundColor = [UIColor clearColor];
    //提示文字
    self.tfLabel.backgroundColor = [UIColor clearColor];
    
    if (self.gestureMode == GesturePWDModeSetPwdMode) {
        self.tfLabel.text = LocalizedString(GesturePwdSettingStr);
    }
    
    //9个大点的布局
    [self createPoint_nine];
    //小按钮上三角的point
    _lastPoint = CGPointMake(0, 0);
}


#pragma mark -
#pragma mark - Touch Event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    if (_confirmCount <= 0 && self.gestureMode == GesturePWDModeConfirmPwdMode){
        [self shake:_tfLabel];
    }else{
        CGPoint point = [self touchLocation:touches];
        [self isContainItem:point];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    if (_confirmCount <= 0 && self.gestureMode == GesturePWDModeConfirmPwdMode){
        return;
    }
    
    CGPoint point = [self touchLocation:touches];
    [self isContainItem:point];
    [self touchMove_triangleAction];
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    if (_confirmCount <= 0 && self.gestureMode == GesturePWDModeConfirmPwdMode){
        return;
    }
    
    [self touchEndAction];
    [self setNeedsDisplay];
}


#pragma mark -
#pragma mark - UILabel  property

- (void)shake:(UIView *)myView
{
    int offset = 8 ;
    
    CALayer *lbl = [myView layer];
    CGPoint posLbl = [lbl position];
    CGPoint y = CGPointMake(posLbl.x-offset, posLbl.y);
    CGPoint x = CGPointMake(posLbl.x+offset, posLbl.y);
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:x]];
    [animation setToValue:[NSValue valueWithCGPoint:y]];
    [animation setAutoreverses:YES];
    [animation setDuration:0.06];
    [animation setRepeatCount:2];
    [lbl addAnimation:animation forKey:nil];
}


#pragma mark -
#pragma mark - total method

/** 下面的9个划线的点   init */
- (void)createPoint_nine
{
    for (int i=0; i<9; i++)
    {
        int row    = i / 3;
        int column = i % 3;
        
        CGFloat spaceFloat  = 40.f;             //每个item的间距是等宽的
        CGFloat topOffset   = 0.f;
        if (_gestureMode == GesturePWDModeConfirmPwdMode) {
            topOffset = 20.f + 30.f + 15.f;
        }else{
            topOffset = 20.f + 40.f + 26.f + 52.f + 41.f + 15.f + 60.f;
        }
        
        CGFloat pointX  = spaceFloat*(column+1)+ITEMWH*column;   //起点X
        CGFloat pointY  = topOffset + ITEMWH*row + spaceFloat*row;     //起点Y
        
        // 对每一个item的frame的布局
        GesturePwdItem *item = [[GesturePwdItem alloc] initWithFrame:CGRectMake( pointX,
                                                                                pointY,
                                                                                ITEMWH,
                                                                                ITEMWH)];
        item.userInteractionEnabled = YES;
        item.backgroundColor = [UIColor clearColor];
        item.isSelect = NO;
        item.tag = ITEMTAG + i ;
        [self addSubview:item];
    }
}

- (CGPoint)touchLocation:(NSSet *)touches
{
    UITouch *touch  = [touches anyObject];
    CGPoint point   = [touch locationInView:self];
    _movePoint      = point;
    
    return point;
}

- (void)isContainItem:(CGPoint)point
{
    for (GesturePwdItem *item  in self.subviews) {
        if (![item isKindOfClass:[GesturePwdSubItem class]] && [item isKindOfClass:[GesturePwdItem class]]) {
            BOOL isContain = CGRectContainsPoint(item.frame, point);
            
            if (isContain && item.isSelect==NO) {
                [self.btnArray addObject:item];
                item.isSelect = YES;
                item.model = selectStyle;
            }
        }
    }
}

- (void)touchMove_triangleAction
{
    NSString *resultStr = [self getResultPwd];
    if (resultStr&&resultStr.length>0) {
        NSArray *resultArr = [resultStr componentsSeparatedByString:@"A"];
        
        if ([resultArr isKindOfClass:[NSArray class]]  &&  resultArr.count>2) {
            NSString *lastTag    = resultArr[resultArr.count-1];
            NSString *lastTwoTag = resultArr[resultArr.count-2];
            
            CGPoint lastP ;
            CGPoint lastTwoP;
            GesturePwdItem *lastItem;
            
            for (GesturePwdItem *item  in self.btnArray) {
                if (item.tag-ITEMTAG == lastTag.intValue) {
                    lastP = item.center;
                }
                
                if (item.tag-ITEMTAG == lastTwoTag.intValue) {
                    lastTwoP = item.center;
                    lastItem = item;
                }
                
                CGFloat x1 = lastTwoP.x;
                CGFloat y1 = lastTwoP.y;
                CGFloat x2 = lastP.x;
                CGFloat y2 = lastP.y;
                
                [lastItem judegeDirectionActionx1:x1 y1:y1 x2:x2 y2:y2 isHidden:NO];
            }
        }
    }
}

- (void)touchEndAction
{
    for (GesturePwdItem *itemssss in self.btnArray) {
        [itemssss judegeDirectionActionx1:0 y1:0 x2:0 y2:0 isHidden:NO];
    }
    
    if ([self.btnArray count] == 0) return;
    
    // if (判断格式少于4个点) [处理密码数据]
    if (self.gestureMode == GesturePWDModeSetPwdMode && [self judgeFormat]){
        [self setPswMethod:[self getResultPwd]];
    }else if (self.gestureMode != GesturePWDModeSetPwdMode && [self judgeFormat]){
        [self setPswMethod:[self getResultPwd]];
    }
    
    [self.btnArray removeAllObjects];
    
    // 选中样式
    for (GesturePwdItem *item  in self.subviews) {
        if (![item isKindOfClass:[GesturePwdSubItem class]] && [item isKindOfClass:[GesturePwdItem class]]) {
            item.isSelect = NO;
            item.model = normalStyle;
            [item judegeDirectionActionx1:0 y1:0 x2:0 y2:0 isHidden:YES];
        }
    }
}

/** 少于4个点 */
- (BOOL)judgeFormat
{
    if (self.btnArray.count <= 3) {
        //不合法
        self.tfLabel.textColor = RedColor;
        if (_gestureMode == GesturePWDModeSetPwdMode) {
            if (_firstInput.length) {
                self.tfLabel.text = LocalizedString(GesturePWdConfirmSettingFailureStr);
            }else{
                self.tfLabel.text = LocalizedString(GesturePwdSettingStr);
            }
        }else{
            _confirmCount --;
            self.tfLabel.text = [NSString stringWithFormat:LocalizedString(@"手势密码错误，还可以输入x次"),@(_confirmCount)];
            if (_confirmCount == 0 && self.confirmFailBlock) {
                //验证失败
                self.confirmFailBlock();
                self.tfLabel.text = LocalizedString(@"请重新登录");
            }
        }
        [self shake:self.tfLabel];
        return NO;
    }
    
    return YES;
}

/** 对密码str进行处理 */
- (NSString *)getResultPwd
{
    NSMutableString *resultStr = [NSMutableString string];
    
    for (GesturePwdItem *item  in self.btnArray) {
        if (![item isKindOfClass:[GesturePwdSubItem class]] && [item isKindOfClass:[GesturePwdItem class]]) {
            [resultStr appendString:@"A"];
            [resultStr appendString:[NSString stringWithFormat:@"%ld", (long)item.tag-ITEMTAG]];
        }
    }
    
    return (NSString *)resultStr;
}


#pragma mark -
#pragma mark - 处理修改，设置，登录的业务逻辑

- (void)setPswMethod:(NSString *)resultStr
{
    UIColor *color = RedColor;
    
    //验证密码
    if (self.gestureMode == GesturePWDModeConfirmPwdMode) {
        color = [self validatePwdJudgeAction:color str:resultStr];
    }else if (self.gestureMode == GesturePWDModeSetPwdMode){
        if (!_firstInput.length ) {
            //第一次输入之后，显示的文字
            self.tfLabel.text = LocalizedString(GesturePwdResetStr);
            self.tfLabel.textColor = AutoNightColor(0xffffff, 0xe9e9ea);
            _firstInput = resultStr;
            [self.subItemsss resultArr:(NSArray *)[resultStr componentsSeparatedByString:@"A"] fillColor:color autoClear:NO];
        }else{
            //设置密码
            color = [self setPwdJudgeAction:color str:resultStr];
        }
    }
}

/** 设置密码 */
- (UIColor *)setPwdJudgeAction:(UIColor *)color str:(NSString *)resultStr
{
    // isRight == YES 2次的密码相同
    if ([_firstInput isEqualToString:resultStr]) {
        // 验证成功
        self.tfLabel.text = LocalizedString(GesturePwdSetSuccessStr);
        self.tfLabel.textColor =  AutoNightColor(0xffffff, 0xe9e9ea);
        [GesturePWdData setPwd:resultStr];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self blockAction:resultStr];
        });
    }else{
        // 失败
        self.tfLabel.text = LocalizedString(GesturePWdConfirmSettingFailureStr);
        self.tfLabel.textColor = RedColor;
        [self shake:self.tfLabel];
        color = RedColor;
    }
    
    return color;
}


/**
 验证登录
 */
- (UIColor *)validatePwdJudgeAction:(UIColor *)color str:(NSString *)resultStr
{
    NSString    * pwd = [GesturePWdData currentPwd];
    BOOL isValidate = [resultStr isEqualToString:pwd];
    if (isValidate) {
        //如果验证成功
        self.tfLabel.text = LocalizedString(GesturePwdConfirmSuccessStr);
        self.tfLabel.textColor = AutoNightColor(0xffffff, 0xe9e9ea);
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self blockAction:resultStr];
        });
    }else{
        //失败
        if (_confirmCount > 0)  _confirmCount -- ;
        
        NSString    * alertStr = [NSString stringWithFormat:LocalizedString(@"手势密码错误，还可以输入x次"),@(_confirmCount)];
        if (_confirmCount == 0) {
            if (self.confirmFailBlock) {
                self.confirmFailBlock();
            }
            alertStr = LocalizedString(@"请重新登录");
        }
        
        self.tfLabel.text = alertStr;
        self.tfLabel.textColor = RedColor;
        [self shake:self.tfLabel];
        color = RedColor;
    }
    
    return color;
}

/** 成功的block回调 */
- (void)blockAction:(NSString *)resultStr
{
    if (self.block) {
        _gestureMode = NoneMode;
        self.block([resultStr stringByReplacingOccurrencesOfString:@"A" withString:@"__"]);
    }
}


#pragma mark -
#pragma mark - getter

- (NSMutableArray *)btnArray
{
    if (_btnArray==nil) {
        _btnArray = [NSMutableArray array];
    }
    return _btnArray;
}

- (UILabel *)tfLabel
{
    if (_tfLabel==nil) {
        _tfLabel = [UILabel new];
        _tfLabel.font = PF_MEDI(13);
        _tfLabel.textAlignment = NSTextAlignmentCenter;
        _tfLabel.textColor = UIColorHexFromRGB(0xffffff);
        
        if (_gestureMode == GesturePWDModeConfirmPwdMode) {
            _tfLabel.frame = CGRectMake(15, 0, kScreenWidth - 30, 45);
        }else{
            _tfLabel.frame = CGRectMake(15, 40 + 26 + 52 + 41, kScreenWidth - 30, 45);
        }
        
        _tfLabel.numberOfLines = 0;
        [self addSubview:_tfLabel];
    }
    return _tfLabel;
}


- (GesturePwdSubItem *)subItemsss
{
    if (_subItemsss==nil) {
        _subItemsss = [[GesturePwdSubItem alloc] initWithFrame:CGRectMake((kScreenWidth - SUBITEMTOTALWH)/2,
                                                                          20 + 40 + 26,
                                                                          SUBITEMTOTALWH,
                                                                          SUBITEMTOTALWH)];
        [self addSubview:_subItemsss];
        _subItemsss.hidden = _gestureMode == GesturePWDModeConfirmPwdMode;
    }
    return _subItemsss;
}


#pragma mark -
#pragma mark - drawRect

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    for (int i=0; i<self.btnArray.count; i++){
        GesturePwdItem *item = (GesturePwdItem *)self.btnArray[i];
        
        if (i==0){
            [path moveToPoint:item.center];
        }else{
            [path addLineToPoint:item.center];
        }
    }
    
    if (_movePoint.x!=0 && _movePoint.y!=0 && NSStringFromCGPoint(_movePoint)){
        [path addLineToPoint:_movePoint];
    }
    
    [path setLineCapStyle:kCGLineCapRound];
    [path setLineJoinStyle:kCGLineJoinRound];
    [path setLineWidth:ITEMRADIUS_LINEWIDTH];
    [RedColor setStroke];
    [path stroke];
}

@end
