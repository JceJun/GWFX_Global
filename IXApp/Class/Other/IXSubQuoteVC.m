//
//  IXSubQuoteVC.m
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubQuoteVC.h"
#import "IXAFRequest.h"
#import "NSString+FormatterPrice.h"
#import "IXBORequestMgr+Quote.h"
#import "IXSubRecordVC.h"

#import "IXDBQuoteDelayMgr.h"
#import "IxItemQuoteDelay.pbobjc.h"
#import "IXDBGlobal.h"

#import "IXDateUtils.h"
#import "IXCpyConfig.h"

#define FEEQUOTEHKCATAID  6002
#define FEEQUOTEUSCATAID  2108

@interface IXSubQuoteVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *contentTV;

@property (nonatomic, assign) QUOTESTATE state;

@property (nonatomic, assign) CHOOSETYPE openType;

@property (nonatomic, strong) UIButton *subRecordBtn;


@property (nonatomic, assign) BOOL chooseAutoFee;

@property (nonatomic, assign) NSInteger hkCataId;

@property (nonatomic, assign) NSInteger usCataId;

@property (nonatomic, assign) BOOL canCancelHK;

@property (nonatomic, assign) long hkLimitDate;

//自动续费
@property (nonatomic, assign) BOOL hkFee;

@property (nonatomic, assign) BOOL canCancelUS;

@property (nonatomic, assign) long usLimitDate;

//自动续费
@property (nonatomic, assign) BOOL usFee;


@end

@implementation IXSubQuoteVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];

    self.title = LocalizedString(@"订阅行情");
    
    [self.contentTV reloadData];
    [self.subRecordBtn addTarget:self
                          action:@selector(responseToDeal)
                forControlEvents:UIControlEventTouchUpInside];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
 
    [self initDataSource];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initDataSource
{
    _hkCataId = FEEQUOTEHKCATAID;
    _usCataId = FEEQUOTEUSCATAID;
    _chooseAutoFee = YES;
    
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    long serverTime = [[stand objectForKey:IXServerTime] longLongValue];
    NSDictionary *hkSubInfo = [IXDBQuoteDelayMgr queryQuoteDelayByCataId:_hkCataId
                                                                                   userid:[IXUserInfoMgr shareInstance].userLogInfo.account.userid];
    if ( hkSubInfo && hkSubInfo[kLimitDate] ) {
        _hkLimitDate = [hkSubInfo[kLimitDate] longLongValue];
        _canCancelHK = (serverTime < _hkLimitDate);
        _hkFee = [hkSubInfo[kType] integerValue];
    }else{
        _hkLimitDate = 0;
        _canCancelHK = NO;
    }
    
    NSDictionary *usSubInfo = [IXDBQuoteDelayMgr queryQuoteDelayByCataId:_usCataId
                                                                                   userid:[IXUserInfoMgr shareInstance].userLogInfo.account.userid];
    if ( usSubInfo && usSubInfo[kLimitDate] ) {
        _usLimitDate = [usSubInfo[kLimitDate] longLongValue];
        _canCancelUS =  (serverTime < _usLimitDate);
        _usFee = [usSubInfo[kType] integerValue];
    }else{
        _usLimitDate = 0;
        _canCancelUS = NO;
    }
    
    if ( !_canCancelHK && !_canCancelHK ) {
        self.chooseAutoFee = YES;
        self.openType = CHOOSETYPEHK;
        _state = QUOTESTATEUNSUB;
    }else{
        _state = QUOTESTATESUB;
    }
    
    [self.contentTV reloadData];
}

- (void)responseToDeal
{
    if ( _state == QUOTESTATESUB ) {
        [self unsubQuote];
    }else{
        [self subQuote];
    }
}

#warning 此功能还没有测试过；弹窗文案也需要统一
//订阅付费行情
- (void)subQuote
{
    NSInteger cataId = (_openType == CHOOSETYPEHK) ? _hkCataId : _usCataId;
    [SVProgressHUD showWithStatus:LocalizedString(@"订阅付费行情...")];

    [IXBORequestMgr quote_subQuoteWithCataId:cataId autoRenew:_chooseAutoFee complete:^(BOOL success, long limitDate) {
        if (success) {
            [SVProgressHUD showSuccessWithStatus:LocalizedString(@"订阅成功")];
            [self cacheSubInfo:limitDate];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"订阅失败")];
        }
    }];
}

//保存订阅信息到数据库
- (void)cacheSubInfo:(long)limitDate
{
    item_quote_delay *pb = [[item_quote_delay alloc] init];
    pb.userid = [IXUserInfoMgr shareInstance].userLogInfo.account.userid;
    pb.symbolCataid = (_openType == CHOOSETYPEHK) ? _hkCataId : _usCataId;
    pb.limitDate = limitDate;
    pb.status = 0;
    pb.type = _chooseAutoFee ? 0 : 1;
    BOOL flag = [IXDBQuoteDelayMgr saveQuoteDelayInfo:pb];
    if( flag ){
        DLog(@"成功保存订阅行情到数据库");
    }
}

- (void)cacheUnsubInfo
{
    item_quote_delay *pb = [[item_quote_delay alloc] init];
    pb.userid = [IXUserInfoMgr shareInstance].userLogInfo.account.userid;
    pb.symbolCataid = (_openType == CHOOSETYPEHK) ? _hkCataId : _usCataId;
    if ( _openType == CHOOSETYPEHK ) {
        pb.limitDate = _hkLimitDate;
    }else{
        pb.limitDate = _usLimitDate;
    }
    pb.status = 0;
    pb.type = 0;
    BOOL flag = [IXDBQuoteDelayMgr saveQuoteDelayInfo:pb];
    if( flag ){
        DLog(@"成功取消订阅行情到数据库");
    }}

//取消订阅付费行情
- (void)unsubQuote
{
    
    NSString *tip = @"";
    if ( _openType == CHOOSETYPEHK ) {
        if ( !_hkFee ) {
            tip = LocalizedString(@"已经取消了港股行情订阅");
        }
    }else if ( _openType == CHOOSETYPEUS ){
        if ( !_hkFee ) {
            tip = LocalizedString(@"已经取消了美股行情订阅");
        }
    }
    
    if ( tip.length != 0 ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocalizedString(@"提示")
                                                        message:tip
                                                       delegate:nil
                                              cancelButtonTitle:LocalizedString(@"确定")
                                              otherButtonTitles:nil, nil];
        [alert show];
        
    }else{
        NSInteger cataId = (_openType == CHOOSETYPEHK) ? _hkCataId : _usCataId;
        
        [SVProgressHUD showWithStatus:LocalizedString(@"取消付费行情...")];
        weakself;
        [IXBORequestMgr quote_unsubQuoteWithCataId:cataId complete:^(BOOL success) {
            if (success) {
                [weakSelf cacheUnsubInfo];
                [SVProgressHUD showSuccessWithStatus:LocalizedString(@"取消订阅成功")];
                [weakSelf.navigationController popViewControllerAnimated:YES];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"取消订阅失败")];
            }
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.row ) {
        case 0:{
            return 44;
        }
            break;
        case 1:{
            return 94;
        }
            break;
        case 2:{
            if ( _state == QUOTESTATESUB ) {
                return 46;
            }
            return 30;
        }
            break;
        case 3:{
            return 106;
        }
            break;
        default:
            return 0;
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.row ) {
        case 0:{
            IXSubQuoteStyle1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSubQuoteStyle1Cell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
        case 1:{
            IXSubQuoteStyle2Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSubQuoteStyle2Cell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            weakself;
            cell.openType = ^(CHOOSETYPE type){
                switch ( type ) {
                    case CHOOSETYPEHK:
                        weakSelf.state = (_canCancelHK) ? QUOTESTATESUB : QUOTESTATEUNSUB;
                        break;
                    case CHOOSETYPEUS:
                        weakSelf.state = (_canCancelUS) ? QUOTESTATESUB : QUOTESTATEUNSUB;
                        break;
                    default:
                        break;
                }
                
                weakSelf.openType = type;
                weakSelf.chooseAutoFee = YES;

                [weakSelf.contentTV reloadData];
            };
            
            return cell;
        }
            break;
        case 2:{
            if ( _state == QUOTESTATESUB ) {
                IXSubQuoteStyle5Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSubQuoteStyle5Cell class])];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                if ( _openType == CHOOSETYPEHK ) {
                    cell.aFee = _hkFee;
                }else{
                    cell.aFee = _usFee;
                }
                return cell;
            }else{
                IXSubQuoteStyle3Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSubQuoteStyle3Cell class])];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                weakself;
                cell.chooseFee = ^(BOOL choose){
                    weakSelf.chooseAutoFee = choose;
                };
                return cell;
            }
            
        }
            break;
        default:{
            IXSubQuoteStyle4Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSubQuoteStyle4Cell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            cell.state = _state;
            
            UIButton *btn =  [cell.contentView viewWithTag:OPERATIONTAG];
            if ( _state == QUOTESTATEUNSUB ) {
                [btn addTarget:self action:@selector(subQuote) forControlEvents:UIControlEventTouchUpInside];
            }else{
                [btn addTarget:self action:@selector(unsubQuote) forControlEvents:UIControlEventTouchUpInside];
            }
            return cell;
        }
             break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ( _state == QUOTESTATESUB ) {
        return 130;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 130)];
    headView.backgroundColor = ViewBackgroundColor;
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 120)];
    [headView addSubview:backView];
    backView.backgroundColor = [UIColor whiteColor];
    
    UILabel *tipLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, kScreenWidth - 15, 15)
                                         WithFont:PF_MEDI(13)
                                        WithAlign:NSTextAlignmentLeft
                                       wTextColor:0x4c6072
                                       dTextColor:0xff0000];
    tipLbl.text = LocalizedString(@"你已开通的服务");
    [headView addSubview:tipLbl];
    
    UIImageView *sepImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 44, kScreenWidth, 1)];
    sepImg.backgroundColor = MarketCellSepColor;
    [headView addSubview:sepImg];
    
    UILabel *HkTitle = [IXUtils createLblWithFrame:CGRectMake( 15, 60, kScreenWidth - 30, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x99abba
                                        dTextColor:0xff0000];
    HkTitle.text = LocalizedString(@"港股实时行情");
    [headView addSubview:HkTitle];
    
    UILabel *HkContent = [IXUtils createLblWithFrame:CGRectMake( 15, 60, kScreenWidth - 30, 15)
                                            WithFont:PF_MEDI(13)
                                           WithAlign:NSTextAlignmentRight
                                          wTextColor:0x4c6072
                                          dTextColor:0xff0000];
    [headView addSubview:HkContent];
    if ( _canCancelHK ) {
        
        HkContent.text = [NSString stringWithFormat:@"%@ %@",LocalizedString(@"有效期至"),[[IXDateUtils sharedInstance] yyyyMdFromSec:_hkLimitDate/1000]];
        HkContent.textColor = MarketSymbolNameColor;
    }else{
        
        HkContent.text = LocalizedString(@"已过期");
        HkContent.textColor = MarketRedPriceColor;

    }
    
    UILabel *USTitle = [IXUtils createLblWithFrame:CGRectMake( 15, 90, kScreenWidth - 30, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x99abba
                                        dTextColor:0xff0000];
    USTitle.text = LocalizedString(@"美股实时行情");
    [headView addSubview:USTitle];
    
    UILabel *USContent = [IXUtils createLblWithFrame:CGRectMake( 15, 90, kScreenWidth - 30, 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentRight
                                          wTextColor:mRedPriceColor
                                          dTextColor:0xff0000];
    [headView addSubview:USContent];
    if ( _canCancelUS ) {
        
        HkContent.text = [NSString stringWithFormat:@"%@ %@",LocalizedString(@"有效期至"),[[IXDateUtils sharedInstance] yyyyMdFromSec:_usLimitDate/1000]];
        USContent.textColor = MarketSymbolNameColor;
    }else{
        
        USContent.text = LocalizedString(@"已过期");
        USContent.textColor = MarketRedPriceColor;
        
    }
    
    UIImageView *bottomSepImg = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 120, kScreenWidth, 1)];
    bottomSepImg.backgroundColor = MarketCellSepColor;
    [headView addSubview:bottomSepImg];
    
    return headView;
}

- (void)popToCheckSubRecord
{
    IXSubRecordVC *subRecord = [[IXSubRecordVC alloc] init];
    [self.navigationController pushViewController:subRecord animated:YES];
}

- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:self.view.bounds];
        _contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _contentTV.backgroundColor = [UIColor whiteColor];
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXSubQuoteStyle1Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSubQuoteStyle1Cell class])];
        [_contentTV registerClass:[IXSubQuoteStyle2Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSubQuoteStyle2Cell class])];
        [_contentTV registerClass:[IXSubQuoteStyle3Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSubQuoteStyle3Cell class])];
        [_contentTV registerClass:[IXSubQuoteStyle4Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSubQuoteStyle4Cell class])];
        [_contentTV registerClass:[IXSubQuoteStyle5Cell class]
           forCellReuseIdentifier:NSStringFromClass([IXSubQuoteStyle5Cell class])];
        _contentTV.scrollEnabled = NO;
        [self.view addSubview:_contentTV];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _contentTV;
}

- (UIButton *)subRecordBtn
{
    if ( !_subRecordBtn ) {
        _subRecordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _subRecordBtn.frame = CGRectMake( (kScreenWidth - 80)/2, kScreenHeight - 41, 80, 32);
        [_subRecordBtn setTitle:LocalizedString(@"查看订阅记录") forState:UIControlStateNormal];
        [_subRecordBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        _subRecordBtn.titleLabel.font = PF_MEDI(12);
        [self.view addSubview:_subRecordBtn];
        [_subRecordBtn addTarget:self action:@selector(popToCheckSubRecord) forControlEvents:UIControlEventTouchUpInside];
    }
    return _subRecordBtn;
}
@end
