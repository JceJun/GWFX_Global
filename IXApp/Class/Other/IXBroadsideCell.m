//
//  IXBroadsideCell.m
//  IXApp
//
//  Created by Evn on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXBroadsideCell.h"

@interface IXBroadsideCell()

@property (nonatomic, strong) UIImageView   *iconImgV;
@property (nonatomic, strong) UILabel   *nameLbl;
@property (nonatomic, strong) UILabel   *numLbl;

@end

@implementation IXBroadsideCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //icon
        CGRect  imgRect = CGRectMake(22, (IXBroadsideCellHeight - 24)/2, 24, 24);
        _iconImgV = [[UIImageView alloc] initWithFrame:imgRect];
        [self.contentView addSubview:_iconImgV];
        
        //title
        CGRect  labRect = CGRectMake(CGRectGetMaxX(imgRect) + 12.5,
                                     (IXBroadsideCellHeight - 20)/2,
                                     kScreenWidth - CGRectGetMaxX(imgRect) - 20.5*2,
                                     20);
        _nameLbl = [IXCustomView createLable:labRect
                                       title:@""
                                        font:PF_MEDI(13)
                                  wTextColor:0x4c6072
                                  dTextColor:0xd4d5dc
                               textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_nameLbl];
        
        //badge
        CGRect  numLabRect = CGRectMake(kScreenWidth*3/4 - 24 - 16,
                                        (IXBroadsideCellHeight - 16)/2,
                                        16,
                                        16);
        _numLbl = [IXCustomView createLable:numLabRect
                                      title:@"3"
                                       font:RO_REGU(10)
                                 wTextColor:0xffffff
                                 dTextColor:0x000000
                              textAlignment:NSTextAlignmentCenter];
        _numLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xff4653, 0xff4d2d);
        _numLbl.layer.cornerRadius = 8.f;
        _numLbl.clipsToBounds = YES;
        [self.contentView addSubview:_numLbl];
    }
    
    return self;
}

- (void)config:(IXBroadsideCellM *)model
{
    _iconImgV.dk_imagePicker = DKImageNames(model.imgStr, model.dImgStr);
    _nameLbl.text = model.title;
    
    [self setNumber:model.number];
}

- (void)setNumber:(NSString *)num
{
    if (num.length && [num integerValue] > 0) {
        _numLbl.hidden = NO;
    }else{
        _numLbl.hidden = YES;
        return;
    }
    
    NSInteger  count = [num integerValue];
    if (count < 100) {
        _numLbl.text = num;
    }else{
        _numLbl.text = @"99+";
    }
    CGSize  size = [_numLbl.text sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
    CGRect  frame = _numLbl.frame;
    frame.size.width = MAX(size.width + 5, 16);
    _numLbl.frame = frame;
}

@end

@implementation IXBroadsideCellM

@end

