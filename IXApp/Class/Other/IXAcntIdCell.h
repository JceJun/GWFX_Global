//
//  IXAcntIdCell.h
//  IXApp
//
//  Created by Evn on 2018/3/24.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

typedef void (^acntIdBlock)(void);
@interface IXAcntIdCell : UITableViewCell

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)IXTextField *cardTF;
@property (nonatomic, strong)acntIdBlock acntIdB;

- (void)loadData:(NSString *)title
         content:(NSString *)content;

@end
