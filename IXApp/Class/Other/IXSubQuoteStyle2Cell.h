//
//  IXSubQuoteStyle2Cell.h
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CHOOSETYPE) {
    CHOOSETYPEHK,
    CHOOSETYPEUS
};

typedef void(^openStockType)(CHOOSETYPE type);


@interface IXSubQuoteStyle2Cell : UITableViewCell

@property (nonatomic ,strong) openStockType openType;

@property (nonatomic, strong) UIButton *hkQuoteBtn;

@property (nonatomic, strong) UIButton *usQuoteBtn;

@end
