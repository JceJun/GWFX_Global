//
//  IXEvaluationVC.m
//  IXApp
//
//  Created by Evn on 2018/1/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXEvaluationVC.h"
#import "IXTouchTableV.h"
#import "IXAcntProgressV.h"
#import "IXHintSureV.h"
#import "IXHintV.h"
#import "IXEvaluationHeaderV.h"
#import "IXAcntStep4VC.h"

#import "IXEvaluationCell.h"
#import "IXEvaluationCellA.h"

#import "IXOpenAcntDataModel.h"
#import "IXEvaluationM.h"
#import "IXBORequestMgr+Survey.h"
#import "IXUserInfoM.h"
#import "IXCpyConfig.h"

@interface IXEvaluationVC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
UITextViewDelegate
>
@property (nonatomic, strong)IXTouchTableV *tableV;
@property (nonatomic, strong)UIButton *nextBtn;
@property (nonatomic, strong)UIView *tableHeaderV;

@property (nonatomic, strong)IXEvaQuestionM *evaQuestionM;
@property (nonatomic, assign)NSInteger pageIndex;//当前页索引
@property (nonatomic, strong)NSMutableArray *dataArr;
@property (nonatomic, assign)NSInteger selectRow;//当前选择的题目
@property (nonatomic, assign)NSInteger selectIndex;//当前题目选择的答案
@property (nonatomic, assign)NSInteger numberLines;
@property (nonatomic, assign)BOOL isSubmit;//是否已经提交过

@end

@implementation IXEvaluationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    self.title = LocalizedString(@"开立真实账户");
    [self initData];
    [self.tableV reloadData];
    [self.view addSubview:self.nextBtn];
    [self loadHintSureView:self.evaluationM.summary enable:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.pageIndex < (self.evaluationM.pageList.count - 1)) {
        self.isSubmit = NO;
        self.evaQuestionM.answer = nil;
        self.evaQuestionM.userInput = nil;
    }
}

- (void)leftBtnItemClicked
{
    [self.view endEditing:YES];
    if (self.pageIndex) {
        self.pageIndex--;
        self.nextBtn.userInteractionEnabled = NO;
        self.nextBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xa7adb5, 0x8395a4);
        self.dataArr = [self.evaluationM.pageList[self.pageIndex] mutableCopy];
        [self.tableV reloadData];
        [self refreshNextBtn];
    } else {
       [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)rightBtnItemClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)initData
{
    self.evaQuestionM = [[IXEvaQuestionM alloc] init];
    self.pageIndex = 0;
    self.dataArr = [self.evaluationM.pageList[self.pageIndex] mutableCopy];
    self.evaQuestionM = self.dataArr[0];//默认第一个点亮
    if (!self.evaQuestionM.enable) {
        self.evaQuestionM.enable = YES;
        [self.dataArr replaceObjectAtIndex:0 withObject:self.evaQuestionM];
    }
}

- (void)loadHintSureView:(NSString *)content
                  enable:(BOOL)enable
{
    IXHintSureV *hintSureV = [[IXHintSureV alloc] initWithFrame:kScreenBound];
    weakself;
    hintSureV.hintSureB = ^{
        if (enable) {
           [weakSelf submitEvaluation];
        }
    };
    [hintSureV showTitle:LocalizedString(@"温馨提示") content:content];
}

- (void)loadHintViewTitle:(NSString *)title
                  content:(NSString *)conten
{
    IXHintV *hintV = [[IXHintV alloc] initWithFrame:kScreenBound];
    [hintV showTitle:title content:conten];
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - 44 - kNavbarHeight) style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.sectionFooterHeight = 0.f;
        _tableV.sectionHeaderHeight = 0.f;
        _tableV.showsVerticalScrollIndicator = NO;
        _tableV.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXEvaluationCell class]
        forCellReuseIdentifier:NSStringFromClass([IXEvaluationCell class])];
        [_tableV registerClass:[IXEvaluationCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXEvaluationCellA class])];
        _tableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    if (_tableHeaderV) {
        [_tableHeaderV removeFromSuperview];
        _tableHeaderV = nil;
    }
    _tableHeaderV = [[IXEvaluationHeaderV alloc] initWithData:self.evaluationM pageIndex:self.pageIndex];
    return _tableHeaderV;
}

- (UIButton *)nextBtn
{
    if (!_nextBtn) {
        _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.frame = CGRectMake(0, kScreenHeight - 44 - kNavbarHeight, kScreenWidth, 44);
        [_nextBtn addTarget:self action:@selector(nextBtnClk:) forControlEvents:UIControlEventTouchUpInside];
        [_nextBtn setTitle:LocalizedString(@"确  定") forState:UIControlStateNormal];
        [_nextBtn dk_setBackgroundColorPicker:DKColorWithRGBs(0x4c6072, 0x50a1e5)];
        _nextBtn.titleLabel.font = PF_REGU(15);
    }
    return _nextBtn;
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.tableHeaderV;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    UIView *headerV = [self tableView:tableView viewForHeaderInSection:section];
    return headerV.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    IXEvaQuestionM *evaQuestionM = self.dataArr[indexPath.row];
    if (evaQuestionM.type == IXQuestionTypeBlankFill) {
        IXEvaluationCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXEvaluationCellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dk_backgroundColorPicker = DKNavBarColor;
        cell.tag = indexPath.row;
        cell.textV.delegate = self;
        cell.textV.tag = indexPath.row;
        weakself;
        cell.selectWordBlock = ^(NSString *key, NSString *desp) {
            [weakSelf loadHintViewTitle:key content:desp];
        };
        self.numberLines = MIN([IXDataProcessTools dealWithTextNumberLine:self.evaQuestionM.answer withWidth:kScreenWidth - 29 font:PF_MEDI(13)], 4);
        [cell refreshData:evaQuestionM numberLines:self.numberLines];
        return cell;
    } else {
        IXEvaluationCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXEvaluationCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dk_backgroundColorPicker = DKNavBarColor;
        weakself;
        cell.tag = indexPath.row;
        cell.selectEvaluateB = ^(NSInteger row, NSInteger index) {
            [weakSelf.view endEditing:YES];
            weakSelf.selectRow = row;
            weakSelf.selectIndex = index;
            weakSelf.evaQuestionM = weakSelf.dataArr[row];
            weakSelf.nextBtn.userInteractionEnabled = YES;
            weakSelf.nextBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
            [weakSelf dealWithSelectData:index];
            [weakSelf refreshTableViewCell];
            [weakSelf refreshNextBtn];
            [weakSelf refreshTextView];
        };
        cell.selectWordBlock = ^(NSString *key, NSString *desp) {
            [weakSelf loadHintViewTitle:key content:desp];
        };
        [cell refreshData:evaQuestionM];
        cell.tField.delegate = self;
        cell.tField.tag = indexPath.row;
        return cell;
    }
}

- (void)dealWithSelectData:(NSInteger)index
{
    self.isSubmit = NO;
    NSString *answer = [IXDataProcessTools asciiCodeToString:index];
    switch (self.evaQuestionM.type) {
        case IXQuestionTypeSingleSel:{
            //单选选择之后不能取消
            if (SameString(self.evaQuestionM.answer, answer)) {
                return;
            } else {
                if ((self.evaQuestionM.optionList.count - 1) == index &&
                    self.evaQuestionM.lastOptionEditable) {
                    if (!self.evaQuestionM.isOptionEditable) {
                        self.evaQuestionM.isOptionEditable = YES;
                    }
                    self.evaQuestionM.answer = nil;
                } else {
                    self.evaQuestionM.isOptionEditable = NO;
                    self.evaQuestionM.userInput = nil;
                    self.evaQuestionM.answer = answer;
                }
            }
        }
            break;
        case IXQuestionTypeMutableSel:{
            if ([IXDataProcessTools isContainString:answer withSet:self.evaQuestionM.answerArr]) {
                [self.evaQuestionM.answerArr removeObject:answer];
                NSString *answerStr = [NSString new];
                for (int i = 0; i < self.evaQuestionM.answerArr.count; i++) {
                    if (i == 0) {
                        answerStr = [answerStr stringByAppendingString:self.evaQuestionM.answerArr[i]];
                    } else {
                        answerStr = [answerStr stringByAppendingFormat:@",%@",self.evaQuestionM.answerArr[i]];
                    }
                }
                if (index == (self.evaQuestionM.optionList.count - 1)) {
                    self.evaQuestionM.userInput = nil;
                    self.evaQuestionM.isOptionEditable = NO;
                }
                self.evaQuestionM.answer = answerStr;
            } else {
                if (answer.length) {
                    if (!self.evaQuestionM.answerArr.count) {
                        self.evaQuestionM.answerArr = [NSMutableArray arrayWithObject:answer];
                    } else {
                        [self.evaQuestionM.answerArr addObject:answer];
                    }
                    if (self.evaQuestionM.answer.length) {
                        self.evaQuestionM.answer = [self.evaQuestionM.answer stringByAppendingFormat:@",%@",answer];
                    } else {
                        self.evaQuestionM.answer = answer;
                    }
                    if (index == (self.evaQuestionM.optionList.count - 1)) {
                        self.evaQuestionM.isOptionEditable = YES;
                    }
                }
            }
        }
            break;
        default:
            break;
    }
     [self clearCacheData];
    [self.dataArr replaceObjectAtIndex:self.selectRow withObject:self.evaQuestionM];
}

- (void)clearCacheData
{
    NSInteger index;
    for (int i = self.pageIndex; i < self.evaluationM.pageList.count; i++) {
        NSMutableArray *tmpArr = [self.evaluationM.pageList[i] mutableCopy];
        if (i == self.pageIndex) {
           index = self.selectRow + 1;
        } else {
            index = 0;
        }
        for (int j = index; j < tmpArr.count; j++) {
            IXEvaQuestionM *evaQuestionM = tmpArr[j];
            evaQuestionM.enable = NO;
            evaQuestionM.answer = nil;
            evaQuestionM.isOptionEditable = NO;
            evaQuestionM.userInput = nil;
            if (evaQuestionM.answerArr.count) {
                [evaQuestionM.answerArr removeAllObjects];
                evaQuestionM.answerArr = nil;
            }
            [tmpArr replaceObjectAtIndex:j withObject:evaQuestionM];
        }
        [self.evaluationM.pageList replaceObjectAtIndex:i withObject:tmpArr];
    }
}

- (void)refreshTableViewCell
{
    [self.tableV reloadData];
    [self.tableV beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectRow inSection:0];
    [self.tableV scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    [self.tableV endUpdates];
}

- (void)refreshNextBtn
{
    if (self.pageIndex == (self.evaluationM.pageList.count - 1) &&
        (self.selectRow + 1) == [(NSArray *)(self.evaluationM.pageList[self.pageIndex]) count]) {
        [_nextBtn setTitle:LocalizedString(@"提  交") forState:UIControlStateNormal];
    } else {
        [_nextBtn setTitle:LocalizedString(@"确  定") forState:UIControlStateNormal];
    }
}

- (void)refreshTextView
{
    if (self.evaQuestionM.type  == IXQuestionTypeBlankFill) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectRow inSection:0];
        IXEvaluationCellA *cell = (IXEvaluationCellA *)[self.tableV cellForRowAtIndexPath:indexPath];;
        [cell.textV becomeFirstResponder];
    } else {
        if (self.evaQuestionM.isOptionEditable) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectRow inSection:0];
            IXEvaluationCell *cell = (IXEvaluationCell *)[self.tableV cellForRowAtIndexPath:indexPath];;
            [cell.tField becomeFirstResponder];
        }
    }
}

#pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.nextBtn.userInteractionEnabled = YES;
    self.nextBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
    self.evaQuestionM = self.dataArr[textField.tag];
    UITableViewCell *cell = (UITableViewCell *) [[textField superview] superview];
    CGFloat offsetY = CGRectGetMaxY(cell.frame);
    CGFloat animationY = CGRectGetHeight(self.tableV.frame) - offsetY - 217;
    if (animationY < 0) {
       [self.tableV setContentOffset:CGPointMake(0, -animationY) animated:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.isSubmit = NO;
    self.evaQuestionM.userInput = textField.text;
    self.selectRow = textField.tag;
    [self.dataArr replaceObjectAtIndex:self.selectRow withObject:self.evaQuestionM];
    [self clearCacheData];
    if (self.tableV.contentOffset.y > 0) {
        [self.tableV setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    [self refreshTableViewCell];
    [self refreshNextBtn];
}

#pragma mark UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.nextBtn.userInteractionEnabled = YES;
    self.nextBtn.dk_backgroundColorPicker = DKColorWithRGBs(0x4c6072, 0x50a1e5);
    self.evaQuestionM = self.dataArr[textView.tag];
    UITableViewCell *cell = (UITableViewCell *) [[textView superview] superview];
    CGFloat offsetY = CGRectGetMaxY(cell.frame);
    CGFloat animationY = CGRectGetHeight(self.tableV.frame) - offsetY - 217;
    if (animationY < 0) {
        [self.tableV setContentOffset:CGPointMake(0, -animationY) animated:YES];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    self.isSubmit = NO;
    [textView resignFirstResponder];
    self.evaQuestionM.answer = textView.text;
    self.selectRow = textView.tag;
    [self.dataArr replaceObjectAtIndex:self.selectRow withObject:self.evaQuestionM];
    [self clearCacheData];
    if (self.tableV.contentOffset.y > 0) {
        [self.tableV setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    [self refreshTableViewCell];
    [self refreshNextBtn];
}

- (void)nextBtnClk:(UIButton *)btn
{
    //已经提交成功返回
    if (self.isSubmit) {
        [self loadHintSureView:self.evaluationM.statement enable:YES];
        return;
    };
    
    if (self.evaQuestionM.answer.length ||
        (self.evaQuestionM.userInput.length
         && self.evaQuestionM.isOptionEditable)) {
        self.isSubmit = NO;
        NSInteger row = 0;
        //默认下一题并且实现翻页
        int page = self.pageIndex,count = 0,preCount = 0;
        BOOL isDir = NO;//是否是指向
        switch (self.evaQuestionM.type) {
            case IXQuestionTypeBlankFill:
            {
                if (self.selectRow == (self.dataArr.count - 1)) {
                    page = self.pageIndex + 1;
                } else {
                    row = self.selectRow + 1;
                }
            }
                break;
            case IXQuestionTypeSingleSel:
            {
                row = ((IXQueOptionM *)(self.evaQuestionM.optionList[self.selectIndex])).gotoSerialNo;
                if (0 == row) {
                    if (self.selectRow == (self.dataArr.count - 1)) {
                        page = self.pageIndex + 1;
                    } else {
                        row = self.selectRow + 1;
                    }
                } else {
                    isDir = YES;
                }
            }
                break;
            case IXQuestionTypeMutableSel:
            {
                if (self.selectRow == (self.dataArr.count - 1)) {
                    page = self.pageIndex + 1;
                } else {
                    row = self.selectRow + 1;
                }
            }
                break;
                
            default:
                break;
        }
        for (int i = 0; i < self.evaluationM.pageList.count; i++) {
            NSArray *data = self.evaluationM.pageList[i];
            count += data.count;
            if (row == count) {
                page = i;
                for (int j = 0; j < i; j++) {
                    preCount += [(NSArray *)(self.evaluationM.pageList[j]) count];
                }
                row = row - ( preCount + 1);
                break;
            } else {
                if (row < count && row != 0) {
                    for (int j = 0; j < i; j++) {
                        preCount += [(NSArray *)(self.evaluationM.pageList[j]) count];
                    }
                    if (i > self.pageIndex) {
                        page = i;
                        row = row - (preCount + 1);//计算翻页索引
                    } else {
                        if (isDir) {
                            row = row - 1;//指向当前页、序号减1
                        }
                    }
                    break;
                }
            }
        }
        //当前页
        IXEvaQuestionM *evaQuestionM = nil;
        if (page == self.pageIndex) {
            if (row > (self.dataArr.count - 1) && self.pageIndex == (self.evaluationM.pageList.count - 1)) {
                [self loadHintSureView:self.evaluationM.statement enable:YES];
            } else {
                self.selectRow = row;
                evaQuestionM = self.dataArr[row];
                evaQuestionM.enable = YES;//启用
                self.evaQuestionM = evaQuestionM;
                [self.dataArr replaceObjectAtIndex:row withObject:evaQuestionM];
                [self refreshTableViewCell];
                [self refreshNextBtn];
                [self refreshTextView];
            }
        } else {
            //保存数据切换页面
            self.pageIndex = page;
            self.selectRow = row;
            if (self.pageIndex < self.evaluationM.pageList.count) {
                self.dataArr = [self.evaluationM.pageList[self.pageIndex] mutableCopy];
                BOOL isExist = NO;
                for (int i = 0; i < self.dataArr.count; i++) {
                    IXEvaQuestionM *evaQuestionM = self.dataArr[i];
                    if (i == self.selectRow) {
                        evaQuestionM.enable = YES;
                        isExist = YES;
                        self.evaQuestionM = evaQuestionM;
                        [self.dataArr replaceObjectAtIndex:i withObject:evaQuestionM];
                        break;
                    }
                }
                //没有设置题目指向、默认指向下一页第一题
                if (!isExist) {
                    IXEvaQuestionM *evaQuestionM = self.dataArr[0];
                    evaQuestionM.enable = YES;
                    self.evaQuestionM = evaQuestionM;
                    [self.dataArr replaceObjectAtIndex:0 withObject:evaQuestionM];
                }
                [self refreshTableViewCell];
                [self refreshNextBtn];
                [self refreshTextView];
            } else {
                self.pageIndex--;
                [self loadHintSureView:self.evaluationM.statement enable:YES];
            }
        }
    }
}

- (void)submitEvaluation
{
    IXSurInfoM  * info = [IXSurInfoM new];
    info.actorId = [IXBORequestMgr shareInstance].userInfo.gts2CustomerId;
    info.companyId = CompanyID;
    info.paperId = self.evaluationM.id_p;
    info.paperTitle = self.evaluationM.title;
    NSDateFormatter * format = [NSDateFormatter new];
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    info.submitDateStr = [format stringFromDate:[NSDate date]];
    
    IXSurRecords    * records = [IXSurRecords new];
    __block NSMutableArray  * arr = [@[] mutableCopy];
    [self.evaluationM.questionList enumerateObjectsUsingBlock:^(IXEvaQuestionM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.answer.length) {
            IXSurRecordM    * m = [IXSurRecordM new];
            m.answer = obj.answer;
            m.questionId = obj.id_p;
            m.questionType = obj.type;
            m.lastEditableOption = obj.userInput;
            [arr addObject:m];
        }
    }];
    records.records = arr;
    if (!arr.count) {
        return;
    }
    weakself;
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    [IXBORequestMgr survey_submitPaperWithInfo:info records:records result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            weakSelf.isSubmit = YES;
            [SVProgressHUD dismiss];
            IXAcntStep4VC *VC = [[IXAcntStep4VC alloc] init];
            VC.acntModel = _acntModel;
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"保存失败")];
            }
        }
    }];
}

@end
