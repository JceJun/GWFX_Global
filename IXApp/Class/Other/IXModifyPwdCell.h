//
//  IXModifyPwdCell.h
//  IXApp
//
//  Created by Bob on 2017/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^errMsg)(NSString *msg);
typedef void(^endInputValue)(NSString *value);

@interface IXModifyPwdCell : UITableViewCell

@property (nonatomic, copy)  NSString *tipName;

@property (nonatomic, copy) errMsg errorMsg;

@property (nonatomic, copy) endInputValue textContent;

@end
