//
//  IXAcntInfoAreaCellA.h
//  IXApp
//
//  Created by Evn on 2017/6/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXAcntInfoAreaCellA : UITableViewCell

@property (nonatomic, strong) UILabel   *desc;
@property (nonatomic, strong) UILabel   *title;

- (void)loadUIWithDesc:(NSString *)desc;

@end
