//
//  IXDeviceMgrInfoVC.h
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXDeviceMgrInfoVC : IXDataBaseVC

@property (nonatomic, assign)NSDictionary *devDic;

@end
