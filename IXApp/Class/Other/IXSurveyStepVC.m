//
//  IXSurveyStep1VC.m
//  IXApp
//
//  Created by Evn on 2017/10/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyStepVC.h"
#import "IXTouchTableV.h"
#import "IXAcntProgressV.h"
#import "IXSurveyStep1Cell.h"
#import "IXSurveyStep1CellA.h"
#import "IXSurveyStep2Cell.h"
#import "IXSurveyMainVC.h"

#import "IXBORequestMgr+Survey.h"
#import "IXUserInfoM.h"
#import "IXCpyConfig.h"

@interface IXSurveyStepVC ()
<
UITableViewDelegate,
UITableViewDataSource,
IXSurveyStep2CellDelegate,
UITextViewDelegate
>

@property (nonatomic, strong)IXTouchTableV *tableV;
@property (nonatomic, strong)UIButton *nextBtn;
@property (nonatomic, strong)UIView *tableHeaderV;

@property (nonatomic, assign)NSInteger index;
@property (nonatomic, strong)NSMutableArray *dataArr;
@property (nonatomic, strong)IXQuestionM *questionM;
@property (nonatomic, assign)NSInteger numberLines;
@property (nonatomic, assign)NSInteger rowLine;

@end

@implementation IXSurveyStepVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    self.title = LocalizedString(@"调查问卷");
    [self initData];
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    [self.tableV reloadData];
}

- (void)initData
{
    self.dataArr = [[self.surveyM getSurvey] mutableCopy];
    self.index = 0;
    self.questionM = self.dataArr[self.index];
    self.numberLines = MIN([IXDataProcessTools dealWithTextNumberLine:self.questionM.lastAnswer withWidth:kScreenWidth - 29 font:PF_MEDI(13)], 4);
}

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (void)submitSurvey
{
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    NSDictionary *paramDic = @{
                               @"resultParam":[self getResultParamInfo],
                               @"records":[self getRecordsInfo]
                               };
    [IXBORequestMgr survey_submitPaperWithParam:paramDic result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [SVProgressHUD dismiss];
            IXSurveyMainVC *VC = [[IXSurveyMainVC alloc] init];
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"保存失败")];
            }
        }
    }];
}

- (NSDictionary *)getResultParamInfo
{
    NSDictionary *dic = @{
            @"actorId":[NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId],
            @"companyId":[NSString stringWithFormat:@"%d",CompanyID],
            @"paperId":self.questionM.paperId
            };
    return dic;
}

- (NSMutableArray *)getRecordsInfo
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.dataArr.count; i++) {
        IXQuestionM *model = self.dataArr[i];
        NSDictionary *dic =  @{
                               @"questionId":model.questionId,
                               @"answer":[IXDataProcessTools dealWithNil:model.lastAnswer]
                               };
        [arr addObject:dic];
    }
    return arr;
}

- (void)leftBtnItemClicked
{
    if (self.index == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.dataArr replaceObjectAtIndex:self.index withObject:self.questionM];
        self.index--;
        self.questionM = self.dataArr[self.index];
        [self refreshUI];
    }
}

- (void)rightBtnItemClicked {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)refreshUI
{
    if (self.questionM.appendArr.count) {
        [self.tableV reloadData];
        [self.tableV setTableHeaderView:[self tableHeaderV]];
        [self.tableV setTableFooterView:[self tableFooterV]];
    } else {
        if ([self.questionM.type integerValue] == 0) {
            self.numberLines = MIN([IXDataProcessTools dealWithTextNumberLine:self.questionM.lastAnswer withWidth:kScreenWidth - 29 font:PF_MEDI(13)], 4);
            [self.tableV reloadData];
            [self.tableV setTableHeaderView:[self tableHeaderV]];
            [self.tableV setTableFooterView:[self tableFooterV]];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
        }
    }
    if ([self.questionM.isRequired isEqualToNumber:@1]) {
        if (self.questionM.lastAnswer.length) {
            _nextBtn.enabled = YES;
        } else {
            _nextBtn.enabled = NO;
        }
    } else {
        _nextBtn.enabled = YES;
    }
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:kScreenBound style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXSurveyStep1Cell class]
        forCellReuseIdentifier:NSStringFromClass([IXSurveyStep1Cell class])];
        [_tableV registerClass:[IXSurveyStep1CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXSurveyStep1CellA class])];
        [_tableV registerClass:[IXSurveyStep2Cell class]
        forCellReuseIdentifier:NSStringFromClass([IXSurveyStep2Cell class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    if (_tableHeaderV) {
        [_tableHeaderV removeFromSuperview];
        _tableHeaderV = nil;
    }
    if (!_tableHeaderV) {
        _tableHeaderV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
        _tableHeaderV.dk_backgroundColorPicker = DKTableHeaderColor;
        
        NSString *titleStr = [NSString stringWithFormat:@"%@(%ld/%lu)",self.surveyM.title,(self.index + 1),(unsigned long)self.dataArr.count];
        CGSize size = [IXDataProcessTools sizeWithString:titleStr font:PF_MEDI(15) width:kScreenWidth - 29];
        if (size.height > (132 - 14.5*2)) {
            size.height = (132 - 14.5*2);
        }
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(14.5, (132 - size.height)/2, kScreenWidth - 29, size.height)];
        label.text = titleStr;
        label.textAlignment = NSTextAlignmentCenter;
        label.dk_textColorPicker = DKGrayTextColor;
        label.font = PF_MEDI(15);
        label.numberOfLines = 0;
        [_tableHeaderV addSubview:label];
        
        IXAcntProgressV *prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
        [_tableHeaderV addSubview:prgV];
        [prgV showFromMole:self.index toMole:(self.index + 1) deno:(self.dataArr.count?self.dataArr.count:1)];
    }
    
    return _tableHeaderV;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*4-kNavbarHeight)];
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_nextBtn setTitle:LocalizedString(@"下一步") forState:UIControlStateNormal];
    _nextBtn.titleLabel.font = PF_REGU(15);
    [v addSubview:_nextBtn];
    
    return v;
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.questionM.type integerValue] == 0) {
        return 2;
    } else {
        return self.questionM.appendArr.count + 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.questionM.type isEqualToNumber:@(0)]) {
        if (self.numberLines > 1 && indexPath.row == 1) {
            return 44.f + (self.numberLines - 1)*21;
        } else {
            return 44.f;
        }
    } else {
        if (self.rowLine > 1) {
            return 44.f + (self.rowLine - 1)*21;
        } else {
            return 44.f;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        IXSurveyStep1CellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSurveyStep1CellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dk_backgroundColorPicker = DKNavBarColor;
        NSString *title = nil;
        if ([self.questionM.type isEqualToNumber:@2]) {
            title = [NSString stringWithFormat:@"%@(%@)",self.questionM.title,LocalizedString(@"可多选")];
        } else {
            title = self.questionM.title;
        }
        self.rowLine = MIN([IXDataProcessTools dealWithTextNumberLine:title withWidth:kScreenWidth - 29 font:PF_MEDI(13)], 4);
        [cell loadUIWithText:title numberLines:self.rowLine];
        return cell;
    } else {
        if ([self.questionM.type isEqualToNumber:@0]) {
            IXSurveyStep2Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSurveyStep2Cell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.dk_backgroundColorPicker = DKNavBarColor;
            [cell loadUIWithTextView:[IXDataProcessTools dealWithNil:self.questionM.lastAnswer] numberLines:self.numberLines];
            cell.textV.delegate = self;
            cell.delegate = self;
            return cell;
          
        } else if ([self.questionM.type isEqualToNumber:@1]) {
            IXSurveyStep1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSurveyStep1Cell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.rowLine = MIN([IXDataProcessTools dealWithTextNumberLine:self.questionM.appendArr[indexPath.row - 1] withWidth:kScreenWidth - 46 font:PF_MEDI(13)], 4);
            [cell loadUIWithText:self.questionM.appendArr[indexPath.row - 1] appear:[self.questionM.lastAnswer isEqualToString:[IXDataProcessTools asciiCodeToString:indexPath.row]] numberLines:self.rowLine];
            return cell;
        } else {
            IXSurveyStep1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXSurveyStep1Cell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.rowLine = MIN([IXDataProcessTools dealWithTextNumberLine:self.questionM.appendArr[indexPath.row - 1] withWidth:kScreenWidth - 46 font:PF_MEDI(13)], 4);
            [cell loadUIWithText:self.questionM.appendArr[indexPath.row - 1] appear:[IXDataProcessTools isContainString:[IXDataProcessTools asciiCodeToString:indexPath.row] withSet:self.questionM.answerArr] numberLines:self.rowLine];
            return cell;
        }
    }
}

- (void)textViewValue:(NSString *)value tag:(NSInteger)tag
{
    if (value.length) {
        self.questionM.lastAnswer = value;
        if (!_nextBtn.enabled) {
            _nextBtn.enabled = YES;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.questionM.lastAnswer = textView.text;
    self.numberLines = [IXDataProcessTools dealWithTextNumberLine:self.questionM.lastAnswer withWidth:textView.frame.size.width font:PF_MEDI(13)];
    if (self.numberLines > 4) {
        self.numberLines = 4;
    } else {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableV reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationAutomatic)];
    }
    if (self.questionM.lastAnswer.length) {
        if (!_nextBtn.enabled) {
            _nextBtn.enabled = YES;
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return;
    } else {
        [self dealWithSelectData:indexPath.row];
    }
    [self.tableV reloadData];
    if (self.questionM.lastAnswer.length) {
        if (!_nextBtn.enabled) {
            _nextBtn.enabled = YES;
        }
    }
}

- (void)dealWithSelectData:(NSInteger)index
{
    if ([self.questionM.type isEqualToNumber:@0]) {
        return;
    } else if ([self.questionM.type isEqualToNumber:@1]) {
        self.questionM.lastAnswer = [IXDataProcessTools asciiCodeToString:index];
    } else {
        NSString *lastAnswer = [IXDataProcessTools asciiCodeToString:index];
        if ([IXDataProcessTools isContainString:lastAnswer withSet:self.questionM.answerArr]) {
            [self.questionM.answerArr removeObject:lastAnswer];
            NSString *answerStr = [NSString new];
            for (int i = 0; i < self.questionM.answerArr.count; i++) {
                if (i == 0) {
                    answerStr = [answerStr stringByAppendingString:self.questionM.answerArr[i]];
                } else {
                    answerStr = [answerStr stringByAppendingFormat:@",%@",self.questionM.answerArr[i]];
                }
            }
            self.questionM.lastAnswer = answerStr;
        } else {
            if (lastAnswer.length) {
                [self.questionM.answerArr addObject:lastAnswer];
                if (self.questionM.lastAnswer.length) {
                    self.questionM.lastAnswer = [self.questionM.lastAnswer stringByAppendingFormat:@",%@",lastAnswer];
                } else {
                    self.questionM.lastAnswer = [self.questionM.lastAnswer stringByAppendingString:lastAnswer];
                }
            }
        }
    }
}

- (void)nextBtnClk
{
    [self.view endEditing:YES];
     [self.dataArr replaceObjectAtIndex:self.index withObject:self.questionM];
    if (self.index == (self.dataArr.count - 1)) {
        [self submitSurvey];
        return;
    }
    if (self.dataArr.count) {
        self.index++;
        self.questionM = self.dataArr[self.index];
        [self refreshUI];
    } else {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
        return;
    }
}

@end
