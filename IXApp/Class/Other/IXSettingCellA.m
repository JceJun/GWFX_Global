//
//  IXSettingCellA.m
//  IXApp
//
//  Created by Evn on 16/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSettingCellA.h"

@interface IXSettingCellA()

@property (nonatomic, strong) UILabel   * titleLbl;
@property (nonatomic, strong) UILabel   * contentLbl;
@property (nonatomic, strong) UIView    * bottomLine;
@property (nonatomic, strong) UIView    * topLine;

@end

@implementation IXSettingCellA

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleLbl];
        [self.contentView addSubview:self.accImgV];
        [self.contentView addSubview:self.contentLbl];
    }
    return self;
}

- (void)configTitle:(NSString *)title content:(NSString *)content
{
    self.titleLbl.text = title;
    self.contentLbl.text = content;
    
    if (SameString(title, LocalizedString(@"退出登录"))) {
        self.titleLbl.textAlignment = NSTextAlignmentCenter;
    } else {
        self.titleLbl.textAlignment = NSTextAlignmentLeft;
    }
}


- (void)showTopLineWithOffsetX:(CGFloat)offset
{
    [self.contentView addSubview:self.topLine];
    self.topLine.frame = CGRectMake(offset, 0, kScreenWidth - offset, kLineHeight);
}

- (void)showBototmLineWithOffsetX:(CGFloat)offset
{
    [self.contentView addSubview:self.bottomLine];
    self.bottomLine.frame = CGRectMake(offset, 44 - kLineHeight, kScreenWidth - offset, kLineHeight);
}

- (void)setTitleFont:(UIFont *)titleFont
{
    _titleLbl.font = titleFont;
}

- (void)setContentFont:(UIFont *)font
{
    self.contentLbl.font = font;
}


#pragma mark -
#pragma mark - lazy loading
- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [IXCustomView createLable:CGRectMake(15,14, kScreenWidth - 30, 17)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x4c6072
                                     dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentLeft];
    }
    return _titleLbl;
}

- (UIImageView *)accImgV
{
    if (!_accImgV) {
        _accImgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - 22, 15, 7, 14)];
        _accImgV.tag = 1;
        _accImgV.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
    }
    return _accImgV;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [IXCustomView createLable:CGRectMake(VIEW_X(_accImgV) - (207.5),14, 200,17)
                                          title:@""
                                           font:PF_MEDI(13)
                                     wTextColor:0x99abba
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentRight];
    }
    return _contentLbl;
}

- (UIView *)bottomLine
{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
        _bottomLine.dk_backgroundColorPicker = DKLineColor;
    }
    return _bottomLine;
}

- (UIView *)topLine
{
    if (!_topLine) {
        _topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
        _topLine.dk_backgroundColorPicker = DKLineColor;
    }
    return _topLine;
}

@end
