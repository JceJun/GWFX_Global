//
//  IXAcntStep1CellB.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep1CellB.h"
#import "UIViewExt.h"
@interface IXAcntStep1CellB()<UITextFieldDelegate>

@property (nonatomic, strong)UIButton *iconBtn;

@end

@implementation IXAcntStep1CellB

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 67, 16)];
        _desc.font = PF_MEDI(13);
        _desc.textAlignment = NSTextAlignmentLeft;
        _desc.dk_textColorPicker = DKCellTitleColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (IXTextField *)textF
{
    if (!_textF) {
        _textF = [[IXTextField alloc] initWithFrame:CGRectMake(99, 8, kScreenWidth-102-10 - 30, 30)];
        _textF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textF.dk_textColorPicker = DKCellContentColor;
        _textF.backgroundColor = [UIColor clearColor];
        _textF.font = PF_MEDI(13);
        _textF.delegate = self;
        _textF.keyboardType = UIKeyboardTypeNumberPad;
        _textF.placeholder = LocalizedString(@"请输入手机号码");
        [self.contentView addSubview:_textF];
    }
    return _textF;
}

- (UIButton *)iconBtn
{
    if (!_iconBtn) {
        _iconBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - (14 + 16), 14, 16, 16)];
        [_iconBtn setImage:AutoNightImageNamed(@"common_positionDetail") forState:UIControlStateNormal];
        [_iconBtn addTarget:self action:@selector(clickInfo:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_iconBtn];
    }
    
    return _iconBtn;
}

- (void)loadUIWithDesc:(NSString *)desc
                  text:(NSString *)title
           placeHolder:(NSString *)holder
          keyboardType:(UIKeyboardType)type
       secureTextEntry:(BOOL)Entry
             iconImage:(UIImage *)image
          isHiddenIcon:(BOOL)isHidden
               iconTag:(NSInteger)tag;{
    
    self.desc.text = desc;
    self.textF.text = title;
    self.textF.tag = tag;
    
    UIColor * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
    self.textF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:holder attributes:@{NSForegroundColorAttributeName:phColor}];
    self.textF.keyboardType = type;
    self.textF.secureTextEntry = Entry;
    self.iconBtn.hidden = isHidden;
    self.iconBtn.tag = tag;
    
    if (!self.iconBtn && image) {
        [_iconBtn setImage:image forState:UIControlStateNormal];
    }
    
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (void)setTextFieldEnable:(BOOL)enable
{
    self.textF.enabled = enable;
}

- (void)clickInfo:(UIButton *)btn
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickInfo:)]) {
        [self.delegate clickInfo:btn];
    }
}


#pragma mark -
#pragma mark - text field

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textValue:tag:)]) {
        [self.delegate textValue:textField.text tag:textField.tag];
    }
}



@end
