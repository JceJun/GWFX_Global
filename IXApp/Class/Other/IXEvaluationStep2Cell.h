//
//  IXEvaluationStep2Cell.h
//  IXApp
//
//  Created by Evn on 2017/11/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "PHTextView.h"

@interface IXEvaluationStep2Cell : UITableViewCell

@property (nonatomic, strong) PHTextView    * textV;
- (void)loadUIWithTextView:(NSString *)text
               numberLines:(NSInteger)numberLines;

@end
