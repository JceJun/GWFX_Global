//
//  IXAcntPswV.m
//  IXApp
//
//  Created by Evn on 2017/12/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntPswV.h"
#import "IXTextField.h"

@interface IXAcntPswV()<UITextFieldDelegate>

@property (nonatomic, strong)UIView *alertV;
@property (nonatomic, strong)UILabel *hint;
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UIView *bgV;
@property (nonatomic, strong)IXTextField *tField;
@property (nonatomic, strong)UIButton *sureBtn;


@end

@implementation IXAcntPswV

- (void)show
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75f];
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    [self addGestureRecognizer:tap];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.center = keyWindow.center;
    [keyWindow addSubview:self];

    self.alertV.center = keyWindow.center;
    [keyWindow addSubview:self.alertV];
    [self hint];
    [self uLine];
    [self bgV];
    [self tField];
    [self sureBtn];
}

- (UIView *)alertV
{
    if (!_alertV) {
        _alertV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 295, 230)];
        _alertV.dk_backgroundColorPicker = DKViewColor;
        _alertV.layer.cornerRadius = 10;
        _alertV.layer.masksToBounds = YES;
        
    }
    return _alertV;
}

- (UILabel *)hint
{
    if ( !_hint ) {
        _hint = [IXUtils createLblWithFrame:CGRectMake( 15, 18, VIEW_W(self.alertV) - 30, 22)
                                   WithFont:PF_MEDI(15)
                                  WithAlign:NSTextAlignmentCenter
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                 ];
        _hint.text = LocalizedString(@"验证登录密码");
        [self.alertV addSubview:_hint];
    }
    return _hint;
}

- (UIView *)uLine
{
    if (!_uLine) {
        _uLine = [[UIView alloc] initWithFrame:CGRectMake(0, GetView_MaxY(self.hint) + 20, VIEW_W(self.alertV), 0.5)];
        _uLine.dk_backgroundColorPicker = DKLineColor;
        [self.alertV addSubview:_uLine];
    }
    return _uLine;
}

- (UIView *)bgV
{
    if (!_bgV) {
        _bgV = [[UIView alloc] initWithFrame:CGRectMake( 20, GetView_MaxY(self.uLine) + 30,  VIEW_W(self.alertV) - 40, 44)];
        _bgV.layer.cornerRadius = 2;
        _bgV.layer.borderWidth = 0.5;
        _bgV.layer.dk_borderColorPicker = DKColorWithRGBs(0xe2eaf2, 0x303b4d);
        [self.alertV addSubview:_bgV];
    }
    return _bgV;
}

- (IXTextField *)tField
{
    if ( !_tField ) {
        _tField = [[IXTextField alloc] initWithFrame:CGRectMake(20, 5,  VIEW_W(self.bgV) - 40, 34)];
        _tField.font = RO_REGU(15);
        _tField.dk_textColorPicker = DKCellTitleColor;
        _tField.textAlignment = NSTextAlignmentLeft;
        _tField.keyboardType = UIKeyboardTypeDecimalPad;
        _tField.placeholder = LocalizedString(@"请输入登录密码");
        _tField.delegate = self;
        [self.bgV addSubview:_tField];
    }
    return _tField;
}

- (UIButton *)sureBtn
{
    if (!_sureBtn) {
        _sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(15, GetView_MaxY(self.bgV) + 25,VIEW_W(self.alertV) - 30, 46)];
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_sureBtn setBackgroundImage:image forState:UIControlStateNormal];
        _sureBtn.titleLabel.font = PF_MEDI(15);
        [_sureBtn setTitle:LocalizedString(@"确定") forState:UIControlStateNormal];
        [_sureBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [self.alertV addSubview:_sureBtn];
    }
    return _sureBtn;
}

- (void)click
{
    if (self.delegate) {
        [self.delegate passWord:_tField.text];
    }
    [self dismiss];
}

- (void)dismiss
{
    if (self.alertV) {
        [self.alertV removeFromSuperview];
    }
    if (self) {
        [self removeFromSuperview];
    }
}

@end
