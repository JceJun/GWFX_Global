//
//  IXAcntCerFileCell.m
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntCerFileCell.h"

@interface IXAcntCerFileCell()

@property (nonatomic,strong)UILabel *title;
@property (nonatomic,strong)UILabel *desc;
@property (nonatomic,strong)UIImageView *sIcon;
@property (nonatomic,strong)UIImageView *rIcon;
@property (nonatomic,strong)UIView *uLine;
@property (nonatomic,strong)UIView *line;

@end

@implementation IXAcntCerFileCell

- (UIImageView *)sIcon
{
    if (!_sIcon) {
        CGRect rect = CGRectMake(kScreenWidth - (7.5 + 16 + 28), 11, 20, 20);
        _sIcon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_sIcon];
    }
    return _sIcon;
}

- (UIImageView *)rIcon
{
    if (!_rIcon) {
        CGRect rect = CGRectMake(kScreenWidth - (7.5 + 16), 15, 7.5, 13.5);
        _rIcon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_rIcon];
    }
    return _rIcon;
}

- (UILabel *)title
{
    if (!_title) {
        CGRect rect = CGRectMake(15, 15, kScreenWidth - 15*2, 14);
        _title = [[UILabel alloc] initWithFrame:rect];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKCellTitleColor;
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        CGRect rect = CGRectMake(kScreenWidth - (7.5 + 16 + 8 + 20 + 13 +180), 15, 180, 14);
        _desc = [[UILabel alloc] initWithFrame:rect];
        _desc.font = PF_MEDI(13);
        _desc.dk_textColorPicker = DKCellContentColor;
        _desc.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (UIView *)uLine
{
    if (!_uLine) {
        CGRect rect = CGRectMake(0, 0, kScreenWidth, kLineHeight);
        _uLine = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_uLine];
    }
    return _uLine;
}

- (UIView *)line
{
    if (!_line) {
        CGRect rect = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
        _line = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_line];
    }
    return _line;
}

- (void)reloadUIWithTitle:(NSString *)title
                     desc:(NSDictionary *)fileDic
             indexPathRow:(NSInteger)index
                  isFirst:(BOOL)firstFlag
{
    self.title.text = title;
    self.rIcon.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
    if (fileDic && [fileDic isKindOfClass:[NSDictionary class]] && [[fileDic allKeys] containsObject:@"proposalStatus"]) {
        NSString *proposalStatus = fileDic[@"proposalStatus"];
        if ([proposalStatus isEqualToString:@"-1"]) {
            self.desc.text = LocalizedString(@"未通过");
            self.sIcon.dk_imagePicker = DKImageNames(@"cerFile_unload", @"cerFile_unload_D");
        } else if ([proposalStatus isEqualToString:@"0"]) {
            self.desc.text = LocalizedString(@"未上传");
            self.sIcon.dk_imagePicker = DKImageNames(@"cerFile_unload", @"cerFile_unload_D");
        } else if ([proposalStatus isEqualToString:@"1"]) {
           self.desc.text = LocalizedString(@"审核中");
            self.sIcon.dk_imagePicker = DKImageNames(@"cerFile_review", @"cerFile_review_D");
        } else if ([proposalStatus isEqualToString:@"2"]) {
            self.desc.text = LocalizedString(@"已通过");
            self.sIcon.dk_imagePicker = DKImageNames(@"cerFile_checked", @"cerFile_checked_D");
        }
    } else {
        if (firstFlag) {
            self.desc.text = LocalizedString(@"未上传");
            self.sIcon.dk_imagePicker = DKImageNames(@"cerFile_unload", @"cerFile_unload_D");
        }
    }
    self.uLine.dk_backgroundColorPicker = DKLineColor;
    self.line.dk_backgroundColorPicker = DKLineColor;
    if (index != 0) {
        self.uLine.hidden = YES;
    } else {
        self.uLine.hidden = NO;
    }
    
    self.dk_backgroundColorPicker = DKNavBarColor;
}

@end
