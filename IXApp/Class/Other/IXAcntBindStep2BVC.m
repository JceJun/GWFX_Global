//
//  IXAcntBindStep2BVC.m
//  IXApp
//
//  Created by Evn on 2017/5/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntBindStep2BVC.h"
#import "IXChangePhoneStep1VC.h"
#import "IXTouchTableV.h"
#import "IXEmailRegStep2CellA.h"
#import "IXLoginMainCellA.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Account.h"

static NSInteger sendCounter = SendSMS_Interval;

@interface IXAcntBindStep2BVC ()
<
UITableViewDelegate,
UITableViewDataSource,
IXLoginMainCellADelegate,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV  * tableV;
@property (nonatomic, strong) NSString  * verifyCodeId;
@property (nonatomic, strong) UIButton  * submitBtn;
@property (nonatomic, strong) NSTimer   * timer;

@end

@implementation IXAcntBindStep2BVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(leftBtnItemClicked)];
    
    self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                                                            target:self
                                                                               sel:@selector(onGoback)];
    self.title = LocalizedString(@"验证邮箱");
    
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    
    if (sendCounter < SendSMS_Interval) {
        [self enableTimer];
    }
    [self sendBtnClicked];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self disableTimer];
    sendCounter = 0;
    [self checkCounter];
}

- (void)dealloc
{
    [self disableTimer];
}

#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)submitBtnClk
{
    [self submitVeriCode];
}

- (void)sendBtnClicked
{
    [self sendVericode];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        NSString    * ident = NSStringFromClass([IXEmailRegStep2CellA class]);
        IXEmailRegStep2CellA    * cell = [tableView dequeueReusableCellWithIdentifier:ident];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell reloadUIWithEmail:[IXUserInfoMgr shareInstance].userLogInfo.user.email];
        [cell sendBtnTitleChanged:sendCounter];
        [cell.sendBtn addTarget:self
                         action:@selector(sendBtnClicked)
               forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }else{
        NSString    * ident = NSStringFromClass([IXLoginMainCellA class]);
        IXLoginMainCellA    * cell = [tableView dequeueReusableCellWithIdentifier:ident];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textF.delegate = self;
        [cell.textF becomeFirstResponder];
        [cell loadUIWithDesc:LocalizedString(@"验证码")
                        text:@""
                 placeHolder:LocalizedString(@"请填写验证码")
                keyboardType:UIKeyboardTypeNumberPad
             secureTextEntry:NO];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        return cell;
    }
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self setSubmitBtnEnable:aimStr.length >= 4];
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    [self setSubmitBtnEnable:NO];
    return YES;
}

- (void)setSubmitBtnEnable:(BOOL)enable
{
    UIImage * img = nil;
    if (enable) {
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        img = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                         topCapHeight:image.size.height/2];
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        img = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                         topCapHeight:image.size.height/2];
    }
    
    _submitBtn.enabled = enable;
    [_submitBtn setBackgroundImage:img forState:UIControlStateNormal];
}


#pragma mark -
#pragma mark - timer

- (void)enableTimer
{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(checkCounter)
                                                userInfo:nil
                                                 repeats:YES];
    }
    [_timer setFireDate:[NSDate distantPast]];
}

- (void)disableTimer
{
    if (_timer) {
        if ([_timer isValid]) {
            [_timer invalidate];
        }
        _timer = nil;
    }
}

- (void)checkCounter
{
    sendCounter --;
    if (sendCounter <= 0) {
        sendCounter = SendSMS_Interval;
        [_timer setFireDate:[NSDate distantFuture]];
    }
    
    IXEmailRegStep2CellA    * cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell sendBtnTitleChanged:sendCounter];
}


#pragma mark -
#pragma mark - create UI

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds
                                                 style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[IXLoginMainCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        
        [_tableV registerClass:[IXEmailRegStep2CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXEmailRegStep2CellA class])];
    }
    
    return _tableV;
}

- (UIButton *)submitBtn{
    if (!_submitBtn) {
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                           topCapHeight:image.size.height/2];
        
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.enabled = NO;
        _submitBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
        _submitBtn.titleLabel.font = PF_REGU(15);
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
        [_submitBtn setTitle:LocalizedString(@"提 交") forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(submitBtnClk)
             forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _submitBtn;
}

- (UIView *)tableHeaderV
{
    UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(20,
                                                                (132-60)/2,
                                                                kScreenWidth - 40,
                                                                60)];
    label.text = LocalizedString(@"邮箱验证码已发送，请填写验证码");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKCellTitleColor;
    label.font = PF_MEDI(15);
    label.numberOfLines = 3;
    [v addSubview:label];
    
    return v;
}

- (UIView *)tableFooterV
{
    CGFloat height = kScreenHeight - 132 - 44*2 - kNavbarHeight;
    UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,height)];
    v.dk_backgroundColorPicker = DKViewColor;
    [v addSubview:self.submitBtn];
    
    return v;
}


#pragma mark -
#pragma mark - request

- (void)sendVericode
{
    weakself;
    [IXBORequestMgr acc_getEmailVerifyCodeWithEmail:[IXUserInfoMgr shareInstance].userLogInfo.user.email
                                           complete:^(NSString *codeId, NSString *errStr)
    {
        if (codeId.length) {
            [weakSelf enableTimer];
            [SVProgressHUD showSuccessWithStatus:LocalizedString(@"邮箱验证码成功发送")];
            weakSelf.verifyCodeId = codeId;
        } else {
            [SVProgressHUD showErrorWithStatus:errStr];
        }
    }];
}

- (void)submitVeriCode
{
    IXLoginMainCellA    * cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    if (_verifyCodeId.length && cell.textF.text.length >= 4) {
        [SVProgressHUD showWithStatus:LocalizedString(@"提交验证码...")];
        
         [IXBORequestMgr acc_checkVerifyCodeWithCode:cell.textF.text
                                              codeId:_verifyCodeId
                                            complete:^(BOOL success,
                                                       NSString *token,
                                                       NSString *errStr)
        {
             if (success) {
                 [self disableTimer];
                 [SVProgressHUD showSuccessWithStatus:LocalizedString(@"验证码正确")];
                 IXChangePhoneStep1VC *VC = [[IXChangePhoneStep1VC alloc] init];
                 [self.navigationController pushViewController:VC animated:YES];
             } else {
                 [SVProgressHUD showErrorWithStatus:errStr];
             }
         }];
    } else {
        if (!_verifyCodeId.length) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入验证码(最少四位数字)")];
        }
    }
}


@end
