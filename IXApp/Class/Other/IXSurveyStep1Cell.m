//
//  IXSurveyStep1Cell.m
//  IXApp
//
//  Created by Evn on 2017/10/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyStep1Cell.h"

@interface IXSurveyStep1Cell()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UIImageView *icon;

@end

@implementation IXSurveyStep1Cell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self icon];
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, kScreenWidth - (14 + 16 + 14.5), 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKCellTitleColor;
        _title.numberOfLines = 0;
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (14 + 16), 14, 16, 16)];
        _icon.image = AutoNightImageNamed(@"common_cell_choose");
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (void)loadUIWithText:(NSString *)title
                appear:(BOOL)appear
           numberLines:(NSInteger)numberLines
{
    self.title.text = title;
    self.icon.hidden = !appear;
    self.dk_backgroundColorPicker = DKNavBarColor;
    if (numberLines > 1) {
        self.title.frame = CGRectMake(14.5, 11, kScreenWidth - 46, numberLines*21);
    } else {
        self.title.frame = CGRectMake(14.5, 14, kScreenWidth - 46, 16);
    }
    CGFloat height = VIEW_Y(self.title)*2 + VIEW_H(self.title);
    CGRect frame = self.icon.frame;
    frame.origin.y = (height - VIEW_H(self.icon))/2;
    self.icon.frame = frame;
}

@end
