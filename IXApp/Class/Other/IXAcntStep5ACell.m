//
//  IXAcntStep5ACell.m
//  IXApp
//
//  Created by Evn on 17/3/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep5ACell.h"
#import <WebKit/WebKit.h>

@interface IXAcntStep5ACell()<UIWebViewDelegate>

@property (nonatomic, strong)UIWebView *webView;

@end

@implementation IXAcntStep5ACell

- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
        _webView.delegate = self;
        _webView.userInteractionEnabled = YES;
        _webView.scrollView.showsVerticalScrollIndicator = NO;
        _webView.scrollView.showsHorizontalScrollIndicator = NO;
//        _webView.backgroundColor = [UIColor clearColor];
        _webView.dk_backgroundColorPicker = DKTableColor;
        [_webView sizeToFit];
        [_webView scalesPageToFit];
        
        for (UIView * v in _webView.subviews) {
//            v.backgroundColor = [UIColor clearColor];
            v.dk_backgroundColorPicker = DKTableColor;
        }
        
        [self.contentView addSubview:_webView];
    }
    return _webView;
}

- (CGFloat)getHeight
{
    return 10.f;
}

- (void)setWebUrl:(NSString *)webUrl
{
    _webUrl = webUrl;
    
    [self webView];
    NSURL *url = [NSURL URLWithString:_webUrl];
    if ( url ) {
        [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

- (void)setContent:(NSString *)content
{
    _content = content;
    if ( content ) {
        [self.webView  loadHTMLString:content baseURL:nil];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    _webView.delegate = self;
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    
    CGRect frame = _webView.frame;
    frame.size.height = kScreenHeight - kNavbarHeight;
    webView.frame = frame;
    if ( _cellHeight != height ) {
        _cellHeight = height;
        CGRect frame = _webView.frame;
        frame.size.height = _cellHeight;
        webView.scrollView.contentSize = CGSizeMake(kScreenWidth - 15 * 2, _cellHeight);
        
        if ( _refreashHeight ) {
            _refreashHeight(_cellHeight);
        }
    }
    if ([IXUserInfoMgr shareInstance].isNightMode) {
        [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '100%'"];
        //字体颜色
        [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#8395a4'"];
        [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.background='#262f3e'"];
    } else {
        [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '100%'"];
        //字体颜色
        [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#4c6072'"];
        [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.background='#fafcfe'"];
    }
}


@end
