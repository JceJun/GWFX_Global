//
//  IXAcntStep2CellB.m
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep2CellB.h"
#import "UIViewExt.h"

@interface IXAcntStep2CellB ()<UITextViewDelegate>

@end

@implementation IXAcntStep2CellB

- (PHTextView *)textV
{
    if (!_textV) {
        CGRect rect = CGRectMake(85+40, 5, kScreenWidth - 70 - 20-40, 70);
        _textV = [[PHTextView alloc] initWithFrame:rect];
        _textV.placeholder = LocalizedString(@"街道门牌信息");
        _textV.font = PF_MEDI(13);
        _textV.delegate = self;
        _textV.backgroundColor = [UIColor clearColor];
        _textV.dk_textColorPicker = DKCellContentColor;
        _textV.placeholderColor = AutoNightColor(0xe2e9f1, 0x303b4d);
        
        [self.contentView addSubview:_textV];
    }
    return _textV;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 67+40, 16)];
        _desc.font = PF_MEDI(13);
        _desc.text = LocalizedString(@"详细地址");
        _desc.textAlignment = NSTextAlignmentLeft;
        _desc.dk_textColorPicker = DKCellTitleColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self desc];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewValue:tag:)]) {        
        [self.delegate textViewValue:textView.text tag:textView.tag];
    }
}

- (void)loadUIWithTextView:(NSString *)text
{
    [self bringSubviewToFront:self.textV];
    self.textV.text = text;
    
//    [_desc sizeToFit];
//    _textV._left = _desc._right + 5;
}

- (void)setTextViewEnable:(BOOL)enable
{
    self.textV.editable = enable;
}

@end
