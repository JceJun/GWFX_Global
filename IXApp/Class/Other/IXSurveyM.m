//
//  IXSurveyM.m
//  IXApp
//
//  Created by Evn on 2017/10/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyM.h"

@interface IXSurveyM()

@property (nonatomic, strong)NSMutableArray *surveyArr;

@end

@implementation IXSurveyM

- (id)init
{
    self = [super init];
    if (self) {
        _surveyArr = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)saveSurvey:(NSArray *)dataArr
{
    if (dataArr.count) {
        if (_surveyArr.count) {
            [_surveyArr removeAllObjects];
        }
        for (int i = 0; i < dataArr.count; i++) {
            [_surveyArr addObject:[self modelByQuestion:dataArr[i]]];
        }
    }
}

- (NSArray *)getSurvey
{
    return _surveyArr;
}

- (void)updateSurveyAtIndex:(NSInteger)index withQuestion:(IXQuestionM *)questionM
{
    if (index < _surveyArr.count) {
        [_surveyArr replaceObjectAtIndex:index withObject:questionM];
    }
}

- (IXQuestionM *)modelByQuestion:(NSDictionary *)dic
{
    if (!dic && dic.count == 0) {
        return nil;
    } else {
        IXQuestionM *model = [[IXQuestionM alloc] init];
        model.answer = [IXDataProcessTools dealWithNil:dic[@"answer"]];
        model.optionAppend = [IXDataProcessTools dealWithNil:dic[@"optionAppend"]];
        model.appendArr = [self splitAppendBySymbol:model.optionAppend];
        model.orderNumber = dic[@"orderNumber"];
        model.paperId = dic[@"paperId"];
        model.rightAnswer = [IXDataProcessTools dealWithNil:dic[@"rightAnswer"]];
        model.score = dic[@"score"];
        model.title = [IXDataProcessTools dealWithNil:dic[@"title"]];
        model.type = dic[@"type"];
        model.lastAnswer = dic[@"lastAnswer"];
        model.answerArr = [self splitAnswerBySymbol:model.lastAnswer withType:model.type];
        model.questionId = dic[@"id"];
        model.isRequired = dic[@"isRequired"];
        return model;
    }
}

- (NSArray *)splitAppendBySymbol:(NSString *)optionAppend
{
    if (optionAppend.length == 0) {
        return nil;
    } else {
        return [optionAppend componentsSeparatedByString:@"#-@"];
    }
}

- (NSMutableArray *)splitAnswerBySymbol:(NSString *)lastAnswer withType:(NSNumber *)type
{
    if ([type isEqualToNumber:@2]) {
        if (lastAnswer.length == 0) {
            return [NSMutableArray new];
        } else {
            return [[lastAnswer componentsSeparatedByString:@","] mutableCopy];
        }
    } else {
        return [NSMutableArray new];;
    }
}

@end


