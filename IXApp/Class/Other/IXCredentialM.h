//
//  IXCredentialM.h
//  IXApp
//
//  Created by Evn on 2017/5/5.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXCredentialM : NSObject

@property (nonatomic, copy)NSString *code;
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *nameCN;
@property (nonatomic, copy)NSString *nameEN;
@property (nonatomic, copy)NSString *nameTW;


- (id)initWithObj:(id)obj;

- (NSString *)localizedName;

@end
