//
//  IXMsgCenterVC.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXMsgCenterVC.h"
#import "IXMsgCenterDetailVC.h"
#import "IXMarkNavTitleV.h"
#import "IXMsgCenterCell.h"
#import "IXPullDownMenu.h"
#import "SVProgressHUD.h"
#import "MJRefresh.h"
#import "IXAFRequest.h"
#import "IXAppUtil.h"
#import "IXNoResultView.h"
#import "IXCpyConfig.h"
#import "IXUserInfoMgr.h"
#import "IXBORequestMgr+MsgCenter.h"

typedef NS_ENUM(NSInteger, RequestMsgListResult){
    RequestMsgListResultNoMoreData, //没有更多消息
    RequestMsgListResultNoMsgData,  //没有消息
    RequestMsgListResultNeedReload, //需要刷新列表
};

@interface IXMsgCenterVC ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout
>

@property (nonatomic, strong) UIView            * collectionBtomV;
@property (nonatomic, strong) UICollectionView  * collectionV;

/** 消息列表 */
@property (nonatomic, strong) NSMutableArray    * resultArr;
/** 无内容提示 */
@property (nonatomic, strong) IXNoResultView    * noResultView;

@property (nonatomic, strong) IXMarkNavTitleV   * navTitleView;
@property (nonatomic, strong) IXPullDownMenu    * menu;

/** 消息类型 */
@property (nonatomic, strong) NSArray           * msgTypes;
/** 记录当前加载页 */
@property (nonatomic, assign) NSInteger         currentPage;
/** 当前的消息分类 */
@property (nonatomic, copy) NSString    * currentTitle;

@end

@implementation IXMsgCenterVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_collectionV reloadData];
    [self refreshTitle];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    NSArray * arr = self.navigationController.viewControllers;
    if (!arr && self.backBlock) {
        //因为有点击按钮和手势两种返回上一层的方式，所以在此处理返回上一页逻辑
        self.backBlock();
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.titleView = self.navTitleView;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(leftNavBtnClick)];
    self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"全部已读") 
                                                                            target:self
                                                                               sel:@selector(rightNavBtnClick)];
    _currentPage = 1;
    _resultArr = [@[] mutableCopy];
    _currentTitle = LocalizedString(@"全部消息");
    
    [self.view addSubview:self.collectionBtomV];
    [self.view addSubview:self.collectionV];
    
    _msgTypes = [IXBORequestMgr msg_refreshMsgTypes:^(NSArray<NSString *> *types, NSString *errorStr) {
        [SVProgressHUD dismiss];
        if (types && types.count) {
            _msgTypes = types;
            [self sortMsgTypes];
            [self.collectionV.header beginRefreshing];
        }
        else if (errorStr.length) {
            [SVProgressHUD showErrorWithStatus:errorStr];
        }
        else if (!_msgTypes.count){
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"请求失败")];
        }
    }];
    
    if (_msgTypes.count) {
        [self sortMsgTypes];
        [self.collectionV.header beginRefreshing];
    }else{
        [SVProgressHUD show];
    }
}

- (void)sortMsgTypes
{
    _msgTypes = [_msgTypes sortedArrayUsingComparator:^NSComparisonResult(IXMsgCenterTypeM *  _Nonnull obj1,
                                                             IXMsgCenterTypeM *  _Nonnull obj2) {
        if (obj1.idx < obj2.idx){
            return NSOrderedAscending;
        }
        
        return NSOrderedDescending;
    }];
}

#pragma mark -
#pragma mark - btn action

- (void)leftNavBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightNavBtnClick
{
    [self readAllMessage];
}

#pragma mark -
#pragma mark - request

- (void)readAllMessage
{
    [IXBORequestMgr msg_readAllMsg:^(BOOL success, NSString * errStr) {
        if (success) {
            [self.resultArr enumerateObjectsUsingBlock:^(IXMsgCenterM *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isKindOfClass:[IXMsgCenterM class]]) {
                    obj.readStatus = YES;
                }
            }];
            _noReadCount = 0;
            [self.collectionV reloadData];
            [self refreshTitle];
            if (self.readAllBlock) {
                self.readAllBlock();
            }
        } else if (errStr.length) {
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"请求失败")];
        }
    }];
}

- (void)requestData
{
    if (_currentPage <= 1) {
        [self.resultArr removeAllObjects];
        [self hideNoresultAlert];
    }
    
    weakself;
    [IXBORequestMgr msg_obtainMsgListAndNoReadCountWithType:[self msgTypeWithTitle:_currentTitle] page:_currentPage complete:^(NSArray<IXMsgCenterM *> *lst, NSInteger noReadCount) {
        weakSelf.noReadCount = noReadCount;
        
        RequestMsgListResult    resultStatus;
        if (lst) {
            [weakSelf.resultArr addObjectsFromArray:lst];
            
            if (self.resultArr.count == 0) {
                resultStatus = RequestMsgListResultNoMsgData;
            }else if (lst.count < 20 || self.resultArr.count < 20) {
                resultStatus = RequestMsgListResultNoMoreData;
            }else{
                resultStatus = RequestMsgListResultNeedReload;
            }
        }else{
            if ([weakSelf.resultArr count] > 0) {
                resultStatus = RequestMsgListResultNoMoreData;
            }else{
                resultStatus = RequestMsgListResultNoMsgData;
            }
        }
        
        switch (resultStatus) {
            case RequestMsgListResultNoMsgData:{
                [weakSelf showNoresultAlert];
                break;
            }
            case RequestMsgListResultNeedReload:{
                [weakSelf.collectionV reloadData];
                break;
            }
            case RequestMsgListResultNoMoreData:{
                if (weakSelf.resultArr.count >= 20){
                    [weakSelf.collectionV.footer endRefreshingWithNoMoreData];
                }else{
                    weakSelf.collectionV.footer = nil;
                }
                [weakSelf.collectionV reloadData];
                break;
            }
        }
        [weakSelf endDataRefresh];
        [weakSelf refreshTitle];
    }];
}


#pragma mark -
#pragma mark - change UI

- (void)showNoresultAlert
{
    self.noResultView.alpha = 0;
    [self.view addSubview:self.noResultView];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.collectionV.alpha  = 0;
        self.noResultView.alpha    = 1;
    } completion:^(BOOL finished) {
        self.collectionV.hidden = YES;
        self.collectionV.alpha  = 1.f;
    }];
}

- (void)hideNoresultAlert
{
    if (self.collectionV.hidden) {
        [self.collectionV reloadData];
        self.collectionV.hidden = NO;
        [self.noResultView removeFromSuperview];
    }
}

- (void)endDataRefresh
{
    if (_collectionV.footer.isRefreshing) {
        [_collectionV.footer endRefreshing];
    }
    
    if (_collectionV.header.isRefreshing){
        [_collectionV.header endRefreshing];
        
        //下拉加载之后消除footer的没有更多消息状态
        if (_collectionV.footer) {
            [_collectionV.footer resetNoMoreData];
        }
    }
}

- (void)refreshTitle
{
    if (_noReadCount > 0) {
        [_navTitleView setTitleContent:[NSString stringWithFormat:@"%@(%ld)",
                                        _currentTitle,
                                        (long)_noReadCount]];
    }else{
        [_navTitleView setTitleContent:_currentTitle];
    }
}

#pragma mark -
#pragma mark - collection view

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [self.resultArr count];
}

static  NSString    * cellIdent = @"msgcell";
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IXMsgCenterCell    * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdent
                                                                               forIndexPath:indexPath];
    if ([self.resultArr count] > indexPath.row) {
        cell.message = self.resultArr[indexPath.row];
    }
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IXMsgCenterM    * model = self.resultArr[indexPath.row];
    
    CGRect rect = [model.msgContent boundingRectWithSize:CGSizeMake(kScreenWidth - 48, MAXFLOAT)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName :PF_MEDI(14)}
                                                 context:nil];
    if (rect.size.height > 45) {
        rect.size.height = 55;
    }
    return CGSizeMake(kScreenWidth, rect.size.height + 80);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    IXMsgCenterM    * msg = self.resultArr[indexPath.row];
    if (!msg.readStatus) {
        msg.readStatus = YES;
        _noReadCount --;
    }
    
    IXMsgCenterDetailVC * detailVC = [IXMsgCenterDetailVC new];
    detailVC.message = msg;
    [self.navigationController pushViewController:detailVC animated:YES];
}


#pragma mark -
#pragma mark - menu

- (void)showMenu
{
    [self.menu showMenuFrom:self.navTitleView items:[self menuItem] offsetY:2];
}

- (void)menuItemClicked:(NSInteger)index title:(NSString *)title
{
    _currentPage = 1;
    if (![LocalizedString(@"_currentMsgType") isEqualToString:title]) {
        _noReadCount = 0;
    }
    _currentTitle = title;
    [self refreshTitle];
    [self.collectionV.header beginRefreshing];
}


#pragma mark -
#pragma mark - lazy loading

- (UIView *)collectionBtomV
{
    if (!_collectionBtomV) {
        _collectionBtomV = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                    0,
                                                                    kScreenWidth,
                                                                    kScreenHeight - kNavbarHeight - kBtomMargin)];
        _collectionBtomV.backgroundColor = [UIColor clearColor];
        _collectionBtomV.clipsToBounds = YES;
    }
    
    return _collectionBtomV;
}

- (UICollectionView *)collectionV
{
    if (!_collectionV) {
        UICollectionViewFlowLayout  * layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 10.f;
        layout.minimumInteritemSpacing = 10.f;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.itemSize = CGSizeZero;
        
        _collectionV = [[UICollectionView alloc] initWithFrame:CGRectMake(0,
                                                                          0,
                                                                          kScreenWidth,
                                                                          kScreenHeight - kNavbarHeight - kBtomMargin)
                                          collectionViewLayout:layout];
        [_collectionV registerClass:[IXMsgCenterCell class] forCellWithReuseIdentifier:cellIdent];
        _collectionV.dataSource = self;
        _collectionV.delegate   = self;
        _collectionV.dk_backgroundColorPicker   = DKTableColor;
        _collectionV.showsHorizontalScrollIndicator = NO;
//        _collectionV.contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
        _collectionV.footer = [self footer];
        _collectionV.header = [self header];
        
        [self.view addSubview:_collectionV];
    }
    
    return _collectionV;
}

- (IXNoResultView *)noResultView
{
    if (!_noResultView) {
        _noResultView = [IXNoResultView noResultViewWithFrame:self.view.bounds
                                                        image:AutoNightImageNamed(@"msgCenter_noMsg")
                                                        title:LocalizedString(@"暂无消息")];
        _noResultView.dk_backgroundColorPicker = DKTableColor;
    }
    
    return _noResultView;
}

- (IXMarkNavTitleV *)navTitleView
{
    if (!_navTitleView) {
        _navTitleView = [[IXMarkNavTitleV alloc] initWithFrame:CGRectMake(100,
                                                                          kNavbarHeight - 41,
                                                                          kScreenWidth - 200,
                                                                          41)];
        if (!_noReadCount){
            [_navTitleView setTitleContent:_currentTitle];
        }else{
            [_navTitleView setTitleContent:[NSString stringWithFormat:@"%@(%ld)",
                                            _currentTitle,
                                            (long)_noReadCount]];
        }
        weakself;
        _navTitleView.tapContent = ^(){
            [weakSelf showMenu];
        };
    }
    
    return _navTitleView;
}

- (IXPullDownMenu *)menu
{
    if (!_menu) {
        
        _menu = [IXPullDownMenu menuWithMenuItems:[self menuItem]
                                        rowHeight:[self menuItemHeight] + 25
                                            width:[self menuItemWidth] + 40];
        
        weakself;
        _menu.itemClicked = ^(NSInteger index ,NSString * title) {
            [weakSelf menuItemClicked:index title:title];
        };
    }
    
    return _menu;
}


#pragma mark -
#pragma mark - other

- (MJRefreshNormalHeader *)header
{
    weakself;
    MJRefreshNormalHeader    * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentPage = 1;
        [weakSelf requestData];
    }];
    header.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return header;
}

- (MJRefreshBackNormalFooter *)footer
{
    weakself;
    MJRefreshBackNormalFooter   * footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage ++;
        [weakSelf requestData];
    }];
    
    footer.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    footer.automaticallyHidden = YES;
    
    return footer;
}

- (CGFloat)menuItemWidth
{
    CGFloat width = 0;
    
    for (NSString * itemStr in [self menuItem]) {
        CGSize size = [itemStr sizeWithAttributes:@{NSFontAttributeName : PF_MEDI(13)}];
        if (size.width > width) {
            width = size.width;
        }
    }
    
    return width;
}

- (CGFloat)menuItemHeight
{
    CGSize size =  [@"hello" sizeWithAttributes:@{NSFontAttributeName : PF_MEDI(13)}];
    return size.height;
}

- (NSArray *)menuItem
{
    __block NSMutableArray  * menuItems = [@[LocalizedString(@"全部消息")] mutableCopy];
    [self.msgTypes enumerateObjectsUsingBlock:^(IXMsgCenterTypeM *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [menuItems addObject:[obj localizedName]];
    }];
    
    return menuItems;
}


- (NSString *)msgTypeWithTitle:(NSString *)title
{
    __block NSString * type = @"";
    [self.msgTypes enumerateObjectsUsingBlock:^(IXMsgCenterTypeM * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([title isEqualToString:[obj localizedName]]) {
            type = obj.code;
            *stop = YES;
        }
    }];
    
    return type;
}

@end


