//
//  IXBroadsideCell.h
//  IXApp
//
//  Created by Evn on 16/12/8.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"
@class IXBroadsideCellM;

#define IXBroadsideCellHeight 55

@interface IXBroadsideCell : IXClickTableVCell

- (void)config:(IXBroadsideCellM *)model;

- (void)setNumber:(NSString *)num;

@end

@interface IXBroadsideCellM : NSObject

@property (nonatomic, copy) NSString    * imgStr;
@property (nonatomic, copy) NSString    * dImgStr;
@property (nonatomic, copy) NSString    * title;
@property (nonatomic, copy) NSString    * number;
@property (nonatomic, assign) BOOL      isLine;

@end

