//
//  GesturePWdData.m
//  AliPayDemo
//
//  Created by pg on 15/7/15.
//  Copyright (c) 2015年 pg. All rights reserved.
//

#import "GesturePWdData.h"

#define kFormerKey [NSString stringWithFormat:@"pwdKey_%@",[IXUserInfoMgr shareInstance].userLogInfo.user.customerNo]

#define kPwdKey [NSString stringWithFormat:@"pwdKey_%llu",[IXUserInfoMgr shareInstance].userLogInfo.user.id_p]

@implementation GesturePWdData

+ (void)fixGesturePwdBugAction
{
    NSString *oldStr = [self objectForKey:kFormerKey];
    if (oldStr.length && [oldStr isKindOfClass:[NSString class]]) {
        [self setObject:oldStr forKey:kPwdKey];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kFormerKey];
        
        id obj = [self objectForKey:KFormerSaveGesLock];
        if (obj && [obj boolValue]){
            [self setObject:obj forKey:KSaveGesLock];
        }
    }else{
        id obj = [self objectForKey:KFormerSaveThumbLock];
        if (obj && [obj boolValue]) {
            [self setObject:@(YES) forKey:KSaveThumbLock];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KFormerSaveThumbLock];
        }
    }
}

+ (void)setObject:(id)object forKey:(id)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
}

+ (id)objectForKey:(id)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (BOOL)hasSeted;
{
    NSString *oldStr = [self objectForKey:kPwdKey];
    if (oldStr.length && [oldStr isKindOfClass:[NSString class]]) {
        return YES;
    }

    return NO;
}


+ (void)removePwd
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPwdKey];
}

+ (void)setPwd:(NSString *)str
{
    [self setObject:str forKey:kPwdKey];
}

+ (NSString *)currentPwd
{
    NSString    * oldStr = [self objectForKey:kPwdKey];
    
    return oldStr;
}

@end
