//
//  IXDeviceMgrInfoVC.m
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDeviceMgrInfoVC.h"
#import "IXDeviceMgrInfoCell.h"

@interface IXDeviceMgrInfoVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation IXDeviceMgrInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"设备详情");
    [self initData];
    [self.tableV reloadData];
    
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initData
{
    _titleArr = @[
                  @[LocalizedString(@"设备名称")],
                  @[LocalizedString(@"最后登陆时间")]
                 ];
    _dataArr = @[
                 @[[IXDataProcessTools dealWithNil:_devDic[@"devName"]]],
                 @[[IXEntityFormatter timeIntervalToString:[_devDic[@"lastLoginTime"] doubleValue]]]
                 ];
}

#pragma mark -
#pragma mark - lazy load

- (UITableView *)tableV
{
    if(!_tableV)
    {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight - 100)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV registerClass:[IXDeviceMgrInfoCell class] forCellReuseIdentifier:NSStringFromClass([IXDeviceMgrInfoCell class])];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}


#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.titleArr  count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [(NSArray *)self.titleArr[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 10)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDeviceMgrInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDeviceMgrInfoCell class])];
    cell.dk_backgroundColorPicker = DKNavBarColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell reloadUIWithTitle:((NSArray *)(_titleArr[indexPath.section]))[indexPath.row] content:((NSArray *)(_dataArr[indexPath.section]))[indexPath.row] indexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

@end
