//
//  IXAcntStep4CellA.h
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXAcntStep4CellA : UITableViewCell

- (void)loadUIWithText:(NSString *)title
             logoImage:(UIImage *)logoImg
             iconImage:(UIImage *)iconImg
          isHiddenIcon:(BOOL)isHidden;

@end
