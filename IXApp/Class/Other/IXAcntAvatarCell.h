//
//  IXAcntAvatarCell.h
//  IXApp
//
//  Created by Seven on 2017/12/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXClickTableVCell.h"

#define kAvatarH    70

@interface IXAcntAvatarCell : IXClickTableVCell

@property (nonatomic, strong) UILabel   * titleLab;
@property (nonatomic, strong) UIImageView   * avatarV;
@property (nonatomic, strong) UIImageView   * nextImgV;

@end
