//
//  IXAcntStep2VC.h
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXOpenAcntDataModel.h"
#import "IXEvaluationM.h"

@interface IXAcntStep2VC : IXDataBaseVC
@property (nonatomic, strong)IXOpenAcntDataModel *acntModel;
@property (nonatomic, strong)IXEvaluationM *evaluationM;
@end
