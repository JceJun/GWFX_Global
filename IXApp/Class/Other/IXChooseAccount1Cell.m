//
//  IXChooseAccount1Cell.m
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXChooseAccount1Cell.h"

@interface IXChooseAccount1Cell ()

@property (nonatomic, strong) UIImageView *statusImg;
@property (nonatomic, strong) UIImageView *chooseImg;

@property (nonatomic, strong) UILabel *currencyTitleLbl;

@end

@implementation IXChooseAccount1Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.dk_backgroundColorPicker = DKNavBarColor;
    }
    return self;
}


- (void)setModel:(NSDictionary *)model
{
    if ( model ) {
        _model = model;
        
        [self.statusImg setImage:GET_IMAGE_NAME([IXUtils tranImageName:@"switchAccount_anatar"])];
        self.currencyTitleLbl.text = LocalizedString(@"模拟账户");
        
        [self.chooseImg setImage:AutoNightImageNamed(@"common_cell_choose")];
        [self.chooseImg setHidden:!([[model stringForKey:@"id"] longLongValue] ==
                                    [IXUserInfoMgr shareInstance].userLogInfo.account.id_p)];
    }else{
        [self.statusImg setImage:AutoNightImageNamed(@"switchAccount_add")];
        self.currencyTitleLbl.text = LocalizedString(@"添加新的真实账户");
    }
}


- (void)setIsCurrentAccount:(BOOL)isCurrentAccount
{
    [self.chooseImg setHidden:!isCurrentAccount];
}

- (UIImageView *)statusImg
{
    if ( !_statusImg ) {
        _statusImg = [[UIImageView alloc] initWithFrame:CGRectMake( 20, 17, 21, 21)];
        [self.contentView addSubview:_statusImg];
    }
    return _statusImg;
}


- (UIImageView *)chooseImg
{
    if ( !_chooseImg ) {
        _chooseImg = [[UIImageView alloc] initWithFrame:CGRectMake( kScreenWidth - 111, 19, 16, 16)];
        [self.contentView addSubview:_chooseImg];
    }
    return _chooseImg;
}
- (UILabel *)currencyTitleLbl
{
    if ( !_currencyTitleLbl ) {
        _currencyTitleLbl = [IXUtils createLblWithFrame:CGRectMake( 51, 20, 180, 15)
                                               WithFont:PF_MEDI(13)
                                              WithAlign:NSTextAlignmentLeft
                                             wTextColor:0x4c6072
                                             dTextColor:0xe9e9ea];
        [self.contentView addSubview:_currencyTitleLbl];
    }
    return _currencyTitleLbl;
}


@end
