//
//  IXSubQuoteStyle4Cell.h
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

#define OPERATIONTAG 1000
typedef NS_ENUM(NSInteger, QUOTESTATE) {
    QUOTESTATEUNSUB,
    QUOTESTATESUB,
};

@interface IXSubQuoteStyle4Cell : UITableViewCell

@property (nonatomic, assign) QUOTESTATE state;

@end
