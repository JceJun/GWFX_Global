//
//  GesturePwdVC.h
//  IXApp
//
//  Created by Seven on 2017/3/30.
//  Copyright © 2017年 IX. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GesturePwdVCState) {
    GesturePwdVCStateSetting,   //设置手势密码
    GesturePwdVCStateConfirm,   //验证手势密码    //显示用户信息，以present方式推出
};

typedef void (^confirmFailBlock)();
typedef void (^clickReloginBtnBlock)();
typedef void (^cancelSettingPwd)();

@interface GesturePwdVC : IXDataBaseVC


/** 页面类型，设置or验证 */
@property (nonatomic, assign) GesturePwdVCState state;

@property (nonatomic, copy) confirmFailBlock    confirmFailBlock;
@property (nonatomic, copy) clickReloginBtnBlock    reloginBlock;
@property (nonatomic, copy) cancelSettingPwd    cancelSetting;

/**
 初始化方法
 
 @param state 页面类型
 @return obj
 */
- (instancetype)initWithState:(GesturePwdVCState)state;


@end
