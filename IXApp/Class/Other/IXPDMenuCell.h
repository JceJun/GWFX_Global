//
//  IXPDMenuCell.h
//  IXApp
//
//  Created by Seven on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 下拉菜单Cell */
@interface IXPDMenuCell : UITableViewCell

@property (nonatomic, copy) NSString    * title;
@property (nonatomic, strong) NSAttributedString    * attributeTitle;

@property (nonatomic, strong) UIColor   * seletedColor;
@property (nonatomic, strong) UIColor   * commonColor;
@property (nonatomic, strong) UIColor   * btomLineColor;

@end
