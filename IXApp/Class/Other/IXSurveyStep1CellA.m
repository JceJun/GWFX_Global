//
//  IXSurveyStep1CellA.m
//  IXApp
//
//  Created by Evn on 2017/10/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyStep1CellA.h"

@interface IXSurveyStep1CellA()

@property (nonatomic, strong)UILabel *title;

@end

@implementation IXSurveyStep1CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, kScreenWidth - 29, 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKCellContentColor;
        _title.textAlignment = NSTextAlignmentLeft;
        _title.numberOfLines = 0;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (void)loadUIWithText:(NSString *)text
           numberLines:(NSInteger)numberLines
{
    self.title.text = text;
    if (numberLines > 1) {
        self.title.frame = CGRectMake(14.5, 11, kScreenWidth - 29, numberLines*21);
    } else {
        self.title.frame = CGRectMake(14.5, 14, kScreenWidth - 29, 16);
    }
    
}


@end
