//
//  IXAcntBindStep1VC.m
//  IXApp
//
//  Created by Evn on 2017/5/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntBindStep1VC.h"
#import "IXAcntBindStep2AVC.h"
#import "IXChangePhoneStep1VC.h"
#import "IXOpenChooseContentView.h"

@interface IXAcntBindStep1VC ()

@end

@implementation IXAcntBindStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleStr = [IXDataProcessTools bindTypeTitle];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    [self.view addSubview:[self headerV]];
    [self.view addSubview:[self nextBtn]];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UIView *)headerV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, kScreenWidth, 132)];
    v.backgroundColor = CellBackgroundColor;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-41)/2, kScreenWidth, 18)];
    label.text = [NSString stringWithFormat:@"%@%@",LocalizedString(@"你已绑定的"),[IXDataProcessTools bindTypeTitle]];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = MarketSymbolNameColor;
    label.font = PF_MEDI(18);
    [v addSubview:label];
    
    UILabel *valueLbl = [IXCustomView createLable:CGRectMake(0, GetView_MaxY(label) + 5, kScreenWidth, 18) title:[IXDataProcessTools bingType] font:PF_MEDI(18) textColor:MarketSymbolNameColor textAlignment:NSTextAlignmentCenter];
    [v addSubview:valueLbl];
    return v;
}

- (UIButton *)nextBtn
{
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, 132 + kNavbarHeight, kScreenWidth - 31, 44);
    [nextBtn addTarget:self
                   action:@selector(nextBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setTitle:[NSString stringWithFormat:@"%@%@",LocalizedString(@"更换"),[IXDataProcessTools bindTypeTitle]]
                forState:UIControlStateNormal];
    nextBtn.titleLabel.font = PF_REGU(18);
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    [nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    return nextBtn;
}

- (void)verifyIDAction
{
    NSArray * arr = @[
                      LocalizedString(@"通过手机验证身份"),
                      LocalizedString(@"通过邮箱验证身份")
                      ];
    IXOpenChooseContentView * chooseV = [[IXOpenChooseContentView alloc] initWithDataSource:arr
                                                                            WithSelectedRow:^(NSInteger row) {
                                                                                if (row == 0){
                                                                            
                                                                                }else{
                                                                               
                                                                                }
                                                                            }];
    [chooseV show];
}

- (void)nextBtnClk
{
//    IXAcntBindStep2AVC *VC = [[IXAcntBindStep2AVC alloc] init];
//    [self.navigationController pushViewController:VC animated:YES];
    IXChangePhoneStep1VC *VC = [[IXChangePhoneStep1VC alloc] init];
    [self.navigationController pushViewController:VC animated:YES];

}

@end
