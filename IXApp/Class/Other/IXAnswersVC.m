//
//  IXAnswersVC.m
//  IXApp
//
//  Created by Evn on 2017/7/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAnswersVC.h"
#import "IXAnswersCell.h"
#import "IXAnswersCellA.h"
#import "IXBORequestMgr+BroadSide.h"
#import "IXTouchTableV.h"
#import "ChatTableViewController.h"
#import "IXAnswersInfoVC.h"
#import "IXCpyConfig.h"
#import "MJRefresh.h"
#import "NSObject+IX.h"
#import "IXBtomBtnV.h"


@interface IXAnswersVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) IXTouchTableV *tableV;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableDictionary *sDic;
@property (nonatomic, strong) NSMutableDictionary *contentDic;
@property (nonatomic, assign) int currentPage;
@property (nonatomic, assign) int pageSize;

@end

@implementation IXAnswersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    self.title = LocalizedString(@"常见问答");
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.tableV];
    _currentPage = 1;
    _pageSize = 20;
    [self addContactBtn];
    [self loadAskedAndQuestionList];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (NSMutableDictionary *)contentDic
{
    if (!_contentDic) {
        _contentDic = [[NSMutableDictionary alloc] init];
    }
    return _contentDic;
}

#pragma mark 加载问答信息
- (void)loadAskedAndQuestionList
{
    NSDictionary *paramDic = @{
                               @"pageNo":[NSString stringWithFormat:@"%d",_currentPage],
                               @"pageSize":[NSString stringWithFormat:@"%d",_pageSize]
                               };
    
    weakself;
    [IXBORequestMgr bs_askedAndQuestionsListWithParam:paramDic
     
                                               result:^(BOOL success,
                                                        NSString *errCode,
                                                        NSString *errStr,
                                                        id obj)
     {
         if (weakSelf.tableV.header.isRefreshing) {
             [weakSelf.tableV.header endRefreshing];
         }
         
         weakSelf.currentPage--;
         if (success) {
             if (obj && [obj ix_isDictionary]) {
                 NSArray * resultList = obj[@"resultList"];
                 if ([resultList ix_isArray] && [resultList count]) {
                     weakSelf.currentPage++;
                     if (weakSelf.currentPage == 1 && weakSelf.dataArr.count) {
                         [weakSelf.dataArr removeAllObjects];
                         [weakSelf.tableV reloadData];
                     }
                     [weakSelf.dataArr addObjectsFromArray:resultList];
                     [weakSelf.tableV.footer endRefreshing];
                 } else {
                     if (weakSelf.currentPage >= 1) {
                         [weakSelf.tableV.footer endRefreshingWithNoMoreData];
                     }
                 }
             } else {
                 if (weakSelf.currentPage >= 1) {
                     [weakSelf.tableV.footer endRefreshingWithNoMoreData];
                 }
             }
         } else {
             [SVProgressHUD showErrorWithStatus:errStr];
         }
         [weakSelf.tableV reloadData];
     }];
}

#pragma mark 加载问答信息
- (void)askedAndQuestionsDetailSection:(NSInteger)section
{
    NSDictionary    * dic = self.dataArr[section];
    NSDictionary    * paramDic = @{
                                   @"id":dic[@"id"]
                                   };
    
    weakself;
    [IXBORequestMgr bs_askedAndQuestionsDetailWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
     {
         if (success) {
             if ([obj ix_isDictionary]) {
                 NSDictionary    * infoDic = obj[@"InfomationCenter"];
                 if ([infoDic ix_isDictionary] && infoDic[@"content"]) {
                     [weakSelf.contentDic setObject:[IXDataProcessTools dealWithNil:infoDic[@"content"]] forKey:@(section)];
                     [weakSelf.tableV reloadData];
                 }
             }
         } else {
             if ([errCode length]) {
                 [SVProgressHUD showErrorWithStatus:errStr];
             } else {
                 [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
             }
         }
     }];
}

- (NSMutableDictionary *)sDic
{
    if (!_sDic) {
        _sDic = [[NSMutableDictionary alloc] init];
    }
    return _sDic;
}

- (IXTouchTableV *)tableV
{
    if (!_tableV){
        CGSize  size = [IXBtomBtnV targetSize];
        _tableV = [[IXTouchTableV alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, kScreenHeight - kNavbarHeight - size.height)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV registerClass:[IXAnswersCell class] forCellReuseIdentifier:NSStringFromClass([IXAnswersCell class])];
        _tableV.header = [self header];
        _tableV.footer = [self footer];
    }
    return _tableV;
}

- (MJRefreshNormalHeader *)header
{
    weakself;
    MJRefreshNormalHeader  *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentPage = 1;
        [weakSelf loadAskedAndQuestionList];
    }];
    header.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return header;
}

- (MJRefreshBackNormalFooter *)footer
{
    weakself;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
        [weakSelf loadAskedAndQuestionList];
    }];
    footer.activityIndicatorViewStyle = [IXUserInfoMgr shareInstance].isNightMode ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    return footer;
}


- (void)addContactBtn
{
    CGSize  size = [IXBtomBtnV targetSize];
    IXBtomBtnV  * btomV = [[IXBtomBtnV alloc] initWithFrame:CGRectMake(0, kScreenHeight - kNavbarHeight - size.height, size.width, size.height)];
    [btomV.btn setTitle:LocalizedString(@"联系客服") forState:UIControlStateNormal];
    [btomV.btn addTarget:self action:@selector(contactBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btomV];
    self.view.clipsToBounds = YES;
}

- (void)contactBtnClk
{
    ChatTableViewController *VC = [[ChatTableViewController alloc] initWithPid:ServicePid
                                                                           key:ServiceKey
                                                                           url:kServiceUrl];
    [self.navigationController pushViewController:VC animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"IXAnswersCell";
    IXAnswersCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXAnswersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKTableHeaderColor;
    NSDictionary *dic = self.dataArr[indexPath.row];
    [cell reloadUI:dic[@"title"] indexPath:indexPath counts:(self.dataArr.count - 1)];
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAnswersInfoVC *VC = [[IXAnswersInfoVC alloc] init];
    NSDictionary *dic = self.dataArr[indexPath.row];
    VC.idStr = dic[@"id"];
    VC.answtTitle = [IXDataProcessTools dealWithNil:dic[@"title"]];
    [self.navigationController pushViewController:VC animated:YES];
}



@end
