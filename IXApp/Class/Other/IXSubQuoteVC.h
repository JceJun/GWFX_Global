//
//  IXSubQuoteVC.h
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

#import "IXSubQuoteStyle1Cell.h"
#import "IXSubQuoteStyle2Cell.h"
#import "IXSubQuoteStyle3Cell.h"
#import "IXSubQuoteStyle4Cell.h"
#import "IXSubQuoteStyle5Cell.h"

@interface IXSubQuoteVC : IXDataBaseVC


@end
