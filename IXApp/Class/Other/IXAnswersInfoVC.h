//
//  IXAnswersInfoVC.h
//  IXApp
//
//  Created by Evn on 2017/7/26.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXAnswersInfoVC : IXDataBaseVC
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) NSString *answtTitle;
@end
