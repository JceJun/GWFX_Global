//
//  IXAcntBankListCellA.m
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntBankListCellA.h"

@interface IXAcntBankListCellA()

@property (nonatomic,strong)UIImageView *lIcon;
@property (nonatomic,strong)UILabel *title;
@property (nonatomic,strong)UIImageView *rIcon;
@property (nonatomic,strong)UIView *uLine;
@property (nonatomic,strong)UIView *dLine;

@end

@implementation IXAcntBankListCellA

- (void)layoutSubviews {
    [super layoutSubviews];
    [self lIcon];
    [self title];
    [self rIcon];
}

- (UIImageView *)lIcon
{
    if (!_lIcon) {
        CGRect rect = CGRectMake(15, 12, 20, 20);
        _lIcon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_lIcon];
    }
    return _lIcon;
}

- (UIImageView *)rIcon
{
    if (!_rIcon) {
        CGRect rect = CGRectMake(kScreenWidth - (7.5 + 16), 15, 7.5, 13.5);
        _rIcon = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_rIcon];
    }
    return _rIcon;
}

- (UILabel *)title
{
    if (!_title) {
        CGRect rect = CGRectMake(45, 15, 180, 14);
        _title = [[UILabel alloc] initWithFrame:rect];
        _title.font = PF_MEDI(13);
        _title.textAlignment = NSTextAlignmentLeft;
        _title.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UIView *)uLine
{
    if (!_uLine) {
        CGRect rect = CGRectMake(0, 0, kScreenWidth, kLineHeight);
        _uLine = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_uLine];
    }
    return _uLine;
}

- (UIView *)dLine {
    
    if (!_dLine) {
        CGRect rect = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
        _dLine = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_dLine];
    }
    return _dLine;
}

- (void)reloadUIWithTitle:(NSString *)title
{
    self.uLine.dk_backgroundColorPicker = DKLineColor;
    self.dLine.dk_backgroundColorPicker = DKLineColor;
    self.title.text = title;
    self.lIcon.image = [UIImage imageNamed:@"search_add"];
    self.rIcon.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
    self.dk_backgroundColorPicker = DKNavBarColor;
}

@end
