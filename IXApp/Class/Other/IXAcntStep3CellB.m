//
//  IXAcntStep3CellB.m
//  IXApp
//
//  Created by Magee on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep3CellB.h"

@interface IXAcntStep3CellB()

@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UIImageView *icon;

@end

@implementation IXAcntStep3CellB

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self icon];
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, kScreenWidth - (46), 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKCellTitleColor;
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (14 + 16), 14, 16, 16)];
        _icon.image = AutoNightImageNamed(@"common_cell_choose");
        [self.contentView addSubview:_icon];
    }
    return _icon;
}

- (void)loadUIWithText:(NSString *)title
            hilightTag:(NSInteger)htag
{    
    self.title.text = title;
    self.icon.hidden = (self.tag == htag)? NO : YES;
    self.dk_backgroundColorPicker = DKNavBarColor;
}

@end
