//
//  IXVerifyStep2VC.h
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXVerifyStepVC : IXDataBaseVC

@property (nonatomic, copy)  NSString *phoneNum;
@property (nonatomic, strong)NSString *phoneId;
@property (nonatomic, strong)NSString *passWord;

@end
