//
//  IXAcntStep6VC.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep6VC.h"
#import "IXTouchTableV.h"
#import "IXAcntStep7VC.h"
#import "IXIncashStep1VC.h"
#import "AppDelegate+UI.h"
#import "SFHFKeychainUtils.h"
#import "IXDPSChannelVC.h"

@interface IXAcntStep6VC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong)IXTouchTableV *tableV;
@end

@implementation IXAcntStep6VC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addBackItemrget:self action:@selector(leftBtnItemClicked)];
    if (_acntModel.state != 2) {
        [self addDismissItemWithTitle:LocalizedString(@"完成")
                               target:self
                               action:@selector(rightBtnItemClicked)];
    }
    
    
    self.title = LocalizedString(@"开立真实账户");
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[UITableViewCell class]
        forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableColor;
    
    UIImageView *logoImgV = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 80)/2, 45, 80, 80)];
    logoImgV.image = GET_IMAGE_NAME(@"openAccount_complete");
    [v addSubview:logoImgV];
    
    UILabel *stateLbl = [IXCustomView createLable:CGRectMake(15.5,
                                                             GetView_MaxY(logoImgV) + 19.5,
                                                             kScreenWidth - 15.5*2,
                                                             18)
                                            title:LocalizedString(@"账户申请成功提交")
                                             font:PF_MEDI(15)
                                       wTextColor:0x4c6072
                                       dTextColor:0xd4d5dc
                                    textAlignment:NSTextAlignmentCenter];
    [v addSubview:stateLbl];
    
    NSString *accStr = nil,*showStr = nil;
    if (_acntModel.state == 0) {
        accStr = [NSString stringWithFormat:@"%@:%@",LocalizedString(@"账户号码"),
                  _acntModel.accountId];
        showStr = LocalizedString(@"您现在就可以进行注资和交易操作");
    } else if (_acntModel.state == 2) {
        stateLbl.text = LocalizedString(@"提交成功，人工审核中");
        accStr = LocalizedString(@"您的开户申请已提交人工审核，请耐心等待");
    } else {
        accStr = LocalizedString(@"此账户在移动端暂不可登录,");
        showStr = LocalizedString(@"您可以到Web端进行注资和交易操作");
    }
    UILabel *accLbl = [IXCustomView createLable:CGRectMake(15.5,
                                                           GetView_MaxY(stateLbl) + 14.5,
                                                           kScreenWidth - 15.5*2,
                                                           12)
                                          title:accStr
                                           font:PF_MEDI(12)
                                     wTextColor:0x99abba
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentCenter];
    [v addSubview:accLbl];
    if (_acntModel.state != 2) {
        UILabel *showLbl = [IXCustomView createLable:CGRectMake(15.5,
                                                                GetView_MaxY(accLbl) + 8.5,
                                                                kScreenWidth - 15.5*2,
                                                                12)
                                               title:showStr
                                                font:PF_MEDI(12)
                                          wTextColor:0x99abba
                                          dTextColor:0x8395a4
                                       textAlignment:NSTextAlignmentCenter];
        [v addSubview:showLbl];
    } else {
        accLbl.numberOfLines = 0;
        CGRect frame = accLbl.frame;
        frame.size.height = 50;
        accLbl.frame = frame;
    }
    
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight - 132 -kNavbarHeight)];
    v.dk_backgroundColorPicker = DKTableColor;
    
    UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    if (_acntModel.state == 0) {
        [nextBtn setTitle:LocalizedString(@"立即注资，进行交易") forState:UIControlStateNormal];
    } else {
        [nextBtn setTitle:LocalizedString(@"完 成") forState:UIControlStateNormal];
    }
    [nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    nextBtn.titleLabel.font = PF_REGU(15);
    [v addSubview:nextBtn];
 
    if (_acntModel.state != 2) {
        if ( [BoLanKey isEqualToString:NAMEEN] ) {
            
            float offsetY = VIEW_H(v) - 190;
            
            UILabel *showLbl = [IXCustomView createLable:CGRectMake(15.5, offsetY, kScreenWidth - 15.5*2, 12)
                                                   title:[NSString stringWithFormat:@"%@%@%@",
                                                          LocalizedString(@"基于监管机构的规定，您需要在申请开户后14天内"),
                                                          LocalizedString(@"提交证明文件。为了不影响您的交易和提款，"),
                                                          LocalizedString(@"敬请阁下尽快提交。")]
                                                    font:PF_MEDI(12)
                                              wTextColor:0x99abba
                                              dTextColor:0x8395a4
                                           textAlignment:NSTextAlignmentLeft];
            showLbl.numberOfLines = 0;
            showLbl.lineBreakMode = NSLineBreakByCharWrapping;
            [showLbl sizeToFit];
            [v addSubview:showLbl];
            
            offsetY += CGRectGetHeight(showLbl.frame) + 3;
            UILabel *tipLbl = [IXCustomView createLable:CGRectMake(15.5, offsetY, kScreenWidth - 15.5*2, 12)
                                                  title:LocalizedString(@"上传证明文件")
                                                   font:PF_MEDI(12)
                                             wTextColor:0x99abba
                                             dTextColor:0xe9e9ea
                                          textAlignment:NSTextAlignmentCenter];
            [v addSubview:tipLbl];
            tipLbl.userInteractionEnabled = YES;
            UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(selectCerFile)];
            [tipLbl addGestureRecognizer:tg];
            
        }else{
            
            NSArray *titleArr = @[LocalizedString(@"基于监管机构的规定，您需要在申请开户后14天内"),
                                  LocalizedString(@"提交证明文件。为了不影响您的交易和提款，"),
                                  LocalizedString(@"敬请阁下尽快提交。"),
                                  LocalizedString(@"上传证明文件")];
            
            float offsetY = VIEW_H(v) - 190;
            
            for (int i = 0; i < titleArr.count; i++) {
                UILabel *showLbl = [IXCustomView createLable:CGRectMake(15.5, offsetY, kScreenWidth - 15.5*2, 12)
                                                       title:titleArr[i]
                                                        font:PF_MEDI(12)
                                                  wTextColor:0x99abba
                                                  dTextColor:0x8395a4
                                               textAlignment:NSTextAlignmentCenter];
                [v addSubview:showLbl];
                if (i == 3) {
                    showLbl.dk_textColorPicker = DKCellTitleColor;
                    showLbl.userInteractionEnabled = YES;
                    UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:@selector(selectCerFile)];
                    [showLbl addGestureRecognizer:tg];
                }
                offsetY += 12 + 8.5;
            }
        }
    }
    return v;
}

- (void)selectCerFile {
    
    IXAcntStep7VC *VC = [[IXAcntStep7VC alloc] init];
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKTableColor;
    
    return cell;
}

- (void)nextBtnClk
{
    if (_acntModel.state == 0) {
//        IXIncashStep1VC *VC = [[IXIncashStep1VC alloc] init];
//        [self.navigationController pushViewController:VC animated:YES];
        
        IXDPSChannelVC *vc = [IXDPSChannelVC new];
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        [self rightBtnItemClicked];
    }
}
 
@end
