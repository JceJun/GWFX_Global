//
//  IXDeviceMgrInfoCell.m
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDeviceMgrInfoCell.h"

@interface IXDeviceMgrInfoCell()

@property (nonatomic, strong)UILabel *nameLbl;
@property (nonatomic, strong)UILabel *contentLbl;//内容
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UIView *dLine;

@end

@implementation IXDeviceMgrInfoCell

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [IXCustomView createLable:CGRectMake(15,12, kScreenWidth - 15, 21)
                                       title:@""
                                        font:PF_MEDI(13)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [IXCustomView createLable:CGRectMake(kScreenWidth/3 + 5,12, kScreenWidth*2/3 - 20, 21)
                                          title:@""
                                           font:PF_MEDI(13)
                                     wTextColor:0x99abba
                                     dTextColor:0x8395a4
                                  textAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UIView *)uLine
{
    if (!_uLine) {
        CGRect rect = CGRectMake(15, 0, kScreenWidth - 15, kLineHeight);
        _uLine = [[UIImageView alloc] initWithFrame:rect];
        _uLine.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_uLine];
    }
    return _uLine;
}

- (UIView *)dLine
{
    if (!_dLine) {
        CGRect rect = CGRectMake(15, 44 - kLineHeight, kScreenWidth - 15, kLineHeight);
        _dLine = [[UIImageView alloc] initWithFrame:rect];
        _dLine.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_dLine];
    }
    return _dLine;
}

- (void)reloadUIWithTitle:(NSString *)title
                  content:(NSString *)content
                indexPath:(NSIndexPath *)indexPath;
{
    self.nameLbl.text = title;
    self.contentLbl.text = content;
    if (indexPath.section == 0) {
        self.uLine.frame = CGRectMake(0, 0, kScreenWidth, kLineHeight);
        self.dLine.frame = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
    } else {
        if (indexPath.row == 0) {
            self.uLine.frame = CGRectMake(0, 0, kScreenWidth, kLineHeight);
            self.dLine.frame = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
        } else {
            self.uLine.hidden = YES;
            self.dLine.frame = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
        }
    }
}

@end
