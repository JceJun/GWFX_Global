//
//  IXEvaluationCellA.h
//  IXApp
//
//  Created by Evn on 2018/2/1.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHTextView.h"
@class IXEvaQuestionM;


@interface IXEvaluationCellA : UITableViewCell

@property (nonatomic, strong) PHTextView    *textV;
@property (nonatomic, copy) void(^selectWordBlock)(NSString * key,NSString * desp);

- (void)refreshData:(IXEvaQuestionM *)evaQuestionM
                    numberLines:(NSInteger)numberLines;

@end
