//
//  IXPDMenuView.h
//  IXApp
//
//  Created by Seven on 2017/4/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^pdmenuItemClicked)(NSInteger index);

/** 下拉菜单子视图 */
@interface IXPDMenuView : UIView

@property (nonatomic, strong) NSArray   <NSString *>* items;
@property (nonatomic, copy) pdmenuItemClicked   itemClicked;
@property (nonatomic, assign)   BOOL    showAttributeText;

+ (IXPDMenuView *)menuViewWithItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width;

- (instancetype)initWithItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width;


/** 设置箭头距离x中心的偏移量 */
- (void)setArrowOffset:(CGFloat)offset;

@end
