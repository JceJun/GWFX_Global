//
//  IXFoldHeaderView.m
//  IXApp
//
//  Created by Evn on 17/2/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXFoldHeaderView.h"

@interface IXFoldHeaderView()
{
    BOOL _created;/** 是否创建过 */
    UILabel *_titleLabel;/** 标题 */
    UIImageView *_imageView;/** 图标 */
    UIButton *_btn;/** 收起按钮 */
    BOOL _canFold;/** 是否可展开 */
    UIView *_uLine;
    UIView *_dLine;
    
}

@end

@implementation IXFoldHeaderView

- (void)setFoldSectionHeaderViewWithData:(NSString *)title
                                    type:(HerderStyle)type
                                 section:(NSInteger)section
                                 canFold:(BOOL)canFold
                                  counts:(NSInteger)counts
{
    
    if (!_created) {
        [self creatUI];
    }
    _section = section;
    _titleLabel.text = title;
    _canFold = canFold;
    if (canFold) {
        _imageView.hidden = NO;
    } else {
        _imageView.hidden = YES;
    }
    if (_section == 0) {
        _uLine.hidden = NO;
        _dLine.frame = CGRectMake(15, 44 - kLineHeight, kScreenWidth - 15, kLineHeight);
    } else {
        _uLine.hidden = YES;
        if (_section != (counts - 1)) {
          _dLine.frame = CGRectMake(15, 44 - kLineHeight, kScreenWidth - 15, kLineHeight);
        } else {
          _dLine.frame = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
        }
    }
}

- (void)creatUI
{
    _created = YES;
    self.contentView.dk_backgroundColorPicker = DKViewColor;
    //标题
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, kScreenWidth - 15 - 25, 15)];
    _titleLabel.font = PF_MEDI(13);
    _titleLabel.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_titleLabel];
    
    
    //按钮
    _btn = [UIButton buttonWithType:UIButtonTypeCustom];
    _btn.frame = CGRectMake(0, 0, kScreenWidth, 44);
    [_btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_btn setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_btn];
    
    //图片
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - 25, 15, 15, 15)];
    [self.contentView addSubview:_imageView];
    
    _uLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
    _uLine.dk_backgroundColorPicker = DKLineColor;
    [self.contentView addSubview:_uLine];
    _uLine.hidden = NO;
    
    //线
    _dLine = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
    _dLine.dk_backgroundColorPicker = DKLineColor;
    [self.contentView addSubview:_dLine];
}

- (void)setFold:(BOOL)fold
{
    _fold = fold;
    if (fold) {
        _imageView.image = [UIImage imageNamed:@"answers_up"];
    } else {
        _imageView.image = [UIImage imageNamed:@"answers_down"];
    }
}

#pragma mark = 按钮点击事件
- (void)btnClick:(UIButton *)btn
{
    if (_canFold) {
        if ([self.delegate respondsToSelector:@selector(foldHeaderInSection:)]) {
            [self.delegate foldHeaderInSection:_section];
        }
    }
}

@end
