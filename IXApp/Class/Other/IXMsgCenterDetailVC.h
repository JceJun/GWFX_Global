//
//  IXMsgCenterDetailVC.h
//  IXApp
//
//  Created by Seven on 2017/4/26.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXMsgCenterM;

/** 消息详情 <- 消息中心 */
@interface IXMsgCenterDetailVC : IXDataBaseVC


/** 消息主体 */
@property (nonatomic, strong) IXMsgCenterM  * message;


@end
