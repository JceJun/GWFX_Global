//
//  IXCountryCell.h
//  IXApp
//
//  Created by Evn on 2018/3/19.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IXCountryM;
@interface IXCountryCell : UITableViewCell

- (void)reloadData:(IXCountryM *)countryM;

@end
