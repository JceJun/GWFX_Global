//
//  IXEvaluationVC.h
//  IXApp
//
//  Created by Evn on 2017/11/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXOpenAcntDataModel.h"
#import "IXEvaluationM.h"

@interface IXEvaluationStepVC : IXDataBaseVC
@property (nonatomic, strong)IXOpenAcntDataModel *acntModel;
@property (nonatomic, strong)IXEvaluationM *evaluationM;
@end
