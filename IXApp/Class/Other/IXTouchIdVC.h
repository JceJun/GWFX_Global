//
//  IXTouchIdVC.h
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^reloginBlock)();

@interface IXTouchIdVC : UIViewController

@property (nonatomic, copy) reloginBlock relogin;

- (BOOL)lockAppWithTouchId;

@end
