//
//  IXAcntStep2VC.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep2VC.h"
#import "IXTouchTableV.h"
#import "IXAcntStep2CellA.h"
#import "IXAcntStep2CellB.h"
#import "IXAcntStep1CellB.h"
#import "IXAcntStep3VC.h"
#import "IXAcntStep4VC.h"
#import "IXAcntProgressV.h"
#import "IXPickerChooseView.h"
#import "IXCountryListVC.h"
#import "IXEvaluationVC.h"
#import "IXBORequestMgr+Region.h"
#import "IXUserDefaultM.h"


@interface IXAcntStep2VC ()<UITableViewDelegate,
UITableViewDataSource,IXAcntStep1CellBDelegate,IXAcntStep2CellBDelegate>

@property (nonatomic, strong) IXTouchTableV *tableV;
@property (nonatomic, strong) UIButton *nextBtn;
@property (nonatomic, strong) NSMutableArray *countryArr;
@property (nonatomic, strong) NSMutableArray *provinceArr;
@property (nonatomic, strong) NSMutableArray *cityArr;

@property (nonatomic, strong) IXPickerChooseView *pickChoose;
@property (nonatomic, strong) IXCountryM    *countryInfo;
@property (nonatomic, strong) NSDictionary *provinceDic;
@property (nonatomic, strong) NSString *regionStr;
@property (nonatomic, assign) BOOL flag;
@property (nonatomic, assign) float curTFPositionY;

@property (nonatomic, strong) IXProvinceM   * province;
@property (nonatomic, strong) IXCityM       * city;

@end

@implementation IXAcntStep2VC

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEdit:)
                                                     name:UITextFieldTextDidBeginEditingNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];

    self.title = LocalizedString(@"开立真实账户");

    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    [self initData];
    [self updateUI];
}

- (void)initData
{
    if ([IXUserDefaultM isChina]) {
        NSString *nationalCode = [IXUserDefaultM getCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityByCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        _regionStr = [[NSString alloc] init];
        [self refreashSProvinceInfo];
    }
}


#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)nextBtnClk
{
    if (!_acntModel.flag) {
        if ( _acntModel.address.length > 0 && _acntModel.postalCode.length > 0 ) {
            if (self.evaluationM.pageList.count) {
                IXEvaluationVC *VC = [[IXEvaluationVC alloc] init];
                VC.acntModel = _acntModel;
                VC.evaluationM = self.evaluationM;
                [self.navigationController pushViewController:VC animated:YES];
            } else {
                IXAcntStep4VC *VC = [[IXAcntStep4VC alloc] init];
                VC.acntModel = _acntModel;
                [self.navigationController pushViewController:VC animated:YES];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"请选择地区")];
        }
    } else {
        if (self.evaluationM.pageList.count) {
            IXEvaluationVC *VC = [[IXEvaluationVC alloc] init];
            VC.acntModel = _acntModel;
            VC.evaluationM = self.evaluationM;
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            IXAcntStep4VC *VC = [[IXAcntStep4VC alloc] init];
            VC.acntModel = _acntModel;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}


#pragma mark -
#pragma mark - table view

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (([IXUserDefaultM isChina] && indexPath.row == 1)
        || (![IXUserDefaultM isChina] && indexPath.row == 0)) {
        return 75.f;
    }
    return 44.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([IXUserDefaultM isChina]) {
        return 3;
    } else {
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    
    if (row == 0) {
        if ([IXUserDefaultM isChina]) {
            static NSString *identifier = @"IXAcntStep2CellA";
            IXAcntStep2CellA *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[IXAcntStep2CellA alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:identifier];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title.dk_textColorPicker = DKGrayTextColor;
            cell.desc.dk_textColorPicker = DKCellTitleColor;
            
            if (!_regionStr.length) {
                cell.desc.textColor = AutoNightColor(0xe2e9f1, 0x303b4d);
            }
            [cell loadUIWithDesc:_regionStr];
            return cell;
        } else {
            return  [self createStep2CellB:tableView];
        }
    } else if (row == 1) {
        if ([IXUserDefaultM isChina]) {
            return  [self createStep2CellB:tableView];
        } else {
            return  [self createStep1CellB:tableView];
        }
    } else {
        return  [self createStep1CellB:tableView];
    }
}

- (UITableViewCell *)createStep2CellB:(UITableView *)tableView
{
    static NSString *identifier = @"IXAcntStep2CellB";
    IXAcntStep2CellB *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXAcntStep2CellB alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.desc.dk_textColorPicker = DKGrayTextColor;
    cell.textV.dk_textColorPicker = DKCellTitleColor;
    cell.delegate = self;
    [cell loadUIWithTextView:_acntModel.address];
    return cell;
}

- (UITableViewCell *)createStep1CellB:(UITableView *)tableView
{
    static NSString *identifier = @"IXAcntStep1CellB";
    IXAcntStep1CellB *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXAcntStep1CellB alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    cell.desc.dk_textColorPicker = DKGrayTextColor;
    cell.textF.dk_textColorPicker = DKCellTitleColor;
    [cell loadUIWithDesc:LocalizedString(@"邮编号码")
                    text:_acntModel.postalCode
             placeHolder:LocalizedString(@"邮编信息")
            keyboardType:UIKeyboardTypeNumberPad
         secureTextEntry:NO iconImage:nil isHiddenIcon:YES iconTag:-1];
    cell.textF.textFont = RO_REGU(15);
    cell.textF.placeHolderFont = PF_MEDI(13);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && [IXUserDefaultM isChina]) {
        if (!self.provinceArr.count) {
            [self.pickChoose show];
        }else{
            [self showPickerChoose];
        }
    }
}

- (void)textValue:(NSString *)value tag:(NSInteger)tag
{
    _acntModel.postalCode = value;
    [self updateUI];
}

#pragma mark - IXAcntStep2CellBDelegate

- (void)textViewValue:(NSString *)value tag:(NSInteger)tag
{
    _acntModel.address = value;
    [self updateUI];
}

- (void)keyboardHidden:(NSNotification *)notify
{
    CGRect  frame = _tableV.frame;
    frame.origin.y = 0;
    _tableV.frame = frame;
}

- (void)didEdit:(NSNotification *)notification
{
    UIView      * contentTF = notification.object;
    UIWindow    * window = [[[UIApplication sharedApplication] delegate] window];
    CGRect      rect = [contentTF convertRect: contentTF.bounds toView:window];
    _curTFPositionY = rect.origin.y + contentTF.superview.frame.size.height;
}

- (void)keyboardShow:(NSNotification *)notification
{
    NSDictionary    * userInfo = [notification userInfo];
    CGRect  keyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    CGRect  frame = _tableV.frame;
    if (_curTFPositionY + keyboardFrame.size.height > kScreenHeight) {
        frame.origin.y = frame.origin.y - (_curTFPositionY + keyboardFrame.size.height - kScreenHeight);
        [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _tableV.frame = frame;
        } completion:nil];
    }
}

- (void)updateUI
{
    if (_acntModel.postalCode.length
        && (_acntModel.province.length || ![IXUserDefaultM isChina])
        && _acntModel.address.length) {
        _flag = YES;
    } else {
        _flag = NO;
    }
    
    if (_flag) {
        _nextBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    } else {
        _nextBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark - other

- (void)showPickerChoose
{
    NSMutableArray  * arr = [@[] mutableCopy];
    if (_province.code) {
        [arr addObject:_province.code];
    }
    if (_city.code) {
        [arr addObject:_city.code];
    }
    
    self.pickChoose.dataArr = self.provinceArr;
    self.pickChoose.seletedTitles = arr;
    
    [self.pickChoose show];
}

#pragma mark -
#pragma mark - request

- (void)refreashSProvinceInfo
{
    weakself;
    NSArray * provinceArr = [IXBORequestMgr region_refreshProvinceWithNationalCode:kChinaNationalCode
                                                                       countryCode:kChinaCountryCoiide complete:^(NSArray<IXProvinceM *> *list, NSString *errStr, BOOL hasNewData)
    {
        [SVProgressHUD dismiss];
        if (list.count) {
            [weakSelf setProvinceArr:[list mutableCopy]];
        }else if (errStr.length){
            [SVProgressHUD showMessage:errStr];
        }
    }];
    
    if (provinceArr.count) {
        [self setProvinceArr:[provinceArr mutableCopy]];
    }else{
        [SVProgressHUD showWithStatus:LocalizedString(@"获取城市信息中...")];
    }
}

- (void)setProvinceArr:(NSMutableArray *)provinceArr
{
    _provinceArr = provinceArr;
    [self.provinceArr enumerateObjectsUsingBlock:^(IXProvinceM * _Nonnull province, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([province.code isEqualToString:self.acntModel.province]) {
            self.province = province;
            [province.subCountryDictParamList enumerateObjectsUsingBlock:^(IXCityM * _Nonnull city, NSUInteger idx1, BOOL * _Nonnull stop1) {
                if ([city.code isEqualToString:self.acntModel.city]) {
                    self.city = city;
                    self.regionStr = [NSString stringWithFormat:@"%@ %@",
                                      [province localizedName],
                                      [city localizedName]];
                    *stop1 = YES;
                }
            }];
            *stop = YES;
        }
    }];
}


#pragma mark -
#pragma mark - lazy loading

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18-22)/2, kScreenWidth, 18)];
    label.text = LocalizedString(@"居住地址");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKGrayTextColor;
    label.font = PF_MEDI(15);
    [v addSubview:label];
    
    label.frame = CGRectMake(0, (132-18)/2 + 5, kScreenWidth, 18);
    
    IXAcntProgressV *prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
    [v addSubview:prgV];
    [prgV showFromMole:1 toMole:2 deno:(4 + self.evaluationM.pageList.count)];
    
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*4-kNavbarHeight)];
    
    _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_nextBtn setTitle:LocalizedString(@"确认地址") forState:UIControlStateNormal];
    _nextBtn.titleLabel.font = PF_REGU(15);
    if (_acntModel.flag) {
        
        _nextBtn.userInteractionEnabled = YES;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    } else {
        
        _nextBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
    
    [v addSubview:_nextBtn];
    return v;
}

- (IXPickerChooseView *)pickChoose
{
    if ( !_pickChoose ) {
        weakself;
        _pickChoose = [[IXPickerChooseView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)
                                                 WithChooseInfo:nil
                                                 WithResultInfo:^(NSInteger section, NSInteger row)
        {
            [weakSelf didChooseRegionWithSection:section row:row];
        }];
    }
    return _pickChoose;
}

- (void)didChooseRegionWithSection:(NSInteger)section row:(NSInteger)row
{
    NSMutableString * regionStr = [NSMutableString string];
    IXProvinceM * model = self.provinceArr[section];
    
    self.province = model;
    self.acntModel.province = model.code;
    if ([self.province localizedName]) {
        [regionStr appendString:[self.province localizedName]];
    }
    
    if (model.subCountryDictParamList.count > row) {
        IXCityM *cityM = model.subCountryDictParamList[row];
        self.city = cityM;
        self.acntModel.city = cityM.code;
        if ([self.city localizedName]) {
            [regionStr appendString:@" "];
            [regionStr appendString:[self.city localizedName]];
        }
    } else {
        self.city = nil;
        self.acntModel.city = @"";
    }
    
    self.regionStr = regionStr;
    [self.tableV reloadData];

    [self updateUI];
}


@end
