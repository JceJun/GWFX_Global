//
//  IXProInfoVC.m
//  IXApp
//
//  Created by Evn on 2018/1/16.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXProInfoVC.h"
#import "IXAcntStep5ACell.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Comp.h"
#import "NSObject+IX.h"

@interface IXProInfoVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, assign) CGFloat webHeight;
@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, copy) NSString *proInfo;

@end

@implementation IXProInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    self.title = LocalizedString(@"产品介绍");
    [self loadData];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadData
{
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    weakself;
    [IXBORequestMgr comp_requestCompanyConfigInfoWithId:CompanyID complete:^(NSString *errStr, NSString *errCode, id obj) {
        [SVProgressHUD dismiss];
        if (!errCode && [obj ix_isDictionary]) {
            if ( [BoLanKey isEqualToString:NAMEEN] ) {
                weakSelf.proInfo = [(NSDictionary *)obj objectForKey:@"symbolIntroduceEn"];
            } else if ( [BoLanKey isEqualToString:NAMECN] ) {
                weakSelf.proInfo = [(NSDictionary *)obj objectForKey:@"symbolIntroduce"];
            } else {
                weakSelf.proInfo = [(NSDictionary *)obj objectForKey:@"symbolIntroduceTw"];
            }
            if (weakSelf.proInfo.length > 0) {
                [weakSelf.tableV reloadData];
            }
        }
    }];
}

#pragma mark -
#pragma makr - initialize

- (UITableView *)tableV
{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.tableFooterView = [[UIView alloc] initWithFrame:ktableFooterFrame];
        _tableV.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
        [_tableV registerClass:[IXAcntStep5ACell class]
        forCellReuseIdentifier:NSStringFromClass([IXAcntStep5ACell class])];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

#pragma mark -
#pragma makr - UITableViewDelegate && UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return MIN(kScreenHeight - kNavbarHeight - kBtomMargin, _webHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntStep5ACell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntStep5ACell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.content = self.proInfo;
    weakself;
    cell.refreashHeight = ^(CGFloat height){
        weakSelf.webHeight = height;
        [weakSelf.tableV reloadData];
    };
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
