//
//  IXDocumentsVC.m
//  IXApp
//
//  Created by Larry on 2018/6/28.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDocumentsVC.h"
#import "IXCommonItemView.h"
#import "IXIDDocVC.h"
#import "IXAddressDocVC.h"
#import "IXWithDrawDocVC.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Asset.h"
#import "UIViewExt.h"
#import "UIKit+Block.h"
#import "IXBankListVC.h"

@interface IXDocumentsVC ()
@property(nonatomic,strong)IXCommonItemView *item_0;
@property(nonatomic,strong)IXCommonItemView *item_1;
@property(nonatomic,strong)IXCommonItemView *item_2;


@property(nonatomic,strong)NSDictionary *id_front_doc;
@property(nonatomic,strong)NSDictionary *id_back_doc;
@property(nonatomic,strong)NSDictionary *address_doc;
@property(nonatomic,strong)NSDictionary *withDraw_doc;

@end

@implementation IXDocumentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Documents";
    [self addBackItem];
    
    //FILE_TYPE_IDCARD_FRONT
    //FILE_TYPE_IDCARD_BACK
    weakself;
    
    IXCommonItemView *item_0 = [IXCommonItemView makeViewInSuperView:self.view lText:@"ID Document" rText:nil topLineStyle:TopLineStyleShadow bottomLineStyle:BottomStyleShadow];
    [item_0 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
    }];
    [item_0 addEnteranceArrowAndStaus:nil block:^{
        [weakSelf gotoNext:@"ID Document"];
    }];
    _item_0 = item_0;
    
    // FILE_TYPE_ADDRESS
    IXCommonItemView *item_1 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Address Documents" rText:nil topLineStyle:TopLineStyleShadow bottomLineStyle:BottomStyleShadow];
    [item_1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_0.bottom).offset(10);
    }];
    [item_1 addEnteranceArrowAndStaus:nil block:^{
        [weakSelf gotoNext:@"Address Documents"];
    }];
    _item_1 = item_1;
    
    // FILE_TYPE_BANK_1
    // FILE_TYPE_BANK_2
    // FILE_TYPE_BANK_3
    IXCommonItemView *item_2 = [IXCommonItemView makeViewInSuperView:self.view lText:@"Bank Information" rText:nil topLineStyle:TopLineStyleShadow bottomLineStyle:BottomStyleShadow];
    [item_2 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_1.bottom).offset(10);
    }];
    [item_2 addEnteranceArrow];
    [item_2 block_whenTapped:^(UIView *aView) {
        [weakSelf gotoNext:@"Bank Information"];
    }];
    
    _item_2 = item_2;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self request];
}

- (void)request{
    [SVProgressHUD show];
    [IXBORequestMgr b_getCustomerFiles:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [self loadData];
            [SVProgressHUD dismiss];
        }else{
            [SVProgressHUD showInfoWithStatus:LocalizedString(@"获取客户文件信息失败")];
        }
    }];
    
    [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString *errStr) {
        
    }];
}

- (void)loadData{
    NSArray *fileArr = [IXBORequestMgr shareInstance].fileArr;
    weakself;
    [fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *info = obj;
        NSString *ftpFilePath = info[@"ftpFilePath"];
        if (ftpFilePath) {
            if ([ftpFilePath length]) {
                if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_IDCARD_FRONT"]) {
                    // 前图
                    weakSelf.id_front_doc = info;
                }else if([info[@"fileType"] isEqualToString:@"FILE_TYPE_IDCARD_BACK"]){
                    // 后图
                    weakSelf.id_back_doc = info;
                }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_ADDRESS"]){
                    // 地址
                    weakSelf.address_doc = info;
                }else if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_WITHDRAW_BANK_1"]){
                    // 出金银行卡1
                    weakSelf.id_back_doc = info;
                }
            }
        }
    }];
    
    __block NSString *status = @"";
    NSString *status2 = @"";
    UIImageView *statusImageV = nil;
    {
        status = [self checkStatus:[_id_front_doc[@"proposalStatus"] integerValue]];
        status2 = [self checkStatus:[_id_back_doc[@"proposalStatus"] integerValue]];
        
        if ([@[status,status] containsObject:@"Not Uploaded"]) {
            status = @"Not Uploaded";
        }else if ([@[status,status] containsObject:@"Not Approved"]){
            status = @"Not Approved";
        }else if ([@[status,status] containsObject:@"Checking"]){
            status = @"Checking";
        }else{
            status = @"Approved";
        }
        
        _item_0.lb_right.text = status;
        if ([status isEqualToString:@"Approved"]) {
            _item_0.userInteractionEnabled = NO;
            _item_0.imgArrow.hidden = YES;
        }
        
        statusImageV = [UIView getSubObjFromSupperObj:_item_0 bySubObjectKey:@"statusImageV"];
        [self checkStatusImage:status statusImageV:statusImageV];
    }
    {
        status = [self checkStatus:[_address_doc[@"proposalStatus"] integerValue]];
        _item_1.lb_right.text = status;
        
        if ([status isEqualToString:@"Approved"]) {
            _item_1.userInteractionEnabled = NO;
            _item_1.imgArrow.hidden = YES;
        }
        statusImageV = [UIView getSubObjFromSupperObj:_item_1 bySubObjectKey:@"statusImageV"];
        [self checkStatusImage:status statusImageV:statusImageV];
    }

}

- (NSString *)checkStatus:(NSInteger)status{
    switch (status) {
        case -1:{
            return @"Not Approved";
        }break;
        case 0:{
            return @"Not Uploaded";
        }break;
        case 1:{
            return @"Checking";
        }break;
        case 2:{
            return @"Approved";
        }break;
    }
    return @"";
}

- (void)checkStatusImage:(NSString *)statusImage statusImageV:(UIImageView *)statusImageV{
    if ([[statusImage lowercaseString] isEqualToString:@"approved"]) {
        
        statusImageV.image = [UIImage imageNamed:@"cerFile_checked"];
    }else if ([[statusImage lowercaseString] isEqualToString:@"checking"]){
        statusImageV.image = [UIImage imageNamed:@"cerFile_review"];
    }else{
        statusImageV.image = [UIImage imageNamed:@"cerFile_unload"];
    }
}


- (void)gotoNext:(NSString *)title{
    if ([[title lowercaseString] containsString:@"id"]) {
        IXIDDocVC *vc = [IXIDDocVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([[title lowercaseString] containsString:@"address"]) {
        IXAddressDocVC *vc = [IXAddressDocVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([[title lowercaseString] containsString:@"bank"]){
        IXBankListVC *vc = [IXBankListVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
