//
//  IXModifyPwdResultVC.m
//  IXApp
//
//  Created by Bob on 2017/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXModifyPwdResultVC.h"

@interface IXModifyPwdResultVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *contentTV;
@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation IXModifyPwdResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = ViewBackgroundColor;
    [self addBackItemrget:self action:@selector(onGoback)];
    [self addDismissItemWithTitle:LocalizedString(@"完成")
                           target:self
                           action:@selector(onGoback)];
    
    self.title = LocalizedString(@"修改登录密码");
    [self.contentTV reloadData];
}

- (void)onGoback
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             NSStringFromClass([UITableViewCell class])];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 163;

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 163)];
    
    UIImageView *tipImg = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 80)/2, 45, 80, 80)];;
    [tipImg setImage:[UIImage imageNamed:@"common_complete"]];
    [headView addSubview:tipImg];
    
    UILabel *tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 145, kScreenWidth, 18)
                                         WithFont:PF_MEDI(15)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0x4c6072
                                       dTextColor:0xff0000];
    [headView addSubview:tipLbl];
    tipLbl.text = LocalizedString(@"成功");

    return headView;

}

- (NSArray *)dataArr
{
    if ( !_dataArr ) {
        _dataArr = @[];
    }
    return _dataArr;
}

- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:self.view.bounds];
        _contentTV.separatorInset = UIEdgeInsetsMake( 0, -10, 0, 0);
        _contentTV.backgroundColor = CellBackgroundColor;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[UITableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        [self.view addSubview:_contentTV];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _contentTV;
}


@end
