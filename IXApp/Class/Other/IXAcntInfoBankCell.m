//
//  IXAcntInfoBankCell.m
//  IXApp
//
//  Created by Evn on 2017/4/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntInfoBankCell.h"

@interface IXAcntInfoBankCell()

@property (nonatomic,strong)UILabel *title;
@property (nonatomic,strong)UIImageView *rIcon;

@end

@implementation IXAcntInfoBankCell

- (UIImageView *)rIcon
{
    if (!_rIcon) {
        _rIcon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (14 + 16), 15, 14, 14)];
        [self.contentView addSubview:_rIcon];
    }
    return _rIcon;
}

- (UILabel *)title
{
    if (!_title) {
        CGRect rect = CGRectMake(15, 12, 180, 21);
        _title = [[UILabel alloc] initWithFrame:rect];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKCellTitleColor;
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UIView *)line
{
    if (!_line) {
        CGRect rect = CGRectMake(15, 44 - kLineHeight, kScreenWidth - 15, kLineHeight);
        _line = [[UIImageView alloc] initWithFrame:rect];
        [self.contentView addSubview:_line];
    }
    return _line;
}

- (void)reloadUIWithTitle:(NSString *)title
                     icon:(NSString *)icon
             isHiddenLine:(BOOL)hidden
{
    self.title.text = title;
    self.rIcon.image = [UIImage imageNamed:icon];
    self.line.dk_backgroundColorPicker = DKLineColor;
    self.line.hidden = hidden;
}

@end
