//
//  IXSurveyStep1VC.m
//  IXApp
//
//  Created by Evn on 2017/10/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyMainVC.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "IXSurveyStepVC.h"
#import "IXSurveyMainVC+Rst.h"

#import "IXBORequestMgr+Survey.h"
#import "NSObject+IX.h"

@interface IXSurveyMainVC ()

@property (nonatomic, strong) UITableView *tableV;

@end

@implementation IXSurveyMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.title = LocalizedString(@"调查问卷");
    self.fd_interactivePopDisabled = YES;
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshData];
}

- (void)refreshData
{
    self.surveyM  = [[IXSurveyM alloc] init];
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    weakself;
    [IXBORequestMgr survey_questionListWith:IXSurveyReqTypeQuest result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        NSDictionary *dataDic = obj;
        if (success && [dataDic ix_isDictionary] && dataDic[@"questionList"]) {
            NSArray *arr = dataDic[@"questionList"];
            if (arr.count) {
                [SVProgressHUD dismiss];
                [weakSelf.surveyM saveSurvey:arr];
                if ([[dataDic allKeys] containsObject:@"answered"]) {
                    weakSelf.surveyM.answered = [dataDic[@"answered"] boolValue];
                }
                if ([[dataDic allKeys] containsObject:@"title"]) {
                    weakSelf.surveyM.title = dataDic[@"title"];
                }
                if ([[dataDic allKeys] containsObject:@"summary"]) {
                    weakSelf.surveyM.summary = dataDic[@"summary"];
                }
                [weakSelf refreshUI];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"没有调查问卷")];
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        } else {
            if ([errCode isEqualToString:@"NODATA"]) {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"没有调查问卷")];
            } else {
                if (errStr.length) {
                    [SVProgressHUD showErrorWithStatus:errStr];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"查询失败")];
                }
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)refreshUI
{
    if (self.surveyM.answered) {
        if (_tableV) {
            [_tableV removeFromSuperview];
            _tableV = nil;
        }
        self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"重填") target:self sel:@selector(rightBtnItemClicked)];
        [self loadRstView];
    } else {
        [self.view addSubview:self.tableV];
        [self.tableV setTableHeaderView:[self headV]];
        [self.tableV reloadData];
    }
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    IXSurveyStepVC *VC = [[IXSurveyStepVC alloc] init];
    VC.surveyM = self.surveyM;
    [self.navigationController pushViewController:VC animated:YES];
}

- (UITableView *)tableV
{
    if ( !_tableV ) {
        _tableV = [[UITableView alloc] initWithFrame:self.view.bounds];
//        _tableV.delegate = self;
//        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableV;
}

- (UIScrollView *)headV
{
    UIScrollView *v = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 200)];
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 161)/2, 43, 161, 161)];
    imgV.dk_imagePicker = DKImageNames(@"survey_edit", @"survey_edit_D");
    [v addSubview:imgV];
    
    UILabel *lbl = [IXCustomView createLable:CGRectMake(20, GetView_MaxY(imgV) + 60, kScreenWidth - 40, 18)
                                       title:[IXDataProcessTools dealWithNil:self.surveyM.title]
                                        font:PF_MEDI(15)
                                  wTextColor:0x4c6072
                                  dTextColor:0xe9e9ea
                               textAlignment:NSTextAlignmentCenter];
    [v addSubview:lbl];
    
    NSString *contentStr = [IXDataProcessTools dealWithNil:self.surveyM.summary];
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 5;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:PF_MEDI(13), NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    CGSize size = [contentStr boundingRectWithSize:CGSizeMake(265 , MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:dic
                                            context:nil].size;
    
    UILabel *content = [IXUtils createLblWithFrame:CGRectMake( 20, GetView_MaxY(lbl) + 19, kScreenWidth - 40, size.height)
                                  WithFont:PF_MEDI(13)
                                 WithAlign:NSTextAlignmentCenter
                                wTextColor:0x99abba
                                dTextColor:0x8395a4
                ];
    content.numberOfLines = 0;
    content.text = contentStr;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentStr];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentStr length])];
    content.attributedText = attributedString;
    content.lineBreakMode = NSLineBreakByCharWrapping;
    content.textAlignment = NSTextAlignmentCenter;
    [v addSubview:content];
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, GetView_MaxY(content) + 41, kScreenWidth - 31, 44);
    [nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    [nextBtn setTitle:LocalizedString(@"开始填写") forState:UIControlStateNormal];
    nextBtn.titleLabel.font = PF_REGU(15);
    [v addSubview:nextBtn];
    
    v.frame = CGRectMake(0, 0, kScreenWidth, (GetView_MaxY(nextBtn) + 40) > (kScreenHeight - kNavbarHeight)?(kScreenHeight - kNavbarHeight):((GetView_MaxY(nextBtn) + 40)));
    v.contentSize = CGSizeMake(kScreenWidth, GetView_MaxY(nextBtn) + 40);
    return v;
    
}

- (void)nextBtnClk
{
    if ([[self.surveyM getSurvey] count]) {
        IXSurveyStepVC *VC = [[IXSurveyStepVC alloc] init];
        VC.surveyM = self.surveyM;
        [self.navigationController pushViewController:VC animated:YES];
    } else {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
    }
}


@end
