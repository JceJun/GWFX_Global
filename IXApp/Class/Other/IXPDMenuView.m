//
//  IXPDMenuView.m
//  IXApp
//
//  Created by Seven on 2017/4/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPDMenuView.h"
#import "IXPDMenuCell.h"
#import "IXUserDefaultM.h"
#import "NSString+IX.h"

#define kArrowHeight    5

@interface IXPDMenuView ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, assign) CGFloat    rowHeight;
@property (nonatomic, assign) CGFloat    width;

@property (nonatomic, strong) UIImageView   * arrowImgV;
@property (nonatomic, strong) UITableView   * tableV;

@end

@implementation IXPDMenuView

+ (IXPDMenuView *)menuViewWithItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width
{
    return [[IXPDMenuView alloc] initWithItems:items rowHeight:rowHeight width:width];
}

- (instancetype)initWithItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width
{
    if (self = [super initWithFrame:CGRectMake(0, 0, width, rowHeight * [items count] + kArrowHeight)]) {
        _rowHeight = rowHeight;
        _width = width;
        _items = [items mutableCopy];
        
        [self createSubview];
    }
    return self;
}

- (void)createSubview
{
    [self addSubview:self.arrowImgV];
    [self addSubview:self.tableV];
    [self.tableV reloadData];
}

/** 设置箭头距离x中心的偏移量 */
- (void)setArrowOffset:(CGFloat)offset
{
    CGFloat x = (_width - 8)/2 + offset;
    
    if (x < 5)  x = 5;
    if (x > _width - 5) x = _width - 5;
    
    _arrowImgV.frame = CGRectMake(x, 1, 8, 4.5);

}

#pragma mark -
#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _rowHeight;
}

static  NSString    * cellIdent = @"msgcell";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXPDMenuCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    if (!cell) {
        cell = [[IXPDMenuCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:cellIdent];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.seletedColor = [IXUserDefaultM isNightMode] ?
    UIColorWithHex(0x303b4d, 1.f) : UIColorWithHex(0x4c6072, 1.f);
    cell.commonColor = [UIColor clearColor];
    
    if (indexPath.row == _items.count - 1) {
        cell.btomLineColor = [UIColor clearColor];
    }else{
        cell.btomLineColor = [IXUserDefaultM isNightMode] ?
        UIColorHexFromRGB(0x242a36) : UIColorHexFromRGB(0xe2eaf2);;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXPDMenuCell    * c = (IXPDMenuCell *)cell;
    
    if (_showAttributeText) {
        c.attributeTitle = [_items[indexPath.row] attributeStringWithFontSize:15 textColor:AutoNightColor(0xffffff, 0xe9e9ea)];
    } else {
        c.title = _items[indexPath.row];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.itemClicked) {
        self.itemClicked(indexPath.row);
    }
}


#pragma mark -
#pragma mark - setter

- (void)setItems:(NSArray<NSString *> *)items
{
    if ( _items.count != items.count ) {
        self.frame = CGRectMake((kScreenWidth - _width)/2, 0, _width, _rowHeight*[items count]+kArrowHeight);
        self.tableV.frame = CGRectMake(0,
                                       kArrowHeight,
                                       self.bounds.size.width,
                                       self.bounds.size.height - kArrowHeight);
    }
    _items = [items mutableCopy];

    [self.tableV reloadData];
}

#pragma mark -
#pragma mark - getter

- (UITableView *)tableV
{
    if (!_tableV) {
        CGRect  rect = CGRectMake(0,
                                  kArrowHeight,
                                  self.bounds.size.width,
                                  self.bounds.size.height - kArrowHeight);
        
        _tableV = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        _tableV.separatorStyle  = UITableViewCellSeparatorStyleNone;
        _tableV.dataSource      = self;
        _tableV.delegate        = self;
        _tableV.layer.cornerRadius = 4.f;
        _tableV.scrollEnabled = NO;
    }
    _tableV.backgroundColor = [IXUserDefaultM isNightMode] ?
    UIColorWithHex(0x303b4d, 0.92) : UIColorWithHex(0x4c6072, 0.92);
    return _tableV;
}

- (UIImageView *)arrowImgV
{
    if (!_arrowImgV) {
        _arrowImgV = [[UIImageView alloc] initWithFrame:CGRectMake((_width - 8)/2,
                                                                   1,
                                                                   8,
                                                                   4.5)];
        _arrowImgV.dk_imagePicker = DKImageNames(@"common_arrow_up", @"common_arrow_up_black");
        _arrowImgV.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _arrowImgV;
}


@end
