//
//  IXAcntAvatarCell.m
//  IXApp
//
//  Created by Seven on 2017/12/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntAvatarCell.h"

@implementation IXAcntAvatarCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.avatarV];
        [self.contentView addSubview:self.nextImgV];
        self.dk_backgroundColorPicker = DKNavBarColor;
    }
    return self;
}


#pragma mark -
#pragma mark - getter

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [UILabel labelWithFrame:CGRectMake(15, (kAvatarH - 18)/2, kScreenWidth/2, 18)
                                       text:@""
                                   textFont:PF_MEDI(13)
                                  textColor:DKCellTitleColor];
    }
    return _titleLab;
}


- (UIImageView *)avatarV
{
    if (!_avatarV) {
        _avatarV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (14 + 16 + 52),
                                                                 (kAvatarH - 40)/2,
                                                                 40, 40)];
        _avatarV.dk_imagePicker = DKImageNames(@"openAccount_avatar", @"openAccount_avatar_D");
        _avatarV.layer.cornerRadius = 20.f;
        _avatarV.layer.masksToBounds = YES;
    }
    return _avatarV;
}

- (UIImageView *)nextImgV
{
    if (!_nextImgV) {
        _nextImgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (14 + 16), (kAvatarH - 14)/2, 14, 14)];
        _nextImgV.image = [UIImage imageNamed:@"acntInfo_arrow_right"];
    }
    return _nextImgV;
}

@end
