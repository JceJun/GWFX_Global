//
//  IXSubQuoteStyle5Cell.m
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubQuoteStyle5Cell.h"

@interface IXSubQuoteStyle5Cell ()

@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation IXSubQuoteStyle5Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        UIImageView *topSep = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 1)];
        topSep.backgroundColor = MarketCellSepColor;
        [self.contentView addSubview:topSep];
        

        UIImageView *bottomSep = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 45, kScreenWidth, 1)];
        bottomSep.backgroundColor = MarketCellSepColor;
        [self.contentView addSubview:bottomSep];
        
        self.tipLbl.text = @"已开通连续包月";
    }
    return self;
}


- (void)setAFee:(BOOL)aFee
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(  kScreenWidth - 46, 0, 46, 46);
    if ( aFee ) {
        [btn setImage:[UIImage imageNamed:@"common_complete"] forState:UIControlStateNormal];
    }else{
        [btn setImage:[UIImage imageNamed:@"common_cell_unchoose"] forState:UIControlStateNormal];
    }
    [btn setImageEdgeInsets:UIEdgeInsetsMake( 15.5, 15.5, 15.5, 15.5)];
    [self.contentView addSubview:btn];
}

- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, kScreenWidth - 30, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentLeft
                                   wTextColor:0x4c6072
                                   dTextColor:0xff0000];
        [self.contentView addSubview:_tipLbl];
    }
    return _tipLbl;
}

@end
