//
//  IXChangeEmailStep1VC.m
//  IXApp
//
//  Created by Evn on 2017/6/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXChangeEmailStep1VC.h"
#import "IXChangeEmailStep2VC.h"
#import "IXTouchTableV.h"
#import "IXEmailRegStep1CellA.h"
#import "IXAppUtil.h"
#import "IXBORequestMgr+Account.h"
#import "IXCpyConfig.h"

@interface IXChangeEmailStep1VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong) IXTouchTableV * tableV;
@property (nonatomic, strong) UIButton  * submitBtn;
@property (nonatomic, copy) NSString    * email;

@end

@implementation IXChangeEmailStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =[IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                                                           target:self
                                                                              sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"更换邮箱");
    self.view.dk_backgroundColorPicker = DKTableColor;
    [self.view addSubview:self.tableV];
    [self.navigationController setNavigationBarHidden:NO];
}


#pragma mark -
#pragma mark - ben action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registBtnClk
{
    [self.view endEditing:YES];
    [self checkEmail];
}

#pragma mark -
#pragma mark - table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString    * ident = NSStringFromClass([IXEmailRegStep1CellA class]);
    IXEmailRegStep1CellA *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    [cell loadUIWithTFText:self.email
                  withDesc:[IXLocalizationModel getLocalizationWordByKey:LocalizedString(@"电子邮箱")]
                   withTag:indexPath.row];
    cell.textF.textFont = RO_REGU(15);
    cell.textF.placeHolderFont = PF_MEDI(13);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textF.delegate = self;
    [cell.textF becomeFirstResponder];
    
    return cell;
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    //不接受输入空格
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:NONESPACECHAR];
        return NO;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self submitBtnEnable:[IXAppUtil isValidateEmail:aimStr]];
    _email = aimStr;
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self submitBtnEnable:NO];
    return YES;
}

- (void)submitBtnEnable:(BOOL)enable
{
    if (enable){
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                           topCapHeight:image.size.height/2];
        
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        UIImage * image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                           topCapHeight:image.size.height/2];
        
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
    _submitBtn.enabled = enable;
}


#pragma mark -
#pragma mark - other

- (void)checkEmail
{
    if ([IXAppUtil isValidateEmail:self.email]) {
        IXEmailPhoneModel   * model = [[IXEmailPhoneModel alloc] init];
        model.email = self.email;
        model.index = 0;
        if (SameString(self.email, [[IXUserInfoMgr shareInstance] demoLoginAccount])) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"用户名错误")];
            return;
        }
        [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
        
        [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model complete:^(BOOL exist, NSString *errStr) {
            if (exist) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD dismiss];
                
                BOOL k = VerifyCodeEnable;
                if (k) {
                    [self popToValidCode];
                }else{
#warning 待处理
                    //                        [strong_self popToStep3VC];
                }
                return;
            }
        }];
    }else{
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入正确的邮箱号")];
    }
}

- (void)popToValidCode
{
    IXChangeEmailStep2VC * vc = [[IXChangeEmailStep2VC alloc] init];
    vc.email = _email;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -
#pragma mark - UI

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 176)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (176-18)/2, kScreenWidth, 18)];
    label.text = LocalizedString(@"请输入电子邮箱地址");
    label.font = PF_MEDI(15);
    label.dk_textColorPicker = DKCellTitleColor;
    label.textAlignment = NSTextAlignmentCenter;
    [v addSubview:label];
    
    return v;
}

- (UIView *)tableFooterV
{
    UIView  * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0,
                                                           kScreenHeight - 176 - 44 - kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage * image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                       topCapHeight:image.size.height/2];
    
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    _submitBtn.titleLabel.font = PF_REGU(15);
    _submitBtn.enabled = NO;
    [_submitBtn setTitle:LocalizedString(@"提 交") forState:UIControlStateNormal];
    [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_submitBtn addTarget:self
                   action:@selector(registBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:_submitBtn];
    
    return v;
}


#pragma mark -
#pragma mark - lazy laoding

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.tableFooterView = [self tableFooterV];
        _tableV.tableHeaderView = [self tableHeaderV];
        [_tableV registerClass:[IXEmailRegStep1CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXEmailRegStep1CellA class])];
    }
    return _tableV;
}


@end
