//
//  IXEvaluationHeaderV.m
//  IXApp
//
//  Created by Evn on 2018/1/30.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXEvaluationHeaderV.h"
#import "IXAcntProgressV.h"

#import "IXEvaluationM.h"

@interface IXEvaluationHeaderV()

@property (nonatomic, strong)IXAcntProgressV *prgV;
@property (nonatomic, strong)UIView *headerV;
@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UIView *dLine;

@property (nonatomic, assign)NSInteger pageIndex;
@property (nonatomic, assign)NSInteger count;
@property (nonatomic, copy)NSString *titleStr;
@property (nonatomic, assign)CGFloat tHeight;

@end

@implementation IXEvaluationHeaderV

- (id)initWithData:(IXEvaluationM *)evaluationM
         pageIndex:(NSInteger)pageIndex
{
    if (self = [super init]) {
        self.titleStr = evaluationM.title;
        self.pageIndex = pageIndex;
        self.count = evaluationM.pageList.count;
        [self refreshUI];
    }
    return self;
}

- (void)refreshUI
{
    [self addSubview:self.headerV];
    [self addSubview:self.prgV];
    [self.headerV addSubview:self.title];
    [self addSubview:self.dLine];
    CGFloat height = 132;
    self.frame = CGRectMake(0, 0, kScreenWidth, height);
}

- (IXAcntProgressV *)prgV
{
    if (!_prgV) {
        _prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
        [_prgV showFromMole:(self.pageIndex + 2) toMole:(self.pageIndex + 3) deno:(self.count + 4)];
    }
    return _prgV;
}

- (UIView *)headerV
{
    if (!_headerV) {
        _headerV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
        _headerV.dk_backgroundColorPicker = DKTableHeaderColor;
    }
    return _headerV;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [IXCustomView createLable:CGRectMake(14.5, (132 - self.tHeight)/2, kScreenWidth - 29, self.tHeight) title:self.titleStr font:PF_MEDI(15) wTextColor:0x4c6072 dTextColor:0x8395a4 textAlignment:NSTextAlignmentCenter];
        _title.numberOfLines = 0;
        _title.lineBreakMode = NSLineBreakByCharWrapping;
    }
    return _title;
}

- (UIView *)dLine
{
    if (!_dLine) {
        _dLine = [[UIView alloc] initWithFrame:CGRectMake(0,132 - kLineHeight, kScreenWidth, kLineHeight)];
        _dLine.dk_backgroundColorPicker = DKLineColor;
    }
    return _dLine;
}


- (CGFloat)tHeight
{
    if (!_tHeight) {
        CGSize size = [IXDataProcessTools sizeWithString:self.titleStr font:PF_MEDI(15) width:kScreenWidth - 29];
        if (size.height > (132 - 14.5*2)) {
            size.height = (132 - 14.5*2);
        }
        _tHeight = size.height;
    }
    return _tHeight;
}

@end
