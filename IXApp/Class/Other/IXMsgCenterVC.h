//
//  IXMsgCenterVC.h
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>


/** 消息中心 */
@interface IXMsgCenterVC : IXDataBaseVC

/** 未读消息数量 */
@property (nonatomic, assign) NSInteger     noReadCount;

@property (nonatomic, copy) void(^backBlock)();
@property (nonatomic, copy) void(^readAllBlock)();

@end
