//
//  IXSubQuoteStyle3Cell.m
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubQuoteStyle3Cell.h"

@interface IXSubQuoteStyle3Cell ()

@property (nonatomic, strong) UILabel *feeLbl;

@property (nonatomic, strong) UILabel *feeTypeLbl;


@end

@implementation IXSubQuoteStyle3Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        self.feeLbl.text = @"¥200/每月";
        self.feeTypeLbl.text = @"自动续费";
        
        
        CGFloat width = [IXEntityFormatter getContentWidth:self.feeTypeLbl.text
                                                  WithFont:self.feeTypeLbl.font];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(  kScreenWidth - width - 46, 0, 31, 20);
        [btn setImage:[UIImage imageNamed:@"common_complete"] forState:UIControlStateSelected];
        [btn setImage:[UIImage imageNamed:@"common_cell_unchoose"] forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake( 5, 8, 0, 8)];
        
        [btn setSelected:YES];
        [btn addTarget:self action:@selector(responeBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        
    }
    return self;
}

- (void)responeBtn:(UIButton *)sender
{
    [sender setSelected:!sender.isSelected];
    if( _chooseFee ){
        _chooseFee(sender.isEnabled);
    }
}


- (UILabel *)feeLbl
{
    if ( !_feeLbl ) {
        _feeLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 5, kScreenWidth - 30, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentLeft
                                   wTextColor:0x4c6072
                                   dTextColor:0xff0000];
        [self.contentView addSubview:_feeLbl];
    }
    return _feeLbl;
}


- (UILabel *)feeTypeLbl
{
    if ( !_feeTypeLbl ) {
        _feeTypeLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 5, kScreenWidth - 30, 15)
                                         WithFont:PF_MEDI(13)
                                        WithAlign:NSTextAlignmentRight
                                       wTextColor:0x4c6072
                                       dTextColor:0xff0000];
        [self.contentView addSubview:_feeTypeLbl];
    }
    return _feeTypeLbl;
}

@end
