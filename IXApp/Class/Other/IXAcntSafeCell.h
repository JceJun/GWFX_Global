//
//  IXAcntSafeCell.h
//  IXApp
//
//  Created by Bob on 2017/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXAcntSafeCell : UITableViewCell

@property (nonatomic, strong) UISwitch      * swiBtn;
@property (nonatomic, strong) UIImageView   * arrImg;
@property (nonatomic, strong) UILabel       * contentLbl;

@property (nonatomic, strong) NSString      * tipName;

@property (nonatomic, strong) NSString      *content;
@property (nonatomic, assign) BOOL      showAttributeText;

//分割线
- (void)showTopLineWithOffsetX:(CGFloat)offset;
- (void)showBototmLineWithOffsetX:(CGFloat)offset;

@end
