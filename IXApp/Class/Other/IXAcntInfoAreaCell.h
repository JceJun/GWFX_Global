//
//  IXAcntInfoAreaCellTableViewCell.h
//  IXApp
//
//  Created by Evn on 2017/6/29.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXClickTableVCell.h"

@interface IXAcntInfoAreaCell : IXClickTableVCell

- (void)reloadUIWithTitle:(NSString *)title
                     icon:(NSString *)icon
             isHiddenLine:(BOOL)hidden;

@end
