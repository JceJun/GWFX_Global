//
//  IXAnswersCellA.m
//  IXApp
//
//  Created by Evn on 2017/7/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAnswersCellA.h"
#import <WebKit/WebKit.h>

@interface IXAnswersCellA()<UIWebViewDelegate>

@property (nonatomic, strong)UIWebView *webView;

@end
@implementation IXAnswersCellA

- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth - 15 * 2, 1)];
        _webView.delegate = self;
        _webView.userInteractionEnabled = YES;
        _webView.scrollView.showsVerticalScrollIndicator = NO;
        _webView.scrollView.showsHorizontalScrollIndicator = NO;
        _webView.backgroundColor = [UIColor clearColor];
        [_webView sizeToFit];
        
        for (UIView * v in _webView.subviews) {
            v.backgroundColor = [UIColor clearColor];
        }
        [self.contentView addSubview:_webView];
    }
    return _webView;
}

- (CGFloat)getHeight
{
    return 10.f;
}

- (void)setWebUrl:(NSString *)webUrl
{
    _webUrl = webUrl;
    
    [self webView];
    NSURL *url = [NSURL URLWithString:_webUrl];
    if ( url ) {
        [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

- (void)setContent:(NSString *)content
{
    _content = content;
    NSLog(@"222222====%@====",content);
    if ( content ) {
        [self.webView  loadHTMLString:content baseURL:nil];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    _webView.delegate = self;
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    
    CGRect frame = _webView.frame;
    if ( _cellHeight != height ) {
        _cellHeight = height;
        frame.size.height = _cellHeight;
        _webView.frame = frame;
//        CGRect frame = _webView.frame;
//        frame.size.height = _cellHeight;
//        webView.scrollView.contentSize = CGSizeMake(kScreenWidth, _cellHeight);
//        
//        if ( _refreashHeight ) {
//            _refreashHeight(_cellHeight);
//        }
       
    }
    frame = self.frame;
    frame.size.height = _cellHeight;
    self.frame = frame;
    NSLog(@"self.frame = %@",NSStringFromCGRect(frame));
    
}

@end
