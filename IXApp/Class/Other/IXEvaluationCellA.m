//
//  IXEvaluationCellA.m
//  IXApp
//
//  Created by Evn on 2018/2/1.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXEvaluationCellA.h"
#import "IXAttributeTapLabel.h"
#import "IXEvaluationM.h"


@interface IXEvaluationCellA()<UITextViewDelegate>

@property (nonatomic, strong)UIView *line;
@property (nonatomic, strong)UILabel *category;
@property (nonatomic, strong)IXAttributeTapLabel *titleLab;
@property (nonatomic, strong)UIView *dLine;
@property (nonatomic, strong)IXEvaQuestionM * questionM;

@end

@implementation IXEvaluationCellA

- (UIView *)line
{
    if (!_line) {
        _line = [[UIView alloc] initWithFrame:CGRectMake(15,GetView_MaxY(self.category), kScreenWidth - 15, kLineHeight)];
        _line.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_line];
    }
    return _line;
}

- (UILabel *)category
{
    if (!_category) {
        _category = [IXCustomView createLable:CGRectMake(15, 0, kScreenWidth - 30, 0)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x99abba
                                   dTextColor:0x8395a4
                                textAlignment:NSTextAlignmentLeft];
        _category.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        _category.numberOfLines = 0;
        _category.lineBreakMode = NSLineBreakByCharWrapping;
        [self.contentView addSubview:_category];
    }
    return _category;
}

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [[IXAttributeTapLabel alloc] initWithFrame:CGRectMake(15,0,kScreenWidth - 30,30)];
        _titleLab.backgroundColor = [UIColor clearColor];
        _titleLab.numberOfLines = 0;
        _titleLab.lineBreakMode = NSLineBreakByCharWrapping;

        weakself;
        _titleLab.tapBlock = ^(NSString *string) {
            __block NSString * desp = @"";
            [weakSelf.questionM.paraphrase enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
                key = [weakSelf dealWithHighLightedKey:key];
                if (SameString(key, string)) {
                    desp = obj;
                    *stop = YES;
                }
            }];

            if (desp.length && weakSelf.selectWordBlock) {
                weakSelf.selectWordBlock(string, desp);
            }
        };
    }
    
    [self.contentView addSubview:_titleLab];
    return _titleLab;
}

- (UIView *)dLine
{
    if (!_dLine) {
        _dLine = [[UIView alloc] initWithFrame:CGRectMake(15,GetView_MaxY(self.category) - kLineHeight, kScreenWidth - 15, kLineHeight)];
        _dLine.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_dLine];
    }
    return _dLine;
}


- (PHTextView *)textV
{
    if (!_textV) {
        CGRect rect = CGRectMake(14.5, 12, kScreenWidth - 29, 30);
        _textV = [[PHTextView alloc] initWithFrame:rect];
        _textV.font = PF_MEDI(13);
        _textV.delegate = self;
        _textV.placeholder = LocalizedString(@"请输入...");
        _textV.contentOffset = CGPointMake(0, 5);
        _textV.backgroundColor = [UIColor clearColor];
        _textV.dk_textColorPicker = DKCellTitleColor;
        _textV.placeholderColor = AutoNightColor(0xe2e9f1, 0x303b4d);
        [self.contentView addSubview:_textV];
    }
    return _textV;
}


- (void)refreshData:(IXEvaQuestionM *)evaQuestionM
        numberLines:(NSInteger)numberLines
{
    self.questionM = evaQuestionM;
    CGFloat offsetY = 0.f;
    __block CGRect frame = CGRectMake(0, 0, 0, 0);
    if (evaQuestionM.sectionTitle.length) {
        [self category];
        self.category.text = evaQuestionM.sectionTitle;
        frame = self.category.frame;
        CGSize size = [IXDataProcessTools sizeWithString:evaQuestionM.sectionTitle font:PF_MEDI(13) width:kScreenWidth - 30];
        frame.size.height = size.height + 23.f;
        self.category.frame = frame;
        offsetY = VIEW_H(self.category);
        [self line];
        [self dLine];
    }
    
    UIColor * textColor = AutoNightColor(0x99abba, 0x8395a4);
    NSDictionary * attr = @{NSForegroundColorAttributeName:textColor,NSFontAttributeName:PF_MEDI(13)};
    [self dealWithQues:evaQuestionM complete:^(NSArray *arr, NSString *title) {
        [self.titleLab setText:title attributes:attr tapStringArray:arr];
        
        frame = self.titleLab.frame;
        frame.size.height = [IXDataProcessTools sizeWithString:evaQuestionM.title font:PF_MEDI(13) width:kScreenWidth - 30].height + 23.f;
        frame.origin.y = offsetY;
        self.titleLab.frame = frame;
    }];
    
    frame = self.dLine.frame;
    frame.origin.y = GetView_MaxY(self.titleLab) - kLineHeight;
    self.dLine.frame = frame;
    
    offsetY = GetView_MaxY(self.titleLab);
    
    [self.contentView bringSubviewToFront:self.textV];
    self.textV.text = evaQuestionM.answer;
    CGFloat height = 20.f*numberLines;
    if (height <= 20.f) {
        height = 30.f;
    }
    self.textV.frame = CGRectMake(14.5, 12 + offsetY, kScreenWidth - 29, height);
    if (evaQuestionM.enable) {
        self.textV.editable = YES;
    } else {
        self.textV.editable = NO;
    }
    
    
    offsetY = GetView_MaxY(self.textV) + 12;
    
    frame = self.frame;
    frame.size.height = offsetY;
    self.frame = frame;
}

- (void)dealWithQues:(IXEvaQuestionM *)question complete:(void(^)(NSArray * arr, NSString * title))comoplete;
{
    __block NSString *titleStr = question.title;
    switch (question.type) {
        case IXQuestionTypeSingleSel:{
            break;
        }
        case IXQuestionTypeMutableSel:{
            titleStr = [NSString stringWithFormat:@"%@(%@)",titleStr,LocalizedString(@"可多选")];
            break;
        }
        default:
            break;
    }
    NSDictionary    * dic = question.paraphrase;
    NSDictionary    * attr = @{NSForegroundColorAttributeName:AutoNightColor(0x11b873, 0x21ce99)};
    __block NSMutableArray * array = [@[] mutableCopy];
    
    [dic enumerateKeysAndObjectsUsingBlock:^(NSString *  _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        NSRange range = [titleStr rangeOfString:key];
        if (range.length > 0) {
            key = [self dealWithHighLightedKey:key];
            titleStr = [titleStr stringByReplacingCharactersInRange:range withString:key];
            
            IXAttributeModel    * model = [IXAttributeModel new];
            model.string = key;
            model.range = range;
            model.alertImg = [UIImage imageNamed:@"common_alert"];
            model.attributeDic = attr;
            [array addObject:model];
        }
    }];
    
    if (comoplete) {
        comoplete (array, titleStr);
    }
}

- (NSString *)dealWithHighLightedKey:(NSString *)key
{
    return [key stringByReplacingOccurrencesOfString:@"#" withString:@" "];
}

@end
