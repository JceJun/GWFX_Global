//
//  IXSubQuoteStyle5Cell.h
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXSubQuoteStyle5Cell : UITableViewCell

//是否连续包月
@property (nonatomic, assign) BOOL aFee;

@end
