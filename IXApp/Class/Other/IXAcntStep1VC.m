//
//  IXAcntStep1VC.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep1VC.h"
#import "IXAcntStep2VC.h"
#import "IXTouchTableV.h"
#import "IXAcntProgressV.h"
#import "IXAcntStep1CellA.h"
#import "IXAcntStep1CellB.h"
#import "IXNewGuideV.h"
#import "IXOpenAcntModel.h"
#import "NSString+FormatterPrice.h"
#import "IXOpenChooseContentView.h"
#import "IXOpenAcntDataModel.h"
#import "IXAcntStep7VC.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Survey.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXCredentialM.h"
#import "NSObject+IX.h"
#import "IXCpyConfig.h"

@interface IXAcntStep1VC ()
<
UITableViewDelegate,
UITableViewDataSource,
IXAcntStep1CellBDelegate
>

@property (nonatomic, strong)IXTouchTableV *tableV;
@property (nonatomic, strong)UIButton *nextBtn;
@property (nonatomic, strong)IXOpenChooseContentView *chooseCata;
@property (nonatomic, strong)NSMutableArray *titleItemArr;
@property (nonatomic, strong)IXOpenAcntDataModel *acntModel;
@property (nonatomic, assign)BOOL flag;
@property (nonatomic, strong)IXEvaluationM *evaluationM;

@property (nonatomic, strong)UIView *tableHeaderV;
@property (nonatomic, assign)BOOL isOpen;

@end

@implementation IXAcntStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    self.title = LocalizedString(@"开立真实账户");

    self.navigationController.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName: AutoNightColor(0x4c6072, 0xe9e9ea),
      NSFontAttributeName :PF_MEDI(15)};
    
    self.acntModel = [[IXOpenAcntDataModel alloc] init];
    [self queryEvaluation];
}

#pragma mark 查询评测内容
- (void)queryEvaluation
{
    self.evaluationM = [[IXEvaluationM alloc] init];
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    weakself;
    [IXBORequestMgr survey_questionListWith:IXSurveyReqTypeReskEval result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        NSDictionary *dataDic = obj;
        if (success && [dataDic ix_isDictionary] && dataDic[@"questionList"]) {
            self.evaluationM = [IXEvaluationM evaluateWithDic:obj];
            [SVProgressHUD dismiss];
            [weakSelf loadData];
        } else {
            if ([errCode isEqualToString:@"NODATA"]) {
                [SVProgressHUD dismiss];
                [weakSelf loadData];
            } else {
                if (errStr.length) {
                    [SVProgressHUD showErrorWithStatus:errStr];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"查询失败")];
                }
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        }
    }];
}

- (void)loadData
{
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    _acntModel.cerType = LocalizedString(@"身份证");
    _acntModel.idDocument = @"0111";
    
    if ([IXBORequestMgr shareInstance].cerArr.count ) {
        if ( [BoLanKey isEqualToString:NAMEEN] ) {
            self.acntModel.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[0]).nameEN;
        } else if ( [BoLanKey isEqualToString:NAMECN] ) {
            self.acntModel.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[0]).nameCN;
        } else {
            self.acntModel.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[0]).nameTW;
        }
        self.acntModel.idDocument = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[0]).code;
        [self.tableV reloadData];
    }
    
    
    if ([IXBORequestMgr shareInstance].userInfo.token.length > 0) {
        [self dealWithBoData];
    } else {
        weakself;
        [IXBORequestMgr refreshUserInfo:^(BOOL success) {
            if (!success) {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [SVProgressHUD dismiss];
                [weakSelf dealWithBoData];
            }
        }];
    }
    //第一次注册成功直接开户
    [self hideNewGuide];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)hideNewGuide
{
    NSArray *subViews = [[UIApplication sharedApplication].keyWindow subviews];
    for (UIView *view in subViews) {
        if ([view isKindOfClass:[IXNewGuideV class]]) {
            [view removeFromSuperview];
        } else {
            if (view.tag == 10000 || view.tag == 10001) {
                [view removeFromSuperview];
            }
        }
    }
}

- (void)dealWithBoData
{
    _acntModel.gts2CustomerId = [NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
    _acntModel.name = [IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName;
    _acntModel.idDocument = [IXBORequestMgr shareInstance].userInfo.detailInfo.idDocument;
    if ([IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber) {
        _acntModel.cerNo = [IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber;
    }
    _acntModel.nationality = [IXBORequestMgr shareInstance].userInfo.detailInfo.nationality;
    _acntModel.country = [IXBORequestMgr shareInstance].userInfo.detailInfo.country;
    _acntModel.province = [IXBORequestMgr shareInstance].userInfo.detailInfo.province;
    _acntModel.city = [IXBORequestMgr shareInstance].userInfo.detailInfo.city;
    _acntModel.address = [IXBORequestMgr shareInstance].userInfo.detailInfo.address;
    _acntModel.postalCode = [IXBORequestMgr shareInstance].userInfo.detailInfo.postalCode;
    if (_acntModel.name.length > 0) {
        _acntModel.flag = YES;
        _flag = YES;
        _nextBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
        if (![IXBORequestMgr shareInstance].cerArr || [IXBORequestMgr shareInstance].cerArr.count == 0) {
            [self idDocumentList];
        } else {
            [self dealWithIdDocumentData];
            if (self.acntModel.flag) {
                [self dealWithCerType];
            }
        }
    }
    [self.tableV reloadData];
}

- (IXOpenChooseContentView *)chooseCata
{
    if ( !_chooseCata ) {
        weakself;
        _chooseCata = [[IXOpenChooseContentView alloc] initWithDataSource:self.titleItemArr WithSelectedRow:^(NSInteger row) {
            if ( row >= 0 && row < [IXBORequestMgr shareInstance].cerArr.count ) {
                if ( [BoLanKey isEqualToString:NAMEEN] ) {
                    weakSelf.acntModel.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).nameEN;
                } else if ( [BoLanKey isEqualToString:NAMECN] ) {
                    weakSelf.acntModel.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).nameCN;
                } else {
                    weakSelf.acntModel.cerType = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).nameTW;
                }
                weakSelf.acntModel.idDocument = ((IXCredentialM *)[IXBORequestMgr shareInstance].cerArr[row]).code;
                [weakSelf.tableV reloadData];
            }
        }];
    }
    return _chooseCata;
}

- (NSMutableArray *)titleItemArr {
    if (!_titleItemArr) {
        _titleItemArr = [[NSMutableArray alloc] init];
    }
    return _titleItemArr;
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:kScreenBound style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXAcntStep1CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXAcntStep1CellA class])];
        [_tableV registerClass:[IXAcntStep1CellB class]
        forCellReuseIdentifier:NSStringFromClass([IXAcntStep1CellB class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    if (!_tableHeaderV) {
        _tableHeaderV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
        _tableHeaderV.dk_backgroundColorPicker = DKTableHeaderColor;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18-22)/2, kScreenWidth, 18)];
        label.text = LocalizedString(@"个人信息");
        label.textAlignment = NSTextAlignmentCenter;
        label.dk_textColorPicker = DKGrayTextColor;
        label.font = PF_MEDI(15);
        [_tableHeaderV addSubview:label];
        
        label.frame = CGRectMake(0, (132-18)/2 + 5, kScreenWidth, 18);
        
        IXAcntProgressV *prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
        [_tableHeaderV addSubview:prgV];
        [prgV showFromMole:0 toMole:1 deno:(4 + self.evaluationM.pageList.count)];
    }
    
    return _tableHeaderV;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*4-kNavbarHeight)];
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_nextBtn setTitle:LocalizedString(@"下一步") forState:UIControlStateNormal];
    _nextBtn.titleLabel.font = PF_REGU(15);
    _nextBtn.userInteractionEnabled = NO;
    [v addSubview:_nextBtn];
    
    return v;
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3) {
        IXAcntStep1CellB *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntStep1CellB class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        cell.desc.dk_textColorPicker = DKGrayTextColor;
        cell.textF.dk_textColorPicker = DKCellTitleColor;
        if (indexPath.row == 0) {
            [cell loadUIWithDesc:LocalizedString(@"姓名")
                            text:_acntModel.name
                     placeHolder:LocalizedString(@"身份证上的姓名")
                    keyboardType:UIKeyboardTypeDefault
                 secureTextEntry:NO iconImage:nil
                    isHiddenIcon:YES
                         iconTag:indexPath.row];
            [cell setTextFieldEnable:!_acntModel.flag];
            cell.textF.textFont = PF_MEDI(13);
            cell.textF.placeHolderFont = PF_MEDI(13);
            if (![IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName.length) {
                cell.textF.userInteractionEnabled = YES;
            } else {
                cell.textF.userInteractionEnabled = NO;
            }
        } else if (indexPath.row == 2) {
            [cell loadUIWithDesc:LocalizedString(@"证件号")
                            text:_acntModel.cerNo
                     placeHolder:LocalizedString(@"请输入证件号")
                    keyboardType:UIKeyboardTypeNamePhonePad
                 secureTextEntry:NO
                       iconImage:nil
                    isHiddenIcon:YES
                         iconTag:indexPath.row];
            cell.textF.textFont = RO_REGU(15);
            cell.textF.placeHolderFont = PF_MEDI(13);
            if (![IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber.length) {
                cell.textF.userInteractionEnabled = YES;
            } else {
                cell.textF.userInteractionEnabled = NO;
            }
        }
        return cell;
    } else {
        IXAcntStep1CellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntStep1CellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImage * icon = AutoNightImageNamed(@"openAccount_arrow_right");
        [cell loadUIWithDesc:_acntModel.cerType
                        text:LocalizedString(@"证件信息")
                   iconImage:icon
                isHiddenIcon:NO];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        if (![IXBORequestMgr shareInstance].userInfo.detailInfo.idDocumentNumber.length) {
            if (![IXBORequestMgr shareInstance].cerArr || [IXBORequestMgr shareInstance].cerArr.count == 0) {
                [self idDocumentList];
                
            } else {
                if (self.titleItemArr.count == 0) {
                    [self dealWithIdDocumentData];
                    if (self.acntModel.flag) {
                        [self dealWithCerType];
                    }
                } else {
                    [self.titleItemArr removeAllObjects];
                    [self dealWithIdDocumentData];
                }
                [self.view endEditing:YES];
                [self.chooseCata show];
            }
        }
    }
}

- (void)idDocumentList
{
    [SVProgressHUD showWithStatus:@"处理中..."];
    
    [IXBORequestMgr acc_dictChildListWithParam:@"IdDocument"
                                    WithResult:^(BOOL success,
                                                 NSString *errCode,
                                                 NSString *errStr,
                                                 id obj)
    {
        [SVProgressHUD dismiss];
        
        if (success) {
            if (obj && [obj ix_isArray]) {
                [IXBORequestMgr processCredentialInfo:obj];
                if ([IXBORequestMgr shareInstance].cerArr.count > 0) {
                    [self dealWithIdDocumentData];
                    if (self.acntModel.flag) {
                        [self dealWithCerType];
                    } else {
                        [self.view endEditing:YES];
                        [self.chooseCata show];
                    }
                }
            }
        }
    }];
}

- (void)dealWithCerType {
    for (int i = 0; i < [IXBORequestMgr shareInstance].cerArr.count; i++) {
        IXCredentialM *model = (IXCredentialM *)([IXBORequestMgr shareInstance].cerArr[i]);
        
        if (model && [model.code isEqualToString:_acntModel.idDocument]) {
            if ( [BoLanKey isEqualToString:NAMEEN] ) {
                _acntModel.cerType = model.nameEN;
            } else if ( [BoLanKey isEqualToString:NAMECN] ) {
                _acntModel.cerType = model.nameCN;
            } else {
                _acntModel.cerType =  model.nameTW;
            }
            [self.tableV reloadData];
            break;
        }
    }
}


- (void)dealWithIdDocumentData
{
    for (int i = 0; i < [IXBORequestMgr shareInstance].cerArr.count; i++) {
        IXCredentialM   * model = (IXCredentialM *)([IXBORequestMgr shareInstance].cerArr[i]);
        NSString *name = nil;
        if ( [BoLanKey isEqualToString:NAMEEN] ) {
            name = model.nameEN;
        } else if ( [BoLanKey isEqualToString:NAMECN] ) {
            name = model.nameCN;
        } else {
            name =  model.nameTW;
        }
        if (name.length) {
            [self.titleItemArr addObject:name];
        } else {
            [self.titleItemArr addObject:@""];
        }
    }
}


#pragma mark -
#pragma mark - others

- (void)showMore
{
    
}

- (void)nextBtnClk
{
    if (!_flag) {
        
        return;
    } else {
        [self pushNextVC];
    }
}

#pragma mark IXAcntStep1CellBDelegate
- (void)clickInfo:(UIButton *)btn {
}

- (void)textValue:(NSString *)value tag:(NSInteger)tag {
    
    switch (tag) {
        case 0:{
            
            _acntModel.name = value;
        }
            break;
        case 2:{
            
            _acntModel.cerNo = value;
        }
            break;
            
        default:
            break;
    }
    if (_acntModel.name.length && _acntModel.cerNo.length) {
        _flag = YES;
    } else {
        _flag = NO;
    }
    if (_flag) {
        _nextBtn.userInteractionEnabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    } else {
        _nextBtn.userInteractionEnabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

- (void)pushNextVC
{
    IXAcntStep2VC *VC = [[IXAcntStep2VC alloc] init];
    if (!_acntModel.idDocument || _acntModel.idDocument.length == 0) {
        _acntModel.idDocument = @"0111";
    }
    VC.acntModel = _acntModel;
    VC.evaluationM = self.evaluationM;
    [self.navigationController pushViewController:VC animated:YES];
}

@end
