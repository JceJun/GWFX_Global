//
//  IXAcntBankListCell.m
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntBankListCell.h"
#import "CAGradientLayer+IX.h"
#import "UIViewExt.h"

@interface IXAcntBankListCell()<UIScrollViewDelegate>

@property (nonatomic,strong)UIImageView *bgView;
@property (nonatomic,strong)UILabel *name;
@property (nonatomic,strong)UILabel *cardNo;
@property (nonatomic,strong)UILabel *state;


@property (nonatomic, strong)UIButton *deleteBtn; //删除按钮
@property (nonatomic, assign)CGFloat rightMargin; //scroll view contentSize.width超出屏幕宽度的部分
@property (nonatomic, assign)NSInteger index;//当前选择的银行卡索引


@property(nonatomic,strong)UIButton *btn_type;

@end

@implementation IXAcntBankListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _rightMargin = 85;
        self.dk_backgroundColorPicker = DKTableColor;
        [self bgScrollV];
        [self.contentView bringSubviewToFront:self.bgScrollV];
        [self btn_type];
    }
    return self;
}

- (BOOL)ixIsEditing
{
    return self.bgScrollV.contentOffset.x > _rightMargin/2;
}

- (UIScrollView *)bgScrollV
{
    if (!_bgScrollV) {
        _bgScrollV = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,82)];
        _bgScrollV.contentSize = CGSizeMake(kScreenWidth + _rightMargin, 82);
        _bgScrollV.showsHorizontalScrollIndicator = NO;
        _bgScrollV.backgroundColor = [UIColor clearColor];
        _bgScrollV.userInteractionEnabled = YES;
        _bgScrollV.delegate = self;
        _bgScrollV.scrollEnabled = NO;
        
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(tapAction)];
        [_bgScrollV addGestureRecognizer:tap];
        
        [self.contentView addSubview:_bgScrollV];
    }
    return _bgScrollV;
}

- (UIImageView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth - 30, 82)];
        [self.bgScrollV addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel *)name
{
    if (!_name) {
        CGRect rect = CGRectMake(25, 15, VIEW_W(self.bgView) - 25, 21);
        _name = [[UILabel alloc] initWithFrame:rect];
        _name.font = PF_MEDI(13);
        _name.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0xe9e9ea);
        _name.textAlignment = NSTextAlignmentLeft;
        [self.bgView addSubview:_name];
    }
    return _name;
}

- (UILabel *)cardNo
{
    if (!_cardNo) {
        CGRect rect = CGRectMake(25, 46 -16, VIEW_W(self.bgView) - 25, 21);
        _cardNo = [[UILabel alloc] initWithFrame:rect];
        _cardNo.font = RO_REGU(15);
        _cardNo.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0xe9e9ea);
        _cardNo.textAlignment = NSTextAlignmentLeft;
        [self.bgView addSubview:_cardNo];
    }
    return _cardNo;
}

- (UILabel *)state
{
    if (!_state) {
        // x: (VIEW_W(self.bgView) - (25 + 200)
        CGRect rect = CGRectMake(25, 46, 200, 21);
        _state = [[UILabel alloc] initWithFrame:rect];
        _state.font = RO_REGU(13);
        _state.dk_textColorPicker = DKColorWithRGBs(0xffffff, 0xe9e9ea);
        _state.textAlignment = NSTextAlignmentLeft;
        [self.bgView addSubview:_state];
    }
    return _state;
}

- (UIButton *)btn_type{
    if (!_btn_type) {
        _btn_type = [UIButton new];
        _btn_type.frame = CGRectMake(150, 18, 100, 15);
        _btn_type.titleLabel.font = ROBOT_FONT(11);
        [_btn_type setTitleColor:HexRGB(0xf9a541) forState:UIControlStateNormal];
        [_btn_type setBackgroundImage:[UIImage imageNamed:@"cell_bank_type"] forState:UIControlStateNormal];
        [self.bgView addSubview:_btn_type];
    }
    return _btn_type;
}



- (UIButton *)deleteBtn
{
    if (!_deleteBtn) {
        _deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 85, 9, 85, 64)];
        [_deleteBtn setBackgroundImage:GET_IMAGE_NAME(@"bank_delete") forState:UIControlStateNormal];
        _deleteBtn.titleLabel.font = PF_MEDI(13);
        _deleteBtn.titleLabel.numberOfLines = 0;
        [_deleteBtn setTitle:LocalizedString(@"删除") forState:UIControlStateNormal];
        [_deleteBtn dk_setTitleColorPicker:DKColorWithRGBs(0xffffff, 0xe9e9ea) forState:UIControlStateNormal];
        [_deleteBtn addTarget:self action:@selector(deleteBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_deleteBtn];
    }
    return _deleteBtn;
}

- (void)deleteBtnAction
{
    if (self.deleteB) {
        self.deleteB(_index);
    }
    [self.bgScrollV setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.contentView bringSubviewToFront:self.bgScrollV];
}

- (void)tapAction
{
    if (![self ixIsEditing]) {
        if (self.tapB) {
            self.tapB(_index);
        }
    } else {
        [self endEditing];
    }
}

#pragma mark -
#pragma mark - scroll view

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (!scrollView.tracking) {
        if (scrollView.contentOffset.x > _rightMargin/2) {
//            [self beginEditing];
        } else {
//            [self endEditing];
        }
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (velocity.x == 0) {
        if (targetContentOffset->x > _rightMargin/2) {
            [scrollView setContentOffset:CGPointMake(_rightMargin, 0) animated:YES];
        } else {
            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > 1) {
        [self.contentView bringSubviewToFront:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x <= 0) {
        self.deleteBtn.hidden = YES;
    } else {
        self.deleteBtn.hidden = NO;
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self.contentView bringSubviewToFront:self.deleteBtn];
}

- (void)beginEditing
{
    [self.bgScrollV setContentOffset:CGPointMake(_rightMargin, 0) animated:YES];
    [self.contentView bringSubviewToFront:self.deleteBtn];
}

- (void)endEditing
{
    [self.bgScrollV setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.contentView bringSubviewToFront:self.bgScrollV];
}

- (void)reloadUIWithName:(NSString *)name
                  cardNo:(NSString *)cardNo
          proposalStatus:(NSString *)proposalStatus
                   index:(NSInteger)index
                    type:(NSString *)type

{
    _index = index;
    self.name.text = name;
    @try{
        self.cardNo.text = [IXDataProcessTools formatterBankCardNoDisplay:cardNo];
        NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:self.cardNo.text];
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        NSDictionary *dic = @{NSFontAttributeName:RO_REGU(15),
                              NSParagraphStyleAttributeName:paraStyle,
                              NSKernAttributeName:@3.f};
        if (self.cardNo.text.length > 4) {
            [attributeStr addAttributes:dic range:NSMakeRange(0, self.cardNo.text.length - 4)];
        }
//        if (kScreenWidth == 320) {
//            CGRect frame = self.state.frame;
//            frame.origin.y = 15;
//            self.state.frame = frame;
//        }
        self.cardNo.attributedText = attributeStr;
    }@catch(NSException *exception) {
        
    }
    
    
    CGSize textSize = [type boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_btn_type.titleLabel.font} context:nil].size;
    CGFloat textW = textSize.width;
    [_btn_type setTitle:type forState:UIControlStateNormal];
    _btn_type._width = textW+20;
    
    textSize = [name boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_name.font} context:nil].size;
    textW = textSize.width;
    
    _btn_type._left = textW + 30;
    
    if (!type.length) {
        _btn_type.hidden = YES;
    }else{
        _btn_type.hidden = NO;
    }
    [_btn_type setTitleColor:HexRGB(0xf9a541) forState:UIControlStateNormal];
    
    if ([proposalStatus isEqualToString:@"-1"]) {
        self.state.text = LocalizedString(@"未通过");
        self.bgView.dk_imagePicker = DKImageNames(@"bank_check", @"bank_check_D");
    } else if ([proposalStatus isEqualToString:@"0"]) {
        self.state.text = LocalizedString(@"未上传");
        self.bgView.dk_imagePicker = DKImageNames(@"bank_check", @"bank_check_D");
    } else if ([proposalStatus isEqualToString:@"1"]) {
        self.state.text = LocalizedString(@"审核中");
        self.bgView.dk_imagePicker = DKImageNames(@"bank_check", @"bank_check_D");
    } else if ([proposalStatus isEqualToString:@"2"]) {
        self.state.text = LocalizedString(@"已通过");
        [self.btn_type setTitleColor:HexRGB(0xf4cdaa0) forState:UIControlStateNormal];
        self.bgView.dk_imagePicker = DKImageNames(@"bank_passed", @"bank_passed_D");
    } else {
        self.state.text = LocalizedString(@"未上传");
        self.bgView.dk_imagePicker = DKImageNames(@"bank_check", @"bank_check_D");
    }
    
    
    
    
    
}

@end

