//
//  IXSubQuoteStyle3Cell.h
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^chooseAutoFee)(BOOL choose);

@interface IXSubQuoteStyle3Cell : UITableViewCell

@property (nonatomic ,strong) chooseAutoFee chooseFee;

@end
