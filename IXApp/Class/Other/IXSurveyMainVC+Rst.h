//
//  IXSurveyMainVC+Rst.h
//  IXApp
//
//  Created by Evn on 2017/10/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyMainVC.h"

@interface IXSurveyMainVC (Rst)

- (void)loadRstView;
- (void)removeAllSubviews;

@end
