//
//  IXSurveyStep1Cell.h
//  IXApp
//
//  Created by Evn on 2017/10/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXSurveyStep1Cell : UITableViewCell

- (void)loadUIWithText:(NSString *)title
                appear:(BOOL)appear
           numberLines:(NSInteger)numberLines;

@end
