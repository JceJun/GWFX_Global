//
//  IXWithdrawBankInfoVC.m
//  IXApp
//
//  Created by Larry on 2018/7/3.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXBankListVC.h"
#import "IXSupplementAccountVC.h"
#import "IXTouchTableV.h"
#import "IXPosDefultView.h"

#import "IXAcntBankListCell.h"
#import "IXAcntBankListCellA.h"

#import "IXBORequestMgr.h"
#import "IXBORequestMgr+Asset.h"
#import "IXBORequestMgr+Account.h"
#import "IXCpyConfig.h"
#import "IXUserInfoM.h"
#import "IXWithdrawBankAddVC.h"
#import "IXTipView.h"
#import "IXAddBankCardTypeVC.h"
#import "IXAddHelp2PayBankVC.h"

@interface IXBankListVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) NSString   *gts2CustomerId;
@property (nonatomic, strong) IXPosDefultView   *posDefView;
@property (nonatomic, strong) IXTouchTableV * contentTV;
@property (nonatomic, strong) NSDictionary  * dataDic;
@property (nonatomic, strong) NSMutableArray *bankListArr;
@property (nonatomic, strong) NSArray  *allBankArr;//支持的银行列表
@end

@implementation IXBankListVC

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationAddBank object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    NSString *onceId = [[NSUserDefaults standardUserDefaults] stringForKey:ONCE_HAPPEN_WithDrawBankListVC_Alert];
    if ([onceId isEqualToString:[@([IXBORequestMgr shareInstance].userInfo.detailInfo.id_p) stringValue]]) {
        [self request];
    }
    
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightItemClicked
{
    IXAddBankCardTypeVC *vc = [IXAddBankCardTypeVC new];
    [self.navigationController pushViewController:vc animated:YES];
    
//    IXWithdrawBankAddVC *vc = [IXWithdrawBankAddVC new];
//    [self.navigationController pushViewController:vc animated:YES];
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0,130, kScreenWidth, 217)];
    }
    return _posDefView;
}

#pragma mark -
#pragma mark - getter

- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds];
        _contentTV.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
        _contentTV.separatorStyle = UITableViewCellSelectionStyleNone;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.scrollEnabled = NO;
        [_contentTV registerClass:[IXAcntBankListCell class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntBankListCell class])];
        [_contentTV registerClass:[IXAcntBankListCellA class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntBankListCellA class])];
    }
    return _contentTV;
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bankListArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.bounds.size.width, 10)];
    return view ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;//section头部高度
}

- (acntBankListBlock)cellDeleteBlock
{
    weakself;
    acntBankListBlock block = ^(NSInteger index){
        [weakSelf unbindBankInfo:index];
    };
    return block;
}

- (acntBankCellTapBloack)cellTapBlock
{
    weakself;
    acntBankCellTapBloack block = ^(NSInteger index){
        [weakSelf pushToNextVC:index];
    };
    return block;
}

- (void)pushToNextVC:(NSUInteger)index
{
    IXUserBankM *bankM = _bankListArr[index];
    
    __block BOOL isGetBank = NO;
    [[IXBORequestMgr shareInstance].bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([@(bankM.id_p) isEqual:obj[@"id"]] ){//&& [obj[@"bankOrder"] isEqual:@(index+1)]) {
    
            [IXBORequestMgr shareInstance].bank = obj;
            [IXBORequestMgr filterDepositBankFile:obj];
            
            // help2pay (入金)
            [IXBORequestMgr shareInstance].bankAdditionInfo = @{@"origin":@"userInfo",
                                                                @"canDeposit":@"1",
                                                                @"canWithdraw":@"1",
                                                                @"effectType":@"withdraw",
                                                                @"gateway_name":@"help2pay",
                                                                @"gateway_code":@"help2pay",
                                                                };
            IXAddHelp2PayBankVC *vc = [IXAddHelp2PayBankVC new];
            [self.navigationController pushViewController:vc animated:YES];
            
            isGetBank = YES;
        }
    }];
    
    if (!isGetBank) {
        [[IXBORequestMgr shareInstance].withdraw_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([@(bankM.id_p) isEqual:obj[@"id"]]){ //&& [obj[@"bankOrder"] isEqual:@(index-[IXBORequestMgr mergedBankList].count+1)]) {
                [IXBORequestMgr shareInstance].withDraw_bank = obj;
                [IXBORequestMgr filterWithDrawBankFile:obj];
                
                // 电汇 (出金)
                [IXBORequestMgr shareInstance].bankAdditionInfo = @{@"origin":@"userInfo",
                                                                    @"canDeposit":@"1",
                                                                    @"canWithdraw":@"0",
                                                                    @"effectType":@"withdraw",
                                                                    @"gateway_name":@"",
                                                                    @"gateway_code":@"",
                                                                    };
                
                IXWithdrawBankAddVC *vc = [IXWithdrawBankAddVC new];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntBankListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntBankListCell class])];
    IXUserBankM   *m = _bankListArr[indexPath.row];
    
    NSString *status = m.proposalStatus;
    
    [cell reloadUIWithName:m.bank
                    cardNo:m.bankAccountNumber
            proposalStatus:status
                     index:indexPath.row
                        type:m.type];
    cell.deleteB = [self cellDeleteBlock];
    cell.tapB = [self cellTapBlock];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
    return cell;
}

- (void)unbindBankInfo:(NSInteger)index
{
    IXUserBankM * bank = self.bankListArr[index];
    if ([bank.proposalStatus isEqualToString:@"1"]) {
        [SVProgressHUD showMessage:LocalizedString(@"审核中")];
        return;
    }
    NSString    * code = [NSString stringWithFormat:@"%ld",(long)bank.id_p];
    if (code && code.length > 0) {
        [SVProgressHUD showWithStatus:LocalizedString(@"解除绑定银行卡...")];
        
        
        [IXBORequestMgr b_cancelCustomerWithdrawBanks:@[@{@"id":code}] rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
            if (success) {
                [SVProgressHUD showSuccessWithStatus:LocalizedString(@"解绑成功")];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [IXBORequestMgr refreshUserInfo:nil];
                    //删除银行卡，刷新数据
                    [self.bankListArr removeObjectAtIndex:index];
                    [self refreshUI];
                });
            } else {
                if ([errCode length]) {
                    [SVProgressHUD showErrorWithStatus:errStr];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"解绑失败")];
                }
            }
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = @"My Bank Account";
    [self.view addSubview:self.contentTV];
    
    __block NSString *onceId = [[NSUserDefaults standardUserDefaults] stringForKey:ONCE_HAPPEN_WithDrawBankListVC_Alert];
    if (![onceId isEqualToString:[@([IXBORequestMgr shareInstance].userInfo.detailInfo.id_p) stringValue]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            onceId = [@([IXBORequestMgr shareInstance].userInfo.detailInfo.id_p) stringValue];
            [[NSUserDefaults standardUserDefaults] setObject:onceId forKey:ONCE_HAPPEN_WithDrawBankListVC_Alert];
        });
        NSString *msg = @"There will no charge for any withdrawal from GWFX Global unless a specify case as a below. \n\nIf the used margin didn't reach 50% of the deposit amount, there'll be charged 6% of the withdrawal amount.\n\nIf withdrawal amount is less than 100USD, there will be charged 20USD for handling fee.\n\nIntermediary banks and local bank may also charge fees in the procedure. Please consult the relevant rules of the bank.";
        weakself;
//        [IXTipView showWithTitle:@"Withdrawal Rules" message:msg cancelTitle:@"I Know" otherTitles:nil btnBlk:^(int btnIdx) {
//            [weakSelf request];
//        }];
        NSArray *hightedArr = @[@{@"content":@"There will no charge for any withdrawal from GWFX Global unless a specify case as a below.",
                                  @"font":ROBOT_FONT(15),
                                  @"color":HexRGB(0x4c6072)
                                    },
                                @{@"content":@"If the used margin didn't reach 50% of the deposit amount, there'll be charged 6% of the withdrawal amount.\n\nIf withdrawal amount is less than 100USD, there will be charged 20USD for handling fee.\n\nIntermediary banks and local bank may also charge fees in the procedure. Please consult the relevant rules of the bank.",
                                  @"font":ROBOT_FONT(13),
                                  @"color":HexRGB(0x99abba)
                                  }
                                ];
        [IXTipView showWithTitle:@"Withdrawal rules" message:msg highLited:hightedArr cancelTitle:@"I Know" otherTitles:nil btnBlk:^(int btnIdx) {
            [weakSelf request];
        }];
        
    }else{
        
    }
}


- (void)request{
    [SVProgressHUD show];
    
    // 更新银行卡信息&文件
    [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString *errStr) {
        if (success) {
            [IXBORequestMgr b_getCustomerFiles:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
                if (success) {
                    [SVProgressHUD dismiss];
                    NSMutableArray *allBanks = [NSMutableArray arrayWithArray:[IXBORequestMgr mergedBankList]];
                    [allBanks addObjectsFromArray:[IXBORequestMgr mergedWithDrawBankList]];
                    _bankListArr = [allBanks mutableCopy];
                    [self refreshUI];
                }else{
                    [SVProgressHUD showInfoWithStatus:LocalizedString(@"获取客户文件信息失败")];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }else if (errStr.length){
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
        }
    }];
}



- (void)refreshUI
{
    if (self.bankListArr.count == 6) {
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightItemWithDayImg:GET_IMAGE_NAME(@"bank_add") nightImg:GET_IMAGE_NAME(@"bank_add_D") target:self sel:@selector(rightItemClicked) aimWidth:55];
        if (self.posDefView) {
            [self.posDefView removeFromSuperview];
        }
        if (self.bankListArr.count == 0) {
            [self.contentTV addSubview:self.posDefView];
            self.posDefView.imageName = @"bank_default";
            self.posDefView.tipMsg = LocalizedString(@"没有绑定银行卡");
            self.contentTV.scrollEnabled = NO;
        }
    }
    [self.contentTV reloadData];
}






@end
