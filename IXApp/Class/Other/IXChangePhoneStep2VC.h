//
//  IXChangePhoneStep2VC.h
//  IXApp
//
//  Created by Evn on 2017/5/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXChangePhoneStep2VC : IXDataBaseVC

@property (nonatomic, copy) NSString *phoneNum;
@property (nonatomic, strong) NSString *phoneId;

@end
