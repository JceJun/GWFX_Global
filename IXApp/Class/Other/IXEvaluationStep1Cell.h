//
//  IXEvaluationStep1Cell.h
//  IXApp
//
//  Created by Evn on 2017/11/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXEvaluationStep1Cell : UITableViewCell

- (void)loadUIWithText:(NSString *)title
                appear:(BOOL)appear
           numberLines:(NSInteger)numberLines;

@end
