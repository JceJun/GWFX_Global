//
//  IXProductInfoVC.m
//  IXApp
//
//  Created by Evn on 17/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXProductInfoVC.h"
#import "IXCpyConfig.h"

@interface IXProductInfoVC ()

@property (nonatomic, strong)UIScrollView *sView;

//for 创富
@property (nonatomic, strong) UIView *cntT;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation IXProductInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"产品介绍");
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    if ( [CompanyToken isEqualToString:@"CFIX"] ){
        [self footerView];
        [self.view addSubview:self.cntT];
        [self.view addSubview:self.lineView];
    } else {
        [self.view addSubview:self.sView];
        if ([CompanyToken isEqualToString:@"GWIX"] ||
            [CompanyToken isEqualToString:@"ix_system"] ||
            [CompanyToken isEqualToString:@"IX_CLOUD"] ||
            [CompanyToken isEqualToString:@"SD"]) {
            [self view1];
        } else {
            [self view1A];
        }
    }
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 创富公司的产品介绍
- (void)footerView
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 130+kNavbarHeight)];
    v.dk_backgroundColorPicker = DKTableColor;
    
    UIImageView *imgV = [[UIImageView alloc] init];
    float height = 116*kScreenWidth/375;
    float width = height;
    [imgV dk_setImagePicker:DKImageNames(@"common_logo", @"common_logo_D")];
    imgV.frame = CGRectMake((kScreenWidth - width)/2, 30+kNavbarHeight, width, height);
    [v addSubview:imgV];
    
    v.frame = CGRectMake(0, 0, kScreenWidth, height + 60 + kNavbarHeight);
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(v) - kLineHeight, kScreenWidth, kLineHeight)];
    lineView.dk_backgroundColorPicker = DKLineColor;
    [v addSubview:lineView];
    [self.view addSubview:v];
}

- (UIView *)cntT
{
    if (!_cntT) {
        UILabel * lab = [IXUtils createLblWithFrame:CGRectMake(15,
                                                               5,
                                                               kScreenWidth - 15*2,
                                                               0)
                                           WithFont:PF_MEDI(13)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0x4c6072
                                         dTextColor:0xe9e9ea];
        lab.numberOfLines = 0;
        lab.text = LocalizedString(@"      创富金融有限公司作为创富集团 (CF GROUPLTD) 旗下子公司。\n      自2008年成立，创富金融获取新西兰 FSP 金融监管牌照，为新西兰政府认可的金融服务商 (FSP牌照号码：FSP507506)，提供专业网上股指期货、外汇、贵金属和商品等多元化交易。");
        lab.lineBreakMode = NSLineBreakByCharWrapping;
        
        CGSize size = [lab sizeThatFits:CGSizeMake(kScreenWidth - 30, MAXFLOAT)];
        
        CGRect frame = lab.frame;
        frame.size.height = size.height;
        lab.frame = frame;
        
        _cntT = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                         116*kScreenWidth/375 + 60 + kNavbarHeight,
                                                         kScreenWidth,
                                                         size.height + 10)];
        _cntT.dk_backgroundColorPicker = DKNavBarColor;
        
        [_cntT addSubview:lab];
    }
    
    return _cntT;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, GetView_MaxY(self.cntT), kScreenWidth, kLineHeight)];
        _lineView.dk_backgroundColorPicker = DKLineColor;
    }
    return _lineView;
}

#pragma mark IX公司的产品介绍
- (UIScrollView *)sView
{
    if (!_sView) {
        _sView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight - kBtomMargin)];
        _sView.dk_backgroundColorPicker = DKTableColor;
    }
    
    return _sView;
}

- (void)view1
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 130)];
    v.dk_backgroundColorPicker = DKNavBarColor;
    [self.sView addSubview:v];
    
    UILabel *tlbl = [IXUtils createLblWithFrame:CGRectMake(15, 18,
                                                           kScreenWidth - 30,
                                                           VIEW_H(v) - 18)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x4c6072
                                     dTextColor:0xe9e9ea];
    tlbl.dk_backgroundColorPicker = DKNavBarColor;
    tlbl.numberOfLines = 0;
    
    NSString *contentStr = nil;
    if ([CompanyToken isEqualToString:@"SD"]) {
        contentStr = LocalizedString(@"      SD Global Limited（圣伦环球）积极提倡移动化交易，并是中国大陆唯一一家全面使用APP交易软件的移动化技术实施的外汇经纪商。我们坚信未来的交易是随时随地的一种生活方式与投资兴趣，只要一部手机就能链接投资者、代理商与全球银行的零距离。");
    } else {
        contentStr = [NSString stringWithFormat:LocalizedString(@"      %@APP手机交易平台集实时开户服务、高效操盘交易、即时报价分析、环球市场资讯及专家深度评价多项功能于一身。用户只需下载即可不受时间地域的限制，洞悉市场先机。"),LocalizedString(CompanyName)];
    }
    tlbl.text = contentStr;
    
    [v addSubview:tlbl];
    
    CGRect frame = tlbl.frame;
    
 
    CGSize  stringSize = [contentStr boundingRectWithSize:CGSizeMake( kScreenWidth - 30, MAXFLOAT)
                                                  options:(NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin)
                                               attributes:@{NSFontAttributeName:PF_MEDI(13)}
                                                  context:NULL].size;

    
    NSInteger height = stringSize.height + 2;
    height = MAX( height, 130);
    frame.size.height = height;
    tlbl.frame = frame;
    
    frame = v.frame;
    frame.size.height = height + 30;
    v.frame = frame;
    
    [self view2:GetView_MaxY(v)];
}

- (void)view1A
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 130)];
    v.dk_backgroundColorPicker = DKViewColor;
    [self.sView addSubview:v];
    
    UILabel *tlbl = [IXUtils createLblWithFrame:CGRectMake(15, 18,
                                                           kScreenWidth - 30,
                                                           VIEW_H(v) - 18)
                                       WithFont:PF_MEDI(13)
                                      WithAlign:NSTextAlignmentLeft
                                     wTextColor:0x4c6072
                                     dTextColor:0xe9e9ea];
    tlbl.dk_backgroundColorPicker = DKViewColor;
    tlbl.numberOfLines = 0;
    tlbl.lineBreakMode = NSLineBreakByCharWrapping;
    
    NSString *cntStr = [self getProductInfoContent];
    tlbl.text = cntStr;
    
    [v addSubview:tlbl];
    
    CGRect frame = tlbl.frame;
    
    
    CGSize  stringSize = [cntStr boundingRectWithSize:CGSizeMake( kScreenWidth - 30, MAXFLOAT)
                                                  options:(NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin)
                                               attributes:@{NSFontAttributeName:PF_MEDI(13)}
                                                  context:NULL].size;
    
    
    NSInteger height = stringSize.height + 2;
    height = MAX( height, 130);
    frame.size.height = height;
    tlbl.frame = frame;
    
    frame = v.frame;
    frame.size.height = height + 30;
    v.frame = frame;
    if (frame.size.height > kScreenHeight - kNavbarHeight) {
        self.sView.contentSize = CGSizeMake(kScreenWidth, frame.size.height + kNavbarHeight);
    }
}

- (NSString *)getProductInfoContent
{
    NSString *cntStr = nil;
    if ([CompanyToken isEqualToString:@"VFX"]) {
        cntStr = LocalizedString(@"    VFX Markets Limited （中文名“启点外汇”）是一家全球领先的外汇交易公司，总部位于美国。由一群在外汇交易行业拥有超过二十年的专业人士创立，受美国全国期货协会（简称NFA）监管。为投资者提供外汇、黄金、白银交易服务。\n    VFX的流动性来自于数十家世界顶尖银行，凭借其流动性为客户提供最好的交易体验。同时VFX通过不断的吸收最好的交易老师、坚持严谨的财务标准，来保护交易者的资金安全。");
    } else if ([CompanyToken isEqualToString:@"firfx"]) {
        cntStr = LocalizedString(@"    火鑫交易是全球著名经纪商JUST FAIP & DIRECT (简称JFD)专为中国投资者开发的一款交易软件。而JFD作为世界级金融市场分析专家和高级职业经纪商，代表着世界领先的电子交易技术理念。JFD的目标是争取在三年时间内成为领先全球的差价合约和外汇散户经纪商，为全球的资深散户投资者和机构合作者提供安全电子化的交易环境，包括保证金交易、资产管理、高频和量化交易、外汇交割、机构经纪商交易以及IT解决方案。\n    JFD同时在塞浦路斯证券和交易委员会(编号 150/11)和英国金融市场行为监管局（简称FCA编号580193）注册，并接受最为严格的监管。我们始终恪守着由当局所推行的财务报告、资本充足度方面的严格标准。\n    我们欢迎您提出评论与建议，并对任何客户关心的问题，或投诉建议（虽然这点不大可能发生）认真进行调查。我们始终准备好，以友好、开放的态度，广博的知识，为您提供服务。");
    } else if ([CompanyToken isEqualToString:@"rdd"]) {
        cntStr = LocalizedString(@"    广州瑞杜达投资管理有限公司成立于2016年，是一家旨在为高净值客户群体提供专业化综合金融服务、实现财富资产全球配置的高端综合金融服务企业。公司主要经营资产管理、财富管理及定制化服务等方面业务，始终坚持以客户财富保值、增值为己任，为客户提供“安全、稳健、专业、诚信”的金融产品及服务，并以专业高效的投资管理模式和严格的风险管理体系，为客户提供多元化、差异化、全球化、定制化的全方位财富管理服务。\n    公司投研团队由银行、信托、证券、保险、基金等各金融机构具有资深从业经验的顶尖精英组成，团队成员超过20人，大多毕业于国内外知名学府，并曾在平安陆金所、中融信托、国泰君安、金元证券等知名企业任职，运用领先的风险管理技术为广大投资者提供个性化、多样性的产品和金融资产配置方案，从而实现财富保值增值，是一支经验丰富、专业过硬的精英投、研、融、控团队。");
    } else if ([CompanyToken isEqualToString:@"VONWAY"]) {
        cntStr = LocalizedString(@"    VonWay Global（万威集团）是一家专注于全球化金融资产配置的专业化金融集团，秉承“安全”、“信诺”、“共赢”、“科技”的经营理念，为广大的个人投资者、投资家、机构提供位于行业前沿的订制化资产配置解决方案，为所有交易者提供安全、稳定、先进的交易平台，2017年，VonWay正式开启区块链技术研究与数字货币业务，除此以外，还提供：美股、港股、外汇、贵金属、能源、指数等多类交易品种。");
    } else if ([CompanyToken isEqualToString:@"ep_global"]) {
        cntStr = LocalizedString(@"    EPFOREX作为一家极具专业性和规模性的经纪商，我们的客户来自全球各地，包括资产管理人、对冲基金、高频自动化交易公司、白标经纪商、个人投资者等。我们专注于为各类机构客户提供便捷、快速、流动性高的交易平台，并结合当前最新的国际交易技术与理念，整合多家银行和流动性提供商，为客户带来绝佳的交易速度和超低的原始市场点差。\n我们的宗旨\n    维持我们在线外汇市场业界领先的地位。\n    通过财经产品的创新，持续扩大我们交易产品范围。\n    为我们的客户提供可持续增值的服务。\n    为所有的客户提供一个无与伦比的交易体验。\n创新与服务\n    面对激烈的市场竞争，我们将继续向前迈进。作为一个有责任心的企业，我们将致力于履行我们对客户的职责和责任。凭借多年深入市场的经验，我们将把创业般的精神和百分百的专业性投入对客户的服务和新产品的研发当中去。我们的目标是，为投资者打造更迅速、更便捷、更高流动性的全球金融产品交易平台。");
    } else if ([CompanyToken isEqualToString:@"MCtrader"]) {
        cntStr = LocalizedString(@"   Forex and CFD trading, Portfolio Management. Flexible leverage, wide range of trading instruments, EU regulated broker, 24/5 support.");
    } else if ([CompanyToken isEqualToString:@"firefoxtrade"]) {
        cntStr = LocalizedString(@"    Fire Fox Fintech Solutions Limited（以下简称FireFoxTrade）为全世界的客户提供网上外汇交易服务、由在国际金融行业拥有专业经验的优秀分析师、技术团队、客户管理团队组建而成。\n    我们深知客户需要什么样的交易商，如何让我们的客户在最公平，最透明的外汇市场交易环境中获利和如何让客户最快速的获得每次提款总是被放到第一位进行考虑。针对每一位客户，不论他账户内存款的多少， FireFoxTrade都会对其提供舒适和有利的交易条件。正是因此，我们在短时间内迅速成长，并被认为是最适合小资金交易的外汇交易商。\n    截止今天我们的客户遍布全世界十几个国家，其中绝大多数的客户来自于亚洲、非洲及部分欧美地区。每天都有数百个新的交易帐户开通，通过FireFoxTrade进入国际外汇和金融市场进行投资。\n    在今天 FireFoxTrade提供的流动性来自全球前6位外汇银行。这使得FireFoxTrade可以始终的站在自己的客户这一方，为客户提供领先优势的服务，这将使客户在外汇市场上取得成功，并通过我们不断的服务质量改进，帮助客户充分发挥其潜力。\n    FireFoxTrade致力于为企业和个人客户提供灵活和反应迅速的服务，作为可靠的合作伙伴帮助其客户发展业务，并为重要的社会和经济计划提供支持。FireFoxTrade 吸引客户的法宝是可靠实惠的服务以及针对客户需求量身定制的个性化方法。我们的稳健源于我们能够敏感察觉市场动态，并及时采用新的投资工具和技术。\n    如今，FireFoxTrade为客户和合作伙伴提供各种服务，帮助他们进行业务管理和有效投资。在 FireFoxTrade，我们的所有客户都能获得同样的高水准服务，无论他们有多少经验或资金。 FireFoxTrade 员工是货币和金融市场领域的顶尖专家，保证了我们的客户可以仰赖准确、专业的决策制定者。");
    }
    
    return cntStr;
}

- (void)view2:(CGFloat)offset
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, offset, kScreenWidth, 160)];
    v.dk_backgroundColorPicker = DKNavBarColor;
    [self.sView addSubview:v];
    float offsetY = 20;
    
    NSString *titleStr = nil;
    NSArray *arr = nil;
    if ([CompanyToken isEqualToString:@"SD"]) {
        titleStr = LocalizedString(@"三大优势");
        arr = @[LocalizedString(@"千余种交易商品：上千余种交易商品，包含外汇、贵金属、股票、期货、指数等多个产品类型，报价来源于世界多家投行、交易所数据服务商，价格紧贴国际市场，突破时空限制，全天候流通供应。"), LocalizedString(@"云端服务器：云端服务器采用资本投入更大的“双活”模式取代传统的“主备模式”，当面临着自然灾害、链路断开、黑客攻击等突发紧急状况，服务器自行转移，保障经纪商业务丝毫不受中断。与此同时，非农、加息等特殊时期。"),LocalizedString(@"代理商客户管理系统：云端管理服务提供功能更加全面完善的代理商管理系统，为代理商维护客户、发展客户、供众多高效管理措施，可让代理商商稳健、迅速的开拓业务。我们将更多的资金、人力与时间投入到维护服务和技术研发上，我们相信SD将为你带来前所未有的移动化交易服务！")];
    } else {
        titleStr = LocalizedString(@"特色功能");
        arr =  @[[NSString stringWithFormat:LocalizedString(@"专家独家分析指导：%@拥有强大的研究团队，为您提供全方位最具价值的评论分析，市况分析，交易策略参考等。"),LocalizedString(CompanyName)],
                 LocalizedString(@"支持云端止损止盈：即使手机断网由于止损止盈放于云端也可照样被触发。"),
                 LocalizedString(@"游客登陆：无需注册即可查看实时行情。")];
    }
    
    UILabel *title = [IXCustomView createLable:CGRectMake(15,offsetY, VIEW_W(v) - 30, 15)
                                         title:titleStr
                                          font:PF_MEDI(13)
                                    wTextColor:0x4c6072
                                    dTextColor:0xe9e9ea
                                 textAlignment:NSTextAlignmentLeft];
    [v addSubview:title];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
    lineView.dk_backgroundColorPicker = DKLineColor;
    [v addSubview:lineView];
    offsetY = GetView_MaxY(title) + 18;
    
    CGRect frame = CGRectMake(0, 0, 0, 0);
    for (int i = 0; i < arr.count; i++) {
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, offsetY, 10, 10)];
        [icon setImage:GET_IMAGE_NAME(@"income_slide")];
        [v addSubview:icon];
    
        
        CGSize labelSize = {0, 0};
        labelSize = [arr[i] boundingRectWithSize:CGSizeMake(kScreenWidth - (GetView_MaxX(icon) + 27), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:PF_MEDI(12)} context:nil].size;
        UILabel *tView = [[UILabel alloc] initWithFrame:frame];
        tView.font = PF_MEDI(12);
        tView.dk_textColorPicker = DKCellTitleColor;
        tView.text = arr[i];
        tView.backgroundColor = [UIColor clearColor];
        [v addSubview:tView];
        
        tView.numberOfLines = 0;//表示label可以多行显示
        tView.lineBreakMode = NSLineBreakByWordWrapping;//换行模式，与上面的计算保持一致。
        tView.frame = CGRectMake(GetView_MaxX(icon) + 12, offsetY - 4, kScreenWidth - (GetView_MaxX(icon) + 27), labelSize.height);
        
        offsetY = GetView_MaxY(tView) + 14;
    }
    v.frame = CGRectMake(0, offset, kScreenWidth, offsetY);
    [self view3:GetView_MaxY(v)];
}

- (void)view3:(CGFloat)offset
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, offset, kScreenWidth, 190)];
    v.dk_backgroundColorPicker = DKNavBarColor;
    [self.sView addSubview:v];
    
    float offsetY = 20;
    NSString *titleStr = nil;
    NSArray *arr = nil;
    if ([CompanyToken isEqualToString:@"SD"]) {
        titleStr = LocalizedString(@"监管牌照");
        arr = @[LocalizedString(@"SD Global Limited（圣伦环球）持有伯利兹国际金融服务委员会（IFSC）监管牌照。IFSC是政府颁布的法例，IFSC自身负责实施监管和遵守法规。IFSC颁布了国际金融服务提供商的注册监管许可条例，IFSC 牌照允许持牌商通过伯利兹从事国际金融服务。")];
    } else {
        titleStr = LocalizedString(@"功能优势");
        arr = @[LocalizedString(@"图标界面简单明了，同时配备丰富的画线工具及分析指标。"),
              LocalizedString(@"横屏看图，显示更大更清晰的图表。"),
              LocalizedString(@"最详细的交易报表，历史纪录数据，资金明显应有尽有，交易投资回报尽在掌中。"),
              LocalizedString(@"操作快人一步、简单、易用。")];
    }
    UILabel *title = [IXCustomView createLable:CGRectMake(15,offsetY, VIEW_W(v) - 30, 15)
                                         title:titleStr
                                          font:PF_MEDI(13)
                                    wTextColor:0x4c6072
                                    dTextColor:0xe9e9ea
                                 textAlignment:NSTextAlignmentLeft];
    [v addSubview:title];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
    lineView.dk_backgroundColorPicker = DKLineColor;
    [v addSubview:lineView];
    
    offsetY = GetView_MaxY(title) + 18;
    CGRect frame = CGRectMake(0, 0, 0, 0);
    
    for (int i = 0; i < arr.count; i++) {
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, offsetY, 10, 10)];
        [icon setImage:GET_IMAGE_NAME(@"income_slide")];
        [v addSubview:icon];
        
        if (i == 0 || i == 1) {
            frame = CGRectMake(GetView_MaxX(icon) + 12, offsetY, kScreenWidth - (GetView_MaxX(icon) + 27), 38);
        } else {
            frame = CGRectMake(GetView_MaxX(icon) + 12, offsetY, kScreenWidth - (GetView_MaxX(icon) + 27), 15);
        }
        CGSize labelSize = {0, 0};
        labelSize = [arr[i] boundingRectWithSize:CGSizeMake(kScreenWidth - (GetView_MaxX(icon) + 27), MAXFLOAT)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:PF_MEDI(12)}
                                         context:nil].size;
        UILabel *tView = [[UILabel alloc] initWithFrame:frame];
        tView.font = PF_MEDI(12);
        tView.dk_textColorPicker = DKCellTitleColor;
        tView.text = arr[i];
        tView.backgroundColor = [UIColor clearColor];
        [v addSubview:tView];
        
        tView.numberOfLines = 0;//表示label可以多行显示
        tView.lineBreakMode = NSLineBreakByWordWrapping;//换行模式，与上面的计算保持一致。
        tView.frame = CGRectMake(GetView_MaxX(icon) + 12,
                                 offsetY - 4,
                                 kScreenWidth - (GetView_MaxX(icon) + 27),
                                 labelSize.height);
        
        offsetY = GetView_MaxY(tView) + 14;
    }
    
    v.frame = CGRectMake(0, offset, kScreenWidth, offsetY);
    
    if ((offset + offsetY) > (kScreenHeight - kNavbarHeight)) {
        self.sView.contentSize = CGSizeMake(kScreenWidth, offset + offsetY + 50);
    }
}

@end
