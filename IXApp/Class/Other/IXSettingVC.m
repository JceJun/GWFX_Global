//
//  IXSettingVC.m
//  IXApp
//
//  Created by Evn on 16/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSettingVC.h"
#import "IXSettingCellA.h"
#import "IXLanguageVC.h"
#import "IXMsgLanVC.h"
#import "IXTimeZoneVC.h"
#import "IXDBGlobal.h"
#import "IxItemUser.pbobjc.h"
#import "IXDateUtils.h"
#import "SFHFKeychainUtils.h"
#import "AppDelegate+UI.h"
#import "IXAccountBalanceModel.h"
#import "IXAlertVC.h"
#import "IXAlertSettingVC.h"
#import "IXAcntSafeCell.h"
#import "IXUserDefaultM.h"
#import "IXUnitSettingVC.h"
#import "IXApperanceSettingVC.h"
#import "IXAboutVC.h"
#import "IXAnswersVC.h"
#import "IXCpyLogoV.h"
#import "IXCpyConfig.h"

@interface IXSettingVC ()
<
UITableViewDataSource,
UITableViewDelegate,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) IXCpyLogoV *logoV;

@end

@implementation IXSettingVC

- (id)init
{
    self = [super init];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadData)
                                                     name:kNotifyChangeLine
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];

    self.title = LocalizedString(@"设置");
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
    
    [self.view addSubview:self.tableV];
    [self initData];
    
    self.view.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handelLongPrs:)];
    [self.view addGestureRecognizer:lpgr];
}

- (void)handelLongPrs:(UILongPressGestureRecognizer *)rec
{
    switch (rec.state) {
        case UIGestureRecognizerStateBegan: {
        }
            break;
        case UIGestureRecognizerStateChanged: {
        }
            break;
        case UIGestureRecognizerStateEnded: {
            [[UIApplication sharedApplication].keyWindow addSubview:self.logoV];
        }
        case UIGestureRecognizerStateCancelled: {
        }
            break;
        default:
            break;
    }
}

- (IXCpyLogoV *)logoV
{
    if (!_logoV) {
        _logoV = [[IXCpyLogoV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    }
    return _logoV;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableV reloadData];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)reloadData
{
    [_tableV reloadData];
}

- (void)initData
{
    _titleArr = @[LanguageMode?@[LocalizedString(@"系统语言"),
                                 LocalizedString(@"通知语言"),
                                 LocalizedString(@"系统时区"),
                                 LocalizedString(@"声音设置"),
                                 LocalizedString(@"常亮设置")]:
                  @[
//                      LocalizedString(@"通知语言"),
                      LocalizedString(@"系统时区"),
                      LocalizedString(@"声音设置"),
                      LocalizedString(@"常亮设置")],
                  @[LocalizedString(@"市场模式"),
                    LocalizedString(@"DOM交易提醒"),
                    LocalizedString(@"单位切换"),
                    LocalizedString(@"升跌颜色"),
                    LocalizedString(@"更换主题")],
                  @[LocalizedString(@"常见问答"),
                    LocalizedString(@"关于我们")],
                  @[LocalizedString(@"当前线路")],
                  @[LocalizedString(@"当前版本")],
                  @[LocalizedString(@"退出登录")]];
    
    [_tableV reloadData];
}

- (UITableView *)tableV
{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,
                                                                kScreenWidth,
                                                                kScreenHeight - kNavbarHeight - kBtomMargin)
                                               style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.sectionFooterHeight = 0.f;
        _tableV.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x242a36);
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV  registerClass:[IXSettingCellA class]
         forCellReuseIdentifier:NSStringFromClass([IXSettingCellA class])];
        [_tableV  registerClass:[IXAcntSafeCell class]
         forCellReuseIdentifier:NSStringFromClass([IXAcntSafeCell class])];
    }
    return _tableV;
}


#pragma mark -
#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _titleArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray *)(_titleArr[section]) count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == self.titleArr.count - 1) {
        return 10.f;
    }
    return 0.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [tableView sectionHeaderShadow];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == self.titleArr.count -1) {
        return [tableView bottomShadow];
    }
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString    * option = _titleArr[indexPath.section][indexPath.row];
    
    if ([option isEqualToString:LocalizedString(@"DOM交易提醒")]
        || [option isEqualToString:LocalizedString(@"常亮设置")]) {
        IXAcntSafeCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntSafeCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tipName = option;
        
        if ([option isEqualToString:LocalizedString(@"DOM交易提醒")]) {
            [cell.swiBtn setOn:[IXUserDefaultM getDomResult]];
            cell.swiBtn.tag = indexPath.row;
            [cell.swiBtn addTarget:self
                            action:@selector(domTipStatus:)
                  forControlEvents:UIControlEventValueChanged];
        }
        else if ([option isEqualToString:LocalizedString(@"常亮设置")]) {
            [cell.swiBtn setOn:[IXUserDefaultM screenNormalLight]];
            [cell.swiBtn addTarget:self
                            action:@selector(normalLightSwitchAction:)
                  forControlEvents:UIControlEventValueChanged];
        }

        cell.dk_backgroundColorPicker = DKNavBarColor;
        return cell;
    }
    
    IXSettingCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    if (!cell) {
        cell = [[IXSettingCellA alloc] initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:NSStringFromClass([UITableViewCell class])];
    }
    cell.accImgV.hidden = NO;
    
    NSString *content = @"";
    if ([option isEqualToString:LocalizedString(@"系统语言")]){
        content = [IXLocalizationModel currentShowLanguage];
    }
    else if ([option isEqualToString:LocalizedString(@"通知语言")]){
        content = [IXLocalizationModel currentMessageShowLanguage];
    }
    else if ([option isEqualToString:LocalizedString(@"系统时区")]){
        content = [IXDateUtils getUserSaveTimeZone];
        [cell setContentFont:RO_REGU(15)];
    }
    else if ([option  isEqualToString:LocalizedString(@"声音设置")]){
    }
    else if ([option isEqualToString:LocalizedString(@"市场模式")]){
        content = ( [IXEntityFormatter getCellStyle] == 0 ) ?
        LocalizedString(@"普通交易") :
        LocalizedString(@"快速交易");
    }
    else if ([option isEqualToString:LocalizedString(@"升跌颜色")]){
        id value = [[NSUserDefaults standardUserDefaults] objectForKey:@"COLORSETTING"] ;
        content = ( !value || [value isEqualToString:@"RED"] )  ?
        LocalizedString(@"红涨绿跌") :
        LocalizedString(@"绿涨红跌");
    }
    else if ([option isEqualToString:LocalizedString(@"单位切换")]){
        if ([IXUserDefaultM unitSetting] == UnitSettingTypeCount) {
            content = LocalizedString(@"数量");
        }else{
            content = LocalizedString(@"手数");
        }
    }
    else if ([option isEqualToString:LocalizedString(@"更换主题")]){
        content = [IXUserDefaultM isNightMode] ? LocalizedString(@"夜间模式") : LocalizedString(@"日间模式");
    }
    else if ([option isEqualToString:LocalizedString(@"当前版本")]){
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        content =  [NSString stringWithFormat:@"V%@",[[[NSBundle mainBundle] infoDictionary]
                                                      objectForKey:@"CFBundleVersion"]];
        
        cell.userInteractionEnabled = NO;
        [cell setContentFont:RO_REGU(15)];
        cell.accImgV.hidden = YES;
    }
    else if ([option isEqualToString:LocalizedString(@"当前线路")]){
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        content = [IXTCPRequest shareInstance].routeType;
        
        cell.userInteractionEnabled = NO;
        [cell setContentFont:RO_REGU(15)];
        cell.accImgV.hidden = YES;
    }
    else if ([option isEqualToString:LocalizedString(@"退出登录")]) {
        cell.accImgV.hidden = YES;
    }
    
    [cell configTitle:option content:content];
    cell.dk_backgroundColorPicker = DKNavBarColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString    * option = _titleArr[indexPath.section][indexPath.row];
    
    UIViewController    * vc = nil;
    if ([option isEqualToString:LocalizedString(@"系统语言")]) {
        vc = [[IXLanguageVC alloc] init];
    }
    else if ([option isEqualToString:LocalizedString(@"通知语言")]){
        vc = [[IXMsgLanVC alloc] init];
    }
    else if ([option isEqualToString:LocalizedString(@"系统时区")]){
        vc = [[IXTimeZoneVC alloc] init];
    }
    else if ([option isEqualToString:LocalizedString(@"市场模式")]){
        vc = [[IXLanguageVC alloc] init];
        [(IXLanguageVC *)vc setType:SettingTypeTradeType];
    }
    else if ([option isEqualToString:LocalizedString(@"单位切换")]){
        vc = [IXUnitSettingVC new];
    }
    else if ([option isEqualToString:LocalizedString(@"升跌颜色")]){
        vc = [[IXLanguageVC alloc] init];
        [(IXLanguageVC *)vc setType:SettingTypeColor];
    }
    else if ([option isEqualToString:LocalizedString(@"声音设置")]){
        //设置声音
        vc = [IXAlertSettingVC new];
    }
    else if ([option isEqualToString:LocalizedString(@"更换主题")]){
        //设置声音
        vc = [IXApperanceSettingVC new];
    }
    else if ([option isEqualToString:LocalizedString(@"退出登录")]) {
        //退出登录
        DLog(@"退出登录");
        IXAlertVC   * alert = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"退出当前登录")
                                                       message:LocalizedString(@"您确定退出当前登录吗")
                                                   cancelTitle:LocalizedString(@"取消")
                                                    sureTitles:LocalizedString(@"确定.")];
        alert.index = 0;
        alert.expendAbleAlartViewDelegate = self;
        [alert showView];
    }
    else if ([option isEqualToString:LocalizedString(@"关于我们")]){
        vc = [[IXAboutVC alloc] init];
    }
    else if ([option isEqualToString:LocalizedString(@"常见问答")]){
        vc = [[IXAnswersVC alloc] init];
    }
    
    else if ([option isEqualToString:LocalizedString(@"清空缓存")]) {
        [SVProgressHUD showWithStatus:LocalizedString(@"正在清理缓存...")];
        if ([IXDataProcessTools clearDBCache]) {
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [delegate dealWithReLogin];
            return;
        } else {
            [SVProgressHUD dismiss];
        }
    }
    
    if (vc) {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (btnIndex == 1 && self.logoutBlock) {
        self.logoutBlock();
    }
    return YES;
}

#pragma mark -
#pragma mark - switch

- (void)domTipStatus:(UISwitch *)swi
{
    @synchronized ( self ) {
        BOOL value = [IXUserDefaultM getDomResult];
        [swi setOn:!value];
        [IXUserDefaultM saveDomResult:!value];
    }
}

- (void)normalLightSwitchAction:(UISwitch *)swi
{
    BOOL value = [IXUserDefaultM screenNormalLight];
    [swi setOn:!value];
    [IXUserDefaultM setScreenNormalLight:!value];
}

@end
