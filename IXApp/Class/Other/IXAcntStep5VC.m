//
//  IXAcntStep5VC.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep5VC.h"
#import "IXTouchTableV.h"
#import "IXAcntStep4CellA.h"
#import "IXAcntStep6VC.h"
#import "IXAcntProgressV.h"
#import "IXAFRequest.h"
#import "IXOpenAcntModel.h"
#import "NSString+FormatterPrice.h"
#import "SFHFKeychainUtils.h"
#import "AppDelegate.h"
#import "IXAcntStep5AVC.h"
#import "IXDBAccountMgr.h"
#import "IXDBGlobal.h"
#import "IXStaticsMgr.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXBORequestMgr+Account.h"
#import "RSA.h"

@interface IXAcntStep5VC ()<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic, strong)IXTouchTableV *tableV;
@property (nonatomic, strong)NSArray *titleArr;
@property (nonatomic, strong)NSArray *iconArr;

@end

@implementation IXAcntStep5VC

- (id)init
{
    self = [super init];
    if ( self ) {
        IXTradeData_listen_regist(self, PB_CMD_USERLOGIN_INFO);
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_USERLOGIN_INFO);
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if(IXTradeData_isSameKey(keyPath, PB_CMD_USERLOGIN_INFO))
    {
        proto_user_login_info *info = ((IXTradeDataCache *)object).pb_user_login_info;
        //当前账户不能登录
        if (info.result == RET_USER_ACCOUNT_GROUP_NO_LOGIN ) {
            _acntModel.state = 1;
        } else {
            _acntModel.state = 0;
        }
        //跳转开户结果页
        IXAcntStep6VC *VC = [[IXAcntStep6VC alloc] init];
        VC.acntModel = _acntModel;
        [self.navigationController pushViewController:VC animated:YES];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *titleLbl = [IXCustomView createLable:CGRectMake(0, 0, 260, 40)
                                            title:LocalizedString(@"开立真实账户")
                                             font:PF_MEDI(15)
                                       wTextColor:0x4c6072
                                       dTextColor:0xe9e9ea
                                    textAlignment:NSTextAlignmentCenter];
    self.navigationItem.titleView = titleLbl;
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    [self loadProtocolList];
}

- (NSArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = [[NSArray alloc] init];
    }
    
    return _titleArr;
}

- (void)loadProtocolList
{
    [SVProgressHUD showWithStatus:LocalizedString(@"数据加载中...")];

    [IXBORequestMgr acc_protocolListWithParam:nil
                                       result:^(BOOL success,
                                                NSString *errCode,
                                                NSString *errStr,
                                                id obj)
    {
        if (success) {
            if ([obj isKindOfClass:[NSDictionary class]] && [[obj allKeys] containsObject:@"titleList"]) {
                [SVProgressHUD dismiss];
                self.titleArr = (NSArray *)(obj[@"titleList"]);
                [self.tableV reloadData];
            } else {
                [SVProgressHUD dismiss];
            }
        } else {
            if ([errCode length]) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        }
    }];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
//        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXAcntStep4CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXAcntStep4CellA class])];
    }
    return _tableV;
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titleArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntStep4CellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntStep4CellA class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dic = _titleArr[indexPath.row];
    [cell loadUIWithText:dic[@"title"]
               logoImage:GET_IMAGE_NAME(@"openAccount_customProfile")
               iconImage:nil isHiddenIcon:NO];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntStep5AVC *VC = [[IXAcntStep5AVC alloc] init];
    VC.idStr = [(NSDictionary *)_titleArr[indexPath.row] objectForKey:@"id"];
    VC.acntTitle = [(NSDictionary *)_titleArr[indexPath.row] objectForKey:@"title"];
    
    if (!VC.idStr) {
        return;
    }
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark -
#pragma mark - others

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth - 30, 132)];
    label.text = LocalizedString(@"本人在此声明已阅读并同意接受以下:");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKGrayTextColor;
    label.font = PF_MEDI(15);
    label.numberOfLines = 0;
    [v addSubview:label];
    
    IXAcntProgressV *prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
    [v addSubview:prgV];
    [prgV showFromMole:(3 + self.evaluationM.pageList.count) toMole:(4 + self.evaluationM.pageList.count) deno:(4 + self.evaluationM.pageList.count)];
    
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*4-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
   
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    [nextBtn setTitle:LocalizedString(@"我已确认，完成开户") forState:UIControlStateNormal];
    nextBtn.titleLabel.font = PF_REGU(15);
    [v addSubview:nextBtn];
    
    return v;
}

- (void)nextBtnClk
{
#warning 已有登录信息
    if ([[IXBORequestMgr shareInstance].userInfo enable]) {
        [self openAccount];
    } else {
        [SVProgressHUD showWithStatus:LocalizedString(@"获取用户信息中...")];
        
        [IXBORequestMgr refreshUserInfo:^(BOOL success) {
            if (!success) {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
            }else{
                [SVProgressHUD dismiss];
                [self openAccount];
            }
        }];
    }
}

- (void)openAccount
{
    IXOpenAcntModel *acntModel = [[IXOpenAcntModel alloc] init];
    acntModel.accountList = (NSArray <IXAcntModel>*)[self packageAccountList];
    IXCustomerModel *customerModel = [[IXCustomerModel alloc] init];
    customerModel = [self packageCustomer];
    
    IXFinanceModel *financeModel = [[IXFinanceModel alloc] init];
    financeModel = [self packageFinance];
    
    NSString *accountStr = [acntModel toJSONString];
    accountStr = [accountStr stringByReplacingOccurrencesOfString:@"{\"accountList\":" withString:@""];
    accountStr = [accountStr stringByReplacingCharactersInRange:NSMakeRange(accountStr.length-1, 1) withString:@""];
    accountStr = [accountStr stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    accountStr = [accountStr stringByReplacingCharactersInRange:NSMakeRange(1, 1) withString:@""];
    accountStr = [accountStr stringByReplacingCharactersInRange:NSMakeRange(accountStr.length-2, 1) withString:@""];
    
    NSDictionary *paramDic = @{
                               @"customer":[customerModel toJSONString],
                               @"accountList":accountStr,
                               @"finance":[financeModel toJSONString]
                               };
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];

    [IXBORequestMgr acc_openAccountWithParam:paramDic
                                      result:^(BOOL success,
                                               NSString *errCode,
                                               NSString *errStr, id obj)
    {
        BOOL    failure = NO;
        if (success) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                NSString *proposalStatus = obj[@"proposalStatus"];
                if ([proposalStatus isEqualToString:@"1"]) {
                    //跳转开户结果页
                    [SVProgressHUD dismiss];
                    _acntModel.state = 2;//人工审核
                    IXAcntStep6VC *VC = [[IXAcntStep6VC alloc] init];
                    VC.acntModel = _acntModel;
                    [self.navigationController pushViewController:VC animated:YES];
                } else {
                    [SVProgressHUD dismiss];
                    //切换账户
                    IXBroadsideVC *side = [AppDelegate getBroadSideVC];
                    uint64_t accountId = [obj int64ForKey:@"accountId"];
                    uint64_t accGroupId;
                    NSDictionary *accDic = [IXDBAccountMgr queryAccountInfoByAccountId:accountId];
                    if (accDic) {
                        accGroupId = [accDic int64ForKey:kAccGroupId];
                    } else {
                        accGroupId = 0;
                    }
                    [side popToSwitchAccountWithAccountId:[obj int64ForKey:@"accountId"] withAccountGroupId:accGroupId];
                    _acntModel.accountId = obj[@"accountId"];
                }
            } else {
                failure = YES;
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"开户失败")];
            }
        } else {
            
            failure = YES;
            if ([errCode length]) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"开户失败")];
            }
        }
        if (failure) {
            [self notifyStatistics];
        }
    }];
}

//打包账户列表
- (NSArray *)packageAccountList {
    
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    if (_acntModel.clientType == 0) {
        IXAcntModel *model = [[IXAcntModel alloc] init];
        model.platform = PLATFORM;
        model.accountLevel = @"STANDARD";
        model.currency = _acntModel.currency;
        model.isAutoApprove = @"true";
        model.gts2CustomerId = [NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
        model.clientType = _acntModel.clientType;
        model.groupType = _acntModel.groupType;
        [retArr addObject:[model toJSONString]];
    }
    return retArr;
}

//打包用户信息
- (IXCustomerModel *)packageCustomer {
    
    IXCustomerModel *model = [[IXCustomerModel alloc] init];
    model.platform = PLATFORM;
    model.gts2CustomerId = [NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
    IXIdDocumentNumberModel *numberModel = [[IXIdDocumentNumberModel alloc] init];
    numberModel.value = _acntModel.cerNo;
    model.idDocumentNumber = numberModel;
    model.idDocumentNumberMd5 = [IXDataProcessTools md5StringByString:@"temp"];
    model.chineseName = _acntModel.name;
    model.idDocument = _acntModel.idDocument;
    model.email = _acntModel.email;
    model.address = _acntModel.address;
    model.postalCode = _acntModel.postalCode;
    model.nationality = _acntModel.nationality;
    model.province = _acntModel.province;
    model.city = _acntModel.city;
    
    return model;
}

//风险能力相关信息
- (IXFinanceModel *)packageFinance {
    
    IXFinanceModel *model = [[IXFinanceModel alloc] init];
    model.financeType = @"1";
    
    return model;
}

- (void)notifyStatistics{
    //埋点
    [[IXStaticsMgr shareInstance] buildAccountFailure];
}

@end
