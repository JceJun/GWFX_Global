//
//  IXOpenAcntModel.m
//  IXApp
//
//  Created by Evn on 17/2/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXOpenAcntModel.h"
#import "IXRegisterIDModel.h"

@implementation IXOpenAcntModel

@end

@implementation IXAcntModel

@end

@implementation IXAcntMt4Model

@end

@implementation IXCustomerModel

@end

@implementation IXIdDocumentNumberModel

@end

@implementation IXFinanceModel

@end

@implementation IXAcntInfoM

- (id)init
{
    self = [super init];
    if ( self ) {
        IXRegisterIDModel   * model = [[IXRegisterIDModel alloc] init];
        _idDocumentNumber = [[model toJSONString] copy];
        _idDocumentNumberMd5 = [IXDataProcessTools md5StringByString:[model toJSONString]];
    }
    return self;
}
@end
