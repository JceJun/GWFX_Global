//
//  IXDeviceVerifyVC.h
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

@interface IXDeviceVerifyVC : IXDataBaseVC

@property (nonatomic, strong)NSString *userName;
@property (nonatomic, strong)NSString *passWord;
@property (nonatomic, strong)NSString *phoneId;

@end
