//
//  IXCountryListVC.m
//  IXApp
//
//  Created by Evn on 17/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCountryListVC.h"
#import "IXCountryCell.h"

#import "IXBORequestMgr+Region.h"



@interface IXCountryListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *contentTV;

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) selectedCountryInfo selectCountry;
@end

@implementation IXCountryListVC

- (id)initWithSelectedInfo:(selectedCountryInfo)selectCountry
{
    self = [super init];
    if ( self ) {
        _selectCountry = selectCountry;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKNavBarColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"国家");
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.contentTV reloadData];
    
    [self checkCountryList];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}


//获取国家列表
- (void)checkCountryList
{
    weakself;
    _dataArr = [IXBORequestMgr region_refreshCountryList:^(NSArray<IXCountryM *> *list,
                                                           NSString *errStr,
                                                           BOOL hasNewData)
    {
        if (!weakSelf.dataArr.count) {
            if (weakSelf) {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取国家列表失败")];
            }
        } else {
            [weakSelf.contentTV reloadData];
        }
    }];
    
    if (!_dataArr || !_dataArr.count) {
        [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    } else {
        [_contentTV reloadData];
    }
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXCountryCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             NSStringFromClass([IXCountryCell class])];
    
    IXCountryM  *countryM = _dataArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKNavBarColor;
    [cell reloadData:countryM];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ( _selectCountry ) {
        _selectCountry(_dataArr[indexPath.row]);
        [self onGoback];
    }
}

- (UITableView *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight  - kNavbarHeight)];
        _contentTV.separatorInset = UIEdgeInsetsMake( 0, -10, 0, 0);
        _contentTV.dk_separatorColorPicker = DKLineColor;
        _contentTV.dk_backgroundColorPicker = DKViewColor;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXCountryCell class]
           forCellReuseIdentifier:NSStringFromClass([IXCountryCell class])];
        [self.view addSubview:_contentTV];
        _contentTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _contentTV;
}


@end
