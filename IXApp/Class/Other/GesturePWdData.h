//
//  GesturePWdData.h
//  AliPayDemo
//
//  Created by pg on 15/7/15.
//  Copyright (c) 2015年 pg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GesturePWdData : NSObject

/**
 处理1.7版本的bug，由于手势密码开关未与userId绑定，而手势密码与userId绑定了，
 带来了user设置手势密码之后切换账号在触发弹出手势解锁界面时需要重新输入密码登录
 */
+ (void)fixGesturePwdBugAction;

/****** 是否设置了密码 ******/
+ (BOOL)hasSeted;

/****** 忘记密码 ******/
+ (void)removePwd;

/****** 设置密码 ******/
+ (void)setPwd:(NSString *)str;

+ (NSString *)currentPwd;

@end
