//
//  IXDeviceVerifyVC.m
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDeviceVerifyVC.h"
#import "IXVerifyStepVC.h"
#import "IXVerifyStepAVC.h"

@interface IXDeviceVerifyVC ()

@end

@implementation IXDeviceVerifyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                                                            target:self
                                                                               sel:@selector(onGoback)];

    self.title = LocalizedString(@"设备验证");
    self.view.dk_backgroundColorPicker = DKTableColor;
    [self addHeaderV];
    [self addSubmitBtn];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    CGSize labelSize = {0, 0};
    labelSize = [LocalizedString(@"由于你在新设备上登陆，需要验证你的") boundingRectWithSize:CGSizeMake(kScreenWidth - 15*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:PF_MEDI(15)} context:nil].size;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, (132 - labelSize.height - 5 - 20)/2, kScreenWidth - 15*2, labelSize.height)];
    label.text = LocalizedString(@"由于你在新设备上登陆，需要验证你的");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKCellTitleColor;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = PF_MEDI(15);
    [v addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, GetView_MaxY(label) + 5, kScreenWidth, 20)];
    if ([_userName containsString:@"@"]) {
        label1.text = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"邮箱"),[IXDataProcessTools formatterPhoneDisplay:_userName]];
    } else {
        label1.text = [NSString stringWithFormat:@"%@(%@)",LocalizedString(@"手机号码"),[IXDataProcessTools formatterPhoneDisplay:_userName]];
    }
    label1.textAlignment = NSTextAlignmentCenter;
    label1.dk_textColorPicker = DKCellTitleColor;
    label1.font = PF_MEDI(15);
    [v addSubview:label1];
    
    UIView *lineV = [[UIView alloc] initWithFrame:CGRectMake(0, VIEW_H(v) - kLineHeight, kScreenWidth, kLineHeight)];
    lineV.dk_backgroundColorPicker = DKLineColor;
    [v addSubview:lineV];
    
    [self.view addSubview:v];
}

- (void)addSubmitBtn
{
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(15.5, 172, kScreenWidth - 31, 44);
    [submitBtn addTarget:self
                   action:@selector(verifyBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [submitBtn setTitle:LocalizedString(@"验证身份")
                forState:UIControlStateNormal];
    submitBtn.titleLabel.font = PF_REGU(15);
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    [submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    [self.view addSubview:submitBtn];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)verifyBtnClk
{
    if ([_userName containsString:@"@"]) {
        IXVerifyStepAVC *VC = [[IXVerifyStepAVC alloc] init];
        VC.email = _userName;
        VC.passWord = _passWord;
        [self.navigationController pushViewController:VC animated:YES];
    } else {
        IXVerifyStepVC *VC = [[IXVerifyStepVC alloc] init];
        VC.phoneNum = _userName;
        VC.phoneId = _phoneId;
        VC.passWord = _passWord;
        [self.navigationController pushViewController:VC animated:YES];
    }
}

@end
