//
//  IXSurveyStep2Cell.h
//  IXApp
//
//  Created by Evn on 2017/10/24.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "PHTextView.h"


@protocol IXSurveyStep2CellDelegate <NSObject>

- (void)textViewValue:(NSString *)value tag:(NSInteger)tag;

@end

@interface IXSurveyStep2Cell : UITableViewCell

@property (nonatomic, strong) PHTextView    * textV;
@property (nonatomic, assign) id <IXSurveyStep2CellDelegate> delegate;

- (void)loadUIWithTextView:(NSString *)text
               numberLines:(NSInteger)numberLines;

@end
