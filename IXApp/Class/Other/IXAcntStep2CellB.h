//
//  IXAcntStep2CellB.h
//  IXApp
//
//  Created by Magee on 2017/1/19.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHTextView.h"

@protocol IXAcntStep2CellBDelegate <NSObject>

- (void)textViewValue:(NSString *)value tag:(NSInteger)tag;

@end
@interface IXAcntStep2CellB : UITableViewCell
@property (nonatomic, strong) UILabel       * desc;
@property (nonatomic, strong) PHTextView    * textV;
@property (nonatomic, assign) id <IXAcntStep2CellBDelegate> delegate;

- (void)loadUIWithTextView:(NSString *)text;

- (void)setTextViewEnable:(BOOL)enable;

@end
