//
//  IXAcntSafe1Cell.m
//  IXApp
//
//  Created by Bob on 2017/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntSafeCell.h"
#import "NSString+IX.h"

@interface IXAcntSafeCell ()

@property (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) UIView    * bottomLine;
@property (nonatomic, strong) UIView    * topLine;

@end

@implementation IXAcntSafeCell

- (void)setTipName:(NSString *)tipName
{
    if (tipName) {
        _tipName = tipName;
        self.tipLbl.text = tipName;
    }
}

- (void)setContent:(NSString *)content
{
    if (content) {
        _content = content;
        self.contentLbl.hidden = NO;
        
        if (_showAttributeText) {
            self.contentLbl.attributedText = [content attributeStringWithFontSize:15
                                                                        textColor:AutoNightColor(0x99abba, 0x8395a4)];
            
            self.contentLbl.frame = CGRectMake( 0, 14,kScreenWidth - 15, 17);
        } else {
            self.contentLbl.text = content;
            self.contentLbl.frame = CGRectMake( 0, 14,kScreenWidth - 27 - 15, 17);
        }
    }
}

- (void)showTopLineWithOffsetX:(CGFloat)offset
{
    [self.contentView addSubview:self.topLine];
    self.topLine.frame = CGRectMake(offset, 0, kScreenWidth - offset, kLineHeight);
}

- (void)showBototmLineWithOffsetX:(CGFloat)offset
{
    [self.contentView addSubview:self.bottomLine];
    self.bottomLine.frame = CGRectMake(offset, 44 - kLineHeight, kScreenWidth - offset, kLineHeight);
}

#pragma mark -
#pragma mark - lazy loading

- (UIView *)bottomLine
{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
        _bottomLine.dk_backgroundColorPicker = DKLineColor;
    }
    return _bottomLine;
}

- (UIView *)topLine
{
    if (!_topLine) {
        _topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
        _topLine.dk_backgroundColorPicker = DKLineColor;
    }
    return _topLine;
}

- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 14, kScreenWidth - 15 - 70, 17)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentLeft
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea];
        [self.contentView addSubview:_tipLbl];
    }
    return _tipLbl;
}

- (UIImageView *)arrImg
{
    if ( !_arrImg ) {
        _arrImg = [[UIImageView alloc] initWithFrame:CGRectMake( kScreenWidth - 27, 18, 8, 13)];
        [self.contentView addSubview:_arrImg];
    }
    return _arrImg;
}

- (UILabel *)contentLbl
{
    if ( !_contentLbl ) {
        _contentLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 14,kScreenWidth - 27 - 15, 17)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentRight
                                       wTextColor:0x99abba
                                       dTextColor:0x8395a4];
        _contentLbl.hidden = YES;
        [self.contentView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UISwitch *)swiBtn
{
    if ( !_swiBtn ) {
        _swiBtn = [[UISwitch alloc] initWithFrame:CGRectMake( kScreenWidth - 66, 6, 51, 32)];
        _swiBtn.dk_onTintColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
//        _swiBtn.dk_thumbTintColorPicker = DKColorWithRGBs(0xffffff, 0xe9e9ea);
        _swiBtn.dk_tintColorPicker = DKColorWithRGBs(0xe5e5e5, 0x4b5667);
//        _swiBtn.dk_backgroundColorPicker = DKColorWithRGBs(0xf1f6fa, 0x171d28);
//        
//        _swiBtn.layer.cornerRadius = 15;
//        _swiBtn.clipsToBounds = YES;
        [self.contentView addSubview:_swiBtn];
    }
    
    return _swiBtn;
}

@end
