//
//  IXAcntStep5AVC.m
//  IXApp
//
//  Created by Evn on 17/3/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep5AVC.h"
#import "IXAcntStep5ACell.h"
#import "NSDictionary+Type.h"
#import "IXAFRequest.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Account.h"

@interface IXAcntStep5AVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, assign) CGFloat webHeight;

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSDictionary *model;
@property (nonatomic , strong) NSDictionary *contentDic;
@end

@implementation IXAcntStep5AVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.title = _acntTitle;
    [self loadProtocolDetail];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (NSDictionary *)contentDic {
    
    if (!_contentDic) {
        
        _contentDic = [[NSDictionary alloc] init];
    }
    
    return _contentDic;
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadProtocolDetail
{
    [SVProgressHUD showWithStatus:LocalizedString(@"数据加载中...")];

    NSDictionary *paramDic = @{
                               @"id":_idStr
                               };
    [IXBORequestMgr acc_protocolDetailWithParam:paramDic
                                         result:^(BOOL success,
                                                  NSString *errCode,
                                                  NSString *errStr,
                                                  id obj)
    {
        if (success) {
            if ([obj isKindOfClass:[NSDictionary class]] && [[obj allKeys] containsObject:@"contentView"]) {
                [SVProgressHUD dismiss];
                self.contentDic = (NSDictionary *)(obj[@"contentView"]);
                [self.tableV reloadData];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        } else {
            if ([errCode length]) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        }
    }];
}

#pragma mark -
#pragma makr - initialize

- (UITableView *)tableV
{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.tableFooterView = [[UIView alloc] initWithFrame:ktableFooterFrame];
        _tableV.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
        [_tableV registerClass:[IXAcntStep5ACell class]
        forCellReuseIdentifier:NSStringFromClass([IXAcntStep5ACell class])];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

#pragma mark -
#pragma makr - UITableViewDelegate && UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return MIN(kScreenHeight - kNavbarHeight - kBtomMargin, _webHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntStep5ACell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntStep5ACell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ( self.contentDic && [self.contentDic objectForKey:@"content"] ) {
        cell.content = [self.contentDic stringForKey:@"content"];
    }

    weakself;
    cell.refreashHeight = ^(CGFloat height){
        weakSelf.webHeight = height;
        [weakSelf.tableV reloadData];
    };
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
