//
//  IXAcntInfoAreaCellA.m
//  IXApp
//
//  Created by Evn on 2017/6/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntInfoAreaCellA.h"

@implementation IXAcntInfoAreaCellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
    [self desc];
    self.dk_backgroundColorPicker = DKNavBarColor;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(15, 12, 70, 20)];
        _title.font = PF_MEDI(13);
        _title.text = LocalizedString(@"所在地区");
        _title.textAlignment = NSTextAlignmentLeft;
        _title.dk_textColorPicker = DKCellTitleColor;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)desc
{
    if (!_desc) {
        _desc = [[UILabel alloc] initWithFrame:CGRectMake(84.5, 12, kScreenWidth - (16 + 84.5), 20)];
        _desc.font = PF_MEDI(13);
        _desc.text = LocalizedString(@"地区信息");
        _desc.textAlignment = NSTextAlignmentLeft;
        _desc.dk_textColorPicker = DKCellContentColor;
        [self.contentView addSubview:_desc];
    }
    return _desc;
}

- (void)loadUIWithDesc:(NSString *)desc
{
    if (!desc || desc.length == 0) {
        self.desc.text = LocalizedString(@"地区信息");
    } else {
        self.desc.text = desc;
    }
}

@end
