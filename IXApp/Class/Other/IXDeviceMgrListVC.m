//
//  IXDeviceMgrListVC.m
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDeviceMgrListVC.h"
#import "IXDeviceMgrCell.h"
#import "IXDeviceMgrInfoVC.h"
#import "IXDBSecureDevMgr.h"
#import "IXBORequestMgr+Account.h"
#import "IXAlertVC.h"
#import "IXAppUtil.h"

@interface IXDeviceMgrListVC ()
<
UITableViewDelegate,
UITableViewDataSource,
ExpendableAlartViewDelegate
>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation IXDeviceMgrListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.dataArr = [[IXDBSecureDevMgr querySecureDevInfoByUserid:[IXUserInfoMgr shareInstance].userLogInfo.user.id_p] mutableCopy];
    self.title = LocalizedString(@"登录设备管理");
    [self.tableV reloadData];
    [self addHint];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - lazy load

- (UITableView *)tableV
{
    if(!_tableV)
    {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight - 110)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        [_tableV registerClass:[IXDeviceMgrCell class] forCellReuseIdentifier:NSStringFromClass([IXDeviceMgrCell class])];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

- (void)addHint
{
    UILabel *lbl1 = [IXCustomView createLable:CGRectMake(0,kScreenHeight - kNavbarHeight - 64, kScreenWidth, 16)
                                        title:LocalizedString(@"您可以删除列表中的设备，删除后在该设备登陆时")
                                         font:PF_MEDI(12)
                                   wTextColor:0x99abba
                                   dTextColor:0x8395a4
                                textAlignment:NSTextAlignmentCenter];
    [self.view addSubview:lbl1];
    
    UILabel *lbl2 = [IXCustomView createLable:CGRectMake(0,GetView_MaxY(lbl1) + 4, kScreenWidth, 16)
                                        title:LocalizedString(@"需要进行身份验证。")
                                         font:PF_MEDI(12)
                                   wTextColor:0x99abba
                                   dTextColor:0x8395a4
                                textAlignment:NSTextAlignmentCenter];
    [self.view addSubview:lbl2];
}

#pragma mark -
#pragma mark - UITableViewDelegate&&dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDeviceMgrCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXDeviceMgrCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKNavBarColor;
    [cell reloadUIWithData:self.dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXDeviceMgrInfoVC *VC = [[IXDeviceMgrInfoVC alloc] init];
    VC.devDic = self.dataArr[indexPath.row];
    [self.navigationController pushViewController:VC animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && self.dataArr.count > 0) {
        return YES;
    } else {
        return NO;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kLineHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return kLineHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0,kScreenWidth, kLineHeight)];
    v.dk_backgroundColorPicker = DKLineColor;
    return v;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0,kScreenWidth, kLineHeight)];
    v.dk_backgroundColorPicker = DKLineColor;
    return v;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return LocalizedString(@"删除");
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.dataArr[indexPath.row];
    if ([dic[@"devToken"] isEqualToString:[IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]]]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"不能删除当前登陆的绑定设备")];
        return;
    }
    [self showDeletePrompt:indexPath.row];
}

- (void)showDeletePrompt:(NSInteger)index
{
    IXAlertVC *VC = [[IXAlertVC alloc] initWithTitle:LocalizedString(@"删除当前设备")
                                             message:LocalizedString(@"您确定删除当前设备吗")
                                         cancelTitle:LocalizedString(@"取消")
                                          sureTitles:LocalizedString(@"确定.")];
    VC.index = index;
    VC.expendAbleAlartViewDelegate = self;
    [VC showView];
}

- (BOOL)customAlertView:(UIView *)alertView clickedButtonAtIndex:(NSInteger)btnIndex
{
    if (btnIndex == 1) {
        [SVProgressHUD showWithStatus:LocalizedString(@"移除绑定的设备...")];

        NSDictionary *dic = self.dataArr[alertView.tag];
        NSDictionary *paramDic = @{
                                   @"deviceId":dic[@"id"],
                                   @"tradeIxCustomerId":@([IXUserInfoMgr shareInstance].userLogInfo.user.id_p)
                                   };
        
        [IXBORequestMgr acc_deleteUserDeviceWithParam:paramDic
                                           result:^(BOOL success,
                                                    NSString *errCode,
                                                    NSString *errStr, id obj)
         {
            if (success) {
                [SVProgressHUD dismiss];
                [self.dataArr removeObjectAtIndex:alertView.tag];
                [self.tableV reloadData];
            } else {
                if ([errCode length]) {
                    [SVProgressHUD showErrorWithStatus:errStr];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"移除绑定的设备失败")];
                }
            }
        }];
    }
    
    return YES;
}

@end
