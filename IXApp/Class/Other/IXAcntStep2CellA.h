//
//  IXAcntStep2CellA.h
//  IXApp
//
//  Created by Magee on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXAcntStep2CellA : UITableViewCell

@property (nonatomic, strong) UILabel   * desc;
@property (nonatomic, strong) UILabel   * title;

- (void)loadUIWithDesc:(NSString *)desc;
- (void)setHiddenIcon:(BOOL)hidden;

@end
