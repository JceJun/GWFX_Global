//
//  IXAcntCerFileVC.m
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntCerFileVC.h"
#import "IXTouchTableV.h"
#import "IXAcntCerFileCell.h"
#import "IXAcntStep7VC.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXBORequestMgr+Account.h"

@interface IXAcntCerFileVC ()<UITableViewDataSource,
UITableViewDelegate>

@property (nonatomic, strong) IXTouchTableV *contentTV;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSMutableArray *contentArr;
@property (nonatomic, strong) NSMutableArray *cerArr;
@property (nonatomic, assign) BOOL firstFlag;

@end

@implementation IXAcntCerFileVC

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(cerFileNoti:)
         name:kNotificationCerFileUpload
         object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationAddBank
                                                  object:nil];
}

- (void)cerFileNoti:(NSNotification *)noti
{
    if (noti) {
        [self loadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"证明文件");
    self.firstFlag = NO;
    [self.view addSubview:self.contentTV];
    [self loadData];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - request

- (void)loadData
{
    if ([[IXBORequestMgr shareInstance].userInfo enable]) {
        [self loadCustomerFiles];
    } else {
        [IXBORequestMgr refreshUserInfo:^(BOOL success) {
            if (!success) {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
            }else{
                [self loadCustomerFiles];
            }
        }];
    }
}

- (void)loadCustomerFiles
{
    NSDictionary *paramDic = @{
                               @"customerNumber":@([IXBORequestMgr shareInstance].userInfo.detailInfo.customerNumber)
                               };
    weakself;
    [IXBORequestMgr acc_customerFilesWithParam:paramDic
                                        result:^(BOOL success,
                                                 NSString *errCode,
                                                 NSString *errStr, id obj)
    {
        if (success && [obj isKindOfClass:[NSArray class]]) {
            weakSelf.firstFlag = YES;
            [weakSelf dealWithCerFile:obj];
            [weakSelf dealWithContentData];
            [weakSelf.contentTV reloadData];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取客户文件信息失败")];
        }
    }];
}

- (void)dealWithCerFile:(NSArray *)fileArr
{
    if (!fileArr || fileArr.count == 0) {
        return;
    }
    if (!_cerArr ) {
        _cerArr = [[NSMutableArray alloc]init];
    } else {
        [_cerArr removeAllObjects];
    }
    for (int i = 0; i < fileArr.count; i++) {
        id file = fileArr[i];
        if (file && [file isKindOfClass:[NSDictionary class]]) {
            id fileType = [(NSDictionary *)file stringForKey:@"fileType"];
            if (fileType && [fileType isKindOfClass:[NSString class]]) {
                if ([(NSString *)fileType isEqualToString:@"FILE_TYPE_IDCARD_FRONT"] || [(NSString *)fileType isEqualToString:@"FILE_TYPE_IDCARD_BACK"] || [(NSString *)fileType isEqualToString:@"FILE_TYPE_ADDRESS"]) {
                    id filePath = [(NSDictionary *)file stringForKey:@"filePath"];
                    if (filePath && [filePath isKindOfClass:[NSString class]] && [(NSString *)filePath length] > 0) {
                        [_cerArr addObject:(NSDictionary *)file];
                    }
                }
            }
        }
    }
}

- (void)dealWithContentData
{
    _contentArr = [[NSMutableArray alloc] initWithObjects:@[@{},@{}],@[@{}] ,nil];
    for (int i = 0; i < _cerArr.count; i++) {
        
        NSDictionary *fileDic = _cerArr[i];
        NSString *fileType = [(NSDictionary *)fileDic stringForKey:@"fileType"];
        if ([fileType isEqualToString:@"FILE_TYPE_IDCARD_FRONT"] || [fileType isEqualToString:@"FILE_TYPE_IDCARD_BACK"]) {
            
            NSMutableArray *retArr = nil;
            if ([_contentArr[0] isKindOfClass:[NSArray class]]) {
                retArr = [_contentArr[0] mutableCopy];
            } else {
                retArr = [[NSMutableArray alloc] initWithCapacity:2];
            }
            if ([fileType isEqualToString:@"FILE_TYPE_IDCARD_FRONT"]) {
                [retArr replaceObjectAtIndex:0 withObject:fileDic];
            } else {
                [retArr replaceObjectAtIndex:1 withObject:fileDic];
            }
            [_contentArr replaceObjectAtIndex:0 withObject:retArr];
        } else if ([fileType isEqualToString:@"FILE_TYPE_ADDRESS"]) {
            NSMutableArray *retArr = nil;
            if ([_contentArr[1] isKindOfClass:[NSArray class]]) {
                retArr = [_contentArr[1] mutableCopy];
            } else {
                retArr = [[NSMutableArray alloc] initWithCapacity:1];
            }
            [retArr replaceObjectAtIndex:0 withObject:fileDic];
            [_contentArr replaceObjectAtIndex:1 withObject:retArr];
        }
    }
}

- (NSArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = @[@[LocalizedString(@"身份证证明文件正面"),
                        LocalizedString(@"身份证证明文件反面")],
                      @[LocalizedString(@"地址证明文件")]];
    }
    return _titleArr;
}

- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds];
        _contentTV.dk_backgroundColorPicker = DKTableColor;
        _contentTV.separatorStyle = UITableViewCellSelectionStyleNone;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        [_contentTV registerClass:[IXAcntCerFileCell class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntCerFileCell class])];
    }
    return _contentTV;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ( 0 == section ) {
        return 10;
    } else {
        return 15;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v;
    if ( 0 == section ) {
        v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    } else {
        v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 15)];
    }
    v.dk_backgroundColorPicker = DKTableColor;
    return v;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray *)self.titleArr[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntCerFileCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntCerFileCell class])];
    [cell reloadUIWithTitle:self.titleArr[indexPath.section][indexPath.row] desc:self.contentArr[indexPath.section][indexPath.row] indexPathRow:indexPath.row isFirst:self.firstFlag];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    IXAcntStep7VC *VC = [[IXAcntStep7VC alloc] init];
    if (_cerArr.count) {
        VC.cerArr = _cerArr;
    }
    [self.navigationController pushViewController:VC animated:YES];
}

@end
