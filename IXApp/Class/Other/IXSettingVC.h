//
//  IXSettingVC.h
//  IXApp
//
//  Created by Evn on 16/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXDataBaseVC.h"

@interface IXSettingVC : IXDataBaseVC

@property (nonatomic, copy) void(^logoutBlock)();

@end
