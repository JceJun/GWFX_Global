//
//  IXAcntBankListCell.h
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXUserInfoM.h"

typedef void(^acntBankListBlock)(NSInteger index); //删除银行卡
typedef void (^acntBankCellTapBloack)(NSInteger index);

@interface IXAcntBankListCell : UITableViewCell
@property (nonatomic, copy) acntBankListBlock  deleteB; //删除银行卡
@property (nonatomic, copy) acntBankCellTapBloack tapB;
@property (nonatomic, strong)UIScrollView *bgScrollV;

- (void)reloadUIWithName:(NSString *)name
                  cardNo:(NSString *)cardNo
          proposalStatus:(NSString *)proposalStatus
                   index:(NSInteger)index
                    type:(NSString *)type;

@end

