//
//  IXEvaluationM.m
//  IXApp
//
//  Created by Evn on 2017/11/17.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXEvaluationM.h"
#import "NSObject+IX.h"

#pragma mark - IXEvaluationM

@interface IXEvaluationM ()

@property (nonatomic, readwrite) NSMutableArray    * questionList; //问题数组，未分类
@property (nonatomic, strong) NSMutableArray    * sectionList;  //问题分类数组

@end

@implementation IXEvaluationM
+ (instancetype)evaluateWithDic:(NSDictionary *)dic
{
    return [[IXEvaluationM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        if ([dic ix_isDictionary]) {
            [self setValuesForKeysWithDictionary:dic];
            [self dealWithData];
        }
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if (SameString(key, @"questionList")) {
        NSMutableArray  * arr = [@[] mutableCopy];
        if ([value ix_isArray]) {
            NSArray * tmpArr = value;
            for (int i = 0; i < tmpArr.count; i ++) {
                NSDictionary * dic = tmpArr[i];
                if ([dic ix_isDictionary]) {
                    [arr addObject:[IXEvaQuestionM evaQuestionWithDic:dic]];
                }
            }
        }
        value = arr;
    }
    else if (SameString(key, @"sectionList")) {
        NSMutableArray  * arr = [@[] mutableCopy];
        if ([value ix_isArray]) {
            NSArray * tmpArr = value;
            for (int i = 0; i < tmpArr.count; i ++) {
                NSDictionary * dic = tmpArr[i];
                if ([dic ix_isDictionary]) {
                    [arr addObject:[IXEvaSectionM evaSectionWithDic:dic]];
                }
            }
        }
        value = arr;
    }
    else if (SameString(key, @"id")) {
        key = @"id_p";
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{};

- (void)dealWithData
{
    int sectionId = -1;
    for (int i = 0; i < self.questionList.count; i ++) {
        IXEvaQuestionM  * quest = self.questionList[i];
        for (int j = 0; j < self.sectionList.count; j ++) {
            IXEvaSectionM   * section = self.sectionList[j];
            if (quest.sectionId == section.id_p && sectionId != section.id_p) {
                quest.sectionTitle = [NSString stringWithFormat:@"%@（%@）",section.title,section.descriptionStr];
                sectionId = section.id_p;
                break;
            }
        }
    }
    
    NSMutableArray  * array = [@[] mutableCopy];
    int page = 0;
    for (int i = 0; i < self.questionList.count; i ++) {
        IXEvaQuestionM  * m = self.questionList[i];
        if (m.allowMobilePaging) {
            NSArray * arr = [self.questionList subarrayWithRange:NSMakeRange(page, i-page+1)];
            [array addObject:arr];
            page = i + 1;
        } else if (i == self.questionList.count - 1) {
            NSArray * arr = [self.questionList subarrayWithRange:NSMakeRange(page, i-page+1)];
            [array addObject:arr];
        }
    }
    self.pageList = array;
}

@end

#pragma mark - IXEvaSectionM

@implementation IXEvaSectionM
+ (instancetype)evaSectionWithDic:(NSDictionary *)dic
{
    return [[IXEvaSectionM alloc] initWithDic:dic];
}
- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        if ([dic ix_isDictionary]) {
            [self setValuesForKeysWithDictionary:dic];
        }
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if (SameString(key, @"description")) {
        key = @"descriptionStr";
    }
    else if (SameString(key, @"id")) {
        key = @"id_p";
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

@end

#pragma mark - IXQueOptionM

@implementation IXQueOptionM

+ (instancetype)queOptionWithDic:(NSDictionary *)dic
{
    return [[IXQueOptionM alloc] initWithDic:dic];
}
- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        if ([dic ix_isDictionary]) {
            [self setValuesForKeysWithDictionary:dic];
        }
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

@end

#pragma mark - IXEvaQuestionM

@implementation IXEvaQuestionM

+ (instancetype)evaQuestionWithDic:(NSDictionary *)dic
{
    return [[IXEvaQuestionM alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init]) {
        if ([dic ix_isDictionary]) {
            [self setValuesForKeysWithDictionary:dic];
        }
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if (SameString(key, @"optionList")) {
        if ([value ix_isArray]) {
            NSMutableArray  * arr = [@[] mutableCopy];
            NSArray * valueArr = (NSArray *)value;
            
            for (int i = 0; i < valueArr.count; i ++) {
                NSDictionary    * dic = valueArr[i];
                [arr addObject:[IXQueOptionM queOptionWithDic:dic]];
            }
            
            value = arr;
        } else {
            value = @[];
        }
    }
    else if (SameString(key, @"paraphraseList")) {
        NSMutableDictionary * dic = [@{} mutableCopy];
        
        if ([value ix_isArray]) {
            [value enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [dic setValue:obj[@"explanation"] forKey:obj[@"noun"]];
            }];
        }
        value = dic;
        key = @"paraphrase";
    }
    else if (SameString(key, @"id")) {
        key = @"id_p";
    }
    
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

@end
