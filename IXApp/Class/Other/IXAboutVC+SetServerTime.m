//
//  IXAboutVC+SetServerTime.m
//  IXApp
//
//  Created by Evn on 2017/9/11.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAboutVC+SetServerTime.h"

@implementation IXAboutVC (SetServerTime)

- (void)addLongTap
{
    self.view.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handelLongPrs:)];
    [self.view addGestureRecognizer:lpgr];
}

- (void)handelLongPrs:(UILongPressGestureRecognizer *)rec
{
    switch (rec.state) {
        case UIGestureRecognizerStateBegan: {
        }
            break;
        case UIGestureRecognizerStateChanged: {
        }
            break;
        case UIGestureRecognizerStateEnded: {
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:LocalizedString(@"修改服务器时间(gmt0)") delegate:self cancelButtonTitle:LocalizedString(@"取消") destructiveButtonTitle:nil otherButtonTitles:@"周五20:20",@"20:30",@"20:59",@"21:00",@"21:01",@"21:59",@"22:00",@"周日21:59",@"22:00",@"22:01", nil];
            [sheet showInView:self.view];
        }
        case UIGestureRecognizerStateCancelled: {
        }
            break;
        default:
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    uint64_t  serverTime = 0;
    switch (buttonIndex) {
        //周五20:20
        case 0:{
            serverTime = 1505506800;
        }
            break;
        //20:30
        case 1:{
            serverTime = 1505507400;
        }
            break;
        //20:59
        case 2:{
            serverTime = 1505509140;
        }
            break;
        //21:00
        case 3:{
            serverTime = 1505509200;
        }
            break;
        //21:01
        case 4:{
            serverTime = 1505566860;
        }
            break;
        //21:59
        case 5:{
            serverTime = 1505512740;
        }
            break;
        //22:00
        case 6:{
            serverTime = 1505512800;
        }
            break;
         //周日21:59
        case 7:{
            serverTime = 1505685540;
        }
            break;
        //22:00
        case 8:{
            serverTime = 1505685600;
        }
            break;
        //22:01
        case 9:{
            serverTime = 1505685660;
        }
            break;
            
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] setObject:@(serverTime) forKey:IXServerTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
