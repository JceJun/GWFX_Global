//
//  IXPullDownMenu.m
//  IXApp
//
//  Created by Seven on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXPullDownMenu.h"
#import "IXPDMenuView.h"
#import "IXPDMenuCell.h"

#define kClearColor [UIColor clearColor]

@interface IXPullDownMenu()


@property (nonatomic, readwrite) CGFloat    rowHeight;
@property (nonatomic, readwrite) CGFloat    width;
@property (nonatomic, readwrite) BOOL       isShowing;

@property (nonatomic, strong) UIButton      * bkgroundBtn;
@property (nonatomic, strong) IXPDMenuView  * menuView;
@property (nonatomic, strong) UIView        * btomView;

@end


@implementation IXPullDownMenu

+ (IXPullDownMenu *)menuWithMenuItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width
{
    return [[IXPullDownMenu alloc] initWithMenuItems:items rowHeight:rowHeight width:width];
}

- (instancetype)initWithMenuItems:(NSArray *)items rowHeight:(CGFloat)rowHeight width:(CGFloat)width
{
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.alpha = 0.f;
        _rowHeight  = rowHeight;
        _width      = width;
        _menuItems  = items;
        [self addSubview:self.bkgroundBtn];
        [self addSubview:self.btomView];
        
        self.btomView.frame = CGRectMake((kScreenWidth - width)/2, 0, width, rowHeight*[items count]+20);
        self.menuView.frame = self.btomView.bounds;
        [self.btomView addSubview:self.menuView];
    }
    
    return self;
}

- (void)showMenuFrom:(UIView *)view items:(NSArray <NSString *>*)items offsetY:(CGFloat)offset
{
    _menuItems = [items mutableCopy];
    _menuView.items = [items mutableCopy];
    [self showMenuFrom:view offsetY:offset];
}

- (void)showMenuFrom:(UIView *)view offsetY:(CGFloat)offset
{
    if (!view)  return;
    
    if (_isShowing) {
        [self dismiss];
        return;
    }
    
    _isShowing = YES;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    CGRect  targetRect = [view convertRect:view.bounds toView:[UIApplication sharedApplication].keyWindow];
    
    CGRect  aimRect = [self cnfigMenuFrameWith:targetRect offsetY:offset];
    self.btomView.frame = aimRect;
    aimRect.origin = CGPointMake(0, -aimRect.size.height);
    self.menuView.frame = aimRect;
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.menuView.frame = CGRectMake(0, 0, aimRect.size.width, aimRect.size.height);
        self.alpha = 1.f;
    } completion:nil];
}

- (CGRect)cnfigMenuFrameWith:(CGRect)rect offsetY:(CGFloat)offset
{
    CGSize  aimSize = CGSizeMake(_width, _rowHeight*[_menuItems count]+20);
    CGFloat x = CGRectGetMidX(rect) - _width/2;
    CGFloat y = CGRectGetMaxY(rect) + offset;
    
    if (x < 0) x = 5;
    if (y < 0) y = 5;
    
    CGRect  aimRet = CGRectMake(x, y, _width, aimSize.height);
    
    if (CGRectGetMaxX(aimRet) > kScreenWidth) {
        x = kScreenWidth - aimSize.width - 5;
        [_menuView setArrowOffset:aimRet.origin.x - x + _offsetX];
    }else{
         [_menuView setArrowOffset:_offsetX];
    }
    
    if (CGRectGetMaxY(aimRet) > kScreenHeight) {
        y = kScreenHeight - aimSize.height - 5;
    }
    
    return CGRectMake(x, y, aimSize.width, aimSize.height);
}

- (void)dismiss
{
    _isShowing = NO;
    
    CGPoint origin = self.menuView.frame.origin;
    CGRect  tabRect = self.menuView.frame;
    CGPoint p = CGPointMake(origin.x, - tabRect.size.height);
    tabRect.origin = p;
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.menuView.frame = tabRect;
        self.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark -
#pragma mark - action

- (void)bkgroundBtnClick
{
    if ( self.endEditBlock ) {
        self.endEditBlock();
    }
    
    [self dismiss];
}

#pragma mark -
#pragma mark - setter

- (void)setMenuItems:(NSArray<NSString *> *)menuItems
{
    if ( menuItems && [menuItems isKindOfClass:[NSArray class]] && [menuItems count] ) {
        _menuView.items = [menuItems mutableCopy];
    }
}

- (void)setShowAttributeText:(BOOL)showAttributeText
{
    _showAttributeText = showAttributeText;
    _menuView.showAttributeText = showAttributeText;
}

#pragma mark -
#pragma mark - getter

- (UIButton *)bkgroundBtn
{
    if (!_bkgroundBtn) {
        _bkgroundBtn = [[UIButton alloc] initWithFrame:self.bounds];
        _bkgroundBtn.backgroundColor = [UIColor clearColor];
        [_bkgroundBtn addTarget:self action:@selector(bkgroundBtnClick)
               forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _bkgroundBtn;
}

- (IXPDMenuView *)menuView
{
    if (!_menuView) {
        _menuView = [IXPDMenuView menuViewWithItems:_menuItems rowHeight:_rowHeight width:_width];
        
        weakself;
        _menuView.itemClicked = ^(NSInteger index) {
            if (weakSelf.itemClicked) {
                weakSelf.itemClicked(index,weakSelf.menuItems[index]);
            }
            
            [weakSelf dismiss];
        };
    }
    
    return _menuView;
}

- (UIView *)btomView
{
    if (!_btomView) {
        _btomView = [[UIView alloc] initWithFrame:CGRectZero];
        _btomView.backgroundColor = [UIColor clearColor];
        _btomView.clipsToBounds = YES;
    }
    
    return _btomView;
}

@end
