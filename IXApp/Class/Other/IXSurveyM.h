//
//  IXSurveyM.h
//  IXApp
//
//  Created by Evn on 2017/10/27.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXQuestionM.h"


@interface IXSurveyM : NSObject


@property (nonatomic, assign) BOOL answered;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *summary;

- (void)saveSurvey:(NSArray *)dataArr;
- (NSArray *)getSurvey;
- (void)updateSurveyAtIndex:(NSInteger)index withQuestion:(IXQuestionM *)questionM;

@end
