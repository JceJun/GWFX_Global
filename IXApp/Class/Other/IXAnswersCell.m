//
//  IXAnswersCell.m
//  IXApp
//
//  Created by Evn on 2017/7/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAnswersCell.h"

@interface IXAnswersCell()

@property (nonatomic, strong)UILabel *contentLbl;
@property (nonatomic, strong)UIView *uLine;
@property (nonatomic, strong)UIView *dLine;
@property (nonatomic, strong)UIImageView *rIcon;

@end

@implementation IXAnswersCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self rIcon];
    [self uLine];
    [self dLine];
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, kScreenWidth - 30, 15)];
        _contentLbl.dk_textColorPicker = DKGrayTextColor;
        _contentLbl.font = PF_MEDI(13);
        _contentLbl.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_contentLbl];
    }
    return _contentLbl;
}

- (UIView *)uLine
{
    if (!_uLine) {
        _uLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
        _uLine.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:_uLine];
    }
    return _dLine;
}

- (UIView *)dLine
{
    if (!_dLine) {
        _dLine = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
        _dLine.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
        [self.contentView addSubview:_dLine];
    }
    return _dLine;
}

- (UIImageView *)rIcon
{
    if (!_rIcon) {
        _rIcon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - 17, 15, 7, 15)];
        _rIcon.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
        [self.contentView addSubview:_rIcon];
    }
    return _rIcon;
}

- (void)reloadUI:(NSString *)content
       indexPath:(NSIndexPath *)indexPath
          counts:(NSInteger)counts
{
    self.contentLbl.text = content;
    if (indexPath.row == 0) {
        self.uLine.frame = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
    } else {
        self.uLine.frame = CGRectMake(15, 44 - kLineHeight, kScreenWidth - 15, kLineHeight);
    }
    if (indexPath.row == counts) {
        self.dLine.hidden = NO;
        self.dLine.frame = CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight);
    } else {
        self.dLine.hidden = YES;
    }
}

@end
