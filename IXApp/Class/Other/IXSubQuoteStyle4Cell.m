//
//  IXSubQuoteStyle4Cell.m
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubQuoteStyle4Cell.h"

@interface IXSubQuoteStyle4Cell ()


@property (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) UIButton *operationBtn;

@end

@implementation IXSubQuoteStyle4Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        ;
    }
    return self;
}

- (void)setState:(QUOTESTATE)state
{
    _state = state;
    switch ( _state ) {
        case QUOTESTATESUB:{
            [self.operationBtn setTitle:@"取消订阅" forState:UIControlStateNormal];
            self.tipLbl.text = @"如果你现在取消，在2017/03/31之前你仍可访问行情";
        }
            break;
        case QUOTESTATEUNSUB:{
            [self.operationBtn setTitle:@"开通服务" forState:UIControlStateNormal];
            self.tipLbl.text = @"免责政策:成功开通服务后，每月均返还订阅金额";
        }
            break;
        default:
            break;
    }
}

- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 0, 94, kScreenWidth, 12)
                                     WithFont:PF_MEDI(12)
                                    WithAlign:NSTextAlignmentCenter
                                   wTextColor:0x99abba
                                   dTextColor:0xff0000];
        [self.contentView addSubview:_tipLbl];
    }
    return _tipLbl;
}

- (UIButton *)operationBtn{
    if ( !_operationBtn ) {
        _operationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _operationBtn.frame = CGRectMake( 15, 40, kScreenWidth - 30, 42);
        [self.contentView addSubview:_operationBtn];
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2
                                           topCapHeight:image.size.height/2];
        [_operationBtn setBackgroundImage:image
                            forState:UIControlStateNormal];
        [_operationBtn.titleLabel setTextColor:[UIColor whiteColor]];
        _operationBtn.tag = OPERATIONTAG;
    }
    return _operationBtn;
}
@end
