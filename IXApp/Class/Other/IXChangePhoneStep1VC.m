//
//  IXChangePhoneStep1VC.m
//  IXApp
//
//  Created by Evn on 2017/5/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXChangePhoneStep1VC.h"
#import "IXChangePhoneStep2VC.h"
#import "IXTouchTableV.h"
#import "IXRegistStep1CellB.h"
#import "IXRegistStep1CellC.h"
#import "IXCountryListVC.h"

#import "NSString+FormatterPrice.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Account.h"
#import "IXUserDefaultM.h"

@interface IXChangePhoneStep1VC ()<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>

@property (nonatomic, strong)IXTouchTableV  * tableV;
@property (nonatomic, strong)UIButton   * registBtn;

@property (nonatomic, strong)IXCountryM    *countryInfo;

@property (nonatomic, copy) NSString    * phoneNum;

@end

@implementation IXChangePhoneStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    self.title = LocalizedString(@"更换手机号码");
    
    [self setCountry:[IXUserDefaultM nationalityByCode:[IXUserDefaultM getCode]]];

    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark -
#pragma mark - btn action
- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registBtnClk
{
    [self.view endEditing:YES];
    IXRegistStep1CellB *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    //手机号合法位数最少为5
    if ( cell.textF.text.length < PHONEMINLENGTH ) {
        [SVProgressHUD showErrorWithStatus:PHONEMINCHAR];
        return;
    }
    [self validPhone:cell.textF.text];
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        static NSString *identifier = @"IXRegistStep1CellC";
        IXRegistStep1CellC *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[IXRegistStep1CellC alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title.dk_textColorPicker = DKGrayTextColor;
        cell.desc.dk_textColorPicker = DKCellTitleColor;
        
        [cell loadUIWithDesc:[self.countryInfo localizedName]];
        return cell;
    } else {
        IXRegistStep1CellB *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
        cell.textF.delegate = self;
        [cell loadUIWithTFText:self.phoneNum withDesc:[NSString stringWithFormat:@"+ %ld",(long)self.countryInfo.countryCode] withTag:indexPath.row];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self selectCountry];
    }
}

- (void)selectCountry
{
    weakself;
    IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
        [weakSelf setCountry:country];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setCountry:(IXCountryM *)country
{
    if (!self.countryInfo) {
        self.countryInfo = [[IXCountryM alloc] init];
    }
    //默认中国
    if (!country) {
        NSString *nationalCode = [IXUserDefaultM getCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityByCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        [IXUserDefaultM saveCountryCode:countryM.nationalCode];
//        if ([nationalCode isEqualToString:@"CN"]) {
//            self.countryInfo.countryCode = 86;
//        }
    } else {
        self.countryInfo.nameCN = country.nameCN;
        self.countryInfo.nameEN = country.nameEN;
        self.countryInfo.nameTW = country.nameTW;
        self.countryInfo.countryCode = country.countryCode;
        [IXUserDefaultM saveCountryCode:country.nationalCode];
        [self.tableV reloadData];
        self.tableV.tableFooterView = [self tableFooterV];
//        if (self.countryInfo.countryCode == 86) {
//            [self.tableV reloadData];
//            self.tableV.tableFooterView = [self tableFooterV];
//        } else {
//            [self.tableV reloadData];
//            self.tableV.tableFooterView = [self tableFooterV];
//        }
        
    }
}


#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (aimStr.length >= 5) {
        [self registerBtnEnable:YES];
        _phoneNum = aimStr;
    }else{
        [self registerBtnEnable:NO];
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self registerBtnEnable:NO];
    return YES;
}

- (void)registerBtnEnable:(BOOL)enable
{
    if (enable) {
        _registBtn.enabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        _registBtn.enabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark - create UI
- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.scrollEnabled = NO;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXRegistStep1CellC class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellC class])];
        [_tableV registerClass:[IXRegistStep1CellB class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep1CellB class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18)/2, kScreenWidth, 18)];
    label.text = LocalizedString(@"请输入手机号码");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKCellTitleColor;
    label.font = PF_MEDI(15);
    [v addSubview:label];
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*2-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    _registBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _registBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_registBtn addTarget:self
                   action:@selector(registBtnClk)
         forControlEvents:UIControlEventTouchUpInside];
    [_registBtn setTitle:LocalizedString(@"提 交")
                forState:UIControlStateNormal];
    _registBtn.titleLabel.font = PF_REGU(15);
    _registBtn.enabled = NO;
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    [_registBtn setBackgroundImage:image forState:UIControlStateNormal];
    [v addSubview:_registBtn];
    
    return v;
}

#pragma mark -
#pragma mark - request

- (void)validPhone:(NSString *)phoneNum
{
    IXEmailPhoneModel *model = [[IXEmailPhoneModel alloc] init];
    model.index = 1;
    model.mobilePhone = phoneNum;
    model.mobilePhonePrefix = [NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode];
    [SVProgressHUD showWithStatus:LocalizedString(@"校验手机号...")];
    
    [IXBORequestMgr acc_checkEmailOrPhoneExistWith:model complete:^(BOOL exist, NSString *errStr) {
        if (exist) {
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD dismiss];
            
            BOOL k = VerifyCodeEnable;
            if (k) {
                [self popToValidCode];
            }else{
#warning 待处理
                // [strong_self popToStep3VC];
            }
            
            return;
        }
    }];
}

- (void)popToValidCode
{
    IXChangePhoneStep2VC *vc = [[IXChangePhoneStep2VC alloc] init];
    vc.phoneNum = self.phoneNum;
    vc.phoneId = [NSString stringWithFormat:@"%ld",(long)self.countryInfo.countryCode];;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
