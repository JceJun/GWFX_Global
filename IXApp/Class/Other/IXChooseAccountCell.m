//
//  IXChooseAccountCell.m
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXChooseAccountCell.h"


@interface IXChooseAccountCell ()

@property (nonatomic, strong) UIImageView *statusImg;
@property (nonatomic, strong) UIImageView *chooseImg;

@property (nonatomic, strong) UILabel *currencyLbl;
@property (nonatomic, strong) UILabel *accountLbl;


@end

@implementation IXChooseAccountCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        self.dk_backgroundColorPicker = DKNavBarColor;
    }
    return self;
}


- (void)setModel:(NSDictionary *)model
{
    if ( model ) {
        _model = model;
        
        NSString    * statusImgName = [IXUtils tranImageName:@"switchAccount_real"];
        [self.statusImg setImage:GET_IMAGE_NAME(statusImgName)];
        int32_t clientType = [model[@"clientType"] intValue];
        uint32_t type = [model[@"type"] intValue];
        int32_t options = [model[@"options"] intValue];
        NSString *typeStr = nil;
        NSString *idStr = nil;
        if (clientType == 0) {
            if (self.isExsitMt4) {
                typeStr = [NSString stringWithFormat:@"-IX-%@",[IXDataProcessTools getAccountNameByType:type]];
            } else {
                typeStr = [NSString stringWithFormat:@"-%@",[IXDataProcessTools getAccountNameByType:type]];
            }
            idStr = [model stringForKey:@"id"];
        } else {
            typeStr = [NSString stringWithFormat:@"-MT4-%@",[IXDataProcessTools getAccountNameByType:type]];
            idStr = [model stringForKey:@"refAccid"];
        }
        //options:3 app当前账户不能登录
        if (options == 3) {
            self.dk_backgroundColorPicker = DKColorWithRGBs(0xfafcfe, 0x4c6072);
        }
        self.currencyLbl.text = [NSString stringWithFormat:@"%@:%@%@",
                                 LocalizedString(@"货币"),
                                 [model stringForKey:@"currency"],typeStr];
        self.accountLbl.text = [NSString stringWithFormat:@"%@:%@",
                                LocalizedString(@"真实账号"),idStr
                                ];
        
        [self.chooseImg setImage:AutoNightImageNamed(@"common_cell_choose")];
        [self.chooseImg setHidden:!([[model stringForKey:@"id"] longLongValue] == [IXUserInfoMgr shareInstance].userLogInfo.account.id_p)];
    }
}

- (void)setIsCurrentAccount:(BOOL)isCurrentAccount
{
    [self.chooseImg setHidden:!isCurrentAccount];
}

- (UIImageView *)statusImg
{
    if ( !_statusImg ) {
        _statusImg = [[UIImageView alloc] initWithFrame:CGRectMake( 20, 17, 21, 21)];
        [self.contentView addSubview:_statusImg];
    }
    return _statusImg;
}

- (UIImageView *)chooseImg
{
    if ( !_chooseImg ) {
        _chooseImg = [[UIImageView alloc] initWithFrame:CGRectMake( kScreenWidth - 111, 19, 16, 16)];
        [self.contentView addSubview:_chooseImg];
    }
    return _chooseImg;
}

- (UILabel *)currencyLbl
{
    if ( !_currencyLbl ) {
        _currencyLbl = [IXUtils createLblWithFrame:CGRectMake( 51, 12, kScreenWidth - (80 + 81), 15)
                                          WithFont:PF_MEDI(13)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x4c6072
                                        dTextColor:0xe9e9ea];
        [self.contentView addSubview:_currencyLbl];
    }
    return _currencyLbl;
}

- (UILabel *)accountLbl
{
    if ( !_accountLbl ) {
        _accountLbl = [IXUtils createLblWithFrame:CGRectMake( 51, 33, 150, 12)
                                         WithFont:PF_MEDI(12)
                                        WithAlign:NSTextAlignmentLeft
                                       wTextColor:0xa7adb5
                                       dTextColor:0x8395a4];
        
        [self.contentView addSubview:_accountLbl];
    }
    return _accountLbl;
}

@end
