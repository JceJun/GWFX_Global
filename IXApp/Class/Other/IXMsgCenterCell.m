//
//  IXMsgCenterCell.m
//  IXApp
//
//  Created by Seven on 2017/4/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXMsgCenterCell.h"
#import "IXUserInfoM.h"
#import "IXUserDefaultM.h"
#import "UIViewExt.h"

#define kTimeLabSize [@"2017/05/31 23:59:590" sizeWithAttributes:@{NSFontAttributeName : RO_REGU(12)}]

@interface IXMsgCenterCell ()

/** 未读标识 */
@property (nonatomic, strong) UIView    * noReadV;
@property (nonatomic, strong) UIColor   * bezierColor;  //消息title背景色
@property (nonatomic, strong) UIColor   * contentColor; //消息主题背景色
@end

@implementation IXMsgCenterCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createSubview];
        self.backgroundColor = [UIColor clearColor];
        
        BOOL    isNightMode = [IXUserDefaultM isNightMode];
        _bezierColor = isNightMode ?
        UIColorHexFromRGB(0x262f3e) : UIColorHexFromRGB(0xfefefe);
        _contentColor = isNightMode ?
        UIColorHexFromRGB(0x262f3e) : UIColorHexFromRGB(0xfafcfe);
        self.layer.shadowRadius = 3.f;
        
        if (![IXUserDefaultM isNightMode]) {
            UIColor * c = isNightMode ? UIColorHexFromRGB(0x242a36) : UIColorHexFromRGB(0xe2eaf2);
            self.layer.shadowColor = c.CGColor;
            self.layer.shadowOffset = CGSizeZero;
            self.layer.shadowOpacity = 1.f;
        }
    }
    return self;
}

- (void)createSubview
{
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.timeLab];
    [self.contentView addSubview:self.detailLab];
}

- (void)setMessage:(IXMsgCenterM *)message
{
    _message = message;
    self.titleLab.text  = message.msgTitle;
    self.detailLab.text = message.msgContent;
    self.timeLab.text   = [message timeString];
    
    CGSize  size = [self.titleLab sizeThatFits:CGSizeMake(MAXFLOAT, 20)];
    CGPoint p = self.titleLab.frame.origin;
    
    //未读提示
    if (!message.readStatus) {
        p = CGPointMake(p.x+size.width, p.y);
        CGFloat x = p.x + 2;
        if (x > GetView_MaxX(_titleLab) - 4) {
            x = GetView_MaxX(_titleLab) - 4;
        }
        
        size = self.noReadV.frame.size;

        self.noReadV.frame = CGRectMake(x, p.y - 2, size.width, size.height);
        self.noReadV.hidden = NO;
        [self.contentView addSubview:self.noReadV];
        
        self.titleLab.dk_textColorPicker = DKColorWithRGBs(0x232935, 0xe9e9ea);
        self.detailLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x8395a4);
        self.timeLab.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
    }else{
        self.noReadV.hidden = YES;
        self.timeLab.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4c6072);
        self.titleLab.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4c6072);
        self.detailLab.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x4c6072);
    }
    
    //详情
    p = self.detailLab.frame.origin;
    size = [self.detailLab sizeThatFits:CGSizeMake(self.frame.size.width - 48, MAXFLOAT)];
    self.detailLab.frame = CGRectMake(p.x, p.y, size.width, size.height);
    self.timeLab._top = self.detailLab._bottom + 10;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    BOOL    isNightMode = [IXUserDefaultM isNightMode];
    //未读标标识
    CGRect  aimR = CGRectMake(CGRectGetMaxX(self.titleLab.frame),
                              CGRectGetMinY(self.titleLab.frame) - 5,
                              8,
                              8);

    UIBezierPath    * path = [UIBezierPath bezierPathWithRoundedRect:aimR cornerRadius:4];
    UIColor * markColor = isNightMode ? UIColorHexFromRGB(0xff4653) : UIColorHexFromRGB(0xff4d2d);
    [markColor setFill];
    [path fill];

    //画圆角
    UIBezierPath    * corner = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:0.f];
    [_bezierColor setFill];
    [corner fill];

    //消息主体视图背景
//    UIBezierPath    * btmBeizer = [UIBezierPath bezierPath];
//    CGPoint p0 = CGPointMake(rect.origin.x, 46);
//    [btmBeizer moveToPoint:p0];
//
//    CGPoint p1 = CGPointMake(CGRectGetMaxX(rect), 46);
//    [btmBeizer addArcWithCenter:p1
//                         radius:0
//                     startAngle:3*M_PI/2
//                       endAngle:0
//                      clockwise:YES];
//
//    CGPoint c0 = CGPointMake(p1.x - 2, CGRectGetMaxY(rect) - 2);
//    [btmBeizer addArcWithCenter:c0
//                         radius:2
//                     startAngle:0
//                       endAngle:M_PI/2
//                      clockwise:YES];
//
//    CGPoint c1 = CGPointMake(p0.x+2, c0.y);
//    [btmBeizer addArcWithCenter:c1
//                         radius:2
//                     startAngle:M_PI/2
//                       endAngle:M_PI
//                      clockwise:YES];
//    [btmBeizer addLineToPoint:p0];
//    [_contentColor setFill];
//    [btmBeizer fill];
}


#pragma mark -
#pragma mark - touch action

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    _bezierColor = [IXUserDefaultM isNightMode] ? UIColorHexFromRGB(0x242a36) : UIColorHexFromRGB(0xf1f6fa);
    _contentColor = [IXUserDefaultM isNightMode] ? UIColorHexFromRGB(0x242a36) : UIColorHexFromRGB(0xf1f6fa);
    
    [UIView animateWithDuration:0.2 animations:^{
        [self setNeedsDisplay];
    }];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self resetBkColor];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    [self resetBkColor];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    [self resetBkColor];
}

- (void)resetBkColor
{
    _bezierColor = [IXUserDefaultM isNightMode] ?
    UIColorHexFromRGB(0x262f3e) : UIColorHexFromRGB(0xfefefe);
    _contentColor = [IXUserDefaultM isNightMode] ?
    UIColorHexFromRGB(0x262f3e) : UIColorHexFromRGB(0xfafcfe);
    
    [UIView animateWithDuration:0.4 animations:^{
        [self setNeedsDisplay];
    }];
}

#pragma mark -
#pragma mark - alzy loading

- (UILabel *)titleLab
{
    if (!_titleLab) {
        CGFloat width = self.frame.size.width;
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(24,
                                                              15,
                                                              width - 24,//kTimeLabSize.width - 45,
                                                              15)];
        _titleLab.font = PF_MEDI(14);
        _titleLab.textColor = MarketSymbolNameColor;
    }
    
    return _titleLab;
}

- (UILabel *)timeLab
{
    if (!_timeLab) {
        CGFloat width = self.frame.size.width;
        _timeLab = [[UILabel alloc] initWithFrame:CGRectMake(width - kTimeLabSize.width - 24,
                                                             100,
                                                             kTimeLabSize.width,
                                                             13)];
        _timeLab.textAlignment = NSTextAlignmentRight;
        _timeLab.textColor = MarketGrayPriceColor;
        _timeLab.font = RO_REGU(12);
    }
    
    return _timeLab;
}

- (UILabel *)detailLab
{
    if (!_detailLab) {
        CGFloat width = self.frame.size.width;
        _detailLab = [[UILabel alloc] initWithFrame:CGRectMake(24,
                                                               45,
                                                               width - 48,
                                                               20)];
        _detailLab.font = PF_MEDI(14);
        _detailLab.numberOfLines = 3;
        _detailLab.textColor = MarketSymbolNameColor;
    }
    
    return _detailLab;
}

- (UIView *)noReadV{
    if (!_noReadV) {
        _noReadV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 8)];
        _noReadV.layer.cornerRadius = 4.f;
        _noReadV.backgroundColor = UIColorHexFromRGB(0xff4653);
    }
    
    return _noReadV;
}

@end
