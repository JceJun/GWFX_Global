//
//  IXDocumentView.m
//  IXApp
//
//  Created by Larry on 2018/6/28.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDocumentView.h"
#import "UIKit+Block.h"
#import "IXZoomScrollView.h"

@interface IXDocumentView()
@property(nonatomic,strong)UIImageView *img_tip;


@end

@implementation IXDocumentView

+ (instancetype)makeDocument:(NSString *)docName superView:(UIView *)superView clearUploadInfo:(void(^)(void))clearUploadInfo{
    IXDocumentView *view = [IXDocumentView new];
    view.dk_backgroundColorPicker = DKViewColor;
    [superView addSubview:view];
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(160);
        make.width.equalTo(250);
        make.centerX.equalTo(0);
    }];
    view.layer.cornerRadius = 5;
    view.layer.masksToBounds = YES;
    
    view.lb_tip.text = docName;
    [view dealDocName:docName];
    view.clearUploadInfo = clearUploadInfo;
    return view;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        UIImageView *img_tip = [UIImageView new];
        [self addSubview:img_tip];
        [img_tip makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(36);
            make.centerX.equalTo(0);
        }];
        _img_tip = img_tip;
        
        UILabel *lb_tip = [UILabel new];
        lb_tip.font = ROBOT_FONT(13);
        lb_tip.dk_textColorPicker = DKCellTitleColor;
        lb_tip.textAlignment = NSTextAlignmentCenter;
        lb_tip.numberOfLines = 0;
        [self addSubview:lb_tip];
        [lb_tip makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(img_tip.bottom).offset(15);
            make.centerX.equalTo(0);
        }];
        _lb_tip = lb_tip;
        
        _imageV = [UIImageView new];
        _imageV.userInteractionEnabled = YES;
        [self addSubview:_imageV];
        [_imageV makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        [_imageV block_whenTapped:^(UIView *aView) {
            UIImageView *imgV1 = (UIImageView  *)aView;
            [IXZoomScrollView showFrom:imgV1 image:imgV1.image];
        }];
        
        
        _imageV.hidden = YES;
        
        UIImageView *dismissV = [UIImageView new];
        dismissV.userInteractionEnabled = YES;
        dismissV.dk_imagePicker = DKImageNames(@"openAccount_remove_btn", @"openAccount_remove_btn_D");
        [_imageV addSubview:dismissV];
        [dismissV makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(-10);
            make.top.equalTo(10);
        }];
        weakself;
        [dismissV block_whenTapped:^(UIView *aView) {
            weakSelf.imageV.hidden = YES;
            if (weakSelf.clearUploadInfo) {
                weakSelf.clearUploadInfo();
            }
        }];
        dismissV.hidden = NO;
        _dismissV = dismissV;
        
        _lb_status = [UILabel new];
        _lb_status.font = ROBOT_FONT(12);
        _lb_status.textColor = [UIColor whiteColor];
        _lb_status.textAlignment = NSTextAlignmentRight;
        _lb_status.numberOfLines = 0;
        [self addSubview:_lb_status];
        [_lb_status makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(-10);
            make.top.equalTo(15);
            make.height.equalTo(20);
        }];
        _lb_status.layer.cornerRadius = 26/2;
        _lb_status.layer.masksToBounds = YES;
        
        self.clipsToBounds = YES;
    }
    return self;
}



-(void)setStatus:(NSString *)status{
    // -1.拒绝   0.无文件   1.审批中   2.已审批
    switch ([status integerValue]) {
        case -1:{
            status = @"Not Approved";
        }break;
        case 0:{
            status = @"Not Uploaded";
        }break;
        case 1:{
            status = @"Checking";
            _dismissV.hidden = YES;
        }break;
        case 2:{
            status = @"Approved";
        }break;
    }
    
    _status = status;
    status = [@"     " stringByAppendingString:status];
    status = [status stringByAppendingString:@"  ."];
    _lb_status.text = status;
    
    if ([[status lowercaseString] containsString:@"not"]) {
        _lb_status.backgroundColor = HexRGB(0xe64a4a);
    }else if ([[status lowercaseString] containsString:@"approved"]) {
        _lb_status.backgroundColor = RGB(38,186,118);
    }else if ([[status lowercaseString] containsString:@"checking"]){
        _lb_status.backgroundColor = RGB(237,151,53);
    }else{
        _lb_status.backgroundColor = HexRGB(0xe64a4a);
    }
    
}

- (void)dealDocName:(NSString *)docName{
    if ([docName isEqualToString:@"ID Front"]) {
        _img_tip.image = [UIImage imageNamed:@"user_idfront"];
    }else if ([docName isEqualToString:@"ID Back"]){
        _img_tip.image = [UIImage imageNamed:@"user_idback"];
    }else if ([docName isEqualToString:@"Click to Upload"]){
        _img_tip.image = [UIImage imageNamed:@"user_bankCard2"];
    }else if ([docName isEqualToString:@"Bank Front"]){
        _img_tip.image = [UIImage imageNamed:@"user_bankCard"];
    }else if ([[docName lowercaseString] containsString:@"address"]){
        _img_tip.image = [UIImage imageNamed:@"user_address"];
    }
}


@end
