//
//  IXAcntProgressV.m
//  IXApp
//
//  Created by Magee on 2017/1/18.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntProgressV.h"

@interface IXAcntProgressV ()

@property (nonatomic,strong)UIView *moleV;
@property (nonatomic,strong)UIView *denoV;

@end

@implementation IXAcntProgressV

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dk_backgroundColorPicker = DKColorWithRGBs(0xE2E9F1, 0x303b4d);
    }
    return self;
}

- (UIView *)moleV
{
    if (!_moleV) {
        _moleV = [[UIView alloc] init];
        _moleV.dk_backgroundColorPicker = DKColorWithRGBs(0x11B873, 0x21ce99);
        [self addSubview:_moleV];
    }
    return _moleV;
}

- (UIView *)denoV
{
    if (!_denoV) {
        _denoV = [[UIView alloc] init];
        _denoV.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
        [self addSubview:_denoV];
    }
    return _denoV;
}

- (void)showFromMole:(NSInteger)fMole
              toMole:(NSInteger)tMole
                deno:(NSInteger)deno
{
    CGFloat width = self.frame.size.width;
    
    self.moleV.frame = CGRectMake(0, 0, width*(float)fMole/(float)deno, 5);
    self.denoV.frame = CGRectMake(GetView_MaxX(_moleV), 0, 3, 5);
    [UIView animateWithDuration:.5f delay:.25f options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.moleV.frame = CGRectMake(0, 0, width*(float)tMole/(float)deno, 5);
        self.denoV.frame = CGRectMake(GetView_MaxX(_moleV), 0, 3, 5);
    } completion:^(BOOL finished) {}];
}

@end
