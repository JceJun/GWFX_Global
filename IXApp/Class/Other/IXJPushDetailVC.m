//
//  IXJPushDetailVC.m
//  IXApp
//
//  Created by Evn on 2017/7/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXJPushDetailVC.h"
#import "IXBORequestMgr+BroadSide.h"
#import "IXAppUtil.h"
#import "NSObject+IX.h"
#import "NSString+IX.h"
#import "UIViewExt.h"
@interface IXJPushDetailVC ()

@property (nonatomic, strong) UILabel   * msgTitleLab;
@property (nonatomic, strong) UILabel   * timeLab;
@property (nonatomic, strong) UITextView   * detailLab;
@property (nonatomic, strong) UIView    * btomLine;
@property (nonatomic, copy) NSString    * msgContent;

@end

@implementation IXJPushDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    
    self.title = LocalizedString(@"消息详情");
    [self layoutSubview];
    if (self.message.msgId) {
        [self requestData];
    }
    
    [self setvalues];
}

- (void)layoutSubview
{
    _msgTitleLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 17, kScreenWidth-30, 44)];
    _msgTitleLab.font = PF_MEDI(13);
    _msgTitleLab.numberOfLines = 0;
    _msgTitleLab.dk_textColorPicker = DKCellTitleColor;
    _msgTitleLab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_msgTitleLab];
    
    _timeLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, kScreenWidth - 30, 14)];
    _timeLab.font = RO_REGU(12);
    _timeLab.textColor = MarketGrayPriceColor;
    _timeLab.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0xe9e9ea);
    _timeLab.backgroundColor = [UIColor clearColor];
    _timeLab.textAlignment = NSTextAlignmentRight;
    
    
    _detailLab = [[UITextView alloc] initWithFrame:CGRectMake(0, 54, kScreenWidth, kScreenHeight - _timeLab._bottom - 100)];
    _detailLab.editable = NO;
    _detailLab.selectable = NO;
    _detailLab.font = PF_MEDI(13);
    _detailLab.dk_textColorPicker = DKColorWithRGBs(0x4c6072, 0x5c6a78);
    _detailLab.dk_backgroundColorPicker = DKNavBarColor;
    _detailLab.textContainerInset = UIEdgeInsetsMake(19, 10, 19, 10);
    [self.view addSubview:_detailLab];
    
//    UIView  * line = [[UIView alloc] initWithFrame:CGRectMake(0, 53, kScreenWidth, 1)];
//    line.dk_backgroundColorPicker = DKLineColor;
//    [self.view addSubview:line];
    
    _btomLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_detailLab.frame), kScreenWidth, 1)];
    _btomLine.dk_backgroundColorPicker = DKLineColor;
    [self.view addSubview:_btomLine];
    
    [self.view addSubview:_timeLab];
}

- (void)onGoback
{ 
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setvalues
{
    _msgTitleLab.text  = _message.msgTitle;
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    _timeLab.text   = [IXEntityFormatter timeIntervalToString:[timeStamp doubleValue]];
    _detailLab.text = _message.msgContent;
    
    CGSize  size = [NSString sizeWithText:_message.msgContent andFont:_detailLab.font andMaxSize:CGSizeMake(kScreenWidth, kScreenHeight - _timeLab._bottom)];
//    CGPoint p = self.detailLab.frame.origin;
    
//    if (size.height > kScreenHeight - p.y) {
//        size.height = kScreenHeight - p.y;
//    }else{
//        _detailLab.scrollEnabled = NO;
//    }
    
//    _detailLab.frame = CGRectMake(p.x, p.y, size.width, size.height);

    _detailLab._top = _msgTitleLab._bottom + 15;
    _timeLab._top = 54 + size.height + 70;
    _btomLine._top = _detailLab._top;
}

- (void)requestData{
    
    NSDictionary *paramDic = @{
                               @"msgId":@(_message.msgId),
                               @"timeZone":[IXAppUtil appTimeZone],
                               };
    
    [IXBORequestMgr bs_jpushMessageDetailWithParam:paramDic
                                            result:^(BOOL success,
                                                     NSString *errCode,
                                                     NSString *errStr,
                                                     id obj)
     {
         if (success) {
             if ([obj ix_isDictionary] && obj[@"jpushMsg"]) {
                 NSDictionary * msg = obj[@"jpushMsg"];
                 if ([msg ix_isDictionary]) {
                     _message.msgTitle = [IXDataProcessTools dealWithNil:msg[@"msgTitle"]];
                     _message.msgContent = [IXDataProcessTools dealWithNil:msg[@"msgContent"]];
                     [self setvalues];
                 } else {
                     [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
                 }
             } else {
                 [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
             }
         } else {
             if ([errCode length]) {
                 [SVProgressHUD showErrorWithStatus:errStr];
             } else {
                 [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
             }
         }
     }];
}


@end
