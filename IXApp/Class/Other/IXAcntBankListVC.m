//
//  IXAcntBankListVC.m
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntBankListVC.h"
//#import "IXSupplementAccountVC.h"
#import "IXAddHelp2PayBankVC.h"
#import "IXTouchTableV.h"
#import "IXPosDefultView.h"

#import "IXAcntBankListCell.h"
#import "IXAcntBankListCellA.h"

#import "IXBORequestMgr.h"
#import "IXBORequestMgr+Asset.h"
#import "IXBORequestMgr+Account.h"
#import "IXCpyConfig.h"
#import "IXUserInfoM.h"

@interface IXAcntBankListVC ()
<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) IXPosDefultView   * posDefView;
@property (nonatomic, strong) IXTouchTableV * contentTV;
@property (nonatomic, strong) NSDictionary  * dataDic;
@property (nonatomic, strong) NSMutableArray    * bankListArr;
@property (nonatomic, strong) NSArray       * allBankArr;//支持的银行列表

@end

@implementation IXAcntBankListVC

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(addBankNoti:)
                                                     name:kNotificationAcntInfoAddBank
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationAddBank object:nil];
}

- (void)addBankNoti:(NSNotification *)noti
{
    if (noti) {
        [IXBORequestMgr refreshUserInfo:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self request];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"我的银行卡");
    [self.view addSubview:self.contentTV];
    
}

- (void)refreshUI
{
    if (self.bankListArr.count == 3) {
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        self.navigationItem.rightBarButtonItem = [IXBaseNavVC getRightItemWithDayImg:GET_IMAGE_NAME(@"bank_add") nightImg:GET_IMAGE_NAME(@"bank_add_D") target:self sel:@selector(rightItemClicked) aimWidth:55];
        if (self.posDefView) {
            [self.posDefView removeFromSuperview];
        }
        if (self.bankListArr.count == 0) {
            [self.contentTV addSubview:self.posDefView];
            self.posDefView.imageName = @"bank_default";
            self.posDefView.tipMsg = LocalizedString(@"没有绑定银行卡");
            self.contentTV.scrollEnabled = NO;
        }
    }
    [self.contentTV reloadData];
}

#pragma mark -
#pragma mark - request

- (void)request{
    [SVProgressHUD show];
    
    // 更新银行卡信息&文件
    [IXBORequestMgr loginBo:^(BOOL response, BOOL success, NSString *errStr) {
        if (success) {
            [IXBORequestMgr b_getCustomerFiles:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
                if (success) {
                    [SVProgressHUD dismiss];
                    _bankListArr = [IXBORequestMgr mergedBankList];
                    [self refreshUI];
                }else{
                    [SVProgressHUD showInfoWithStatus:LocalizedString(@"获取客户文件信息失败")];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }else if (errStr.length){
            [SVProgressHUD showErrorWithStatus:errStr];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
        }
    }];
}




- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightItemClicked
{
    [IXBORequestMgr resetAddDepostiBankContainer];
    IXAddHelp2PayBankVC *vc = [IXAddHelp2PayBankVC new];
    [self.navigationController pushViewController:vc animated:YES];
    
//    IXSupplementAccountVC *model = [[IXSupplementAccountVC alloc] init];
//    [self.navigationController pushViewController:model animated:YES];
}

- (NSString *)dealWithBankName:(NSString *)bankCode
{
    if (!_allBankArr || _allBankArr.count == 0) {
        return bankCode;
    }
    for (int i = 0; i < _allBankArr.count; i++) {
        IXEnableBankM   * m = _allBankArr[i];
        if ([bankCode isEqualToString:m.code]) {
            return [m localizedName];
        }
    }
    return bankCode;
}

- (IXPosDefultView *)posDefView
{
    if ( !_posDefView ) {
        _posDefView = [[IXPosDefultView alloc] initWithFrame:CGRectMake( 0,130, kScreenWidth, 217)];
    }
    return _posDefView;
}

#pragma mark -
#pragma mark - getter

- (IXTouchTableV *)contentTV
{
    if ( !_contentTV ) {
        _contentTV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds];
        _contentTV.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
        _contentTV.separatorStyle = UITableViewCellSelectionStyleNone;
        _contentTV.delegate = self;
        _contentTV.dataSource = self;
        _contentTV.scrollEnabled = NO;
        [_contentTV registerClass:[IXAcntBankListCell class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntBankListCell class])];
        [_contentTV registerClass:[IXAcntBankListCellA class]
           forCellReuseIdentifier:NSStringFromClass([IXAcntBankListCellA class])];
    }
    return _contentTV;
}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bankListArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.bounds.size.width, 10)];
    return view ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;//section头部高度
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAcntBankListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAcntBankListCell class])];
    IXUserBankM   *m = _bankListArr[indexPath.row];
    [cell reloadUIWithName:[m localizedName]
                    cardNo:m.bankAccountNumber
            proposalStatus:m.proposalStatus
                     index:indexPath.row
                    type:@""];
    cell.deleteB = [self cellDeleteBlock];
    cell.tapB = [self cellTapBlock];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x242a36);
    cell.bgScrollV.scrollEnabled = YES;
    return cell;
}

- (acntBankListBlock)cellDeleteBlock
{
    weakself;
    acntBankListBlock block = ^(NSInteger index){
        [weakSelf unbindBankInfo:index];
    };
    return block;
}

- (acntBankCellTapBloack)cellTapBlock
{
    weakself;
    acntBankCellTapBloack block = ^(NSInteger index){
        [weakSelf pushToNextVC:index];
    };
    return block;
}

- (void)unbindBankInfo:(NSInteger)index
{
    IXUserBankM * bank = self.bankListArr[index];
    if ([bank.proposalStatus isEqualToString:@"1"]) {
        [SVProgressHUD showMessage:LocalizedString(@"审核中")];
        return;
    }
    NSString    * code = [NSString stringWithFormat:@"%ld",(long)bank.id_p];
    if (code && code.length > 0) {
        NSDictionary    * param = @{
                                    @"gts2CustomerId":[IXBORequestMgr shareInstance].gts2CustomerId,
                                    @"banks":@[@{@"id":code}]
                                    };
        [SVProgressHUD showWithStatus:LocalizedString(@"解除绑定银行卡...")];
        
        [IXBORequestMgr unbindBankWithParam:param
                                     result:^(BOOL success,
                                              NSString *errCode,
                                              NSString *errStr,
                                              id obj)
         {
             if (success) {
                 [SVProgressHUD showSuccessWithStatus:LocalizedString(@"解绑成功")];
                 
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [IXBORequestMgr refreshUserInfo:nil];
                     //删除银行卡，刷新数据
                     [self.bankListArr removeObjectAtIndex:index];
                     [self refreshUI];
                 });
             } else {
                 if ([errCode length]) {
                     [SVProgressHUD showErrorWithStatus:errStr];
                 } else {
                     [SVProgressHUD showErrorWithStatus:LocalizedString(@"解绑失败")];
                 }
             }
         }];
    }
}

- (void)pushToNextVC:(NSUInteger)index
{
    IXUserBankM *bankM = _bankListArr[index];
    [[IXBORequestMgr shareInstance].bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj[@"bankOrder"] integerValue]  == bankM.bankOrder) {
            [IXBORequestMgr shareInstance].bank = obj;
        }
    }];
    [IXBORequestMgr shareInstance].bankFile = [IXBORequestMgr filterDepositBankFile:[IXBORequestMgr shareInstance].bank];

    IXAddHelp2PayBankVC *vc = [IXAddHelp2PayBankVC new];
    [self.navigationController pushViewController:vc animated:YES];
    
    
//    IXUserBankM * bank = _bankListArr[index];
//    IXSupplementAccountVC   * vc = [IXSupplementAccountVC new];
//    vc.bankInfo = bank;
//    vc.modifyBankInfo = YES;
//    [self.navigationController pushViewController:vc animated:YES];
}

@end

