//
//  IXAcntStep4VC.m
//  IXApp
//
//  Created by Magee on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep4VC.h"
#import "IXAcntStep5VC.h"
#import "IXTouchTableV.h"
#import "IXAcntPswV.h"
#import "IXAcntProgressV.h"
#import "IXAcntStep3CellB.h"
#import "IXAcntPswV.h"
#import "IXDBAccountGroupMgr.h"

#import "IXDBAccountMgr.h"
#import "IXDBGlobal.h"
#import "SFHFKeychainUtils.h"
#import "IXCpyConfig.h"
#import "NSObject+IX.h"
#import "IXBORequestMgr+Account.h"


@interface IXAcntStep4VC ()
<
UITableViewDelegate,
UITableViewDataSource,
IXAcntPswVDelegate
>
{
    NSInteger curSelectedRow;
}

@property (nonatomic,strong)IXTouchTableV *tableV;
@property (nonatomic,strong)NSMutableArray *currencyArr;
@property (nonatomic,assign)BOOL isExsitMt4;//是否存在MT4账户
@property (nonatomic, copy)  NSString *type;//type:"agent"、"user"

@end

@implementation IXAcntStep4VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];

    self.title = LocalizedString(@"开立真实账户");
    self.isExsitMt4 = NO;
    self.type = [NSString new];
    [self checkUserType];
}

- (void)checkUserType
{
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    weakself;
    [IXBORequestMgr acc_checkUserTypeWithResult:^(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1) {
        [SVProgressHUD dismiss];
        if (success1 && [obj1 ix_isDictionary]) {
            weakSelf.type = [(NSDictionary *)obj1 objectForKey:@"type"];
            [self groupCurrency];
        } else {
            [SVProgressHUD showErrorWithStatus:errStr1];
        }
    }];
}

- (void)groupCurrency {
    
    _currencyArr = [[NSMutableArray alloc] init];
    uint32_t clientType = 0;
    //0:IX平台
    NSArray *ixArr = nil;
    if (SameString(self.type, @"agent")) {
        ixArr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:0 type:8];
    } else {
        ixArr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:0 wipeOffType:8];
    }
    ixArr = [IXDataProcessTools uniqData:ixArr];
    if (ixArr.count) {
        ixArr = [self dealWithAccountInfo:ixArr clientType:clientType];
    }
    if (ixArr.count) {
        [_currencyArr addObjectsFromArray:ixArr];
    }
    
    NSArray *accountArr = [IXDBAccountMgr queryAllMt4LicAccountInfoByClientType:1];
    if (accountArr.count) {
        self.isExsitMt4 = YES;
        clientType = 2;
        //2:MT4平台同步
        NSArray *mt4Arr = nil;
        if (SameString(self.type, @"agent")) {
            mt4Arr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:2 type:8];
        } else {
            mt4Arr = [IXDBAccountGroupMgr queryAllAccountGroupByClientType:2 wipeOffType:8];
        }
        mt4Arr = [IXDataProcessTools uniqData:mt4Arr];
        if (mt4Arr.count) {
            mt4Arr = [self dealWithAccountInfo:mt4Arr clientType:clientType];
        }
        if (mt4Arr.count) {
            [_currencyArr addObjectsFromArray:mt4Arr];
        }
    }
    [self.tableV reloadData];
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
}

- (NSArray *)dealWithAccountInfo:(NSArray *)accArr
                      clientType:(int32_t)clientType
{
    if (!accArr.count) {
        return nil;
    }
//    //每个平台目前不允许开多个相同货币的账户
//    NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
//    if (accArr.count > 0) {
//        [tmpArr  addObject:accArr[0]];
//    }
//    for (int i = 1; i < accArr.count; i++) {
//        BOOL flag = NO;
//        NSDictionary *dic = accArr[i];
//        for (int j = 0; j < tmpArr.count; j++) {
//            NSDictionary *tmpDic = tmpArr[j];
//            if ([dic[@"currency"] isEqualToString:tmpDic[@"currency"]]) {
//                flag = YES;
//                break;
//            }
//        }
//        if (!flag) {
//            [tmpArr addObject:dic];
//        }
//    }
    
    NSMutableArray *tmpArr = [[NSMutableArray alloc] initWithArray:accArr];
    NSArray *accountArr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                                   [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:clientType];
    NSMutableArray *currencyArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < tmpArr.count; i++) {
        
        BOOL flag = NO;
        NSDictionary *tmpDic = tmpArr[i];
        for (int j = 0; j < accountArr.count; j++) {
            NSMutableDictionary *dataDic = [accountArr[j] mutableCopy];
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:[dataDic[kAccGroupId] longLongValue]]];
            //货币+类型
            if ([tmpDic[kCurrency] isEqualToString:dic[kCurrency]] &&
                [tmpDic[kType] isEqualToString:dic[kType]]) {
                flag = YES;
                break;
            }
        }
        if (!flag) {
            [currencyArr addObject:tmpDic];
        }
    }
    return currencyArr;
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds
                                                 style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18-22)/2, kScreenWidth, 18)];
    label.text = LocalizedString(@"货币账户");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKGrayTextColor;
    label.font = PF_MEDI(15);
    [v addSubview:label];
    
    label.frame = CGRectMake(0, (132-18)/2 + 5, kScreenWidth, 18);
    IXAcntProgressV *prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
    [v addSubview:prgV];
    [prgV showFromMole:(2 + self.evaluationM.pageList.count) toMole:(3 + self.evaluationM.pageList.count) deno:(4 + self.evaluationM.pageList.count)];
    
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*4-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    [nextBtn setTitle:LocalizedString(@"下一步") forState:UIControlStateNormal];
    nextBtn.titleLabel.font = PF_REGU(15);
   
    [v addSubview:nextBtn];
    return v;
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _currencyArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    static NSString *identifier = @"IXAcntStep3CellB";
    IXAcntStep3CellB *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXAcntStep3CellB alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = row;
    }
    [cell loadUIWithText:[self showCurrency:_currencyArr[indexPath.row]] hilightTag:curSelectedRow];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    curSelectedRow = indexPath.row;
    [tableView reloadData];
}

- (NSString *)showCurrency:(NSDictionary *)dic
{
    uint32_t clientType = [dic[kClientType] intValue];
    uint32_t type = [dic[kType] intValue];
    NSString *ret = nil;
    if (clientType == 0) {
        if (self.isExsitMt4) {
            ret = [NSString stringWithFormat:@"%@-IX-%@",dic[@"currency"],[IXDataProcessTools getAccountNameByType:type]];
        } else {
            ret = [NSString stringWithFormat:@"%@-%@",dic[@"currency"],[IXDataProcessTools getAccountNameByType:type]];
        }
    } else {
       ret = [NSString stringWithFormat:@"%@-MT4-%@",dic[@"currency"],[IXDataProcessTools getAccountNameByType:type]];
    }
    return ret;
}

#pragma mark -
#pragma mark - others

- (void)nextBtnClk
{    
    if (!_currencyArr || _currencyArr.count == 0) {
        return;
    }
    NSDictionary *dic = _currencyArr[curSelectedRow];
    _acntModel.currency = dic[@"currency"];
    //IX/MT4（0或者2标识）
    uint32_t clientType = [dic[kClientType] intValue];
    _acntModel.groupType = [dic[kType] intValue];
    if (clientType == 0) {
        _acntModel.clientType = 0;
        [self popToNextVC];
    } else {
        _acntModel.clientType = 2;
        [self showMT4Login];
    }
}

- (void)popToNextVC
{
    IXAcntStep5VC *VC = [[IXAcntStep5VC alloc] init];
    VC.acntModel = _acntModel;
    VC.evaluationM = self.evaluationM;
    [self.navigationController pushViewController:VC animated:YES];
}

- (void)showMT4Login
{
    IXAcntPswV *pswV = [[IXAcntPswV alloc] initWithFrame:kScreenBound];
    pswV.delegate = self;
    [pswV show];
    return;
}

- (void)passWord:(NSString *)psw
{
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kIXLoginName];
    if (userName && userName.length > 0) {
         NSString *passWord = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:kServiceName error:nil];
        if ([passWord isEqualToString:psw]) {
            _acntModel.encryptPassword = psw;
            [self popToNextVC];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"校验失败")];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"校验失败")];
    }
}

@end
