//
//  IXLanguageVC.h
//  IXApp
//
//  Created by Evn on 17/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"

typedef NS_ENUM(NSInteger, SettingType) {
    SettingTypeLanguage,
    SettingTypeTradeType,
    SettingTypeColor
};

@interface IXLanguageVC : IXDataBaseVC

@property (nonatomic, assign) SettingType type;

@end
