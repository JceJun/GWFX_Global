//
//  IXMsgCenterCell.h
//  IXApp
//
//  Created by Seven on 2017/4/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IXMsgCenterM;

@interface IXMsgCenterCell : UICollectionViewCell

@property (nonatomic, strong) UILabel   * titleLab;
@property (nonatomic, strong) UILabel   * timeLab;
@property (nonatomic, strong) UILabel   * detailLab;

@property (nonatomic, strong) IXMsgCenterM  * message;

@end
