//
//  IXCountryListVC.h
//  IXApp
//
//  Created by Evn on 17/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
typedef void(^selectedCountryInfo)(IXCountryM * country);

@interface IXCountryListVC : IXDataBaseVC

- (id)initWithSelectedInfo:(selectedCountryInfo)bankInfo;

@end
