//
//  IXOpenAcntDataModel.h
//  IXApp
//
//  Created by Evn on 17/2/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXEvaluationM.h"

@interface IXOpenAcntDataModel : NSObject
@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSString *cerType;
@property (nonatomic, strong)NSString *cerNo;
@property (nonatomic, strong)NSString *idDocument;
@property (nonatomic, strong)NSString *email;
@property (nonatomic, strong)NSString *nationality;
@property (nonatomic, strong)NSString *country;
@property (nonatomic, strong)NSString *province;
@property (nonatomic, strong)NSString *city;
@property (nonatomic, strong)NSString *address;
@property (nonatomic, strong)NSString *postalCode;
@property (nonatomic, strong)NSString *riskType;
@property (nonatomic, strong)NSString *currency;
@property (nonatomic, strong)NSString *accountId;
@property (nonatomic, strong)NSString *gts2CustomerId;
@property (nonatomic, strong)NSString *mobilePhonePrefix;
@property (nonatomic, strong)NSString *mobilePhone;
@property (nonatomic, strong)NSString *nameCN;
@property (nonatomic, strong)NSString *nameEN;
@property (nonatomic, strong)NSString *nameTW;
@property (nonatomic, assign)BOOL flag;//1:已开户
@property (nonatomic, assign)NSInteger clientType;
@property (nonatomic, strong)NSString *encryptPassword;
@property (nonatomic, assign)NSInteger state;//账户切换 0:成功登录 1:不能登录 2:人工审核
@property (nonatomic, assign)NSInteger groupType;//1：STANDARD、2：VIP
@end
