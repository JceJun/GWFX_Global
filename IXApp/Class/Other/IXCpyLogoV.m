//
//  IXCpyLogoV.m
//  IXApp
//
//  Created by Evn on 2017/9/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCpyLogoV.h"

@interface IXCpyLogoV()

@property (nonatomic, strong)UIImageView *imgView;

@end

@implementation IXCpyLogoV

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dsimissAction)];
    [self addGestureRecognizer:tap];
    [self addSubview:self.imgView];
}

- (UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 145)];
        _imgView.center = self.center;
        _imgView.image = GET_IMAGE_NAME(@"ix_companyLogo");
    }
    return _imgView;
}

- (void)dsimissAction
{
    if (self) {
        [self removeFromSuperview];
    }
}

@end
