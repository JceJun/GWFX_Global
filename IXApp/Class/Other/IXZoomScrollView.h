//
//  IXZoomScrollView.h
//  PhotoBrowserDemo
//
//  Created by Seven on 2017/6/29.
//  Copyright © 2017年 Seven. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol IXTapDetectingVDelegate;
@protocol IXTapDetectingImgVDelegate;

@interface IXTapDetectingV : UIView

@property (nonatomic, weak) id <IXTapDetectingVDelegate> tapDelegate;

- (void)handleSingleTap:(UITouch *)touch;
- (void)handleDoubleTap:(UITouch *)touch;
- (void)handleTripleTap:(UITouch *)touch;

@end

//代理
@protocol IXTapDetectingVDelegate <NSObject>

@optional
- (void)view:(UIView *)view singleTapDetected:(UITouch *)touch;
- (void)view:(UIView *)view doubleTapDetected:(UITouch *)touch;
- (void)view:(UIView *)view tripleTapDetected:(UITouch *)touch;

@end

// --------------------------------------------------------------------------

@interface IXTapDetectingImgV : UIImageView

@property (nonatomic, weak) id <IXTapDetectingImgVDelegate> tapDelegate;

- (void)handleSingleTap:(UITouch *)touch;
- (void)handleDoubleTap:(UITouch *)touch;
- (void)handleTripleTap:(UITouch *)touch;

@end

//代理
@protocol IXTapDetectingImgVDelegate <NSObject>

@optional
- (void)imageView:(UIImageView *)imageView singleTapDetected:(UITouch *)touch;
- (void)imageView:(UIImageView *)imageView doubleTapDetected:(UITouch *)touch;
- (void)imageView:(UIImageView *)imageView tripleTapDetected:(UITouch *)touch;

@end

// --------------------------------------------------------------------------

@interface IXZoomScrollView : UIScrollView

@property (nonatomic, readonly) IXTapDetectingV     * tapView;
@property (nonatomic, readonly) IXTapDetectingImgV  * photoImageView;
@property (nonatomic, strong) UIImage   * image;
@property (nonatomic, assign) CGFloat maximumDoubleTapZoomScale;

+ (void)showFrom:(UIView *)view image:(UIImage *)img;

- (void)showFrom:(UIView *)view image:(UIImage *)img;

@end
