//
//  IXChangePhoneStep2VC.m
//  IXApp
//
//  Created by Evn on 2017/5/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXChangePhoneStep2VC.h"
#import "IXAcntSafetyVC.h"
#import "IXTouchTableV.h"
#import "IXRegistStep2CellA.h"
#import "IXLoginMainCellA.h"
#import "IXCheckCodeV.h"

#import "NSString+FormatterPrice.h"
#import "IXCpyConfig.h"
#import "IXBORequestMgr+Account.h"
#import "IXUserInfoM.h"
#import "IXCodeM.h"

static NSInteger sendCounter = SendSMS_Interval;

@interface IXChangePhoneStep2VC ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
IXRegistStep2CellADelegate

>


@property (nonatomic, strong) UIButton  *submitBtn;
@property (nonatomic, strong)IXTouchTableV *tableV;
@property (nonatomic, strong)IXCheckCodeV *codeV;

@property (nonatomic, strong) NSTimer   *timer;
@property (nonatomic, strong) IXCodeM   *codeM;


@end

@implementation IXChangePhoneStep2VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(onGoback)];
    self.title = LocalizedString(@"更换手机号码");
    _codeM = [[IXCodeM alloc] init];
    
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
    if (sendCounter < SendSMS_Interval) {
        [self enableTimer];
    }
    [self sendBtnClicked];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self disableTimer];
}

- (void)dealloc
{
    [self disableTimer];
}

#pragma mark -
#pragma mark - btn action

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGoback
{
    [self popToVC];
}

- (void)submitBtnClk
{
    [self checkVerifyCode];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        IXRegistStep2CellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXRegistStep2CellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell reloadUIWithPhoneNum:_phoneNum phoneId:_phoneId];
        [cell sendBtnTitleChanged:sendCounter];
        cell.delegate = self;
        return cell;
    } else {
        IXLoginMainCellA *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textF.delegate = self;
        [cell.textF becomeFirstResponder];
        [cell loadUIWithDesc:LocalizedString(@"验证码")
                        text:@""
                 placeHolder:LocalizedString(@"请填写验证码")
                keyboardType:UIKeyboardTypeNumberPad
             secureTextEntry:NO];
        cell.textF.textFont = RO_REGU(15);
        cell.textF.placeHolderFont = PF_MEDI(13);
        return cell;
    }
    return nil;
}

#pragma mark -
#pragma mark - text field

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "]) {
        [SVProgressHUD showErrorWithStatus:LocalizedString(NONESPACECHAR)];
        return NO;
    }
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    NSString    * aimStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self submitBtnEnable:aimStr.length >= 4];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self submitBtnEnable:NO];
    return YES;
}

- (void)submitBtnEnable:(BOOL)enable
{
    if (enable) {
        _submitBtn.enabled = YES;
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        _submitBtn.enabled = NO;
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark - create UI

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
        [_tableV registerClass:[IXLoginMainCellA class]
        forCellReuseIdentifier:NSStringFromClass([IXLoginMainCellA class])];
        [_tableV registerClass:[IXRegistStep2CellA class]
        forCellReuseIdentifier:NSStringFromClass([IXRegistStep2CellA class])];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth - 30, 132)];
    label.text = LocalizedString(@"短信验证码已发送，请填写验证码");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKCellTitleColor;
    label.font = PF_MEDI(15);
    label.numberOfLines = 0;
    [v addSubview:label];
    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*2-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_unable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [_submitBtn addTarget:self action:@selector(submitBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [_submitBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_submitBtn setTitle:LocalizedString(@"提 交") forState:UIControlStateNormal];
    _submitBtn.titleLabel.font = PF_REGU(15);
    [v addSubview:_submitBtn];
    _submitBtn.enabled = NO;
    
    return v;
}


#pragma mark -
#pragma mark - request

- (void)checkVerifyCode
{
    IXLoginMainCellA *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if ( [_codeM.verifyCodeId length] > 0 && [cell.textF.text length] >= 4 ) {

        [SVProgressHUD showWithStatus:LocalizedString(@"提交验证码...")];
        [IXBORequestMgr acc_checkVerifyCodeWithCode:cell.textF.text
                                             codeId:_codeM.verifyCodeId
                                           complete:^(BOOL success,
                                                      NSString *token,
                                                      NSString *errStr)
        {
            if (success) {
                [self disableTimer];
                [SVProgressHUD showSuccessWithStatus:LocalizedString(@"验证码正确")];
                [self bindPhone];
            } else {
                [SVProgressHUD showErrorWithStatus:errStr];
            }
        }];
    } else{
        if (!_codeM.verifyCodeId.length) {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"请输入验证码(最少四位数字)")];
        }
    }
}

- (void)bindPhone
{
    NSDictionary *param = @{
                            @"gts2CustomerId":[NSString stringWithFormat:@"%ld",(long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId],
                            @"mobilePhonePrefix":_phoneId,
                            @"mobilePhone":_phoneNum,
                            @"email":[IXUserInfoMgr shareInstance].userLogInfo.user.email,
                            @"flag":@"phone",
                            @"companyId":[NSString stringWithFormat:@"%d",CompanyID]
                            };
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    
    [IXBORequestMgr acc_bindEmailOrPhoneWithParam:param
                                           result:^(BOOL success,
                                                    NSString *errCode,
                                                    NSString *errStr,
                                                    id obj)
    {
        if (success) {
            [SVProgressHUD showSuccessWithStatus:LocalizedString(@"绑定成功")];
            [IXUserInfoMgr shareInstance].userLogInfo.user.phone = self.phoneNum;
            [IXBORequestMgr shareInstance].userInfo.detailInfo.mobilePhone = self.phoneNum;
            [self popToVC];
            return ;
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"绑定失败")];
        }
    }];
}

- (void)popToVC
{
    NSArray * arr = self.navigationController.viewControllers;
    __block UINavigationController  * navCtrl = nil;
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[IXAcntSafetyVC class]]) {
            navCtrl = obj;
            *stop = YES;
        }
    }];
    if (navCtrl) {
        [self.navigationController popToViewController:navCtrl animated:YES];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark -
#pragma mark - request

- (void)sendBtnClicked
{
    [SVProgressHUD showWithStatus:LocalizedString(@"短信获取中...")];
    weakself;
    [IXBORequestMgr acc_getPhoneVerifyCodeWithLimitedWithNumber:_phoneNum areaCode:_phoneId imgVerifiCodeCode:_codeM.imgVerifiCodeCode imgVerifiCodeId:_codeM.imgVerifiCodeId result:^(NSString *errCode, NSString *errStr, NSString *codeId, NSString *imageVerifiCodeUrl, NSString *imageVerifiCodeId) {
        if (!errCode) {
            if (codeId.length) {
                weakSelf.codeM.verifyCodeId = codeId;
                [weakSelf enableTimer];
                [SVProgressHUD showSuccessWithStatus:LocalizedString(@"验证码发送成功")];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        } else {
            //需要图形验证
            if ([errCode integerValue] == 2203) {
                [SVProgressHUD dismiss];
                weakSelf.codeM.imgVerifiCodeId = imageVerifiCodeId;
                weakSelf.codeM.imageVerifiCodeUrl = imageVerifiCodeUrl;
                if (!weakSelf.codeV) {
                    weakSelf.codeV = [[IXCheckCodeV alloc] initWithCodeImageUrl:weakSelf.codeM.imageVerifiCodeUrl];
                    weakSelf.codeV.checkB = ^(NSString *code) {
                        weakSelf.codeM.imgVerifiCodeCode = code;
                        [weakSelf sendBtnClicked];
                    };
                    weakSelf.codeV.refreshB = ^{
                        [weakSelf sendBtnClicked];
                    };
                    weakSelf.codeV.dismissB = ^{
                        weakSelf.codeV = nil;
                    };
                    [weakSelf.codeV show];
                } else {
                    [weakSelf.codeV refreshImageUrl:weakSelf.codeM.imageVerifiCodeUrl];
                }
            } else {
                if (errStr.length) {
                    [SVProgressHUD showErrorWithStatus:errStr];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
                }
            }
        }
    }];
}

#pragma mark -
#pragma mark - timer

- (void)enableTimer
{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                selector:@selector(checkCounter)
                                                userInfo:nil
                                                 repeats:YES];
    }
    [_timer setFireDate:[NSDate distantPast]];
}

- (void)disableTimer
{
    if (_timer) {
        if ([_timer isValid]) {
            [_timer invalidate];
        }
        _timer = nil;
    }
}

- (void)checkCounter
{
    sendCounter --;
    if (sendCounter <= 0) {
        sendCounter = SendSMS_Interval;
        [_timer setFireDate:[NSDate distantFuture]];
    }
    IXRegistStep2CellA *cell = [_tableV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell sendBtnTitleChanged:sendCounter];
}


@end
