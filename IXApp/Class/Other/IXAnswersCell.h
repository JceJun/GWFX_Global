//
//  IXAnswersCell.h
//  IXApp
//
//  Created by Evn on 2017/7/21.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXClickTableVCell.h"

@interface IXAnswersCell : IXClickTableVCell

- (void)reloadUI:(NSString *)content
       indexPath:(NSIndexPath *)indexPath
          counts:(NSInteger)counts;

@end
