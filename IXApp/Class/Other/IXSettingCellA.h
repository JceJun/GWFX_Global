//
//  IXSettingCellA.h
//  IXApp
//
//  Created by Evn on 16/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@interface IXSettingCellA : IXClickTableVCell

@property (nonatomic, strong) UIImageView * accImgV;
@property (nonatomic, strong) UIFont    * titleFont;

- (void)configTitle:(NSString *)title content:(NSString *)content;


- (void)showTopLineWithOffsetX:(CGFloat)offset;
- (void)showBototmLineWithOffsetX:(CGFloat)offset;
- (void)setContentFont:(UIFont *)font;


@end
