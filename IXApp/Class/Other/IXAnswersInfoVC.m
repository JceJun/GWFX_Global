//
//  IXAnswersInfoVC.m
//  IXApp
//
//  Created by Evn on 2017/7/26.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAnswersInfoVC.h"
#import "IXBORequestMgr+BroadSide.h"
#import "IXAnswersInfoCell.h"
#import "NSObject+IX.h"

@interface IXAnswersInfoVC ()<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, assign) CGFloat webHeight;

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSDictionary *model;
@property (nonatomic , strong) NSDictionary *contentDic;

@end

@implementation IXAnswersInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.title = LocalizedString(@"问答详情");
    [self addHeadView];
    [self askedAndQuestionsDetail];
}

- (NSDictionary *)contentDic {
    
    if (!_contentDic) {
        
        _contentDic = [[NSDictionary alloc] init];
    }
    
    return _contentDic;
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)askedAndQuestionsDetail
{
    NSDictionary *paramDic = @{
                               @"id":_idStr
                               };
    
    [IXBORequestMgr bs_askedAndQuestionsDetailWithParam:paramDic
                                                 result:^(BOOL success,
                                                          NSString *errCode,
                                                          NSString *errStr,
                                                          id obj)
    {
        if (success) {
            if ([obj ix_isDictionary] && obj[@"InfomationCenter"]) {
                NSDictionary    * infoDic = obj[@"InfomationCenter"];
                if ([infoDic ix_isDictionary] && infoDic[@"content"]) {
                    [SVProgressHUD dismiss];
                    self.contentDic = (NSDictionary *)(infoDic);
                    [self.tableV reloadData];
                } else {
                    [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        } else {
            if ([errCode length]) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据异常")];
            }
        }
    }];
}

- (void)addHeadView
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 57)];
    bgView.dk_backgroundColorPicker = DKTableColor;
    [self.view addSubview:bgView];
    UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 21, kScreenWidth - 30, 15)];
    titleLbl.dk_textColorPicker = DKGrayTextColor;
    titleLbl.font = PF_MEDI(13);
    titleLbl.textAlignment = NSTextAlignmentLeft;
    titleLbl.text = _answtTitle;
    [bgView addSubview:titleLbl];
    
    UIView *uLine = [[UIView alloc] initWithFrame:CGRectMake(0, 57 - kLineHeight, kScreenWidth, kLineHeight)];
    uLine.dk_backgroundColorPicker = DKColorWithRGBs(0xdae6f0, 0x242a36);
    [bgView addSubview:uLine];
    
}

#pragma mark -
#pragma makr - initialize

- (UITableView *)tableV
{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 57, kScreenWidth, kScreenHeight - kNavbarHeight - 57)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableV.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
        [_tableV registerClass:[IXAnswersInfoCell class]
        forCellReuseIdentifier:NSStringFromClass([IXAnswersInfoCell class])];
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

#pragma mark -
#pragma makr - UITableViewDelegate && UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _webHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXAnswersInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IXAnswersInfoCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ( self.contentDic && [self.contentDic objectForKey:@"content"] ) {
        cell.content = [self.contentDic stringForKey:@"content"];
    }
    
    weakself;
    cell.refreashHeight = ^(CGFloat height){
        weakSelf.webHeight = height;
        [weakSelf.tableV reloadData];
    };
    
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


@end
