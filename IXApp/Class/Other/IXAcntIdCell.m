//
//  IXAcntIdCell.m
//  IXApp
//
//  Created by Evn on 2018/3/24.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXAcntIdCell.h"

@interface IXAcntIdCell()<UITextFieldDelegate>

@end

@implementation IXAcntIdCell

- (UILabel *)title
{
    if (!_title) {
        _title =  [IXCustomView createLable:CGRectMake(15,
                                                       15,
                                                       kScreenWidth/2 - 15,
                                                       15)
                                      title:@""
                                       font:PF_MEDI(13)
                                 wTextColor:0x4c6072
                                 dTextColor:0xe9e9ea
                              textAlignment:NSTextAlignmentLeft];
        _title.userInteractionEnabled = YES;
        UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectIdRecognizer)];
        [_title addGestureRecognizer:tg];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (IXTextField *)cardTF
{
    if ( !_cardTF ) {
        _cardTF = [[IXTextField alloc] initWithFrame:CGRectMake(kScreenWidth/2,15, kScreenWidth/2 - 15, 15)];
        _cardTF.textAlignment = NSTextAlignmentRight;
        _cardTF.dk_textColorPicker = DKCellContentColor;
        _cardTF.font = RO_REGU(15);
        _cardTF.delegate = self;
        [self.contentView addSubview:_cardTF];
        [self setTextFieldInputAccessoryView];
    }
    return _cardTF;
}

- (void)setTextFieldInputAccessoryView
{
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    [topView setBarStyle:UIBarStyleDefault];
    topView.backgroundColor = MarketCellSepColor;
    
    UIBarButtonItem * spaceBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:self
                                                                              action:nil];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setTitle:LocalizedString(@"取消") forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:PF_MEDI(13)];
    [cancelBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(15, 9, 60, 25);
    [cancelBtn addTarget:self action:@selector(dealKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBtnItem = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneBtn setTitle:LocalizedString(@"完成") forState:UIControlStateNormal];
    [doneBtn.titleLabel setFont:PF_MEDI(13)];
    [doneBtn setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
    doneBtn.frame = CGRectMake(kScreenWidth - 90, 9, 80, 25);
    [doneBtn addTarget:self action:@selector(dealKeyboardHidden) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneBtnItem = [[UIBarButtonItem alloc]initWithCustomView:doneBtn];
    NSArray * buttonsArray = [NSArray arrayWithObjects:cancelBtnItem,spaceBtn,doneBtnItem,nil];
    [topView setItems:buttonsArray];
    [_cardTF setInputAccessoryView:topView];
    [_cardTF setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_cardTF setAutocapitalizationType:UITextAutocapitalizationTypeNone];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (void)dealKeyboardHidden{
    [_cardTF resignFirstResponder];
}

- (void)selectIdRecognizer
{
    if (self.acntIdB) {
        self.acntIdB();
    }
}

- (void)loadData:(NSString *)title
         content:(NSString *)content
{
    self.title.text = title;
    self.cardTF.text = content;
}

@end
