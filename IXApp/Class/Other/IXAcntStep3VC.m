//
//  IXAcntStep3VC.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep3VC.h"
#import "IXAcntStep3CellA.h"
#import "IXAcntStep3CellB.h"
#import "IXTouchTableV.h"
#import "IXAcntStep4VC.h"
#import "IXAcntProgressV.h"

@interface IXAcntStep3VC ()
<
UITableViewDelegate,
UITableViewDataSource
>
{
    NSInteger curSelectedRow;
}

@property (nonatomic, strong)IXTouchTableV *tableV;
@property (nonatomic, strong) NSArray *dataAry;

@end

@implementation IXAcntStep3VC

- (void)viewDidLoad {
    [super viewDidLoad];
    curSelectedRow = 0;
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.navigationItem.rightBarButtonItem =
    [IXBaseNavVC getRightBtnItemWithTitle:LocalizedString(@"取消")
                                   target:self
                                      sel:@selector(rightBtnItemClicked)];

    self.title = LocalizedString(@"开立真实账户");
    _dataAry = @[
                 LocalizedString(@"进取型"),
                 LocalizedString(@"稳健型、灵活型"),
                 LocalizedString(@"保守型")
                 ];
    [self.tableV setTableHeaderView:[self tableHeaderV]];
    [self.tableV setTableFooterView:[self tableFooterV]];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IXTouchTableV *)tableV
{
    if (!_tableV) {
        _tableV = [[IXTouchTableV alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.scrollEnabled = NO;
        [self.view addSubview:_tableV];
    }
    return _tableV;
}

- (UIView *)tableHeaderV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 132)];
    v.dk_backgroundColorPicker = DKTableHeaderColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (132-18-22)/2, kScreenWidth, 18)];
    label.text = LocalizedString(@"我的风险承担能力");
    label.textAlignment = NSTextAlignmentCenter;
    label.dk_textColorPicker = DKGrayTextColor;
    label.font = PF_MEDI(15);
    [v addSubview:label];
    
    label.frame = CGRectMake(0, (132-18)/2 + 5, kScreenWidth, 18);
    IXAcntProgressV *prgV = [[IXAcntProgressV alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
    [v addSubview:prgV];
    [prgV showFromMole:2 toMole:3 deno:5];

    return v;
}

- (UIView *)tableFooterV
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, kScreenHeight-132-44*3-kNavbarHeight)];
    v.dk_backgroundColorPicker = DKViewColor;
    
    UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(15.5, 40.5, kScreenWidth - 31, 44);
    [nextBtn addTarget:self action:@selector(nextBtnClk) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn setBackgroundImage:image forState:UIControlStateNormal];
    [nextBtn setTitle:LocalizedString(@"下一步") forState:UIControlStateNormal];
    nextBtn.titleLabel.font = PF_REGU(15);
    [v addSubview:nextBtn];

    return v;
}

#pragma mark -
#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .1f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    static NSString *identifier = @"IXAcntStep3CellB";
    IXAcntStep3CellB *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[IXAcntStep3CellB alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = row;
    }
    [cell loadUIWithText:_dataAry[row] hilightTag:curSelectedRow];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    curSelectedRow = indexPath.row;
    [tableView reloadData];
}

#pragma mark -
#pragma mark - others
- (void)nextBtnClk
{
    IXAcntStep4VC *VC = [[IXAcntStep4VC alloc] init];
    VC.acntModel = _acntModel;
    VC.evaluationM = self.evaluationM;
    [self.navigationController pushViewController:VC animated:YES];
}

@end
