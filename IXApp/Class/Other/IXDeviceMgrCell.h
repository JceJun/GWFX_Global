//
//  IXDeviceMgrCell.h
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXClickTableVCell.h"

@interface IXDeviceMgrCell : IXClickTableVCell

- (void)reloadUIWithData:(NSDictionary *)data;

@end
