//
//  IXSurveyMainVC+Rst.m
//  IXApp
//
//  Created by Evn on 2017/10/31.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSurveyMainVC+Rst.h"
#import "IXSurveyM.h"

@implementation IXSurveyMainVC (Rst)

- (void)loadRstView
{
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake( (kScreenWidth - 80)/2, 45, 80, 80)];
    icon.dk_imagePicker = DKImageNames(@"openAccount_complete", @"openAccount_complete_D");
    [self.view addSubview:icon];
    
    UILabel *labelU = [IXUtils createLblWithFrame:CGRectMake( 0, 145, kScreenWidth, 15)
                                         WithFont:PF_MEDI(13)
                                        WithAlign:NSTextAlignmentCenter
                                       wTextColor:0x4c6072
                                       dTextColor:0xffffff];
    labelU.text = LocalizedString(@"问卷已提交");
    [self.view addSubview:labelU];
    
    UIScrollView *sView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, GetView_MaxY(labelU) + 36, kScreenWidth - 20, 283)];
    sView.dk_backgroundColorPicker = DKNavBarColor;
    sView.showsHorizontalScrollIndicator = NO;
    sView.showsVerticalScrollIndicator = NO;
    sView.layer.cornerRadius = 4;
    [self.view addSubview:sView];
    
    CGFloat offsetY = 15.f;
    UILabel *title = [IXUtils createLblWithFrame:CGRectMake( 15, offsetY, VIEW_W(sView) - 30, 15)
                                        WithFont:PF_MEDI(13)
                                       WithAlign:NSTextAlignmentLeft
                                      wTextColor:0x4c6072
                                      dTextColor:0xffffff];
    title.text = LocalizedString(@"调查问卷回顾");
    [sView addSubview:title];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, GetView_MaxY(title) + 15, kScreenWidth, kLineHeight)];
    lineView.dk_backgroundColorPicker = DKColorWithRGBs(0xcbcfd6, 0x242a36);
    [sView addSubview:lineView];
    
    offsetY = GetView_MaxY(lineView) + 0.5 + 15;

    NSArray *dataArr = [self.surveyM getSurvey];
    for (int i = 0; i < dataArr.count; i++) {
        
        IXQuestionM *questionM = dataArr[i];
        UILabel *title = [IXUtils createLblWithFrame:CGRectMake( 16, offsetY, VIEW_W(sView) - 32, 15)
                                            WithFont:PF_MEDI(13)
                                           WithAlign:NSTextAlignmentLeft
                                          wTextColor:0x4c6072
                                          dTextColor:0xffffff];
        if ([questionM.type isEqualToNumber:@2]) {
            title.text = [NSString stringWithFormat:@"%@(%@)",questionM.title,LocalizedString(@"可多选")];
        } else {
            title.text = [NSString stringWithFormat:@"%@",questionM.title];
        }
        title.numberOfLines = 0;
        [sView addSubview:title];
        
        CGSize size = [title.text boundingRectWithSize:CGSizeMake(VIEW_W(sView) - 32 , MAXFLOAT)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:PF_MEDI(13)}
                                                 context:nil].size;
        title.frame = CGRectMake(16, offsetY, VIEW_W(sView) - 32, size.height);
        
        offsetY = GetView_MaxY(title) + 12;
        UILabel *content = [IXUtils createLblWithFrame:CGRectMake( 16, offsetY, VIEW_W(sView) - 32, 15)
                                              WithFont:PF_MEDI(13)
                                             WithAlign:NSTextAlignmentLeft
                                            wTextColor:0x99abba
                                            dTextColor:0x8395a4];
        content.text = [self getSurveyContent:questionM];
        content.numberOfLines = 0;
        [sView addSubview:content];
        
        size = [content.text boundingRectWithSize:CGSizeMake(VIEW_W(sView) - 32 , MAXFLOAT)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:PF_MEDI(13)}
                                               context:nil].size;
        content.frame = CGRectMake(16, offsetY, VIEW_W(sView) - 32, size.height);
        
        if (i != (dataArr.count - 1)) {
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(16, GetView_MaxY(content) + 15, VIEW_W(sView) - 16, kLineHeight)];
            line.dk_backgroundColorPicker = DKColorWithRGBs(0xcbcfd6, 0x242a36);
            [sView addSubview:line];
            
            offsetY = GetView_MaxY(content) + 31;
        } else {
            
            offsetY = GetView_MaxY(content) + 15;
        }
    }
    sView.contentSize = CGSizeMake(kScreenWidth - 20, offsetY);
}

- (NSString *)getSurveyContent:(IXQuestionM *)questionM
{
    NSString *retStr = [NSString new];
    if ([questionM.type isEqualToNumber:@0]) {
        retStr = questionM.lastAnswer;
    } else if ([questionM.type isEqualToNumber:@1]) {
        int index = [IXDataProcessTools stringToAsciiCode:questionM.lastAnswer];
        if (index < questionM.appendArr.count) {
            retStr = [questionM.appendArr objectAtIndex:index];
        } else {
            retStr = @"";
        }
    } else {
        for (int i = 0; i < questionM.answerArr.count; i++) {
            int index = [IXDataProcessTools stringToAsciiCode:questionM.answerArr[i]];
            if (index < questionM.appendArr.count) {
                if (retStr.length) {
                    retStr = [retStr stringByAppendingFormat:@"、%@",[questionM.appendArr objectAtIndex:index]];
                } else {
                    retStr = [retStr stringByAppendingString:[questionM.appendArr objectAtIndex:index]];
                }
            } else {
                retStr = [retStr stringByAppendingString:@""];
            }
        }
    }
    return retStr;
}

- (void)removeAllSubviews
{
    
}

@end
