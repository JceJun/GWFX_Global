//
//  IXLanguageCell.h
//  IXApp
//
//  Created by Evn on 17/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@interface IXLanguageCell : IXClickTableVCell

- (void)configTitle:(NSString *)title selectState:(BOOL)flag;

//分割线
- (void)showTopLineWithOffsetX:(CGFloat)offset;
- (void)showBototmLineWithOffsetX:(CGFloat)offset;

- (void)selected:(BOOL)select;

@end
