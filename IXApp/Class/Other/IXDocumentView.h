//
//  IXDocumentView.h
//  IXApp
//
//  Created by Larry on 2018/6/28.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDocumentView : UIView

@property(nonatomic,copy)NSString *status;

@property(nonatomic,strong)UIImageView *imageV;
@property(nonatomic,strong)UIImageView *dismissV;
@property(nonatomic,strong)UILabel *lb_tip;
@property(nonatomic,strong)UILabel *lb_status;

@property(nonatomic,copy) void(^clearUploadInfo)(void);

+ (instancetype)makeDocument:(NSString *)docName superView:(UIView *)superView clearUploadInfo:(void(^)(void))clearUploadInfo;

@end
