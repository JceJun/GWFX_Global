//
//  IXUnitSettingVC.m
//  IXApp
//
//  Created by Seven on 2017/6/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUnitSettingVC.h"
#import "IXLanguageCell.h"
#import "IXUserDefaultM.h"

@interface IXUnitSettingVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) NSArray       * titleArr;

@end

@implementation IXUnitSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.title = LocalizedString(@"单位切换");
    
    [self.view addSubview:self.tableV];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}

static  NSString    * ident = @"IXLanguageCell";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IXLanguageCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        cell = [[IXLanguageCell alloc] initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:ident];
    }
    
    NSString    * title = self.titleArr[indexPath.row];
    BOOL    selected = NO;
    
    if (indexPath.row == 0) {
        [cell showTopLineWithOffsetX:0];
        selected = [IXUserDefaultM unitSetting] == UnitSettingTypeCount;
    }else{
        [cell showTopLineWithOffsetX:15];
        [cell showBototmLineWithOffsetX:0];
        selected = [IXUserDefaultM unitSetting] == UnitSettingTypeLot;
    }
    
    [cell configTitle:title selectState:selected];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString    * title = self.titleArr[indexPath.row];
    if ([title isEqualToString:LocalizedString(@"数量")]) {
        [IXUserDefaultM setUnit:UnitSettingTypeCount];
    }else{
        [IXUserDefaultM setUnit:UnitSettingTypeLot];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUnit object:nil];//刷新仓位模块单位显示
    [self leftBtnItemClicked];
}

#pragma mark -
#pragma mark - getter

- (UITableView *)tableV
{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, kScreenHeight - 10)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV  registerClass:[IXLanguageCell class]
         forCellReuseIdentifier:NSStringFromClass([IXLanguageCell class])];
    }
    return _tableV;
}

- (NSArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = @[
                      LocalizedString(@"数量"),
                      LocalizedString(@"手数")
                      ];
    }
    
    return _titleArr;
}

@end
