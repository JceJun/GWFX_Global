//
//  IXSubQuoteStyle1Cell.m
//  IXApp
//
//  Created by Bob on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXSubQuoteStyle1Cell.h"


@interface IXSubQuoteStyle1Cell ()

@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation IXSubQuoteStyle1Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self ) {
        
        UIImageView *topSep = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 1)];
        topSep.backgroundColor = MarketCellSepColor;
        [self.contentView addSubview:topSep];
        
        self.tipLbl.text = @"请选择要开通的服务";
    }
    return self;
}

- (UILabel *)tipLbl
{
    if ( !_tipLbl ) {
        _tipLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, kScreenWidth - 30, 15)
                                     WithFont:PF_MEDI(13)
                                    WithAlign:NSTextAlignmentLeft
                                   wTextColor:0x4c6072
                                   dTextColor:0xff0000];
        [self.contentView addSubview:_tipLbl];
    }
    return _tipLbl;
}

@end
