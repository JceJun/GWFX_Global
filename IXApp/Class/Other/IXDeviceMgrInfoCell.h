//
//  IXDeviceMgrInfoCell.h
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXDeviceMgrInfoCell : UITableViewCell

- (void)reloadUIWithTitle:(NSString *)title
                  content:(NSString *)content
                indexPath:(NSIndexPath *)indexPath;

@end
