//
//  IXAcntBankListCellA.h
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@interface IXAcntBankListCellA : IXClickTableVCell

- (void)reloadUIWithTitle:(NSString *)title;

@end
