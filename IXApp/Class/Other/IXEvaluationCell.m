//
//  IXEvaluationCell.m
//  IXApp
//
//  Created by Evn on 2018/1/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXEvaluationCell.h"
#import "IXAttributeTapLabel.h"
#import "IXEvaluationM.h"

@interface IXEvaluationCell()<UITextFieldDelegate>

@property (nonatomic, strong)UIView *line;
@property (nonatomic, strong)UILabel *category;
@property (nonatomic, strong)IXAttributeTapLabel *titleLab;
@property (nonatomic, strong)UIView *dLine;
@property (nonatomic, strong)IXEvaQuestionM * questionM;

@end

@implementation IXEvaluationCell

- (UIView *)line
{
    if (!_line) {
        _line = [[UIView alloc] initWithFrame:CGRectMake(15,GetView_MaxY(self.category), kScreenWidth - 15, kLineHeight)];
        _line.dk_backgroundColorPicker = DKLineColor;
    }
    [self.contentView addSubview:_line];
    return _line;
}

- (UILabel *)category
{
    if (!_category) {
        _category = [IXCustomView createLable:CGRectMake(15, 0, kScreenWidth - 30, 0)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x99abba
                                   dTextColor:0x8395a4
                                textAlignment:NSTextAlignmentLeft];
        _category.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
        _category.numberOfLines = 0;
        _category.lineBreakMode = NSLineBreakByCharWrapping;
    }
    [self.contentView addSubview:_category];
    return _category;
}

- (IXAttributeTapLabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [[IXAttributeTapLabel alloc] initWithFrame:CGRectMake(15,0,kScreenWidth - 30,30)];
        _titleLab.backgroundColor = [UIColor clearColor];
        _titleLab.numberOfLines = 0;
        _titleLab.lineBreakMode = NSLineBreakByCharWrapping;
        weakself;
        _titleLab.tapBlock = ^(NSString *string) {
            __block NSString * desp = @"";
            [weakSelf.questionM.paraphrase enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
                key = [weakSelf dealWithHighLightedKey:key];
                if (SameString(key, string)) {
                    desp = obj;
                    *stop = YES;
                }
            }];
            
            if (desp.length && weakSelf.selectWordBlock) {
                weakSelf.selectWordBlock(string, desp);
            }
        };
    }
     [self.contentView addSubview:_titleLab];
    return _titleLab;
}

- (UIView *)dLine
{
    if (!_dLine) {
        _dLine = [[UIView alloc] initWithFrame:CGRectMake(15,GetView_MaxY(self.category) - kLineHeight, kScreenWidth - 15, kLineHeight)];
        _dLine.dk_backgroundColorPicker = DKLineColor;
    }
    [self.contentView addSubview:_dLine];
    return _dLine;
}

- (void)refreshData:(IXEvaQuestionM *)evaQuestionM
{
    _questionM = evaQuestionM;
    [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat offsetY = 0.f;
    __block CGRect frame = CGRectMake(0, 0, 0, 0);
    if (evaQuestionM.sectionTitle.length) {
        [self category];
        self.category.text = evaQuestionM.sectionTitle;
        frame = self.category.frame;
        CGSize size = [IXDataProcessTools sizeWithString:evaQuestionM.sectionTitle font:PF_MEDI(13) width:kScreenWidth - 30];
        frame.size.height = size.height + 23.f;
        self.category.frame = frame;
        offsetY = VIEW_H(self.category);
        [self line];
        [self dLine];
    }
    
    UIColor * textColor = AutoNightColor(0x99abba, 0x8395a4);
    NSDictionary * attr = @{NSForegroundColorAttributeName:textColor,NSFontAttributeName:PF_MEDI(13)};
    [self dealWithQues:evaQuestionM complete:^(NSArray *arr, NSString *title) {
        [self.titleLab setText:title attributes:attr tapStringArray:arr];
        frame = self.titleLab.frame;
        frame.size.height = [self textHeightContent:title controlWidth:kScreenWidth - 30 font:PF_MEDI(13)] + 23.f;
        frame.origin.y = offsetY;
        self.titleLab.frame = frame;
    }];
    
    frame = self.dLine.frame;
    frame.origin.y = GetView_MaxY(self.titleLab) - kLineHeight;
    self.dLine.frame = frame;
    
    offsetY = GetView_MaxY(self.titleLab);
    
    for (int i = 0; i < evaQuestionM.optionList.count; i++) {

        BOOL flag = NO;//选中标志
        IXQueOptionM *queOptionM = evaQuestionM.optionList[i];
        switch (evaQuestionM.type) {
            case IXQuestionTypeSingleSel:
            {
                if (evaQuestionM.answer.length &&
                    [IXDataProcessTools stringToAsciiCode:evaQuestionM.answer] == i) {
                    flag = YES;
                }
            }
                break;
            case IXQuestionTypeMutableSel:
            {
                flag = [IXDataProcessTools isContainString:[IXDataProcessTools asciiCodeToString:i] withSet:evaQuestionM.answerArr];
            }
                break;
                
            default:
                break;
        }
        
        //特殊情况-选中单选和多选”其它“
        if (evaQuestionM.isOptionEditable &&
            i == (evaQuestionM.optionList.count - 1)) {
            flag = YES;
        }
        
        frame = CGRectMake(45,offsetY,kScreenWidth - 125,30);
        frame.size.height = [self textHeightContent:queOptionM.content controlWidth:kScreenWidth - 125 font:PF_MEDI(13)] + 23.f;
        UILabel *lbl = [IXCustomView createLable:frame
                                           title:queOptionM.content
                                            font:PF_MEDI(13)
                                      wTextColor:0x4c6072
                                      dTextColor:0xe9e9ea
                                   textAlignment:NSTextAlignmentLeft];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.tag = i;
        lbl.numberOfLines = 0;
        lbl.lineBreakMode = NSLineBreakByCharWrapping;
        [self.contentView addSubview:lbl];
        
        UIButton *iconBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, offsetY + (VIEW_H(lbl) - 30)/2, 50, 30)];
        [iconBtn addTarget:self action:@selector(iconBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        iconBtn.tag = i;
        iconBtn.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:iconBtn];
        
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(15,offsetY + (VIEW_H(lbl) - 20)/2, 20, 20)];
        switch (evaQuestionM.type) {
            case IXQuestionTypeSingleSel:{
                if (flag) {
                    icon.dk_imagePicker = DKImageNames(@"evaluation_circle_enable", @"evaluation_circle_enable");
                } else {
                    icon.dk_imagePicker = DKImageNames(@"evaluation_circle_unable", @"evaluation_circle_unable");
                }
            }
                break;
            case IXQuestionTypeMutableSel:{
                if (flag) {
                    icon.dk_imagePicker = DKImageNames(@"evaluation_rect_enable", @"evaluation_rect_enable");
                } else {
                    icon.dk_imagePicker = DKImageNames(@"evaluation_rect_unable", @"evaluation_rect_unable");
                }
            }
                break;
                
            default:
                break;
        }
        icon.tag = i;
        if (evaQuestionM.enable) {
            lbl.userInteractionEnabled = YES;
            iconBtn.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAction:)];
            [lbl addGestureRecognizer:tap];
            icon.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAction:)];
            [icon addGestureRecognizer:tapGr];
        } else {
            iconBtn.userInteractionEnabled = NO;
            lbl.dk_textColorPicker = DKColorWithRGBs(0x99abba, 0x8395a4);
        }
        [self.contentView addSubview:icon];
        
        if (queOptionM.gotoSerialNo) {
            NSString *nValue = [NSString stringWithFormat:@"%d",queOptionM.gotoSerialNo];
            CGSize size = [IXDataProcessTools textSizeByText:nValue height:15 font:RO_REGU(12)];
            CGFloat width = size.width + 8.f;
            
            UILabel *nLbl = [IXCustomView createLable:CGRectMake(kScreenWidth - (width + 15),offsetY + (VIEW_H(lbl) - 15)/2,width,15)
                                                title:nValue
                                                 font:RO_REGU(12)
                                           wTextColor:0x4c6072
                                           dTextColor:0xe9e9ea
                                        textAlignment:NSTextAlignmentCenter];
            nLbl.backgroundColor = [UIColor clearColor];
            nLbl.layer.dk_borderColorPicker = DKColorWithRGBs(0x4c6072, 0xe9e9ea);
            nLbl.layer.cornerRadius = 4;
            nLbl.layer.borderWidth = kLineHeight;
            [self.contentView addSubview:nLbl];
            
            UIImageView *rIcon = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (VIEW_W(nLbl) + 37), offsetY + (VIEW_H(lbl) - 10)/2, 12, 10)];
            rIcon.dk_imagePicker = DKImageNames(@"evaluation_arrows", @"evaluation_arrows");
            [self.contentView addSubview:rIcon];
        }
        
        if (evaQuestionM.lastOptionEditable &&
            evaQuestionM.enable &&
            (evaQuestionM.optionList.count - 1) == i &&
            evaQuestionM.isOptionEditable) {
            self.tField = [[IXTextField alloc] initWithFrame:CGRectMake(GetView_MaxX(icon) + 10, GetView_MaxY(lbl), kScreenWidth - (GetView_MaxX(icon) + 25), 30)];
            self.tField.text = evaQuestionM.userInput;
            self.tField.font = PF_MEDI(13);
            self.tField.delegate = self;
            self.tField.placeholder = LocalizedString(@"请输入...");
            self.tField.dk_textColorPicker = DKCellTitleColor;
            [self.contentView addSubview:self.tField];
            offsetY = GetView_MaxY(self.tField) - kLineHeight + 10.f;
        } else {
            offsetY = GetView_MaxY(lbl) - kLineHeight;
        }
        if (i < (evaQuestionM.optionList.count - 1)) {
            UIView *dLine = [[UIView alloc] initWithFrame:CGRectMake(15,offsetY, kScreenWidth - 15, kLineHeight)];
            dLine.dk_backgroundColorPicker = DKLineColor;
            [self.contentView addSubview:dLine];
        }
    }
    frame = self.frame;
    frame.size.height = offsetY;
    self.frame = frame;
}

- (void)dealWithQues:(IXEvaQuestionM *)question complete:(void(^)(NSArray * arr, NSString * title))comoplete;
{
    __block NSString *titleStr = question.title;
    switch (question.type) {
        case IXQuestionTypeSingleSel:{
            break;
        }
        case IXQuestionTypeMutableSel:{
            titleStr = [NSString stringWithFormat:@"%@(%@)",titleStr,LocalizedString(@"可多选")];
            break;
        }
        default:
            break;
    }
    NSDictionary    * dic = question.paraphrase;
    NSDictionary    * attr = @{NSForegroundColorAttributeName:AutoNightColor(0x11b873, 0x21ce99)};
    __block NSMutableArray * array = [@[] mutableCopy];
   
    [dic enumerateKeysAndObjectsUsingBlock:^(NSString *  _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        NSRange range = [titleStr rangeOfString:key];
        if (range.length > 0) {
            key = [self dealWithHighLightedKey:key];
            titleStr = [titleStr stringByReplacingCharactersInRange:range withString:key];
            
            IXAttributeModel    * model = [IXAttributeModel new];
            model.string = key;
            model.range = range;
            model.alertImg = [UIImage imageNamed:@"common_alert"];
            model.attributeDic = attr;
            [array addObject:model];
        }
    }];
    
    if (comoplete) {
        comoplete (array, titleStr);
    }
}

- (NSString *)dealWithHighLightedKey:(NSString *)key
{
    return [key stringByReplacingOccurrencesOfString:@"#" withString:@" "];
}

- (void)iconBtnAction:(UIButton *)btn
{
    [self selectIndex:btn.tag];
}

- (void)selectAction:(UITapGestureRecognizer *)tap
{
    NSInteger index = [[tap view] tag];
    [self selectIndex:index];
}

- (void)selectIndex:(NSInteger)index
{
    if (self.selectEvaluateB) {
        self.selectEvaluateB(self.tag, index);
    }
}

- (CGFloat)textHeightContent:(NSString *)content
               controlWidth:(CGFloat)width
                        font:(UIFont *)font
{
    CGSize size = [IXDataProcessTools sizeWithString:content font:font width:width];
    return size.height;
}

@end
