//
//  IXAcntPswV.h
//  IXApp
//
//  Created by Evn on 2017/12/7.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IXAcntPswVDelegate

- (void)passWord:(NSString *)psw;

@end

@interface IXAcntPswV : UIView

@property (nonatomic, assign)id <IXAcntPswVDelegate> delegate;
- (void)show;

@end
