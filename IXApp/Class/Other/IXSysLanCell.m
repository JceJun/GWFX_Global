//
//  IXSysLanCell.m
//  IXApp
//
//  Created by Bob on 16/12/01.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXSysLanCell.h"

@implementation IXSysLanCell
{
    /**名称*/
    UILabel *titleLabel_;
    UIImageView *imageView_;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        titleLabel_ = [IXUtils createLblWithFrame:CGRectMake(15,12, kScreenWidth - 30, 20)
                                         WithFont:PF_MEDI(13)
                                        WithAlign:NSTextAlignmentLeft
                                       wTextColor:0x4c6072
                                       dTextColor:0xe9e9ea];
        [self.contentView addSubview:titleLabel_];

        imageView_ = [[UIImageView alloc]initWithFrame:CGRectMake(kScreenWidth - 31, 14, 16, 16)];
        imageView_.dk_imagePicker = DKImageNames(@"common_cell_choose", @"common_cell_choose_D");
        [self.contentView addSubview:imageView_];
        [imageView_ setHidden:YES];
        self.dk_backgroundColorPicker = DKNavBarColor;
    }
    return self;

}

-(void)setModel:(IXSysLanModel *)model{
    _model = model;
    titleLabel_.text = model.name;
    imageView_.hidden = !model.isSelect;
}

@end

@implementation IXSysLanModel

@end

