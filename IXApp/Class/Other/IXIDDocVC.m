//
//  IXIDDocVC.m
//  IXApp
//
//  Created by Larry on 2018/6/28.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXIDDocVC.h"
#import "YYText.h"
#import "IXDocumentView.h"
#import "UIKit+Block.h"
#import "IXOpenChooseContentView.h"
#import "UIImageView+WebCache.h"
#import "IXBORequestMgr+Account.h"
#import "IXBORequestMgr+Asset.h"
#import "IXUserInfoM.h"

@interface IXIDDocVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)UIView *mainView;
@property(nonatomic,strong)UILabel *lb_title;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,assign)NSInteger photoGiveIndex;
@property(nonatomic,strong)IXDocumentView * imgV1;
@property(nonatomic,strong)IXDocumentView * imgV2;
@property(nonatomic,strong)NSDictionary *imgInfo1;
@property(nonatomic,strong)NSDictionary *imgInfo2;
@property(nonatomic,strong)NSDictionary *uploadInfo1;
@property(nonatomic,strong)NSDictionary *uploadInfo2;
@property(nonatomic,strong)UIButton * nextBtn;

@end

@implementation IXIDDocVC

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.bounces = NO;
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
            make.width.equalTo(kScreenWidth);
        }];
    }
    return _scrollView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showProgressWithAimStep:3 totalStep:4 originY:0];
    self.title = @"ID Document";
    [self addBackItem];
    [self addCancelItem];
    
    [self scrollView];
    [self lb_title];
    [self mainView];
    
    [self loadUI];
    
    [self reloadSubmitUI];
}

- (void)loadUI{
    weakself;
    NSArray *fileArr = [IXBORequestMgr shareInstance].fileArr;
    [fileArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *info = obj;
        if ([info[@"fileType"] isEqualToString:@"FILE_TYPE_IDCARD_FRONT"]) {
            // 前图
            weakSelf.imgInfo1 = info;
        }else if([info[@"fileType"] isEqualToString:@"FILE_TYPE_IDCARD_BACK"]){
            // 后图
            weakSelf.imgInfo2 = info;
        }
    }];
    
    if (_imgInfo1) {
        if ([_imgInfo1[@"ftpFilePath"] length]) {
            [_imgV1.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo1[@"ftpFilePath"]]];
            _imgV1.imageV.hidden = NO;
        }
        _imgV1.status = _imgInfo1[@"proposalStatus"];
    }
    if (_imgInfo2) {
        if ([_imgInfo2[@"ftpFilePath"] length]) {
            [_imgV2.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo2[@"ftpFilePath"]]];
            _imgV2.imageV.hidden = NO;
        }
        _imgV2.status = _imgInfo2[@"proposalStatus"];
    }
    
}


- (UILabel *)lb_title{
    if (!_lb_title) {
        _lb_title = [UILabel new];
        _lb_title.font = ROBOT_FONT(11);
        _lb_title.dk_textColorPicker = DKCellTitleColor;
        _lb_title.textAlignment = NSTextAlignmentCenter;
        _lb_title.numberOfLines = 0;
        [self.scrollView addSubview:_lb_title];
        [_lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.right.equalTo(-15);
            make.top.equalTo(20);
            make.centerX.equalTo(0);
        }];
        [self textToYY:@"Please make sure" label:_lb_title];
        weakself;
        [_lb_title block_whenTapped:^(UIView *aView) {
            [weakSelf showMuskTipView];
        }];
    }
    return _lb_title;
}


- (void)textToYY:(NSString *)title label:(UILabel *)label{
    NSString *textStr;
    NSMutableAttributedString *text;
    
    NSString *rangeStr0;
    NSString *rangeStr1;
    
    NSRange range0;
    NSRange range1;
    
    if ([title isEqualToString:@"Please make sure"]) {
        rangeStr0 = @"Please make sure the uploaded photo complete and clear in order to get approved successfully.\nClick to check\n";
        rangeStr1 = @"the photo sample and rules";
    }else{
//        rangeStr0 = @"The Skrill Payment Gateway is opening\n";
//        rangeStr1 = @"VISA,AE,JCB,Diners Club,etc";
    }
    
    
    textStr = [NSString stringWithFormat:@"%@%@",rangeStr0,rangeStr1];
    text = [[NSMutableAttributedString alloc] initWithString:textStr];
    
    range0 = [[text string] rangeOfString:rangeStr0 options:NSCaseInsensitiveSearch];
    [text yy_setColor:HexRGB(0x99abba) range:range0];
    [text yy_setFont:PINGFANG_MEDI_FONT(11) range:range0];
    
    range1 = [[text string] rangeOfString:rangeStr1 options:NSCaseInsensitiveSearch];
    [text yy_setColor:HexRGB(0x4c6072)  range:range1];
    [text yy_setFont:PINGFANG_MEDI_FONT(11) range:range1];
    
    //文字间距
    // [text yy_setKern:@(2) range:range0];
    // 行间距
//    text.yy_lineSpacing = 8;
    
    //下划线
    [text setAttributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)}
                  range:range1];
    
    // 高亮
    weakself;
    [text yy_setTextHighlightRange:range1
                             color:HexRGB(0x4c6072)
                   backgroundColor:[UIColor clearColor]
                         tapAction:^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect){
                             //点击事件
                             [weakSelf showMuskTipView];
                         }];
    
    label.attributedText = text;
}

- (UIView *)mainView{
    if (!_mainView) {
        _mainView = [UIView new];
        _mainView.dk_backgroundColorPicker = DKNavBarColor;
        [self.scrollView addSubview:_mainView];
        [_mainView makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(kScreenWidth);
            make.top.equalTo(_lb_title.bottom).offset(20);
            make.bottom.equalTo(0);
            make.centerX.equalTo(0);
        }];
        
        UILabel *lb_tip = [UILabel new];
        lb_tip.font = ROBOT_FONT(13);
        lb_tip.dk_textColorPicker = DKCellTitleColor;
        lb_tip.text = @"Please upload your ID Photo";
        [_mainView addSubview:lb_tip];
        [lb_tip makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(15);
        }];
        weakself;
        IXDocumentView * imgV1 = [IXDocumentView makeDocument:@"ID Front" superView:_mainView clearUploadInfo:^{
            weakSelf.uploadInfo1 = nil;
            [weakSelf reloadSubmitUI];
        }];
        [imgV1 makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lb_tip.bottom).offset(15);
        }];
        [imgV1 block_whenTapped:^(UIView *aView) {
            weakSelf.photoGiveIndex = 1;
            [weakSelf pickOne];
        }];
        _imgV1 = imgV1;
        
        IXDocumentView * imgV2 = [IXDocumentView makeDocument:@"ID Back" superView:_mainView clearUploadInfo:^{
            weakSelf.uploadInfo2 = nil;
            [weakSelf reloadSubmitUI];
        }];
        [imgV2 makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imgV1.bottom).offset(15);
        }];
        [imgV2 block_whenTapped:^(UIView *aView) {
            weakSelf.photoGiveIndex = 2;
            [weakSelf pickOne];
        }];
        _imgV2 = imgV2;
        
        UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        
        UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
        dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
        
        UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.titleLabel.font = PF_REGU(15);
        [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
        [nextBtn setTitle:@"Submission" forState:UIControlStateNormal];
        [_mainView addSubview:nextBtn];
        
        [nextBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.right.equalTo(-15);
            make.top.equalTo(imgV2.bottom).offset(15);
            make.height.equalTo(44);
            make.bottom.equalTo(-30);
        }];
        [nextBtn block_touchUpInside:^(UIButton *aButton) {
            [weakSelf submitInfo];
        }];
        _nextBtn = nextBtn;
        _nextBtn.enabled = NO;
        
    }
    return _mainView;
}

- (void)gotoNext:(NSString *)title{
    
}

- (void)pickOne
{
    NSArray * arr =   @[
                         LocalizedString(@"选择相机"),
                         LocalizedString(@"选择相册"),
                        ];
    
    weakself;
    IXOpenChooseContentView * chooseV = [[IXOpenChooseContentView alloc] initWithDataSource:arr
                                                                            WithSelectedRow:^(NSInteger row)
                                         {
                                             if (row == 0){
                                                [weakSelf takePhoto];
                                             }else{
                                                [weakSelf joinAlbum];
                                             }
                                         }];
    [chooseV show];
}

- (void)takePhoto{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    // 调用照相机
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)joinAlbum{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    // 访问相册
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = NO;
        imagePicker.navigationBar.translucent = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

// 代理方法（当照完相或者选择完照片会调用这个代理）
#pragma mark UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    UIImage *image = nil;
    NSString *imageId;
    NSString *imageType;
    
    if ([mediaType isEqualToString:@"public.image"])
    {
        image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        NSURL *url = [info objectForKey: @"UIImagePickerControllerReferenceURL"];
        imageId = [url absoluteString];
        NSArray *tmpArray = [imageId componentsSeparatedByString: @"="];
        imageId = [tmpArray objectAtIndex:1];
        imageType = [tmpArray objectAtIndex:2];
        NSArray *tmpArray2 = [imageId componentsSeparatedByString: @"&"];
        imageId = [tmpArray2 objectAtIndex:0];
    }
    
    // 把file和fileName赋值
    if (imageId == nil) {
        double s = [NSDate timeIntervalSinceReferenceDate];
        imageId = [NSString stringWithFormat:@"%f", s];
        imageType = @"PNG";
    }
//    NSString *fileName = [[NSString alloc] initWithFormat: @"%@.%@", imageId, imageType];
//    NSData *img_data = UIImageJPEGRepresentation(image, 0.5);
    
    weakself;
    [picker dismissViewControllerAnimated:YES completion:^{
        switch (weakSelf.photoGiveIndex) {
            case 1:{
                weakSelf.imgV1.imageV.image = image;
                weakSelf.imgV1.imageV.hidden = NO;
                weakSelf.imgV1.lb_status.hidden = YES;
                
            }break;
            case 2:{
                weakSelf.imgV2.imageV.image = image;
                weakSelf.imgV2.imageV.hidden = NO;
                weakSelf.imgV2.lb_status.hidden = YES;
            }break;
        }
        [weakSelf submitFile:image];
    }];
}


- (void)showMuskTipView{
    UIView *view = [UIView new];
    view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    [KEYWINDOW addSubview:view];
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(KEYWINDOW);
    }];
    [view block_whenTapped:^(UIView *aView) {
        [aView removeFromSuperview];
    }];
    
    UILabel *lb_Correct = [UILabel new];
    lb_Correct.font = ROBOT_FONT(13);
    lb_Correct.textColor = [UIColor whiteColor];
    lb_Correct.text = @"Correct";
    [view addSubview:lb_Correct];
    [lb_Correct makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.top.equalTo(25);
    }];
    
    UILabel *lb_content = [UILabel new];
    lb_content.font = ROBOT_FONT(11);
    lb_content.textColor = [UIColor whiteColor];
    lb_content.text = @"Please make sure that the photos of ID front or ID back you took do have a clear border and brightness uniformity.";
                        
    lb_content.numberOfLines = 0;
    [view addSubview:lb_content];
    [lb_content makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lb_Correct);
        make.right.equalTo(-15);
        make.top.equalTo(lb_Correct.bottom).offset(10);
    }];
    
    UIImageView *imgV1 = [UIImageView new];
    imgV1.image = [UIImage imageNamed:@"id_card_tip1"];
    [view addSubview:imgV1];
    [imgV1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lb_content.bottom).offset(10);
        make.centerX.equalTo(0);
    }];
    
    UILabel *lb_wrong = [UILabel new];
    lb_wrong.font = ROBOT_FONT(13);
    lb_wrong.textColor = [UIColor whiteColor];
    lb_wrong.text = @" ";
    [view addSubview:lb_wrong];
    [lb_wrong makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lb_Correct);
        make.top.equalTo(imgV1.bottom).offset(20);
    }];
    
    UIImageView *imgV2 = [UIImageView new];
    imgV2.image = [UIImage imageNamed:@"id_card_tip2"];
    [view addSubview:imgV2];
    [imgV2 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgV1.bottom).offset(10);
        make.centerX.equalTo(0);
    }];
    
    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(18);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"I Know" forState:UIControlStateNormal];
    nextBtn.userInteractionEnabled = NO;
    [view addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.height.equalTo(44);
        make.top.equalTo(imgV2.bottom).offset(10);
    }];
}

- (void)submitFile:(UIImage *)image{
    NSInteger index = _photoGiveIndex;
    [SVProgressHUD showWithStatus:@"Uploading.."];
    [IXBORequestMgr b_appUploadFile:image rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            switch (index) {
                case 1:{
                    _uploadInfo1 = obj;
                    _imgV1.dismissV.hidden = NO;
                }break;
                case 2:{
                    _uploadInfo2 = obj;
                    _imgV2.dismissV.hidden = NO;
                }break;
            }
            [self reloadSubmitUI];
            [SVProgressHUD dismiss];
        }else{
            switch (self.photoGiveIndex) {
                case 1:{
                    [_imgV1.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo1[@"ftpFilePath"]]];
                    _imgV1.imageV.hidden = NO;
                    _imgV1.status = _imgInfo1[@"proposalStatus"];
                    _imgV1.lb_status.hidden = NO;
                }break;
                case 2:{
                    [_imgV2.imageV sd_setImageWithURL:[NSURL URLWithString:_imgInfo2[@"ftpFilePath"]]];
                    _imgV2.imageV.hidden = NO;
                    _imgV2.status = _imgInfo1[@"proposalStatus"];
                    _imgV2.lb_status.hidden = NO;
                }break;
            }
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}

- (void)submitInfo{
    NSString *gts2CustomerId =  [@([IXBORequestMgr shareInstance].userInfo.gts2CustomerId) stringValue];
    NSMutableArray *files = [NSMutableArray array];
    if (_uploadInfo1) {
        NSDictionary *dic1 = @{
                               @"gts2CustomerId":gts2CustomerId,
                               @"fileType":_imgInfo1[@"fileType"],
                               @"id":_imgInfo1[@"id"], // 列编号,登录接口用户信息中返回
                               
                               @"fileName":_uploadInfo1[@"fileName"],
                               @"filePath":_uploadInfo1[@"fileStorePath"], // 上传接口返回的：fileStorePath
                               @"ftpFilePath":_uploadInfo1[@"webFilePath"], // 上传接口返回的：webFilePath
                               };
        [files addObject:dic1];
    }
    if (_uploadInfo2) {
        NSDictionary *dic2 = @{
                               @"gts2CustomerId":gts2CustomerId,
                               @"fileType":_imgInfo2[@"fileType"],
                               @"id":_imgInfo2[@"id"], // 列编号,登录接口用户信息中返回
                               
                               @"fileName":_uploadInfo2[@"fileName"],
                               @"filePath":_uploadInfo2[@"fileStorePath"], // 上传接口返回的：fileStorePath
                               @"ftpFilePath":_uploadInfo2[@"webFilePath"], // 上传接口返回的：webFilePath
                               };
        [files addObject:dic2];
    }
    
    [IXBORequestMgr b_updateCustomerFiles:files rsp:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [SVProgressHUD showSuccessWithStatus:@"Success"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [SVProgressHUD showInfoWithStatus:@"Upload Faild"];
        }
    }];
}

- (void)reloadSubmitUI{
    if (_uploadInfo1 || _uploadInfo2) {
        _nextBtn.enabled = YES;
    }else{
        _nextBtn.enabled = NO;
    }
}



@end
