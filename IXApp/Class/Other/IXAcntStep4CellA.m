//
//  IXAcntStep4CellA.m
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAcntStep4CellA.h"

@interface IXAcntStep4CellA()

@property (nonatomic, strong)UIImageView *logoImgV;
@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UIImageView *iconImgV;

@end

@implementation IXAcntStep4CellA


- (UIImageView *)logoImgV
{
    if (!_logoImgV) {
        _logoImgV = [[UIImageView alloc] initWithFrame:CGRectMake(13, 12.5, 19, 19)];
        [self.contentView addSubview:_logoImgV];
    }
    
    return _logoImgV;
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(42, 14, kScreenWidth - (42 + 15), 16)];
        _title.font = PF_MEDI(13);
        _title.dk_textColorPicker = DKCellTitleColor;
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UIImageView *)iconImgV
{
    if (!_iconImgV) {
        _iconImgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (7.5 + 16), 15, 7.5, 13.5)];
        _iconImgV.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
    }
    
    return _iconImgV;
}

- (void)loadUIWithText:(NSString *)title
             logoImage:(UIImage *)logoImg
             iconImage:(UIImage *)iconImg
          isHiddenIcon:(BOOL)isHidden
{
    self.title.text = title;
    self.logoImgV.image = logoImg;
    self.iconImgV.hidden = isHidden;
    
    if (!isHidden && iconImg) {
        self.iconImgV.image = iconImg;
    }
    
    self.dk_backgroundColorPicker = DKNavBarColor;
}


@end
