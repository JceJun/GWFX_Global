//
//  IXAcntCerFileCell.h
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@interface IXAcntCerFileCell : IXClickTableVCell

- (void)reloadUIWithTitle:(NSString *)title
                     desc:(NSDictionary *)fileDic
             indexPathRow:(NSInteger)index
                  isFirst:(BOOL)firstFlag;

@end
