//
//  IXMsgLanVC.m
//  IXApp
//
//  Created by Evn on 2018/1/25.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXMsgLanVC.h"
#import "IXLanguageCell.h"
#import "IXBORequestMgr+MsgCenter.h"
#import "NSObject+IX.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"

@interface IXMsgLanVC ()
<
UITableViewDataSource,
UITableViewDelegate
>

@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) NSArray       * titleArr;
@property (nonatomic, strong) NSString      * languageStr;
@end

@implementation IXMsgLanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    [self.view addSubview:self.tableV];
    [self initData];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initData
{
    self.title = LocalizedString(@"通知语言");
    _titleArr = @[@"简体中文",@"English"];
    _languageStr = [IXLocalizationModel currentMessageShowLanguage];
    [_tableV reloadData];
}

- (UITableView *)tableV{
    if (!_tableV){
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, kScreenHeight - 10)];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.dk_backgroundColorPicker = DKTableColor;
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableV  registerClass:[IXLanguageCell class]
         forCellReuseIdentifier:NSStringFromClass([IXLanguageCell class])];
    }
    return _tableV;
}

#pragma mark delegate && datasource method moduel
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_titleArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IXLanguageCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!cell) {
        cell = [[IXLanguageCell alloc] initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:NSStringFromClass([UITableViewCell class])];
    }
    
    [cell showBototmLineWithOffsetX:15];
    if( 0 == indexPath.row ){
        [cell showTopLineWithOffsetX:0];
    }
    else if (indexPath.row == _titleArr.count-1){
        [cell showBototmLineWithOffsetX:0];
    }
    
    BOOL flag = NO;
    if ([_titleArr[indexPath.row] isEqualToString:_languageStr]) {
        flag = YES;
    }
    [cell configTitle:_titleArr[indexPath.row] selectState:flag];
    cell.dk_backgroundColorPicker = DKNavBarColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([_languageStr isEqualToString:_titleArr[indexPath.row]]) {
        return;
    }
    [self updateMessageLang:_titleArr[indexPath.row]];
}

- (void)updateMessageLang:(NSString *)messageLang
{
    [SVProgressHUD showWithStatus:LocalizedString(@"处理中...")];
    weakself;
    messageLang = [IXLocalizationModel getLanguageKeyByLanguage:messageLang];
    [IXBORequestMgr msg_updateMessageLangWithMessageLang:messageLang result:^(BOOL success, NSString *errCode, NSString *errStr, id obj) {
        if (success) {
            [SVProgressHUD dismiss];
            [IXBORequestMgr shareInstance].userInfo.detailInfo.messageLang = messageLang;
            [weakSelf leftBtnItemClicked];
        } else {
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"消息邮件语种修改失败")];
            }
        }
    }];
}

@end
