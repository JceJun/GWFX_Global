//
//  IXChooseAccountCell.h
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXChooseAccountCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *model;
@property (nonatomic, assign) BOOL isCurrentAccount;
@property (nonatomic, assign) BOOL isExsitMt4;//是否存在MT4账户

@end
