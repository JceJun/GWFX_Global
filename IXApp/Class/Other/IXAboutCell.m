//
//  IXAboutCell.m
//  IXApp
//
//  Created by Evn on 17/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAboutCell.h"

@interface IXAboutCell()

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIImageView *accImgV;
@property (nonatomic, strong) UIView *lineView;

@end
@implementation IXAboutCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self accImgV];
    [self lineView];
    self.dk_backgroundColorPicker = DKNavBarColor;
}
- (UILabel *)title
{
    if (!_title) {
        _title = [IXCustomView createLable:CGRectMake(15,14,200, 17)
                                     title:@""
                                      font:PF_MEDI(13)
                                 wTextColor:0x4c6072
                                dTextColor:0xe9e9ea
                             textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UIImageView *)accImgV
{
    if (!_accImgV) {
        _accImgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth - (15 +7), 15, 7, 15)];
        _accImgV.dk_imagePicker = DKImageNames(@"openAccount_arrow_right", @"openAccount_arrow_right_D");
        [self.contentView addSubview:_accImgV];
    }
    
    return _accImgV;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
        _lineView.dk_backgroundColorPicker = DKLineColor;
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)reloadUIData:(NSString *)title
{
    self.title.text = title;
}

@end
