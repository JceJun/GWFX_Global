//
//  IXAcntStep5VC.h
//  IXApp
//
//  Created by Evn on 16/12/22.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
#import "IXOpenAcntDataModel.h"
#import "IXEvaluationM.h"

@interface IXAcntStep5VC : IXDataBaseVC
@property (nonatomic, strong)IXOpenAcntDataModel *acntModel;
@property (nonatomic, strong)IXEvaluationM *evaluationM;
@end
