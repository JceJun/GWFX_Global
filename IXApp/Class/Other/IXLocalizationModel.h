//
//  IXLocalizationModel.h
//  IXApp
//
//  Created by Bob on 2016/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#define  LANGUAGENOTIFY @"CHANGELANGUAGE"
#define LANGUAGE @"localizationlanguage"

#define NAMEEN @"nameEN"
#define NAMECN @"nameCN"
#define NAMETW @"nameTW"

#define LANEN @"en_US"
#define LANCN @"zh_CN"
#define LANTW @"zh_TW"

@interface IXLocalizationModel : NSObject

/**
 *  适配Bo语言的多语言适配
 */
+ (NSString *)getBoLanuageKey;

/**
 *  设置当前语言
 *
 *  @param langugae 语言类型 //@"en" @"zh-Hans" @"zh-Hant"  数据库保存类型 //zh-cn zh-tw en-us
 */
+ (void)setLanguage:(NSString *)langugae;


/**
 *  获取当前语言 :对接UI
 */
+ (NSString *)currentShowLanguage;

/**
 *  获取消息语言类型
 */
+ (NSString *)currentMessageShowLanguage;

/**
 *  获取当前语言 :对接数据库
 */
+ (NSString *)currentCheckLanguage;

/**
 *  根据key获取当前word
 *
 *  @param key 需要查找的key
 */
+ (NSString *)getLocalizationWordByKey:(NSString *)key;

/**
 *  根据消息语言获取当前语言key
 *
 *  @param language 需要处理的语言
 */
+ (NSString *)getLanguageKeyByLanguage:(NSString *)language;


    
@end
