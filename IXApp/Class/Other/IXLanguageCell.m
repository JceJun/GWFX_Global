//
//  IXLanguageCell.m
//  IXApp
//
//  Created by Evn on 17/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXLanguageCell.h"

@interface IXLanguageCell()

@property (nonatomic, strong) UILabel   * titleLbl;
@property (nonatomic, strong) UIButton  * bgBtn;
@property (nonatomic, strong )UIButton  * iconBtn;

@property (nonatomic, strong) UIView    * bottomLine;
@property (nonatomic, strong) UIView    * topLine;

@end

@implementation IXLanguageCell

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)configTitle:(NSString *)title selectState:(BOOL)flag
{
    self.titleLbl.text = title;
    
    if (flag) {
        self.bgBtn.hidden = NO;
        self.iconBtn.hidden = NO;
    } else {
        self.bgBtn.hidden = YES;
        self.iconBtn.hidden = YES;
    }
}

- (void)selected:(BOOL)select
{
    if (select) {
        self.bgBtn.hidden = NO;
        self.iconBtn.hidden = NO;
    }else{
        self.bgBtn.hidden = YES;
        self.iconBtn.hidden = YES;
    }
}

- (void)showTopLineWithOffsetX:(CGFloat)offset
{
    [self.contentView addSubview:self.topLine];
    self.topLine.frame = CGRectMake(offset, 0, kScreenWidth - offset, kLineHeight);
}

- (void)showBototmLineWithOffsetX:(CGFloat)offset
{
    [self.contentView addSubview:self.bottomLine];
    self.bottomLine.frame = CGRectMake(offset, 44 - kLineHeight, kScreenWidth - offset, kLineHeight);
}


#pragma mark -
#pragma mark - lazy loading

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [IXCustomView createLable:CGRectMake(15,12, kScreenWidth - 30, 20)
                                        title:@""
                                         font:PF_MEDI(13)
                                   wTextColor:0x4c6072
                                   dTextColor:0xe9e9ea
                                textAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

- (UIButton *)bgBtn
{
    if (!_bgBtn) {
        _bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 70, 0, 70, 44)];
        [self.contentView addSubview:_bgBtn];
    }
    return _bgBtn;
}

- (UIButton *)iconBtn
{
    if (!_iconBtn) {
        _iconBtn = [[UIButton alloc] initWithFrame:CGRectMake(VIEW_W(_bgBtn)- 31, 14, 16, 16)];
        [_iconBtn dk_setImage:DKImageNames(@"common_cell_choose", @"common_cell_choose_D")
                     forState:UIControlStateNormal];
        
        [self.bgBtn addSubview:_iconBtn];
    }
    
    return _iconBtn;
}


- (UIView *)bottomLine
{
    if (!_bottomLine) {
        
        _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - kLineHeight, kScreenWidth, kLineHeight)];
        _bottomLine.dk_backgroundColorPicker = DKLineColor;
    }
    return _bottomLine;
}

- (UIView *)topLine
{
    if (!_topLine) {
        
        _topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kLineHeight)];
        _topLine.dk_backgroundColorPicker = DKLineColor;
    }
    return _topLine;
}

@end
