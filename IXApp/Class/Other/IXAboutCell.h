//
//  IXAboutCell.h
//  IXApp
//
//  Created by Evn on 17/2/15.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXClickTableVCell.h"

@interface IXAboutCell : IXClickTableVCell

- (void)reloadUIData:(NSString *)title;


@end
