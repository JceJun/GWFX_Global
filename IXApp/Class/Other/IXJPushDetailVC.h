//
//  IXJPushDetailVC.h
//  IXApp
//
//  Created by Evn on 2017/7/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXMsgCenterM;

@interface IXJPushDetailVC : IXDataBaseVC

@property (nonatomic, strong) IXMsgCenterM  *message;

@end
