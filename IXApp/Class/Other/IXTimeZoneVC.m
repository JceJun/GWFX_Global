//
//  IXTimeZoneVC.m
//  IXApp
//
//  Created by Bob on 16/12/01.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTimeZoneVC.h"

#import "IXDateUtils.h"
#import "IXSysLanCell.h"
#import "IXTradeDataCache.h"

@interface IXTimeZoneVC ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tableView_;
    NSMutableArray *dataArray_;
}

@end

@implementation IXTimeZoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    
    self.title = LocalizedString(@"系统时区");

    CGRect frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavbarHeight);
    
    tableView_ = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.dk_separatorColorPicker = DKLineColor;
    tableView_.dk_backgroundColorPicker = DKTableColor;
    
    tableView_.tableFooterView = [[UIView alloc] initWithFrame:ktableFooterFrame];;
    [self.view addSubview:tableView_];
    
    [self creatData];
}


- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)creatData{
    
    dataArray_ = [NSMutableArray array];
 
    
    NSArray *_defaultTimeZone = @[
                                  [NSString stringWithFormat:LocalizedString(@"%@北京时间%@"),@"GMT+8(",@")"],
                                  [NSString stringWithFormat:LocalizedString(@"%@格林尼治标准时间%@"),@"GMT+0(",@")"]
                                  ];
    NSString *saveName = [IXDateUtils getUserSaveTimeZone];
    NSMutableArray *section1 = [NSMutableArray array];
    for(NSInteger i = 0; i < _defaultTimeZone.count; i++){
        IXSysLanModel *model = [[IXSysLanModel alloc]init];
        model.name = _defaultTimeZone[i];
        NSRange range = [model.name rangeOfString:saveName];
        NSRange range1 = [model.name rangeOfString:@"("];
        if(range.length > 0){
            if(model.name.length == saveName.length || (range1.location == range.length+range.location))
                model.isSelect = YES;
        }
        [section1 addObject:model];
    }
    [dataArray_ addObject:section1];
    
    
    NSArray *allTimes = @[@"GMT-12",@"GMT-11",@"GMT-10",@"GMT-9",@"GMT-8",
                          [NSString stringWithFormat:LocalizedString(@"%@洛杉矶%@"),@"GMT-7(",@")"],
                          @"GMT-6",@"GMT-5",
                          [NSString stringWithFormat:LocalizedString(@"%@纽约%@"),@"GMT-4(",@")"],
                          @"GMT-3",@"GMT-2",@"GMT-1",
                          [NSString stringWithFormat:LocalizedString(@"%@伦敦%@"),@"GMT+1(",@")"],
                          [NSString stringWithFormat:LocalizedString(@"%@柏林%@"),@"GMT+2(",@")"],
                          [NSString stringWithFormat:LocalizedString(@"%@莫斯科%@"),@"GMT+3(",@")"],
                          @"GMT+4",@"GMT+5",@"GMT+6",@"GMT+7",
                          [NSString stringWithFormat:LocalizedString(@"%@东京%@"),@"GMT+9(",@")"],
                          [NSString stringWithFormat:LocalizedString(@"%@悉尼%@"),@"GMT+10(",@")"],
                          @"GMT+11",@"GMT+12"];
    NSMutableArray *section2 = [NSMutableArray array];
    for(NSInteger i = 0; i < allTimes.count; i++){
        IXSysLanModel *model = [[IXSysLanModel alloc] init];
        model.name = allTimes[i];
        NSRange range = [model.name rangeOfString:saveName];
        NSRange range1 = [model.name rangeOfString:@"("];
        if(range.length > 0){
            if(model.name.length == saveName.length || (range1.location == range.length+range.location))
                model.isSelect = YES;
        }
        [section2 addObject:model];
    }
    [dataArray_ addObject:section2];
    
    [tableView_ reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray_.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr = dataArray_[section];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(section == 1){
        return 14;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if ( 1 == section ) {
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 14)];
        headView.backgroundColor = [UIColor clearColor];
        return headView;
    }
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *IdentifierCell = @"IdentifierCell";
    IXSysLanCell *cell = [tableView dequeueReusableCellWithIdentifier:IdentifierCell];
    
    if (cell == nil) {
        cell = [[IXSysLanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IdentifierCell];
    }
    cell.model = dataArray_[indexPath.section][indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView_ deselectRowAtIndexPath:indexPath animated:YES];
    
    IXSysLanModel *selectedModel = dataArray_[indexPath.section][indexPath.row];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:selectedModel.name forKey:GTScTimeZone];
    [ud synchronize];
    
    NSString *name = selectedModel.name;
    NSInteger second = [name rangeOfString:@"("].location;
    if ( second != NSNotFound ) {
        name = [name substringToIndex:second];
    }
    NSInteger first = [name rangeOfString:@"GMT"].location;
    if( first != NSNotFound ){
        name = [name substringFromIndex:(first + 3)];
    }
    [ud setObject:name forKey:IXnTimeZone];
    [ud synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [IXTradeDataCache shareInstance].timezone_update =  ![IXTradeDataCache shareInstance].timezone_update;
}

@end
