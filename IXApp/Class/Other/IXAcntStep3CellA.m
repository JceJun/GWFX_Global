//
//  IXAcntStep3CellA.m
//  IXApp
//
//  Created by Magee on 2017/1/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep3CellA.h"

@interface IXAcntStep3CellA ()

@property (nonatomic,strong)UILabel *title;

@end

@implementation IXAcntStep3CellA

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self title];
}

- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 170, 16)];
        _title.font = PF_MEDI(13);
        _title.textColor = MarketGrayPriceColor;
        _title.text = @"请选择";
        _title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_title];
    }
    return _title;
}

@end
