//
//  IXChooseAccountView.h
//  IXApp
//
//  Created by Bob on 2017/2/13.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^chooseContent)(NSIndexPath *indexPath);
@interface IXChooseAccountView : UIView

@property (nonatomic, assign) BOOL isExsitMt4;//是否存在MT4账户

@property (nonatomic, strong) NSArray *accountArr;

@property (nonatomic, strong) UITableView *contentTV;

@property (nonatomic, strong) chooseContent choose;

@end
