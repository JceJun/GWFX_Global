//
//  IXAcntStep7VC.m
//  IXApp
//
//  Created by Evn on 17/2/10.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXAcntStep7VC.h"
#import "IXAppUtil.h"
#import "NSString+FormatterPrice.h"
#import "NSDictionary+Type.h"
#import "UIImageView+AFNetworking.h"
#import "IXSupplementFileM.h"
#import "IXUserDefaultM.h"
#import "IXAcntCerFileVC.h"
#import "UIImageView+WebCache.h"
#import "IXOpenChooseContentView.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXBORequestMgr+Account.h"
#import "IXZoomScrollView.h"
#import "NSObject+IX.h"

@interface IXAcntStep7VC ()
<
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
UIActionSheetDelegate
>

@property (nonatomic, assign)BOOL state;
@property (nonatomic, strong)UIScrollView   * sView;
@property (nonatomic, strong)UIImageView    * leftImgV;
@property (nonatomic, strong)UIImageView    * rightImgV;
@property (nonatomic, strong)UIImageView    * addressImgV;

@property (nonatomic, strong)UIButton   *leftbtn;
@property (nonatomic, strong)UIButton   *leftDelBtn;
@property (nonatomic, strong)UILabel    *leftLbl;
@property (nonatomic, strong)UILabel    *leftStateLbl;
@property (nonatomic, strong)NSString   *lProposalStatus;

@property (nonatomic, strong)UIButton   *rightbtn;
@property (nonatomic, strong)UIButton   *rightDelBtn;
@property (nonatomic, strong)UILabel    *rightLbl;
@property (nonatomic, strong)UILabel    *rightStateLbl;
@property (nonatomic, strong)NSString   *rProposalStatus;

@property (nonatomic, strong)UIButton   *addressBtn;
@property (nonatomic, strong)UIButton   *addressDelBtn;
@property (nonatomic, strong)UILabel    *addressLbl;
@property (nonatomic, strong)UILabel    *addressStateLbl;
@property (nonatomic, strong)NSString   *aProposalStatus;

@property (nonatomic, strong) IXOpenChooseContentView *chooseCata;
@property (nonatomic, strong) NSMutableArray *titleItemArr;

@property (nonatomic, assign)NSInteger  iconIndex;
@property (nonatomic, assign)NSInteger  delIndex;
@property (nonatomic, strong)IXSupplementFileM  * fileModel;
@property (nonatomic, strong)NSMutableArray     * fileArr;
@property (nonatomic, strong)NSString           * gts2CustomerId;

@end

@implementation IXAcntStep7VC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = LocalizedString(@"证明文件");
    [self addBackItemrget:self action:@selector(leftBtnItemClicked)];
    [self addDismissItemWithTitle:LocalizedString(@"完成")
                              target:self
                              action:@selector(rightBtnItemClicked)];
    
    _iconIndex = -1;
    _delIndex = -1;
    _fileArr = [[NSMutableArray alloc] init];
    _sView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,
                                                            kScreenWidth,
                                                            kScreenHeight - kNavbarHeight)];
    _sView.showsVerticalScrollIndicator = NO;
    _sView.showsHorizontalScrollIndicator = NO;
    _sView.dk_backgroundColorPicker = DKTableColor;
    [self.view addSubview:_sView];
    [self addHearderView];
    [self addIdentifyFileView];
    [self addAddressFileView];
    [self refreshUI];
    
    if ([[IXBORequestMgr shareInstance].userInfo enable]) {
        _gts2CustomerId = [NSString stringWithFormat:@"%ld",
                           (long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
        [self loadCustomerFiles];
    }else{
        [SVProgressHUD showWithStatus:LocalizedString(@"获取用户信息中...")];
        [IXBORequestMgr  refreshUserInfo:^(BOOL success) {
            if (!success) {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取用户信息失败")];
            }else{
                [SVProgressHUD dismiss];
                _gts2CustomerId = [NSString stringWithFormat:@"%ld",
                                   (long)[IXBORequestMgr shareInstance].userInfo.gts2CustomerId];
                [self loadCustomerFiles];
            }
        }];
    }
}

- (void)leftBtnItemClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnItemClicked
{
    if (_leftImgV.image && _rightImgV.image && _addressImgV.image) {
        if (_fileArr.count) {
            [self submitCerFileInfo];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"数据没有变化，无需修改")];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"请补充证明文件资料")];
    }
}


#pragma mark -
#pragma mark - btn action

- (void)onGoback
{
    BOOL flag = NO;
    NSArray *temArray = self.navigationController.viewControllers;
    for(UIViewController *temVC in temArray){
        if ([temVC isKindOfClass:[IXAcntCerFileVC class]]){
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCerFileUpload object:nil];
            flag = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToViewController:temVC animated:YES];
            });
        }
    }
    if (!flag) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    }
}


#pragma mark -
#pragma mark - UI

- (void)addHearderView
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, kScreenWidth, 133)];
    headView.dk_backgroundColorPicker = DKTableColor;
    UILabel *tipTitle = [IXUtils createLblWithFrame:CGRectMake( 0, 26, kScreenWidth, 18)
                                           WithFont:PF_MEDI(15)
                                          WithAlign:NSTextAlignmentCenter
                                         wTextColor:0x4c6072
                                         dTextColor:0x8395a4];
    tipTitle.text = LocalizedString(@"上传证明文件");
    [headView addSubview:tipTitle];
    
    if(!_state){
        if ([BoLanKey isEqualToString:NAMEEN]) {
            UILabel *lbl = [IXUtils createLblWithFrame:CGRectMake( 15, 55, kScreenWidth - 30, 78)
                                              WithFont:PF_MEDI(12)
                                             WithAlign:NSTextAlignmentCenter
                                            wTextColor:0x99abba
                                            dTextColor:0x8395a4];
            lbl.text = [NSString stringWithFormat:@"%@%@%@",
                        LocalizedString(@"基于监管机构的规定，您需要在申请开户后14天内"),
                        LocalizedString(@"提交证明文件。为了不影响您的交易和提款，"),
                        LocalizedString(@"敬请阁下尽快提交。")];
            [headView addSubview:lbl];
            lbl.numberOfLines = 0;
        } else {
            UILabel *rule1 = [IXUtils createLblWithFrame:CGRectMake( 0, 55, kScreenWidth, 12)
                                                WithFont:PF_MEDI(12)
                                               WithAlign:NSTextAlignmentCenter
                                              wTextColor:0x99abba
                                              dTextColor:0x8395a4];
            rule1.text = LocalizedString(@"基于监管机构的规定，您需要在申请开户后14天内");
            [headView addSubview:rule1];
            rule1.numberOfLines = 0;
            CGSize size = [rule1.text boundingRectWithSize:CGSizeMake(kScreenWidth , MAXFLOAT)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:@{NSFontAttributeName:PF_MEDI(12)}
                                                   context:nil].size;
            CGRect rect = rule1.frame;
            rect.size.height = size.height;
            rule1.frame = rect;
            
            UILabel *rule2 = [IXUtils createLblWithFrame:CGRectMake( 0,
                                                                    VIEW_Y(rule1) + VIEW_H(rule1) + 5,
                                                                    kScreenWidth, 12)
                                                WithFont:PF_MEDI(12)
                                               WithAlign:NSTextAlignmentCenter
                                              wTextColor:0x99abba
                                              dTextColor:0x8395a4];
            rule2.text = LocalizedString(@"提交证明文件。为了不影响您的交易和提款，");
            [headView addSubview:rule2];
            
            UILabel *rule3 = [IXUtils createLblWithFrame:CGRectMake( 0, VIEW_Y(rule2) +
                                                                    VIEW_H(rule2) + 5,
                                                                    kScreenWidth, 12)
                                                WithFont:PF_MEDI(12)
                                               WithAlign:NSTextAlignmentCenter
                                              wTextColor:0x99abba
                                              dTextColor:0x8395a4];
            rule3.text = LocalizedString(@"敬请阁下尽快提交。");
            [headView addSubview:rule3];
        }
    }else{
        
        CGRect frame = tipTitle.frame;
        frame.origin.y = 40;
        tipTitle.frame = frame;
        
        UIImageView *stateImg = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 62)/2,
                                                                              65, 62, 24)];
        [stateImg setImage:[UIImage imageNamed:@"openAccount_review_Status"]];
        [headView addSubview:stateImg];
    }
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                VIEW_H(headView) - kLineHeight,
                                                                kScreenWidth, kLineHeight)];
    lineView.dk_backgroundColorPicker = DKLineColor;
    [headView addSubview:lineView];
    
    [_sView addSubview:headView];
}

- (void)addIdentifyFileView
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,133,
                                                                kScreenWidth,
                                                                200)];
    headView.dk_backgroundColorPicker = DKViewColor;
    
    UILabel *titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15,
                                                               15,
                                                               kScreenWidth - 30,
                                                               15)
                                           WithFont:PF_MEDI(13)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0x4c6072
                                         dTextColor:0xe9e9ea];
    titleLbl.text = LocalizedString(@"身份证明文件");
    [headView addSubview:titleLbl];
    
    UILabel *descLbl = [IXUtils createLblWithFrame:CGRectMake(15,
                                                              GetView_MaxY(titleLbl) + 10,
                                                              kScreenWidth - 30,
                                                              36)
                                          WithFont:PF_MEDI(12)
                                         WithAlign:NSTextAlignmentLeft
                                        wTextColor:0x99abba
                                        dTextColor:0x8395a4];
    descLbl.text = LocalizedString(@"身份证正反面拍照清晰不可有光点或者复印件。");
    [headView addSubview:descLbl];
    descLbl.numberOfLines = 0;
    
    CGSize size = [descLbl.text boundingRectWithSize:CGSizeMake(kScreenWidth , MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:PF_MEDI(12)}
                                             context:nil].size;
    CGRect rect = descLbl.frame;
    rect.size.height = size.height;
    descLbl.frame = rect;
    
    _leftImgV = [[UIImageView alloc] initWithFrame:CGRectMake(15,
                                                              GetView_MaxY(descLbl) + 15,
                                                              kScreenWidth/2 - (15 + 15/2),
                                                              120)];
    [_leftImgV setUserInteractionEnabled:YES];
    _leftImgV.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x242a36);
    _leftImgV.layer.cornerRadius = 5;
    _leftImgV.layer.masksToBounds = YES;
    [_leftImgV setContentMode:UIViewContentModeScaleAspectFill];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(responseLeftPhoto)];
    [_leftImgV addGestureRecognizer:tap];
    [headView addSubview:_leftImgV];
    
    _leftbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _leftbtn.frame = CGRectMake( (VIEW_W(_leftImgV) - 108)/2, 15, 108, 69);
    [_leftbtn dk_setImage:DKImageNames(@"openAccount_idCard_pos", @"openAccount_idCard_pos_D")
                 forState:UIControlStateNormal];
    [_leftbtn addTarget:self action:@selector(responseLeftPhoto)
       forControlEvents:UIControlEventTouchUpInside];
    [_leftImgV addSubview:_leftbtn];
    _leftLbl = [IXCustomView createLable:CGRectMake(0,
                                                    GetView_MaxY(_leftbtn) + 10,
                                                    VIEW_W(_leftImgV),
                                                    15)
                                   title:LocalizedString(@"上传证件正面")
                                    font:PF_MEDI(13)
                              wTextColor:0x99abba
                              dTextColor:0x8395a4
                           textAlignment:NSTextAlignmentCenter];
    [_leftImgV addSubview:_leftLbl];
    
    _leftDelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _leftDelBtn.frame = CGRectMake( VIEW_W(_leftImgV) - (5 + 25), 5, 25, 25);
    [_leftDelBtn dk_setImage:DKImageNames(@"openAccount_remove_btn", @"openAccount_remove_btn_D")
                    forState:UIControlStateNormal];
    [_leftImgV addSubview:_leftDelBtn];
    [_leftDelBtn addTarget:self action:@selector(responseLeftDeletePhoto) forControlEvents:UIControlEventTouchUpInside];
    _leftDelBtn.hidden = YES;
    
    _leftStateLbl = [[UILabel alloc] initWithFrame:CGRectMake(-12, 10, 76, 24)];
    _leftStateLbl.textColor = UIColorHexFromRGB(0xffffff);
    _leftStateLbl.font = PF_MEDI(12);
    _leftStateLbl.textAlignment = NSTextAlignmentCenter;
    _leftStateLbl.layer.cornerRadius = 12;
    _leftStateLbl.layer.masksToBounds = YES;
    [_leftImgV addSubview:_leftStateLbl];
    _leftStateLbl.hidden = YES;
    
    _rightImgV = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth/2 + 15/2,
                                                               GetView_MaxY(descLbl) + 15,
                                                               kScreenWidth/2 - (15 + 15/2),
                                                               120)];
    [_rightImgV setUserInteractionEnabled:YES];
    _rightImgV.backgroundColor = MarketCellSepColor;
    _rightImgV.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x242a36);
    _rightImgV.layer.cornerRadius = 5;
    _rightImgV.layer.masksToBounds = YES;
    [_rightImgV setContentMode:UIViewContentModeScaleAspectFill];
    UITapGestureRecognizer *rTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(responseRightPhoto)];
    [_rightImgV addGestureRecognizer:rTap];
    [headView addSubview:_rightImgV];
    
    _rightbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightbtn.frame = CGRectMake( (VIEW_W(_rightImgV) - 108)/2, 15, 108, 69);
    [_rightbtn dk_setImage:DKImageNames(@"openAccount_idCard_reverse", @"openAccount_idCard_reverse_D")
                  forState:UIControlStateNormal];
    [_rightbtn addTarget:self action:@selector(responseRightPhoto)
        forControlEvents:UIControlEventTouchUpInside];
    [_rightImgV addSubview:_rightbtn];
    
    _rightLbl = [IXCustomView createLable:CGRectMake(0,
                                                     GetView_MaxY(_rightbtn) + 10,
                                                     VIEW_W(_rightImgV),
                                                     15)
                                    title:LocalizedString(@"上传证件背面")
                                     font:PF_MEDI(13)
                               wTextColor:0x99abba
                               dTextColor:0x8395a4
                            textAlignment:NSTextAlignmentCenter];
    [_rightImgV addSubview:_rightLbl];
    
    _rightDelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightDelBtn.frame = CGRectMake( VIEW_W(_rightImgV) - (5 + 25), 5, 25, 25);
    [_rightDelBtn dk_setImage:DKImageNames(@"openAccount_remove_btn", @"openAccount_remove_btn_D")
                     forState:UIControlStateNormal];
    [_rightImgV addSubview:_rightDelBtn];
    [_rightDelBtn addTarget:self action:@selector(responseRightDeletePhoto)
           forControlEvents:UIControlEventTouchUpInside];
    _rightDelBtn.hidden = YES;
    
    _rightStateLbl = [[UILabel alloc] initWithFrame:CGRectMake(-12, 10, 76, 24)];
    [_rightImgV addSubview:_rightStateLbl];
    _rightStateLbl.textColor = UIColorHexFromRGB(0xffffff);
    _rightStateLbl.font = PF_MEDI(12);
    _rightStateLbl.layer.cornerRadius = 12;
    _rightStateLbl.layer.masksToBounds = YES;
    _rightStateLbl.textAlignment = NSTextAlignmentCenter;
    _rightStateLbl.hidden = YES;
    [_sView addSubview:headView];
}

- (void)addAddressFileView
{    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,133 + 200, kScreenWidth, 1)];
    lineView.dk_backgroundColorPicker = DKLineColor;
    [_sView addSubview:lineView];
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,133 + 200, kScreenWidth, 268)];
    headView.dk_backgroundColorPicker = DKViewColor;
    
    UILabel *titleLbl = [IXUtils createLblWithFrame:CGRectMake( 15, 15, kScreenWidth, 15)
                                           WithFont:PF_MEDI(13)
                                          WithAlign:NSTextAlignmentLeft
                                         wTextColor:0x4c6072
                                         dTextColor:0xe9e9ea];
    titleLbl.text = LocalizedString(@"地址证明文件");
    [headView addSubview:titleLbl];
    
    UITextView *descV = [[UITextView alloc] initWithFrame:CGRectMake(15,
                                                                     GetView_MaxY(titleLbl) + 5,
                                                                     kScreenWidth - 15*2,
                                                                     90)];
    descV.dk_textColorPicker = DKCellContentColor;
    descV.backgroundColor = [UIColor clearColor];
    descV.text = LocalizedString(@"可用身份证地址作为地址证明，住址证明可以使用三个月内的公用事业收费单（例如水、电、煤气收费单等）、银行账户（信用卡、借记卡）月结单，住址证明上显示的姓名须与您的名字一致，同时地址须为您的家庭地址。");
    descV.font = PF_MEDI(12);
    descV.editable = NO;
    descV.textAlignment = NSTextAlignmentLeft;
    descV.userInteractionEnabled = NO;
    [headView addSubview:descV];
    
    _addressImgV = [[UIImageView alloc] initWithFrame:CGRectMake(15,
                                                                 GetView_MaxY(descV) + 5,
                                                                 kScreenWidth - 15*2,
                                                                 120)];
    [_addressImgV setUserInteractionEnabled:YES];
    _addressImgV.dk_backgroundColorPicker = DKColorWithRGBs(0xe2eaf2, 0x242a36);
    _addressImgV.layer.cornerRadius = 5;
    _addressImgV.layer.masksToBounds = YES;
    [_addressImgV setContentMode:UIViewContentModeScaleAspectFill];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(responseAddressPhoto)];
    [_addressImgV addGestureRecognizer:tap];
    [headView addSubview:_addressImgV];
    
    _addressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _addressBtn.frame = CGRectMake( (VIEW_W(_addressImgV) - 108)/2, 15, 108, 69);
    [_addressBtn dk_setImage:DKImageNames(@"openAccount_address", @"openAccount_address_D")
                    forState:UIControlStateNormal];
    [_addressBtn addTarget:self action:@selector(responseAddressPhoto)
          forControlEvents:UIControlEventTouchUpInside];
    [_addressImgV addSubview:_addressBtn];
    
    _addressLbl = [IXCustomView createLable:CGRectMake(0,
                                                       GetView_MaxY(_addressBtn) + 10,
                                                       VIEW_W(_addressImgV),
                                                       15)
                                      title:LocalizedString(@"上传住址证明文件")
                                       font:PF_MEDI(13)
                                 wTextColor:0x99abba
                                 dTextColor:0x8395a4
                              textAlignment:NSTextAlignmentCenter];
    [_addressImgV addSubview:_addressLbl];
    
    _addressDelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _addressDelBtn.frame = CGRectMake( VIEW_W(_addressImgV) - (20 + 25), 20, 25, 25);
    [_addressDelBtn dk_setImage:DKImageNames(@"openAccount_remove_btn", @"openAccount_remove_btn_D")
                       forState:UIControlStateNormal];
    [_addressImgV addSubview:_addressDelBtn];
    [_addressDelBtn addTarget:self action:@selector(responseAddressDeletePhoto)
             forControlEvents:UIControlEventTouchUpInside];
    _addressDelBtn.hidden = YES;
    
    _addressStateLbl = [[UILabel alloc] initWithFrame:CGRectMake(-12, 10, 76, 24)];
    _addressStateLbl.textColor = UIColorHexFromRGB(0xffffff);
    _addressStateLbl.font = PF_MEDI(12);
    _addressStateLbl.textAlignment = NSTextAlignmentCenter;
    [_addressImgV addSubview:_addressStateLbl];
    _addressStateLbl.hidden = YES;
    _addressStateLbl.layer.cornerRadius = 12;
    _addressStateLbl.layer.masksToBounds = YES;
    [_sView addSubview:headView];
    
    if (((GetView_MaxY(headView) + kNavbarHeight) > kScreenHeight)) {
        _sView.contentSize = CGSizeMake(kScreenWidth, GetView_MaxY(headView)+1);
    }else{
        _sView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight - kNavbarHeight + 1);
    }
}

- (void)refreshUI
{
    if (!_cerArr || _cerArr.count == 0) {
        return;
    }
    for (int i = 0; i < _cerArr.count; i++) {
        NSDictionary *file = _cerArr[i];
        NSString *fileType = [(NSDictionary *)file stringForKey:@"fileType"];
        NSString *proposalStatus = file[@"proposalStatus"];
        if ([fileType isEqualToString:@"FILE_TYPE_IDCARD_FRONT"]) {
            [_leftImgV sd_setImageWithURL:[NSURL URLWithString:file[@"ftpFilePath"]] placeholderImage:GET_IMAGE_NAME(@"") options:SDWebImageAllowInvalidSSLCertificates];
            _leftbtn.hidden = YES;
            _leftLbl.hidden = YES;
            //-1,拒绝 0,无文件 1,审批中，2已审批
            _lProposalStatus = proposalStatus;
            _leftImgV.userInteractionEnabled = YES;
            if ([proposalStatus isEqualToString:@"-1"]) {
                _leftStateLbl.text = LocalizedString(@"未通过");
                _leftStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xff4653, 0xff4d2d);
                _leftStateLbl.hidden = NO;
                _leftDelBtn.hidden = NO;
            } else if ([proposalStatus isEqualToString:@"0"]) {
                _leftStateLbl.hidden = YES;
                _leftDelBtn.hidden = YES;
            } else if ([proposalStatus isEqualToString:@"1"]) {
                _leftStateLbl.text = LocalizedString(@"审核中");
                _leftStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xebad62, 0xf7931b);
                _leftStateLbl.hidden = NO;
                _leftDelBtn.hidden = YES;
                _leftImgV.userInteractionEnabled = NO;
            } else if ([proposalStatus isEqualToString:@"2"]) {
                _leftStateLbl.text = LocalizedString(@"已通过");
                _leftStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
                _leftStateLbl.hidden = NO;
                _leftDelBtn.hidden = YES;
            }
        } else if ([fileType isEqualToString:@"FILE_TYPE_IDCARD_BACK"]) {
            [_rightImgV sd_setImageWithURL:[NSURL URLWithString:file[@"ftpFilePath"]] placeholderImage:GET_IMAGE_NAME(@"") options:SDWebImageAllowInvalidSSLCertificates];
            _rightbtn.hidden = YES;
            _rightLbl.hidden = YES;
            _rProposalStatus = proposalStatus;
            _rightImgV.userInteractionEnabled = YES;
            if ([proposalStatus isEqualToString:@"-1"]) {
                _rightStateLbl.text = LocalizedString(@"未通过");
                _rightStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xff4653, 0xff4d2d);
                _rightStateLbl.hidden = NO;
                _rightDelBtn.hidden = NO;
            } else if ([proposalStatus isEqualToString:@"0"]) {
                _rightStateLbl.hidden = YES;
                _rightDelBtn.hidden = YES;
            } else if ([proposalStatus isEqualToString:@"1"]) {
                _rightStateLbl.text = LocalizedString(@"审核中");
                _rightStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xebad62, 0xf7931b);
                _rightStateLbl.hidden = NO;
                _rightDelBtn.hidden = YES;
                _rightImgV.userInteractionEnabled = NO;
            } else if ([proposalStatus isEqualToString:@"2"]) {
                _rightStateLbl.text = LocalizedString(@"已通过");
                _rightStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
                _rightStateLbl.hidden = NO;
                _rightDelBtn.hidden = YES;
            }
        } else if ([fileType isEqualToString:@"FILE_TYPE_ADDRESS"]) {
            [_addressImgV sd_setImageWithURL:[NSURL URLWithString:file[@"ftpFilePath"]] placeholderImage:GET_IMAGE_NAME(@"") options:SDWebImageAllowInvalidSSLCertificates];
            _addressBtn.hidden = YES;
            _addressLbl.hidden = YES;
            _aProposalStatus = proposalStatus;
            _addressImgV.userInteractionEnabled = YES;
            if ([proposalStatus isEqualToString:@"-1"]) {
                _addressStateLbl.text = LocalizedString(@"未通过");
                _addressStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xff4653, 0xff4d2d);
                _addressStateLbl.hidden = NO;
                _addressDelBtn.hidden = NO;
            } else if ([proposalStatus isEqualToString:@"0"]) {
                _addressStateLbl.hidden = YES;
                _addressDelBtn.hidden = YES;
            } else if ([proposalStatus isEqualToString:@"1"]) {
                _addressStateLbl.text = LocalizedString(@"审核中");
                _addressStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0xebad62, 0xf7931b);
                _addressStateLbl.hidden = NO;
                _addressDelBtn.hidden = YES;
                _addressImgV.userInteractionEnabled = NO;
            } else if ([proposalStatus isEqualToString:@"2"]) {
                _addressStateLbl.text = LocalizedString(@"已通过");
                _addressStateLbl.dk_backgroundColorPicker = DKColorWithRGBs(0x11b873, 0x21ce99);
                _addressStateLbl.hidden = NO;
                _addressDelBtn.hidden = YES;
            }
        }
    }
}

- (void)updateUI:(NSInteger)index image:(UIImage *)image
{
    switch (index) {
        case 0:{
            [_leftImgV setImage:image];
            _leftbtn.hidden = YES;
            _leftLbl.hidden = YES;
            _leftDelBtn.hidden = NO;
        }
            break;
        case 1:{
            [_rightImgV setImage:image];
            _rightbtn.hidden = YES;
            _rightLbl.hidden = YES;
            _rightDelBtn.hidden = NO;
        }
            break;
        case 2:{
            [_addressImgV setImage:image];
            _addressBtn.hidden = YES;
            _addressLbl.hidden = YES;
            _addressDelBtn.hidden = NO;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark - button action

- (void)responseLeftPhoto
{
    if ([_lProposalStatus isEqualToString:@"2"]) {
        [IXZoomScrollView showFrom:_leftImgV image:_leftImgV.image];
        return;
    }
    _iconIndex = 0;
    [self responseToPhoto];
}

- (void)responseRightPhoto
{
    if ([_rProposalStatus isEqualToString:@"2"]) {
        [IXZoomScrollView showFrom:_rightImgV image:_rightImgV.image];
        return;
    }
    _iconIndex = 1;
    [self responseToPhoto];
}

- (void)responseLeftDeletePhoto
{
    _delIndex = 0;
    [_leftImgV setImage:nil];
    _leftbtn.hidden = NO;
    _leftLbl.hidden = NO;
    _leftDelBtn.hidden = YES;
    _leftStateLbl.hidden = YES;
}

- (void)responseRightDeletePhoto
{
    _delIndex = 0;
    [_rightImgV setImage:nil];
    _rightbtn.hidden = NO;
    _rightLbl.hidden = NO;
    _rightDelBtn.hidden = YES;
    _rightStateLbl.hidden = YES;
}

- (void)responseAddressPhoto
{
    if ([_aProposalStatus isEqualToString:@"2"]) {
        [IXZoomScrollView showFrom:_addressImgV image:_addressImgV.image];
        return;
    }
    _iconIndex = 2;
    [self responseToPhoto];
}

- (void)responseAddressDeletePhoto
{
    _delIndex = 2;
    [_addressImgV setImage:nil];
    _addressBtn.hidden = NO;
    _addressLbl.hidden = NO;
    _addressDelBtn.hidden = YES;
    _addressStateLbl.hidden = YES;
}

- (void)responseToPhoto
{
    [self.chooseCata show];
}

- (NSMutableArray *)titleItemArr {
    if (!_titleItemArr) {
        _titleItemArr = [[NSMutableArray alloc] initWithObjects:LocalizedString(@"选择相册"), LocalizedString(@"选择相机"), nil];
    }
    return _titleItemArr;
}

- (IXOpenChooseContentView *)chooseCata
{
    if ( !_chooseCata ) {
        _chooseCata = [[IXOpenChooseContentView alloc] initWithDataSource:self.titleItemArr WithSelectedRow:^(NSInteger row) {
            UIImagePickerController * picker = [[UIImagePickerController alloc] init];
            switch (row) {
                case 0:{
                    if ([IXAppUtil albumEnable]) {
                        [IXUserDefaultM dealStatusBarWithAlbumState:YES];
                        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                        picker.delegate = self;
                        picker.allowsEditing = NO;
                        picker.navigationBar.translucent = NO;
                        [self presentViewController:picker animated:YES completion:nil];
                    }
                }
                    break;
                case 1:{
                    if ([IXAppUtil cameraEnable]) {
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        picker.delegate = self;
                        picker.allowsEditing = NO;
                        [self presentViewController:picker animated:YES completion:nil];
                    }
                }
                    break;
                default:
                    break;
            }
        }];
    }
    return _chooseCata;
}

#pragma mark -
#pragma mark - iamge picker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    CGSize imagesize = image.size;
//    imagesize.height = 120 * 2;
//

 
    switch (_iconIndex) {
        case 0:{
//            imagesize.width = (kScreenWidth/2 - 45)*2;
//            image = [self imageWithImage:image scaledToSize:imagesize];
            [self uploadPhoto:0 image:image];
        }
            break;
        case 1:{
//            imagesize.width = (kScreenWidth/2 - 45)*2;
//            image = [self imageWithImage:image scaledToSize:imagesize];
            [self uploadPhoto:1 image:image];
        }
            break;
        case 2:{
//            imagesize.width = (kScreenWidth - 30);
//            image = [self imageWithImage:image scaledToSize:imagesize];
            [self uploadPhoto:2 image:image];
        }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        //调用摄像头后会导致常亮失效，再此处处理屏幕常亮问题
        [IXUserDefaultM dealScreenNormalLight];
        [IXUserDefaultM dealStatusBarWithAlbumState:NO];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //调用摄像头后会导致常亮失效，再此处处理屏幕常亮问题
    [IXUserDefaultM dealScreenNormalLight];
    [IXUserDefaultM dealStatusBarWithAlbumState:NO];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//对图片尺寸进行压缩--
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)loadCustomerFiles
{
    NSDictionary *paramDic = @{
                               @"customerNumber":@([IXBORequestMgr shareInstance].userInfo.detailInfo.customerNumber)
                               };

    weakself;
    [IXBORequestMgr acc_customerFilesWithParam:paramDic
                                        result:^(BOOL success,
                                                 NSString *errCode,
                                                 NSString *errStr,
                                                 id obj)
    {
        if (success && [obj isKindOfClass:[NSArray class]]) {
            [weakSelf dealWithCerFile:obj];
            [weakSelf refreshUI];
        } else {
            [SVProgressHUD showErrorWithStatus:LocalizedString(@"获取客户文件信息失败")];
        }
    }];
}

- (void)dealWithCerFile:(NSArray *)fileArr
{
    if (!fileArr || fileArr.count == 0) {
        return;
    }
    if (!_cerArr ) {
        _cerArr = [[NSMutableArray alloc]init];
    } else {
        [_cerArr removeAllObjects];
    }
    for (int i = 0; i < fileArr.count; i++) {
        id file = fileArr[i];
        if (file && [file isKindOfClass:[NSDictionary class]]) {
            id fileType = [(NSDictionary *)file stringForKey:@"fileType"];
            if (fileType && [fileType isKindOfClass:[NSString class]]) {
                if ([(NSString *)fileType isEqualToString:@"FILE_TYPE_IDCARD_FRONT"] || [(NSString *)fileType isEqualToString:@"FILE_TYPE_IDCARD_BACK"] || [(NSString *)fileType isEqualToString:@"FILE_TYPE_ADDRESS"]) {
                    id filePath = [(NSDictionary *)file stringForKey:@"filePath"];
                    if (filePath && [filePath isKindOfClass:[NSString class]] && [(NSString *)filePath length] > 0) {
                        [_cerArr addObject:(NSDictionary *)file];
                    }
                }
            }
        }
    }
}

- (void)uploadPhoto:(NSInteger)index image:(UIImage *)image
{
    NSData *imageData;
    imageData = UIImageJPEGRepresentation(image, 0.5);
    
//    if (index == 0) {
//        imageData = UIImageJPEGRepresentation(image,0.01);
//    } else if (index == 1) {
//        imageData = UIImageJPEGRepresentation(image,0.01);
//    } else {
//        imageData = UIImageJPEGRepresentation(image,0.01);
//    }
    
    NSNumber *timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];
    timeStamp = [NSNumber numberWithLong:([timeStamp longValue] + index)];
    NSString *fileName = [NSString stringWithFormat:@"%@.png",[timeStamp stringValue]];

    NSDictionary *param = @{@"imageData":imageData,
                            @"timeStamp":timeStamp,
                            @"fileName":fileName};
    [SVProgressHUD showWithStatus:LocalizedString(@"上传中...")];
    
    [IXBORequestMgr acc_uploadBankPhotoWithParam:param
                                      WithResult:^(BOOL success,
                                                   NSString *errCode,
                                                   NSString *errStr,
                                                   id obj)
    {
        [SVProgressHUD dismiss];
        
        if (success) {
            if (obj && [obj ix_isDictionary]) {
                switch (index) {
                    case 0:{
                        IXSupplementFile *file = [[IXSupplementFile alloc] init];
                        file.filePath = [obj stringForKey:@"fileStorePath"];
                        file.gts2CustomerId = _gts2CustomerId;
                        file.ftpFilePath = [obj stringForKey:@"webFilePath"];
                        file.fileType = @"FILE_TYPE_IDCARD_FRONT";
                        file.fileName = fileName;
                        [self saveFile:file index:index];
                        [self updateUI:index image:image];
                    }
                        break;
                    case 1:{
                        IXSupplementFile *file = [[IXSupplementFile alloc] init];
                        file.filePath = [obj stringForKey:@"fileStorePath"];
                        file.gts2CustomerId = _gts2CustomerId;
                        file.ftpFilePath = [obj stringForKey:@"webFilePath"];
                        file.fileType = @"FILE_TYPE_IDCARD_BACK";
                        file.fileName = fileName;
                        [self saveFile:file index:index];
                        [self updateUI:index image:image];
                    }
                        break;
                    case 2:{
                        IXSupplementFile *file = [[IXSupplementFile alloc] init];
                        file.filePath = [obj stringForKey:@"fileStorePath"];
                        file.gts2CustomerId = _gts2CustomerId;
                        file.ftpFilePath = [obj stringForKey:@"webFilePath"];
                        file.fileType = @"FILE_TYPE_ADDRESS";
                        file.fileName = fileName;
                        [self saveFile:file index:index];
                        [self updateUI:index image:image];
                    }
                        break;
                    default:
                        break;
                }
                return ;
            }
        }
        
        [SVProgressHUD showErrorWithStatus:LocalizedString(@"图片上传失败")];
    }];

}

- (void)submitCerFileInfo
{
    _fileModel = [[IXSupplementFileM alloc] init];
    NSMutableArray *tmpArr = [NSMutableArray array];
    
    for (int i = 0; i < _fileArr.count; i++) {
        NSDictionary *dic = _fileArr[i];
        NSArray *valueArr = [dic allValues];
        [tmpArr addObject:valueArr[0]];
    }
    
    _fileModel.files = [tmpArr copy];
    NSDictionary *param = @{
                            @"gts2CustomerId" : _gts2CustomerId,
                            NSStringFromClass([IXSupplementFileM class]):_fileModel
                            };
    [IXBORequestMgr acc_uploadAttachWithParam:param
                                  WithResult:^(BOOL success,
                                               NSString *errCode,
                                               NSString *errStr, id obj)
    {
        if (success) {
            [SVProgressHUD showSuccessWithStatus:LocalizedString(@"上传证明文件成功")];
            [IXBORequestMgr refreshUserInfo:nil];
            [IXBORequestMgr shareInstance].uploadedCustomerFile = YES;
            [self onGoback];
        } else {
            if (errStr.length) {
                [SVProgressHUD showErrorWithStatus:errStr];
            } else {
                [SVProgressHUD showErrorWithStatus:LocalizedString(@"提交证明文件失败")];
            }
        }
    }];
}

- (void)saveFile:(IXSupplementFile *)file index:(NSInteger)index
{
    BOOL flag = NO;
    for (int i = 0; i < _fileArr.count; i++) {
        NSMutableDictionary *dic = [_fileArr[i] mutableCopy];
        if ([[dic allKeys] containsObject:@(index)]) {
            flag = YES;
            [dic setObject:file forKey:@(index)];
            [_fileArr replaceObjectAtIndex:i withObject:dic];
            break;
        }
    }
    if (!flag) {
        NSDictionary *dic = @{
                              @(index):file
                              };
        [_fileArr addObject:dic];
    }
}


@end
