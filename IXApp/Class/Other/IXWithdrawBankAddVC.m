//
//  IXWithdrawBankAddVC.m
//  IXApp
//
//  Created by Larry on 2018/7/3.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXWithdrawBankAddVC.h"
#import "IXCommonTextView.h"
#import "UIKit+Block.h"
#import "IXWithDrawDocVC.h"
#import "IXUserDefaultM.h"
#import "IXCountryListVC.h"
#import "IXBORequestMgr+Asset.h"

@interface IXWithdrawBankAddVC ()<UIScrollViewDelegate,UITextViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIView *sectionView;
@property (nonatomic, strong)IXCountryM    *countryInfo;

@property(nonatomic,strong)IXCommonTextView *item_0;
@property(nonatomic,strong)IXCommonTextView *item_1;
@property(nonatomic,strong)IXCommonTextView *item_2;
@property(nonatomic,strong)IXCommonTextView *item_3;
@property(nonatomic,strong)IXCommonTextView *item_4;
@property(nonatomic,strong)IXCommonTextView *item_5;
@property(nonatomic,strong)IXCommonTextView *item_6;
@property(nonatomic,strong)IXCommonTextView *item_7;

@property(nonatomic,strong)NSDictionary *bank;
@property(nonatomic,strong)UIButton * nextBtn;
@end

@implementation IXWithdrawBankAddVC

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        _scrollView.delegate = self;
        [self.view addSubview:_scrollView];
        [_scrollView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
            make.width.equalTo(kScreenWidth);
        }];
    }
    return _scrollView;
}

- (UIView *)sectionView{
    if (!_sectionView) {
        _sectionView = [UIView new];
        _sectionView.dk_backgroundColorPicker = DKViewColor;
        [self.scrollView addSubview:_sectionView];
        
        [_sectionView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.width.equalTo(kScreenWidth);
            make.height.equalTo(35);
        }];
        
        UILabel *lb_title = [UILabel new];
        lb_title.font = ROBOT_FONT(11);
        lb_title.dk_textColorPicker = DKCellContentColor;
        lb_title.text = @"Bank Card Relate Information Bank Address";
        [_sectionView addSubview:lb_title];
        [lb_title makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.centerY.equalTo(0);
        }];
        
    }
    return _sectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self gotoNext:@"Next Step"];
    
    self.title = @"Withdrawl Information";
    [self addBackItem];
    [self showProgressWithAimStep:2 totalStep:5 originY:0];
    self.automaticallyAdjustsScrollViewInsets= NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;

    weakself;
    IXCommonTextView *item_0 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Account Name" rPlaceHoler:@"Enter account owner name"];
    item_0.tv_content.delegate = self;
    [item_0 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(5);
    }];
    
    IXCommonTextView *item_1 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Passbook(Savings) account number" rPlaceHoler:@"Enter account numnber"];
    item_1.tv_content.keyboardType = UIKeyboardTypeNumberPad;
    item_1.tv_content.delegate = self;
    [item_1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_0.bottom).offset(0);
    }];
    
    IXCommonTextView *item_2 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Card Number" rPlaceHoler:@"Enter bank card number"];
    item_2.tv_content.keyboardType = UIKeyboardTypeNumberPad;
    item_2.tv_content.delegate = self;
    [item_2 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_1.bottom).offset(0);
    }];
    
    IXCommonTextView *item_3 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Bank name(English)" rPlaceHoler:@"Enter bank name"];
    item_3.tv_content.delegate = self;
    [item_3 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_2.bottom).offset(0);
    }];
    
    IXCommonTextView *item_4 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Bank account currency" rPlaceHoler:@"Enter Bank account currency"];
    item_4.tv_content.delegate = self;
    [item_4 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_3.bottom).offset(0);
    }];
    
    IXCommonTextView *item_5 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"SWIFT/IBAN/ABA/Sort code" rPlaceHoler:@"Enter the code"];
    item_5.tv_content.delegate = self;
    [item_5 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_4.bottom).offset(0);
    }];
    
    [self.sectionView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(item_5.bottom);
    }];
    
    IXCommonTextView *item_6 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Country" rPlaceHoler:nil];
    item_6.tv_content.delegate = self;
    [item_6 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sectionView.bottom).offset(0);
    }];
    [item_6 addEnteranceArrow:^{
        [weakSelf gotoNext:@"选择国家"];
    }];
    _item_6 = item_6;
    
    
    IXCommonTextView *item_7 = [IXCommonTextView makeViewInSuperView:self.scrollView lText:@"Branch" rPlaceHoler:@"Fill in the branch"];
    item_7.tv_content.delegate = self;
    [item_7 makeConstraints:^(MASConstraintMaker *make) {
         make.top.equalTo(item_6.bottom);
    }];
    
    _item_7 = item_7;
    
//    IXCommonTextView *item_7 = [IXCommonTextView makeViewUpDownInSuperView:self.scrollView uText:@"Branch" dPlaceHoler:nil];
//    item_7.tv_content.delegate = self;
//    [item_7  makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(item_6.bottom);
//    }];
    
    _item_0 = item_0;
    _item_1 = item_1;
    _item_2 = item_2;
    _item_3 = item_3;
    _item_4 = item_4;
    _item_5 = item_5;
    _item_6 = item_6;
    _item_7 = item_7;

    UIImage *image = GET_IMAGE_NAME(@"regist_btn_enable");
    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    UIImage * dImg = GET_IMAGE_NAME(@"regist_btn_enable_D");
    dImg = [dImg stretchableImageWithLeftCapWidth:dImg.size.width/2 topCapHeight:dImg.size.height/2];
    
    UIButton * nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.titleLabel.font = PF_REGU(15);
    [nextBtn dk_setBackgroundImage:DKImageWithImgs(image, dImg) forState:UIControlStateNormal];
    [nextBtn setTitle:@"Next Step" forState:UIControlStateNormal];
    [self.scrollView addSubview:nextBtn];
    
    [nextBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.width.equalTo(kScreenWidth - 30);
        make.top.equalTo(item_7.bottom).offset(30);
        make.height.equalTo(44);
        make.bottom.equalTo(-30);
    }];
    
    [nextBtn block_touchUpInside:^(UIButton *aButton) {
        [weakSelf gotoNext:@"Next Step"];
    }];
    _nextBtn = nextBtn;
    
    [self.view block_whenTapped:^(UIView *aView) {
        [self.view endEditing:YES];
    }];
    
    [self registKeyboard];
    

    [self initEditInfo];
}

- (void)initEditInfo{
    NSDictionary *info = [IXBORequestMgr shareInstance].withDraw_bank;
    if (info && info[@"bankAccountNumber"]) {
        _item_0.tv_content.text = info[@"bankAccountName"];   // 持卡人姓名
        _item_1.tv_content.text = info[@"bankAccountNumber"]; // 账户号码
        _item_2.tv_content.text = info[@"bankCardNumber"];// 银行卡号
        _item_3.tv_content.text = info[@"bank"];// 银行类型
        _item_4.tv_content.text = info[@"bankCurrency"];// 币种
        _item_5.tv_content.text = info[@"internationalRemittanceCode"];
        _item_6.tv_content.text = info[@"bankCountry"]; // 银行地址：国家
        _item_7.tv_content.text = info[@"bankBranch"];// 分行地址
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return [self isInputRule:text];
}

- (BOOL)isInputRule:(NSString *)str{
    if ([str isEqualToString:@""] || [str isEqualToString:@" "]) {
        // 退格
        return YES;
    }
    // 小写:a-z  大写:A-Z   汉字: \u4E00-\u9FA5   数字: \u0030-\u0039
    NSString *pattern = @"[a-zA-Z\u4E00-\u9FA5\\u0030-\\u0039]";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:str];
    return isMatch;
}

// 判断是否是Emoji
- (BOOL)hasEmoji:(NSString*)str{
    NSString *pattern = @"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:str];
    return isMatch;
}

// 字符限制
//[textField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged]; //防止中文联想
#define kMaxLength 20
-(NSString *)getSubString:(NSString*)string
{
    NSStringEncoding encoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSData* data = [string dataUsingEncoding:encoding];
    NSInteger length = [data length];
    if (length > kMaxLength) {
        NSData *data1 = [data subdataWithRange:NSMakeRange(0, kMaxLength)];
        NSString *content = [[NSString alloc] initWithData:data1 encoding:encoding];
        if (!content || content.length == 0) {
            data1 = [data subdataWithRange:NSMakeRange(0, kMaxLength - 1)];
            content = [[NSString alloc] initWithData:data1 encoding:encoding];
        }
        return content;
    }
    return nil;
}


#pragma mark - Keyboard
- (void)registKeyboard{
    //监听键盘出现和消失
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘出现
-(void)keyboardWillShow:(NSNotification *)note
{
    CGRect keyBoardRect= [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyBoardRect.size.height, 0);
}
#pragma mark 键盘消失
-(void)keyboardWillHide:(NSNotification *)note
{
    self.scrollView.contentInset = UIEdgeInsetsZero;
    [self checkInput];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)gotoNext:(NSString *)title{
    if ([title isEqualToString:@"Next Step"]) {

        IXWithDrawDocVC *vc = [IXWithDrawDocVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"选择国家"]) {
        weakself;
        IXCountryListVC *vc = [[IXCountryListVC alloc] initWithSelectedInfo:^(IXCountryM *country) {
            [weakSelf setCountry:country];
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@""]) {
        
    }
}

- (void)checkInput{
    if (_item_0.tv_content.text.length &&
        _item_1.tv_content.text.length &&
        _item_2.tv_content.text.length &&
        _item_3.tv_content.text.length &&
        _item_4.tv_content.text.length &&
        _item_5.tv_content.text.length &&
        _item_6.tv_content.text.length &&
        _item_7.tv_content.text.length
                                        ) {
        _nextBtn.enabled = YES;
        
        __block NSString *uuid = @"";
        if ([IXBORequestMgr shareInstance].withDraw_bank[@"id"]) {
            if ([[IXBORequestMgr shareInstance].withDraw_bank[@"id"] isKindOfClass:[NSString class]]) {
                uuid = [IXBORequestMgr shareInstance].withDraw_bank[@"id"];
            }else{
                uuid = [[IXBORequestMgr shareInstance].withDraw_bank[@"id"] stringValue];
            }
        }
        // 第一张卡没有id
        if (!uuid.length) {
//            NSInteger newBankOrder = [IXBORequestMgr shareInstance].withDraw_bank.count;
//            uuid = [[IXBORequestMgr shareInstance].withdraw_bankFileArr[newBankOrder][@"id"] stringValue];
            [[IXBORequestMgr shareInstance].withdraw_bankArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj[@"bankAccountNumber"] length] == 0) {
                    uuid = [obj[@"id"] stringValue];
                    *stop = YES;
                }
            }];
        }
        NSString *bankOrder = [IXBORequestMgr shareInstance].withDraw_bank[@"bankOrder"];
        if (!bankOrder) {
            bankOrder = @"1";
        }
        NSDictionary *bank = @{ @"id":uuid, // 列编号
                                @"bankAccountName":_item_0.tv_content.text, // 持卡人姓名
                                @"bankAccountNumber":_item_1.tv_content.text, // 账户号码
                                @"bankCardNumber":_item_2.tv_content.text, // 银行卡号
                                @"bank":_item_3.tv_content.text, // 银行类型
                                @"bankCurrency":_item_4.tv_content.text, // 币种
                                @"internationalRemittanceCode":_item_5.tv_content.text,
                                @"bankCountry":_item_6.tv_content.text, // 银行地址：国家
                                @"bankAddress":@"",// _item_7.tv_content.text,  // 银行地址
                                @"bankProvince":@"", // 银行地址：省 0
                                @"bankBranch":_item_7.tv_content.text, // 银行分行名称 0
                                @"bankAccountType":@"", // 银行账户类型 0
                                @"bankOther":@"", // 其他银行 0
                                @"bankOrder":bankOrder
                                };
        [IXBORequestMgr filterWithDrawBankFile:[IXBORequestMgr shareInstance].withDraw_bank];
        [IXBORequestMgr shareInstance].withDraw_bank = bank;
    }else{
        _nextBtn.enabled = NO;
    }
}

- (void)setCountry:(IXCountryM *)country
{
    if (!self.countryInfo) {
        self.countryInfo = [[IXCountryM alloc] init];
    }
    //默认中国
    if (!country) {
        NSString *nationalCode = [IXUserDefaultM getCode];
        IXCountryM *countryM = [IXUserDefaultM nationalityByCode:nationalCode];
        self.countryInfo.nameCN = countryM.nameCN;
        self.countryInfo.nameEN = countryM.nameEN;
        self.countryInfo.nameTW = countryM.nameTW;
        self.countryInfo.countryCode = countryM.countryCode;
        [IXUserDefaultM saveCountryCode:countryM.nationalCode];
        
    } else {
        self.countryInfo.nameCN = country.nameCN;
        self.countryInfo.nameEN = country.nameEN;
        self.countryInfo.nameTW = country.nameTW;
        self.countryInfo.countryCode = country.countryCode;
        [IXUserDefaultM saveCountryCode:country.nationalCode];
    }
    _item_6.tv_content.text = self.countryInfo.nameEN;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self checkInput];
}


@end
