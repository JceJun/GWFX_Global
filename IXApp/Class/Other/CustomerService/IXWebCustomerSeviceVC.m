//
//  IXWebCustomerSeviceVC.m
//  IXApp
//
//  Created by Larry on 2018/5/8.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXWebCustomerSeviceVC.h"
#import "IXAppUtil.h"
#import "IXCpyConfig.h"
#import <UMMobClick/MobClick.h>
#import <WebKit/WebKit.h>
#import "ChatTableViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

//WKUIDelegate,,JSHelperDelegate,WKScriptMessageHandler
@interface IXWebCustomerSeviceVC ()<WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler>
@property (nonatomic, strong) WKWebView  *webView;
@property(nonatomic,strong)JSContext *context;
@property (nonatomic, strong) UIProgressView    * progressV;
@end

@implementation IXWebCustomerSeviceVC

- (void)dealloc{
    [_webView removeObserver:self forKeyPath:@"title"];
    [_webView removeObserver:self forKeyPath:@"loading"];
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
}


- (void)cleanCookies
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 9.0) {
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,
                                                                     NSUserDomainMask,
                                                                     YES)
                                 objectAtIndex:0];
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        NSError *errors = nil;
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
    }else{
        WKWebsiteDataStore *dateStore = [WKWebsiteDataStore defaultDataStore];
        [dateStore fetchDataRecordsOfTypes:[WKWebsiteDataStore allWebsiteDataTypes] completionHandler:^(NSArray<WKWebsiteDataRecord *> * __nonnull records) {
            for (WKWebsiteDataRecord *record  in records)
            {
                [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:record.dataTypes
                                                          forDataRecords:@[record]
                                                       completionHandler:^{}];
            }
        }];
    }
}


- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"IxJsHook"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"ixMarketPage"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"IxJsHook"];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"ixMarketPage"];
}


- (WKWebView *)webView{
    if (!_webView) {
        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
        WKPreferences *preferences = [WKPreferences new];
        preferences.javaScriptCanOpenWindowsAutomatically = YES;
        preferences.minimumFontSize = 40.0;
        configuration.preferences = preferences;

        _webView = [[WKWebView alloc] initWithFrame:self.view.frame];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        [self.view addSubview:_webView];

        [_webView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
        [_webView addObserver:self forKeyPath:@"title"
                          options:NSKeyValueObservingOptionNew
                          context:nil];
        [_webView addObserver:self forKeyPath:@"loading"
                          options:NSKeyValueObservingOptionNew
                          context:nil];
        [_webView addObserver:self
                       forKeyPath:@"estimatedProgress"
                          options:NSKeyValueObservingOptionNew
                          context:nil];
        
    }
    return _webView;
}

- (UIProgressView *)progressV
{
    if (!_progressV) {
        _progressV = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressV.dk_trackTintColorPicker = DKColorWithRGBs(0xE2E9F1, 0x303b4d);
        _progressV.dk_progressTintColorPicker = DKColorWithRGBs(0x11B873, 0x21ce99);
        _progressV.frame = CGRectMake(0, 0, kScreenWidth, 5);
        [self.webView addSubview:_progressV];
    }
    
    return _progressV;
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat p = [change[@"new"] floatValue];
        [_progressV setProgress:p animated:YES];
        if (p == 1.0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [_progressV setProgress:0 animated:NO];
                _progressV.hidden = YES;
            });
        }else{
            _progressV.hidden = NO;
        }
    }
    else if ([keyPath isEqualToString:@"title"]){
        NSString    * t = change[@"new"];
        self.title = t;
    }
}

-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
//    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeClear];
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
//    [SVProgressHUD dismiss];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (UMAppKey.length) {
        [MobClick event:UM_customerService];
    }
    
    self.view.dk_backgroundColorPicker = DKTableColor;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    self.title = LocalizedString(@"在线客服");
    
    
    /*
     
    http://192.168.35.94:8011/#/home?flag=fx&ac=86111111&platform=android&app=app
    ac: 真实账号: account, 游客：visitor_唯一标识(app: deviceId（deviceId 去除掉:）, web:cookierid)
    flag: pm/fx/hx/cf
    platform：android/ios/gts2client
    
    以上是一个例子：
    
    改天我帮你们配置好以下参数：
    flag：ix_cn (简体)/ix_tw(繁体)/ix_en(英文）
                                    platform：android/ios
                                    ac:你们的账号
*/
    NSString *ip =  @"https://aics.gwfx.hk";
    if ([CompanyName containsString:@"UAT"]) {
        ip = @"http://192.168.35.94:8011";
    }
    NSString *account_id = [@([IXUserInfoMgr shareInstance].userLogInfo.account.id_p) stringValue];
    if (!account_id.length) {
        account_id = [@"visitor_" stringByAppendingString:[IXDataProcessTools dealWithWhiffletree:[IXAppUtil idfa]]];
    }
    NSString *flag = @"fx_en";
    NSString *platform = @"ios";
    NSString *app = @"app";
    
    NSString *url = [NSString stringWithFormat:@"%@/en/#/home?flag=%@&ac=%@&platform=%@&app=%@",ip,flag,account_id,platform,app];
//    url = @"http://testm.gwfxglobal.com/app/en/index.html";  // UAT
//    url = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    
    [self progressV];
    
}

- (void)onGoback{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
//    message.body  --  Allowed types are NSNumber, NSString, NSDate, NSArray,NSDictionary, and NSNull.
    if ([message.name isEqualToString:@"ixMarketPage"]) {
        [self appChat];
    }else if ([message.name isEqualToString:@"IxJsHook"]){
        [self appChat];
    }
}

- (void)appChat{
    // Native客服中心
    ChatTableViewController *vc = [[ChatTableViewController alloc] initWithPid:ServicePid
                                                                           key:ServiceKey
                                                                           url:kServiceUrl];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
