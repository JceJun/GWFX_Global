//
//  IXEvaluationVC.h
//  IXApp
//
//  Created by Evn on 2018/1/29.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDataBaseVC.h"
@class IXOpenAcntDataModel;
@class IXEvaluationM;

@interface IXEvaluationVC : IXDataBaseVC
@property (nonatomic, strong)IXOpenAcntDataModel *acntModel;
@property (nonatomic, strong)IXEvaluationM *evaluationM;
@end
