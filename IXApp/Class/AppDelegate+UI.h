//
//  AppDelegate+UI.h
//  IXApp
//
//  Created by Magee on 2016/12/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "AppDelegate.h"
#import "IxProtoUser.pbobjc.h"

@interface AppDelegate (UI)

- (void)dealWithReLogin;    //处理重新登录
- (void)showUI;            //决断显示
+ (void)showPreview;        //预览
+ (void)showLogin;          //登陆／注册
+ (void)showLoginMain;       //登陆
+ (void)showRegist;         //单纯注册
+ (void)showRoot;           //主页面
+ (void)showVisiterPage;    //游客

+ (void)startAutoLoginUserName:(NSString *)userName
                      passWord:(NSString *)passWord
                     phoneCode:(NSString *)phoneCode;

+ (item_data_version *)dataVersion;
+ (void)saveUserBehavoir;

// 显示弹窗广告位
+ (void)showAdContainer:(NSString *)imgUrl link:(NSString *)url;
// 隐藏弹窗广告位
+ (void)hideAdContainer;

// 注册成功后弹窗提示
+ (void)showLoginAfterRigistAlert;

// 跳转通用网页
+ (void)gotoCommonWeb:(NSString *)url;

// 任意页面跳转Native
+ (void)gotoNativeVC:(NSString *)classStr isPresent:(BOOL)isPresent param:(id)param;
@end
