//
//  IXCrc16.h
//  IXApp
//
//  Created by bob on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXCrc16 : NSObject

+ (uint16_t)crc16:(const char *)buf length:(int)len;

@end
