//
//  FXNormalCell.h
//  zBlank
//
//  Created by Larry on 2017/7/27.
//  Copyright © 2017年 Larry. All rights reserved.
//

#define aDarkBlueColor COLOR_HEX(@"0x4c6072")
#define aRedColor      COLOR_HEX(@"0xff4653")
#define aGreenColor    COLOR_HEX(@"0x11b873")
#define aGrayColor     COLOR_HEX(@"0x99abba")
#define aLineColor     COLOR_HEX(@"e2eaf2")

#import <UIKit/UIKit.h>
#import "IXLCellItem.h"

//#import "UIButton+WebCache.h"
//#import "NSString+Formatter.h"
//#import "FXAppUtil.h"
//#import "NSString+FormatterPrice.h"

@interface IXNormalCell : UITableViewCell
@property (nonatomic, strong)UIImageView *bgView;  // cell背景图
@property (nonatomic, strong)UIView *lineView;     // cell分割线
@property (nonatomic, strong)IXLCellItem *item;     // cell分割线

+ (instancetype)cellWithTableView:(UITableView *)tableView;


@end
