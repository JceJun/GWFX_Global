//
//  FXNormalHeader.h
//  zBlank
//
//  Created by Larry on 2017/7/27.
//  Copyright © 2017年 Larry. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "IXCellFooterItem.h"
@interface IXNormalFooter : UIView
@property (nonatomic, strong)UIImageView *bgView;  // view背景图
@property (nonatomic, strong)UIView *topLine;     // view分割线
@property (nonatomic, strong)UIView *bottomLine;     // view分割线
@property(nonatomic,strong)IXCellFooterItem *item;
// 子类继承需调用 1
//+ (instancetype)footerWithItem:(IXCellFooterItem *)item;
// 子类继承需调用 2
- (void)addSubViews;
// 子类继承需调用 3
- (void)addConstraints;
// 子类事件传递
- (void)sendActionToUpperResponder:(NSString *)key object:(id)obj;

@end
