//
//  FXNormalTableView.h
//  交易记录
//
//  Created by Larry on 2017/7/24.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXLGroup.h"
#import "IXLCellItem.h"
#import "IXCellHeaderItem.h"

//#import "FXRoomLG.h"

//#import "IXTouchTableV.h"

typedef void(^refreashData)(void);

@interface IXNormalTableView : UITableView

/* 继承时，使用此方法初始化 FXNormalTableView
 * tableStyle:
 * Plain - Header悬停 SectionHeader悬停
 * Gruop - Header不悬停 SectionHeader不悬停  section间会多间距
           去掉间距:self.table.sectionFooterHeight = 0; self.table.sectionHeaderHeight = 0;
 */


-(instancetype)initWithFrame:(CGRect)frame
                  tableStyle:(UITableViewStyle)tableStyle
            estimatedHeaderH:(BOOL)needHeader
            estimatedFooterH:(BOOL)needFooter;

@property(nonatomic,strong)NSMutableArray *items;
@property(nonatomic,strong)NSMutableArray *groups;
@property(nonatomic,strong)UIView *sectionHeader;
@property (nonatomic, copy) refreashData loadMoreBlock;
@property (nonatomic, copy) refreashData refreashBlock;

@property(nonatomic,assign)BOOL isScrolling;

// 回调
@property(nonatomic,strong)void(^viewAction)(NSString *key, id obj);
- (void)sendActionToUpperResponder:(NSString *)key object:(id)obj;

//无数据
- (void)showNodataView:(NSString *)title image:(NSString *)imgName;

- (void)reloadTableView;

- (void)beginRefreshing;
- (void)addRefreash;
- (void)loadComplete;
- (void)endRefreash;

@end
