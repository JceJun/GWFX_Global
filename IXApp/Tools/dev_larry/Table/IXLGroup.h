//
//  FXLGroup.h
//  FXAPP
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXCellHeaderItem.h"
#import "IXCellFooterItem.h"

@interface IXLGroup : NSObject
@property(nonatomic,strong)IXCellHeaderItem *headerItem;   // 头部项
@property(nonatomic,assign,getter=isOpen)BOOL open;         // 中间开关
@property(nonatomic,strong)NSArray *cellItems;           // 中间项
@property(nonatomic,strong)IXCellFooterItem *footerItem;   // 底部项


+ (instancetype)groupWithCellItems:(NSArray *)cellItems
                        headerItem:(IXCellHeaderItem *)headerItem
                        footerItem:(IXCellFooterItem *)footerItem;
@end
