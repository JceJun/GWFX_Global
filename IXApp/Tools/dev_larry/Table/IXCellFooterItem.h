//
//  FXCellFooterItem.h
//  FXAPP
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXCellFooterItem : NSObject
@property(nonatomic,strong)NSString *title;

@property(nonatomic,strong)NSDictionary *dataDic;

@property(nonatomic,assign)CGFloat height; // 记录高度

@property(nonatomic,strong)void(^footerAction)(NSString *key , id obj); // 点击Footer主事件


+ (instancetype)footerWithTitle:(NSString *)title
                        dataDic:(NSDictionary *)dataDic
                   footerAction:(void(^)(NSString *key , id obj))footerAction;
@end
