//
//  FXNormalHeader.h
//  zBlank
//
//  Created by Larry on 2017/7/27.
//  Copyright © 2017年 Larry. All rights reserved.
//

#define aDarkBlueColor HexRGB(0x4c6072)
#define aRedColor      HexRGB(0xff4653)
#define aGreenColor    HexRGB(0x11b873)
#define aGrayColor     HexRGB(0x99abba)
#define aLineColor     HexRGB(0xe2eaf2)

#import <UIKit/UIKit.h>
#import "IXCellHeaderItem.h"
@interface IXNormalHeader : UIView
@property (nonatomic, strong)UIImageView *bgView;  // view背景图
@property (nonatomic, strong)UIView *topLine;     // view分割线
@property (nonatomic, strong)UIView *bottomLine;     // view分割线
@property(nonatomic,strong)IXCellHeaderItem *item;
// 子类继承需调用 1
//+ (instancetype)headerWithItem:(IXCellHeaderItem *)item;
// 子类继承需调用 2
- (void)addSubViews;
// 子类继承需调用 3
- (void)addConstraints;
// 子类事件传递
- (void)sendActionToUpperResponder:(NSString *)key object:(id)obj;

@end
