//
//  FXNormalCell.m
//  zBlank
//
//  Created by Larry on 2017/7/27.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "IXNormalCell.h"
#import "Masonry.h"



@interface IXNormalCell()

@end


@implementation IXNormalCell
/* 继承Copy  IXNormalCell 改名 为子类Cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"IXNormalCell";
    IXNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[[self class] alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = CellBackgroundColor;
    }
    return cell;
}
*/
// 继承Copy
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self initSubviews];
    }
    return self;
}

// 子类继承不能重名，父类否则不走此方法
- (void)initSubviews{
    _bgView = [UIImageView new];
    _bgView.dk_backgroundColorPicker = DKNavBarColor;
    _bgView.userInteractionEnabled = YES;
//    UIImage * wImg = [[UIImage imageNamed:@"positionList_closeby_back"] resizableImageWithCapInsets:UIEdgeInsetsMake( 5, 5, 5, 5)];
//    UIImage * dImg = [[UIImage imageNamed:@"positionList_closeby_back_D"] resizableImageWithCapInsets:UIEdgeInsetsMake( 5, 5, 5, 5)];
//    _bgView.image = wImg;
    [self.contentView addSubview:_bgView];
    
    _lineView = [UIView new];
    _lineView.dk_backgroundColorPicker = DKLineColor2;
    [_bgView addSubview:_lineView];
    
    [self initConstraint];
}

// 子类继承不能重名，父类否则不走此方法
- (void)initConstraint{
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
//        make.height.equalTo(100);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(_bgView);
        make.height.equalTo(@0.5);
        make.bottom.equalTo(@0);
        
//        make.size.mas_equalTo(CGSizeMake(kScreenWidth,PXFLOAT(1)));
//        make.top.greaterThanOrEqualTo(_lb_direction.mas_bottom).mas_offset(15);
//        make.top.greaterThanOrEqualTo(_lb_currentPrice.mas_bottom).mas_offset(15);
    }];
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    /* 自定义左滑按钮
    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UITableViewCellDeleteConfirmationView")]) {
            [subview.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isKindOfClass:[UIButton class]]) {
                    UIButton *deleteBtn = obj;
                    // deleteBtn 就是那个删除按钮 在这里自定义按钮的字体、背景色、添加图片等
                    deleteBtn.backgroundColor = aGreenColor;
                    deleteBtn.layer.cornerRadius = 1;
                    deleteBtn.layer.masksToBounds = YES;
                    deleteBtn.titleLabel.font = PF_MEDI(13);
                    CGRect f = deleteBtn.frame;
                    f.origin.y = 10;
                    f.size.height = 115;
                    f.size.width = 56;
                    deleteBtn.frame = f;
                    
                    deleteBtn.superview.backgroundColor = [UIColor clearColor];
                }
            }];
            
            break;
        }
    }
    */
}


// 子类需要调用 [super setItem:item]
-(void)setItem:(IXLCellItem *)item{
    _item = item;
    //    NSDictionary *dic = item.dataDic;
//    _lb_name.text = [@"股票名_" stringByAppendingFormat:@"%d",arc4random_uniform(999)];
//
//    if (RANDOM(0,1)) {
//        _lb_direction.text = @"买";
//        _lb_direction.textColor = HexRGB(0xffffff);
//        _lb_direction.backgroundColor = HexRGB(0x11b873);
//        
//        //        self.dirLbl.dk_textColorPicker = DKRGBs(0xffffff, 0x262f3e);
//        //        self.dirLbl.dk_backgroundColorPicker = DKRGBs(0x11b873, 0x21ce99);
//    } else {
//        //        self.dirLbl.text = LocalizedString(@"卖.");
//        //        self.dirLbl.dk_textColorPicker = DKRGBs(0xffffff, 0x262f3e);
//        //        self.dirLbl.dk_backgroundColorPicker = DKRGBs(0xff4653, 0xff4d2d);
//        _lb_direction.text = @"卖";
//        _lb_direction.textColor = HexRGB(0xffffff);
//        _lb_direction.backgroundColor = HexRGB(0xff4653);
//    }
//    
//    if (RANDOM(0,1)) {
//        _lb_resultNum.text = [NSString stringWithFormat:@"-%d",RANDOM(1000, 100000)];
//        _lb_resultNum.textColor = aGreenColor;
//    }else{
//        _lb_resultNum.textColor = aRedColor;
//        _lb_resultNum.text = [NSString stringWithFormat:@"%d",RANDOM(1000, 100000)];
//    }
//    _lb_openPriceNum.text = [NSString stringWithFormat:@"%d",RANDOM(1000, 100000)];
}

@end
