//
//  FXNormalHeader.m
//  zBlank
//
//  Created by Larry on 2017/7/27.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "IXNormalFooter.h"
#import "Masonry.h"
@implementation IXNormalFooter
/* 继承Copy
 + (instancetype)footerWithItem:(FXCellFooterItem *)item{
     FXNormalFooter *header = [FXNormalFooter new];
     header.backgroundColor = CellBackgroundColor;
     [header subViews];
     header.item = item;
     return header;
 }
 */

// 子类继承需调用
- (void)addSubViews{
    _bgView = [UIImageView new];
    _bgView.userInteractionEnabled = YES;
    //    _bgView.backgroundColor = [UIColor cyanColor];
    //    UIImage * wImg = [[UIImage imageNamed:@"positionList_closeby_back"] resizableImageWithCapInsets:UIEdgeInsetsMake( 5, 5, 5, 5)];
    [self addSubview:_bgView];
    
}

// 子类继承需调用
- (void)addConstraints{
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    
}


-(void)setItem:(IXCellFooterItem *)item{
    _item = item;
    //    NSDictionary *dataDic = item.dataDic;
    
}

#pragma mark - 回调
- (void)sendActionToUpperResponder:(NSString *)key object:(id)obj{
    if (self.item.footerAction) {
        self.item.footerAction(key, obj);
    }
}

/*
 NSDictionary *footerDic = @{};
 FXCellFooterItem *footer = [FXCellFooterItem footerWithTitle:@"" dataDic:footerDic  footerAction:nil];
 
 FXLGroup *group = [FXLGroup groupWithCellItems:itemArr
 headerItem:header
 footerItem:footer];
 */

@end
