//
//  DictionaryItem.m
//  CJPlayGround
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "IXLCellItem.h"

@implementation IXLCellItem
+ (instancetype)itemWithTitle:(NSString *)title
                  displayType:(DisplayTypeDic)displayType
                      dataDic:(NSDictionary *)dataDic
                   cellAction:(void(^)(NSString *key , id obj))cellAction{
    IXLCellItem *item = [[self class] new];
    item.title= title;
    item.displayType = displayType;
    item.dataDic = dataDic;
    item.cellAction = cellAction;
    
    return item;
}
@end
