//
//  FXNormalTableView.m
//  交易记录
//
//  Created by Larry on 2017/7/24.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "IXNormalTableView.h"
#import "IXNoResultView.h"
#import "MJRefresh.h"
@interface IXNormalTableView()
<
UITableViewDataSource,
UITableViewDelegate,
UIGestureRecognizerDelegate,
UIScrollViewDelegate
>

@end

@implementation IXNormalTableView

-(instancetype)initWithFrame:(CGRect)frame
                  tableStyle:(UITableViewStyle)tableStyle
            estimatedHeaderH:(BOOL)needHeader
            estimatedFooterH:(BOOL)needFooter
{
    self = [super initWithFrame:frame style:tableStyle];
    if (self) {
        [self configTableH:needHeader F:needFooter];
    }
    return self;
}

- (void)configTableH:(BOOL)needHeader F:(BOOL)needFooter{
    self.backgroundColor = [UIColor clearColor];
    
    // row自动计算高度，不能高估，否则影响cell会重新创建子控件，卡帧。  低估会第一次会一屏加载很多cell
    self.estimatedRowHeight = 50;
    self.rowHeight = UITableViewAutomaticDimension;
    
    if (needHeader) {
        // 有header要写估算，否则header内嵌到cell中UI混乱; 无header不能写，否则cell不展示
        self.estimatedSectionHeaderHeight = 44;
    }
    if (needFooter) {
        // 有footer要写估算，否则footer内嵌到cell中UI混乱; 无header不能写，否则cell不展示
        self.estimatedSectionFooterHeight = 44;
    }
    
    self.showsVerticalScrollIndicator = NO;
    
    // 背景色: 透明
    // _table.backgroundColor = [UIColor colorWithESHexString:@"F1F1F1"];
    
    // 分割线：无
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.delegate = self;
    self.dataSource = self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self configTableH:NO F:NO];
    }
    return self;
}


-(NSMutableArray *)groups{
    if (!_groups) {
        _groups = [NSMutableArray array];
    }
    return _groups;
}

- (void)reloadTableView
{

}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.groups.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    IXLGroup *group = self.groups[section];
    NSInteger count = group.open ? group.cellItems.count : 0;
    if (count) {
        [self dismissNodataView];
    }
    return count;
}

#pragma mark - Group头部项
// 高度
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    FXLGroup *group = self.groups[section];
//    CGFloat height = group.headerItem ? group.headerItem.height : 0;
//    return height;
//}
// 视图
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    FXLGroup *group = self.groups[section];
//    if ([group.headerItem.title isEqualToString:@"用户管理"]) {
//        FXLUserMgrSecHeaderView *header = [FXLUserMgrSecHeaderView headerWithItem:group.headerItem];
//        return header;
//    }
//    return [UIView new];
//}

#pragma mark - Group中间项
// 高度
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    FXLGroup *group = self.groups[indexPath.section];
//    FXLCellItem *item = group.cellItems[indexPath.row];
//    return item.height;
//}

// 视图
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    FXLGroup *group = self.groups[indexPath.section];
//    FXLCellItem *item = group.cellItems[indexPath.row];
//
//    if ([item.title isEqualToString:@"积分明细_成员"]){
//        FXCreditRecordMemberCell *cell = [FXCreditRecordMemberCell cellWithTableView:tableView];
//        cell.item = item;
//        return cell;
//    }
//    return [UITableViewCell new];
//}


#pragma mark - Group底部项
// 高度
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    FXLGroup *group = self.groups[section];
//    NSInteger height = group.footerItem ? group.footerItem.height : 0;
//    return height;
//}

// 视图
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    FXLGroup *group = self.groups[section];
//    CJCellFooter *cell_footer = [footer footerWithItem:group.footerItem];
//    return cell_footer;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    IXLGroup *group = self.groups[indexPath.section];
    IXLCellItem *item = group.cellItems[indexPath.row];
    if (item.cellAction) {
        item.cellAction(NSStringFromCGPoint(CGPointMake(indexPath.section, indexPath.row)),item);
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self endEditing:YES];
}

/*  滚动监听
 
 #pragma mark - UISrollView
 
 -(void)scrollViewDidScroll:(UIScrollView *)sender
 {
 [NSObject cancelPreviousPerformRequestsWithTarget:self];
 [self performSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:sender afterDelay:0];
 
 self.isScrolling = YES;
 }
 
 
 -(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
 [NSObject cancelPreviousPerformRequestsWithTarget:self];
 
 self.isScrolling = NO;
 }
 */


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark-- UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    
    return YES;
}


#pragma mark - 回调
- (void)sendActionToUpperResponder:(NSString *)key object:(id)obj{
    if (self.viewAction) {
        self.viewAction(key, obj);
    }
}

- (void)showNodataView:(NSString *)title image:(NSString *)imgName{
    if (!title.length) {
        title = @"暂无记录";
    }
    if (!imgName.length) {
        imgName = @"newList_noneMsg_avatar";
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissNodataView];
        
        IXNoResultView * v = [[IXNoResultView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 300)
                                                             image:[UIImage imageNamed:imgName]
                                                             title:title];
        v.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2 - 50);
        v.tag = 4444;
        [self addSubview:v];
    });
}

- (void)dismissNodataView{
    UIView *v = [self viewWithTag:4444];
    if (v) {
        [v removeFromSuperview];
    }
}

/*
#pragma mark - 上下拉
- (void)beginRefreshing{
    [self endRefreash];
    [self.mj_header beginRefreshing];
}

- (void)addRefreash{
    [self addHeader];
    [self addFooter];
}

- (void)loadComplete{
    [self.mj_footer setState:MJRefreshStateNoMoreData];
}

- (void)endRefreash{
    if ( [self.mj_header isRefreshing] ) {
        [self.mj_header endRefreshing];
    }

    if ( [self.mj_footer isRefreshing] ) {
        [self.mj_footer endRefreshing];
    }
}

- (void)addHeader
{
    weakself;
    MJRefreshNormalHeader  *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if ( weakSelf.refreashBlock ) {
            weakSelf.refreashBlock();
        }
    }];
//    header.activityIndicatorViewStyle = [FXUserInfoMgr shareInstance].isNightMode ?
//    UIActivityIndicatorViewStyleWhite :
//    UIActivityIndicatorViewStyleGray;
    self.mj_header = header;
}

- (void)addFooter
{
    weakself;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if ( weakSelf.loadMoreBlock ) {
            weakSelf.loadMoreBlock();
        }
    }];
//    footer.activityIndicatorViewStyle = [FXUserInfoMgr shareInstance].isNightMode ?
//    UIActivityIndicatorViewStyleWhite :
//    UIActivityIndicatorViewStyleGray;
    self.mj_footer = footer;
}
*/

/*
- (void)addSection0{
    [self.groups removeAllObjects];
    weakself;
    NSMutableArray *itemArr = [NSMutableArray array];
    
    //    [self.roomLG.mem_arr_deal enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    //        item_deal *info = obj;
    //
    for (int i = 0; i < 10; i++) {
        
        
        IXLCellItem *item = [IXLCellItem itemWithTitle:@""
                                           displayType:DisplayTypeDicNormal
                                               dataDic:@{
                                                         //                                                         @"item_deal":info,
                                                         //                                                         @"FXRoomLG":weak_self.roomLG,
                                                         }
                                            cellAction:^(NSString *key, id obj) {
                                                [weakSelf sendActionToUpperResponder:key object:obj];
                                            }];
        [itemArr addObject:item];
        //    }];
        
    };
    //
    //    if (!itemArr.count) {
    //        [self showNodataView:@"暂无成交" image:nil];
    //    }
    //
    IXLGroup *group = [IXLGroup groupWithCellItems:itemArr
                                        headerItem:nil
                                        footerItem:nil];
    [self.groups addObject:group];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IXLGroup *group = self.groups[indexPath.section];
    IXLCellItem *item = group.cellItems[indexPath.row];
    
    return [UITableViewCell new];
}
*/

@end
