//
//  FXCellHeaderItem.h
//  FXAPP
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXCellHeaderItem : NSObject

@property(nonatomic,strong)NSString *title;

@property(nonatomic,strong)NSDictionary *dataDic;

@property(nonatomic,assign)CGFloat height; // 记录高度

@property(nonatomic,strong)void(^headerAction)(NSString *key , id obj); // 点击Header主事件

+ (instancetype)headerWithTitle:(NSString *)title
                        dataDic:(NSDictionary *)dataDic
                     headerAction:(void(^)(NSString *key , id obj))headerAction;

@end
