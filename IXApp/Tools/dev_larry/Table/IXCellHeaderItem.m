//
//  FXCellHeaderItem.m
//  FXAPP
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "IXCellHeaderItem.h"

@implementation IXCellHeaderItem
+ (instancetype)headerWithTitle:(NSString *)title
                        dataDic:(NSDictionary *)dataDic
                     headerAction:(void(^)(NSString *key , id obj))headerAction{
    IXCellHeaderItem *item = [IXCellHeaderItem new];
    item.title = title;
    item.headerAction = headerAction;
    item.dataDic = dataDic;
    return item;
}
@end
