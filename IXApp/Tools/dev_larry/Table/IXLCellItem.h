//
//  DictionaryItem.h
//  CJPlayGround
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, DisplayTypeDic) {
    DisplayTypeDicNormal,
    DisplayTypeDicUnnormal, // 待扩展
};
@interface IXLCellItem : NSObject
@property(nonatomic,copy)NSString *title;
@property(nonatomic,assign)double height;                // 记录高度
@property(nonatomic,assign)DisplayTypeDic displayType;      // 展示类型
@property(nonatomic,strong)NSDictionary *dataDic;
@property(nonatomic,assign)BOOL selected;

@property(nonatomic,copy) void(^cellAction)(NSString *key , id obj);

+ (instancetype)itemWithTitle:(NSString *)title
                  displayType:(DisplayTypeDic)displayType
                      dataDic:(NSDictionary *)dataDic
                   cellAction:(void(^)(NSString *key , id obj))cellAction;
@end
