//
//  FXLGroup.m
//  FXAPP
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "IXLGroup.h"

@implementation IXLGroup
+ (instancetype)groupWithCellItems:(NSArray *)cellItems
                        headerItem:(IXCellHeaderItem *)headerItem
                        footerItem:(IXCellFooterItem *)footerItem{
    
    IXLGroup *group = [IXLGroup new];
    group.cellItems = cellItems;
    group.headerItem = headerItem;
    group.footerItem = footerItem;
    group.open = YES;
    return group;
}
@end
