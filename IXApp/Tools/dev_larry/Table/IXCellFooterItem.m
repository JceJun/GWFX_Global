//
//  FXCellFooterItem.m
//  FXAPP
//
//  Created by Larry on 2017/7/19.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "IXCellFooterItem.h"

@implementation IXCellFooterItem
+ (instancetype)footerWithTitle:(NSString *)title
                        dataDic:(NSDictionary *)dataDic
                     footerAction:(void(^)(NSString *key , id obj))footerAction{
    IXCellFooterItem *item = [IXCellFooterItem new];
    item.title = title;
    item.footerAction = footerAction;
    item.dataDic = dataDic;
    
    return item;
}

+ (instancetype)footerWithTitle:(NSString *)title
                     mainAction:(void(^)(IXCellFooterItem *item))footerAction
{
    return nil;
}

@end
