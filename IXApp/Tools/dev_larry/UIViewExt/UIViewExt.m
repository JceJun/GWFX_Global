//
//  UIView+Larry.m
//
//  Created by Larry on 17/10/11.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import "UIViewExt.h"
#import "UIButton+Block.h"
#import <objc/message.h>
@implementation UIView (Larry)

- (void)set_width:(CGFloat)KWidth
{
    CGRect frame = self.frame;
    frame.size.width = KWidth;
    self.frame = frame;
}


- (void)set_height:(CGFloat)KHeight
{
    CGRect frame = self.frame;
    frame.size.height = KHeight;
    self.frame = frame;
}

- (void)set_x:(CGFloat)Kx
{
    CGRect frame = self.frame;
    frame.origin.x = Kx;
    self.frame = frame;
}

- (void)set_y:(CGFloat)Ky
{
    CGRect frame = self.frame;
    frame.origin.y = Ky;
    self.frame = frame;
}

- (CGFloat)_width
{
    return self.frame.size.width;
}

- (CGFloat)_height
{
    return self.frame.size.height;
}

- (CGFloat)_x
{
    return self.frame.origin.x;
}

- (CGFloat)_y
{
    return self.frame.origin.y;
}

-(void)set_centerX:(CGFloat)KCenterX{
    CGPoint center = self.center;
    center.x = KCenterX;
    self.center = center;
}

- (CGFloat)_centerX
{
    return self.center.x;
}

- (void)set_centerY:(CGFloat)KCenterY
{
    CGPoint center = self.center;
    center.y = KCenterY;
    self.center = center;
}

- (CGFloat)_centerY
{
    return self.center.y;
}

// Retrieve and set the origin
-(CGPoint)_origin{
    return self.frame.origin;
}

- (void) set_origin:(CGPoint) aPoint
{
    CGRect newframe = self.frame;
    newframe.origin = aPoint;
    self.frame = newframe;
}

// Retrieve and set the size
-(CGSize)_size{
    return self.frame.size;
}
- (void) set_size:(CGSize) aSize
{
    CGRect newframe = self.frame;
    newframe.size = aSize;
    self.frame = newframe;
}

// Query other frame locations
- (CGPoint) bottomRight
{
    CGFloat x = self.frame.origin.x + self.frame.size.width;
    CGFloat y = self.frame.origin.y + self.frame.size.height;
    return CGPointMake(x, y);
}

- (CGPoint) bottomLeft
{
    CGFloat x = self.frame.origin.x;
    CGFloat y = self.frame.origin.y + self.frame.size.height;
    return CGPointMake(x, y);
}

- (CGPoint) topRight
{
    CGFloat x = self.frame.origin.x + self.frame.size.width;
    CGFloat y = self.frame.origin.y;
    return CGPointMake(x, y);
}

- (CGFloat) _top
{
    return self.frame.origin.y;
}

- (void) set_top: (CGFloat) newtop
{
    CGRect newframe = self.frame;
    newframe.origin.y = newtop;
    self.frame = newframe;
}
- (CGFloat)_left{
    return self.frame.origin.x;
}

- (void)set_left:(CGFloat)newleft
{
    CGRect newframe = self.frame;
    newframe.origin.x = newleft;
    self.frame = newframe;
}

-(CGFloat)_bottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (void) set_bottom:(CGFloat) newbottom
{
    CGRect newframe = self.frame;
    newframe.origin.y = newbottom - self.frame.size.height;
    self.frame = newframe;
}

-(CGFloat)_right
{
    return self.frame.origin.x + self.frame.size.width;
}

-(void)set_right:(CGFloat)_right{
    CGFloat delta = _right - (self.frame.origin.x + self.frame.size.width);
    CGRect newframe = self.frame;
    newframe.origin.x += delta ;
    self.frame = newframe;
}




// Move via offset
- (void) moveBy: (CGPoint) delta
{
    CGPoint newcenter = self.center;
    newcenter.x += delta.x;
    newcenter.y += delta.y;
    self.center = newcenter;
}

// Scaling
- (void) scaleBy: (CGFloat) scaleFactor
{
    CGRect newframe = self.frame;
    newframe.size.width *= scaleFactor;
    newframe.size.height *= scaleFactor;
    self.frame = newframe;
}

// Ensure that both dimensions fit within the given size by scaling down
- (void) fitInSize: (CGSize) aSize
{
    CGFloat scale;
    CGRect newframe = self.frame;
    
    if (newframe.size.height && (newframe.size.height > aSize.height))
    {
        scale = aSize.height / newframe.size.height;
        newframe.size.width *= scale;
        newframe.size.height *= scale;
    }
    
    if (newframe.size.width && (newframe.size.width >= aSize.width))
    {
        scale = aSize.width / newframe.size.width;
        newframe.size.width *= scale;
        newframe.size.height *= scale;
    }
    
    self.frame = newframe;
}

#pragma mark - Extension By Larry.Chenjun
- (void)makeSize:(CGSize)size{
    self._height = size.height;
    self._width = size.width;
}

- (void)makeCenter:(CGPoint)center{
    self._left = center.x-self.bounds.size.width/2;
    self._top = center.y-self.bounds.size.height/2;
}

- (void)makeCenterInView:(UIView *)superView marginX:(CGFloat)x marginY:(CGFloat)y{
    self._left = (superView.bounds.size.width - self.bounds.size.width)/2;
    self._top = (superView.bounds.size.height - self.bounds.size.height)/2;
    
    if (x != 0) {
        self._left = x;
    }
    if (y != 0) {
        self._top = y;
    }
    
    if (![superView.subviews containsObject:self] && superView) {
        [superView addSubview:self];
    }
}

- (void)makePointX:(CGFloat)x Y:(CGFloat)y inView:(UIView *)superView{
    self._left = x;
    self._top = y;
    
    if (![superView.subviews containsObject:self] && superView) {
        [superView addSubview:self];
    }
}

+ (UIColor *) colorWithHexString: (NSString *)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    // 判断前缀
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}

// 添加分割线
+ (UIView *)insertSeparateView:(UIColor *)bgColor
                        yPoint:(CGFloat)yPoint
                        height:(CGFloat)height
                     superView:(UIView *)superView{
    
//    UIView *separatView = [[UIView alloc] initWithFrame:CGRectMake(0, yPoint, kScreenWidth, height)];
//    separatView.backgroundColor = bgColor;
//
//    if (height > 0) {
//        UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, PXFLOAT(1))];
//        topLine.backgroundColor = MarketCellSepColor;
//        [separatView addSubview:topLine];
//
//        UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, height - PXFLOAT(1), kScreenWidth, PXFLOAT(1))];
//        bottomLine.backgroundColor = topLine.backgroundColor;
//        [separatView addSubview:bottomLine];
//    }else{
//        separatView._height = PXFLOAT(1);
//        separatView._top = superView.bounds.size.height - separatView.bounds.size.height;
//        separatView.backgroundColor = MarketCellSepColor;
//    }
    
//    [superView addSubview:separatView];
    return [UIView new];
//    return separatView;
}

- (void)addCornerRadius:(CGFloat)cornerRadius viewSize:(CGSize)size{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
    
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (void)addCornerRadius:(CGFloat)cornerRadius{
    CGFloat view_W = self.bounds.size.width;
    CGFloat view_H = self.bounds.size.height;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, view_W, view_H) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    maskLayer.frame = CGRectMake(0, 0, view_W, view_H);
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (UIView *)borderForColor:(UIColor *)color borderWidth:(CGFloat)borderWidth borderType:(UIBorderSideType)borderType {
    
    if (borderType == UIBorderSideTypeAll) {
        self.layer.borderWidth = borderWidth;
        self.layer.borderColor = color.CGColor;
        return self;
    }
    
    /// 左侧
    if (borderType & UIBorderSideTypeLeft) {
        /// 左侧线路径
        [self.layer addSublayer:[self addLineOriginPoint:CGPointMake(0.f, 0.f) toPoint:CGPointMake(0.0f, self.frame.size.height) color:color borderWidth:borderWidth]];
    }
    
    /// 右侧
    if (borderType & UIBorderSideTypeRight) {
        /// 右侧线路径
        [self.layer addSublayer:[self addLineOriginPoint:CGPointMake(self.frame.size.width, 0.0f) toPoint:CGPointMake( self.frame.size.width, self.frame.size.height) color:color borderWidth:borderWidth]];
    }
    
    /// top
    if (borderType & UIBorderSideTypeTop) {
        /// top线路径
        [self.layer addSublayer:[self addLineOriginPoint:CGPointMake(0.0f, 0.0f) toPoint:CGPointMake(self.frame.size.width, 0.0f) color:color borderWidth:borderWidth]];
    }
    
    /// bottom
    if (borderType & UIBorderSideTypeBottom) {
        /// bottom线路径
        [self.layer addSublayer:[self addLineOriginPoint:CGPointMake(0.0f, self.frame.size.height) toPoint:CGPointMake( self.frame.size.width, self.frame.size.height) color:color borderWidth:borderWidth]];
    }
    
    return self;
}

/*
 * shadowColor 阴影颜色
 * shadowOpacity 阴影透明度，默认0
 * shadowRadius  阴影半径，默认3
 * shadowPathSide 设置哪一侧的阴影，
 * shadowPathWidth 阴影的宽度，
 */
-(void)LX_SetShadowPathWith:(UIColor *)shadowColor shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius shadowSide:(LXShadowPathSide)shadowPathSide shadowPathWidth:(CGFloat)shadowPathWidth{
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = shadowColor.CGColor;
    self.layer.shadowOpacity = shadowOpacity;
    self.layer.shadowRadius =  shadowRadius;
    self.layer.shadowOffset = CGSizeZero;
    CGRect shadowRect;
    
    CGFloat originX = 0;
    CGFloat originY = 0;
    CGFloat originW = self.bounds.size.width;
    CGFloat originH = self.bounds.size.height;
    
    switch (shadowPathSide) {
        case LXShadowPathTop:
            shadowRect  = CGRectMake(originX, originY - shadowPathWidth/2, originW,  shadowPathWidth);
            break;
        case LXShadowPathBottom:
            shadowRect  = CGRectMake(originX, originH -shadowPathWidth/2, originW, shadowPathWidth);
            break;
            
        case LXShadowPathLeft:
            shadowRect  = CGRectMake(originX - shadowPathWidth/2, originY, shadowPathWidth, originH);
            break;
            
        case LXShadowPathRight:
            shadowRect  = CGRectMake(originW - shadowPathWidth/2, originY, shadowPathWidth, originH);
            break;
        case LXShadowPathNoTop:
            shadowRect  = CGRectMake(originX -shadowPathWidth/2, originY +1, originW +shadowPathWidth,originH + shadowPathWidth/2 );
            break;
        case LXShadowPathAllSide:
            shadowRect  = CGRectMake(originX - shadowPathWidth/2, originY - shadowPathWidth/2, originW +  shadowPathWidth, originH + shadowPathWidth);
            break;
            
    }
    UIBezierPath *path =[UIBezierPath bezierPathWithRect:shadowRect];
    self.layer.shadowPath = path.CGPath;
}

- (CAShapeLayer *)addLineOriginPoint:(CGPoint)p0 toPoint:(CGPoint)p1 color:(UIColor *)color borderWidth:(CGFloat)borderWidth {
    
    /// 线的路径
    UIBezierPath * bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:p0];
    [bezierPath addLineToPoint:p1];
    
    CAShapeLayer * shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = color.CGColor;
    shapeLayer.fillColor  = [UIColor clearColor].CGColor;
    /// 添加路径
    shapeLayer.path = bezierPath.CGPath;
    /// 线宽度
    shapeLayer.lineWidth = borderWidth;
    return shapeLayer;
}

+ (UIImage*)createImageWithColor: (UIColor*) color{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

// 渐变色
- (void)gradientWithColors:(NSArray *)colorArr{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = colorArr;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    gradient.locations = @[@0.0, @0.2, @0.5];
    [self.layer addSublayer:gradient];
}



// 绑定对象
+ (void)bondSupperObject:(id)supperObj subObject:(id)subObj byKey:(NSString *)key{
    objc_setAssociatedObject(supperObj, [key UTF8String], subObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

// 获取绑定对象
+ (id)getSubObjFromSupperObj:(id)supperObj bySubObjectKey:(NSString *)key{
    return objc_getAssociatedObject(supperObj, [key UTF8String]);
}

// 工厂：Label
+ (UILabel *)makeLabel:(UIFont *)font color:(UIColor *)color superView:(UIView *)superView{
    UILabel *lb = [UILabel new];
    lb.font = font;
    lb.textColor = color;
    [superView addSubview:lb];
    return lb;
}

// 工厂：Button
+ (UIButton *)makeButton:(UIFont *)font txtColor:(UIColor *)txtColor bgColor:(UIColor *)bgColor superView:(UIView *)superView action:(void (^)(UIButton *aButton))action{
    UIButton *btn = [UIButton new];
    [superView addSubview:btn];
    btn.titleLabel.font = font;
    [btn setTitleColor:txtColor forState:UIControlStateNormal];
    [btn setBackgroundImage:[[self class] createImageWithColor:bgColor] forState:UIControlStateNormal];
    [btn block_touchUpInside:^(UIButton *aButton) {
        action(aButton);
    }];
    return btn;
}

//  颜色转换为背景图片
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
