//
//  UIView+Larry.m
//
//  Created by Larry on 17/10/11.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, UIBorderSideType) {
    UIBorderSideTypeAll  = 0,
    UIBorderSideTypeTop = 1 << 0,
    UIBorderSideTypeBottom = 1 << 1,
    UIBorderSideTypeLeft = 1 << 2,
    UIBorderSideTypeRight = 1 << 3,
}; // 边框（四边）

typedef enum :NSInteger{
    LXShadowPathLeft,
    LXShadowPathRight,
    LXShadowPathTop,
    LXShadowPathBottom,
    LXShadowPathNoTop,
    LXShadowPathAllSide
} LXShadowPathSide;  // 阴影（四边）

// 随机色
#define RGB_COLOR_SET(view)  if(view) {view.backgroundColor = [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:0.8];}
#define RGB_COLOR [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:0.8]

#define COLOR_HEX(s) [UIView colorWithHexString:s]
// 像素转换
#define PXFLOAT(num) num / [UIScreen mainScreen].scale

// 随机数
#define RANDOM(from,to) (int)(from + (arc4random() % (to - from + 1)))

@interface UIView (Larry)
@property (nonatomic, assign) CGFloat _width;
@property (nonatomic, assign) CGFloat _height;
@property (nonatomic, assign) CGFloat _x;
@property (nonatomic, assign) CGFloat _y;
@property (nonatomic, assign) CGFloat _centerX;
@property (nonatomic, assign) CGFloat _centerY;

@property (nonatomic, assign)CGFloat _top;
@property (nonatomic, assign)CGFloat _left;

@property (nonatomic, assign)CGFloat _bottom;
@property (nonatomic, assign)CGFloat _right;

@property (nonatomic, assign)CGPoint _origin;
@property (nonatomic, assign)CGSize _size;


// Extension By Larry.Chenjun
// 设置：坐标，大小，superView
- (void)makeSize:(CGSize)size;
- (void)makeCenter:(CGPoint)size;
- (void)makeCenterInView:(UIView *)superView marginX:(CGFloat)x marginY:(CGFloat)y;
- (void)makePointX:(CGFloat)x Y:(CGFloat)y inView:(UIView *)superView;

// 16进制颜色
+ (UIColor *) colorWithHexString: (NSString *)hex;

// 添加分割线
+ (UIView *)insertSeparateView:(UIColor *)bgColor
                        yPoint:(CGFloat)yPoint
                        height:(CGFloat)height
                     superView:(UIView *)superView;


/*---------------------- UIView 设置圆角（非离屏渲染）----------------------------*/
// 前提：用于AutoLayout布局，View的宽高不定
- (void)addCornerRadius:(CGFloat)cornerRadius viewSize:(CGSize)size;
// 前提：用于Frame布局，View的宽高已设置
- (void)addCornerRadius:(CGFloat)cornerRadius;

// 边框（四边）
- (UIView *)borderForColor:(UIColor *)color borderWidth:(CGFloat)borderWidth borderType:(UIBorderSideType)borderType;

/*
 * shadowColor 阴影颜色
 * shadowOpacity 阴影透明度，默认0
 * shadowRadius  阴影半径，默认3
 * shadowPathSide 设置哪一侧的阴影，
 * shadowPathWidth 阴影的宽度，
 */
// 阴影（四边）
-(void)LX_SetShadowPathWith:(UIColor *)shadowColor shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius shadowSide:(LXShadowPathSide)shadowPathSide shadowPathWidth:(CGFloat)shadowPathWidth;

// 色值转图片
+ (UIImage*)createImageWithColor:(UIColor*)color;

// 渐变色
- (void)gradientWithColors:(NSArray *)colorArr;

// 绑定对象
+ (void)bondSupperObject:(id)supperObj subObject:(id)subObj byKey:(NSString *)key;
+ (id)getSubObjFromSupperObj:(id)supperObj bySubObjectKey:(NSString *)key;

// 工厂：Label
+ (UILabel *)makeLabel:(UIFont *)font color:(UIColor *)color superView:(UIView *)superView;
// 工厂：Button
+ (UIButton *)makeButton:(UIFont *)font txtColor:(UIColor *)txtColor bgColor:(UIColor *)bgColor superView:(UIView *)superView action:(void (^)(UIButton *aButton))action;

+ (UIImage *)imageWithColor:(UIColor *)color;



// 动态添加属性(对象)
#define DYSYNTH_DYNAMIC_PROPERTY_OBJECT(_getter_, _setter_, _association_, _type_) \
- (void)_setter_ : (_type_)object { \
[self willChangeValueForKey:@#_getter_]; \
objc_setAssociatedObject(self, _cmd, object, OBJC_ASSOCIATION_ ## _association_); \
[self didChangeValueForKey:@#_getter_]; \
} \
- (_type_)_getter_ { \
return objc_getAssociatedObject(self, @selector(_setter_:)); \
}

// 动态添加属性(基本数据类型)
#define DYSYNTH_DYNAMIC_PROPERTY_CTYPE(_getter_, _setter_, _type_) \
- (void)_setter_ : (_type_)object { \
[self willChangeValueForKey:@#_getter_]; \
NSValue *value = [NSValue value:&object withObjCType:@encode(_type_)]; \
objc_setAssociatedObject(self, _cmd, value, OBJC_ASSOCIATION_RETAIN); \
[self didChangeValueForKey:@#_getter_]; \
} \
- (_type_)_getter_ { \
_type_ cValue = { 0 }; \
NSValue *value = objc_getAssociatedObject(self, @selector(_setter_:)); \
[value getValue:&cValue]; \
return cValue; \
}

// Category动态添加属性快捷set get宏
//#pragma 属性
//DYSYNTH_DYNAMIC_PROPERTY_OBJECT(agent, setAgent, RETAIN, DYTableViewAgent *)
//DYSYNTH_DYNAMIC_PROPERTY_CTYPE(autoReload, setAutoReload, BOOL)

@end
