//
//  UIAlertView+Block.h
//  FXApp
//
//  Created by Larry on 2017/8/3.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Block)
//点以一个点击回调Block
typedef void(^AlertClickedBlock)(NSInteger buttonIndex);

@property (nonatomic, copy) AlertClickedBlock alertViewCallBackBlock;

+ (void)creat_AlertViewWithClickBlock:(AlertClickedBlock)alertViewClickBackBlock
                                title:(NSString *)title message:(NSString *)message
                      cancelButtonStr:(NSString *)cancelButtonStr
                    otherButtonTitles:(NSString *)otherButtonTitles, ...NS_REQUIRES_NIL_TERMINATION;


@end
