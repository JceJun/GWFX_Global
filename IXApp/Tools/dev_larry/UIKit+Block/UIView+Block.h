//
//  UIView+Block.h
//  FXApp
//
//  Created by Larry on 2017/7/26.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^TapBlock)(UIView *aView);
@interface UIView (Block)
- (void)block_whenTapped:(TapBlock)block;
@end
