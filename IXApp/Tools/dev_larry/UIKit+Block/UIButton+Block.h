//
//  UIButton+Block.h
//  123
//
//  Created by Larry on 2017/7/26.
//  Copyright © 2017年 Larry. All rights reserved.
//

#import <UIKit/UIKit.h>
#define defaultInterval 0.5 // 默认时间间隔

typedef void (^ButtonBlock)(UIButton *aButton);

@interface UIButton (Block)

- (void)block_touchUpInside:(ButtonBlock)block;

// 防止重复点击
@property(nonatomic,assign)NSTimeInterval timeInterval; //用这个给重复点击加间隔
@property(nonatomic,assign)BOOL isIgnoreEvent;  //YES不允许点击NO允许点击

@end
