//
//  UIView+Block.m
//  FXApp
//
//  Created by Larry on 2017/7/26.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "UIView+Block.h"
#import <objc/message.h>
static const char ViewKey;
@implementation UIView (Block)
- (void)block_whenTapped:(TapBlock)block;
{
    if (block)
    {
        objc_setAssociatedObject(self, &ViewKey, block, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)]];
}

- (void)tapped:(UITapGestureRecognizer *)tap
{
    TapBlock block = objc_getAssociatedObject(self, &ViewKey);
    if (block) {
        block(tap.view);
    }
}
@end
