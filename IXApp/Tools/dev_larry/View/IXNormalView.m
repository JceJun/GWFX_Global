//
//  CommonView.m
//  FXApp
//
//  Created by Larry on 2017/7/29.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "IXNormalView.h"

@implementation IXNormalView

+ (instancetype)instanceWithViewAction:(void(^)(NSString* key,id obj))viewAction superView:(UIView *)superView
{
    IXNormalView *view = [IXNormalView new];
    view.viewAction = viewAction;
    [superView addSubview:view];
    return view;
}

#pragma mark - 触发回调
- (void)sendActionToUpperResponder:(NSString *)key object:(id)obj
{
    if (self.viewAction) {
        self.viewAction(key, obj);
    }
}

@end
