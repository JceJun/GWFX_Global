//
//  CommonView.h
//  FXApp
//
//  Created by Larry on 2017/7/29.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXNormalView : UIView

@property(nonatomic,copy) void (^viewAction)(NSString *key, id obj);

// VC初始化View
+ (instancetype)instanceWithViewAction:(void(^)(NSString* key,id obj))viewAction superView:(UIView *)superView;

// 发送Block到VC
- (void)sendActionToUpperResponder:(NSString *)key object:(id)obj;

@end
