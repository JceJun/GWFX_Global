//
//  GTUdpSocket.h
//  FX_GTS2
//
//  Created by cheik.chen on 2018/3/12.
//  Copyright © 2018年 gw. All rights reserved.
//

#define GTSVisit_index_page @"visit_index_page" //首页
#define GTSVisit_chat_page @"visit_chat_page" //解盘
#define GTSVisit_mehotactivities_page @"visit_mehotactivities_page" //热门活动
#define GTSVisit_mereferral_page @"visit_mereferral_page" //推荐有奖
#define GTSVisit_openaccount_page @"visit_openaccount_page" //开户
#define GTSVisit_deposit_page @"visit_deposit_page" //存款
#define GTSVisit_withdrawal_page @"visit_withdrawal_page" //取款



#import <Foundation/Foundation.h>

@interface GTUdpSocketManager : NSObject
+(instancetype)sharedUdpSocketManager;

/**
 发送监控数据

 @param page 页面标识
 @param success 是否成功 0 是失败，1是成功
 @param elapsed 成功加载时长
 */
-(void)sendDataWithPage:(NSString *)page success:(NSInteger)success elapsed:(float)elapsed;
@end
