//
//  GTUdpSocket.m
//  FX_GTS2
//
//  Created by cheik.chen on 2018/3/12.
//  Copyright © 2018年 gw. All rights reserved.
//

#import "GTUdpSocketManager.h"
#import "GCDAsyncUdpSocket.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "IXCpyConfig.h"
//#import "GTFuncManager.h"

@interface GTUdpSocketManager()<GCDAsyncUdpSocketDelegate>
{
    GCDAsyncUdpSocket *udpSocket; // 定义一个socket的对象 签订代理 GCDAsyncUdpSocketDelegate
}
@end

@implementation GTUdpSocketManager
static GTUdpSocketManager *udpSocketManager = nil;

+(instancetype)sharedUdpSocketManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        udpSocketManager = [[self alloc]init];
        
    });
    return udpSocketManager;
}
+(instancetype)allocWithZone:(struct _NSZone *)zone{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        udpSocketManager = [super allocWithZone:zone];
    });
    return udpSocketManager;
}
-(id)copyWithZone:(NSZone *)zone {
    return udpSocketManager;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        NSError *error = nil;
    
         [udpSocket enableBroadcast:YES error:&error];
        // 启用广播
        [udpSocket enableBroadcast:YES error:&error];
        if (error) {
            NSLog(@"UdpSocket启用失败");
        }else {
            NSLog(@"UdpSocket %@", [udpSocket localHost]);
            // 开始接收消息
            [udpSocket beginReceiving:&error];
        }
    }
    return self;
}

-(NSString *)getIPAddress {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

-(NSString *)deviceWANIPAddress
{
    NSURL *ipURL = [NSURL URLWithString:@"http://ip.taobao.com/service/getIpInfo.php?ip=myip"];
    NSData *data = [NSData dataWithContentsOfURL:ipURL];
    NSString *ipStr = nil;
    if(data){
        NSDictionary *ipDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]; 
        if (ipDic && [ipDic[@"code"] integerValue] == 0) { //获取成功
            ipStr = ipDic[@"data"][@"ip"];
        }
    }
    return (ipStr ? ipStr : @"");
    
}

-(void)sendDataWithPage:(NSString *)page success:(NSInteger)success elapsed:(float)elapsed{
    
    dispatch_async(dispatch_queue_create("sendDataWithPage", NULL), ^{
        NSString *IP = [self deviceWANIPAddress];
        NSString *version = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
        NSString *defulString = @"business_unit=fxint,type=performance_data,access_terminal_type=app,access_terminal_name=ix,mobile_platform=ios";
        NSString *string = [NSString stringWithFormat:@"%@,%@,version=%@,IP=%@ errno=%d,elapsed=%f  ",page,defulString,version,IP,success,elapsed];
        NSLog(@"UdpSocket---%@",string);
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *host =  @"223.197.91.54";
        if ([CompanyName containsString:@"UAT"] ) {
            host = @"192.168.35.199";
        }
        uint16_t port = 8091;
        [udpSocket sendData:data toHost:host port:port withTimeout:-1 tag:100];
    });
}


#pragma mark - GCDAsyncUdpSocketDelegate
-(void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag{
    if (tag == 100) {
        NSLog(@"UdpSocket---表示标记为100的数据发送完成了");
    }
}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
    NSLog(@"UdpSocket---标记为tag %ld的发送失败 失败原因 %@",tag, error);
}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext{
    
    NSString *ip = [GCDAsyncUdpSocket hostFromAddress:address];
    uint16_t port = [GCDAsyncUdpSocket portFromAddress:address];
    NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    // 继续来等待接收下一次消息
    NSLog(@"收到服务端的响应 [%@:%d] %@", ip, port, s);
    [sock receiveOnce:nil];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error{
    NSLog(@"udpSocket关闭");
}

-(void)sendBackToHost:(NSString *)ip port:(uint16_t)port withMessage:(NSString *)s{
    NSString *msg = @"我收到了";
    NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
    [udpSocket sendData:data toHost:ip port:port withTimeout:0.1 tag:200];
    
}

@end
