//
//  CAGradientLayer+Night.h
//  IXApp
//
//  Created by Seven on 2017/12/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "NSObject+Night.h"

@interface CAGradientLayer (Night)

@property (nonatomic, strong) NSArray   * dk_colors;

@end
