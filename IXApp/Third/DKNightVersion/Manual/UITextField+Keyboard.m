//
//  UITextField+Keyboard.m
//  DKNightVersion
//
//  Created by Draveness on 16/4/11.
//  Copyright © 2016年 Draveness. All rights reserved.
//

#import "UITextField+Keyboard.h"
#import "NSObject+Night.h"
#import <objc/runtime.h>
#import "IXHookUtility.h"

@interface NSObject ()

- (void)night_updateColor;

@end


@implementation UITextField (Keyboard)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];

        SEL originalInit = @selector(init);
        SEL swizzledInit = @selector(dk_init);

        [IXHookUtility swizzlingClass:class
                     originalSelector:originalInit
                     swizzledSelector:swizzledInit];
        
        SEL originalInitWithFrame = @selector(initWithFrame:);
        SEL swizzledInitWithFrame = @selector(dk_initWithFrame:);
        
        [IXHookUtility swizzlingClass:class
                     originalSelector:originalInitWithFrame
                     swizzledSelector:swizzledInitWithFrame];
    });

}

- (instancetype)dk_init {
    UITextField *obj = [self dk_init];
    if (self.dk_manager.supportsKeyboard
        && [self.dk_manager.themeVersion isEqualToString:DKThemeVersionNight]) {
        obj.keyboardAppearance = UIKeyboardAppearanceDark;
    } else {
        obj.keyboardAppearance = UIKeyboardAppearanceDefault;
    }
    return obj;
}

- (instancetype)dk_initWithFrame:(CGRect)frame
{
    UITextField *obj = [self dk_initWithFrame:frame];
    if (self.dk_manager.supportsKeyboard
        && [self.dk_manager.themeVersion isEqualToString:DKThemeVersionNight]) {
        obj.keyboardAppearance = UIKeyboardAppearanceDark;
    } else {
        obj.keyboardAppearance = UIKeyboardAppearanceDefault;
    }
    return obj;
}

- (void)night_updateColor {
    [super night_updateColor];
    if (self.dk_manager.supportsKeyboard
        && [self.dk_manager.themeVersion isEqualToString:DKThemeVersionNight]) {
        self.keyboardAppearance = UIKeyboardAppearanceDark;
    } else {
        self.keyboardAppearance = UIKeyboardAppearanceDefault;
    }
}

@end
