//
//  UISearchBar+Keyboard.m
//  DKNightVersion
//
//  Created by Draveness on 6/8/16.
//  Copyright © 2016 Draveness. All rights reserved.
//

#import "UISearchBar+Keyboard.h"
#import "NSObject+Night.h"
#import "IXHookUtility.h"

@interface NSObject ()

- (void)night_updateColor;

@end

@implementation UISearchBar (Keyboard)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalInit = @selector(init);
        SEL swizzledInit = @selector(dk_init);
        
        [IXHookUtility swizzlingClass:class
                     originalSelector:originalInit
                     swizzledSelector:swizzledInit];
        
        SEL originalInitWithFrame = @selector(initWithFrame:);
        SEL swizzledInitWithFrame = @selector(dk_initWithFrame:);
        
        [IXHookUtility swizzlingClass:class
                     originalSelector:originalInitWithFrame
                     swizzledSelector:swizzledInitWithFrame];
    });

}

- (instancetype)dk_initWithFrame:(CGRect)frame
{
    UISearchBar *obj = [self dk_initWithFrame:frame];
    if (self.dk_manager.supportsKeyboard
        && [self.dk_manager.themeVersion isEqualToString:DKThemeVersionNight]) {
        UITextField *searchField = [obj valueForKey:@"_searchField"];
        searchField.keyboardAppearance = UIKeyboardAppearanceDark;
    } else {
        UITextField *searchField = [obj valueForKey:@"_searchField"];
        searchField.keyboardAppearance = UIKeyboardAppearanceDefault;
    }
    
    return obj;
}

- (instancetype)dk_init {
    UISearchBar *obj = [self dk_init];
    if (self.dk_manager.supportsKeyboard
        && [self.dk_manager.themeVersion isEqualToString:DKThemeVersionNight]) {
        UITextField *searchField = [obj valueForKey:@"_searchField"];
        searchField.keyboardAppearance = UIKeyboardAppearanceDark;
    } else {
        UITextField *searchField = [obj valueForKey:@"_searchField"];
        searchField.keyboardAppearance = UIKeyboardAppearanceDefault;
    }

    return obj;
}

- (void)night_updateColor {
    [super night_updateColor];
    if (self.dk_manager.supportsKeyboard
        && [self.dk_manager.themeVersion isEqualToString:DKThemeVersionNight]) {
        UITextField *searchField = [self valueForKey:@"_searchField"];
        searchField.keyboardAppearance = UIKeyboardAppearanceDark;
    } else {
        UITextField *searchField = [self valueForKey:@"_searchField"];
        searchField.keyboardAppearance = UIKeyboardAppearanceDefault;
    }
}

@end
