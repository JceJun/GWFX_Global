//
//  UIKit.m
//  Pods
//
//  Created by Matt Miao on 8/4/2016.
//
//

#import "UIImage+Bundle.h"

@implementation UIImage (Bundle)

+ (UIImage *)loadImageFromBundleName:(NSString *)name {

//    NSString* bundlePath = [[NSBundle mainBundle].resourcePath
//                            stringByAppendingPathComponent:@"WLLib.bundle"];
//    
//    NSBundle* bundle = [NSBundle bundleWithPath:bundlePath];
//    
//    NSString *img_path = [bundlePath stringByAppendingPathComponent:name];
//    UIImage* image = [UIImage imageWithContentsOfFile:img_path];
//    if (image) {
//        return image;
//    } else {
//        img_path = [bundle pathForResource:name ofType:@"png"];
//        image = [UIImage imageWithContentsOfFile:img_path];
//        
//        if (image == nil) {
//            img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@@2x",name] ofType:@"png"];
//            image = [UIImage imageWithContentsOfFile:img_path];
//        }
//        
//    }
    
    UIImage* image = [UIImage imageNamed:name];
    
    return image;
}

@end
