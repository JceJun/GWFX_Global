//
//  UIKit.h
//  Pods
//
//  Created by Matt Miao on 8/4/2016.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Bundle)

+ (UIImage *)loadImageFromBundleName:(NSString *)name;

@end
