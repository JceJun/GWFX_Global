//
//  XHEmotionCollectionViewFlowLayout.m
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-5-3.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import "XHEmotionCollectionViewFlowLayout.h"
#import "XHEmotionManagerView.h"
#import "XHMacro.h"

@implementation XHEmotionCollectionViewFlowLayout

- (id)init {
    self = [super init];
    
    if (self) {
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        int count = kXHEmotionPerRowItemCount;
        
        int emotionImageViewSize;
        CGFloat spacing;
        CGFloat verticalSpacing;
        
        verticalSpacing = (kIsiPad ? 30 : 10);
        
        if (kXHEmotionImageViewSize * kXHEmotionPerRowItemCount + kXHEmotionMinimumLineSpacing < MDK_SCREEN_WIDTH) {
            emotionImageViewSize = kXHEmotionImageViewSize;
            spacing = MDK_SCREEN_WIDTH/count - kXHEmotionImageViewSize;
        } else {
            emotionImageViewSize = (MDK_SCREEN_WIDTH - kXHEmotionMinimumLineSpacing) / count;
            spacing = kXHEmotionMinimumLineSpacing;
        }
        
        self.itemSize = CGSizeMake(emotionImageViewSize, emotionImageViewSize);
        self.minimumLineSpacing = spacing;
        self.sectionInset = UIEdgeInsetsMake(verticalSpacing, spacing/2, verticalSpacing, spacing/2);
        self.collectionView.alwaysBounceVertical = YES;
    }
    
    return self;
}

@end
