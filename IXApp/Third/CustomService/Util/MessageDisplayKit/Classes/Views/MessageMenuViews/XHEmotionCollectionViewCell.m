//
//  XHEmotionCollectionViewCell.m
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-5-3.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import "XHEmotionCollectionViewCell.h"
#import "XHEmotionManagerView.h"
#import "Emoji.h"



@interface XHEmotionCollectionViewCell ()

/**
 *  显示表情封面的控件
 */
@property (nonatomic, weak) UIImageView *emotionImageView;

@property (nonatomic, weak)UIButton *emotionButton;

@property (nonatomic)int emotionType;

@property (nonatomic) NSArray* emojiFaces;

@property (nonatomic)int emotionImageViewSize;

/**
 *  配置默认控件和参数
 */
- (void)setup;
@end

@implementation XHEmotionCollectionViewCell


- (NSArray*)emojiFaces
{
    if (!_emojiFaces) {
        _emojiFaces = [Emoji allEmoji];
    }
    return  _emojiFaces;
}

- (UIButton*)emotionButton
{
    if (!_emotionButton) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor redColor];
        [button setEnabled:NO];
        [button setBackgroundColor:[UIColor clearColor]];
        [button setFrame:CGRectMake(0, 0, _emotionImageViewSize, _emotionImageViewSize)];
        
        _emotionButton = button;
    }
    
    return _emotionButton;
}

- (void)reloadData
{
    
    float iosVersion =[[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (iosVersion < 8) {
        
        if (!_emotion.emotionConverPhoto) {
            self.emotionType = 0;
            //set emoji
            int page = self.emotion.emotionNumber / (kXHEmotionPerRowItemCount * kXHEmotionPerColumnItemCount);
            int row = (self.emotion.emotionNumber % (kXHEmotionPerRowItemCount * kXHEmotionPerColumnItemCount)) / kXHEmotionPerRowItemCount;
            int column = (self.emotion.emotionNumber % (kXHEmotionPerRowItemCount * kXHEmotionPerColumnItemCount)) % kXHEmotionPerRowItemCount;
            
            //        if (row == (kXHEmotionPerColumnItemCount-1) && column == (kXHEmotionPerRowItemCount-1)) {
            //            [self.emotionButton setImage:[UIImage imageNamed:@"DeleteEmoticonBtn_ios7"] forState:UIControlStateNormal];
            //            self.emotionButton.tag=10000;
            //            self.emotion.emotionType = 1;
            //            [self.emotionButton.titleLabel setFrame:CGRectMake(0, 0, self.emotionButton.frame.size.width, self.emotionButton.frame.size.height)];
            //            [self.emotionButton.titleLabel setFrame:CGRectMake(0, 0, 0, 0)];
            //            [self.emotionButton setTitle:@"" forState:UIControlStateNormal];
            //
            //        } else {
            //            [self.emotionButton.imageView setFrame:CGRectMake(0, 0, 0, 0)];
            //            [self.emotionButton.titleLabel setFrame:CGRectMake(0, 0, self.emotionButton.frame.size.width, self.emotionButton.frame.size.height)];
            //            //[self.emotionButton.titleLabel setFont:[UIFont fontWithName:@"AppleColorEmoji" size:13.0]];
            //            //[self.emotionButton.titleLabel setTextColor:[UIColor blackColor]];
            //
            //
            //            [self.emotionButton.titleLabel setFont:[UIFont fontWithName:@"AppleColorEmoji" size:11.0]];
            //            self.emotionButton.backgroundColor = [UIColor yellowColor];
            //            [self.emotionButton setImage:nil forState:UIControlStateNormal];
            //
            //            [self.emotionButton setTitle:self.emotion.emoji forState:UIControlStateNormal];
            //            self.emotionButton.tag=row*3+column+(page*19);
            //            DLog(@"%@", self.emotion.emoji);
            //        }
            if (_emotionButton) {
                [_emotionButton removeFromSuperview];
                _emotionButton = nil;
                [self emotionButton];
                
            }
            if (row == (kXHEmotionPerColumnItemCount-1) && column == (kXHEmotionPerRowItemCount-1)) {
                [_emotionButton setImage:[UIImage imageNamed:@"DeleteEmoticonBtn_ios7"] forState:UIControlStateNormal];
                _emotionButton.tag=10000;
                self.emotion.emotionType = 1;
                [_emotionButton.titleLabel setFrame:CGRectMake(0, 0, _emotionButton.frame.size.width, _emotionButton.frame.size.height)];
                [_emotionButton.titleLabel setFrame:CGRectMake(0, 0, 0, 0)];
                [_emotionButton setTitle:@"" forState:UIControlStateNormal];
                
            } else {
                [_emotionButton.imageView setFrame:CGRectMake(0, 0, 0, 0)];
                [_emotionButton.imageView setFrame:CGRectMake(0, 0, _emotionButton.frame.size.width, _emotionButton.frame.size.height)];
                [_emotionButton.titleLabel setFont:[UIFont fontWithName:@"AppleColorEmoji" size:29.0]];
                [_emotionButton setImage:nil forState:UIControlStateNormal];
                
                [_emotionButton setTitle:self.emotion.emoji forState:UIControlStateNormal];
                _emotionButton.tag=row*3+column+(page*19);
            }
            [self.contentView addSubview:_emotionButton];
        } else {
            self.emotionType = 1;
            self.emotionImageView.image = _emotion.emotionConverPhoto;
        }
        
    } else {
        if (!_emotion.emotionConverPhoto) {
            self.emotionType = 0;
            //set emoji
            int page = self.emotion.emotionNumber / (kXHEmotionPerRowItemCount * kXHEmotionPerColumnItemCount);
            int row = (self.emotion.emotionNumber % (kXHEmotionPerRowItemCount * kXHEmotionPerColumnItemCount)) / kXHEmotionPerRowItemCount;
            int column = (self.emotion.emotionNumber % (kXHEmotionPerRowItemCount * kXHEmotionPerColumnItemCount)) % kXHEmotionPerRowItemCount;
            if (row == (kXHEmotionPerColumnItemCount-1) && column == (kXHEmotionPerRowItemCount-1)) {
                [_emotionButton.titleLabel setFrame:CGRectMake(0, 0, _emotionButton.frame.size.width, _emotionButton.frame.size.height)];
                [_emotionButton.titleLabel setFrame:CGRectMake(0, 0, 0, 0)];
                [self.emotionButton setImage:[UIImage imageNamed:@"DeleteEmoticonBtn_ios7"] forState:UIControlStateNormal];
                self.emotionButton.tag=10000;
                self.emotion.emotionType = 1;
                
            } else {
                [_emotionButton.imageView setFrame:CGRectMake(0, 0, 0, 0)];
                [_emotionButton.imageView setFrame:CGRectMake(0, 0, _emotionButton.frame.size.width, _emotionButton.frame.size.height)];
                [self.emotionButton.titleLabel setFont:[UIFont fontWithName:@"AppleColorEmoji" size:29.0]];
                [self.emotionButton setImage:nil forState:UIControlStateNormal];
                [self.emotionButton setTitle:_emotion.emoji forState:UIControlStateNormal];
                self.emotionButton.tag=row*3+column+(page*19);
            }
        } else {
            self.emotionType = 1;
            if (self.emotionImageView) {
                self.emotionImageView.image = _emotion.emotionConverPhoto;
            }
            else{
                [self.emotionButton setImage:_emotion.emotionConverPhoto forState:UIControlStateNormal];
            }
        }
        
    }
    return;
}

#pragma setter method

- (void)setEmotion:(XHEmotion *)emotion {
    _emotion = emotion;
    // TODO:
    
    if (!emotion.emotionConverPhoto) {
        //emoji
        self.emotionType = 0;
    } else{
        //图片
        self.emotionType = 1;
        [self.emotionButton setTitle:@"" forState:UIControlStateNormal];
        self.emotionImageView.image = emotion.emotionConverPhoto;
    }
    [self reloadData];
    
}

#pragma mark - Life cycle

- (void)setup {
    if (!_emotionButton) {
        //int emotionImageViewSize;
        CGFloat spacing;
        if (kXHEmotionImageViewSize * kXHEmotionPerRowItemCount + kXHEmotionMinimumLineSpacing < MDK_SCREEN_WIDTH) {
            _emotionImageViewSize = kXHEmotionImageViewSize;
            spacing = MDK_SCREEN_WIDTH/kXHEmotionPerRowItemCount - kXHEmotionImageViewSize;
        } else {
            _emotionImageViewSize = (MDK_SCREEN_WIDTH - kXHEmotionMinimumLineSpacing) / kXHEmotionPerRowItemCount;
            spacing = kXHEmotionMinimumLineSpacing;
        }
        
        if (self.emotionType == 0) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setEnabled:NO];
            [button setBackgroundColor:[UIColor clearColor]];
            [button setFrame:CGRectMake(0, 0, _emotionImageViewSize, _emotionImageViewSize)];
            [self.contentView addSubview:button];
            _emotionButton = button;
            
        } else {
            UIImageView *emotionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _emotionImageViewSize, _emotionImageViewSize)];
            emotionImageView.backgroundColor = [UIColor clearColor];
            [self.contentView addSubview:emotionImageView];
            self.emotionImageView = emotionImageView;
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (id)initWithFrame:(CGRect)frame {
    
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)dealloc {
    self.emotion = nil;
}

@end
