//
//  XHMessageTableViewCell.m
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-4-24.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import "XHMessageTableViewCell.h"
#import "LocalizationSystem.h"
#import "UIImageView+AFNetworking.h"
#import "AFImageDownloader.h"
#import "XHMessageAvatarFactory.h"
#import "IXBORequestMgr.h"

NSString    * const XHMessageTableViewCellWillShowMenu = @"XHMessageTableViewCellWillShowMenu";

static const CGFloat kXHLabelPadding = 15.0f;
static const CGFloat kXHTimeStampLabelHeight = 18.0f;

static const CGFloat kXHAvatarPaddingX = 10.0;
static const CGFloat kXHAvatarPaddingY = 8;

static const CGFloat kXHUserNameLabelHeight = 20;

@interface XHMessageTableViewCell () {
    
}

@property (nonatomic, weak, readwrite) XHMessageBubbleView *messageBubbleView;

//@property (nonatomic, weak, readwrite) UIButton *avatarButton;

@property (nonatomic, weak, readwrite) UIImageView  *avatarImgView;


@property (nonatomic, weak, readwrite) UILabel *userNameLabel;

@property (nonatomic, weak, readwrite) LKBadgeView *timestampLabel;

/**
 *  是否显示时间轴Label
 */
@property (nonatomic, assign) BOOL displayTimestamp;

/**
 *  1、是否显示Time Line的label
 *
 *  @param message 需要配置的目标消息Model
 */
- (void)configureTimestamp:(BOOL)displayTimestamp atMessage:(id <XHMessageModel>)message;

/**
 *  2、配置头像
 *
 *  @param message 需要配置的目标消息Model
 */
- (void)configAvatarWithMessage:(id <XHMessageModel>)message;

/**
 *  3、配置需要显示什么消息内容，比如语音、文字、视频、图片
 *
 *  @param message 需要配置的目标消息Model
 */
- (void)configureMessageBubbleViewWithMessage:(id <XHMessageModel>)message;

/**
 *  头像按钮，点击事件
 *
 *  @param sender 头像按钮对象
 */
- (void)avatarButtonClicked:(id)sender;

/**
 *  发送失败按钮，点击事件
 *
 *  @param sender 发送失败按钮对象
 */
- (void)resendButtonClicked:(UIButton *)sender;

/**
 *  统一一个方法隐藏MenuController，多处需要调用
 */
- (void)setupNormalMenuController;

/**
 *  点击Cell的手势处理方法，用于隐藏MenuController的
 *
 *  @param tapGestureRecognizer 点击手势对象
 */
- (void)tapGestureRecognizerHandle:(UITapGestureRecognizer *)tapGestureRecognizer;

/**
 *  长按Cell的手势处理方法，用于显示MenuController的
 *
 *  @param longPressGestureRecognizer 长按手势对象
 */
- (void)longPressGestureRecognizerHandle:(UILongPressGestureRecognizer *)longPressGestureRecognizer;

/**
 *  单击手势处理方法，用于点击多媒体消息触发方法，比如点击语音需要播放的回调、点击图片需要查看大图的回调
 *
 *  @param tapGestureRecognizer 点击手势对象
 */
- (void)sigleTapGestureRecognizerHandle:(UITapGestureRecognizer *)tapGestureRecognizer;

/**
 *  双击手势处理方法，用于双击文本消息，进行放大文本的回调
 *
 *  @param tapGestureRecognizer 双击手势对象
 */
- (void)doubleTapGestureRecognizerHandle:(UITapGestureRecognizer *)tapGestureRecognizer;

@end

@implementation XHMessageTableViewCell

- (void)avatarButtonClicked:(id)sender {
    DLog(@"%s",__func__);
    if ([self.delegate respondsToSelector:@selector(didSelectedAvatarOnMessage:atIndexPath:)]) {
        [self.delegate didSelectedAvatarOnMessage:self.messageBubbleView.message atIndexPath:self.indexPath];
    }
}

- (void)resendButtonClicked:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickedResendOnMessage:atIndexPath:)]) {
        [self.delegate didClickedResendOnMessage:self.messageBubbleView.message atIndexPath:self.indexPath];
    }
}

#pragma mark - Copying Method

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)becomeFirstResponder {
    return [super becomeFirstResponder];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == @selector(copyed:) || action == @selector(deleteMessage:) || action == @selector(favorites:) || action == @selector(more:));
}

#pragma mark - Menu Actions

- (void)copyed:(id)sender {
    [[UIPasteboard generalPasteboard] setString:self.messageBubbleView.displayTextView.text];
    [self resignFirstResponder];
    DLog(@"Cell was copy");
}

- (void)transpond:(id)sender {
    DLog(@"Cell was transpond");
}

- (void)deleteMessage:(id)sender {
    DLog(@"Cell was delete");
    if ([self.delegate respondsToSelector:@selector(didClickedDeleteOnMessage:atIndexPath:)]) {
        [self.delegate didClickedDeleteOnMessage:self.messageBubbleView.message atIndexPath:self.indexPath];
    }
}

- (void)favorites:(id)sender {
    DLog(@"Cell was favorites");
}

- (void)more:(id)sender {
    DLog(@"Cell was more");
}

#pragma mark - Setters

- (void)configureCellWithMessage:(id <XHMessageModel>)message
               displaysTimestamp:(BOOL)displayTimestamp {
    
    // 1、是否显示Time Line的label
    [self configureTimestamp:displayTimestamp atMessage:message];
    
    
    // 3、配置用户名
    [self configUserNameWithMessage:message];
    
    // 4、配置需要显示什么消息内容，比如语音、文字、视频、图片
    [self configureMessageBubbleViewWithMessage:message];
    
    // 2、配置头像
    [self configAvatarWithMessage:message];
    
}

- (void)configureTimestamp:(BOOL)displayTimestamp atMessage:(id <XHMessageModel>)message {
    self.displayTimestamp = displayTimestamp;
    self.timestampLabel.hidden = !self.displayTimestamp;
    if (displayTimestamp) {
        NSString *dateText = nil;
        NSString *timeText = nil;
        
        NSDate *today = [NSDate date];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:-1];
        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:today options:0];
        
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:message.timestamp];
        NSDateComponents *todayComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:today];
        NSDateComponents *yesterdayComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:yesterday];
        
        if (dateComponents.year == todayComponents.year && dateComponents.month == todayComponents.month && dateComponents.day == todayComponents.day) {
            dateText = AMLocalizedString(@"Today", @"今天");
        } else if (dateComponents.year == yesterdayComponents.year && dateComponents.month == yesterdayComponents.month && dateComponents.day == yesterdayComponents.day) {
            dateText = AMLocalizedString(@"Yesterday", @"昨天");
        } else {
            dateText = [NSDateFormatter localizedStringFromDate:message.timestamp dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
        }
        timeText = [NSDateFormatter localizedStringFromDate:message.timestamp dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
        
        self.timestampLabel.text = [NSString stringWithFormat:@"%@ %@",dateText,timeText];
    }
}

- (void)configAvatarWithMessage:(id <XHMessageModel>)message {
    WEAKSELF;
    self.avatarImgView.image = [self getAvatarComplete:^(UIImage *img) {
        weakSelf.avatarImgView.image = img;
    }];
}

- (UIImage *)getAvatarComplete:(void(^)(UIImage *img))complete{
    if ([self isSender]) {
        NSString    * headUrl = [IXBORequestMgr shareInstance].headUrl;
        if (headUrl && headUrl.length > 0) {
            //获取本地文件
            NSString    * path = NSHomeDirectory();
            path = [path stringByAppendingString:@"/tmp/"];
            path = [path stringByAppendingString:[headUrl lastPathComponent]];
            
            BOOL exist = [[NSFileManager defaultManager] fileExistsAtPath:path];
            if (exist) {
                return [UIImage imageWithContentsOfFile:path];
            }
            
            //根据url获取图片
            NSURL   * url = [NSURL URLWithString:headUrl];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            AFImageDownloader *downloader = [UIImageView sharedImageDownloader];
            id <AFImageRequestCache> imageCache = downloader.imageCache;
            
            //Use the image from the image cache if it exists
            UIImage *cachedImage = [imageCache imageforRequest:request withAdditionalIdentifier:nil];
            
            if (cachedImage) {
                UIImage     * avatar = [XHMessageAvatarFactory avatarImageNamed:cachedImage messageAvatarType:XHMessageAvatarTypeCircle];
                [self writeImg:avatar toFile:path];
                return avatar;
            }else{
                [downloader downloadImageForURLRequest:request success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {
                    if (complete && [responseObject isKindOfClass:[UIImage class]]) {
                        UIImage * avatar = [XHMessageAvatarFactory avatarImageNamed:responseObject messageAvatarType:XHMessageAvatarTypeCircle];
                        complete(avatar);
                        
                        [self writeImg:avatar toFile:path];
                    }
                } failure:nil];
            }
            return GET_IMAGE_NAME(@"broadSide_photo_l");
        }
        
        return GET_IMAGE_NAME(@"broadSide_photo_l");
    }
    else{
        return GET_IMAGE_NAME(@"客服");
    }
}

- (void)writeImg:(UIImage *)img toFile:(NSString *)path{
    NSData  * data = UIImagePNGRepresentation(img);
    [data writeToURL:[NSURL fileURLWithPath:path] atomically:YES];
}

- (void)configUserNameWithMessage:(id <XHMessageModel>)message {
    //不需要显示用户名
    //    self.userNameLabel.text = [message sender];
}

- (void)configureMessageBubbleViewWithMessage:(id <XHMessageModel>)message {
    XHBubbleMessageMediaType currentMediaType = message.messageMediaType;
    for (UIGestureRecognizer *gesTureRecognizer in self.messageBubbleView.bubbleImageView.gestureRecognizers) {
        [self.messageBubbleView.bubbleImageView removeGestureRecognizer:gesTureRecognizer];
    }
    for (UIGestureRecognizer *gesTureRecognizer in self.messageBubbleView.bubblePhotoImageView.gestureRecognizers) {
        [self.messageBubbleView.bubblePhotoImageView removeGestureRecognizer:gesTureRecognizer];
    }
    switch (currentMediaType) {
        case XHBubbleMessageMediaTypePhoto:
        case XHBubbleMessageMediaTypeVideo:
        case XHBubbleMessageMediaTypeLocalPosition: {
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sigleTapGestureRecognizerHandle:)];
            [self.messageBubbleView.bubblePhotoImageView addGestureRecognizer:tapGestureRecognizer];
            break;
        }
        case XHBubbleMessageMediaTypeText:
        case XHBubbleMessageMediaTypeVoice:
        case XHBubbleMessageMediaTypeEmotion: {
            UITapGestureRecognizer *tapGestureRecognizer;
            if (currentMediaType == XHBubbleMessageMediaTypeText) {
                tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGestureRecognizerHandle:)];
            } else {
                tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sigleTapGestureRecognizerHandle:)];
            }
            tapGestureRecognizer.numberOfTapsRequired = (currentMediaType == XHBubbleMessageMediaTypeText ? 2 : 1);
            [self.messageBubbleView.bubbleImageView addGestureRecognizer:tapGestureRecognizer];
            break;
        }
        default:
            break;
    }
    [self.messageBubbleView configureCellWithMessage:message];
}

#pragma mark - Gestures

- (void)setupNormalMenuController {
    UIMenuController *menu = [UIMenuController sharedMenuController];
    if (menu.isMenuVisible) {
        [menu setMenuVisible:NO animated:YES];
    }
}

- (void)tapGestureRecognizerHandle:(UITapGestureRecognizer *)tapGestureRecognizer {
    [self updateMenuControllerVisiable];
}

- (void)updateMenuControllerVisiable {
    [self setupNormalMenuController];
}

- (void)longPressGestureRecognizerHandle:(UILongPressGestureRecognizer *)longPressGestureRecognizer {
    if (longPressGestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    NSArray *popMenuTitles = [[XHConfigurationHelper appearance] popMenuTitles];
    NSMutableArray *menuItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < popMenuTitles.count; i ++) {
        NSString *title = popMenuTitles[i];
        SEL action = nil;
        switch (i) {
            case 0: {
                if ([self.messageBubbleView.message messageMediaType] == XHBubbleMessageMediaTypeText) {
                    action = @selector(copyed:);
                }
                break;
            }
            case 1: {
                action = @selector(deleteMessage:);
                break;
            }
            case 2: {
                action = @selector(favorites:);
                break;
            }
            case 3: {
                action = @selector(more:);
                break;
            }
            default:
                break;
        }
        if (action) {
            UIMenuItem *item = [[UIMenuItem alloc] initWithTitle:title action:action];
            if (item) {
                [menuItems addObject:item];
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:XHMessageTableViewCellWillShowMenu object:self];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setMenuItems:menuItems];
    
    CGRect targetRect = [self convertRect:[self.messageBubbleView bubbleFrame]
                                 fromView:self.messageBubbleView];
    
    [menu setTargetRect:CGRectInset(targetRect, 0.0f, 4.0f) inView:self];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMenuWillShowNotification:)
                                                 name:UIMenuControllerWillShowMenuNotification
                                               object:nil];
    [menu setMenuVisible:YES animated:YES];
}

- (void)sigleTapGestureRecognizerHandle:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (tapGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [self setupNormalMenuController];
        if ([self.delegate respondsToSelector:@selector(multiMediaMessageDidSelectedOnMessage:atIndexPath:onMessageTableViewCell:)]) {
            [self.delegate multiMediaMessageDidSelectedOnMessage:self.messageBubbleView.message atIndexPath:self.indexPath onMessageTableViewCell:self];
        }
    }
}

- (void)doubleTapGestureRecognizerHandle:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (tapGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if ([self.delegate respondsToSelector:@selector(didDoubleSelectedOnTextMessage:atIndexPath:)]) {
            [self.delegate didDoubleSelectedOnTextMessage:self.messageBubbleView.message atIndexPath:self.indexPath];
        }
    }
}

#pragma mark - Notifications

- (void)handleMenuWillHideNotification:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIMenuControllerWillHideMenuNotification
                                                  object:nil];
}

- (void)handleMenuWillShowNotification:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIMenuControllerWillShowMenuNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMenuWillHideNotification:)
                                                 name:UIMenuControllerWillHideMenuNotification
                                               object:nil];
}

#pragma mark - Getters

- (XHBubbleMessageType)bubbleMessageType {
    return self.messageBubbleView.message.bubbleMessageType;
}


/** 返回消息是否为自己发送出的 */
- (BOOL)isSender{
    return (self.messageBubbleView.message.bubbleMessageType == XHBubbleMessageTypeSending);
}

+ (CGFloat)calculateCellHeightWithMessage:(id <XHMessageModel>)message
                        displaysTimestamp:(BOOL)displayTimestamp {
    
    // 第一，是否有时间戳的显示
    CGFloat timestampHeight = displayTimestamp ? (kXHTimeStampLabelHeight + kXHLabelPadding * 2) : 0;
    
    //不显示用户名
    CGFloat userInfoNeedHeight = kXHAvatarPaddingY*2 + kXHAvatarImageSize + timestampHeight;
    
    CGFloat bubbleMessageHeight = [XHMessageBubbleView calculateCellHeightWithMessage:message] + timestampHeight;
    
    return MAX(bubbleMessageHeight, userInfoNeedHeight);
}

#pragma mark - Life cycle

- (void)setup {
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    self.accessoryView = nil;
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognizerHandle:)];
    [recognizer setMinimumPressDuration:0.4f];
    [self addGestureRecognizer:recognizer];
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerHandle:)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (instancetype)initWithMessage:(id <XHMessageModel>)message
              displaysTimestamp:(BOOL)displayTimestamp
                reuseIdentifier:(NSString *)cellIdentifier {
    self = [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (self) {
        // 如果初始化成功，那就根据Message类型进行初始化控件，比如配置头像，配置发送和接收的样式
        
        // 1、是否显示Time Line的label
        if (!_timestampLabel) {
            UIColor *timestampLabelTextColor = [[XHConfigurationHelper appearance].messageTableStyle objectForKey:kXHMessageTableTimestampTextColorKey];
            if (!timestampLabelTextColor) {
                timestampLabelTextColor = [UIColor whiteColor];
            }
            UIColor *timestampBackgroundColor = [[XHConfigurationHelper appearance].messageTableStyle objectForKey:kXHMessageTableTimestampBackgroundColorKey];
            if (!timestampBackgroundColor) {
                timestampBackgroundColor = [UIColor colorWithWhite:0.734 alpha:1.000];
            }
            LKBadgeView *timestampLabel = [[LKBadgeView alloc] initWithFrame:CGRectMake(0, kXHLabelPadding, MDK_SCREEN_WIDTH, kXHTimeStampLabelHeight)];
            
            timestampLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
            timestampLabel.badgeColor = UIColorWithHex(0x4c6072, 0.21);
            timestampLabel.textColor = [UIColor whiteColor];
            timestampLabel.font = PF_MEDI(12);
            timestampLabel.center = CGPointMake(CGRectGetWidth([[UIScreen mainScreen] bounds]) / 2.0, timestampLabel.center.y);
            [self.contentView addSubview:timestampLabel];
            [self.contentView bringSubviewToFront:timestampLabel];
            _timestampLabel = timestampLabel;
        }
        
        // 2、配置头像
        CGRect avatarButtonFrame;
        CGFloat avatarButtonOriginY = self.displayTimestamp ? kXHTimeStampLabelHeight + 15*2 : kXHAvatarPaddingY;
        switch (message.bubbleMessageType) {
            case XHBubbleMessageTypeReceiving:
                avatarButtonFrame = CGRectMake(kXHAvatarPaddingX,
                                               avatarButtonOriginY,
                                               kXHAvatarImageSize,
                                               kXHAvatarImageSize);
                break;
            case XHBubbleMessageTypeSending:
                avatarButtonFrame = CGRectMake(CGRectGetWidth(self.bounds) - kXHAvatarImageSize - kXHAvatarPaddingX,
                                               avatarButtonOriginY,
                                               kXHAvatarImageSize,
                                               kXHAvatarImageSize);
                break;
            default:
                break;
        }
        
        UIImageView * avatarImgView = [[UIImageView alloc] initWithFrame:avatarButtonFrame];
        avatarImgView.contentMode = UIViewContentModeScaleAspectFill;
        avatarImgView.userInteractionEnabled = YES;
        [self.contentView addSubview:avatarImgView];
        
        UITapGestureRecognizer  * tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarButtonClicked:)];
        [avatarImgView addGestureRecognizer:tapAvatar];
        avatarImgView.image = [self getAvatarComplete:nil];
        self.avatarImgView = avatarImgView;
        
        //无需显示用户名
        BOOL    showUserName = message.shouldShowUserName;
        showUserName = NO;
        if (showUserName) {
            // 3、配置用户名
            UILabel *userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.avatarImgView.bounds) + 20, kXHUserNameLabelHeight)];
            userNameLabel.textAlignment = NSTextAlignmentCenter;
            userNameLabel.backgroundColor = [UIColor clearColor];
            userNameLabel.font = [UIFont systemFontOfSize:12];
            userNameLabel.textColor = [UIColor colorWithRed:0.140 green:0.635 blue:0.969 alpha:1.000];
            [self.contentView addSubview:userNameLabel];
            self.userNameLabel = userNameLabel;
        }
        
        // 4、配置需要显示什么消息内容，比如语音、文字、视频、图片
        if (!_messageBubbleView) {
            // bubble container
            XHMessageBubbleView *messageBubbleView = [[XHMessageBubbleView alloc] initWithFrame:CGRectZero message:message];
            [messageBubbleView.resendButton addTarget:self action:@selector(resendButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:messageBubbleView];
            [self.contentView sendSubviewToBack:messageBubbleView];
            self.messageBubbleView = messageBubbleView;
        }
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // 布局头像
    CGFloat layoutOriginY = self.displayTimestamp ? kXHTimeStampLabelHeight + 15*2 : kXHAvatarPaddingY;
    CGRect avatarButtonFrame = self.avatarImgView.frame;
    avatarButtonFrame.origin.y = layoutOriginY;
    avatarButtonFrame.origin.x = [self isSender] ? (CGRectGetWidth(self.bounds) - kXHAvatarPaddingX - kXHAvatarImageSize) : kXHAvatarPaddingX;
    self.avatarImgView.frame = avatarButtonFrame;
    
    //无需显示用户名
    BOOL    showUserName = self.messageBubbleView.message.shouldShowUserName;
    showUserName = NO;
    if (self.messageBubbleView.message.shouldShowUserName) {
        // 布局用户名
        self.userNameLabel.center = CGPointMake(CGRectGetMidX(avatarButtonFrame), CGRectGetMaxY(avatarButtonFrame) + CGRectGetMidY(self.userNameLabel.bounds));
    }
    
    // 布局消息内容的View
    CGFloat bubbleX = 0.0f;
    CGFloat offsetX = 0.0f;
    if ([self isSender]) {
        offsetX = kXHAvatarImageSize + kXHAvatarPaddingX + 5.f;
    } else {
        bubbleX = kXHAvatarImageSize + kXHAvatarPaddingX + 5.f;
    }
    CGFloat timeStampLabelNeedHeight = (self.displayTimestamp ? (kXHTimeStampLabelHeight + kXHLabelPadding*2 - 8) : 0);
    
    CGRect bubbleMessageViewFrame = CGRectMake(bubbleX,
                                               timeStampLabelNeedHeight,
                                               CGRectGetWidth(self.contentView.bounds) - bubbleX - offsetX,
                                               CGRectGetHeight(self.contentView.bounds) - timeStampLabelNeedHeight);
    self.messageBubbleView.frame = bubbleMessageViewFrame;
}

- (void)dealloc {
    _avatarImgView = nil;
    
    _timestampLabel = nil;
    _messageBubbleView = nil;
    _indexPath = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - TableViewCell

- (void)prepareForReuse {
    // 这里做清除工作
    [super prepareForReuse];
    self.messageBubbleView.displayTextView.text = nil;
    self.messageBubbleView.displayTextView.attributedText = nil;
    self.messageBubbleView.bubbleImageView.image = nil;
    self.messageBubbleView.emotionImageView.animatedImage = nil;
    self.messageBubbleView.animationVoiceImageView.image = nil;
    self.messageBubbleView.voiceDurationLabel.text = nil;
    self.messageBubbleView.bubblePhotoImageView.messagePhoto = nil;
    self.messageBubbleView.geolocationsLabel.text = nil;
    
    self.userNameLabel.text = nil;
    self.avatarImgView.image = nil;
    
    self.timestampLabel.text = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
