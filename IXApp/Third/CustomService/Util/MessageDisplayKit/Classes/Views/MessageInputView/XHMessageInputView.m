//
//  XHMessageInputView.m
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-4-24.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XHMessageInputView.h"

#import "NSString+MessageInputView.h"
#import "XHMacro.h"
#import "XHConfigurationHelper.h"
#import "LocalizationSystem.h"
#import "IXAppUtil.h"

@interface XHMessageInputView () <UITextViewDelegate>

@property (nonatomic, weak, readwrite) XHMessageTextView *inputTextView;

@property (nonatomic, weak, readwrite) UIButton *voiceChangeButton;

@property (nonatomic, weak, readwrite) UIButton *multiMediaSendButton;

@property (nonatomic, weak, readwrite) UIButton *faceSendButton;

@property (nonatomic, weak, readwrite) UIButton *holdDownButton;

/**
 *  是否取消錄音
 */
@property (nonatomic, assign, readwrite) BOOL isCancelled;

/**
 *  是否正在錄音
 */
@property (nonatomic, assign, readwrite) BOOL isRecording;

/**
 *  在切换语音和文本消息的时候，需要保存原本已经输入的文本，这样达到一个好的UE
 */
@property (nonatomic, copy) NSString *inputedText;

/**
 *  输入框内的所有按钮，点击事件所触发的方法
 *
 *  @param sender 被点击的按钮对象
 */
- (void)messageStyleButtonClicked:(UIButton *)sender;

/**
 *  当录音按钮被按下所触发的事件，这时候是开始录音
 */
- (void)holdDownButtonTouchDown;

/**
 *  当手指在录音按钮范围之外离开屏幕所触发的事件，这时候是取消录音
 */
- (void)holdDownButtonTouchUpOutside;

/**
 *  当手指在录音按钮范围之内离开屏幕所触发的事件，这时候是完成录音
 */
- (void)holdDownButtonTouchUpInside;

/**
 *  当手指滑动到录音按钮的范围之外所触发的事件
 */
- (void)holdDownDragOutside;

/**
 *  当手指滑动到录音按钮的范围之内所触发的时间
 */
- (void)holdDownDragInside;

#pragma mark - layout subViews UI
/**
 *  根据正常显示和高亮状态创建一个按钮对象
 *
 *  @param image   正常显示图
 *  @param hlImage 高亮显示图
 *
 *  @return 返回按钮对象
 */
- (UIButton *)createButtonWithImage:(UIImage *)image HLImage:(UIImage *)hlImage ;

/**
 *  根据输入框的样式类型配置输入框的样式和UI布局
 *
 *  @param style 输入框样式类型
 */
- (void)setupMessageInputViewBarWithStyle:(XHMessageInputViewStyle)style ;

/**
 *  配置默认参数
 */
- (void)setup ;

#pragma mark - Message input view
/**
 *  动态改变textView的高度
 *
 *  @param changeInHeight 动态的高度
 */
- (void)adjustTextViewHeightBy:(CGFloat)changeInHeight;

@end

@implementation XHMessageInputView

#pragma mark - Action

- (void)messageStyleButtonClicked:(UIButton *)sender {
    NSInteger index = sender.tag;
    switch (index) {
        case 0: {
            sender.selected = !sender.selected;
            if (sender.selected) {
                self.inputedText = self.inputTextView.text;
                self.inputTextView.text = @"";
                [self.inputTextView resignFirstResponder];
            } else {
                self.inputTextView.text = self.inputedText;
                self.inputedText = nil;
                [self.inputTextView becomeFirstResponder];
            }
            
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.holdDownButton.alpha = sender.selected;
            } completion:nil];
            
            if ([self.delegate respondsToSelector:@selector(didChangeSendVoiceAction:)]) {
                [self.delegate didChangeSendVoiceAction:sender.selected];
            }
            
            break;
        }
        case 1: {
            sender.selected = !sender.selected;
            self.voiceChangeButton.selected = !sender.selected;
            
            if (!sender.selected) {
                [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.holdDownButton.alpha = sender.selected;
                    self.inputTextView.alpha = !sender.selected;
                } completion:nil];
            } else {
                [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.holdDownButton.alpha = !sender.selected;
                    self.inputTextView.alpha = sender.selected;
                } completion:nil];
            }
            
            if ([self.delegate respondsToSelector:@selector(didSendFaceAction:)]) {
                [self.delegate didSendFaceAction:sender.selected];
            }
            break;
        }
        case 2: {
            self.faceSendButton.selected = NO;
            if ([self.delegate respondsToSelector:@selector(didSelectedMultipleMediaAction)]) {
                [self.delegate didSelectedMultipleMediaAction];
            }
            break;
        }
        default:
            break;
    }
}

- (void)holdDownButtonTouchDown {
    if (![IXAppUtil microphoneEnable]) {
        return ;
    }
    
    self.isCancelled = NO;
    self.isRecording = NO;
    if ([self.delegate respondsToSelector:@selector(prepareRecordingVoiceActionWithCompletion:)]) {
        WEAKSELF
        
        //這邊回調 return 的 YES, 或 NO, 可以讓底層知道該次錄音是否成功, 進而處理無用的 record 對象
        [self.delegate prepareRecordingVoiceActionWithCompletion:^BOOL{
            STRONGSELF
            
            //這邊要判斷回調回來的時候, 使用者是不是已經早就鬆開手了
            if (strongSelf && !strongSelf.isCancelled) {
                strongSelf.isRecording = YES;
                [strongSelf.delegate didStartRecordingVoiceAction];
                return YES;
            } else {
                return NO;
            }
        }];
    }
}

- (void)holdDownButtonTouchUpOutside {
    
    //如果已經開始錄音了, 才需要做取消的動作, 否則只要切換 isCancelled, 不讓錄音開始.
    if (self.isRecording) {
        if ([self.delegate respondsToSelector:@selector(didCancelRecordingVoiceAction)]) {
            [self.delegate didCancelRecordingVoiceAction];
        }
    } else {
        self.isCancelled = YES;
    }
}

- (void)holdDownButtonTouchUpInside {
    
    //如果已經開始錄音了, 才需要做結束的動作, 否則只要切換 isCancelled, 不讓錄音開始.
    if (self.isRecording) {
        if ([self.delegate respondsToSelector:@selector(didFinishRecoingVoiceAction)]) {
            [self.delegate didFinishRecoingVoiceAction];
        }
    } else {
        self.isCancelled = YES;
    }
}

- (void)holdDownDragOutside {
    
    //如果已經開始錄音了, 才需要做拖曳出去的動作, 否則只要切換 isCancelled, 不讓錄音開始.
    if (self.isRecording) {
        if ([self.delegate respondsToSelector:@selector(didDragOutsideAction)]) {
            [self.delegate didDragOutsideAction];
        }
    } else {
        self.isCancelled = YES;
    }
}

- (void)holdDownDragInside {
    
    //如果已經開始錄音了, 才需要做拖曳回來的動作, 否則只要切換 isCancelled, 不讓錄音開始.
    if (self.isRecording) {
        if ([self.delegate respondsToSelector:@selector(didDragInsideAction)]) {
            [self.delegate didDragInsideAction];
        }
    } else {
        self.isCancelled = YES;
    }
}

#pragma mark - layout subViews UI

- (UIButton *)createButtonWithImage:(UIImage *)image HLImage:(UIImage *)hlImage {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [XHMessageInputView textViewLineHeight], [XHMessageInputView textViewLineHeight])];

    image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    if (image){
        [button setBackgroundImage:image forState:UIControlStateNormal];
    }
    if (hlImage){
        [button setBackgroundImage:hlImage forState:UIControlStateHighlighted];
    }
    return button;
}

- (void)setupMessageInputViewBarWithStyle:(XHMessageInputViewStyle)style {
    // 配置输入工具条的样式和布局
    
    //左右边距
    CGFloat lrPadding = 4.f;
    
    // 垂直间隔
    CGFloat verticalPadding = 7.f;
    
    // 每个按钮统一使用的frame变量
    CGRect buttonFrame;
    
    CGFloat btnHeight = [XHMessageInputView textViewLineHeight];
    CGFloat btnOriginY = self.frame.size.height - verticalPadding - btnHeight;
    
    // 按钮对象消息
    UIButton *button;
    
    BOOL    isNight = [IXUserInfoMgr shareInstance].isNightMode;
    
    // 允许发送语音
    if (self.allowsSendVoice) {
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,
                                                            [XHMessageInputView textViewLineHeight],
                                                            [XHMessageInputView textViewLineHeight])];
        
        [button setImage:AutoNightImageNamed(@"keyboard") forState:UIControlStateSelected];
        [button setImage:AutoNightImageNamed(@"voice") forState:UIControlStateNormal];
        [button setImage:AutoNightImageNamed(@"voice_HL") forState:UIControlStateHighlighted];

        [button addTarget:self action:@selector(messageStyleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 0;
        buttonFrame = button.frame;
        buttonFrame.origin = CGPointMake(lrPadding, btnOriginY);
        button.frame = buttonFrame;
        [self addSubview:button];
        
        self.voiceChangeButton = button;
    }
    
    // 允许发送多媒体消息，为什么不是先放表情按钮呢？因为布局的需要！
    if (self.allowsSendMultiMedia) {
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,
                                                            [XHMessageInputView textViewLineHeight],
                                                            [XHMessageInputView textViewLineHeight])];
        
        [button setImage:AutoNightImageNamed(@"keyboard") forState:UIControlStateSelected];
        [button setImage:AutoNightImageNamed(@"multiMedia") forState:UIControlStateNormal];
        [button setImage:AutoNightImageNamed(@"multiMedia_HL") forState:UIControlStateHighlighted];
        button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [button addTarget:self action:@selector(messageStyleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 2;
        buttonFrame = button.frame;
        buttonFrame.origin = CGPointMake(CGRectGetWidth(self.bounds) -
                                         lrPadding -
                                         CGRectGetWidth(buttonFrame),
                                         btnOriginY);
        button.frame = buttonFrame;
        [self addSubview:button];
        
        self.multiMediaSendButton = button;
    }
    
    // 允许发送表情
    if (self.allowsSendFace) {
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,
                                                            [XHMessageInputView textViewLineHeight],
                                                            [XHMessageInputView textViewLineHeight])];
        button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [button setImage:AutoNightImageNamed(@"keyboard") forState:UIControlStateSelected];
        [button setImage:AutoNightImageNamed(@"face") forState:UIControlStateNormal];
        [button setImage:AutoNightImageNamed(@"face_HL") forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(messageStyleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 1;
        buttonFrame = button.frame;
        buttonFrame.origin = CGPointMake(CGRectGetMinX(self.multiMediaSendButton.frame) - CGRectGetWidth(buttonFrame) - 4, btnOriginY);
        button.frame = buttonFrame;
        [self addSubview:button];
        
        self.faceSendButton = button;
    }
    
    // 输入框的高度和宽度
    CGFloat width = self.faceSendButton.frame.origin.x - self.voiceChangeButton.frame.origin.x - CGRectGetWidth(self.faceSendButton.bounds) - 14;
    CGFloat height = [XHMessageInputView textViewLineHeight];
    
    // 初始化输入框
    XHMessageTextView *textView = [[XHMessageTextView  alloc] initWithFrame:CGRectZero];
    textView.returnKeyType = UIReturnKeySend;
    textView.enablesReturnKeyAutomatically = YES;
    textView.font = PF_MEDI(13);
    textView.textColor = MarketSymbolNameColor;
    textView.dk_backgroundColorPicker = DKTableColor;
    textView.placeHolder = @"";
    textView.delegate = self;
    [self addSubview:textView];
    
    _inputTextView = textView;
    
    // 配置不同iOS SDK版本的样式
    switch (style) {
        case XHMessageInputViewStyleFlat: {
            UIColor *inputBackgroundColor = [XHConfigurationHelper appearance].messageInputViewStyle[kXHMessageInputViewBackgroundColorKey];
            NSString *inputViewBackgroundImageName = nil;
            if (!inputBackgroundColor) {
                inputViewBackgroundImageName = [[XHConfigurationHelper appearance].messageInputViewStyle objectForKey:kXHMessageInputViewBackgroundImageNameKey];
                if (!inputViewBackgroundImageName) {
                    if (isNight) {
                        inputViewBackgroundImageName = @"input-bar-flat_D";
                    }else{
                        inputViewBackgroundImageName = @"input-bar-flat";
                    }
                }
            }
            
            CGFloat x = self.voiceChangeButton.frame.origin.x + self.voiceChangeButton.bounds.size.width + 7.f;
            
            _inputTextView.frame = CGRectMake(x, 7.f, width, height);
            _inputTextView.dk_backgroundColorPicker = DKTableColor;
            _inputTextView.layer.dk_borderColorPicker = DKTextFBorderColor;
            _inputTextView.layer.borderWidth = 0.5;
            _inputTextView.layer.cornerRadius = 5.f;
            break;
        }
        default:
            break;
    }
    
    // 如果是可以发送语音的，那就需要一个按钮录音的按钮，事件可以在外部添加
    if (self.allowsSendVoice) {
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,
                                                            [XHMessageInputView textViewLineHeight],
                                                            [XHMessageInputView textViewLineHeight])];
        
        UIImage * image = AutoNightImageNamed(@"VoiceBtn_Black");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        image = AutoNightImageNamed(@"VoiceBtn_BlackHL");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        [button setBackgroundImage:image forState:UIControlStateHighlighted];
        
        [button setTitle:LocalizedString(@"HoldToTalk") forState:UIControlStateNormal];
        [button setTitle:LocalizedString(@"ReleaseToSend") forState:UIControlStateHighlighted];
        buttonFrame = CGRectMake(self.inputTextView.frame.origin.x-5, 0, width+10, self.frame.size.height);
        button.frame = buttonFrame;
        button.titleLabel.font = PF_MEDI(13);

        button.alpha = self.voiceChangeButton.selected;
        [button addTarget:self action:@selector(holdDownButtonTouchDown)
         forControlEvents:UIControlEventTouchDown];
        [button addTarget:self action:@selector(holdDownButtonTouchUpOutside)
         forControlEvents:UIControlEventTouchUpOutside];
        [button addTarget:self action:@selector(holdDownButtonTouchUpInside)
         forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(holdDownDragOutside)
         forControlEvents:UIControlEventTouchDragExit];
        [button addTarget:self action:@selector(holdDownDragInside)
         forControlEvents:UIControlEventTouchDragEnter];
       
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:MarketSymbolNameColor forState:UIControlStateNormal];
        
        [self addSubview:button];
        self.holdDownButton = button;
    }
}


- (void)relayoutBtnsFrame{
    // 垂直间隔
    CGFloat verticalPadding = 7.f;
    CGFloat btnHeight = [XHMessageInputView textViewLineHeight];
    CGFloat originY = self.frame.size.height - verticalPadding - btnHeight;
    
    CGRect  frame = self.voiceChangeButton.frame;
    frame.origin = CGPointMake(frame.origin.x, originY);
    self.voiceChangeButton.frame = frame;
    
    frame = self.faceSendButton.frame;
    frame.origin = CGPointMake(frame.origin.x, originY);
    self.faceSendButton.frame = frame;
    
    frame = self.multiMediaSendButton.frame;
    frame.origin = CGPointMake(frame.origin.x, originY);
    self.multiMediaSendButton.frame = frame;
}

#pragma mark - Life cycle

- (void)setup {
    // 配置自适应
    self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
    self.opaque = YES;
    // 由于继承UIImageView，所以需要这个属性设置
    self.userInteractionEnabled = YES;
    
    // 默认设置
    _allowsSendVoice = YES;
    _allowsSendFace = YES;
    _allowsSendMultiMedia = YES;
    
    _messageInputViewStyle = XHMessageInputViewStyleFlat;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
        self.dk_backgroundColorPicker = DKViewColor;
    }
    return self;
}

- (void)dealloc {
    self.inputedText = nil;
    _inputTextView.delegate = nil;
    _inputTextView = nil;
    
    _voiceChangeButton = nil;
    _multiMediaSendButton = nil;
    _faceSendButton = nil;
    _holdDownButton = nil;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    // 当别的地方需要add的时候，就会调用这里
    if (newSuperview) {
        [self setupMessageInputViewBarWithStyle:self.messageInputViewStyle];
    }
}

#pragma mark - Message input view

- (void)adjustTextViewHeightBy:(CGFloat)changeInHeight {
    // 动态改变自身的高度和输入框的高度
    CGRect prevFrame = self.inputTextView.frame;
    
    NSUInteger numLines = MAX([self.inputTextView numberOfLinesOfText],
                              [self.inputTextView.text numberOfLines]);
    
    self.inputTextView.frame = CGRectMake(prevFrame.origin.x,
                                          prevFrame.origin.y,
                                          prevFrame.size.width,
                                          prevFrame.size.height + changeInHeight);
    
    
    self.inputTextView.contentInset = UIEdgeInsetsMake((numLines >= 6 ? 4.0f : 0.0f),
                                                       0.0f,
                                                       (numLines >= 6 ? 4.0f : 0.0f),
                                                       0.0f);
    
    // from iOS 7, the content size will be accurate only if the scrolling is enabled.
    self.inputTextView.scrollEnabled = YES;
    
    if (numLines >= 6) {
        CGPoint bottomOffset = CGPointMake(0.0f, self.inputTextView.contentSize.height - self.inputTextView.bounds.size.height);
        [self.inputTextView setContentOffset:bottomOffset animated:YES];
        [self.inputTextView scrollRangeToVisible:NSMakeRange(self.inputTextView.text.length - 2, 1)];
    }
}

+ (CGFloat)textViewLineHeight {
    return 35.0f; // for fontSize 16.0f
}

+ (CGFloat)buttonHeight{
    return 30.f;
}

+ (CGFloat)maxLines {
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) ? 3.0f : 8.0f;
}

+ (CGFloat)maxHeight {
    return ([XHMessageInputView maxLines] + 1.0f) * [XHMessageInputView textViewLineHeight];
}

#pragma mark - Text view delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(inputTextViewWillBeginEditing:)]) {
        [self.delegate inputTextViewWillBeginEditing:self.inputTextView];
    }
    self.faceSendButton.selected = NO;
    self.voiceChangeButton.selected = NO;
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [textView becomeFirstResponder];
    if ([self.delegate respondsToSelector:@selector(inputTextViewDidBeginEditing:)]) {
        [self.delegate inputTextViewDidBeginEditing:self.inputTextView];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        if ([self.delegate respondsToSelector:@selector(didSendTextAction:)]) {
            [self.delegate didSendTextAction:textView.text];
            textView.text = @"";
        }
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if ([self.delegate respondsToSelector:@selector(inputTextDidChanged:)]) {
        [self.delegate inputTextDidChanged:textView.text];
    }
    
    return;
}

@end
