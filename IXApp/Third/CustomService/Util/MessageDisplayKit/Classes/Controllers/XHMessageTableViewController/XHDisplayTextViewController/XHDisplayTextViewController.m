//
//  XHDisplayTextViewController.m
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-5-6.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import "XHDisplayTextViewController.h"
#import "LocalizationSystem.h"


@interface XHDisplayTextViewController ()

@property (nonatomic, weak) UITextView *displayTextView;

@end

@implementation XHDisplayTextViewController

- (UITextView *)displayTextView {
    if (!_displayTextView) {
        UITextView *displayTextView = [[UITextView alloc] initWithFrame:self.view.frame];
        displayTextView.font = [UIFont systemFontOfSize:16.0f];
        displayTextView.textColor = [UIColor blackColor];
        displayTextView.userInteractionEnabled = YES;
        displayTextView.editable = NO;
        displayTextView.backgroundColor = [UIColor clearColor];
        displayTextView.dataDetectorTypes = UIDataDetectorTypeAll;
        [self.view addSubview:displayTextView];
        _displayTextView = displayTextView;
    }
    return _displayTextView;
}

- (void)setMessage:(id<XHMessageModel>)message {
    _message = message;
    self.displayTextView.text = [message text];
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];

    self.title = AMLocalizedString(@"TextDetail", @"文本消息");
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)onGoback{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)dealloc {
    self.displayTextView = nil;
}

@end
