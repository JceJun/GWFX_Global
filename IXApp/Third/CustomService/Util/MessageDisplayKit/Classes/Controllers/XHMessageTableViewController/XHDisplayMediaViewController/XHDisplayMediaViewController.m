//
//  XHDisplayMediaViewController.m
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-5-6.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import "XHDisplayMediaViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <SDWebImage/SDWebImageManager.h>

#import "XHConfigurationHelper.h"
#import "LocalizationSystem.h"
#import "IXZoomScrollView.h"

@interface XHDisplayMediaViewController ()

@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;

@property (nonatomic, strong) IXZoomScrollView  * photoImageView;

@end

@implementation XHDisplayMediaViewController

- (MPMoviePlayerController *)moviePlayerController {
    if (!_moviePlayerController) {
        _moviePlayerController = [[MPMoviePlayerController alloc] init];
        _moviePlayerController.repeatMode = MPMovieRepeatModeOne;
        _moviePlayerController.scalingMode = MPMovieScalingModeAspectFill;
        _moviePlayerController.view.frame = self.view.frame;
        [self.view addSubview:_moviePlayerController.view];
    }
    return _moviePlayerController;
}

- (IXZoomScrollView *)photoImageView {
    if (!_photoImageView) {
        IXZoomScrollView *photoImageView = [[IXZoomScrollView alloc] initWithFrame:self.view.frame];
        photoImageView.contentMode = UIViewContentModeScaleAspectFit;
        photoImageView.clipsToBounds = YES;
        [self.view addSubview:photoImageView];
        _photoImageView = photoImageView;
    }
    return _photoImageView;
}

- (void)setMessage:(id<XHMessageModel>)message {
    _message = message;
    if ([message messageMediaType] == XHBubbleMessageMediaTypeVideo) {
        self.title = AMLocalizedString(@"Video", @"详细视频");
        self.moviePlayerController.contentURL = [NSURL fileURLWithPath:[message videoPath]];
        [self.moviePlayerController play];
    } else if ([message messageMediaType] ==XHBubbleMessageMediaTypePhoto) {
        self.title = AMLocalizedString(@"Photo", @"详细照片");
        self.photoImageView.image = message.photo;
        
        NSURL   * imgUrl = nil;
        
        if (message.originPhotoUrl) {
            if ([message.originPhotoUrl rangeOfString:@"/Library"].location != NSNotFound) {
                //图片沙盒路径
                NSRange range = [message.originPhotoUrl rangeOfString:@"/Library"];
                NSString    * tmp = [(NSString *)message.originPhotoUrl substringFromIndex:range.location];
                NSString    * homePath = NSHomeDirectory();
                NSString    * imgPath = [homePath stringByAppendingString:tmp];
                BOOL    fileExist = [[NSFileManager defaultManager] fileExistsAtPath:imgPath];
                if (fileExist) {
                    imgUrl = [NSURL fileURLWithPath:imgPath];
                }
            }
            else if ([message.originPhotoUrl rangeOfString:@"http"].location != NSNotFound){
                if ([message.originPhotoUrl rangeOfString:@"_sm."].location != NSNotFound) {
                    imgUrl = [NSURL URLWithString:[message.originPhotoUrl stringByReplacingOccurrencesOfString:@"_sm" withString:@""]];
                }
                else{
                    imgUrl = [NSURL URLWithString:message.originPhotoUrl];
                }
            }
        }
        
        if (!message.photo && !imgUrl) {
            if (message.thumbnailUrl && [message.thumbnailUrl rangeOfString:@"http"].location != NSNotFound) {
                imgUrl = [NSURL URLWithString:message.thumbnailUrl];
            }
            else if ([message.text rangeOfString:@"http"].location != NSNotFound) {
                imgUrl = [NSURL URLWithString:message.text];
            }
        }
        
        if (imgUrl) {
            UIImage * plaeholderImg = message.photo;
            
            if (!plaeholderImg) {
                NSString *placeholderImageName = [[XHConfigurationHelper appearance].messageInputViewStyle objectForKey:kXHMessageTablePlaceholderImageNameKey];
                if (!placeholderImageName) {
                    placeholderImageName = @"placeholderImage";
                }
                plaeholderImg = [UIImage imageNamed:placeholderImageName];
            }

            [[SDWebImageManager sharedManager] downloadImageWithURL:imgUrl options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                [SVProgressHUD showProgress:1.f*receivedSize/expectedSize];
            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                [SVProgressHUD dismiss];
                self.photoImageView.image = image;
            }];
        }
    }
}

#pragma mark - Life cycle

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self.message messageMediaType] == XHBubbleMessageMediaTypeVideo) {
        [self.moviePlayerController stop];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
}

- (void)onGoback{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)dealloc {
    [_moviePlayerController stop];
    _moviePlayerController = nil;
    
    _photoImageView = nil;
}

@end
