//
//  UIImage+XHRounded.h
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-4-25.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (XHRounded)


/**
 创建带圆角图片

 @param radius 图片圆角半径，若为0，则创建圆形图片
 @return 矩形带圆角图片
 */
- (UIImage *)createRoundedWithRadius:(CGFloat)radius;

/** 根据长宽最小值截取并返回居中的最大圆形图片 */
- (UIImage *)circleImage;

/** 根据长宽最小值截取并返回居中的最大正方形图片 */
- (UIImage *)rectImage;

@end
