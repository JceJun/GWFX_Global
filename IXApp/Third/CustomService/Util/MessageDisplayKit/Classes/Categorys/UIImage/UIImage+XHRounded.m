//
//  UIImage+XHRounded.m
//  MessageDisplayExample
//
//  Created by HUAJIE-1 on 14-4-25.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import "UIImage+XHRounded.h"

@implementation UIImage (XHRounded)

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth,
                                 float ovalHeight) {
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM(context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth(rect) / ovalWidth;
    fh = CGRectGetHeight(rect) / ovalHeight;
    
    CGContextMoveToPoint(context, fw, fh/2);  // Start at lower right corner
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);  // Top right corner
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1); // Top left corner
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1); // Lower left corner
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); // Back to lower right
    
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

- (UIImage *)createRoundedWithRadius:(CGFloat)radius {
    // the size of CGContextRef
    UIImage *img = [self rectImage];
    
    int w = MIN(img.size.width, img.size.height);
    if (!radius) radius = w/2;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, w, w, 8, 4 * w, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);
    CGRect rect = CGRectMake(0, 0, w, w);
    
    CGContextBeginPath(context);
    addRoundedRectToPath(context, rect, radius, radius);
    CGContextClosePath(context);
    CGContextClip(context);
    CGContextDrawImage(context, CGRectMake(0, 0, w, w), img.CGImage);
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIImage *image = [UIImage imageWithCGImage:imageMasked];
    CGImageRelease(imageMasked);
    return image;
}

- (UIImage *)circleImage{
    return [self createRoundedWithRadius:0];
}

//剪切成方形image
- (UIImage *)rectImage{
    if (self.size.width == self.size.height) {
        return self;
    }
    
    CGImageRef  image_cg = [self CGImage];
    CGSize      imgSize = CGSizeMake(CGImageGetWidth(image_cg), CGImageGetHeight(image_cg));
    
    CGFloat imgW = imgSize.width;
    CGFloat imgH = imgSize.height;
    
    int width = (int)MIN(imgW, imgH);
    CGPoint purePoint = CGPointMake((imgW - width) / 2, (imgH - width) / 2);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image_cg, CGRectMake(purePoint.x, purePoint.y, width, width));
    UIImage * thumb = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return thumb;
}

@end
