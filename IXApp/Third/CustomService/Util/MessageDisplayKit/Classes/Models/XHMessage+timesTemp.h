//
//  XHMessage+timesTemp.h
//  IXApp
//
//  Created by Seven on 2017/4/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "XHMessage.h"


/** 给消息添加是否显示时间属性 */
@interface XHMessage (timesTemp)

@property (nonatomic, strong) NSNumber * showTimestamp;


@end
