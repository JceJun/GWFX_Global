//
//  XHMessage+timesTemp.m
//  IXApp
//
//  Created by Seven on 2017/4/25.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "XHMessage+timesTemp.h"
#import <objc/runtime.h>

const   NSString    * kShowTimestamp = @"showTimestamp";

@implementation XHMessage (timesTemp)


- (NSNumber *)showTimestamp{
    return objc_getAssociatedObject(self, &kShowTimestamp);
}

- (void)setShowTimestamp:(NSNumber *)show{
    objc_setAssociatedObject(self, &kShowTimestamp, show, OBJC_ASSOCIATION_RETAIN);
}

@end
