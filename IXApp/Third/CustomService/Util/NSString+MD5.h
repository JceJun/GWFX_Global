//
//  NSString+MD5.h
//  Pods
//
//  Created by Matt Miao on 5/5/2016.
//
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

+ (NSString *)md5:(NSString*)str;
@end
