//
//  AudioHelper.h
//  welive
//
//  Created by Matt Miao on 16/3/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioHelper : NSObject
- (void)convertFromWavToMp3:(NSString *)mp3Path WavPath:(NSString*)wavPath completionHandler:(void (^)(int, NSError *))handler;

@end
