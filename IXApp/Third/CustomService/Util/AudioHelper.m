//
//  AudioHelper.m
//  welive
//
//  Created by Matt Miao on 16/3/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import "AudioHelper.h"
#import "lame.h"

@implementation AudioHelper

- (void)convertFromWavToMp3:(NSString *)mp3Path WavPath:(NSString*)wavPath completionHandler:(void (^)(int, NSError *))handler {
    
    @try {
        int read, write;
        
        FILE *pcm = fopen([wavPath cStringUsingEncoding:1], "rb");  //source
        fseek(pcm, 4*1024, SEEK_CUR);                                   //skip file header
        FILE *mp3 = fopen([mp3Path cStringUsingEncoding:1], "wb");  //output
        
        const int PCM_SIZE = 8192;
        const int MP3_SIZE = 8192;
        short int pcm_buffer[PCM_SIZE*2];
        unsigned char mp3_buffer[MP3_SIZE];
        
        lame_t lame = lame_init();
        lame_set_in_samplerate(lame, 8000);
        lame_set_VBR(lame, vbr_default);
        lame_set_num_channels(lame, 0);
        
        lame_init_params(lame);
        
        do {
            read = fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
            if (read == 0)
                write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
            else
                write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
            
            fwrite(mp3_buffer, write, 1, mp3);
            
        } while (read != 0);
        
        lame_close(lame);
        fclose(mp3);
        fclose(pcm);
    }
    @catch (NSException *exception) {
        DLog(@"%@",[exception description]);
        handler(0, nil);
    }
    @finally {
        handler(1,nil);
    }
}

- (void)convertMp3Finish
{
    DLog(@"Finished");
}

@end
