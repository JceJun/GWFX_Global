//
//  ChatMessageVC.h
//  IXApp
//
//  Created by Seven on 2017/6/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CSLib/CSClient.h>

@interface ChatMessageVC : IXDataBaseVC

@property (nonatomic, strong) CSClient  * csClient;

@end
