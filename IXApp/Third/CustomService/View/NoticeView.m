//
//  NoticeView.m
//  welive
//
//  Created by Matt Miao on 9/3/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import "NoticeView.h"

@implementation NoticeView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    _noticeLabel = nil;
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, rect.size.width -4, rect.size.height - 4)];
    _noticeLabel = label;
    _noticeLabel.textAlignment = NSTextAlignmentCenter;
    _noticeLabel.text = self.notice;
    _noticeLabel.textColor = self.color;
    [self addSubview:_noticeLabel];
}




@end
