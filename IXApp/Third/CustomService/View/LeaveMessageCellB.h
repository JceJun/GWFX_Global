//
//  LeaveMessageCellB.h
//  IXApp
//
//  Created by Seven on 2017/6/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaveMessageCellB : UITableViewCell

@property (nonatomic, strong) UITextView    * textV;
@property (nonatomic, strong) UILabel       * titleLab;

- (void)setTitle:(NSString *)title placeHolder:(NSString *)placeHolder;

@end
