//
//  LeaveMessageCellB.m
//  IXApp
//
//  Created by Seven on 2017/6/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "LeaveMessageCellB.h"

@interface LeaveMessageCellB ()
<
UITextViewDelegate
>

@property (nonatomic, strong) UITextField   * textF;

@property (nonatomic, copy) NSString    * placeHolder;

@end

@implementation LeaveMessageCellB

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.textF];
        [self.contentView addSubview:self.textV];
        self.dk_backgroundColorPicker = DKViewColor;
    }
    return self;
}

- (void)setTitle:(NSString *)title placeHolder:(NSString *)placeHolder
{
    _titleLab.text = title;
    
    UIColor * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
    _textF.attributedPlaceholder = [[NSAttributedString alloc]
                                    initWithString:placeHolder
                                    attributes:@{NSForegroundColorAttributeName:phColor}];
    _placeHolder = placeHolder;
}


#pragma mark -
#pragma mark - text view

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0) {
        UIColor * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
        _textF.attributedPlaceholder = [[NSAttributedString alloc]
                                        initWithString:_placeHolder
                                        attributes:@{NSForegroundColorAttributeName:phColor}];
    }else{
        _textF.placeholder = nil;
    }
}


#pragma mark -
#pragma mark - lazy loading

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 87.5, 16)];
        _titleLab.font = PF_MEDI(13);
        _titleLab.textAlignment = NSTextAlignmentLeft;
        _titleLab.dk_textColorPicker = DKGrayTextColor;
        [self.contentView addSubview:_titleLab];
    }
    
    return _titleLab;
}

- (UITextField *)textF
{
    if (!_textF) {
        _textF = [[UITextField alloc] initWithFrame:CGRectMake(110, 8, kScreenWidth-102-10, 30)];
        _textF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textF.tintColor = MarketSymbolNameColor;
        _textF.dk_textColorPicker = DKCellTitleColor;
        _textF.font = PF_MEDI(13);
        _textF.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    return _textF;
}

- (UITextView *)textV
{
    if (!_textV) {
        _textV = [[UITextView alloc] initWithFrame:CGRectMake(106, 6, kScreenWidth-98-10, 205)];
        _textV.backgroundColor = [UIColor clearColor];
        _textV.delegate = self;
        _textV.font = PF_MEDI(13);
        _textV.dk_textColorPicker = DKCellTitleColor;
    }
    
    return _textV;
}

@end
