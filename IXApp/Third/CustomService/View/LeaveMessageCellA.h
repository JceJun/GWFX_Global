//
//  LeaveMessageCellA.h
//  IXApp
//
//  Created by Seven on 2017/6/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXTextField.h"

@interface LeaveMessageCellA : UITableViewCell

@property (nonatomic, strong) UILabel       * titleLab;
@property (nonatomic, strong) IXTextField   * textF;

- (void)setTitle:(NSString *)title placeHolder:(NSString *)placeHolder;

@end
