//
//  CommentStartView.m
//  welive
//
//  Created by Matt Miao on 8/3/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import "CommentStartView.h"


@interface CommentStartView()
{
    //星星的高度
    CGFloat starHeight;
    
    //宽度间距
    CGFloat spaceWidth;
    
    //星星总个数
    NSInteger totalNumber;
    
    //单个代表的评分
    CGFloat singlePoint;
    
    //最大分数
    NSInteger maxPoints;
    
    //星星的tag
    NSInteger starBaseTag;
    
    //填充的视图
    UIView *starView;
    
    //填充星星的偏移量
    CGFloat starOffset;
}
@end

@implementation CommentStartView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    //传进来的高度
    CGFloat height = rect.size.height;
    
    NSInteger space = 15;
    NSInteger totalStar = 5;
    CGFloat totalPoint = 10;
    //减去间距后的平均的宽度（我设置的星星 高度＝宽度）
    CGFloat averageHeight = (rect.size.width-space*(totalStar-1))/totalStar;
    
    if (height>averageHeight) {
        starHeight = averageHeight;
    }else{
        starHeight = height;
    }
    
    starBaseTag = 6666;
    spaceWidth = space;
    totalNumber = totalStar;
    singlePoint = totalPoint/totalStar;
    maxPoints = totalPoint;
    
    [self loadCustomViewWithTotal:totalStar];
    
}

- (void)loadCustomViewWithTotal:(NSInteger)totalStar
{
    //先铺背景图片（空的星星）
    for (int i =0 ; i<totalStar; i++) {        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.backgroundColor = [UIColor clearColor];
        imageButton.frame = CGRectMake(i*starHeight+i*spaceWidth, self.frame.size.height-starHeight, starHeight, starHeight);
        imageButton.tag = starBaseTag + i;
        [imageButton setBackgroundImage:[UIImage imageNamed:@"starBackImage"] forState:UIControlStateNormal];
        [imageButton addTarget:self action:@selector(starButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:imageButton];
        
    }
    
}

- (void)starButtonClicked:(UIButton*)sender
{
    [self.superview endEditing:YES];
    UIImage* starImage = AutoNightImageNamed(@"starImage");
    UIImage* starBackImage = AutoNightImageNamed(@"starBackImage");
    [sender setBackgroundImage:starImage forState:UIControlStateNormal];
    
    for (UIView* view in self.subviews) {
        if (view.tag >= starBaseTag && view.tag <= sender.tag) {
            UIButton* startButton = (UIButton*)view;
            [startButton setBackgroundImage:starImage forState:UIControlStateNormal];
            _commentPoint = view.tag - starBaseTag + 1;
            
        } else if (view.tag >= sender.tag && view.tag <= starBaseTag + 5) {
            UIButton* startButton = (UIButton*)view;
            [startButton setBackgroundImage:starBackImage forState:UIControlStateNormal];
        }
    }
}

//当你设置评分时 开始填充整颗星星
- (void)setCommentPoint:(CGFloat)commentPoint
{
    _commentPoint = commentPoint;
    if (commentPoint > maxPoints) {
        commentPoint = maxPoints;
    }
    
    CGFloat showNumber = commentPoint/singlePoint;
    
    //覆盖的长图
    if (!starView) {
        starView = [[UIView alloc]init];
    }
    
    starView.frame = CGRectZero;
    //整颗星星
    NSInteger fullNumber = showNumber/1;
    
    if (starOffset > 0) {
        starView.frame = CGRectMake(starOffset, self.frame.size.height-starHeight, starHeight*showNumber+spaceWidth*fullNumber, starHeight);
        
    }else{
        starView.frame = CGRectMake(0, self.frame.size.height-starHeight, starHeight*showNumber+spaceWidth*fullNumber, starHeight);
        
    }
    starView.clipsToBounds = YES;
    
    //在长图上填充完整的星星
    for (int j = 0; j< fullNumber; j++) {
        UIImageView *starImageView = [[UIImageView alloc]init];
        starImageView.image = [UIImage imageNamed:@"starImage"];
        starImageView.frame = CGRectMake(j*starHeight+j*spaceWidth, 0, starHeight, starHeight);
        [starView addSubview:starImageView];
    }
    
    CGFloat part = showNumber - fullNumber;
    //如果有残缺的星星 则添加
    if (part > 0) {
        UIImageView *partImage = [[UIImageView alloc]initWithFrame:CGRectMake(fullNumber*starHeight+fullNumber*spaceWidth, 0, starHeight, starHeight)];
        partImage.image = [UIImage imageNamed:@"starImage"];
        [starView addSubview:partImage];
    }
    
    [self addSubview:starView];
}
//设置星星的对齐方式
- (void)setStarAliment:(StarAliment)starAliment
{
    _starAliment = starAliment;
    
    switch (starAliment) {
            //居中对齐
        case StarAlimentCenter:
        {
            CGFloat starRealWidth = totalNumber*starHeight+(totalNumber-1)*spaceWidth;
            CGFloat leftWidth = self.frame.size.width-starRealWidth;
            
            for (int i =0 ; i< totalNumber; i++) {
                UIImageView *starImageView = (UIImageView*)[self viewWithTag:i+starBaseTag];
                starImageView.frame = CGRectMake(leftWidth/2+starImageView.frame.origin.x, starImageView.frame.origin.y, starImageView.frame.size.width, starImageView.frame.size.height);
            }
            starOffset = leftWidth/2;
            starView.frame = CGRectMake(leftWidth/2+starView.frame.origin.x, starView.frame.origin.y, starView.frame.size.width, starView.frame.size.height);
            
        }
            break;
            //右对齐
        case StarAlimentRight:
        {
            CGFloat starRealWidth = totalNumber*starHeight+(totalNumber-1)*spaceWidth;
            CGFloat leftWidth = self.frame.size.width-starRealWidth;
            
            for (int i =0 ; i< totalNumber; i++) {
                UIImageView *starImageView = (UIImageView*)[self viewWithTag:i+starBaseTag];
                starImageView.frame = CGRectMake(leftWidth+starImageView.frame.origin.x, starImageView.frame.origin.y, starImageView.frame.size.width, starImageView.frame.size.height);
            }
            starOffset = leftWidth;
            starView.frame = CGRectMake(leftWidth+starView.frame.origin.x, starView.frame.origin.y, starView.frame.size.width, starView.frame.size.height);
            
        }
            break;
            //默认的左对齐
        case StarAlimentDefault:
        {
            
            for (int i =0 ; i< totalNumber; i++) {
                UIImageView *starImageView = (UIImageView*)[self viewWithTag:i+starBaseTag];
                starImageView.frame = CGRectMake(i*starHeight+i*spaceWidth, self.frame.size.height-starHeight, starHeight, starHeight);
            }
            
            
            CGFloat showNumber = self.commentPoint/singlePoint;
            
            //整颗星星
            NSInteger fullNumber = showNumber/1;
            starOffset = 0;
            starView.frame = CGRectMake(0, self.frame.size.height-starHeight, starHeight*showNumber+spaceWidth*fullNumber, starHeight);
            
            
        }
            break;
        default:
        {
            
        }
            break;
    }
    
    
}

@end
