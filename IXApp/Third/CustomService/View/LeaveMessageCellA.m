//
//  LeaveMessageCellA.m
//  IXApp
//
//  Created by Seven on 2017/6/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "LeaveMessageCellA.h"

@implementation LeaveMessageCellA

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.textF];
        self.dk_backgroundColorPicker = DKViewColor;
    }
    return self;
}

- (void)setTitle:(NSString *)title placeHolder:(NSString *)placeHolder
{
    _titleLab.text = title;
    
    UIColor * phColor = AutoNightColor(0xe2e9f1, 0x303b4d);
    _textF.attributedPlaceholder = [[NSAttributedString alloc]
                                    initWithString:placeHolder
                                    attributes:@{NSForegroundColorAttributeName:phColor}];
}



#pragma mark -
#pragma mark - lazy loading

- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(14.5, 14, 87.5, 16)];
        _titleLab.font = PF_MEDI(13);
        _titleLab.textAlignment = NSTextAlignmentLeft;
        _titleLab.dk_textColorPicker = DKGrayTextColor;
        [self.contentView addSubview:_titleLab];
    }
    return _titleLab;
}

- (UITextField *)textF
{
    if (!_textF) {
        _textF = [[IXTextField alloc] initWithFrame:CGRectMake(110, 8, kScreenWidth-102-10, 30)];
        _textF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textF.tintColor = MarketSymbolNameColor;
        _textF.dk_textColorPicker = DKCellTitleColor;
        _textF.font = PF_MEDI(13);
    }
    return _textF;
}


@end
