////
////  CommentViewController.m
////  welive
////
////  Created by Matt Miao on 7/3/2016.
////  Copyright © 2016年 Matt Miao. All rights reserved.
////
//
#import "CommentViewController.h"
#import "CommentStartView.h"
#import "LocalizationSystem.h"

@interface CommentViewController ()
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet CommentStartView *commentStartView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionTitleLabel;

@property (assign, nonatomic) CGRect  originFrame;

@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAppearance:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    BOOL    isNight = [IXUserInfoMgr shareInstance].isNightMode;
    
    _titleTextView.text = AMLocalizedString(@"COMMENT_TITLE", nil);
    _titleTextView.dk_textColorPicker = DKCellContentColor;
    _titleTextView.font = PF_MEDI(13);
    
    _descriptionTitleLabel.text = AMLocalizedString(@"DESCRIPTION_LABEL_TITLE", nil);
    _descriptionTitleLabel.textColor = MarketSymbolNameColor;
    _descriptionTitleLabel.font = PF_MEDI(13);
    
    [_submitButton setTitle:AMLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_submitButton dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    _submitButton.titleLabel.font = PF_MEDI(13);
    
    [_cancelButton setTitle:AMLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [_cancelButton dk_setTitleColorPicker:DKCellContentColor forState:UIControlStateNormal];
    _cancelButton.titleLabel.font = PF_MEDI(13);
    
    self.commentView.dk_backgroundColorPicker = DKViewColor;
    self.commentView.layer.cornerRadius = 15;
    self.commentView.layer.dk_borderColorPicker = DKLineColor;
    self.commentView.layer.borderWidth = 0.5;
    
    self.descriptionTextView.layer.cornerRadius = 5;
    self.descriptionTextView.layer.borderWidth = 0.5;
    self.descriptionTextView.dk_backgroundColorPicker = DKTableColor;
    self.descriptionTextView.dk_textColorPicker = DKCellContentColor;
    self.descriptionTextView.font = PF_MEDI(13);
    if (!isNight) {
        self.descriptionTextView.layer.borderColor = MarketGrayPriceColor.CGColor;
    }else{
        self.descriptionTextView.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
    self.commentStartView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
    [self.view addGestureRecognizer:singleRecognizer];
    
    
}

- (void)singleTap
{
    [self.view endEditing:YES];
}

#pragma mark - keyboard response

- (void)keyboardWillAppearance:(NSNotification*)notif
{
    CGFloat keyboardHeight = [notif.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    CGFloat animationDuration = [notif.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    _originFrame = self.commentView.frame;
    CGRect  commentViewFrame = self.commentView.frame;
    
    if (CGRectGetMaxY(commentViewFrame) + keyboardHeight > kScreenHeight - 50) {
      
        [UIView animateWithDuration:animationDuration delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            CGFloat    y = commentViewFrame.origin.y - (CGRectGetMaxY(commentViewFrame) + keyboardHeight - (kScreenHeight - 64));
            
            self.commentView.frame = CGRectMake(commentViewFrame.origin.x,
                                                y,
                                                commentViewFrame.size.width,
                                                commentViewFrame.size.height);
            
        } completion:nil];
    }
}

- (void)keyboardWillHide:(NSNotification*)notif
{
    //CGSize size = [UIScreen mainScreen].bounds.size;
    CGFloat animationDuration = [notif.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:animationDuration delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.commentView.frame = _originFrame;
    } completion:nil];
}

- (IBAction)cancel:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didCancelComment)]) {
        [self.delegate didCancelComment];
    }
}
- (IBAction)submit:(id)sender
{
    NSInteger rate = (NSInteger)self.commentStartView.commentPoint;
    if ([self.delegate respondsToSelector:@selector(didSubmitComment:withRate:)]) {
        [self.delegate didSubmitComment:self.descriptionTextView.text withRate:rate];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
