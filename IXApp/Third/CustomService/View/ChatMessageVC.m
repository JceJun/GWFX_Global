//
//  ChatMessageVC.m
//  IXApp
//
//  Created by Seven on 2017/6/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "ChatMessageVC.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "LeaveMessageCellA.h"
#import "LeaveMessageCellB.h"
#import "LocalizationSystem.h"
#import "IXBORequestMgr.h"
#import "IXUserInfoM.h"
#import "IXAppUtil.h"

@interface ChatMessageVC ()
<
UITableViewDelegate,
UITableViewDataSource,
CSClientDelegate
>

@property (nonatomic, strong) UITableView   * tableV;
@property (nonatomic, strong) UIView        * tableFooterV;
@property (nonatomic, strong) NSArray       * titleArr;
@property (nonatomic, strong) NSMutableArray    * tfArr;

@end

@implementation ChatMessageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.dk_backgroundColorPicker = DKViewColor;
    self.navigationItem.leftBarButtonItem =
    [IXBaseNavVC getDefaultBackWithTarget:self sel:@selector(leftBtnItemClicked)];
    self.fd_interactivePopDisabled = YES;
    
    self.title = LocalizedString(@"留言客服");
    self.tfArr = [@[@"",@"",@"",@""] mutableCopy];
    
    _titleArr = @[
                  @[LocalizedString(@"姓名"),LocalizedString(@"请输入您的姓名")],
                  @[LocalizedString(@"电话"),LocalizedString(@"请输入您的联系方式")],
                  @[LocalizedString(@"邮箱"),LocalizedString(@"请输入您的邮箱地址")],
                  @[LocalizedString(@"内容描述"),LocalizedString(@"请简单描述您的问题或意见")]
                  ];
    [self.tableV reloadData];
    self.csClient.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SVProgressHUD showMessage:LocalizedString(@"客服不在线，请留言")];
}

- (void)leftBtnItemClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)submitBtnAction
{
    NSString* messageContent = [(UITextView *)[self.tfArr lastObject] text];
    NSString* messageName   = [(UITextField *)[self.tfArr objectAtIndex:0] text];
    NSString* messageEmail  = [(UITextField *)[self.tfArr objectAtIndex:2] text];
    NSString* messageTel    = [(UITextField *)[self.tfArr objectAtIndex:1] text];
    
    NSString* noticeString = LocalizedString(@"通知");
    NSString* noticeContentString = LocalizedString(@"请输入留言内容");
    NSString* confirmString = LocalizedString(@"确定");
    
    if ([messageName isEqualToString:@""]) {
        noticeContentString = AMLocalizedString(@"PLEASE_ADD_MESSAGE_NAME", @"请输入名字");
    }else if ([messageTel isEqualToString:@""]) {
        noticeContentString = AMLocalizedString(@"PLEASE_ADD_MESSAGE_TEL", @"请输入电话");
    }else if (messageTel.length < 5){
        noticeContentString = AMLocalizedString(@"PLEASE_ADD_LEGAL_TEL", @"手机号格式不正确");
    }else if ([messageEmail isEqualToString:@""]) {
        noticeContentString = AMLocalizedString(@"PLEASE_ADD_MESSAGE_EMAIL", @"请输入Email");
    }else if (![IXAppUtil isValidateEmail:messageEmail]){
        noticeContentString = AMLocalizedString(@"ILLEGAL_EMAIL", @"Email格式不正确");
    }else if ([messageContent isEqualToString:@""]) {
        noticeContentString = AMLocalizedString(@"PLEASE_ADD_MESSAGE_CONTENT", @"请输入留言内容");
    }else {
        [self.csClient submitLeaveMessage:messageContent
                                     Name:messageName
                                    Email:messageEmail
                                    Phone:messageTel];
        [self resignKeyboard];
        return;
    }
    
    UIAlertController* alert = [self getAlertControllerWithTitle:noticeString
                                                         message:noticeContentString
                                                   confirmButton:confirmString
                                                    cancelButton:nil
                                                         handler:nil];
    if (alert) {
        [self presentViewController:alert animated:YES completion:nil];
    }

}

#pragma mark -
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3) {
        return 215.f;
    }
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5.f;
}

static NSString * identA = @"LeaveMessageCellA";
static NSString * identB = @"LeaveMessageCellB";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * arr = _titleArr[indexPath.row];
    NSString    * title = [arr firstObject];
    NSString    * placeHolder = [arr lastObject];
    
    if ([title isEqualToString:LocalizedString(@"内容描述")]){
        LeaveMessageCellB   * cell = [tableView dequeueReusableCellWithIdentifier:identB forIndexPath:indexPath];
        if (!cell) {
            cell = [[LeaveMessageCellB alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identB];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setTitle:title placeHolder:placeHolder];
        [self.tfArr replaceObjectAtIndex:indexPath.row withObject:cell.textV];
        
        return cell;
    }else{
        LeaveMessageCellA   * cell = [tableView dequeueReusableCellWithIdentifier:identA forIndexPath:indexPath];
        if (!cell) {
            cell = [[LeaveMessageCellA alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identA];
        }
        cell.textF.font = PF_MEDI(13);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setTitle:title placeHolder:placeHolder];
        
        if (SameString(title, LocalizedString(@"电话"))){
            cell.textF.keyboardType = UIKeyboardTypeNumberPad;
            cell.textF.text = [IXBORequestMgr shareInstance].userInfo.detailInfo.mobilePhone;
            cell.textF.textFont = RO_REGU(15);
            cell.textF.placeHolderFont = PF_MEDI(13);
        }
        else if (SameString(title, LocalizedString(@"邮箱"))){
            cell.textF.keyboardType = UIKeyboardTypeEmailAddress;
            cell.textF.text = [IXBORequestMgr shareInstance].userInfo.detailInfo.email;
        }else{
            cell.textF.keyboardType = UIKeyboardTypeDefault;
            cell.textF.text = [IXBORequestMgr shareInstance].userInfo.detailInfo.chineseName;
        }
        
        [self.tfArr replaceObjectAtIndex:indexPath.row withObject:cell.textF];
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableV endEditing:YES];
}

-(void)client:(CSClient *)client didLeaveMsgFailed:(NSString *)info
{
    [self didSubmitLeaveMessage:0];
}


-(void)client:(CSClient *)client didLeaveMsgSuccess:(NSString *)info
{
    [self didSubmitLeaveMessage:1];
}

#pragma mark -
#pragma mark - other

- (UIAlertController*)getAlertControllerWithTitle:(NSString*)title message:(NSString*)message confirmButton:(NSString*)confirmButton cancelButton:cancelButton handler:(void (^)(UIAlertAction * action))handler
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* confirm = [UIAlertAction
                              actionWithTitle:confirmButton
                              style:UIAlertActionStyleDefault
                              handler:handler];
    [alert addAction:confirm];
    
    if (cancelButton) {
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:cancelButton
                                 style:UIAlertActionStyleCancel
                                 handler:nil];
        
        [alert addAction:cancel];
    }
    
    return alert;
}

- (void)didSubmitLeaveMessage:(NSInteger)result
{
    NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
    NSString* submitSuccessfullyString = AMLocalizedString(@"SUBMIT_MESSAGE_SUCCESSFULLY", nil);
    NSString* submitFailedString = AMLocalizedString(@"SUBMIT_MESSAGE_FAILED", nil);
    NSString* confirmString = AMLocalizedString(@"CONFIRM", nil);
    
    UIAlertController * alert = nil;
    
    if (result == 1) {
        alert = [self getAlertControllerWithTitle:noticeString
                                          message:submitSuccessfullyString
                                    confirmButton:confirmString
                                     cancelButton:nil
                                          handler:^(UIAlertAction * action){
                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                          }];
        
    }else {
        alert = [self getAlertControllerWithTitle:noticeString
                                          message:submitFailedString
                                    confirmButton:confirmString
                                     cancelButton:nil
                                          handler:nil];
    }
    
    if (alert) {
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)resignKeyboard
{
    for (UIView * v in self.tfArr) {
        [v resignFirstResponder];
    }
}

#pragma mark -
#pragma mark - lazy loading

- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                kScreenHeight - kNavbarHeight)
                                               style:UITableViewStylePlain];
        _tableV.dk_backgroundColorPicker = DKViewColor;
        _tableV.dk_separatorColorPicker = DKLineColor;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.tableFooterView = self.tableFooterV;
        _tableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        [self.view addSubview:_tableV];
        
        [_tableV registerClass:[LeaveMessageCellA class] forCellReuseIdentifier:identA];
        [_tableV registerClass:[LeaveMessageCellB class] forCellReuseIdentifier:identB];
    }
    
    return _tableV;
}

- (UIView *)tableFooterV
{
    if (!_tableFooterV) {
        _tableFooterV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,
                                                                 kScreenHeight -
                                                                 kNavbarHeight -
                                                                 44*3 - 215)];
        _tableFooterV.dk_backgroundColorPicker = DKViewColor;
        
        UIImage *image = AutoNightImageNamed(@"regist_btn_enable");
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];

        UIButton * submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        submitBtn.frame = CGRectMake(15.5, 44, kScreenWidth - 31, 44);
        submitBtn.titleLabel.font = PF_REGU(15);
        
        [submitBtn setBackgroundImage:image forState:UIControlStateNormal];
        [submitBtn setTitle:LocalizedString(@"提 交") forState:UIControlStateNormal];
        [submitBtn addTarget:self action:@selector(submitBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_tableFooterV addSubview:submitBtn];
        
        UITapGestureRecognizer  * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFooterViewAction)];
        [_tableFooterV addGestureRecognizer:tap];
    }
    
    return _tableFooterV;
}

- (void)tapFooterViewAction
{
    [self resignKeyboard];
}

@end
