//
//  ChatTableViewController.h
//  welive
//
//  Created by Matt Miao on 19/1/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XHMessageTableViewController.h"

@interface ChatTableViewController : XHMessageTableViewController


-(instancetype)initWithPid:(NSString*)pid key:(NSString*)key url:(NSString*)url;



@end
