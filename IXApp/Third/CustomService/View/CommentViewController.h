//
//  CommentViewController.h
//  welive
//
//  Created by Matt Miao on 7/3/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
////

#import <UIKit/UIKit.h>

@protocol CommentViewControllerDelegate;

@interface CommentViewController : UIViewController
@property (nonatomic, weak) id<CommentViewControllerDelegate> delegate;
@end

@protocol CommentViewControllerDelegate <NSObject>

@optional
- (void)didSubmitComment:(NSString*)comment withRate:(NSInteger)rate;

- (void)didCancelComment;

@end