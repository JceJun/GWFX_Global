//
//  NoticeView.h
//  welive
//
//  Created by Matt Miao on 9/3/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeView : UIView

@property(nonatomic, weak)UILabel *noticeLabel;

@property(nonatomic, copy)NSString* notice;

@property(nonatomic)UIColor* color;

@end
