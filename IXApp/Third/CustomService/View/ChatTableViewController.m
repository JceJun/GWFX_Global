//
//  ChatTableViewController.m
//  welive
//
//  Created by Matt Miao on 19/1/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import "ChatTableViewController.h"

#import "XHDisplayTextViewController.h"
#import "XHDisplayMediaViewController.h"
#import "XHDisplayLocationViewController.h"
#import "HistoryMessageService.h"
#import "IXSweetTitleV.h"

#import "XHAudioPlayerHelper.h"
#import <CSLib/CSClient.h>
#import "AudioHelper.h"

#import "VoiceConverter.h"
#import "CommentViewController.h"
#import "ChatMessageVC.h"

#import "NoticeView.h"
#import "Emoji.h"
#import "LocalizationSystem.h"
#import "IXCpyConfig.h"

typedef NS_ENUM(NSInteger,NoticeAnimationOption) {
    NoticeAnimationOptionPositionTop,       //先改变位置再删除
    NoticeAnimationOptionPositionLeft,
    NoticeAnimationOptionPositionRight,
    NoticeAnimationOptionPositionBottom,
    NoticeAnimationOptionScale,     //先缩小至width = 0，height = 0再删除
    NoticeAnimationOptionAlpha,     //改变透明度至alpha = 0再删除
};

@interface ChatTableViewController ()
<
CSClientDelegate,
CommentViewControllerDelegate,
UIAlertViewDelegate
>

@property (nonatomic, strong) HistoryMessageService     * historyMessageService;
@property (nonatomic, strong) XHMessageTableViewCell    * currentSelectedCell;
@property (nonatomic, strong) AudioHelper   * audioHelper;
@property (nonatomic, strong) NSArray   * emotionManagers;
@property (nonatomic, strong) CSClient  * csClient;
@property (nonatomic, strong) NSTimer   * noticeTimer;

@property (nonatomic, copy) NSString    * pid;
@property (nonatomic, copy) NSString    * key;
@property (nonatomic, copy) NSString    * url;
@property (nonatomic, copy) NSString    * assistantName;

@property (nonatomic) NSInteger noticeTag;

@property (nonatomic) BOOL isWaiting;
@property (nonatomic) BOOL isConnected;

@property (nonatomic, weak) UIAlertController* alertController;

@end

@implementation ChatTableViewController

-(instancetype)initWithPid:(NSString*)pid
{
    self = [super init];
    if (self) {
        [[LocalizationSystem sharedLocalSystem] setLanguage:@"en"];
        self.pid = pid;
    }
    return self;
}

-(instancetype)initWithPid:(NSString*)pid key:(NSString*)key url:(NSString *)url
{
    self = [super init];
    if (self) {
        self.pid = pid;
        self.key = key;
        self.url = url;
        _noticeTag = 200;
    }
    return self;
}

- (HistoryMessageService*)historyMessageService
{
    if (!_historyMessageService) {
        _historyMessageService = [[HistoryMessageService alloc] initWithPid:_pid];
    }
    return _historyMessageService;
}

- (CSClient*)csClient
{
    if (!_csClient) {
        _csClient = [CSClient sharedInstance];
        
        _csClient = [_csClient initWithPid:_pid Key:_key Url:_url Encrypt:YES];
        
        _csClient.delegate = self;
    }
    
    DLog(@"Version: %@", [_csClient getVersion]);
    return _csClient;
}


- (AudioHelper*)audioHelper
{
    if (!_audioHelper) {
        _audioHelper = [[AudioHelper alloc] init];
    }
    return _audioHelper;
}


- (void)connectToServer
{
    if (!_isConnected) {
        //设置客服端显示的用户信息
        NSString    * title = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
        if (!title.length) {
            title = [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo;
        }else{
            title = [title stringByAppendingFormat:@":%@",[IXUserInfoMgr shareInstance].userLogInfo.user.customerNo];
        }
        title = [title stringByAppendingFormat:@"@%d",CompanyID];
        
         [self.csClient setExtraInfoWithLoginName:title
                                         NickName:@""
                                        UserLevel:@""
                                            Phone:[IXUserInfoMgr shareInstance].userLogInfo.user.phone
                                      SpecifiedCS:@""];
        
        [self.csClient startConversation];
    }
}


- (void)closeConversation
{
    if (_isConnected) {
        _isWaiting = false;
        _isConnected = false;
        [self.csClient stopConversation];
    }
}


- (void)loadHistoryData
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSInteger timestamp = (NSInteger)([[NSDate date] timeIntervalSince1970]*1000);
        NSMutableArray *messages = [self.historyMessageService getHistoryMessagesWithTimestamp:timestamp size:10];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.messages = messages;
            [self.messageTableView reloadData];
            [self scrollToBottomAnimated:NO];
            if (self.messageTableView.contentSize.height <= self.messageTableView.bounds.size.height) {
                self.messageTableView.tableHeaderView = nil;
            }
        });
    });
}

- (void)addNoticeView:(NSString*)notice withSeconds:(NSInteger)time
{
    _noticeTag++;
    if (!_isWaiting) {
        NoticeView* noticeView = [[NoticeView alloc] initWithFrame:CGRectMake(0, -50, kScreenWidth, 50)];
        noticeView.backgroundColor = [UIColor lightTextColor];
        noticeView.notice = notice;
        noticeView.color = [UIColor redColor];
        noticeView.tag = _noticeTag;
        
        if (_noticeTimer) {
            [_noticeTimer invalidate];
            _noticeTimer = nil;
        }
        
        [self removeNoticeView:[self.view viewWithTag:_noticeTag-1]
                        option:NoticeAnimationOptionAlpha
                     animation:YES];
        [self.view addSubview:noticeView];
        
        _noticeTimer = [NSTimer scheduledTimerWithTimeInterval:time
                                                        target:self
                                                      selector:@selector(removeNoticeViewWithTimer:)
                                                      userInfo:[NSNumber numberWithInteger:_noticeTag]
                                                       repeats:NO];
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            noticeView.frame = CGRectMake(0, 0, kScreenWidth, 50);
        } completion:nil];
        
    } else {
        NoticeView* noticeView = [[NoticeView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
        noticeView.backgroundColor = [UIColor lightTextColor];
        noticeView.notice = notice;
        noticeView.color = [UIColor greenColor];
        noticeView.tag = _noticeTag;
        
        if (_noticeTimer) {
            [_noticeTimer invalidate];
            _noticeTimer = nil;
        }
        
        [self removeNoticeView:[self.view viewWithTag:_noticeTag-1]
                        option:NoticeAnimationOptionAlpha
                     animation:YES];
        
        [self.view addSubview:noticeView];
        
        _noticeTimer = [NSTimer scheduledTimerWithTimeInterval:time
                                                        target:self
                                                      selector:@selector(removeNoticeViewWithTimer:)
                                                      userInfo:[NSNumber numberWithInteger:_noticeTag]
                                                       repeats:NO];
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            noticeView.frame = CGRectMake(0, 50, kScreenWidth, 50);
        } completion:nil];
    }
}


- (void)removeNoticeViewWithTimer:(NSTimer*)timer
{
    NSNumber* tagNumber = [timer userInfo];
    
    [self removeNoticeView:[self.view viewWithTag:tagNumber.integerValue]
                    option:NoticeAnimationOptionPositionTop
                 animation:YES];
}

- (void)removeAllNoticeView
{
    for (UIView* view in [self.view subviews]) {
        if ([view isKindOfClass:[NoticeView class]]) {
            [self removeNoticeView:view
                            option:NoticeAnimationOptionPositionTop
                         animation:YES];
        }
    }
}

- (void)addWaitingNoticeLabelWith:(NSInteger)index;
{
    NoticeView* noticeView = [self.view viewWithTag:100];
    if (noticeView) {
        noticeView.color = [UIColor redColor];
        noticeView.notice = [NSString stringWithFormat:@"%@%ld%@",
                             AMLocalizedString(@"THERE_ARE", nil),
                             (long)(index+1),
                             AMLocalizedString(@"CUSTOMERS_ARE_WAITING", nil)];
        
        return;
    }
    
    noticeView = [[NoticeView alloc] initWithFrame:CGRectMake(0, - 50, self.view.bounds.size.width, 50)];
    noticeView.tag = 100;
    noticeView.backgroundColor = [UIColor lightTextColor];
    noticeView.color = [UIColor redColor];
    noticeView.notice = [NSString stringWithFormat:@"%@%ld%@",
                         AMLocalizedString(@"THERE_ARE", nil),
                         (long)(index+1),
                         AMLocalizedString(@"CUSTOMERS_ARE_WAITING", nil)];
    
    [self.view addSubview:noticeView];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        noticeView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 50);
    } completion:nil];
}

- (void)removeWaitingNoticeLabel
{
    [self removeNoticeView:[self.view viewWithTag:100]
                    option:NoticeAnimationOptionPositionTop
                 animation:YES];
}

/**
 删除提示
 
 @param view 需要删除的view
 @param animation 是否需要动效
 */
- (void)removeNoticeView:(UIView *)view option:(NoticeAnimationOption)option animation:(BOOL)animation
{
    if (!view || ![view isKindOfClass:[NoticeView class]]) return;
    
    if (!animation) {
        [view removeFromSuperview];
    }else {
        CGPoint origin = view.frame.origin;
        CGSize  size = view.frame.size;
        CGRect  aimFrame = CGRectZero;
        
        switch (option) {
            case NoticeAnimationOptionPositionTop:{
                aimFrame = CGRectMake(origin.x, - size.height, size.width, size.height);
                break;
            }
            case NoticeAnimationOptionPositionLeft:{
                aimFrame = CGRectMake(- size.width, origin.y, size.width, size.height);
                break;
            }
            case NoticeAnimationOptionPositionRight:{
                aimFrame = CGRectMake(kScreenWidth + size.width, origin.y, size.width, size.height);
                break;
            }
            case NoticeAnimationOptionPositionBottom:{
                aimFrame = CGRectMake(origin.x, kScreenHeight + size.height, size.width, size.height);
                break;
            }
            case NoticeAnimationOptionScale:{
                aimFrame = CGRectMake(origin.x + size.width / 2, origin.y + size.height / 2, size.width, size.height);
                break;
            }
            case NoticeAnimationOptionAlpha:{
                aimFrame = view.frame;
                break;
            }
            default:
                break;
        }
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            if (option == NoticeAnimationOptionAlpha) {
                view.alpha = 0;
            }else {
                view.frame = aimFrame;
            }
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
        }];
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        // 配置输入框UI的样式
        self.allowsSendVoice = YES;
        self.allowsSendFace = YES;
        self.allowsSendMultiMedia = YES;
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[XHAudioPlayerHelper shareInstance] stopAudio];
    if (_noticeTimer) {
        [_noticeTimer invalidate];
        [self removeAllNoticeView];
        _noticeTimer = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //connect websocket
    [self connectToServer];
    
    //去掉系统nabar底部自带的阴影线
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    // 设置自身用户名
    self.messageSender = [IXUserInfoMgr shareInstance].userLogInfo.user.name;
    self.navigationItem.leftBarButtonItem = [IXBaseNavVC getDefaultBackWithTarget:self
                                                                              sel:@selector(onGoback)];
    [self setTitle:LocalizedString(@"在线客服")];
    self.view.dk_backgroundColorPicker = DKTableColor;
    
    // 添加第三方接入数据
    NSMutableArray *shareMenuItems = [NSMutableArray array];
    NSArray *plugIcons = [IXUserInfoMgr shareInstance].isNightMode ? @[@"相片_D", @"拍照_D"] : @[@"相片", @"拍照"];
    NSArray *plugTitle = @[LocalizedString(@"照片"), LocalizedString(@"拍摄")];
    
    for (NSString *plugIcon in plugIcons) {
        NSString    * title = [plugTitle objectAtIndex:[plugIcons indexOfObject:plugIcon]];
        XHShareMenuItem *shareMenuItem = [[XHShareMenuItem alloc] initWithNormalIconImage:[UIImage imageNamed:plugIcon]
                                                                                    title:title];
        [shareMenuItems addObject:shareMenuItem];
    }
    self.shareMenuItems = shareMenuItems;
    [self initEmotion];
    
    //load History Data;
    [self loadHistoryData];
}

- (void)onGoback
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initEmotion
{
    NSMutableArray  *emotionManagers = [NSMutableArray array];
    
    //init emoji emotion
    XHEmotionManager    * emotionManager = [[XHEmotionManager alloc] init];
    NSMutableArray      * emotions = [NSMutableArray array];
    
    NSBundle    * bundle = [self getBundle];
    NSString    * plistPath = [bundle pathForResource:@"expression2" ofType:@"plist"];
    NSDictionary    * dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    NSArray * emojiArry = [dict objectForKey:@"emojiArray"];
    
    for (NSInteger j = 0; j < 140; j ++) {
        int page    = j / 28;
        int row     = j % 28 % 4;
        int line    = j % 28 / 4;
        int number  = (row * 7) + line + (page * 28) - (j / 28);
        
        XHEmotion *emotion  = [[XHEmotion alloc] init];
        emotion.emotionType = 0;
        emotion.emotionNumber = j;
        
        if (number >= emojiArry.count || j % 28 == 27) {
            emotion.emoji = @"";
            emotion.emotionType = 3;
            
            if (j%28 == 27){
                emotion.emotionType = 1;    //表示删除功能
                emotion.emotionConverPhoto = GET_IMAGE_NAME(@"backspace");
            }
        }else {
            emotion.emoji = [emojiArry objectAtIndex:number];
        }
        
        [emotions addObject:emotion];
    }
    
    emotionManager.emotions = emotions;
    [emotionManagers addObject:emotionManager];
    
    self.emotionManagers = emotionManagers;
    [self.emotionManagerView reloadData];
    
    [self.shareMenuView reloadData];
}


-(void)actionWhenAppDidBecomeActive:(NSNotification*)notif
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:UIApplicationDidBecomeActiveNotification
                                                 object:nil];
    [self connectToServer];
}

- (void)dealloc
{
    DLog(@"ChatTableViewController dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.csClient stopConversation];
    self.csClient.delegate = nil;
    _csClient = nil;
    _emotionManagers = nil;
}


#pragma mark -
#pragma mark - XHMessageTableViewCell delegate

- (void)multiMediaMessageDidSelectedOnMessage:(id<XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath onMessageTableViewCell:(XHMessageTableViewCell *)messageTableViewCell {
    UIViewController *disPlayViewController;
    switch (message.messageMediaType) {
        case XHBubbleMessageMediaTypeVideo:
        case XHBubbleMessageMediaTypePhoto: {
            
            XHDisplayMediaViewController *messageDisplayTextView = [[XHDisplayMediaViewController alloc] init];
            messageDisplayTextView.message = message;
            disPlayViewController = messageDisplayTextView;
            break;
        }
        case XHBubbleMessageMediaTypeVoice: {
            DLog(@"message : %@", message.voicePath);
            
            // Mark the voice as read and hide the red dot.
            message.isRead = YES;
            messageTableViewCell.messageBubbleView.voiceUnreadDotImageView.hidden = YES;
            
            [[XHAudioPlayerHelper shareInstance] setDelegate:(id<NSFileManagerDelegate>)self];
            if (_currentSelectedCell) {
                [_currentSelectedCell.messageBubbleView.animationVoiceImageView stopAnimating];
            }
            if (_currentSelectedCell == messageTableViewCell) {
                [messageTableViewCell.messageBubbleView.animationVoiceImageView stopAnimating];
                [[XHAudioPlayerHelper shareInstance] stopAudio];
                self.currentSelectedCell = nil;
            } else {
                self.currentSelectedCell = messageTableViewCell;
                [messageTableViewCell.messageBubbleView.animationVoiceImageView startAnimating];
                [[XHAudioPlayerHelper shareInstance] managerAudioWithFileName:message.voicePath toPlay:YES];
            }
            break;
        }
        case XHBubbleMessageMediaTypeEmotion:{
            DLog(@"facePath : %@", message.emotionPath);
            break;
        }
        case XHBubbleMessageMediaTypeLocalPosition: {
            DLog(@"facePath : %@", message.localPositionPhoto);
            XHDisplayLocationViewController *displayLocationViewController = [[XHDisplayLocationViewController alloc] init];
            displayLocationViewController.message = message;
            disPlayViewController = displayLocationViewController;
            break;
        }
        default:
            break;
    }
    
    if (disPlayViewController) {
        [self.navigationController pushViewController:disPlayViewController animated:YES];
    }
}

- (void)didDoubleSelectedOnTextMessage:(id<XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"text : %@", message.text);
    XHDisplayTextViewController *displayTextViewController = [[XHDisplayTextViewController alloc] init];
    displayTextViewController.message = message;
    [self.navigationController pushViewController:displayTextViewController animated:YES];
}

- (void)didSelectedAvatarOnMessage:(id<XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"indexPath : %@", indexPath);
}

- (void)didClickedResendOnMessage:(id<XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"didClickedResendOnMessage indexPath : %@", indexPath);
    [self.historyMessageService deleteMessage:(XHMessage*)message];
    [self removeMessageAtIndexPath:indexPath];
    
    XHBubbleMessageMediaType messageType = [message messageMediaType];
    
    switch (messageType) {
        case XHBubbleMessageMediaTypePhoto:{
            [self didSendPhoto:[message photo] fromSender:[message sender] onDate:[NSDate date]];
            break;
        }
        case XHBubbleMessageMediaTypeText:{
            [self didSendText:[message text] fromSender:[message sender] onDate:[NSDate date]];
            break;
        }
        case XHBubbleMessageMediaTypeVoice:{
            [self didSendVoice:[message voicePath] voiceDuration:[message voiceDuration] fromSender:[message sender] onDate:[NSDate date]];
            break;
        }
        default:
            break;
    }
}


- (void)didClickedDeleteOnMessage:(id <XHMessageModel>)message atIndexPath:(NSIndexPath *)indexPath
{
    [self.historyMessageService deleteMessage:(XHMessage*)message];
    
    [self removeMessageAtIndexPath:indexPath];
}

- (void)menuDidSelectedAtBubbleMessageMenuSelecteType:(XHBubbleMessageMenuSelecteType)bubbleMessageMenuSelecteType
{
    
}

#pragma mark -
#pragma mark - XHAudioPlayerHelper Delegate

- (void)didAudioPlayerStopPlay:(AVAudioPlayer *)audioPlayer
{
    if (!_currentSelectedCell) {
        return;
    }
    
    [_currentSelectedCell.messageBubbleView.animationVoiceImageView stopAnimating];
    self.currentSelectedCell = nil;
}

#pragma mark -
#pragma mark - XHEmotionManagerView DataSource

- (NSInteger)numberOfEmotionManagers
{
    return self.emotionManagers.count;
}

- (XHEmotionManager *)emotionManagerForColumn:(NSInteger)column
{
    return [self.emotionManagers objectAtIndex:column];
}

- (NSArray *)emotionManagersAtManager
{
    return self.emotionManagers;
}

#pragma mark -
#pragma mark - XHMessageTableViewController Delegate

- (BOOL)shouldLoadMoreMessagesScrollToTop
{
    return YES;
}

- (void)loadMoreMessagesScrollTotop
{
    if (!self.loadingMoreMessage) {
        self.loadingMoreMessage = YES;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            XHMessage* message = [self.messages firstObject];
            NSInteger timestamp =(NSInteger)([message.timestamp timeIntervalSince1970] * 1000);
            
            NSMutableArray *messages = [self.historyMessageService getHistoryMessagesWithTimestamp:timestamp size:6];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (messages.count > 0) {
                        [self insertOldMessages:messages];
                    }else {
                        DLog(@"get no more message");
                        [self noMoreEarlierMessage];
                    }
                    
                    self.loadingMoreMessage = NO;
                });
            });
        });
    }
}

/**
 *  发送文本消息的回调方法
 *
 *  @param text   目标文本字符串
 *  @param sender 发送者的名字
 *  @param date   发送时间
 */
- (void)didSendText:(NSString *)text fromSender:(NSString *)sender onDate:(NSDate *)date
{
    //check text size
    NSUInteger bytes = [text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    if (bytes > 960) {
        NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
        NSString* commentString = AMLocalizedString(@"TEXT_LENGTH_TOO_LONG", @"输入的消息过长");
        NSString* confirmString = AMLocalizedString(@"CONFIRM", nil);
        
        UIAlertController* alert = [self getAlertControllerWithTitle:noticeString message:commentString confirmButton:confirmString cancelButton:nil handler:nil];
        if (alert) {
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        return;
    }
    
    //send text message to server
    XHMessage *textMessage = [[XHMessage alloc] initWithText:text sender:sender timestamp:date];
    textMessage.sended = YES;
    textMessage.receiver = _assistantName;
    textMessage.senderRole = @"GUEST";
    textMessage.timing = @"TIMING_INPUTED";
    textMessage.shouldShowUserName = YES;
    textMessage.avatar = [UIImage imageNamed:@"avatar1"];
    textMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
    [self.historyMessageService addMessage:textMessage];
    [self addMessage:textMessage];
    [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeText];
    
    //[self.view endEditing:YES];
    NSBundle* bundle = [self getBundle];
    NSString *plistPath = [bundle pathForResource:@"expression" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    
    XHMessage *copyMessage = [textMessage copy];
    for (NSString* emoji in dict.allKeys) {
        copyMessage.text = [copyMessage.text stringByReplacingOccurrencesOfString:emoji withString:[dict valueForKey:emoji]];
    }
    
    [self.csClient sendTextMessage:copyMessage.text ID:@"test" Status:copyMessage.timing];
}

- (void)didInputText:(NSString*)text fromSender:(NSString *)sender onDate:(NSDate *)date
{
    //send text message to server
    XHMessage *textMessage = [[XHMessage alloc] initWithText:text sender:sender timestamp:date];
    
    textMessage.sended = YES;
    textMessage.senderRole = @"GUEST";
    textMessage.timing = @"TIMING_INPUTING";
    textMessage.shouldShowUserName = YES;
    textMessage.avatar = [UIImage imageNamed:@"avatar1"];
    textMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
    
    [self.csClient sendTextMessage:textMessage.text ID:@"test" Status:textMessage.timing];
}

/**
 *  发送图片消息的回调方法
 *
 *  @param photo  目标图片对象，后续有可能会换
 *  @param sender 发送者的名字
 *  @param date   发送时间
 */
- (void)didSendPhoto:(UIImage *)photo fromSender:(NSString *)sender onDate:(NSDate *)date
{
    XHMessage *photoMessage = [[XHMessage alloc] initWithPhoto:photo
                                                  thumbnailUrl:nil
                                                originPhotoUrl:nil
                                                        sender:sender
                                                     timestamp:date];
    
    NSString* orignalImageUrl = [self getImageSavePath];
    [UIImageJPEGRepresentation(photo, 0.5) writeToFile:orignalImageUrl atomically:YES];
    photoMessage.originPhotoUrl = orignalImageUrl;
    
    [self.csClient uploadImageFile:photo completionHandler:^(NSString *url, int result, NSError *error){
        if (result == 1) {
            photoMessage.sended = YES;
            photoMessage.text = url;
            photoMessage.senderRole = @"GUEST";
            photoMessage.timing = @"TIMING_INPUTED";
            photoMessage.shouldShowUserName = YES;
            [self.csClient sendPicMessage:photoMessage.text ID:@"test"];
        }else {
            photoMessage.sended = NO;
        }
        
        photoMessage.avatar = [UIImage imageNamed:@"avatar1"];
        photoMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
        [self.historyMessageService addMessage:photoMessage];
        [self addMessage:photoMessage];
        [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypePhoto];
    }];
}

/**
 *  发送视频消息的回调方法
 *
 *  @param videoPath 目标视频本地路径
 *  @param sender    发送者的名字
 *  @param date      发送时间
 */
- (void)didSendVideoConverPhoto:(UIImage *)videoConverPhoto videoPath:(NSString *)videoPath fromSender:(NSString *)sender onDate:(NSDate *)date {
    XHMessage *videoMessage = [[XHMessage alloc] initWithVideoConverPhoto:videoConverPhoto
                                                                videoPath:videoPath
                                                                 videoUrl:nil
                                                                   sender:sender
                                                                timestamp:date];
    
    videoMessage.avatar = [UIImage imageNamed:@"avatar"];
    videoMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
    [self.historyMessageService addMessage:videoMessage];
    [self addMessage:videoMessage];
    [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeVideo];
}

/**
 *  发送语音消息的回调方法
 *
 *  @param voicePath        目标语音本地路径
 *  @param voiceDuration    目标语音时长
 *  @param sender           发送者的名字
 *  @param date             发送时间
 */
- (void)didSendVoice:(NSString *)voicePath voiceDuration:(NSString *)voiceDuration fromSender:(NSString *)sender onDate:(NSDate *)date
{
    NSString* mp3Path =[self getMP3RecorderPath];
    
    if ([voiceDuration floatValue] < 1.f) {
        //too short
        NSError * error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:voicePath error:&error];
        if (error) {
            DLog(@"error : %@",error);
        }
        
        [SVProgressHUD showInfoWithStatus:LocalizedString(@"录音时间太短")];
        return;
    }
    
    WEAKSELF
    [self.audioHelper convertFromWavToMp3:mp3Path WavPath:voicePath completionHandler:^(int result, NSError *error) {
       
        STRONGSELF
        XHMessage *voiceMessage = [[XHMessage alloc] initWithVoicePath:voicePath
                                                              voiceUrl:nil
                                                         voiceDuration:voiceDuration
                                                                sender:sender
                                                             timestamp:date];
        
        if (result == 0) {
            voiceMessage.sended = NO;
            voiceMessage.avatar = [UIImage imageNamed:@"avatar1"];
            voiceMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
            [strongSelf addMessage:voiceMessage];
            [strongSelf finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeVoice];
        } else{
            STRONGSELF
            [weakSelf.csClient uploadVoiceFile:mp3Path completionHandler:^(NSString *url, int result, NSError *error){
                if (result == 1) {
                    voiceMessage.avatar = [UIImage imageNamed:@"avatar1"];
                    voiceMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
                    voiceMessage.text = url;
                    voiceMessage.sended = YES;
                    voiceMessage.senderRole = @"GUEST";
                    voiceMessage.timing = @"TIMING_INPUTED";
                    voiceMessage.shouldShowUserName = YES;
                    
                    [strongSelf.historyMessageService addMessage:voiceMessage];
                    [strongSelf addMessage:voiceMessage];
                    [strongSelf.csClient sendVoiceMessage:voiceMessage.text ID:@"test"];
                    [strongSelf finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeVoice];
                    
                } else{
                    voiceMessage.sended = NO;
                    voiceMessage.avatar = [UIImage imageNamed:@"avatar1"];
                    voiceMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
                    voiceMessage.shouldShowUserName = YES;
                    [strongSelf.historyMessageService addMessage:voiceMessage];
                    [strongSelf addMessage:voiceMessage];
                    [strongSelf finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeVoice];
                }
            }];
        }
    }];
}

/**
 *  发送第三方表情消息的回调方法
 *
 *  @param facePath 目标第三方表情的本地路径
 *  @param sender   发送者的名字
 *  @param date     发送时间
 */
- (void)didSendEmotion:(NSString *)emotionPath fromSender:(NSString *)sender onDate:(NSDate *)date
{
    XHMessage *emotionMessage = [[XHMessage alloc] initWithEmotionPath:emotionPath sender:sender timestamp:date];
    emotionMessage.avatar = [UIImage imageNamed:@"avatar"];
    emotionMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
    [self addMessage:emotionMessage];
    [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeEmotion];
}

/**
 *  有些网友说需要发送地理位置，这个我暂时放一放
 */
- (void)didSendGeoLocationsPhoto:(UIImage *)geoLocationsPhoto geolocations:(NSString *)geolocations location:(CLLocation *)location fromSender:(NSString *)sender onDate:(NSDate *)date
{
    XHMessage *geoLocationsMessage = [[XHMessage alloc] initWithLocalPositionPhoto:geoLocationsPhoto
                                                                      geolocations:geolocations
                                                                          location:location
                                                                            sender:sender
                                                                         timestamp:date];
    
    geoLocationsMessage.avatar = [UIImage imageNamed:@"avatar"];
    geoLocationsMessage.avatarUrl = @"http://www.pailixiu.com/jack/meIcon@2x.png";
    [self addMessage:geoLocationsMessage];
    [self finishSendMessageWithBubbleMessageType:XHBubbleMessageMediaTypeLocalPosition];
}

/**
 *  是否显示时间轴Label的回调方法
 *
 *  @param indexPath 目标消息的位置IndexPath
 *
 *  @return 根据indexPath获取消息的Model的对象，从而判断返回YES or NO来控制是否显示时间轴Label
 */
- (BOOL)shouldDisplayTimestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.row % 2 == 0);
}

/**
 *  协议回掉是否支持用户手动滚动
 *
 *  @return 返回YES or NO
 */
- (BOOL)shouldPreventScrollToBottomWhileUserScrolling
{
    return YES;
}

#pragma mark -
#pragma mark - CSClient Delegate

-(void)client:(CSClient *)client didLoginSuccess:(NSString *)csName
{
    _assistantName = csName;
    if (_isWaiting) {
        [self removeWaitingNoticeLabel];
        self.messageInputView.userInteractionEnabled = YES;
    }
    
    NSString* noticeString = [NSString stringWithFormat:@"%@%@%@",
                              AMLocalizedString(@"NOW_CUSTOMER", nil),
                              _assistantName,
                              AMLocalizedString(@"START_SERVE", nil)];
    
    [self addNoticeView:noticeString withSeconds:10];
    self.messageInputView.userInteractionEnabled = YES;
}


- (void)client:(CSClient*)client didReceiveMessage:(NSString*)message messageType:(NSString*)type
{
    NSDate* now = [NSDate date];
    XHMessage *receivedMessage;
    
    if ([type isEqualToString:@"text"]) {
        NSBundle* bundle = [self getBundle];
        NSString *plistPath = [bundle pathForResource:@"expressionreverse" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
        
        for (NSString* emoji in dict.allKeys) {
            message = [message stringByReplacingOccurrencesOfString:emoji
                                                         withString:[dict valueForKey:emoji]];
        }
        
        receivedMessage = [[XHMessage alloc] initWithText:message
                                                   sender:_assistantName
                                                timestamp:now];
        
    }else if ([type isEqualToString:@"img"]) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:message]]];
        receivedMessage = [[XHMessage alloc] initWithPhoto:image
                                              thumbnailUrl:nil
                                            originPhotoUrl:message
                                                    sender:_assistantName
                                                 timestamp:now];
    }else {
        return;
    }
    
    receivedMessage.bubbleMessageType = XHBubbleMessageTypeReceiving;
    receivedMessage.sender = _assistantName;
    receivedMessage.avatar = [UIImage imageNamed:@"avatar2"];
    receivedMessage.shouldShowUserName = NO;
    
    [self.historyMessageService addMessage:receivedMessage];
    [self addMessage:receivedMessage];
}

//排队等待
-(void)client:(CSClient *)client needWaitingWithIndex:(int)position
{
    _isWaiting = true;
    _isConnected = true;
    self.messageInputView.userInteractionEnabled = false;
    [self addWaitingNoticeLabelWith:(NSInteger)index];
}


//断开连接
- (void)client:(CSClient *)client didLostConnection:(NSString *)info
{
    NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
    NSString* lostConnectionString = AMLocalizedString(@"CONNECTION_LOST", nil);
    NSString* confirmString = AMLocalizedString(@"CONFIRM", nil);
    
    UIAlertController* alert = [self getAlertControllerWithTitle:noticeString
                                                         message:lostConnectionString
                                                   confirmButton:confirmString
                                                    cancelButton:nil
                                                         handler:^(UIAlertAction * action){
                                                             [self.navigationController popViewControllerAnimated:NO];
                                                         }];
    
    if (alert) {
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//请客户进行评价
- (void)client:(CSClient *)client didInviteCustomerToComment:(NSString *)info
{
    [self.view endEditing:YES];
    NSBundle* bundle = [self getBundle];
    CommentViewController* commentVC =  [[CommentViewController alloc] initWithNibName:@"CommentViewController"
                                                                                bundle:bundle];
    commentVC.delegate = self;
    [self addChildViewController:commentVC];
    commentVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:commentVC.view];
    
    return;
}

//切换到留言模式
-(void)client:(CSClient *)client noCustomerServiceAvailable:(NSString *)info
{
    ChatMessageVC   * vc = [ChatMessageVC new];
    vc.csClient = self.csClient;
    [self.navigationController pushViewController:vc animated:YES];
}

//被客服踢出聊天
-(void)client:(CSClient *)client didForceOffline:(NSString *)info
{
    NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
    NSString* forceOfflineString = AMLocalizedString(@"CUSTOMER_FORCE_OFFLINE", nil);
    NSString* confirmString = AMLocalizedString(@"CONFIRM", nil);
    
    UIAlertController* alert = [self getAlertControllerWithTitle:noticeString
                                                         message:forceOfflineString
                                                   confirmButton:confirmString
                                                    cancelButton:nil
                                                         handler:^(UIAlertAction * action){
                                                             DLog(@"force offline");
                                                             [self.navigationController popViewControllerAnimated:NO];
                                                         }];
    
    if (alert) {
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//客服已经退出聊天
-(void)client:(CSClient *)client didCSCloseConversation:(NSString *)info{
    NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
    NSString* customerLeaveConversationString = AMLocalizedString(@"CUSTOMER_LEAVE_CONVERSATION", nil);
    NSString* confirmString = AMLocalizedString(@"CONFIRM", nil);
    
    weakself;
    UIAlertController* alert = [self getAlertControllerWithTitle:noticeString
                                                         message:customerLeaveConversationString
                                                   confirmButton:confirmString
                                                    cancelButton:nil
                                                         handler:^(UIAlertAction * action){
                                                             weakSelf.messageInputView.userInteractionEnabled = NO;
                                                             [weakSelf.navigationController popViewControllerAnimated:YES];
                                                         }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    NSString* conversationClosedString = AMLocalizedString(@"CONVERSATION_HAS_CLOSED", nil);
    [self addNoticeView:conversationClosedString withSeconds:10];
}

//转接到其他客服
-(void)client:(CSClient *)client didTransferToOtherCS:(NSString *)csName
{
    _assistantName = csName;
    NSString* transferToString = AMLocalizedString(@"YOU_HAVE_BEEN_TRANSFER_TO", nil);
    NSString* startServeString = AMLocalizedString(@"START_SERVE", nil);
    
    NSString    * notice = [NSString stringWithFormat:@"%@，%@%@",
                            transferToString,
                            _assistantName,
                            startServeString];
    
    [self addNoticeView:notice withSeconds:10];
}

-(void)client:(CSClient *)client didFailTransferToOtherCustomer:(NSError *)error
{
    //转接失败，跳转留言页面
    [self client:client noCustomerServiceAvailable:nil];
}

//被禁言
-(void)client:(CSClient *)client didForbidSendMessage:(NSString *)info{
    NSString* startServeString = AMLocalizedString(@"YOU_HAVE_BEEN_FORBIDDEN", nil);
    [self addNoticeView:startServeString withSeconds:10];
    self.messageInputView.userInteractionEnabled = NO;
}

//禁言恢复
-(void)client:(CSClient *)client didRecoverSendMessage:(NSString *)info
{
    NSString* beForbiddenString = AMLocalizedString(@"YOU_HAVE_RECOVER_FORBIDDEN", nil);
    [self addNoticeView:beForbiddenString withSeconds:10];
    self.messageInputView.userInteractionEnabled = YES;
}

//客服掉线
-(void)client:(CSClient *)client didLeaveConversation:(NSString *)info
{
    NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
    NSString* transferString = AMLocalizedString(@"TRANSFER", nil);
    NSString* waitString = AMLocalizedString(@"WAIT", nil);
    NSString* customerLostConnectionString = AMLocalizedString(@"CUSTOMER_LOST_CONNECTION_PLEASE_WAIT", nil);
    
    self.messageInputView.userInteractionEnabled = NO;
    UIAlertController* alert = [self getAlertControllerWithTitle:noticeString
                                                         message:customerLostConnectionString
                                                   confirmButton:transferString
                                                    cancelButton:waitString
                                                         handler:^(UIAlertAction * action){
                                                             //请求转接 未实现
                                                             [self.csClient requestToTransfer];
                                                         }];
    _alertController = alert;
    
    if (alert) {
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self addNoticeView:customerLostConnectionString withSeconds:10];
    self.messageInputView.userInteractionEnabled = NO;
}

//客服回到对话
-(void)client:(CSClient *)client didCSBackToConversation:(NSString *)info
{
    NSString* customerReConnectedString = AMLocalizedString(@"CUSTOMER_RECONNECTED", nil);
    
    if (_alertController) {
        [_alertController dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self addNoticeView:customerReConnectedString withSeconds:10];
    self.messageInputView.userInteractionEnabled = YES;
}



- (void)didSubmitLeaveMessage:(NSInteger)result
{
    NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
    NSString* submitSuccessfullyString = AMLocalizedString(@"SUBMIT_MESSAGE_SUCCESSFULLY", nil);
    NSString* submitFailedString = AMLocalizedString(@"SUBMIT_MESSAGE_FAILED", nil);
    NSString* confirmString = AMLocalizedString(@"CONFIRM", nil);
    
    UIAlertController * alert = nil;
        
    if (result == 1) {
        alert = [self getAlertControllerWithTitle:noticeString
                                          message:submitSuccessfullyString
                                    confirmButton:confirmString
                                     cancelButton:nil
                                          handler:^(UIAlertAction * action){
                                              [self.navigationController popViewControllerAnimated:NO];
                                          }];
        
    }else {
        alert = [self getAlertControllerWithTitle:noticeString
                                          message:submitFailedString
                                    confirmButton:confirmString
                                     cancelButton:nil
                                          handler:nil];
    }
    
    if (alert) {
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)client:(CSClient *)client didLoginError:(NSError *)error
{
    DLog(@"error");
}

//利用正则表达式验证邮箱正确性
-(BOOL)isValidateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (NSString *)getAmrRecorderPath {
    NSString *recorderPath = nil;
    recorderPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex: 0];
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    recorderPath = [recorderPath stringByAppendingFormat:@"%@-MySound.amr", [dateFormatter stringFromDate:now]];
    return recorderPath;
}

- (NSString *)getImageSavePath {
    NSString *imageSavePath = nil;
    imageSavePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex: 0];
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    imageSavePath = [imageSavePath stringByAppendingFormat:@"%@-MyImage.jpg", [dateFormatter stringFromDate:now]];
    return imageSavePath;
}

- (NSString *)getMP3RecorderPath
{
    NSString *recorderPath = nil;
    recorderPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex: 0];
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    recorderPath = [recorderPath stringByAppendingFormat:@"%@-MySound.mp3", [dateFormatter stringFromDate:now]];
    return recorderPath;
}

- (NSBundle*)getBundle
{
    return [NSBundle mainBundle];
}

- (void)removeCommentViewFromSuperView
{
    for (UIViewController* vc in self.childViewControllers) {
        if ([vc isKindOfClass:[CommentViewController class]]) {
            [vc removeFromParentViewController];
            [vc.view removeFromSuperview];
        }
    }
}


#pragma mark -
#pragma mark - CommentViewController Delegate

- (void)didSubmitComment:(NSString *)comment withRate:(NSInteger)rate
{
    if (rate < 1) {
        NSString* noticeString = AMLocalizedString(@"NOTICE", nil);
        NSString* commentString = AMLocalizedString(@"PLEASE_COMMENT", @"请评价");
        NSString* confirmString = AMLocalizedString(@"CONFIRM", nil);
        
        UIAlertController* alert = [self getAlertControllerWithTitle:noticeString
                                                             message:commentString
                                                       confirmButton:confirmString
                                                        cancelButton:nil
                                                             handler:nil];
        if (alert) {
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else {
        [self.csClient submitComment:comment Rate:rate];
    }
}

- (void)didCancelComment
{
    [self removeCommentViewFromSuperView];
}

-(void)client:(CSClient *)client didCommentSuccess:(NSString *)info
{
    [self didSubmitComment:1];
}

-(void)client:(CSClient *)client didCommentFailed:(NSString *)info
{
    [self didSubmitComment:0];
}

#pragma mark -
#pragma mark - CommentService Delegate
- (void)didSubmitComment:(NSInteger)result
{
    NSString* submitSuccessfullyString = AMLocalizedString(@"COMMENT_SUCCESSFULLY", nil);
    NSString* submitFailedString = AMLocalizedString(@"COMMMENT_FAILED", nil);
    [self removeCommentViewFromSuperView];
    
    if (result == 1) {
        [self addNoticeView:submitSuccessfullyString withSeconds:10];
    }else {
        [self addNoticeView:submitFailedString withSeconds:10];
    }
}

- (UIAlertController*)getAlertControllerWithTitle:(NSString*)title message:(NSString*)message confirmButton:(NSString*)confirmButton cancelButton:cancelButton handler:(void (^)(UIAlertAction * action))handler
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* confirm = [UIAlertAction
                              actionWithTitle:confirmButton
                              style:UIAlertActionStyleDefault
                              handler:handler];
    [alert addAction:confirm];
    
    if (cancelButton) {
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:cancelButton
                                 style:UIAlertActionStyleCancel
                                 handler:nil];
        
        [alert addAction:cancel];
    }
    
    return alert;
}


-(void)client:(CSClient *)client didLeaveMsgFailed:(NSString *)info
{
    [self didSubmitLeaveMessage:0];
}


-(void)client:(CSClient *)client didLeaveMsgSuccess:(NSString *)info
{
    [self didSubmitLeaveMessage:1];
}

-(void)client:(CSClient *)client didSubmitWrongPid:(NSError *)error
{
    [self closeConversation];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark - TitleV

- (void)setTitle:(NSString *)title
{
    [self setTitleView:[self setDefaulttitleView:title] title:title];
}

- (UIView *)setDefaulttitleView:(NSString *)title
{
    UILabel *titleLbl = [IXCustomView createLable:CGRectMake(0, 0, 230, 40)
                                            title:title
                                             font:PF_MEDI(15)
                                       wTextColor:0x4c6072
                                       dTextColor:0xe9e9ea
                                    textAlignment:NSTextAlignmentCenter];
    
    return titleLbl;
}

- (void)setTitleView:(UIView *)titleView title:(NSString *)title
{
    [self.navigationItem setTitleView:titleView];
}

@end
