//
//  CommentStartView.h
//  welive
//
//  Created by Matt Miao on 8/3/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentStartView : UIView

typedef NS_ENUM(NSInteger,StarAliment) {
    StarAlimentDefault,
    StarAlimentCenter,
    StarAlimentRight
};

/**
 *  评分
 */
@property (nonatomic,assign) CGFloat commentPoint;
/**
 *  对齐方式
 */
@property (nonatomic,assign) StarAliment starAliment;

/**
 *  初始化方法
 *
 *  @param frame      整个星星视图的frame
 *  @param totalStar  总的星星的个数
 *  @param totalPoint 星星表示的总分数
 *  @param space      星星之间的间距
 *
 *  @return WQLStarView
 */
//- (instancetype)initWithFrame:(CGRect)frame withTotalStar:(NSInteger)totalStar withTotalPoint:(CGFloat)totalPoint starSpace:(NSInteger)space;



@end
