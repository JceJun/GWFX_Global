//
//  CSClient.h
//  Pods
//
//  Created by Matt Miao on 13/12/2016.
//
//

#import <Foundation/Foundation.h>


@class CSClient;
@protocol CSClientDelegate <NSObject>

@optional
//登录成功
- (void)client:(CSClient*)client didLoginSuccess:(NSString*)csName;

//登录失败
- (void)client:(CSClient*)client didLoginError:(NSError*)error;

//收到消息
- (void)client:(CSClient*)client didReceiveMessage:(NSString*)message messageType:(NSString*)type;

//发送成功与否
//- (void)client:(CSClient*)client didSendMessage:

//强迫下线
- (void)client:(CSClient*)client didForceOffline:(NSString*)info;

//禁言
- (void)client:(CSClient *)client didForbidSendMessage:(NSString *)info;

//取消禁言
- (void)client:(CSClient*)client didRecoverSendMessage:(NSString *)info;


//排队位置
- (void)client:(CSClient *)client needWaitingWithIndex:(int)position;

//断开连接
- (void)client:(CSClient *)client didLostConnection:(NSString *)info;

//客服关闭对话
- (void)client:(CSClient *)client didCSCloseConversation:(NSString *)info;

//客服暂时离开对话
- (void)client:(CSClient *)client didLeaveConversation:(NSString *)info;


//客服回到对话
- (void)client:(CSClient *)client didCSBackToConversation:(NSString *)info;


//客服邀请客户评价
- (void)client:(CSClient *)client didInviteCustomerToComment:(NSString *)info;

//被转接到其他客服
- (void)client:(CSClient *)client didTransferToOtherCS:(NSString *)csName;


//没有客服有空

- (void)client:(CSClient *)client noCustomerServiceAvailable:(NSString *)info;

//wrong pid
- (void)client:(CSClient *)client didSubmitWrongPid:(NSError*)error;

//转接失败
- (void)client:(CSClient *)client didFailTransferToOtherCustomer:(NSError*)error;



//留言回调

- (void)client:(CSClient *)client didLeaveMsgSuccess:(NSString *)info;
- (void)client:(CSClient *)client didLeaveMsgFailed:(NSString *)info;


//评价回调
- (void)client:(CSClient *)client didCommentSuccess:(NSString *)info;
- (void)client:(CSClient *)client didCommentFailed:(NSString *)info;

@end


@interface CSClient : NSObject


+(instancetype)sharedInstance;

-(instancetype)initWithPid:(NSString*)pid Key:(NSString*)key Url:(NSString*)url;

-(instancetype)initWithPid:(NSString*)pid Key:(NSString*)key Url:(NSString*)url Encrypt:(BOOL)isEncrypt;

-(void)setExtraInfoWithLoginName:(NSString*)loginName NickName:(NSString*)nickName UserLevel:(NSString*)userLevel Phone:(NSString*)phone SpecifiedCS:(NSString*)specifiedCs;

-(void)startConversation;

-(void)stopConversation;

-(void)sendTextMessage:(NSString*)message ID:(NSString*)msgid Status:(NSString*)status;


-(void)sendPicMessage:(NSString*)filePath ID:(NSString*)msgid;

-(void)sendVoiceMessage:(NSString*)filePath ID:(NSString*)msgid;

-(void)queryWaitingList;

-(void)submitLeaveMessage:(NSString*)message Name:(NSString*)name Email:(NSString*)email Phone:(NSString*)phone;

-(void)submitComment:(NSString*)comment Rate:(int)rate;

- (void)uploadImageFile:(UIImage*)image completionHandler:(void (^)(NSString *, int, NSError *))handler;
- (void)uploadVoiceFile:(NSString *)voicePath completionHandler:(void (^)(NSString *, int, NSError *))handler;

-(void)requestToTransfer;
- (int)reConnectToServer;

- (NSString*)getVersion;

-(BOOL)getIsEncrypt;



@property(nonatomic, weak)id<CSClientDelegate> delegate;

@end
