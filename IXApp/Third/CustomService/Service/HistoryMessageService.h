//
//  HistoryMessageService.h
//  welive
//
//  Created by Matt Miao on 29/2/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XHMessage.h"

#import "DBManager.h"

@interface HistoryMessageService : NSObject

- (instancetype)initWithPid:(NSString*)pid;




- (XHMessage*)addMessage:(XHMessage*)message;

- (void)deleteMessage:(XHMessage*)message;

- (XHMessage*)updateMessage:(XHMessage*)message;

- (NSMutableArray*)getHistoryMessagesWithTimestamp:(NSInteger)timestamp size:(NSInteger)size;

@end
