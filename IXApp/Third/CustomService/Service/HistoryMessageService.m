//
//  HistoryMessageService.m
//  welive
//
//  Created by Matt Miao on 29/2/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import "HistoryMessageService.h"
#import "XHMessage+timesTemp.h"

@interface HistoryMessageService ()
@property(nonatomic) DBManager* dbManager;

@property(nonatomic,copy) NSString* tableName;
@end

@implementation HistoryMessageService


- (instancetype)initWithPid:(NSString*)pid
{
    self = [super init];
    if (self) {
        self.tableName = [NSString stringWithFormat:@"A%@", pid];
    }
    
    return self;
}


- (DBManager*)dbManager
{
    if (!_dbManager) {
        _dbManager = [[DBManager alloc] initWithDatabaseFilename:@"historymessage.db" tableName:self.tableName];
    }
    return _dbManager;
}

- (XHMessage*)addMessage:(XHMessage*)message
{
    
    NSInteger time = (NSInteger)([message.timestamp timeIntervalSince1970]*1000);
    NSString* text = [message.text stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    NSString* query = [NSString stringWithFormat:@"insert into %@ values(null, '%@', %ld, '%@', %ld, %ld, %ld,'%@', %ld, %d, '%@', '%@', '%@', '%@')",
                       self.tableName,
                       message.sender,
                       (long)message.senderId,
                       message.receiver,
                       (long)message.receiverId,
                       (long)message.bubbleMessageType,
                       (long)message.messageMediaType,
                       text,
                       (long)time,
                       message.sended,
                       message.originPhotoUrl,
                       message.voicePath,
                       message.voiceDuration,
                       [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo];
    
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        message.mid = self.dbManager.lastInsertedRowID;
        return message;
    }else{
        ELog(@"Could not execute the query.");
        return  nil;
    }
}

- (void)deleteMessage:(XHMessage*)message
{
    //DELETE * FROM table_name;
    NSString* query = [NSString stringWithFormat:@"delete from %@ where id=%lld and customerNo=%@",
                       self.tableName,
                       [message mid],
                       [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo];
    
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        message.mid = self.dbManager.lastInsertedRowID;
    }else{
        ELog(@"Could not execute the query.");
    }
}

- (XHMessage*)updateMessage:(XHMessage*)message
{
    NSInteger time = (NSInteger)([message.timestamp timeIntervalSince1970]*1000);
    
    NSString* query = [NSString stringWithFormat:@"update %@ set sender='%@', senderId='%ld', receiver='%@', receiverId=%ld, msgType=%ld, msgContent='%@', dateTime=%ld,  where id=%lld",
                       self.tableName,
                       message.sender,
                       (long)message.senderId,
                       message.receiver,
                       (long)message.receiverId,
                       (long)message.bubbleMessageType,
                       message.text,
                       (long)time,
                       message.mid];
    
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        message.mid = self.dbManager.lastInsertedRowID;
        return message;
    }else{
        ELog(@"Could not execute the query.");
        return  nil;
    }
}

- (NSMutableArray*)getHistoryMessagesWithTimestamp:(NSInteger)timestamp size:(NSInteger)size
{
    // Create the query.
    NSString *query = [NSString stringWithFormat:@"select * from %@ where dateTime<%ld AND customerNo=%@ order by id desc limit %ld;",
                       self.tableName,
                       (long)timestamp,
                       [IXUserInfoMgr shareInstance].userLogInfo.user.customerNo,
                       (long)size];
    
    // Load the relevant data.
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    NSMutableArray *messageArray = [[NSMutableArray alloc] init];
    
    for (NSArray* object in results) {
        XHMessage* message  = [[XHMessage alloc] init];
        message.sender      = [object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"sender"]];
        message.mid         = [[object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]] integerValue];
        message.senderId    = [[object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"senderId"]] integerValue];
        message.receiver    = [object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"receiver"]];
        message.receiverId  = [[object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"receiverId"]] integerValue];
        message.bubbleMessageType   = [[object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"bubbleMessageType"]] integerValue];
        message.messageMediaType    = [[object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"msgType"]] integerValue];
        message.text    = [object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"msgContent"]];
        message.sended  = [[object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"sended"]] boolValue];
        
        if (message.messageMediaType == XHBubbleMessageMediaTypePhoto) {
            if (message.bubbleMessageType == XHBubbleMessageTypeSending) {
                message.originPhotoUrl = [object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"orignalImageUrl"]];
                UIImage *image  = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL fileURLWithPath:message.originPhotoUrl]]];
                message.photo   = image;
            }else{
                message.originPhotoUrl = [object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"orignalImageUrl"]];
                UIImage *image  = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:message.originPhotoUrl]]];
                message.photo   = image;
            }
        }else if (message.messageMediaType == XHBubbleMessageMediaTypeVoice) {
            message.voicePath = [object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"voicePath"]];
            message.voiceDuration = [object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"voiceDuration"]];
            message.isRead = YES;
        }

        if (message.bubbleMessageType == XHBubbleMessageTypeSending) {
            message.avatar = [UIImage imageNamed:@"avatar1"];
        }else{
            message.avatar = [UIImage imageNamed:@"avatar2"];
        }
        
        double timestamp = [[object objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"dateTime"] ] doubleValue];
        
        message.timestamp = [[NSDate alloc] initWithTimeIntervalSince1970:timestamp/1000.000];
        message.shouldShowUserName = YES;
        [messageArray addObject:message];
    }
    
    if (messageArray.count > 0) {
        messageArray = [[NSMutableArray alloc] initWithArray:[[messageArray reverseObjectEnumerator] allObjects]];
        
        __block NSInteger   timeInterval = 0;
        [messageArray enumerateObjectsUsingBlock:^(XHMessage *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.timestamp timeIntervalSince1970] - timeInterval > 210) {
                timeInterval = [obj.timestamp timeIntervalSince1970];
                obj.showTimestamp = @YES;
            }
        }];
    }
    
    return messageArray;
}



@end
