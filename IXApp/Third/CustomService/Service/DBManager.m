//
//  DBManager.m
//  welive
//
//  Created by Matt Miao on 25/2/2016.
//  Copyright © 2016年 Matt Miao. All rights reserved.
//

#import "DBManager.h"
#import <sqlite3.h>

@interface DBManager()
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
}

@property (nonatomic, strong) NSString *documentsDirectory;

@property (nonatomic, strong) NSString *databaseFilename;

@property (nonatomic, strong) NSMutableArray *arrResults;



-(void)copyDatabaseIntoDocumentsDirectory;

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;

@end


@implementation DBManager

#pragma mark - Initialization

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename tableName:(NSString *)tableName{
    self = [super init];
    if (self) {
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        
        NSString *myDocPath = [paths objectAtIndex:0];
        NSString *filename = [myDocPath stringByAppendingPathComponent:dbFilename];
        
        // Copy the database file into the documents directory if necessary.
        //[self copyDatabaseIntoDocumentsDirectory];
        [self createTable:filename tableName:tableName];
    }
    return self;
}


#pragma mark - Private method implementation

-(void)copyDatabaseIntoDocumentsDirectory{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            ELog([error localizedDescription]);
        }
    }
}

//创建数据库和表
-(int) createTable:(NSString*) filePath tableName:(NSString*)tableName
{
    //读写模式
    //若是数据库存在，则用sqlite3_open_v2直接打开（若是数据库不存在sqlite3_open_v2会主动创建）
    int resultCode = sqlite3_open_v2([filePath cStringUsingEncoding:NSUTF8StringEncoding],
                                     &sqlite3Database, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
    
    if (SQLITE_OK != resultCode) {
        sqlite3_close(sqlite3Database);
        ELog(@"打开数据库失败");
    }else{
        NSArray * columnArr = [self getColumnWithTable:tableName];
        
        __block BOOL exist = NO;
        [columnArr enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isEqualToString:@"customerNo"]) {
                exist = YES;
                *stop = YES;
            }
        }];
        
        //如果customerNo不存在,则添加此字段
        if (!exist && columnArr.count > 0) {
            //添加字段
            [self updateColumnWithTable:tableName column:@"customerNo"];
        }
        
        NSString* queryString = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( id INTEGER PRIMARY KEY AUTOINCREMENT,sender  TEXT, senderId INTEGER, receiver TEXT,receiverId INTEGER, bubbleMessageType INTEGER, msgType INTEGER, msgContent TEXT, dateTime INTEGER, sended INTEGER, orignalImageUrl TEXT, voicePath TEXT, voiceDuration TEXT, customerNo TEXT)", tableName];

        const char *query = [queryString UTF8String];

        char * errMsg;
        resultCode = sqlite3_exec(sqlite3Database, query,NULL,NULL,&errMsg);
        if (SQLITE_OK != resultCode) {
            NSString    * errorStr = [NSString stringWithFormat:@"创建表失败:%d, msg=%s",resultCode,errMsg];
            ELog(errorStr);
        }
        
        sqlite3_close(sqlite3Database);
    }
    
    return resultCode;
}



-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if (openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);
        if (prepareStatementResult == SQLITE_OK) {
            if (!queryExecutable){
                NSMutableArray *arrDataRow;
                
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    arrDataRow = [[NSMutableArray alloc] init];
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    for (int i=0; i<totalColumns; i++){
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);

                        if (dbDataAsChars != NULL) {
                            [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                        }
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }
                }
            }else{
                // This is the case of an executable query (insert, update, ...).
                // Execute the query.
                int executeQueryResults = sqlite3_step(compiledStatement);
                
                if (executeQueryResults == SQLITE_DONE) {
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                }else {
                    // If could not execute the query show the error message on the debugger.
                    NSString    * errorStr = [NSString stringWithFormat:@"DB Error: %s", sqlite3_errmsg(sqlite3Database)];
                    ELog(errorStr);
                }
            }
        }else{
            // In the database cannot be opened then show the error message on the debugger.
            NSString    * errorStr = [NSString stringWithFormat:@"%s",sqlite3_errmsg(sqlite3Database)];
            ELog(errorStr);
        }
        
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(sqlite3Database);
}

- (NSArray *)getColumnWithTable:(NSString *)tableName{
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    sqlite3 *database;
    sqlite3_open([databasePath UTF8String], &database);
    
    sqlite3_stmt *statement;
    
    NSMutableArray  * arr = [NSMutableArray array];
    
    //获取表中的字段名
    NSString   * sql = [NSString stringWithFormat:@"PRAGMA table_info(%@)",tableName];
    const char *getColumn = [sql cStringUsingEncoding:NSUTF8StringEncoding];
    sqlite3_prepare_v2(database, getColumn, -1, &statement, nil);
    
    while (sqlite3_step(statement) == SQLITE_ROW) {
        char *nameData = (char *)sqlite3_column_text(statement, 1);
        NSString *columnName = [[NSString alloc] initWithUTF8String:nameData];
        [arr addObject:columnName];
        DLog(@"columnName:%@",columnName);
    }
    return arr;
}

- (void)updateColumnWithTable:(NSString *)tableName column:(NSString *)column{
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    sqlite3 *database;
    sqlite3_open([databasePath UTF8String], &database);
    
    sqlite3_stmt *statement;
    
    NSString* sysMessageSql = [NSString stringWithFormat:@"alter table %@ add %@",tableName,column];
    const char * sql = [sysMessageSql cStringUsingEncoding:NSUTF8StringEncoding];
    sqlite3_prepare_v2(database, sql, -1, &statement, nil);
    
    int executeQueryResults = sqlite3_step(statement);
    if (executeQueryResults == SQLITE_DONE) {
        DLog(@"update table : %@ column : %@ success",tableName,column);
    }else{
        DLog(@"update table : %@ column : %@ failure",tableName,column);
    }
}

#pragma mark - Public method implementation

-(NSArray *)loadDataFromDB:(NSString *)query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}


-(void)executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

@end
