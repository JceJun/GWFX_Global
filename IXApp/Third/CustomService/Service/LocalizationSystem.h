//
//  LocalizationSystem.h
//  GovApp
//
//  Created by Son on 26/07/2011.
//  Copyright 2011 ice Carrot. All rights reserved.
//


#import <Foundation/Foundation.h>

#define EVT_LANGUAGE_CHANGED            @"evt-language-changed"

#define LANGUAGE_CN  @"zh-Hans"
#define LANGUAGE_TR  @"zh-Hant"
#define LANGUAGE_EN  @"en"

#define AMLocalizedString(key, comment) \
LocalizedString(key)

#define AMLocalized(key) \
[[LocalizationSystem sharedLocalSystem] localizedStringForKey:(key) value:@""]

#define AMAppendLangCode(string) \
[NSString stringWithFormat:@"%@_%@", string, AMLocalizedString(@"en", @"en")]

#define AMAppendLangCodeImg(string) \
[NSString stringWithFormat:@"%@_%@.png", string, AMLocalizedString(@"en", @"en")]

#define AMAppendLangCodeSound(string) \
[NSString stringWithFormat:@"%@%@", AMLocalizedString(@"sound_lan", @""), string]

#define AMLanguage() \
[[LocalizationSystem sharedLocalSystem] getLanguage]

/* NOTE:
 key
 The key for a string in the table identified by tableName.
 value (comment)
 The value to return if key is nil or if a localized string for key can’t be found in the table.
 */

#define LocalizationSetLanguage(language) \
[[LocalizationSystem sharedLocalSystem] setLanguage:(language)]

#define LocalizationGetLanguage \
[[LocalizationSystem sharedLocalSystem] getLanguage]

#define LocalizationReset \
[[LocalizationSystem sharedLocalSystem] resetLocalization]

#define LocalizedBundle \
[[LocalizationSystem sharedLocalSystem] localizedBundle]

@interface LocalizationSystem : NSObject {
    //NSString *language;
}

// you really shouldn't care about this functions and use the MACROS
+ (LocalizationSystem *)sharedLocalSystem;

//gets the string localized
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;

//sets the language
- (void) setLanguage:(NSString*) language;

//gets the current language
//- (NSString*) getLanguage;
//
////resets this system.
//- (void) resetLocalization;
//
//- (NSBundle *)localizedBundle;


@end
