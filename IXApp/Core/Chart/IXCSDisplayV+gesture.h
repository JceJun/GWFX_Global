//
//  IXCSDisplayV+gesture.h
//  KLine
//
//  Created by Magee on 2016/12/8.
//  Copyright © 2016年 wsz. All rights reserved.
//

#import "IXCSDisplayV.h"

@interface IXCSDisplayV (gesture)

- (void)registGestures;
- (void)resignGestures;

@end
