//
//  IXCSDisplayV+calc.m
//  KLine
//
//  Created by Magee on 2016/12/8.
//  Copyright © 2016年 wsz. All rights reserved.
//

#import "IXCSDisplayV+calc.h"

#import "IXCSDisplayV+index.h"
#import "IXCSDisplayV+detect.h"

@implementation IXCSDisplayV (calc)

#pragma mark ----------------------------------主指计算----------------------------------
- (void)mainIndex
{
    if (self.mainIdxArr.count == self.pData.count) {
        return;
    }
    [self.mainIdxArr removeAllObjects];
    
    switch (self.mainIdxType) {
        case IXCSMainIdxTypeMA:
            [self load_M_MA];
            break;
        case IXCSMainIdxTypeBBI:
            [self load_M_BBI];
            break;
        case IXCSMainIdxTypeBOLL:
            [self load_M_BOLL];
            break;
        case IXCSMainIdxTypeMIKE:
            [self load_M_MIKE];
            break;
        case IXCSMainIdxTypePBX:
            [self load_M_PBX];
            break;
        default:
            break;
    }
}

#pragma mark ----------------------------------副指计算----------------------------------
- (void)scndIndex
{
    if (self.scndIdxArr.count == self.pData.count) {
        return;
    }
    [self.scndIdxArr removeAllObjects];
    
    switch (self.scndIdxType) {
        case IXCSScndIdxTypeMACD:
            [self load_S_MACD];
            break;
        case IXCSScndIdxTypeARBR:
            [self load_S_ARBR];
            break;
        case IXCSScndIdxTypeATR:
            [self load_S_ATR];
            break;
        case IXCSScndIdxTypeBIAS:
            [self load_S_BIAS];
            break;
        case IXCSScndIdxTypeCCI:
            [self load_S_CCI];
            break;
        case IXCSScndIdxTypeDKBY:
            [self load_S_DKBY];
            break;
        case IXCSScndIdxTypeKD:
            [self load_S_KD];
            break;
        case IXCSScndIdxTypeKDJ:
            [self load_S_KDJ];
            break;
        case IXCSScndIdxTypeLWR:
            [self load_S_LWR];
            break;
        case IXCSScndIdxTypeRSI:
            [self load_S_RSI];
            break;
        case IXCSScndIdxTypeQHLSR:
            [self load_S_QHLSR];
            break;
        case IXCSScndIdxTypeWR:
            [self load_S_WR];
            break;
        default:
            break;
    }
}

#pragma mark ----------------------------------主指检测----------------------------------
- (void)mainBryDet
{
    if (self.RIdx < 0 || self.LIdx < 0) {
        return;
    }
    switch (self.mainIdxType) {
        case IXCSMainIdxTypeMA:
            [self detect_M_MA];
            break;
        case IXCSMainIdxTypeBBI:
            [self detect_M_BBI];
            break;
        case IXCSMainIdxTypeBOLL:
            [self detect_M_BOLL];
            break;
        case IXCSMainIdxTypeMIKE:
            [self detect_M_MIKE];
            break;
        case IXCSMainIdxTypePBX:
            [self detect_M_PBX];
            break;
        default:
            break;
    }
}

#pragma mark ----------------------------------副指检测----------------------------------
- (void)scndBryDet
{
    if (self.RIdx < 0 || self.LIdx < 0) {
        return;
    }
    
    switch (self.scndIdxType) {
        case IXCSScndIdxTypeMACD:
            [self detect_S_MACD];
            break;
        case IXCSScndIdxTypeARBR:
            [self detect_S_ARBR];
            break;
        case IXCSScndIdxTypeATR:
            [self detect_S_ATR];
            break;
        case IXCSScndIdxTypeBIAS:
            [self detect_S_BIAS];
            break;
        case IXCSScndIdxTypeWR:
            [self detect_S_WR];
            break;
        case IXCSScndIdxTypeCCI:
            [self detect_S_CCI];
            break;
        case IXCSScndIdxTypeDKBY:
            [self detect_S_DKBY];
            break;
        case IXCSScndIdxTypeKD:
            [self detect_S_KD];
            break;
        case IXCSScndIdxTypeKDJ:
            [self detect_S_KDJ];
            break;
        case IXCSScndIdxTypeLWR:
            [self detect_S_LWR];
            break;
        case IXCSScndIdxTypeRSI:
            [self detect_S_RSI];
            break;
        case IXCSScndIdxTypeQHLSR:
            [self detect_S_QHLSR];
            break;
        default:
            break;
    }
}

@end
