//
//  IXCSDisplayV+draw.h
//  KLine
//
//  Created by Magee on 16/12/1.
//  Copyright © 2016年 wsz. All rights reserved.
//

/**k线图绘图区表格、基本UI*/

#import "IXCSDisplayV.h"

@interface IXCSDisplayV (draw)

- (void)drawForm;           //绘制框架

- (void)drawMainData;       //绘制主图
- (void)drawMainIndex;      //主图指标
- (void)drawScndIndex;      //副图指标

- (void)drawYscaleMark;     //价格刻度
- (void)drawSYscaleMark;
- (void)drawXscaleMark;     //时间刻度

- (void)drawCurrentPriceLine;   //绘制最新价虚线
- (void)drawCurrentPrice;       //绘制最新价
- (void)drawCrossLine;      //绘制十字标
- (void)drawYMoveScale;     //价格移动刻度
- (void)drawXMoveScale;     //时间移动刻度

- (void)drawMainIdxText;    //主指标text
- (void)drawScndIdxText;    //副指标text
@end

