//
//  CAShapeLayer+Chart.m
//  IXApp
//
//  Created by Seven on 2017/8/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "CAShapeLayer+Chart.h"

@implementation CAShapeLayer (Chart)

+ (instancetype)dashLayerWithP:(CGPoint)p1
                            p2:(CGPoint)p2
                       dashArr:(NSArray *)dash
                     lineWidth:(CGFloat)width
                   strokeColor:(UIColor *)color1
{
    UIBezierPath    * path  = [UIBezierPath bezierPath];
    [path moveToPoint:p1];
    [path addLineToPoint:p2];
    
    CAShapeLayer    * layer = [CAShapeLayer layer];
    layer.strokeColor = color1.CGColor;
    if (!dash) {
        dash = @[@6,@3,@2,@3];
    }
    layer.lineDashPattern = dash;
    layer.lineDashPhase = 4.f;
    layer.path = path.CGPath;
    layer.lineWidth = width;
    return layer;
}

+ (instancetype)layerWithP:(CGPoint)p1
                        p2:(CGPoint)p2
                 lineWidth:(CGFloat)width
               strokeColor:(UIColor *)color1
{
    UIBezierPath    * path  = [UIBezierPath bezierPath];
    path.lineWidth = width;
    [path moveToPoint:p1];
    [path addLineToPoint:p2];
    
    return [self layerWithPath:path lineWidth:width strokeColor:color1 fillColor:nil];
}

+ (instancetype)layerWithRect:(CGRect)rect
                    lineWidth:(CGFloat)width
                  strokeColor:(UIColor *)color1
                    fillColor:(UIColor *)color2
{
    UIBezierPath    * path = [UIBezierPath bezierPathWithRect:rect];
    path.lineWidth = width;
    
    return [self layerWithPath:path lineWidth:width strokeColor:color1 fillColor:color2];
}

+ (instancetype)layerWithRect:(CGRect)rect
                         topY:(CGFloat)topY
                        btomY:(CGFloat)btomY
                    lineWidth:(CGFloat)width
                  strokeColor:(UIColor *)color1
                    fillColor:(UIColor *)color2
{
    CGFloat xLine = rect.origin.x + rect.size.width/2;
    
    UIBezierPath    * path = [UIBezierPath bezierPathWithRect:rect];
    path.lineWidth = width;
    [path moveToPoint:CGPointMake(xLine, topY)];
    [path addLineToPoint:CGPointMake(xLine, btomY)];
    
    return [self layerWithPath:path lineWidth:width strokeColor:color1 fillColor:color2];
}

+ (instancetype)layerWithPath:(UIBezierPath *)path
                    lineWidth:(CGFloat)width
                  strokeColor:(UIColor *)color1
                    fillColor:(UIColor *)color2
{
    CAShapeLayer    * layer = [CAShapeLayer layer];
    if (color1) {
        layer.strokeColor = color1.CGColor;
    }
    if (color2) {
        layer.fillColor = color2.CGColor;
    }
    layer.path = path.CGPath;
    layer.lineWidth = width;
    layer.lineJoin =  kCALineJoinRound;
    layer.lineCap = kCALineCapRound;
    [layer strokeColor];
    [path removeAllPoints];
    return layer;
}


#pragma mark -
#pragma mark - --

+ (instancetype)layerWithPath:(UIBezierPath *)path
                  strokeColor:(UIColor *)color
                    lineWidth:(CGFloat)width
{
    CAShapeLayer    * layer = [CAShapeLayer layer];
    if (color) {
        layer.strokeColor = color.CGColor;
        layer.fillColor = [UIColor clearColor].CGColor;
    }
    layer.path = path.CGPath;
    layer.lineWidth = width;
    layer.lineJoin =  kCALineJoinRound;
    layer.lineCap = kCALineCapRound;
    [layer strokeColor];
    return layer;
}


@end
