//
//  IXTSEngine.m
//  IXApp
//
//  Created by Magee on 2016/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTSEngine.h"
#import "IXTSDataSC.h"

@interface IXTSEngine ()
<
IXTSDataSCDelegate,
IXTSDisplayVDelegate
>

@property (nonatomic, strong)IXTSDataSC *dataSC;
@property (nonatomic,assign)IXTSEngineReqStatus reqStatus;

@end

@implementation IXTSEngine

- (void)dealloc
{
    NSLog(@" -- %s --",__func__);
    _displayV.delegate = nil;
    _dataSC.delegate = nil;
}

- (instancetype)initWithDigit:(int)digit
               scheduleCateID:(int)schedule
                             lastPrice:(double)lastPrice
{
    self = [super init];
    if (self) {
        self.displayV = [[IXTSDisplayV alloc] init];
        self.displayV.delegate = self;
        self.displayV.lastPrice = lastPrice;
        self.displayV.curDigit  = digit;
        self.displayV.scheduleCateID = schedule;
        
        self.dataSC   = [[IXTSDataSC alloc] init];
        self.dataSC.delegate = self;
        self.dataSC.scheduleCateID = schedule;
    }
    return self;
}

- (void)setNightMode:(BOOL)isNightMode
{
    self.displayV.isNightMode = isNightMode;
}

- (void)setRedUpBlueDown:(BOOL)isRedUp
{
    self.displayV.settingRed = isRedUp;
}

- (void)dynamicDataReseponse:(IXMetaData *)meta
{
    if (_dataSC.min1CT.count || _reqStatus != IXTSEngineReqBusy) {
        BOOL needRefresh = (self.displayV.superview != nil) && self.displayV.alpha;
        [_dataSC dynamicDataRsp:meta needRefresh:needRefresh];
    }
}

- (void)requestDataResponse:(NSMutableArray *)array
{
    [_dataSC requestDataRsp:array];
    
    IXMetaData  * meta = [[IXMetaData alloc] initWithDic:[array lastObject]];
    if (meta.offset + meta.count == meta.total + 1) {
        [self finishRequest];
    }
}

- (void)display
{
    [self.dataSC display];
}

#pragma mark -
#pragma mark - IXTSDataSCDelegate

- (void)metaDataShuoldBeRequested
{
    NSLog(@" -- %s --",__func__);
    if (_reqStatus == IXTSEngineReqBusy) {
        return;
    }
    self.reqStatus = IXTSEngineReqBusy;
    
    if (self.request) {
        self.request(0,1440);
    }
    
    [self performSelector:@selector(requestTimeOut)
               withObject:nil
               afterDelay:5];
}

- (void)displayVShouldBeRefreshedWithToltal:(int)total
{
    [_displayV refreshWithTotal:total];
}

- (void)displayDotShouldActionWithTotal:(int)total price:(CGFloat)price
{
    [_displayV refrashDotWithTotal:total price:price];
}

#pragma mark -
#pragma mark - IXTSDisplayVDelegate

- (NSMutableArray *)IXTSDisplayVDataSource
{
    return _dataSC.min1CT;
}

- (void)IXTSDisplayVDoubleClicked
{
    if (self.clicked) {
        self.clicked();
    }
}

#pragma mark -
#pragma mark -

- (void)setReqStatus:(IXTSEngineReqStatus)reqStatus
{
    _reqStatus = reqStatus;
    if (self.status) {
        self.status(reqStatus);
    }
    
    
    if (reqStatus == IXTSEngineReqBusy) {
        [_displayV.loadingV startAnimating];
    } else {
        [_displayV.loadingV stopAnimating];
    }
}

- (void)requestTimeOut
{
    self.reqStatus = IXTSEngineeReqFree;
    
//    if (!_dataSC.min1CT.count) {
//        [self metaDataShuoldBeRequested];
//    }
}

- (void)finishRequest
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(requestTimeOut)
                                               object:nil];
    self.reqStatus = IXTSEngineeReqFree;
}

@end
