//
//  IXCSDetailV.h
//  IXApp
//
//  Created by Magee on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IXMetaData;
@interface IXCSDetailV : UIView
@property (nonatomic, assign) int digit;

/**
 刷新详情

 @param curMeta 当前meta
 @param lstMeta 上一条meta
 @param currend 是否为最新行情
 */
- (void)reloadUIWith:(IXMetaData *)curMeta lst:(IXMetaData *)lstMeta isCurrentData:(BOOL)currend;

@end
