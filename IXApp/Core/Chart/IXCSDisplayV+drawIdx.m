//
//  IXCSDisplayV+drawIdx.m
//  IXApp
//
//  Created by Magee on 2017/3/29.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCSDisplayV+drawIdx.h"

@implementation IXCSDisplayV (drawIdx)

- (void)drawWithColorR:(CGFloat)r
                     G:(CGFloat)g
                     B:(CGFloat)b
                points:(CGPoint *)points
                 count:(int)cnt
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, r, g, b, 1.0);
    CGContextSetLineWidth(context, 1.f);
    CGContextAddLines(context, points, cnt);
    CGContextDrawPath(context, kCGPathStroke);
}

- (void)make_M_PointsWithContainer:(CGPoint *)ctner
                             width:(CGFloat)width
                               cnt:(int *)cnt
                               key:(NSString *)key
{
    if (self.curMHth != 0) {
        CGFloat qt_price_mhth = (self.curMainTop - self.curMainBtm)/self.curMHth;
        for (NSInteger i = self.LIdx; i >= self.RIdx && !(i < 0); i--) {
            CGFloat x = self.curLWth + (self.LIdx - i) * width + width / 2;
            CGFloat y;
            NSDictionary *dic = self.mainIdxArr[i];
            if (dic[key] != [NSNull null]) {
                CGFloat price = [dic[key] floatValue];
                y = self.curMBtmY - (price - self.curMainBtm) / qt_price_mhth;
                ctner[*cnt] = CGPointMake(x, y);
                (*cnt) ++;
            }
        }
    }
}

- (void)make_S_PointsWithContainer:(CGPoint *)ctner
                             width:(CGFloat)width
                               cnt:(int *)cnt
                               key:(NSString *)key
{
    if (self.curSHth != 0) {
        CGFloat qt_price_mhth = (self.curScndTop - self.curScndBtm)/self.curSHth;
        for (NSInteger i = self.LIdx; i >= self.RIdx; i--) {
            NSDictionary *dic = self.scndIdxArr[i];
            if (dic[key] != [NSNull null]) {
                CGFloat x = self.curLWth + (self.LIdx - i) * width + width / 2;
                CGFloat y;
                CGFloat rst = [dic[key] floatValue];
                y = self.curSBtmY - (rst - self.curScndBtm) / qt_price_mhth;
                ctner[*cnt] = CGPointMake(x, y);
                (*cnt) ++;
            }
        }
    }
}

#pragma mark -----------------------------------------------------------------------------------------------
#pragma mark ------------------------------------------- 主指标绘制 ------------------------------------------

- (void)draw_M_MA:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMA_5];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMA_10];
    [self drawWithColorR:.298 G:.686 B:.906 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMA_20];
    [self drawWithColorR:.914 G:.456 B:.867 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMA_60];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
}

- (void)draw_M_BBI:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kBBI_BBI];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
}

- (void)draw_M_BOLL:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kBOLL_UP];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kBOLL_MA];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kBOLL_DN];
    [self drawWithColorR:.914 G:.456 B:.867 points:container count:n];
}

- (void)draw_M_MIKE:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMIKE_WR];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMIKE_MR];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMIKE_SR];
    [self drawWithColorR:.914 G:.456 B:.867 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMIKE_WS];
    [self drawWithColorR:.298 G:.686 B:.906 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMIKE_MS];
    [self drawWithColorR:.941 G:.553 B:.525 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kMIKE_SS];
    [self drawWithColorR:.696 G:.616 B:.902 points:container count:n];
}

- (void)draw_M_PBX:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kPBX_1];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kPBX_2];
    [self drawWithColorR:.298 G:.686 B:.906 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kPBX_3];
    [self drawWithColorR:.914 G:.456 B:.867 points:container count:n];
    
    n = 0;
    [self make_M_PointsWithContainer:container width:width cnt:&n key:kPBX_4];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
}

#pragma mark -----------------------------------------------------------------------------------------------
#pragma mark ------------------------------------------- 副指标绘制 ------------------------------------------

- (void)draw_S_MACD:(NSInteger)cnt width:(CGFloat)width bodyWidth:(CGFloat)bodyWidth
{
    int nDIF = 0;
    int nDEA = 0;
    CGPoint difPoints[cnt];
    CGPoint deaPoints[cnt];
    CGFloat scale = (self.curScndTop - self.curScndBtm)/self.curSHth;
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (NSInteger i = self.LIdx; i >= self.RIdx && !(i < 0); i--) {
        NSDictionary *dic = self.scndIdxArr[self.scndIdxArr.count -i - 1];
        CGFloat mac = [dic[kMACD_MAC] floatValue];
        
        if (mac > 0) {
            if (self.settingRed) {
                //红色
                if (self.isNightMode) {
                    CGContextSetRGBStrokeColor(context, 1.f,  0.302, 0.176, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(context, .890, .318, .357, 1.0);
                }
            }else{
                //绿色
                if (self.isNightMode){
                    CGContextSetRGBStrokeColor(context, 0.129, 0.808, 0.600, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(context, .067, .722, .451, 1.0);
                }
                
            }
        } else if (mac < 0) {
            if (self.settingRed) {
                //绿色
                if (self.isNightMode){
                    CGContextSetRGBStrokeColor(context, 0.129, 0.808, 0.600, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(context, .067, .722, .451, 1.0);
                }
            }else{
                //红色
                if (self.isNightMode) {
                    //                    CGContextSetRGBStrokeColor(context, 1.f,  0.302, 0.176, 1.0);
                    CGContextSetRGBStrokeColor(context, 255.0/255.0,  70.0/255.0, 83.0/255.0, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(context, .890, .318, .357, 1.0);
                }
            }
        } else {
            CGContextSetRGBStrokeColor(context,.5, .5, .5, 1.0);
        }
        
        CGPoint body[2];
        body[0].x = self.curLWth + (self.LIdx - i) * width + width/2;
        body[0].y = self.curSTopY + self.curSHth/2.f;
        
        body[1].x = body[0].x;
        body[1].y = body[0].y - mac/scale;
        CGContextSetLineWidth(context, bodyWidth);
        CGContextAddLines(context, body, 2);
        CGContextDrawPath(context, kCGPathStroke);
        
        if (dic[kMACD_DIF]) {
            CGFloat y = 0.f;
            CGFloat vol = [dic[kMACD_DIF] floatValue];
            y = self.curSBtmY - (vol - self.curScndBtm)/scale;
            difPoints[nDIF] = CGPointMake(body[0].x, y);
            nDIF ++;
        }
        if (dic[kMACD_DEA]) {
            CGFloat y = 0.f;
            CGFloat vol = [dic[kMACD_DEA] floatValue];
            y = self.curSBtmY - (vol - self.curScndBtm)/scale;
            deaPoints[nDEA] = CGPointMake(body[0].x, y);
            nDEA ++;
        }
    }
    [self drawWithColorR:.282 G:.525 B:.788 points:deaPoints count:nDIF];
    [self drawWithColorR:.922 G:.678 B:.384 points:difPoints count:nDEA];
}

- (void)draw_S_ARBR:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kARBR_AR];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kARBR_BR];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
}

- (void)draw_S_ATR:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kATR_TR];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kATR_ATR];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
}

- (void)draw_S_BIAS:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kBIAS_1];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kBIAS_2];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kBIAS_3];
    [self drawWithColorR:.914 G:.455 B:.875 points:container count:n];
}

- (void)draw_S_CCI:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kCCI_CCI];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
}

- (void)draw_S_DKBY:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kDKBY_ENE1];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kDKBY_ENE2];
    [self drawWithColorR:.914 G:.455 B:.875 points:container count:n];
}

- (void)draw_S_KD:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kKD_K];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n ];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kKD_D];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
}

- (void)draw_S_KDJ:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kKDJ_D];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kKDJ_K];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kKDJ_J];
    [self drawWithColorR:.914 G:.455 B:.875 points:container count:n];
}

- (void)draw_S_LWR:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kLWR_1];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kLWR_2];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
}

- (void)draw_S_QHLSR:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kQHLSR_5];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kQHLSR_10];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
}

- (void)draw_S_RSI:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kRSI_1];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kRSI_2];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kRSI_3];
    [self drawWithColorR:.914 G:.455 B:.875 points:container count:n];
}

- (void)draw_S_WR:(NSInteger)cnt width:(CGFloat)width
{
    int n = 0;   CGPoint container[cnt];
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kWR_1];
    [self drawWithColorR:.282 G:.525 B:.788 points:container count:n];
    
    n = 0;
    [self make_S_PointsWithContainer:container width:width cnt:&n key:kWR_2];
    [self drawWithColorR:.922 G:.678 B:.384 points:container count:n];
}

#pragma mark -----------------------------------------------------------------------------------------------
#pragma mark ----------------------------------------- 主指标Text绘制 ----------------------------------------

- (void)draw_M_Title:(NSString *)title
              pointX:(CGFloat)pointX
                   x:(CGFloat *)x
{
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
    *x = size.width + pointX;
    [title drawInRect:CGRectMake(pointX, 5, 70, 12) withAttributes:@{NSFontAttributeName:RO_REGU(10),              NSForegroundColorAttributeName:UIColorHexFromRGB(0x99abba)}];
}

- (void)draw_M_T_WithIdx:(NSInteger)idx
                     key:(NSString *)key
                  pointX:(CGFloat)pointX
                   color:(UIColor *)color
                       x:(CGFloat *)x
{
    NSDictionary *dic = self.mainIdxArr[idx];
    if (dic[key] != [NSNull null]) {
        NSString *str = [NSString stringWithFormat:@"%f",[dic[key] floatValue]];
        NSString *fmtStr = [NSString formatterPrice:str WithDigits:self.curDigit];
        NSString *rstStr = [NSString stringWithFormat:@"%@:%@",key,fmtStr];
        CGSize  size = [rstStr sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
        *x = size.width + pointX;
        [rstStr drawInRect:CGRectMake(pointX, 5, 70, 12) withAttributes:@{NSFontAttributeName:RO_REGU(10),              NSForegroundColorAttributeName:color}];
    }
}

- (void)draw_S_Title:(NSString *)title
              pointX:(CGFloat)pointX
                   x:(CGFloat *)x
{
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
    *x = size.width + pointX;
    [title drawInRect:CGRectMake(pointX, self.curMBtmY + 5, 70, 12) withAttributes:@{NSFontAttributeName:RO_REGU(10),              NSForegroundColorAttributeName:UIColorHexFromRGB(0x99abba)}];
}

- (void)draw_S_T_withIdx:(NSInteger)idx
                     key:(NSString *)key
                  pointX:(CGFloat)pointX
                   color:(UIColor *)color
                       x:(CGFloat *)x
{
    NSDictionary *dic = self.scndIdxArr[idx];
    if (dic[key] != [NSNull null]) {
        NSString *str = [NSString stringWithFormat:@"%f",[dic[key] floatValue]];
        NSString *fmtStr = [NSString formatterPrice:str WithDigits:self.curDigit];
        NSString *rstStr = [NSString stringWithFormat:@"%@:%@",key,fmtStr];
        CGSize  size = [rstStr sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
        *x = pointX + size.width;
        [rstStr drawInRect:CGRectMake(pointX, self.curMBtmY + 5, 70, 12) withAttributes:@{NSFontAttributeName:RO_REGU(10),              NSForegroundColorAttributeName:color}];
    }
}

- (void)draw_M_MA_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_M_Title:@"(5,10,20,60)"    pointX:x x:&x];
    [self draw_M_T_WithIdx:idx key:kMA_5  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_M_T_WithIdx:idx key:kMA_10 pointX:x + 10 color:UIColorHexFromRGB(0x4cafe7) x:&x];
    [self draw_M_T_WithIdx:idx key:kMA_20 pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
    [self draw_M_T_WithIdx:idx key:kMA_60 pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
}

- (void)draw_M_BBI_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_M_Title:@"(3,6,12,24)"        pointX:x  x:&x];
    [self draw_M_T_WithIdx:idx key:kBBI_BBI  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
}

- (void)draw_M_BOLL_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_M_Title:@"(26,2)"             pointX:x  x:&x];
    [self draw_M_T_WithIdx:idx key:kBOLL_MA  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_M_T_WithIdx:idx key:kBOLL_UP  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_M_T_WithIdx:idx key:kBOLL_DN  pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
}

- (void)draw_M_MIKE_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_M_Title:@"(12)"               pointX:x  x:&x];
    [self draw_M_T_WithIdx:idx key:kMIKE_WR  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_M_T_WithIdx:idx key:kMIKE_MR  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_M_T_WithIdx:idx key:kMIKE_SR  pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
    [self draw_M_T_WithIdx:idx key:kMIKE_WS  pointX:x + 10 color:UIColorHexFromRGB(0x4cafe7) x:&x];
    [self draw_M_T_WithIdx:idx key:kMIKE_MS  pointX:x + 10 color:UIColorHexFromRGB(0xf08d86) x:&x];
    [self draw_M_T_WithIdx:idx key:kMIKE_SS  pointX:x + 10 color:UIColorHexFromRGB(0x9890e6) x:&x];
}

- (void)draw_M_PBX_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_M_Title:@"(4,6,9,13)"       pointX:x x:&x];
    [self draw_M_T_WithIdx:idx key:kPBX_1  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_M_T_WithIdx:idx key:kPBX_2  pointX:x + 10 color:UIColorHexFromRGB(0x4cafe7) x:&x];
    [self draw_M_T_WithIdx:idx key:kPBX_3  pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
    [self draw_M_T_WithIdx:idx key:kPBX_4  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
}

#pragma mark -----------------------------------------------------------------------------------------------
#pragma mark ----------------------------------------- 副指标Text绘制 ----------------------------------------

- (void)draw_S_MACD_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(9,12,26)"          pointX:x  x:&x];
    [self draw_S_T_withIdx:idx key:kMACD_DIF pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_S_T_withIdx:idx key:kMACD_DEA pointX:x + 10 color:UIColorHexFromRGB(0x4cafe7) x:&x];
    [self draw_S_T_withIdx:idx key:kMACD_MAC pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
}

- (void)draw_S_ARBR_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(26)"              pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kARBR_AR pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kARBR_BR pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
}

- (void)draw_S_ATR_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(14)"              pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kATR_TR  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kATR_ATR pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
}

- (void)draw_S_BIAS_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(6,12,24)"         pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kBIAS_1  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kBIAS_2  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_S_T_withIdx:idx key:kBIAS_3  pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
}

- (void)draw_S_CCI_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(14)"               pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kCCI_CCI  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
}

- (void)draw_S_DKBY_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(5,8,13,21)"          pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kDKBY_ENE1  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_S_T_withIdx:idx key:kDKBY_ENE2  pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
}

- (void)draw_S_KD_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(9,3)"          pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kKD_K  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kKD_D  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
}

- (void)draw_S_KDJ_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(9,3,3)"          pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kKDJ_K  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kKDJ_D  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_S_T_withIdx:idx key:kKDJ_J  pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
}

- (void)draw_S_LWR_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(9,3,3)"          pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kLWR_1  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kLWR_2  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
}

- (void)draw_S_QHLSR_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(5,10)"              pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kQHLSR_5   pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kQHLSR_10  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
}

- (void)draw_S_RSI_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(6,12,24)"        pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kRSI_1  pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kRSI_2  pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
    [self draw_S_T_withIdx:idx key:kRSI_3  pointX:x + 10 color:UIColorHexFromRGB(0xe974df) x:&x];
}

- (void)draw_S_WR_T:(NSInteger)idx
{
    CGFloat x = self.curLWth;
    [self draw_S_Title:@"(6,10)"         pointX:x x:&x];
    [self draw_S_T_withIdx:idx key:kWR_1 pointX:x + 10 color:UIColorHexFromRGB(0x4886c9) x:&x];
    [self draw_S_T_withIdx:idx key:kWR_2 pointX:x + 10 color:UIColorHexFromRGB(0xebad62) x:&x];
}

@end

