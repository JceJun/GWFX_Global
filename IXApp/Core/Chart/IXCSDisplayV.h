//
//  IXCSDisplayV.h
//  IXApp
//
//  Created by Magee on 16/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

/**K线图主文件（candle static chart）*/

#import <UIKit/UIKit.h>
#import "IXCSType.h"
#import "IXCSDetailV.h"
#import "IXKLineType.h"

#pragma mark ------------------------------------------------
#pragma mark --------------------CallBack--------------------

@protocol IXCSDisplayVDelegate <NSObject>

- (void)IXCSDisplayVNeedMoreData;
- (void)IXCSDisplayVDoubleClicked;

@end

#pragma mark ------------------------------------------------
#pragma mark --------------------Interface-------------------

@interface IXCSDisplayV : UIView

@property (nonatomic,strong)NSMutableArray *pData;         //数据源指针
@property (nonatomic,assign)uint8_t curDigit;
@property (nonatomic,assign)NSInteger curMarketID;
@property (nonatomic,assign)IXKLineType csType;
@property (nonatomic,assign)IXCSMainIdxType  mainIdxType;  //当前主指
@property (nonatomic,assign)IXCSScndIdxType  scndIdxType;  //当前副指

@property (nonatomic,strong)NSMutableArray *mainIdxArr;    //主指容器(缺省MA)
@property (nonatomic,strong)NSMutableArray *scndIdxArr;    //副指容器(缺省MACD)

//主图坐标
@property (nonatomic,assign)CGFloat    curMTopY;       //主图顶Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curMBtmY;       //主图底Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curMWth;        //主图宽
@property (nonatomic,assign)CGFloat    curMHth;        //主图高

//副图坐标
@property (nonatomic,assign)CGFloat    curSTopY;       //副图顶Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curSBtmY;       //副图底Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curSWth;        //副图宽
@property (nonatomic,assign)CGFloat    curSHth;        //副图高

@property (nonatomic,assign)CGFloat    curLWth;        //主副图左侧留白

//边界界限
@property (nonatomic,assign)CGFloat    curMainTop;     //主图上限(量化)
@property (nonatomic,assign)CGFloat    curMainBtm;     //主图下限(量化)
@property (nonatomic,assign)CGFloat    curMainTopU;    //主图上限(非量化)
@property (nonatomic,assign)CGFloat    curMainBtmU;    //主图下限(非量化)
@property (nonatomic,assign)CGFloat    curScndTop;     //副图上限(量化)
@property (nonatomic,assign)CGFloat    curScndBtm;     //副图下限(量化)

@property (nonatomic,strong)IXMetaData  * maxPriceMeta; //绘图区最高价meta
@property (nonatomic,strong)IXMetaData  * minPriceMeta; //绘图区最低价meta

@property (nonatomic,assign)NSUInteger maxElmtCnt;     //绘图区最大elmt数量
@property (nonatomic,assign)NSUInteger minElmtCnt;     //绘图区最小elmt数量
@property (nonatomic,assign)NSUInteger curElmtCnt;     //绘图区当前elmt数量
@property (nonatomic,assign)NSUInteger curElmtCntTmp;  //绘图区当前elmt数量(捏合手势的临时变量)

@property (nonatomic,assign)NSInteger  LIdx;           //绘图区elmt左游标
@property (nonatomic,assign)NSInteger  RIdx;           //绘图区elmt右游标

@property (nonatomic,assign)IXCSGstType                    gstType;  //当前手势
@property (nonatomic,strong)UIPanGestureRecognizer         *gstPan;  //手势拖动
@property (nonatomic,strong)UILongPressGestureRecognizer   *gstPrs;  //手势长按
@property (nonatomic,strong)UIPinchGestureRecognizer       *gstpnc;  //手势缩放
@property (nonatomic,strong)UITapGestureRecognizer         *gsttap;  //手势双击
@property (nonatomic,strong)NSMutableArray  * longPressGesOp;   //处理长按手势延时操作

@property (nonatomic,strong)CATextLayer     * highPriceLay; //最高价
@property (nonatomic,strong)CATextLayer     * lowPriceLay;  //最低价

@property (nonatomic,assign)CGPoint    pointCrossLine;

@property (nonatomic,assign)IXCSOrientationType orientation;    //横竖屏

@property (nonatomic,assign)id<IXCSDisplayVDelegate> delegate;

@property (nonatomic,strong)IXCSDetailV *detailV;
@property (nonatomic,strong)UIActivityIndicatorView * loadingV;
#pragma mark ------------------------------------------------
#pragma mark ---------------------Colors--------------------


/** 是否为夜间模式 */
@property (nonatomic, assign) BOOL  isNightMode;
/** 是否为红涨绿跌 */
@property (nonatomic, assign) BOOL  settingRed;

@property (nonatomic, readonly) UIColor * hollowRectColor;  //空心烛体边框颜色
@property (nonatomic, readonly) UIColor * hollowColor;      //空心烛体颜色
@property (nonatomic, readonly) UIColor * crossCurveTextColor;  //十字刻线字体颜色
@property (nonatomic, readonly) UIColor * crossCurveBgColor;  //十字刻线背景颜色

#pragma mark ------------------------------------------------
#pragma mark ---------------------Methods--------------------

- (void)redirectKline;
- (void)forceRefresh;
- (void)checkPdata;

@end
