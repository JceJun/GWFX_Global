//
//  IXCSDetailV.m
//  IXApp
//
//  Created by Magee on 2017/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCSDetailV.h"
#import "IXMetaData.h"
#import "NSString+FormatterPrice.h"
#import "IXRateCaculate.h"

@interface IXCSDetailV ()

@property (nonatomic,strong)IXMetaData *lastMeta;

@property (nonatomic,strong)UILabel *l_time;

@property (nonatomic,strong)UILabel *l_open;
@property (nonatomic,strong)UILabel *l_open_num;

@property (nonatomic,strong)UILabel *l_high;
@property (nonatomic,strong)UILabel *l_high_num;

@property (nonatomic,strong)UILabel *l_low;
@property (nonatomic,strong)UILabel *l_low_num;

@property (nonatomic,strong)UILabel *l_close;
@property (nonatomic,strong)UILabel *l_close_num;

@property (nonatomic,strong)UILabel *l_quota;
@property (nonatomic,strong)UILabel *l_quota_num;

@property (nonatomic,strong)UILabel *l_extent;
@property (nonatomic,strong)UILabel *l_extent_num;

@property (nonatomic,strong)UILabel *l_vol;
@property (nonatomic,strong)UILabel *l_vol_num;

@property (nonatomic,strong)UILabel *l_amt;
@property (nonatomic,strong)UILabel *l_amt_num;
@end

@implementation IXCSDetailV

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.layer.borderWidth = .5;
        self.layer.dk_borderColorPicker = DKColorWithRGBs(0xcbcfd6, 0x232a36);
        self.dk_backgroundColorPicker = DKNavBarColor;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self l_open];
    [self l_high];
    [self l_low];
    [self l_close];
    [self l_quota];
    [self l_extent];
    
    if (self.frame.size.height == 135) {
        [self l_vol];
        [self l_amt];
    }
}

- (UILabel *)l_time
{
    if (!_l_time) {
        _l_time = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, self.frame.size.width, 10)];
        _l_time.text = @"----";
        _l_time.font = RO_REGU(9);
        _l_time.textAlignment = NSTextAlignmentCenter;
        _l_time.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_l_time];
    }
    return _l_time;
}

- (UILabel *)l_open
{
    if (!_l_open) {
        _l_open = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, 40, 10)];
        _l_open.text = LocalizedString(@"开盘");
        _l_open.font = RO_REGU(9);
        _l_open.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_l_open];
    }
    return _l_open;
}
- (UILabel *)l_open_num
{
    if (!_l_open_num) {
        _l_open_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 20, 50, 10)];
        _l_open_num.text = @"--";
        _l_open_num.font = RO_REGU(9);
        _l_open_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_open_num];
    }
    return _l_open_num;
}

- (UILabel *)l_high
{
    if (!_l_high) {
        _l_high = [[UILabel alloc] initWithFrame:CGRectMake(5, 34, 40, 10)];
        _l_high.text = LocalizedString(@"最高");
        _l_high.font = RO_REGU(9);
        _l_high.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_l_high];
    }
    return _l_high;
}
- (UILabel *)l_high_num
{
    if (!_l_high_num) {
        _l_high_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 34, 50, 10)];
        _l_high_num.text = @"--";
        _l_high_num.font = RO_REGU(9);
        _l_high_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_high_num];
    }
    return _l_high_num;
}

- (UILabel *)l_low
{
    if (!_l_low) {
        _l_low = [[UILabel alloc] initWithFrame:CGRectMake(5, 48, 40, 10)];
        _l_low.text = LocalizedString(@"最低");
        _l_low.font = RO_REGU(9);
        _l_low.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_l_low];
    }
    return _l_low;
}
- (UILabel *)l_low_num
{
    if (!_l_low_num) {
        _l_low_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 48, 50, 10)];
        _l_low_num.text = @"--";
        _l_low_num.font = RO_REGU(9);
        _l_low_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_low_num];
    }
    return _l_low_num;
}

- (UILabel *)l_close
{
    if (!_l_close) {
        _l_close = [[UILabel alloc] initWithFrame:CGRectMake(5, 62, 40, 10)];
        _l_close.text = LocalizedString(@"收盘");
        _l_close.font = RO_REGU(9);
        _l_close.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_l_close];
    }
    return _l_close;
}
- (UILabel *)l_close_num
{
    if (!_l_close_num) {
        _l_close_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 62, 50, 10)];
        _l_close_num.text = @"--";
        _l_close_num.font = RO_REGU(9);
        _l_close_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_close_num];
    }
    return _l_close_num;
}

- (UILabel *)l_quota
{
    if (!_l_quota) {
        _l_quota = [[UILabel alloc] initWithFrame:CGRectMake(5, 76, 40, 10)];
        _l_quota.text = LocalizedString(@"升跌额");
        _l_quota.font = RO_REGU(9);
        _l_quota.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_l_quota];
    }
    return _l_quota;
}
- (UILabel *)l_quota_num
{
    if (!_l_quota_num) {
        _l_quota_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 76, 50, 10)];
        _l_quota_num.text = @"--";
        _l_quota_num.font = RO_REGU(9);
        _l_quota_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_quota_num];
    }
    return _l_quota_num;
}

- (UILabel *)l_extent
{
    if (!_l_extent) {
        _l_extent = [[UILabel alloc] initWithFrame:CGRectMake(5, 90, 45, 10)];
        _l_extent.text = LocalizedString(@"涨跌幅");
        _l_extent.font = RO_REGU(9);
        _l_extent.textColor = AutoNightColor(0x4c6072, 0xe9e9ea);
        [self addSubview:_l_extent];
    }
    return _l_extent;
}
- (UILabel *)l_extent_num
{
    if (!_l_extent_num) {
        _l_extent_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 90, 50, 10)];
        _l_extent_num.text = @"--";
        _l_extent_num.font = RO_REGU(9);
        _l_extent_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_extent_num];
    }
    return _l_extent_num;
}

- (UILabel *)l_vol
{
    if (!_l_vol) {
        _l_vol = [[UILabel alloc] initWithFrame:CGRectMake(5, 104, 40, 10)];
        _l_vol.text = LocalizedString(@"成交量");
        _l_vol.font = RO_REGU(9);
        _l_vol.textColor = UIColorHexFromRGB(0x4c6072);
        [self addSubview:_l_vol];
    }
    return _l_vol;
}
- (UILabel *)l_vol_num
{
    if (!_l_vol_num) {
        _l_vol_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 104, 50, 10)];
        _l_vol_num.text = @"--";
        _l_vol_num.font = RO_REGU(9);
        _l_vol_num.textColor = UIColorHexFromRGB(0x4c6072);
        _l_vol_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_vol_num];
    }
    return _l_vol_num;
}

- (UILabel *)l_amt
{
    if (!_l_amt) {
        _l_amt = [[UILabel alloc] initWithFrame:CGRectMake(5, 118, 40, 10)];
        _l_amt.text = LocalizedString(@"成交额");
        _l_amt.font = RO_REGU(9);
        _l_amt.textColor = UIColorHexFromRGB(0x4c6072);
        [self addSubview:_l_amt];
    }
    return _l_amt;
}
- (UILabel *)l_amt_num
{
    if (!_l_amt_num) {
        _l_amt_num = [[UILabel alloc] initWithFrame:CGRectMake(45, 118, 50, 10)];
        _l_amt_num.text = @"--";
        _l_amt_num.font = RO_REGU(9);
        _l_amt_num.textColor = UIColorHexFromRGB(0x4c6072);
        _l_amt_num.textAlignment = NSTextAlignmentRight;
        [self addSubview:_l_amt_num];
    }
    return _l_amt_num;
}

- (void)reloadUIWith:(IXMetaData *)curMeta lst:(IXMetaData *)lstMeta isCurrentData:(BOOL)currend
{
    if (self.lastMeta == curMeta && !currend) {
        return;
    } else {
        self.lastMeta = curMeta;
    }
    
    NSString    * formater = (curMeta.type == DAY || curMeta.type >= Week_1) ? @"YY/MM/dd" : @"YY/MM/dd HH:mm";
    NSString    * timeStr = [IXEntityFormatter timeIntervalToString:curMeta.time formate:formater];
    NSString    * weekDayString = [IXEntityFormatter weekDayStringWithTimeInterval:curMeta.time];
    self.l_time.text =  [NSString stringWithFormat:@"%@ %@",timeStr, weekDayString];
    
    if (lstMeta) {
        self.l_open_num.text = [IXPublicTool decimalNumberWithCGFloat:curMeta.open digit:_digit];
        self.l_open_num.textColor = [self compareColorWithNumber:curMeta.open number2:lstMeta.close];
        
        self.l_high_num.text = [IXPublicTool decimalNumberWithCGFloat:curMeta.high digit:_digit];
        self.l_high_num.textColor = [self compareColorWithNumber:curMeta.high number2:lstMeta.close];
        
        self.l_low_num.text  = [IXPublicTool decimalNumberWithCGFloat:curMeta.low digit:_digit];
        self.l_low_num.textColor = [self compareColorWithNumber:curMeta.low number2:lstMeta.close];
        
        self.l_close_num.text= [IXPublicTool decimalNumberWithCGFloat:curMeta.close digit:_digit];
        self.l_close_num.textColor = [self compareColorWithNumber:curMeta.close number2:lstMeta.close];
        
        CGFloat quota = curMeta.close - lstMeta.close;
        self.l_quota_num.text = [IXPublicTool decimalNumberWithCGFloat:quota digit:_digit];
        self.l_quota_num.textColor = [self compareColorWithNumber:quota number2:0];
        
        CGFloat extent = quota/lstMeta.close * 100.f;
        self.l_extent_num.text = [NSString stringWithFormat:@"%@%%",[NSString formatterPrice:[IXPublicTool decimalNumberWithCGFloat:extent digit:_digit] WithDigits:2]];
        self.l_extent_num.textColor = [self compareColorWithNumber:extent number2:0];
    } else {
        self.l_open_num.text = [IXPublicTool decimalNumberWithCGFloat:curMeta.open digit:_digit];
        self.l_open_num.textColor = MarketGrayPriceColor;
        
        self.l_high_num.text = [IXPublicTool decimalNumberWithCGFloat:curMeta.high digit:_digit];
        self.l_high_num.textColor = MarketGrayPriceColor;
        
        self.l_low_num.text  = [IXPublicTool decimalNumberWithCGFloat:curMeta.low digit:_digit];
        self.l_low_num.textColor = MarketGrayPriceColor;
        
        self.l_close_num.text= [IXPublicTool decimalNumberWithCGFloat:curMeta.close digit:_digit];
        self.l_close_num.textColor = MarketGrayPriceColor;
        
        self.l_quota_num.text = [NSString formatterPrice:@"0" WithDigits:_digit];
        self.l_quota_num.textColor = MarketGrayPriceColor;
        
        self.l_extent_num.text = @"0.00%%";
        self.l_extent_num.textColor = MarketGrayPriceColor;
        if (curMeta.vol > 0) {
            if (curMeta.vol/100000000 >= 1) {
                self.l_vol_num.text = [NSString stringWithFormat:@"%@亿手",[NSString formatterPrice:[NSString stringWithFormat:@"%f",curMeta.vol/100000000.0] WithDigits:_digit]];
            } else if (curMeta.vol/10000 >= 1) {
                self.l_vol_num.text = [NSString stringWithFormat:@"%@万手",[NSString formatterPrice:[NSString stringWithFormat:@"%lld",curMeta.vol/10000] WithDigits:_digit]];
            } else {}
        }
        
        if (curMeta.amt > 0) {
            if (curMeta.amt/100000000 >= 1) {
                self.l_amt_num.text = [NSString stringWithFormat:@"%@亿",[NSString formatterPrice:[NSString stringWithFormat:@"%f",curMeta.amt/100000000.0] WithDigits:_digit]];
            } else if (curMeta.amt/10000 >= 1) {
                self.l_amt_num.text = [NSString stringWithFormat:@"%@万",[NSString formatterPrice:[NSString stringWithFormat:@"%lld",curMeta.amt/10000] WithDigits:_digit]];
            } else {}
        }
    }
}

- (UIColor *)compareColorWithNumber:(CGFloat)number1 number2:(CGFloat)number2
{
    BOOL isNight = [IXUserInfoMgr shareInstance].isNightMode;
    
    if (number1 > number2) {
        if (isNight) {
            return UIColorHexFromRGB(mDRedPriceColor);
        }
        return UIColorHexFromRGB(mRedPriceColor);
    } else if (number1 == number2){
        if (isNight) {
            return UIColorHexFromRGB(0x99abba);
        }
        return UIColorHexFromRGB(0x8395a4);
    } else {
        if (isNight) {
            return UIColorHexFromRGB(mDGreenPriceColor);
        }
        return UIColorHexFromRGB(mGreenPriceColor);
    }
}


@end
