//
//  IXKLineType.h
//  FXApp
//
//  Created by Seven on 2017/9/6.
//  Copyright © 2017年 wsz. All rights reserved.
//

#ifndef IXKLineType_h
#define IXKLineType_h

typedef enum : NSUInteger {
    IXKLineTypeTline = 0,   //分时图
    IXKLineTypeDay,         //日k
    IXKLineTypeWeek,
    IXKLineTypeMonth,
    
    IXKLineTypeMin1 = 5,
    IXKLineTypeMin5,
    IXKLineTypeMin15,
    IXKLineTypeMin30,
    IXKLineTypeMin60,
    IXKLineTypeHour4
} IXKLineType;

#endif /* IXKLineType_h */
