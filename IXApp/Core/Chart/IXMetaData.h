//
//  IXMetaData.h
//  IXApp
//
//  Created by Magee on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXMetaData : NSObject <NSMutableCopying>

@property (nonatomic,assign)uint32_t offset;
@property (nonatomic,assign)uint32_t count;
@property (nonatomic,assign)uint32_t total;

@property (nonatomic,assign)int64_t seq;    //序号
@property (nonatomic,assign)int64_t id_p;   //id
@property (nonatomic,assign)int32_t type;   //类型

@property (nonatomic,assign)CGFloat   high;   //最高价
@property (nonatomic,assign)CGFloat   close;  //收盘价
@property (nonatomic,assign)CGFloat   open;   //开盘价
@property (nonatomic,assign)CGFloat   low;    //最低价
@property (nonatomic,assign)int64_t amt;    //交易总额
@property (nonatomic,assign)int64_t vol;    //交易手数
@property (nonatomic,assign)int64_t time;   //时间戳
@property (nonatomic,assign)int64_t pre_time;   //后继时间戳
@property (nonatomic,assign)int     digit;

- (instancetype)initWithDic:(NSDictionary *)dic;

/** 将旧数据合并至相对较新的数据 */
- (void)mergePropertyWithMeta:(IXMetaData *)meta;

/** 将新数据合并至相对较旧的数据 */
- (void)mergePropertyWithNewMeta:(IXMetaData *)meta;

@end
