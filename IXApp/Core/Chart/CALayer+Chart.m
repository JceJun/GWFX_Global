//
//  CALayer+Chart.m
//  IXApp
//
//  Created by Seven on 2017/8/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "CALayer+Chart.h"
#import "CAShapeLayer+Chart.h"

@implementation CALayer (Chart)

- (void)removeAllSublayers
{
    [self.sublayers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperlayer];
    }];
}

- (void)addLineItemWith:(IXKLineModel *)model
{
//    UIColor * color = nil;
//    if (model.rect.size.height > 0) {
//        color =  UIColorWithHex(0xff4653, 1);
//    } else if (model.rect.size.height < 0) {
//        //红涨绿跌时应为绿色
//        color = UIColorWithHex(0x21ce99, 1);
//    } else {
//        color = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.f];
//    }
//    
//    if (model.top != model.low) {
//        CAShapeLayer    * wickL = [CAShapeLayer layerWithP:CGPointMake(model.x, model.top)
//                                                        p2:CGPointMake(model.x, model.low)
//                                               strokeColor:color];
//        [self addSublayer:wickL];
//    }
//    
//    if (model.rect.size.height >= 0) {
//        CAShapeLayer    * layer = [CAShapeLayer layerWithRect:model.rect strokeColor:color fillColor:color];
//        [self addSublayer:layer];
//    } else {
//        CAShapeLayer    * layer = [CAShapeLayer layerWithRect:model.rect strokeColor:color fillColor:[UIColor whiteColor]];
//        [self addSublayer:layer];
//    } 
}

@end
