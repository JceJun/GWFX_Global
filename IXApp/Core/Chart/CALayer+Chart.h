//
//  CALayer+Chart.h
//  IXApp
//
//  Created by Seven on 2017/8/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
@class IXKLineModel;

@interface CALayer (Chart)

- (void)removeAllSublayers;
- (void)addLineItemWith:(IXKLineModel *)model;

@end
