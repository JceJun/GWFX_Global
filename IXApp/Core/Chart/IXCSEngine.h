//
//  IXCSEngine.h
//  IXApp
//
//  Created by Magee on 2017/2/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXCSDisplayV.h"

typedef enum {
    IXCSEngineReqFree,
    IXCSEngineReqBusy
}IXCSEngineReqStatus;

typedef void(^csRequestData)(K_TYPE ktype, time_t endTime, int32_t cnt);
typedef void(^csRequestStatus)(IXCSEngineReqStatus status);
typedef void(^csDoubleClicked)();

@interface IXCSEngine : NSObject

@property (nonatomic, strong)IXCSDisplayV *displayV;
@property (nonatomic, copy)csRequestData request;
@property (nonatomic, copy)csRequestStatus status;
@property (nonatomic, copy)csDoubleClicked clicked;
@property (nonatomic, assign) int64_t   symbolId;   //当前k线产品id

- (instancetype)initWithDigit:(int)digit marketID:(NSInteger)marketID;

/** 设置夜间模式 */
- (void)setNightMode:(BOOL)isNightMode;
/** 设置红涨绿跌 */
- (void)setRedUpBlueDown:(BOOL)isRedUp;

//动态行情
- (void)dynamicDataReseponse:(IXMetaData *)obj;

//插入k线请求数据
- (void)klineDataResponse:(NSMutableArray *)array;

- (void)showKLineWithType:(IXKLineType)type;

@end
