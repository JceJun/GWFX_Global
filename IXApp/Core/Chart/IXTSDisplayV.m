//
//  IXTSDisplayV.m
//  IXApp
//
//  Created by Magee on 2016/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTSDisplayV.h"
#import "IXTSDisplayV+calc.h"
#import "IXTSDisplayV+detect.h"
#import "IXTSDisplayV+draw.h"
#import "IXTSDisplayV+gesture.h"
#import "IXDBScheduleMgr.h"

@interface IXTSDisplayV ()

@property (nonatomic, assign) int   dayOfWeek;

@end

@implementation IXTSDisplayV

- (void)dealloc
{
    [self resignGestures];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.dk_backgroundColorPicker = DKNavBarColor;
        
        self.scndIdxArr  = [[NSMutableArray alloc] init];
        
        self.LIdx = -1;
        self.RIdx = -1;
        [self registGestures];
        [self configColor];
    }
    return self;
}

- (void)setIsNightMode:(BOOL)isNightMode
{
    _isNightMode = isNightMode;
    [self configColor];
    [self setNeedsDisplay];
}

- (void)setSettingRed:(BOOL)settingRed
{
    _settingRed = settingRed;
    [self setNeedsDisplay];
}

//切换日夜间模式
- (void)configColor
{
    if (self.isNightMode) {
        _crossCurveTextColor = UIColorHexFromRGB(0xe9e9ea);
        _crossCurveBgColor = UIColorHexFromRGB(0x4886c9);
    }else{
        _crossCurveTextColor = UIColorHexFromRGB(0xffffff);
        _crossCurveBgColor = UIColorHexFromRGB(0x4c6072);
    }
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    //主图尺寸
    self.curMTopY = 0.f;
    if (self.orientation == IXTSOrientationV)
        self.curMWth = frame.size.width;
    else
        self.curMWth = frame.size.width - 40;
    
    self.curMHth = (self.frame.size.height - 3 * 10.f)/3.f * 2.f;
    self.curMBtmY = self.curMTopY + self.curMHth;
    self.curMMidY = self.curMTopY + self.curMHth/2;
    
    //副图尺寸
    self.curSWth =  self.curMWth;
    
    if (self.orientation == IXTSOrientationV) {
        self.curSTopY = self.curMBtmY + 10.f;
        self.curSBtmY = frame.size.height - 20.f;
    } else {
        self.curSTopY = self.curMBtmY + 20.f;
        self.curSBtmY = frame.size.height - 20;
    }
    
    self.curSHth =  self.curSBtmY - self.curSTopY;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    [self drawForm];
    
    if (!self.pData.count) return;
    
    [self scndIndex];   //副指数据计算
    
    [self mainBryDet];  //主图边界检测
    [self scndBryDet];  //副图边界检测
    
    [self drawMainData];   //主图绘制
    [self drawScndData];   //副图绘制
    
    [self drawYscaleMark]; //价格刻度
    [self drawSYscaleMark];
    
    [self drawCurrentPriceLine];    //最新价格虚线
    
    if (_gstType==IXTSGstTypePrs) {
        [self drawCrossLine];   //十字光标
        [self drawIntersect];   //十字交汇
        [self drawYMoveScale];  //纵轴标签
    } else {
        [self cleanSublayers];
    }
    if (self.orientation == IXTSOrientationH) {
//        [self drawMainExtent];      //右侧主幅度
    }
}

- (NSMutableArray *)pData
{
    if ([self.delegate respondsToSelector:@selector(IXTSDisplayVDataSource)]) {
        return [self.delegate IXTSDisplayVDataSource];
    }
    return nil;
}

- (void)refreshWithTotal:(int)total
{
    self.shouldRefreshDot = NO;
    self.curElmtCnt = total;
    self.LIdx = self.pData.count - 1;
    self.RIdx = 0;
    [self setNeedsDisplay];
}

int i = 0;
- (void)refrashDotWithTotal:(int)total price:(CGFloat)price
{
    i ++;
    i = i%10;
    self.shouldRefreshDot = (i % 2 == 0);
    
    self.dotPrice = price;
    self.curElmtCnt = total;
    [self correctLIdx];
    [self setNeedsDisplay];
}

- (void)correctLIdx
{
    if ((self.LIdx < 0 || self.RIdx < 0) && self.pData.count > 0) {
        if (self.pData.count > self.self.curElmtCnt) {
            self.LIdx = self.curElmtCnt - 1;
            self.RIdx = 0;
        } else {
            self.LIdx = self.pData.count - 1;
            self.RIdx = 0;
        }
    }
}

#pragma mark -
#pragma mark - loadingV

- (UIActivityIndicatorView *)loadingV
{
    if (!_loadingV) {
        _loadingV = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _loadingV.frame = CGRectMake((self.bounds.size.width-40)/2,
                                     (self.bounds.size.height-40)/2,
                                     40, 40);
        [self addSubview:_loadingV];
    }
    UIActivityIndicatorViewStyle    style = self.isNightMode ?
    UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    _loadingV.activityIndicatorViewStyle = style;
    return _loadingV;
}

- (NSArray *)scheduleArr
{
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *now = [calendar components:NSCalendarUnitWeekday fromDate:date];
    int k = now.weekday - 1;
    
    if (k != _dayOfWeek || !_scheduleArr) {
        _dayOfWeek = k;
        NSMutableArray  * arr = [[IXDBScheduleMgr queryScheduleByScheduleCataId:_scheduleCateID dayOfWeek:k] mutableCopy];
        
        [arr sortUsingComparator:^NSComparisonResult(NSDictionary * _Nonnull obj1, NSDictionary * _Nonnull obj2) {
            return [obj1[@"cancelOrder"] intValue] > [obj2[@"cancelOrder"] intValue];
        }];
        _scheduleArr = arr.copy;
    }
    return _scheduleArr;
}

- (void)setLastPrice:(double)lastPrice
{
    _lastPrice = lastPrice;
    NSLog(@" ---- lastPrice = %f ----",lastPrice);
}

@end
