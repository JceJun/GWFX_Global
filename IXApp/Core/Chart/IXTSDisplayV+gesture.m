//
//  IXTSDisplayV+gesture.m
//  IXApp
//
//  Created by Magee on 2016/12/20.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTSDisplayV+gesture.h"

@implementation IXTSDisplayV (gesture)

- (void)registGestures
{
    self.gstPrs = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPrs:)];
    self.gstPrs.minimumPressDuration = 0.2f;
    self.gstPrs.cancelsTouchesInView = NO;
    self.gsttap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handelDoubleClk:)];
    self.gsttap.numberOfTapsRequired = 2;
    
    [self addGestureRecognizer:self.gstPrs];
    [self addGestureRecognizer:self.gsttap];
    
    self.longPressGesOp = [@[] mutableCopy];
}

- (void)resignGestures
{
    [self removeGestureRecognizer:self.gstPrs];
    [self removeGestureRecognizer:self.gsttap];
}

- (void)handleLongPrs:(UILongPressGestureRecognizer *)rec
{
    switch (rec.state) {
        case UIGestureRecognizerStateBegan: {
            [self cleanLongpressOp];
            CGPoint point = [rec locationInView:self];
            CGFloat prsY = point.y;
            if(prsY <= self.curMTopY ||
               prsY >= self.curSBtmY ){
                return;
            }
            self.gstType = IXTSGstTypePrs;
            self.pointCrossLine = point;
            [self setNeedsDisplay];
        }
            break;
        case UIGestureRecognizerStateChanged: {
            CGPoint point = [rec locationInView:self];
            CGFloat prsY = point.y;
            if(prsY <= self.curMTopY ||
               prsY >= self.curSBtmY ){
                return;
            }
            self.pointCrossLine = point;
            [self setNeedsDisplay];
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
            [self cleanLongpressOp];
            
            NSInvocationOperation * op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(longPressEnd) object:nil];
            [self.longPressGesOp addObject:op];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (!op.cancelled) {
                    [op start];
                }
            });
        }
            break;
        default:
            break;
    }
}

- (void)cleanLongpressOp
{
    for (NSInvocationOperation * op in self.longPressGesOp) {
        [op cancel];
    }
    [self.longPressGesOp removeAllObjects];
}

- (void)longPressEnd
{
    [self.longPressGesOp removeAllObjects];
    self.gstType = IXTSGstTypeNone;
    [self setNeedsDisplay];
}

- (void)handelDoubleClk:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(IXTSDisplayVDoubleClicked)]) {
        [self.delegate IXTSDisplayVDoubleClicked];
    }
}

@end

