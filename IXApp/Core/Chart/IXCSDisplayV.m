//
//  IXCSDisplayV.m
//  IXApp
//
//  Created by Magee on 16/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXCSDisplayV.h"
#import "IXCSDisplayV+calc.h"
#import "IXCSDisplayV+draw.h"
#import "IXCSDisplayV+gesture.h"

@interface IXCSDisplayV ()

@property (nonatomic, readwrite) UIColor * hollowRectColor;
@property (nonatomic, readwrite) UIColor * hollowColor;
@property (nonatomic, readwrite) UIColor * crossCurveTextColor;
@property (nonatomic, readwrite) UIColor * crossCurveBgtColor;

@end

@implementation IXCSDisplayV

- (void)dealloc
{
    [self resignGestures];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dk_backgroundColorPicker = DKNavBarColor;
        
        self.mainIdxArr  = [[NSMutableArray alloc] init];
        self.scndIdxArr  = [[NSMutableArray alloc] init];
        
        self.RIdx = -1;
        self.LIdx = -1;
        
        [self registGestures];  //注册手势

        [self configColor];
    }
    return self;
}

- (void)setIsNightMode:(BOOL)isNightMode
{
    _isNightMode = isNightMode;
    [self configColor];
    [self setNeedsDisplay];
}

- (void)setSettingRed:(BOOL)settingRed
{
    _settingRed = settingRed;
    [self configColor];
    [self setNeedsDisplay];
}

//切换日夜间模式
- (void)configColor
{
    if (_isNightMode) {
        if (_settingRed){
            //红色
            _hollowRectColor = UIColorHexFromRGB(0xff4d2d);
        }else{
            //绿色
             _hollowRectColor = UIColorHexFromRGB(0x21ce99);
        }
        _hollowColor = UIColorHexFromRGB(0x262f3e);
        _crossCurveTextColor = UIColorHexFromRGB(0xe9e9ea);
        _crossCurveBgColor = UIColorHexFromRGB(0x4886c9);
    }else{
        if (_settingRed) {
            //红色
            _hollowRectColor = UIColorHexFromRGB(0xff4653);
        }else{
            //绿色
            _hollowRectColor = UIColorHexFromRGB(0x11b873);
        }
        _hollowColor = [UIColor whiteColor];
        _crossCurveTextColor = UIColorHexFromRGB(0xffffff);
        _crossCurveBgColor = UIColorHexFromRGB(0x4c6072);
    }
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    //主图尺寸
    if (self.orientation == IXCSOrientationH) {
        self.curMTopY = 22.f;
    } else {
        self.curMTopY = 0.f;
    }
    self.curLWth  = 0.f;
    self.curMWth  = self.frame.size.width - _curLWth;
    self.curMHth  = (self.frame.size.height - 3 * 10.f)/6.f * 4.f;
    self.curMBtmY = self.curMTopY + self.curMHth;
    
    //副图尺寸
    self.curSWth  = self.curMWth;
    if (self.orientation == IXCSOrientationV) {
        self.curSTopY = self.curMBtmY + 10.f;
        self.curSBtmY = self.frame.size.height - 20.f;
    } else {
        self.curSTopY = self.curMBtmY + 20.f;
        self.curSBtmY = self.frame.size.height - 20.f;
    }
    
    self.curSHth  = self.curSBtmY - self.curSTopY;
    //elmt数量
    self.maxElmtCnt = self.curMWth/2;   //最小占2单位 （4pixel／6pixel）
    self.minElmtCnt = self.curMWth/40;  //最大占40单位（80pixel／120pixel）
    
    if (self.orientation == IXCSOrientationV)
        self.curElmtCnt = 60;
    else
        self.curElmtCnt = 100;
}

#pragma mark - draw method
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self drawForm];    //绘制框架
    
    if (!self.pData.count) {
        [self.highPriceLay removeFromSuperlayer];
        [self.lowPriceLay removeFromSuperlayer];
        return;
    }
    [self mainIndex];   //主指数据计算
    [self scndIndex];   //副指数据计算
    
    [self mainBryDet];  //主图边界检测
    [self scndBryDet];  //副图边界检测
    
    [self drawMainData];   //主图绘制
    
    [self drawMainIndex];  //主指绘制
    [self drawScndIndex];  //副指绘制
    
    [self drawYscaleMark]; //价格刻度
    [self drawSYscaleMark];//
    [self drawXscaleMark]; //时间刻度
    
    [self drawCurrentPriceLine]; //最新价格虚线
    [self drawCurrentPrice];
    
    if (_gstType==IXCSGstTypePrs) {
        [self drawCrossLine];
        [self drawYMoveScale];
        [self drawXMoveScale];
    } else {
        if (self.orientation == IXCSOrientationH) {
            [self drawMainIdxText];
            [self drawScndIdxText];
        }
    }
}

- (IXCSDetailV *)detailV
{
    if (!_detailV) {
        _detailV = [[IXCSDetailV alloc] init];
        _detailV.hidden = YES;
        _detailV.digit = self.curDigit;
    }
    return _detailV;
}

#pragma mark - 重定向k线
- (void)redirectKline
{
    [self.pData removeAllObjects];
    [self.mainIdxArr removeAllObjects];
    [self.scndIdxArr removeAllObjects];
    
    self.gstType = IXCSGstTypeNone;
    if (self.orientation == IXCSOrientationV)
        self.curElmtCnt = 60;
    else
        self.curElmtCnt = 100;
    
    [self checkPdata];
    
    self.RIdx = -1;
    self.LIdx = -1;
    [self correctLIdx];
    
    [self setNeedsDisplay];
}

#pragma mark - 强制刷新
- (void)forceRefresh
{
    [self correctLIdx];
    [self setNeedsDisplay];
}

- (void)correctLIdx
{
    int cnt = _pData.count;
    if ((self.LIdx < 0 || self.RIdx < 0) && cnt > 0) {
        if (cnt > self.curElmtCnt) {
            self.LIdx = self.curElmtCnt - 1;
            self.RIdx = 0;
        } else {
            self.LIdx = cnt - 1;
            self.RIdx = 0;
        }
    }
}

- (void)checkPdata
{
    if ((_pData.count > 60 || !_pData.count) && _pData.count < self.LIdx + 30) {
        if ([self.delegate respondsToSelector:@selector(IXCSDisplayVNeedMoreData)]) {
            [self.delegate IXCSDisplayVNeedMoreData];
        }
    }
}

- (void)setMainIdxType:(IXCSMainIdxType)mainIdxType
{
    _mainIdxType = mainIdxType;
    [self.mainIdxArr removeAllObjects];
    [self setNeedsDisplay];
}

- (void)setScndIdxType:(IXCSScndIdxType)scndIdxType
{
    _scndIdxType = scndIdxType;
    [self.scndIdxArr removeAllObjects];
    [self setNeedsDisplay];
}

#pragma mark -
#pragma mark - lazy loading

- (UIActivityIndicatorView *)loadingV
{
    if (!_loadingV) {
        _loadingV = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _loadingV.frame = CGRectMake((self.bounds.size.width-40)/2,
                                     (self.bounds.size.height-40)/2,
                                     40, 40);
        [self addSubview:_loadingV];
    }
    UIActivityIndicatorViewStyle    style = self.isNightMode ?
    UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    _loadingV.activityIndicatorViewStyle = style;
    return _loadingV;
}

@end
