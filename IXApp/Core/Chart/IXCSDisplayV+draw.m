//
//  IXCSDisplayV+draw.m
//  KLine
//
//  Created by Magee on 16/12/1.
//  Copyright © 2016年 wsz. All rights reserved.
//

#import "IXCSDisplayV+draw.h"
#import "IXCSDisplayV+drawIdx.h"
#import "CATextLayer+Chart.h"

#import "IXMetaData.h"
#import "NSString+FormatterPrice.h"

@implementation IXCSDisplayV (draw)

- (void)drawForm
{
    CGFloat cellH = self.curMHth/2.f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (self.isNightMode) {
        CGContextSetRGBStrokeColor(context, 0.137, 0.165, 0.212, 1.0);
    }else{
        CGContextSetRGBStrokeColor(context, .870, .870, .870, 1.0);
    }
    
    CGContextSetLineWidth(context, .5f);
    CGFloat lengths[] = {3.f,2.f};
    
    //x轴
    CGPoint xL[2];
    CGPoint xR[2];
    for (int i = 0; i <= 2; i++) {
        if (i == 0) {
            xL[i] = CGPointMake(self.curLWth,self.curMTopY + i * cellH);
            xR[i] = CGPointMake(self.frame.size.width, self.curMTopY + i * cellH);
        } else {
            xL[i] = CGPointMake(self.curLWth, self.curSBtmY);
            xR[i] = CGPointMake(self.frame.size.width, self.curSBtmY);
        }

        CGPoint aPoints[2];
        aPoints[0] = xL[i];
        aPoints[1] = xR[i];
        if (i == 1) {
            CGContextSetLineDash(context, 0, lengths, 2);
            CGContextAddLines(context, aPoints, 2);
            CGContextDrawPath(context, kCGPathStroke);
            CGContextSetLineDash(context, 0, lengths, 0);
        } else {
            CGContextAddLines(context, aPoints, 2);
            CGContextDrawPath(context, kCGPathStroke);
        }
    }
}

//主图
CGFloat width;
CGFloat bodyWidth;
- (void)drawMainData
{
    CGFloat scale = (self.curMainTop - self.curMainBtm)/self.curMHth;
    //单个烛图所占区域宽
    if (self.gstType == IXCSGstTypePnc) {
        width = self.curMWth/self.curElmtCntTmp;
    } else {
        width = self.curMWth/self.curElmtCnt;
    }
    //烛图间隙
    CGFloat Interspace = 1;
    if (Interspace > width/2.f) {
        Interspace = width/2.f;
    }
    //烛体宽
    bodyWidth = width - Interspace;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    BOOL    isNightMode = self.isNightMode;
    BOOL    settingRed  = self.settingRed;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        IXMetaData *meta = [self.pData objectAtIndex:i];
        if (meta.open < meta.close) {
            //红涨绿跌时应为红色
            if (settingRed) {
                //红色
                if (isNightMode) {
                    CGContextSetRGBStrokeColor(ctx, 1.f,  0.302, 0.176, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(ctx, .890, .318, .357, 1.0);
                }
            }else{
                //绿色
                if (isNightMode){
                    CGContextSetRGBStrokeColor(ctx, 0.129, 0.808, 0.600, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(ctx, .067, .722, .451, 1.0);
                }
            }
        } else if (meta.open > meta.close) {
            //红涨绿跌时应为绿色
            if (settingRed) {
                //绿色
                if (isNightMode){
                    CGContextSetRGBStrokeColor(ctx, 0.129, 0.808, 0.600, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(ctx, .067, .722, .451, 1.0);
                }
            }else{
                //红色
                if (isNightMode) {
                    CGContextSetRGBStrokeColor(ctx, 1.f,  0.302, 0.176, 1.0);
                }else{
                    CGContextSetRGBStrokeColor(ctx, .890, .318, .357, 1.0);
                }
            }
        } else {
            CGContextSetRGBStrokeColor(ctx,.5, .5, .5, 1.0);
        }
        
        CGContextSetLineWidth(ctx, 1.0);
        //烛芯
        CGPoint wickPoint0,wickPoint1;
        wickPoint0.x = self.curLWth + (self.LIdx - i) * width + width/2;
        wickPoint0.y = self.curMBtmY - (meta.high - self.curMainBtm)/scale;
        
        wickPoint1.x = wickPoint0.x;
        wickPoint1.y = self.curMBtmY - (meta.low - self.curMainBtm)/scale;
        
        CGPoint wick[2] = {wickPoint0,wickPoint1};
        CGContextAddLines(ctx, wick, 2);
        CGContextDrawPath(ctx, kCGPathStroke);
        
        if (wickPoint0.y > wickPoint1.y) {
            [self drawMaxMinPrice:meta topP:wickPoint1 btomP:wickPoint0];
        } else {
            [self drawMaxMinPrice:meta topP:wickPoint0 btomP:wickPoint1];
        }
        
        //实心烛体
        if (meta.open >= meta.close) {
            CGPoint bodyPoint0,bodyPoint1;
            bodyPoint0.x = wickPoint0.x;
            bodyPoint1.x = wickPoint0.x;
            CGPoint body[2];
            if (meta.open == meta.close) {
                bodyPoint0.y = self.curMBtmY - (meta.open - self.curMainBtm)/scale;
                body[0] =CGPointMake(bodyPoint0.x, bodyPoint0.y);
                
                bodyPoint1.y = self.curMBtmY - (meta.close - self.curMainBtm)/scale;
                body[1] =CGPointMake(bodyPoint0.x, bodyPoint1.y + 1);
            } else {
                bodyPoint0.y = self.curMBtmY - (meta.open - self.curMainBtm)/scale;
                body[0] =CGPointMake(bodyPoint0.x, bodyPoint0.y);
                
                bodyPoint1.y = self.curMBtmY - (meta.close - self.curMainBtm)/scale;
                body[1] =CGPointMake(bodyPoint0.x, bodyPoint1.y);
            }
            CGContextSetLineWidth(ctx, bodyWidth);
            CGContextAddLines(ctx, body, 2);
            CGContextDrawPath(ctx, kCGPathStroke);
        }
        
        //空心烛体
        else {
            CGPoint bodyPoint0,bodyPoint1;
            bodyPoint0.x = wickPoint0.x;
            bodyPoint1.x = wickPoint0.x;
            bodyPoint0.y = self.curMBtmY - (meta.open - self.curMainBtm)/scale;
            bodyPoint1.y = self.curMBtmY - (meta.close - self.curMainBtm)/scale;
            
            CGMutablePathRef path = CGPathCreateMutable();
            CGRect rectangle = CGRectMake(bodyPoint0.x - bodyWidth/2.f+.5f,bodyPoint0.y, bodyWidth - 1, bodyPoint1.y - bodyPoint0.y);
            CGPathAddRect(path,NULL,rectangle);
            CGContextAddPath(ctx, path);
            [self.hollowColor setFill];
            [self.hollowRectColor setStroke];
            CGContextSetLineWidth(ctx,1.f);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            CGPathRelease(path);
        }
    }
}

//最高最低价
- (void)drawMaxMinPrice:(IXMetaData *)meta topP:(CGPoint)top btomP:(CGPoint)btomP
{
    if (meta.high == self.maxPriceMeta.high) {
        NSString    * price = [IXPublicTool decimalNumberWithCGFloat:meta.high digit:self.curDigit];
        NSString    * tmp = [NSString stringWithFormat:@"←%@",price];
        CGSize  size = [tmp sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
        CGRect  frame = CGRectZero;
        
        if (top.x > self.frame.size.width - size.width - 10) {
            price = [NSString stringWithFormat:@"%@→",price];
            frame = CGRectMake(top.x - size.width - 2, top.y - size.height/2, size.width, size.height);
        } else {
            price = [NSString stringWithFormat:@"←%@",price];
            frame = CGRectMake(top.x + 2, top.y - size.height/2, size.width, size.height);
        }
        
        if (!self.highPriceLay) {
            self.highPriceLay = [CATextLayer textLayerWithText:price
                                                          font:RO_REGU(10)
                                                     textColor:UIColorHexFromRGB(0x4c6072)
                                               backgroundColor:[UIColor clearColor]
                                                         frame:frame
                                                 textAlignment:NSTextAlignmentCenter];
        } else {
            [CATransaction begin];
            [CATransaction setDisableActions:YES];
            self.highPriceLay.frame = frame;
            [CATransaction commit];
            self.highPriceLay.string = price;
        }
        
        if (!self.highPriceLay.superlayer) {
            [self.layer addSublayer:self.highPriceLay];
        }
    }
    else if (meta.low == self.minPriceMeta.low){
        NSString    * price = [IXPublicTool decimalNumberWithCGFloat:meta.low digit:self.curDigit];
        NSString    * tmp = [NSString stringWithFormat:@"←%@",price];
        CGSize  size = [tmp sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
        CGRect  frame = CGRectZero;
        
        if (top.x > self.frame.size.width - size.width - 10) {
            price = [NSString stringWithFormat:@"%@→",price];
            frame = CGRectMake(btomP.x - size.width - 2, btomP.y - size.height/2, size.width, size.height);
        } else {
            price = [NSString stringWithFormat:@"←%@",price];
            frame = CGRectMake(btomP.x + 2, btomP.y - size.height/2, size.width, size.height);
        }
        
        if (!self.lowPriceLay) {
            self.lowPriceLay = [CATextLayer textLayerWithText:price
                                                         font:RO_REGU(10)
                                                    textColor:UIColorHexFromRGB(0x4c6072)
                                              backgroundColor:[UIColor clearColor]
                                                        frame:frame
                                                textAlignment:NSTextAlignmentCenter];
        } else {
            [CATransaction begin];
            [CATransaction setDisableActions:YES];
            self.lowPriceLay.frame = frame;
            [CATransaction commit];
            self.lowPriceLay.string = price;
        }
        
        if (!self.lowPriceLay.superlayer) {
            [self.layer addSublayer:self.lowPriceLay];
        }
    }
}

//主指
- (void)drawMainIndex
{
    NSInteger cnt;
    if (self.gstType==IXCSGstTypePnc) {
        cnt = self.curElmtCntTmp;
    } else {
        cnt = self.curElmtCnt;
    }
    switch (self.mainIdxType) {
        case IXCSMainIdxTypeMA:
            [self draw_M_MA:cnt width:width];
            break;
        case IXCSMainIdxTypeBBI:
            [self draw_M_BBI:cnt width:width];
            break;
        case IXCSMainIdxTypeBOLL:
            [self draw_M_BOLL:cnt width:width];
            break;
        case IXCSMainIdxTypeMIKE:
            [self draw_M_MIKE:cnt width:width];
            break;
        case IXCSMainIdxTypePBX:
            [self draw_M_PBX:cnt width:width];
            break;
        default:
            break;
    }
}

//副指
- (void)drawScndIndex
{
    NSInteger cnt;
    if (self.gstType==IXCSGstTypePnc) {
        cnt =self.curElmtCntTmp;
    } else {
        cnt =self.curElmtCnt;
    }
    
    switch (self.scndIdxType) {
        case IXCSScndIdxTypeMACD:
            [self draw_S_MACD:cnt width:width bodyWidth:bodyWidth];
            break;
        case IXCSScndIdxTypeARBR:
            [self draw_S_ARBR:cnt width:width];
            break;
        case IXCSScndIdxTypeATR:
            [self draw_S_ATR:cnt width:width];
            break;
        case IXCSScndIdxTypeBIAS:
            [self draw_S_BIAS:cnt width:width];
            break;
        case IXCSScndIdxTypeCCI:
            [self draw_S_CCI:cnt width:width];
            break;
        case IXCSScndIdxTypeDKBY:
            [self draw_S_DKBY:cnt width:width];
            break;
        case IXCSScndIdxTypeKD:
            [self draw_S_KD:cnt width:width];
            break;
        case IXCSScndIdxTypeKDJ:
            [self draw_S_KDJ:cnt width:width];
            break;
        case IXCSScndIdxTypeQHLSR:
            [self draw_S_QHLSR:cnt width:width];
            break;
        case IXCSScndIdxTypeWR:
            [self draw_S_WR:cnt width:width];
            break;
        case IXCSScndIdxTypeRSI:
            [self draw_S_RSI:cnt width:width];
            break;
        case IXCSScndIdxTypeLWR:
            [self draw_S_LWR:cnt width:width];
            break;
        default:
            break;
    }
}

//主图y轴静态刻度
- (void)drawYscaleMark
{
    CGPoint point;
    for (int i = 0; i < 3; i++) {
         CGFloat price = 0.f;
        switch (i) {
            case 0: {
                point = CGPointMake(5, self.curMTopY + i * self.curMHth/2.f);
                price = self.curMainTopU;
            }
                break;
            case 1: {
                point = CGPointMake(5, self.curMTopY + i * self.curMHth/2.f - 6);
                price = (self.curMainTopU + self.curMainBtmU) / 2.f;
            }
                break;
            case 2: {
                point = CGPointMake(5, self.curMTopY + i * self.curMHth/2.f - 12);
                price = self.curMainBtmU;
            }
                break;
            default:
                break;
        }
        NSString *prc = [IXPublicTool decimalNumberWithCGFloat:price digit:self.curDigit];
        [prc drawInRect:CGRectMake(point.x, point.y, 100, 20) withAttributes:@{NSFontAttributeName:RO_REGU(10),
                                                                               NSForegroundColorAttributeName:UIColorHexFromRGB(0x99abba)}];
    }
}

//副图x轴静态刻度
- (void)drawSYscaleMark
{
    NSString *prc = nil;
    CGPoint point;
    for(int i = 0; i < 2; i++) {
        if (i == 0) {
            point = CGPointMake(5, self.curSTopY);
            prc = [IXPublicTool decimalNumberWithCGFloat:self.curScndTop digit:self.curDigit];
        } else {
            point = CGPointMake(5, self.curSBtmY - 12);
            prc = [IXPublicTool decimalNumberWithCGFloat:self.curScndBtm digit:self.curDigit];
        }
        [prc drawInRect:CGRectMake(point.x, point.y, 100, 20) withAttributes:@{NSFontAttributeName:RO_REGU(10),
                                                                               NSForegroundColorAttributeName:UIColorHexFromRGB(0x99abba)}];
    }
}

//主副图x轴静态刻度
- (void)drawXscaleMark
{
    for (int i = 0;i < 2; i ++) {
        CGFloat tmpX = 0.f;
        if (i == 0) {
            tmpX = self.curLWth + self.curMWth / 4.f;
        } else {
            tmpX = self.curLWth + self.curMWth / 4.f * 3.f;
        }
        
        int idx = self.LIdx - (int)(tmpX - width/2 - self.curLWth)/width;
        if(idx > self.pData.count || idx < 0) {
            return;
        }
        IXMetaData *meta = self.pData[idx];

        NSString    * formater = self.csType <= IXKLineTypeMonth ? @"yyyy/MM/dd" : @"MM/dd HH:mm";
        NSString *testStr = [IXEntityFormatter timeIntervalToString:meta.time formate:formater];
        [testStr drawInRect:CGRectMake(tmpX - 25, self.curSBtmY, 200, 14)
             withAttributes:@{NSFontAttributeName:RO_REGU(10),
                              NSForegroundColorAttributeName:UIColorHexFromRGB(0x99abba)}];
    }
}

CGPoint lpPoints[2];
- (void)drawCurrentPriceLine
{
    IXMetaData * meta = [self.pData firstObject];
    CGFloat scale = (self.curMainTop - self.curMainBtm)/self.curMHth;
    CGFloat y = self.curMBtmY - (meta.close - self.curMainBtm)/scale;
    if (y < self.curMTopY || y > self.curMBtmY) {
        return;
    }
    
    lpPoints[0] = CGPointMake(self.curLWth - 5, y);
    lpPoints[1] = CGPointMake(self.curLWth + self.curMWth, y);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat lengths[] = {6,3,2,3};
    CGContextSetLineDash(context, 0, lengths, 4);
    CGContextSetLineWidth(context, .5f);
    
    if (self.isNightMode) {
        CGContextSetRGBStrokeColor(context, 1., 1.,  1., 1);
    }else{
        CGContextSetRGBStrokeColor(context, 0.600, 0.670,  0.729, 1);
    }
    
    CGContextMoveToPoint(context, lpPoints[0].x, lpPoints[0].y);
    CGContextAddLineToPoint(context, lpPoints[1].x,lpPoints[1].y);
    
    CGContextStrokePath(context);
}

- (void)drawCurrentPrice
{
    IXMetaData  * meta = [self.pData firstObject];
    CGFloat     scale = (self.curMainTop - self.curMainBtm)/self.curMHth;
    
    CGFloat     y = self.curMBtmY - (meta.close - self.curMainBtm)/scale;
    if (y < self.curMTopY || y > self.curMBtmY) {
        return;
    }
    
    NSString    * str = [NSString formatterPrice:[NSString stringWithFormat:@"%f",meta.close]
                                      WithDigits:self.curDigit];
    CGSize  size = [str sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
    CGRect  rect = CGRectMake(5, y - 7.f, size.width + 5, 14);
    if (self.pData.count <= 10) {
        rect.origin.x = self.frame.size.width - size.width - 5;
    }
    [str drawInRect:rect
     withAttributes:@{NSFontAttributeName:RO_REGU(10),
                      NSForegroundColorAttributeName:self.crossCurveTextColor,
                      NSBackgroundColorAttributeName:self.crossCurveBgColor}];
}

//移动十字
int idx;
CGPoint cxPoints[2];
CGPoint cyPoints[2];

- (void)drawCrossLine
{
    cxPoints[0] = CGPointMake(5, self.pointCrossLine.y);
    cxPoints[1] = CGPointMake(self.frame.size.width, self.pointCrossLine.y);
    
    CGFloat tmpX = self.pointCrossLine.x < self.curLWth ? self.curLWth : self.pointCrossLine.x;
    
    idx = self.LIdx - (int)(tmpX - width/2 - self.curLWth)/width;
    tmpX = self.curLWth + (self.LIdx - idx) * width + width/2;
    cyPoints[0] = CGPointMake(tmpX, self.curMTopY);
    cyPoints[1] = CGPointMake(tmpX, self.curSBtmY);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat lengths[] = {6,3,2,3};
    CGContextSetLineDash(context, 0, lengths, 4);
    CGContextSetLineWidth(context, 1.f);
    
    if (self.isNightMode) {
        CGContextSetRGBStrokeColor(context, 0.282, 0.525,  0.788, 1);
    }else{
        CGContextSetRGBStrokeColor(context, 0.298, 0.376,  0.447, 1);
    }
    
    CGContextMoveToPoint(context, cxPoints[0].x, cxPoints[0].y);
    CGContextAddLineToPoint(context, cxPoints[1].x,cxPoints[1].y);
    
    CGContextMoveToPoint(context, cyPoints[0].x, cyPoints[0].y);
    CGContextAddLineToPoint(context, cyPoints[1].x, cyPoints[1].y);
    
    CGContextStrokePath(context);
}

//移动价格刻度
- (void)drawYMoveScale
{
    CGFloat hth = 12.f;
    CGFloat disY = cxPoints[0].y > self.curMTopY + hth/2.f ? cxPoints[0].y - hth/2.f : self.curMTopY;
    disY = disY > self.curSBtmY - hth ? self.curSBtmY - hth : disY;
    
    NSString *str = @"";
    //为防止刚到主图底线价格就立即消失，再此处多加3的偏移量，幅图原理相同
    if (cxPoints[0].y <= self.curMBtmY + 3) {
        str = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.curMainTop - ((self.curMainTop - self.curMainBtm)/self.curMHth) * cxPoints[0].y] WithDigits:self.curDigit];
    }
    else if (cxPoints[0].y >= self.curSTopY - 3) {
        str = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.curScndTop - ((self.curScndTop - self.curScndBtm)/self.curSHth) * (cxPoints[0].y - self.curSTopY)] WithDigits:self.curDigit];
    }
    
    if (str.length) {
        [str drawInRect:CGRectMake(5, disY, 100, hth)
         withAttributes:@{NSFontAttributeName:RO_REGU(10),
                          NSForegroundColorAttributeName:self.crossCurveTextColor,
                          NSBackgroundColorAttributeName:self.crossCurveBgColor}];
    }
}

//移动时间刻度
- (void)drawXMoveScale
{
    CGFloat wth = 80.f;
    CGFloat disX;
    
    if (cyPoints[0].x <= wth/2) {
        disX = 0.f;
    } else if(wth/2.f < cyPoints[0].x && cyPoints[0].x <= self.curMWth - wth/2.f) {
        disX = cyPoints[0].x - wth/2.f;
    } else {
        disX = self.curMWth - wth;
    }
    
    if(idx > self.pData.count || idx < 0) {
        return;
    }
    IXMetaData *curMeta = self.pData[idx];
    IXMetaData *lstMeta = (idx + 1) < self.pData.count ? self.pData[idx + 1] : nil;
    
    NSString    * formater = self.csType <= IXKLineTypeMonth ? @"yyyy/MM/dd" : @"MM/dd HH:mm";
    NSString *testStr = [IXEntityFormatter timeIntervalToString:curMeta.time formate:formater];

    NSString    * weekDayString = [IXEntityFormatter weekDayStringWithTimeInterval:curMeta.time];
    testStr = [NSString stringWithFormat:@" %@ %@ ",testStr,weekDayString];
    
    [testStr drawInRect:CGRectMake(disX, self.curSBtmY, 200, 14)
         withAttributes:@{NSFontAttributeName:RO_REGU(10),
                          NSForegroundColorAttributeName:self.crossCurveTextColor,
                          NSBackgroundColorAttributeName:self.crossCurveBgColor}];
    [self.detailV reloadUIWith:curMeta lst:lstMeta isCurrentData:idx == 0];
    if (self.orientation == IXCSOrientationH) {
        [self drawMainIdxText:idx];
        [self drawScndIdxText:idx];
    }
}

- (void)drawMainIdxText:(NSInteger)idx
{
    switch (self.mainIdxType) {
        case IXCSMainIdxTypeMA:
            [self draw_M_MA_T:idx];
            break;
            
        case IXCSMainIdxTypeBBI:
            [self draw_M_BBI_T:idx];
            break;
            
        case IXCSMainIdxTypeBOLL:
            [self draw_M_BOLL_T:idx];
            break;
            
        case IXCSMainIdxTypeMIKE:
            [self draw_M_MIKE_T:idx];
            break;
            
        case IXCSMainIdxTypePBX:
            [self draw_M_PBX_T:idx];
            break;
        default:
            break;
    }
}

- (void)drawScndIdxText:(NSInteger)idx
{
    switch (self.scndIdxType) {
        case IXCSScndIdxTypeMACD:
            [self draw_S_MACD_T:idx];
            break;
            
        case IXCSScndIdxTypeARBR:
            [self draw_S_ARBR_T:idx];
            break;
            
        case IXCSScndIdxTypeATR:
            [self draw_S_ATR_T:idx];
            break;
            
        case IXCSScndIdxTypeBIAS:
            [self draw_S_BIAS_T:idx];
            break;
            
        case IXCSScndIdxTypeCCI:
            [self draw_S_CCI_T:idx];
            break;
            
        case IXCSScndIdxTypeDKBY:
            [self draw_S_DKBY_T:idx];
            break;
            
        case IXCSScndIdxTypeKD:
            [self draw_S_KD_T:idx];
            break;
            
        case IXCSScndIdxTypeKDJ:
            [self draw_S_KDJ_T:idx];
            break;
            
        case IXCSScndIdxTypeLWR:
            [self draw_S_LWR_T:idx];
            break;
            
        case IXCSScndIdxTypeQHLSR:
            [self draw_S_QHLSR_T:idx];
            break;
            
        case IXCSScndIdxTypeRSI:
            [self draw_S_RSI_T:idx];
            break;
            
        case IXCSScndIdxTypeWR:
            [self draw_S_WR_T:idx];
            break;
        default:
            break;
    }
}

- (void)drawMainIdxText
{
    [self drawMainIdxText:self.RIdx];
}

- (void)drawScndIdxText
{
    [self drawScndIdxText:self.RIdx];
}

@end
