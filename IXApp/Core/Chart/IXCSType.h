//
//  IXCSType.h
//  KLine
//
//  Created by Magee on 2016/12/6.
//  Copyright © 2016年 wsz. All rights reserved.
//

#ifndef IXCSType_h
#define IXCSType_h

/**主指key*/

//MA
#define kMA_5       @"MA5"
#define kMA_10      @"MA10"
#define kMA_20      @"MA20"
#define kMA_60      @"MA60"

#define cMA_5        5.f
#define cMA_10      10.f
#define cMA_20      20.f
#define cMA_60      60.f

//BBI
#define kBBI_BBI    @"BBI"

#define cBBI_3       3.f
#define cBBI_6       6.f
#define cBBI_12     12.f
#define cBBI_24     24.f

//BOLL
#define kBOLL_MA   @"MA"
#define kBOLL_UP   @"UP"
#define kBOLL_DN   @"DN"

#define cBOLL_2      2.f
#define cBOLL_26    26.f

//MIKE
#define kMIKE_WR    @"WR"
#define kMIKE_MR    @"MR"
#define kMIKE_SR    @"SR"
#define kMIKE_WS    @"WS"
#define kMIKE_MS    @"MS"
#define kMIKE_SS    @"SS"

#define cMIKE_12    12.f

//PBX
#define kPBX_1      @"PBX1"
#define kPBX_2      @"PBX2"
#define kPBX_3      @"PBX3"
#define kPBX_4      @"PBX4"

#define cPBX_4       4.f
#define cPBX_6       6.f
#define cPBX_9       9.f
#define cPBX_13     13.f

/**副指key*/

//MACD
#define kMACD_DIF       @"DIFF"
#define kMACD_DEA       @"DEA"
#define kMACD_MAC       @"MAC"

#define cEMA_9           9.f
#define cEMA_12         12.f
#define cEMA_26         26.f

//ARBR
#define kARBR_AR        @"AR"
#define kARBR_BR        @"BR"

#define cARBR_26        26.f

//ATR
#define kATR_TR         @"TR"
#define kATR_ATR        @"ATR"

#define cATR_14         14.f

//BIAS
#define kBIAS_1         @"BIAS1"
#define kBIAS_2         @"BIAS2"
#define kBIAS_3         @"BIAS3"

#define cBIAS_6         6.f
#define cBIAS_12        12.f
#define cBIAS_24        24.f

//CCI
#define kCCI_CCI        @"CCI"

#define cCCI_14         14.f

//DKBY
#define kDKBY_ENE1      @"ENE1"
#define kDKBY_ENE2      @"ENE2"

#define cDKBY_5         5.f
#define cDKBY_8         8.f
#define cDKBY_13        13.f
#define cDKBY_21        21.f

//KD
#define kKD_K           @"K"
#define kKD_D           @"D"

#define cKKD_9          9.f
#define cKKD_3          3.f


//KDJ
#define kKDJ_K          @"K"
#define kKDJ_D          @"D"
#define kKDJ_J          @"J"

//LWR
#define kLWR_1          @"LWR1"
#define kLWR_2          @"LWR2"

#define cLWR_9          9.0f
#define cLWR_3          3.0f

//QHLSR
#define kQHLSR_5        @"QHLSR5"
#define kQHLSR_10       @"QHLSR10"

//RSI
#define kRSI_1          @"RSI1"
#define kRSI_2          @"RSI2"
#define kRSI_3          @"RSI3"

#define cRSI_6          6.0f
#define cRSI_12         12.0f
#define cRSI_24         24.0f

//WR
#define kWR_1           @"WR1"
#define kWR_2           @"WR2"

#define cWR_6           6.f
#define cWR_10          10.f

/**Others*/
//数据请求最小时间间隔
#define k_Req_Interval 5u
#define k_Req_Intervalsssss 5u

//主指类型
typedef enum {
    IXCSMainIdxTypeMA,
    IXCSMainIdxTypeBBI,
    IXCSMainIdxTypeBOLL,
    IXCSMainIdxTypeMIKE,
    IXCSMainIdxTypePBX
} IXCSMainIdxType;

//副指类型
typedef enum {
    IXCSScndIdxTypeMACD,
    IXCSScndIdxTypeARBR,
    IXCSScndIdxTypeATR,
    IXCSScndIdxTypeBIAS,
    IXCSScndIdxTypeCCI,
    IXCSScndIdxTypeDKBY,
    IXCSScndIdxTypeKD,
    IXCSScndIdxTypeKDJ,
    IXCSScndIdxTypeLWR,
    IXCSScndIdxTypeQHLSR,
    IXCSScndIdxTypeRSI,
    IXCSScndIdxTypeWR
} IXCSScndIdxType;

//横竖屏
typedef enum {
    IXCSOrientationV,
    IXCSOrientationH
} IXCSOrientationType;

typedef enum {
    IXTSOrientationV,
    IXTSOrientationH
} IXTSOrientationType;

//手势类型
typedef enum {
    IXCSGstTypeNone,
    IXCSGstTypePan,         //拖动
    IXCSGstTypePrs,         //长按
    IXCSGstTypePnc          //缩放
} IXCSGstType;

#endif /* IXCSType_h */
