//
//  IXCSDataSC.h
//  IXApp
//
//  Created by Magee on 2017/2/10.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
@class IXMetaData;

@interface IXCSDataSC : NSObject
//动态k线更新至新的时段时，请求最新的两条数据
@property (nonatomic, copy) void(^needLatestData)(NSInteger count);

@property (nonatomic,strong)NSMutableArray  * currentTc;

/** 处理其他数据源 */
- (void)dealWithNormalData:(NSArray <IXMetaData *>*)array;

/** 处理周k数据源 */
- (void)dealWithWeekDataWith:(NSArray <IXMetaData *>*)array;

/** 处理月k数据源 */
- (void)dealWithMonthDataWith:(NSArray <IXMetaData *>*)array;


/**
 跳动k线行情数据处理
 
 @param data 行情数据
 @return 是否需要刷新
 */
- (BOOL)insertNewQuoteData:(IXMetaData *)data;

@end
