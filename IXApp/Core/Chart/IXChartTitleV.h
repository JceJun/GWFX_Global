//
//  IXChartTitleV.h
//  IXApp
//
//  Created by Magee on 2017/3/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IXChartTitleV : UIView

@property (nonatomic,copy)void(^btnClick)(NSInteger tag);
@property (nonatomic,assign)NSInteger   selectedTag;

@end
