//
//  IXTSDataSC.m
//  IXApp
//
//  Created by Magee on 2017/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXTSDataSC.h"
#import "IXMetaData.h"
#import "IXDBScheduleMgr.h"

#define DayofSeconds 86400

@interface IXTSDataSC ()
{
    BOOL isDynamicWorking;
    int total;
}

@end

@implementation IXTSDataSC

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.min1CT  = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)display
{
    if (isDynamicWorking) {
        return;
    }
    if ([self.delegate respondsToSelector:@selector(metaDataShuoldBeRequested)]) {
        [self.delegate metaDataShuoldBeRequested];
    }
}

/** 重新请求数据时调用 */
- (void)resetDataSuorce
{
    [_min1CT removeAllObjects];
    _lastMeta = nil;
    isDynamicWorking = NO;
    total = 0;
}

#pragma mark ------------------------------------------------------------------------
#pragma mark -----------------------------statical datas-----------------------------

- (void)requestDataRsp:(NSMutableArray *)array
{
    if (![self isDataTypeMatchWithCT:array]) return;
    
    NSMutableArray  * newMetaArr = [@[] mutableCopy];
    for (int i = 0; i < array.count; i++) {
        IXMetaData *meta = [[IXMetaData alloc] initWithDic:array[i]];
        [newMetaArr addObject:meta];
    }
    
    IXMetaData  * newFirstMeta = [newMetaArr firstObject];
    IXMetaData  * oldFirstmeta = [_min1CT firstObject];
    if (newFirstMeta && oldFirstmeta && newFirstMeta.time >= oldFirstmeta.time) {
        IXMetaData  * lastMeta = [newMetaArr lastObject];
        IXMetaData  * firstMeta = [newMetaArr firstObject];
        __block NSInteger   startIdx = 0;
        [_min1CT enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(IXMetaData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.time >= lastMeta.time && obj.time <= firstMeta.time) {
                [_min1CT removeObject:obj];
                startIdx = idx;
            }
        }];
        
        [_min1CT insertObjects:newMetaArr atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIdx, newFirstMeta.count)]];
    } else {
        [_min1CT addObjectsFromArray:newMetaArr];
    }
    
    IXMetaData *meta = _min1CT.lastObject;
    if (meta.offset + meta.count == meta.total + 1) {
        IXMetaData *data0 = _min1CT[0];
        int dayOfWeek = [IXTSDataSC indexOftheWeek:data0.time];
        NSArray *arr = [IXDBScheduleMgr queryScheduleByScheduleCataId:_scheduleCateID dayOfWeek:dayOfWeek];
        
        int effect =0;
        int min = INT_MAX;
        total = 0;
        for (NSDictionary *dic in arr) {
            int enable = [dic[@"enable"] intValue];
            if (enable <= 0) {
                continue;
            }
            
            int startTime = [dic[@"startTime"] intValue];
            int endTime   = [dic[@"endTime"]   intValue];
            total += endTime - startTime;
            if (startTime < min) {
                min = startTime;
            }
        }
        
        //cut计算逻辑请查看《IX各种操作逻辑 - 分时图cut计算逻辑》
        
        //cut为计算出的起始时间
        uint64_t cut = data0.time - data0.time % DayofSeconds;
        cut = cut + min * 60;
        //对应情况1修改cut
        if (min >= 0 && data0.time % DayofSeconds < min * 60) {
            cut = cut - DayofSeconds;
        }
        //对应情况2修改cut
        if (min < 0 && data0.time % DayofSeconds > (1440 + min) * 60) {
            cut = cut + DayofSeconds;
        }
        
        for (int i = 0; i<_min1CT.count; i++) {
            IXMetaData *meta = _min1CT[i];

            effect = i;
            
            if (meta.time < cut) {
                break;
            } else if (meta.time == cut) {
                effect += 1;
                break;
            }
        }
        
        if (effect <= _min1CT.count) {
            if (effect > 0) {
                [_min1CT removeObjectsInRange:NSMakeRange(effect, _min1CT.count - effect)];
                if ([self.delegate respondsToSelector:@selector(displayVShouldBeRefreshedWithToltal:)]) {
                    [self.delegate displayVShouldBeRefreshedWithToltal:total];
                    isDynamicWorking = YES;
                }
            } else {
                [_min1CT removeAllObjects];
                WLog(@"effect == 0!!!!");
            }
        } else {
            ELog(@"total、effect 有问题");
        }
    }
}

- (BOOL)isDataTypeMatchWithCT:(NSMutableArray *)arr
{
    NSDictionary *dic = arr.lastObject;
    if ([[dic allKeys] containsObject:@"type"]) {
        if ([dic[@"type"] integerValue] == MIN_1) {
            return YES;
        }
    }
    return NO;
}

#pragma mark ------------------------------------------------------------------------
#pragma mark -----------------------------dynamic datas------------------------------

- (void)dynamicDataRsp:(IXMetaData *)meta needRefresh:(BOOL)need
{
    if (!isDynamicWorking) return;
    
    IXMetaData *ct_meta = _min1CT.firstObject;
    [ct_meta mergePropertyWithNewMeta:meta];
    
    if (need) {
        //刷新dot
        if (self.lastMeta == nil) {
            //根据这个meta刷新dot
            self.lastMeta = meta;
            
            if ([self.delegate respondsToSelector:@selector(displayDotShouldActionWithTotal:price:)]) {
                [self.delegate displayDotShouldActionWithTotal:total price:meta.close];
            }
        } else {
            self.lastMeta = meta;
            if ([self.delegate respondsToSelector:@selector(displayDotShouldActionWithTotal:price:)]) {
                [self.delegate displayDotShouldActionWithTotal:total price:meta.close];
            }
        }
    }
    
    //刷新容器
    if (meta.time - ct_meta.time > 59) {
        [_min1CT insertObject:meta atIndex:0];
        if ([self.delegate respondsToSelector:@selector(displayVShouldBeRefreshedWithToltal:)]) {
            [self.delegate displayVShouldBeRefreshedWithToltal:total];
        }
    }
}

#pragma mark ------------------------------------------------------------------------
#pragma mark ---------------------------------others---------------------------------

//是否在同一周
+ (int)indexOftheWeek:(int64_t)time1
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *now = [calendar components:NSCalendarUnitWeekday fromDate:date];
    int k = now.weekday - 1;
    if (k < 0) {
        ELog(@"indexOftheWee计算错误");
    }
    return k;
}

@end
