//
//  IXCSDisplayV+calc.h
//  KLine
//
//  Created by Magee on 2016/12/8.
//  Copyright © 2016年 wsz. All rights reserved.
//

#import "IXCSDisplayV.h"

@interface IXCSDisplayV (calc)

- (void)mainIndex;  //主图指标计算
- (void)scndIndex;  //副图指标计算

- (void)mainBryDet; //主图边界检测
- (void)scndBryDet; //副图边界检测

@end
