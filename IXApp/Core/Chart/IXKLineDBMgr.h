//
//  IXKLineDBMgr.h
//  FXApp
//
//  Created by Seven on 2017/8/7.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXKSqlHeader.h"
@class IXMetaData;

#define kMaxRecord      180
#define kWeekRecord     360
#define kMonthRecord    10000

@interface IXKLineDBMgr : NSObject

+ (instancetype)shareInstance;


/**
 获取k线缓存数据，若返回值为nil，则根据RAM中最后一条k线数据的时间请求180条

 @param id_p 产品id
 @param type k线类型
 @patam time GMT0时间（单位：s）
 @return [180条k线数据 or nil]
 */
+ (NSArray <IXMetaData*>*)db_getKLineDataWithSymbolId:(int64_t)id_p
                                            kLineType:(K_TYPE)type
                                          befomreTime:(int64_t)time;



/**
 存储行情数据

 @param id_p 产品id
 @param metaArr 行情数组
 */
+ (void)db_saveDataWithSymbolId:(int64_t)id_p metaArray:(NSArray <IXMetaData *>*)metaArr;



@end
