//
//  IXTSDisplayV.h
//  IXApp
//
//  Created by Magee on 2016/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IXCSType.h"
#import "IXMetaData.h"

//手势类型
typedef enum{
    IXTSGstTypeNone,
    IXTSGstTypePan,    //拖动
    IXTSGstTypePrs,    //长按
}IXTSGstType;


@protocol IXTSDisplayVDelegate <NSObject>

- (NSMutableArray *)IXTSDisplayVDataSource;
- (void)IXTSDisplayVDoubleClicked;

@end

@interface IXTSDisplayV : UIView

@property (nonatomic,strong)NSMutableArray *pData;         //元数据
@property (nonatomic,assign)double lastPrice;              //昨日收盘价
@property (nonatomic,assign)CGFloat dotPrice;
@property (nonatomic,assign)float  curDigit;
@property (nonatomic,strong)NSMutableArray *scndIdxArr;    //副指容器(缺省MACD)

//主图坐标
@property (nonatomic,assign)CGFloat    curMTopY;       //主图顶Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curMMidY;       //主图中Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curMBtmY;       //主图底Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curMWth;        //主图宽
@property (nonatomic,assign)CGFloat    curMHth;        //主图高

@property (nonatomic,assign)CGFloat     maxPrice;   //最高价
@property (nonatomic,assign)CGFloat     minPrice;   //最低价

//副图坐标
@property (nonatomic,assign)CGFloat    curSTopY;       //副图顶Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curSBtmY;       //副图底Y坐标(二象限为原点)
@property (nonatomic,assign)CGFloat    curSWth;        //副图宽
@property (nonatomic,assign)CGFloat    curSHth;        //副图高

//边界界限
@property (nonatomic,assign)CGFloat    curMainTop;     //主图上限(量化)

@property (nonatomic,assign)CGFloat    curScndTop;     //副图上限(量化)
@property (nonatomic,assign)CGFloat    curScndBtm;     //副图下限(量化)

@property (nonatomic,assign)NSInteger curElmtCnt;      //绘图区当前elmt数量

@property (nonatomic,assign)NSInteger LIdx;            //绘图区左游标
@property (nonatomic,assign)NSInteger RIdx;            //绘图区右游标

@property (nonatomic,assign)IXTSOrientationType orientation;    //横竖屏

@property (nonatomic,strong)UIScrollView    * scrollView;

//手势
@property (nonatomic,assign)IXTSGstType                    gstType;  //当前手势
@property (nonatomic,strong)UILongPressGestureRecognizer   *gstPrs;  //手势长按
@property (nonatomic,strong)UITapGestureRecognizer         *gsttap;  //手势双击
@property (nonatomic,strong)NSMutableArray  * longPressGesOp;   //处理长按手势延时操作

@property (nonatomic,assign)CGPoint    pointCrossLine;
@property (nonatomic,strong)UIActivityIndicatorView * loadingV;
@property (nonatomic,assign)id<IXTSDisplayVDelegate> delegate;

@property (nonatomic,assign)BOOL    shouldRefreshDot;
@property (nonatomic,assign)BOOL    isNightMode;    //夜间模式
@property (nonatomic,assign)BOOL    settingRed;     //红涨绿跌
@property (nonatomic, readonly) UIColor * crossCurveTextColor;  //十字刻线字体颜色
@property (nonatomic, readonly) UIColor * crossCurveBgColor;  //十字刻线背景颜色
@property (nonatomic, strong) NSArray * scheduleArr;  //用户计算显示分时图时间
@property (nonatomic, assign) int       scheduleCateID;

@property (nonatomic, strong) CAShapeLayer  * mainLay;  //折线
@property (nonatomic, strong) CALayer       * shadowLay;    //shadow
@property (nonatomic, strong) CAShapeLayer  * curPriceLine;
@property (nonatomic, strong) CATextLayer   * curPriceLay;

@property (nonatomic, strong) CATextLayer   * mainPrice0Lay;    //主图价格分级
@property (nonatomic, strong) CATextLayer   * mainPrice1Lay;
@property (nonatomic, strong) CATextLayer   * mainPrice2Lay;
@property (nonatomic, strong) CATextLayer   * secPrice0Lay;     //副图价格
@property (nonatomic, strong) CATextLayer   * secPrice1Lay;

//十字线组件
@property (nonatomic, strong) CAShapeLayer  * cros_horLay;  //十字线横线
@property (nonatomic, strong) CAShapeLayer  * cros_verLay;  //十字线竖线
@property (nonatomic, strong) CAShapeLayer  * cros_cirLay;  //十字线圆点
@property (nonatomic, strong) CATextLayer   * cros_priceLay;    //价格
@property (nonatomic, strong) CATextLayer   * cros_timeLay; //时间

- (void)refreshWithTotal:(int)total;

- (void)refrashDotWithTotal:(int)total price:(CGFloat)price;

@end

