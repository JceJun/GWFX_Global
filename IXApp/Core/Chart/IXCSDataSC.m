//
//  IXCSDataSC.m
//  IXApp
//
//  Created by Magee on 2017/2/10.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCSDataSC.h"
#import "IXMetaData.h"
#import "NSDate+IX.h"


@implementation IXCSDataSC

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentTc = [[NSMutableArray alloc] init];
    }
    return self;
}

/** 处理其他数据源 */
- (void)dealWithNormalData:(NSArray <IXMetaData *>*)array
{
    array = [array sortedArrayUsingComparator:^NSComparisonResult(IXMetaData * _Nonnull obj1, IXMetaData * _Nonnull obj2) {
        return obj1.time < obj2.time;
    }];
    
    IXMetaData  * meta1 = [array firstObject];
    IXMetaData  * meta2 = [_currentTc lastObject];
    if (meta2.time > meta1.time) {
        [_currentTc addObjectsFromArray:array];
    } else {
        meta1 = [array lastObject];
        meta2 = [_currentTc firstObject];
        
        if (meta1.time > meta2.time){
            //一屏数据是60条，为保证一屏显示完整，此处以60作为判断条件
            if (array.count > 60) {
                [_currentTc removeAllObjects];
                [_currentTc addObjectsFromArray:array];
            } else {
                [_currentTc insertObjects:array
                                atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, array.count)]];
            }
        } else {
            //旧数据
            //先删除
            //后插入
            meta2 = [array firstObject];
            __block NSUInteger  startIdx = 0;
            [_currentTc enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(IXMetaData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.time >= meta1.time && obj.time <= meta2.time) {
                    [_currentTc removeObject:obj];
                    startIdx = idx;
                }
            }];
            
            [_currentTc insertObjects:array atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIdx, array.count)]];
        }
    }
}

/** 处理周k数据源 */
- (void)dealWithWeekDataWith:(NSArray <IXMetaData *>*)array
{
    IXMetaData  * meta = [array lastObject];
    if (!meta || (meta.type != DAY && meta.type < Week_1)) {
        ELog(@"周k数据出错");
        return;
    }
    
    IXMetaData *meta1 = array.lastObject;
    IXMetaData *meta2 = _currentTc.lastObject;
    if (meta2) {
        if (meta1.time >= meta2.time) {
            //数据重复，丢弃处理
            return;
        }
    }
    
    IXMetaData *tmp = nil;
    NSInteger     cnt = 0;
    
    for (int i = 0; i < array.count; i++) {
        if (!tmp) {
            tmp = [(IXMetaData *)array[i] mutableCopy];
            tmp.type = Week_1;
            if (meta2.time <= tmp.time) {
                continue;
            }
            if ([NSDate isInSameWeek:tmp.time time2:meta2.time]) {
                [meta2 mergePropertyWithMeta:tmp];
                tmp = nil;
                continue;
            }
            cnt ++;
        } else {
            IXMetaData *meta = [array[i] mutableCopy];
            meta.type = Week_1;
            if ([NSDate isInSameWeek:tmp.time time2:meta.time]) {
                [tmp mergePropertyWithMeta:meta];
                cnt ++;
            } else {
                [_currentTc addObject:tmp];
                cnt = 1;
                tmp = meta;
            }
        }
    }
    
    if (tmp&&cnt>0) {
        [_currentTc addObject:tmp];
    }
}

/** 处理月k数据源 */
- (void)dealWithMonthDataWith:(NSArray <IXMetaData *>*)array
{
    IXMetaData  * meta = [array lastObject];
    if (!meta || (meta.type != DAY && meta.type < Week_1)) {
        ELog(@"月k数据出错");
        return;
    }
    
    IXMetaData *meta1 = array.lastObject;
    IXMetaData *meta2 = _currentTc.lastObject;
    if (meta2) {
        if (meta1.time >= meta2.time) {
            //数据重复，丢弃处理
            return ;
        }
    }
    
    IXMetaData  *tmp = nil;
    NSInteger   cnt = 0;
    
    for (int i = 0; i < array.count; i++) {
        if (!tmp) {
            tmp = [(IXMetaData *)array[i] mutableCopy];
            tmp.type = Month_1;
            if (meta2.time <= tmp.time) {
                continue;
            }
            if ([NSDate isInSameMonth:tmp.time time2:meta2.time]) {
                [meta2 mergePropertyWithMeta:tmp];
                tmp = nil;
                continue;
            }
            cnt ++;
        } else {
            IXMetaData *meta = [array[i] mutableCopy];
            meta.type = Month_1;
            if ([NSDate isInSameMonth:tmp.time time2:meta.time]) {
                [tmp mergePropertyWithMeta:meta];
                cnt ++;
            } else {
                [_currentTc addObject:tmp];
                cnt = 1;
                tmp = meta;
            }
        }
    }
    
    if (tmp&&cnt>0) {
        [_currentTc addObject:tmp];
    }
}

NSUInteger lastQuoteTime = 0;
/**
 跳动k线行情数据处理
 
 @param data 行情数据
 @return 是否需要刷新
 */
- (BOOL)insertNewQuoteData:(IXMetaData *)data
{
    IXMetaData  * firstMeta = [_currentTc firstObject];
    if (!_currentTc.count) {
        return NO;
    }
    //k线是否需要刷新（最新价格改变则需要刷新）
    BOOL    needRefresh = firstMeta.close != data.close;
    NSInteger    needLatestCount = 0;
    data.type = firstMeta.type;
    
    switch (firstMeta.type) {
        case MIN_1:{
            if (data.time > firstMeta.time) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = llabs(data.time/60 - firstMeta.time/60) + 2;
                data.time = (data.time / 60 + 1) * 60;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case MIN_5:{
            if (data.time > firstMeta.time) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = llabs(data.time/300 - firstMeta.time/300) + 2;
                data.time = (data.time / 300 + 1) * 300;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case MIN_15:{
            if (data.time > firstMeta.time) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = llabs(data.time/900 - firstMeta.time/900) + 2;
                data.time = (data.time / 900 + 1) * 900;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case MIN_30:{
            if (data.time > firstMeta.time) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = llabs(data.time/1800 - firstMeta.time/1800) + 2;
                data.time = (data.time / 1800 + 1) * 1800;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case HOUR_1:{
            if (data.time > firstMeta.time) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = llabs(data.time/3600 - firstMeta.time/3600) + 2;
                data.time = (data.time / 3600 + 1) * 3600;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case HOUR_4:{
            if (data.time > firstMeta.time) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = llabs(data.time/14400 - firstMeta.time/14400) + 2;
                data.time = (data.time / 14400 + 1) * 14400;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case DAY:{
            if (![NSDate isInSameDay:data.time time2:firstMeta.time]) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = 2;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case Week_1:{
            if (![NSDate isInSameWeek:data.time time2:firstMeta.time]) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = 2;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        case Month_1:{
            if (![NSDate isInSameMonth:data.time time2:firstMeta.time]) {
                [self dealNewMeta:data withOldMeta:firstMeta];
                [_currentTc insertObject:data atIndex:0];
                needLatestCount = 2;
                needRefresh = YES;
            }else{
                [firstMeta mergePropertyWithNewMeta:data];
            }
            break;
        }
        default:
            break;
    }
    
    if (needLatestCount > 0 && self.needLatestData) {
        self.needLatestData(needLatestCount);
    }

    return needRefresh;
}

- (void)dealNewMeta:(IXMetaData *)newMeta withOldMeta:(IXMetaData *)oldMeta
{
    newMeta.open = oldMeta.close;
    newMeta.low = oldMeta.close;
    newMeta.high = oldMeta.close;
}


@end
