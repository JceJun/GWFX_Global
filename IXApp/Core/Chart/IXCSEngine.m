//
//  IXCSEngine.m
//  IXApp
//
//  Created by Magee on 2017/2/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCSEngine.h"
#import "IXMetaData.h"
#import "IXCSDataSC.h"
#import "IXKLineDBMgr.h"

@interface IXCSEngine ()<IXCSDisplayVDelegate>

@property (nonatomic,assign)uint8_t     curDigit;
@property (nonatomic,assign)IXKLineType curKtype;
@property (nonatomic,strong)IXCSDataSC  * dataSC;
@property (nonatomic,assign)IXCSEngineReqStatus reqStatus;

@end

@implementation IXCSEngine

- (void)dealloc
{
    NSLog(@" -- %s --",__func__);
    _displayV.delegate = nil;
}

- (instancetype)initWithDigit:(int)digit marketID:(NSInteger)marketID
{
    self = [super init];
    if (self) {
        self.curKtype = -1;
        self.curDigit = digit;
        self.dataSC = [[IXCSDataSC alloc] init];
        self.displayV = [[IXCSDisplayV alloc] init];
        self.displayV.csType = -1;
        self.displayV.curDigit = digit;
        self.displayV.curMarketID = marketID;
        self.displayV.delegate = self;
        self.displayV.pData = self.dataSC.currentTc;
        weakself;
        self.dataSC.needLatestData = ^(NSInteger count){
            [weakSelf IXCSDisplayVNeedLatestData:count];
        };
    }
    return self;
}

/** 设置夜间模式 */
- (void)setNightMode:(BOOL)isNightMode
{
    self.displayV.isNightMode = isNightMode;
}
/** 设置红涨绿跌 */
- (void)setRedUpBlueDown:(BOOL)isRedUp
{
    self.displayV.settingRed = isRedUp;
}

- (void)showKLineWithType:(IXKLineType)type
{
    if (type == _curKtype) {
        return;
    }
    _curKtype = type;
    _displayV.csType = type;
    [_displayV redirectKline];
}

#pragma mark -
#pragma mark - k线数据入口

- (void)dynamicDataReseponse:(IXMetaData *)obj
{
    //动态行情
    BOOL needRefresh = [_dataSC insertNewQuoteData:obj];
    
    if (self.displayV.superview && self.displayV.alpha && needRefresh) {
        [_displayV.loadingV stopAnimating];
        [_displayV forceRefresh];
    }
}

- (void)klineDataResponse:(NSMutableArray *)array
{
    [array sortUsingComparator:^NSComparisonResult(NSDictionary * _Nonnull obj1, NSDictionary * _Nonnull obj2) {
        int64_t time1 = [obj1[@"nTime"] integerValue];
        int64_t time2 = [obj2[@"nTime"] integerValue];
        return time1 < time2;
    }];
    
    if (!array.count || ![self isDataTypeMatchWithCT:array]) {
        self.reqStatus = IXCSEngineReqFree;
        return;
    }
    
    NSMutableArray  * newDataArr = [@[] mutableCopy];
    for (int i = 0; i < array.count; i++)  {
        IXMetaData *meta = [[IXMetaData alloc] initWithDic:array[i]];
        [newDataArr addObject:meta];
    }
    
    [self kLineData:newDataArr needCache:YES];
    
    
    //debug操作
#ifdef DEBUG
    [self debugKLineData:array];
#endif
}

- (void)debugKLineData:(NSArray <NSDictionary *>*)dataArr
{
    if (dataArr.count > 50) {
        return;
    }
    
    NSString    * path = [NSHomeDirectory() stringByAppendingString:@"/Caches/debugKLine/"];
    BOOL    isDirect = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirect]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    path = [path stringByAppendingString:@"kLineDebug"];
    
    BOOL    error = NO;
    for (int i = 0; i < dataArr.count-1; i++) {
        IXMetaData  * meta1 = [[IXMetaData alloc] initWithDic:dataArr[i]];
        IXMetaData  * meta2 = [[IXMetaData alloc] initWithDic:dataArr[i+1]];
        NSInteger   time = [self timeSpace];
        
        if (llabs(meta1.time - meta2.time) > time) {
            error = YES;
            break;
        }
    }
    
    if (error) {
        NSMutableArray  * arr = [NSMutableArray arrayWithContentsOfFile:path];
        
        NSDateFormatter * formater = [NSDateFormatter new];
        [formater setDateFormat:@"yyyy/MM/dd HH/mm"];
        
        [arr addObject:[NSString stringWithFormat:@" --------- %@ ----------",[formater stringFromDate:[NSDate date]]]];
        [arr addObjectsFromArray:dataArr];
        [arr writeToFile:path atomically:YES];
    }
}

- (void)kLineData:(NSMutableArray *)array needCache:(BOOL)need
{
    
    IXMetaData *meta = nil;
    switch (_curKtype) {
        case IXKLineTypeWeek:{
            [_dataSC dealWithWeekDataWith:array];
            meta = array.lastObject;
            break;
        }
        case IXKLineTypeMonth:{
            [_dataSC dealWithMonthDataWith:array];
            meta = array.lastObject;
            break;
        }
        default:
            [_dataSC dealWithNormalData:array];
            meta = array.lastObject;
            break;
    }
    
    if (meta.offset + meta.count == meta.total + 1) {
        [self finishRequest];
        if (self.displayV.superview && self.displayV.alpha) {
            [_displayV forceRefresh];
        }
    }
    
    if (need) {
        //数据入库
        [IXKLineDBMgr db_saveDataWithSymbolId:self.symbolId metaArray:array];
    }
}


#pragma mark - 检测数据类型是否与当前容器类型匹配

- (BOOL)isDataTypeMatchWithCT:(NSMutableArray *)arr
{
    NSDictionary *dic = arr.lastObject;
    if ([[dic allKeys] containsObject:@"type"]) {
        switch ([dic[@"type"] integerValue]) {
            case MIN_1: {
                if (_curKtype == IXKLineTypeMin1){
                    return YES;
                }
                break;
            }
            case MIN_5: {
                if (_curKtype == IXKLineTypeMin5){
                    return YES;
                }
                break;
            }
            case MIN_15: {
                if (_curKtype == IXKLineTypeMin15){
                    return YES;
                }
                break;
            }
            case MIN_30: {
                if (_curKtype == IXKLineTypeMin30){
                    return YES;
                }
                break;
            }
            case HOUR_1:{
                if (_curKtype == IXKLineTypeMin60){
                    return YES;
                }
                break;
            }
            case HOUR_4:{
                if (_curKtype == IXKLineTypeHour4) {
                    return YES;
                }
                break;
            }
            case DAY:{
                if (_curKtype == IXKLineTypeDay ||
                    _curKtype == IXKLineTypeWeek||
                    _curKtype == IXKLineTypeMonth){
                    return YES;
                }
                break;
            }
            default:
                break;
        }
    }
    return NO;
}


#pragma mark -
#pragma mark - k线请求

- (void)IXCSDisplayVNeedLatestData:(NSInteger)count
{
    if (count <= 0 || _reqStatus == IXCSEngineReqBusy || !self.request) {
        return;
    }
    
    count = MIN(count, 180);
    K_TYPE type = [self CSTypeConvert2Ktype];
    
    self.reqStatus = IXCSEngineReqBusy;
    self.request(type,0,count);
    
    [self performSelector:@selector(requestTimeOut)
               withObject:nil
               afterDelay:k_Req_Interval];
}

- (void)IXCSDisplayVNeedMoreData
{
    if (_reqStatus == IXCSEngineReqBusy || !self.request) {
        return;
    }
    self.reqStatus = IXCSEngineReqBusy;
    
    K_TYPE type = [self CSTypeConvert2Ktype];
    
    time_t endTime = 0;
    IXMetaData *meta = _displayV.pData.lastObject;
    if (meta) {
        endTime = (time_t)(meta.time - 1);
    }
    
    if (endTime < 0 || meta.seq == 1) {
        self.reqStatus = IXCSEngineReqFree;
        return;
    }
    
    int searchTime = endTime;
    if (endTime > 0) {
        searchTime = endTime + 2;
    }
    NSArray * metaArr = [IXKLineDBMgr db_getKLineDataWithSymbolId:self.symbolId
                                                        kLineType:[self dbTypeWithCSType]
                                                      befomreTime:searchTime];
    if (metaArr) {
        DLog(@" ====== k data from data base ====== ");
        self.reqStatus = IXCSEngineReqFree;
        [self kLineData:[metaArr mutableCopy] needCache:NO];
    } else {
        DLog(@" ====== k data from request ====== ");
        [_displayV.loadingV startAnimating];
        self.request(type,endTime,(int)[self numberOfReq]);
        [self performSelector:@selector(requestTimeOut)
                   withObject:nil
                   afterDelay:k_Req_Interval];
    }
}

- (K_TYPE)dbTypeWithCSType
{
    switch (_curKtype) {
        case IXKLineTypeMin1:
            return MIN_1;
        case IXKLineTypeMin5:
            return MIN_5;
        case IXKLineTypeMin15:
            return MIN_15;
        case IXKLineTypeMin30:
            return MIN_30;
        case IXKLineTypeMin60:
            return HOUR_1;
        case IXKLineTypeHour4:
            return HOUR_4;
        case IXKLineTypeDay:
            return DAY;
        case IXKLineTypeWeek:
            return Week_1;
        case IXKLineTypeMonth:
            return Month_1;
        default:
            return UNKNOWN;
    }
}

- (NSInteger)timeSpace
{
    switch (_curKtype) {
        case IXKLineTypeMin1:
            return 60;
        case IXKLineTypeMin5:
            return 300;
        case IXKLineTypeMin15:
            return 900;
        case IXKLineTypeMin30:
            return 1800;
        case IXKLineTypeMin60:
            return 3600;
        case IXKLineTypeDay:
            return 3600*24;
        case IXKLineTypeWeek:
            return INT_MAX;
        case IXKLineTypeMonth:
            return INT_MAX;
        default:
            return UNKNOWN;
    }
}

- (K_TYPE)CSTypeConvert2Ktype
{
    switch (_curKtype) {
        case IXKLineTypeMin1:
            return MIN_1;
        case IXKLineTypeMin5:
            return MIN_5;
        case IXKLineTypeMin15:
            return MIN_15;
        case IXKLineTypeMin30:
            return MIN_30;
        case IXKLineTypeMin60:
            return HOUR_1;
        case IXKLineTypeHour4:
            return HOUR_4;
        case IXKLineTypeDay:
        case IXKLineTypeWeek:
        case IXKLineTypeMonth:
            return DAY;
        default:
            return UNKNOWN;
    }
}

- (NSInteger)numberOfReq
{
    switch (_curKtype) {
        case IXKLineTypeMin1:
        case IXKLineTypeMin5:
        case IXKLineTypeMin15:
        case IXKLineTypeMin30:
        case IXKLineTypeMin60:
        case IXKLineTypeHour4:
        case IXKLineTypeDay:
            return 180;
        case IXKLineTypeWeek:
        case IXKLineTypeMonth:
            return 1000;
        default:
            return 0;
    }
}

- (void)IXCSDisplayVDoubleClicked
{
    if (self.clicked) {
        self.clicked();
    }
}

#pragma mark - k线请求超时处理

- (void)setReqStatus:(IXCSEngineReqStatus)reqStatus
{
    _reqStatus = reqStatus;
    if (self.status) {
        self.status(reqStatus);
    }

    if (reqStatus == IXCSEngineReqFree) {
        [_displayV.loadingV stopAnimating];
    }
}

- (void)requestTimeOut
{
    [_displayV forceRefresh];
    self.reqStatus = IXCSEngineReqFree;
}

- (void)finishRequest
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(requestTimeOut)
                                               object:nil];
    self.reqStatus = IXCSEngineReqFree;
}

@end
