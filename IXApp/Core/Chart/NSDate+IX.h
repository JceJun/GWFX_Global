//
//  NSDate+IX.h
//  FXApp
//
//  Created by Seven on 2017/8/8.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (IX)

+ (BOOL)isInSameDay:(int64_t)time1 time2:(int64_t)time2;
+ (BOOL)isInSameWeek:(int64_t)time1 time2:(int64_t)time2;
+ (BOOL)isInSameMonth:(int64_t)time1 time2:(int64_t)time2;

@end
