//
//  IXTSEngine.h
//  IXApp
//
//  Created by Magee on 2016/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXTSDisplayV.h"

typedef enum {
    IXTSEngineeReqFree,
    IXTSEngineReqBusy
}IXTSEngineReqStatus;

typedef void(^tsRequestData)(time_t endTime, int32_t cnt);
typedef void(^tsRequestStatus)(IXTSEngineReqStatus status);
typedef void(^tsDoubleClicked)();

@class IXMetaData;
@interface IXTSEngine : NSObject

@property (nonatomic,strong)IXTSDisplayV *displayV;
@property (nonatomic, copy)tsRequestData request;
@property (nonatomic, copy)tsRequestStatus status;
@property (nonatomic, copy)tsDoubleClicked clicked;


- (instancetype)initWithDigit:(int)digit
               scheduleCateID:(int)schedule
                             lastPrice:(double)lastPrice;

/** 设置夜间模式 */
- (void)setNightMode:(BOOL)isNightMode;
/** 设置红涨绿跌 */
- (void)setRedUpBlueDown:(BOOL)isRedUp;

//动态行情数据
- (void)dynamicDataReseponse:(IXMetaData *)obj;
//插入行情数据请求结果
- (void)requestDataResponse:(NSMutableArray *)array;

- (void)display;

@end
