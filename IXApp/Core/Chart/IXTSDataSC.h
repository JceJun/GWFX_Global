//
//  IXTSDataSC.h
//  IXApp
//
//  Created by Magee on 2017/3/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IXTSDataSCDelegate <NSObject>

- (void)metaDataShuoldBeRequested;
- (void)displayVShouldBeRefreshedWithToltal:(int)total;
- (void)displayDotShouldActionWithTotal:(int)total price:(CGFloat)price;

@end

@class IXMetaData;
@interface IXTSDataSC : NSObject

@property (nonatomic,strong)IXMetaData *lastMeta;
@property (nonatomic,strong)NSMutableArray *min1CT;
@property (nonatomic,assign)int     scheduleCateID;
@property (nonatomic,assign)id<IXTSDataSCDelegate>delegate;

- (void)display;

/** 重新请求数据时调用 */
- (void)resetDataSuorce;

- (void)dynamicDataRsp:(IXMetaData *)meta needRefresh:(BOOL)need;

- (void)requestDataRsp:(NSMutableArray *)array;

@end
