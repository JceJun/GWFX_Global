//
//  IXTSDisplayV+draw.m
//  IXApp
//
//  Created by Magee on 2016/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTSDisplayV+draw.h"
#import "CAShapeLayer+Chart.h"
#import "CATextLayer+Chart.h"
#import "IXMetaData.h"

@implementation IXTSDisplayV (draw)

//框架
- (void)drawForm
{
    CGFloat cellH = self.curMHth/2.f;
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (self.isNightMode) {
        CGContextSetRGBStrokeColor(context, 0.137, 0.165, 0.212, 1.0);
    }else{
        CGContextSetRGBStrokeColor(context, .870, .870, .870, 1.0);
    }
    
    CGContextSetLineWidth(context, .5f);
    
    //x轴
    CGPoint xL[2];
    CGPoint xR[2];
    
    for (int i = 0; i < 2; i++) {
        if (i == 0) {
            xL[i] = CGPointMake(0, i * cellH);
            xR[i] = CGPointMake(self.curMWth, i * cellH);
        } else {
            xL[i] = CGPointMake(0, self.curSBtmY);
            xR[i] = CGPointMake(self.curMWth, self.curSBtmY);
        }
        CGPoint aPoints[2];
        aPoints[0] = xL[i];
        aPoints[1] = xR[i];

        CGContextAddLines(context, aPoints, 2);
        CGContextDrawPath(context, kCGPathStroke);
    }
    
    [self drawTime];
}

- (void)drawTime
{
    if (!self.scheduleArr || !self.scheduleArr.count) {
        return;
    }
    
    NSMutableArray  * timeArr = [@[] mutableCopy];
    NSInteger   index = 0;
    for (int i = 0; i < self.scheduleArr.count; i ++) {
        NSDictionary    * dic = self.scheduleArr[i];
        int enable = [dic[@"enable"] intValue];
        if (enable <= 0) {
            continue;
        }

        int startTime = [dic[@"startTime"] intValue];
        int endTime   = [dic[@"endTime"]   intValue];
        
        if (i == 0) {
            [timeArr addObject:@{
                                 @"time":@(startTime),
                                 @"index":@(index)
                                 }];
        }
        
        if (startTime < 0) {
            if (endTime >= 360) {
                [timeArr addObject:@{
                                     @"time":@(360),
                                     @"index":@(index + 360 - startTime)
                                     }];
            }
            if (endTime >= 720) {
                [timeArr addObject:@{
                                     @"time":@(720),
                                     @"index":@(index + 720 - startTime)
                                     }];
            }
            if (endTime >= 1080) {
                [timeArr addObject:@{
                                     @"time":@(1080),
                                     @"index":@(index + 1080 - startTime)
                                     }];
            }
        } else {
            if (endTime - startTime >= 1080) {
                int time = ((int)startTime/360 + 1) * 360;
                [timeArr addObject:@{
                                     @"time":@(time),
                                     @"index":@(index + time - startTime)
                                     }];
                time += 360;
                [timeArr addObject:@{
                                     @"time":@(time),
                                     @"index":@(index + time - startTime)
                                     }];
                time += 360;
                [timeArr addObject:@{
                                     @"time":@(time),
                                     @"index":@(index + time - startTime)
                                     }];
            }
            else if (endTime - startTime >= 720) {
                int time = ((int)startTime/360 + 1) * 360;
                [timeArr addObject:@{
                                     @"time":@(time),
                                     @"index":@(index + time - startTime)
                                     }];
                time += 360;
                [timeArr addObject:@{
                                     @"time":@(time),
                                     @"index":@(index + time - startTime)
                                     }];
            }
            else if (endTime - startTime >= 360) {
                int time = ((int)startTime/360 + 1) * 360;
                [timeArr addObject:@{
                                     @"time":@(time),
                                     @"index":@(index + time - startTime)
                                     }];
            }
        }
        

        if (i == self.scheduleArr.count - 1) {
            [timeArr addObject:@{
                                 @"time":@(endTime),
                                 @"index":@(index + endTime - startTime)
                                 }];
        }
        index += (endTime - startTime);
    }
    
    CGFloat width = self.curMWth/self.curElmtCnt;
    for (int i = 0; i < timeArr.count; i ++) {
        NSDictionary    * dic = timeArr[i];
        NSInteger   time = [dic[@"time"] integerValue];
        NSInteger   index = [dic[@"index"] integerValue];
        
        NSInteger   current = [[NSDate date] timeIntervalSinceNow];
        NSInteger   tmp = current % 86400;
        time = current - tmp + time * 60;
        
        NSString    * formater = @"HH:mm";
        NSString *testStr = [IXEntityFormatter timeIntervalToString:time formate:formater];
        
        CGRect  rect = CGRectMake(index * width, self.curSBtmY, 200, 14);
        if (i > 0) {
            CGSize  size = [testStr sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
            if (i == timeArr.count - 1) {
                rect.origin.x -= size.width;
            } else {
                rect.origin.x -= size.width/2;
            }
        }
        [testStr drawInRect:rect
             withAttributes:@{NSFontAttributeName:RO_REGU(10),
                              NSForegroundColorAttributeName:UIColorHexFromRGB(0x99abba)}];
    }
}

//主图
- (void)drawMainData
{
    if (!self.curElmtCnt) {
        return;
    }
    
    CGFloat width = self.curMWth/self.curElmtCnt;
    CGFloat mHeight = self.curMHth - 10;
    NSInteger cnt = self.LIdx - self.RIdx + 1;
//    CGPoint points[cnt+1];
    CGFloat minY = MAXFLOAT;
    UIBezierPath    * bezierPath = [UIBezierPath bezierPath];
    
    CGFloat x = 0;
    CGFloat y = 0;
    for (NSInteger i = 0; i < cnt; i++) {
        x = i * width;
        IXMetaData *meta = self.pData[self.LIdx - i];
        y = self.curMTopY + (self.maxPrice - meta.close)/(self.maxPrice - self.minPrice) * mHeight + 5;
        minY = MIN(minY, y);
        if (i == 0) {
            [bezierPath moveToPoint:CGPointMake(x, y)];
        } else {
            [bezierPath addLineToPoint:CGPointMake(x, y)];
        }
    }
    
    //折线延伸至dot的位置
    x = (cnt + 3) * width;
    if (self.dotPrice != 0) {
        y = self.curMTopY + (self.maxPrice - self.dotPrice)/(self.maxPrice - self.minPrice) * mHeight + 5;
    }
    [bezierPath addLineToPoint:CGPointMake(x, y)];
    
    if (!self.mainLay) {
        self.mainLay = [CAShapeLayer layerWithPath:bezierPath strokeColor:UIColorHexFromRGB(0x3472ff) lineWidth:.5f];
    } else {
        self.mainLay.path = bezierPath.CGPath;
    }
    
    [bezierPath addLineToPoint:CGPointMake(x, self.curMBtmY)];
    [bezierPath addLineToPoint:CGPointMake(0, self.curMBtmY)];
    [bezierPath addLineToPoint:CGPointMake(0, self.curMTopY)];
    
    CAGradientLayer * gradientL = [CAGradientLayer layer];
    gradientL = [CAGradientLayer layer];
    gradientL.startPoint = CGPointMake(0, 0);
    gradientL.endPoint = CGPointMake(0, 1);
    gradientL.cornerRadius = 5;
    gradientL.masksToBounds = YES;
    UIColor * topClr = AutoNightColor(0xdce7ff, 0x24467e);
    UIColor * btomClr = AutoNightColor(0xffffff, 0x262f3e);
    gradientL.colors = @[(__bridge id)topClr.CGColor,
                         (__bridge id)btomClr.CGColor];
    gradientL.locations = @[@0.5f];
    gradientL.frame = CGRectMake(0, minY, x, self.curMBtmY - minY);
    CALayer * shadow = [CALayer layer];
    [shadow addSublayer:gradientL];
    
    CAShapeLayer    * shapedLay = [CAShapeLayer layer];
    shapedLay.path = bezierPath.CGPath;
    [shadow setMask:shapedLay];
    
    
    if (self.shadowLay.superlayer) {
        [self.shadowLay removeFromSuperlayer];
    }
    [self.layer addSublayer:shadow];
    self.shadowLay = shadow;
    
    [self.layer addSublayer:self.mainLay];
}

//副图
- (void)drawScndData
{
    int n5 = 0;
    int n10 = 0;
    
    CGFloat width = self.curMWth/self.curElmtCnt;
    CGFloat bodyWidth = 0.f;
    
    CGFloat Interspace = 1;
    if (Interspace > width/2.f) {
        Interspace = width/2.f;
    }
    
    bodyWidth = width - Interspace;
    
    NSInteger cnt = self.LIdx - self.RIdx + 1;
    
    CGPoint ma5Points[cnt];
    CGPoint ma10Points[cnt];
    //最大价格差量化于副绘图区高
    CGFloat scale = (self.curScndTop - self.curScndBtm)/self.curSHth;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (NSInteger i = self.LIdx; i >= self.RIdx && !(i < 0); i--) {
        
        NSDictionary *dic = self.scndIdxArr[self.scndIdxArr.count -i - 1];
        
        //MACD
        CGFloat mac = [dic[kMACD_MAC] floatValue];
        if (mac > 0) {
            CGContextSetRGBStrokeColor(context,.890, .318, .357, 1.0);
        } else if (mac < 0) {
            CGContextSetRGBStrokeColor(context,.067, .722, .451, 1.0);
        } else {
            CGContextSetRGBStrokeColor(context,.5, .5, .5, 1.0);
        }
        
        CGPoint body[2];
        body[0].x = (self.LIdx - i) * width + width/2;
        body[0].y = self.curSTopY + self.curSHth/2.f;
        
        body[1].x = body[0].x;
        body[1].y = body[0].y - mac/scale;
        CGContextSetLineWidth(context, bodyWidth);
        CGContextAddLines(context, body, 2);
        CGContextDrawPath(context, kCGPathStroke);
        //2、DIF
        if (dic[kMACD_DIF]) {
            CGFloat y = 0.f;
            CGFloat vol = [dic[kMACD_DIF] floatValue];
            y = self.curSBtmY - (vol - self.curScndBtm)/scale;
            ma5Points[n5] = CGPointMake(body[0].x, y);
            n5 ++;
        }
        
        //3、DEA
        if (dic[kMACD_DEA]) {
            CGFloat y = 0.f;
            CGFloat vol = [dic[kMACD_DEA] floatValue];
            y = self.curSBtmY - (vol - self.curScndBtm)/scale;
            ma10Points[n10] = CGPointMake(body[0].x, y);
            n10 ++;
        }
    }
    
    CGContextSetRGBStrokeColor(context, .922, .678, .384, 1.0);
    CGContextSetLineWidth(context, 1.f);
    CGContextAddLines(context, ma5Points, n5);
    CGContextDrawPath(context, kCGPathStroke);
    
    CGContextSetRGBStrokeColor(context, .282, .525, .788, 1.0);
    CGContextSetLineWidth(context, 1.f);
    CGContextAddLines(context, ma10Points, n10);
    CGContextDrawPath(context, kCGPathStroke);
}

//y轴 静态价格
- (void)drawYscaleMark
{
    CGPoint point0 = CGPointMake(5, self.curMTopY);
    CGPoint point1 = CGPointMake(5, self.curMHth/2.f - 6);
    CGPoint point2 = CGPointMake(5, 2 * self.curMHth/2.f - 12);
    
    NSString *prc0 = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.maxPrice]
                                   WithDigits:self.curDigit];
    NSString *prc1 = [NSString formatterPrice:[NSString stringWithFormat:@"%f",(self.maxPrice + self.minPrice)/2]
                                   WithDigits:self.curDigit];
    NSString *prc2 = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.minPrice]
                                   WithDigits:self.curDigit];
    
    CGSize  size = [prc0 sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];

    if (!self.mainPrice0Lay) {
        self.mainPrice0Lay = [CATextLayer textLayerWithText:prc0
                                                       font:RO_REGU(10)
                                                  textColor:UIColorHexFromRGB(0x99abba)
                                            backgroundColor:[UIColor clearColor]
                                                      frame:CGRectMake(point0.x, point0.y, size.width + 5, 20)
                                              textAlignment:NSTextAlignmentLeft];
    } else {
        self.mainPrice0Lay.string = prc0;
    }
    
    if (!self.mainPrice1Lay) {
        self.mainPrice1Lay = [CATextLayer textLayerWithText:prc1
                                                       font:RO_REGU(10)
                                                  textColor:UIColorHexFromRGB(0x99abba)
                                            backgroundColor:[UIColor clearColor]
                                                      frame:CGRectMake(point1.x, point1.y, size.width + 5, 20)
                                              textAlignment:NSTextAlignmentLeft];
    } else {
        self.mainPrice1Lay.string = prc1;
    }
    
    if (!self.mainPrice2Lay) {
        self.mainPrice2Lay = [CATextLayer textLayerWithText:prc2
                                                       font:RO_REGU(10)
                                                  textColor:UIColorHexFromRGB(0x99abba)
                                            backgroundColor:[UIColor clearColor]
                                                      frame:CGRectMake(point2.x, point2.y, size.width + 5, 20)
                                              textAlignment:NSTextAlignmentLeft];
    } else {
        self.mainPrice2Lay.string = prc2;
    }
    
    [self.layer addSublayer:self.mainPrice0Lay];
    [self.layer addSublayer:self.mainPrice1Lay];
    [self.layer addSublayer:self.mainPrice2Lay];
}

//y轴副图静态
- (void)drawSYscaleMark
{
    CGPoint point0 = CGPointMake(5, self.curSTopY);
    CGPoint point1 = CGPointMake(5, self.curSBtmY - 12);
    
    NSString *prc0 = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.curScndTop] WithDigits:self.curDigit];
    NSString *prc1 = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.curScndBtm] WithDigits:self.curDigit];
    
    CGSize  size = [prc0 sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
    
    if (!self.secPrice0Lay) {
        self.secPrice0Lay = [CATextLayer textLayerWithText:prc0
                                                      font:RO_REGU(10)
                                                 textColor:UIColorHexFromRGB(0x99abba)
                                           backgroundColor:[UIColor clearColor]
                                                     frame:CGRectMake(point0.x, point0.y, size.width + 5, 20)
                                             textAlignment:NSTextAlignmentLeft];
    } else {
        self.secPrice0Lay.string = prc0;
    }
    
    if (!self.secPrice1Lay) {
        self.secPrice1Lay = [CATextLayer textLayerWithText:prc1
                                                      font:RO_REGU(10)
                                                 textColor:UIColorHexFromRGB(0x99abba)
                                           backgroundColor:[UIColor clearColor]
                                                     frame:CGRectMake(point1.x, point1.y, size.width + 5, 20)
                                             textAlignment:NSTextAlignmentLeft];
    } else {
        self.secPrice1Lay.string = prc1;
    }
    
    [self.layer addSublayer:self.secPrice0Lay];
    [self.layer addSublayer:self.secPrice1Lay];
}


//移动十字
- (void)drawCrossLine
{
    NSArray * dashArr = @[@6,@3,@2,@3];
    UIColor * color = self.isNightMode ? UIColorFromRGB(0.282, 0.525, 0.788) : UIColorFromRGB(0.298, 0.376, 0.447);
    
    if (!self.cros_verLay) {
        self.cros_verLay = [CAShapeLayer dashLayerWithP:CGPointMake(0, 0)
                                                     p2:CGPointMake(0, self.curSBtmY)
                                                dashArr:dashArr
                                              lineWidth:0.5
                                            strokeColor:color];
    }
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.cros_verLay.frame = CGRectMake(self.pointCrossLine.x, 0, 0.5, self.curMHth);
    [CATransaction commit];
    
    [self.layer addSublayer:self.cros_verLay];
}

//移动十字
- (void)drawIntersect
{
    CGPoint origin;
    CGFloat width = (CGFloat)self.curMWth/self.curElmtCnt;
    CGFloat mHeight = self.curMHth - 10;
    CGFloat tmp = self.pointCrossLine.x/width;
    CGFloat tail = tmp - (int)tmp;
    if (tail > 0.5){
        tmp ++;
    }
    
    int k = self.LIdx-(int)tmp;
    if (k < 0 || k >= self.pData.count) {
        [self.cros_horLay removeFromSuperlayer];
        [self.cros_cirLay removeFromSuperlayer];
        [self.cros_priceLay removeFromSuperlayer];
        return;
    }
    
    
    
    IXMetaData *meta = self.pData[self.LIdx-(int)tmp];
    origin.y = self.curMTopY + (self.maxPrice - meta.close)/(self.maxPrice - self.minPrice) * mHeight + 5;
    origin.x = self.pointCrossLine.x;
    
    UIBezierPath    * path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 4, 4)];
    UIColor * color = self.isNightMode ? UIColorFromRGB(0.282, 0.525, 0.788) : UIColorFromRGB(0.298, 0.376, 0.447);
    
    
    if (!self.cros_cirLay) {
        self.cros_cirLay = [CAShapeLayer layer];
        self.cros_cirLay.fillColor = color.CGColor;
        self.cros_cirLay.path = path.CGPath;
    }
    
    //x
    NSArray * dashArr = @[@6,@3,@2,@3];
    if (!self.cros_horLay) {
        self.cros_horLay = [CAShapeLayer dashLayerWithP:CGPointMake(0, 0)
                                                     p2:CGPointMake(self.frame.size.width, 0)
                                                dashArr:dashArr
                                              lineWidth:0.5
                                            strokeColor:color];
    }
    
    //画移动价格
    CGFloat curPrc = self.lastPrice + (self.curMMidY - origin.y) / self.curMainTop;
    if (self.curMMidY == origin.y) {
        curPrc = (self.minPrice + self.maxPrice)/2;
    }
    
    NSString *prc = [NSString formatterPrice:[NSString stringWithFormat:@"%f",curPrc] WithDigits:meta.digit];
    CGSize  size = [prc sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
    CGFloat x = self.pointCrossLine.x > size.width + 10 ? 5 : self.frame.size.width - size.width - 10;
    
    if (!self.cros_priceLay) {
        self.cros_priceLay = [CATextLayer textLayerWithText:prc
                                                       font:RO_REGU(10)
                                                  textColor:self.crossCurveTextColor
                                            backgroundColor:self.crossCurveBgColor
                                                      frame:CGRectMake(0, 0, size.width + 5, 14)
                                              textAlignment:NSTextAlignmentCenter];
    }
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.cros_cirLay.frame = CGRectMake(origin.x-2, origin.y-2, 4, 4);
    self.cros_horLay.frame = CGRectMake(0, origin.y, self.frame.size.width, 0.5);
    self.cros_priceLay.frame = CGRectMake(x, origin.y - 7, size.width + 5, 14);
    self.cros_priceLay.string = prc;
    [CATransaction commit];
    
    [self.layer addSublayer:self.cros_horLay];
    [self.layer addSublayer:self.cros_cirLay];
    [self.layer addSublayer:self.cros_priceLay];
}

//x轴移动时间
- (void)drawYMoveScale
{
    int idx = (self.pointCrossLine.x / self.self.curMWth) * self.curElmtCnt;
    
    if (idx >= 0 && idx <= self.pData.count) {
        IXMetaData *meta = nil;
        if (idx == 0) {
            meta = [self.pData lastObject];
        } else {
            meta = self.pData[self.pData.count - idx];
        }
        
        NSString *testStr = [IXEntityFormatter timeIntervalToString:meta.time formate:@"MM/dd HH:mm"];
        CGSize  size = [testStr sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
        
        if (!self.cros_timeLay) {
            self.cros_timeLay = [CATextLayer textLayerWithText:testStr
                                                          font:RO_REGU(10)
                                                     textColor:self.crossCurveTextColor
                                               backgroundColor:self.crossCurveBgColor
                                                         frame:CGRectMake(0, 0, size.width + 5, size.height)
                                                 textAlignment:NSTextAlignmentCenter];
        } else {
            self.cros_timeLay.string = testStr;
        }
        
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        self.cros_timeLay.frame = CGRectMake(MAX(self.pointCrossLine.x - 28, 2), self.curSBtmY, size.width + 5, 14);
        [CATransaction commit];
        
        [self.layer addSublayer:self.cros_timeLay];
    } else {
        if (self.cros_timeLay.superlayer) {
            [self.cros_timeLay removeFromSuperlayer];
        }
    }
}


- (void)drawCurrentPriceLine
{
    CGFloat mHeight = self.curMHth - 10;
    CGFloat y = self.curMTopY + (self.maxPrice - self.dotPrice)/(self.maxPrice - self.minPrice) * mHeight + 5;
    if (y <= 0) {
        return;
    }
    
    //最新价格虚线
    if (!self.curPriceLine) {
        UIColor * color = self.isNightMode ? [UIColor whiteColor] : UIColorFromRGB(0.600, 0.670, 0.729);
        self.curPriceLine = [CAShapeLayer dashLayerWithP: CGPointMake(0, 0)
                                                      p2:CGPointMake(self.frame.size.width, 0)
                                                 dashArr:@[@6,@3,@2,@3]
                                               lineWidth:0.5f
                                             strokeColor:color];
    }
    self.curPriceLine.frame = CGRectMake(0, y, self.frame.size.width, 0.5);
    [self.layer addSublayer:self.curPriceLine];
    
    
    //price
    NSString    * str = [NSString formatterPrice:[NSString stringWithFormat:@"%f",self.dotPrice]
                                      WithDigits:self.curDigit];
    CGSize  size = [str sizeWithAttributes:@{NSFontAttributeName:RO_REGU(10)}];
    CGRect  frame = CGRectMake(self.frame.size.width - size.width - 10, y - 7.f, size.width + 5, 14);
    
    if (!self.curPriceLay) {
        self.curPriceLay = [CATextLayer textLayerWithText:str
                                                     font:RO_REGU(10)
                                                textColor:self.crossCurveTextColor
                                          backgroundColor:self.crossCurveBgColor
                                                    frame:CGRectMake(frame.origin.x, 0, size.width + 5, 14)
                                            textAlignment:NSTextAlignmentCenter];
    }
    self.curPriceLay.frame = frame;
    self.curPriceLay.string = str;
    [self.layer addSublayer:self.curPriceLay];
}


//右侧主幅度
- (void)drawMainExtent
{
    CGPoint point0 = CGPointMake(self.curMWth + 5, self.curMTopY);
    CGPoint point1 = CGPointMake(self.curMWth + 5, self.curMHth/2.f - 6);
    CGPoint point2 = CGPointMake(self.curMWth + 5, 2 * self.curMHth/2.f - 12);
    
//    CGFloat kk = fabs(self.curBDPrice - self.lastPrice) / self.lastPrice * 100;
    CGFloat kk = fabs(self.dotPrice - self.lastPrice) / self.lastPrice * 100;

    NSString *prc0 = [NSString stringWithFormat:@"+%.2f%%",kk];
    [prc0 drawInRect:CGRectMake(point0.x, point0.y, 100, 20) withAttributes:@{NSFontAttributeName:RO_REGU(10),NSForegroundColorAttributeName:MarketRedPriceColor}];

    NSString *prc1 = [NSString stringWithFormat:@"%.2f%%",0.f];
    [prc1 drawInRect:CGRectMake(point1.x, point1.y, 100, 20) withAttributes:@{NSFontAttributeName:RO_REGU(10),NSForegroundColorAttributeName:MarketGrayPriceColor}];

    NSString *prc2 = [NSString stringWithFormat:@"-%.2f%%",kk];
    [prc2 drawInRect:CGRectMake(point2.x, point2.y, 100, 20) withAttributes:@{NSFontAttributeName:RO_REGU(10),NSForegroundColorAttributeName:MarketGreenPriceColor}];
}

- (void)cleanSublayers
{
    if (self.cros_verLay.superlayer) {
        [self.cros_verLay removeFromSuperlayer];
    }
    if (self.cros_horLay.superlayer) {
        [self.cros_horLay removeFromSuperlayer];
    }
    if (self.cros_cirLay.superlayer) {
        [self.cros_cirLay removeFromSuperlayer];
    }
    if (self.cros_timeLay.superlayer) {
        [self.cros_timeLay removeFromSuperlayer];
    }
    if (self.cros_priceLay.superlayer) {
        [self.cros_priceLay removeFromSuperlayer];
    }
}
@end

