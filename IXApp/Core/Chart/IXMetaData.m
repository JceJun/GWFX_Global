//
//  IXMetaData.m
//  IXApp
//
//  Created by Magee on 2017/2/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXMetaData.h"
#import "IXQuoteDataCache.h"

@implementation IXMetaData

- (id)mutableCopyWithZone:(NSZone *)zone
{
    IXMetaData *meta = [[[self class] allocWithZone:zone] init];
    meta.offset = _offset;
    meta.count  = _count;
    meta.total  = _total;
    
    meta.seq    = _seq;
    meta.id_p   = _id_p;
    meta.type   = _type;
    
    meta.high   = _high;
    meta.close  = _close;
    meta.open   = _open;
    meta.low    = _low;
    meta.amt    = _amt;
    meta.vol    = _vol;
    meta.time   = _time;
    meta.digit  = _digit;
    
    return meta;
}

- (instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        self.offset = [dic[@"offset"] intValue];
        self.count  = [dic[@"count"] intValue];
        self.total  = [dic[@"total"] intValue];
        
        self.seq    = [dic[@"seq"] longValue];
        self.id_p   = [dic[@"id_p"] longValue];
        self.type   = [dic[@"type"] intValue];
        self.digit  = [dic[@"digits"] intValue];
        
        self.high   = [self getRealPriceWithPrice:[dic[@"nHigh"] intValue] Digit:self.digit];
        self.low    = [self getRealPriceWithPrice:[dic[@"nLow"] intValue] Digit:self.digit];
        self.close  = [self getRealPriceWithPrice:[dic[@"nClose"] intValue] Digit:self.digit];
        self.open   = [self getRealPriceWithPrice:[dic[@"nOpen"] intValue] Digit:self.digit];
        
        self.time   = [dic[@"nTime"] intValue];
        self.vol    = [dic[@"nVolume"] longValue];
        self.amt    = [dic[@"nAmount"] longValue];
    }
    return self;
}

- (CGFloat)getRealPriceWithPrice:(unsigned int)price Digit:(uint8_t)digit
{
    CGFloat formatterPrice = pow(10, - digit) * price;
    NSString *result = [NSString stringWithFormat:@"%lf",formatterPrice];
    NSUInteger location = [result rangeOfString:@"."].location;
    if (location != NSNotFound) {
        NSInteger length = location + digit + 1;
        if ([result length] >= length) {
            result = [result substringToIndex:length];
        }
    }
    return [result doubleValue];
}

- (void)mergePropertyWithMeta:(IXMetaData *)meta
{
    _seq  = meta.seq;
    
    _id_p = meta.id_p;
    _type = meta.type;
    
    _open = meta.open;
    
    _high = meta.high > _high ? meta.high : _high;
    _low  = meta.low  < _low  ? meta.low  : _low;
    _amt  += meta.amt;
    _vol  += meta.vol;
    _time = meta.time;
    _digit = meta.digit;
}

- (void)mergePropertyWithNewMeta:(IXMetaData *)meta
{
    _seq  = meta.seq;
    
    _id_p = meta.id_p;
    //此数据为服务器下发的行情数据，self为需要绘图的k线数据，两者type不同，为保证原数据type，此处不予赋值
    //    _type = meta.type;
    
    _close = meta.close;
    
    _high = MAX(meta.close, _high);
    _low = MIN(meta.low, _low);
    _amt  += meta.amt;
    _vol  += meta.vol;
}

@end
