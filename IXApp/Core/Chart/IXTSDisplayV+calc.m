//
//  IXTSDisplayV+calc.m
//  IXApp
//
//  Created by Magee on 2017/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXTSDisplayV+calc.h"
#import "IXMetaData.h"

@implementation IXTSDisplayV (calc)

- (void)scndIndex
{
    if (self.scndIdxArr.count == self.pData.count) {
        return;
    }
    [self.scndIdxArr removeAllObjects];
    
    NSMutableArray *ema12arr = [[NSMutableArray alloc] init];
    NSMutableArray *ema26arr = [[NSMutableArray alloc] init];
    NSMutableArray *deaArr   = [[NSMutableArray alloc] init];
    
    int cnt = self.pData.count;
    
    for (int i = cnt - 1; i >= 0; i--) {
        CGFloat ema12,ema12Old;
        CGFloat ema26,ema26Old;
        CGFloat dea,  deaOld;
        
        IXMetaData *meta = [self.pData objectAtIndex:i];
        if (i == cnt - 1) {
            ema12 = meta.close;
            ema26 = meta.close;
            dea = 0;
        } else {
            ema12Old = [[ema12arr objectAtIndex:cnt - i - 2] floatValue];
            ema26Old = [[ema26arr objectAtIndex:cnt - i - 2] floatValue];
            deaOld   = [[deaArr   objectAtIndex:cnt - i - 2] floatValue];
            ema12 = (2 * meta.close + ema12Old * (cEMA_12 - 1))/(cEMA_12 + 1);
            ema26 = (2 * meta.close + ema26Old * (cEMA_26 - 1))/(cEMA_26 + 1);
            dea   = (2 * (ema12 - ema26) + deaOld * (cEMA_9 - 1))/(cEMA_9 + 1);
        }
        
        [ema12arr addObject:[NSNumber numberWithFloat:ema12]];
        [ema26arr addObject:[NSNumber numberWithFloat:ema26]];
        [deaArr   addObject:[NSNumber numberWithFloat:dea]];
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i == cnt - 1) {
            [dic setValue:@(0) forKey:kMACD_DIF];
            [dic setValue:@(0) forKey:kMACD_DEA];
            [dic setValue:@(0) forKey:kMACD_MAC];
        } else {
            [dic setValue:@(ema12 - ema26)               forKey:kMACD_DIF];
            [dic setValue:@(dea)                         forKey:kMACD_DEA];
            [dic setValue:@(((ema12 - ema26) - dea) * 2) forKey:kMACD_MAC];
        }
        [self.scndIdxArr addObject:dic];
    }
}

@end
