//
//  IXTSDisplayV+gesture.h
//  IXApp
//
//  Created by Magee on 2016/12/20.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTSDisplayV.h"

@interface IXTSDisplayV (gesture)

- (void)registGestures;     //注册手势
- (void)resignGestures;     //注销手势

@end
