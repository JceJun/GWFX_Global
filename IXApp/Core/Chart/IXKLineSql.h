//
//  IXKLineSql.h
//  FXApp
//
//  Created by Seven on 2017/8/7.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXMetaData.h"

/**
 k线相关sql
 */
@interface IXKLineSql : NSObject

// -------------------------------------------------------------------------------------------------
// ----------------------------------------- create table ------------------------------------------
// -------------------------------------------------------------------------------------------------

/**
 create table

 @param table table name
 @return create table sql
 */
+ (NSString *)sql_createTable:(NSString *)table;

// -------------------------------------------------------------------------------------------------
// -------------------------------------------- select ---------------------------------------------
// -------------------------------------------------------------------------------------------------

/**
 获取时间为time的记录

 @param table 表名
 @param id_p 产品id
 @param time 行情时间
 @return select sql
 */
+ (NSString *)sql_selectMetaWithTable:(NSString *)table
                             symbolId:(int64_t)id_p
                                 time:(int64_t)time;


/**
 获取preTime == time的数据

 @param table 表名
 @param id_p 产品id
 @param time 行情时间
 @return select sql
 */
+ (NSString *)sql_selectMateWithTable:(NSString *)table
                                       symbolId:(int64_t)id_p
                                           preTime:(int64_t)time;


/**
 获取时间小于time的最新数据
 若time为0，则取当前时间作为基准
 
 @param table 表明
 @param id_p 产品id
 @param time 时间
 @return sql
 */
+ (NSString *)sql_selectMetaWithTable:(NSString *)table
                             symbolId:(int64_t)id_p
                           beforeTime:(int64_t)time;

/**
 获取时间大于time的最老数据

 @param table 表名
 @param id_p 产品id
 @param time 数据时间
 @return select sql
 */
+ (NSString *)sql_selectMetaWithTable:(NSString *)table
                             symbolId:(int64_t)id_p
                            afterTime:(int64_t)time;

// -------------------------------------------------------------------------------------------------
// -------------------------------------------- delete ---------------------------------------------
// -------------------------------------------------------------------------------------------------

/**
 批量删除某段时间之间的数据

 @param table 表名
 @param id_p 产品id
 @param sTime start time开始时间（GMT0 - 单位：s）
 @param eTime end time 结束时间（GMT0 - 单位：s）
 @return delete sql
 */
+ (NSString *)sql_deleteTable:(NSString *)table
                     symbolId:(int64_t)id_p
                    startTime:(int64_t)sTime
                      endTime:(int64_t)eTime;


/**
 批量删除time之前的数据

 @param table 表名
 @param time GMT0时间（单位：s）
 @return delete sql
 */
+ (NSString *)sql_deleteTable:(NSString *)table
                   beforeTime:(int64_t)time;


// -------------------------------------------------------------------------------------------------
// -------------------------------------------- update ---------------------------------------------
// -------------------------------------------------------------------------------------------------

/**
 插入更新sql
 
 @param table 表名
 @return sql
 */
+ (NSString *)sql_insertTable:(NSString *)table;



/**
 更新某一数据的preTime字段

 @param table 表名
 @param id_p 产品id
 @param time 行情时间
 @param pretime 前一条数据时间
 @return sql
 */
+ (NSString *)sql_updatePreTimeWithTable:(NSString *)table
                                 symbold:(int64_t)id_p
                                metaTime:(int64_t)time
                                 preTime:(int64_t)pretime;

@end
