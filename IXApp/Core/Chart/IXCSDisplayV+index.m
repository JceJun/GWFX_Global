//
//  IXCSDisplayV+index.m
//  KLine
//
//  Created by Magee on 16/12/1.
//  Copyright © 2016年 wsz. All rights reserved.
//

#import "IXCSDisplayV+index.h"
#import "IXMetaData.h"

@implementation IXCSDisplayV (index)

#pragma mark -
#pragma mark - 主图指标

#pragma mark ----------------------------------MA----------------------------------
- (void)load_M_MA
{
    int cnt = self.pData.count;
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i + cMA_5 <= cnt) {
            CGFloat price = 0.f;
            for(NSInteger j = i; j < i + cMA_5; j++) {
                IXMetaData *meta = self.pData[j];
                price = price + meta.close;
            }
            price = price / cMA_5;
            [dic setValue:[NSNumber numberWithFloat:price] forKey:kMA_5];
        } else {
            [dic setValue:[NSNull null] forKey:kMA_5];
        }
        
        if (i + cMA_10 <= cnt) {
            CGFloat price = 0.f;
            for (NSInteger j = i; j < i + cMA_10; j++) {
                IXMetaData *meta = self.pData[j];
                price = price + meta.close;
            }
            price = price / cMA_10 ;
            [dic setValue:[NSNumber numberWithFloat:price] forKey:kMA_10];
        } else {
            [dic setValue:[NSNull null] forKey:kMA_10];
        }
        
        if (i + cMA_20 <= cnt) {
            CGFloat price = 0.f;
            for (NSInteger j = i; j < i + cMA_20; j++) {
                IXMetaData *meta = self.pData[j];
                price = price + meta.close;
            }
            price = price / cMA_20;
            [dic setValue:[NSNumber numberWithFloat:price] forKey:kMA_20];
        } else {
            [dic setValue:[NSNull null] forKey:kMA_20];
        }
        
        if (i + cMA_60 <= cnt) {
            CGFloat price = 0.f;
            for (NSInteger j = i; j < i + cMA_60; j++) {
                IXMetaData *meta = self.pData[j];
                price = price + meta.close;
            }
            price = price / cMA_60;
            [dic setValue:[NSNumber numberWithFloat:price] forKey:kMA_60];
        } else {
            [dic setValue:[NSNull null] forKey:kMA_60];
        }
        [self.mainIdxArr addObject:dic];
    }
}

#pragma mark ----------------------------------BBI----------------------------------

- (void)load_M_BBI
{
    int cnt = self.pData.count;
    for (int i = 0; i < cnt; i++) {
        CGFloat MA3 = 0.f;
        if (i + cBBI_3 <= cnt) {
            for(NSInteger j = i; j < i + cBBI_3; j++) {
                IXMetaData *meta = self.pData[j];
                MA3 = MA3 + meta.close;
            }
            MA3 = MA3 / cBBI_3;
        }
        CGFloat MA6 = 0.f;
        if (i + cBBI_6 <= cnt) {
            for (NSInteger j = i; j < i + cBBI_6; j++) {
                IXMetaData *meta = self.pData[j];
                MA6 = MA6 + meta.close;
            }
            MA6 = MA6 / cBBI_6;
        }
        CGFloat MA12 = 0.f;
        if (i + cBBI_12 <= cnt) {
            for (NSInteger j = i; j < i + cBBI_12; j++) {
                IXMetaData *meta = self.pData[j];
                MA12 = MA12 + meta.close;
            }
            MA12 = MA12 / cBBI_12;
        }
        CGFloat MA24 = 0.f;
        if (i + cBBI_24 <= cnt) {
            for (NSInteger j = i; j < i + cBBI_24; j++) {
                IXMetaData *meta = self.pData[j];
                MA24 = MA24 + meta.close;
            }
            MA24 = MA24 / cBBI_24;
        }
        
        if (MA3 > 0 && MA6 > 0 && MA12 > 0 && MA24 > 0) {
            CGFloat bbi = (MA3 + MA6 + MA12 + MA24)/4.f;
            [self.mainIdxArr addObject:@{kBBI_BBI:@(bbi)}];
        } else {
            [self.mainIdxArr addObject:@{kBBI_BBI:[NSNull null]}];
        }
    }
}

#pragma mark ---------------------------------BOLL---------------------------------

- (void)load_M_BOLL
{
    int cnt = self.pData.count;
    for (int i = 0; i < cnt; i++) {
        CGFloat mid = 0.f;
        if (i + cBOLL_26 <= cnt) {
            for(NSInteger j = i; j < i + cBOLL_26; j++) {
                IXMetaData *meta = self.pData[j];
                mid = mid + meta.close;
            }
            mid = mid / cBOLL_26;
        }
        CGFloat sum = 0.f;
        CGFloat upp = 0.f;
        CGFloat low = 0.f;
        if (i + cBOLL_26 <= cnt) {
            for (NSInteger j = i; j < i + cBOLL_26; j++) {
                IXMetaData *meta = self.pData[j];
                sum += powf((meta.close - mid), 2);
            }
            if (mid > 0) {
                upp = mid + cBOLL_2 * sqrt(sum / cBOLL_26);
                low = mid - cBOLL_2 * sqrt(sum / cBOLL_26);
            }
        }
        
        if (mid > 0) {
            [self.mainIdxArr addObject:@{kBOLL_UP:@(upp),
                                         kBOLL_MA:@(mid),
                                         kBOLL_DN:@(low)}];
        } else {
            [self.mainIdxArr addObject:@{kBOLL_UP:[NSNull null],
                                         kBOLL_MA:[NSNull null],
                                         kBOLL_DN:[NSNull null]}];
            
        }
    }
}

#pragma mark ---------------------------------MIKE---------------------------------

- (void)load_M_MIKE
{
    int cnt = self.pData.count;
    for (int i = 0; i < cnt; i++) {
        IXMetaData *meta = self.pData[i];
        CGFloat typ = (meta.high + meta.low + meta.close) / 3.f;
        
        CGFloat hh = 0.f;
        CGFloat ll = CGFLOAT_MAX;
        if (i + cMIKE_12 <= cnt) {
            for(NSInteger j = i; j < i + cMIKE_12; j++) {
                IXMetaData *meta = self.pData[j];
                hh = meta.high > hh ? meta.high : hh;
                ll = meta.low  < ll ? meta.low  : ll;
            }
            CGFloat wr = 2 * typ - ll;
            CGFloat mr = typ + hh - ll;
            CGFloat sr = 2 * hh - ll;
            CGFloat ws = 2 * typ - hh;
            CGFloat ms = typ - hh + ll;
            CGFloat ss = 2 * ll - hh;
            [self.mainIdxArr addObject:@{kMIKE_WR:@(wr),
                                         kMIKE_MR:@(mr),
                                         kMIKE_SR:@(sr),
                                         kMIKE_WS:@(ws),
                                         kMIKE_MS:@(ms),
                                         kMIKE_SS:@(ss)}];
        } else {
            [self.mainIdxArr addObject:@{kMIKE_WR:[NSNull null],
                                         kMIKE_MR:[NSNull null],
                                         kMIKE_SR:[NSNull null],
                                         kMIKE_WS:[NSNull null],
                                         kMIKE_MS:[NSNull null],
                                         kMIKE_SS:[NSNull null]}];
        }
    }
}

#pragma mark ----------------------------------PBX---------------------------------

- (void)load_M_PBX
{
    int cnt = self.pData.count;
    
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i + cPBX_4 * 4 - 1 < cnt) {
            CGFloat MA16 = 0.f;
            for(NSInteger j = i; j <= i + cPBX_4 * 4 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA16 = MA16 + meta.close;
            }
            MA16 = MA16 / (cPBX_4 * 4);
            
            CGFloat MA8 = 0.f;
            for (NSInteger j = i; j <= i + cPBX_4 * 2 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA8 = MA8 + meta.close;
            }
            MA8 = MA8 / (cPBX_4 * 2);
            
            CGFloat EMA4 = 0.f;
            NSMutableArray *ema4arr = [[NSMutableArray alloc] init];
            for (NSInteger j = i + cPBX_4 * 1 - 1; j >= i; j--) {
                CGFloat EMA4Old = 0.f;
                IXMetaData *meta = [self.pData objectAtIndex:j];
                if (j == i + cPBX_4 * 1 - 1) {
                    EMA4 = meta.close;
                } else {
                    EMA4Old = [[ema4arr objectAtIndex:i + cPBX_4 * 1 - j - 2] floatValue];
                    EMA4 = (2 * meta.close + EMA4Old * (cPBX_4 - 1))/(cPBX_4 + 1);
                }
                [ema4arr addObject:[NSNumber numberWithFloat:EMA4]];
            }
            CGFloat pbx1 = (EMA4 + MA8 + MA16) / 3.f;
            [dic setValue:@(pbx1) forKey:kPBX_1];
        } else {
            [dic setValue:[NSNull null] forKey:kPBX_1];
        }
        
        if (i + cPBX_6 * 4 - 1 < cnt) {
            CGFloat MA24 = 0.f;
            for(NSInteger j = i; j <= i + cPBX_6 * 4 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA24 = MA24 + meta.close;
            }
            MA24 = MA24 / (cPBX_6 * 4);
            
            CGFloat MA12 = 0.f;
            for (NSInteger j = i; j <= i + cPBX_6 * 2 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA12 = MA12 + meta.close;
            }
            MA12 = MA12 / (cPBX_6 * 2);
            
            CGFloat EMA6 = 0.f;
            NSMutableArray *ema6arr = [[NSMutableArray alloc] init];
            for (NSInteger j = i + cPBX_6 * 1 - 1; j >= i; j--) {
                CGFloat EMA6Old = 0.f;
                IXMetaData *meta = [self.pData objectAtIndex:j];
                if (j == i + cPBX_6 * 1 - 1) {
                    EMA6 = meta.close;
                } else {
                    EMA6Old = [[ema6arr objectAtIndex:i + cPBX_6 * 1 - j - 2] floatValue];
                    EMA6 = (2 * meta.close + EMA6Old * (cPBX_6 - 1))/(cPBX_6 + 1);
                }
                [ema6arr addObject:[NSNumber numberWithFloat:EMA6]];
            }
            CGFloat pbx2 = (EMA6 + MA12 + MA24) / 3.f;
            [dic setValue:@(pbx2) forKey:kPBX_2];
        } else {
            [dic setValue:[NSNull null] forKey:kPBX_2];
        }
        
        if (i + cPBX_9 * 4 - 1 < cnt) {
            CGFloat MA36 = 0.f;
            for(NSInteger j = i; j <= i + cPBX_9 * 4 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA36 = MA36 + meta.close;
            }
            MA36 = MA36 / (cPBX_9 * 4);
            
            CGFloat MA18 = 0.f;
            for (NSInteger j = i; j <= i + cPBX_9 * 2 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA18 = MA18 + meta.close;
            }
            MA18 = MA18 / (cPBX_9 * 2);
            
            CGFloat EMA9 = 0.f;
            NSMutableArray *ema9arr = [[NSMutableArray alloc] init];
            for (NSInteger j = i + cPBX_9 * 1 - 1; j >= i; j--) {
                CGFloat EMA9Old = 0.f;
                IXMetaData *meta = [self.pData objectAtIndex:j];
                if (j == i + cPBX_9 * 1 - 1) {
                    EMA9 = meta.close;
                } else {
                    EMA9Old = [[ema9arr objectAtIndex:i + cPBX_9 * 1 - j - 2] floatValue];
                    EMA9 = (2 * meta.close + EMA9Old * (cPBX_9 - 1))/(cPBX_9 + 1);
                }
                [ema9arr addObject:[NSNumber numberWithFloat:EMA9]];
            }
            CGFloat pbx3 = (EMA9 + MA18 + MA36) / 3.f;
            [dic setValue:@(pbx3) forKey:kPBX_3];
        } else {
            [dic setValue:[NSNull null] forKey:kPBX_3];
        }
        
        if (i + cPBX_13 * 4 - 1 < cnt) {
            CGFloat MA52 = 0.f;
            for(NSInteger j = i; j <= i + cPBX_13 * 4 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA52 = MA52 + meta.close;
            }
            MA52 = MA52 / (cPBX_13 * 4);
            
            CGFloat MA26 = 0.f;
            for (NSInteger j = i; j <= i + cPBX_13 * 2 - 1; j++) {
                IXMetaData *meta = self.pData[j];
                MA26 = MA26 + meta.close;
            }
            MA26 = MA26 / (cPBX_13 * 2);
            
            CGFloat EMA13 = 0.f;
            NSMutableArray *ema13arr = [[NSMutableArray alloc] init];
            for (NSInteger j = i + cPBX_13 * 1 - 1; j >= i; j--) {
                CGFloat EMA13Old = 0.f;
                IXMetaData *meta = [self.pData objectAtIndex:j];
                if (j == i + cPBX_13 * 1 - 1) {
                    EMA13 = meta.close;
                } else {
                    EMA13Old = [[ema13arr objectAtIndex:i + cPBX_13 * 1 - j - 2] floatValue];
                    EMA13 = (2 * meta.close + EMA13Old * (cPBX_13 - 1))/(cPBX_13 + 1);
                }
                [ema13arr addObject:[NSNumber numberWithFloat:EMA13]];
            }
            CGFloat pbx4 = (EMA13 + MA26 + MA52) / 3.f;
            [dic setValue:@(pbx4) forKey:kPBX_4];
        } else {
            [dic setValue:[NSNull null] forKey:kPBX_4];
        }
        [self.mainIdxArr addObject:dic];
    }
}

#pragma mark -
#pragma mark - 副图指标

#pragma mark ---------------------------------MACD--------------------------------
- (void)load_S_MACD
{
    NSMutableArray *ema12arr = [[NSMutableArray alloc] init];
    NSMutableArray *ema26arr = [[NSMutableArray alloc] init];
    NSMutableArray *deaArr   = [[NSMutableArray alloc] init];
    
    int cnt = self.pData.count;
    
    for (int i = cnt - 1; i >= 0; i--) {
        CGFloat ema12,ema12Old;
        CGFloat ema26,ema26Old;
        CGFloat dea,  deaOld;
        
        IXMetaData *meta = [self.pData objectAtIndex:i];
        if (i == cnt - 1) {
            ema12 = meta.close;
            ema26 = meta.close;
            dea = 0;
        } else {
            ema12Old = [[ema12arr objectAtIndex:cnt - i - 2] floatValue];
            ema26Old = [[ema26arr objectAtIndex:cnt - i - 2] floatValue];
            deaOld   = [[deaArr   objectAtIndex:cnt - i - 2] floatValue];
            ema12 = (2 * meta.close + ema12Old * (cEMA_12 - 1))/(cEMA_12 + 1);
            ema26 = (2 * meta.close + ema26Old * (cEMA_26 - 1))/(cEMA_26 + 1);
            dea   = (2 * (ema12 - ema26) + deaOld * (cEMA_9 - 1))/(cEMA_9 + 1);
        }
        
        [ema12arr addObject:[NSNumber numberWithFloat:ema12]];
        [ema26arr addObject:[NSNumber numberWithFloat:ema26]];
        [deaArr   addObject:[NSNumber numberWithFloat:dea]];
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i == cnt - 1) {
            [dic setValue:@(0) forKey:kMACD_DIF];
            [dic setValue:@(0) forKey:kMACD_DEA];
            [dic setValue:@(0) forKey:kMACD_MAC];
        } else {
            [dic setValue:@(ema12 - ema26)               forKey:kMACD_DIF];
            [dic setValue:@(dea)                         forKey:kMACD_DEA];
            [dic setValue:@(((ema12 - ema26) - dea) * 2) forKey:kMACD_MAC];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------ARBR--------------------------------

- (void)load_S_ARBR
{
    
    int cnt = self.pData.count;
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i + cARBR_26 <= cnt) {
            CGFloat h_o = 0.f;    CGFloat o_l = 0.f;
            CGFloat h_p = 0.f;    CGFloat p_l = 0.f;
            for(NSInteger j = i; j < i + cARBR_26; j++) {
                IXMetaData *meta = self.pData[j];
                h_o += (meta.high - meta.open);
                o_l += (meta.open - meta.low);
                
                if (j+1<cnt) {
                    IXMetaData *meta2 = self.pData[j+1];
                    h_p += (meta.high - meta2.close);
                    p_l += (meta2.close - meta.low);
                }else{
                    [dic setValue:[NSNull null] forKey:kARBR_BR];
                }
            }
            CGFloat ar = h_o / o_l * 100;
            CGFloat br = h_p / p_l * 100;
            
            [dic setValue:@(ar) forKey:kARBR_AR];
            [dic setValue:@(br) forKey:kARBR_BR];
        } else {
            [dic setValue:[NSNull null] forKey:kARBR_AR];
            [dic setValue:[NSNull null] forKey:kARBR_BR];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------ATR---------------------------------

- (void)load_S_ATR
{
    int cnt = self.pData.count;
    NSMutableArray *trMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i < cnt; i++) {
        CGFloat tr = 0;
        IXMetaData *meta0 = self.pData[i];
        if (i + 1 < cnt) {
            IXMetaData *meta1 = self.pData[i + 1];
            tr = fmax(fmax(meta0.high - meta0.low, fabs(meta1.close - meta0.high)), fabs(meta1.close - meta0.low));
        }else{
            tr = fmax(fmax(meta0.high - meta0.low, fabs(meta0.close - meta0.high)), fabs(meta0.close - meta0.low));
        }
        [trMutableArray addObject:[NSNumber numberWithFloat:tr]];
    }
    NSMutableArray *atrMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i + cATR_14 < trMutableArray.count; i++) {
        CGFloat atrSum = 0;
        for (NSInteger j = i; j< i + cATR_14; j++) {
            atrSum += [trMutableArray[j] floatValue];
        }
        [atrMutableArray addObject:[NSNumber numberWithFloat:atrSum/cATR_14]];
    }
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i < trMutableArray.count) {
            [dic setValue:trMutableArray[i] forKey:kATR_TR];
        } else {
            [dic setValue:[NSNull null] forKey:kATR_TR];
        }
        
        if (i < atrMutableArray.count) {
            [dic setValue:atrMutableArray[i] forKey:kATR_ATR];
        } else {
            [dic setValue:[NSNull null] forKey:kATR_ATR];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------BIAS--------------------------------

- (void)load_S_BIAS
{
    int cnt = self.pData.count;
    
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        IXMetaData *meta = self.pData[i];
        CGFloat MA6 = 0.f;
        if (i + cBIAS_6 <= cnt) {
            for (NSInteger j = i; j < i + cBIAS_6; j++) {
                IXMetaData *meta = self.pData[j];
                MA6 = MA6 + meta.close;
            }
            MA6 = MA6 / cBIAS_6;
            CGFloat bias6 = (meta.close - MA6) / MA6 * 100;
            [dic setValue:@(bias6) forKey:kBIAS_1];
        } else {
            [dic setValue:[NSNull null] forKey:kBIAS_1];
        }
        
        CGFloat MA12 = 0.f;
        if (i + cBIAS_12 <= cnt) {
            for (NSInteger j = i; j < i + cBIAS_12; j++) {
                IXMetaData *meta = self.pData[j];
                MA12 = MA12 + meta.close;
            }
            MA12 = MA12 / cBIAS_12;
            CGFloat bias12 = (meta.close - MA12) / MA12 * 100;
            [dic setValue:@(bias12) forKey:kBIAS_2];
        } else {
            [dic setValue:[NSNull null] forKey:kBIAS_2];
        }
        
        CGFloat MA24 = 0.f;
        if (i + cBIAS_24 <= cnt) {
            for (NSInteger j = i; j < i + cBIAS_24; j++) {
                IXMetaData *meta = self.pData[j];
                MA24 = MA24 + meta.close;
            }
            MA24 = MA24 / cBIAS_24;
            CGFloat bias24 = (meta.close - MA24) / MA24 * 100;
            [dic setValue:@(bias24) forKey:kBIAS_3];
        } else {
            [dic setValue:[NSNull null] forKey:kBIAS_3];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------CCI---------------------------------

- (void)load_S_CCI
{
    int cnt = self.pData.count;
    NSMutableArray *typMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i < cnt; i++) {
        IXMetaData *meta = self.pData[i];
        CGFloat ftyp = (meta.high + meta.low + meta.close)/3.f;
        [typMutableArray addObject:[NSNumber numberWithFloat:ftyp]];
    }
    
    NSMutableArray *typAveMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i + cCCI_14 < typMutableArray.count; i++) {
        CGFloat typSum = 0;
        for (NSInteger j = i; j < i + cCCI_14; j++) {
            typSum += [typMutableArray[j] floatValue];
        }
        [typAveMutableArray addObject:[NSNumber numberWithFloat:typSum / cCCI_14]];
    }
    
    NSMutableArray *typAveDEVMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i + cCCI_14 < typMutableArray.count; i++) {
        CGFloat avedev = 0;
        for (NSInteger j = i; j < i + cCCI_14; j++) {
            avedev += fabs([typAveMutableArray[i] floatValue]-[typMutableArray[j] floatValue]);
        }
        [typAveDEVMutableArray addObject:[NSNumber numberWithFloat:avedev / cCCI_14]];
    }
    
    NSMutableArray *cciMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i<typAveDEVMutableArray.count; i++) {
        CGFloat cci = 0.f;
        if ([typAveDEVMutableArray[i] floatValue] == 0) {
            cci = ([typMutableArray[i] floatValue]-[typAveMutableArray[i] floatValue])/(0.00001);
        }else{
            cci = ([typMutableArray[i] floatValue]-[typAveMutableArray[i] floatValue])/(0.015*[typAveDEVMutableArray[i] floatValue]);
        }
        
        [cciMutableArray addObject:[NSNumber numberWithFloat:cci]];
    }
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i < cciMutableArray.count) {
            [dic setValue:cciMutableArray[i] forKey:kCCI_CCI];
        } else {
            [dic setValue:[NSNull null] forKey:kCCI_CCI];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------DKBY--------------------------------
- (void)load_S_DKBY
{
    int cnt = self.pData.count;
    
    NSMutableArray *var1Arr = [NSMutableArray array];
    NSMutableArray *var2Arr = [NSMutableArray array];
    for (NSInteger i = 0; i < cnt; i++) {
        CGFloat hest = -MAXFLOAT;
        CGFloat lest =  MAXFLOAT;
        if (i + cDKBY_21 <= cnt) {
            for (NSInteger j = i; j < i + cDKBY_21; j++) {
                IXMetaData *meta = self.pData[j];
                hest = fmax(hest, meta.high);
                lest = fmin(lest, meta.low);
            }
            CGFloat var1 = 0.f;
            CGFloat var2 = 0.f;
            IXMetaData *meta = self.pData[i];
            if (hest - lest == 0) {
                var1 = (hest - meta.close) / (0.001) * 100 - 10;
                var2 = (meta.close - lest) / (0.001) * 100;
            }else{
                var1 = (hest - meta.close) / (hest - lest) * 100 - 10;
                var2 = (meta.close - lest) / (hest - lest) * 100;
            }
            [var1Arr addObject:@(var1)];
            [var2Arr addObject:@(var2)];
        }
    }
    
    NSMutableArray *var3Arr = [NSMutableArray array];
    NSUInteger var2Cnt = var2Arr.count;
    for (NSInteger i = 0; i + cDKBY_13 <= var2Cnt; i++) {
        CGFloat trace = 0;
        if (var2Arr.count > cDKBY_13 * cDKBY_5) {
            trace = cDKBY_5;
        }else{
            trace = var2Arr.count / cDKBY_13;
        }
        float var3 = [self recurSMA:var2Arr num:cDKBY_13 days:(NSInteger)cDKBY_13 * trace weight:cDKBY_8 time:trace];
        [var3Arr addObject:@(var3)];
        [var2Arr removeObjectAtIndex:0];
    }
    
    NSUInteger var1Cnt = var1Arr.count;
    NSUInteger var3Cnt = var3Arr.count;
    for (NSInteger i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i + cDKBY_13 < var3Cnt) {
            CGFloat trace = 0;
            if (var3Arr.count > cDKBY_13 * cDKBY_5) {
                trace = cDKBY_5;
            } else {
                trace = var3Arr.count / cDKBY_13;
            }
            CGFloat ene1 = [self recurSMA:var3Arr num:cDKBY_13 days:(NSInteger)cDKBY_13 * trace weight:cDKBY_8 time:trace];
            [dic setValue:@(ene1) forKey:kDKBY_ENE1];
            [var3Arr removeObjectAtIndex:0];
        } else {
            [dic setValue:[NSNull null] forKey:kDKBY_ENE1];
        }
        
        if (i + cDKBY_21 < var1Cnt) {
            CGFloat trace = 0;
            if (var1Arr.count > cDKBY_21 * cDKBY_5) {
                trace = cDKBY_5;
            }else{
                trace = var1Arr.count / cDKBY_21;
            }
            CGFloat ene2 = [self recurSMA:var1Arr num:cDKBY_21 days:(NSInteger)cDKBY_21 * trace weight:cDKBY_8 time:trace];
            [dic setValue:@(ene2) forKey:kDKBY_ENE2];
            [var1Arr removeObjectAtIndex:0];
        } else {
            [dic setValue:[NSNull null] forKey:kDKBY_ENE2];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------KD----------------------------------
- (void)load_S_KD
{
    int cnt = self.pData.count;
    
    NSMutableArray *rsvMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i < cnt; i++) {
        IXMetaData *meta = self.pData[i];
        CGFloat high = meta.high;
        CGFloat low  = meta.low;
        
        NSInteger cirl = 0;
        if (i + cKKD_9 < cnt) {
            cirl = cKKD_9;
        }else{
            cirl = cnt - i;
        }
        for (NSInteger j = i; j < i + cirl; j++) {
            IXMetaData *meta = self.pData[j];
            high = fmax(high, meta.high);
            low  = fmin(low, meta.low);
        }
        CGFloat rsv = 0;
        if (high-low == 0) {
            rsv = (meta.close - low) / (0.001) * 100;
        }else{
            rsv = (meta.close - low) / (high - low) *100;
        }
        [rsvMutableArray addObject:@(rsv)];
    }
    
    NSMutableArray *kdKMutableArray = [NSMutableArray array];
    NSInteger rsvNum = rsvMutableArray.count;
    for (NSInteger i = 0; i < rsvNum - cKKD_3; i++) {
        CGFloat rsvTrace = 0;
        if (rsvMutableArray.count > cKKD_3 * 5) {
            rsvTrace = 5;
        }else{
            rsvTrace = rsvMutableArray.count / (int)cKKD_3;
        }
        CGFloat kdk = [self recurSMA:rsvMutableArray num:cKKD_3 days:cKKD_3 * rsvTrace weight:1 time:rsvTrace];
        [kdKMutableArray addObject:[NSNumber numberWithFloat:kdk]];
        [rsvMutableArray removeObjectAtIndex:0];
    }
    
    NSArray *kdKArray = [kdKMutableArray copy];
    NSMutableArray *kdDMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i + cKKD_3 < kdKArray.count; i++) {
        CGFloat kdkTrace = 0;
        if (kdKMutableArray.count>cKKD_3 * 5) {
            kdkTrace = 5;
        }else{
            kdkTrace = kdKMutableArray.count/(float)cKKD_3;
        }
        CGFloat kdd = [self recurSMA:kdKMutableArray num:cKKD_3 days:cKKD_3 * kdkTrace weight:1 time:kdkTrace];
        [kdDMutableArray addObject:[NSNumber numberWithFloat:kdd]];
        [kdKMutableArray removeObjectAtIndex:0];
    }
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i < kdKArray.count) {
            [dic setValue:kdKArray[i] forKey:kKD_K];
        } else {
            [dic setValue:[NSNull null] forKey:kKD_K];
        }
        
        if (i < kdDMutableArray.count) {
            [dic setValue:kdDMutableArray[i] forKey:kKD_D];
        } else {
            [dic setValue:[NSNull null] forKey:kKD_D];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------KDJ---------------------------------

- (void)load_S_KDJ
{
    int cnt = self.pData.count;
    
    NSMutableArray *rsvMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i < cnt; i++) {
        IXMetaData *meta = self.pData[i];
        CGFloat high = meta.high;
        CGFloat low  = meta.low;
        
        NSInteger cirl = 0;
        if (i + cKKD_9 < cnt) {
            cirl = cKKD_9;
        } else {
            cirl = cnt - i;
        }
        for (NSInteger j = i; j < i + cirl; j++) {
            IXMetaData *meta = self.pData[j];
            high = fmax(high, meta.high);
            low  = fmin(low, meta.low);
        }
        CGFloat rsv = 0;
        if (high-low == 0) {
            rsv = (meta.close - low) / (0.001) * 100;
        } else {
            rsv = (meta.close - low) / (high - low) *100;
        }
        [rsvMutableArray addObject:@(rsv)];
    }
    
    NSMutableArray *kdKMutableArray = [NSMutableArray array];
    NSInteger rsvNum = rsvMutableArray.count;
    for (NSInteger i = 0; i < rsvNum - cKKD_3; i++) {
        CGFloat rsvTrace = 0;
        if (rsvMutableArray.count > cKKD_3 * 5) {
            rsvTrace = 5;
        } else {
            rsvTrace = rsvMutableArray.count / (int)cKKD_3;
        }
        CGFloat kdk = [self recurSMA:rsvMutableArray num:cKKD_3 days:cKKD_3 * rsvTrace weight:1 time:rsvTrace];
        [kdKMutableArray addObject:[NSNumber numberWithFloat:kdk]];
        [rsvMutableArray removeObjectAtIndex:0];
    }
    
    NSArray *kdKArray = [kdKMutableArray copy];
    NSMutableArray *kdDMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i + cKKD_3 < kdKArray.count; i++) {
        CGFloat kdkTrace = 0;
        if (kdKMutableArray.count>cKKD_3 * 5) {
            kdkTrace = 5;
        } else {
            kdkTrace = kdKMutableArray.count/(float)cKKD_3;
        }
        CGFloat kdd = [self recurSMA:kdKMutableArray num:cKKD_3 days:cKKD_3 * kdkTrace weight:1 time:kdkTrace];
        [kdDMutableArray addObject:[NSNumber numberWithFloat:kdd]];
        [kdKMutableArray removeObjectAtIndex:0];
    }
    
    NSMutableArray *kdjJMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i<kdDMutableArray.count; i++) {
        CGFloat kdjJ = 3 * [kdKArray[i] floatValue] - 2 * [kdDMutableArray[i] floatValue];
        [kdjJMutableArray addObject:[NSNumber numberWithFloat:kdjJ]];
    }
    
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i < kdKArray.count) {
            [dic setValue:kdKArray[i] forKey:kKDJ_K];
        } else {
            [dic setValue:[NSNull null] forKey:kKDJ_K];
        }
        
        if (i < kdDMutableArray.count) {
            [dic setValue:kdDMutableArray[i] forKey:kKDJ_D];
        } else {
            [dic setValue:[NSNull null] forKey:kKDJ_D];
        }
        
        if (i < kdjJMutableArray.count) {
            [dic setValue:kdjJMutableArray[i] forKey:kKDJ_J];
        } else {
            [dic setValue:[NSNull null] forKey:kKDJ_J];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------LWR---------------------------------
- (void)load_S_LWR
{
    int cnt = self.pData.count;
    NSMutableArray *rsvMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i < cnt; i++) {
        IXMetaData *meta = self.pData[i];
        CGFloat high = meta.high;
        CGFloat low  = meta.low;
        NSInteger cirl = 0;
        if (i + cLWR_9 < cnt) {
            cirl = cLWR_9;
        } else {
            cirl = cnt - i;
        }
        for (NSInteger j = i; j< i + cirl; j++) {
            IXMetaData *meta = self.pData[j];
            high = fmax(high, meta.high);
            low  = fmin(low,   meta.low);
        }
        CGFloat rsv = 0.f;
        if (high - low == 0) {
            rsv = (high - meta.close) / (0.001) * 100;
        } else {
            rsv = (high - meta.close) / (high - low)*100;
        }
        [rsvMutableArray addObject:[NSNumber numberWithFloat:rsv]];
    }
    
    NSMutableArray *lwr1MutableArray = [NSMutableArray array];
    NSInteger rsvNum = rsvMutableArray.count;
    for (NSInteger i = 0; i < rsvNum - cLWR_3; i++) {
        CGFloat rsvTrace = 0;
        if (rsvMutableArray.count > cLWR_3 * 5) {
            rsvTrace = 5;
        } else {
            rsvTrace = rsvMutableArray.count / (float)cLWR_3;
        }
        CGFloat lwr1 = [self recurSMA:rsvMutableArray num:cLWR_3 days:cLWR_3 * rsvTrace weight:1 time:rsvTrace];
        [lwr1MutableArray addObject:[NSNumber numberWithFloat:lwr1]];
        [rsvMutableArray removeObjectAtIndex:0];
    }
    
    NSArray *lwr1Array = [lwr1MutableArray copy];
    NSMutableArray *lwr2MutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i + cLWR_3 < lwr1Array.count; i++) {
        CGFloat lwrTrace = 0;
        if (lwr1MutableArray.count>cLWR_3 * 5) {
            lwrTrace = 5;
        } else {
            lwrTrace = lwr1MutableArray.count / (float)cLWR_3;
        }
        CGFloat lwr2 = [self recurSMA:lwr1MutableArray num:cLWR_3 days:cLWR_3 * lwrTrace weight:1 time:lwrTrace];
        [lwr2MutableArray addObject:[NSNumber numberWithFloat:lwr2]];
        [lwr1MutableArray removeObjectAtIndex:0];
    }
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i < lwr1Array.count) {
            [dic setValue:lwr1Array[i] forKey:kLWR_1];
        } else {
            [dic setValue:[NSNull null] forKey:kLWR_1];
        }
        
        if (i < lwr2MutableArray.count) {
            [dic setValue:lwr2MutableArray[i] forKey:kLWR_2];
        } else {
            [dic setValue:[NSNull null] forKey:kLWR_2];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark --------------------------------QHLSR--------------------------------
- (void)load_S_QHLSR
{
    int cnt = self.pData.count;
    NSMutableArray *ohlMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i + 1 < cnt; i++) {
        IXMetaData *meta0 = self.pData[i];
        IXMetaData *meta1 = self.pData[i + 1];
        CGFloat ohl = meta0.close - meta1.close;
        [ohlMutableArray addObject:[NSNumber numberWithFloat:ohl]];
    }
    NSMutableArray *dMutableArray = [NSMutableArray array];
    NSMutableArray *gMutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i+5 < ohlMutableArray.count; i++) {
        CGFloat a = 0;
        CGFloat b = 0;
        for (NSInteger j = i; j < i + 5; j++) {
            a += ([ohlMutableArray[j] floatValue]>0)?[ohlMutableArray[j] floatValue]:0;
            b += ([ohlMutableArray[j] floatValue]<0)?[ohlMutableArray[j] floatValue]:0;
        }
        CGFloat d = a / (a+fabs(b));
        [dMutableArray addObject:[NSNumber numberWithFloat:d]];
        if (i+10 < ohlMutableArray.count) {
            CGFloat e = 0;
            CGFloat f = 0;
            for (NSInteger n = i; n<i+10; n++) {
                e += ([ohlMutableArray[n] floatValue]>0)?[ohlMutableArray[n] floatValue]:0;
                f += ([ohlMutableArray[n] floatValue]<0)?[ohlMutableArray[n] floatValue]:0;
            }
            CGFloat g = e/(e+fabs(f));
            [gMutableArray addObject:[NSNumber numberWithFloat:g]];
        }
    }
    NSArray *qhl10Array = [gMutableArray copy];
    NSMutableArray *qhl5MutableArray = [NSMutableArray array];
    for (NSInteger i = 0; i<dMutableArray.count; i++) {
        CGFloat sum1 = 0;
        CGFloat sum2 = 0;
        for (NSInteger j = i; j<i+5; j++) {
            sum1 += ([ohlMutableArray[j] floatValue]>0)?1:0;
            sum2 += ([ohlMutableArray[j] floatValue]<0)?1:0;
        }
        CGFloat qhl5 = (sum1==5)?1:((sum2==5)?0:[dMutableArray[i] floatValue]);
        [qhl5MutableArray addObject:[NSNumber numberWithFloat:qhl5]];
    }
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i < qhl5MutableArray.count) {
            [dic setValue:qhl5MutableArray[i] forKey:kQHLSR_5];
        } else {
            [dic setValue:[NSNull null] forKey:kQHLSR_5];
        }
        
        if (i < qhl10Array.count) {
            [dic setValue:qhl10Array[i] forKey:kQHLSR_10];
        } else {
            [dic setValue:[NSNull null] forKey:kQHLSR_10];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------RSI---------------------------------

- (void)load_S_RSI
{
    int cnt = self.pData.count;
    NSMutableArray *maxMutableArray = [NSMutableArray array];
    NSMutableArray *absMutableArray = [NSMutableArray array];
    CGFloat maxClose = 0;
    CGFloat absClose = 0;
    for (NSInteger i = 0; i + 1 < cnt; i++) {
        IXMetaData *meta0 = self.pData[i];
        IXMetaData *meta1 = self.pData[i + 1];
        maxClose = fmax(0, meta0.close - meta1.close);
        absClose = fabs(meta0.close - meta1.close);
        [maxMutableArray addObject:[NSNumber numberWithFloat:maxClose]];
        [absMutableArray addObject:[NSNumber numberWithFloat:absClose]];
    }
    
    NSMutableArray *rsi1MutableArray = [NSMutableArray array];
    NSMutableArray *rsi2MutableArray = [NSMutableArray array];
    NSMutableArray *rsi3MutableArray = [NSMutableArray array];
    NSInteger num = maxMutableArray.count;
    for (NSInteger i = 0; i < num - cRSI_6; i++) {
        CGFloat trace1 = 0;
        if (maxMutableArray.count>cRSI_6 * 5) {
            trace1 = 5;
        } else {
            trace1 = maxMutableArray.count / (CGFloat)cRSI_6;
        }
        CGFloat a1 = [self recurSMA:maxMutableArray num:cRSI_6 days:cRSI_6 * trace1 weight:1 time:trace1];
        CGFloat b1 = [self recurSMA:absMutableArray num:cRSI_6 days:cRSI_6 * trace1 weight:1 time:trace1];
        [rsi1MutableArray addObject:[NSNumber numberWithFloat:a1 / b1 * 100]];
        
        if (i < num - cRSI_12) {
            CGFloat trace2 = 0;
            if (maxMutableArray.count > cRSI_12 * 5) {
                trace2 = 5;
            }else{
                trace2 = maxMutableArray.count / (float)cRSI_12;
            }
            CGFloat a2 = [self recurSMA:maxMutableArray num:cRSI_12 days:cRSI_12 * trace2 weight:1 time:trace2];
            CGFloat b2 = [self recurSMA:absMutableArray num:cRSI_12 days:cRSI_12 * trace2 weight:1 time:trace2];
            [rsi2MutableArray addObject:[NSNumber numberWithFloat:a2 / b2 * 100]];
        }
        
        if (i<num - cRSI_24) {
            CGFloat trace3 = 0;
            if (maxMutableArray.count > cRSI_24 * 5) {
                trace3 = 5;
            } else {
                trace3 = maxMutableArray.count / (float)cRSI_24;
            }
            CGFloat a3 = [self recurSMA:maxMutableArray num:cRSI_24 days:cRSI_24 * trace3 weight:1 time:trace3];
            CGFloat b3 = [self recurSMA:absMutableArray num:cRSI_24 days:cRSI_24 * trace3 weight:1 time:trace3];
            [rsi3MutableArray addObject:[NSNumber numberWithFloat:a3 / b3 * 100]];
        }
        [maxMutableArray removeObjectAtIndex:0];
        [absMutableArray removeObjectAtIndex:0];
    }
    for (int i = 0; i < cnt; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (i < rsi1MutableArray.count) {
            [dic setValue:rsi1MutableArray[i] forKey:kRSI_1];
        } else {
            [dic setValue:[NSNull null] forKey:kRSI_1];
        }
        if (i < rsi2MutableArray.count) {
            [dic setValue:rsi2MutableArray[i] forKey:kRSI_2];
        } else {
            [dic setValue:[NSNull null] forKey:kRSI_2];
        }
        if (i < rsi3MutableArray.count) {
            [dic setValue:rsi3MutableArray[i] forKey:kRSI_3];
        } else {
            [dic setValue:[NSNull null] forKey:kRSI_3];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------WR----------------------------------
- (void)load_S_WR
{
    int cnt = self.pData.count;
    for (int i = 0; i < cnt; i++) {
        IXMetaData *meta = self.pData[i];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        CGFloat high_6 = - MAXFLOAT;
        CGFloat low_6  =   MAXFLOAT;
        if (i + cWR_6 <= cnt) {
            for(NSInteger j = i; j < i + cWR_6; j++) {
                IXMetaData *meta = self.pData[j];
                high_6 = fmax(high_6, meta.high);
                low_6  = fmin(low_6, meta.low);
            }
        }
        
        if (high_6 != 0.f && low_6 != 0.f) {
            CGFloat rsv = 0.f;
            if (high_6 - low_6 == 0) {
                rsv = (high_6 - meta.close) / (0.001) * 100;
            } else {
                rsv = (high_6 - meta.close) / (high_6 - low_6) * 100;
            }
            [dic setValue:@(rsv) forKey:kWR_1];
        } else {
            [dic setValue:[NSNull null] forKey:kWR_1];
        }
        
        CGFloat high_10 = - MAXFLOAT;
        CGFloat low_10  =   MAXFLOAT;
        if (i + cWR_10 <= cnt) {
            for(NSInteger j = i; j < i + cWR_10; j++) {
                IXMetaData *meta = self.pData[j];
                high_10 = fmax(high_10, meta.high);
                low_10  = fmin(low_10, meta.low);
            }
        }
        
        if (high_10 != 0.f && low_10 != 0.f) {
            CGFloat rsv = 0.f;
            if (high_10 - low_10 == 0) {
                rsv = (high_10 - meta.close) / (0.001) * 100;
            } else {
                rsv = (high_10 - meta.close) / (high_10 - low_10) * 100;
            }
            [dic setValue:@(rsv) forKey:kWR_2];
        } else {
            [dic setValue:[NSNull null] forKey:kWR_2];
        }
        [self.scndIdxArr addObject:dic];
    }
}

#pragma mark ---------------------------------------------------------------------
#pragma mark -------------------------------public--------------------------------
#pragma mark ---------------------------------------------------------------------
#pragma mark ---------------------------------MA----------------------------------

#pragma mark ---------------------------------SMA---------------------------------

- (CGFloat)recurSMA:(NSArray *)arr num:(NSInteger)n days:(NSInteger)d weight:(NSInteger)w time:(CGFloat)t
{
    CGFloat sma = 0.0;
    if (d > 0) {
        sma = (w * [arr[(NSInteger)(n * t) - (NSInteger)d] floatValue] + (n - w) * [self recurSMA:arr num:n days:d - 1 weight:w time:t]) / n;
    }else{
        sma = [arr[(NSInteger)(n * t - 1)] floatValue];
    }
    return sma;
}


#pragma mark ---------------------------------EMA---------------------------------

- (void)recurEMA:(id)a count:(NSInteger)cnt nothing:(NSInteger)no
{
    
}

@end
