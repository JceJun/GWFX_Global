//
//  IXTSDisplayV+detect.h
//  IXApp
//
//  Created by Magee on 2017/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXTSDisplayV.h"

@interface IXTSDisplayV (detect)

- (void)mainBryDet;

- (void)scndBryDet;

@end
