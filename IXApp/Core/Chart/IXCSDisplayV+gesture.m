//
//  IXCSDisplayV+gesture.m
//  KLine
//
//  Created by Magee on 2016/12/8.
//  Copyright © 2016年 wsz. All rights reserved.
//

#import "IXCSDisplayV+gesture.h"

@implementation IXCSDisplayV (gesture)

- (void)registGestures
{
    self.gstPan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    self.gstpnc = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinch:)];
    self.gstPrs = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handelLongPrs:)];
    self.gstPrs.minimumPressDuration = 0.2f;
    self.gstPrs.cancelsTouchesInView = NO;
    self.gsttap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handelDoubleClk:)];
    self.gsttap.numberOfTapsRequired = 2;
    
    [self addGestureRecognizer:self.gstPan];
    [self addGestureRecognizer:self.gstPrs];
    [self addGestureRecognizer:self.gstpnc];
    [self addGestureRecognizer:self.gsttap];
    
    self.longPressGesOp = [@[] mutableCopy];
}

- (void)resignGestures
{
    [self removeGestureRecognizer:self.gstPan];
    [self removeGestureRecognizer:self.gstPrs];
    [self removeGestureRecognizer:self.gstpnc];
    [self removeGestureRecognizer:self.gsttap];
}

CGPoint cPrior;
CGPoint cNext;
- (void)handlePan:(UIPanGestureRecognizer *)rec
{
    switch (rec.state) {
        case UIGestureRecognizerStateBegan: {
            cPrior = [rec locationInView:self];
            cNext  = [rec locationInView:self];
        }
            break;
        case UIGestureRecognizerStateChanged: {
            cPrior = [rec locationInView:self];
            CGFloat dis = cPrior.x - cNext.x;
            CGFloat candleWdth = self.curMWth/self.curElmtCnt;
            
            if (fabs(dis) >= candleWdth) {
                int disIndex = (int)dis/(int)candleWdth;
                
                if (dis > 0) {
                    if (self.LIdx == self.pData.count - 1 ||
                        self.pData.count <= self.curElmtCnt) {
                        return;
                    } else {
                        self.LIdx += disIndex;
                        if (self.LIdx >= self.pData.count) {
                            self.LIdx = self.pData.count - 1;
                        }
                        if (self.LIdx - self.RIdx >= self.curElmtCnt) {
                            self.RIdx = self.LIdx - self.curElmtCnt + 1;
                        }
                    }
                } else {
                    if (self.RIdx == 0) {
                        return;
                    } else {
                        self.RIdx += disIndex;
                        if (self.RIdx < 0) {
                            self.RIdx = 0;
                        }
                        if (self.LIdx - self.RIdx >= self.curElmtCnt) {
                            self.LIdx = self.RIdx + self.curElmtCnt - 1;
                        }
                    }
                }
                cNext = cPrior;
                [self setNeedsDisplay];
            }
            [self checkPdata];
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:{
            cPrior = CGPointZero;
            cNext = CGPointZero;
        }
            break;
        default:
            break;
    }
}


- (void)handelLongPrs:(UILongPressGestureRecognizer *)rec
{
    switch (rec.state) {
        case UIGestureRecognizerStateBegan: {
            [self cleanLongPressOp];
            CGPoint point = [rec locationInView:self];
            CGFloat prsY = point.y;
            if(prsY <= self.curMTopY ||
               prsY >= self.curSBtmY ){
                return;
            }
            self.gstType = IXCSGstTypePrs;
            self.pointCrossLine = point;
            [self setNeedsDisplay];
            self.detailV.hidden = NO;
            [self addSubview:self.detailV];
            
            CGFloat height = 135;
            if (self.curMarketID != 0 &&
                self.curMarketID != 1 &&
                self.curMarketID != 2 &&
                self.curMarketID != 9) {
                height = 105;
            }
            
            if (point.x < self.curLWth + self.curMWth/2) {
                self.detailV.frame = CGRectMake(self.frame.size.width - 100, self.curMTopY, 100, height);
            } else {
                self.detailV.frame = CGRectMake(self.curLWth, self.curMTopY, 100, height);
            }
        }
            break;
        case UIGestureRecognizerStateChanged: {
            CGPoint point = [rec locationInView:self];
            if(point.y <= self.curMTopY ||
               point.y - 1 >= self.curSBtmY){
                return;
            }
            self.pointCrossLine = point;
            [self setNeedsDisplay];
            
            CGFloat height = 135;
            if (self.curMarketID != 0 &&
                self.curMarketID != 1 &&
                self.curMarketID != 2 &&
                self.curMarketID != 9) {
                height = 105;
            }
            
            if (point.x < self.curLWth + self.curMWth/2) {
                self.detailV.frame = CGRectMake(self.frame.size.width - 100, self.curMTopY, 100, height);
            } else {
                self.detailV.frame = CGRectMake(self.curLWth, self.curMTopY, 100, height);
            }
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
            [self cleanLongPressOp];
            
            NSInvocationOperation * op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(longPressEnd) object:nil];
            [self.longPressGesOp addObject:op];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (!op.cancelled) {
                    [op start];
                }
            });
        }
            break;
        default:
            break;
    }
}

- (void)cleanLongPressOp
{
    for (NSInvocationOperation * op in self.longPressGesOp) {
        [op cancel];
    }
    [self.longPressGesOp removeAllObjects];
}

- (void)longPressEnd
{
    [self.longPressGesOp removeAllObjects];
    self.gstType = IXCSGstTypeNone;
    [self setNeedsDisplay];
    self.detailV.hidden = YES;
    [self.detailV removeFromSuperview];
}

-(void)handlePinch:(UIPinchGestureRecognizer *)rec
{
    switch (rec.state) {
        case UIGestureRecognizerStateBegan: {
            self.gstType = IXCSGstTypePnc;
            self.curElmtCntTmp = self.curElmtCnt;
        }
            break;
        case UIGestureRecognizerStateChanged: {
            self.curElmtCntTmp = self.curElmtCnt/rec.scale;
            if (self.curElmtCntTmp > self.maxElmtCnt) {
                self.curElmtCntTmp = self.maxElmtCnt;
            } else if (self.curElmtCntTmp < self.minElmtCnt){
                self.curElmtCntTmp = self.minElmtCnt;
            }
            self.LIdx = self.RIdx + self.curElmtCntTmp - 1;
            if(self.LIdx >= self.pData.count) {
                self.LIdx = self.pData.count -1;
            }
            if(self.LIdx == self.pData.count -1) {
                self.RIdx = self.LIdx - self.curElmtCntTmp + 1;
                if(self.RIdx < 0) {
                    self.RIdx = 0;
                }
            }
            [self setNeedsDisplay];
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
            self.curElmtCnt = self.curElmtCntTmp;
            self.gstType = IXCSGstTypeNone;
        }
            break;
        default:
            break;
    }
}

- (void)handelDoubleClk:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(IXCSDisplayVDoubleClicked)]) {
        [self.delegate IXCSDisplayVDoubleClicked];
    }
}

@end
