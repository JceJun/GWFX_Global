//
//  IXCSDisplayV+detect.h
//  IXApp
//
//  Created by Magee on 2017/3/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCSDisplayV.h"

@interface IXCSDisplayV (detect)

#pragma mark - 主图边界检测
- (void)detect_M_MA;     //移动平均线
- (void)detect_M_BBI;    //多空指标
- (void)detect_M_BOLL;   //布林线指标
- (void)detect_M_MIKE;   //麦克支撑压力指标
- (void)detect_M_PBX;    //瀑布线

#pragma mark - 副图边界检测
- (void)detect_S_MACD;   //指数平滑移动平均线
- (void)detect_S_ARBR;   //人气意愿
- (void)detect_S_ATR;    //真实波动幅度均值
- (void)detect_S_BIAS;   //乖离率(偏离率)
- (void)detect_S_CCI;    //超卖超买
- (void)detect_S_DKBY;   //多空博弈
- (void)detect_S_KD;     //随机指数???
- (void)detect_S_KDJ;    //随机指标
- (void)detect_S_LWR;    //反向指标
- (void)detect_S_QHLSR;  //阻力指标
- (void)detect_S_RSI;    //相对强弱指标
- (void)detect_S_WR;     //威廉指标

@end
