//
//  IXKLineSql.m
//  FXApp
//
//  Created by Seven on 2017/8/7.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "IXKLineSql.h"
#import "IXKSqlHeader.h"

@implementation IXKLineSql

/**
 create table
 
 @param table table name
 @return create table sql
 */
+ (NSString *)sql_createTable:(NSString *)table
{
    if (!table.length) {
        return nil;
    }
    
    NSArray * textArr = @[
                          klp_KLineType,klp_symbolId,klp_time,klp_preTime,klp_high,klp_low,klp_close,
                          klp_open,klp_amt,klp_vol,klp_idp,klp_total,klp_offset,klp_count,klp_seq,
                          klp_digit,klp_reservedText,klp_reservedNum,
                          ];
    
    NSArray * paramArr = @[@" int,",@" long,",@" long,",@" long,",@" float,",@" float,",@" float,",
                           @" float,",@" long,",@" long,",@" long,",@" int,",@" int,",@" int,",
                           @" long,",@" short,",@" text not null,",@" long,"
                           ];

    NSString    * sql = [NSString stringWithFormat:@"create table %@ (",table];
    for (int i = 0; i < textArr.count; i++) {
        NSString    * text = textArr[i];
        NSString    * param = paramArr[i];
        sql = [sql stringByAppendingString:text];
        sql = [sql stringByAppendingString:param];
    }
    
    sql = [sql stringByAppendingFormat:@"primary key (%@,%@) );",klp_symbolId,klp_time];
    
    return sql;
}


#pragma mark -
#pragma mark - 查询语句

/** 获取时间为time的记录*/
+ (NSString *)sql_selectMetaWithTable:(NSString *)table
                             symbolId:(int64_t)id_p
                                 time:(int64_t)time
{
    NSString    * sql = [NSString stringWithFormat:@"select * from %@ where %@ = %lld and %@ = %lld;",
                         table,
                         klp_symbolId,id_p,
                         klp_time,time
                         ];
    return sql;
}

/* 获取preTime == time的数据 */
+ (NSString *)sql_selectMateWithTable:(NSString *)table
                             symbolId:(int64_t)id_p
                              preTime:(int64_t)time
{
    NSString    * sql = [NSString stringWithFormat:@"select * from %@ where %@ = %lld and %@ = %lld;",
                         table,
                         klp_symbolId,id_p,
                         klp_preTime,time
                         ];
    return sql;
}

/* 获取时间小于time的最新记录 */
+ (NSString *)sql_selectMetaWithTable:(NSString *)table
                             symbolId:(int64_t)id_p
                           beforeTime:(int64_t)time
{
    if (time == 0) {
        time = (int64_t)[[NSDate date] timeIntervalSince1970];
    }
    //降序获取1条记录，则该记录即为目标结果
    NSString    * sql = [NSString stringWithFormat:@"select * from %@ where %@ = %lld and %@ < %lld order by time desc limit 1;",
                         table,
                         klp_symbolId,id_p,
                         klp_time,time
                         ];
    
    return sql;
}

/** 获取时间大于time的最老记录 */
+ (NSString *)sql_selectMetaWithTable:(NSString *)table
                             symbolId:(int64_t)id_p
                            afterTime:(int64_t)time
{
    //升序获取1条记录，则该记录即为目标结果
    NSString    * sql = [NSString stringWithFormat:@"select * from %@ where %@ = %lld and %@ > %lld order by time asc limit 1;",
                         table,
                         klp_symbolId,id_p,
                         klp_time,time
                         ];
    return sql;
}


#pragma mark -
#pragma mark - 删除语句

/* 批量删除某段时间之间的数据 */
+ (NSString *)sql_deleteTable:(NSString *)table
                     symbolId:(int64_t)id_p
                    startTime:(int64_t)sTime
                      endTime:(int64_t)eTime
{
    NSString    * sql = [NSString stringWithFormat:@"delete from %@ where %@ = %lld and %@ >= %lld and %@ <= %lld;",
                         table,
                         klp_symbolId,id_p,
                         klp_time,sTime,
                         klp_time,eTime];
    
    return sql;
}

/* 批量删除time之前的数据 */
+ (NSString *)sql_deleteTable:(NSString *)table
                   beforeTime:(int64_t)time
{
    NSString    * sql = [NSString stringWithFormat:@"delete from %@ where %@ < %lld;",
                         table,
                         klp_time,time];
    
    return sql;
}


#pragma mark -
#pragma mark - 更新语句（插入／更新）

/* 插入／更新sql */
+ (NSString *)sql_insertTable:(NSString *)table
{
    NSString    * sql = [NSString stringWithFormat:@"insert or replace into %@ (%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@) values (%%d,%%lld,%%lld,%%lld,%%f,%%f,%%f,%%f,%%lld,%%lld,%%lld,%%u,%%u,%%u,%%lld,%%d,%%@,%%d);",
                         table,
                         klp_KLineType,
                         klp_symbolId,
                         klp_time,
                         klp_preTime,
                         klp_high,
                         klp_low,
                         klp_close,
                         klp_open,
                         klp_amt,
                         klp_vol,
                         klp_idp,
                         klp_total,
                         klp_offset,
                         klp_count,
                         klp_seq,
                         klp_digit,
                         klp_reservedText,
                         klp_reservedNum
                         ];
    
    return sql;
}

/** 更新某条数据记录的前一条数据时间 */
+ (NSString *)sql_updatePreTimeWithTable:(NSString *)table
                                 symbold:(int64_t)id_p
                                metaTime:(int64_t)time
                                 preTime:(int64_t)pretime
{
    NSString    * sql = [NSString stringWithFormat:@"update %@ set %@ = %lld where %@ = %lld and %@ = %lld;",
                         table,
                         klp_preTime,pretime,
                         klp_symbolId,id_p,
                         klp_time,time
                         ];
    
    return sql;
}


@end
