//
//  IXTSDisplayV+detect.m
//  IXApp
//
//  Created by Magee on 2017/3/8.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXTSDisplayV+detect.h"
#import "IXMetaData.h"

@implementation IXTSDisplayV (detect)

- (void)mainBryDet
{
    CGFloat max = -MAXFLOAT;
    CGFloat min =  MAXFLOAT;
    
    for (NSInteger i = self.LIdx; i >= self.RIdx; i--) {
        IXMetaData *meta = self.pData[i];
        max = fmax(max, meta.high);
        min = fmin(min, meta.low);
    }
//    if (self.lastPrice != 0) {
//        if (self.lastPrice <= 0) {
//            self.lastPrice = min;
//        }
//        max = fmax(self.lastPrice, max);
//        min = fmin(self.lastPrice, min);
//    }
    
    //之前逻辑
    if (self.lastPrice != 0) {
        CGFloat dis = MAX(fabs(self.lastPrice - max), fabs(self.lastPrice - min));
        
        max = self.lastPrice + dis;
        min = self.lastPrice - dis;
    }
    
    CGFloat disF = fabs(max - min);
    
    self.curMainTop = (self.curMHth - 10.f) / disF;
    
    if (self.dotPrice > 0) {
        self.maxPrice = fmax(max, self.dotPrice);
        self.minPrice = fmin(min, self.dotPrice);
    } else {
        self.maxPrice = max;
        self.minPrice = min;
    }
    
    //类似停牌特殊处理
    if (self.minPrice == self.maxPrice) {
        self.minPrice = 0.f;
        self.maxPrice = self.maxPrice*2;
    }
}

- (void)scndBryDet
{
    CGFloat max = -MAXFLOAT;
    CGFloat min =  MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[self.scndIdxArr.count - i - 1];
        CGFloat dif = [dic[kMACD_DIF] floatValue];
        CGFloat dea = [dic[kMACD_DEA] floatValue];
        CGFloat mac = [dic[kMACD_MAC] floatValue];
        
        max = fmax(mac, fmax(dea, fmax(max, dif)));
        min = fmin(mac, fmin(dea, fmin(min, dif)));
        
        if (max > fabs(min)) {
            self.curScndTop =  fabs(max);
            self.curScndBtm = -fabs(max);
        } else {
            self.curScndTop =  fabs(min);
            self.curScndBtm = -fabs(min);
        }
    }
}

@end
