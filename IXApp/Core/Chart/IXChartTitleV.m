//
//  IXChartTitleV.m
//  IXApp
//
//  Created by Magee on 2017/3/1.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXChartTitleV.h"
#import "IXPullDownMenu.h"
#import "NSString+IX.h"

#define kVernier_wdth  (kScreenWidth/5.f-10*2)
#define kVernier_hdth  2

#define kPdview_hdth   160

@interface IXChartTitleV ()

@property (nonatomic,strong)UIView      * vernier;
@property (nonatomic,strong)IXPullDownMenu  * pdView;
@property (nonatomic,strong)NSArray * btnTitles;

@end

@implementation IXChartTitleV

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self vernier];
    }
    self.dk_backgroundColorPicker = DKColorWithRGBs(0xffffff, 0x262f3e);
    return self;
}

- (UIView *)vernier
{
    if (!_vernier) {
        _vernier = [[UIView alloc] initWithFrame:CGRectMake((kScreenWidth/5.f-kVernier_wdth)/2 + 4*kScreenWidth/5.0, self.frame.size.height-2, kVernier_wdth, 2)];
        _vernier.dk_backgroundColorPicker = DKCellTitleColor;
        [self addSubview:_vernier];
    }
    return _vernier;
}

- (IXPullDownMenu *)pdView
{
    if (!_pdView) {
        _pdView = [[IXPullDownMenu alloc] initWithMenuItems:[self menuItems] rowHeight:32 width:kVernier_wdth];
        _pdView.showAttributeText = YES;
        weakself;
        _pdView.itemClicked = ^(NSInteger index, NSString *title) {
            if (weakSelf.btnClick) {
                weakSelf.btnClick(index+5);
            }
            
            weakSelf.selectedTag = (index+5);
        };
        _pdView.endEditBlock = ^{
            [weakSelf enableBtnH:weakSelf.selectedTag];
        };
    }
    return _pdView;
}

- (void)initUI
{
    CGFloat wdth = kScreenWidth/5.f;
    for (int i=0; i<5; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = i;
        CGRect rect = CGRectZero;
        rect.size = CGSizeMake(wdth, self.frame.size.height);
        rect.origin = CGPointMake(i*wdth, 0);
        btn.frame = rect;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn.titleLabel setFont:PF_MEDI(12)];
        [btn setTitle:self.btnTitles[i] forState:UIControlStateNormal];
        
        if (i == 4) {
            NSString    * str = self.btnTitles[i];
            CGSize  size = [str sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(12)}];
            UIImageView * imgV = [[UIImageView alloc] initWithFrame:CGRectMake((rect.size.width+size.width)/2+2,
                                                                               rect.size.height/2,
                                                                               5, 5)];
            imgV.dk_imagePicker = DKImageNames(@"kLine_arrow", @"kLine_arrow_D");
            imgV.tag = 100;
            [btn addSubview:imgV];
        }
        
        [self addSubview:btn];
    }
    
    [self enableBtnH:4];
    _selectedTag = 4;
}

- (void)enableBtnH:(NSInteger)tag
{
    for (id v in self.subviews) {
        if ([v isKindOfClass:[UIButton class]]) {
            UIButton *b = (UIButton *)v;
            if (b.tag == tag) {
                [b dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
            } else {
                [b dk_setTitleColorPicker:DKGrayTextColor1 forState:UIControlStateNormal];
            }
        }
    }
}

- (void)btnClick:(UIButton *)btn
{
    if (btn.tag != 4) {
        _selectedTag = btn.tag;
        [self enableBtnH:btn.tag];
        [self resetMinBtnTitle:LocalizedString(@"分钟")];
        [self resetVernierPositionWithBtnTag:btn.tag];
        if (self.btnClick) {
            self.btnClick(btn.tag);
        }
    } else {
        [btn dk_setTitleColorPicker:DKCellTitleColor forState:UIControlStateNormal];
        [self.pdView showMenuFrom:btn offsetY:0];
    }
}

- (void)resetMinBtnTitle:(NSString *)title
{
    UIButton * btn =[self viewWithTag:4];
    
    if (SameString(title, LocalizedString(@"分钟"))) {
        [btn setAttributedTitle:nil forState:UIControlStateNormal];
        [btn setTitle:title forState:UIControlStateNormal];
    } else {
        UIColor * color = AutoNightColor(0x4c6072, 0xe9e9ea);
        NSAttributedString * attTitle = [title attributeStringWithFontSize:12 textColor:color];
        [btn setAttributedTitle:attTitle forState:UIControlStateNormal];
    }
    
    CGRect  rect = btn.frame;
    CGSize  size = [title sizeWithAttributes:@{NSFontAttributeName:PF_MEDI(12)}];
    [btn viewWithTag:100].frame = CGRectMake((rect.size.width+size.width)/2+2,
                                             rect.size.height/2,
                                             5, 5);
}

- (void)resetVernierPositionWithBtnTag:(NSInteger)tag
{
    [UIView animateWithDuration:.2 animations:^{
        self.vernier.frame = CGRectMake(tag*kScreenWidth/5.f+(kScreenWidth/5.f-kVernier_wdth)/2,
                                    self.frame.size.height-2, kVernier_wdth, kVernier_hdth);
    }];
}

- (void)setSelectedTag:(NSInteger)selectedTag
{
    if (selectedTag > 4) {
        _selectedTag = 4;
    } else {
        _selectedTag = selectedTag;
    }
    [self enableBtnH:_selectedTag];
    [self resetVernierPositionWithBtnTag:_selectedTag];
    
    if (selectedTag > 4) {
        NSString    * title = [[self menuItems] objectAtIndex:selectedTag - 5];
        [self resetMinBtnTitle:title];
    } else {
        [self resetMinBtnTitle:LocalizedString(@"分钟")];
    }
}

- (NSArray *)menuItems
{
    return @[ LocalizedString(@"1分"),
              LocalizedString(@"5分"),
              LocalizedString(@"15分"),
              LocalizedString(@"30分"),
              LocalizedString(@"60分"),
              LocalizedString(@"4小时")];
}

- (NSArray *)btnTitles
{
    if (!_btnTitles) {
        _btnTitles = @[
                       LocalizedString(@"分时"),
                       LocalizedString(@"日K"),
                       LocalizedString(@"周K"),
                       LocalizedString(@"月K"),
                       LocalizedString(@"1分"),
                       ];
    }
    
    return _btnTitles;
}

@end

