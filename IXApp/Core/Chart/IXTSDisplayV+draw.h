//
//  IXTSDisplayV+draw.h
//  IXApp
//
//  Created by Magee on 2016/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTSDisplayV.h"

@interface IXTSDisplayV (draw)

- (void)drawForm;

- (void)drawMainData;   //主图绘制
- (void)drawScndData;   //副图绘制

- (void)drawYscaleMark; //y轴刻度

- (void)drawSYscaleMark;

- (void)drawCrossLine;
- (void)drawIntersect;  //相交点
- (void)drawYMoveScale;

- (void)drawCurrentPriceLine;   //绘制最新价虚线

- (void)drawMainExtent;

- (void)cleanSublayers;

@end
