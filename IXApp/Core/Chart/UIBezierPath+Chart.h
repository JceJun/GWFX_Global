//
//  UIBezierPath+Chart.h
//  FXApp
//
//  Created by Seven on 2017/10/12.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBezierPath (Chart)

+ (instancetype)bezierPathWithPoints:(NSArray *)points;

@end
