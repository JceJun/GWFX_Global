//
//  IXCSDisplayV+detect.m
//  IXApp
//
//  Created by Magee on 2017/3/3.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCSDisplayV+detect.h"
#import "IXMetaData.h"

@implementation IXCSDisplayV (detect)

#pragma mark -
#pragma mark - 主图边界指标

#pragma mark ----------------------------------MA----------------------------------
- (void)detect_M_MA
{
    CGFloat max = -MAXFLOAT;
    CGFloat min =  MAXFLOAT;
    self.minPriceMeta = nil;
    self.maxPriceMeta = nil;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        IXMetaData *meta = self.pData[i];
        NSDictionary *dic = self.mainIdxArr[i];
        
        if (i == 0) {
            max = fmax(meta.high, meta.close);
            min = fmin(meta.low, meta.close);
        }
        
        max = fmax(max, meta.high);
        min = fmin(min, meta.low);
        
        if (!self.maxPriceMeta || meta.high > self.maxPriceMeta.high) {
            self.maxPriceMeta = meta;
        }
        if (!self.minPriceMeta || meta.low < self.minPriceMeta.low) {
            self.minPriceMeta = meta;
        }
        
        id obj5 = dic[kMA_5];
        if (obj5 != [NSNull null]) {
            CGFloat rst  = [dic[kMA_5] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj10 = dic[kMA_10];
        if (obj10 != [NSNull null]) {
            CGFloat rst  = [dic[kMA_10] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj20 = dic[kMA_20];
        if (obj20 != [NSNull null]) {
            CGFloat rst  = [dic[kMA_20] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj60 = dic[kMA_60];
        if (obj60 != [NSNull null]) {
            CGFloat rst  = [dic[kMA_60] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curMainTopU = max;
    self.curMainBtmU = min;
    
    CGFloat f = 5.f/self.curMHth * (max - min);
    max = max + f;
    min = min - f;
    
    self.curMainTop = max;
    self.curMainBtm = min;
    
    //类似停牌特殊处理
    if (self.curMainTop == self.curMainBtm) {
        self.curMainBtm = 0.f;
        self.curMainBtmU = 0.f;
        self.curMainTop = self.curMainTop*2;
        self.curMainTopU = self.curMainTop*2;
    }
}

#pragma mark ----------------------------------BBI----------------------------------
- (void)detect_M_BBI
{
    CGFloat max = -MAXFLOAT;
    CGFloat min =  MAXFLOAT;
    self.minPriceMeta = nil;
    self.maxPriceMeta = nil;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        IXMetaData *meta = self.pData[i];
        NSDictionary *dic = self.mainIdxArr[i];
        
        max = fmax(max, meta.high);
        min = fmin(min, meta.low);
        
        if (!self.maxPriceMeta || meta.high > self.maxPriceMeta.high) {
            self.maxPriceMeta = meta;
        }
        if (!self.minPriceMeta || meta.low < self.minPriceMeta.low) {
            self.minPriceMeta = meta;
        }
        
        id obj = dic[kBBI_BBI];
        if (obj != [NSNull null]) {
            CGFloat rst  = [dic[kBBI_BBI] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curMainTopU = max;
    self.curMainBtmU = min;
    
    CGFloat f = 5.f/self.curMHth * (max - min);
    max = max + f;
    min = min - f;
    
    self.curMainTop = max;
    self.curMainBtm = min;
}

#pragma mark ----------------------------------BOLL---------------------------------
- (void)detect_M_BOLL
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    self.minPriceMeta = nil;
    self.maxPriceMeta = nil;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        IXMetaData *meta = self.pData[i];
        NSDictionary *dic = self.mainIdxArr[i];
        
        max = fmax(max, meta.high);
        min = fmin(min, meta.low);
        
        if (!self.maxPriceMeta || meta.high > self.maxPriceMeta.high) {
            self.maxPriceMeta = meta;
        }
        if (!self.minPriceMeta || meta.low < self.minPriceMeta.low) {
            self.minPriceMeta = meta;
        }
        
        id obju = dic[kBOLL_UP];
        if (obju != [NSNull null]) {
            CGFloat rst  = [dic[kBOLL_UP] floatValue];
            max = fmax(max, rst);
        }
        
        id objm = dic[kBOLL_MA];
        if (objm != [NSNull null]) {
            CGFloat rst  = [dic[kBOLL_MA] floatValue];
            max = fmax(max, rst);
        }
        
        id objl = dic[kBOLL_DN];
        if (objl != [NSNull null]) {
            CGFloat rst  = [dic[kBOLL_DN] floatValue];
            min = fmin(min, rst);
        }
    }
    
    self.curMainTopU = max;
    self.curMainBtmU = min;
    
    CGFloat f = 5.f/self.curMHth * (max - min);
    max = max + f;
    min = min - f;
    
    self.curMainTop = max;
    self.curMainBtm = min;
}

#pragma mark ----------------------------------MIKE---------------------------------
- (void)detect_M_MIKE
{
    CGFloat max = -MAXFLOAT;
    CGFloat min =  MAXFLOAT;
    self.minPriceMeta = nil;
    self.maxPriceMeta = nil;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        IXMetaData *meta = self.pData[i];
        NSDictionary *dic = self.mainIdxArr[i];
        
        max = fmax(max, meta.high);
        min = fmin(min, meta.low);
        
        if (!self.maxPriceMeta || meta.high > self.maxPriceMeta.high) {
            self.maxPriceMeta = meta;
        }
        if (!self.minPriceMeta || meta.low < self.minPriceMeta.low) {
            self.minPriceMeta = meta;
        }
        
        id objwr = dic[kMIKE_WR];
        if (objwr != [NSNull null]) {
            CGFloat rst = [dic[kMIKE_WR] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
            
        }
        
        id objmr = dic[kMIKE_MR];
        if (objmr != [NSNull null]) {
            CGFloat rst = [dic[kMIKE_MR] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id objsr = dic[kMIKE_SR];
        if (objsr != [NSNull null]) {
            CGFloat rst = [dic[kMIKE_SR] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id objws = dic[kMIKE_WS];
        if (objws != [NSNull null]) {
            CGFloat rst = [dic[kMIKE_WS] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id objms = dic[kMIKE_MS];
        if (objms != [NSNull null]) {
            CGFloat rst = [dic[kMIKE_MS] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id objss = dic[kMIKE_SS];
        if (objss != [NSNull null]) {
            CGFloat rst = [dic[kMIKE_SS] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curMainTopU = max;
    self.curMainBtmU = min;
    
    CGFloat f = 5.f/self.curMHth * (max - min);
    max = max + f;
    min = min - f;
    
    self.curMainTop = max;
    self.curMainBtm = min;
}

#pragma mark ----------------------------------PBX----------------------------------
- (void)detect_M_PBX
{
    CGFloat max = -MAXFLOAT;
    CGFloat min =  MAXFLOAT;
    self.minPriceMeta = nil;
    self.maxPriceMeta = nil;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        IXMetaData *meta = self.pData[i];
        NSDictionary *dic = self.mainIdxArr[i];
        
        max = fmax(max, meta.high);
        min = fmin(min, meta.low);
        
        if (!self.maxPriceMeta || meta.high > self.maxPriceMeta.high) {
            self.maxPriceMeta = meta;
        }
        if (!self.minPriceMeta || meta.low < self.minPriceMeta.low) {
            self.minPriceMeta = meta;
        }
        
        id obj1 = dic[kPBX_1];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kPBX_1] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kPBX_2];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kPBX_2] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj3 = dic[kPBX_3];
        if (obj3 != [NSNull null]) {
            CGFloat rst = [dic[kPBX_3] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj4 = dic[kPBX_4];
        if (obj4 != [NSNull null]) {
            CGFloat rst = [dic[kPBX_4] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curMainTopU = max;
    self.curMainBtmU = min;
    
    CGFloat f = 5.f/self.curMHth * (max - min);
    max = max + f;
    min = min - f;
    
    self.curMainTop = max;
    self.curMainBtm = min;
}

#pragma mark -
#pragma mark - 副图边界指标

#pragma mark ----------------------------------MACD---------------------------------
- (void)detect_S_MACD
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[self.scndIdxArr.count - i - 1];
        CGFloat dif = [dic[kMACD_DIF] floatValue];
        CGFloat dea = [dic[kMACD_DEA] floatValue];
        CGFloat mac = [dic[kMACD_MAC] floatValue];
        
        max = fmax(mac, fmax(dea, fmax(max, dif)));
        min = fmin(mac, fmin(dea, fmin(min, dif)));
        
        if (max > fabs(min)) {
            self.curScndTop =  fabs(max);
            self.curScndBtm = -fabs(max);
        } else {
            self.curScndTop =  fabs(min);
            self.curScndBtm = -fabs(min);
        }
    }
}

#pragma mark ----------------------------------ARBR---------------------------------
- (void)detect_S_ARBR
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kARBR_AR];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kARBR_AR] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kARBR_BR];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kARBR_BR] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark ----------------------------------ATR----------------------------------
- (void)detect_S_ATR
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kATR_TR];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kATR_TR] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kATR_ATR];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kATR_ATR] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark ----------------------------------BIAS---------------------------------
- (void)detect_S_BIAS
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kBIAS_1];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kBIAS_1] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kBIAS_2];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kBIAS_2] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj3 = dic[kBIAS_3];
        if (obj3 != [NSNull null]) {
            CGFloat rst = [dic[kBIAS_3] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark ----------------------------------CCI----------------------------------
- (void)detect_S_CCI
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        id obj1 = dic[kCCI_CCI];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kCCI_CCI] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark ----------------------------------DKBY---------------------------------
- (void)detect_S_DKBY
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kDKBY_ENE1];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kDKBY_ENE1] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kDKBY_ENE2];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kDKBY_ENE2] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark -----------------------------------KD----------------------------------
- (void)detect_S_KD
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kKD_K];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kKD_K] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kKD_D];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kKD_D] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark -----------------------------------KDJ---------------------------------
- (void)detect_S_KDJ
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kKDJ_K];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kKDJ_K] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kKDJ_D];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kKDJ_D] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj3 = dic[kKDJ_J];
        if (obj3 != [NSNull null]) {
            CGFloat rst = [dic[kKDJ_J] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark -----------------------------------LWR---------------------------------
- (void)detect_S_LWR
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kLWR_1];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kLWR_1] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kLWR_2];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kLWR_2] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark ----------------------------------QHLSR--------------------------------
- (void)detect_S_QHLSR
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kQHLSR_5];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kQHLSR_5] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kQHLSR_10];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kQHLSR_10] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark -----------------------------------RSI---------------------------------
- (void)detect_S_RSI
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kRSI_1];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kRSI_1] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kRSI_2];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kRSI_2] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj3 = dic[kRSI_3];
        if (obj3 != [NSNull null]) {
            CGFloat rst = [dic[kRSI_3] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    
    self.curScndTop = max;
    self.curScndBtm = min;
}

#pragma mark -----------------------------------WR----------------------------------
- (void)detect_S_WR
{
    CGFloat max = - MAXFLOAT;
    CGFloat min =   MAXFLOAT;
    
    for (NSInteger i = self.RIdx; i <= self.LIdx; i++) {
        NSDictionary *dic = self.scndIdxArr[i];
        
        id obj1 = dic[kWR_1];
        if (obj1 != [NSNull null]) {
            CGFloat rst = [dic[kWR_1] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
        
        id obj2 = dic[kWR_2];
        if (obj2 != [NSNull null]) {
            CGFloat rst = [dic[kWR_2] floatValue];
            max = fmax(max, rst);
            min = fmin(min, rst);
        }
    }
    self.curScndTop = max;
    self.curScndBtm = min;
}

@end

