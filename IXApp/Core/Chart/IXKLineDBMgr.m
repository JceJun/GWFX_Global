//
//  IXKLineDBMgr.m
//  FXApp
//
//  Created by Seven on 2017/8/7.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "IXKLineDBMgr.h"
#import "IXKLineSql.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMResultSet.h"
#import "NSDate+IX.h"
#import "IXTradeDataCache.h"
#import "IxProtoKlineRepair.pbobjc.h"
#import "IxItemKlineRepair.pbobjc.h"

@implementation IXKLineDBMgr

//自建串行队列，用于数据库写操作
dispatch_queue_t kLineCacheQueue;
static  IXKLineDBMgr    *instance;

+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[IXKLineDBMgr alloc] init];
        kLineCacheQueue = dispatch_queue_create("com.ixdigit.fx.klineCache", DISPATCH_QUEUE_SERIAL);
        [instance reisterObserver];
        [IXKLineDBMgr createDB];
        [IXKLineDBMgr cleanOldData];
    });
    return instance;
}

- (void)reisterObserver
{
    //注册数据废弃监听
    IXTradeData_listen_regist(self, KLINE_CMD_REPARE_NOTIFY);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (IXTradeData_isSameKey(keyPath, KLINE_CMD_REPARE_NOTIFY)) {
        proto_kline_repair_list * list = ((IXTradeDataCache *)object).pb_kLine_repare_noti;
        NSArray * repareArr = list.klineRepairArray;
        [IXKLineDBMgr discardDataWith:repareArr];
    }
}

/* 废弃k线数据 */
+ (void)discardDataWith:(NSArray <item_kline_repair *>*)itemArray
{
    for (int i = 0; i < itemArray.count; i++) {
        item_kline_repair   * item = itemArray[i];
        if (item.status == item_kline_repair_estatus_Normal) {
            //废弃数据操作
            NSString    * tableName = [IXKLineDBMgr tableNameWithMetaType:item.type];
            [self db_discardDataWithTable:tableName symbolId:item.symbolid sTime:item.startTime eTime:item.endTime];
        }
    }
}

/* 废弃某一时间段内的数据 */
+ (void)db_discardDataWithTable:(NSString *)table
                       symbolId:(int64_t)id_p
                          sTime:(int64_t)sTime
                          eTime:(int64_t)eTime
{
    if (sTime > eTime) {
        return;
    }
    
    FMDatabase  * db = [self db];
    //1、找到eTime的前驱，将其preTime更新为-1
    //2、删除sTime～eTime之间的数据
    NSString    * sql = [IXKLineSql sql_selectMateWithTable:table symbolId:id_p preTime:eTime];
    IXMetaData  * meta = [self execuSelectWith:sql db:db];
    
    if (meta) {
        sql = [IXKLineSql sql_updatePreTimeWithTable:table symbold:id_p metaTime:meta.time preTime:-1];
        BOOL    success = [self execuUpdateWithSql:sql db:db];
        if (!success) {
            //此处更新失败不影响后面数据读写
            ELog(@"重置数据后继节点时间失败 -----@_@--");
        }
    } else {
        sql = [IXKLineSql sql_selectMetaWithTable:table symbolId:id_p afterTime:eTime];
        meta = [self execuSelectWith:sql db:db];
        if (meta && meta.pre_time != -1) {
            sql = [IXKLineSql sql_updatePreTimeWithTable:table symbold:id_p metaTime:meta.time preTime:-1];
            BOOL    success = [self execuUpdateWithSql:sql db:db];
            if (!success) {
                //此处更新失败不影响后面数据读写
                ELog(@"重置数据后继节点时间失败 -----#_#--");
            }
        }
    }
    
    sql = [IXKLineSql sql_deleteTable:table symbolId:id_p startTime:sTime endTime:eTime];
    BOOL    success = [self execuUpdateWithSql:sql db:db];
    if (!success) {
        NSLog(@"废弃数据失败");
    }
    
    [db close];
}


// -------------------------------------------------------------------------------------------------
// ---------------------------------------- public 类方法 -------------------------------------------
// -------------------------------------------------------------------------------------------------

/**
 获取k线缓存数据，若返回值为nil，则根据RAM中最后一条k线数据的时间请求
 
 @param id_p 产品id
 @param type k线类型
 @patam time GMT0时间（单位：s）
 @return [数据数组 or nil]
 */
+ (NSArray <IXMetaData*>*)db_getKLineDataWithSymbolId:(int64_t)id_p
                                            kLineType:(K_TYPE)type
                                          befomreTime:(int64_t)time
{
    NSInteger count = kMaxRecord;
    if (type == Week_1) {
        count = kWeekRecord;
    } else if (type == Month_1) {
        count = kMonthRecord;
    }
    
    FMDatabase  * db = [self db];
    NSString    * tableName = [self tableNameWithMetaType:type];
    NSMutableArray  * aimArr = [@[] mutableCopy];
   
    NSString    * sql = [IXKLineSql sql_selectMetaWithTable:tableName
                                                   symbolId:id_p
                                                 beforeTime:time];
    IXMetaData  * meta = [self execuSelectWith:sql db:db];
    if (meta) {
        if (![self needRequestWithMeta:meta endTime:time]) {
            time = meta.time;
        } else {
            [db close];
            return nil;
        }
    } else {
        [db close];
        return nil;
    }
    
    for (int i = 0; i < count; i++) {
        sql = [IXKLineSql sql_selectMetaWithTable:tableName symbolId:id_p time:time];
        IXMetaData  * meta = [self execuSelectWith:sql db:db];
        if (!meta || meta.pre_time == -1) {
            break;
        } else {
            [aimArr addObject:meta];
            time = meta.pre_time;
        }
    }
    
    if (aimArr.count == count || count == kMonthRecord) {
        [db close];
        return aimArr;
    }
    
    [db close];
    return nil;
}


/**
 存储行情数据
 
 @param id_p 产品id
 @param metaArr 行情数组
 */
+ (void)db_saveDataWithSymbolId:(int64_t)id_p metaArray:(NSArray <IXMetaData *>*)metaArr
{
    dispatch_async(kLineCacheQueue, ^{
        FMDatabase  * db = [self db];
        [db beginTransaction];
        NSMutableArray  * aimArray = [self dealWithMetaArray:metaArr];
        //1、处理时间最远的那条数据，查询该条数据是否已经存储
        //通过symbolId,time作为查询条件，查询该条数据是否已经存在
        //symbolID = id_p & time = time
        IXMetaData  * meta = [aimArray lastObject];
        if (meta.id_p != id_p) {
            ELog(@"产品id不对应，已取消k线数据存储操作，请检查roomLG及FXCSEngine是否能正常释放");
            return ;
        }
        
        NSString    * tableName = [self tableNameWithMetaType:meta.type];
        NSString    * sql = [IXKLineSql sql_selectMetaWithTable:tableName
                                                       symbolId:id_p
                                                           time:meta.time];
        IXMetaData  * selectMeta = [self execuSelectWith:sql db:db];
        
        if (selectMeta) {
            meta.pre_time = selectMeta.pre_time;
        }
        
        //2、处理时间最近的那条数据
        //通过symbolId,time作为查询条件，查询该条数据的前驱是否已经存在
        //symbolID = id_p & pre_time = time
        meta = [aimArray firstObject];
        sql = [IXKLineSql sql_selectMateWithTable:tableName
                                         symbolId:id_p
                                          preTime:meta.time];
        selectMeta = [self execuSelectWith:sql db:db];
        if (!selectMeta) {
            //3、处理时间最近的那条数据
            //通过symbolId、time作为查询条件，查询大于time的记录中time最小的记录
            //symbolId = id_p & time > time & limit 1 order by time desc
            sql = [IXKLineSql sql_selectMetaWithTable:tableName
                                             symbolId:id_p
                                            afterTime:meta.time];
            selectMeta = [self execuSelectWith:sql db:db];
            
            if (selectMeta) {
                //如果存在则认为其前驱存在
                selectMeta.pre_time = meta.time;
                sql = [IXKLineSql sql_insertTable:tableName];
                sql = [IXKLineSql sql_updatePreTimeWithTable:tableName
                                                     symbold:id_p
                                                    metaTime:selectMeta.time
                                                     preTime:meta.time];
                BOOL updateSuccess = [self execuUpdateWithSql:sql db:db];
                if (!updateSuccess) {
                    ELog(@" -- 更新前驱失败 -- ");
                }
            }
        }
        
        //for循环入库
        for (int i = 0; i < aimArray.count; i++) {
            sql = [IXKLineSql sql_insertTable:tableName];
            [self execuUpdateWithSql:sql meta:aimArray[i] isymbolId:id_p db:db];
        }
        [db commit];
        [db close];
    });
}


/** 删除sTime和eTime之间的数据 */
+ (void)db_deleteTable:(NSString *)table startTime:(int64_t)sTime endTime:(int64_t)eTime
{
    //1、先处理右端点，将大于eTime的
}


// -------------------------------------------------------------------------------------------------
// ------------------------------------------- 私有类方法 --------------------------------------------
// -------------------------------------------------------------------------------------------------

+ (void)createDB
{
    FMDatabase  * db = [self db];
    NSArray     * tableArr = [self tableArray];
    
    for (NSString * table in tableArr) {
        if (![db tableExists:table]) {
            NSString    * sql = [IXKLineSql sql_createTable:table];
            [db executeUpdate:sql];
        }
    }
    [db close];
}

+ (void)cleanOldData
{
    dispatch_async(kLineCacheQueue, ^{
        NSDate      * date = [NSDate dateWithTimeIntervalSinceNow:-3600*24*7];
        NSInteger   time = [date timeIntervalSince1970];
        
        FMDatabase  * db = [IXKLineDBMgr db];
        [db beginTransaction];
        
        NSArray * tableArr = [self tableArray];
        
        for (int i = 0; i < tableArr.count ; i++) {
            NSString    * table = tableArr[i];
            if (SameString(table, klt_day)) {
                //日k删除15年前的数据
                time = time - 3600*24*365*15;
            }

            NSString    * sql = [IXKLineSql sql_deleteTable:table beforeTime:time];
            BOOL success = [db executeUpdate:sql];
            if (!success) {
                ELog(@" **** 批量删除k线缓存旧数据失败 **** ");
            }
        }
        
        [db commit];
        [db close];
    });
}


// -------------------------------------------------------------------------------------------------
// ------------------------------------------ help method ------------------------------------------
// -------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark - help method


+ (IXMetaData *)execuSelectWith:(NSString *)sql db:(FMDatabase *)db
{
    FMResultSet * set = [db executeQuery:sql];
    IXMetaData  * meta = nil;
    
    while ([set next]) {
        meta = [IXMetaData new];
        meta.offset = [set intForColumn:klp_offset];
        meta.count = [set intForColumn:klp_count];
        meta.total = [set intForColumn:klp_total];
        meta.seq   = [set longForColumn:klp_seq];
        meta.id_p = [set longForColumn:klp_idp];
        meta.type = [set intForColumn:klp_KLineType];
        meta.close = [set doubleForColumn:klp_close];
        meta.open = [set doubleForColumn:klp_open];
        meta.high = [set doubleForColumn:klp_high];
        meta.low = [set doubleForColumn:klp_low];
        meta.amt = [set longForColumn:klp_amt];
        meta.vol = [set longForColumn:klp_vol];
        meta.time = [set longForColumn:klp_time];
        meta.pre_time = [set longForColumn:klp_preTime];
        meta.digit = [set intForColumn:klp_digit];
    }
    return meta;
}

+ (BOOL)execuUpdateWithSql:(NSString *)sql meta:(IXMetaData *)meta isymbolId:(int64_t)id_p db:(FMDatabase *)db
{
    return [db executeUpdateWithFormat:sql,
            meta.type,id_p,meta.time,meta.pre_time,meta.high,meta.low,meta.close,
            meta.open,meta.amt,meta.vol,meta.id_p,meta.total,meta.offset,meta.count,
            meta.seq,meta.digit,@" - ",0];
}

+ (BOOL)execuUpdateWithSql:(NSString *)sql db:(FMDatabase *)db
{
    return [db executeUpdate:sql];
}

//处理行情数组，将后一个行情的time赋给前一个行情的preTime，最后一个行情的preTime设置为-1
+ (NSMutableArray *)dealWithMetaArray:(NSArray <IXMetaData *>*)array
{
    NSMutableArray  * aimAarr = [@[] mutableCopy];
    
    if (array.count == 0) {
        IXMetaData  * meta = [array firstObject];
        meta.pre_time = -1;
        [aimAarr addObject:meta];
    } else {
        for (int i = 0; i < array.count-1; i++) {
            IXMetaData  * meta = array[i];
            IXMetaData  * latterMeta = array[i+1];
            meta.pre_time = latterMeta.time;
            
            [aimAarr addObject:meta];
            
            if (i == array.count-2) {
                latterMeta.pre_time = -1;
                [aimAarr addObject:latterMeta];
            }
        }
    }
    
    return aimAarr;
}

+ (NSString *)tableNameWithMetaType:(K_TYPE)type
{
    switch (type) {
        case MIN_1:
            return klt_1min;
        case MIN_5:
            return klt_5min;
        case MIN_15:
            return klt_15min;
        case MIN_30:
            return klt_30min;
        case HOUR_1:
            return klt_60min;
        case HOUR_4:
            return klt_hour4;
        case DAY:
        case Week_1:
        case Month_1:
            return klt_day;
        default:
            return @"unknown";
    }
}

+ (BOOL)needRequestWithMeta:(IXMetaData *)meta endTime:(NSInteger)time
{
    if (time == 0) {
        time = [[NSDate date] timeIntervalSince1970];
    }
    
    switch (meta.type) {
        case MIN_1:
            return meta.time/60 != time/60;
        case MIN_5:
            return meta.time/300 != time/300;
        case MIN_15:
            return meta.time/900 != time/900;
        case MIN_30:
            return meta.time/1800 != time/1800;
        case HOUR_1:
            return meta.time/3600 != time/3600;
        case HOUR_4:
            return meta.time/14400 != time/14400;
        case DAY:
            return ![NSDate isInSameDay:meta.time time2:time];
        case Week_1:
            return ![NSDate isInSameWeek:meta.time time2:time];
        case Month_1:
            return ![NSDate isInSameMonth:meta.time time2:time];
        default:
            return YES;
            break;

    }
}

#pragma mark -
#pragma mark - other

+ (NSString *)dbPath
{
    NSString    * path = [NSString stringWithFormat:@"%@%@",NSHomeDirectory(),kldb_cachePath];
    BOOL    isDirectory = YES;
    BOOL    exist = [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory];
    
    if (!exist) {
        NSError * error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&error];
        if (error) {
            NSLog(@" ---- 创建数据库路径失败 \n error : %@ \n path:%@----",error,path);
        }
    }
    return [path stringByAppendingString:kldb_name];
}

+ (FMDatabase *)db
{
    NSString    * fixKey = @"fixDigitBug";
    NSString    * path = [self dbPath];
    
    id  record = [[NSUserDefaults standardUserDefaults] objectForKey:fixKey];
    if (nil == record) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:NULL];
        [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:fixKey];
        NSLog(@" -- %s --",__func__);
    }
    
    FMDatabase  * db = [FMDatabase databaseWithPath:path];
    
    if (![db open]) {
        [db open];
    }
    
    return db;
}

+ (NSArray *)tableArray
{
    return @[klt_1min,klt_5min,klt_15min,klt_30min,klt_60min,klt_day,klt_hour4];
}

@end

