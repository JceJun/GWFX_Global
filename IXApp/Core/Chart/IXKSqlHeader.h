//
//  IXKSqlHeader.h
//  FXApp
//
//  Created by Seven on 2017/8/7.
//  Copyright © 2017年 wsz. All rights reserved.
//

#ifndef IXKSqlHeader_h
#define IXKSqlHeader_h

// ------------------------------------------------------------------------------
// ---------------------------- 数据库路径相关 -------------------------------------
// ------------------------------------------------------------------------------
static  NSString    * kldb_cachePath    = @"/Library/KLineCache/";
static  NSString    * kldb_name         = @"KLineDB";

// ------------------------------------------------------------------------------
// ---------------------------- 数据库table名称 -----------------------------------
// ------------------------------------------------------------------------------
static  NSString    * klt_1min  = @"min1";
static  NSString    * klt_5min  = @"min5";
static  NSString    * klt_15min = @"min15";
static  NSString    * klt_30min = @"min30";
static  NSString    * klt_60min = @"hour";
static  NSString    * klt_hour4 = @"hour4";
static  NSString    * klt_day   = @"day";


// ------------------------------------------------------------------------------
// ---------------------------- 数据库table字段 -----------------------------------
// ------------------------------------------------------------------------------
static  NSString    * klp_KLineType = @"kLineType";
static  NSString    * klp_symbolId  = @"symbolId";
static  NSString    * klp_time      = @"time";
static  NSString    * klp_preTime   = @"preTime";
static  NSString    * klp_high      = @"high";
static  NSString    * klp_low       = @"low";
static  NSString    * klp_close     = @"close";
static  NSString    * klp_open      = @"open";
static  NSString    * klp_amt       = @"amt";
static  NSString    * klp_vol       = @"vol";
static  NSString    * klp_idp       = @"id_p";
static  NSString    * klp_total     = @"total";
static  NSString    * klp_offset    = @"offset";
static  NSString    * klp_count     = @"count";
static  NSString    * klp_seq       = @"seq";
static  NSString    * klp_digit     = @"digit";
static  NSString    * klp_reservedText  = @"reservedText"; //保留字段（TEXT类型）
static  NSString    * klp_reservedNum   = @"reservedNum";  //保留字段（int类型）


#endif /* IXKSqlHeader_h */
