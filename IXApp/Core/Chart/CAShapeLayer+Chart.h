//
//  CAShapeLayer+Chart.h
//  IXApp
//
//  Created by Seven on 2017/8/30.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

#define kLineWidth 1.f

@interface CAShapeLayer (Chart)

//虚线
+ (instancetype)dashLayerWithP:(CGPoint)p1
                            p2:(CGPoint)p2
                       dashArr:(NSArray *)dash
                     lineWidth:(CGFloat)width
                   strokeColor:(UIColor *)color1;
//实线
+ (instancetype)layerWithP:(CGPoint)p1
                        p2:(CGPoint)p2
                 lineWidth:(CGFloat)width
               strokeColor:(UIColor *)color1;

+ (instancetype)layerWithRect:(CGRect)rect
                    lineWidth:(CGFloat)width
                  strokeColor:(UIColor *)color1
                    fillColor:(UIColor *)color2;

+ (instancetype)layerWithRect:(CGRect)rect
                         topY:(CGFloat)topY
                        btomY:(CGFloat)btomY
                    lineWidth:(CGFloat)width
                  strokeColor:(UIColor *)color1
                    fillColor:(UIColor *)color2;

+ (instancetype)layerWithPath:(UIBezierPath *)path
                    lineWidth:(CGFloat)width
                  strokeColor:(UIColor *)color1
                    fillColor:(UIColor *)color2;

+ (instancetype)layerWithPath:(UIBezierPath *)path
                  strokeColor:(UIColor *)color
                    lineWidth:(CGFloat)width;

@end
