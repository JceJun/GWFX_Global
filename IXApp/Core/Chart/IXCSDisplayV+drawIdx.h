//
//  IXCSDisplayV+drawIdx.h
//  IXApp
//
//  Created by Magee on 2017/3/29.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXCSDisplayV.h"

@interface IXCSDisplayV (drawIdx)

#pragma mark - 主指标绘制
- (void)draw_M_MA:  (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_M_BBI: (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_M_BOLL:(NSInteger)cnt  width:(CGFloat)width;
- (void)draw_M_MIKE:(NSInteger)cnt  width:(CGFloat)width;
- (void)draw_M_PBX: (NSInteger)cnt  width:(CGFloat)width;

#pragma mark - 副指标绘制
- (void)draw_S_MACD:(NSInteger)cnt  width:(CGFloat)width bodyWidth:(CGFloat)bodyWidth;
- (void)draw_S_ARBR:(NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_ATR: (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_BIAS:(NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_CCI: (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_DKBY:(NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_KD:  (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_KDJ: (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_LWR: (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_QHLSR:(NSInteger)cnt width:(CGFloat)width;
- (void)draw_S_RSI: (NSInteger)cnt  width:(CGFloat)width;
- (void)draw_S_WR:  (NSInteger)cnt  width:(CGFloat)width;

#pragma mark - 主指标Text绘制
- (void)draw_M_MA_T:  (NSInteger)idx;
- (void)draw_M_BBI_T: (NSInteger)idx;
- (void)draw_M_BOLL_T:(NSInteger)idx;
- (void)draw_M_MIKE_T:(NSInteger)idx;
- (void)draw_M_PBX_T: (NSInteger)idx;

#pragma mark - 副指标Text绘制
- (void)draw_S_MACD_T:(NSInteger)idx;
- (void)draw_S_ARBR_T:(NSInteger)idx;
- (void)draw_S_ATR_T: (NSInteger)idx;
- (void)draw_S_BIAS_T:(NSInteger)idx;
- (void)draw_S_CCI_T: (NSInteger)idx;
- (void)draw_S_DKBY_T:(NSInteger)idx;
- (void)draw_S_KD_T:  (NSInteger)idx;
- (void)draw_S_KDJ_T: (NSInteger)idx;
- (void)draw_S_LWR_T: (NSInteger)idx;
- (void)draw_S_QHLSR_T:(NSInteger)idx;
- (void)draw_S_RSI_T: (NSInteger)idx;
- (void)draw_S_WR_T:  (NSInteger)idx;



@end
