//
//  UIBezierPath+Chart.m
//  FXApp
//
//  Created by Seven on 2017/10/12.
//  Copyright © 2017年 wsz. All rights reserved.
//

#import "UIBezierPath+Chart.h"

@implementation UIBezierPath (Chart)

+ (instancetype)bezierPathWithPoints:(NSArray *)points
{
    UIBezierPath    * path = [UIBezierPath bezierPath];
    
    for (int i = 0; i < points.count ; i++) {
        CGPoint p = [points[i] CGPointValue];
        if (i == 0) {
            [path moveToPoint:p];
        } else {
            [path addLineToPoint:p];
        }
    }
    
    return path;
}

@end
