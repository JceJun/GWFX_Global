//
//  IXCSDisplayV+index.h
//  KLine
//
//  Created by Magee on 16/12/1.
//  Copyright © 2016年 wsz. All rights reserved.
//

#import "IXCSDisplayV.h"

@interface IXCSDisplayV (index)

#pragma mark - 主图指标
- (void)load_M_MA;
- (void)load_M_BBI;
- (void)load_M_BOLL;
- (void)load_M_MIKE;
- (void)load_M_PBX;

#pragma mark - 副图指标
- (void)load_S_MACD;
- (void)load_S_ARBR;
- (void)load_S_ATR;
- (void)load_S_BIAS;
- (void)load_S_CCI;
- (void)load_S_DKBY;
- (void)load_S_KD;
- (void)load_S_KDJ;
- (void)load_S_LWR;
- (void)load_S_QHLSR;
- (void)load_S_RSI;
- (void)load_S_WR;

@end
