//
//  IXUserInfoMgr+Setting.m
//  IXApp
//
//  Created by Seven on 2017/8/23.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUserInfoMgr+Setting.h"
#import "IXUserInfoMgr_Setting.h"
#import "IXDateUtils.h"

NSString * const kSettingRedNoti = @"ixSettingRedNoti";
NSString * const kSettingLotUnitNoti = @"ixSettingLotUnitNoti";

static  NSString    * kNightModeEnable = @"isNightMode";
static  NSString    * kSoundAlertEnable = @"soundAlertEnable";
static  NSString    * kShakeAlertEnable = @"shakeAlertEnable";
static  NSString    * kNormalLightEnable = @"settingNormalLight";
static  NSString    * kDomAlertEnable = @"kIXDomTip";
static  NSString    * kisNormalDeal = @"SubSymbolCellStyle";
static  NSString    * kSettingRed = @"COLORSETTING";
static  NSString    * kIsLotUnit = @"unitSettingOption";
static  NSString    * kSystemLang = @"localizationlanguage";
static  NSString    * kSystemTimeZone = @"GTSTimeZone";

@interface IXUserInfoMgr ()
{
    BOOL    _nightModeEnable;
    BOOL    _soundAlertEnable;   //声音提醒
    BOOL    _shakeAlertEnable;   //震动提醒
    BOOL    _normalLightEnable;  //常亮
    BOOL    _domDealAlsertEnable; //dom交易提醒
    BOOL    _isNormalDeal;   //是否为普通交易模式
    BOOL    _isSeettingRed;  //是否为红涨绿跌
    BOOL    _isLotUnit;  //单位是否为手数
    NSString    * _systemLang;       //语言设置
    NSString    * _systemTimeZone;   //时区设置
}

@end

@implementation IXUserInfoMgr (Setting)
/** 初始化用户设置 */
- (void)configUserSetting
{
    NSUserDefaults  * defaults = [NSUserDefaults standardUserDefaults];
    _nightModeEnable    = [defaults boolForKey:kNightModeEnable];
    _soundAlertEnable   = [defaults boolForKey:kSoundAlertEnable];
    _shakeAlertEnable   = [defaults boolForKey:kShakeAlertEnable];
    _normalLightEnable  = [defaults boolForKey:kNormalLightEnable];
    _domDealAlsertEnable= [defaults boolForKey:kDomAlertEnable];
    _isNormalDeal   = [defaults boolForKey:kisNormalDeal];
    _isSeettingRed  = [defaults boolForKey:kSettingRed];
    _isLotUnit      = [defaults boolForKey:kIsLotUnit];
    _systemLang     = [defaults objectForKey:kSystemLang];
    _systemTimeZone = [defaults objectForKey:kSystemTimeZone];
    
    if (!_systemTimeZone.length) {
        _systemTimeZone = [NSString stringWithFormat:@"GMT%@",_systemTimeZone];
    } else {
        _systemTimeZone = [IXDateUtils getDefaultTimeZone];
    }
    
    if (!_systemLang.length) {
        NSString * currentLan = [[NSLocale preferredLanguages] firstObject];
        NSArray * arr = [currentLan componentsSeparatedByString:@"-"];
        if (arr.count > 2) {
            currentLan = [NSString stringWithFormat:@"%@-%@",arr[0],arr[1]];
        }
        
        if ([currentLan containsString:@"zh-Han"]) {
            _systemLang = @"简体中文";
        }else{
            _systemLang = @"English";
        }
    }
}


#pragma mark -
#pragma mark - setter

//夜间模式
- (void)setNightModeEnable:(BOOL)nightModeEnable
{
    _nightModeEnable = nightModeEnable;
    [[NSUserDefaults standardUserDefaults] setBool:nightModeEnable forKey:kNightModeEnable];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [IXUserInfoMgr confitNightMode];
}

//声音提醒
- (void)setSoundAlertEnable:(BOOL)soundAlertEnable
{
    _soundAlertEnable = soundAlertEnable;
    [[NSUserDefaults standardUserDefaults] setBool:soundAlertEnable forKey:kSoundAlertEnable];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (soundAlertEnable) {
        [[IXUserInfoMgr shareInstance] showSoundAlert];
    }
}

//震动提醒
- (void)setShakeAlertEnable:(BOOL)shakeAlertEnable
{
    _shakeAlertEnable = shakeAlertEnable;
    [[NSUserDefaults standardUserDefaults] setBool:shakeAlertEnable forKey:kShakeAlertEnable];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (shakeAlertEnable) {
        [[IXUserInfoMgr shareInstance] showShakeAlert];
    }
}

//屏幕常亮
- (void)setNormalLightEnable:(BOOL)normalLightEnable
{
    _normalLightEnable = normalLightEnable;
    [[NSUserDefaults standardUserDefaults] setBool:normalLightEnable forKey:kNormalLightEnable];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //设置屏幕常亮
    [[UIApplication sharedApplication] setIdleTimerDisabled:normalLightEnable];
}

//普通交易／正常交易
- (void)setIsNormalDeal:(BOOL)isNormalDeal
{
    _isNormalDeal = isNormalDeal;
    [[NSUserDefaults standardUserDefaults] setBool:isNormalDeal forKey:kisNormalDeal];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//设置红涨绿跌
- (void)setIsSeettingRed:(BOOL)isSeettingRed
{
    _isSeettingRed = isSeettingRed;
    [[NSUserDefaults standardUserDefaults] setBool:isSeettingRed forKey:kSettingRed];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSettingRedNoti object:@(isSeettingRed)];
}

//dom交易提醒
- (void)setDomDealAlsertEnable:(BOOL)domDealAlsertEnable
{
    _domDealAlsertEnable = domDealAlsertEnable;
    [[NSUserDefaults standardUserDefaults] setBool:domDealAlsertEnable forKey:kDomAlertEnable];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//交易单位（YES - 手数／NO - 数量）
- (void)setIsLotUnit:(BOOL)isLotUnit
{
    _isLotUnit = isLotUnit;
    [[NSUserDefaults standardUserDefaults] setBool:isLotUnit forKey:kIsLotUnit];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSettingLotUnitNoti object:@(isLotUnit)];
}

//app语言
- (void)setSystemLang:(NSString *)systemLang
{
    _systemLang = systemLang;
    [[NSUserDefaults standardUserDefaults] setObject:systemLang forKey:kSystemLang];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//交易时区
- (void)setSystemTimeZone:(NSString *)systemTimeZone
{
    _systemTimeZone = [NSString stringWithFormat:@"GMT%@",systemTimeZone];
    [[NSUserDefaults standardUserDefaults] setObject:systemTimeZone forKey:kSystemLang];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/** 切换夜间模式 */
+ (void)confitNightMode
{
    if ([IXUserInfoMgr shareInstance].nightModeEnable) {
        [[DKNightVersionManager sharedManager] nightFalling];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }else{
        [[DKNightVersionManager sharedManager] dawnComing];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
}

/** 设置屏幕常亮 */
+ (void)configScreenNormalLight
{
    BOOL disable = [UIApplication sharedApplication].idleTimerDisabled;
    if ([IXUserInfoMgr shareInstance].normalLightEnable && !disable) {
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    }
    else if (![IXUserInfoMgr shareInstance].normalLightEnable && disable) {
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

/** 打开／关闭相册和相机时处理屏幕常亮 */
+ (void)dealStatusBarWithAlbumState:(BOOL)open
{
    if ([IXUserInfoMgr shareInstance].nightModeEnable) {
        if (open) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }else{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
    }
}



//// ------------------ 声音／震动提示 -----------------
///** 系统声音，附带震动 */
//+ (void)showSoundAlert
//{
//    
//}
//
///** 仅震动 */
//+ (void)showShakeAlert
//{
//    
//}
//
///** 声音+震动 */
//+ (void)showSoundAndShakeAlert
//{
//    
//}

@end
