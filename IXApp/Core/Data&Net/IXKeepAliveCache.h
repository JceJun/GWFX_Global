//
//  IXKeepAliveCache.h
//  IXApp
//
//  Created by Magee on 16/11/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXKeepAliveCache : NSObject

+ (IXKeepAliveCache *)shareInstance;

- (NSData *)tradeTickData;

- (void)increaseQuoteSeqNo;
- (void)resetQuoteSeqNo;
- (NSData *)quoteLoginData;
- (NSData *)quoteTickData;

- (long)getSeqNo;

@end
