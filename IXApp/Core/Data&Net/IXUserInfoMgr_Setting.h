//
//  IXUserInfoMgr_Setting.h
//  IXApp
//
//  Created by Seven on 2017/8/23.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUserInfoMgr.h"

@interface IXUserInfoMgr ()

@property (nonatomic, assign) BOOL  nightModeEnable;
@property (nonatomic, assign) BOOL  soundAlertEnable;   //声音提醒
@property (nonatomic, assign) BOOL  shakeAlertEnable;   //震动提醒
@property (nonatomic, assign) BOOL  normalLightEnable;  //常亮
@property (nonatomic, assign) BOOL  isNormalDeal;   //是否为普通交易模式
@property (nonatomic, assign) BOOL  isSeettingRed;  //是否为红涨绿跌
@property (nonatomic, assign) BOOL  domDealAlsertEnable; //dom交易提醒
@property (nonatomic, assign) BOOL  isLotUnit;  //单位是否为手数
@property (nonatomic, copy) NSString    * systemLang;       //语言设置
@property (nonatomic, copy) NSString    * systemTimeZone;   //时区设置

@end
