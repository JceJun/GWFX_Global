//
//  IXTCPConnect.h
//  Communivation
//
//  Created by Bill on 16/10/25.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXTCPType.h"

@class GCDAsyncSocket;
@class Reachability;

@protocol IXTCPConnectDelegate <NSObject>

- (void)IXTCPConnectStatus:(IXTCPConnectStatus)status
                serverType:(IXTCPServerType)type;

- (void)IXTCPConnectRuote:(IXTCPServerRouteType)route
               serverType:(IXTCPServerType)type;

- (void)IXTCPConnectDidReadData:(NSData *)data
                     serverType:(IXTCPServerType)type;
@end

@interface IXTCPConnect : NSObject

//服务器路线
@property (nonatomic,assign) IXTCPServerRouteType routeType;

//连接状态
@property (nonatomic,assign,readonly) IXTCPConnectStatus connectStatus;

//代理
@property (nonatomic, assign) id <IXTCPConnectDelegate> delegate;

/**
 *  初始化
 *
 *  @param  serverType TCP类型
 *  @param  delegate   代理对象
 */
- (id)initWithSeverType:(IXTCPServerType)serverType delegate:(id)delegate;
 
/**
 *  连接服务器
 *
 */
- (void)connectWithRouteType:(IXTCPServerRouteType)type;

/**
 *  断开连接
 */
- (void)disConnect;


/**
 切换线路
 */
- (void)swiSever;

/**
 *  发送数据
 *
 *  @param data    数据
 *  @param timeOut 超时
 *  @param tag     标记
 */
- (void)sendData:(NSData *)data withTimeOut:(NSTimeInterval)timeOut tag:(uint16_t)tag;

@end
