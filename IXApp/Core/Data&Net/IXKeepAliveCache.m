//
//  IXKeepAliveCache.m
//  IXApp
//
//  Created by Magee on 16/11/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXKeepAliveCache.h"
#import "IXTradeDataFilter+process.h"
#import "IXCrc16.h"

#define kQuoteTickInitialValue 30001

static short quoteTickNo;

@interface IXKeepAliveCache ()
{
    proto_user_keepalive *_proto;
}

@end

@implementation IXKeepAliveCache

+ (IXKeepAliveCache *)shareInstance
{
    static IXKeepAliveCache *share;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!share) {
            share = [[IXKeepAliveCache alloc] init];
        }
    });
    return share;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self resetQuoteSeqNo];
    }
    return self;
}

#pragma mark - 
#pragma mark - trade

- (NSData *)tradeTickData
{
    if ([IXUserInfoMgr shareInstance].userLogInfo.token.length &&
        [IXUserInfoMgr shareInstance].userLogInfo.account.userid!= 0 &&
        [IXUserInfoMgr shareInstance].userLogInfo.account.id_p != 0){
        
        if(!_proto){
            _proto = [[proto_user_keepalive alloc] init];
        }
        _proto.token = [IXUserInfoMgr shareInstance].userLogInfo.token;
        _proto.type = [IXUserInfoMgr shareInstance].itemType;
        _proto.accountid = [IXUserInfoMgr shareInstance].userLogInfo.account.id_p;
        
        return [IXTradeDataFilter packetWithPBMsg:_proto Command:CMD_USER_KEEPALIVE];
    }
    return nil;
}

#pragma mark - 
#pragma mark - quote

- (long)getSeqNo
{
    return quoteTickNo++;
}

- (void)resetQuoteSeqNo
{
    quoteTickNo = kQuoteTickInitialValue;
}

- (void)increaseQuoteSeqNo
{
    quoteTickNo++;
}

- (NSData *)quoteLoginData
{
    //head
    proto_header head;
    memset(&head, 0, HEADER_LENGTH);
    head.cmd = htons(CMD_QUOTE_LOGIN);
    head.seq = htonl([[IXKeepAliveCache shareInstance] getSeqNo]);
    NSData *head_data = [NSData dataWithBytes:&head length:HEADER_LENGTH];
    
    //body
    proto_quote_login_req *req = [[proto_quote_login_req alloc] init];
    req.userName = @"test";
    req.passwd   = @"111111";
    NSString *ver = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSMutableString *muti = [NSMutableString stringWithString:ver];
    [muti replaceOccurrencesOfString:@"." withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, muti.length)];
    req.version = [muti integerValue];
    NSData *body_data = [req data];
    
    //merge
    uint16_t lenth = head_data.length + body_data.length;
    
    char *tmp = (char *)malloc(lenth);
    memset(tmp, 0, lenth);
    [head_data getBytes:tmp range:NSMakeRange(0, head_data.length)];
    [body_data getBytes:tmp + head_data.length range:NSMakeRange(0, body_data.length)];
    
    uint16_t lenth_tmp = htons(lenth);
    memcpy(tmp, &lenth_tmp, 2);
    
    uint16_t crc   = [IXCrc16 crc16:tmp length:lenth];
    crc = htons(crc);
    memcpy(tmp + 4, &crc, 2);
    
    NSData *rst_data = [NSData dataWithBytes:tmp length:lenth];
    free(tmp);
    return rst_data;
}

- (NSData *)quoteTickData
{
    //head
    proto_header head;
    memset(&head, 0, HEADER_LENGTH);
    head.cmd = htons(CMD_QUOTE_HB);
    head.seq = htonl([[IXKeepAliveCache shareInstance] getSeqNo]);
    NSData *head_data = [NSData dataWithBytes:&head length:HEADER_LENGTH];
    
    //body
    proto_quote_hb *req = [[proto_quote_hb alloc] init];
    req.time = time(NULL);
    NSData *body_data = [req data];
    
    //merge
    uint16_t lenth = head_data.length + body_data.length;

    char *tmp = (char *)malloc(lenth);
    memset(tmp, 0, lenth);
    [head_data getBytes:tmp range:NSMakeRange(0, head_data.length)];
    [body_data getBytes:tmp + head_data.length range:NSMakeRange(0, body_data.length)];
    
    uint16_t lenth_tmp = htons(lenth);
    memcpy(tmp, &lenth_tmp, 2);
    
    uint16_t crc   = [IXCrc16 crc16:tmp length:lenth];
    crc = htons(crc);
    memcpy(tmp + 4, &crc, 2);
    
    NSData *rst_data = [NSData dataWithBytes:tmp length:lenth];
    free(tmp);
    return rst_data;
}

@end
