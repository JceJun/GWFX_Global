//
//  IXUserInfoMgr+Setting.h
//  IXApp
//
//  Created by Seven on 2017/8/23.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXUserInfoMgr.h"

extern NSString * const kSettingRedNoti;
extern NSString * const kSettingLotUnitNoti;

@interface IXUserInfoMgr (Setting)

/** 初始化用户设置 */
- (void)configUserSetting;
/** 切换夜间模式 */
+ (void)confitNightMode;
/** 设置屏幕常亮 */
+ (void)configScreenNormalLight;
/** 打开／关闭相册和相机时处理屏幕常亮 */
+ (void)dealStatusBarWithAlbumState:(BOOL)open;



//// ------------------ 声音／震动提示 -----------------
///** 系统声音，附带震动 */
//+ (void)showSoundAlert;
///** 仅震动 */
//+ (void)showShakeAlert;
///** 声音+震动 */
//+ (void)showSoundAndShakeAlert;

@end
