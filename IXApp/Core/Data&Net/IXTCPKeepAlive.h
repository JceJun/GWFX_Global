//
//  IXTCPKeepAlive.h
//  IXApp
//
//  Created by Magee on 16/11/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXTCPType.h"

@protocol IXTCPKeepAliveDelegate <NSObject>

/**
 *  心跳包回调
 *
 *  @param   data 心跳包
 *  @param   type 心跳包类型
 */
- (void)IXTCPKeepAliveTickWithData:(NSData *)data
                         aliveType:(IXTCPServerType)type;

/**
 *  心跳终止回调
 */
- (void)IXTCPKeepAliveDidStopWithAliveType:(IXTCPServerType)type;
 
@end

@interface IXTCPKeepAlive : NSObject

/**
 *  回调指针
 */
@property (nonatomic,assign) id<IXTCPKeepAliveDelegate>delegate;

/**
 *  初始化
 *
 *  @param aDelegate    回调指针
 */
- (id)initWithDelegate:(id)delegate keepAliveType:(IXTCPServerType)type;

/**
 *  开启心跳
 */
- (void)startKeepAliveTick;

/**
 *  停止心跳
 */
- (void)stopKeepAliveTick;

@end
