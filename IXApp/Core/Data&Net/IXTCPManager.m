//
//  IXTCPManager.m
//  IXApp
//
//  Created by bob on 16/11/3.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTCPManager.h"
#import "IXTCPConnect.h"
#import "IXCpyConfig.h"

#define InternalTime  2

@interface IXTCPManager ()<IXTCPConnectDelegate>

@property (nonatomic,strong) IXTCPConnect *quoteTcpConnect;
@property (nonatomic,strong) IXTCPConnect *tradeTcpConnect;
@property (nonatomic,strong) IXTCPConnect *spareTcpConnect;

//服务器路线
@property (nonatomic,assign) IXTCPServerRouteType routeType;
@property (nonatomic,assign) BOOL                 isPing;

@property (nonatomic,strong) dispatch_source_t connectMainTimer;

@end

@implementation IXTCPManager

- (id)init{
    self = [super init];
    if (self) {
        self.quoteTcpConnect = [[IXTCPConnect alloc] initWithSeverType:IXTCPServerTypeQuote
                                                              delegate:(id)self];
        
        self.tradeTcpConnect = [[IXTCPConnect alloc] initWithSeverType:IXTCPServerTypeTrade
                                                              delegate:(id)self];
        
        self.spareTcpConnect = [[IXTCPConnect alloc] initWithSeverType:IXTCPServerTypeSpare
                                                              delegate:(id)self];

        _routeType = IXTCPServerRouteMain;
        [self reconnectLogic];
    }
    return self;
}

- (void)reconnectLogic
{
    _connectMainTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(0, 0));
    dispatch_source_set_timer(_connectMainTimer, dispatch_walltime(NULL, 0),InternalTime * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_connectMainTimer, ^{
        DLog(@"当前线路为备用线路，尝试连接主线路...");
        [self.spareTcpConnect connectWithRouteType:IXTCPServerRouteMain];
    });
}

- (NSString *)curLine
{
    NSString *result = @"";
    
    BOOL disConnect = (self.tradeTcpConnect.connectStatus == IXTCPConnectStatusDisconnect);
    BOOL maxTrix = (self.tradeTcpConnect.connectStatus == IXTCPConnectStatusMaxTried);
    
    if(disConnect || maxTrix){
        result = LocalizedString(@"无可用线路");
    }else{
        result = (_routeType != IXTCPServerRouteMain) ? LocalizedString(@"备用线路")  : LocalizedString(@"主线路");
    }
    return result;
}

- (void)connectWithSeverType:(IXTCPServerType)type
{
    switch (type) {
        case IXTCPServerTypeQuote:{
            if (_quoteTcpConnect.connectStatus != IXTCPConnectStatusConnected) {
                [_quoteTcpConnect connectWithRouteType:_routeType];
            }
        }
            break;
        case IXTCPServerTypeTrade:{
            if (_tradeTcpConnect.connectStatus != IXTCPConnectStatusConnected) {
                [_tradeTcpConnect connectWithRouteType:_routeType];
            }
        }
            break;
        default:
            break;
    }
}

- (void)disConnectWithSeverType:(IXTCPServerType)type
{
    switch (type) {
        case IXTCPServerTypeQuote:{
            [_quoteTcpConnect disConnect];
        }
            break;
        case IXTCPServerTypeTrade:{
            [_tradeTcpConnect disConnect];
            [IXUserInfoMgr shareInstance].userLogInfo.token = nil;
        }
            break;
        default:
            break;
    }
}
 
- (void)sendData:(NSData *)data severType:(IXTCPServerType)type
{
    switch (type) {
        case IXTCPServerTypeQuote:{
            [_quoteTcpConnect sendData:data withTimeOut:-1 tag:0];
        }
            break;
        case IXTCPServerTypeTrade:{
            [_tradeTcpConnect sendData:data withTimeOut:-1 tag:0];
        }
            break;
        default:
            break;
    }
}


#pragma mark -
#pragma mark - IXTCPConnectDelegate
- (void)IXTCPConnectDidReadData:(NSData *)data serverType:(IXTCPServerType)type
{
    if (type != IXTCPServerTypeSpare) {
        if ([self.delegate respondsToSelector:@selector(IXTCPManagerDidReadData:serverType:)]){
            [self.delegate IXTCPManagerDidReadData:data serverType:type];
        }
    }
}

- (void)IXTCPConnectStatus:(IXTCPConnectStatus)status serverType:(IXTCPServerType)type
{
    if (type != IXTCPServerTypeSpare) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(IXTCPManagerRealTimeServerType:serverStatus:)]){
                [self.delegate IXTCPManagerRealTimeServerType:type serverStatus:status];
            }
        });      
    }else{
        //备用线路的连接状态
        if(status == IXTCPConnectStatusConnected){
            DLog(@"ping线路成功，结束ping操作...");
            if (_isPing) {
                _isPing = NO;
                [self.spareTcpConnect disConnect];
                dispatch_suspend(_connectMainTimer);
            }
            
            _routeType = IXTCPServerRouteMain;
            [self.tradeTcpConnect swiSever];
            [self.quoteTcpConnect swiSever];
        }else{
            DLog(@"ping线路正在连接...");
        }
    }
    
    if(IXTCPServerTypeTrade == type){
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyChangeLine object:nil];
        });
    }
}

- (void)IXTCPConnectRuote:(IXTCPServerRouteType)route serverType:(IXTCPServerType)type
{
    if (type != IXTCPServerTypeSpare) {
        _routeType = route;
        DLog(@"切换线路到：%@",@(_routeType));
        if (_routeType != IXTCPServerRouteMain) {
            if (!_isPing) {
                _isPing = YES;
                dispatch_resume(_connectMainTimer);
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyChangeLine object:nil];
        });
    }else{
        NSString *errMsg = [NSString stringWithFormat:@"ping服务触发了线路切换方法，当前线路:%d",_routeType];
        ELog(errMsg);
    }
}

@end
