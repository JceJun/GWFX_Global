//
//  IXTCPConnect.m
//  Communivation
//
//  Created by Bill on 16/10/25.
//  Copyright © 2016年 Bill. All rights reserved.
//

#import "IXTCPConnect.h"
#import "GCDAsyncSocket.h"
#import "IXTCPKeepAlive.h"
#import "Reachability.h"
#import "IXTCPType.h"
#import "IXCpyConfig.h"

#define SERVERMAXCOUNT 2
@interface IXTCPConnect()
<
GCDAsyncSocketDelegate,
IXTCPKeepAliveDelegate
>
{
    dispatch_queue_t  _tcpQueue;
    
    __block NSInteger _reconnectCount;
    int               serverCount;
    
    BOOL              _reconnectRunning;
    BOOL              _isManualDisCnt;
    BOOL              _isSwiServer;
    BOOL              _netCgeNeedRec;
}

@property (nonatomic,assign) IXTCPServerType serverType;    //服务器类型
@property (nonatomic,strong) GCDAsyncSocket *asyncSocket;   //socket实例
@property (nonatomic,strong) IXTCPKeepAlive *keepAlive;     //心跳管理
@property (nonatomic,strong) Reachability *hostReach;       //网络监听

@property (nonatomic,strong) dispatch_source_t reconnectTimer;

@end

@implementation IXTCPConnect

- (id)initWithSeverType:(IXTCPServerType)serverType delegate:(id)delegate
{
    if (self = [super init]) {
        _isSwiServer = NO;
        _netCgeNeedRec = NO;
        
        self.delegate = delegate;
        self.serverType = serverType;
        
        if (serverType == IXTCPServerTypeSpare) {
            _routeType = IXTCPServerRouteSpare;
        }else{
            _routeType = IXTCPServerRouteMain;
            [self reconnectLogic];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(reachabilityChanged:)
                                                         name:kReachabilityChangedNotification
                                                       object:nil];
 
            self.keepAlive = [[IXTCPKeepAlive alloc] initWithDelegate:self
                                                        keepAliveType:_serverType];
        }
        
        if (serverType==IXTCPServerTypeQuote) {
            _tcpQueue = dispatch_queue_create("IXTCPConnect_tcpQuoteQueue", DISPATCH_QUEUE_SERIAL);
        }else if(serverType==IXTCPServerTypeTrade){
            _tcpQueue = dispatch_queue_create("IXTCPConnect_tcpTradeQueue", DISPATCH_QUEUE_SERIAL);
        }else if(serverType==IXTCPServerTypeSpare){
            _tcpQueue = dispatch_queue_create("IXTCPConnect_tcpSpareQueue", DISPATCH_QUEUE_SERIAL);
        }
        
        self.asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self
                                                      delegateQueue:_tcpQueue];
        
        if (serverType != IXTCPServerTypeSpare) {
            [self.hostReach startNotifier];
        }
    }
    return self;
}

- (Reachability *)hostReach
{
    NSString *hostName = @"";
    if (_serverType==IXTCPServerTypeQuote) {
        hostName = (_routeType == IXTCPServerRouteMain) ? QuoteSeverIP :SpareQServerIP;
        _hostReach = [Reachability reachabilityWithHostName:hostName];
        _hostReach.tag = (NSInteger)IXTCPServerTypeQuote;
    } else if(_serverType==IXTCPServerTypeTrade){
        hostName = (_routeType == IXTCPServerRouteMain) ? TradeSeverIP :SpareTServerIP;
        _hostReach = [Reachability reachabilityWithHostName:hostName];
        _hostReach.tag = (NSInteger)IXTCPServerTypeTrade;
    }else if (_serverType==IXTCPServerTypeSpare){
        hostName = TradeSeverIP;
        _hostReach = [Reachability reachabilityWithHostName:hostName];
        _hostReach.tag = (NSInteger)IXTCPServerTypeSpare;
    }
    return _hostReach;
}
 
- (void)setConnectStatus:(IXTCPConnectStatus)connectStatus
{
    _connectStatus = connectStatus;
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(IXTCPConnectStatus:serverType:)]) {
            [self.delegate IXTCPConnectStatus:_connectStatus serverType:_serverType];
        }
    });
} 

- (void)connectWithRouteType:(IXTCPServerRouteType)type
{
    dispatch_async(_tcpQueue, ^{
        _routeType = type;
        if (_connectStatus != IXTCPConnectStatusConneting) {
            self.connectStatus = IXTCPConnectStatusConneting;
            [_asyncSocket connectToHost:[self getHostName]
                                 onPort:[self getHostPort]
                                  error:nil];
        }
    });
}

- (NSString *)getHostName
{
    NSString *hostName = @"";
    if (self.serverType==IXTCPServerTypeQuote) {
        hostName = [self getProperIPWithAddress:QuoteSeverIP port:QuoteSeverPort];
        if (_routeType == IXTCPServerRouteSpare) {
            hostName = [self getProperIPWithAddress:SpareQServerIP port:SpareQServerPort];
        }
    }else {
        hostName = [self getProperIPWithAddress:TradeSeverIP port:TradeSeverPort];
        if (_routeType == IXTCPServerRouteSpare) {
            hostName = [self getProperIPWithAddress:SpareTServerIP port:SpareTServerPort];
        }
    }
    return hostName;
}

- (NSInteger)getHostPort
{
    NSInteger hostport = -1;
    if (self.serverType==IXTCPServerTypeQuote) {
        hostport = QuoteSeverPort;
        if (_routeType == IXTCPServerRouteSpare) {
            hostport = SpareQServerPort;
        }
    }else {
        hostport = TradeSeverPort;
        if (_routeType == IXTCPServerRouteSpare) {
            hostport = SpareTServerPort;
        }
    }
    return hostport;
}

- (void)swiSever
{
    dispatch_async(_tcpQueue, ^{
        if (_routeType != IXTCPServerRouteMain) {
            _routeType = IXTCPServerRouteMain;
            _isSwiServer = YES;
            [self stopReconnect];
            [_asyncSocket disconnect];
            [_keepAlive stopKeepAliveTick];
            self.connectStatus = IXTCPConnectStatusDisconnect;
            [self connectWithRouteType:IXTCPServerRouteMain];
        }else{
            if (_connectStatus != IXTCPConnectStatusConnected ||
                _connectStatus != IXTCPConnectStatusConneting) {
                [self connectWithRouteType:IXTCPServerRouteMain];
            }
        }
    });
}

- (void)disConnect
{
    dispatch_async(_tcpQueue, ^{
        _isManualDisCnt = YES;
       [_keepAlive stopKeepAliveTick];
       [_asyncSocket disconnect];
    });
} 
 
- (void)sendData:(NSData *)data withTimeOut:(NSTimeInterval)timeOut tag:(uint16_t)tag
{
    [_asyncSocket writeData:data withTimeout:timeOut tag:tag];
    [_asyncSocket readDataWithTimeout:timeOut tag:tag];
}

/**
 线路切换的逻辑:如果网络需要重连，每次都会从主服务器开始重连
 切换到连接主服务器，5s * 3次;
 切换到连接备用服务器，10s * 3次;
 如果当前为备用服务器，则10s * 10次，心跳主服务器；10次均成功则切换到主服务器；如果有一次失败则重新计时；
 */
- (void)reconnectLogic
{
    unsigned int cycle = 0;
    unsigned int count = 0;
    if (_serverType == IXTCPServerTypeQuote) {
        count = IXTCPQuoteReconnectCount;
        if (_routeType == IXTCPServerRouteMain) {
            cycle = IXTCPQuoteReconnectCycle;
        }else{
            cycle = IXTCPSQuoteReconnectCycle;
        }
    }else{
        count = IXTCPTradeReconnectCount;
        if (_routeType == IXTCPServerRouteMain) {
            cycle = IXTCPTradeReconnectCycle;
        }else{
            cycle = IXTCPSTradeReconnectCycle;
        }
    }

    _reconnectTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, _tcpQueue);
    dispatch_source_set_timer(_reconnectTimer, dispatch_walltime(NULL, 0),cycle * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_reconnectTimer, ^{
        if (serverCount < SERVERMAXCOUNT) {
            if (_reconnectCount < count) {
                self.connectStatus = IXTCPConnectStatusConneting;
                [self reconnect];
            } else {
                [self timerDisconnect];
            }
        }else{
            [self stopReconnect];
            self.connectStatus = IXTCPConnectStatusMaxTried;
        }
    });
}

- (void)reconnect
{
    dispatch_async(_tcpQueue, ^{
        _reconnectCount++;
        if (![_hostReach isReachable]) {
            DLog(@"<%@重连：第%ld次重连,网络不可用--------------------->\n",[self severStr],(long)_reconnectCount);
        } else {
            if (self.connectStatus != IXTCPConnectStatusConnected) {
                [_asyncSocket disconnect];
                [_asyncSocket connectToHost:[self getHostName]
                                     onPort:[self getHostPort]
                                      error:nil];
                DLog(@"<%@重连：第%ld次重连------------------------------>\n",[self severStr],(long)_reconnectCount);
            }
        }
    });
}


/**
 开启重连
 */
- (void)startReconnect
{
    dispatch_async(_tcpQueue, ^{
        if (!_reconnectRunning) {
            _reconnectRunning = YES;
            [_keepAlive stopKeepAliveTick];
            if (_connectStatus != IXTCPConnectStatusConneting) {
                self.connectStatus = IXTCPConnectStatusConneting;
                _reconnectCount = 0;
            }
            DLog(@"%@开始重连...",[self severStr]);
            dispatch_resume(_reconnectTimer);
        }else{
            self.connectStatus = IXTCPConnectStatusConneting;
            DLog(@"%@正在重连...",[self severStr]);
        }
    });
}

/**
 重连超出最大次数
 */
- (void)timerDisconnect
{
    dispatch_async(_tcpQueue, ^{
        //如果是备用线路则不循环
        if (_routeType == IXTCPServerRouteSpare) {
            serverCount = SERVERMAXCOUNT;
        }else{
            serverCount++;
        }
        _reconnectCount = 0;
        if (serverCount < SERVERMAXCOUNT) {
            _routeType = (_routeType == IXTCPServerRouteMain) ? IXTCPServerRouteSpare: IXTCPServerRouteMain;
            [self hostReach];
            if ([self.delegate respondsToSelector:@selector(IXTCPConnectRuote:serverType:)]) {
                [self.delegate IXTCPConnectRuote:_routeType serverType:_serverType];
            }
            
            NSString *routeTypeStr = @"主线路";
            if (_routeType == IXTCPServerRouteSpare) {
                routeTypeStr = @"备用线路";
            }
            _isSwiServer = NO;
            DLog(@"<%@重连：已经超出最大重试次数，切换到%@------------->\n",[self severStr],routeTypeStr);
        }
    });
}

/**
 结束重连
 */
- (void)stopReconnect
{
    dispatch_async(_tcpQueue, ^{
        if (_reconnectRunning) {
            _reconnectRunning = NO;
            serverCount = 0;
            _isSwiServer = NO;
            dispatch_suspend(_reconnectTimer);
        }
    });
}

- (NSString *)severStr
{
    if (_serverType==IXTCPServerTypeSpare) {
        return @"ping服务";
    }
    return _serverType==IXTCPServerTypeQuote?@"行情TCP":@"交易TCP";
}

#pragma mark -
#pragma mark - Reachability response
- (void)reachabilityChanged:(NSNotification *)notify
{
    Reachability *curReach = [notify object];
    DLog(@"%@,recive reachabilityChanged...",[self severStr]);
    if(curReach.tag!=_serverType)return;
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    dispatch_async(_tcpQueue, ^{
        if (NotReachable != status) {
            DLog(@"网络已连通...");
            [self startReconnect];
        } else {
            DLog(@"网络已断开...");
            _netCgeNeedRec = YES;
            [self stopReconnect];
            [self disConnect];
            [_keepAlive stopKeepAliveTick];
        }
    });
}



#pragma mark -
#pragma mark - GCDAsyncSocketDelegate
- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    dispatch_async(_tcpQueue, ^{
        DLog(@"<%@断开连接------------------------------------>\n",[self severStr]);
        [_keepAlive stopKeepAliveTick];
        
        self.connectStatus = IXTCPConnectStatusDisconnect;
        if (_serverType != IXTCPServerTypeSpare) {
            if (!_isSwiServer) {
                if(!_isManualDisCnt || _netCgeNeedRec){
                    [self startReconnect];
                }else{
                    if (!_isManualDisCnt && !_netCgeNeedRec) {
                        //网络断开，未检测到变化
                        ELog(@"网络断开，未检测到变化...");
                        _netCgeNeedRec = YES;
                        [self stopReconnect];
                        [self disConnect];
                        [_keepAlive stopKeepAliveTick];
                    }else{
                        self.connectStatus = IXTCPConnectStatusMaxTried;
                        ELog(@"网络断开，没有重连...");
                    }
                }
            }else{
                _isSwiServer = NO;
                ELog(@"切换了服务器...");
            }
        }else{
            ELog(@"ping服务器断开连接...");
        }
        _isManualDisCnt = NO;
    });
}

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
    dispatch_async(_tcpQueue, ^{
        DLog(@"<%@成功连接------------------------------------>\n",[self severStr]);
        _netCgeNeedRec = NO;
        _isManualDisCnt = NO;
        if (_serverType != IXTCPServerTypeSpare) {
            [self stopReconnect];
        }
        self.connectStatus = IXTCPConnectStatusConnected;
        [_keepAlive startKeepAliveTick];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (_serverType == IXTCPServerTypeTrade) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"needRelogin" object:nil];
            }else if(_serverType == IXTCPServerTypeQuote){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"needRefresh" object:nil];
            }
        });
    });
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    if ([self.delegate respondsToSelector:@selector(IXTCPConnectDidReadData:serverType:)]) {
        [self.delegate IXTCPConnectDidReadData:data serverType:_serverType];
    }
    [sock readDataWithTimeout:-1 tag:tag];
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
    return -1;
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutWriteWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
    return -1;
}

#pragma mark -
#pragma mark - IXTCPKeepAliveDelegate
- (void)IXTCPKeepAliveTickWithData:(NSData *)data aliveType:(IXTCPServerType)type
{
    [self sendData:data withTimeOut:-1 tag:0];
    DLog(@"<%@心跳--------------------------------------->\n",[self severStr]);
}

- (void)IXTCPKeepAliveDidStopWithAliveType:(IXTCPServerType)type
{
    DLog(@"<%@结束心跳------------------------------------>\n",[self severStr]);
}

- (NSString *)getProperIPWithAddress:(NSString *)ipAddr port:(UInt32)port
{
    NSError *addresseError = nil;
    NSArray *addresseArray = [GCDAsyncSocket lookupHost:ipAddr
                                                   port:port
                                                  error:&addresseError];
    NSString *ipv6Addr = @"";
    for (NSData *addrData in addresseArray) {
        if ([GCDAsyncSocket isIPv6Address:addrData]) {
            ipv6Addr = [GCDAsyncSocket hostFromAddress:addrData];
        }
    }
    
    if (ipv6Addr.length == 0) {
        ipv6Addr = ipAddr;
    }
    return ipv6Addr;
}

@end
