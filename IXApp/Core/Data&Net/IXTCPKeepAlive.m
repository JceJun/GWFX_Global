//
//  IXTCPKeepAlive.m
//  IXApp
//
//  Created by Magee on 16/11/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTCPKeepAlive.h"
#import "IxProtoUser.pbobjc.h"
#import "IXKeepAliveCache.h"

@interface IXTCPKeepAlive ()
{
    BOOL  _isRunning;
}

@property (nonatomic,assign) IXTCPServerType aliveType;
@property (nonatomic,strong) dispatch_source_t tickTimer;

@end

@implementation IXTCPKeepAlive

- (id)initWithDelegate:(id)delegate keepAliveType:(IXTCPServerType)type
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.aliveType = type;
        
        [self initTickTimer];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(startKeepAliveTick)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];

    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initTickTimer
{
    _tickTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER,
                                        0,
                                        0,
                                        dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    
    dispatch_source_set_timer(_tickTimer,
                              dispatch_walltime(NULL, 0),
                              IXKeepAliveQuoteSourceTimerCycle*NSEC_PER_SEC,
                              IXKeepAliveQuoteSourceTimerLeeway);
    
    dispatch_source_set_event_handler(_tickTimer, ^{
        [self requestKeepAlive];
    });
}

//发送连接心跳
- (void)requestKeepAlive
{
    NSData *data = nil;
    if (_aliveType == IXTCPServerTypeQuote) {
        data = [[IXKeepAliveCache shareInstance] quoteTickData];
    } else {
        data = [[IXKeepAliveCache shareInstance] tradeTickData];
    }
    if (data) {
        if([self.delegate respondsToSelector:@selector(IXTCPKeepAliveTickWithData:aliveType:)])
            [self.delegate IXTCPKeepAliveTickWithData:data aliveType:_aliveType];
    }
}

//开启心跳计时
- (void)startKeepAliveTick
{
    if (_isRunning) return;
    _isRunning = YES;
    dispatch_resume(_tickTimer);
}

//关闭心跳计时
- (void)stopKeepAliveTick
{    
    if (!_isRunning) return;
    _isRunning = NO;
    dispatch_suspend(_tickTimer);
    
    if ([self.delegate respondsToSelector:@selector(IXTCPKeepAliveDidStopWithAliveType:)])
        [self.delegate IXTCPKeepAliveDidStopWithAliveType:_aliveType];
}

@end
