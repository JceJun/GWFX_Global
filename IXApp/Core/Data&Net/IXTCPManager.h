 //
//  IXTCPManager.h
//  IXApp
//
//  Created by bob on 16/11/3.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXTCPType.h"

@protocol IXTCPManagerDelegate <NSObject>

- (void)IXTCPManagerDidReadData:(NSData *)data
                     serverType:(IXTCPServerType)serverType;

- (void)IXTCPManagerRealTimeServerType:(IXTCPServerType)serverType
                          serverStatus:(IXTCPConnectStatus)serverStatus;

@end

@interface IXTCPManager : NSObject

//代理指针
@property (nonatomic,assign) id<IXTCPManagerDelegate>delegate;

/**
 *  连接服务器
 */
- (void)connectWithSeverType:(IXTCPServerType)type;

/**
 *  断开连接服务器
 */
- (void)disConnectWithSeverType:(IXTCPServerType)type;
 
/**
 *  发送数据
 *
 *  @param data
 */
- (void)sendData:(NSData *)data severType:(IXTCPServerType)type;


/**
 获取线路类型

 @return 线路类型
 */
- (NSString *)curLine;

@end
