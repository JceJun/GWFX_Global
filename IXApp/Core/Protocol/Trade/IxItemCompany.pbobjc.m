// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_company.proto

#import "GPBProtocolBuffers_RuntimeSupport.h"
#import "IxItemCompany.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma mark - IxItemCompanyRoot

@implementation IxItemCompanyRoot

@end

static GPBFileDescriptor *IxItemCompanyRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_company

@implementation item_company

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic name;
@dynamic timezone;
@dynamic messageUser;
@dynamic signature;
@dynamic companyToken;
@dynamic status;

typedef struct item_company_Storage {
  uint32_t _has_storage_[1];
  uint32_t timezone;
  item_company_estatus status;
  NSString *name;
  NSString *messageUser;
  NSString *signature;
  NSString *companyToken;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
} item_company_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .number = item_company_FieldNumber_Id_p,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(item_company_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "uuid",
        .number = item_company_FieldNumber_Uuid,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(item_company_Storage, uuid),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "uutime",
        .number = item_company_FieldNumber_Uutime,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeFixed64,
        .offset = offsetof(item_company_Storage, uutime),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "name",
        .number = item_company_FieldNumber_Name,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(item_company_Storage, name),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "timezone",
        .number = item_company_FieldNumber_Timezone,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_company_Storage, timezone),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "messageUser",
        .number = item_company_FieldNumber_MessageUser,
        .hasIndex = 5,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(item_company_Storage, messageUser),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "signature",
        .number = item_company_FieldNumber_Signature,
        .hasIndex = 6,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(item_company_Storage, signature),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "companyToken",
        .number = item_company_FieldNumber_CompanyToken,
        .hasIndex = 7,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(item_company_Storage, companyToken),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "status",
        .number = item_company_FieldNumber_Status,
        .hasIndex = 8,
        .flags = GPBFieldOptional | GPBFieldHasEnumDescriptor,
        .type = GPBTypeEnum,
        .offset = offsetof(item_company_Storage, status),
        .defaultValue.valueEnum = item_company_estatus_Normal,
        .typeSpecific.enumDescFunc = item_company_estatus_EnumDescriptor,
        .fieldOptions = NULL,
      },
    };
    static GPBMessageEnumDescription enums[] = {
      { .enumDescriptorFunc = item_company_estatus_EnumDescriptor },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[item_company class]
                                              rootClass:[IxItemCompanyRoot class]
                                                   file:IxItemCompanyRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:enums
                                              enumCount:sizeof(enums) / sizeof(GPBMessageEnumDescription)
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(item_company_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

int32_t item_company_Status_RawValue(item_company *message) {
  GPBDescriptor *descriptor = [item_company descriptor];
  GPBFieldDescriptor *field = [descriptor fieldWithNumber:item_company_FieldNumber_Status];
  return GPBGetInt32IvarWithField(message, field);
}

void Setitem_company_Status_RawValue(item_company *message, int32_t value) {
  GPBDescriptor *descriptor = [item_company descriptor];
  GPBFieldDescriptor *field = [descriptor fieldWithNumber:item_company_FieldNumber_Status];
  GPBSetInt32IvarWithFieldInternal(message, field, value, descriptor.file.syntax);
}

#pragma mark - Enum item_company_estatus

GPBEnumDescriptor *item_company_estatus_EnumDescriptor(void) {
  static GPBEnumDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageEnumValueDescription values[] = {
      { .name = "Normal", .number = item_company_estatus_Normal },
      { .name = "Deleted", .number = item_company_estatus_Deleted },
    };
    static const char *extraTextFormatInfo = "\002\000&\000\001\'\000";
    descriptor = [GPBEnumDescriptor allocDescriptorForName:GPBNSStringifySymbol(item_company_estatus)
                                                   values:values
                                               valueCount:sizeof(values) / sizeof(GPBMessageEnumValueDescription)
                                             enumVerifier:item_company_estatus_IsValidValue
                                      extraTextFormatInfo:extraTextFormatInfo];
  }
  return descriptor;
}

BOOL item_company_estatus_IsValidValue(int32_t value__) {
  switch (value__) {
    case item_company_estatus_Normal:
    case item_company_estatus_Deleted:
      return YES;
    default:
      return NO;
  }
}


// @@protoc_insertion_point(global_scope)
