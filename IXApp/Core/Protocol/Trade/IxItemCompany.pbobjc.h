// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_company.proto

#import "GPBProtocolBuffers.h"

#if GOOGLE_PROTOBUF_OBJC_GEN_VERSION != 30000
#error This file was generated by a different version of protoc-gen-objc which is incompatible with your Protocol Buffer sources.
#endif

// @@protoc_insertion_point(imports)

CF_EXTERN_C_BEGIN

#pragma mark - Enum item_company_estatus

typedef GPB_ENUM(item_company_estatus) {
  item_company_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_company_estatus_Normal = 0,
  item_company_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_company_estatus_EnumDescriptor(void);

BOOL item_company_estatus_IsValidValue(int32_t value);


#pragma mark - IxItemCompanyRoot

@interface IxItemCompanyRoot : GPBRootObject

// The base class provides:
//   + (GPBExtensionRegistry *)extensionRegistry;
// which is an GPBExtensionRegistry that includes all the extensions defined by
// this file and all files that it depends on.

@end

#pragma mark - item_company

typedef GPB_ENUM(item_company_FieldNumber) {
  item_company_FieldNumber_Id_p = 1,
  item_company_FieldNumber_Uuid = 2,
  item_company_FieldNumber_Uutime = 3,
  item_company_FieldNumber_Name = 4,
  item_company_FieldNumber_Timezone = 6,
  item_company_FieldNumber_MessageUser = 7,
  item_company_FieldNumber_Signature = 8,
  item_company_FieldNumber_CompanyToken = 9,
  item_company_FieldNumber_Status = 10,
};

@interface item_company : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite, copy) NSString *name;

@property(nonatomic, readwrite) uint32_t timezone;

@property(nonatomic, readwrite, copy) NSString *messageUser;

@property(nonatomic, readwrite, copy) NSString *signature;

@property(nonatomic, readwrite, copy) NSString *companyToken;

@property(nonatomic, readwrite) item_company_estatus status;

@end

int32_t item_company_Status_RawValue(item_company *message);
void Setitem_company_Status_RawValue(item_company *message, int32_t value);

CF_EXTERN_C_END

// @@protoc_insertion_point(global_scope)
