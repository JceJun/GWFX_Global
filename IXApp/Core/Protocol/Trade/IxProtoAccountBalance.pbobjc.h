// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_account_balance.proto

#import "GPBProtocolBuffers.h"

#if GOOGLE_PROTOBUF_OBJC_GEN_VERSION != 30000
#error This file was generated by a different version of protoc-gen-objc which is incompatible with your Protocol Buffer sources.
#endif

// @@protoc_insertion_point(imports)

CF_EXTERN_C_BEGIN

@class item_account_balance;
@class item_header;


#pragma mark - IxProtoAccountBalanceRoot

@interface IxProtoAccountBalanceRoot : GPBRootObject

// The base class provides:
//   + (GPBExtensionRegistry *)extensionRegistry;
// which is an GPBExtensionRegistry that includes all the extensions defined by
// this file and all files that it depends on.

@end

#pragma mark - proto_account_balance_add

typedef GPB_ENUM(proto_account_balance_add_FieldNumber) {
  proto_account_balance_add_FieldNumber_Header = 1,
  proto_account_balance_add_FieldNumber_Id_p = 2,
  proto_account_balance_add_FieldNumber_Result = 3,
  proto_account_balance_add_FieldNumber_Comment = 4,
  proto_account_balance_add_FieldNumber_AccountBalance = 5,
};

@interface proto_account_balance_add : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@property(nonatomic, readwrite) BOOL hasAccountBalance;
@property(nonatomic, readwrite, strong) item_account_balance *accountBalance;

@end

#pragma mark - proto_account_balance_update

typedef GPB_ENUM(proto_account_balance_update_FieldNumber) {
  proto_account_balance_update_FieldNumber_Header = 1,
  proto_account_balance_update_FieldNumber_Id_p = 2,
  proto_account_balance_update_FieldNumber_Result = 3,
  proto_account_balance_update_FieldNumber_Comment = 4,
  proto_account_balance_update_FieldNumber_AccountBalance = 5,
};

@interface proto_account_balance_update : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@property(nonatomic, readwrite) BOOL hasAccountBalance;
@property(nonatomic, readwrite, strong) item_account_balance *accountBalance;

@end

#pragma mark - proto_account_balance_delete

typedef GPB_ENUM(proto_account_balance_delete_FieldNumber) {
  proto_account_balance_delete_FieldNumber_Header = 1,
  proto_account_balance_delete_FieldNumber_Id_p = 2,
  proto_account_balance_delete_FieldNumber_Result = 3,
  proto_account_balance_delete_FieldNumber_Comment = 4,
};

@interface proto_account_balance_delete : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@end

#pragma mark - proto_account_balance_get

typedef GPB_ENUM(proto_account_balance_get_FieldNumber) {
  proto_account_balance_get_FieldNumber_Header = 1,
  proto_account_balance_get_FieldNumber_Id_p = 2,
  proto_account_balance_get_FieldNumber_Result = 3,
  proto_account_balance_get_FieldNumber_Comment = 4,
  proto_account_balance_get_FieldNumber_AccountBalance = 5,
};

@interface proto_account_balance_get : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@property(nonatomic, readwrite) BOOL hasAccountBalance;
@property(nonatomic, readwrite, strong) item_account_balance *accountBalance;

@end

#pragma mark - proto_account_balance_list

typedef GPB_ENUM(proto_account_balance_list_FieldNumber) {
  proto_account_balance_list_FieldNumber_Header = 1,
  proto_account_balance_list_FieldNumber_Id_p = 2,
  proto_account_balance_list_FieldNumber_Offset = 3,
  proto_account_balance_list_FieldNumber_Count = 4,
  proto_account_balance_list_FieldNumber_Total = 5,
  proto_account_balance_list_FieldNumber_AccountBalanceArray = 6,
};

@interface proto_account_balance_list : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite) uint32_t total;

// |accountBalanceArray| contains |item_account_balance|
@property(nonatomic, readwrite, strong) NSMutableArray *accountBalanceArray;

@end

CF_EXTERN_C_END

// @@protoc_insertion_point(global_scope)
