// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_symbol_margin_set.proto

#import "GPBProtocolBuffers_RuntimeSupport.h"
#import "IxItemSymbolMarginSet.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma mark - IxItemSymbolMarginSetRoot

@implementation IxItemSymbolMarginSetRoot

@end

static GPBFileDescriptor *IxItemSymbolMarginSetRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_symbol_margin_set

@implementation item_symbol_margin_set

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic marginType;
@dynamic rangeLeft;
@dynamic rangeRight;
@dynamic percent;
@dynamic status;

typedef struct item_symbol_margin_set_Storage {
  uint32_t _has_storage_[1];
  uint32_t marginType;
  uint32_t rangeLeft;
  uint32_t rangeRight;
  uint32_t percent;
  uint32_t status;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
} item_symbol_margin_set_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .number = item_symbol_margin_set_FieldNumber_Id_p,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(item_symbol_margin_set_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "uuid",
        .number = item_symbol_margin_set_FieldNumber_Uuid,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(item_symbol_margin_set_Storage, uuid),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "uutime",
        .number = item_symbol_margin_set_FieldNumber_Uutime,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeFixed64,
        .offset = offsetof(item_symbol_margin_set_Storage, uutime),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "marginType",
        .number = item_symbol_margin_set_FieldNumber_MarginType,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_symbol_margin_set_Storage, marginType),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "rangeLeft",
        .number = item_symbol_margin_set_FieldNumber_RangeLeft,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_symbol_margin_set_Storage, rangeLeft),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "rangeRight",
        .number = item_symbol_margin_set_FieldNumber_RangeRight,
        .hasIndex = 5,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_symbol_margin_set_Storage, rangeRight),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "percent",
        .number = item_symbol_margin_set_FieldNumber_Percent,
        .hasIndex = 6,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_symbol_margin_set_Storage, percent),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "status",
        .number = item_symbol_margin_set_FieldNumber_Status,
        .hasIndex = 7,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_symbol_margin_set_Storage, status),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[item_symbol_margin_set class]
                                              rootClass:[IxItemSymbolMarginSetRoot class]
                                                   file:IxItemSymbolMarginSetRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(item_symbol_margin_set_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end


// @@protoc_insertion_point(global_scope)
