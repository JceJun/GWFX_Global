// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_symbol_sub_cata.proto

#import "GPBProtocolBuffers_RuntimeSupport.h"
#import "IxProtoSymbolSubCata.pbobjc.h"
#import "IxProtoHeader.pbobjc.h"
#import "IxItemSymbolSubCata.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma mark - IxProtoSymbolSubCataRoot

@implementation IxProtoSymbolSubCataRoot

+ (GPBExtensionRegistry*)extensionRegistry {
  // This is called by +initialize so there is no need to worry
  // about thread safety and initialization of registry.
  static GPBExtensionRegistry* registry = nil;
  if (!registry) {
    registry = [[GPBExtensionRegistry alloc] init];
    static GPBExtensionDescription descriptions[] = {
    };
    #pragma unused (descriptions)
    [registry addExtensions:[IxProtoHeaderRoot extensionRegistry]];
    [registry addExtensions:[IxItemSymbolSubCataRoot extensionRegistry]];
  }
  return registry;
}

@end

static GPBFileDescriptor *IxProtoSymbolSubCataRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - proto_symbol_sub_cata_add

@implementation proto_symbol_sub_cata_add

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasSymbolSubCata, symbolSubCata;

typedef struct proto_symbol_sub_cata_add_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_symbol_sub_cata *symbolSubCata;
  uint64_t id_p;
} proto_symbol_sub_cata_add_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_sub_cata_add_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_add_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_sub_cata_add_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_sub_cata_add_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_sub_cata_add_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_sub_cata_add_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_sub_cata_add_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_sub_cata_add_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolSubCata",
        .number = proto_symbol_sub_cata_add_FieldNumber_SymbolSubCata,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_add_Storage, symbolSubCata),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_sub_cata),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_sub_cata_add class]
                                              rootClass:[IxProtoSymbolSubCataRoot class]
                                                   file:IxProtoSymbolSubCataRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_sub_cata_add_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_sub_cata_update

@implementation proto_symbol_sub_cata_update

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasSymbolSubCata, symbolSubCata;

typedef struct proto_symbol_sub_cata_update_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_symbol_sub_cata *symbolSubCata;
  uint64_t id_p;
} proto_symbol_sub_cata_update_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_sub_cata_update_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_update_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_sub_cata_update_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_sub_cata_update_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_sub_cata_update_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_sub_cata_update_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_sub_cata_update_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_sub_cata_update_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolSubCata",
        .number = proto_symbol_sub_cata_update_FieldNumber_SymbolSubCata,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_update_Storage, symbolSubCata),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_sub_cata),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_sub_cata_update class]
                                              rootClass:[IxProtoSymbolSubCataRoot class]
                                                   file:IxProtoSymbolSubCataRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_sub_cata_update_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_sub_cata_delete

@implementation proto_symbol_sub_cata_delete

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic accountid;

typedef struct proto_symbol_sub_cata_delete_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  uint64_t id_p;
  uint64_t accountid;
} proto_symbol_sub_cata_delete_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_sub_cata_delete_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_delete_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_sub_cata_delete_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_sub_cata_delete_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_sub_cata_delete_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_sub_cata_delete_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_sub_cata_delete_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_sub_cata_delete_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "accountid",
        .number = proto_symbol_sub_cata_delete_FieldNumber_Accountid,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_sub_cata_delete_Storage, accountid),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_sub_cata_delete class]
                                              rootClass:[IxProtoSymbolSubCataRoot class]
                                                   file:IxProtoSymbolSubCataRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_sub_cata_delete_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_sub_cata_get

@implementation proto_symbol_sub_cata_get

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasSymbolSubCata, symbolSubCata;

typedef struct proto_symbol_sub_cata_get_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_symbol_sub_cata *symbolSubCata;
  uint64_t id_p;
} proto_symbol_sub_cata_get_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_sub_cata_get_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_get_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_sub_cata_get_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_sub_cata_get_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_sub_cata_get_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_sub_cata_get_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_sub_cata_get_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_sub_cata_get_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolSubCata",
        .number = proto_symbol_sub_cata_get_FieldNumber_SymbolSubCata,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_get_Storage, symbolSubCata),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_sub_cata),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_sub_cata_get class]
                                              rootClass:[IxProtoSymbolSubCataRoot class]
                                                   file:IxProtoSymbolSubCataRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_sub_cata_get_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_sub_cata_list

@implementation proto_symbol_sub_cata_list

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic offset;
@dynamic count;
@dynamic total;
@dynamic symbolSubCataArray;

typedef struct proto_symbol_sub_cata_list_Storage {
  uint32_t _has_storage_[1];
  uint32_t offset;
  uint32_t count;
  uint32_t total;
  item_header *header;
  NSMutableArray *symbolSubCataArray;
  uint64_t id_p;
} proto_symbol_sub_cata_list_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_sub_cata_list_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_list_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_sub_cata_list_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_sub_cata_list_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "offset",
        .number = proto_symbol_sub_cata_list_FieldNumber_Offset,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_sub_cata_list_Storage, offset),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "count",
        .number = proto_symbol_sub_cata_list_FieldNumber_Count,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_sub_cata_list_Storage, count),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "total",
        .number = proto_symbol_sub_cata_list_FieldNumber_Total,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_sub_cata_list_Storage, total),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolSubCataArray",
        .number = proto_symbol_sub_cata_list_FieldNumber_SymbolSubCataArray,
        .hasIndex = GPBNoHasBit,
        .flags = GPBFieldRepeated,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_sub_cata_list_Storage, symbolSubCataArray),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_sub_cata),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_sub_cata_list class]
                                              rootClass:[IxProtoSymbolSubCataRoot class]
                                                   file:IxProtoSymbolSubCataRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_sub_cata_list_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end


// @@protoc_insertion_point(global_scope)
