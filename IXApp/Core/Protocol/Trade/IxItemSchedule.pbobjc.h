// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_schedule.proto

#import "GPBProtocolBuffers.h"

#if GOOGLE_PROTOBUF_OBJC_GEN_VERSION != 30000
#error This file was generated by a different version of protoc-gen-objc which is incompatible with your Protocol Buffer sources.
#endif

// @@protoc_insertion_point(imports)

CF_EXTERN_C_BEGIN

#pragma mark - Enum item_schedule_estatus

typedef GPB_ENUM(item_schedule_estatus) {
  item_schedule_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_schedule_estatus_Normal = 0,
  item_schedule_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_schedule_estatus_EnumDescriptor(void);

BOOL item_schedule_estatus_IsValidValue(int32_t value);


#pragma mark - IxItemScheduleRoot

@interface IxItemScheduleRoot : GPBRootObject

// The base class provides:
//   + (GPBExtensionRegistry *)extensionRegistry;
// which is an GPBExtensionRegistry that includes all the extensions defined by
// this file and all files that it depends on.

@end

#pragma mark - item_schedule

typedef GPB_ENUM(item_schedule_FieldNumber) {
  item_schedule_FieldNumber_Id_p = 1,
  item_schedule_FieldNumber_Uuid = 2,
  item_schedule_FieldNumber_Uutime = 3,
  item_schedule_FieldNumber_DayOfWeek = 4,
  item_schedule_FieldNumber_StartTime = 5,
  item_schedule_FieldNumber_EndTime = 6,
  item_schedule_FieldNumber_MarginSet = 7,
  item_schedule_FieldNumber_Holiday = 8,
  item_schedule_FieldNumber_Enable = 9,
  item_schedule_FieldNumber_NonTradable = 10,
  item_schedule_FieldNumber_NeedStopout = 11,
  item_schedule_FieldNumber_CancelOrder = 12,
  item_schedule_FieldNumber_Reserve1 = 13,
  item_schedule_FieldNumber_CreateTime = 14,
  item_schedule_FieldNumber_CreateUserid = 15,
  item_schedule_FieldNumber_ModiTime = 16,
  item_schedule_FieldNumber_ModiUserid = 17,
  item_schedule_FieldNumber_Status = 18,
  item_schedule_FieldNumber_Reserve = 19,
  item_schedule_FieldNumber_ScheduleCataid = 20,
  item_schedule_FieldNumber_MarginType = 21,
};

@interface item_schedule : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite) uint32_t dayOfWeek;

@property(nonatomic, readwrite) int32_t startTime;

@property(nonatomic, readwrite) int32_t endTime;

@property(nonatomic, readwrite) uint32_t marginSet;

@property(nonatomic, readwrite) uint32_t holiday;

@property(nonatomic, readwrite) uint32_t enable;

@property(nonatomic, readwrite) uint32_t nonTradable;

@property(nonatomic, readwrite) uint32_t needStopout;

@property(nonatomic, readwrite) uint32_t cancelOrder;

@property(nonatomic, readwrite, copy) NSString *reserve1;

@property(nonatomic, readwrite) uint64_t createTime;

@property(nonatomic, readwrite) uint64_t createUserid;

@property(nonatomic, readwrite) uint64_t modiTime;

@property(nonatomic, readwrite) uint64_t modiUserid;

@property(nonatomic, readwrite, copy) NSString *reserve;

@property(nonatomic, readwrite) uint64_t scheduleCataid;

@property(nonatomic, readwrite) item_schedule_estatus status;

@property(nonatomic, readwrite) uint32_t marginType;

@end

int32_t item_schedule_Status_RawValue(item_schedule *message);
void Setitem_schedule_Status_RawValue(item_schedule *message, int32_t value);

CF_EXTERN_C_END

// @@protoc_insertion_point(global_scope)
