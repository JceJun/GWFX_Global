// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_account.proto

#import "GPBProtocolBuffers.h"

#if GOOGLE_PROTOBUF_OBJC_GEN_VERSION != 30000
#error This file was generated by a different version of protoc-gen-objc which is incompatible with your Protocol Buffer sources.
#endif

// @@protoc_insertion_point(imports)

CF_EXTERN_C_BEGIN

#pragma mark - Enum item_account_estatus

typedef GPB_ENUM(item_account_estatus) {
  item_account_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_estatus_Normal = 0,
  item_account_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_account_estatus_EnumDescriptor(void);

BOOL item_account_estatus_IsValidValue(int32_t value);

#pragma mark - Enum item_account_etype

typedef GPB_ENUM(item_account_etype) {
  item_account_etype_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_etype_Virtuals = 0,
  item_account_etype_Reality = 1,
};

GPBEnumDescriptor *item_account_etype_EnumDescriptor(void);

BOOL item_account_etype_IsValidValue(int32_t value);

#pragma mark - Enum item_account_eclear_negative

typedef GPB_ENUM(item_account_eclear_negative) {
  item_account_eclear_negative_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_eclear_negative_Checked = 0,
  item_account_eclear_negative_Clear = 1,
  item_account_eclear_negative_Nothing = 2,
};

GPBEnumDescriptor *item_account_eclear_negative_EnumDescriptor(void);

BOOL item_account_eclear_negative_IsValidValue(int32_t value);


#pragma mark - IxItemAccountRoot

@interface IxItemAccountRoot : GPBRootObject

// The base class provides:
//   + (GPBExtensionRegistry *)extensionRegistry;
// which is an GPBExtensionRegistry that includes all the extensions defined by
// this file and all files that it depends on.

@end

#pragma mark - item_account

typedef GPB_ENUM(item_account_FieldNumber) {
  item_account_FieldNumber_Id_p = 1,
  item_account_FieldNumber_Uuid = 2,
  item_account_FieldNumber_Uutime = 3,
  item_account_FieldNumber_Userid = 5,
  item_account_FieldNumber_AccountGroupid = 6,
  item_account_FieldNumber_Enable = 7,
  item_account_FieldNumber_CloseOnly = 8,
  item_account_FieldNumber_Balance = 9,
  item_account_FieldNumber_Credit = 10,
  item_account_FieldNumber_Bonus = 11,
  item_account_FieldNumber_Equity = 12,
  item_account_FieldNumber_Margin = 13,
  item_account_FieldNumber_MarginMaintenance = 14,
  item_account_FieldNumber_MarginStopout = 15,
  item_account_FieldNumber_FreeMargin = 16,
  item_account_FieldNumber_MarginLevel = 17,
  item_account_FieldNumber_VolumeMax = 18,
  item_account_FieldNumber_LastDealid = 20,
  item_account_FieldNumber_LastOrderTime = 21,
  item_account_FieldNumber_ActiveTime = 22,
  item_account_FieldNumber_Platform = 24,
  item_account_FieldNumber_AccountNo = 25,
  item_account_FieldNumber_FreeBalance = 26,
  item_account_FieldNumber_Frozen = 27,
  item_account_FieldNumber_Volumes = 28,
  item_account_FieldNumber_Positions = 29,
  item_account_FieldNumber_CompanyToken = 30,
  item_account_FieldNumber_Status = 31,
  item_account_FieldNumber_DelayLeft = 40,
  item_account_FieldNumber_DelayRight = 41,
  item_account_FieldNumber_Type = 42,
  item_account_FieldNumber_Lpuserid = 51,
  item_account_FieldNumber_ClearNegative = 52,
};

@interface item_account : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite) uint64_t userid;

@property(nonatomic, readwrite) uint64_t accountGroupid;

@property(nonatomic, readwrite) uint32_t enable;

@property(nonatomic, readwrite) BOOL closeOnly;

@property(nonatomic, readwrite) double balance;

@property(nonatomic, readwrite) double credit;

@property(nonatomic, readwrite) double bonus;

@property(nonatomic, readwrite) double equity;

@property(nonatomic, readwrite) double margin;

@property(nonatomic, readwrite) double marginMaintenance;

@property(nonatomic, readwrite) double marginStopout;

@property(nonatomic, readwrite) double freeMargin;

@property(nonatomic, readwrite) double marginLevel;

@property(nonatomic, readwrite) double volumeMax;

@property(nonatomic, readwrite) uint64_t lastDealid;

@property(nonatomic, readwrite) uint64_t lastOrderTime;

@property(nonatomic, readwrite) uint64_t activeTime;

// uint32		companyid                = 23;			 
@property(nonatomic, readwrite, copy) NSString *platform;

@property(nonatomic, readwrite, copy) NSString *accountNo;

@property(nonatomic, readwrite) double freeBalance;

@property(nonatomic, readwrite) double frozen;

@property(nonatomic, readwrite) double volumes;

@property(nonatomic, readwrite) uint32_t positions;

@property(nonatomic, readwrite, copy) NSString *companyToken;

@property(nonatomic, readwrite) item_account_estatus status;

@property(nonatomic, readwrite) uint32_t delayLeft;

@property(nonatomic, readwrite) uint32_t delayRight;

@property(nonatomic, readwrite) item_account_etype type;

@property(nonatomic, readwrite) uint64_t lpuserid;

@property(nonatomic, readwrite) uint32_t clearNegative;

@end

int32_t item_account_Status_RawValue(item_account *message);
void Setitem_account_Status_RawValue(item_account *message, int32_t value);

int32_t item_account_Type_RawValue(item_account *message);
void Setitem_account_Type_RawValue(item_account *message, int32_t value);

CF_EXTERN_C_END

// @@protoc_insertion_point(global_scope)
