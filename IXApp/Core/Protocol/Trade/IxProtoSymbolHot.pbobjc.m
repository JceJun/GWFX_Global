// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_symbol_hot.proto

#import "GPBProtocolBuffers_RuntimeSupport.h"
#import "IxProtoSymbolHot.pbobjc.h"
#import "IxProtoHeader.pbobjc.h"
#import "IxItemSymbolHot.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma mark - IxProtoSymbolHotRoot

@implementation IxProtoSymbolHotRoot

+ (GPBExtensionRegistry*)extensionRegistry {
  // This is called by +initialize so there is no need to worry
  // about thread safety and initialization of registry.
  static GPBExtensionRegistry* registry = nil;
  if (!registry) {
    registry = [[GPBExtensionRegistry alloc] init];
    static GPBExtensionDescription descriptions[] = {
    };
    #pragma unused (descriptions)
    [registry addExtensions:[IxProtoHeaderRoot extensionRegistry]];
    [registry addExtensions:[IxItemSymbolHotRoot extensionRegistry]];
  }
  return registry;
}

@end

static GPBFileDescriptor *IxProtoSymbolHotRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - proto_symbol_hot_add

@implementation proto_symbol_hot_add

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasSymbolHot, symbolHot;

typedef struct proto_symbol_hot_add_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_symbol_hot *symbolHot;
  uint64_t id_p;
} proto_symbol_hot_add_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_hot_add_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_add_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_hot_add_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_hot_add_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_hot_add_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_hot_add_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_hot_add_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_hot_add_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolHot",
        .number = proto_symbol_hot_add_FieldNumber_SymbolHot,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_add_Storage, symbolHot),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_hot),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_hot_add class]
                                              rootClass:[IxProtoSymbolHotRoot class]
                                                   file:IxProtoSymbolHotRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_hot_add_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_hot_update

@implementation proto_symbol_hot_update

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasSymbolHot, symbolHot;

typedef struct proto_symbol_hot_update_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_symbol_hot *symbolHot;
  uint64_t id_p;
} proto_symbol_hot_update_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_hot_update_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_update_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_hot_update_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_hot_update_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_hot_update_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_hot_update_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_hot_update_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_hot_update_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolHot",
        .number = proto_symbol_hot_update_FieldNumber_SymbolHot,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_update_Storage, symbolHot),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_hot),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_hot_update class]
                                              rootClass:[IxProtoSymbolHotRoot class]
                                                   file:IxProtoSymbolHotRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_hot_update_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_hot_delete

@implementation proto_symbol_hot_delete

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;

typedef struct proto_symbol_hot_delete_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  uint64_t id_p;
} proto_symbol_hot_delete_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_hot_delete_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_delete_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_hot_delete_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_hot_delete_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_hot_delete_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_hot_delete_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_hot_delete_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_hot_delete_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_hot_delete class]
                                              rootClass:[IxProtoSymbolHotRoot class]
                                                   file:IxProtoSymbolHotRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_hot_delete_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_hot_get

@implementation proto_symbol_hot_get

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasSymbolHot, symbolHot;

typedef struct proto_symbol_hot_get_Storage {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_symbol_hot *symbolHot;
  uint64_t id_p;
} proto_symbol_hot_get_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_hot_get_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_get_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_symbol_hot_get_FieldNumber_Id_p,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_hot_get_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "result",
        .number = proto_symbol_hot_get_FieldNumber_Result,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_hot_get_Storage, result),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_symbol_hot_get_FieldNumber_Comment,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_symbol_hot_get_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolHot",
        .number = proto_symbol_hot_get_FieldNumber_SymbolHot,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_get_Storage, symbolHot),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_hot),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_hot_get class]
                                              rootClass:[IxProtoSymbolHotRoot class]
                                                   file:IxProtoSymbolHotRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_hot_get_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_symbol_hot_list

@implementation proto_symbol_hot_list

@dynamic hasHeader, header;
@dynamic companyid;
@dynamic offset;
@dynamic count;
@dynamic total;
@dynamic symbolHotArray;

typedef struct proto_symbol_hot_list_Storage {
  uint32_t _has_storage_[1];
  uint32_t offset;
  uint32_t count;
  uint32_t total;
  item_header *header;
  NSMutableArray *symbolHotArray;
  uint64_t companyid;
} proto_symbol_hot_list_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_symbol_hot_list_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_list_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "companyid",
        .number = proto_symbol_hot_list_FieldNumber_Companyid,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_symbol_hot_list_Storage, companyid),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "offset",
        .number = proto_symbol_hot_list_FieldNumber_Offset,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_hot_list_Storage, offset),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "count",
        .number = proto_symbol_hot_list_FieldNumber_Count,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_hot_list_Storage, count),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "total",
        .number = proto_symbol_hot_list_FieldNumber_Total,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_symbol_hot_list_Storage, total),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "symbolHotArray",
        .number = proto_symbol_hot_list_FieldNumber_SymbolHotArray,
        .hasIndex = GPBNoHasBit,
        .flags = GPBFieldRepeated,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_symbol_hot_list_Storage, symbolHotArray),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_symbol_hot),
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_symbol_hot_list class]
                                              rootClass:[IxProtoSymbolHotRoot class]
                                                   file:IxProtoSymbolHotRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_symbol_hot_list_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end


// @@protoc_insertion_point(global_scope)
