// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_account_group_symbol_cata.proto

#import "GPBProtocolBuffers.h"

#if GOOGLE_PROTOBUF_OBJC_GEN_VERSION != 30000
#error This file was generated by a different version of protoc-gen-objc which is incompatible with your Protocol Buffer sources.
#endif

// @@protoc_insertion_point(imports)

CF_EXTERN_C_BEGIN

@class item_account_group_symbol_cata;
@class item_header;


#pragma mark - IxProtoAccountGroupSymbolCataRoot

@interface IxProtoAccountGroupSymbolCataRoot : GPBRootObject

// The base class provides:
//   + (GPBExtensionRegistry *)extensionRegistry;
// which is an GPBExtensionRegistry that includes all the extensions defined by
// this file and all files that it depends on.

@end

#pragma mark - proto_account_group_symbol_cata_add

typedef GPB_ENUM(proto_account_group_symbol_cata_add_FieldNumber) {
  proto_account_group_symbol_cata_add_FieldNumber_Header = 1,
  proto_account_group_symbol_cata_add_FieldNumber_Id_p = 2,
  proto_account_group_symbol_cata_add_FieldNumber_Result = 3,
  proto_account_group_symbol_cata_add_FieldNumber_Comment = 4,
  proto_account_group_symbol_cata_add_FieldNumber_AccountGroupSymbolCata = 5,
};

@interface proto_account_group_symbol_cata_add : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@property(nonatomic, readwrite) BOOL hasAccountGroupSymbolCata;
@property(nonatomic, readwrite, strong) item_account_group_symbol_cata *accountGroupSymbolCata;

@end

#pragma mark - proto_account_group_symbol_cata_update

typedef GPB_ENUM(proto_account_group_symbol_cata_update_FieldNumber) {
  proto_account_group_symbol_cata_update_FieldNumber_Header = 1,
  proto_account_group_symbol_cata_update_FieldNumber_Id_p = 2,
  proto_account_group_symbol_cata_update_FieldNumber_Result = 3,
  proto_account_group_symbol_cata_update_FieldNumber_Comment = 4,
  proto_account_group_symbol_cata_update_FieldNumber_AccountGroupSymbolCata = 5,
};

@interface proto_account_group_symbol_cata_update : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@property(nonatomic, readwrite) BOOL hasAccountGroupSymbolCata;
@property(nonatomic, readwrite, strong) item_account_group_symbol_cata *accountGroupSymbolCata;

@end

#pragma mark - proto_account_group_symbol_cata_delete

typedef GPB_ENUM(proto_account_group_symbol_cata_delete_FieldNumber) {
  proto_account_group_symbol_cata_delete_FieldNumber_Header = 1,
  proto_account_group_symbol_cata_delete_FieldNumber_Id_p = 2,
  proto_account_group_symbol_cata_delete_FieldNumber_Result = 3,
  proto_account_group_symbol_cata_delete_FieldNumber_Comment = 4,
};

@interface proto_account_group_symbol_cata_delete : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@end

#pragma mark - proto_account_group_symbol_cata_get

typedef GPB_ENUM(proto_account_group_symbol_cata_get_FieldNumber) {
  proto_account_group_symbol_cata_get_FieldNumber_Header = 1,
  proto_account_group_symbol_cata_get_FieldNumber_Id_p = 2,
  proto_account_group_symbol_cata_get_FieldNumber_Result = 3,
  proto_account_group_symbol_cata_get_FieldNumber_Comment = 4,
  proto_account_group_symbol_cata_get_FieldNumber_AccountGroupSymbolCata = 5,
};

@interface proto_account_group_symbol_cata_get : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy) NSString *comment;

@property(nonatomic, readwrite) BOOL hasAccountGroupSymbolCata;
@property(nonatomic, readwrite, strong) item_account_group_symbol_cata *accountGroupSymbolCata;

@end

#pragma mark - proto_account_group_symbol_cata_list

typedef GPB_ENUM(proto_account_group_symbol_cata_list_FieldNumber) {
  proto_account_group_symbol_cata_list_FieldNumber_Header = 1,
  proto_account_group_symbol_cata_list_FieldNumber_Id_p = 2,
  proto_account_group_symbol_cata_list_FieldNumber_Offset = 3,
  proto_account_group_symbol_cata_list_FieldNumber_Count = 4,
  proto_account_group_symbol_cata_list_FieldNumber_Total = 5,
  proto_account_group_symbol_cata_list_FieldNumber_AccountGroupSymbolCataArray = 6,
};

@interface proto_account_group_symbol_cata_list : GPBMessage

@property(nonatomic, readwrite) BOOL hasHeader;
@property(nonatomic, readwrite, strong) item_header *header;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite) uint32_t total;

// |accountGroupSymbolCataArray| contains |item_account_group_symbol_cata|
@property(nonatomic, readwrite, strong) NSMutableArray *accountGroupSymbolCataArray;

@end

CF_EXTERN_C_END

// @@protoc_insertion_point(global_scope)
