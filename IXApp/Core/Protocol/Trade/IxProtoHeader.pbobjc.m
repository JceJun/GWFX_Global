// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_header.proto

#import "GPBProtocolBuffers_RuntimeSupport.h"
#import "IxProtoHeader.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma mark - IxProtoHeaderRoot

@implementation IxProtoHeaderRoot

@end

static GPBFileDescriptor *IxProtoHeaderRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_header

@implementation item_header

@dynamic length;
@dynamic command;
@dynamic id_p;
@dynamic token;
@dynamic crc;

typedef struct item_header_Storage {
  uint32_t _has_storage_[1];
  uint32_t length;
  uint32_t command;
  uint32_t crc;
  NSString *token;
  uint64_t id_p;
} item_header_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "length",
        .number = item_header_FieldNumber_Length,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_header_Storage, length),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "command",
        .number = item_header_FieldNumber_Command,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_header_Storage, command),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = item_header_FieldNumber_Id_p,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(item_header_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "token",
        .number = item_header_FieldNumber_Token,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(item_header_Storage, token),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "crc",
        .number = item_header_FieldNumber_Crc,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_header_Storage, crc),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[item_header class]
                                              rootClass:[IxProtoHeaderRoot class]
                                                   file:IxProtoHeaderRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(item_header_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - item_simple_header

@implementation item_simple_header

@dynamic length;
@dynamic command;
@dynamic id_p;

typedef struct item_simple_header_Storage {
  uint32_t _has_storage_[1];
  uint32_t length;
  uint32_t command;
  uint64_t id_p;
} item_simple_header_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "length",
        .number = item_simple_header_FieldNumber_Length,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_simple_header_Storage, length),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "command",
        .number = item_simple_header_FieldNumber_Command,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(item_simple_header_Storage, command),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = item_simple_header_FieldNumber_Id_p,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(item_simple_header_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[item_simple_header class]
                                              rootClass:[IxProtoHeaderRoot class]
                                                   file:IxProtoHeaderRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(item_simple_header_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end

#pragma mark - proto_simple_response

@implementation proto_simple_response

@dynamic hasHeader, header;
@dynamic command;
@dynamic id_p;
@dynamic ret;
@dynamic comment;

typedef struct proto_simple_response_Storage {
  uint32_t _has_storage_[1];
  uint32_t command;
  uint32_t ret;
  item_header *header;
  NSString *comment;
  uint64_t id_p;
} proto_simple_response_Storage;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = NULL;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .number = proto_simple_response_FieldNumber_Header,
        .hasIndex = 0,
        .flags = GPBFieldOptional,
        .type = GPBTypeMessage,
        .offset = offsetof(proto_simple_response_Storage, header),
        .defaultValue.valueMessage = nil,
        .typeSpecific.className = GPBStringifySymbol(item_header),
        .fieldOptions = NULL,
      },
      {
        .name = "command",
        .number = proto_simple_response_FieldNumber_Command,
        .hasIndex = 1,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_simple_response_Storage, command),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "id_p",
        .number = proto_simple_response_FieldNumber_Id_p,
        .hasIndex = 2,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt64,
        .offset = offsetof(proto_simple_response_Storage, id_p),
        .defaultValue.valueUInt64 = 0ULL,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "ret",
        .number = proto_simple_response_FieldNumber_Ret,
        .hasIndex = 3,
        .flags = GPBFieldOptional,
        .type = GPBTypeUInt32,
        .offset = offsetof(proto_simple_response_Storage, ret),
        .defaultValue.valueUInt32 = 0U,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
      {
        .name = "comment",
        .number = proto_simple_response_FieldNumber_Comment,
        .hasIndex = 4,
        .flags = GPBFieldOptional,
        .type = GPBTypeString,
        .offset = offsetof(proto_simple_response_Storage, comment),
        .defaultValue.valueString = nil,
        .typeSpecific.className = NULL,
        .fieldOptions = NULL,
      },
    };
    descriptor = [GPBDescriptor allocDescriptorForClass:[proto_simple_response class]
                                              rootClass:[IxProtoHeaderRoot class]
                                                   file:IxProtoHeaderRoot_FileDescriptor()
                                                 fields:fields
                                             fieldCount:sizeof(fields) / sizeof(GPBMessageFieldDescription)
                                                 oneofs:NULL
                                             oneofCount:0
                                                  enums:NULL
                                              enumCount:0
                                                 ranges:NULL
                                             rangeCount:0
                                            storageSize:sizeof(proto_simple_response_Storage)
                                             wireFormat:NO];
  }
  return descriptor;
}

@end


// @@protoc_insertion_point(global_scope)
