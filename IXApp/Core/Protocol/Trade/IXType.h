#pragma once

#ifdef __cplusplus
extern "C" {
#endif
typedef signed char         INT8;
typedef unsigned char       UINT8;
typedef signed short        INT16;
typedef unsigned short      UINT16;
typedef signed int          INT32;
typedef unsigned int        UINT32;
typedef signed long long    INT64;
typedef unsigned long long  UINT64;

typedef signed char         int8;
typedef unsigned char       uint8;
typedef unsigned char       uchar;
typedef signed short        int16;
typedef unsigned short      uint16;
typedef signed int          int32;
typedef unsigned int        uint32;
typedef signed long long    int64;
typedef unsigned long long  uint64;
#ifdef __cplusplus
}
#endif

