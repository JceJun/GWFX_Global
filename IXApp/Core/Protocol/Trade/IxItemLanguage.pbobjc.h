// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_language.proto

#import "GPBProtocolBuffers.h"

#if GOOGLE_PROTOBUF_OBJC_GEN_VERSION != 30000
#error This file was generated by a different version of protoc-gen-objc which is incompatible with your Protocol Buffer sources.
#endif

// @@protoc_insertion_point(imports)

CF_EXTERN_C_BEGIN


#pragma mark - IxItemLanguageRoot

@interface IxItemLanguageRoot : GPBRootObject

// The base class provides:
//   + (GPBExtensionRegistry *)extensionRegistry;
// which is an GPBExtensionRegistry that includes all the extensions defined by
// this file and all files that it depends on.

@end

#pragma mark - item_language

typedef GPB_ENUM(item_language_FieldNumber) {
  item_language_FieldNumber_Id_p = 1,
  item_language_FieldNumber_Uuid = 2,
  item_language_FieldNumber_Uutime = 3,
  item_language_FieldNumber_NameSpace = 4,
  item_language_FieldNumber_Keyid = 5,
  item_language_FieldNumber_Value = 6,
  item_language_FieldNumber_Country = 7,
  item_language_FieldNumber_Status = 8,
};

@interface item_language : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite, copy) NSString *nameSpace;

@property(nonatomic, readwrite) uint64_t keyid;

@property(nonatomic, readwrite, copy) NSString *value;

@property(nonatomic, readwrite, copy) NSString *country;

@property(nonatomic, readwrite) uint32_t status;

@end

CF_EXTERN_C_END

// @@protoc_insertion_point(global_scope)
