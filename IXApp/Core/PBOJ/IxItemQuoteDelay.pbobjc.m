// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_quote_delay.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

#import <stdatomic.h>

#import "IxItemQuoteDelay.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - IxItemQuoteDelayRoot

@implementation IxItemQuoteDelayRoot

// No extensions in the file and no imports, so no need to generate
// +extensionRegistry.

@end

#pragma mark - IxItemQuoteDelayRoot_FileDescriptor

static GPBFileDescriptor *IxItemQuoteDelayRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_quote_delay

@implementation item_quote_delay

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic userid;
@dynamic symbolCataid;
@dynamic limitDate;
@dynamic status;
@dynamic type;

typedef struct item_quote_delay__storage_ {
  uint32_t _has_storage_[1];
  item_quote_delay_estatus status;
  uint32_t type;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
  uint64_t userid;
  uint64_t symbolCataid;
  uint64_t limitDate;
} item_quote_delay__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = item_quote_delay_FieldNumber_Id_p,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uuid",
        .dataTypeSpecific.className = NULL,
        .number = item_quote_delay_FieldNumber_Uuid,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, uuid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uutime",
        .dataTypeSpecific.className = NULL,
        .number = item_quote_delay_FieldNumber_Uutime,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, uutime),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "userid",
        .dataTypeSpecific.className = NULL,
        .number = item_quote_delay_FieldNumber_Userid,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, userid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "symbolCataid",
        .dataTypeSpecific.className = NULL,
        .number = item_quote_delay_FieldNumber_SymbolCataid,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, symbolCataid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "limitDate",
        .dataTypeSpecific.className = NULL,
        .number = item_quote_delay_FieldNumber_LimitDate,
        .hasIndex = 5,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, limitDate),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "status",
        .dataTypeSpecific.enumDescFunc = item_quote_delay_estatus_EnumDescriptor,
        .number = item_quote_delay_FieldNumber_Status,
        .hasIndex = 6,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, status),
        .flags = (GPBFieldFlags)(GPBFieldOptional | GPBFieldHasEnumDescriptor),
        .dataType = GPBDataTypeEnum,
      },
      {
        .name = "type",
        .dataTypeSpecific.className = NULL,
        .number = item_quote_delay_FieldNumber_Type,
        .hasIndex = 7,
        .offset = (uint32_t)offsetof(item_quote_delay__storage_, type),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[item_quote_delay class]
                                     rootClass:[IxItemQuoteDelayRoot class]
                                          file:IxItemQuoteDelayRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(item_quote_delay__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

int32_t item_quote_delay_Status_RawValue(item_quote_delay *message) {
  GPBDescriptor *descriptor = [item_quote_delay descriptor];
  GPBFieldDescriptor *field = [descriptor fieldWithNumber:item_quote_delay_FieldNumber_Status];
  return GPBGetMessageInt32Field(message, field);
}

void Setitem_quote_delay_Status_RawValue(item_quote_delay *message, int32_t value) {
  GPBDescriptor *descriptor = [item_quote_delay descriptor];
  GPBFieldDescriptor *field = [descriptor fieldWithNumber:item_quote_delay_FieldNumber_Status];
  GPBSetInt32IvarWithFieldInternal(message, field, value, descriptor.file.syntax);
}

#pragma mark - Enum item_quote_delay_estatus

GPBEnumDescriptor *item_quote_delay_estatus_EnumDescriptor(void) {
  static _Atomic(GPBEnumDescriptor*) descriptor = nil;
  if (!descriptor) {
    static const char *valueNames =
        "Normal\000Deleted\000";
    static const int32_t values[] = {
        item_quote_delay_estatus_Normal,
        item_quote_delay_estatus_Deleted,
    };
    static const char *extraTextFormatInfo = "\002\000&\000\001\'\000";
    GPBEnumDescriptor *worker =
        [GPBEnumDescriptor allocDescriptorForName:GPBNSStringifySymbol(item_quote_delay_estatus)
                                       valueNames:valueNames
                                           values:values
                                            count:(uint32_t)(sizeof(values) / sizeof(int32_t))
                                     enumVerifier:item_quote_delay_estatus_IsValidValue
                              extraTextFormatInfo:extraTextFormatInfo];
    GPBEnumDescriptor *expected = nil;
    if (!atomic_compare_exchange_strong(&descriptor, &expected, worker)) {
      [worker release];
    }
  }
  return descriptor;
}

BOOL item_quote_delay_estatus_IsValidValue(int32_t value__) {
  switch (value__) {
    case item_quote_delay_estatus_Normal:
    case item_quote_delay_estatus_Deleted:
      return YES;
    default:
      return NO;
  }
}

#pragma mark - Enum item_quote_delay_etype

GPBEnumDescriptor *item_quote_delay_etype_EnumDescriptor(void) {
  static _Atomic(GPBEnumDescriptor*) descriptor = nil;
  if (!descriptor) {
    static const char *valueNames =
        "PerOnce\000PerMonth\000";
    static const int32_t values[] = {
        item_quote_delay_etype_PerOnce,
        item_quote_delay_etype_PerMonth,
    };
    static const char *extraTextFormatInfo = "\002\000#\244\000\001#\245\000";
    GPBEnumDescriptor *worker =
        [GPBEnumDescriptor allocDescriptorForName:GPBNSStringifySymbol(item_quote_delay_etype)
                                       valueNames:valueNames
                                           values:values
                                            count:(uint32_t)(sizeof(values) / sizeof(int32_t))
                                     enumVerifier:item_quote_delay_etype_IsValidValue
                              extraTextFormatInfo:extraTextFormatInfo];
    GPBEnumDescriptor *expected = nil;
    if (!atomic_compare_exchange_strong(&descriptor, &expected, worker)) {
      [worker release];
    }
  }
  return descriptor;
}

BOOL item_quote_delay_etype_IsValidValue(int32_t value__) {
  switch (value__) {
    case item_quote_delay_etype_PerOnce:
    case item_quote_delay_etype_PerMonth:
      return YES;
    default:
      return NO;
  }
}


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
