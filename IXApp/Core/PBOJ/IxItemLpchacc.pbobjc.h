// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_lpchacc.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Enum item_lpchacc_estatus

typedef GPB_ENUM(item_lpchacc_estatus) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_lpchacc_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_lpchacc_estatus_Normal = 0,
  item_lpchacc_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_lpchacc_estatus_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_lpchacc_estatus_IsValidValue(int32_t value);

#pragma mark - IxItemLpchaccRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxItemLpchaccRoot : GPBRootObject
@end

#pragma mark - item_lpchacc

typedef GPB_ENUM(item_lpchacc_FieldNumber) {
  item_lpchacc_FieldNumber_Id_p = 1,
  item_lpchacc_FieldNumber_Uuid = 2,
  item_lpchacc_FieldNumber_Uutime = 3,
  item_lpchacc_FieldNumber_Channelid = 4,
  item_lpchacc_FieldNumber_LpCompanyid = 5,
  item_lpchacc_FieldNumber_Refaccid = 6,
  item_lpchacc_FieldNumber_Token = 7,
  item_lpchacc_FieldNumber_Name = 8,
  item_lpchacc_FieldNumber_Ip = 9,
  item_lpchacc_FieldNumber_Port = 10,
  item_lpchacc_FieldNumber_Status = 11,
  item_lpchacc_FieldNumber_TargetCompid = 12,
  item_lpchacc_FieldNumber_SenderCompid = 13,
  item_lpchacc_FieldNumber_SenderSubid = 14,
  item_lpchacc_FieldNumber_DeliverToCompid = 15,
  item_lpchacc_FieldNumber_Username = 16,
  item_lpchacc_FieldNumber_Password = 17,
};

@interface item_lpchacc : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite) uint32_t channelid;

@property(nonatomic, readwrite) uint64_t lpCompanyid;

@property(nonatomic, readwrite) uint64_t refaccid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *token;

@property(nonatomic, readwrite, copy, null_resettable) NSString *name;

@property(nonatomic, readwrite, copy, null_resettable) NSString *ip;

@property(nonatomic, readwrite) int32_t port;

@property(nonatomic, readwrite) item_lpchacc_estatus status;

@property(nonatomic, readwrite, copy, null_resettable) NSString *targetCompid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *senderCompid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *senderSubid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *deliverToCompid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *username;

@property(nonatomic, readwrite, copy, null_resettable) NSData *password;

@end

/**
 * Fetches the raw value of a @c item_lpchacc's @c status property, even
 * if the value was not defined by the enum at the time the code was generated.
 **/
int32_t item_lpchacc_Status_RawValue(item_lpchacc *message);
/**
 * Sets the raw value of an @c item_lpchacc's @c status property, allowing
 * it to be set to a value that was not defined by the enum at the time the code
 * was generated.
 **/
void Setitem_lpchacc_Status_RawValue(item_lpchacc *message, int32_t value);

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
