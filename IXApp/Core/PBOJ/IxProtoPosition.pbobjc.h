// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_position.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

@class item_header;
@class item_position;
@class item_position_summary;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - IxProtoPositionRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxProtoPositionRoot : GPBRootObject
@end

#pragma mark - proto_position_add

typedef GPB_ENUM(proto_position_add_FieldNumber) {
  proto_position_add_FieldNumber_Header = 1,
  proto_position_add_FieldNumber_Id_p = 2,
  proto_position_add_FieldNumber_Result = 3,
  proto_position_add_FieldNumber_Comment = 4,
  proto_position_add_FieldNumber_Position = 5,
};

@interface proto_position_add : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) item_position *position;
/** Test to see if @c position has been set. */
@property(nonatomic, readwrite) BOOL hasPosition;

@end

#pragma mark - proto_position_update

typedef GPB_ENUM(proto_position_update_FieldNumber) {
  proto_position_update_FieldNumber_Header = 1,
  proto_position_update_FieldNumber_Id_p = 2,
  proto_position_update_FieldNumber_Result = 3,
  proto_position_update_FieldNumber_Comment = 4,
  proto_position_update_FieldNumber_Position = 5,
};

@interface proto_position_update : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) item_position *position;
/** Test to see if @c position has been set. */
@property(nonatomic, readwrite) BOOL hasPosition;

@end

#pragma mark - proto_position_close

typedef GPB_ENUM(proto_position_close_FieldNumber) {
  proto_position_close_FieldNumber_Header = 1,
  proto_position_close_FieldNumber_Id_p = 2,
  proto_position_close_FieldNumber_Result = 3,
  proto_position_close_FieldNumber_Comment = 4,
  proto_position_close_FieldNumber_PositionArray = 5,
};

/**
 * used by stopout/colse pending to trade
 * CMD_POSITION_FORCE_CLOSE/(CMD_POSITION_STOPLOSS_CLOSE|CMD_POSITION_TAKEPROFIT_CLOSE)
 **/
@interface proto_position_close : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position*> *positionArray;
/** The number of items in @c positionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionArray_Count;

@end

#pragma mark - proto_position_update_swap

typedef GPB_ENUM(proto_position_update_swap_FieldNumber) {
  proto_position_update_swap_FieldNumber_Header = 1,
  proto_position_update_swap_FieldNumber_Id_p = 2,
  proto_position_update_swap_FieldNumber_TotalSwap = 3,
  proto_position_update_swap_FieldNumber_LastEndOfDay = 4,
  proto_position_update_swap_FieldNumber_PositionSwap = 5,
};

/**
 * used by ix_swap calc to update pos/gen deal/update acc
 * not used.
 **/
@interface proto_position_update_swap : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

/** account id */
@property(nonatomic, readwrite) uint64_t id_p;

/** trade will check again. */
@property(nonatomic, readwrite) double totalSwap;

@property(nonatomic, readwrite) uint32_t lastEndOfDay;

/** repeated item_position position = 5 ; */
@property(nonatomic, readwrite, strong, null_resettable) GPBUInt64DoubleDictionary *positionSwap;
/** The number of items in @c positionSwap without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionSwap_Count;

@end

#pragma mark - proto_start_calc_swap

typedef GPB_ENUM(proto_start_calc_swap_FieldNumber) {
  proto_start_calc_swap_FieldNumber_Header = 1,
  proto_start_calc_swap_FieldNumber_Id_p = 2,
  proto_start_calc_swap_FieldNumber_Triple = 3,
  proto_start_calc_swap_FieldNumber_Result = 4,
  proto_start_calc_swap_FieldNumber_Comment = 5,
};

@interface proto_start_calc_swap : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) BOOL triple;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@end

#pragma mark - proto_position_close_one_account

typedef GPB_ENUM(proto_position_close_one_account_FieldNumber) {
  proto_position_close_one_account_FieldNumber_Header = 1,
  proto_position_close_one_account_FieldNumber_Id_p = 2,
  proto_position_close_one_account_FieldNumber_Result = 3,
  proto_position_close_one_account_FieldNumber_Comment = 4,
  proto_position_close_one_account_FieldNumber_Accountid = 5,
  proto_position_close_one_account_FieldNumber_Strategyid = 6,
};

@interface proto_position_close_one_account : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite) uint64_t accountid;

@property(nonatomic, readwrite) uint64_t strategyid;

@end

#pragma mark - proto_position_close_room_account

typedef GPB_ENUM(proto_position_close_room_account_FieldNumber) {
  proto_position_close_room_account_FieldNumber_Header = 1,
  proto_position_close_room_account_FieldNumber_Id_p = 2,
  proto_position_close_room_account_FieldNumber_Result = 3,
  proto_position_close_room_account_FieldNumber_Comment = 4,
  proto_position_close_room_account_FieldNumber_AccountidArray = 5,
  proto_position_close_room_account_FieldNumber_StartTime = 6,
  proto_position_close_room_account_FieldNumber_EndTime = 7,
};

@interface proto_position_close_room_account : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) GPBUInt64Array *accountidArray;
/** The number of items in @c accountidArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger accountidArray_Count;

@property(nonatomic, readwrite) uint64_t startTime;

@property(nonatomic, readwrite) uint64_t endTime;

@end

#pragma mark - proto_position_delete

typedef GPB_ENUM(proto_position_delete_FieldNumber) {
  proto_position_delete_FieldNumber_Header = 1,
  proto_position_delete_FieldNumber_Id_p = 2,
  proto_position_delete_FieldNumber_Accountid = 3,
};

@interface proto_position_delete : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t accountid;

@end

#pragma mark - proto_position_get

typedef GPB_ENUM(proto_position_get_FieldNumber) {
  proto_position_get_FieldNumber_Header = 1,
  proto_position_get_FieldNumber_Id_p = 2,
  proto_position_get_FieldNumber_Result = 3,
  proto_position_get_FieldNumber_Comment = 4,
  proto_position_get_FieldNumber_Position = 5,
};

@interface proto_position_get : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) item_position *position;
/** Test to see if @c position has been set. */
@property(nonatomic, readwrite) BOOL hasPosition;

@end

#pragma mark - proto_position_list

typedef GPB_ENUM(proto_position_list_FieldNumber) {
  proto_position_list_FieldNumber_Header = 1,
  proto_position_list_FieldNumber_Accountid = 2,
  proto_position_list_FieldNumber_Offset = 3,
  proto_position_list_FieldNumber_Count = 4,
  proto_position_list_FieldNumber_Total = 5,
  proto_position_list_FieldNumber_PositionArray = 6,
};

@interface proto_position_list : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t accountid;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite) uint32_t total;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position*> *positionArray;
/** The number of items in @c positionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionArray_Count;

@end

#pragma mark - proto_position_sync

typedef GPB_ENUM(proto_position_sync_FieldNumber) {
  proto_position_sync_FieldNumber_Header = 1,
  proto_position_sync_FieldNumber_Offset = 2,
  proto_position_sync_FieldNumber_Count = 3,
  proto_position_sync_FieldNumber_PositionArray = 4,
};

@interface proto_position_sync : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position*> *positionArray;
/** The number of items in @c positionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionArray_Count;

@end

#pragma mark - proto_position_history_list

typedef GPB_ENUM(proto_position_history_list_FieldNumber) {
  proto_position_history_list_FieldNumber_Header = 1,
  proto_position_history_list_FieldNumber_Accountid = 3,
  proto_position_history_list_FieldNumber_Offset = 4,
  proto_position_history_list_FieldNumber_Count = 5,
  proto_position_history_list_FieldNumber_Total = 6,
  proto_position_history_list_FieldNumber_From = 7,
  proto_position_history_list_FieldNumber_To = 8,
  proto_position_history_list_FieldNumber_PositionArray = 9,
};

@interface proto_position_history_list : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

/** uint8 status = 2 ; */
@property(nonatomic, readwrite) uint64_t accountid;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite) uint32_t total;

@property(nonatomic, readwrite) uint64_t from;

@property(nonatomic, readwrite) uint64_t to;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position*> *positionArray;
/** The number of items in @c positionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionArray_Count;

@end

#pragma mark - proto_position_manager_list

typedef GPB_ENUM(proto_position_manager_list_FieldNumber) {
  proto_position_manager_list_FieldNumber_Header = 1,
  proto_position_manager_list_FieldNumber_Manageruserid = 2,
  proto_position_manager_list_FieldNumber_Offset = 3,
  proto_position_manager_list_FieldNumber_Count = 4,
  proto_position_manager_list_FieldNumber_Total = 5,
  proto_position_manager_list_FieldNumber_PositionArray = 6,
};

@interface proto_position_manager_list : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t manageruserid;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite) uint32_t total;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position*> *positionArray;
/** The number of items in @c positionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionArray_Count;

@end

#pragma mark - proto_position_summary

typedef GPB_ENUM(proto_position_summary_FieldNumber) {
  proto_position_summary_FieldNumber_Header = 1,
  proto_position_summary_FieldNumber_Count = 2,
  proto_position_summary_FieldNumber_SummaryArray = 3,
};

@interface proto_position_summary : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position_summary*> *summaryArray;
/** The number of items in @c summaryArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger summaryArray_Count;

@end

#pragma mark - proto_position_notify

typedef GPB_ENUM(proto_position_notify_FieldNumber) {
  proto_position_notify_FieldNumber_Header = 1,
  proto_position_notify_FieldNumber_Orderid = 3,
  proto_position_notify_FieldNumber_Reforderid = 4,
  proto_position_notify_FieldNumber_Comment = 5,
  proto_position_notify_FieldNumber_Position = 6,
};

@interface proto_position_notify : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

/** uint8 dealreason = 2 ; */
@property(nonatomic, readwrite) uint64_t orderid;

@property(nonatomic, readwrite) uint64_t reforderid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) item_position *position;
/** Test to see if @c position has been set. */
@property(nonatomic, readwrite) BOOL hasPosition;

@end

#pragma mark - proto_position_dealer

typedef GPB_ENUM(proto_position_dealer_FieldNumber) {
  proto_position_dealer_FieldNumber_Header = 1,
  proto_position_dealer_FieldNumber_Result = 2,
  proto_position_dealer_FieldNumber_Count = 3,
  proto_position_dealer_FieldNumber_PositionArray = 4,
};

@interface proto_position_dealer : GPBMessage

/** header->id = dealer user id */
@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position*> *positionArray;
/** The number of items in @c positionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionArray_Count;

@end

#pragma mark - proto_position_update_batch

typedef GPB_ENUM(proto_position_update_batch_FieldNumber) {
  proto_position_update_batch_FieldNumber_Header = 1,
  proto_position_update_batch_FieldNumber_Id_p = 2,
  proto_position_update_batch_FieldNumber_Result = 3,
  proto_position_update_batch_FieldNumber_Comment = 4,
  proto_position_update_batch_FieldNumber_PositionArray = 5,
  proto_position_update_batch_FieldNumber_Accountid = 6,
};

@interface proto_position_update_batch : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_position*> *positionArray;
/** The number of items in @c positionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger positionArray_Count;

@property(nonatomic, readwrite) uint64_t accountid;

@end

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
