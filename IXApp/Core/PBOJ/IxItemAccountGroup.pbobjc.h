// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_account_group.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Enum item_account_group_estatus

typedef GPB_ENUM(item_account_group_estatus) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_account_group_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_group_estatus_Normal = 0,
  item_account_group_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_account_group_estatus_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_account_group_estatus_IsValidValue(int32_t value);

#pragma mark - Enum item_account_group_etype

typedef GPB_ENUM(item_account_group_etype) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_account_group_etype_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_group_etype_Virtuals = 0,
  item_account_group_etype_Standard = 1,
  item_account_group_etype_Vip = 2,
  item_account_group_etype_Manager = 3,
  item_account_group_etype_Turnover = 4,
  item_account_group_etype_Room = 5,
  item_account_group_etype_Diamond = 6,
  item_account_group_etype_Visitor = 7,
  item_account_group_etype_Agent = 8,
};

GPBEnumDescriptor *item_account_group_etype_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_account_group_etype_IsValidValue(int32_t value);

#pragma mark - Enum item_account_group_eclitype

typedef GPB_ENUM(item_account_group_eclitype) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_account_group_eclitype_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_group_eclitype_ClientIx = 0,

  /** stp mode group */
  item_account_group_eclitype_ClientMt4Stp = 1,

  /** sync mode group, this has been used by stp mode. */
  item_account_group_eclitype_ClientMt4Syn = 2,

  /** sync mode group */
  item_account_group_eclitype_ClientMt4Mgr = 6,
};

GPBEnumDescriptor *item_account_group_eclitype_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_account_group_eclitype_IsValidValue(int32_t value);

#pragma mark - Enum item_account_group_eoptions

typedef GPB_ENUM(item_account_group_eoptions) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_account_group_eoptions_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_group_eoptions_OptNone = 0,

  /** not implemented. */
  item_account_group_eoptions_OptNoWebLogin = 1,

  /** not implemented. */
  item_account_group_eoptions_OptNoWebTrade = 2,
  item_account_group_eoptions_OptNoAppLogin = 3,
  item_account_group_eoptions_OptNoAppTrade = 4,
};

GPBEnumDescriptor *item_account_group_eoptions_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_account_group_eoptions_IsValidValue(int32_t value);

#pragma mark - Enum item_account_group_edefault

typedef GPB_ENUM(item_account_group_edefault) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_account_group_edefault_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_account_group_edefault_DefaultNone = 0,
  item_account_group_edefault_DefaultOpen = 1,
};

GPBEnumDescriptor *item_account_group_edefault_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_account_group_edefault_IsValidValue(int32_t value);

#pragma mark - IxItemAccountGroupRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxItemAccountGroupRoot : GPBRootObject
@end

#pragma mark - item_account_group

typedef GPB_ENUM(item_account_group_FieldNumber) {
  item_account_group_FieldNumber_Id_p = 1,
  item_account_group_FieldNumber_Uuid = 2,
  item_account_group_FieldNumber_Uutime = 3,
  item_account_group_FieldNumber_Serverid = 4,
  item_account_group_FieldNumber_Name = 5,
  item_account_group_FieldNumber_Currency = 6,
  item_account_group_FieldNumber_Platform = 7,
  item_account_group_FieldNumber_Level = 8,
  item_account_group_FieldNumber_Authentication = 9,
  item_account_group_FieldNumber_MinPwdLength = 10,
  item_account_group_FieldNumber_BalanceClearNegative = 11,
  item_account_group_FieldNumber_Reserve1 = 12,
  item_account_group_FieldNumber_Companyid = 13,
  item_account_group_FieldNumber_Company = 14,
  item_account_group_FieldNumber_CompanySite = 15,
  item_account_group_FieldNumber_CompanyEmail = 16,
  item_account_group_FieldNumber_SupportSite = 17,
  item_account_group_FieldNumber_SupportEmail = 18,
  item_account_group_FieldNumber_TemplatesFolder = 19,
  item_account_group_FieldNumber_Reserve2 = 20,
  item_account_group_FieldNumber_MarginCallLevel = 21,
  item_account_group_FieldNumber_StopOutLevel = 22,
  item_account_group_FieldNumber_WeekendLevel = 23,
  item_account_group_FieldNumber_Enable = 24,
  item_account_group_FieldNumber_OrderMax = 25,
  item_account_group_FieldNumber_ClearNegative = 26,
  item_account_group_FieldNumber_CreateTime = 27,
  item_account_group_FieldNumber_CreateUserId = 28,
  item_account_group_FieldNumber_ModiTime = 29,
  item_account_group_FieldNumber_ModiUserid = 30,
  item_account_group_FieldNumber_VolumesMax = 32,
  item_account_group_FieldNumber_Reserve = 33,
  item_account_group_FieldNumber_Status = 34,
  item_account_group_FieldNumber_Type = 35,
  item_account_group_FieldNumber_Spread = 40,
  item_account_group_FieldNumber_Lpuserid = 51,
  item_account_group_FieldNumber_ClientType = 53,
  item_account_group_FieldNumber_Options = 54,
  item_account_group_FieldNumber_DefaultType = 55,
};

@interface item_account_group : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite) uint64_t serverid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *name;

@property(nonatomic, readwrite, copy, null_resettable) NSString *currency;

@property(nonatomic, readwrite, copy, null_resettable) NSString *platform;

@property(nonatomic, readwrite, copy, null_resettable) NSString *level;

@property(nonatomic, readwrite, copy, null_resettable) NSString *authentication;

@property(nonatomic, readwrite) BOOL minPwdLength;

@property(nonatomic, readwrite) BOOL balanceClearNegative;

@property(nonatomic, readwrite, copy, null_resettable) NSString *reserve1;

@property(nonatomic, readwrite) uint64_t companyid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *company;

@property(nonatomic, readwrite, copy, null_resettable) NSString *companySite;

@property(nonatomic, readwrite, copy, null_resettable) NSString *companyEmail;

@property(nonatomic, readwrite, copy, null_resettable) NSString *supportSite;

@property(nonatomic, readwrite, copy, null_resettable) NSString *supportEmail;

@property(nonatomic, readwrite, copy, null_resettable) NSString *templatesFolder;

@property(nonatomic, readwrite, copy, null_resettable) NSString *reserve2;

@property(nonatomic, readwrite) uint64_t marginCallLevel;

@property(nonatomic, readwrite) uint64_t stopOutLevel;

@property(nonatomic, readwrite) uint64_t weekendLevel;

@property(nonatomic, readwrite) BOOL enable;

@property(nonatomic, readwrite) uint64_t orderMax;

@property(nonatomic, readwrite) uint64_t clearNegative;

@property(nonatomic, readwrite) uint64_t createTime;

@property(nonatomic, readwrite) uint64_t createUserId;

@property(nonatomic, readwrite) uint64_t modiTime;

@property(nonatomic, readwrite) uint64_t modiUserid;

/** uint64		status					 = 31; */
@property(nonatomic, readwrite) double volumesMax;

@property(nonatomic, readwrite, copy, null_resettable) NSString *reserve;

@property(nonatomic, readwrite) item_account_group_estatus status;

@property(nonatomic, readwrite) uint32_t type;

@property(nonatomic, readwrite) int32_t spread;

@property(nonatomic, readwrite) uint64_t lpuserid;

@property(nonatomic, readwrite) uint32_t clientType;

@property(nonatomic, readwrite) uint32_t options;

@property(nonatomic, readwrite) uint32_t defaultType;

@end

/**
 * Fetches the raw value of a @c item_account_group's @c status property, even
 * if the value was not defined by the enum at the time the code was generated.
 **/
int32_t item_account_group_Status_RawValue(item_account_group *message);
/**
 * Sets the raw value of an @c item_account_group's @c status property, allowing
 * it to be set to a value that was not defined by the enum at the time the code
 * was generated.
 **/
void Setitem_account_group_Status_RawValue(item_account_group *message, int32_t value);

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
