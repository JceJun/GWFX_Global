// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_secure_dev.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Enum item_secure_dev_emode

typedef GPB_ENUM(item_secure_dev_emode) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_secure_dev_emode_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_secure_dev_emode_ByPhone = 0,
  item_secure_dev_emode_ByEmail = 1,
  item_secure_dev_emode_ByOther = 2,
};

GPBEnumDescriptor *item_secure_dev_emode_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_secure_dev_emode_IsValidValue(int32_t value);

#pragma mark - Enum item_secure_dev_epass

typedef GPB_ENUM(item_secure_dev_epass) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_secure_dev_epass_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_secure_dev_epass_PassNo = 0,
  item_secure_dev_epass_PassYes = 1,
  item_secure_dev_epass_PassDeny = 2,
  item_secure_dev_epass_PassUnknown = 3,
};

GPBEnumDescriptor *item_secure_dev_epass_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_secure_dev_epass_IsValidValue(int32_t value);

#pragma mark - Enum item_secure_dev_estatus

typedef GPB_ENUM(item_secure_dev_estatus) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_secure_dev_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_secure_dev_estatus_Normal = 0,
  item_secure_dev_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_secure_dev_estatus_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_secure_dev_estatus_IsValidValue(int32_t value);

#pragma mark - IxItemSecureDevRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxItemSecureDevRoot : GPBRootObject
@end

#pragma mark - item_secure_dev

typedef GPB_ENUM(item_secure_dev_FieldNumber) {
  item_secure_dev_FieldNumber_Id_p = 1,
  item_secure_dev_FieldNumber_Uuid = 2,
  item_secure_dev_FieldNumber_Uutime = 3,
  item_secure_dev_FieldNumber_Userid = 4,
  item_secure_dev_FieldNumber_DevToken = 5,
  item_secure_dev_FieldNumber_DevName = 6,
  item_secure_dev_FieldNumber_LastLoginTime = 7,
  item_secure_dev_FieldNumber_VerifyTime = 8,
  item_secure_dev_FieldNumber_VerifyMode = 9,
  item_secure_dev_FieldNumber_VerifyNo = 10,
  item_secure_dev_FieldNumber_Pass = 11,
  item_secure_dev_FieldNumber_Status = 12,
};

@interface item_secure_dev : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite) uint64_t userid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *devToken;

@property(nonatomic, readwrite, copy, null_resettable) NSString *devName;

@property(nonatomic, readwrite) uint64_t lastLoginTime;

@property(nonatomic, readwrite) uint64_t verifyTime;

@property(nonatomic, readwrite) uint32_t verifyMode;

@property(nonatomic, readwrite, copy, null_resettable) NSString *verifyNo;

@property(nonatomic, readwrite) uint32_t pass;

@property(nonatomic, readwrite) item_secure_dev_estatus status;

@end

/**
 * Fetches the raw value of a @c item_secure_dev's @c status property, even
 * if the value was not defined by the enum at the time the code was generated.
 **/
int32_t item_secure_dev_Status_RawValue(item_secure_dev *message);
/**
 * Sets the raw value of an @c item_secure_dev's @c status property, allowing
 * it to be set to a value that was not defined by the enum at the time the code
 * was generated.
 **/
void Setitem_secure_dev_Status_RawValue(item_secure_dev *message, int32_t value);

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
