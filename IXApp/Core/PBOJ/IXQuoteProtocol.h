#pragma once

#pragma pack(push) 
#pragma pack(1)

typedef struct {
    uint16  length;     // 2 bytes
    uint16  cmd;        // 2 bytes
    uint16  crc;        // 2 bytes
    uint32  seq;        // 4 bytes
}proto_header;

typedef struct {
    uint16  length;     // 2 bytes
    uint16  cmd;        // 2 bytes
}proto_simple_header;


enum { HEADER_LENGTH = sizeof(proto_header) };
enum { SIMPLE_HEADER_LENGTH = sizeof(proto_simple_header) };

#pragma pack(pop)

typedef enum {
	CMD_QUOTE_INITIAL = 0,
	
	// proto
	CMD_QUOTE_HB            = 0x1000,  // heart beat
	CMD_QUOTE_PUB           = 0x1009,
    
	CMD_QUOTE_PUB_DETAIL    = 0x1002,
	CMD_QUOTE_LOGIN         = 0x1003,
    
	CMD_QUOTE_SUB           = 0x1011,
	CMD_QUOTE_UNSUB         = 0x1012,
    

	CMD_QUOTE_SUB_DETAIL    = 0x1006,
	CMD_QUOTE_UNSUB_DETAIL  = 0x1007,
	CMD_QUOTE_CLOSE_PRICE   = 0x1008,

	CMD_QUOTE_KDATA         = 0x1010,
	CMD_QUOTE_MAX,
}PROTO_QUOTE_COMMAND;

typedef enum {
    UNKNOWN = -1,
    MIN_1   = 0,    //1分
    MIN_5   = 1,    //5分
    HOUR_1  = 2,    //60分
    DAY     = 3,    //日k
    MIN_15  = 4,    //15分
    MIN_30  = 5,    //30分
    HOUR_4  = 6,    //4小时
    //以下两个type为处理k线跳动而添加，实际上服务器返回的k线数据并不包含这两个type
    Week_1  = 10,
    Month_1 = 11
}K_TYPE;
