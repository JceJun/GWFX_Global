// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_room_cost.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

#import <stdatomic.h>

#import "IxItemRoomCost.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - IxItemRoomCostRoot

@implementation IxItemRoomCostRoot

// No extensions in the file and no imports, so no need to generate
// +extensionRegistry.

@end

#pragma mark - IxItemRoomCostRoot_FileDescriptor

static GPBFileDescriptor *IxItemRoomCostRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_room_cost

@implementation item_room_cost

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic capacity;
@dynamic diamonds;
@dynamic status;

typedef struct item_room_cost__storage_ {
  uint32_t _has_storage_[1];
  uint32_t capacity;
  uint32_t diamonds;
  uint32_t status;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
} item_room_cost__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = item_room_cost_FieldNumber_Id_p,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(item_room_cost__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uuid",
        .dataTypeSpecific.className = NULL,
        .number = item_room_cost_FieldNumber_Uuid,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(item_room_cost__storage_, uuid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uutime",
        .dataTypeSpecific.className = NULL,
        .number = item_room_cost_FieldNumber_Uutime,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(item_room_cost__storage_, uutime),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "capacity",
        .dataTypeSpecific.className = NULL,
        .number = item_room_cost_FieldNumber_Capacity,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(item_room_cost__storage_, capacity),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "diamonds",
        .dataTypeSpecific.className = NULL,
        .number = item_room_cost_FieldNumber_Diamonds,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(item_room_cost__storage_, diamonds),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "status",
        .dataTypeSpecific.className = NULL,
        .number = item_room_cost_FieldNumber_Status,
        .hasIndex = 5,
        .offset = (uint32_t)offsetof(item_room_cost__storage_, status),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[item_room_cost class]
                                     rootClass:[IxItemRoomCostRoot class]
                                          file:IxItemRoomCostRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(item_room_cost__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

#pragma mark - Enum item_room_cost_estatus

GPBEnumDescriptor *item_room_cost_estatus_EnumDescriptor(void) {
  static _Atomic(GPBEnumDescriptor*) descriptor = nil;
  if (!descriptor) {
    static const char *valueNames =
        "Normal\000Deleted\000";
    static const int32_t values[] = {
        item_room_cost_estatus_Normal,
        item_room_cost_estatus_Deleted,
    };
    static const char *extraTextFormatInfo = "\002\000&\000\001\'\000";
    GPBEnumDescriptor *worker =
        [GPBEnumDescriptor allocDescriptorForName:GPBNSStringifySymbol(item_room_cost_estatus)
                                       valueNames:valueNames
                                           values:values
                                            count:(uint32_t)(sizeof(values) / sizeof(int32_t))
                                     enumVerifier:item_room_cost_estatus_IsValidValue
                              extraTextFormatInfo:extraTextFormatInfo];
    GPBEnumDescriptor *expected = nil;
    if (!atomic_compare_exchange_strong(&descriptor, &expected, worker)) {
      [worker release];
    }
  }
  return descriptor;
}

BOOL item_room_cost_estatus_IsValidValue(int32_t value__) {
  switch (value__) {
    case item_room_cost_estatus_Normal:
    case item_room_cost_estatus_Deleted:
      return YES;
    default:
      return NO;
  }
}


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
