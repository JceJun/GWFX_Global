// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_inform.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

@class item_session;
@class proto_inform;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Enum proto_inform_etype

typedef GPB_ENUM(proto_inform_etype) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  proto_inform_etype_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  proto_inform_etype_All = 0,
  proto_inform_etype_Account = 1,
  proto_inform_etype_Company = 2,
  proto_inform_etype_User = 3,
  proto_inform_etype_AcctType = 4,
  proto_inform_etype_AcctGroup = 5,
};

GPBEnumDescriptor *proto_inform_etype_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL proto_inform_etype_IsValidValue(int32_t value);

#pragma mark - IxProtoInformRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxProtoInformRoot : GPBRootObject
@end

#pragma mark - proto_inform

typedef GPB_ENUM(proto_inform_FieldNumber) {
  proto_inform_FieldNumber_Command = 1,
  proto_inform_FieldNumber_Seq = 2,
  proto_inform_FieldNumber_AccountidArray = 3,
  proto_inform_FieldNumber_Companyid = 4,
  proto_inform_FieldNumber_Userid = 5,
  proto_inform_FieldNumber_Data_p = 6,
  proto_inform_FieldNumber_Type = 7,
  proto_inform_FieldNumber_SessionType = 8,
  proto_inform_FieldNumber_SessionArray = 9,
  proto_inform_FieldNumber_Conn = 10,
  proto_inform_FieldNumber_AcctGroupid = 11,
};

@interface proto_inform : GPBMessage

@property(nonatomic, readwrite) uint32_t command;

@property(nonatomic, readwrite) uint32_t seq;

@property(nonatomic, readwrite, strong, null_resettable) GPBUInt64Array *accountidArray;
/** The number of items in @c accountidArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger accountidArray_Count;

@property(nonatomic, readwrite) uint64_t companyid;

@property(nonatomic, readwrite) uint64_t userid;

@property(nonatomic, readwrite, copy, null_resettable) NSData *data_p;

@property(nonatomic, readwrite) proto_inform_etype type;

@property(nonatomic, readwrite) uint32_t sessionType;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_session*> *sessionArray;
/** The number of items in @c sessionArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger sessionArray_Count;

@property(nonatomic, readwrite) uint64_t conn;

@property(nonatomic, readwrite) uint64_t acctGroupid;

@end

/**
 * Fetches the raw value of a @c proto_inform's @c type property, even
 * if the value was not defined by the enum at the time the code was generated.
 **/
int32_t proto_inform_Type_RawValue(proto_inform *message);
/**
 * Sets the raw value of an @c proto_inform's @c type property, allowing
 * it to be set to a value that was not defined by the enum at the time the code
 * was generated.
 **/
void Setproto_inform_Type_RawValue(proto_inform *message, int32_t value);

#pragma mark - proto_inform_list

typedef GPB_ENUM(proto_inform_list_FieldNumber) {
  proto_inform_list_FieldNumber_InformArray = 1,
};

@interface proto_inform_list : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<proto_inform*> *informArray;
/** The number of items in @c informArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger informArray_Count;

@end

#pragma mark - proto_forward

typedef GPB_ENUM(proto_forward_FieldNumber) {
  proto_forward_FieldNumber_Name = 1,
  proto_forward_FieldNumber_Data_p = 2,
  proto_forward_FieldNumber_Seq = 3,
};

@interface proto_forward : GPBMessage

@property(nonatomic, readwrite, copy, null_resettable) NSString *name;

@property(nonatomic, readwrite, copy, null_resettable) NSData *data_p;

@property(nonatomic, readwrite) uint32_t seq;

@end

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
