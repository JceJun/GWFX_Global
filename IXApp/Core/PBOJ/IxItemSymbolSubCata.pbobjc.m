// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_symbol_sub_cata.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

#import <stdatomic.h>

#import "IxItemSymbolSubCata.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - IxItemSymbolSubCataRoot

@implementation IxItemSymbolSubCataRoot

// No extensions in the file and no imports, so no need to generate
// +extensionRegistry.

@end

#pragma mark - IxItemSymbolSubCataRoot_FileDescriptor

static GPBFileDescriptor *IxItemSymbolSubCataRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_symbol_sub_cata

@implementation item_symbol_sub_cata

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic accountid;
@dynamic name;
@dynamic status;

typedef struct item_symbol_sub_cata__storage_ {
  uint32_t _has_storage_[1];
  item_symbol_sub_cata_estatus status;
  NSString *name;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
  uint64_t accountid;
} item_symbol_sub_cata__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = item_symbol_sub_cata_FieldNumber_Id_p,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(item_symbol_sub_cata__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uuid",
        .dataTypeSpecific.className = NULL,
        .number = item_symbol_sub_cata_FieldNumber_Uuid,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(item_symbol_sub_cata__storage_, uuid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uutime",
        .dataTypeSpecific.className = NULL,
        .number = item_symbol_sub_cata_FieldNumber_Uutime,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(item_symbol_sub_cata__storage_, uutime),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "accountid",
        .dataTypeSpecific.className = NULL,
        .number = item_symbol_sub_cata_FieldNumber_Accountid,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(item_symbol_sub_cata__storage_, accountid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "name",
        .dataTypeSpecific.className = NULL,
        .number = item_symbol_sub_cata_FieldNumber_Name,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(item_symbol_sub_cata__storage_, name),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
      {
        .name = "status",
        .dataTypeSpecific.enumDescFunc = item_symbol_sub_cata_estatus_EnumDescriptor,
        .number = item_symbol_sub_cata_FieldNumber_Status,
        .hasIndex = 5,
        .offset = (uint32_t)offsetof(item_symbol_sub_cata__storage_, status),
        .flags = (GPBFieldFlags)(GPBFieldOptional | GPBFieldHasEnumDescriptor),
        .dataType = GPBDataTypeEnum,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[item_symbol_sub_cata class]
                                     rootClass:[IxItemSymbolSubCataRoot class]
                                          file:IxItemSymbolSubCataRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(item_symbol_sub_cata__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

int32_t item_symbol_sub_cata_Status_RawValue(item_symbol_sub_cata *message) {
  GPBDescriptor *descriptor = [item_symbol_sub_cata descriptor];
  GPBFieldDescriptor *field = [descriptor fieldWithNumber:item_symbol_sub_cata_FieldNumber_Status];
  return GPBGetMessageInt32Field(message, field);
}

void Setitem_symbol_sub_cata_Status_RawValue(item_symbol_sub_cata *message, int32_t value) {
  GPBDescriptor *descriptor = [item_symbol_sub_cata descriptor];
  GPBFieldDescriptor *field = [descriptor fieldWithNumber:item_symbol_sub_cata_FieldNumber_Status];
  GPBSetInt32IvarWithFieldInternal(message, field, value, descriptor.file.syntax);
}

#pragma mark - Enum item_symbol_sub_cata_estatus

GPBEnumDescriptor *item_symbol_sub_cata_estatus_EnumDescriptor(void) {
  static _Atomic(GPBEnumDescriptor*) descriptor = nil;
  if (!descriptor) {
    static const char *valueNames =
        "Normal\000Deleted\000";
    static const int32_t values[] = {
        item_symbol_sub_cata_estatus_Normal,
        item_symbol_sub_cata_estatus_Deleted,
    };
    static const char *extraTextFormatInfo = "\002\000&\000\001\'\000";
    GPBEnumDescriptor *worker =
        [GPBEnumDescriptor allocDescriptorForName:GPBNSStringifySymbol(item_symbol_sub_cata_estatus)
                                       valueNames:valueNames
                                           values:values
                                            count:(uint32_t)(sizeof(values) / sizeof(int32_t))
                                     enumVerifier:item_symbol_sub_cata_estatus_IsValidValue
                              extraTextFormatInfo:extraTextFormatInfo];
    GPBEnumDescriptor *expected = nil;
    if (!atomic_compare_exchange_strong(&descriptor, &expected, worker)) {
      [worker release];
    }
  }
  return descriptor;
}

BOOL item_symbol_sub_cata_estatus_IsValidValue(int32_t value__) {
  switch (value__) {
    case item_symbol_sub_cata_estatus_Normal:
    case item_symbol_sub_cata_estatus_Deleted:
      return YES;
    default:
      return NO;
  }
}


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
