// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_holiday.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Enum item_holiday_estatus

typedef GPB_ENUM(item_holiday_estatus) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_holiday_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_holiday_estatus_Normal = 0,
  item_holiday_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_holiday_estatus_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_holiday_estatus_IsValidValue(int32_t value);

#pragma mark - IxItemHolidayRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxItemHolidayRoot : GPBRootObject
@end

#pragma mark - item_holiday

typedef GPB_ENUM(item_holiday_FieldNumber) {
  item_holiday_FieldNumber_Id_p = 1,
  item_holiday_FieldNumber_Uuid = 2,
  item_holiday_FieldNumber_Uutime = 3,
  item_holiday_FieldNumber_EveryYear = 4,
  item_holiday_FieldNumber_FromTime = 5,
  item_holiday_FieldNumber_Totime = 6,
  item_holiday_FieldNumber_Description_p = 7,
  item_holiday_FieldNumber_Enable = 8,
  item_holiday_FieldNumber_NeedStopout = 9,
  item_holiday_FieldNumber_CancelOrder = 10,
  item_holiday_FieldNumber_SwapDays = 11,
  item_holiday_FieldNumber_SettlementDate = 12,
  item_holiday_FieldNumber_MarginSetpre = 13,
  item_holiday_FieldNumber_MarginTime = 14,
  item_holiday_FieldNumber_MarginSet = 15,
  item_holiday_FieldNumber_CreateTime = 16,
  item_holiday_FieldNumber_CreateUserid = 17,
  item_holiday_FieldNumber_ModiTime = 18,
  item_holiday_FieldNumber_ModiUserid = 19,
  item_holiday_FieldNumber_Reserve = 21,
  item_holiday_FieldNumber_HolidayCataid = 22,
  item_holiday_FieldNumber_Name = 23,
  item_holiday_FieldNumber_Status = 24,
  item_holiday_FieldNumber_MarginType = 25,
  item_holiday_FieldNumber_Tradable = 26,
};

@interface item_holiday : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite) BOOL everyYear;

@property(nonatomic, readwrite) uint64_t fromTime;

@property(nonatomic, readwrite) uint64_t totime;

@property(nonatomic, readwrite, copy, null_resettable) NSString *description_p;

@property(nonatomic, readwrite) BOOL enable;

@property(nonatomic, readwrite) BOOL needStopout;

@property(nonatomic, readwrite) BOOL cancelOrder;

@property(nonatomic, readwrite) uint32_t swapDays;

@property(nonatomic, readwrite) uint64_t settlementDate;

@property(nonatomic, readwrite) uint32_t marginSetpre;

@property(nonatomic, readwrite) uint32_t marginTime;

@property(nonatomic, readwrite) uint32_t marginSet;

@property(nonatomic, readwrite) uint64_t createTime;

@property(nonatomic, readwrite) uint64_t createUserid;

@property(nonatomic, readwrite) uint64_t modiTime;

@property(nonatomic, readwrite) uint64_t modiUserid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *reserve;

@property(nonatomic, readwrite) uint64_t holidayCataid;

@property(nonatomic, readwrite, copy, null_resettable) NSString *name;

@property(nonatomic, readwrite) item_holiday_estatus status;

@property(nonatomic, readwrite) uint32_t marginType;

@property(nonatomic, readwrite) BOOL tradable;

@end

/**
 * Fetches the raw value of a @c item_holiday's @c status property, even
 * if the value was not defined by the enum at the time the code was generated.
 **/
int32_t item_holiday_Status_RawValue(item_holiday *message);
/**
 * Sets the raw value of an @c item_holiday's @c status property, allowing
 * it to be set to a value that was not defined by the enum at the time the code
 * was generated.
 **/
void Setitem_holiday_Status_RawValue(item_holiday *message, int32_t value);

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
