// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_session.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

#import <stdatomic.h>

#import "IxItemSession.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - IxItemSessionRoot

@implementation IxItemSessionRoot

// No extensions in the file and no imports, so no need to generate
// +extensionRegistry.

@end

#pragma mark - IxItemSessionRoot_FileDescriptor

static GPBFileDescriptor *IxItemSessionRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_session

@implementation item_session

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic createTime;
@dynamic companyid;
@dynamic userid;
@dynamic accountGroupid;
@dynamic accountid;
@dynamic serverid;
@dynamic socket;
@dynamic type;
@dynamic token;
@dynamic expire;
@dynamic inuse;
@dynamic ip;
@dynamic options;
@dynamic version;

typedef struct item_session__storage_ {
  uint32_t _has_storage_[1];
  uint32_t type;
  uint32_t inuse;
  uint32_t options;
  uint32_t version;
  NSString *token;
  NSString *ip;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
  uint64_t createTime;
  uint64_t companyid;
  uint64_t userid;
  uint64_t accountGroupid;
  uint64_t accountid;
  uint64_t serverid;
  uint64_t socket;
  uint64_t expire;
} item_session__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Id_p,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(item_session__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uuid",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Uuid,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(item_session__storage_, uuid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uutime",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Uutime,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(item_session__storage_, uutime),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "createTime",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_CreateTime,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(item_session__storage_, createTime),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "companyid",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Companyid,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(item_session__storage_, companyid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "userid",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Userid,
        .hasIndex = 5,
        .offset = (uint32_t)offsetof(item_session__storage_, userid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "accountGroupid",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_AccountGroupid,
        .hasIndex = 6,
        .offset = (uint32_t)offsetof(item_session__storage_, accountGroupid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "accountid",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Accountid,
        .hasIndex = 7,
        .offset = (uint32_t)offsetof(item_session__storage_, accountid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "serverid",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Serverid,
        .hasIndex = 8,
        .offset = (uint32_t)offsetof(item_session__storage_, serverid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "socket",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Socket,
        .hasIndex = 9,
        .offset = (uint32_t)offsetof(item_session__storage_, socket),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "type",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Type,
        .hasIndex = 10,
        .offset = (uint32_t)offsetof(item_session__storage_, type),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "token",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Token,
        .hasIndex = 11,
        .offset = (uint32_t)offsetof(item_session__storage_, token),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
      {
        .name = "expire",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Expire,
        .hasIndex = 12,
        .offset = (uint32_t)offsetof(item_session__storage_, expire),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "inuse",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Inuse,
        .hasIndex = 13,
        .offset = (uint32_t)offsetof(item_session__storage_, inuse),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "ip",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Ip,
        .hasIndex = 14,
        .offset = (uint32_t)offsetof(item_session__storage_, ip),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
      {
        .name = "options",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Options,
        .hasIndex = 15,
        .offset = (uint32_t)offsetof(item_session__storage_, options),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "version",
        .dataTypeSpecific.className = NULL,
        .number = item_session_FieldNumber_Version,
        .hasIndex = 16,
        .offset = (uint32_t)offsetof(item_session__storage_, version),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[item_session class]
                                     rootClass:[IxItemSessionRoot class]
                                          file:IxItemSessionRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(item_session__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

#pragma mark - Enum item_session_etype

GPBEnumDescriptor *item_session_etype_EnumDescriptor(void) {
  static _Atomic(GPBEnumDescriptor*) descriptor = nil;
  if (!descriptor) {
    static const char *valueNames =
        "TypeInitial\000TypePc\000TypeApp\000TypeWeb\000TypeO"
        "thers\000TypeManager\000TypeAppIos\000TypeAppAndr"
        "oid\000TypeMt4\000";
    static const int32_t values[] = {
        item_session_etype_TypeInitial,
        item_session_etype_TypePc,
        item_session_etype_TypeApp,
        item_session_etype_TypeWeb,
        item_session_etype_TypeOthers,
        item_session_etype_TypeManager,
        item_session_etype_TypeAppIos,
        item_session_etype_TypeAppAndroid,
        item_session_etype_TypeMt4,
    };
    GPBEnumDescriptor *worker =
        [GPBEnumDescriptor allocDescriptorForName:GPBNSStringifySymbol(item_session_etype)
                                       valueNames:valueNames
                                           values:values
                                            count:(uint32_t)(sizeof(values) / sizeof(int32_t))
                                     enumVerifier:item_session_etype_IsValidValue];
    GPBEnumDescriptor *expected = nil;
    if (!atomic_compare_exchange_strong(&descriptor, &expected, worker)) {
      [worker release];
    }
  }
  return descriptor;
}

BOOL item_session_etype_IsValidValue(int32_t value__) {
  switch (value__) {
    case item_session_etype_TypeInitial:
    case item_session_etype_TypePc:
    case item_session_etype_TypeApp:
    case item_session_etype_TypeWeb:
    case item_session_etype_TypeOthers:
    case item_session_etype_TypeManager:
    case item_session_etype_TypeAppIos:
    case item_session_etype_TypeAppAndroid:
    case item_session_etype_TypeMt4:
      return YES;
    default:
      return NO;
  }
}

#pragma mark - Enum item_session_eoptions

GPBEnumDescriptor *item_session_eoptions_EnumDescriptor(void) {
  static _Atomic(GPBEnumDescriptor*) descriptor = nil;
  if (!descriptor) {
    static const char *valueNames =
        "OptNone\000OptNoWebLogin\000OptNoWebTrade\000OptN"
        "oAppLogin\000OptNoAppTrade\000";
    static const int32_t values[] = {
        item_session_eoptions_OptNone,
        item_session_eoptions_OptNoWebLogin,
        item_session_eoptions_OptNoWebTrade,
        item_session_eoptions_OptNoAppLogin,
        item_session_eoptions_OptNoAppTrade,
    };
    GPBEnumDescriptor *worker =
        [GPBEnumDescriptor allocDescriptorForName:GPBNSStringifySymbol(item_session_eoptions)
                                       valueNames:valueNames
                                           values:values
                                            count:(uint32_t)(sizeof(values) / sizeof(int32_t))
                                     enumVerifier:item_session_eoptions_IsValidValue];
    GPBEnumDescriptor *expected = nil;
    if (!atomic_compare_exchange_strong(&descriptor, &expected, worker)) {
      [worker release];
    }
  }
  return descriptor;
}

BOOL item_session_eoptions_IsValidValue(int32_t value__) {
  switch (value__) {
    case item_session_eoptions_OptNone:
    case item_session_eoptions_OptNoWebLogin:
    case item_session_eoptions_OptNoWebTrade:
    case item_session_eoptions_OptNoAppLogin:
    case item_session_eoptions_OptNoAppTrade:
      return YES;
    default:
      return NO;
  }
}


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
