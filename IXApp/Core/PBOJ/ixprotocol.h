#pragma once
#include "ixtype.h"

#define HEADER_TOKEN_LEN    32
#define STR_COMMENT_LEN     64
#define ERR_CODE_OFFSET     8

#pragma pack(push)
#pragma pack(1)

struct protocol_simple_header
{
    uint16  length;     // 2 bytes
    uint16  command;    // 2 bytes
    uint16  crc;        // 2 bytes
    uint32  seq;        // 4 bytes
};
enum { simple_header_length = sizeof(struct protocol_simple_header) };

struct protocol_proto
{
    struct protocol_simple_header header;
    char    Body[1];
};

struct protocol_header   // internal use only
{
    uint16  length;     // 2 bytes
    uint16  command;    // 2 bytes
    uint16  crc;        // 2 bytes
    uint64  id;
    char    token[HEADER_TOKEN_LEN];
};

#pragma pack(pop)
typedef enum
{
    CMD_INITIAL         = 0,
    
    // server
    CMD_SERVER_ADD      = 0x1001,
    CMD_SERVER_UPDATE,
    CMD_SERVER_DELETE,
    CMD_SERVER_GET,
    CMD_SERVER_LIST,
    CMD_SERVER_GET_TIME,
    
    
    // symbol
    CMD_SYMBOL_ADD      = 0x1101,
    CMD_SYMBOL_UPDATE,
    CMD_SYMBOL_DELETE,
    CMD_SYMBOL_GET,
    CMD_SYMBOL_LIST,
    CMD_SYMBOL_EXPIRE,
    CMD_SYMBOL_CANCEL_ORDER,
    
    
    // group
    CMD_GROUP_ADD       = 0x1201,
    CMD_GROUP_UPDATE,
    CMD_GROUP_DELETE,
    CMD_GROUP_GET,
    CMD_GROUP_LIST,
    
    
    // user
    CMD_USER_ADD        = 0x1301,
    CMD_USER_UPDATE,
    CMD_USER_DELETE,
    CMD_USER_GET,
    CMD_USER_LIST,
    CMD_USER_ONLINE_LIST,
    CMD_USER_LOGIN,
    CMD_USER_LOGOUT,
    CMD_USER_LOGIN_INFO,
    CMD_USER_LOGIN_ACCOUNT,
    CMD_USER_ACCOUNT_LIST,
    CMD_USER_KICK_OUT,
    CMD_USER_KEEPALIVE,
    CMD_USER_SERVER_TIME,
    CMD_USER_CHANGE_PASSWORD,
    CMD_USER_LOGIN_DATA_TOTAL,
    CMD_USER_LOGIN_DATA,
    CMD_USER_CHANGE_LOGIN_DATA,
    CMD_USER_CHANGE_INFO_DATA,
    CMD_USER_CHANGE_MGR_DATA,
    CMD_USER_CLOSE,
    CMD_USER_UNLOCK,
    CMD_USER_LOGIN_CRYPTO,
    CMD_USER_CHANGE_MT4_PASSWORD,      // change passwd from bo
    CMD_USER_CHANGE_MT4_PASSWORD_SYN,  // change passwd from MT4
    CMD_USER_SET_OPTIONS,
    CMD_USER_OPEN_ACCOUNT,
    
    // account
    CMD_ACCOUNT_ADD     = 0x1401,
    CMD_ACCOUNT_UPDATE, // async response
    CMD_ACCOUNT_DELETE,
    CMD_ACCOUNT_GET,
    CMD_ACCOUNT_LIST,
    CMD_ACCOUNT_MARGIN_CALL_NOTIFY,
    CMD_ACCOUNT_STOPOUT_NOTIFY,
    CMD_ACCOUNT_REAL_TIME,
    CMD_ACCOUNT_CHANGE_ENABLE_CLOSE_ONLY,
    CMD_ACCOUNT_CHANGE_GROUP,
    CMD_ACCOUNT_UPDATE_BASE,
    CMD_ACCOUNT_UPDATE_LPUSER,
    CMD_ACCOUNT_MT4_ADD,
    CMD_ACCOUNT_MT4_UPDATE,
    CMD_ACCOUNT_MT4_DELETE,
    CMD_ACCOUNT_REAL_TIME_MT4,
    CMD_ACCOUNT_CLEAR,
    CMD_ACCOUNT_ALIVE_CHECK,
    
    // balance
    CMD_BALANCE_ADD     = 0x1501,
    CMD_BALANCE_UPDATE,
    CMD_BALANCE_DELETE,
    CMD_BALANCE_GET,
    CMD_BALANCE_LIST,
    CMD_BALANCE_SYNC,
    CMD_BALANCE_CONFIRM,
    
    // session
    CMD_SESSION_GET     = 0x1601,
    CMD_SESSION_ADD,
    CMD_SESSION_LIST,
    CMD_SESSION_SYNC,
    CMD_SESSION_UPDATE,
    CMD_SESSION_DELETE,
    CMD_SESSION_REQUEST,
    
    // order
    CMD_ORDER_ADD       = 0x1701,
    CMD_ORDER_UPDATE,
    CMD_ORDER_CANCEL,
    CMD_ORDER_DELETE,
    CMD_ORDER_GET,
    CMD_ORDER_LIST,
    CMD_ORDER_ADD_SYS,     // used by stopout
    CMD_ORDER_UPDATE_SYS,  // used by pending2trade
    CMD_ORDER_CANCEL_SYS,  // used by pending2trade
    CMD_ORDER_DELAY,
    CMD_ORDER_CANCEL_ACCOUNT,
    CMD_ORDER_ADD_STP,
    CMD_ORDER_ADD_BATCH,
    
    // deal
    CMD_DEAL_ADD        = 0x1801,
    CMD_DEAL_UPDATE,
    CMD_DEAL_DELETE,
    CMD_DEAL_GET,
    CMD_DEAL_LIST,
    CMD_DEAL_ADD_STP,
    CMD_DEAL_ADD_BATCH,
    
    // position
    CMD_POSITION_ADD    = 0x1901,
    CMD_POSITION_UPDATE,
    CMD_POSITION_DELETE,
    CMD_POSITION_GET,
    CMD_POSITION_LIST,
    CMD_POSITION_SUMMARY,
    CMD_POSITION_CLOSE,
    CMD_POSITION_STOPLOSS_CLOSE,        // used by close pending
    CMD_POSITION_TAKEPROFIT_CLOSE,      // used by close pending
    CMD_POSITION_FORCE_CLOSE,  // used by stopout
    CMD_POSITION_CLOSE_ONE_ACCOUNT,
    CMD_POSITION_CALC_SWAP,
    CMD_POSITION_CLOSE_ROOM_ACCOUNT,
    CMD_POSITION_UPDATE_BATCH,
    CMD_POSITION_CLOSE_STRATEGY,
    
    // tick
    CMD_TICK_ADD        = 0x1A01,
    
    // manager
    CMD_MANAGER_ADD     = 0x1B01,
    CMD_MANAGER_UPDATE,
    CMD_MANAGER_DELETE,
    CMD_MANAGER_GET,
    CMD_MANAGER_LIST,
    CMD_MANAGER_LOGIN,
    CMD_MANAGER_LOGOUT,
    CMD_MANAGER_LOGIN_INFO,
    CMD_MANAGER_BO_LOGIN,
    
    
    // holiday
    CMD_HOLIDAY_ADD     = 0x1C01,
    CMD_HOLIDAY_UPDATE,
    CMD_HOLIDAY_DELETE,
    CMD_HOLIDAY_GET,
    CMD_HOLIDAY_LIST,
    
    // report
    
    // trade time
    CMD_SCHEDULE_ADD    = 0x1E01,
    CMD_SCHEDULE_UPDATE,
    CMD_SCHEDULE_DELETE,
    CMD_SCHEDULE_GET,
    CMD_SCHEDULE_LIST,
    
    
    // end of day
    CMD_ENDOFDAY_ADD    = 0x2101,
    CMD_ENDOFDAY_UPDATE,
    CMD_ENDOFDAY_DELETE,
    CMD_ENDOFDAY_GET,
    CMD_ENDOFDAY_LIST,
    
    // symbol settlement
    CMD_SYMBOL_SETTLEMENT_ADD   = 0x2191,
    CMD_SYMBOL_SETTLEMENT_GET,
    
    // feed client
    CMD_FEEDCLIENT_ADD      = 0x2201,
    CMD_FEEDCLIENT_UPDATE,
    CMD_FEEDCLIENT_DELETE,
    CMD_FEEDCLIENT_GET,
    CMD_FEEDCLIENT_LIST,
    
    // company
    CMD_COMPANY_ADD     = 0x2301,
    CMD_COMPANY_UPDATE,
    CMD_COMPANY_DELETE,
    CMD_COMPANY_GET,
    CMD_COMPANY_LIST,
    
    // account reference
    
    
    // commission
    CMD_COMMISSION_ADD      = 0x2501,
    CMD_COMMISSION_UPDATE,
    CMD_COMMISSION_DELETE,
    CMD_COMMISSION_GET,
    CMD_COMMISSION_LIST,
    
    // system version
    CMD_VERSION_ADD         = 0x2601,
    CMD_VERSION_UPDATE,
    CMD_VERSION_DELETE,
    CMD_VERSION_GET,
    CMD_VERSION_LIST,
    CMD_VERSION_VALIDATE,
    
    // feed symbol
    CMD_FEEDSYMBOL_ADD      = 0x2701,
    CMD_FEEDSYMBOL_UPDATE,
    CMD_FEEDSYMBOL_DELETE,
    CMD_FEEDSYMBOL_GET,
    CMD_FEEDSYMBOL_LIST,
    
    // system
    CMD_SYSTEM_QUERY_LOGIN          = 0x2801,
    CMD_SYSTEM_QUERY_LOGIN_ACCESS,
    CMD_SYSTEM_QUERY_LOGIN_PLATFORM,
    
    // login stat
    CMD_LOGINSTAT_GET           = 0x2901,
    CMD_LOGINSTAT_LIST,
    CMD_LOGINSTAT_SYNC,
    CMD_LOGINSTAT_DUMP,
    
    // commission 2
    CMD_COMMISSION2_ADD     = 0x3001,
    CMD_COMMISSION2_UPDATE,
    CMD_COMMISSION2_DELETE,
    CMD_COMMISSION2_GET,
    CMD_COMMISSION2_LIST,
    CMD_COMMISSION2_SYNC,
    
    // add code here
    
    // common
    CMD_SIMPLE_RES      = 0x4001,
    
    // room 0x4200 - 0x45FF
    CMD_ROOM_FORWARD    = 0x4200,
    // reserved 256*4, do not use!
    
    // back office, reserved for BO ret codes.
    CMD_BO_USER_LOGIN   = 0x5001,
    CMD_BO_USER_LOGOUT,
    CMD_BO_CREATE_USER,
    CMD_BO_UPDATE_USER,
    CMD_BO_OPEN_ACCOUNT,
    CMD_BO_UPDATE_ACCOUNT,
    CMD_BO_UPDATE_ACCOUNT_STATUS,
    CMD_BO_USER_INFO_ENQ,
    CMD_BO_USER_ACCOUNT_ENQ,            // 0x5009
    CMD_BO_BALANCE,
    CMD_BO_CREDIT,
    CMD_BO_CHARGE,
    CMD_BO_CORRECTION,
    CMD_BO_BONUS,
    CMD_BO_COMMISSION,
    CMD_BO_UPDATE_USER_PASSWORD,        // 0x5010
    CMD_BO_UPDATE_USER_STATUS,
    CMD_BO_UPDATE_ACCOUNT_CLOSEONLY,
    CMD_BO_UPDATE_ACCOUNT_GROUP,
    CMD_BO_UPDATE_USER_UNLOCK,
    CMD_BO_TICK_REAL_TIME_LIST,
    CMD_BO_UPDATE_ACCOUNT_TRADABLE,
    CMD_BO_MT4_ORDER_RESEND,
    
    // trade info notification
    CMD_BO_HEART_BEAT      = 0x5901,
    
    
    // reserved command and retcode for stp channel
    CMD_STP                = 0x6001,
    CMD_STP_MT4            = 0x6011,
    CMD_STP_INTG           = 0x6021,
    CMD_SYN_ACCESS         = 0x6101,
    CMD_SYN_MGR            = 0x6102,
    
    
    // Guest
    CMD_GUEST_SYMBOL_LIST  = 0x7001,
    
    //symbol cata
    CMD_SYMBOL_CATA_ADD    = 0x8001,
    CMD_SYMBOL_CATA_UPDATE,
    CMD_SYMBOL_CATA_DELETE,
    CMD_SYMBOL_CATA_LIST,
    
    //language
    CMD_LANGUAGE_ADD       =0x8101,
    CMD_LANGUAGE_UPDATE,
    CMD_LANGUAGE_DELETE,
    CMD_LANGUAGE_GET,
    CMD_LANGUAGE_LIST,
    
    //Symbol Margin Set
    CMD_SYMBOL_MARGIN_SET_ADD = 0x8201,
    CMD_SYMBOL_MARGIN_SET_UPDATE,
    CMD_SYMBOL_MARGIN_SET_DELETE,
    CMD_SYMBOL_MARGIN_SET_GET,
    CMD_SYMBOL_MARGIN_SET_LIST,
    
    //symbol sub cata
    CMD_SYMBOL_SUB_CATA_ADD = 0x8301,
    CMD_SYMBOL_SUB_CATA_UPDATE,
    CMD_SYMBOL_SUB_CATA_DELETE,
    CMD_SYMBOL_SUB_CATA_LIST,
    
    //symbol sub
    CMD_SYMBOL_SUB_ADD     = 0x8401,
    CMD_SYMBOL_SUB_UPDATE,
    CMD_SYMBOL_SUB_DELETE,
    CMD_SYMBOL_SUB_LIST,
    
    //account symbol cate
    CMD_ACCOUNT_SYMBOL_CATE_ADD = 0x8501,
    CMD_ACCOUNT_SYMBOL_CATE_UPDATE,
    CMD_ACCOUNT_SYMBOL_CATE_DELETE,
    
    //HolidayCata
    CMD_HOLIDAY_CATA_ADD    = 0x8601,
    CMD_HOLIDAY_CATA_UPDATE,
    CMD_HOLIDAY_CATA_DELETE,
    CMD_HOLIDAY_CATA_LIST,
    
    //HolidayMargin
    CMD_HOLIDAYMARGIN_ADD  = 0x8701,
    CMD_HOLIDAYMARGIN_UPDATE,
    CMD_HOLIDAYMARGIN_DELETE,
    
    //ScheduleCata
    //CMD_SCHEDULECATA_ADD   = 0x8801,
    //CMD_SCHEDULECATA_UPDATE,
    //CMD_SCHEDULECATA_DELETE,
    //CMD_SCHEDULECATA_LIST,
    //ScheduleMargin
    CMD_SCHEDULEMARGIN_ADD = 0x8901,
    CMD_SCHEDULEMARGIN_UPDATE,
    CMD_SCHEDULEMARGIN_DELETE,
    
    //item_account_balance
    CMD_ACCOUNTBALANCE_ADD = 0x8911,
    CMD_ACCOUNTBALANCE_UPDATE,
    CMD_ACCOUNTBALANCE_DELETE,
    CMD_ACCOUNTBALANCE_GET,
    
    //item_account_group_symbol_cata
    CMD_ACCOUNTGROUPSYMBOLCATA_ADD = 0x8921,
    CMD_ACCOUNTGROUPSYMBOLCATA_UPDATE,
    CMD_ACCOUNTGROUPSYMBOLCATA_DELETE,
    CMD_ACCOUNTGROUPSYMBOLCATA_GET,
    CMD_ACCOUNTGROUPSYMBOLCATA_LIST,
    CMD_ACCOUNTGROUPSYMBOLCATA_ADD_BATCH,
    CMD_ACCOUNTGROUPSYMBOLCATA_UPDATE_BATCH,
    CMD_ACCOUNTGROUPSYMBOLCATA_DELETE_BATCH,
    CMD_ACCOUNTGROUPSYMBOLCATA_MAGNIFY,
    
    //item_holiday_margin
    CMD_HOLIDAY_MARGIN_ADD = 0x8931,
    CMD_HOLIDAY_MARGIN_UPDATE,
    CMD_HOLIDAY_MARGIN_DELETE,
    CMD_HOLIDAY_MARGIN_GET,
    CMD_HOLIDAY_MARGIN_LIST,
    
    //item_schedule_cata
    CMD_SCHEDULE_CATA_ADD = 0X8941,
    CMD_SCHEDULE_CATA_UPDATE,
    CMD_SCHEDULE_CATA_DELETE,
    CMD_SCHEDULE_CATA_LIST,
    
    //item_schedule_margin
    CMD_SCHEDULE_MARGIN_ADD = 0X8951,
    CMD_SCHEDULE_MARGIN_UPDATE,
    CMD_SCHEDULE_MARGIN_DELETE,
    CMD_SCHEDULE_MARGIN_GET,
    CMD_SCHEDULE_MARGIN_LIST,
    
    //item_account_group
    CMD_ACCOUNT_GROUP_ADD = 0x8961,
    CMD_ACCOUNT_GROUP_UPDATE,
    CMD_ACCOUNT_GROUP_DELETE,
    CMD_ACCOUNT_GROUP_GET,
    CMD_ACCOUNT_GROUP_LIST,
    CMD_ACCOUNT_GROUP_UPDATE_LPUSER,
    
    //item_symbol_hot
    CMD_SYMBOL_HOT_ADD     = 0x8971,
    CMD_SYMBOL_HOT_UPDATE,
    CMD_SYMBOL_HOT_DELETE,
    CMD_SYMBOL_HOT_LIST,
    
    CMD_SYMBOL_LABEL_ADD  = 0x8981,
    CMD_SYMBOL_LABEL_UPDATE,
    CMD_SYMBOL_LABEL_DELETE,
    CMD_SYMBOL_LABEL_GET,
    CMD_SYMBOL_LABEL_LIST,
    
    //item_address
    CMD_ADDRESS_LIST     = 0x8991,
    
    //add by hong.tian
    //right_dividend
    CMD_RIGHT_DIVIDEND     = 0x9001,
    
    // item_online monitor
    CMD_ONLINE_MONITOR_GET = 0x9101,
    CMD_ONLINE_MONITOR_LIST,
    
    CMD_EXPIRE_SETTELMENT_PRICE_ADD = 0x9201,
    CMD_EXPIRE_SETTELMENT_PRICE_UPDATE,
    CMD_EXPIRE_SETTELMENT_PRICE_DELETE,
    CMD_EXPIRE_SETTELMENT_PRICE_GET,
    CMD_EXPIRE_SETTELMENT_PRICE_LIST,
    
    CMD_EOD_TIME_ADD     = 0x9301,
    CMD_EOD_TIME_UPDATE,
    CMD_EOD_TIME_DELETE,
    CMD_EOD_TIME_LIST,
    CMD_EOD_TIME_GET,
    
    CMD_VIRTUAL_CURRENCY_ADD  = 0x9401,
    CMD_VIRTUAL_CURRENCY_UPDATE,
    CMD_VIRTUAL_CURRENCY_DELETE,
    CMD_VIRTUAL_CURRENCY_LIST,
    CMD_VIRTUAL_CURRENCY_GET,
    
    CMD_QUOTE_DELAY_ADD = 0x9501,
    CMD_QUOTE_DELAY_UPDATE,
    CMD_QUOTE_DELAY_DELETE,
    CMD_QUOTE_DELAY_LIST,
    CMD_QUOTE_DELAY_GET,
    
    // item_tick_control
    CMD_TICK_CONTROL_ADD = 0x9601,
    CMD_TICK_CONTROL_UPDATE,
    CMD_TICK_CONTROL_DELETE,
    
    
    CMD_KLINE_REPAIR_ADD = 0x9701,
    CMD_KLINE_REPAIR_UPDATE,
    CMD_KLINE_REPAIR_DELETE,
    CMD_KLINE_REPAIR_GET,
    CMD_KLINE_REPAIR_LIST,
    
    CMD_SECURE_DEV_ADD = 0x9801,
    CMD_SECURE_DEV_UPDATE,
    CMD_SECURE_DEV_DELETE,
    CMD_SECURE_DEV_LIST,
    CMD_SECURE_DEV_GET,
    
    CMD_GROUP_SYMBOL_ADD = 0x9901,
    CMD_GROUP_SYMBOL_UPDATE,
    CMD_GROUP_SYMBOL_DELETE,
    CMD_GROUP_SYMBOL_LIST,
    CMD_GROUP_SYMBOL_GET,
    CMD_GROUP_SYMBOL_UPDATE_LPUSER,
    
    CMD_GROUP_SYMBOLCATA_ADD = 0x9A01,
    CMD_GROUP_SYMBOLCATA_UPDATE,
    CMD_GROUP_SYMBOLCATA_DELETE,
    CMD_GROUP_SYMBOLCATA_LIST,
    CMD_GROUP_SYMBOLCATA_GET,
    
    CMD_MT4_LICENSE_CHECK = 0x9B01,
    CMD_MT4_ORDER_NOTIFY  = 0x9B11,
    
    CMD_QUOTE_TICK_ADD    = 0x9C01,
    CMD_QUOTE_TICK_UPDATE = 0x9C02,
    CMD_QUOTE_TICK_DELETE = 0x9C03,
    
    CMD_MT4_CHECK_DEAL  = 0x9D01,
    
    CMD_LPCHANNEL_ADD = 0x9E01,
    CMD_LPCHANNEL_UPDATE,
    CMD_LPCHANNEL_DELETE,
    CMD_LPCHANNEL_LIST,
    CMD_LPCHANNEL_GET,
    
    CMD_LPCHANNEL_ACCOUNT_ADD = 0x9F01,
    CMD_LPCHANNEL_ACCOUNT_UPDATE,
    CMD_LPCHANNEL_ACCOUNT_DELETE,
    CMD_LPCHANNEL_ACCOUNT_LIST,
    CMD_LPCHANNEL_ACCOUNT_GET,
    
    CMD_LPCHANNEL_SYMBOL_ADD = 0xA001,
    CMD_LPCHANNEL_SYMBOL_UPDATE,
    CMD_LPCHANNEL_SYMBOL_DELETE,
    CMD_LPCHANNEL_SYMBOL_LIST,
    CMD_LPCHANNEL_SYMBOL_GET,
    
    CMD_LPCHANNEL_ACCOUNT_SYMBOL_ADD = 0xA101,
    CMD_LPCHANNEL_ACCOUNT_SYMBOL_UPDATE,
    CMD_LPCHANNEL_ACCOUNT_SYMBOL_DELETE,
    CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST,
    CMD_LPCHANNEL_ACCOUNT_SYMBOL_GET,
    
    CMD_LPIB_BIND_ADD = 0xA201,
    CMD_LPIB_BIND_UPDATE,
    CMD_LPIB_BIND_DELETE,
    CMD_LPIB_BIND_LIST,
    CMD_LPIB_BIND_GET,
    
    CMD_MAX,
    
    
} PROTOCOL_HEADER_COMMAND;

typedef enum
{
    RET_OK         = 0,
    RET_FAILED,
    // RET_ROOM_RESERVED, 2-> 1000
    // server
    RET_SERVER_NOT_FOUND                 = CMD_SERVER_ADD                 << 8,
    RET_SERVER_ALREADY_EXIST,
    // symbol
    RET_SYMBOL_NOT_FOUND                 = CMD_SYMBOL_ADD                 << 8,
    RET_SYMBOL_ALREADY_EXIST,
    RET_SYMBOL_NOT_VALID,
    RET_SYMBOL_BEYOND_MAX_VOLUME,
    RET_SYMBOL_ROLLBACK_VOLUME_FAILED,
    RET_SYMBOL_NOT_TRADABLE,
    RET_SYMBOL_NOT_ENABLE,
    RET_SYMBOL_CLOSE_ONLY,
    RET_SYMBOL_NOT_STARTED,
    RET_SYMBOL_NOT_EXPIRED,
    RET_SYMBOL_EXPIRED,
    RET_SYMBOL_HAS_OPEN_POSITIONS,
    RET_SYMBOL_HAS_OPEN_ORDERS,
    RET_SYMBOL_GET_FAILED,
    RET_SYMBOL_BUY_ONLY,
    RET_SYMBOL_SELL_ONLY,
    RET_SYMBOL_NOT_COVER,
    RET_SYMBOL_PROTO_NOT_VALID,
    RET_SYMBOL_TOKEN_NOT_VALID,
    RET_SYMBOL_INSERT_FAILED,
    RET_SYMBOL_CHANNELID_NOT_VALID,
    // group
    RET_GROUP_NOT_FOUND                  = CMD_GROUP_ADD                  << 8,
    RET_GROUP_ALREADY_EXIST,
    // user
    RET_USER_NOT_FOUND                   = CMD_USER_ADD                   << 8,
    RET_USER_ALREADY_EXIST,
    RET_USER_NOT_VALID,
    RET_USER_ITEM_EMPTY,
    RET_USER_TOKEN_NOT_VALID,
    RET_USER_INSERT_FAILED,
    RET_USER_INSERT_ACCOUNT_FAILED,
    RET_USER_LOAD_FAILED,
    RET_USER_SAVE_FAILED,
    RET_USER_NAME_NOT_VALID,
    RET_USER_EMAIL_NOT_VALID,
    RET_USER_PHONE_NOT_VALID,
    RET_USER_PWD_NOT_VALID,
    RET_USER_COMPANY_NOT_VALID,
    RET_USER_ACCOUNT_EMPTY,
    RET_USER_ACCOUNT_NOT_VALID,
    RET_USER_ACCOUNT_GROUP_NOT_VALID,
    RET_USER_WRITE_SESSION_FAILED,
    RET_USER_GET_SESSION_FAILED,
    RET_USER_USERID_NOT_VALID,
    RET_USER_ACCOUNTID_NOT_VALID,
    RET_USER_SESSION_TYPE_NOT_VALID,
    RET_USER_IP_NOT_VALID,
    RET_USER_PROTO_NOT_VALID,
    RET_USER_LPUSER_NOT_FOUND,
    RET_USER_STATUS_NOT_VALID,
    RET_USER_EMAIL_SIZE_NOT_VALID,
    RET_USER_PHONE_SIZE_NOT_VALID,
    RET_USER_LOGIN_NAME_SIZE_NOT_VALID,
    RET_USER_QQ_SIZE_NOT_VALID,
    RET_USER_WECHAT_SIZE_NOT_VALID,
    RET_USER_WEIBO_SIZE_NOT_VALID,
    RET_USER_FACEBOOK_SIZE_NOT_VALID,
    RET_USER_GOOGLE_SIZE_NOT_VALID,
    RET_USER_NAME_SIZE_NOT_VALID,
    RET_USER_COUNTRY_SIZE_NOT_VALID,
    RET_USER_STATE_SIZE_NOT_VALID,
    RET_USER_CITY_SIZE_NOT_VALID,
    RET_USER_ZIPCODE_SIZE_NOT_VALID,
    RET_USER_ADDRESS_SIZE_NOT_VALID,
    RET_USER_LANG_SIZE_NOT_VALID,
    RET_USER_LPMIN_VOLUME_LTZERO,
    RET_USER_WECHAT_URL_FAILED,
    RET_USER_WECHAT_PARSE_FAILED,
    RET_USER_WECHAT_RETWITH_ERROR,
    RET_USER_WECHAT_NO_OPENID,
    RET_USER_HAS_NOT_VIRTUAL_ACCOUNT,
    RET_USER_IS_INUSE,
    RET_USER_STATUS_DELETE,
    RET_USER_HAD_DELETE,
    RET_USER_IS_LOCKED,
    RET_USER_NO_LOCKED,
    RET_USER_CODEC_KEY_NOT_VALID,
    RET_USER_LOGIN_UNSECURE_DEV,
    RET_USER_LPUSER_NOT_VALID,
    RET_USER_SECURE_DEV_SIZE_NOT_VALID,
    RET_USER_SECURE_DEV_ADD_FAILED,
    RET_USER_SECURE_DEV_UPDATE_FAILED,
    RET_USER_LOGIN_WEB_NOT_ALLOW,
    RET_USER_WECHAT_NO_UNIONID,
    RET_USER_WECHAT_NO_NICKNAME,
    RET_USER_REASON_NOT_VALID,
    RET_USER_MANAGER_NOT_VALID,
    RET_USER_ACCOUNT_GROUP_NO_LOGIN,
    RET_USER_CHECK_MASTER_PASSWD_FAILED,
    RET_USER_CHECK_INVESTOR_PASSWD_FAILED,
    RET_USER_PASSWD_SEND_MT4_FAILED,
    RET_USER_LPUSER_NOT_BIND,
    RET_USER_GET_FAILED,
    RET_USER_SET_OPTIONS_FAILED,
    RET_USER_OPEN_ACCOUNT,
    
    // account
    RET_ACCOUNT_NOT_FOUND                = CMD_ACCOUNT_ADD                << 8,
    RET_ACCOUNT_ALREADY_EXIST,
    RET_ACCOUNT_LOAD_FAILED,
    RET_ACCOUNT_SAVE_FAILED,
    RET_ACCOUNT_NOT_VALID,
    RET_ACCOUNT_TOKEN_NOT_VALID,
    RET_ACCOUNT_ITEM_EMPTY,
    RET_ACCOUNT_INSERT_FAILED,
    RET_ACCOUNT_USERID_NOT_VALID,
    RET_ACCOUNT_ACCOUNT_GROUP_NOT_VALID,
    RET_ACCOUNT_TYPE_NOT_VALID,
    RET_ACCOUNT_PROTO_NOT_VALID,
    RET_ACCOUNT_USER_STATUS_NOT_VALID,
    RET_ACCOUNT_GET_USER_NOT_FAILED,
    RET_ACCOUNT_GET_LPUSER_NOT_VALID,
    RET_ACCOUNT_STATUS_DELETE,
    RET_ACCOUNT_HAD_DELETE,
    RET_ACCOUNT_UPDATE_LPUSER_SIZE_NOT_VALID,
    RET_ACCOUNT_UPDATE_LPUSER_FAILED,
    RET_ACCOUNT_HAS_BALANCE,
    RET_ACCOUNT_HAS_OPEN_POSITIONS,
    RET_ACCOUNT_HAS_OPEN_ORDERS,
    RET_ACCOUNT_GET_FAILED,
    RET_ACCOUNT_HAS_UNFROZEN_BALANCES,
    RET_ACCOUNT_NO_ENABLE,
    
    
    // balance
    RET_BALANCE_NOT_FOUND                = CMD_BALANCE_ADD                << 8,
    RET_BALANCE_ALREADY_EXIST,
    // session
    RET_SESSION_NOT_FOUND                = CMD_SESSION_ADD                << 8,
    RET_SESSION_ALREADY_EXIST,
    // order
    RET_ORDER_NOT_FOUND                  = CMD_ORDER_ADD                  << 8,
    RET_ORDER_ALREADY_EXIST,
    RET_ORDER_INSERT_FAILED,
    RET_ORDER_GET_FAILED,
    RET_ORDER_CALC_PNL_FAILED,
    RET_ORDER_SAVE_FAILED,
    RET_ORDER_NOT_VALID,
    RET_ORDER_PRICE_NOT_VALID,
    RET_ORDER_STOP_LOSS_NOT_VALID,
    RET_ORDER_TAKE_PROFIT_NOT_VALID,
    RET_ORDER_PRICE_RANGE_NOT_VALID,
    RET_ORDER_VOLUME_NOT_VALID,
    RET_ORDER_STOP_LEVEL_NOT_VALID,
    RET_ORDER_CALC_COMM_FAILED,
    RET_ORDER_ALLOCATE_ID_FAILED,
    RET_ORDER_ADD_TO_MEM_FAILED,
    RET_ORDER_DIRECTION_NOT_VALID,
    RET_ORDER_LIMIT_BUY_NOT_TOUCHED,
    RET_ORDER_LIMIT_SELL_NOT_TOUCHED,
    RET_ORDER_STOP_BUY_NOT_TOUCHED,
    RET_ORDER_STOP_SELL_NOT_TOUCHED,
    RET_ORDER_BALANCE_NOT_ENOUGH,
    RET_ORDER_UPDATE_FAILED,
    RET_ORDER_REQUEST_PRICE_NOT_VALID,
    RET_ORDER_HEADER_EMPTY,
    RET_ORDER_ACCOUNTID_ZERO,
    RET_ORDER_TOKEN_NOT_VALID,
    RET_ORDER_DIRECTION_BUY_ONLY,
    RET_ORDER_DIRECTION_SELL_ONLY,
    RET_ORDER_CANCEL_ACCOUNT_FAILED,
    RET_ORDER_CANCEL_ACCOUNT_PARTIAL_FAILED,
    RET_ORDER_CLOSE_BY_DIRECTION_NOT_VALID,
    RET_ORDER_CLOSE_BY_SYMBOL_NOT_VALID,
    RET_ORDER_CLOSE_BY_LPUSER_NOT_VALID,
    RET_ORDER_CLOSE_BY_MODE_NOT_VALID,
    RET_ORDER_MT4_DEAL_PARTIAL,
    RET_ORDER_POSITION_NOT_VALID,
    RET_ORDER_TYPE_NOT_VALID,
    RET_ORDER_ACCOUNT_NOT_VALID,
    RET_ORDER_GET_PRICE_FAILED,
    RET_ORDER_ACCOUNT_GROUP_NO_TRADE,
    RET_ORDER_DEALING,
    RET_ORDER_USER_NO_TRADE,
    
    // deal
    RET_DEAL_NOT_FOUND                   = CMD_DEAL_ADD                   << 8,
    RET_DEAL_ALREADY_EXIST,
    RET_DEAL_ALLOCATE_ID_FAILED,
    RET_DEAL_GET_FAILED,
    RET_DEAL_HEADER_EMPTY,
    RET_DEAL_TOKEN_NOT_VALID,
    // position
    RET_POSITION_NOT_FOUND               = CMD_POSITION_ADD               << 8,
    RET_POSITION_ALREADY_EXIST,
    RET_POSITION_ALLOCATE_ID_FAILED,
    RET_POSITION_GET_FAILED,
    RET_POSITION_CALC_PNL_FAILED,
    RET_POSITION_CALC_MARGIN_FAILED,
    RET_POSITION_NOT_VALID,
    RET_POSITION_STOPLOSS_NOT_VALID,
    RET_POSITION_HEADER_EMPTY,
    RET_POSITION_TOKEN_NOT_VALID,
    RET_POSITION_STOPOUT_NOT_TOUCH0,
    RET_POSITION_STOPOUT_NOT_TOUCH,
    RET_POSITION_NO_STOPOUT,
    // tick
    RET_TICK_NOT_FOUND                   = CMD_TICK_ADD                   << 8,
    RET_TICK_ALREADY_EXIST,
    RET_TICK_TIMEOUT,
    RET_TICK_DEEP_VOLUME_NOT_ENOUGH,
    RET_TICK_SIZE_INVALID,
    RET_TICK_LOAD_FAILED,
    // manager
    RET_MANAGER_NOT_FOUND                = CMD_MANAGER_ADD                << 8,
    RET_MANAGER_ALREADY_EXIST,
    // holiday
    RET_HOLIDAY_NOT_FOUND                = CMD_HOLIDAY_ADD                << 8,
    RET_HOLIDAY_ALREADY_EXIST,
    // schedule
    RET_SCHEDULE_NOT_FOUND               = CMD_SCHEDULE_ADD               << 8,
    RET_SCHEDULE_ALREADY_EXIST,
    RET_SCHEDULE_NOT_VALID,
    // endofday
    RET_ENDOFDAY_NOT_FOUND               = CMD_ENDOFDAY_ADD               << 8,
    RET_ENDOFDAY_ALREADY_EXIST,
    // symbol_settlement
    RET_SYMBOL_SETTLEMENT_NOT_FOUND      = CMD_SYMBOL_SETTLEMENT_ADD      << 8,
    RET_SYMBOL_SETTLEMENT_ALREADY_EXIST,
    // company
    RET_COMPANY_NOT_FOUND                = CMD_COMPANY_ADD                << 8,
    RET_COMPANY_ALREADY_EXIST,
    RET_COMPANY_NOT_VALID,
    RET_COMPANY_NAME_EMPTY,
    RET_COMPANY_NAME_TOO_LONG,
    RET_COMPANY_COMPANY_TOKEN_EMPTY,
    RET_COMPANY_COMPANY_TOKEN_TOO_LONG,
    RET_COMPANY_TOKEN_NOT_VALID,
    RET_COMPANY_INSERT_FAILED,
    RET_COMPANY_UPDATE_FAILED,
    RET_COMPANY_ACCOUNT_GROUP_FAILED,
    RET_COMPANY_ITEM_EMPTY,
    RET_COMPANY_PROTO_NOT_VALID,
    RET_COMPANY_STATUS_DELETE,
    RET_COMPANY_GET_FAILED,
    RET_COMPANY_HAD_DELETE,
    RET_COMPANY_MODE_NOT_SUPPORT,
    
    // commission
    RET_COMMISSION_NOT_FOUND             = CMD_COMMISSION_ADD             << 8,
    RET_COMMISSION_ALREADY_EXIST,
    // version
    RET_VERSION_NOT_FOUND                = CMD_VERSION_ADD                << 8,
    RET_VERSION_ALREADY_EXIST,
    // loginstat
    // RET_LOGINSTAT_NOT_FOUND              = CMD_LOGINSTAT_ADD              << 8,
    // RET_LOGINSTAT_ALREADY_EXIST,
    RET_ROOM_NOT_FOUND                   = CMD_ROOM_FORWARD               << 8,
    RET_ROOM_ALREADY_EXIST,
    RET_ROOM_LOAD_FAILED,
    RET_ROOM_LOAD_SYMBOLS_FAILED,
    RET_ROOM_LOAD_ITEM_FAILED,
    RET_ROOM_ACCOUNT_NON_TRADABLE,
    RET_ROOM_SYMBOL_NOT_FOUND,
    RET_ROOM_SYMBOL_NOT_TRADABLE,
    RET_ROOM_SYMBOL_NOT_STARTED,
    RET_ROOM_SYMBOL_NOT_EXPIRED,
    RET_ROOM_SYMBOL_EXPIRED,
    
    RET_BO_RESERVED                      = CMD_BO_USER_LOGIN              << 8, // = 0x5001
    // 0x500100 -> 0x5004FF reserved for bo. DO NOT USE!!!
    
    // below is for stp.
    RET_STP_CHANNEL_TIMEOUT              = CMD_STP  << 8,
    
    RET_STP_MT4_TIMEOUT                  = CMD_STP_MT4  << 8,
    
    RET_STP_INTG_TIMEOUT                 = CMD_STP_INTG  << 8,
    
    CMD_SYN_MGR_TIMEOUT                  = CMD_SYN_MGR << 8,
    CMD_SYN_MGR_SEND_FAILED,
    
    
    
    // symbol_cata
    RET_SYMBOL_CATA_NOT_FOUND            = CMD_SYMBOL_CATA_ADD            << 8,
    RET_SYMBOL_CATA_ALREADY_EXIST,
    RET_SYMBOL_CATA_NAME_EMPTY,
    RET_SYMBOL_CATA_NAME_TOO_LONG,
    RET_SYMBOL_CATA_TOKEN_NOT_VALID,
    RET_SYMBOL_CATA_ITEM_EMPTY,
    RET_SYMBOL_CATA_INSERT_FAILED,
    RET_SYMBOL_CATA_UPDATE_FAILED,
    RET_SYMBOL_CATA_GET_FAILED,
    RET_SYMBOL_CATA_PROTO_NOT_VALID,
    RET_SYMBOL_CATA_NOT_VALID,
    RET_SYMBOL_CATA_STATUS_DELETE,
    RET_SYMBOL_CATA_HAD_DELETE,
    RET_SYMBOL_CATA_NOT_COVER,
    RET_SYMBOL_CATA_HAS_OPEN_POSITIONS,
    RET_SYMBOL_CATA_HAS_OPEN_ORDERS,
    
    // language
    RET_LANGUAGE_NOT_FOUND               = CMD_LANGUAGE_ADD               << 8,
    RET_LANGUAGE_ALREADY_EXIST,
    // symbol_margin_set
    RET_SYMBOL_MARGIN_SET_NOT_FOUND      = CMD_SYMBOL_MARGIN_SET_ADD      << 8,
    RET_SYMBOL_MARGIN_SET_ALREADY_EXIST,
    // symbol_sub_cata
    RET_SYMBOL_SUB_CATA_NOT_FOUND        = CMD_SYMBOL_SUB_CATA_ADD        << 8,
    RET_SYMBOL_SUB_CATA_ALREADY_EXIST,
    // symbol_sub
    RET_SYMBOL_SUB_NOT_FOUND             = CMD_SYMBOL_SUB_ADD             << 8,
    RET_SYMBOL_SUB_ALREADY_EXIST,
    RET_SYMBOL_SUB_SYMBOL_NOT_VALID,
    RET_SYMBOL_SUB_INSET_FAILED,
    RET_SYMBOL_SUB_UPDATE_FAILED,
    RET_SYMBOL_SUB_DELETE_FAILED,
    RET_SYMBOL_SUB_STATUS_DELETE,
    
    // account_symbol_cata
    RET_ACCOUNT_SYMBOL_CATA_NOT_FOUND    = CMD_ACCOUNT_SYMBOL_CATE_ADD    << 8,
    RET_ACCOUNT_SYMBOL_CATA_ALREADY_EXIST,
    // holiday_cata
    RET_HOLIDAY_CATA_NOT_FOUND           = CMD_HOLIDAY_CATA_ADD           << 8,
    RET_HOLIDAY_CATA_ALREADY_EXIST,
    // holidaymargin
    RET_HOLIDAYMARGIN_NOT_FOUND          = CMD_HOLIDAYMARGIN_ADD          << 8,
    RET_HOLIDAYMARGIN_ALREADY_EXIST,
    // schedulemargin
    RET_SCHEDULEMARGIN_NOT_FOUND         = CMD_SCHEDULEMARGIN_ADD         << 8,
    RET_SCHEDULEMARGIN_ALREADY_EXIST,
    // accountbalance
    RET_ACCOUNTBALANCE_NOT_FOUND         = CMD_ACCOUNTBALANCE_ADD         << 8,
    RET_ACCOUNTBALANCE_ALREADY_EXIST,
    RET_ACCOUNTBALANCE_TOKEN_NOT_VALID,
    RET_ACCOUNTBALANCE_ITEM_IS_EMPTY,
    // accountgroupsymbolcata
    RET_ACCOUNTGROUPSYMBOLCATA_NOT_FOUND = CMD_ACCOUNTGROUPSYMBOLCATA_ADD << 8,
    RET_ACCOUNTGROUPSYMBOLCATA_ALREADY_EXIST,
    RET_ACCOUNTGROUPSYMBOLCATA_NOT_VALID,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_NOT_TRADABLE,
    RET_ACCOUNTGROUPSYMBOLCATA_TOKEN_INVALID,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_REPEATED,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOLCATA_REPEATED,
    RET_ACCOUNTGROUPSYMBOLCATA_ACCOUNT_GROUP_INVALID,
    RET_ACCOUNTGROUPSYMBOLCATA_ACCOUNT_GROUP_DIFFERENT,
    RET_ACCOUNTGROUPSYMBOLCATA_INSERT_FAILED,
    RET_ACCOUNTGROUPSYMBOLCATA_ACCOUNT_GROUP_IDZERO,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_SYMBOLCATA_BOTH_SET,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_SYMBOLCATA_BOTH_UNSET,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_CATA_NOT_VALID,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_NOT_VALID,
    RET_ACCOUNTGROUPSYMBOLCATA_DELETED,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOLCATA_EXIST,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_EXIST,
    RET_ACCOUNTGROUPSYMBOLCATA_CANT_SET_SPREAD,
    RET_ACCOUNTGROUPSYMBOLCATA_CANT_SET_SPREAD_COMMISSION,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_NOT_FOUND,
    RET_ACCOUNTGROUPSYMBOLCATA_GET_FAILED,
    RET_ACCOUNTGROUPSYMBOLCATA_UPDATE_FAILED,
    RET_ACCOUNTGROUPSYMBOLCATA_HAS_OPEN_POSITION_OR_ORDER,
    RET_ACCOUNTGROUPSYMBOLCATA_MAGNIFY_ITEM_NOT_VALID,
    RET_ACCOUNTGROUPSYMBOLCATA_MAGNIFY_SYMBOL_ABSENT,
    RET_ACCOUNTGROUPSYMBOLCATA_SYMBOL_CATA_DIFFERENT,
    // holiday_margin
    RET_HOLIDAY_MARGIN_NOT_FOUND         = CMD_HOLIDAY_MARGIN_ADD         << 8,
    RET_HOLIDAY_MARGIN_ALREADY_EXIST,
    // schedule_cata
    RET_SCHEDULE_CATA_NOT_FOUND          = CMD_SCHEDULE_CATA_ADD          << 8,
    RET_SCHEDULE_CATA_ALREADY_EXIST,
    // schedule_margin
    RET_SCHEDULE_MARGIN_NOT_FOUND        = CMD_SCHEDULE_MARGIN_ADD        << 8,
    RET_SCHEDULE_MARGIN_ALREADY_EXIST,
    // account_group
    RET_ACCOUNT_GROUP_NOT_FOUND          = CMD_ACCOUNT_GROUP_ADD          << 8,
    RET_ACCOUNT_GROUP_ALREADY_EXIST,
    RET_ACCOUNT_GROUP_NOT_VALID,
    RET_ACCOUNT_GROUP_PROTO_NOT_VALID,
    RET_ACCOUNT_GROUP_TOKEN_NOT_VALID,
    RET_ACCOUNT_GROUP_GET_FAILED,
    RET_ACCOUNT_GROUP_HAD_DELETE,
    RET_ACCOUNT_GROUP_UPDATE_FAILED,
    RET_ACCOUNT_GROUP_UPDATE_LPUSER_SIZE_NOT_VALID,
    RET_ACCOUNT_GROUP_UPDATE_LPUSER_FAILED,
    RET_ACCOUNT_GROUP_EMPTY,
    RET_ACCOUNT_GROUP_CURRENCY_NOT_VALID,
    RET_ACCOUNT_GROUP_TYPE_NOT_VALID,
    RET_ACCOUNT_GROUP_COMPANY_NOT_VALID,
    RET_ACCOUNT_GROUP_NOT_CHANGED,
    RET_ACCOUNT_GROUP_NOT_BIND_LPUSER,
    RET_ACCOUNT_GROUP_HAD_BIND_LPUSER,
    
    // symbol_hot
    RET_SYMBOL_HOT_NOT_FOUND             = CMD_SYMBOL_HOT_ADD                << 8,
    RET_SYMBOL_HOT_ALREADY_EXIST,
    
    // address
    RET_ADDRESS_NOT_FOUND             = CMD_ADDRESS_LIST                     << 8,
    
    RET_RIGHT_DIVIDEND_NOT_FOUND      =  CMD_RIGHT_DIVIDEND                  << 8,
    
    RET_ONLINE_MONITOR_NOT_FOUND      =  CMD_ONLINE_MONITOR_GET              << 8,
    
    RET_EXPIRE_SETTELMENT_PRICE_NOT_FOUND =  CMD_EXPIRE_SETTELMENT_PRICE_ADD << 8,
    RET_EXPIRE_SETTELMENT_PRICE_ALREADY_EXIST,
    RET_EXPIRE_SETTELMENT_PRICE_TOKEN_NOT_VALID,
    RET_EXPIRE_SETTELMENT_PRICE_INSERT_FAILED,
    RET_EXPIRE_SETTELMENT_PRICE_STATUS_DELETE,
    RET_EXPIRE_SETTELMENT_PRICE_HAD_DELETE,
    RET_EXPIRE_SETTELMENT_PRICE_GET_FAILED,
    RET_EXPIRE_SETTELMENT_PRICE_NOT_VALID,
    RET_EXPIRE_SETTELMENT_PRICE_UPDATE_FAILED,
    
    RET_EOD_TIME_NOT_FOUND         =  CMD_EOD_TIME_ADD                      << 8,
    
    RET_VIRTUAL_CURRENCY_NOT_FOUND =  CMD_VIRTUAL_CURRENCY_ADD              << 8,
    
    RET_QUOTE_DELAY_NOT_FOUND      =  CMD_QUOTE_DELAY_ADD                   << 8,
    // tick_control
    RET_TICK_CONTROL_NOT_FOUND            = CMD_TICK_CONTROL_ADD            << 8,
    RET_TICK_CONTROL_ALREADY_EXIST,
    RET_TICK_CONTROL_NOT_EXIST,
    RET_TICK_CONTROL_TOKEN_NOT_VALID,
    RET_TICK_CONTROL_ITEM_EMPTY,
    RET_TICK_CONTROL_INSERT_FAILED,
    RET_TICK_CONTROL_UPDATE_FAILED,
    RET_TICK_CONTROL_DELETE_FAILED,
    RET_TICK_CONTROL_GET_FAILED,
    RET_TICK_CONTROL_PROTO_NOT_VALID,
    RET_TICK_CONTROL_NOT_VALID,
    RET_TICK_CONTROL_STATUS_DELETE,
    RET_TICK_CONTROL_HAD_DELETE,
    
    RET_GROUP_SYMBOL_NOT_FOUND            = CMD_GROUP_SYMBOL_ADD            << 8,
    RET_GROUP_SYMBOL_ALREADY_EXIST,
    RET_GROUP_SYMBOL_NOT_TRADABLE,
    RET_GROUP_SYMBOL_TOKEN_INVALID,
    RET_GROUP_SYMBOL_ACCOUNT_GROUP_INVALID,
    RET_GROUP_SYMBOL_REPEATED,
    RET_GROUP_SYMBOL_DELETED,
    RET_GROUP_SYMBOL_SYMBOLCATA_DIFFERENT,
    RET_GROUP_SYMBOL_VOLUMES_LTZERO,
    RET_GROUP_SYMBOL_HAS_NO_RIGHTS,
    RET_GROUP_SYMBOL_ACCOUNT_GROUP_DIFFERENT,
    RET_GROUP_SYMBOL_INSERT_FAILED,
    RET_GROUP_SYMBOL_UPDATE_FAILED,
    RET_GROUP_SYMBOL_GET_FAILED,
    RET_GROUP_SYMBOL_TIME_NOT_VALID,
    RET_GROUP_SYMBOL_FX_NO_EXPIRE,
    RET_GROUP_SYMBOL_NO_VALID,
    RET_GROUP_SYMBOL_UPDATE_LPUSER_SIZE_NOT_VALID,
    RET_GROUP_SYMBOL_UPDATE_LPUSER_FAILED,
    RET_GROUP_SYMBOL_VOLUMES_NOT_VALID,
    RET_GROUP_SYMBOL_HAD_BIND_LPUSER,
    
    RET_GROUP_SYMBOLCATA_NOT_FOUND        = CMD_GROUP_SYMBOLCATA_ADD        << 8,
    RET_GROUP_SYMBOLCATA_ALREADY_EXIST,
    RET_GROUP_SYMBOLCATA_TOKEN_INVALID,
    RET_GROUP_SYMBOLCATA_ACCOUNT_GROUP_INVALID,
    RET_GROUP_SYMBOLCATA_REPEATED,
    RET_GROUP_SYMBOLCATA_DELETED,
    RET_GROUP_SYMBOLCATA_ACCOUNT_GROUP_DIFFERENT,
    RET_GROUP_SYMBOLCATA_HAS_NO_RIGHTS,
    RET_GROUP_SYMBOLCATA_INSERT_FAILED,
    RET_GROUP_SYMBOLCATA_UPDATE_FAILED,
    RET_GROUP_SYMBOLCATA_SYMBOL_CATA_INVALID,
    RET_GROUP_SYMBOLCATA_GET_FAILED,
    
    // LPIB_BIND
    RET_LPIBBIND_NOT_FOUND = CMD_LPIB_BIND_ADD     << 8,
    RET_LPIBBIND_LOAD_FAILED,
    RET_LPIBBIND_NOT_VALID,
    RET_LPIBBIND_ITEM_EMPTY,
    RET_LPIBBIND_STATUS_DELETE,
    RET_LPUSER_NULL,
    RET_LPUSER_TURNOVER_ACCOUNT_NO_FOUND,
    RET_LPIBBIND_LPMM,
    
    // LPCHACC
    RET_LPCHACC_NOT_FOUND = CMD_LPCHANNEL_ACCOUNT_ADD     << 8,
    RET_LPCHACC_LOAD_FAILED,
    RET_LPCHACC_NOT_VALID,
    RET_LPCHACC_ITEM_EMPTY,
    RET_LPCHACC_STATUS_DELETE,
    RET_LPCHACC_TOKEN_NO_VALID,
    RET_LPCHACC_CHANNEL_NOT_VALID,
    RET_LPCHACC_COMPANY_NOT_VALID,
    RET_LPCHACC_SYMBOL_CANT_CHANGE,
    RET_LPCHACC_IN_USED,
    
    // LPCHANNEL
    RET_LPCHANNEL_NOT_FOUND = CMD_LPCHANNEL_ADD     << 8,
    RET_LPCHANNEL_LOAD_FAILED,
    RET_LPCHANNEL_NOT_VALID,
    RET_LPCHANNEL_ITEM_EMPTY,
    RET_LPCHANNEL_STATUS_DELETE,
    RET_LPCHANNEL_NO_SETTING,
    RET_LPCHANNEL_TYPE_NOT_VALID,
    RET_LPCHANNEL_SYMBOL_ALREADY_EXIST,
    RET_LPCHANNEL_SYMBOL_CANT_CHANGE,
    RET_LPCHANNEL_HAS_SYMBOLS,
    
    
    // LPCHACC_SYMBOL
    RET_LPCHACC_SYMBOL_NOT_FOUND = CMD_LPCHANNEL_ACCOUNT_SYMBOL_ADD     << 8,
    RET_LPCHACC_SYMBOL_LOAD_FAILED,
    RET_LPCHACC_SYMBOL_NOT_VALID,
    RET_LPCHACC_SYMBOL_ITEM_EMPTY,
    RET_LPCHACC_SYMBOL_STATUS_DELETE,
    RET_LPCHACC_SYMBOL_NO_SETTING,
    RET_LPCHACC_SYMBOL_NO_CONTAIN,
    RET_LPCHACC_SYMBOL_ALREADY_EXIST,
    
    // LPCHANNEL_SYMBOL
    RET_LPCHANNEL_SYMBOL_NOT_FOUND = CMD_LPCHANNEL_SYMBOL_ADD     << 8,
    RET_LPCHANNEL_SYMBOL_LOAD_FAILED,
    RET_LPCHANNEL_SYMBOL_NOT_VALID,
    RET_LPCHANNEL_SYMBOL_ITEM_EMPTY,
    RET_LPCHANNEL_SYMBOL_STATUS_DELETE,
    RET_LPCHANNEL_SYMBOL_NO_SETTING,
    
    RET_MAX,
    
    
}RETURN_ERROR_CODE;


