// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_right_dividend.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

#import "IxItemRightDividend.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - IxItemRightDividendRoot

@implementation IxItemRightDividendRoot

// No extensions in the file and no imports, so no need to generate
// +extensionRegistry.

@end

#pragma mark - IxItemRightDividendRoot_FileDescriptor

static GPBFileDescriptor *IxItemRightDividendRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_right_dividend

@implementation item_right_dividend

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic symbolid;
@dynamic recordDate;
@dynamic baseDate;
@dynamic exDividendDate;
@dynamic exDividend;
@dynamic stopOut;
@dynamic rollPosition;
@dynamic dividendPrice;
@dynamic sendSharesRate;
@dynamic allotmentRate;
@dynamic allotmentPrice;
@dynamic closePrice;
@dynamic executeClosePrice;
@dynamic exRightPrice;
@dynamic executeRightPrice;
@dynamic execute;
@dynamic reserve;

typedef struct item_right_dividend__storage_ {
  uint32_t _has_storage_[1];
  NSString *reserve;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
  uint64_t symbolid;
  uint64_t recordDate;
  uint64_t baseDate;
  uint64_t exDividendDate;
  double dividendPrice;
  double sendSharesRate;
  double allotmentRate;
  double allotmentPrice;
  double closePrice;
  double executeClosePrice;
  double exRightPrice;
  double executeRightPrice;
} item_right_dividend__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_Id_p,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uuid",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_Uuid,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, uuid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uutime",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_Uutime,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, uutime),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "symbolid",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_Symbolid,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, symbolid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "recordDate",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_RecordDate,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, recordDate),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "baseDate",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_BaseDate,
        .hasIndex = 5,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, baseDate),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "exDividendDate",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_ExDividendDate,
        .hasIndex = 6,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, exDividendDate),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "exDividend",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_ExDividend,
        .hasIndex = 7,
        .offset = 8,  // Stored in _has_storage_ to save space.
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeBool,
      },
      {
        .name = "stopOut",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_StopOut,
        .hasIndex = 9,
        .offset = 10,  // Stored in _has_storage_ to save space.
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeBool,
      },
      {
        .name = "rollPosition",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_RollPosition,
        .hasIndex = 11,
        .offset = 12,  // Stored in _has_storage_ to save space.
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeBool,
      },
      {
        .name = "dividendPrice",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_DividendPrice,
        .hasIndex = 13,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, dividendPrice),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "sendSharesRate",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_SendSharesRate,
        .hasIndex = 14,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, sendSharesRate),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "allotmentRate",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_AllotmentRate,
        .hasIndex = 15,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, allotmentRate),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "allotmentPrice",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_AllotmentPrice,
        .hasIndex = 16,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, allotmentPrice),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "closePrice",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_ClosePrice,
        .hasIndex = 17,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, closePrice),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "executeClosePrice",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_ExecuteClosePrice,
        .hasIndex = 18,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, executeClosePrice),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "exRightPrice",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_ExRightPrice,
        .hasIndex = 19,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, exRightPrice),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "executeRightPrice",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_ExecuteRightPrice,
        .hasIndex = 20,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, executeRightPrice),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeDouble,
      },
      {
        .name = "execute",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_Execute,
        .hasIndex = 21,
        .offset = 22,  // Stored in _has_storage_ to save space.
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeBool,
      },
      {
        .name = "reserve",
        .dataTypeSpecific.className = NULL,
        .number = item_right_dividend_FieldNumber_Reserve,
        .hasIndex = 23,
        .offset = (uint32_t)offsetof(item_right_dividend__storage_, reserve),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[item_right_dividend class]
                                     rootClass:[IxItemRightDividendRoot class]
                                          file:IxItemRightDividendRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(item_right_dividend__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
