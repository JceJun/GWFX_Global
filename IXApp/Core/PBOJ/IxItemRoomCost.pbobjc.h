// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_room_cost.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Enum item_room_cost_estatus

typedef GPB_ENUM(item_room_cost_estatus) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_room_cost_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_room_cost_estatus_Normal = 0,
  item_room_cost_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_room_cost_estatus_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_room_cost_estatus_IsValidValue(int32_t value);

#pragma mark - IxItemRoomCostRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxItemRoomCostRoot : GPBRootObject
@end

#pragma mark - item_room_cost

typedef GPB_ENUM(item_room_cost_FieldNumber) {
  item_room_cost_FieldNumber_Id_p = 1,
  item_room_cost_FieldNumber_Uuid = 2,
  item_room_cost_FieldNumber_Uutime = 3,
  item_room_cost_FieldNumber_Capacity = 4,
  item_room_cost_FieldNumber_Diamonds = 5,
  item_room_cost_FieldNumber_Status = 10,
};

@interface item_room_cost : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

/** 房间容量 */
@property(nonatomic, readwrite) uint32_t capacity;

/** 每天需要的钻石数 */
@property(nonatomic, readwrite) uint32_t diamonds;

@property(nonatomic, readwrite) uint32_t status;

@end

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
