// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_kline_repair.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Enum item_kline_repair_estatus

typedef GPB_ENUM(item_kline_repair_estatus) {
  /**
   * Value used if any message's field encounters a value that is not defined
   * by this enum. The message will also have C functions to get/set the rawValue
   * of the field.
   **/
  item_kline_repair_estatus_GPBUnrecognizedEnumeratorValue = kGPBUnrecognizedEnumeratorValue,
  item_kline_repair_estatus_Normal = 0,
  item_kline_repair_estatus_Deleted = 1,
};

GPBEnumDescriptor *item_kline_repair_estatus_EnumDescriptor(void);

/**
 * Checks to see if the given value is defined by the enum or was not known at
 * the time this source was generated.
 **/
BOOL item_kline_repair_estatus_IsValidValue(int32_t value);

#pragma mark - IxItemKlineRepairRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxItemKlineRepairRoot : GPBRootObject
@end

#pragma mark - item_kline_repair

typedef GPB_ENUM(item_kline_repair_FieldNumber) {
  item_kline_repair_FieldNumber_Id_p = 1,
  item_kline_repair_FieldNumber_Uuid = 2,
  item_kline_repair_FieldNumber_Uutime = 3,
  item_kline_repair_FieldNumber_Symbolid = 4,
  item_kline_repair_FieldNumber_StartTime = 5,
  item_kline_repair_FieldNumber_EndTime = 6,
  item_kline_repair_FieldNumber_Type = 7,
  item_kline_repair_FieldNumber_Status = 8,
};

@interface item_kline_repair : GPBMessage

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite) uint64_t symbolid;

@property(nonatomic, readwrite) uint64_t startTime;

@property(nonatomic, readwrite) uint64_t endTime;

/** kline type */
@property(nonatomic, readwrite) uint32_t type;

@property(nonatomic, readwrite) item_kline_repair_estatus status;

@end

/**
 * Fetches the raw value of a @c item_kline_repair's @c status property, even
 * if the value was not defined by the enum at the time the code was generated.
 **/
int32_t item_kline_repair_Status_RawValue(item_kline_repair *message);
/**
 * Sets the raw value of an @c item_kline_repair's @c status property, allowing
 * it to be set to a value that was not defined by the enum at the time the code
 * was generated.
 **/
void Setitem_kline_repair_Status_RawValue(item_kline_repair *message, int32_t value);

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
