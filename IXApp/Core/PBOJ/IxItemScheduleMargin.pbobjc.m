// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.item_schedule_margin.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

#import "IxItemScheduleMargin.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - IxItemScheduleMarginRoot

@implementation IxItemScheduleMarginRoot

// No extensions in the file and no imports, so no need to generate
// +extensionRegistry.

@end

#pragma mark - IxItemScheduleMarginRoot_FileDescriptor

static GPBFileDescriptor *IxItemScheduleMarginRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - item_schedule_margin

@implementation item_schedule_margin

@dynamic id_p;
@dynamic uuid;
@dynamic uutime;
@dynamic scheduleid;
@dynamic symbolid;
@dynamic marginType;
@dynamic status;

typedef struct item_schedule_margin__storage_ {
  uint32_t _has_storage_[1];
  uint32_t marginType;
  uint32_t status;
  uint64_t id_p;
  uint64_t uuid;
  uint64_t uutime;
  uint64_t scheduleid;
  uint64_t symbolid;
} item_schedule_margin__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = item_schedule_margin_FieldNumber_Id_p,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(item_schedule_margin__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uuid",
        .dataTypeSpecific.className = NULL,
        .number = item_schedule_margin_FieldNumber_Uuid,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(item_schedule_margin__storage_, uuid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "uutime",
        .dataTypeSpecific.className = NULL,
        .number = item_schedule_margin_FieldNumber_Uutime,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(item_schedule_margin__storage_, uutime),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeFixed64,
      },
      {
        .name = "scheduleid",
        .dataTypeSpecific.className = NULL,
        .number = item_schedule_margin_FieldNumber_Scheduleid,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(item_schedule_margin__storage_, scheduleid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "symbolid",
        .dataTypeSpecific.className = NULL,
        .number = item_schedule_margin_FieldNumber_Symbolid,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(item_schedule_margin__storage_, symbolid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "marginType",
        .dataTypeSpecific.className = NULL,
        .number = item_schedule_margin_FieldNumber_MarginType,
        .hasIndex = 5,
        .offset = (uint32_t)offsetof(item_schedule_margin__storage_, marginType),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "status",
        .dataTypeSpecific.className = NULL,
        .number = item_schedule_margin_FieldNumber_Status,
        .hasIndex = 6,
        .offset = (uint32_t)offsetof(item_schedule_margin__storage_, status),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[item_schedule_margin class]
                                     rootClass:[IxItemScheduleMarginRoot class]
                                          file:IxItemScheduleMarginRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(item_schedule_margin__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
