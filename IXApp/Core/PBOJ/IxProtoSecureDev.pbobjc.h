// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_secure_dev.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

@class item_header;
@class item_secure_dev;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - IxProtoSecureDevRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxProtoSecureDevRoot : GPBRootObject
@end

#pragma mark - proto_secure_dev_add

typedef GPB_ENUM(proto_secure_dev_add_FieldNumber) {
  proto_secure_dev_add_FieldNumber_Header = 1,
  proto_secure_dev_add_FieldNumber_Id_p = 2,
  proto_secure_dev_add_FieldNumber_Result = 3,
  proto_secure_dev_add_FieldNumber_Comment = 4,
  proto_secure_dev_add_FieldNumber_SecureDev = 5,
};

@interface proto_secure_dev_add : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) item_secure_dev *secureDev;
/** Test to see if @c secureDev has been set. */
@property(nonatomic, readwrite) BOOL hasSecureDev;

@end

#pragma mark - proto_secure_dev_update

typedef GPB_ENUM(proto_secure_dev_update_FieldNumber) {
  proto_secure_dev_update_FieldNumber_Header = 1,
  proto_secure_dev_update_FieldNumber_Id_p = 2,
  proto_secure_dev_update_FieldNumber_Result = 3,
  proto_secure_dev_update_FieldNumber_Comment = 4,
  proto_secure_dev_update_FieldNumber_SecureDev = 5,
};

@interface proto_secure_dev_update : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) item_secure_dev *secureDev;
/** Test to see if @c secureDev has been set. */
@property(nonatomic, readwrite) BOOL hasSecureDev;

@end

#pragma mark - proto_secure_dev_delete

typedef GPB_ENUM(proto_secure_dev_delete_FieldNumber) {
  proto_secure_dev_delete_FieldNumber_Header = 1,
  proto_secure_dev_delete_FieldNumber_Id_p = 2,
  proto_secure_dev_delete_FieldNumber_Result = 3,
  proto_secure_dev_delete_FieldNumber_Comment = 4,
  proto_secure_dev_delete_FieldNumber_Userid = 5,
};

@interface proto_secure_dev_delete : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite) uint64_t userid;

@end

#pragma mark - proto_secure_dev_get

typedef GPB_ENUM(proto_secure_dev_get_FieldNumber) {
  proto_secure_dev_get_FieldNumber_Header = 1,
  proto_secure_dev_get_FieldNumber_Id_p = 2,
  proto_secure_dev_get_FieldNumber_Result = 3,
  proto_secure_dev_get_FieldNumber_Comment = 4,
  proto_secure_dev_get_FieldNumber_SecureDev = 5,
};

@interface proto_secure_dev_get : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@property(nonatomic, readwrite, strong, null_resettable) item_secure_dev *secureDev;
/** Test to see if @c secureDev has been set. */
@property(nonatomic, readwrite) BOOL hasSecureDev;

@end

#pragma mark - proto_secure_dev_list

typedef GPB_ENUM(proto_secure_dev_list_FieldNumber) {
  proto_secure_dev_list_FieldNumber_Header = 1,
  proto_secure_dev_list_FieldNumber_Companyid = 2,
  proto_secure_dev_list_FieldNumber_Offset = 3,
  proto_secure_dev_list_FieldNumber_Count = 4,
  proto_secure_dev_list_FieldNumber_Total = 5,
  proto_secure_dev_list_FieldNumber_SecureDevArray = 6,
};

@interface proto_secure_dev_list : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t companyid;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite) uint32_t total;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_secure_dev*> *secureDevArray;
/** The number of items in @c secureDevArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger secureDevArray_Count;

@end

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
