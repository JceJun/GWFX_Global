// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_language.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

#import "IxProtoLanguage.pbobjc.h"
#import "IxProtoHeader.pbobjc.h"
#import "IxItemLanguage.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - IxProtoLanguageRoot

@implementation IxProtoLanguageRoot

// No extensions in the file and none of the imports (direct or indirect)
// defined extensions, so no need to generate +extensionRegistry.

@end

#pragma mark - IxProtoLanguageRoot_FileDescriptor

static GPBFileDescriptor *IxProtoLanguageRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"ix"
                                                     syntax:GPBFileSyntaxProto3];
  }
  return descriptor;
}

#pragma mark - proto_language_add

@implementation proto_language_add

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasLanguage, language;

typedef struct proto_language_add__storage_ {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_language *language;
  uint64_t id_p;
} proto_language_add__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .dataTypeSpecific.className = GPBStringifySymbol(item_header),
        .number = proto_language_add_FieldNumber_Header,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(proto_language_add__storage_, header),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_add_FieldNumber_Id_p,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(proto_language_add__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "result",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_add_FieldNumber_Result,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(proto_language_add__storage_, result),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "comment",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_add_FieldNumber_Comment,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(proto_language_add__storage_, comment),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
      {
        .name = "language",
        .dataTypeSpecific.className = GPBStringifySymbol(item_language),
        .number = proto_language_add_FieldNumber_Language,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(proto_language_add__storage_, language),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[proto_language_add class]
                                     rootClass:[IxProtoLanguageRoot class]
                                          file:IxProtoLanguageRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(proto_language_add__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

#pragma mark - proto_language_update

@implementation proto_language_update

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasLanguage, language;

typedef struct proto_language_update__storage_ {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_language *language;
  uint64_t id_p;
} proto_language_update__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .dataTypeSpecific.className = GPBStringifySymbol(item_header),
        .number = proto_language_update_FieldNumber_Header,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(proto_language_update__storage_, header),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_update_FieldNumber_Id_p,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(proto_language_update__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "result",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_update_FieldNumber_Result,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(proto_language_update__storage_, result),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "comment",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_update_FieldNumber_Comment,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(proto_language_update__storage_, comment),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
      {
        .name = "language",
        .dataTypeSpecific.className = GPBStringifySymbol(item_language),
        .number = proto_language_update_FieldNumber_Language,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(proto_language_update__storage_, language),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[proto_language_update class]
                                     rootClass:[IxProtoLanguageRoot class]
                                          file:IxProtoLanguageRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(proto_language_update__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

#pragma mark - proto_language_delete

@implementation proto_language_delete

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;

typedef struct proto_language_delete__storage_ {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  uint64_t id_p;
} proto_language_delete__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .dataTypeSpecific.className = GPBStringifySymbol(item_header),
        .number = proto_language_delete_FieldNumber_Header,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(proto_language_delete__storage_, header),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_delete_FieldNumber_Id_p,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(proto_language_delete__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "result",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_delete_FieldNumber_Result,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(proto_language_delete__storage_, result),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "comment",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_delete_FieldNumber_Comment,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(proto_language_delete__storage_, comment),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[proto_language_delete class]
                                     rootClass:[IxProtoLanguageRoot class]
                                          file:IxProtoLanguageRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(proto_language_delete__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

#pragma mark - proto_language_get

@implementation proto_language_get

@dynamic hasHeader, header;
@dynamic id_p;
@dynamic result;
@dynamic comment;
@dynamic hasLanguage, language;

typedef struct proto_language_get__storage_ {
  uint32_t _has_storage_[1];
  uint32_t result;
  item_header *header;
  NSString *comment;
  item_language *language;
  uint64_t id_p;
} proto_language_get__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .dataTypeSpecific.className = GPBStringifySymbol(item_header),
        .number = proto_language_get_FieldNumber_Header,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(proto_language_get__storage_, header),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
      {
        .name = "id_p",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_get_FieldNumber_Id_p,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(proto_language_get__storage_, id_p),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "result",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_get_FieldNumber_Result,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(proto_language_get__storage_, result),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "comment",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_get_FieldNumber_Comment,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(proto_language_get__storage_, comment),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeString,
      },
      {
        .name = "language",
        .dataTypeSpecific.className = GPBStringifySymbol(item_language),
        .number = proto_language_get_FieldNumber_Language,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(proto_language_get__storage_, language),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[proto_language_get class]
                                     rootClass:[IxProtoLanguageRoot class]
                                          file:IxProtoLanguageRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(proto_language_get__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end

#pragma mark - proto_language_list

@implementation proto_language_list

@dynamic hasHeader, header;
@dynamic companyid;
@dynamic offset;
@dynamic count;
@dynamic total;
@dynamic languageArray, languageArray_Count;

typedef struct proto_language_list__storage_ {
  uint32_t _has_storage_[1];
  uint32_t offset;
  uint32_t count;
  uint32_t total;
  item_header *header;
  NSMutableArray *languageArray;
  uint64_t companyid;
} proto_language_list__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "header",
        .dataTypeSpecific.className = GPBStringifySymbol(item_header),
        .number = proto_language_list_FieldNumber_Header,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(proto_language_list__storage_, header),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeMessage,
      },
      {
        .name = "companyid",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_list_FieldNumber_Companyid,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(proto_language_list__storage_, companyid),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt64,
      },
      {
        .name = "offset",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_list_FieldNumber_Offset,
        .hasIndex = 2,
        .offset = (uint32_t)offsetof(proto_language_list__storage_, offset),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "count",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_list_FieldNumber_Count,
        .hasIndex = 3,
        .offset = (uint32_t)offsetof(proto_language_list__storage_, count),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "total",
        .dataTypeSpecific.className = NULL,
        .number = proto_language_list_FieldNumber_Total,
        .hasIndex = 4,
        .offset = (uint32_t)offsetof(proto_language_list__storage_, total),
        .flags = GPBFieldOptional,
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "languageArray",
        .dataTypeSpecific.className = GPBStringifySymbol(item_language),
        .number = proto_language_list_FieldNumber_LanguageArray,
        .hasIndex = GPBNoHasBit,
        .offset = (uint32_t)offsetof(proto_language_list__storage_, languageArray),
        .flags = GPBFieldRepeated,
        .dataType = GPBDataTypeMessage,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[proto_language_list class]
                                     rootClass:[IxProtoLanguageRoot class]
                                          file:IxProtoLanguageRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(proto_language_list__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
