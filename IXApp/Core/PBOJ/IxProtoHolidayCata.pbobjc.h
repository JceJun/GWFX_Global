// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ix.proto_holiday_cata.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers.h>
#else
 #import "GPBProtocolBuffers.h"
#endif

#if GOOGLE_PROTOBUF_OBJC_VERSION < 30002
#error This file was generated by a newer version of protoc which is incompatible with your Protocol Buffer library sources.
#endif
#if 30002 < GOOGLE_PROTOBUF_OBJC_MIN_SUPPORTED_VERSION
#error This file was generated by an older version of protoc which is incompatible with your Protocol Buffer library sources.
#endif

// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

CF_EXTERN_C_BEGIN

@class item_header;
@class item_holiday_cata;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - IxProtoHolidayCataRoot

/**
 * Exposes the extension registry for this file.
 *
 * The base class provides:
 * @code
 *   + (GPBExtensionRegistry *)extensionRegistry;
 * @endcode
 * which is a @c GPBExtensionRegistry that includes all the extensions defined by
 * this file and all files that it depends on.
 **/
@interface IxProtoHolidayCataRoot : GPBRootObject
@end

#pragma mark - proto_holiday_cata_add

typedef GPB_ENUM(proto_holiday_cata_add_FieldNumber) {
  proto_holiday_cata_add_FieldNumber_Header = 1,
  proto_holiday_cata_add_FieldNumber_Id_p = 2,
  proto_holiday_cata_add_FieldNumber_HolidayCata = 3,
  proto_holiday_cata_add_FieldNumber_Result = 4,
  proto_holiday_cata_add_FieldNumber_Comment = 5,
};

@interface proto_holiday_cata_add : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite, strong, null_resettable) item_holiday_cata *holidayCata;
/** Test to see if @c holidayCata has been set. */
@property(nonatomic, readwrite) BOOL hasHolidayCata;

/** 0:success 1:failed */
@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@end

#pragma mark - proto_holiday_cata_update

typedef GPB_ENUM(proto_holiday_cata_update_FieldNumber) {
  proto_holiday_cata_update_FieldNumber_Header = 1,
  proto_holiday_cata_update_FieldNumber_Id_p = 2,
  proto_holiday_cata_update_FieldNumber_HolidayCata = 3,
  proto_holiday_cata_update_FieldNumber_Result = 4,
  proto_holiday_cata_update_FieldNumber_Comment = 5,
};

@interface proto_holiday_cata_update : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

/**
 * uint32     uuid    = 3 ;
 * fixed64    uutime  = 4 ;
 * string     name    = 5 ;
 * string     margin_type = 6;
 * uint32     status  = 7;
 **/
@property(nonatomic, readwrite, strong, null_resettable) item_holiday_cata *holidayCata;
/** Test to see if @c holidayCata has been set. */
@property(nonatomic, readwrite) BOOL hasHolidayCata;

/** 0:success 1:failed */
@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@end

#pragma mark - proto_holiday_cata_delete

typedef GPB_ENUM(proto_holiday_cata_delete_FieldNumber) {
  proto_holiday_cata_delete_FieldNumber_Header = 1,
  proto_holiday_cata_delete_FieldNumber_Id_p = 2,
  proto_holiday_cata_delete_FieldNumber_Result = 3,
  proto_holiday_cata_delete_FieldNumber_Comment = 4,
};

@interface proto_holiday_cata_delete : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

/** 0:success 1:failed */
@property(nonatomic, readwrite) uint32_t result;

@property(nonatomic, readwrite, copy, null_resettable) NSString *comment;

@end

#pragma mark - proto_holiday_cata_get

typedef GPB_ENUM(proto_holiday_cata_get_FieldNumber) {
  proto_holiday_cata_get_FieldNumber_Header = 1,
  proto_holiday_cata_get_FieldNumber_Id_p = 2,
  proto_holiday_cata_get_FieldNumber_Uuid = 3,
  proto_holiday_cata_get_FieldNumber_Uutime = 4,
  proto_holiday_cata_get_FieldNumber_Name = 5,
  proto_holiday_cata_get_FieldNumber_MarginType = 6,
  proto_holiday_cata_get_FieldNumber_Status = 7,
  proto_holiday_cata_get_FieldNumber_HolidayCata = 8,
};

@interface proto_holiday_cata_get : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint64_t id_p;

@property(nonatomic, readwrite) uint64_t uuid;

@property(nonatomic, readwrite) uint64_t uutime;

@property(nonatomic, readwrite, copy, null_resettable) NSString *name;

@property(nonatomic, readwrite, copy, null_resettable) NSString *marginType;

@property(nonatomic, readwrite) uint32_t status;

@property(nonatomic, readwrite, strong, null_resettable) item_holiday_cata *holidayCata;
/** Test to see if @c holidayCata has been set. */
@property(nonatomic, readwrite) BOOL hasHolidayCata;

@end

#pragma mark - proto_holiday_cata_list

typedef GPB_ENUM(proto_holiday_cata_list_FieldNumber) {
  proto_holiday_cata_list_FieldNumber_Header = 1,
  proto_holiday_cata_list_FieldNumber_Offset = 2,
  proto_holiday_cata_list_FieldNumber_Count = 3,
  proto_holiday_cata_list_FieldNumber_Total = 4,
  proto_holiday_cata_list_FieldNumber_HolidayCataArray = 5,
};

@interface proto_holiday_cata_list : GPBMessage

@property(nonatomic, readwrite, strong, null_resettable) item_header *header;
/** Test to see if @c header has been set. */
@property(nonatomic, readwrite) BOOL hasHeader;

@property(nonatomic, readwrite) uint32_t offset;

@property(nonatomic, readwrite) uint32_t count;

@property(nonatomic, readwrite) uint32_t total;

@property(nonatomic, readwrite, strong, null_resettable) NSMutableArray<item_holiday_cata*> *holidayCataArray;
/** The number of items in @c holidayCataArray without causing the array to be created. */
@property(nonatomic, readonly) NSUInteger holidayCataArray_Count;

@end

NS_ASSUME_NONNULL_END

CF_EXTERN_C_END

#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
