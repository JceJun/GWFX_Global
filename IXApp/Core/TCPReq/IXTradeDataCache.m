//
//  IXTradeDataCache.m
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeDataCache.h"
#import "IxItemDeal.pbobjc.h"

/**更新缓存监听，数据可从缓存中直接读取*/
NSString * const PB_CMD_CACHE_POSITION_LIST = @"positionChangedFlag";
NSString * const PB_CMD_CACHE_ORDER_LIST    = @"orderChangedFlag";
NSString * const PB_CMD_CACHE_DEAL_LIST     = @"dealChangedFlag";
NSString * const PB_CMD_GROUP_SYMBOL_CATA_CHANGE  = @"accGroupSymCataFlag";

/**更新数据库监听，数据不提供缓存读写，需要从数据库中读写*/
//user
NSString * const PB_CMD_USERLOGIN_INFO      = @"pb_user_login_info";
NSString * const PB_CMD_USER_KICT_OUT       = @"pb_user_kick_out";
NSString * const PB_CMD_USERLOGOUT          = @"pb_user_logout";
NSString * const PB_CMD_USER_ADD            = @"pb_user_add";
NSString * const PB_CMD_USER_LOGIN_DATA     = @"pb_user_login_data";
NSString * const PB_CMD_USER_LOGIN_DATA_TOTAL = @"pb_user_login_data_total";




NSString * const PB_CMD_ACCOUNT_LIST        = @"pb_account_list";
NSString * const PB_CMD_ACCOUNT_UPDATE      = @"pb_account_update";
NSString * const PB_CMD_ACCOUNT_BALANCE_GET = @"pb_account_balance_get";


//account_Group
NSString * const PB_CMD_ACCOUNT_GROUP_LIST  = @"pb_account_group_list";
NSString * const PB_CMD_ACCOUNT_GROUP_UPDATE  = @"pb_account_group_update";
NSString * const PB_CMD_ACCOUNT_GROUP_ADD  = @"pb_account_group_add";
NSString * const PB_CMD_ACCOUNT_GROUP_DELETE  = @"pb_account_group_delete";
NSString * const PB_CMD_ACCOUNT_GROUP_CHANGE  = @"accountGroupChange";

NSString * const PB_CMD_ACCOUNT_CHANGE       = @"swiAccount";

//symbol
NSString * const PB_CMD_SYMBOL_CHANGE       = @"symbolChangedFlag";
NSString * const PB_CMD_SYMBOL_LIST         = @"pb_symbol_list";
NSString * const PB_CMD_SYMBOL_UPDATE      = @"pb_symbol_update";
NSString * const PB_CMD_SYMBOL_ADD         = @"pb_symbol_add";
NSString * const PB_CMD_SYMBOL_DELETE      = @"pb_symbol_delete";


NSString * const PB_CMD_SYMBOL_SUB_LIST     = @"pb_symbol_sub_list";
NSString * const PB_CMD_SYMBOL_SUB_ADD      = @"pb_symbol_sub_add";
NSString * const PB_CMD_SYMBOL_SUB_DELETE   = @"pb_symbol_sub_delete";
NSString * const PB_CMD_SYMBOL_HOT_LIST     = @"pb_symbol_hot_list";
NSString * const PB_CMD_SYMBOL_HOT_UPDATE   = @"pb_symbol_hot_update";
NSString * const PB_CMD_SYMBOL_HOT_ADD      = @"pb_symbol_hot_add";

//sym_cata
NSString * const PB_CMD_SYMBOL_CATA_LIST    = @"pb_symbol_cata_list";
NSString * const PB_CMD_SYMBOL_CATA_UPDATE  = @"pb_symbol_cata_update";
NSString * const PB_CMD_SYMBOL_CATA_ADD     = @"pb_symbol_cata_add";
NSString * const PB_CMD_SYMBOL_CATA_DELETE  = @"pb_symbol_cata_delete";

NSString * const PB_CMD_LANGUAGE_LIST       = @"pb_language_list";
NSString * const PB_CMD_LANGUAGE_ADD        = @"pb_language_add";
NSString * const PB_CMD_LANGUAGE_UPDATE     = @"pb_language_update";

//position
NSString * const PB_CMD_POSITION_LIST       = @"pb_position_list";
NSString * const PB_CMD_POSITION_UPDATE     = @"pb_position_update";
NSString * const PB_CMD_POSITION_ADD        = @"pb_position_add";

//order
NSString * const PB_CMD_ORDER_LIST          = @"pb_order_list";
NSString * const PB_CMD_ORDER_UPDATE        = @"pb_order_update";
NSString * const PB_CMD_ORDER_ADD           = @"pb_order_add";
NSString * const PB_CMD_ORDER_CANCEL        = @"pb_order_cancel";
NSString * const PB_CMD_ORDER_ADD_BATCH     = @"pb_order_add_batch";

//deal
NSString * const PB_CMD_DEAL_LIST           = @"pb_deal_list";
NSString * const PB_CMD_DEAL_UPDATE         = @"pb_deal_update";
NSString * const PB_CMD_DEAL_ADD            = @"pb_deal_add";

NSString * const CMD_TIMEZONE_UPDATE        = @"timezone_update";
NSString * const CMD_UPLOAD_HEAD_UPDATE     = @"upload_head_update";

//sym_lable
NSString * const PB_CMD_SYMBOL_LABLE_LIST   = @"pb_symbol_lable_list";
NSString * const PB_CMD_SYMBOL_LABLE_UPDATE = @"pb_symbol_lable_update";
NSString * const PB_CMD_SYMBOL_LABLE_ADD    = @"pb_symbol_lable_add";
NSString * const PB_CMD_SYMBOL_LABLE_DELETE = @"pb_symbol_lable_delete";

//sym_margin_set
NSString * const PB_CMD_SYMBOL_MARGIN_SET_LIST  = @"pb_symbol_margin_set_list";
NSString * const PB_CMD_SYMBOL_MARGIN_SET_UPDATE = @"pb_symbol_margin_set_update";
NSString * const PB_CMD_SYMBOL_MARGIN_SET_ADD = @"pb_symbol_margin_set_add";
NSString * const PB_CMD_SYMBOL_MARGIN_SET_DELETE = @"pb_symbol_margin_set_delete";

//holiday_cata
NSString * const PB_CMD_HOLIDAY_CATA_LIST = @"pb_holiday_cata_list";
NSString * const PB_CMD_HOLIDAY_CATA_UPDATE = @"pb_holiday_cata_update";
NSString * const PB_CMD_HOLIDAY_CATA_ADD = @"pb_holiday_cata_add";
NSString * const PB_CMD_HOLIDAY_CATA_DELETE = @"pb_holiday_cata_delete";

//holiday
NSString * const PB_CMD_HOLIDAY_LIST = @"pb_holiday_list";
NSString * const PB_CMD_HOLIDAY_UPDATE = @"pb_holiday_update";
NSString * const PB_CMD_HOLIDAY_ADD = @"pb_holiday_add";
NSString * const PB_CMD_HOLIDAY_DELETE = @"pb_holiday_delete";

//holiday_margin
NSString * const PB_CMD_HOLIDAY_MARGIN_LIST = @"pb_holiday_margin_list";
NSString * const PB_CMD_HOLIDAY_MARGIN_UPDATE = @"pb_holiday_margin_update";
NSString * const PB_CMD_HOLIDAY_MARGIN_ADD = @"pb_holiday_margin_add";
NSString * const PB_CMD_HOLIDAY_MARGIN_DELETE = @"pb_holiday_margin_delete";

//company
NSString * const PB_CMD_COMPANY_LIST = @"pb_company_list";
NSString * const PB_CMD_COMPANY_UPDATE = @"pb_company_update";
NSString * const PB_CMD_COMPANY_ADD = @"pb_company_add";
NSString * const PB_CMD_COMPANY_DELETE = @"pb_company_delete";

//shedule_cata
NSString * const PB_CMD_SCHEDULE_CATA_LIST = @"pb_schedule_cata_list";
NSString * const PB_CMD_SCHEDULE_CATA_UPDATE = @"pb_schedule_cata_update";
NSString * const PB_CMD_SCHEDULE_CATA_ADD = @"pb_schedule_cata_add";
NSString * const PB_CMD_SCHEDULE_CATA_DELETE = @"pb_schedule_cata_delete";


//schedule
NSString * const PB_CMD_SCHEDULE_LIST = @"pb_schedule_list";
NSString * const PB_CMD_SCHEDULE_UPDATE = @"pb_schedule_update";
NSString * const PB_CMD_SCHEDULE_ADD = @"pb_schedule_add";
NSString * const PB_CMD_SCHEDULE_DELETE = @"pb_schedule_delete";


//schedule_margin
NSString * const PB_CMD_SCHEDULE_MARGIN_LIST = @"pb_schedule_margin_list";
NSString * const PB_CMD_SCHEDULE_MARGIN_UPDATE = @"pb_schedule_margin_update";
NSString * const PB_CMD_SCHEDULE_MARGIN_ADD = @"pb_schedule_margin_add";
NSString * const PB_CMD_SCHEDULE_MARGIN_DELETE = @"pb_schedule_margin_delete";

//quote_delay
NSString * const PB_CMD_QUOTE_DELAY_LIST = @"pb_quote_delay_list";   //行情订阅
NSString * const PB_CMD_QUOTE_DELAY_UPDATE = @"pb_quote_delay_update";
NSString * const PB_CMD_QUOTE_DELAY_ADD = @"pb_quote_delay_add";

//account_group_symbol_cata
NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_LIST = @"pb_account_group_symbol_cata_list";   //账户组产品分类
NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_UPDATE = @"pb_account_group_symbol_cata_update";
NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_ADD = @"pb_account_group_symbol_cata_add";
NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_DELETE = @"pb_account_group_symbol_cata_delete";

//lp_channel_account
NSString * const PB_CMD_LPCHACC_LIST = @"pb_lpchacc_list";   //lpchacc
NSString * const PB_CMD_LPCHACC_UPDATE = @"pb_lpchacc_update";
NSString * const PB_CMD_LPCHACC_ADD = @"pb_lpchacc_add";
NSString * const PB_CMD_LPCHACC_DELETE = @"pb_lpchacc_delete";

//lp_channel_account_symbol
NSString * const PB_CMD_LPCHACC_SYMBOL_LIST = @"pb_lpchacc_symbol_list";   //lpchacc_symbol
NSString * const PB_CMD_LPCHACC_SYMBOL_UPDATE = @"pb_lpchacc_symbol_update";
NSString * const PB_CMD_LPCHACC_SYMBOL_ADD = @"pb_lpchacc_symbol_add";
NSString * const PB_CMD_LPCHACC_SYMBOL_DELETE = @"pb_lpchacc_symbol_delete";

//lp_channel
NSString * const PB_CMD_LPCHANNEL_LIST = @"pb_lpchannel_list";   //lp_channel
NSString * const PB_CMD_LPCHANNEL_UPDATE = @"pb_lpchannel_update";
NSString * const PB_CMD_LPCHANNEL_ADD = @"pb_lpchannel_add";
NSString * const PB_CMD_LPCHANNEL_DELETE = @"pb_lpchannel_delete";

//lp_channel_symbol
NSString * const PB_CMD_LPCHANNEL_SYMBOL_LIST = @"pb_lpchannel_symbol_list";   //lp_channel_symbol
NSString * const PB_CMD_LPCHANNEL_SYMBOL_UPDATE = @"pb_lpchannel_symbol_update";
NSString * const PB_CMD_LPCHANNEL_SYMBOL_ADD = @"pb_lpchannel_symbol_add";
NSString * const PB_CMD_LPCHANNEL_SYMBOL_DELETE = @"pb_lpchannel_symbol_delete";

//lp_IbBind
NSString * const PB_CMD_LPIB_BIND_LIST = @"pb_lpib_bind_list";   //lp_IbBind
NSString * const PB_CMD_LPIB_BIND_UPDATE = @"pb_lpib_bind_update";
NSString * const PB_CMD_LPIB_BIND_ADD = @"pb_lpib_bind_add";
NSString * const PB_CMD_LPIB_BIND_DELETE = @"pb_lpib_bind_delete";

NSString * const PB_CMD_EOD_TIME_LIST = @"pb_eod_time_list";   //结算时间
NSString * const PB_CMD_EOD_TIME_UPDATE = @"pb_eod_time_update";
NSString * const PB_CMD_EOD_TIME_ADD = @"pb_eod_time_add";
NSString * const PB_CMD_EOD_TIME_DELETE = @"pb_eod_time_delete";

NSString * const PB_CMD_SECURE_DEV_LIST = @"pb_secure_dev_list";;   //登录设备
NSString * const PB_CMD_SECURE_DEV_UPDATE = @"pb_secure_dev_update";
NSString * const PB_CMD_SECURE_DEV_ADD = @"pb_secure_dev_add";
NSString * const PB_CMD_SECURE_DEV_DELETE = @"pb_secure_dev_delete";

NSString * const PB_CMD_GROUP_SYMBOL_LIST = @"pb_group_symbol_list";   //账户组产品
NSString * const PB_CMD_GROUP_SYMBOL_UPDATE = @"pb_group_symbol_update";
NSString * const PB_CMD_GROUP_SYMBOL_ADD = @"pb_group_symbol_add";
NSString * const PB_CMD_GROUP_SYMBOL_DELETE = @"pb_group_symbol_delete";

NSString * const PB_CMD_GROUP_SYMBOL_CATA_LIST = @"pb_group_symbol_cata_list";   //账户组产品分类
NSString * const PB_CMD_GROUP_SYMBOL_CATA_UPDATE = @"pb_group_symbol_cata_update";
NSString * const PB_CMD_GROUP_SYMBOL_CATA_ADD = @"pb_group_symbol_cata_add";
NSString * const PB_CMD_GROUP_SYMBOL_CATA_DELETE = @"pb_group_symbol_cata_delete";

NSString * const KLINE_CMD_REPARE_NOTIFY = @"pb_kLine_repare_noti";

@interface IXTradeDataCache ()


@end

@implementation IXTradeDataCache

@synthesize pb_cache_position_list = _pb_cache_position_list;
@synthesize pb_cache_order_list    = _pb_cache_order_list;
@synthesize pb_cache_deal_list     = _pb_cache_deal_list;

#pragma mark - 
#pragma mark - 单例操作

+ (IXTradeDataCache *)shareInstance{
    
    static IXTradeDataCache *share;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(!share){
            share = [[IXTradeDataCache alloc] init];
        }
    });
    return share;
}

- (void)clearInstance
{
    [self.pb_cache_position_list removeAllObjects];
    [self.pb_cache_order_list removeAllObjects];
    [self.pb_cache_deal_list removeAllObjects];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.positionChangedFlag = !self.positionChangedFlag;
    });
}

#pragma mark -
#pragma mark - pb_cache_position_list

- (NSMutableArray *)pb_cache_position_list
{
    if(!_pb_cache_position_list) {
        _pb_cache_position_list = [[NSMutableArray alloc] init];
    }
    return _pb_cache_position_list;
}

- (void)addPb_cache_position_list:(NSMutableArray *)pb_cache_position_list
{
    for (NSInteger i=0; i<pb_cache_position_list.count; i++) {
        item_position *tmpPosition = pb_cache_position_list[i];
        BOOL flag = NO;
        
        for (item_position *position in self.pb_cache_position_list) {
            if (position.id_p == tmpPosition.id_p) {
                flag = YES;
                break;
            }
        }
        if (!flag) {
            [self.pb_cache_position_list insertObject:tmpPosition atIndex:0];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.positionChangedFlag = !self.positionChangedFlag;
    });
}

- (void)deletePb_cache_position_list:(uint64_t)positionId
{
    for (int i = 0; i < self.pb_cache_position_list.count; i++) {
        item_position *position = self.pb_cache_position_list[i];
        if (position.id_p == positionId) {
            [self.pb_cache_position_list removeObjectAtIndex:i];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.positionChangedFlag = !self.positionChangedFlag;
            });
            break;
        }
    }
}

//部分平仓替换原来持仓数据
- (void)updatePb_cache_position_list:(item_position *)position
{
    for (int i = 0; i < self.pb_cache_position_list.count; i++) {
        item_position *tmpPosition = self.pb_cache_position_list[i];
        if (tmpPosition.id_p == position.id_p) {
            [self.pb_cache_position_list replaceObjectAtIndex:i withObject:position];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.positionChangedFlag = !self.positionChangedFlag;
            });
            break;
        }
    }
}

#pragma mark -
#pragma mark - pb_cache_order_list

- (NSMutableArray *)pb_cache_order_list
{
    if (!_pb_cache_order_list) {
        _pb_cache_order_list = [[NSMutableArray alloc] init];
    }
    return _pb_cache_order_list;
   
}

- (void)setPb_cache_order_list:(NSMutableArray *)pb_cache_order_list
{
    return [self.pb_cache_order_list addObjectsFromArray:pb_cache_order_list];
}

- (void)addPb_cache_order_list:(NSMutableArray *)pb_cache_order_list
{
    for (int i = 0; i < pb_cache_order_list.count; i++) {
        item_order *tmpOrder = pb_cache_order_list[i];
        BOOL flag = NO;
        for (item_order *order in self.pb_cache_order_list) {
            if (order.id_p == tmpOrder.id_p) {
                flag = YES;
                break;
            }
        }
        if (!flag) {
            [self.pb_cache_order_list insertObject:tmpOrder atIndex:0];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.orderChangedFlag = !self.orderChangedFlag;
    });

}

- (void)updatePb_cache_order:(item_order *)order
{
    if (order) {
        for (int i = 0; i < self.pb_cache_order_list.count; i++) {
            item_order *tmpOrder = self.pb_cache_order_list[i];
            if (tmpOrder.id_p == order.id_p) {
                [self.pb_cache_order_list replaceObjectAtIndex:i withObject:order];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.orderChangedFlag = !self.orderChangedFlag;
                });
                break;
            }
        }
    }
}

- (void)deletePb_cache_order_list:(uint64_t)orderId
{
    if (self.pb_cache_order_list.count > 0) {
        for (int i = 0; i < self.pb_cache_order_list.count; i++) {
            item_order *order = self.pb_cache_order_list[i];
            if (order.id_p == orderId) {
                [self.pb_cache_order_list removeObjectAtIndex:i];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.orderChangedFlag = !self.orderChangedFlag;
                });
                break;
            }
        }
    }

}

#pragma mark -
#pragma mark - pb_cache_deal_list

- (NSMutableArray *)pb_cache_deal_list
{
    if(!_pb_cache_deal_list){
        _pb_cache_deal_list = [[NSMutableArray alloc] init];
    }
    return _pb_cache_deal_list;
}

- (void)setPb_cache_deal_list:(NSMutableArray *)pb_cache_deal_list
{
    return [self.pb_cache_deal_list addObjectsFromArray:pb_cache_deal_list];
}

- (void)addPb_cache_deal_list:(NSMutableArray *)pb_cache_deal_list
{
    for (int i = 0; i < pb_cache_deal_list.count; i++) {
        item_deal *tmpDeal = pb_cache_deal_list[i];
        BOOL flag = NO;
        for (item_deal *deal in self.pb_cache_deal_list) {
            if (deal.id_p == tmpDeal.id_p) {
                flag = YES;
                break;
            }
        }
        if (!flag) {
            [self.pb_cache_deal_list insertObject:tmpDeal atIndex:0];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.dealChangedFlag = !self.dealChangedFlag;
    });

}

- (void)deletePb_cache_deal_list:(uint64_t)dealId
{
    if (self.pb_cache_deal_list.count > 0) {
        for (int i = 0; i < self.pb_cache_deal_list.count; i++) {
            item_deal *deal = [[item_deal alloc] init];
            deal = self.pb_cache_deal_list[i];
            if (deal.id_p == dealId) {
                [self.pb_cache_deal_list removeObjectAtIndex:i];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.dealChangedFlag = !self.dealChangedFlag;
                });
                break;
            }
        }
    }
}

#pragma mark -
#pragma mark - pb_cache_symbol_list
- (void)dealWithSymbolChange
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.symbolChangedFlag = !self.symbolChangedFlag;
    });
}

@end
