//
//  IXTradeDataFilter+DataBase.m
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeDataFilter+DataBase.h"

#import "IxProtoUser.pbobjc.h"
#import "IxProtoAccount.pbobjc.h"
#import "IxProtoAccountGroup.pbobjc.h"
#import "IxProtoAccountGroupSymbolCata.pbobjc.h"
#import "IxItemAccountGroupSymbolCata.pbobjc.h"
#import "IxProtoSymbol.pbobjc.h"
#import "IxProtoSymbolCata.pbobjc.h"
#import "IxItemSymbolCata.pbobjc.h"
#import "IxProtoSymbolSub.pbobjc.h"
#import "IxProtoSymbolHot.pbobjc.h"
#import "IxProtoSymbolMarginSet.pbobjc.h"
#import "IxItemSymbolMarginSet.pbobjc.h"
#import "IxProtoSymbolLabel.pbobjc.h"
#import "IxItemSymbolLabel.pbobjc.h"
#import "IxItemSymbolHot.pbobjc.h"
#import "IxItemSymbolSub.pbobjc.h"
#import "IxItemSymbol.pbobjc.h"

#import "IxProtoHolidayCata.pbobjc.h"
#import "IxProtoHoliday.pbobjc.h"
#import "IxProtoHolidayMargin.pbobjc.h"
#import "IxItemHolidayMargin.pbobjc.h"
#import "IxProtoCompany.pbobjc.h"
#import "IxItemCompany.pbobjc.h"
#import "IxProtoSchedule.pbobjc.h"
#import "IxProtoScheduleCata.pbobjc.h"
#import "IxProtoScheduleMargin.pbobjc.h"
#import "IxItemScheduleMargin.pbobjc.h"
#import "IxProtoPosition.pbobjc.h"
#import "IxProtoOrder.pbobjc.h"
#import "IxProtoDeal.pbobjc.h"
#import "IxItemDeal.pbobjc.h"
#import "IxProtoLanguage.pbobjc.h"
#import "IxProtoAccountBalance.pbobjc.h"
#import "IxProtoQuoteDelay.pbobjc.h"
#import "IxItemQuoteDelay.pbobjc.h"
#import "IxProtoKlineRepair.pbobjc.h"
#import "IxProtoEodTime.pbobjc.h"
#import "IxItemEodTime.pbobjc.h"
#import "IxProtoSecureDev.pbobjc.h"
#import "IxItemSecureDev.pbobjc.h"
#import "IxProtoGroupSymbol.pbobjc.h"
#import "IxItemGroupSymbol.pbobjc.h"
#import "IxProtoGroupSymbolCata.pbobjc.h"
#import "IxItemGroupSymbolCata.pbobjc.h"
#import "IxProtoLpchacc.pbobjc.h"
#import "IxItemLpchacc.pbobjc.h"
#import "IxProtoLpchaccSymbol.pbobjc.h"
#import "IxItemLpchaccSymbol.pbobjc.h"
#import "IxProtoLpchannel.pbobjc.h"
#import "IxItemLpchannel.pbobjc.h"
#import "IxProtoLpchannelSymbol.pbobjc.h"
#import "IxItemLpchannelSymbol.pbobjc.h"
#import "IxProtoLpibBind.pbobjc.h"
#import "IxItemLpibBind.pbobjc.h"

//other
#import "IXDataBase.h"
#import "IXTradeDataCache.h"

@implementation IXTradeDataFilter (DataBase)

+ (void)user_keep_alive:(id)obj
{
    proto_user_keepalive *info = (proto_user_keepalive *)obj;
    if (!info) {
        ELog(@"proto_user_keepalive 反序列化失败");
    }else{
#warning 修改服务器时间供测试
        #if DEBUG
        #else
        [[NSUserDefaults standardUserDefaults] setObject:@(info.serverTime) forKey:IXServerTime];
        #endif
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970])
                                                  forKey:IXLocaleTime];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)user_login_info:(id)obj
{
    proto_user_login_info *info = (proto_user_login_info *)obj;
    if (!info) {
        ELog(@"proto_user_login_info 反序列化失败");
    } else {
        if (info.result == 0) {
            #warning 修改服务器时间供测试
            #if DEBUG
                DLog(@"################Dbug Server Time###############");
            [[NSUserDefaults standardUserDefaults] setObject:@(time(0)) forKey:IXServerTime];
            #else
                [[NSUserDefaults standardUserDefaults] setObject:@(info.serverTime) forKey:IXServerTime];
            #endif
            if (![[IXDBVersionMgr queryDBVersion] count]) {
                [IXDataProcessTools saveDBVersion];
            }
            [IXUserInfoMgr shareInstance].userLogInfo = info;
            if (info.account) {
                if (![IXDBAccountMgr saveAccountInfo:info.account
                                                               userId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid] ) {
                    ELog(@"proto_user_login_info 保存失败");
                }
            } else {
                ELog(@"proto_user_login_info account is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_user_login_info result error! info.comment:%@",info.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        //更新用户信息
        [IXTradeDataCache shareInstance].pb_user_login_info = obj;
#warning 切换账号对应的操作；没有切换账号对应的指令码被建议写在这里；建议添加对应的cmd。
        [IXTradeDataCache shareInstance].swiAccount = ![IXTradeDataCache shareInstance].swiAccount;
    });
}

+ (void)user_add:(id)obj
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_user_add = obj;
    });
}

+ (void)user_kick_out:(id)obj
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_user_kick_out = obj;
    });
}

+ (void)user_login_data_total:(id)obj
{
    proto_user_login_data_total *pb = (proto_user_login_data_total *)obj;
    if(!pb) {
        ELog(@"proto_user_login_data_total 反序列化失败");
    } else {
        if (pb.symTotal == 0) {
            [IXUserInfoMgr shareInstance].symFlag = YES;
        }
        if (pb.symcataTotal == 0) {
            [IXUserInfoMgr shareInstance].symCataFlag = YES;
        }
        if (pb.accgroupSymcataTotal == 0) {
            [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
        }
        [IXTradeDataFilter saveAccGroupSymCata];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_user_login_data_total = obj;
    });
}

+ (void)user_login_data:(id)obj
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_user_login_data = obj;
    });
}

#pragma mark -
#pragma mark - account

+ (void)account_list:(id)obj
{
    proto_account_list *pb = (proto_account_list *)obj;
    if (!pb) {
        ELog(@"proto_account_list 反序列化失败");
    } else {
        if(pb.accountArray.count){
            if ([IXDBAccountMgr saveBatchAccountInfos:pb.accountArray userId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid]) {
                if ((pb.accountMt4LicArray.count &&
                    [IXDBAccountMgr saveBatchAccountInfos:pb.accountMt4LicArray userId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid]) || pb.accountMt4LicArray.count == 0) {
                    if (pb.offset + pb.count - 1 == pb.total) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [IXTradeDataCache shareInstance].pb_account_list = obj;
                        });
                    }
                } else {
                    ELog(@"proto_account_list 保存失败");
                }
            } else {
                ELog(@"proto_account_list 保存失败");
            }
        } else {
            WLog(@"proto_account_list 列表为空");
        }
    }
}

+ (void)account_update:(id)obj
{
    proto_account_update *pb = (proto_account_update *)obj;
    if(!pb) {
        ELog(@"proto_account_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.account)
            {
                if (![IXDBAccountMgr saveAccountInfo:pb.account userId:pb.account.userid]) {
                    ELog(@"proto_account_update 保存失败");
                }
                if (pb.account.id_p == [IXUserInfoMgr shareInstance].userLogInfo.account.id_p) {
                    [IXUserInfoMgr shareInstance].userLogInfo.account = pb.account;
                }
            } else {
                ELog(@"proto_account_update account is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_account_update result error! comment is : %@",pb.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_account_update = obj;
    });
}

+ (void)account_add:(id)obj
{
    proto_account_add *pb = (proto_account_add *)obj;
    if(!pb) {
        ELog(@"proto_account_add 反序列化失败");
    } else {
        if (pb.result == 0 && pb.account) {
            if (![IXDBAccountMgr saveAccountInfo:pb.account userId:pb.account.userid]) {
                ELog(@"proto_account_add 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_account_add result error! comment is : %@",pb.comment]);
        }
    }
}

//account_group
+ (void)account_group_list:(id)obj
{
    proto_account_group_list *pb = (proto_account_group_list *)obj;
    if (!pb) {
        ELog(@"proto_account_group_list 反序列化失败");
    } else {
        if (pb.accountGroupArray.count) {
            if ([IXDBAccountGroupMgr saveBatchAccountGroupInfos:pb.accountGroupArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_account_group_list = obj;
                    });
                }
            } else {
                ELog(@"proto_account_group_list 保存失败");
            }
        } else {
            WLog(@"proto_account_group_list 列表为空");
        }
    }
}

+ (void)account_group_update:(id)obj
{
    proto_account_group_update *pb = (proto_account_group_update *)obj;
    if (!pb) {
        ELog(@"proto_account_group_update 反序列化失败");
    } else {
        if (pb.accountGroup) {
            if ([IXDBAccountGroupMgr saveAccountGroupInfo:pb.accountGroup]) {
                
            } else {
                ELog(@"proto_account_group_update 保存失败");
            }
        } else {
            WLog(@"proto_account_group_update 列表为空");
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_account_group_update = obj;
    });
}

+ (void)account_group_add:(id)obj
{
    proto_account_group_add *pb = (proto_account_group_add *)obj;
    if (!pb) {
        ELog(@"proto_account_group_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.accountGroup) {
                if ([IXDBAccountGroupMgr saveAccountGroupInfo:pb.accountGroup]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_account_group_add = obj;
                    });
                } else {
                    ELog(@"proto_account_group_add 保存失败");
                }
            } else {
                WLog(@"proto_account_group_add 列表为空");
            }
        }
    }
}

+ (void)account_group_delete:(id)obj
{
    proto_account_group_delete *pb = (proto_account_group_delete *)obj;
    if (!pb) {
        ELog(@"proto_account_group_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            if ([IXDBAccountGroupMgr deleteAccountGroup:pb.accountGroup]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_account_group_delete = obj;
                });
            } else {
                ELog(@"proto_account_group_delete 保存失败");
            }
        } else {
            WLog(@"proto_account_group_delete 列表为空");
        }
    }
}

+ (void)account_group_symbol_cata_list:(id)obj
{
    proto_account_group_symbol_cata_list *pb = (proto_account_group_symbol_cata_list *)obj;
    if(!pb) {
        ELog(@"proto_account_group_symbol_cata_list 反序列化失败");
    } else {
        
        [IXUserInfoMgr shareInstance].accSymCataCount += pb.count;
        if ([IXUserInfoMgr shareInstance].accSymCataCount == pb.total) {
            
            [IXUserInfoMgr shareInstance].accSymCataCount = 0;
            [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
        }
        if (pb.accountGroupSymbolCataArray) {
            if ([IXDBAccountGroupSymCataMgr saveBatchAccountGroupSymCataInfos:pb.accountGroupSymbolCataArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    [IXTradeDataFilter saveAccGroupSymCata];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [IXTradeDataCache shareInstance].pb_account_group_symbol_cata_list = obj;
                    });
                }
            } else {
                
                ELog(@"proto_account_group_symbol_cata_list 保存失败");
            }
        } else {
            WLog(@"proto_account_group_symbol_cata_list 列表为空");
        }
    }
}

+ (void)account_group_symbol_cata_add:(id)obj
{
    proto_account_group_symbol_cata_add *pb = (proto_account_group_symbol_cata_add *)obj;
    if(!pb) {
        ELog(@"proto_account_group_symbol_cata_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.accountGroupSymbolCata) {
                
                [IXUserInfoMgr shareInstance].symFlag = YES;
                [IXUserInfoMgr shareInstance].symCataFlag = YES;
                [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
                if ([IXDBAccountGroupSymCataMgr saveAccountGroupSymCataInfo:pb.accountGroupSymbolCata]) {
                    
                    [IXTradeDataFilter saveAccGroupSymCata];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_account_group_symbol_cata_add = obj;
                    });
                    
                } else {
                    
                    ELog(@"proto_account_group_symbol_cata_add 保存失败");
                }
            } else {
                ELog(@"proto_account_group_symbol_cata_add accountGroupSymbolCata is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_account_group_symbol_cata_add result error! comment is : %@",pb.comment]);
        }
    }
}

+ (void)account_group_symbol_cata_update:(id)obj
{
    proto_account_group_symbol_cata_update *pb = (proto_account_group_symbol_cata_update *)obj;
    if(!pb) {
        ELog(@"proto_account_group_symbol_cata_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.accountGroupSymbolCata) {
                [IXUserInfoMgr shareInstance].symFlag = YES;
                [IXUserInfoMgr shareInstance].symCataFlag = YES;
                [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
                if ([IXDBAccountGroupSymCataMgr saveAccountGroupSymCataInfo:pb.accountGroupSymbolCata]) {
                    
                    [IXTradeDataFilter saveAccGroupSymCata];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_account_group_symbol_cata_update = obj;
                        
                        WLog(@"监听到点差更新！！！！！！！！");
                    });
                } else {
                    
                    ELog(@"proto_account_group_symbol_cata_update 保存失败");
                }
            } else {
                ELog(@"proto_account_group_symbol_cata_update accountGroupSymbolCata is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_account_group_symbol_cata_update result error! comment is : %@",pb.comment]);
        }
    }
}

+ (void)account_group_symbol_cata_delete:(id)obj {
    
    proto_account_group_symbol_cata_delete *pb = (proto_account_group_symbol_cata_delete *)obj;
    if(!pb) {
        ELog(@"proto_account_group_symbol_cata_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            
            item_account_group_symbol_cata *model = [[item_account_group_symbol_cata alloc] init];
            model.id_p = pb.id_p;
            [IXUserInfoMgr shareInstance].symFlag = YES;
            [IXUserInfoMgr shareInstance].symCataFlag = YES;
            [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
            if ([IXDBAccountGroupSymCataMgr deleteAccountGroupSymCata:model]) {
                
                [IXTradeDataFilter saveAccGroupSymCata];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_account_group_symbol_cata_delete = obj;
                });
            } else {
                
                ELog(@"proto_account_group_symbol_cata_delete 保存失败");
            }
        } else {
            ELog(@"proto_account_group_symbol_cata_delete accountGroupSymbolCata is NULL!");
        }
    }
}


#pragma mark -
#pragma mark - symbol

+ (void)symbol_list:(id)obj
{
    proto_symbol_list *pb = (proto_symbol_list *)obj;
    if(!pb) {
        ELog(@"proto_symbol_list 反序列化失败");
    } else {
        [IXUserInfoMgr shareInstance].symCount += pb.count;
        if ([IXUserInfoMgr shareInstance].symCount == pb.total) {
            [IXUserInfoMgr shareInstance].symCount = 0;
            [IXUserInfoMgr shareInstance].symFlag = YES;
        }
        if (pb.symbolArray.count) {
            if ([IXDBSymbolMgr saveBatchSymbolInfos:pb.symbolArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    [IXTradeDataFilter saveAccGroupSymCata];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_list = obj;
                        [[IXTradeDataCache shareInstance] dealWithSymbolChange];
                    });
                }
            } else {
                ELog(@"proto_symbol_list 保存失败");
            }
        } else {
            WLog(@"proto_symbol_list 列表为空");
        }
    }
}

+ (void)symbol_update:(id)obj
{
    proto_symbol_update *pb = (proto_symbol_update *)obj;
    if(!pb) {
        ELog(@"proto_symbol_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbol) {
                
                [IXUserInfoMgr shareInstance].symFlag = YES;
                [IXUserInfoMgr shareInstance].symCataFlag = YES;
                [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
                [IXTradeDataFilter saveAccGroupSymCata];
                if ([IXDBSymbolMgr saveSymbolInfo:pb.symbol]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_update = obj;
                        [[IXTradeDataCache shareInstance] dealWithSymbolChange];
                    });
                } else {
                    ELog(@"proto_symbol_update 保存失败");
                }
            } else {
                ELog(@"proto_symbol_update symbol is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_symbol_update result error! comment is : %@",pb.comment]);
        }
    }
}

+ (void)symbol_add:(id)obj
{
    proto_symbol_add *pb = (proto_symbol_add *)obj;
    if(!pb) {
        ELog(@"proto_symbol_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbol) {
                
                [IXUserInfoMgr shareInstance].symFlag = YES;
                [IXUserInfoMgr shareInstance].symCataFlag = YES;
                [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
                [IXTradeDataFilter saveAccGroupSymCata];
                if ([IXDBSymbolMgr saveSymbolInfo:pb.symbol]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_add = obj;
                        [[IXTradeDataCache shareInstance] dealWithSymbolChange];
                    });
                } else {
                    ELog(@"proto_symbol_add 保存失败");
                }
            } else {
                ELog(@"proto_symbol_add symbol is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_symbol_add result error! comment is : %@",pb.comment]);
        }
    }
}

+ (void)symbol_delete:(id)obj
{
    proto_symbol_delete *pb = (proto_symbol_delete *)obj;
    if(!pb) {
        ELog(@"proto_symbol_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_symbol *symbol = [[item_symbol alloc] init];
            symbol.id_p = pb.id_p;
            if ([IXDBSymbolMgr deleteSymbolInfo:symbol]) {
                
                [IXUserInfoMgr shareInstance].symFlag = YES;
                [IXTradeDataFilter saveAccGroupSymCata];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_symbol_delete = obj;
                    [[IXTradeDataCache shareInstance] dealWithSymbolChange];
                });
            } else {
                ELog(@"proto_symbol_delete 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_symbol_delete result error! comment is : %@",pb.comment]);
        }
    }
}

#pragma mark -
#pragma mark - symbol_sub

+ (void)symbol_sub_list:(id)obj
{
    proto_symbol_sub_list *pb = (proto_symbol_sub_list *)obj;
    if (!pb) {
        ELog(@"proto_symbol_sub_list 反序列化失败");
    } else {
        if (pb.symbolSubArray.count) {
            /** 删除自选重复商品 */
            NSMutableArray *subArr = [[NSMutableArray alloc] init];
            [subArr addObject:pb.symbolSubArray[0]];
            BOOL flag = NO;
            for (int i = 1; i < pb.symbolSubArray.count; i++) {
                flag = NO;
                item_symbol_hot *tmpModel = [[item_symbol_hot alloc] init];
                tmpModel = pb.symbolSubArray[i];
                for (int j = 0; j < subArr.count; j++) {
                    item_symbol_hot *subModel = [[item_symbol_hot alloc] init];
                    subModel = subArr[j];
                    if (tmpModel.symbolid == subModel.symbolid) {
                        flag = YES;
                        break;
                    }
                }
                if (!flag) {
                    [subArr addObject:tmpModel];
                }
            }
            if (![IXDBSymbolSubMgr saveBatchSymbolSubInfos:subArr accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p]) {
                ELog(@"proto_symbol_sub_list 保存失败");
            }
        } else {
            WLog(@"proto_symbol_sub_list 列表为空");
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_symbol_sub_list = pb;
    });
}

+ (void)symbol_sub_add:(id)obj
{
    proto_symbol_sub_add *pb = (proto_symbol_sub_add *)obj;
    if (!pb) {
        ELog(@"proto_symbol_sub_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (![IXDBSymbolSubMgr saveSymbolSubInfo:pb.symbolSub accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p]) {
                ELog(@"proto_symbol_sub_add 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_symbol_sub_add result error! comment is : %@",pb.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_symbol_sub_add = obj;
    });
}

+ (void)symbol_sub_delete:(id)obj
{
    proto_symbol_sub_delete *pb = (proto_symbol_sub_delete *)obj;
    if (!pb) {
        ELog(@"proto_symbol_sub_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_symbol_sub *tmpModel = [[item_symbol_sub alloc] init];
            tmpModel.id_p = pb.id_p;
            if ([IXDBSymbolSubMgr deleteSymbolSubInfo:tmpModel accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p]) {
                NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
                tmpDic = [[IXDBSearchBrowseMgr queryAllSearchBrowserHistoryInfoUserId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p symbolSubId:pb.id_p] mutableCopy];
                if (tmpDic && tmpDic.count > 0) {
                    tmpDic[kEnable] = @(0);
                    [IXDBSearchBrowseMgr saveSearchBrowserHistoryInfoSymbolId:tmpDic userId:[IXUserInfoMgr shareInstance].userLogInfo.account.userid accountId:[IXUserInfoMgr shareInstance].userLogInfo.account.id_p];
                }
            } else {
                ELog(@"proto_symbol_sub_delete 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_symbol_sub_delete result error! comment is : %@",pb.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_symbol_sub_delete = obj;
    });
}

+ (void)symbol_hot_list:(id)obj
{
    proto_symbol_hot_list *pb = (proto_symbol_hot_list *)obj;
    if (!pb) {
        ELog(@"proto_symbol_hot_list 反序列化失败");
    } else {
        if (pb.symbolHotArray.count) {
            if ([IXDBSymbolHotMgr saveBatchSymbolHotInfos:pb.symbolHotArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_hot_list = obj;
                    });
                }
            } else {
                ELog(@"proto_symbol_hot_list 保存失败");
            }
        } else {
            WLog(@"proto_symbol_hot_list 列表为空");
        }
    }
}

+ (void)symbol_hot_update:(id)obj
{
    proto_symbol_hot_update *pb = (proto_symbol_hot_update *)obj;
    if (!pb) {
        ELog(@"proto_symbol_hot_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolHot) {
                if ([IXDBSymbolHotMgr saveSymbolHotInfo:pb.symbolHot]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_hot_update = obj;
                    });
                } else {
                    ELog(@"proto_symbol_hot_update 保存失败");
                }
            } else {
                WLog(@"proto_symbol_hot_update 列表为空");
            }
        }
    }
}

+ (void)symbol_hot_add:(id)obj
{
    proto_symbol_hot_add *pb = (proto_symbol_hot_add *)obj;
    if (!pb) {
        ELog(@"proto_symbol_hot_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolHot) {
                if ([IXDBSymbolHotMgr saveSymbolHotInfo:pb.symbolHot]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_hot_add = obj;
                    });
                } else {
                    ELog(@"proto_symbol_hot_add 保存失败");
                }
            } else {
                WLog(@"proto_symbol_hot_add 列表为空");
            }
        }
    }
}

+ (void)symbol_cata_list:(id)obj
{
    proto_symbol_cata_list *pb = (proto_symbol_cata_list *)obj;
    if (!pb) {
        ELog(@"proto_symbol_cata_list 反序列化失败");
    } else {
        
        [IXUserInfoMgr shareInstance].symCataCount += pb.count;
        if ([IXUserInfoMgr shareInstance].symCataCount == pb.total) {
            
            [IXUserInfoMgr shareInstance].symCataCount = 0;
            [IXUserInfoMgr shareInstance].symCataFlag = YES;
        }
        if (pb.symbolcataArray.count) {
            if ([IXDBSymbolCataMgr saveBatchSymbolCatasInfos:pb.symbolcataArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    [IXTradeDataFilter saveAccGroupSymCata];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_cata_list = obj;
                    });
                }
            } else {
                ELog(@"proto_symbol_cata_list 保存失败");
            }
        } else {
            WLog(@"proto_symbol_cata_list 列表为空");
        }
    }
}

+ (void)symbol_cata_update:(id)obj
{
    proto_symbol_cata_update *pb = (proto_symbol_cata_update *)obj;
    if (!pb) {
        ELog(@"proto_symbol_cata_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolcata) {
                
                [IXUserInfoMgr shareInstance].symFlag = YES;
                [IXUserInfoMgr shareInstance].symCataFlag = YES;
                [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
                if ([IXDBSymbolCataMgr saveSymbolCataInfo:pb.symbolcata]) {
                    
                    [IXTradeDataFilter saveAccGroupSymCata];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_cata_update = obj;
                    });
                } else {
                    ELog(@"proto_symbol_cata_update 保存失败");
                }
            } else {
                WLog(@"proto_symbol_cata_update 列表为空");
            }
        }
    }
}

+ (void)symbol_cata_add:(id)obj
{
    proto_symbol_cata_add *pb = (proto_symbol_cata_add *)obj;
    if (!pb) {
        ELog(@"proto_symbol_cata_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolcata) {
                
                [IXUserInfoMgr shareInstance].symFlag = YES;
                [IXUserInfoMgr shareInstance].symCataFlag = YES;
                [IXUserInfoMgr shareInstance].accSymCataFlag = YES;
                if ([IXDBSymbolCataMgr saveSymbolCataInfo:pb.symbolcata]) {
                    
                    [IXTradeDataFilter saveAccGroupSymCata];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_cata_add = obj;
                    });
                } else {
                    ELog(@"proto_symbol_cata_add 保存失败");
                }
            } else {
                WLog(@"proto_symbol_cata_add 列表为空");
            }
        }
    }
}

+ (void)symbol_cata_delete:(id)obj
{
    proto_symbol_cata_delete *pb = (proto_symbol_cata_delete *)obj;
    if(!pb) {
        ELog(@"proto_symbol_cata_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_symbol_cata *symCata = [[item_symbol_cata alloc] init];
            symCata.id_p = pb.id_p;
            if ([IXDBSymbolCataMgr deleteSymbolCataInfo:symCata]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_symbol_cata_delete = obj;
                });
            } else {
                ELog(@"proto_symbol_cata_delete 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_symbol_cata_delete result error! comment is : %@",pb.comment]);
        }
    }
}

+ (void)symbol_lable_list:(id)obj
{
    proto_symbol_label_list *pb = (proto_symbol_label_list *)obj;
    if (!pb) {
        ELog(@"proto_symbol_label_list 反序列化失败");
    } else {
        if (pb.symbolLabelArray.count) {
            if ([IXDBSymbolLableMgr saveBatchSymbolLableInfos:pb.symbolLabelArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_lable_list = obj;
                    });
                }
            } else {
                ELog(@"proto_symbol_label_list 保存失败");
            }
        } else {
            WLog(@"proto_symbol_label_list 列表为空");
        }
    }
}

+ (void)symbol_lable_update:(id)obj
{
    proto_symbol_label_update *pb = (proto_symbol_label_update *)obj;
    if (!pb) {
        ELog(@"proto_symbol_label_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolLabel) {
                if ([IXDBSymbolLableMgr saveSymbolLableInfo:pb.symbolLabel]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_lable_update = obj;
                    });
                } else {
                    ELog(@"proto_symbol_label_update 保存失败");
                }
            } else {
                WLog(@"proto_symbol_label_update 列表为空");
            }
        }
    }
}

+ (void)symbol_lable_add:(id)obj
{
    proto_symbol_label_add *pb = (proto_symbol_label_add *)obj;
    if (!pb) {
        ELog(@"proto_symbol_label_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolLabel) {
                if ([IXDBSymbolLableMgr saveSymbolLableInfo:pb.symbolLabel]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_lable_add = obj;
                    });
                } else {
                    ELog(@"proto_symbol_label_add 保存失败");
                }
            } else {
                WLog(@"proto_symbol_label_add 列表为空");
            }
        }
    }
}

+ (void)symbol_lable_delete:(id)obj
{
    proto_symbol_label_delete *pb = (proto_symbol_label_delete *)obj;
    if (!pb) {
        ELog(@"proto_symbol_label_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_symbol_label *symLabel = [[item_symbol_label alloc] init];
            symLabel.id_p = pb.id_p;
            if ([IXDBSymbolLableMgr deleteLableInfo:symLabel]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_symbol_lable_delete = obj;
                });
            } else {
                ELog(@"proto_symbol_label_delete 保存失败");
            }
        } else {
            ELog(@"proto_symbol_label_delete 保存失败");
        }
    }
}

+ (void)symbol_margin_set_list:(id)obj
{
    proto_symbol_margin_set_list *pb = (proto_symbol_margin_set_list *)obj;
    if (!pb) {
        ELog(@"proto_symbol_margin_set_list 反序列化失败");
    } else {
        if (pb.symbolMarginSetArray.count) {
            if ([IXDBMarginSetMgr saveBatchMarginSetInfos:pb.symbolMarginSetArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_margin_set_list = obj;
                    });
                }
            } else {
                ELog(@"proto_symbol_margin_set_list 保存失败");
            }
        } else {
            WLog(@"proto_symbol_margin_set_list 列表为空");
        }
    }
}

+ (void)symbol_margin_set_update:(id)obj
{
    proto_symbol_margin_set_update *pb = (proto_symbol_margin_set_update *)obj;
    if (!pb) {
        ELog(@"proto_symbol_margin_set_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolMarginSet) {
                if ([IXDBMarginSetMgr saveMarginSetInfo:pb.symbolMarginSet]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_margin_set_update = obj;
                    });
                } else {
                    ELog(@"proto_symbol_margin_set_update 保存失败");
                }
            } else {
                WLog(@"proto_symbol_margin_set_update 列表为空");
            }
        }
    }
}

+ (void)symbol_margin_set_add:(id)obj
{
    proto_symbol_margin_set_add *pb = (proto_symbol_margin_set_add *)obj;
    if (!pb) {
        ELog(@"proto_symbol_margin_set_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.symbolMarginSet) {
                if ([IXDBMarginSetMgr saveMarginSetInfo:pb.symbolMarginSet]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_symbol_margin_set_add = obj;
                    });
                } else {
                    ELog(@"proto_symbol_margin_set_add 保存失败");
                }
            } else {
                WLog(@"proto_symbol_margin_set_add 列表为空");
            }
        }
    }
}

+ (void)symbol_margin_set_delete:(id)obj
{
    proto_symbol_margin_set_delete *pb = (proto_symbol_margin_set_delete *)obj;
    if(!pb) {
        ELog(@"proto_symbol_margin_set_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_symbol_margin_set *symMarginSet = [[item_symbol_margin_set alloc] init];
            symMarginSet.id_p = pb.id_p;
            if ([IXDBMarginSetMgr deleteMarginSetInfo:symMarginSet]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_symbol_margin_set_delete = obj;
                });
            } else {
                ELog(@"proto_symbol_margin_set_delete 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_symbol_margin_set_delete result error! comment is : %@",pb.comment]);
        }
    }
}

#pragma mark -
#pragma mark - holiday

+ (void)holiday_cata_list:(id)obj
{
    proto_holiday_cata_list *pb = (proto_holiday_cata_list *)obj;
    if (!pb) {
        ELog(@"proto_holiday_cata_list 反序列化失败");
    } else {
        if (pb.holidayCataArray.count) {
            if ([IXDBHolidayCataMgr saveBatchHolidayCatasInfos:pb.holidayCataArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_cata_list = obj;
                    });
                }
            } else {
                ELog(@"proto_holiday_cata_list 保存失败");
            }
        } else {
            WLog(@"proto_holiday_cata_list 列表为空");
        }
    }
}

+ (void)holiday_cata_update:(id)obj
{
    proto_holiday_cata_update *pb = (proto_holiday_cata_update *)obj;
    if (!pb) {
        ELog(@"proto_holiday_cata_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.holidayCata) {
                if ([IXDBHolidayCataMgr saveHolidayCataInfo:pb.holidayCata]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_cata_update = obj;
                    });
                } else {
                    ELog(@"proto_holiday_cata_update 保存失败");
                }
            } else {
                WLog(@"proto_holiday_cata_update 列表为空");
            }
        }
    }
}

+ (void)holiday_cata_add:(id)obj
{
    proto_holiday_cata_add *pb = (proto_holiday_cata_add *)obj;
    if (!pb) {
        ELog(@"proto_holiday_cata_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.holidayCata) {
                if ([IXDBHolidayCataMgr saveHolidayCataInfo:pb.holidayCata]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_cata_add = obj;
                    });
                } else {
                    ELog(@"proto_holiday_cata_add 保存失败");
                }
            } else {
                WLog(@"proto_holiday_cata_add 列表为空");
            }
        }
    }
}

+ (void)holiday_cata_delete:(id)obj
{
    proto_holiday_cata_delete *pb = (proto_holiday_cata_delete *)obj;
    if (!pb) {
        ELog(@"proto_holiday_cata_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            if ([IXDBHolidayCataMgr deleteHolidayCata:pb]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_holiday_cata_delete = obj;
                });
            } else {
                ELog(@"proto_holiday_cata_delete 删除失败");
            }
        } else {
            WLog(@"proto_holiday_cata_delete 列表为空");
        }
    }
}

+ (void)holiday_list:(id)obj
{
    proto_holiday_list *pb = (proto_holiday_list *)obj;
    if (!pb) {
        ELog(@"proto_holiday_list 反序列化失败");
    } else {
        if (pb.holidayArray.count) {
            if ([IXDBHolidayMgr saveBatchHolidayInfos:pb.holidayArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_cata_list = obj;
                    });
                }
            } else {
                ELog(@"proto_holiday_list 保存失败");
            }
        } else {
            WLog(@"proto_holiday_list 列表为空");
        }
    }
}

+ (void)holiday_update:(id)obj
{
    proto_holiday_update *pb = (proto_holiday_update *)obj;
    if (!pb) {
        ELog(@"proto_holiday_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.holiday) {
                if ([IXDBHolidayMgr saveHolidayInfo:pb.holiday]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_cata_update = obj;
                    });
                } else {
                    ELog(@"proto_holiday_update 保存失败");
                }
            } else {
                WLog(@"proto_holiday_update 列表为空");
            }
        }
    }
}

+ (void)holiday_add:(id)obj
{
    proto_holiday_add *pb = (proto_holiday_add *)obj;
    if (!pb) {
        ELog(@"proto_holiday_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.holiday) {
                if ([IXDBHolidayMgr saveHolidayInfo:pb.holiday]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_cata_add = obj;
                    });
                } else {
                    ELog(@"proto_holiday_update 保存失败");
                }
            } else {
                WLog(@"proto_holiday_update 列表为空");
            }
        }
    }
}

+ (void)holiday_delete:(id)obj
{
    proto_holiday_delete *pb = (proto_holiday_delete *)obj;
    if (!pb) {
        ELog(@"proto_holiday_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            if ([IXDBHolidayMgr deleteHoliday:pb]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_holiday_delete = obj;
                });
            } else {
                ELog(@"proto_holiday_delete 删除失败");
            }
        } else {
            WLog(@"proto_holiday_delete 列表为空");
        }
    }
}

+ (void)holiday_margin_list:(id)obj
{
    proto_holiday_margin_list *pb = (proto_holiday_margin_list *)obj;
    if (!pb) {
        ELog(@"proto_holiday_margin_list 反序列化失败");
    } else {
        if (pb.holidayMarginArray.count) {
            if ([IXDBHolidayMarginMgr saveBatchHolidayMarginInfos:pb.holidayMarginArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_margin_list = obj;
                    });
                }
            } else {
                ELog(@"proto_holiday_margin_list 列表为空");
            }
        } else {
            WLog(@"proto_holiday_margin_list 列表为空");
        }
    }
}

+ (void)holiday_margin_update:(id)obj
{
    proto_holiday_margin_update *pb = (proto_holiday_margin_update *)obj;
    if (!pb) {
        ELog(@"proto_holiday_margin_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.holidayMargin) {
                if ([IXDBHolidayMarginMgr saveHolidayMarginInfo:pb.holidayMargin]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_margin_update = obj;
                    });
                } else {
                    ELog(@"proto_holiday_margin_update 列表为空");
                }
            } else {
                WLog(@"proto_holiday_margin_update 列表为空");
            }
        }
    }
}

+ (void)holiday_margin_add:(id)obj
{
    proto_holiday_margin_add *pb = (proto_holiday_margin_add *)obj;
    if (!pb) {
        ELog(@"proto_holiday_margin_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.holidayMargin) {
                if ([IXDBHolidayMarginMgr saveHolidayMarginInfo:pb.holidayMargin]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_margin_add = obj;
                    });
                } else {
                    ELog(@"proto_holiday_margin_add 列表为空");
                }
            } else {
                WLog(@"proto_holiday_margin_add 列表为空");
            }
        }
    }
}

+ (void)holiday_margin_delete:(id)obj
{
    proto_holiday_margin_delete *pb = (proto_holiday_margin_delete *)obj;
    if(!pb) {
        ELog(@"proto_holiday_margin_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_holiday_margin *holidayMargin = [[item_holiday_margin alloc] init];
            holidayMargin.id_p = pb.id_p;
            if ([IXDBHolidayMarginMgr deleteHolidayMarginInfo:holidayMargin]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_holiday_margin_delete = obj;
                    });
            } else {
                ELog(@"proto_holiday_margin_delete 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_holiday_margin_delete result error! comment is : %@",pb.comment]);
        }
    }
}

#pragma mark -
#pragma mark - company

+ (void)company_list:(id)obj
{
    proto_company_list *pb = (proto_company_list *)obj;
    if (!pb) {
        ELog(@"proto_company_list 反序列化失败");
    } else {
        if (pb.companyArray.count) {
            if ([IXDBCompanyMgr saveBatchCompanyInfos:pb.companyArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_company_list = obj;
                    });
                }
            } else {
                ELog(@"proto_company_list 保存失败");
            }
        } else {
            WLog(@"proto_company_list 列表为空");
        }
    }
}

+ (void)company_update:(id)obj
{
    proto_company_update *pb = (proto_company_update *)obj;
    if (!pb) {
        ELog(@"proto_company_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.company) {
                if ([IXDBCompanyMgr saveCompanyInfo:pb.company]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_company_update = obj;
                    });
                } else {
                    ELog(@"proto_company_update 保存失败");
                }
            } else {
                WLog(@"proto_company_update 列表为空");
            }
        }
    }
}

+ (void)company_add:(id)obj
{
    proto_company_add *pb = (proto_company_add *)obj;
    if (!pb) {
        ELog(@"proto_company_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.company) {
                if ([IXDBCompanyMgr saveCompanyInfo:pb.company]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_company_add= obj;
                    });
                } else {
                    ELog(@"proto_company_add 保存失败");
                }
            } else {
                WLog(@"proto_company_add 列表为空");
            }
        }
    }
}

+ (void)company_delete:(id)obj
{
    proto_company_delete *pb = (proto_company_delete *)obj;
    if(!pb) {
        ELog(@"proto_company_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_company *company = [[item_company alloc] init];
            company.id_p = pb.id_p;
            if ([IXDBCompanyMgr deleteCompanyInfo:company]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_company_delete= obj;
                });
            } else {
                ELog(@"proto_company_delete 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_company_delete result error! comment is : %@",pb.comment]);
        }
    }
}

#pragma mark -
#pragma mark - schedule

+ (void)schedule_list:(id)obj
{
    proto_schedule_list *pb = (proto_schedule_list *)obj;
    if (!pb) {
        ELog(@"proto_schedule_list 反序列化失败");
    } else {
        if (pb.scheduleArray.count) {
            if ([IXDBScheduleMgr saveBatchScheduleInfos:pb.scheduleArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_list = obj;
                    });
                }
            } else {
                ELog(@"proto_schedule_list 保存失败");
            }
        } else {
            WLog(@"proto_schedule_list 列表为空");
        }
    }
}

+ (void)schedule_update:(id)obj
{
    proto_schedule_update *pb = (proto_schedule_update *)obj;
    if (!pb) {
        ELog(@"proto_schedule_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            
            if (pb.schedule) {
                if ([IXDBScheduleMgr saveScheduleInfo:pb.schedule]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_update = obj;
                    });
                } else {
                    ELog(@"proto_schedule_update 保存失败");
                }
            } else {
                WLog(@"proto_schedule_update 列表为空");
            }
        }
    }
}

+ (void)schedule_add:(id)obj
{
    proto_schedule_add *pb = (proto_schedule_add *)obj;
    if (!pb) {
        ELog(@"proto_schedule_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            
            if (pb.schedule) {
                if ([IXDBScheduleMgr saveScheduleInfo:pb.schedule]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_add = obj;
                    });
                } else {
                    ELog(@"proto_schedule_add 保存失败");
                }
            } else {
                WLog(@"proto_schedule_add 列表为空");
            }
        }
    }
}

+ (void)schedule_delete:(id)obj
{
    proto_schedule_delete *pb = (proto_schedule_delete *)obj;
    if (!pb) {
        ELog(@"proto_schedule_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            if ([IXDBScheduleMgr deleteSchedule:pb]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_schedule_delete = obj;
                });
            } else {
                ELog(@"proto_schedule_delete 删除失败");
            }
        } else {
            WLog(@"proto_schedule_delete 列表为空");
        }
    }
}

+ (void)schedule_cata_list:(id)obj
{
    proto_schedule_cata_list *pb = (proto_schedule_cata_list *)obj;
    if (!pb) {
        ELog(@"proto_schedule_cata_list 反序列化失败");
    } else {
        if (pb.scheduleCataArray.count) {
            if ([IXDBScheduleCataMgr saveBatchScheduleCataInfos:pb.scheduleCataArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_cata_list = obj;
                    });
                }
            } else {
                ELog(@"proto_schedule_cata_list 保存失败");
            }
        } else {
            WLog(@"proto_schedule_cata_list 列表为空");
        }
    }
}

+ (void)schedule_cata_update:(id)obj
{
    proto_schedule_cata_update *pb = (proto_schedule_cata_update *)obj;
    if (!pb) {
        ELog(@"proto_schedule_cata_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.scheduleCata) {
                if ([IXDBScheduleCataMgr saveScheduleCataInfo:pb.scheduleCata]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_cata_update = obj;
                    });
                } else {
                    ELog(@"proto_schedule_cata_update 保存失败");
                }
            } else {
                WLog(@"proto_schedule_cata_update 列表为空");
            }
        }
    }
}

+ (void)schedule_cata_add:(id)obj
{
    proto_schedule_cata_add *pb = (proto_schedule_cata_add *)obj;
    if (!pb) {
        ELog(@"proto_schedule_cata_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.scheduleCata) {
                if ([IXDBScheduleCataMgr saveScheduleCataInfo:pb.scheduleCata]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_cata_add = obj;
                    });
                } else {
                    ELog(@"proto_schedule_cata_add 保存失败");
                }
            } else {
                WLog(@"proto_schedule_cata_add 列表为空");
            }
        }
    }
}

+ (void)schedule_cata_delete:(id)obj
{
    proto_schedule_cata_delete *pb = (proto_schedule_cata_delete *)obj;
    if (!pb) {
        ELog(@"proto_schedule_cata_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            if ([IXDBScheduleCataMgr deleteScheduleCata:pb]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_schedule_cata_delete = obj;
                });
            } else {
                ELog(@"proto_schedule_cata_delete 删除失败");
            }
        } else {
            WLog(@"proto_schedule_cata_delete 列表为空");
        }
    }
}

//schedule_margin
+ (void)schedule_margin_list:(id)obj
{
    proto_schedule_margin_list *pb = (proto_schedule_margin_list *)obj;
    if (!pb) {
        ELog(@"proto_schedule_margin_list 反序列化失败");
    } else {
        if (pb.scheduleMarginArray) {
            if ([IXDBScheduleMarginMgr saveBatchScheduleMarginInfos:pb.scheduleMarginArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_margin_list = obj;
                    });
                }
            } else {
                ELog(@"proto_schedule_margin_list 保存失败");
            }
        } else {
            WLog(@"proto_schedule_margin_list 列表为空");
        }
    }
}

+ (void)schedule_margin_update:(id)obj
{
    proto_schedule_margin_update *pb = (proto_schedule_margin_update *)obj;
    if (!pb) {
        ELog(@"proto_schedule_margin_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.scheduleMargin) {
                if ([IXDBScheduleMarginMgr saveScheduleMarginInfo:pb.scheduleMargin]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_margin_update = obj;
                    });
                } else {
                    ELog(@"proto_schedule_margin_update 保存失败");
                }
            } else {
                WLog(@"proto_schedule_margin_update 列表为空");
            }
        }
    }
}

+ (void)schedule_margin_add:(id)obj
{
    proto_schedule_margin_add *pb = (proto_schedule_margin_add *)obj;
    if (!pb) {
        ELog(@"proto_schedule_margin_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.scheduleMargin) {
                if ([IXDBScheduleMarginMgr saveScheduleMarginInfo:pb.scheduleMargin]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_schedule_margin_add = obj;
                    });
                } else {
                    ELog(@"proto_schedule_margin_add 保存失败");
                }
            } else {
                WLog(@"proto_schedule_margin_add 列表为空");
            }
        }
    }
}

+ (void)schedule_margin_delete:(id)obj
{
    proto_schedule_margin_delete *pb = (proto_schedule_margin_delete *)obj;
    if(!pb) {
        ELog(@"proto_schedule_margin_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_schedule_margin *scheduleMargin = [[item_schedule_margin alloc] init];
            scheduleMargin.id_p = pb.id_p;
            if ([IXDBScheduleMarginMgr deleteScheduleMarginInfo:scheduleMargin]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_schedule_margin_delete = obj;
                });
            } else {
                ELog(@"proto_schedule_margin_delete 保存失败");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_schedule_margin_delete result error! comment is : %@",pb.comment]);
        }
    }
}

#pragma mark -
#pragma mark - pisiton
+ (void)position_list:(id)obj
{
    proto_position_list *pb = (proto_position_list *)obj;
    if (!pb) {
        ELog(@"proto_position_list 反序列化失败");
    } else {
        if (pb.positionArray.count) {
                [[IXTradeDataCache shareInstance] addPb_cache_position_list:pb.positionArray];
        } else {
            WLog(@"proto_position_list 列表为空");
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ( pb && pb.positionArray.count ) {
            [IXTradeDataCache shareInstance].pb_position_list = pb;
        } else {
            ELog(@"proto_position_list 数据异常");
        }
    });
}

+ (void)position_add:(id)obj
{
    proto_position_add *pb = (proto_position_add *)obj;
    if (!pb) {
        ELog(@"proto_position_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.position) {
                if (pb.position.status == item_position_estatus_StatusOpened) {
                    [[IXTradeDataCache shareInstance] addPb_cache_position_list:(NSMutableArray *)@[pb.position]];
                }
            } else {
                ELog(@"proto_position_add position is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_position_add result error! comment is : %@",pb.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_position_add = pb;
    });
}

+ (void)position_update:(id)obj
{
    proto_position_update *pb = (proto_position_update *)obj;
    if (!pb) {
        ELog(@"proto_position_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.position) {
                //全部平仓
                if (pb.position.status == item_position_estatus_StatusClosed) {
                    [[IXTradeDataCache shareInstance] deletePb_cache_position_list:pb.position.id_p];
                } else if (pb.position.status == item_position_estatus_StatusOpened) {
                    [[IXTradeDataCache shareInstance] updatePb_cache_position_list:pb.position];
                } //部分平仓
            } else {
                ELog(@"proto_position_update position is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_position_update result error! comment is : %@",pb.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_position_update = pb;
    });
}

#pragma mark -
#pragma mark - order

+ (void)order_list:(id)obj
{
    proto_order_list *pb = (proto_order_list *)obj;
    if (!pb) {
        ELog(@"proto_order_list 反序列化失败");
    } else {
        if (pb.orderArray.count) {
            [[IXTradeDataCache shareInstance] addPb_cache_order_list:pb.orderArray];
        } else {
            WLog(@"proto_order_list 列表为空");
        }
    }
    
    if (pb.offset + pb.count - 1 == pb.total) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [IXTradeDataCache shareInstance].pb_order_list = pb;
        });
    }
}

+ (void)order_update:(id)obj
{
    proto_order_update *pb = (proto_order_update *)obj;
    if (!pb) {
        ELog(@"proto_order_update 反序列化失败");
    } else{
        if (pb.result == 0) {
            if (pb.order) {
                if (pb.order.status == item_order_estatus_StatusFilled ||
                    pb.order.status == item_order_estatus_StatusRejected) {
                    [[IXTradeDataCache shareInstance] deletePb_cache_order_list:pb.order.id_p];
                } else if (pb.order.status == item_order_estatus_StatusPlaced) {
                    [[IXTradeDataCache shareInstance] updatePb_cache_order:pb.order];
                }
            } else {
                ELog(@"proto_order_update order is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_order_update result error! comment is : %@",pb.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_order_update = pb;
    });
}

+ (void)order_add:(id)obj
{
    proto_order_add *pb = (proto_order_add *)obj;
    if (!pb) {
        ELog(@"proto_order_add 反序列化失败");
    } else {
        if (pb.order) {
            if (pb.result == 0) {
                //已成交
                if (pb.order.status == item_order_estatus_StatusPlaced) {
                    [[IXTradeDataCache shareInstance] addPb_cache_order_list:(NSMutableArray *)@[pb.order]];
                }
            } else if (pb.order.status == item_order_estatus_StatusRejected) {
                //已拒绝
                [[IXTradeDataCache shareInstance] deletePb_cache_order_list:pb.order.id_p];
            } else {
                ELog(@"warning: proto_order_add unprocess status!");
            }
        } else {
            ELog(@"proto_order_add order is NULL!");
        }
    }
    dispatch_sync(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_order_add = pb;
    });
}

+ (void)order_cancel:(id)obj
{
    proto_order_cancel *pb = (proto_order_cancel *)obj;
    if (!pb) {
        ELog(@"proto_order_cancel 反序列化失败");
    } else{
        if (pb.order) {
            if (pb.result == 0) {
                //已成交
                [[IXTradeDataCache shareInstance] deletePb_cache_order_list:pb.order.id_p];
            } else {
                ELog(@"warning: proto_order_cancel unprocess status!");
            }
        } else {
            ELog(@"proto_order_cancel order is NULL!");
        }
    }
    dispatch_sync(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_order_cancel = pb;
    });
}

+ (void)order_add_batch:(id)obj
{
    proto_order_add_batch *pb = (proto_order_add_batch *)obj;
    if (!pb) {
        ELog(@"proto_order_add_batch 反序列化失败");
    }
    dispatch_sync(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_order_add_batch = pb;
    });
}

#pragma mark -
#pragma mark - deal

+ (void)deal_list:(id)obj
{
    proto_deal_list *pb = (proto_deal_list *)obj;
    if (!pb) {
        ELog(@"proto_deal_list 反序列化失败");
    } else {
        if (pb.dealArray.count) {
            [[IXTradeDataCache shareInstance] addPb_cache_deal_list:pb.dealArray];
        } else {
            WLog(@"proto_deal_list 列表为空");
        }
    }
    if (pb.offset + pb.count - 1 == pb.total) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [IXTradeDataCache shareInstance].pb_deal_list = pb;
        });
    }
}

+ (void)deal_add:(id)obj
{
    proto_deal_add *pb = (proto_deal_add *)obj;
    if (!pb) {
        ELog(@"proto_deal_add 反序列化失败");
    } else {
        if (pb.deal) {
            if (pb.deal.proposalNo.length == 0) {
                [[IXTradeDataCache shareInstance] addPb_cache_deal_list:(NSMutableArray *)@[pb.deal]];
            }
        } else {
            ELog(@"proto_deal_add deal is NULL!");
        }
    }
    //开仓立马被强平需同步刷新UI
    dispatch_sync(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_deal_add = obj;
    });
}

+ (void)deal_update:(id)obj
{
    proto_deal_update *pb = (proto_deal_update *)obj;
    if (!pb) {
        ELog(@"proto_deal_update 反序列化失败");
    } else{
        if (pb.deal) {
            if (pb.result == 0) {
                //已成交
                if (pb.deal.status == item_order_estatus_StatusPlaced) {
                    [IXTradeDataCache shareInstance].pb_deal_update = pb;
                } else {
                    ELog(@"warning: proto_deal_update unprocess status!");
                }
            } else {
                ELog([NSString stringWithFormat:@"proto_deal_update result error! comment is : %@",pb.comment]);
            }
        } else {
            ELog(@"proto_deal_update deal is NULL!");
        }
    }
    [[IXTradeDataCache shareInstance] deletePb_cache_order_list:pb.deal.id_p];
}

#pragma mark - language
+ (void)language_list:(id)obj
{
    proto_language_list *pb = (proto_language_list *)obj;
    if (!pb) {
        ELog(@"proto_language_list 反序列化失败");
    } else {
        if (pb.languageArray.count) {
            if (![IXDBLanguageMgr saveBatchLanguageInfos:pb.languageArray]) {
                ELog(@"proto_language_list 保存失败");
            }
        } else {
            WLog(@"proto_language_list 列表为空");
        }
    }
    if (pb.offset + pb.count - 1 == pb.total) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [IXTradeDataCache shareInstance].pb_language_list = obj;
        });
    }
}

+ (void)language_add:(id)obj
{
    proto_language_add *pb = (proto_language_add *)obj;
    if (!pb) {
        ELog(@"proto_language_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.language) {
                if (![IXDBLanguageMgr saveBatchLanguageInfos:@[pb.language]]) {
                    ELog(@"proto_language_add 保存失败");
                }
            } else {
                ELog(@"proto_language_add language is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_language_add result error! comment is : %@",pb.comment]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_language_list = obj;
    });
}

+ (void)language_update:(id)obj
{
    proto_language_update *pb = (proto_language_update *)obj;
    if (!pb) {
        ELog(@"proto_language_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.language) {
                if (![IXDBLanguageMgr saveLanguageInfo:pb.language]) {
                    ELog(@"proto_language_update 保存失败");
                }
            } else {
                ELog(@"proto_language_update language is NULL!");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_language_update result error! comment is : %@",pb.comment]);
        }
    }
}

#pragma mark - accountGroupSymbolCata
+ (void)accountGroupSymbolCata_list:(id)obj
{
    proto_account_group_symbol_cata_list *pb = (proto_account_group_symbol_cata_list *)obj;
    if(!pb) {
        ELog(@"proto_account_group_symbol_cata_list 反序列化失败");
    } else {
        if (pb.accountGroupSymbolCataArray > 0) {
            if (![IXDBAccountGroupSymCataMgr saveBatchAccountGroupSymCataInfos:pb.accountGroupSymbolCataArray]) {
                ELog(@"proto_account_group_symbol_cata_list 保存失败");
            }
        } else {
            WLog(@"proto_account_group_symbol_cata_list 列表为空");
        }
    }
}

+ (void)accountGroupSymbolCata_add:(id)obj
{
    proto_account_group_symbol_cata_add *pb = (proto_account_group_symbol_cata_add *)obj;
    if(!pb) {
        ELog(@"proto_account_group_symbol_cata_add 反序列化失败");
    } else {
        if (pb.result== 0) {
            if (pb.accountGroupSymbolCata) {
                if (![IXDBAccountGroupSymCataMgr saveAccountGroupSymCataInfo:pb.accountGroupSymbolCata]) {
                    ELog(@"proto_account_group_symbol_cata_add 保存失败");
                }
            } else {
                WLog(@"proto_account_group_symbol_cata_add accountGroupSymbolCata is NULL");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_account_group_symbol_cata_add result error! comment is : %@",pb.comment]);
        }
    }
}

+ (void)accountGroupSymbolCata_update:(id)obj
{
    proto_account_group_symbol_cata_update *pb = (proto_account_group_symbol_cata_update *)obj;
    if(!pb) {
        ELog(@"proto_account_group_symbol_cata_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.accountGroupSymbolCata) {
                if (![IXDBAccountGroupSymCataMgr saveAccountGroupSymCataInfo:pb.accountGroupSymbolCata]) {
                    ELog(@"proto_account_group_symbol_cata_update 保存失败");
                }
            } else {
                WLog(@"proto_account_group_symbol_cata_update accountGroupSymbolCata is NULL");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_account_group_symbol_cata_update result error! comment is : %@",pb.comment]);
        }
    }
}

#pragma mark - accountBalance
+ (void)accountBalance_get:(id)obj
{
    proto_account_balance_get *pb = (proto_account_balance_get *)obj;
    dispatch_async(dispatch_get_main_queue(), ^{
        [IXTradeDataCache shareInstance].pb_account_balance_get = pb;
    });
}

#pragma mark - symbol_lable
+ (void)symbolLable_list:(id)obj
{
    proto_symbol_label_list *pb = (proto_symbol_label_list *)obj;
    if (!pb) {
        ELog(@"proto_symbol_label_list 反序列化失败");
    } else {
        if (pb.symbolLabelArray.count > 0) {
            if (![IXDBSymbolLableMgr saveBatchSymbolLableInfos:pb.symbolLabelArray]) {
                ELog(@"proto_symbol_label_list 保存失败");
            }
        } else {
            WLog(@"proto_symbol_label_list 列表为空");
        }
    }
}

+ (void)quoteDelay_list:(id)obj
{
    proto_quote_delay_list *pb = (proto_quote_delay_list *)obj;
    if (!pb) {
        ELog(@"proto_quote_delay_list 反序列化失败");
    } else {
        if (pb.quoteDelayArray > 0) {
            for (int i = 0; i < pb.quoteDelayArray.count; i++) {
                item_quote_delay *qdPb = (item_quote_delay *)pb.quoteDelayArray[i];
                if (qdPb) {
                    NSDictionary *qdDic = [IXDBQuoteDelayMgr queryQuoteDelayByCataId:qdPb.symbolCataid userid:qdPb.userid];
                    if (qdDic && qdDic.count > 0) {
                        if (![IXDBQuoteDelayMgr deleteQuoteDelayInfo:qdPb]) {
                            ELog(@"proto_quote_delay_list 删除失败");
                        }
                    }
                }
            }
            if (![IXDBQuoteDelayMgr saveBatchQuoteDelayInfos:pb.quoteDelayArray]) {
                ELog(@"proto_quote_delay_list 保存失败");
            } else {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_quote_delay_list    = obj;
                    });
                }
            }
        } else {
            WLog(@"proto_quote_delay_list 列表为空");
        }
    }
}

+ (void)quoteDelay_add:(id)obj
{
    proto_quote_delay_add *pb = (proto_quote_delay_add *)obj;
    if (!pb) {
        ELog(@"proto_quote_delay_add 反序列化失败");
        return;
    } else {
        if (pb.result == 0) {
            if (pb.quoteDelay) {
                NSDictionary *qdDic = [IXDBQuoteDelayMgr queryQuoteDelayByCataId:pb.quoteDelay.symbolCataid userid:pb.quoteDelay.userid];
                if (qdDic && qdDic.count > 0) {
                    if (![IXDBQuoteDelayMgr deleteQuoteDelayInfo:pb.quoteDelay]) {
                        ELog(@"proto_quote_delay_add 删除失败");
                    }
                }
                if (![IXDBQuoteDelayMgr saveQuoteDelayInfo:pb.quoteDelay]) {
                    ELog(@"proto_quote_delay_add 保存失败");
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_quote_delay_add = obj;
                    });
                }
            } else {
                ELog(@"proto_quote_delay_add quoteDelay is NULL");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_quote_delay_add result error! comment is :%@",pb.comment]);
        }
    }
}

+ (void)quoteDelay_update:(id)obj
{
    proto_quote_delay_update *pb = (proto_quote_delay_update *)obj;
    if (!pb) {
        ELog(@"proto_quote_delay_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if(pb.quoteDelay) {
                NSDictionary *qdDic = [IXDBQuoteDelayMgr queryQuoteDelayByCataId:pb.quoteDelay.symbolCataid userid:pb.quoteDelay.userid];
                if (qdDic && qdDic.count > 0) {
                    if (![IXDBQuoteDelayMgr deleteQuoteDelayInfo:pb.quoteDelay]) {
                        ELog(@"proto_quote_delay_update 删除失败");
                    }
                }
                if (![IXDBQuoteDelayMgr saveQuoteDelayInfo:pb.quoteDelay]) {
                    ELog(@"proto_quote_delay_update 保存失败");
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_quote_delay_update = obj;
                    });
                }
            } else {
                ELog(@"proto_quote_delay_update quoteDelay is NULL");
            }
        } else {
            ELog([NSString stringWithFormat:@"proto_quote_delay_update result error! comment is :%@",pb.comment]);
        }
    }
}

+ (void)eod_time_list:(id)obj
{
    proto_eod_time_list *pb = (proto_eod_time_list *)obj;
    if (!pb) {
        ELog(@"proto_eod_time_list 反序列化失败");
    } else {
        if (pb.eodTimeArray.count) {
            if ([IXDBEodTimeMgr saveBatchEodTimeInfos:pb.eodTimeArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_eod_time_list = obj;
                    });
                }
            } else {
                ELog(@"proto_eod_time_list 保存失败");
            }
        } else {
            WLog(@"proto_eod_time_list 列表为空");
        }
    }
}
+ (void)eod_time_update:(id)obj;
{
    proto_eod_time_update *pb = (proto_eod_time_update *)obj;
    if (!pb) {
        ELog(@"proto_eod_time_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.eodTime) {
                if ([IXDBEodTimeMgr saveEodTimeInfo:pb.eodTime]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_eod_time_update = obj;
                    });
                } else {
                    ELog(@"proto_eod_time_update 保存失败");
                }
            } else {
                WLog(@"proto_eod_time_update 列表为空");
            }
        }
    }
}
+ (void)eod_time_add:(id)obj
{
    proto_eod_time_add *pb = (proto_eod_time_add *)obj;
    if (!pb) {
        ELog(@"proto_eod_time_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.eodTime) {
                if ([IXDBEodTimeMgr saveEodTimeInfo:pb.eodTime]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_eod_time_add = obj;
                    });
                } else {
                    ELog(@"proto_eod_time_add 保存失败");
                }
            } else {
                WLog(@"proto_eod_time_add 列表为空");
            }
        }
    }
}
+ (void)eod_time_delete:(id)obj
{
    proto_eod_time_delete *pb = (proto_eod_time_delete *)obj;
    if (!pb) {
        ELog(@"proto_eod_time_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_eod_time *eodTime = [[item_eod_time alloc] init];
            eodTime.id_p = pb.id_p;
            if ([IXDBEodTimeMgr deleteEodTimeInfo:eodTime]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_eod_time_delete = obj;
                });
            } else {
                ELog(@"proto_eod_time_delete 删除失败");
            }
        } else {
            WLog(@"proto_eod_time_delete 列表为空");
        }
    }
}

#pragma mark secure_dev

+ (void)secure_dev_list:(id)obj
{
    proto_secure_dev_list *pb = (proto_secure_dev_list *)obj;
    if (!pb) {
        ELog(@"proto_secure_dev_list 反序列化失败");
    } else {
        if (pb.secureDevArray.count) {
            if (pb.offset <= 1) {
                if ([IXDBSecureDevMgr deleteSecureDevInfos]) {
                } else {
                    ELog(@"proto_secure_dev_list 清空失败");
                }
            }
            if ([IXDBSecureDevMgr saveBatchSecureDevInfos:pb.secureDevArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_secure_dev_list = obj;
                    });
                }
            } else {
                ELog(@"proto_secure_dev_list 保存失败");
            }
        } else {
            WLog(@"proto_secure_dev_list 列表为空");
        }
    }
}
+ (void)secure_dev_update:(id)obj
{
    proto_secure_dev_update *pb = (proto_secure_dev_update *)obj;
    if (!pb) {
        ELog(@"proto_secure_dev_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.secureDev) {
                if ([IXDBSecureDevMgr saveSecureDevInfo:pb.secureDev]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_secure_dev_update = obj;
                    });
                } else {
                    ELog(@"proto_secure_dev_update 保存失败");
                }
            } else {
                WLog(@"proto_secure_dev_update 列表为空");
            }
        }
    }
}
+ (void)secure_dev_add:(id)obj
{
    proto_secure_dev_add *pb = (proto_secure_dev_add *)obj;
    if (!pb) {
        ELog(@"proto_secure_dev_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.secureDev) {
                if ([IXDBSecureDevMgr saveSecureDevInfo:pb.secureDev]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_secure_dev_add = obj;
                    });
                } else {
                    ELog(@"proto_secure_dev_add 保存失败");
                }
            } else {
                WLog(@"proto_secure_dev_add 列表为空");
            }
        }
    }
}
+ (void)secure_dev_delete:(id)obj
{
    proto_secure_dev_delete *pb = (proto_secure_dev_delete *)obj;
    if (!pb) {
        ELog(@"proto_secure_dev_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_secure_dev *secureDev = [[item_secure_dev alloc] init];
            secureDev.id_p = pb.id_p;
            if ([IXDBSecureDevMgr deleteSecureDevInfo:secureDev userid:pb.userid]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_eod_time_delete = obj;
                });
            } else {
                ELog(@"proto_secure_dev_delete 删除失败");
            }
        } else {
            WLog(@"proto_secure_dev_delete 列表为空");
        }
    }
}

#pragma mark group_sym
+ (void)group_sym_list:(id)obj
{
    proto_group_symbol_list *pb = (proto_group_symbol_list *)obj;
    
    if (!pb) {
        ELog(@"proto_group_symbol_list 反序列化失败");
    } else {
        if (pb.groupSymbolArray.count) {
            if ([IXDBG_SMgr saveBatchG_SInfos:pb.groupSymbolArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_group_symbol_list = obj;
                    });
                }
            } else {
                ELog(@"proto_group_symbol_list 保存失败");
            }
        } else {
            WLog(@"proto_group_symbol_list 列表为空");
        }
    }
}
+ (void)group_sym_update:(id)obj
{
    proto_group_symbol_update *pb = (proto_group_symbol_update *)obj;
    if (!pb) {
        ELog(@"proto_group_symbol_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.groupSymbolArray) {
                if ([IXDBG_SMgr saveBatchG_SInfos:pb.groupSymbolArray]) {
                    if ([IXTradeDataFilter saveAccGroupSymCata]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [IXTradeDataCache shareInstance].pb_group_symbol_update = obj;
                        });
                    }
                } else {
                    ELog(@"proto_group_symbol_update 保存失败");
                }
            } else {
                WLog(@"proto_group_symbol_update 列表为空");
            }
        }
    }
}
+ (void)group_sym_add:(id)obj
{
    proto_group_symbol_add *pb = (proto_group_symbol_add *)obj;
    if (!pb) {
        ELog(@"proto_group_symbol_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.groupSymbolArray) {
                if ([IXDBG_SMgr saveBatchG_SInfos:pb.groupSymbolArray]) {
                    if ([IXTradeDataFilter saveAccGroupSymCata]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [IXTradeDataCache shareInstance].pb_group_symbol_add = obj;
                        });
                    }
                } else {
                    ELog(@"proto_group_symbol_add 保存失败");
                }
            } else {
                WLog(@"proto_group_symbol_add 列表为空");
            }
        }
    }
}
+ (void)group_sym_delete:(id)obj
{
    proto_group_symbol_delete *pb = (proto_group_symbol_delete *)obj;
    if (!pb) {
        ELog(@"proto_group_symbol_delete 反序列化失败");
    } else {
        if (pb.result == 0 && pb.idsArray.count) {
            if ([IXDBG_SMgr batchDeleteG_SInfo:pb.idsArray]) {
                if ([IXTradeDataFilter saveAccGroupSymCata]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_group_symbol_delete = obj;
                    });
                }
            } else {
                ELog(@"proto_group_symbol_delete 删除失败");
            }
        } else {
            WLog(@"proto_group_symbol_delete 列表为空");
        }
    }
}

#pragma mark  group_sym_cata
+ (void)group_sym_cata_list:(id)obj
{
    proto_group_symbol_cata_list *pb = (proto_group_symbol_cata_list *)obj;
    if (!pb) {
        ELog(@"proto_group_symbol_cata_list 反序列化失败");
    } else {
        if (pb.groupSymbolCataArray.count) {
            if ([IXDBG_S_CataMgr saveBatchG_S_CataInfos:pb.groupSymbolCataArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                       [IXTradeDataCache shareInstance].pb_group_symbol_cata_list = obj;
                    });
                }
            } else {
                ELog(@"proto_group_symbol_cata_list 保存失败");
            }
        } else {
            WLog(@"proto_group_symbol_cata_list 列表为空");
        }
    }

}
+ (void)group_sym_cata_update:(id)obj
{
    proto_group_symbol_cata_update *pb = (proto_group_symbol_cata_update *)obj;
    if (!pb) {
        ELog(@"proto_group_symbol_cata_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.groupSymbolCataArray) {
                if ([IXDBG_S_CataMgr saveBatchG_S_CataInfos:pb.groupSymbolCataArray]) {
                    if ([IXTradeDataFilter saveAccGroupSymCata]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [IXTradeDataCache shareInstance].pb_group_symbol_cata_update = obj;
                        });
                    }
                } else {
                    ELog(@"proto_group_symbol_cata_update 保存失败");
                }
            } else {
                WLog(@"proto_group_symbol_cata_update 列表为空");
            }
        }
    }
}
+ (void)group_sym_cata_add:(id)obj
{
    proto_group_symbol_cata_add *pb = (proto_group_symbol_cata_add *)obj;
    if (!pb) {
        ELog(@"proto_group_symbol_cata_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.groupSymbolCataArray) {
                if ([IXDBG_S_CataMgr saveBatchG_S_CataInfos:pb.groupSymbolCataArray]) {
                    if ([IXTradeDataFilter saveAccGroupSymCata]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [IXTradeDataCache shareInstance].pb_group_symbol_cata_add = obj;
                        });
                    }
                } else {
                    ELog(@"proto_group_symbol_cata_add 保存失败");
                }
            } else {
                WLog(@"proto_group_symbol_cata_add 列表为空");
            }
        }
    }

}
+ (void)group_sym_cata_delete:(id)obj
{
    proto_group_symbol_cata_delete *pb = (proto_group_symbol_cata_delete *)obj;
    if (!pb) {
        ELog(@"proto_group_symbol_cata_delete 反序列化失败");
    } else {
        if (pb.result == 0 && pb.idsArray.count) {
            if ([IXDBG_S_CataMgr batchDeleteG_S_CataInfo:pb.idsArray]) {
                if ([IXTradeDataFilter saveAccGroupSymCata]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_group_symbol_cata_delete = obj;
                    });
                }
            } else {
                ELog(@"proto_group_symbol_cata_delete 删除失败");
            }
        } else {
            WLog(@"proto_group_symbol_cata_delete 列表为空");
        }
    }

}

#pragma mark lp_channel_account
+ (void)lpchacc_list:(id)obj
{
    proto_lpchacc_list *pb = (proto_lpchacc_list *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_list 反序列化失败");
    } else {
        if (pb.lpchaccArray.count) {
            if ([IXDBLpchaccMgr saveBatchLpchaccInfos:pb.lpchaccArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchacc_list = obj;
                    });
                }
            } else {
                ELog(@"proto_lpchacc_list 保存失败");
            }
        } else {
            WLog(@"proto_lpchacc_list 列表为空");
        }
    }
}
+ (void)lpchacc_update:(id)obj;
{
    proto_lpchacc_update *pb = (proto_lpchacc_update *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchacc) {
                if ([IXDBLpchaccMgr saveLpchaccInfo:pb.lpchacc]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchacc_update = obj;
                    });
                } else {
                    ELog(@"proto_lpchacc_update 保存失败");
                }
            } else {
                WLog(@"proto_lpchacc_update 列表为空");
            }
        }
    }
}
+ (void)lpchacc_add:(id)obj
{
    proto_lpchacc_add *pb = (proto_lpchacc_add *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchacc) {
                if ([IXDBLpchaccMgr saveLpchaccInfo:pb.lpchacc]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchacc_add = obj;
                    });
                } else {
                    ELog(@"proto_lpchacc_add 保存失败");
                }
            } else {
                WLog(@"proto_lpchacc_add 列表为空");
            }
        }
    }
}
+ (void)lpchacc_delete:(id)obj
{
    proto_lpchacc_delete *pb = (proto_lpchacc_delete *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_lpchacc *lpchacc = [[item_lpchacc alloc] init];
            lpchacc.id_p = pb.id_p;
            if ([IXDBLpchaccMgr deleteLpchacc:lpchacc]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_lpchacc_delete = obj;
                });
            } else {
                ELog(@"proto_lpchacc_delete 删除失败");
            }
        } else {
            WLog(@"proto_lpchacc_delete 列表为空");
        }
    }
}

#pragma mark lp_channel_account_symbol
+ (void)lpchacc_symbol_list:(id)obj
{
    proto_lpchacc_symbol_list *pb = (proto_lpchacc_symbol_list *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_symbol_list 反序列化失败");
    } else {
        if (pb.lpchaccSymbolArray.count) {
            if ([IXDBLpchaccSymbolMgr saveBatchLpchaccSymbolInfos:pb.lpchaccSymbolArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchacc_symbol_list = obj;
                    });
                }
            } else {
                ELog(@"proto_lpchacc_symbol_list 保存失败");
            }
        } else {
            WLog(@"proto_lpchacc_symbol_list 列表为空");
        }
    }
}
+ (void)lpchacc_symbol_update:(id)obj
{
    proto_lpchacc_symbol_update *pb = (proto_lpchacc_symbol_update *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_symbol_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchaccSymbol) {
                if ([IXDBLpchaccSymbolMgr saveLpchaccSymbolInfo:pb.lpchaccSymbol]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchacc_symbol_update = obj;
                    });
                } else {
                    ELog(@"proto_lpchacc_symbol_update 保存失败");
                }
            } else {
                WLog(@"proto_lpchacc_symbol_update 列表为空");
            }
        }
    }
}
+ (void)lpchacc_symbol_add:(id)obj
{
    proto_lpchacc_symbol_add *pb = (proto_lpchacc_symbol_add *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_symbol_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchaccSymbol) {
                if ([IXDBLpchaccSymbolMgr saveLpchaccSymbolInfo:pb.lpchaccSymbol]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchacc_symbol_add = obj;
                    });
                } else {
                    ELog(@"proto_lpchacc_symbol_add 保存失败");
                }
            } else {
                WLog(@"proto_lpchacc_symbol_add 列表为空");
            }
        }
    }
}
+ (void)lpchacc_symbol_delete:(id)obj
{
    proto_lpchacc_symbol_delete *pb = (proto_lpchacc_symbol_delete *)obj;
    if (!pb) {
        ELog(@"proto_lpchacc_symbol_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_lpchacc_symbol *lpchaccSym = [[item_lpchacc_symbol alloc] init];
            lpchaccSym.id_p = pb.id_p;
            if ([IXDBLpchaccSymbolMgr deleteLpchaccSymbol:lpchaccSym]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_lpchacc_symbol_delete = obj;
                });
            } else {
                ELog(@"proto_lpchacc_symbol_delete 删除失败");
            }
        } else {
            WLog(@"proto_lpchacc_symbol_delete 列表为空");
        }
    }
}

#pragma mark lp_channel
+ (void)lpchannel_list:(id)obj
{
    proto_lpchannel_list *pb = (proto_lpchannel_list *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_list 反序列化失败");
    } else {
        if (pb.lpchannelArray.count) {
            if ([IXDBLpchannelMgr saveBatchLpchannelInfos:pb.lpchannelArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchannel_list = obj;
                    });
                }
            } else {
                ELog(@"proto_lpchannel_list 保存失败");
            }
        } else {
            WLog(@"proto_lpchannel_list 列表为空");
        }
    }
}
+ (void)lpchannel_update:(id)obj
{
    proto_lpchannel_update *pb = (proto_lpchannel_update *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchannel) {
                if ([IXDBLpchannelMgr saveLpchannelInfo:pb.lpchannel]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchannel_update = obj;
                    });
                } else {
                    ELog(@"proto_lpchannel_update 保存失败");
                }
            } else {
                WLog(@"proto_lpchannel_update 列表为空");
            }
        }
    }
}
+ (void)lpchannel_add:(id)obj
{
    proto_lpchannel_add *pb = (proto_lpchannel_add *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchannel) {
                if ([IXDBLpchannelMgr saveLpchannelInfo:pb.lpchannel]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchannel_add = obj;
                    });
                } else {
                    ELog(@"proto_lpchannel_add 保存失败");
                }
            } else {
                WLog(@"proto_lpchannel_add 列表为空");
            }
        }
    }
}
+ (void)lpchannel_delete:(id)obj
{
    proto_lpchannel_delete *pb = (proto_lpchannel_delete *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_lpchannel *lpchannel = [[item_lpchannel alloc] init];
            lpchannel.id_p = pb.id_p;
            if ([IXDBLpchannelMgr deleteLpchannel:lpchannel]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_lpchannel_delete = obj;
                });
            } else {
                ELog(@"proto_lpchannel_delete 删除失败");
            }
        } else {
            WLog(@"proto_lpchannel_delete 列表为空");
        }
    }
}

#pragma mark lp_channel_symbol
+ (void)lpchannel_symbol_list:(id)obj
{
    proto_lpchannel_symbol_list *pb = (proto_lpchannel_symbol_list *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_symbol_list 反序列化失败");
    } else {
        if (pb.lpchannelSymbolArray.count) {
            if ([IXDBLpchannelSymbolMgr saveBatchLpchannelSymbolInfos:pb.lpchannelSymbolArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchannel_symbol_list = obj;
                    });
                }
            } else {
                ELog(@"proto_lpchannel_symbol_list 保存失败");
            }
        } else {
            WLog(@"proto_lpchannel_symbol_list 列表为空");
        }
    }
}
+ (void)lpchannel_symbol_update:(id)obj
{
    proto_lpchannel_symbol_update *pb = (proto_lpchannel_symbol_update *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_symbol_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchannelSymbol) {
                if ([IXDBLpchannelSymbolMgr saveLpchannelSymbolInfo:pb.lpchannelSymbol]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchannel_symbol_update = obj;
                    });
                } else {
                    ELog(@"proto_lpchannel_symbol_update 保存失败");
                }
            } else {
                WLog(@"proto_lpchannel_symbol_update 列表为空");
            }
        }
    }
}
+ (void)lpchannel_symbol_add:(id)obj
{
    proto_lpchannel_symbol_add *pb = (proto_lpchannel_symbol_add *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_symbol_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpchannelSymbol) {
                if ([IXDBLpchannelSymbolMgr saveLpchannelSymbolInfo:pb.lpchannelSymbol]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpchannel_symbol_add = obj;
                    });
                } else {
                    ELog(@"proto_lpchannel_symbol_add 保存失败");
                }
            } else {
                WLog(@"proto_lpchannel_symbol_add 列表为空");
            }
        }
    }
}
+ (void)lpchannel_symbol_delete:(id)obj
{
    proto_lpchannel_symbol_delete *pb = (proto_lpchannel_symbol_delete *)obj;
    if (!pb) {
        ELog(@"proto_lpchannel_symbol_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_lpchannel_symbol *lpchannelSym = [[item_lpchannel_symbol alloc] init];
            lpchannelSym.id_p = pb.id_p;
            if ([IXDBLpchannelSymbolMgr deleteLpchannelSymbol:lpchannelSym]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_lpchannel_symbol_delete = obj;
                });
            } else {
                ELog(@"proto_lpchannel_symbol_delete 删除失败");
            }
        } else {
            WLog(@"proto_lpchannel_symbol_delete 列表为空");
        }
    }
}

#pragma mark lp_IbBind
+ (void)lpib_bind_list:(id)obj
{
    proto_lpib_bind_list *pb = (proto_lpib_bind_list *)obj;
    if (!pb) {
        ELog(@"proto_lpib_bind_list 反序列化失败");
    } else {
        if (pb.lpibBindArray.count) {
            if ([IXDBLpibBindMgr saveBatchLpibBindInfos:pb.lpibBindArray]) {
                if (pb.offset + pb.count - 1 == pb.total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpib_bind_list = obj;
                    });
                }
            } else {
                ELog(@"proto_lpib_bind_list 保存失败");
            }
        } else {
            WLog(@"proto_lpib_bind_list 列表为空");
        }
    }
}
+ (void)lpib_bind_update:(id)obj
{
    proto_lpib_bind_update *pb = (proto_lpib_bind_update *)obj;
    if (!pb) {
        ELog(@"proto_lpib_bind_update 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpibBind) {
                if ([IXDBLpibBindMgr saveLpibBindInfo:pb.lpibBind]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpib_bind_update = obj;
                    });
                } else {
                    ELog(@"proto_lpib_bind_update 保存失败");
                }
            } else {
                WLog(@"proto_lpib_bind_update 列表为空");
            }
        }
    }
}
+ (void)lpib_bind_add:(id)obj
{
    proto_lpib_bind_add *pb = (proto_lpib_bind_add *)obj;
    if (!pb) {
        ELog(@"proto_lpib_bind_add 反序列化失败");
    } else {
        if (pb.result == 0) {
            if (pb.lpibBind) {
                if ([IXDBLpibBindMgr saveLpibBindInfo:pb.lpibBind]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [IXTradeDataCache shareInstance].pb_lpib_bind_add = obj;
                    });
                } else {
                    ELog(@"proto_lpib_bind_add 保存失败");
                }
            } else {
                WLog(@"proto_lpib_bind_add 列表为空");
            }
        }
    }
}
+ (void)lpib_bind_delete:(id)obj
{
    proto_lpib_bind_delete *pb = (proto_lpib_bind_delete *)obj;
    if (!pb) {
        ELog(@"proto_lpib_bind_delete 反序列化失败");
    } else {
        if (pb.result == 0) {
            item_lpib_bind *lpIbBind = [[item_lpib_bind alloc] init];
            lpIbBind.id_p = pb.id_p;
            if ([IXDBLpibBindMgr deleteLpibBind:lpIbBind]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].pb_lpib_bind_delete = obj;
                });
            } else {
                ELog(@"proto_lpib_bind_delete 删除失败");
            }
        } else {
            WLog(@"proto_lpib_bind_delete 列表为空");
        }
    }
}

+ (void)kline_repair_list:(id)obj
{
    proto_kline_repair_list *pb = (proto_kline_repair_list *)obj;
    if (!pb) {
        ELog(@"proto_kline_repair_list 反序列化失败");
    } else {
        if (pb.klineRepairArray.count) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [IXTradeDataCache shareInstance].pb_kLine_repare_noti = obj;
            });
        } else {
            WLog(@"proto_quote_delay_list 列表为空");
        }
    }
}

#pragma mark - others
+ (BOOL)saveAccGroupSymCata
{
    if ([IXUserInfoMgr shareInstance].symFlag &&
        [IXUserInfoMgr shareInstance].symCataFlag &&
        [IXUserInfoMgr shareInstance].accSymCataFlag) {
        if ([IXDBGroupSymbolMgr deleteGroupSymbolInfos] &&
            [IXDBGroupSymCataMgr deleteGroupSymbolCataInfos]) {
            if ([IXDataProcessTools dealwithAccGroupSymCata]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IXTradeDataCache shareInstance].accGroupSymCataFlag = ![IXTradeDataCache shareInstance].accGroupSymCataFlag;
                });
                return YES;
            }
        }
    }
    return NO;
}

@end
