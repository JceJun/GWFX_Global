//
//  IXQuoteCacheDigit.h
//  IXApp
//
//  Created by Bob on 2017/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXQuoteCacheDigit : NSObject

+ (IXQuoteCacheDigit *)shareIntance;

- (NSString *)transforPrice:(NSInteger)price WithSymbolId:(NSInteger)symbolId;

@end
