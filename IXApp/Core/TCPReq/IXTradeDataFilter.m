//
//  IXTradeDataFilter.m
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeDataFilter.h"
#import "IXTradeDataFilter+process.h"
#import "IXTradeDataFilter+DataBase.h"

@interface IXTradeDataFilter ()
{
    dispatch_queue_t tQueue;
    NSMutableData    *_clipCT;
    NSMutableArray   *_frameCT;
}

@property (nonatomic,strong) NSDictionary *cmds;

@end

@implementation IXTradeDataFilter

- (id)init
{
    self = [super init];
    if (self) {
        tQueue = dispatch_queue_create("TradeDataFilter_Serial", DISPATCH_QUEUE_SERIAL);
        _clipCT = [[NSMutableData alloc] init];
        _frameCT = [[NSMutableArray alloc] init];
#ifdef DEBUG
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"IXCmdMap" ofType:@"plist"];
        self.cmds = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
#endif
    }
    return self;
}

- (void)filtrate:(NSData *)data
{
    if (![data length]) {
        ELog(@"Trade data is NULL!");
        return;
    }
    
    dispatch_async(tQueue, ^{
        [self processData:data];
    });
}

- (void)processData:(NSData *)data
{
    NSArray *array = [self clipData:data];

    for(NSData *data in array) {
        NSDictionary *dic = [IXTradeDataFilter unpacketData:data];
        PROTOCOL_HEADER_COMMAND cmd = (PROTOCOL_HEADER_COMMAND)[dic[@"command"] intValue];
        NSData *body = dic[@"prototag"];
        
        if(cmd == CMD_USER_KEEPALIVE){
            DLog(@"<--------------------------------------交易心跳回包>");
        }
        if (cmd != CMD_USER_KEEPALIVE) {
            DLog(@"<----------------Trade response cmd: %@ :%x>",self.cmds[[NSString stringWithFormat:@"%d",cmd]],cmd);
        }
        
        id obj = [IXTradeDataFilter dataConvert2PBObj:body cmd:cmd];
   
        if(!obj){
            ELog([NSString stringWithFormat:@"Trade bodydata convert to pb fiald,cmd : %@",self.cmds[[NSString stringWithFormat:@"%x",cmd]]]);
//             [_clipCT replaceBytesInRange:NSMakeRange(0, _clipCT.length) withBytes:NULL length:0];
            continue;
        }
        [self processSpecialPBObj:obj cmd:cmd];
        
        if([self.delegate respondsToSelector:@selector(IXTradePB:cmd:)]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate IXTradePB:obj cmd:cmd];
            });
        }
    }
}

- (NSMutableArray *)clipData:(NSData *)data
{
    [_clipCT appendData:data];
    [_frameCT removeAllObjects];
    
    while ([_clipCT length]>simple_header_length) {
        NSData *headData = [_clipCT subdataWithRange:NSMakeRange(0, simple_header_length)];
        struct protocol_proto pkt;
        memset(&pkt,0, simple_header_length);
        memcpy(&pkt, [headData bytes], simple_header_length);
        uint16 length = ntohs(pkt.header.length);
        
        if(_clipCT.length >= length) {
            NSRange range = NSMakeRange(0, length);
            NSData *package = [_clipCT subdataWithRange:range];
            [_clipCT replaceBytesInRange:range withBytes:NULL length:0];
            [_frameCT addObject:package];
        }else{
            break;
        }
    }
    return [_frameCT mutableCopy];
}

- (void)processSpecialPBObj:(id)obj cmd:(uint16)cmd
{
    switch (cmd){
        //user
        case CMD_USER_KEEPALIVE:
            [IXTradeDataFilter user_keep_alive:obj];
            break;
        case CMD_USER_LOGIN_INFO:
            [IXTradeDataFilter user_login_info:obj];
            break;
        case CMD_USER_ADD:
            [IXTradeDataFilter user_add:obj];
            break;
        case CMD_USER_KICK_OUT:
            [IXTradeDataFilter user_kick_out:obj];
            break;
        case CMD_USER_LOGIN_DATA_TOTAL:
            [IXTradeDataFilter user_login_data_total:obj];
            break;
        case CMD_USER_LOGIN_DATA:
            [IXTradeDataFilter user_login_data:obj];
            break;
            
        //accuont
        case CMD_ACCOUNT_LIST:
            [IXTradeDataFilter account_list:obj];
            break;
        case CMD_ACCOUNT_UPDATE:
            [IXTradeDataFilter account_update:obj];
            break;
        case CMD_ACCOUNT_ADD:
            [IXTradeDataFilter account_add:obj];
            break;
            
        //account_group
        case CMD_ACCOUNT_GROUP_LIST:
            [IXTradeDataFilter account_group_list:obj];
            break;
        case CMD_ACCOUNT_GROUP_UPDATE:
            [IXTradeDataFilter account_group_update:obj];
            break;
        case CMD_ACCOUNT_GROUP_ADD:
            [IXTradeDataFilter account_group_add:obj];
            break;
        case CMD_ACCOUNT_GROUP_DELETE:
            [IXTradeDataFilter account_group_delete:obj];
            break;
            
        //balance
        case CMD_ACCOUNTBALANCE_GET:
            [IXTradeDataFilter accountBalance_get:obj];
            break;
            
        //account group
        case CMD_ACCOUNTGROUPSYMBOLCATA_LIST:
            [IXTradeDataFilter account_group_symbol_cata_list:obj];
            break;
        case CMD_ACCOUNTGROUPSYMBOLCATA_ADD:
            [IXTradeDataFilter account_group_symbol_cata_add:obj];
            break;
        case CMD_ACCOUNTGROUPSYMBOLCATA_UPDATE:
            [IXTradeDataFilter account_group_symbol_cata_update:obj];
            break;
            
        case CMD_ACCOUNTGROUPSYMBOLCATA_DELETE:
            [IXTradeDataFilter account_group_symbol_cata_delete:obj];
            break;
        
        //symbol
        case CMD_SYMBOL_LIST:
            [IXTradeDataFilter symbol_list:obj];
            break;
        case CMD_SYMBOL_UPDATE:
            [IXTradeDataFilter symbol_update:obj];
            break;
        case CMD_SYMBOL_ADD:
            [IXTradeDataFilter symbol_add:obj];
            break;
        case CMD_SYMBOL_DELETE:
            [IXTradeDataFilter symbol_delete:obj];
            break;
            
        //sym_cata
        case CMD_SYMBOL_CATA_LIST:
            [IXTradeDataFilter symbol_cata_list:obj];
            break;
        case CMD_SYMBOL_CATA_UPDATE:
            [IXTradeDataFilter symbol_cata_update:obj];
            break;
        case CMD_SYMBOL_CATA_ADD:
            [IXTradeDataFilter symbol_cata_add:obj];
            break;
        case CMD_SYMBOL_CATA_DELETE:
            [IXTradeDataFilter symbol_cata_delete:obj];
            break;
            
        //sym_sub
        case CMD_SYMBOL_SUB_LIST:
            [IXTradeDataFilter symbol_sub_list:obj];
            break;
        case CMD_SYMBOL_SUB_ADD:
            [IXTradeDataFilter symbol_sub_add:obj];
            break;
        case CMD_SYMBOL_SUB_DELETE:
            [IXTradeDataFilter symbol_sub_delete:obj];
            break;
            
        case CMD_SYMBOL_SUB_CATA_LIST:
            break;
            
        //sym_hot
        case CMD_SYMBOL_HOT_LIST:
            [IXTradeDataFilter symbol_hot_list:obj];
            break;
        case CMD_SYMBOL_HOT_ADD:
            [IXTradeDataFilter symbol_hot_add:obj];
            break;
        case CMD_SYMBOL_HOT_UPDATE:
            [IXTradeDataFilter symbol_hot_update:obj];
            break;
            
        //sym_margin_set
        case CMD_SYMBOL_MARGIN_SET_LIST:
            [IXTradeDataFilter symbol_margin_set_list:obj];
            break;
        case CMD_SYMBOL_MARGIN_SET_UPDATE:
            [IXTradeDataFilter symbol_margin_set_update:obj];
            break;
        case CMD_SYMBOL_MARGIN_SET_ADD:
            [IXTradeDataFilter symbol_margin_set_add:obj];
            break;
        case CMD_SYMBOL_MARGIN_SET_DELETE:
            [IXTradeDataFilter symbol_margin_set_delete:obj];
            break;
            
        //sym_lable
        case CMD_SYMBOL_LABEL_LIST:
            [IXTradeDataFilter symbol_lable_list:obj];
            break;
        case CMD_SYMBOL_LABEL_UPDATE:
            [IXTradeDataFilter symbol_lable_update:obj];
            break;
        case CMD_SYMBOL_LABEL_ADD:
            [IXTradeDataFilter symbol_lable_add:obj];
            break;
        case CMD_SYMBOL_LABEL_DELETE:
            [IXTradeDataFilter symbol_lable_delete:obj];
            break;
            
        //holiday
        case CMD_HOLIDAY_LIST:
            [IXTradeDataFilter holiday_list:obj];
            break;
        case CMD_HOLIDAY_UPDATE:
            [IXTradeDataFilter holiday_update:obj];
            break;
        case CMD_HOLIDAY_ADD:
            [IXTradeDataFilter holiday_add:obj];
            break;
        case CMD_HOLIDAY_DELETE:
            [IXTradeDataFilter holiday_delete:obj];
            break;
            
        //holiday_cata
        case CMD_HOLIDAY_CATA_LIST:
            [IXTradeDataFilter holiday_cata_list:obj];
            break;
        case CMD_HOLIDAY_CATA_UPDATE:
            [IXTradeDataFilter holiday_cata_update:obj];
            break;
        case CMD_HOLIDAY_CATA_ADD:
            [IXTradeDataFilter holiday_cata_add:obj];
            break;
        case CMD_HOLIDAY_CATA_DELETE:
            [IXTradeDataFilter holiday_cata_delete:obj];
            break;
        
        //holiday_margin
        case CMD_HOLIDAY_MARGIN_LIST:
            [IXTradeDataFilter holiday_margin_list:obj];
            break;
        case CMD_HOLIDAY_MARGIN_UPDATE:
            [IXTradeDataFilter holiday_margin_update:obj];
            break;
        case CMD_HOLIDAY_MARGIN_ADD:
            [IXTradeDataFilter holiday_margin_add:obj];
            break;
        case CMD_HOLIDAY_MARGIN_DELETE:
            [IXTradeDataFilter holiday_margin_delete:obj];
            break;
    
        //company
        case CMD_COMPANY_LIST:
            [IXTradeDataFilter company_list:obj];
            break;
        case CMD_COMPANY_UPDATE:
            [IXTradeDataFilter company_update:obj];
            break;
        case CMD_COMPANY_ADD:
            [IXTradeDataFilter company_add:obj];
            break;
        case CMD_COMPANY_DELETE:
            [IXTradeDataFilter company_delete:obj];
            break;
            
        //schedule
        case CMD_SCHEDULE_LIST:
            [IXTradeDataFilter schedule_list:obj];
            break;
        case CMD_SCHEDULE_UPDATE:
            [IXTradeDataFilter schedule_update:obj];
            break;
        case CMD_SCHEDULE_ADD:
            [IXTradeDataFilter schedule_add:obj];
            break;
        case CMD_SCHEDULE_DELETE:
            [IXTradeDataFilter schedule_delete:obj];
            break;
            
        //schedule_cata
        case CMD_SCHEDULE_CATA_LIST:
            [IXTradeDataFilter schedule_cata_list:obj];
            break;
        case CMD_SCHEDULE_CATA_UPDATE:
            [IXTradeDataFilter schedule_cata_update:obj];
            break;
        case CMD_SCHEDULE_CATA_ADD:
            [IXTradeDataFilter schedule_cata_add:obj];
            break;
        case CMD_SCHEDULE_CATA_DELETE:
            [IXTradeDataFilter schedule_cata_delete:obj];
            break;
            
        //schedule_margin
        case CMD_SCHEDULE_MARGIN_LIST:
            [IXTradeDataFilter schedule_margin_list:obj];
            break;
        case CMD_SCHEDULE_MARGIN_UPDATE:
            [IXTradeDataFilter schedule_margin_update:obj];
            break;
        case CMD_SCHEDULE_MARGIN_ADD:
            [IXTradeDataFilter schedule_margin_add:obj];
            break;
        case CMD_SCHEDULE_MARGIN_DELETE:
            [IXTradeDataFilter schedule_margin_delete:obj];
            break;
            
        //position
        case CMD_POSITION_LIST:
            [IXTradeDataFilter position_list:obj];
            break;
        case CMD_POSITION_ADD:
            [IXTradeDataFilter position_add:obj];
            break;
        case CMD_POSITION_UPDATE:
            [IXTradeDataFilter position_update:obj];
            break;
            
        //order
        case CMD_ORDER_LIST:
            [IXTradeDataFilter order_list:obj];
            break;
        case CMD_ORDER_CANCEL:
            [IXTradeDataFilter order_cancel:obj];
            break;
        case CMD_ORDER_UPDATE:
            [IXTradeDataFilter order_update:obj];
            break;
        case CMD_ORDER_ADD:
            [IXTradeDataFilter order_add:obj];
            break;
        case CMD_ORDER_ADD_BATCH:
            [IXTradeDataFilter order_add_batch:obj];
            break;
            
        //deal
        case CMD_DEAL_LIST:
            [IXTradeDataFilter deal_list:obj];
            break;
        case CMD_DEAL_ADD:
            [IXTradeDataFilter deal_add:obj];
            break;
        case CMD_DEAL_UPDATE:
            [IXTradeDataFilter deal_update:obj];
            break;
            
        //language
        case CMD_LANGUAGE_LIST:
            [IXTradeDataFilter language_list:obj];
            break;
        case CMD_LANGUAGE_UPDATE:
            [IXTradeDataFilter language_update:obj];
            break;
        case CMD_LANGUAGE_ADD:
            [IXTradeDataFilter language_add:obj];
            break;
            
       //quote_delay
        case CMD_QUOTE_DELAY_LIST:
            [IXTradeDataFilter quoteDelay_list:obj];
            break;
        case CMD_QUOTE_DELAY_UPDATE:
            [IXTradeDataFilter quoteDelay_update:obj];
            break;
        case CMD_QUOTE_DELAY_ADD:
            [IXTradeDataFilter quoteDelay_add:obj];
            break;
            
       //eod_time
        case CMD_EOD_TIME_LIST:
            [IXTradeDataFilter eod_time_list:obj];
            break;
        case CMD_EOD_TIME_UPDATE:
            [IXTradeDataFilter eod_time_update:obj];
            break;
        case CMD_EOD_TIME_ADD:
            [IXTradeDataFilter eod_time_add:obj];
            break;
        case CMD_EOD_TIME_DELETE:
            [IXTradeDataFilter eod_time_delete:obj];
            break;
            
        //secure_dev
        case CMD_SECURE_DEV_LIST:
            [IXTradeDataFilter secure_dev_list:obj];
            break;
        case CMD_SECURE_DEV_UPDATE:
            [IXTradeDataFilter secure_dev_update:obj];
            break;
        case CMD_SECURE_DEV_ADD:
            [IXTradeDataFilter secure_dev_add:obj];
            break;
        case CMD_SECURE_DEV_DELETE:
            [IXTradeDataFilter secure_dev_delete:obj];
            break;
            
        //group_sym
        case CMD_GROUP_SYMBOL_LIST:
            [IXTradeDataFilter group_sym_list:obj];
            break;
        case CMD_GROUP_SYMBOL_UPDATE:
            [IXTradeDataFilter group_sym_update:obj];
            break;
        case CMD_GROUP_SYMBOL_ADD:
            [IXTradeDataFilter group_sym_add:obj];
            break;
        case CMD_GROUP_SYMBOL_DELETE:
            [IXTradeDataFilter group_sym_delete:obj];
            break;
            
        //group_sym_cata
        case CMD_GROUP_SYMBOLCATA_LIST:
            [IXTradeDataFilter group_sym_cata_list:obj];
            break;
        case CMD_GROUP_SYMBOLCATA_UPDATE:
            [IXTradeDataFilter group_sym_cata_update:obj];
            break;
        case CMD_GROUP_SYMBOLCATA_ADD:
            [IXTradeDataFilter group_sym_cata_add:obj];
            break;
        case CMD_GROUP_SYMBOLCATA_DELETE:
            [IXTradeDataFilter group_sym_cata_delete:obj];
            break;
            
        //lp_channel_account
        case CMD_LPCHANNEL_ACCOUNT_LIST:
            [IXTradeDataFilter lpchacc_list:obj];
            break;
        case CMD_LPCHANNEL_ACCOUNT_UPDATE:
            [IXTradeDataFilter lpchacc_update:obj];
            break;
        case CMD_LPCHANNEL_ACCOUNT_ADD:
            [IXTradeDataFilter lpchacc_add:obj];
            break;
        case CMD_LPCHANNEL_ACCOUNT_DELETE:
            [IXTradeDataFilter lpchacc_delete:obj];
            break;
            
        //lp_channel_account_symbol
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST:
            [IXTradeDataFilter lpchacc_symbol_list:obj];
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_UPDATE:
            [IXTradeDataFilter lpchacc_symbol_update:obj];
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_ADD:
            [IXTradeDataFilter lpchacc_symbol_add:obj];
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_DELETE:
            [IXTradeDataFilter lpchacc_symbol_delete:obj];
            break;
            
        //lp_channel
        case CMD_LPCHANNEL_LIST:
            [IXTradeDataFilter lpchannel_list:obj];
            break;
        case CMD_LPCHANNEL_UPDATE:
            [IXTradeDataFilter lpchannel_update:obj];
            break;
        case CMD_LPCHANNEL_ADD:
            [IXTradeDataFilter lpchannel_add:obj];
            break;
        case CMD_LPCHANNEL_DELETE:
            [IXTradeDataFilter lpchannel_delete:obj];
            break;
            
        //lp_channel_symbol
        case CMD_LPCHANNEL_SYMBOL_LIST:
            [IXTradeDataFilter lpchannel_symbol_list:obj];
            break;
        case CMD_LPCHANNEL_SYMBOL_UPDATE:
            [IXTradeDataFilter lpchannel_symbol_update:obj];
            break;
        case CMD_LPCHANNEL_SYMBOL_ADD:
            [IXTradeDataFilter lpchannel_symbol_add:obj];
            break;
        case CMD_LPCHANNEL_SYMBOL_DELETE:
            [IXTradeDataFilter lpchannel_symbol_delete:obj];
            break;
            
        //lp_IbBind
        case CMD_LPIB_BIND_LIST:
            [IXTradeDataFilter lpib_bind_list:obj];
            break;
        case CMD_LPIB_BIND_UPDATE:
            [IXTradeDataFilter lpib_bind_update:obj];
            break;
        case CMD_LPIB_BIND_ADD:
            [IXTradeDataFilter lpib_bind_add:obj];
            break;
        case CMD_LPIB_BIND_DELETE:
            [IXTradeDataFilter lpib_bind_delete:obj];
            break;
            
        //kline
        case CMD_KLINE_REPAIR_LIST:
            [IXTradeDataFilter kline_repair_list:obj];
            break;
            
        default:
            break;
    }
}

@end
