//
//  IXQuoteDataFilter.h
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IXQuoteDataFilterDelegate <NSObject>

@required

- (void)IXQuoteData:(NSMutableArray *)arr cmd:(uint16)cmd;

@end

@interface IXQuoteDataFilter : NSObject

@property (nonatomic, assign)id<IXQuoteDataFilterDelegate>delegate;

- (void)filtrate:(NSData *)data;

@end
