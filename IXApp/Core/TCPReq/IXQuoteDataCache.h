//
//  IXQuoteDataCache.h
//  IXApp
//
//  Created by Bob on 2017/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXQuoteDataCache : NSObject

//动态行情
@property (nonatomic, strong) NSMutableDictionary *dynQuoteDic;

//昨日收盘价
@property (nonatomic, strong) NSMutableDictionary *lastClosePriceDic;

+ (IXQuoteDataCache *)shareInstance;

- (void)setDynQuote:(NSMutableArray *)quoteArr;
- (void)setLastClosePrice:(NSMutableArray *)closePriceArr;

- (void)saveDynQuote;
- (void)saveLastClosePrice;


- (void)saveOneDynQuote:(IXQuoteM *)model;
- (void)saveOneLastClosePrice:(IXLastQuoteM *)model;

//切换账户
- (void)clearInstance;

- (IXQuoteM *)tickToQuoteM:(item_tick *)tick;

//根据digit格式化返回的价格
- (NSString *)transforPrice:(NSInteger)price WithSymbolId:(NSInteger)symbolId;

@end


@interface StpSpreadM : NSObject

@property (nonatomic, assign) long stpSpd;

@property (nonatomic, assign) long lpSpd;


@end
