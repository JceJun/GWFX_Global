//
//  IXQuoteDataFilter+DataBase.m
//  IXApp
//
//  Created by Magee on 16/11/29.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteDataFilter+DataBase.h"
#import "IXDBKayLineMgr.h"

@implementation IXQuoteDataFilter (DataBase)

+ (void)kline:(NSMutableArray *)array
{
    if (array.count > 0) {
        
#warning CMD_KAYLINE_MONTH 写死数据
        if ([[IXDBKayLineMgr sharedInstance] saveBatchKayLineInfos:array commondType:CMD_KAYLINE_MONTH]) {
            DLog(@"<Kay Line save: 成功>");
        }else{
            ELog(@"Kay Line save: 失败");
        }
    }
}

@end
