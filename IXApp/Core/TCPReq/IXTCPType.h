//
//  IXTCPType.h
//  IXApp
//
//  Created by Magee on 16/11/14.
//  Copyright © 2016年 IX. All rights reserved.
//

//#ifndef IXTCPType_h
//#define IXTCPType_h

#define IXTCPQuoteReconnectCycle 5u    //行情重连间隔(s)
#define IXTCPQuoteReconnectCount 5u    //行情最大重连次数

#define IXTCPTradeReconnectCycle 5u    //交易重连间隔(s)
#define IXTCPTradeReconnectCount 5u    //交易最大重连次数

#define IXTCPSQuoteReconnectCycle 10u    //備用行情重连间隔(s)
#define IXTCPSTradeReconnectCycle 10u    //備用交易重连间隔(s)



#define IXKeepAliveQuoteSourceTimerCycle  10u   //行情心跳标准间隔(s)
#define IXKeepAliveQuoteSourceTimerLeeway 2u    //行情心跳允许偏差(s)

#define IXKeepAliveTradeSourceTimerCycle  10u   //交易心跳标准间隔(s)
#define IXKeepAliveTradeSourceTimerLeewat 2u    //交易心跳允许偏差(s)

//TCP服务器类型
typedef enum {
    IXTCPServerTypeQuote,     //行情服务器
    IXTCPServerTypeTrade,     //交易服务器
    IXTCPServerTypeSpare      //ping服务器
}IXTCPServerType;

//TCP长连接状态
typedef enum {
    IXTCPConnectStatusDisconnect = 1,   //已经断开
    IXTCPConnectStatusConneting,    //正在连接／重连
    IXTCPConnectStatusConnected,    //已经连接
    IXTCPConnectStatusMaxTried,     //超过最大重连次数，需要手动重连
}IXTCPConnectStatus;

typedef enum {
    IXTCPServerRouteMain,           //主线路
    IXTCPServerRouteSpare           //备用线路
}IXTCPServerRouteType;
