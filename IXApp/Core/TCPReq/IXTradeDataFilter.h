//
//  IXTradeDataFilter.h
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IXTradeDataFilterDelegate <NSObject>

@required

- (void)IXTradePB:(id)pbobj cmd:(PROTOCOL_HEADER_COMMAND)cmd;
 
@end

@interface IXTradeDataFilter : NSObject

@property (nonatomic, assign)id<IXTradeDataFilterDelegate>delegate;

- (void)filtrate:(NSData *)data;

@end
