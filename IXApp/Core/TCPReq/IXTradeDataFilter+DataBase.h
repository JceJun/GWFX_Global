//
//  IXTradeDataFilter+DataBase.h
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeDataFilter.h"

@interface IXTradeDataFilter (DataBase)

//user
+ (void)user_keep_alive:(id)obj;
+ (void)user_login_info:(id)obj;
+ (void)user_add:(id)obj;
+ (void)user_kick_out:(id)obj;
+ (void)user_login_data_total:(id)obj;
+ (void)user_login_data:(id)obj;

//account
+ (void)account_list:(id)obj;
+ (void)account_update:(id)obj;
+ (void)account_add:(id)obj;

//account_group
+ (void)account_group_list:(id)obj;
+ (void)account_group_update:(id)obj;
+ (void)account_group_add:(id)obj;
+ (void)account_group_delete:(id)obj;

//accout_group_sym_cata
+ (void)account_group_symbol_cata_list:(id)obj;
+ (void)account_group_symbol_cata_add:(id)obj;
+ (void)account_group_symbol_cata_update:(id)obj;
+ (void)account_group_symbol_cata_delete:(id)obj;

//balance
+ (void)accountBalance_get:(id)obj;

//symbol
+ (void)symbol_list:(id)obj;
+ (void)symbol_update:(id)obj;
+ (void)symbol_add:(id)obj;
+ (void)symbol_delete:(id)obj;

//sym_cata
+ (void)symbol_cata_list:(id)obj;
+ (void)symbol_cata_update:(id)obj;
+ (void)symbol_cata_add:(id)obj;
+ (void)symbol_cata_delete:(id)obj;

//sym_sub
+ (void)symbol_sub_list:(id)obj;
+ (void)symbol_sub_add:(id)obj;
+ (void)symbol_sub_delete:(id)obj;

//sym_hot
+ (void)symbol_hot_list:(id)obj;
+ (void)symbol_hot_update:(id)obj;
+ (void)symbol_hot_add:(id)obj;

//sym_lable
+ (void)symbol_lable_list:(id)obj;
+ (void)symbol_lable_update:(id)obj;
+ (void)symbol_lable_add:(id)obj;
+ (void)symbol_lable_delete:(id)obj;

//sym_margin_set
+ (void)symbol_margin_set_list:(id)obj;
+ (void)symbol_margin_set_update:(id)obj;
+ (void)symbol_margin_set_add:(id)obj;
+ (void)symbol_margin_set_delete:(id)obj;

//holiday_cata
+ (void)holiday_cata_list:(id)obj;
+ (void)holiday_cata_update:(id)obj;
+ (void)holiday_cata_add:(id)obj;
+ (void)holiday_cata_delete:(id)obj;


//holiday
+ (void)holiday_list:(id)obj;
+ (void)holiday_update:(id)obj;
+ (void)holiday_add:(id)obj;
+ (void)holiday_delete:(id)obj;

//holiday_margin
+ (void)holiday_margin_list:(id)obj;
+ (void)holiday_margin_update:(id)obj;
+ (void)holiday_margin_add:(id)obj;
+ (void)holiday_margin_delete:(id)obj;

//company
+ (void)company_list:(id)obj;
+ (void)company_update:(id)obj;
+ (void)company_add:(id)obj;
+ (void)company_delete:(id)obj;


//schedule
+ (void)schedule_list:(id)obj;
+ (void)schedule_update:(id)obj;
+ (void)schedule_add:(id)obj;
+ (void)schedule_delete:(id)obj;

//schedule_cata
+ (void)schedule_cata_list:(id)obj;
+ (void)schedule_cata_update:(id)obj;
+ (void)schedule_cata_add:(id)obj;
+ (void)schedule_cata_delete:(id)obj;

//shedule_margin
+ (void)schedule_margin_list:(id)obj;
+ (void)schedule_margin_update:(id)obj;
+ (void)schedule_margin_add:(id)obj;
+ (void)schedule_margin_delete:(id)obj;

//position
+ (void)position_list:(id)obj;
+ (void)position_add:(id)obj;
+ (void)position_update:(id)obj;

//order
+ (void)order_list:(id)obj;
+ (void)order_add:(id)obj;
+ (void)order_update:(id)obj;
+ (void)order_cancel:(id)obj;
+ (void)order_add_batch:(id)obj;

//deal
+ (void)deal_list:(id)obj;
+ (void)deal_add:(id)obj;
+ (void)deal_update:(id)obj;

//language
+ (void)language_list:(id)obj;
+ (void)language_add:(id)obj;
+ (void)language_update:(id)obj;

//quote_delay
+ (void)quoteDelay_list:(id)obj;
+ (void)quoteDelay_add:(id)obj;
+ (void)quoteDelay_update:(id)obj;

//eod_time
+ (void)eod_time_list:(id)obj;
+ (void)eod_time_update:(id)obj;
+ (void)eod_time_add:(id)obj;
+ (void)eod_time_delete:(id)obj;

//secure_dev
+ (void)secure_dev_list:(id)obj;
+ (void)secure_dev_update:(id)obj;
+ (void)secure_dev_add:(id)obj;
+ (void)secure_dev_delete:(id)obj;

//group_sym
+ (void)group_sym_list:(id)obj;
+ (void)group_sym_update:(id)obj;
+ (void)group_sym_add:(id)obj;
+ (void)group_sym_delete:(id)obj;

//group_sym_cata
+ (void)group_sym_cata_list:(id)obj;
+ (void)group_sym_cata_update:(id)obj;
+ (void)group_sym_cata_add:(id)obj;
+ (void)group_sym_cata_delete:(id)obj;

//lp_channel_account
+ (void)lpchacc_list:(id)obj;
+ (void)lpchacc_update:(id)obj;
+ (void)lpchacc_add:(id)obj;
+ (void)lpchacc_delete:(id)obj;

//lp_channel_account_symbol
+ (void)lpchacc_symbol_list:(id)obj;
+ (void)lpchacc_symbol_update:(id)obj;
+ (void)lpchacc_symbol_add:(id)obj;
+ (void)lpchacc_symbol_delete:(id)obj;

//lp_channel
+ (void)lpchannel_list:(id)obj;
+ (void)lpchannel_update:(id)obj;
+ (void)lpchannel_add:(id)obj;
+ (void)lpchannel_delete:(id)obj;

//lp_channel_symbol
+ (void)lpchannel_symbol_list:(id)obj;
+ (void)lpchannel_symbol_update:(id)obj;
+ (void)lpchannel_symbol_add:(id)obj;
+ (void)lpchannel_symbol_delete:(id)obj;

//lp_IbBind
+ (void)lpib_bind_list:(id)obj;
+ (void)lpib_bind_update:(id)obj;
+ (void)lpib_bind_add:(id)obj;
+ (void)lpib_bind_delete:(id)obj;

//kline
+ (void)kline_repair_list:(id)obj;

@end
