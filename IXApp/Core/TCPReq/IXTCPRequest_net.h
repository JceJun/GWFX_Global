//
//  IXTCPRequest_net.h
//  IXApp
//
//  Created by Magee on 2016/12/17.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTCPRequest.h"

#define IXNetStatus_listen_regist(target,keyPath)       \
[[IXTCPRequest shareInstance] addObserver:target    \
forKeyPath:keyPath options:NSKeyValueObservingOptionNew \
context:NULL]

#define IXNetStatus_listen_resign(target,keyPath)       \
[[IXTCPRequest shareInstance] removeObserver:target \
forKeyPath:keyPath]

#define IXNetStatus_isSameKey(key1,key2)                \
[key1 isEqualToString:key2]

#define IXTradeStatusKey @"tradeStatus"
#define IXQuoteStatusKey @"quoteStatus"

@interface IXTCPRequest ()

@property (atomic,assign)IXTCPConnectStatus quoteStatus;
@property (atomic,assign)IXTCPConnectStatus tradeStatus;

@end
