//
//  IXQuoteDataFilter+process.m
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteDataFilter+process.h"
#import "IXCrc16.h"
#import "IXKeepAliveCache.h"
#import "IXQuoteDataCache.h"
#import "IxProtoQuote.pbobjc.h"
#import "IxItemTick.pbobjc.h"

@implementation IXQuoteDataFilter (process)

+ (NSData *)loadLastCloseWithSymbolList:(NSArray *)symbolArr
{
    if (!symbolArr.count) {
        return nil;
    }
    
    //head
    proto_header head;
    memset(&head, 0, HEADER_LENGTH);
    head.cmd = htons(CMD_QUOTE_CLOSE_PRICE);
    head.seq = htonl([[IXKeepAliveCache shareInstance] getSeqNo]);
    NSData *head_data = [NSData dataWithBytes:&head length:HEADER_LENGTH];

    //body
    proto_quote_close_price_req *req = [[proto_quote_close_price_req alloc] init];
    NSInteger count = symbolArr.count;
    for (int i = 0; i < count; i++) {
        item_stk *stk = [[item_stk alloc] init];
        stk.marketid = [[(NSDictionary *)symbolArr[i] objectForKey:@"marketId"] integerValue];
        stk.id_p     = [[(NSDictionary *)symbolArr[i] objectForKey:@"id"] intValue];
        [req.stkArray addObject:stk];
    }
    NSData *body_data = [req data];
    
    //merge
    uint16_t lenth = head_data.length + body_data.length;
    
    char *tmp = (char *)malloc(lenth);
    memset(tmp, 0, lenth);
    [head_data getBytes:tmp range:NSMakeRange(0, head_data.length)];
    [body_data getBytes:tmp + head_data.length range:NSMakeRange(0, body_data.length)];
    
    uint16_t lenth_tmp = htons(lenth);
    memcpy(tmp, &lenth_tmp, 2);
    
    uint16_t crc   = [IXCrc16 crc16:tmp length:lenth];
    crc = htons(crc);
    memcpy(tmp + 4, &crc, 2);
    
    NSData *rst_data = [NSData dataWithBytes:tmp length:lenth];
    free(tmp);
    return rst_data;
}

+ (NSData *)loadQuotePriceWithSymbolList:(NSArray *)symbolArr WithCmdType:(PROTO_QUOTE_COMMAND)qCom
{
    if (!symbolArr.count) {
        return nil;
    }
    
    //head
    proto_header head;
    memset(&head, 0, HEADER_LENGTH);
    head.cmd = htons(qCom);
    head.seq = htonl([[IXKeepAliveCache shareInstance] getSeqNo]);
    NSData *head_data = [NSData dataWithBytes:&head length:HEADER_LENGTH];
    
    //body
    proto_quote_sub_req *req = [[proto_quote_sub_req alloc] init];
    NSInteger count = symbolArr.count;
    for (int i = 0; i < count; i++) {
        item_stk *stk = [[item_stk alloc] init];
        stk.marketid = [[(NSDictionary *)symbolArr[i] objectForKey:@"marketId"] integerValue];
        stk.id_p     = [[(NSDictionary *)symbolArr[i] objectForKey:@"id"] intValue];
        [req.stkArray addObject:stk];
    }
    NSData *body_data = [req data];
    

    //merge
    uint16_t lenth = head_data.length + body_data.length;
    
    char *tmp = (char *)malloc(lenth);
    memset(tmp, 0, lenth);
    [head_data getBytes:tmp range:NSMakeRange(0, head_data.length)];
    [body_data getBytes:tmp + head_data.length range:NSMakeRange(0, body_data.length)];
    
    uint16_t lenth_tmp = htons(lenth);
    memcpy(tmp, &lenth_tmp, 2);
    
    uint16_t crc   = [IXCrc16 crc16:tmp length:lenth];
    crc = htons(crc);
    memcpy(tmp + 4, &crc, 2);
    
    NSData *rst_data = [NSData dataWithBytes:tmp length:lenth];
    free(tmp);
    return rst_data;
}

+ (NSData *)loadKDataWithParam:(proto_quote_kdata_req *)param
{
    //head
    proto_header head;
    memset(&head, 0, HEADER_LENGTH);
    head.cmd = htons(CMD_QUOTE_KDATA);
    head.seq = htonl([[IXKeepAliveCache shareInstance] getSeqNo]);
    NSData *head_data = [NSData dataWithBytes:&head length:HEADER_LENGTH];
    
    //body
    NSData *body_data = [param data];
    
    //merge
    uint16_t lenth = head_data.length + body_data.length;
    
    char *tmp = (char *)malloc(lenth);
    memset(tmp, 0, lenth);
    [head_data getBytes:tmp range:NSMakeRange(0, head_data.length)];
    [body_data getBytes:tmp + head_data.length range:NSMakeRange(0, body_data.length)];
    
    uint16_t lenth_tmp = htons(lenth);
    memcpy(tmp, &lenth_tmp, 2);
    
    uint16_t crc   = [IXCrc16 crc16:tmp length:lenth];
    crc = htons(crc);
    memcpy(tmp + 4, &crc, 2);
    
    NSData *rst_data = [NSData dataWithBytes:tmp length:lenth];
    free(tmp);
    return rst_data;
}

+ (NSMutableArray *)proto_quote_close_price_rsp:(id)obj
{
    proto_quote_close_price_rsp *pb = (proto_quote_close_price_rsp *)obj;
    if (!pb) {
        ELog(@"proto_quote_close_price_rsp 反序列化失败");
    } else {
        int count = pb.closePriceArray.count;
        if (count) {
            NSMutableArray *result = [NSMutableArray array];
            for (int i = 0; i < count; i++) {
                item_quote_close_price *item = pb.closePriceArray[i];
                
                IXLastQuoteM *dataModel = [[IXLastQuoteM alloc] init];
                dataModel.symbolId = item.id_p;
                dataModel.nLastClosePrice = [[[IXQuoteDataCache shareInstance] transforPrice:item.price
                                                                                WithSymbolId:item.id_p] doubleValue];
                [result addObject:dataModel];
            }
            return result;
        } else {
            WLog(@"proto_quote_close_price_rsp 列表为空");
        }
    }
    return nil;
}

+ (NSMutableArray *)proto_quote_pub:(id)obj
{
    proto_quote_pub *pb = (proto_quote_pub *)obj;
    if (!pb) {
        ELog(@"proto_quote_pub 反序列化失败");
    } else {
        if (pb.tick) {
            IXQuoteDataCache *cache = [IXQuoteDataCache shareInstance];
            NSMutableArray *result = [NSMutableArray array];
            [result addObject:[cache tickToQuoteM:pb.tick]];
            return result;
        } else {
            ELog(@"proto_quote_pub tick is NULL!");
        }
    }
    return nil;
}

+ (NSMutableArray *)proto_quote_sub_rsp:(id)obj
{
    proto_quote_sub_rsp *pb = (proto_quote_sub_rsp *)obj;
    if (!pb) {
        ELog(@"proto_quote_sub_rsp 反序列化失败");
    } else {
        __block NSMutableArray *result = [@[] mutableCopy];
        [pb.tickArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            item_tick *tick = (item_tick *)obj;
            IXQuoteM *qModel = [[IXQuoteDataCache shareInstance] tickToQuoteM:tick];
            [result addObject:qModel];

        }];
        return result;
    }
    return nil;
}

+ (NSMutableArray *)proto_quote_kdata_rsp:(id)obj
{
    proto_quote_kdata_rsp *pb = (proto_quote_kdata_rsp *)obj;
    
    if (!pb) {
        ELog(@"proto_quote_kdata_rsp 反序列化失败");
    } else {
        int count = pb.kdataArray.count;
        NSMutableArray *result = [NSMutableArray array];
        if (count) {
            for (int i = 0; i < count; i++) {
                item_tick_kline *item = pb.kdataArray[i];
                
                NSDictionary *dic = @{
                                      @"offset" :@(pb.offset),
                                      @"count"  :@(pb.count),
                                      @"total"  :@(pb.total),
                                      
                                      @"seq"    :@(item.seq),
                                      @"id_p"   :@(item.id_p),
                                      @"type"   :@(item.type),
                                      
                                      @"nOpen"  :@(item.priceOpen),
                                      @"nHigh"  :@(item.priceHigh),
                                      @"nLow"   :@(item.priceLow),
                                      @"nClose" :@(item.priceClose),
                                      @"nVolume":@(item.volume),
                                      @"nAmount":@(item.amount),
                                      @"nTime"  :@(item.time),
                                      @"digits" :@(item.digits)
                                      };
                [result addObject:dic];
            }
        } else {
            WLog(@"proto_quote_kdata_rsp 列表为空");
        }
        return result;
    }
    return [@[] mutableCopy];
}

+ (void)proto_quote_login_rsp:(id)obj
{
    proto_quote_login_rsp *pb = (proto_quote_login_rsp *)obj;
    if (!pb) {
        ELog(@"proto_quote_login_rsp 反序列化失败");
    } else {
        if (!pb.result) {
            ELog(@"行情服务器登陆失败!!!");
        }
    }
}

#pragma mark -
#pragma makr ---------------------------------------------------

+ (NSDictionary *)unpacketData:(NSData *)data
{
    if (![data length]) return nil;
    
    NSData *classDesInfo = [data subdataWithRange:NSMakeRange(0, SIMPLE_HEADER_LENGTH)];
    
    proto_simple_header pkt;
    memset(&pkt,0, SIMPLE_HEADER_LENGTH);
    memcpy(&pkt, [classDesInfo bytes], SIMPLE_HEADER_LENGTH);
    uint16 command = ntohs(pkt.cmd);
    
    NSData *bodyData = nil;
    
    switch (command) {
        case CMD_QUOTE_PUB:
        case CMD_QUOTE_PUB_DETAIL:
            bodyData = [data subdataWithRange:NSMakeRange(SIMPLE_HEADER_LENGTH, [data length] - SIMPLE_HEADER_LENGTH)];
            break;
        case CMD_QUOTE_HB:
        case CMD_QUOTE_LOGIN:
        case CMD_QUOTE_SUB:
        case CMD_QUOTE_UNSUB:
        case CMD_QUOTE_SUB_DETAIL:
        case CMD_QUOTE_UNSUB_DETAIL:
        case CMD_QUOTE_CLOSE_PRICE:
        case CMD_QUOTE_KDATA:
            bodyData = [data subdataWithRange:NSMakeRange(HEADER_LENGTH, [data length] - HEADER_LENGTH)];
            break;
        default:
            break;
    }
    
    if (bodyData&&bodyData.length) {
        NSDictionary *dic = @{@"prototag":bodyData,
                              @"command":[NSNumber numberWithLong:command]};
        return dic;
    }
    
    return nil;
}

+ (id)dataConvert2PBObj:(NSData *)bodyData cmd:(uint16)cmd
{
    id obj = nil;
    switch (cmd) {
        case CMD_QUOTE_PUB_DETAIL:
            obj = [proto_quote_pub parseFromData:bodyData error:nil];
            break;
        case CMD_QUOTE_LOGIN:
            obj = [proto_quote_login_rsp parseFromData:bodyData error:nil];
            break;
        case CMD_QUOTE_PUB:
        case CMD_QUOTE_SUB:
        case CMD_QUOTE_SUB_DETAIL:
            obj = [proto_quote_sub_rsp parseFromData:bodyData error:nil];
            break;
        case CMD_QUOTE_UNSUB:
        case CMD_QUOTE_UNSUB_DETAIL:
            obj = [proto_quote_unsub_rsp parseFromData:bodyData error:nil];
            break;
        case CMD_QUOTE_CLOSE_PRICE:
            obj = [proto_quote_close_price_rsp parseFromData:bodyData error:nil];
            break;
        case CMD_QUOTE_KDATA:
            obj = [proto_quote_kdata_rsp parseFromData:bodyData error:nil];
            break;
        default:
            ELog([NSString stringWithFormat:@"Quote Sever Presents Unexpected Cmd : %x",cmd]);
            break;
    }
    return obj;
}

@end
