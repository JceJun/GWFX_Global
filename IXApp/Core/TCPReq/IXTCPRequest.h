//
//  IXTCPRequest.h
//  IXApp
//
//  Created by Magee on 16/11/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IXTCPType.h"
#import "IxProtoUser.pbobjc.h"
#import "IxProtoOrder.pbobjc.h"
#import "IxProtoAccount.pbobjc.h"
#import "IxProtoSymbolSub.pbobjc.h"
#import "IxProtoAccountBalance.pbobjc.h"
#import "IxProtoPosition.pbobjc.h"
#import "IxProtoQuote.pbobjc.h"

@protocol IXTCPRequestDelegate <NSObject>

@optional

/**
 *  行情服务器接收数据回调
 *
 *  @prama arr  回调数据
 *  @prama cmd  信令类型
 */
- (void)IXTCPQuoteSuccessData:(NSMutableArray *)arr cmd:(uint16)cmd;

@end

@interface IXTCPRequest : NSObject

@property (nonatomic,assign)id<IXTCPRequestDelegate>delegate;

@property (nonatomic,copy,readonly) NSString *routeType;

+ (IXTCPRequest *)shareInstance;

//断开连接
- (void)disConnect;

//重连
- (void)reconnect;
 

//更新用户基础表信息
- (void)userDataVersion:(proto_user_login_data *)param;

//登陆
- (void)loginWithParam:(proto_user_login *)param;

//退出
- (void)logoutWithParam:(proto_user_logout *)param;

//注册
- (void)registWithParam:(proto_user_add *)param;

//修改用户信息
- (void)modifyUserInfoWithParam:(proto_user_update*)param;

//切换用户账号
- (void)swiAccountWithParam:(proto_user_login_account *)param;

//删除用户
- (void)deleteUserWithParam:(proto_user_delete *)param;

//用户登出
- (void)logOutWithParam:(proto_user_logout *)praram;

//获取用户
- (void)getUserInfoWithParam:(proto_user_get *)param;

//获取用户列表
- (void)getUserListWithParam:(proto_user_list *)param;

//开户
- (void)openAccountWithParam:(proto_account_add *)param;

//账户查询
- (void)accountQueryWithParam:(proto_account_balance_get *)param;

//下单
- (void)orderWithParam:(proto_order_add *)param;

//批量下单
- (void)batchOrderWithParam:(proto_order_add_batch *)param;

//更新订单
- (void)updateOrderWithParam:(proto_order_update *)param;

//订单取消
- (void)orderCancelWithParam:(proto_order_cancel *)param;

//更新仓位
- (void)updatePositionWithParam:(proto_position_update *)param;

- (void)forceCloseWithParam:(proto_position_close *)param;

//交易明细
- (void)getOrderInfoWithParam:(proto_order_get *)param;

//持仓历史
- (void)getPositionHistoryWithParam:(proto_position_history_list *)param;

//账户出入金
- (void)accountBalanceWithParam:(proto_account_balance_add *)param;

//添加自选
- (void)addSubSymbolWithParam:(proto_symbol_sub_add *)param;

//删除自选
- (void)delSubSymbolWithParam:(proto_symbol_sub_delete *)param;
 

#pragma mark 行情
- (void)subscribeDynPriceWithSymbolAry:(NSArray *)symbolArr;

- (void)unSubQuotePriceWithSymbolAry:(NSArray *)symbolArr;

- (void)subscribeDetailPriceWithSymbolAry:(NSArray *)symbolArr;

- (void)unSubscribeDetailPriceWithSymbolAry:(NSArray *)symbolArr;


- (void)getLastClostPriceWithSymbolAry:(NSArray *)symbolArr;

#pragma mark chart

- (void)subscribeChartDataWithParam:(proto_quote_kdata_req *)param;

@end
