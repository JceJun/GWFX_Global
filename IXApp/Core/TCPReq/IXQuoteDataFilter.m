//
//  IXQuoteDataFilter.m
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.

#import "IXQuoteDataFilter.h"
#import "IXQuoteDataFilter+process.h"
#import "IXQuoteDataFilter+DataBase.h"
#import "IXQuoteDataCache.h"
#import "IXCrc16.h"
#import "IxProtoQuote.pbobjc.h"

@interface IXQuoteDataFilter ()
{
    dispatch_queue_t _qQueue;
    NSMutableData    *_clipCT;
    NSMutableArray   *_frameCT;
}

@end

@implementation IXQuoteDataFilter

- (id)init
{
    self = [super init];
    if (self) {
        _qQueue = dispatch_queue_create("QuoteDataFilter_Serial", DISPATCH_QUEUE_SERIAL);
        _clipCT = [[NSMutableData alloc] init];
        _frameCT = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)filtrate:(NSData *)data
{
    if (![data length]) {
        ELog(@"Quote data is NULL!");
        return;
    }
    
    dispatch_async(_qQueue, ^{
        [self processData:data];
    });
}

- (void)processData:(NSData *)data
{
    NSArray *array = [self clipData:data];
    
    for(NSData *data in array) {
        NSDictionary *dic = [IXQuoteDataFilter unpacketData:data];
        
        PROTO_QUOTE_COMMAND cmd = (PROTO_QUOTE_COMMAND)[dic[@"command"] intValue];
        NSData *body = dic[@"prototag"];
        
        if (cmd == CMD_QUOTE_INITIAL ||
            cmd == CMD_QUOTE_HB) {
            if(cmd == CMD_QUOTE_HB) {
                DLog(@"<--------------------------------------行情心跳回包>");
            }
            continue;
        }
        
        id obj = [IXQuoteDataFilter dataConvert2PBObj:body cmd:cmd];
        if(!obj){
            ELog([NSString stringWithFormat:@"Quote bodydata convert to pb fiald,cmd : [%x]",cmd]);
//            [_clipCT replaceBytesInRange:NSMakeRange(0, _clipCT.length) withBytes:NULL length:0];
            break;
        }
        [self processFrame:cmd obj:obj];
    }
}

- (NSMutableArray *)clipData:(NSData *)data
{
    [_clipCT appendData:data];
    [_frameCT removeAllObjects];
    
    while ([_clipCT length] > SIMPLE_HEADER_LENGTH) {
        NSData *simpleHead = [_clipCT subdataWithRange:NSMakeRange(0, SIMPLE_HEADER_LENGTH)];
        proto_simple_header pkt;
        memset(&pkt,0, SIMPLE_HEADER_LENGTH);
        memcpy(&pkt, [simpleHead bytes], SIMPLE_HEADER_LENGTH);
        uint16 length = ntohs(pkt.length);
        if (_clipCT.length >= length) {
            NSRange range = NSMakeRange(0, length);
            NSMutableData *package = [NSMutableData dataWithData:[_clipCT subdataWithRange:range]];
            [_clipCT replaceBytesInRange:range withBytes:NULL length:0];
            [_frameCT addObject:package];
        } else {
            break;
        }
    }
    return _frameCT;
}

- (void)processFrame:(PROTO_QUOTE_COMMAND)cmd obj:(id)obj
{
    switch (cmd){
        //登陆
        case CMD_QUOTE_LOGIN:
            [IXQuoteDataFilter proto_quote_login_rsp:obj];
            break;
            
        //行情连续数据快照（1档价格深度）只发一次
        case CMD_QUOTE_SUB: {
            DLog(@"成功订阅1级深度价格动态行情");

            NSMutableArray *arr = [IXQuoteDataFilter proto_quote_sub_rsp:obj];
            [[IXQuoteDataCache shareInstance] setDynQuote:arr];
            
            if([self.delegate respondsToSelector:@selector(IXQuoteData:cmd:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate IXQuoteData:arr cmd:CMD_QUOTE_SUB];
                });
            }
        }
            break;
            
        //行情连续数据（1档价格深度),是在CMD_QUOTE_SUB之后自动推送的连续数据
        case CMD_QUOTE_PUB: {
            NSMutableArray *arr = [IXQuoteDataFilter proto_quote_sub_rsp:obj];
            [[IXQuoteDataCache shareInstance] setDynQuote:arr];
            
            //执行的代码，其中可能有异常。一旦发现异常，则立即跳到catch执行。否则不会执行catch里面的内容
            if([self.delegate respondsToSelector:@selector(IXQuoteData:cmd:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate IXQuoteData:arr cmd:CMD_QUOTE_PUB];
                });
            }  
        }
            break;
            
        //取消行情连续数据，取消之后CMD_QUOTE_PUB随之也不再推送
        case CMD_QUOTE_UNSUB: {
            DLog(@"成功取消1级深度价格动态行情");
            if([self.delegate respondsToSelector:@selector(IXQuoteData:cmd:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate IXQuoteData:[NSMutableArray array] cmd:CMD_QUOTE_UNSUB];
                });
            }
        }
            break;
            
        //产品详情内部行情连续数据快照（10档价格深度）只发一次
        case CMD_QUOTE_SUB_DETAIL: {
            NSMutableArray *arr = [IXQuoteDataFilter proto_quote_sub_rsp:obj];
            [[IXQuoteDataCache shareInstance] setDynQuote:arr];
            if([self.delegate respondsToSelector:@selector(IXQuoteData:cmd:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate IXQuoteData:arr cmd:CMD_QUOTE_SUB_DETAIL];
                });
            }
        }
            break;
            
        //产品详情内部行情连续数据（10档价格深度）是在CMD_QUOTE_SUB_DETAIL之后自动推送的连续数据
        case CMD_QUOTE_PUB_DETAIL: {
            NSMutableArray *arr = [IXQuoteDataFilter  proto_quote_pub:obj]; // proto_quote_sub_rsp:obj];
            [[IXQuoteDataCache shareInstance] setDynQuote:arr];
            
            if([self.delegate respondsToSelector:@selector(IXQuoteData:cmd:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate IXQuoteData:arr cmd:CMD_QUOTE_PUB_DETAIL];
                });
            }
        }
            break;
        
        //取消产品详情内部行情连续数据，取消之后CMD_QUOTE_PUB_DETAIL随之也不再推送
        case CMD_QUOTE_UNSUB_DETAIL:
            DLog(@"成功取消5级深度价格动态行情");
            break;
       
        //昨收价
        case CMD_QUOTE_CLOSE_PRICE: {
            NSMutableArray *arr = [IXQuoteDataFilter proto_quote_close_price_rsp:obj];
            if (arr.count) {
                [[IXQuoteDataCache shareInstance] setLastClosePrice:arr];
                if([self.delegate respondsToSelector:@selector(IXQuoteData:cmd:)]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate IXQuoteData:arr cmd:CMD_QUOTE_CLOSE_PRICE];
                    });
                }
            }
        }
            break;
            
        //k线／分时
        case CMD_QUOTE_KDATA: {
            NSMutableArray *arr = [IXQuoteDataFilter proto_quote_kdata_rsp:obj];
            if([self.delegate respondsToSelector:@selector(IXQuoteData:cmd:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate IXQuoteData:arr cmd:CMD_QUOTE_KDATA];
                });
            }
        }
            break;
        default:
            break;
    }
}

@end
