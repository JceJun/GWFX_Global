//
//  IXQuoteDataCache.m
//  IXApp
//
//  Created by Bob on 2017/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXQuoteDataCache.h"
#import "IXQuoteM.h"
#import "IXLastQuoteM.h"
#import "IXDBGlobal.h"
#import "IXDBAccountGroupMgr.h"
#import "IXDBAccountGroupSymCataMgr.h"
#import "IXDBG_SMgr.h"
#import "IXDBLpibBindMgr.h"
#import "IXDBLpchaccMgr.h"
#import "IXDBLpchaccSymbolMgr.h"
#import "IXDBLpchannelSymbolMgr.h"
#import "IXAccountGroupModel.h"

@implementation StpSpreadM
@end


@interface IXQuoteDataCache ()
{
    dispatch_queue_t dealCacheQueue;
}

@property (nonatomic, strong) NSMutableDictionary *accGroupDic;  //账户组
@property (nonatomic, strong) NSMutableDictionary *groupSymCata;

@property (nonatomic, strong) NSMutableDictionary *cacheDigit;

@property (nonatomic, strong) NSMutableDictionary *spdDic;

@end

@implementation IXQuoteDataCache

+ (IXQuoteDataCache *)shareInstance{
    static IXQuoteDataCache *share;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(!share){
            share = [[IXQuoteDataCache alloc] init];
        }
    });
    return share;
}

- (id)init
{
    self = [super init];
    if (self) {
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_CHANGE);

        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_ADD);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_DELETE);

        IXTradeData_listen_regist(self, PB_CMD_LPCHACC_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_LPCHACC_ADD);
        IXTradeData_listen_regist(self, PB_CMD_LPCHACC_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_LPCHACC_SYMBOL_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_LPCHACC_SYMBOL_ADD);
        IXTradeData_listen_regist(self, PB_CMD_LPCHACC_SYMBOL_DELETE);
      
        IXTradeData_listen_regist(self, PB_CMD_LPCHANNEL_SYMBOL_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_LPCHANNEL_SYMBOL_ADD);
        IXTradeData_listen_regist(self, PB_CMD_LPCHANNEL_SYMBOL_DELETE);
       
        IXTradeData_listen_regist(self, PB_CMD_LPIB_BIND_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_LPIB_BIND_ADD);
        IXTradeData_listen_regist(self, PB_CMD_LPIB_BIND_DELETE);
        
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_ADD);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_UPDATE);
        IXTradeData_listen_regist(self, PB_CMD_GROUP_SYMBOL_DELETE);

        
         dealCacheQueue = dispatch_queue_create("QuoteDataCache_Serial", DISPATCH_QUEUE_SERIAL);
        
        _dynQuoteDic = [@{} mutableCopy];
        _lastClosePriceDic = [@{} mutableCopy];
        
        _cacheDigit = [@{} mutableCopy];
        _accGroupDic = [@{} mutableCopy];
        _groupSymCata = [@{} mutableCopy];

        _spdDic = [@{} mutableCopy];
        
    }
    return self;
}

- (void)dealloc
{
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_CHANGE);
 
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_ADD);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_LPCHACC_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_LPCHACC_ADD);
    IXTradeData_listen_resign(self, PB_CMD_LPCHACC_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_LPCHACC_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_LPCHACC_SYMBOL_ADD);
    IXTradeData_listen_resign(self, PB_CMD_LPCHACC_SYMBOL_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_LPCHANNEL_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_LPCHANNEL_SYMBOL_ADD);
    IXTradeData_listen_resign(self, PB_CMD_LPCHANNEL_SYMBOL_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_LPIB_BIND_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_LPIB_BIND_ADD);
    IXTradeData_listen_resign(self, PB_CMD_LPIB_BIND_DELETE);
    
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_ADD);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_UPDATE);
    IXTradeData_listen_resign(self, PB_CMD_GROUP_SYMBOL_DELETE);
}

- (void)clearInstance
{
    dispatch_async(dealCacheQueue, ^{
        [_accGroupDic removeAllObjects];
        [_groupSymCata removeAllObjects];
        [_spdDic removeAllObjects];
        
        [_cacheDigit removeAllObjects];
        [_dynQuoteDic removeAllObjects];
        [_lastClosePriceDic removeAllObjects];
    });
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if(IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_CHANGE)){
        dispatch_async(dealCacheQueue, ^{
            [_accGroupDic removeAllObjects];
        });
    }else if (IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_ADD)||
              IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_UPDATE)||
              IXTradeData_isSameKey( keyPath, PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_DELETE)){
        dispatch_async(dealCacheQueue, ^{
            [_groupSymCata removeAllObjects];
        });
    }else{
        dispatch_async(dealCacheQueue, ^{
            [_spdDic removeAllObjects];
            [_groupSymCata removeAllObjects];
        });
    }
}

#pragma mark --
#pragma mark 行情缓存

/**
 缓存动态行情

 @param quoteArr 行情数组
 */
- (void)setDynQuote:(NSMutableArray *)quoteArr
{
    dispatch_async(dealCacheQueue, ^{
        for ( IXQuoteM *model in quoteArr ) {
            [_dynQuoteDic setValue:model forKey:[NSString stringWithFormat:@"%d",model.symbolId]];
        }
    });
}


/**
缓存昨收价

 @param closePriceArr 昨收价数组
 */
- (void)setLastClosePrice:(NSMutableArray *)closePriceArr
{
    dispatch_async(dealCacheQueue, ^{
        for ( IXLastQuoteM *model in closePriceArr ) {
            [_lastClosePriceDic setValue:model forKey:[NSString stringWithFormat:@"%ld",(long)model.symbolId]];
        }
    });
}

- (void)saveOneDynQuote:(IXQuoteM *)model
{
    dispatch_async(dealCacheQueue, ^{
        [IXDataProcessTools saveBatchQuotePrice:@[model]];
    });
}

- (void)saveOneLastClosePrice:(IXLastQuoteM *)model
{
    dispatch_async(dealCacheQueue, ^{
        [IXDataProcessTools saveBatchYesterdayPrice:@[model]];
    });
}

/**
 动态行情写入数据库
 */
- (void)saveDynQuote
{
    dispatch_async(dealCacheQueue, ^{
        if ([IXDataProcessTools saveBatchQuotePrice:[_dynQuoteDic allValues]]) {
            [_dynQuoteDic removeAllObjects];
            [_groupSymCata removeAllObjects];
        }
    });
}


/**
 昨收价写入数据库
 */
- (void)saveLastClosePrice
{
    dispatch_async(dealCacheQueue, ^{
        if ([IXDataProcessTools saveBatchYesterdayPrice:[_lastClosePriceDic allValues]]) {
            [_lastClosePriceDic removeAllObjects];
        }
    });
} 


/**
 tick转行情

 @param tick tick
 @return 行情
 */ 
- (IXQuoteM *)tickToQuoteM:(item_tick *)tick
{
    StpSpreadM *model = [self getStpLpSpdWithSymId:tick.id_p];
    tick = [self spdTick:tick Spread:model.stpSpd];
    tick = [self spdTick:tick Spread:model.lpSpd];
    
    {
        //acg点差
        long acgSpd = [self spdWithSymId:tick.id_p];
        tick = [self spdTick:tick Spread:acgSpd];
    }
    
    IXQuoteM *qModel = [[IXQuoteM alloc] initWithTick:tick];
    qModel.nPrice =  [[self transforPrice:tick.price WithSymbolId:tick.id_p] doubleValue];
    qModel.nOpen = [[self transforPrice:tick.open WithSymbolId:tick.id_p] doubleValue];
    
    [tick.tickDeepArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        item_tick_deep *deep = (item_tick_deep *)obj;
        [qModel.BuyPrc addObject:[self transforPrice:deep.priceAsk WithSymbolId:tick.id_p]];
        [qModel.BuyVol addObject:[NSString stringWithFormat:@"%.lf",deep.volumeAsk/10000.0]];
        
        [qModel.SellPrc addObject:[self transforPrice:deep.priceBid WithSymbolId:tick.id_p]];
        [qModel.SellVol addObject:[NSString stringWithFormat:@"%.lf",deep.volumeBid/10000.0]];
        
        if (idx >= 5) {
            *stop = YES;
        }
    }];
    return qModel;
}


/**
 tick偏移

 @param tick 行情
 @param spread 点差
 @return 偏移后的tick
 */
- (item_tick *)spdTick:(item_tick *)tick Spread:(long)spread{
    
    __block long afAskPrc = 0;
    __block long afBidPrc = 0;
    
    __block long askSpd = 0;
    __block long bidSpd = 0;
    __block BOOL refreshSpd = NO;
    NSMutableArray *tickDeepArray = [@[] mutableCopy];
    [tick.tickDeepArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        item_tick_deep *deep = (item_tick_deep *)obj;
        
        if (refreshSpd) {
            afAskPrc = deep.priceAsk + askSpd;
            afBidPrc = deep.priceBid - bidSpd;
        }else{
            if (0 == spread%2) {
                afAskPrc = deep.priceAsk + spread/2;
                afBidPrc = deep.priceBid - spread/2;
            }else{
                afAskPrc = deep.priceAsk + (spread + 1)/2;
                afBidPrc = deep.priceBid - (spread - 1)/2;
            }
        }
        
        //点差偏移后，如果买入价小于卖出价，则做偏移校准
        if (afAskPrc < afBidPrc) {
            long midPrc = (afAskPrc + afBidPrc)/2;
            afAskPrc = midPrc;
            afBidPrc = midPrc;
            
            askSpd = deep.priceAsk - midPrc;
            bidSpd = midPrc - deep.priceBid;
            
            refreshSpd = YES;
        }
        
        deep.priceAsk = afAskPrc;
        deep.priceBid = afBidPrc;
        [tickDeepArray addObject:deep];
    }];
    
    tick.tickDeepArray = tickDeepArray;
    return tick;
}

#pragma mark --
#pragma mark 查询点差
- (StpSpreadM *)getStpLpSpdWithSymId:(NSInteger)symId
{
    StpSpreadM *model = [[StpSpreadM alloc] init];
    
    NSString *key = [NSString stringWithFormat:@"%ld",(long)symId];
    id obj = _spdDic[key];
    if (obj) {
        model = obj;
    }else{
        uint64_t agId = [IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid;
        
        NSDictionary *dic = [IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:agId];
        double lpUId = [dic[kLpuserid] doubleValue];
        if (!lpUId) {
            NSDictionary *dic = [IXDBG_SMgr queryG_SByAccountGroupId:agId symbolId:symId];
            lpUId = [dic[kLpuserid] doubleValue];
        }
        
        if (lpUId) {
            NSDictionary *dic = [IXDBLpibBindMgr queryLpIbBindByLpUserId:lpUId];
            
            NSInteger comId = [dic int64ForKey:kLpCompanyid];
            NSInteger accid = [dic int64ForKey:kLpchaccid];
            if (comId && accid) {
                NSDictionary *lpDic = [IXDBLpchaccMgr queryLpchaccByLpchaccid:accid
                                                                  lpCompanyid:comId];
                NSInteger channelId = [lpDic int64ForKey:kChannelid];
                
                NSDictionary *cDic = [IXDBLpchannelSymbolMgr queryLpchaccSymbolByLpCompanyid:comId
                                                                                    symbolid:symId
                                                                                   channelid:channelId];
                
                model.stpSpd = [cDic[kChspread] doubleValue];
                
            }
            
            if (accid) {
                NSDictionary *sDic = [IXDBLpchaccSymbolMgr queryLpchaccSymbolByLpchaccid:accid
                                                                                symbolid:symId];
                model.lpSpd = [sDic[kLpspread] doubleValue];
            }
        }
        
        [_spdDic setValue:model forKey:key];
    }
    return model;
}


//计算点差
- (long)spdWithSymId:(NSInteger)symId
{
    //获取账户组设置的点差
    NSInteger groupId = [IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid;
    
    uint64 groupSpread = 0;
    if(![_accGroupDic.allKeys count]){
        _accGroupDic = [[IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:groupId] mutableCopy];
    }
    
    groupSpread = [_accGroupDic int64ForKey:kSpread];
    
    //获取账户组symbolCata设置的点差
    uint64 symbolCataSpread  = 0;
    NSString *symbolKey = [NSString stringWithFormat:@"%ld",(long)symId];
    if (![_groupSymCata objectForKey:symbolKey]) {
        NSDictionary *dic = [IXDBG_SMgr queryG_SByAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid symbolId:symId];
        [_groupSymCata setValue:@([dic int64ForKey:kSpread]) forKey:symbolKey];

    }
    symbolCataSpread = [_groupSymCata int64ForKey:symbolKey];

    return (symbolCataSpread + groupSpread);
}


#pragma mark --
#pragma mark 格式化
- (NSString *)transforPrice:(NSInteger)price WithSymbolId:(NSInteger)symbolId
{
    NSInteger currentDigit = 0;
    NSNumber *digit = [_cacheDigit objectForKey:[NSString stringWithFormat:@"%ld",(long)symbolId]];
    if (!digit) {
        currentDigit = [IXDataProcessTools getQuotoDigitBySymbolId:symbolId];
        [_cacheDigit setValue:@(currentDigit)
                       forKey:[NSString stringWithFormat:@"%ld",(long)symbolId]];
    }else{
        currentDigit = [digit integerValue];
    }
    
    NSDecimalNumber *formatterPrc = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%ld",(long)price]];
    formatterPrc = [formatterPrc decimalNumberByMultiplyingByPowerOf10:(-currentDigit)];
    
    return [formatterPrc stringValue];
}

- (NSDecimalNumberHandler *)handlerDigits:(NSInteger)digits
{
    return  [NSDecimalNumberHandler
             decimalNumberHandlerWithRoundingMode:NSRoundPlain
             scale:digits
             raiseOnExactness:NO
             raiseOnOverflow:NO
             raiseOnUnderflow:NO
             raiseOnDivideByZero:YES];
}

@end
