//
//  IXTradeDataCache.h
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IxItemPosition.pbobjc.h"

#define IXTradeData_listen_regist(target,keyPath)       \
[[IXTradeDataCache shareInstance] addObserver:target    \
forKeyPath:keyPath options:NSKeyValueObservingOptionNew \
context:NULL]

#define IXTradeData_listen_resign(target,keyPath)       \
[[IXTradeDataCache shareInstance] removeObserver:target \
forKeyPath:keyPath]

#define IXTradeData_isSameKey(key1,key2)                \
[key1 isEqualToString:key2]

/**更新缓存监听，数据可从缓存中直接读取*/

extern NSString * const PB_CMD_CACHE_POSITION_LIST;       //持仓列表key
extern NSString * const PB_CMD_CACHE_ORDER_LIST;          //挂单列表key
extern NSString * const PB_CMD_CACHE_DEAL_LIST;           //成交纪录key

/**更新数据库监听，数据不提供缓存读写，需要从数据库中读写,*/
//user
extern NSString * const PB_CMD_USERLOGIN_INFO;      //用户登陆key
extern NSString * const PB_CMD_USERLOGOUT;          //用户退出key
extern NSString * const PB_CMD_USER_KICT_OUT;       //用户挤下线Key

extern NSString * const PB_CMD_ACCOUNT_LIST;        //账号列表key
extern NSString * const PB_CMD_ACCOUNT_UPDATE;      //账号更新key

extern NSString * const PB_CMD_ACCOUNT_BALANCE_GET; //账号列表key
extern NSString * const PB_CMD_USER_ADD;            //添加用户key
extern NSString * const PB_CMD_USER_LOGIN_DATA;
extern NSString * const PB_CMD_USER_LOGIN_DATA_TOTAL;

//account_Group
extern NSString * const PB_CMD_ACCOUNT_GROUP_LIST;  //账户组更新key
extern NSString * const PB_CMD_ACCOUNT_GROUP_UPDATE;  //账户组更新key
extern NSString * const PB_CMD_ACCOUNT_GROUP_ADD;
extern NSString * const PB_CMD_ACCOUNT_GROUP_DELETE;
extern NSString * const PB_CMD_ACCOUNT_GROUP_CHANGE;

extern NSString * const PB_CMD_ACCOUNT_CHANGE;      //切换账户

//symbol
extern NSString * const PB_CMD_SYMBOL_CHANGE;       //产品改变key
extern NSString * const PB_CMD_SYMBOL_LIST;         //产品列表key
extern NSString * const PB_CMD_SYMBOL_UPDATE;        //产品更新key
extern NSString * const PB_CMD_SYMBOL_ADD;          //产品新增key
extern NSString * const PB_CMD_SYMBOL_DELETE;         //产品删除key

//sym_sub
extern NSString * const PB_CMD_SYMBOL_SUB_LIST;     //自选列表key
extern NSString * const PB_CMD_SYMBOL_SUB_ADD;      //添加自选key
extern NSString * const PB_CMD_SYMBOL_SUB_DELETE;   //删除自选key

//sym_hot
extern NSString * const PB_CMD_SYMBOL_HOT_LIST;     //热门列表key
extern NSString * const PB_CMD_SYMBOL_HOT_UPDATE;   //热门更新key
extern NSString * const PB_CMD_SYMBOL_HOT_ADD;     //热门增加key

//symbolCata
extern NSString * const PB_CMD_SYMBOL_CATA_LIST;    //产品分类key
extern NSString * const PB_CMD_SYMBOL_CATA_UPDATE;   //产品分类key
extern NSString * const PB_CMD_SYMBOL_CATA_ADD;       //产品分类key
extern NSString * const PB_CMD_SYMBOL_CATA_DELETE;    //产品分类key

//language
extern NSString * const PB_CMD_LANGUAGE_LIST;       //语言包key
extern NSString * const PB_CMD_LANGUAGE_ADD;       //语言包key
extern NSString * const PB_CMD_LANGUAGE_UPDATE;       //语言包key

//holiday
extern NSString * const PB_CMD_HOLIDAY_CATA_LIST;   //产品假期key

//position
extern NSString * const PB_CMD_POSITION_LIST;       //持仓增加(市价/限价)key
extern NSString * const PB_CMD_POSITION_UPDATE;     //持仓增加(市价/限价)key
extern NSString * const PB_CMD_POSITION_ADD;        //持仓增加(市价/限价)key

//order
extern NSString * const PB_CMD_ORDER_LIST;          //挂单增加(市价/限价)key
extern NSString * const PB_CMD_ORDER_UPDATE;        //挂单增加(市价/限价)key
extern NSString * const PB_CMD_ORDER_ADD;           //挂单增加(市价/限价)key
extern NSString * const PB_CMD_ORDER_CANCEL;         //取消订单
extern NSString * const PB_CMD_ORDER_ADD_BATCH;      //批量挂单增加(市价/限价)key

//deal
extern NSString * const PB_CMD_DEAL_LIST;           //成交历史增加
extern NSString * const PB_CMD_DEAL_UPDATE;         //成交历史更新
extern NSString * const PB_CMD_DEAL_ADD;            //成交历史增加

//sym_lable
extern NSString * const PB_CMD_SYMBOL_LABLE_LIST;   //产品标签
extern NSString * const PB_CMD_SYMBOL_LABLE_UPDATE;   //产品标签
extern NSString * const PB_CMD_SYMBOL_LABLE_ADD;   //产品标签
extern NSString * const PB_CMD_SYMBOL_LABLE_DELETE;

//sym_margin_set
extern NSString * const PB_CMD_SYMBOL_MARGIN_SET_LIST;   //产品保证金
extern NSString * const PB_CMD_SYMBOL_MARGIN_SET_UPDATE; //产品保证金
extern NSString * const PB_CMD_SYMBOL_MARGIN_SET_ADD;   //产品保证金
extern NSString * const PB_CMD_SYMBOL_MARGIN_SET_DELETE;

//holiday_cata
extern NSString * const PB_CMD_HOLIDAY_CATA_LIST;   //假期分类
extern NSString * const PB_CMD_HOLIDAY_CATA_UPDATE;
extern NSString * const PB_CMD_HOLIDAY_CATA_ADD;
extern NSString * const PB_CMD_HOLIDAY_CATA_DELETE;

//holiday
extern NSString * const PB_CMD_HOLIDAY_LIST;   //假期
extern NSString * const PB_CMD_HOLIDAY_UPDATE;
extern NSString * const PB_CMD_HOLIDAY_ADD;
extern NSString * const PB_CMD_HOLIDAY_DELETE;

//holiday_margin
extern NSString * const PB_CMD_HOLIDAY_MARGIN_LIST;   //假期margin
extern NSString * const PB_CMD_HOLIDAY_MARGIN_UPDATE;
extern NSString * const PB_CMD_HOLIDAY_MARGIN_ADD;
extern NSString * const PB_CMD_HOLIDAY_MARGIN_DELETE;

//campany
extern NSString * const PB_CMD_COMPANY_LIST;        //公司列表key
extern NSString * const PB_CMD_COMPANY_UPDATE;
extern NSString * const PB_CMD_COMPANY_ADD;
extern NSString * const PB_CMD_COMPANY_DELETE;

//schedule_cata
extern NSString * const PB_CMD_SCHEDULE_CATA_LIST;  //产品交易时段分类key
extern NSString * const PB_CMD_SCHEDULE_CATA_UPDATE;
extern NSString * const PB_CMD_SCHEDULE_CATA_ADD;
extern NSString * const PB_CMD_SCHEDULE_CATA_DELETE;

//schedule
extern NSString * const PB_CMD_SCHEDULE_LIST;       //产品交易时段key
extern NSString * const PB_CMD_SCHEDULE_UPDATE;
extern NSString * const PB_CMD_SCHEDULE_ADD;
extern NSString * const PB_CMD_SCHEDULE_DELETE;

//schedule_margin
extern NSString * const PB_CMD_SCHEDULE_MARGIN_LIST;   //产品交易margin key
extern NSString * const PB_CMD_SCHEDULE_MARGIN_UPDATE;
extern NSString * const PB_CMD_SCHEDULE_MARGIN_ADD;
extern NSString * const PB_CMD_SCHEDULE_MARGIN_DELETE;

//quote_delay
extern NSString * const PB_CMD_QUOTE_DELAY_LIST;   //行情订阅
extern NSString * const PB_CMD_QUOTE_DELAY_UPDATE;
extern NSString * const PB_CMD_QUOTE_DELAY_ADD;

//account_group_symbol_cata
extern NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_LIST;   //账户组产品分类
extern NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_UPDATE;
extern NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_ADD;
extern NSString * const PB_CMD_ACCOUNT_GROUP_SYMBOL_CATA_DELETE;
extern NSString * const PB_CMD_GROUP_SYMBOL_CATA_CHANGE;

//eod_time
extern NSString * const PB_CMD_EOD_TIME_LIST;   //结算时间
extern NSString * const PB_CMD_EOD_TIME_UPDATE;
extern NSString * const PB_CMD_EOD_TIME_ADD;
extern NSString * const PB_CMD_EOD_TIME_DELETE;

//secure_dev
extern NSString * const PB_CMD_SECURE_DEV_LIST;   //登录设备
extern NSString * const PB_CMD_SECURE_DEV_UPDATE;
extern NSString * const PB_CMD_SECURE_DEV_ADD;
extern NSString * const PB_CMD_SECURE_DEV_DELETE;

//group_symbol
extern NSString * const PB_CMD_GROUP_SYMBOL_LIST;   //账户组产品
extern NSString * const PB_CMD_GROUP_SYMBOL_UPDATE;
extern NSString * const PB_CMD_GROUP_SYMBOL_ADD;
extern NSString * const PB_CMD_GROUP_SYMBOL_DELETE;

//group_symbol_cata
extern NSString * const PB_CMD_GROUP_SYMBOL_CATA_LIST;   //账户组产品分类
extern NSString * const PB_CMD_GROUP_SYMBOL_CATA_UPDATE;
extern NSString * const PB_CMD_GROUP_SYMBOL_CATA_ADD;
extern NSString * const PB_CMD_GROUP_SYMBOL_CATA_DELETE;

//lp_channel_account
extern NSString * const PB_CMD_LPCHACC_LIST;   //lpchacc
extern NSString * const PB_CMD_LPCHACC_UPDATE;
extern NSString * const PB_CMD_LPCHACC_ADD;
extern NSString * const PB_CMD_LPCHACC_DELETE;

//lp_channel_account_symbol
extern NSString * const PB_CMD_LPCHACC_SYMBOL_LIST;   //lpchacc_symbol
extern NSString * const PB_CMD_LPCHACC_SYMBOL_UPDATE;
extern NSString * const PB_CMD_LPCHACC_SYMBOL_ADD;
extern NSString * const PB_CMD_LPCHACC_SYMBOL_DELETE;

//lp_channel
extern NSString * const PB_CMD_LPCHANNEL_LIST;   //lp_channel
extern NSString * const PB_CMD_LPCHANNEL_UPDATE;
extern NSString * const PB_CMD_LPCHANNEL_ADD;
extern NSString * const PB_CMD_LPCHANNEL_DELETE;

//lp_channel_symbol
extern NSString * const PB_CMD_LPCHANNEL_SYMBOL_LIST;   //lp_channel_symbol
extern NSString * const PB_CMD_LPCHANNEL_SYMBOL_UPDATE;
extern NSString * const PB_CMD_LPCHANNEL_SYMBOL_ADD;
extern NSString * const PB_CMD_LPCHANNEL_SYMBOL_DELETE;

//lp_IbBind
extern NSString * const PB_CMD_LPIB_BIND_LIST;   //lp_IbBind
extern NSString * const PB_CMD_LPIB_BIND_UPDATE;
extern NSString * const PB_CMD_LPIB_BIND_ADD;
extern NSString * const PB_CMD_LPIB_BIND_DELETE;

extern NSString * const CMD_TIMEZONE_UPDATE;        //时区更新
extern NSString * const CMD_UPLOAD_HEAD_UPDATE;    //头像更新

//kline repare
extern NSString * const KLINE_CMD_REPARE_NOTIFY;

@interface IXTradeDataCache : NSObject

+ (IXTradeDataCache *)shareInstance;

- (void)clearInstance;

#pragma mark -
#pragma mark - others

//user
@property (atomic, weak) id pb_user_login_info;        //用户登陆 (sql)
@property (atomic, weak) id pb_user_logout;            //用户退出
@property (atomic, weak) id pb_user_add;               //添加账号
@property (atomic, weak) id pb_user_kick_out;          //多设备登录
@property (atomic, weak) id pb_user_login_data_total;
@property (atomic, weak) id pb_user_login_data;        //增量数据下发


@property (atomic, strong) id pb_account_list;           //账号列表 (sql)
@property (atomic, strong) id pb_account_update;         //账号更新(sql)

//account_group
@property (atomic, weak) id pb_account_group_list;    //账户组更新(sql)
@property (atomic, weak) id pb_account_group_update;    //账户组更新(sql)
@property (atomic, weak) id pb_account_group_add;
@property (atomic, weak) id pb_account_group_delete;
@property (nonatomic, assign) BOOL accountGroupChange;


@property (atomic, weak) id pb_account_balance_get;    //获取账户

@property (nonatomic, assign) BOOL swiAccount;            //切换账号

@property (atomic, weak) id pb_symbol_sub_list;        //自选列表 (sql)
@property (atomic, weak) id pb_symbol_sub_add;         //添加自选 (sql)
@property (atomic, weak) id pb_symbol_sub_delete;      //删除自选 (sql)
@property (atomic, weak) id pb_symbol_hot_list;        //热门列表 (sql)
@property (atomic, weak) id pb_symbol_hot_update;      //热门更新 (sql)
@property (atomic, weak) id pb_symbol_hot_add;         //热门增加 (sql)
@property (atomic, weak) id pb_symbol_cata_list;       //产品分类 (sql)
@property (atomic, weak) id pb_symbol_cata_update;     //产品分类 (sql)
@property (atomic, weak) id pb_symbol_cata_add;       //产品分类 (sql)
@property (atomic, weak) id pb_symbol_cata_delete;    //产品分类 (sql)

#pragma mark -
#pragma mark - pb_cache_language_list
//language
@property (atomic, weak) id pb_language_list;           //语言包 (sql)
@property (atomic, weak) id pb_language_add;           //语言包 (sql)
@property (atomic, weak) id pb_language_update;        //语言包 (sql)

@property (atomic, assign) BOOL timezone_update;       //时区更新(sql)

//sym_lable
@property (atomic, weak) id pb_symbol_lable_list;       //产品标签(sql)
@property (atomic, weak) id pb_symbol_lable_update;     //产品标签(sql)
@property (atomic, weak) id pb_symbol_lable_add;        //产品标签(sql)
@property (atomic, weak) id pb_symbol_lable_delete;

//sym_margin_set
@property (atomic, weak) id pb_symbol_margin_set_list;       //产品保证金(sql)
@property (atomic, weak) id pb_symbol_margin_set_update;     //产品保证金(sql)
@property (atomic, weak) id pb_symbol_margin_set_add;        //产品保证金(sql)
@property (atomic, weak) id pb_symbol_margin_set_delete;

//holiday_cata
@property (atomic, weak) id pb_holiday_cata_list;       //产品假期分类 (sql)
@property (atomic, weak) id pb_holiday_cata_update;
@property (atomic, weak) id pb_holiday_cata_add;
@property (atomic, weak) id pb_holiday_cata_delete;

//holiday
@property (atomic, weak) id pb_holiday_list;       //产品假期(sql)
@property (atomic, weak) id pb_holiday_update;
@property (atomic, weak) id pb_holiday_add;
@property (atomic, weak) id pb_holiday_delete;

//holiday_margin
@property (atomic, weak) id pb_holiday_margin_list;   //产品假期margin (sql)
@property (atomic, weak) id pb_holiday_margin_update;
@property (atomic, weak) id pb_holiday_margin_add;
@property (atomic, weak) id pb_holiday_margin_delete;

//company
@property (atomic, weak) id pb_company_list;   //公司 (sql)
@property (atomic, weak) id pb_company_update;
@property (atomic, weak) id pb_company_add;
@property (atomic, weak) id pb_company_delete;

//schedule_cata
@property (atomic, weak) id pb_schedule_cata_list;          //产品交易分类时段 (sql)
@property (atomic, weak) id pb_schedule_cata_update;
@property (atomic, weak) id pb_schedule_cata_add;
@property (atomic, weak) id pb_schedule_cata_delete;

//schedule
@property (atomic, weak) id pb_schedule_list;          //产品交易时段 (sql)
@property (atomic, weak) id pb_schedule_update;
@property (atomic, weak) id pb_schedule_add;
@property (atomic, weak) id pb_schedule_delete;

//schedule_margin
@property (atomic, weak) id pb_schedule_margin_list;          //产品交易时段 (sql)
@property (atomic, weak) id pb_schedule_margin_update;
@property (atomic, weak) id pb_schedule_margin_add;
@property (atomic, weak) id pb_schedule_margin_delete;

//account_group_symbol_cata
@property (atomic, weak) id pb_account_group_symbol_cata_list;          //账户组产品分类 (sql)
@property (atomic, weak) id pb_account_group_symbol_cata_update;
@property (atomic, weak) id pb_account_group_symbol_cata_add;
@property (atomic, weak) id pb_account_group_symbol_cata_delete;
@property (nonatomic, assign) BOOL accGroupSymCataFlag;

//quote_delay
@property (atomic, weak) id pb_quote_delay_list;     //行情订阅 (sql)
@property (atomic, weak) id pb_quote_delay_update;
@property (atomic, weak) id pb_quote_delay_add;

//eod_time
@property (atomic, weak) id pb_eod_time_list;     //结算时间 (sql)
@property (atomic, weak) id pb_eod_time_update;
@property (atomic, weak) id pb_eod_time_add;
@property (atomic, weak) id pb_eod_time_delete;

//secure_dev
@property (atomic, weak) id pb_secure_dev_list;     //登录设备 (sql)
@property (atomic, weak) id pb_secure_dev_update;
@property (atomic, weak) id pb_secure_dev_add;
@property (atomic, weak) id pb_secure_dev_delete;

//group_symbol
@property (atomic, weak) id pb_group_symbol_list;     //账户组产品 (sql)
@property (atomic, weak) id pb_group_symbol_update;
@property (atomic, weak) id pb_group_symbol_add;
@property (atomic, weak) id pb_group_symbol_delete;

//group_symbol_cata
@property (atomic, weak) id pb_group_symbol_cata_list;     //账户组产品分类 (sql)
@property (atomic, weak) id pb_group_symbol_cata_update;
@property (atomic, weak) id pb_group_symbol_cata_add;
@property (atomic, weak) id pb_group_symbol_cata_delete;

//lp_channel_account
@property (atomic, weak) id pb_lpchacc_list;     //lpchacc (sql)
@property (atomic, weak) id pb_lpchacc_update;
@property (atomic, weak) id pb_lpchacc_add;
@property (atomic, weak) id pb_lpchacc_delete;

//lp_channel_account_symbol
@property (atomic, weak) id pb_lpchacc_symbol_list;     //lpchacc_symbol (sql)
@property (atomic, weak) id pb_lpchacc_symbol_update;
@property (atomic, weak) id pb_lpchacc_symbol_add;
@property (atomic, weak) id pb_lpchacc_symbol_delete;

//lp_channel
@property (atomic, weak) id pb_lpchannel_list;     //lpchannel (sql)
@property (atomic, weak) id pb_lpchannel_update;
@property (atomic, weak) id pb_lpchannel_add;
@property (atomic, weak) id pb_lpchannel_delete;

//lp_channel_symbol
@property (atomic, weak) id pb_lpchannel_symbol_list;     //lpchannel_symbol (sql)
@property (atomic, weak) id pb_lpchannel_symbol_update;
@property (atomic, weak) id pb_lpchannel_symbol_add;
@property (atomic, weak) id pb_lpchannel_symbol_delete;

//lp_IbBind
@property (atomic, weak) id pb_lpib_bind_list;     //lpib_bind (sql)
@property (atomic, weak) id pb_lpib_bind_update;
@property (atomic, weak) id pb_lpib_bind_add;
@property (atomic, weak) id pb_lpib_bind_delete;



//kline repate
@property (atomic, weak) id pb_kLine_repare_noti;

#pragma mark -
#pragma mark - pb_cache_position_list

@property (nonatomic, assign) BOOL positionChangedFlag;
@property (nonatomic, strong) NSMutableArray *pb_cache_position_list;  //持仓列表 (ram)
//增加
- (void)addPb_cache_position_list:(NSMutableArray *)pb_cache_position_list;
//删除
- (void)deletePb_cache_position_list:(uint64_t)positionId;
//更新（部分平仓）
- (void)updatePb_cache_position_list:(item_position *)position;

//position
@property (atomic, strong) id pb_position_list;          //持仓更新(市价/限价) (sql)
@property (atomic, strong) id pb_position_update;        //持仓更新(市价/限价) (sql)
@property (atomic, strong) id pb_position_add;           //持仓增加(市价/限价) (sql)

#pragma mark -
#pragma mark - pb_cache_order_list

@property (nonatomic, assign) BOOL orderChangedFlag;
@property (atomic, strong) NSMutableArray *pb_cache_order_list;     //挂单列表 (ram)

//增加
- (void)addPb_cache_order_list:(NSMutableArray *)pb_cache_order_list;
//更新
- (void)updatePb_cache_order:(item_order *)order;
//删除
- (void)deletePb_cache_order_list:(uint64_t)orderId;
//order
@property (atomic, strong) id pb_order_list;                //挂单更新(市价/限价) (sql)
@property (atomic, strong) id pb_order_update;              //挂单更新(市价/限价) (sql)
@property (atomic, strong) id pb_order_add;                 //挂单增加(市价/限价) (sql)
@property (atomic, strong) id pb_order_cancel;              //挂单取消(市价/限价) (sql)
@property (atomic, strong) id pb_order_add_batch;           //挂单增加(市价/限价) (sql)

#pragma mark -
#pragma mark - pb_cache_deal_list

@property (nonatomic, assign) BOOL dealChangedFlag;
@property (atomic, strong) NSMutableArray *pb_cache_deal_list;      //成交纪录 (ram)

//增加
- (void)addPb_cache_deal_list:(NSMutableArray *)pb_cache_deal_list;
//删除
- (void)deletePb_cache_deal_list:(uint64_t)dealId;
//deal
@property (atomic, weak) id pb_deal_list;               //成交历史增加(市价/限价) (sql)
@property (atomic, weak) id pb_deal_update;            //成交历史更新(市价/限价) (sql)
@property (atomic, weak) id pb_deal_add;               //成交历史增加(市价/限价) (sql)

#pragma mark -
#pragma mark - pb_cache_deal_list
@property (nonatomic, assign) BOOL symbolChangedFlag;
@property (atomic, strong) id pb_symbol_list;            //产品列表 (sql)
@property (atomic, weak) id pb_symbol_update;            //产品更新 (sql)
@property (atomic, weak) id pb_symbol_add;              //产品新增 (sql)
@property (atomic, weak) id pb_symbol_delete;            //产品删除 (sql)

- (void)dealWithSymbolChange;

@end
