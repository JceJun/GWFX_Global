//
//  IXQuoteCacheDigit.m
//  IXApp
//
//  Created by Bob on 2017/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXQuoteCacheDigit.h"

@interface IXQuoteCacheDigit ()

@property (nonatomic, strong) NSMutableDictionary *cacheDigit;

@end

@implementation IXQuoteCacheDigit

+ (IXQuoteCacheDigit *)shareIntance
{
    static IXQuoteCacheDigit *share = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ( !share ) {
            share = [[IXQuoteCacheDigit alloc] init];
        }
    });
    return share;
}

- (id)init
{
    self = [super init];
    if ( self ) {
        _cacheDigit = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSString *)transforPrice:(NSInteger)price WithSymbolId:(NSInteger)symbolId
{
    NSNumber *digit = [_cacheDigit objectForKey:[NSString stringWithFormat:@"%ld",(long)symbolId]];
    NSString *priceStr = [NSString stringWithFormat:@"%ld",(long)price];

    if (  digit ) {
        NSDecimalNumber *result = [[NSDecimalNumber decimalNumberWithString:priceStr]
                                   decimalNumberByMultiplyingByPowerOf10:-[digit integerValue]
                                   withBehavior:[self handlerDigits:[digit integerValue]]];
        return [result stringValue];
    }else{
        NSInteger currentDigit = [IXDataProcessTools getQuotoDigitBySymbolId:symbolId];
        [_cacheDigit setValue:@(currentDigit)
                       forKey:[NSString stringWithFormat:@"%ld",symbolId]];

        NSDecimalNumber *result = [[NSDecimalNumber decimalNumberWithString:priceStr]
                                   decimalNumberByMultiplyingByPowerOf10:-currentDigit
                                   withBehavior:[self handlerDigits:currentDigit]];
        return [result stringValue];
    }
}

- (NSDecimalNumberHandler *)handlerDigits:(NSInteger)digits
{
    return  [NSDecimalNumberHandler
             decimalNumberHandlerWithRoundingMode:NSRoundDown
             scale:digits
             raiseOnExactness:NO
             raiseOnOverflow:NO
             raiseOnUnderflow:NO
             raiseOnDivideByZero:YES];
}

@end
