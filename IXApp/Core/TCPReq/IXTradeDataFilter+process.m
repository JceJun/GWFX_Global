//
//  IXTradeDataFilter+process.m
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeDataFilter+process.h"
#include "IXCrc16.h"

#import "IxProtoUser.pbobjc.h"
#import "IxProtoSymbol.pbobjc.h"
#import "IxProtoSymbolCata.pbobjc.h"
#import "IxProtoSymbolSub.pbobjc.h"
#import "IxProtoSymbolSubCata.pbobjc.h"
#import "IxProtoHoliday.pbobjc.h"
#import "IxProtoHolidayCata.pbobjc.h"
#import "IxProtoCompany.pbobjc.h"
#import "IxProtoSchedule.pbobjc.h"
#import "IxProtoScheduleCata.pbobjc.h"
#import "IxProtoPosition.pbobjc.h"
#import "IxProtoOrder.pbobjc.h"
#import "IxProtoAccount.pbobjc.h"
#import "IxProtoAccountBalance.pbobjc.h"
#import "IxProtoDeal.pbobjc.h"
#import "IxProtoSymbolHot.pbobjc.h"
#import "IxProtoLanguage.pbobjc.h"
#import "IxProtoSymbolMarginSet.pbobjc.h"
#import "IxProtoSymbolLabel.pbobjc.h"
#import "IxProtoScheduleMargin.pbobjc.h"
#import "IxProtoHolidayMargin.pbobjc.h"
#import "IxProtoAccountGroup.pbobjc.h"
#import "IxProtoAccountGroupSymbolCata.pbobjc.h"
#import "IxProtoQuoteDelay.pbobjc.h"
#import "IxProtoKlineRepair.pbobjc.h"
#import "IxProtoEodTime.pbobjc.h"
#import "IxProtoSecureDev.pbobjc.h"
#import "IxProtoGroupSymbol.pbobjc.h"
#import "IxProtoGroupSymbolCata.pbobjc.h"
#import "IxProtoLpchacc.pbobjc.h"
#import "IxProtoLpchaccSymbol.pbobjc.h"
#import "IxProtoLpchannel.pbobjc.h"
#import "IxProtoLpchannelSymbol.pbobjc.h"
#import "IxProtoLpibBind.pbobjc.h"

@implementation IXTradeDataFilter (process)


+ (NSData *)packetWithPBMsg:(GPBMessage *)proto Command:(uint16)command
{
    if(!proto)return nil;
    
    struct protocol_proto pkt;
    size_t hsize = sizeof(struct protocol_simple_header);
    memset(&pkt,0,hsize);
    pkt.header.command = htons(command);
    
    size_t size =  [[proto data] length];
    size_t szt = hsize + size;
    pkt.header.length = htons(szt);
    
    NSData *bodyData = [NSData dataWithData:[proto data]];
    NSData *headerData = [NSData dataWithBytes:&pkt.header length:hsize];
    NSMutableData *desData = [NSMutableData dataWithData:headerData];
    [desData appendData:bodyData];
    
    uint16 crc = [IXCrc16 crc16:[desData bytes] length:(int)szt];
    pkt.header.crc = htons(crc);
    
    headerData = [NSData dataWithBytes:&pkt.header length:hsize];
    NSMutableData *lastData = [NSMutableData dataWithData:headerData];
    [lastData appendData:bodyData];
    
    return lastData;
}

+ (NSDictionary *)unpacketData:(NSData *)data
{    
    if(![data length])return nil;
    
    NSData *classDesInfo = [data subdataWithRange:NSMakeRange(0, simple_header_length)];
    struct protocol_proto pkt;
    memset(&pkt,0, simple_header_length);
    memcpy(&pkt, [classDesInfo bytes], simple_header_length);
    uint16 command = ntohs(pkt.header.command);
    NSData *bodyData = [data subdataWithRange:NSMakeRange(simple_header_length, [data length] - simple_header_length)];
    
    NSDictionary *dic = @{@"prototag":bodyData,@"command":[NSNumber numberWithLong:command]};
    return dic;
}

+ (id)dataConvert2PBObj:(NSData *)bodyData cmd:(uint16)cmd
{
    id obj = nil;
    switch (cmd) {
        //user
        case CMD_USER_KEEPALIVE:
            obj = [proto_user_keepalive parseFromData:bodyData error:nil];
            break;
        case CMD_USER_LOGIN_INFO:
            obj = [proto_user_login_info parseFromData:bodyData error:nil];
            break;
        case CMD_USER_ADD:
            obj = [proto_user_add parseFromData:bodyData error:nil];
            break;
        case CMD_USER_KICK_OUT:
            obj = [proto_user_kickout parseFromData:bodyData error:nil];
            break;
        case CMD_USER_LOGIN_DATA_TOTAL:
            obj = [proto_user_login_data_total parseFromData:bodyData error:nil];
            break;
        case CMD_USER_LOGIN_DATA:
            obj = [proto_user_login_data parseFromData:bodyData error:nil];
            break;
        //account
        case CMD_ACCOUNT_LIST:
            obj = [proto_account_list parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNT_UPDATE:
            obj = [proto_account_update parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNT_ADD:
            obj = [proto_account_add parseFromData:bodyData error:nil];
            break;
            
        case CMD_ACCOUNTBALANCE_ADD:
            obj = [proto_account_balance_add parseFromData:bodyData error:nil];
            break;
            
        case CMD_ACCOUNTBALANCE_GET:
            obj = [proto_account_balance_get parseFromData:bodyData error:nil];
            break;
        
         //account_group
        case CMD_ACCOUNT_GROUP_LIST:
            obj = [proto_account_group_list parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNT_GROUP_UPDATE:
            obj = [proto_account_group_update parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNT_GROUP_ADD:
            obj = [proto_account_group_add parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNT_GROUP_DELETE:
            obj = [proto_account_group_delete parseFromData:bodyData error:nil];
            break;
            
        case CMD_ACCOUNTGROUPSYMBOLCATA_LIST:
            obj = [proto_account_group_symbol_cata_list parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNTGROUPSYMBOLCATA_ADD:
            obj = [proto_account_group_symbol_cata_add parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNTGROUPSYMBOLCATA_UPDATE:
            obj = [proto_account_group_symbol_cata_update parseFromData:bodyData error:nil];
            break;
        case CMD_ACCOUNTGROUPSYMBOLCATA_DELETE:
            obj = [proto_account_group_symbol_cata_delete parseFromData:bodyData error:nil];
            break;
            
        //symbol_cata
        case CMD_SYMBOL_CATA_LIST:
            obj = [proto_symbol_cata_list parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_CATA_UPDATE:
            obj = [proto_symbol_cata_update parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_CATA_ADD:
            obj = [proto_symbol_cata_add parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_CATA_DELETE:
            obj = [proto_symbol_cata_delete parseFromData:bodyData error:nil];
            break;
            
        case CMD_SYMBOL_LIST:
            obj = [proto_symbol_list parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_UPDATE:
            obj = [proto_symbol_update parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_ADD:
            obj = [proto_symbol_add parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_DELETE:
            obj = [proto_symbol_delete parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_HOT_ADD:
            obj = [proto_symbol_hot_add parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_HOT_DELETE:
            obj = [proto_symbol_hot_delete parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_HOT_UPDATE:
            obj = [proto_symbol_hot_update parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_HOT_LIST:
            obj = [proto_symbol_hot_list parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_SUB_ADD:
            obj = [proto_symbol_sub_add parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_SUB_DELETE:
            obj = [proto_symbol_sub_delete parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_SUB_LIST:
            obj = [proto_symbol_sub_list parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_SUB_CATA_LIST:
            obj = [proto_symbol_sub_cata_list parseFromData:bodyData error:nil];
            break;
            
        //sym_margin_set
        case CMD_SYMBOL_MARGIN_SET_LIST:
            obj = [proto_symbol_margin_set_list parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_MARGIN_SET_UPDATE:
            obj = [proto_symbol_margin_set_update parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_MARGIN_SET_ADD:
            obj = [proto_symbol_margin_set_add parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_MARGIN_SET_DELETE:
            obj = [proto_symbol_margin_set_delete parseFromData:bodyData error:nil];
            break;
            
        //sym_lable
        case CMD_SYMBOL_LABEL_LIST:
            obj = [proto_symbol_label_list parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_LABEL_UPDATE:
            obj = [proto_symbol_label_update parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_LABEL_ADD:
            obj = [proto_symbol_label_add parseFromData:bodyData error:nil];
            break;
        case CMD_SYMBOL_LABEL_DELETE:
            obj = [proto_symbol_label_delete parseFromData:bodyData error:nil];
            break;
            
        //holiday
        case CMD_HOLIDAY_LIST:
            obj = [proto_holiday_list parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_UPDATE:
            obj = [proto_holiday_update parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_ADD:
            obj = [proto_holiday_add parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_DELETE:
            obj = [proto_holiday_delete parseFromData:bodyData error:nil];
            break;
            
        //holiday_cata
        case CMD_HOLIDAY_CATA_LIST:
            obj = [proto_holiday_cata_list parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_CATA_UPDATE:
            obj = [proto_holiday_cata_update parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_CATA_ADD:
            obj = [proto_holiday_cata_add parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_CATA_DELETE:
            obj = [proto_holiday_cata_delete parseFromData:bodyData error:nil];
            break;
            
        //holiday_margin
        case CMD_HOLIDAY_MARGIN_LIST:
            obj = [proto_holiday_margin_list parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_MARGIN_UPDATE:
            obj = [proto_holiday_margin_update parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_MARGIN_ADD:
            obj = [proto_holiday_margin_add parseFromData:bodyData error:nil];
            break;
        case CMD_HOLIDAY_MARGIN_DELETE:
            obj = [proto_holiday_margin_delete parseFromData:bodyData error:nil];
            break;
            
        //company
        case CMD_COMPANY_LIST:
            obj = [proto_company_list parseFromData:bodyData error:nil];
            break;
        case CMD_COMPANY_ADD:
            obj = [proto_company_add parseFromData:bodyData error:nil];
            break;
        case CMD_COMPANY_UPDATE:
            obj = [proto_company_update parseFromData:bodyData error:nil];
            break;
        case CMD_COMPANY_DELETE:
            obj = [proto_company_delete parseFromData:bodyData error:nil];
            break;
            
        //schedule
        case CMD_SCHEDULE_LIST:
            obj = [proto_schedule_list parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_UPDATE:
            obj = [proto_schedule_update parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_ADD:
            obj = [proto_schedule_add parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_DELETE:
            obj = [proto_schedule_delete parseFromData:bodyData error:nil];
            break;
            
        //shedule_cata
        case CMD_SCHEDULE_CATA_LIST:
            obj = [proto_schedule_cata_list parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_CATA_UPDATE:
            obj = [proto_schedule_cata_update parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_CATA_ADD:
            obj = [proto_schedule_cata_add parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_CATA_DELETE:
            obj = [proto_schedule_cata_delete parseFromData:bodyData error:nil];
            break;
            
        //schedule_margin
        case CMD_SCHEDULE_MARGIN_LIST:
            obj = [proto_schedule_margin_list parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_MARGIN_UPDATE:
            obj = [proto_schedule_margin_update parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_MARGIN_ADD:
            obj = [proto_schedule_margin_add parseFromData:bodyData error:nil];
            break;
        case CMD_SCHEDULE_MARGIN_DELETE:
            obj = [proto_schedule_margin_delete parseFromData:bodyData error:nil];
            break;
            
        //position
        case CMD_POSITION_ADD:
            obj = [proto_position_add parseFromData:bodyData error:nil];
            break;
        case CMD_POSITION_LIST:
            obj = [proto_position_list parseFromData:bodyData error:nil];
            break;
        case CMD_POSITION_UPDATE:
            obj = [proto_position_update parseFromData:bodyData error:nil];
            break;
       
        //order
        case CMD_ORDER_LIST:
            obj = [proto_order_list parseFromData:bodyData error:nil];
            break;
        case CMD_ORDER_ADD:
            obj = [proto_order_add parseFromData:bodyData error:nil];
            break;
        case CMD_ORDER_UPDATE:
            obj = [proto_order_update parseFromData:bodyData error:nil];
            break;
        case CMD_ORDER_CANCEL:
            obj = [proto_order_cancel parseFromData:bodyData error:nil];
            break;
        case CMD_ORDER_ADD_BATCH:
            obj = [proto_order_add_batch parseFromData:bodyData error:nil];
            break;
        
        //deal
        case CMD_DEAL_ADD:
            obj = [proto_deal_add parseFromData:bodyData error:nil];
            break;
        case CMD_DEAL_LIST:
            obj = [proto_deal_list parseFromData:bodyData error:nil];
            break;
        
        //language
        case CMD_LANGUAGE_LIST:
            obj = [proto_language_list parseFromData:bodyData error:nil];
            break;
        case CMD_LANGUAGE_UPDATE:
            obj = [proto_language_update parseFromData:bodyData error:nil];
            break;
        case CMD_LANGUAGE_ADD:
            obj = [proto_language_add parseFromData:bodyData error:nil];
            break;
            
        //quote_delay
        case CMD_QUOTE_DELAY_LIST:
            obj = [proto_quote_delay_list parseFromData:bodyData error:nil];
            break;
        case CMD_QUOTE_DELAY_UPDATE:
            obj = [proto_quote_delay_update parseFromData:bodyData error:nil];
            break;
        case CMD_QUOTE_DELAY_ADD:
            obj = [proto_quote_delay_add parseFromData:bodyData error:nil];
            break;
            
       //eod_time
        case CMD_EOD_TIME_LIST:
            obj = [proto_eod_time_list parseFromData:bodyData error:nil];
            break;
        case CMD_EOD_TIME_UPDATE:
            obj = [proto_eod_time_update parseFromData:bodyData error:nil];
            break;
        case CMD_EOD_TIME_ADD:
            obj = [proto_eod_time_add parseFromData:bodyData error:nil];
            break;
        case CMD_EOD_TIME_DELETE:
            obj = [proto_eod_time_delete parseFromData:bodyData error:nil];
            break;
            
            //secure_dev
        case CMD_SECURE_DEV_LIST:
            obj = [proto_secure_dev_list parseFromData:bodyData error:nil];
            break;
        case CMD_SECURE_DEV_UPDATE:
            obj = [proto_secure_dev_update parseFromData:bodyData error:nil];
            break;
        case CMD_SECURE_DEV_ADD:
            obj = [proto_secure_dev_add parseFromData:bodyData error:nil];
            break;
        case CMD_SECURE_DEV_DELETE:
            obj = [proto_secure_dev_delete parseFromData:bodyData error:nil];
            break;
            
        //group_sym
        case CMD_GROUP_SYMBOL_LIST:
            obj = [proto_group_symbol_list parseFromData:bodyData error:nil];
            break;
        case CMD_GROUP_SYMBOL_UPDATE:
            obj = [proto_group_symbol_update parseFromData:bodyData error:nil];
            break;
        case CMD_GROUP_SYMBOL_ADD:
            obj = [proto_group_symbol_add parseFromData:bodyData error:nil];
            break;
        case CMD_GROUP_SYMBOL_DELETE:
            obj = [proto_group_symbol_delete parseFromData:bodyData error:nil];
            break;
            
        //group_sym_cata
        case CMD_GROUP_SYMBOLCATA_LIST:
            obj = [proto_group_symbol_cata_list parseFromData:bodyData error:nil];
            break;
        case CMD_GROUP_SYMBOLCATA_UPDATE:
            obj = [proto_group_symbol_cata_update parseFromData:bodyData error:nil];
            break;
        case CMD_GROUP_SYMBOLCATA_ADD:
            obj = [proto_group_symbol_cata_add parseFromData:bodyData error:nil];
            break;
        case CMD_GROUP_SYMBOLCATA_DELETE:
            obj = [proto_group_symbol_cata_delete parseFromData:bodyData error:nil];
            break;
            
        //lp_channel_account
        case CMD_LPCHANNEL_ACCOUNT_LIST:
            obj = [proto_lpchacc_list parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_ACCOUNT_UPDATE:
            obj = [proto_lpchacc_update parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_ACCOUNT_ADD:
            obj = [proto_lpchacc_add parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_ACCOUNT_DELETE:
            obj = [proto_lpchacc_delete parseFromData:bodyData error:nil];
            break;
            
        //lp_channel_account_symbol
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST:
            obj = [proto_lpchacc_symbol_list parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_UPDATE:
            obj = [proto_lpchacc_symbol_update parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_ADD:
            obj = [proto_lpchacc_symbol_add parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_DELETE:
            obj = [proto_lpchacc_symbol_delete parseFromData:bodyData error:nil];
            break;
            
        //lp_channel
        case CMD_LPCHANNEL_LIST:
            obj = [proto_lpchannel_list parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_UPDATE:
            obj = [proto_lpchannel_update parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_ADD:
            obj = [proto_lpchannel_add parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_DELETE:
            obj = [proto_lpchannel_delete parseFromData:bodyData error:nil];
            break;
            
        //lp_channel_symbol
        case CMD_LPCHANNEL_SYMBOL_LIST:
            obj = [proto_lpchannel_symbol_list parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_SYMBOL_UPDATE:
            obj = [proto_lpchannel_symbol_update parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_SYMBOL_ADD:
            obj = [proto_lpchannel_symbol_add parseFromData:bodyData error:nil];
            break;
        case CMD_LPCHANNEL_SYMBOL_DELETE:
            obj = [proto_lpchannel_symbol_delete parseFromData:bodyData error:nil];
            break;
            
        //lp_IbBind
        case CMD_LPIB_BIND_LIST:
            obj = [proto_lpib_bind_list parseFromData:bodyData error:nil];
            break;
        case CMD_LPIB_BIND_UPDATE:
            obj = [proto_lpib_bind_update parseFromData:bodyData error:nil];
            break;
        case CMD_LPIB_BIND_ADD:
            obj = [proto_lpib_bind_add parseFromData:bodyData error:nil];
            break;
        case CMD_LPIB_BIND_DELETE:
            obj = [proto_lpib_bind_delete parseFromData:bodyData error:nil];
            break;
            
        //kline
        case CMD_KLINE_REPAIR_LIST:
            obj = [proto_kline_repair_list parseFromData:bodyData error:nil];
            break;
            
        default:
            ELog([NSString stringWithFormat:@"Trade Sever Presents Unexpected Cmd : %x",cmd]);
            break;
    }
    return obj;
}

@end
