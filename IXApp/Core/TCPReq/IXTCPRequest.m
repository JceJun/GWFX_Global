//
//  IXTCPRequest.m
//  IXApp
//
//  Created by Magee on 16/11/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTCPRequest.h"
#import "IXTCPManager.h"
#import "IXKeepAliveCache.h"
#import "IXStaticsMgr.h"
#import "IXTradeDataFilter+process.h"
#import "IXQuoteDataFilter+process.h"

@interface IXTCPRequest ()<IXTCPManagerDelegate,
                           IXQuoteDataFilterDelegate,
                           IXTradeDataFilterDelegate>

@property (nonatomic,strong) IXTCPManager *tcpManager;
@property (nonatomic,assign) IXTCPConnectStatus quoteStatus;
@property (nonatomic,assign) IXTCPConnectStatus tradeStatus;

@property (nonatomic,strong) id holdParam;
@property (nonatomic,assign) PROTOCOL_HEADER_COMMAND holdCmd;

@property (nonatomic,strong) IXQuoteDataFilter *quoteFilter;
@property (nonatomic,strong) IXTradeDataFilter *tradeFilter;

@end

@implementation IXTCPRequest

+ (IXTCPRequest *)shareInstance
{
    static IXTCPRequest *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!instance) {
            instance = [[IXTCPRequest alloc] init];
            [instance.tcpManager connectWithSeverType:IXTCPServerTypeQuote];
        }
    });
    return instance;
}

- (id)init
{
    self = [super init];
    if(self){
        self.tcpManager = [[IXTCPManager alloc] init];
        self.tcpManager.delegate = self;
        
        self.quoteFilter = [[IXQuoteDataFilter alloc] init];
        self.quoteFilter.delegate = self;
        
        self.tradeFilter = [[IXTradeDataFilter alloc] init];
        self.tradeFilter.delegate = self;
    }
    return self;
}

- (NSString *)routeType
{
    return [self.tcpManager curLine];
}

#pragma mark -
#pragma mark - IXTCPManagerDelegate

- (void)IXTCPManagerRealTimeServerType:(IXTCPServerType)serverType
                          serverStatus:(IXTCPConnectStatus)serverStatus
{
    if(serverType==IXTCPServerTypeQuote){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.quoteStatus = serverStatus;
            if(serverStatus==IXTCPConnectStatusConnected){
                NSData *loginData = [[IXKeepAliveCache shareInstance] quoteLoginData];
                [_tcpManager sendData:loginData severType:IXTCPServerTypeQuote];
            }else if(_quoteStatus == IXTCPConnectStatusDisconnect ||
                     _quoteStatus == IXTCPConnectStatusMaxTried){
                [[IXKeepAliveCache shareInstance] resetQuoteSeqNo];
            }
        });       
    }else{
        //登陆/注册比较特殊，有可能用户在登陆/注册时 trade服务器并不在连接状态，
        //此时需要api层首先连接到trade服务器，然后再次尝试登陆／注册
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tradeStatus = serverStatus;
            if(serverStatus==IXTCPConnectStatusConnected){
                if(self.holdCmd==CMD_USER_LOGIN&&self.holdParam){
                    [self loginWithParam:self.holdParam];
                    self.holdCmd = CMD_INITIAL;
                    self.holdParam = nil;
                }
                else if(self.holdCmd==CMD_USER_ADD&&self.holdParam){
                    [self registWithParam:self.holdParam];
                    self.holdCmd = CMD_INITIAL;
                    self.holdParam = nil;
                }
            }
        });
    }
}

- (void)IXTCPManagerDidReadData:(NSData *)data serverType:(IXTCPServerType)serverType
{
    if (serverType == IXTCPServerTypeQuote) {
        [_quoteFilter filtrate:data];
    } else {
        [_tradeFilter filtrate:data];
    }
}

#pragma mark -
#pragma mark - IXTCPDataFilterDelegate

- (void)IXTradePB:(id)pbobj cmd:(PROTOCOL_HEADER_COMMAND)cmd
{    
    if(cmd==CMD_USER_LOGIN_INFO){
        DLog(@"重连行情服务器...");
        [_tcpManager connectWithSeverType:IXTCPServerTypeQuote];
    }
}

- (void)IXQuoteData:(NSMutableArray *)arr cmd:(uint16)cmd
{
    @try{
        if([self.delegate respondsToSelector:@selector(IXTCPQuoteSuccessData:cmd:)]){
            [self.delegate IXTCPQuoteSuccessData:arr cmd:(uint16)cmd];
        }
    }
    @catch (NSExpression *ex){
        //异常处理代码
    }
} 

#pragma mark 断开连接
- (void)disConnect {
    [_tcpManager disConnectWithSeverType:IXTCPServerTypeTrade];
    [_tcpManager disConnectWithSeverType:IXTCPServerTypeQuote];
}

- (void)reconnect
{
    //因不知道断开了多久，所以这里直接做重新登陆
    [_tcpManager disConnectWithSeverType:IXTCPServerTypeTrade];
    [_tcpManager disConnectWithSeverType:IXTCPServerTypeQuote];
    
    [_tcpManager connectWithSeverType:IXTCPServerTypeTrade];
}

#pragma mark -
#pragma mark - Reqeusts
//登录
- (void)loginWithParam:(proto_user_login *)param
{
    //登录trade埋点相关
    [[IXStaticsMgr shareInstance] beginLoginTrade];
    
    if(_tradeStatus==IXTCPConnectStatusConnected){
        NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_USER_LOGIN];
        if(data){
            [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
        }
    }else{
        self.holdCmd = CMD_USER_LOGIN;
        self.holdParam = param;
        [_tcpManager connectWithSeverType:IXTCPServerTypeTrade];
    }
}

- (void)userDataVersion:(proto_user_login_data *)param
{
    if(_tradeStatus==IXTCPConnectStatusConnected){
        NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_USER_LOGIN_DATA];
        if(data){
            [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
        }
    }else{
        self.holdCmd = CMD_USER_LOGIN_DATA;
        self.holdParam = param;
        [_tcpManager connectWithSeverType:IXTCPServerTypeTrade];
    }
}

//退出
- (void)logoutWithParam:(proto_user_logout *)param
{
    if(_tradeStatus==IXTCPConnectStatusConnected){
        NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_USER_LOGOUT];
        if(data){
            [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
        }
    }else{
        self.holdCmd = CMD_USER_LOGOUT;
        self.holdParam = param;
        [_tcpManager connectWithSeverType:IXTCPServerTypeTrade];
    }
}

//注册
- (void)registWithParam:(proto_user_add *)param
{
    if(_tradeStatus==IXTCPConnectStatusConnected){
        NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_USER_ADD];
        if(data){
            [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
        }
    }else{
        self.holdCmd = CMD_USER_ADD;
        self.holdParam = param;
        [_tcpManager connectWithSeverType:IXTCPServerTypeTrade];
    }
}

//切换用户账号
- (void)swiAccountWithParam:(proto_user_login_account *)param
{
    //proto_user_login_account
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_USER_LOGIN_ACCOUNT];
    if(data){
        [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
    }
}

//修改用户信息
- (void)modifyUserInfoWithParam:(proto_user_update*)param
{
    
}

//删除用户
- (void)deleteUserWithParam:(proto_user_delete *)param
{
    
}

//用户登出
- (void)logOutWithParam:(proto_user_logout *)praram
{
    
}

//获取用户
- (void)getUserInfoWithParam:(proto_user_get *)param
{
    
}

//获取用户列表
- (void)getUserListWithParam:(proto_user_list *)param
{
    
}

- (void)addSubSymbolWithParam:(proto_symbol_sub_add *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_SYMBOL_SUB_ADD];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

- (void)delSubSymbolWithParam:(proto_symbol_sub_delete *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_SYMBOL_SUB_DELETE];
    [_tcpManager sendData:data  severType:IXTCPServerTypeTrade];
}

#pragma mark 开户
- (void)openAccountWithParam:(proto_account_add *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ACCOUNT_ADD];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 账户查询
- (void)accountQueryWithParam:(proto_account_balance_get *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ACCOUNTBALANCE_GET];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 下单
- (void)orderWithParam:(proto_order_add *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ORDER_ADD];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 批量下单
- (void)batchOrderWithParam:(proto_order_add_batch *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ORDER_ADD_BATCH];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 更新订单
- (void)updateOrderWithParam:(proto_order_update *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ORDER_UPDATE];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 订单取消
- (void)orderCancelWithParam:(proto_order_cancel *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ORDER_CANCEL];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 更新仓位
- (void)updatePositionWithParam:(proto_position_update *)param
{
    
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_POSITION_UPDATE];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

- (void)forceCloseWithParam:(proto_position_close *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_POSITION_FORCE_CLOSE];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 交易明细
- (void)getOrderInfoWithParam:(proto_order_get *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ORDER_GET];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 持仓历史
- (void)getPositionHistoryWithParam:(proto_position_history_list *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_POSITION_SUMMARY];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 账户出入金
- (void)accountBalanceWithParam:(proto_account_balance_add *)param
{
    NSData *data = [IXTradeDataFilter packetWithPBMsg:param Command:CMD_ACCOUNTBALANCE_ADD];
    [_tcpManager sendData:data severType:IXTCPServerTypeTrade];
}

#pragma mark 行情服务器
- (void)subscribeDynPriceWithSymbolAry:(NSArray *)symbolArr
{
    [self subscribePriceWithSymbolAry:symbolArr WithCmdType:CMD_QUOTE_SUB];
}

- (void)unSubQuotePriceWithSymbolAry:(NSArray *)symbolArr
{
    NSData *sendData = [IXQuoteDataFilter loadQuotePriceWithSymbolList:symbolArr
                                                           WithCmdType:CMD_QUOTE_UNSUB];
    if ( sendData ) {
        [_tcpManager sendData:sendData severType:IXTCPServerTypeQuote];
    }
}


- (void)subscribeDetailPriceWithSymbolAry:(NSArray *)symbolArr
{
    [self subscribePriceWithSymbolAry:symbolArr WithCmdType:CMD_QUOTE_SUB_DETAIL];
    
}

- (void)unSubscribeDetailPriceWithSymbolAry:(NSArray *)symbolArr
{
    NSData *sendData = [IXQuoteDataFilter loadQuotePriceWithSymbolList:symbolArr
                                                           WithCmdType:CMD_QUOTE_UNSUB_DETAIL];
    if ( sendData ) {
        [_tcpManager sendData:sendData severType:IXTCPServerTypeQuote];
    }
}


- (void)subscribePriceWithSymbolAry:(NSArray *)symbolArr WithCmdType:(PROTO_QUOTE_COMMAND)type
{
    NSData *sendData = [IXQuoteDataFilter loadQuotePriceWithSymbolList:symbolArr WithCmdType:type];
    if ( sendData ) {
        [_tcpManager sendData:sendData severType:IXTCPServerTypeQuote];
    }
}

- (void)getLastClostPriceWithSymbolAry:(NSArray *)symbolArr
{
    NSData *sendData = [IXQuoteDataFilter loadLastCloseWithSymbolList:symbolArr];
    if ( sendData ) {
        [_tcpManager sendData:sendData severType:IXTCPServerTypeQuote];
    }
}

#pragma mark -
#pragma mark - chart

- (void)subscribeChartDataWithParam:(proto_quote_kdata_req *)param;
{
    NSData *sendData = [IXQuoteDataFilter loadKDataWithParam:param];
    [_tcpManager sendData:sendData severType:IXTCPServerTypeQuote];
}

@end
