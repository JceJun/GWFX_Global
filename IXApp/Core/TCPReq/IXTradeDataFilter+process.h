//
//  IXTradeDataFilter+process.h
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXTradeDataFilter.h"

@class GPBMessage;

@interface IXTradeDataFilter (process)

/**
 *  封装数据包
 *
 *  @param proto   proto对象
 *  @param command proto协议
 *
 *  @return 封装后的数据包
 */
+ (NSData *)packetWithPBMsg:(GPBMessage *)proto Command:(uint16)command;

/**
 *  解析数据包
 *
 *  @param data 解析数据流
 *
 *  @return 解析后的信息
 */
+ (NSDictionary *)unpacketData:(NSData *)data;

/**
 *  bodyData 转化成 PB对象
 *
 *  @param bodyData body字节流
 *  @param cmd cmd
 *
 *  @return 转化后的PB对象
 */
+ (id)dataConvert2PBObj:(NSData *)bodyData cmd:(uint16)cmd;


@end
