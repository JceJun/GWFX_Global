//
//  IXQuoteDataFilter+process.h
//  IXApp
//
//  Created by Magee on 16/11/24.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXQuoteDataFilter.h"

@class proto_quote_kdata_req;
@interface IXQuoteDataFilter (process)

#pragma mark - req

+ (NSData *)loadQuotePriceWithSymbolList:(NSArray *)symbolArr
                             WithCmdType:(PROTO_QUOTE_COMMAND)qCom;
+ (NSData *)loadLastCloseWithSymbolList:(NSArray *)symbolArr;

+ (NSData *)loadKDataWithParam:(proto_quote_kdata_req *)param;

#pragma mark - rsp

+ (void)proto_quote_login_rsp:(id)obj;
+ (NSMutableArray *)proto_quote_pub:(id)obj;
+ (NSMutableArray *)proto_quote_sub_rsp:(id)obj;
+ (NSMutableArray *)proto_quote_close_price_rsp:(id)obj;
+ (NSMutableArray *)proto_quote_kdata_rsp:(id)obj;

#pragma mark - others

+ (NSDictionary *)unpacketData:(NSData *)data;
+ (id)dataConvert2PBObj:(NSData *)bodyData cmd:(uint16)cmd;

@end
