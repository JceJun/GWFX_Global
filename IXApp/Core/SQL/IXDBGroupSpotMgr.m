//
//  IXDBGroupSpotMgr.m
//  IXApp
//
//  Created by Evn on 2018/3/4.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDBGroupSpotMgr.h"
#import "IXDBModel.h"

@implementation IXDBGroupSpotMgr

#pragma mark   保存GroupSpot
+ (BOOL)saveGroupSpotInfo:(id)model
{
    return YES;
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
//    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_GROUP_SPOT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存GroupSpot信息
+ (BOOL)saveBatchGroupSpotInfos:(NSArray *)modelArr
{
    return YES;
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
//    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_GROUP_SPOT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark 删除GroupSpot信息
+ (BOOL)deleteGroupSpotInfo:(id)model
{
    return YES;
//    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_GROUP_SPOT_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  通过账户组ID查询GroupSpot信息
+ (NSArray *)queryGroupSpotInfoByGroupId:(uint64_t)groupId
{
    return [NSArray array];
//    NSDictionary *condDic = @{
//                              kGroupId:@(groupId)
//                              };
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
//    return  [IXDBModel dealWithDataCheck:[item_group_spot new] protocolCommand:CMD_GROUP_SPOT_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark  通过货币类型查询GroupSpot信息
+ (NSDictionary *)queryGroupSpotInfoByType:(uint32)type
{
    return [NSDictionary dictionary];
//    NSDictionary *condDic = @{
//                              kType:@(type)
//                              };
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
//    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_group_spot new] protocolCommand:CMD_GROUP_SPOT_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1A];
//    if (allArr && allArr.count > 0) {
//        return allArr[0];
//    } else {
//        return nil;
//    }
}

#pragma mark  通过货币名称查询GroupSpot信息
+ (NSDictionary *)queryGroupSpotInfoByName:(NSString *)name
{
    return [NSDictionary dictionary];
//    if (!name) {
//        return nil;
//    }
//    NSDictionary *condDic = @{
//                              kName:name
//                              };
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
//    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_group_spot new] protocolCommand:CMD_GROUP_SPOT_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1B];
//    if (allArr && allArr.count > 0) {
//        return allArr[0];
//    } else {
//        return nil;
//    }
}

#pragma mark  查询GroupSpotUUID最大值
+ (NSDictionary *)queryGroupSpotUUIDMax
{
    return [NSDictionary dictionary];
//    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_GROUP_SPOT_LIST userId:0];
}

#pragma mark  清空表数据
+ (BOOL)deleteGroupSpotInfos
{
    return YES;
//    return [IXDBModel deleteDBTable:kTableNameGroupSpot];
}

@end
