//
//  IXDataBaseAccountMgr.m
//  IXApp
//
//  Created by ixiOSDev on 16/11/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBAccountMgr.h"
#import "IXDBModel.h"


@implementation IXDBAccountMgr


#pragma mark   保存账户信息
+ (BOOL)saveAccountInfo:(id)model userId:(uint64_t)userId
{
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:userId];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_ACCOUNT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:userId conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存账户信息
+ (BOOL)saveBatchAccountInfos:(NSArray *)modelArr userId:(uint64_t)userId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:userId];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_ACCOUNT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:userId conditionType:ConditionQueryTypeValue0];
    
}

#pragma mark  查询账户信息
+ (NSArray *)queryAllAccountInfoByUserId:(uint64_t)userId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:userId];
    return  [IXDBModel dealWithDataCheck:[item_account new] protocolCommand:CMD_ACCOUNT_LIST sqlIndexKey:indexDic queryCond:nil userId:userId conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询账户信息(大账户不关联用户)
+ (NSArray *)queryAllMt4LicAccountInfoByClientType:(int32_t)clientType
{
    NSDictionary *dic = @{
                          kClientType:@(clientType)
                          };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_account new] protocolCommand:CMD_ACCOUNT_LIST sqlIndexKey:indexDic queryCond:dic userId:0 conditionType:ConditionQueryTypeValue1B];
}

+ (NSArray *)queryAllAccountInfoByUserId:(uint64_t)userId
                              clientType:(int32_t)clientType
{
    NSDictionary *dic = @{
                          kClientType:@(clientType)
                          };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:userId];
    return  [IXDBModel dealWithDataCheck:[item_account new] protocolCommand:CMD_ACCOUNT_LIST sqlIndexKey:indexDic queryCond:dic userId:userId conditionType:ConditionQueryTypeValue1A];
}

+ (NSDictionary *)queryAccountInfoByAccountId:(uint64_t)accountId {
    
    NSDictionary *dic = @{
                          kID:@(accountId)
                          };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_account new] protocolCommand:CMD_ACCOUNT_LIST sqlIndexKey:indexDic queryCond:dic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询用户账户UUID最大值
+ (NSDictionary *)queryAccountUUIDMaxByUserId:(uint64_t)userId {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_ACCOUNT_LIST userId:userId];
}

@end
