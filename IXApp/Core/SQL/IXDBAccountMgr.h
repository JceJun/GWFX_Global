//
//  IXDataBaseAccountMgr.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBAccountMgr : NSObject

/**
 *   保存账户信息
 *
 *  @param model protocalModel
 *  @param userId 用户ID
 *
 */
+ (BOOL)saveAccountInfo:(id)model userId:(uint64_t)userId;

/**
 *  批量保存账户信息
 *
 *  @param modelArr protocalModel集合
 *  @param userId 用户ID
 *
 */
+ (BOOL)saveBatchAccountInfos:(NSArray *)modelArr userId:(uint64_t)userId;
/**
 *  查询账户信息
 *  @param userId 用户ID
 *
 */
+ (NSArray *)queryAllAccountInfoByUserId:(uint64_t)userId;

/**
 *  查询账户信息(大账户不关联用户)
 *  @param clientType 客户端类型
 *
 */
+ (NSArray *)queryAllMt4LicAccountInfoByClientType:(int32_t)clientType;

/**
 *  查询账户信息
 *  @param userId 用户ID
 *  @param clientType 客户端类型
 *
 */
+ (NSArray *)queryAllAccountInfoByUserId:(uint64_t)userId
                              clientType:(int32_t)clientType;


/**
 *  查询账户信息
 *  @param accountId 账户ID
 *
 */
+ (NSDictionary *)queryAccountInfoByAccountId:(uint64_t)accountId;

/**
 *  查询用户账户UUID最大值
 *  @param userId 用户ID
 *
 */
+ (NSDictionary *)queryAccountUUIDMaxByUserId:(uint64_t)userId;

@end
