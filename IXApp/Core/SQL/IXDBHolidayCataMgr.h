//
//  IXDBHolidayCataMgr.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBHolidayCataMgr : NSObject

/**
 *  保存假期分类
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveHolidayCataInfo:(id)model;

/**
 *  批量保存假期分类信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchHolidayCatasInfos:(NSArray *)modelArr;

/**
 *  查询假期分类信息
 *
 */
+ (NSArray *)queryHolidayAllCatas;

/**
 *  根据假期分类ID查询假期信息
 *  @param holidayCataId 假期分类ID
 *
 */
+ (NSDictionary *)queryHolidayCataByHolidayCataId:(uint64_t)holidayCataId;

/**
 *  查询假期分类UUID最大值
 *
 */
+ (NSDictionary *)queryHolidayCatasUUIDMax;

/**
 *  删除假期分类
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteHolidayCata:(id)model;

@end
