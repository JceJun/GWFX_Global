//
//  IXDBAccountGroupSymCataMgr.h
//  IXApp
//
//  Created by Evn on 17/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBAccountGroupSymCataMgr : NSObject

/**
 *  保存账户组产品分类信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveAccountGroupSymCataInfo:(id)model;

/**
 *  批量保存账户组产品分类信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchAccountGroupSymCataInfos:(NSArray *)modelArr;

/**
 *  查询账户组产品分类信息
 *  @param accGroupId 账户组ID
 *
 */
+ (NSArray *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId;

/**
 *  查询账户组产品分类信息
 *  @param accGroupId 账户组ID
 *  @param type 权限类型
 *
 */
+ (NSArray *)queryAccountGroupSymCataBySymbolIdAccountGroupId:(uint64_t)accGroupId type:(uint64_t)type;

/**
 *  查询账户组产品分类信息
 *  @param accGroupId 账户组ID
 *  @param type 权限类型
 *  @param symbolCata 产品分类ID
 *
 */
+ (NSArray *)queryAccountGroupSymCataBySymbolCataIdAccountGroupId:(uint64_t)accGroupId type:(uint64_t)type;

/**
 *  查询账户组产品分类信息
 *  @param accGroupId 账户组ID
 *  @param symbolId 产品ID
 *
 */
+ (NSDictionary *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId symbolId:(uint64_t)symbolId;

/**
 *  查询账户组产品分类信息
 *  @param accGroupId 账户组ID
 *  @param symbolId 产品ID
 *  @param type 权限类型
 *
 */
+ (NSDictionary *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId
                                                  symbolId:(uint64_t)symbolId
                                                      type:(uint64_t)type;


/**
 *  查询账户组产品分类信息
 *  @param accGroupId 账户组ID
 *  @param symbolId 产品ID
 *
 */
+ (NSDictionary *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId symbolCataId:(uint64_t)symbolCataId;

/**
 *  查询账户组信息
 *
 */
+ (NSArray *)queryAllAccountGroupSymCata;

/**
 *  查询账户组UUID最大值数据
 *
 */
+ (NSDictionary *)queryAccountGroupSymCataUUIDMax;

/**
 *  查询账户组UUID最大值数据
 *  @param infoDic 附加信息
 *
 */
+ (NSDictionary *)queryAccountGroupSymCataUUIDMaxByAddInfo:(NSDictionary *)infoDic;

/**
 *  删除账户组产品分类信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteAccountGroupSymCata:(id)model;

@end
