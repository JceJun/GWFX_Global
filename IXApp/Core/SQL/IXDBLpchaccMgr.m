//
//  IXDBLpchaccMgr.m
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBLpchaccMgr.h"
#import "IXDBModel.h"

@implementation IXDBLpchaccMgr

#pragma mark 保存LPChannelAccount信息
+ (BOOL)saveLpchaccInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存LPChannelAccount信息
+ (BOOL)saveBatchLpchaccInfos:(NSArray *)modelArr
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询LPChannelAccount
+ (NSDictionary *)queryLpchaccByLpchaccid:(uint64_t)lpchaccid
                              lpCompanyid:(uint64_t)lpCompanyid
{
    NSDictionary *condDic = @{
                              kID:@(lpchaccid),
                              kLpCompanyid:@(lpCompanyid),
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_lpchacc new] protocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        return nil;
    }
}


#pragma mark  查询LPChannelAccountUUID最大值数据
+ (NSDictionary *)queryLpchaccUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST userId:0];
}

#pragma mark  删除LPChannelAccount信息
+ (BOOL)deleteLpchacc:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_LPCHANNEL_ACCOUNT_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
