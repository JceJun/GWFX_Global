//
//  IXDataBaseManager.h
//  IXFMDB
//
//  Created by ixiOSDev on 16/11/1.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "IXDBGlobal.h"

@interface IXDBManager : NSObject

+ (id)sharedInstance;

/** 初始化数据库 */
- (BOOL)initDataBaseDataName:(NSString *)dbName;

/** 是否存在数据库 */
- (BOOL)isExistDataBase:(NSString *)dbName;

/** 创建数据表 */
- (BOOL)createDataTableSql:(NSString *)data
                 tableName:(NSString *)tableName;

- (BOOL)createDataTableSql:(NSString *)data
                 protocolCommand:(uint16)cmd;

/* 删除数据库 */
- (BOOL)deleteDataBaseDataName:(NSString *)dbName;

//删除表
- (BOOL)deleteTableTableName:(NSString *)tableName;

/** 保存单个数据 */
- (BOOL)save:(NSString *)data;

/** 批量保存数据 */
- (BOOL)saveObjects:(NSArray *)dataArr;
//
/** 更新单个数据 */
- (BOOL)update:(NSString *)data;
//
/** 批量更新数据*/
- (BOOL)updateObjects:(NSArray *)dataArr;
//
/** 删除单个数据 */
- (BOOL)deleteObject:(NSString *)data;
//
/** 批量删除数据 */
- (BOOL)deleteObjects:(NSArray *)dataArr;
//
/** 通过条件删除数据 */
- (BOOL)deleteObjectsByAddtion:(NSString *)data;

/** 通过条件查找数据 */
- (NSArray *)checkObjects:(NSString *)data
                indexKeys:(NSDictionary *)indexDic;

@end
