//
//  IXDataBaseModel.m
//  IXFMDB
//
//  Created by ixiOSDev on 16/11/1.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#import "IXDBModel.h"
#import "IXDBManager.h"
#import "IXCpyConfig.h"

@implementation IXDBModel

#pragma mark 创建表sql语句
+ (NSString *)tableNameToSql:(NSString *)tableName
                 sqlIndexKey:(NSDictionary *)indexDic
                      userId:(uint64_t)userId
{
    NSArray *allKeys = [indexDic allKeys];
    NSString *indexStr = [[NSString alloc]init];
    BOOL flag = NO;
    int index = 0;
    for (NSString *key in allKeys) {
        if (![key isEqualToString:kID]) {
            indexStr = [indexStr stringByAppendingFormat:@",%@ %@",key,indexDic[key]];
            index++;
        }
    }
    if (index == allKeys.count) {
        flag = YES;
    }
    if (flag) {
        return [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT %@)",tableName,indexStr];
    } else {
        return [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY %@)",tableName,indexStr];
    }
}

+ (NSString *)tableNameToSqlProtocolCommand:(uint16)cmd
                                sqlIndexKey:(NSDictionary *)indexDic
                                     userId:(uint64_t)userId
{
    return [IXDBModel tableNameToSql:[IXDBModel protocolCommandToTableName:cmd] sqlIndexKey:indexDic userId:userId];
}

#pragma mark 执行sql语句
+ (NSString *)sqlModelToSql:(id)model
                  tableName:(NSString *)tableName
                    additon:(NSDictionary *)addDic
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
              conditionType:(ConditionQueryType)condType
{
    return [IXDBModel baseSqlModelToSql:model tableName:tableName additon:addDic sqlIndexKey:indexDic dataBaseOperateType:type userId:userId conditionType:condType];
}

+ (NSString *)sqlModelToSql:(id)model
            protocolCommand:(uint16)cmd
                    additon:(NSDictionary *)addDic
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
              conditionType:(ConditionQueryType)condType
{
    return [IXDBModel sqlModelToSql:model tableName:[IXDBModel protocolCommandToTableName:cmd] additon:addDic sqlIndexKey:indexDic dataBaseOperateType:type userId:userId conditionType:condType];
}

#pragma mark 执行自定义参数sql语句

#pragma mark 执行条件sql语句
+ (NSString *)sqlCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                          userId:(uint64_t)userId
{
    return [NSString stringWithFormat:@"select * from %@ where %@ = (select max(%@) from %@)",[self protocolCommandToTableName:cmd],kUUID,kUUID,[self protocolCommandToTableName:cmd]];
}

+ (NSString *)sqlCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                         addInfo:(NSDictionary *)infoDic
{
    if (infoDic && infoDic.count > 0) {
        NSArray *allKeys = [infoDic allKeys];
        switch (cmd) {
            case CMD_ACCOUNTGROUPSYMBOLCATA_LIST:{
                 return [NSString stringWithFormat:@"select * from %@ where %@ = (select max(%@) from %@ where %@ = '%@') and %@ = '%@'",[self protocolCommandToTableName:cmd],kUUID,kUUID,[self protocolCommandToTableName:cmd],allKeys[0],infoDic[allKeys[0]],allKeys[0],infoDic[allKeys[0]]];
            }
                break;
                
            default:{
                return [NSString stringWithFormat:@"select * from %@ where %@ = (select max(%@) from %@) and %@ = '%@'",[self protocolCommandToTableName:cmd],kUUID,kUUID,[self protocolCommandToTableName:cmd],allKeys[0],infoDic[allKeys[0]]];
            }
                break;
        }
    } else {
        return [NSString stringWithFormat:@"select * from %@ where %@ = (select max(%@) from %@)",[self protocolCommandToTableName:cmd],kUUID,kUUID,[self protocolCommandToTableName:cmd]];
    }
}

#pragma mark 执行sql集合语句

+ (NSArray *)sqlModelToSqls:(NSArray *)modelArr
                  tableName:(NSString *)tableName
               additonInfos:(NSArray *)addArr
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
              conditionType:(ConditionQueryType)condType
{
    NSMutableArray *retArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < modelArr.count; i++) {
        id model = modelArr[i];
        NSDictionary *addDic = addArr[i];
        NSString *sql = [self sqlModelToSql:model tableName:tableName additon:addDic sqlIndexKey:indexDic dataBaseOperateType:type userId:userId conditionType:condType];
        if (sql && sql.length) {
            [retArr addObject:sql];
        }
    }
    return retArr;
}

#pragma mark 执行自定义sql集合语句
+ (NSArray *)sqlSelfDefineToSqls:(NSArray *)modelArr
                 protocolCommand:(uint16)cmd
                    additonInfos:(NSArray *)addArr
                     sqlIndexKey:(NSDictionary *)indexDic
             dataBaseOperateType:(DataBaseOperateType)type
                          userId:(uint64_t)userId
                       accountId:(uint64_t)accountId
                   conditionType:(ConditionQueryType)condType
{
    NSMutableArray *retArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < modelArr.count; i++) {
        NSDictionary *infoDic = modelArr[i];
        if (indexDic && indexDic.count > 0) {
            NSString *sql = [IXDBModel baseSelfDefineDataToSql:infoDic protocolCommand:cmd sqlIndexKey:indexDic dataBaseOperateType:type conditionType:condType userId:userId accountId:accountId];
            if (sql && sql.length) {
                [retArr addObject:sql];
            }
        } else {
            continue;
        }
    }
    return retArr;
}

+ (NSArray *)sqlModelToSqls:(NSArray *)modelArr
            protocolCommand:(uint16)cmd
               additonInfos:(NSArray *)addArr
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
              conditionType:(ConditionQueryType)condType
{
    return [IXDBModel sqlModelToSqls:modelArr tableName:[IXDBModel protocolCommandToTableName:cmd] additonInfos:addArr sqlIndexKey:indexDic dataBaseOperateType:type userId:userId conditionType:condType];
}

+ (NSArray *)sqlModelToSqls:(NSArray *)modelArr
            protocolCommand:(uint16)cmd
               additonInfos:(NSArray *)addArr
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
{
    return nil;
}

#pragma mark 处理参数转sql
+ (NSString *)baseSqlModelToSql:(id)model
                      tableName:(NSString *)tableName
                        additon:(NSDictionary *)addDic
                    sqlIndexKey:(NSDictionary *)indexDic
            dataBaseOperateType:(DataBaseOperateType)type
                         userId:(uint64_t)userId
                  conditionType:(ConditionQueryType)condType
{
    NSString *keyStr = [[NSString alloc]init];
    NSString *valueStr = [[NSString alloc]init];
    if (type == DataBaseOperateTypeSave) {
        NSDictionary *dataDic = [IXDBModel groupSqlKeysValuesSqlModel:model sqlIndexKey:indexDic userId:userId];
        if (dataDic) {
            keyStr = dataDic[kSqlIndexKey];
            valueStr = dataDic[kSqlValue];
        }
    }
    switch (type) {
        case DataBaseOperateTypeSave:
            return  [NSString stringWithFormat:@"replace into %@ (%@) values(%@)",tableName,keyStr,valueStr];
            break;
        case DataBaseOperateTypeDelete: {
            int id_p = -1;
            if ([model isKindOfClass:[item_user class]]) {
                id_p = (int)((item_user *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_symbol_cata class]]) {
                id_p = (int)((item_symbol_cata *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_symbol class]]) {
                id_p = (int)((item_symbol *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_account class]]) {
                id_p = (int)((item_account *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_symbol_sub class]]) {
                id_p = (int)((item_symbol_sub *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d' and %@ = '%lld'",tableName,kID,id_p,kUserId,userId];
            } else if ([model isKindOfClass:[item_quote_delay class]]) {
               item_quote_delay *tmpModel = (item_quote_delay *)model;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%lld' and %@ = '%lld'",tableName,kUserId,tmpModel.userid,KSymbolCataId,tmpModel.symbolCataid];
            } else if ([model isKindOfClass:[item_account_group class]]) {
                id_p = (int)((item_account_group *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_account_group_symbol_cata class]]) {
                id_p = (int)((item_account_group_symbol_cata *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_holiday_cata class]]) {
                id_p = (int)((item_holiday_cata *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_holiday class]]) {
                id_p = (int)((item_holiday *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_schedule_cata class]]) {
                id_p = (int)((item_schedule_cata *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_schedule class]]) {
                id_p = (int)((item_schedule *)model).id_p;
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_eod_time class]]) {
                id_p = (int)((item_eod_time *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_symbol_label class]]) {
                id_p = (int)((item_symbol_label *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_symbol_cata class]]) {
                id_p = (int)((item_symbol_cata *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_symbol_margin_set class]]) {
                id_p = (int)((item_symbol_margin_set *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_holiday_margin class]]) {
                id_p = (int)((item_holiday_margin *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_company class]]) {
                id_p = (int)((item_company *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_schedule_margin class]]) {
                id_p = (int)((item_schedule_margin *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_secure_dev class]]) {
                id_p = (int)((item_secure_dev *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d' and %@ = '%lld'",tableName,kID,id_p,kUserId,userId];
            } else if ([model isKindOfClass:[item_group_symbol class]]) {
                id_p = (int)((item_group_symbol *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_group_symbol_cata class]]) {
                id_p = (int)((item_group_symbol_cata *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_lpchacc class]]) {
                id_p = (int)((item_lpchacc *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_lpchacc_symbol class]]) {
                id_p = (int)((item_lpchacc_symbol *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_lpchannel class]]) {
                id_p = (int)((item_lpchannel *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_lpchannel_symbol class]]) {
                id_p = (int)((item_lpchannel_symbol *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            } else if ([model isKindOfClass:[item_lpib_bind class]]) {
                id_p = (int)((item_lpib_bind *)model).id_p;
                return [NSString stringWithFormat:@"delete from %@ where %@ = '%d'",tableName,kID,id_p];
            }
        }
            break;
        case DataBaseOperateTypeQuery: {
            if ([model isKindOfClass:[item_user class]]) {
                return  [NSString stringWithFormat:@"select distinct * from %@",tableName];
            } else if ([model isKindOfClass:[item_symbol_cata class]]) {
                if (addDic.count > 0) {
                    NSArray *allKey = [addDic allKeys];
                    if (condType == ConditionQueryTypeValue1) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0'and %@ = '%@' order by %@",tableName,kState,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],kSequence];
                    } else if (condType == ConditionQueryTypeValue2) {
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.%@ = '0'and %@.%@ = '%@' and %@.%@ = %@.%@ and %@.%@ = %@ order by %@",tableName,tableName,kTableNameGroupSymCata,tableName,kState,tableName,kparentId,addDic[kparentId],tableName,kID,kTableNameGroupSymCata,kID,kTableNameGroupSymCata,kGroupId,addDic[kAccGroupId],kSequence];
                    }
                } else {
                    return  [NSString stringWithFormat:@"select distinct * from %@  where %@ = '0' order by %@",tableName,kState,kSequence];
                }
            } else if ([model isKindOfClass:[item_symbol class]]) {
                switch (condType) {
                    case ConditionQueryTypeValue0:
                        return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '1' order by %@",tableName,kState,kEnable,kSequence];
                        break;
                    case ConditionQueryTypeValue1: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count) {
                            return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '1' and %@ = '%@'",tableName,kState,kEnable,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]]];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue2: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count) {
                            return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '1' and %@ = '%@' and %@ = '%@'",tableName,kState,kEnable,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],allKey[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[1]]]];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue2A: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count) {
                            return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '1' and %@ = '%@' and %@ = '%@'",tableName,kState,kEnable,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],allKey[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[1]]]];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue3:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '1' and %@ = '%@' order by %@ limit '%@','%@'",tableName,kState,kEnable,kCataId,addDic[kCataId],kSequence,addDic[kStartIndex],addDic[kOffset]];
                        break;
                    case ConditionQueryTypeValue3A:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@,%@ where %@.%@ = '0' and %@.%@ = '1' and (%@ like '%%%@%%' or %@ like '%%%@%%' or %@ like '%%%@%%') and ((%@.%@ = %@.%@ and %@.%@ != %@.%@ and %@.%@ = '%@') or (%@.%@ = %@.%@ and %@.%@ = %@.%@ and %@.%@ = '%@')) and %@.%@ = 0 order by %@",tableName,tableName,kTableNameGroupSym,kTableNameGroupSymCata,tableName,kState,tableName,kEnable,kName,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kPinYin,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kSource,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kTableNameSymbol,kCataId,kTableNameGroupSymCata,kID,kTableNameSymbol,kCataId,kTableNameGroupSym,kCataId,kTableNameGroupSymCata,kGroupId,addDic[kGroupId],kTableNameSymbol,kCataId,kTableNameGroupSym,kCataId,kTableNameSymbol,kID,kTableNameGroupSym,kID,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly,kSequence];
                        break;
                    case ConditionQueryTypeValue3B:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.%@ = '0' and %@.%@ = '1' and (%@ like '%%%@%%' or %@ like '%%%@%%' or %@ like '%%%@%%' or %@.%@ in (select %@.%@ from %@ where %@.%@ like '%%%@%%')) and (%@.%@ = %@.%@ and %@.%@ = '%@') and %@.%@ = 0 order by %@",tableName,tableName,kTableNameGroupSym,tableName,kState,tableName,kEnable,kName,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kPinYin,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kSource,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],tableName,kName,kTableNameLanguage,kNameSpace,kTableNameLanguage,kTableNameLanguage,kValue,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],tableName,kID,kTableNameGroupSym,kID,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly,kSequence];
                        break;
                    case ConditionQueryTypeValue4:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.%@ = '0' and %@.%@ = '1' and %@.%@ = '%@' and %@.%@ = %@.%@ and %@.%@ = %@.%@ and %@.%@ = '%@' and %@.%@ = 0 order by %@.%@ limit '%@','%@'",tableName,tableName,kTableNameGroupSym,tableName,kState,tableName,kEnable,tableName,kCataId,addDic[kCataId],tableName,kID,kTableNameGroupSym,kID,tableName,kCataId,kTableNameGroupSym,kCataId,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly,tableName,kSequence,addDic[kStartIndex],addDic[kOffset]];
                        break;
                    case ConditionQueryTypeOther:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@.%@ = '0' and %@.%@ = '1' and (%@ like '%%%@%%' or %@ like '%%%@%%' or %@ like '%%%@%%' or %@.%@ in (select %@.%@ from %@ where %@.%@ like '%%%@%%')) order by %@",tableName,tableName,kState,tableName,kEnable,kName,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kPinYin,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kSource,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],tableName,kName,kTableNameLanguage,kNameSpace,kTableNameLanguage,kTableNameLanguage,kValue,[IXDataProcessTools dealWithSqlSingleQuote:addDic[kValue]],kSequence];
                        break;
                    default:
                        break;
                }
            } else if ([model isKindOfClass:[item_account class]]) {
                switch (condType) {
                    case ConditionQueryTypeValue0:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%lld' and  %@ = '0'and %@ = '1' order by %@",tableName,kUserId,userId,kState,kEnable,kType];
                        break;
                    case ConditionQueryTypeValue1:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and  %@ = '0'and %@ = '1' order by %@",tableName,kID,addDic[kID],kState,kEnable,kType];
                        break;
                    case ConditionQueryTypeValue1A:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%lld' and %@ = '%@' and  %@ = '0' and %@ = '1' order by %@",tableName,kUserId,userId,kClientType,addDic[kClientType],kState,kEnable,kType];
                        break;
                    case ConditionQueryTypeValue1B:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and  %@ = '0' and %@ = '1' order by %@",tableName,kClientType,addDic[kClientType],kState,kEnable,kType];
                        break;
                    default:
                        break;
                }
            } else if ([model isKindOfClass:[item_account_group class]]) {
                switch (condType) {
                    case ConditionQueryTypeValue0:
                        return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '1'",tableName,kState,kEnable];
                        break;
                    case ConditionQueryTypeValue1:
                        return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and  %@ = '0' and %@ = '1' and %@ = '1'",tableName,kClientType,addDic[kClientType],kState,kEnable,kDefaultType];
                        break;
                    case ConditionQueryTypeValue1A: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count) {
                            return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0' and %@ = '1'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],kState,kEnable];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue2A: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count) {
                            return [NSString stringWithFormat:@"select distinct * from %@ where  %@ = %@ and  %@ = %@ and %@ = '0' and %@ = '1'",tableName,kClientType,addDic[kClientType],kType,addDic[kType],kState,kEnable];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue2B: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count) {
                            return [NSString stringWithFormat:@"select distinct * from %@ where  %@ = %@ and  %@ != %@ and %@ = '0' and %@ = '1' and %@ = '1'",tableName,kClientType,addDic[kClientType],kType,addDic[kType],kState,kEnable,kDefaultType];
                        }
                    }
                        break;
                    default:
                        break;
                }
            } else if ([model isKindOfClass:[item_symbol_hot class]]){
                switch (condType) {
                    case ConditionQueryTypeValue0:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.'%@' = %@.'%@' and %@.'%@' = 1 and %@.'%@' = 0 and %@.'%@' = '0'",kTableNameSymbol,tableName,kTableNameSymbol,tableName,kSymbolId,kTableNameSymbol,kID,kTableNameSymbol,kEnable,kTableNameSymbol,kState,tableName,kState];
                        break;
                    case ConditionQueryTypeValue1A:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@,%@,%@ where %@.'%@' = %@.'%@' and %@.'%@' = 1 and %@.'%@' = 0 and %@.'%@' = '0' and ((%@.%@ = %@.%@ and %@.%@ != %@.%@ and %@.%@ = '%@') or (%@.%@ = %@.%@ and %@.%@ = %@.%@ and %@.%@ = '%@')) and %@.%@ = 0",kTableNameSymbol,tableName,kTableNameSymbol,kTableNameGroupSym,kTableNameGroupSymCata,tableName,kSymbolId,kTableNameSymbol,kID,kTableNameSymbol,kEnable,kTableNameSymbol,kState,tableName,kState,kTableNameSymbol,kCataId,kTableNameGroupSymCata,kID,kTableNameSymbol,kCataId,kTableNameGroupSym,kCataId,kTableNameGroupSymCata,kGroupId,addDic[kGroupId],kTableNameSymbol,kCataId,kTableNameGroupSym,kCataId,kTableNameSymbol,kID,kTableNameGroupSym,kID,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly];
                        break;
                    case ConditionQueryTypeValue1B:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@,%@ where %@.'%@' = %@.'%@' and %@.'%@' = 1 and %@.'%@' = 0 and %@.'%@' = '0' and %@.%@ = %@.%@ and %@.%@ = '%@' and %@.%@ = 0",kTableNameSymbol,tableName,kTableNameSymbol,kTableNameGroupSym,tableName,kSymbolId,kTableNameSymbol,kID,kTableNameSymbol,kEnable,kTableNameSymbol,kState,tableName,kState,tableName,kSymbolId,kTableNameGroupSym,kID,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly];
                        break;
                    case ConditionQueryTypeValue1C:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@,%@,%@ where %@.'%@' = %@.'%@' and %@.'%@' = 1 and %@.'%@' = 0 and %@.'%@' = '0' and %@.%@ = %@.%@ and %@.%@ != %@.%@ and %@.%@ = '%@'and %@.%@ = 0",kTableNameSymbol,tableName,kTableNameSymbol,kTableNameGroupSym,kTableNameGroupSymCata,tableName,kSymbolId,kTableNameSymbol,kID,kTableNameSymbol,kEnable,kTableNameSymbol,kState,tableName,kState,kTableNameSymbol,kCataId,kTableNameGroupSym,kCataId,kTableNameSymbol,kID,kTableNameGroupSym,kID,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly];
                        break;
                    case ConditionQueryTypeValue2: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count > 1) {
                            
                            return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.'%@' = %@.'%@' and %@.'%@' = 1 and %@.'%@' = 0 and %@.'%@' = '0' limit '%@','%@'",kTableNameSymbol,tableName,kTableNameSymbol,tableName,kSymbolId,kTableNameSymbol,kID,kTableNameSymbol,kEnable,kTableNameSymbol,kState,tableName,kState,addDic[kStartIndex],addDic[kOffset]];
                        }
                    }
                        break;
                    default:
                        break;
                }
            } else if ([model isKindOfClass:[item_account_group_symbol_cata class]]) {
                switch (condType) {
                    case ConditionQueryTypeValue1: {
                        NSArray *allKeys = [addDic allKeys];
                        if (allKeys.count) {
                            return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = %@",tableName,kState,allKeys[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKeys[0]]]];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue2: {
                        NSArray *allKeys = [addDic allKeys];
                        if (allKeys.count > 1) {
                            return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = %@ and %@ = %@ and type = '1'",tableName,kState,allKeys[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKeys[0]]],allKeys[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKeys[1]]]];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue2A:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = %@ and %@ = %@ and %@ > 0",tableName,kState,kAccGroupId,addDic[kAccGroupId],kType,addDic[kType],kSymbolId];
                        break;
                    case ConditionQueryTypeValue2B:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = %@ and %@ = %@ and %@ > 0",tableName,kState,kAccGroupId,addDic[kAccGroupId],kType,addDic[kType],KSymbolCataId];
                        break;
                    case ConditionQueryTypeValue3:
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = %@ and %@ = %@ and %@ = %@",tableName,kState,kAccGroupId,addDic[kAccGroupId],kType,addDic[kType],kSymbolId,addDic[kSymbolId]];
                        break;
                    default:
                        break;
                }
            } else if ([model isKindOfClass:[item_symbol_sub class]]){
                
                switch (condType) {
                    case ConditionQueryTypeValue0:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.'%@' = %@.'%@' and %@ = '%lld' and %@.%@ = '0'and %@.'%@' = 1 and %@.'%@' = 0",kTableNameSymbol,tableName,kTableNameSymbol,tableName,kSymbolId,kTableNameSymbol,kID,kAccountId,userId,tableName,kState,kTableNameSymbol,kEnable,kTableNameSymbol,kState];
                        break;
                    case ConditionQueryTypeValue1: {
                        NSArray *allKeys = [addDic allKeys];
                        if (allKeys.count) {
                            if ([allKeys[0] isEqualToString:kSymbolSubCataid]) {
                                return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.'%@' = %@.'%@' and %@.%@ = '%@' and %@ = '%lld' and %@.%@ = '0' and %@.'%@' = 1 and %@.'%@' = 0",kTableNameSymbol,tableName,kTableNameSymbol,tableName,kSymbolId,kTableNameSymbol,kID,tableName,allKeys[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKeys[0]]],kAccountId,userId,tableName,kState,kTableNameSymbol,kEnable,kTableNameSymbol,kState];
                            } else if ([allKeys[0] isEqualToString:kSymbolId]) {
                                return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%lld' and %@ = '0'",tableName,allKeys[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKeys[0]]],kUserId,userId,kState];
                            }
                        }
                    }
                        break;
                    case ConditionQueryTypeValue1A:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@,%@,%@ where %@.'%@' = %@.'%@' and %@ = '%lld' and %@.%@ = '0'and %@.'%@' = 1 and %@.'%@' = 0 and ((%@.%@ = %@.%@ and %@.%@ != %@.%@ and %@.%@ = '%@') or (%@.%@ = %@.%@ and %@.%@ = %@.%@ and %@.%@ = '%@')) and %@.%@ = 0",kTableNameSymbol,tableName,kTableNameGroupSym,kTableNameGroupSymCata,kTableNameSymbol,tableName,kSymbolId,kTableNameSymbol,kID,kUserId,userId,tableName,kState,kTableNameSymbol,kEnable,kTableNameSymbol,kState,kTableNameSymbol,kCataId,kTableNameGroupSymCata,kID,kTableNameSymbol,kCataId,kTableNameGroupSym,kCataId,kTableNameGroupSymCata,kGroupId,addDic[kGroupId],kTableNameSymbol,kCataId,kTableNameGroupSym,kCataId,kTableNameSymbol,kID,kTableNameGroupSym,kID,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly];
                        break;
                    case ConditionQueryTypeValue1B:
                        return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@,%@ where %@.'%@' = %@.'%@' and %@.'%@' = %lld and %@.'%@' = 1 and %@.'%@' = 0 and %@.'%@' = '0' and %@.%@ = %@.%@ and %@.%@ = '%@' and %@.%@ = 0",kTableNameSymbol,tableName,kTableNameSymbol,kTableNameGroupSym,tableName,kSymbolId,kTableNameSymbol,kID,tableName,kAccountId,userId,kTableNameSymbol,kEnable,kTableNameSymbol,kState,tableName,kState,tableName,kSymbolId,kTableNameGroupSym,kID,kTableNameGroupSym,kGroupId,addDic[kGroupId],kTableNameGroupSym,kNoDispaly];
                        break;
                    case ConditionQueryTypeValue2: {
                        NSArray *allKey = [addDic allKeys];
                        if (allKey.count > 1) {
                            return  [NSString stringWithFormat:@"select distinct %@.* from %@,%@ where %@.'%@' = %@.'%@' and %@ = '%lld' and %@.%@ = '0' and %@.'%@' = 1 and %@.'%@' = 0 limit '%@','%@'",kTableNameSymbol,tableName,kTableNameSymbol,tableName,kSymbolId,kTableNameSymbol,kID,kUserId,userId,tableName,kState,kTableNameSymbol,kEnable,kTableNameSymbol,kState,addDic[kStartIndex],addDic[kOffset]];
                        }
                    }
                        break;
                    case ConditionQueryTypeValue3:
                        return [NSString stringWithFormat:@"select %@.%@ ,%@.%@ as %@,%@.%@,%@.%@ as %@,%@.%@ from %@,%@,%@ where %@.symbolId = %@.id and %@.%@ = 0 and %@.cataId = %@.id %@ = '%lld' and %@.%@ = '0' and %@.'%@' = 1 and %@.'%@' = 0 order by %@",tableName,kID,kTableNameSymbol,kID,kSymbolId,kTableNameSymbol,kName,kTableNameSymbolCatas,kID,KSymbolCataId,kTableNameSymbolCatas,kMarketId,tableName,kTableNameSymbol,kTableNameSymbolCatas,tableName,kTableNameSymbol,tableName,kState,kTableNameSymbol,kTableNameSymbolCatas,kUserId,userId,tableName,kState,kTableNameSymbol,kEnable,kTableNameSymbol,kState,kSequence];
                        break;
                    
                    default:
                        break;
                }
            }  else if ([model isKindOfClass:[item_holiday_cata class]]){
                if (addDic.count) {
                    NSArray *allKey = [addDic allKeys];
                    if (condType == ConditionQueryTypeValue1) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],kState];
                    }
                } else {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0'",tableName,kState];
                }
                
            } else if ([model isKindOfClass:[item_holiday class]]) {
                if (addDic.count) {
                    NSArray *allKey = [addDic allKeys];
                    switch (condType) {
                        case ConditionQueryTypeValue1:
                            return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0' and %@ = '1'" ,tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],kState,kEnable];
                            break;
                        case ConditionQueryTypeValue2:
                            return  [NSString stringWithFormat:@"select distinct * from %@ where %@.%@ <= %@ and %@ <= %@.%@ and %@ = '%@' and %@ = '0'" ,tableName,tableName,kFromTime,addDic[kCurrentTime],addDic[kCurrentTime],tableName,kToTime,kEnable,addDic[kEnable],kState];
                            break;
                        default:
                            break;
                    }
                } else {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '1'",tableName,kState,kEnable];
                }
                
            } else if ([model isKindOfClass:[item_company class]]){
                if (condType == ConditionQueryTypeValue0) {
                   return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0'",tableName,kState];
                } else if (condType == ConditionQueryTypeValue1) {
                  return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0' and %@ = '%@'",tableName,kState,kID,addDic[kCompanyId]];
                }
            } else if ([model isKindOfClass:[item_schedule_cata class]]) {
                if (addDic.count) {
                    NSArray *allKey = [addDic allKeys];
                    if (condType == ConditionQueryTypeValue1) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],kState];
                    }
                } else {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0'",tableName,kState];
                }
            } else if ([model isKindOfClass:[item_schedule class]]) {
                if (addDic.count) {
                    NSArray *allKey = [addDic allKeys];
                    switch (condType) {
                        case ConditionQueryTypeValue1:
                            return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0'" ,tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],kState];
                            break;
                        case ConditionQueryTypeValue2: {
                            if (allKey.count > 1) {
                                return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '1' and %@ = '%@' and %@ = '%@' and %@ = '0'" ,tableName,kEnable,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],allKey[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[1]]],kState];
                            }
                        }
                            break;
                        case ConditionQueryTypeValue3:
                            return  [NSString stringWithFormat:@"select distinct * from %@ where %@.%@ <= %@ and %@ <= %@.%@ and %@ = '%@' and %@ = '%@' and %@ = '0'" ,tableName,tableName,kStartTime,addDic[kCurrentTime],addDic[kCurrentTime],tableName,kEndTime,kEnable,addDic[kEnable],kDayOfWeek,addDic[kDayOfWeek],kState];
                            break;
                        default:
                            break;
                    }
                } else {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0'",tableName,kState];
                }
            } else if ([model isKindOfClass:[item_language class]]) {
                if (condType == ConditionQueryTypeValue2) {
                    NSArray *allKey = [addDic allKeys];
                    if (allKey.count) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],allKey[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[1]]],kState];
                    }
                }
            } else if ([model isKindOfClass:[item_symbol_margin_set class]]) {
                if (condType == ConditionQueryTypeValue1) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0' order by %@",tableName,kMarginType,addDic[kMarginType],kState,kRangeLeft];
                }
            } else if ([model isKindOfClass:[item_symbol_label class]]) {
                if (condType == ConditionQueryTypeValue1) {
                    NSArray *allKey = [addDic allKeys];
                    if (allKey.count) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],kState];
                    }
                }
            } else if ([model isKindOfClass:[item_holiday_margin class]]) {
                if (condType == ConditionQueryTypeValue2) {
                    NSArray *allKey = [addDic allKeys];
                    if (allKey.count) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],allKey[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[1]]],kState];
                    }
                }
            } else if ([model isKindOfClass:[item_schedule_margin class]]) {
                if (condType == ConditionQueryTypeValue2) {
                    NSArray *allKey = [addDic allKeys];
                    if (allKey.count) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],allKey[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[1]]],kState];
                    }
                }
            } else if ([model isKindOfClass:[item_quote_delay class]]) {
                if (condType == ConditionQueryTypeValue0) {
                    return [NSString stringWithFormat:@"select distinct * from %@ where %@ = '0'",tableName,kState];
                } else if (condType == ConditionQueryTypeValue2) {
                    NSArray *allKey = [addDic allKeys];
                    if (allKey.count) {
                        return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,allKey[0],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[0]]],allKey[1],[IXDataProcessTools dealWithSqlSingleQuote:addDic[allKey[1]]],kState];
                    }
                }
            } else if ([model isKindOfClass:[item_eod_time class]]) {
                if (condType == ConditionQueryTypeValue2) {
                   return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,kWeekDay,addDic[kWeekDay],kType,addDic[kType],kState];
                }
            } else if ([model isKindOfClass:[item_secure_dev class]]) {
                if (condType == ConditionQueryTypeValue1) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0' and %@ = '1'",tableName,kUserId,addDic[kUserId],kState,kPass];
                }
            } else if ([model isKindOfClass:[item_group_symbol class]]) {
                if (condType == ConditionQueryTypeValue2) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,kAccGroupId,addDic[kAccGroupId],kSymbolId,addDic[kSymbolId],kState];
                } else if (condType == ConditionQueryTypeValue1) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0'",tableName,kAccGroupId,addDic[kAccGroupId],kState];
                }
            } else if ([model isKindOfClass:[item_group_symbol_cata class]]) {
                if (condType == ConditionQueryTypeValue2) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,kAccGroupId,addDic[kAccGroupId],kCataId,addDic[kCataId],kState];
                }
            } else if ([model isKindOfClass:[item_lpib_bind class]]) {
                if (condType == ConditionQueryTypeValue1) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '0'",tableName,kID,addDic[kID],kState];
                }
            } else if ([model isKindOfClass:[item_lpchacc class]]) {
                if (condType == ConditionQueryTypeValue2) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@'  and %@ = '0'",tableName,kID,addDic[kID],kLpCompanyid,addDic[kLpCompanyid],kState];
                }
            } else if ([model isKindOfClass:[item_lpchacc_symbol class]]) {
                if (condType == ConditionQueryTypeValue2) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,kLpchaccid,addDic[kLpchaccid],kSymbolId,addDic[kSymbolId],kState];
                }
            } else if ([model isKindOfClass:[item_lpchannel_symbol class]]) {
                if (condType == ConditionQueryTypeValue3) {
                    return  [NSString stringWithFormat:@"select distinct * from %@ where %@ = '%@' and %@ = '%@' and %@ = '%@' and %@ = '0'",tableName,kLpCompanyid,addDic[kLpCompanyid],kSymbolId,addDic[kSymbolId],kChannelid,addDic[kChannelid],kState];
                }
            }
        }
            break;
        default:
            break;
    }
    return [NSString string];
}

#pragma mark 自定参数转sql
+ (NSString *)baseSelfDefineDataToSql:(NSDictionary *)infoDic
                      protocolCommand:(uint16)cmd
                          sqlIndexKey:(NSDictionary *)indexDic
                  dataBaseOperateType:(DataBaseOperateType)type
                        conditionType:(ConditionQueryType)condType
                               userId:(uint64_t)userId
                            accountId:(uint64_t)accountId
{
    NSString *keyStr = [[NSString alloc]init];
    NSString *valueStr = [[NSString alloc]init];
    NSString *tableName = [IXDBModel protocolCommandToTableName:cmd];
    if (type == DataBaseOperateTypeSave) {
        NSDictionary *dataDic = [IXDBModel groupSqlKeysValuesSelfDefineData:infoDic protocolCommand:cmd conditionType:condType userId:userId  accountId:accountId];
        if (dataDic) {
            keyStr = dataDic[kSqlIndexKey];
            valueStr = dataDic[kSqlValue];
        }
    }
    switch (type) {
        case DataBaseOperateTypeSave:
            return  [NSString stringWithFormat:@"replace into %@ (%@) values(%@)",tableName,keyStr,valueStr];
            break;
        case DataBaseOperateTypeDelete: {
            if (cmd == CMD_KAYLINE_MINUTE ||
                cmd == CMD_KAYLINE_FIVE_MINUTE ||
                cmd == CMD_KAYLINE_DAY) {
                return  [NSString stringWithFormat:@"delete from %@ where %@ = '%@'",tableName,kSymbolId,infoDic[kSymbolId]];
            } else if (cmd == CMD_SEARCH_BROWSE_HISTORY) {
                return  [NSString stringWithFormat:@"delete from %@",tableName];
            }
        }
            break;
        case DataBaseOperateTypeQuery: {
            switch (cmd) {
                case CMD_BROWSER_HISTORY:
                    return  [NSString stringWithFormat:@"select * from %@",tableName];
                    break;
                case CMD_SYMBOL_MARKETID:
                    return [NSString stringWithFormat:@"select %@.id,%@.marketId from %@,%@ where %@.id = %@ and %@.cataId = %@.id",kTableNameSymbol,kTableNameSymbolCatas,kTableNameSymbol,kTableNameSymbolCatas,kTableNameSymbol,infoDic[kSymbolId],kTableNameSymbol,kTableNameSymbolCatas];
                    break;
                case CMD_NETWORK_FLOW: {
                    switch (condType) {
                        case ConditionQueryTypeValue3:
                            return [NSString stringWithFormat:@"select sum(%@) as %@ from %@ where %@ between '%@' and '%@' and %@ = '%@'",kNetworkFlowData,kSums,tableName,kUUTime,infoDic[kStartIndex],infoDic[KEndIndex],kCategory,infoDic[kCategory]];
                            break;
                        case ConditionQueryTypeValue1:
                            return [NSString stringWithFormat:@"select sum(%@) as %@ from %@ where %@ = '%@'",kNetworkFlowData,kSums,tableName,kCategory,infoDic[kCategory]];
                            break;
                        default:
                            break;
                    }
                }
                    break;
                case CMD_KAYLINE_MINUTE:
                case CMD_KAYLINE_FIVE_MINUTE:
                case CMD_KAYLINE_DAY: {
                    switch (condType) {
                        case ConditionQueryTypeValue0:
                            return  [NSString stringWithFormat:@"select * from %@",tableName];
                            break;
                        case ConditionQueryTypeValue1:
                            return  [NSString stringWithFormat:@"select * from %@ where %@ = '%@'",tableName,kSymbolId,infoDic[kSymbolId]];
                            break;
                        case ConditionQueryTypeValue3:
                            return [NSString stringWithFormat:@"select * from %@  where %@ between '%@' and '%@' and %@ = '%@'",tableName,kNTime,infoDic[kStartIndex],infoDic[KEndIndex],kSymbolId,infoDic[kSymbolId]];
                        default:
                            break;
                    }
                }
                    break;
                case CMD_SEARCH_BROWSE_HISTORY:
                    switch (condType) {
                        case ConditionQueryTypeValue0:
                            return [NSString stringWithFormat:@"select * from %@ where %@ = '%lld' and %@ = '%lld'",tableName,kUserId,userId,kAccountId,accountId];
                            break;
                        case ConditionQueryTypeValue1:
                            return [NSString stringWithFormat:@"select * from %@ where %@ = '%lld' and %@ = '%lld' and %@ = '%@'",tableName,kUserId,userId,kAccountId,accountId,kSymbolSubId,infoDic[kSymbolSubId]];
                            break;
                        default:
                            break;
                    }
                    break;
                case CMD_YESTERDAY_PRICE: {
                    if (condType == ConditionQueryTypeValue1) {
                        return [NSString stringWithFormat:@"select * from %@ where %@ = '%@' and %@ < %@.%@",tableName,kID,infoDic[kID],infoDic[kDeadTimeStamp],tableName,kDeadTimeStamp];
                    }
                }
                    break;
                case CMD_QUOTE_PRICE: {
                    if (condType == ConditionQueryTypeValue1) {
                        
                        return [NSString stringWithFormat:@"select * from %@ where %@ = '%@'",tableName,kID,infoDic[kID]];
                    }
                }
                    break;
                case CMD_DEEP_PRICE: {
                    switch (condType) {
                        case ConditionQueryTypeValue1:
                            return [NSString stringWithFormat:@"select * from %@ where %@ = '%@' order by %@",tableName,kID,infoDic[kID],kDeepIndex];
                            break;
                        case ConditionQueryTypeValue2:
                            return [NSString stringWithFormat:@"select * from %@ where %@ = '%@' and %@ = '%@'",tableName,kID,infoDic[kID],kDeepIndex,infoDic[kDeepIndex]];
                            break;
                        default:
                            break;
                    }
                }
                    break;
                case CMD_GROUP_SYM: {
                    if (condType == ConditionQueryTypeValue2) {
                        return [NSString stringWithFormat:@"select * from %@ where %@ = '%@' and %@ = '%@'",tableName,kID,infoDic[kID],kGroupId,infoDic[kGroupId]];
                    }
                }
                    break;
                case CMD_GROUP_SYM_CATA: {
                    if (condType == ConditionQueryTypeValue2) {
                        return [NSString stringWithFormat:@"select * from %@ where %@ = '%@' and %@ = '%@'",tableName,kID,infoDic[kCataId],kGroupId,infoDic[kGroupId]];
                    }
                }
                    break;
                case CMD_DB_VERSION: {
                    if (condType == ConditionQueryTypeValue0) {
                        
                        return [NSString stringWithFormat:@"select * from %@",tableName];
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return [NSString string];
}

#pragma mark 组包数据库键值数据
+ (NSDictionary *)groupSqlKeysValuesSqlModel:(id)model
                                 sqlIndexKey:(NSDictionary *)indexDic
                                      userId:(uint64_t)userId
{
    NSDictionary *dataDic = [[NSDictionary alloc]init];
    NSString *keyStr = [[NSString alloc]init];
    NSString *valueStr = [[NSString alloc]init];
    
    if ([model isKindOfClass:[item_user class]]) {
        item_user *tmpModel = (item_user *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kCountry,kState,kCustomerNo,kLoginName,kPhone,kUUID,kUUTime,kCompany,kCity,kEmail,kName,kCompanyId];
        valueStr = [NSString stringWithFormat:@"'%lld','%@','%@','%@','%@','%@','%lld','%lld','%@','%@','%@','%@','%lld'",tmpModel.id_p,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.country],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.state],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.customerNo],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.loginName],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.phone],tmpModel.uuid,tmpModel.uutime,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.company],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.city],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.email],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],tmpModel.companyid];
    } else if ([model isKindOfClass:[item_account class]]) {
        item_account *tmpModel = (item_account *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUserId,kEnable,kBalance,kCredit,kBonus,kCompanyToken,kFreeMargin,kAccGroupId,kType,kLpuserid,kClearNegative,kClientType,kMt4Accid,kRefAccid,kNonTradable,kState];
        valueStr = [NSString stringWithFormat:@"%lld,%lld,'%lld',%d,%f,%f,%f,'%@',%f,%llu,%d,%lld,'%d','%d','%lld','%lld','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.userid,tmpModel.enable,tmpModel.balance,tmpModel.credit,tmpModel.bonus,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.companyToken],tmpModel.freeMargin,tmpModel.accountGroupid,tmpModel.type,tmpModel.lpuserid,tmpModel.clearNegative,tmpModel.clientType,tmpModel.mt4Accid,tmpModel.refAccid,tmpModel.nonTradable,tmpModel.status];
    }  else if ([model isKindOfClass:[item_symbol_cata class]]) {
        item_symbol_cata *tmpModel = (item_symbol_cata *)model;
        keyStr   = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kName,kSequence,kMarketId,kparentId,kState,kQuoteDelayMinutes,kScheduleDelayMinutes,kQuoteSubFee,kQuoteSubCurrency];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%@','%lld','%d','%lld','%d','%d','%d','%f','%@'",tmpModel.id_p,tmpModel.uuid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],tmpModel.sequence,tmpModel.marketid,tmpModel.parentid,tmpModel.status,tmpModel.quoteDelayMinutes,tmpModel.scheduleDelayMinutes,tmpModel.quoteSubFee,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.quoteSubCurrency]];
    } else if ([model isKindOfClass:[item_symbol class]]) {
        item_symbol *tmpModel = (item_symbol *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kName,kCataId,kSource,kBaseCurrency,kProfitcurrency,kUUID,kUUTime,kPinYin,kHolidayCataId,kScheduleCataId,kSequence,kDigits,kPipsRatio,kVolumesMin,kVolumesStep,kVolumsMax,kMarginType,kContractSize,kState,kStopLevel,kMaxStopLevel,kEnable,kSwapDaysPerYear,kSpread,kSpreadBalance,kPositionVolumeMax,kDisplayName,kLongSwap,kShortSwap,kStartTime,kExpiryTime,KTradable,kScheduleDelayMinutes,kContractSizeNew,kVolDigits];
        valueStr = [NSString stringWithFormat:@"'%lld','%@','%lld','%@','%@','%@','%lld','%lld','%@','%lld','%lld','%lld','%d','%d','%f','%f','%f','%d','%d','%d','%d','%d','%d','%d','%d','%d','%f','%@','%f','%f','%lld','%lld','%d','%d','%d','%lld'",tmpModel.id_p,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],tmpModel.symbolCataid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.source],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.baseCurrency],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.profitCurrency],tmpModel.uuid,tmpModel.uutime,[IXDBModel chineseToPinyin:[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name]],tmpModel.holidayCataid,tmpModel.scheduleCataid,tmpModel.sequence,tmpModel.digits,tmpModel.pipsRatio,tmpModel.volumesMin,tmpModel.volumesStep,tmpModel.volumesMax,tmpModel.marginType,tmpModel.contractSize,tmpModel.status,tmpModel.stopLevel,tmpModel.maxstopLevel,tmpModel.enable,tmpModel.swapDaysPerYear,tmpModel.spread,tmpModel.spreadBalance,tmpModel.positionVolumesMax,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.displayName],tmpModel.longSwap,tmpModel.shortSwap,tmpModel.startTime,tmpModel.expiryTime,tmpModel.tradable,tmpModel.scheduleDelayMinutes,tmpModel.contractSizeNew,tmpModel.volDigits];
    } else if ([model isKindOfClass:[item_symbol_hot class]]){
        item_symbol_hot *tmpModel = (item_symbol_hot *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kCompanyId,kSymbolId,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.companyid,tmpModel.symbolid,tmpModel.status];
    } else if ([model isKindOfClass:[item_symbol_sub class]]){
        item_symbol_sub *tmpModel = (item_symbol_sub *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kAccountId,kSymbolId,kSymbolSubCataid,kState,kUserId];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%lld','%d','%lld'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.accountid,tmpModel.symbolid,tmpModel.symbolSubCataid,tmpModel.status,userId];
    }  else if ([model isKindOfClass:[item_holiday_cata class]]){
        item_holiday_cata *tmpModel = (item_holiday_cata *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kName,kMarginType,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%@','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],tmpModel.marginType,tmpModel.status];
    } else if ([model isKindOfClass:[item_holiday class]]) {
        item_holiday *tmpModel = (item_holiday *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kEveryYear,kFromTime,kToTime,Kdescription,kEnable,kNeedStopout,kCancelOrder,kSwapDays,kSettlementDate,kMarginSetpre,kHolidayCataId,kName,kState,kMarginType,KTradable];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%d','%lld','%lld','%@','%d','%d','%d','%d','%lld','%d','%lld','%@','%d','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.everyYear,tmpModel.fromTime,tmpModel.totime,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.description_p],tmpModel.enable,tmpModel.needStopout,tmpModel.cancelOrder,tmpModel.swapDays,tmpModel.settlementDate,tmpModel.marginSetpre,tmpModel.holidayCataid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],tmpModel.status,tmpModel.marginType,tmpModel.tradable];
    } else if ([model isKindOfClass:[item_company class]]) {
        item_company *tmpModel = (item_company *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kName,kCompanyToken,kMode,kState,kType];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%@','%@','%d','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.companyToken],tmpModel.mode,tmpModel.status,tmpModel.type];
    }  else if ([model isKindOfClass:[item_schedule_cata class]]) {
        item_schedule_cata *tmpModel = (item_schedule_cata *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kName,kMarginType,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%@','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],tmpModel.marginType,tmpModel.status];
    } else if ([model isKindOfClass:[item_schedule class]]) {
        item_schedule *tmpModel = (item_schedule *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kDayOfWeek,kStartTime,kEndTime,kMarginSet,kHoliday,kEnable,KTradable,kNeedStopout,kCancelOrder,kState,kScheduleCataId,kMarginType];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%lld','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.dayOfWeek,tmpModel.startTime,tmpModel.endTime,tmpModel.marginSet,tmpModel.holiday,tmpModel.enable,tmpModel.nonTradable,tmpModel.needStopout,tmpModel.cancelOrder,tmpModel.status,tmpModel.scheduleCataid,tmpModel.marginType];
    } else if ([model isKindOfClass:[item_language class]]) {
        item_language *tmpModel = (item_language *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kNameSpace,kKeyId,kValue,kCountry,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%@','%llu','%@','%@','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.nameSpace],tmpModel.keyid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.value],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.country],tmpModel.status];
    } else if ([model isKindOfClass:[item_symbol_margin_set class]]) {
        item_symbol_margin_set *tmpModel = (item_symbol_margin_set *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kMarginType,kPercent,kState,kRangeLeft,kRangeRight];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%d','%d','%d','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.marginType,tmpModel.percent,tmpModel.status,tmpModel.rangeLeft,tmpModel.rangeRight];
    } else if ([model isKindOfClass:[item_symbol_label class]]) {
        item_symbol_label *tmpModel = (item_symbol_label *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@",kID,kUUID,kSymbolId,kName,kColour,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%@','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.symbolid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.symbolName],tmpModel.colour,tmpModel.status];
    } else if ([model isKindOfClass:[item_holiday_margin class]]) {
        item_holiday_margin *tmpModel = (item_holiday_margin *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kHolidayId,kSymbolId,kMarginType,kStatus];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.holidayid,tmpModel.symbolid,tmpModel.marginType,tmpModel.status];
    } else if ([model isKindOfClass:[item_schedule_margin class]]) {
        item_schedule_margin *tmpModel = (item_schedule_margin *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kScheduleId,kSymbolId,kMarginType,kStatus];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.scheduleid,tmpModel.symbolid,tmpModel.marginType,tmpModel.status];
    }  else if ([model isKindOfClass:[item_account_group class]]) {
        item_account_group *tmpModel = (item_account_group *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kServerId,kName,kCurrency,kMarginCallLevel,kStopOutLevel,kCompanyId,kType,kState,kEnable,kSpread,kLpuserid,kClientType,kOptions,kDefaultType];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%@','%@','%lld','%lld','%lld','%d','%d','%d','%d','%lld','%d','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.serverid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.currency],tmpModel.marginCallLevel,tmpModel.stopOutLevel,tmpModel.companyid,tmpModel.type,tmpModel.status,tmpModel.enable,tmpModel.spread,tmpModel.lpuserid,tmpModel.clientType,tmpModel.options,tmpModel.defaultType];
    } else if ([model isKindOfClass:[item_account_group_symbol_cata class]]) {
        item_account_group_symbol_cata *tmpModel = (item_account_group_symbol_cata *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kAccGroupId,KSymbolCataId,kSymbolId,kCommission,kState,kType,kSpread];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%lld','%d','%d','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.accountGroupid,tmpModel.symbolCataid,tmpModel.symbolid,tmpModel.commission,tmpModel.status,tmpModel.type,tmpModel.spread];
    } else if ([model isKindOfClass:[item_quote_delay class]]) {
        item_quote_delay *tmpModel = (item_quote_delay *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",kUUID,kUUTime,kUserId,KSymbolCataId,kLimitDate,kState,kType];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%d','%d'",tmpModel.uuid,tmpModel.uutime,tmpModel.userid,tmpModel.symbolCataid,tmpModel.limitDate,tmpModel.status,tmpModel.type];
    } else if ([model isKindOfClass:[item_eod_time class]]) {
        item_eod_time *tmpModel = (item_eod_time *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kServerId,kWeekDay,kStartTime,kType,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%d','%lld','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.serverid,tmpModel.weekday,tmpModel.startTime,tmpModel.type,tmpModel.status];
    } else if ([model isKindOfClass:[item_secure_dev class]]) {
        item_secure_dev *tmpModel = (item_secure_dev *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kUserId,kDevToken,kDevName,kLastLoginTime,kVerifyTime,kVerifyMode,kVerifyNo,kPass,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%@','%@','%lld','%lld','%d','%@','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.userid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.devToken],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.devName],tmpModel.lastLoginTime,tmpModel.verifyTime,tmpModel.verifyMode,tmpModel.verifyNo,tmpModel.pass,tmpModel.status];
    } else if ([model isKindOfClass:[item_group_symbol class]]) {
        item_group_symbol *tmpModel = (item_group_symbol *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kAccGroupId,KSymbolCataId,kSymbolId,kCommission,kSpread,KTradable,kVolumesMin,kVolumsMax,kVolumesStep,kVolumesSidemax,kLongSwap,kShortSwap,kNormalMarginType,kWeekendMarginType,kHolidayMarginType,kState,kCloseOnly,kNoDispaly,kStartTime,kExpiryTime,kLpuserid,kLpstatus,kLpExpiryTime];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%lld','%d','%d','%d','%f','%f','%f','%f','%f','%f','%d','%d','%d','%d','%d','%d','%lld','%lld','%lld','%d','%lld'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.accountGroupid,tmpModel.symbolCataid,tmpModel.symbolid,tmpModel.commission,tmpModel.spread,tmpModel.tradable,tmpModel.volumesMin,tmpModel.volumesMax,tmpModel.volumesStep,tmpModel.volumesSidemax,tmpModel.longSwap,tmpModel.shortSwap,tmpModel.normalMarginType,tmpModel.weekendMarginType,tmpModel.holidayMarginType,tmpModel.status,tmpModel.closeOnly,tmpModel.noDisplay,tmpModel.startTime,tmpModel.expiryTime,tmpModel.lpuserid,tmpModel.lpstatus,tmpModel.lpexpiryTime];
    } else if ([model isKindOfClass:[item_group_symbol_cata class]]) {
        item_group_symbol_cata *tmpModel = (item_group_symbol_cata *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kAccGroupId,kCataId,kCommission,kNormalMarginType,kWeekendMarginType,kHolidayMarginType,kState,kCommissionType];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%d','%d','%d','%d','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.accountGroupid,tmpModel.symbolCataid,tmpModel.commission,tmpModel.normalMarginType,tmpModel.weekendMarginType,tmpModel.holidayMarginType,tmpModel.status,tmpModel.commissionType];
    } else if ([model isKindOfClass:[item_lpchacc class]]) {
        item_lpchacc *tmpModel = (item_lpchacc *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kChannelid,kLpCompanyid,kRefAccid,kToken,kName,kIp,kPort,kState,kTargetCompid,kSenderCompid,kSenderSubid,kDeliverToCompid];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%d','%lld','%lld','%@','%@','%@','%d','%d','%@','%@','%@','%@'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.channelid,tmpModel.lpCompanyid,tmpModel.refaccid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.token],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.ip],tmpModel.port,tmpModel.status,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.targetCompid],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.senderCompid],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.senderSubid],[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.deliverToCompid]];
    } else if ([model isKindOfClass:[item_lpchacc_symbol class]]) {
        item_lpchacc_symbol *tmpModel = (item_lpchacc_symbol *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kLpchaccid,kSymbolId,kLpspread,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.lpchaccid,tmpModel.symbolid,tmpModel.lpspread,tmpModel.status];
    } else if ([model isKindOfClass:[item_lpchannel class]]) {
        item_lpchannel *tmpModel = (item_lpchannel *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kName,kType,kIp,kPort,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%@','%d','%@','%d','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.name],tmpModel.type,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.ip],tmpModel.port,tmpModel.status];
    } else if ([model isKindOfClass:[item_lpchannel_symbol class]]) {
        item_lpchannel_symbol *tmpModel = (item_lpchannel_symbol *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kLpCompanyid,kSymbolId,kChspread,kChannelid,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%d','%lld','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.lpCompanyid,tmpModel.symbolid,tmpModel.chspread,tmpModel.channelid,tmpModel.status];
    } else if ([model isKindOfClass:[item_lpib_bind class]]) {
        item_lpib_bind *tmpModel = (item_lpib_bind *)model;
        keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",kID,kUUID,kUUTime,kIbCompanyid,kLpCompanyid,kLpchaccToken,kLpchaccid,kState];
        valueStr = [NSString stringWithFormat:@"'%lld','%lld','%lld','%lld','%lld','%@','%lld','%d'",tmpModel.id_p,tmpModel.uuid,tmpModel.uutime,tmpModel.ibCompanyid,tmpModel.lpCompanyid,[IXDataProcessTools dealWithSqlSingleQuote:tmpModel.lpchaccToken],tmpModel.lpchaccid,tmpModel.status];
    }
    dataDic = @{
                kSqlIndexKey:keyStr,
                kSqlValue:valueStr
                };
    
    return dataDic;
}

#pragma mark 组包数据库自定义键值数据
+ (NSDictionary *)groupSqlKeysValuesSelfDefineData:(NSDictionary *)infoDic
                                   protocolCommand:(uint16)cmd
                                     conditionType:(ConditionQueryType)condType
                                            userId:(uint64_t)userId
                                         accountId:(uint64_t)accountId
{
    NSString *keyStr = nil;
    NSString *valueStr = nil;
    switch (cmd) {
        case CMD_BROWSER_HISTORY: {
            keyStr = [NSString stringWithFormat:@"%@,%@",kID,kUUTime];
            valueStr = [NSString stringWithFormat:@"'%@','%@'",infoDic[kSymbolId],infoDic[kUUTime]];
        }
            break;
        case CMD_NETWORK_FLOW: {
            if (condType == ConditionQueryTypeValue1) {
                keyStr = [NSString stringWithFormat:@"%@,%@,%@",kNetworkFlowData,kUUTime,kCategory];
                valueStr = [NSString stringWithFormat:@"'%@','%@','%@'",infoDic[kNetworkFlowData],infoDic[kUUTime],infoDic[kCategory]];
            }
        }
            break;
        case CMD_KAYLINE_MINUTE:
        case CMD_KAYLINE_FIVE_MINUTE:
        case CMD_KAYLINE_DAY: {
            int maxNum = 0;
            if (cmd == CMD_KAYLINE_MINUTE) {
                
                maxNum = kMinuteMaxNum;
            } else if (cmd == CMD_KAYLINE_FIVE_MINUTE) {
                
                maxNum = kFiveMinuteMaxNum;
            } else if (cmd == CMD_KAYLINE_DAY) {
                
                maxNum = kHourMaxNum;
            }
            keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@",kNTime,kAmount,kNOpenPrice,kClosePrice,kHighPrice,kLowPrice,kVolume,kSymbolId,kMaxNum];
            valueStr = [NSString stringWithFormat:@"'%@','%@','%@','%@','%@','%@','%@','%@','%d'",infoDic[kNTime],infoDic[kAmount],infoDic[kNOpenPrice],infoDic[kClosePrice],infoDic[kHighPrice],infoDic[kLowPrice],infoDic[kVolume],infoDic[kSymbolId],maxNum];
        }
            
            break;
        case CMD_SEARCH_BROWSE_HISTORY: {
            keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@",kID,kName,kEnable,kUserId,kMarketName,kLanguageName,kSource,kAccountId,kSymbolSubId];
            valueStr = [NSString stringWithFormat:@"'%@','%@','%@','%lld','%@','%@','%@','%lld','%@'",infoDic[kID],[IXDataProcessTools dealWithSqlSingleQuote:infoDic[kName]],infoDic[kEnable],userId,[IXDataProcessTools dealWithSqlSingleQuote:infoDic[kMarketName]],[IXDataProcessTools dealWithSqlSingleQuote:infoDic[kLanguageName]],[IXDataProcessTools dealWithSqlSingleQuote:infoDic[kSource]],accountId,infoDic[kSymbolSubId]];
        }
            break;
        case CMD_YESTERDAY_PRICE: {
            keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@",kID,kMarketId,kPrice,kDeadTimeStamp];
            valueStr = [NSString stringWithFormat:@"'%@','%@','%@','%@'",infoDic[kSymbolId],infoDic[kMarketId],infoDic[kPrice],infoDic[kDeadTimeStamp]];
        }
            break;
        case CMD_QUOTE_PRICE: {
//            keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",kID,kNGmtTime,kNOpen,kNHight,kNPrice];
//            valueStr = [NSString stringWithFormat:@"'%@','%@','%@','%@','%@'",infoDic[kSymbolId],infoDic[kNGmtTime],infoDic[kNOpen],infoDic[kNHight],infoDic[kNPrice]];
            keyStr = [NSString stringWithFormat:@"%@,%@",kID,kQuoteData];
            valueStr = [NSString stringWithFormat:@"'%@','%@'",infoDic[kSymbolId],infoDic[kQuoteData]];
        }
            break;
        case CMD_DEEP_PRICE: {
            keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@",kID,kBuyPrice,kBuyVolume,kSellPrice,kSellVolume,kDeepIndex];
            valueStr = [NSString stringWithFormat:@"'%@','%@','%@','%@','%@','%@'",infoDic[kSymbolId],infoDic[kBuyPrice],infoDic[kBuyVolume],infoDic[kSellPrice],infoDic[kSellVolume],infoDic[kDeepIndex]];
        }
            break;
        case CMD_GROUP_SYM_CATA: {
            keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@",kID,kUUID,kGroupId,kLevel];
            valueStr = [NSString stringWithFormat:@"'%@','%@','%@','%@'",infoDic[KSymbolCataId],infoDic[kUUID],infoDic[kAccGroupId],infoDic[kLevel]];
        }
            break;
        case CMD_GROUP_SYM: {
            keyStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",kID,kUUID,kGroupId,kCataId,kNoDispaly];
            valueStr = [NSString stringWithFormat:@"'%@','%@','%@','%@','%@'",infoDic[kSymbolId],infoDic[kUUID],infoDic[kAccGroupId],infoDic[kCataId],infoDic[kNoDispaly]];
        }
            break;
        case CMD_DB_VERSION: {
            keyStr = [NSString stringWithFormat:@"%@",kDBVersion];
            valueStr = [NSString stringWithFormat:@"'%@'",infoDic[kDBVersion]];
        }
            break;
        default:
            break;
    }
    NSDictionary *dataDic = @{kSqlIndexKey:keyStr,
                              kSqlValue:valueStr};
    
    return dataDic;
}

#pragma mark 获取表名
+ (NSString *)protocolCommandToTableName:(uint16)cmd
{
    NSString *retStr = [[NSString alloc]init];
    switch (cmd) {
        case CMD_USER_LOGIN_INFO:
            return kTableNameUserInfo;
            break;
        case CMD_ACCOUNT_LIST:
            return kTableNameAccount;
            break;
        case CMD_SYMBOL_CATA_LIST:
            return kTableNameSymbolCatas;
            break;
        case CMD_SYMBOL_LIST:
            return kTableNameSymbol;
            break;
        case CMD_POSITION_LIST:
            return kTableNamePosition;
            break;
        case CMD_HOLIDAY_CATA_LIST:
            return kTableNameHolidayCatas;
            break;
        case CMD_HOLIDAY_LIST:
            return kTableNameHoliday;
            break;
        case CMD_SYMBOL_SUB_LIST:
            return kTableNameSymbolSub;
            break;
        case CMD_SYMBOL_SUB_DELETE:
            return kTableNameSymbolSub;
            break;
        case CMD_SYMBOL_SUB_CATA_LIST:
            return kTableNameSymbolSubCata;
            break;
        case CMD_SYMBOL_HOT_LIST:
            return kTableNameSymbolHot;
            break;
        case CMD_COMPANY_LIST:
            return kTableNameCompany;
            break;
        case CMD_SCHEDULE_LIST:
            return kTableNameSchedule;
            break;
        case CMD_SCHEDULE_CATA_LIST:
            return kTableNameScheduleCata;
            break;
        case CMD_BROWSER_HISTORY:
            return kTableNameBrowserHistory;
            break;
        case CMD_KAYLINE_MINUTE:
            return kTableNameKayLineMinute;
            break;
        case CMD_KAYLINE_FIVE_MINUTE:
            return kTableNameKayLineFiveMinute;
            break;
        case CMD_KAYLINE_DAY:
            return kTableNameKayLineDay;
            break;
        case CMD_NETWORK_FLOW:
            return kTableNameNetworkFlow;
            break;
        case CMD_LANGUAGE_LIST:
            return kTableNameLanguage;
            break;
        case CMD_SYMBOL_MARGIN_SET_LIST:
            return kTableNameMarginSet;
            break;
        case CMD_ACCOUNT_GROUP_LIST:
            return kTableNameAccountGroup;
            break;
        case CMD_ACCOUNTGROUPSYMBOLCATA_LIST:
            return kTableNameAccountGroupSymCate;
            break;
        case CMD_SEARCH_BROWSE_HISTORY:
            return kTableNameSearchBrowse;
            break;
        case CMD_SYMBOL_LABEL_LIST:
            return kTableNameSymbolLable;
            break;
        case CMD_HOLIDAY_MARGIN_LIST:
            return kTableNameHolidayMargin;
            break;
        case CMD_SCHEDULE_MARGIN_LIST:
            return kTableNameScheduleMargin;
            break;
        case CMD_EOD_TIME_LIST:
            return KTableNameEodTime;
            break;
        case CMD_SECURE_DEV_LIST:
            return kTableNameSecureDev;
            break;
        case CMD_YESTERDAY_PRICE:
            return kTableNameYesterdayPrice;
            break;
        case CMD_QUOTE_PRICE:
            return kTableNameQuotePrice;
            break;
        case CMD_DEEP_PRICE:
            return kTableNameDeepPrice;
            break;
        case CMD_QUOTE_DELAY_LIST:
            return kTableNameQuoteDelay;
            break;
        case CMD_GROUP_SYM_CATA:
            return kTableNameGroupSymCata;
            break;
        case CMD_GROUP_SYM:
            return kTableNameGroupSym;
            break;
        case CMD_DB_VERSION:
            return kTableNameDBVersion;
            break;
        case CMD_GROUP_SYMBOL_LIST:
            return kTableNameGroupSymNew;
            break;
        case CMD_GROUP_SYMBOLCATA_LIST:
            return kTableNameGroupSymCataNew;
            break;
        case CMD_LPCHANNEL_ACCOUNT_LIST:
            return kTableNameLpchacc;
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST:
            return kTableNameLpchaccSymbol;
            break;
        case CMD_LPCHANNEL_LIST:
            return kTableNameLpchannel;
            break;
        case CMD_LPCHANNEL_SYMBOL_LIST:
            return kTableNameLpchannelSymbol;
            break;
        case CMD_LPIB_BIND_LIST:
            return kTableNameLpibBind;
            break;
        default:
            break;
    }
    return retStr;
}

#pragma mark 处理数据保存
+ (BOOL)dealWithDataSave:(id)model
         protocolCommand:(uint16)cmd
             sqlIndexKey:(NSDictionary *)indexDic
     dataBaseOperateType:(DataBaseOperateType)type
                  userId:(uint64_t)userId
           conditionType:(ConditionQueryType)condType
{
    
    NSString *exeSql = [IXDBModel sqlModelToSql:model protocolCommand:cmd additon:nil sqlIndexKey:indexDic dataBaseOperateType:type userId:userId conditionType:condType];
    NSString *createSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    if (type == DataBaseOperateTypeSave) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd] && model) {
            return [[IXDBManager sharedInstance] save:exeSql];
        } else {
            return NO;
        }
    } else if (type == DataBaseOperateTypeDelete) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd]) {
            return [[IXDBManager sharedInstance] deleteObject:exeSql];
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

#pragma mark  批量处理数据保存
+ (BOOL)dealWithBatchDataSave:(NSArray *)modelArr
              protocolCommand:(uint16)cmd
                  sqlIndexKey:(NSDictionary *)indexDic
          dataBaseOperateType:(DataBaseOperateType)type
                       userId:(uint64_t)userId
                conditionType:(ConditionQueryType)condType

{
    NSArray *exeSqlArr = [IXDBModel sqlModelToSqls:modelArr protocolCommand:cmd additonInfos:nil sqlIndexKey:indexDic dataBaseOperateType:type userId:userId conditionType:condType];
    NSString *createSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    if (type == DataBaseOperateTypeSave) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd]) {
            return [[IXDBManager sharedInstance] saveObjects:exeSqlArr];
        } else {
            return NO;
        }
    } else if (type == DataBaseOperateTypeDelete) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd]) {
            return [[IXDBManager sharedInstance] deleteObjects:exeSqlArr];
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

#pragma mark 处理数据查询
+ (NSArray *)dealWithDataCheck:(id)model
               protocolCommand:(uint16)cmd
                   sqlIndexKey:(NSDictionary *)indexDic
                     queryCond:(NSDictionary *)condDic
                        userId:(uint64_t)userId
                 conditionType:(ConditionQueryType)condType
{
    NSString *exeSql = [IXDBModel sqlModelToSql:model protocolCommand:cmd additon:condDic sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeQuery userId:userId conditionType:condType];
    NSString *tableSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    return [IXDBModel executeCheckExeSql:exeSql createTableSql:tableSql protocolCommand:cmd sqlIndexKey:indexDic userId:userId];
}

#pragma mark 处理自定义数据查询
+ (NSArray *) dealWithSelfDefineCheckProtocolCommand:(uint16)cmd
                                        sqlIndexKey:(NSDictionary *)indexDic
                                          queryCond:(NSArray *)condArr
                                      conditionType:(ConditionQueryType)condType
                                             userId:(uint64_t)userId
                                          accountId:(uint64_t)accountId
{
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    if (condType == ConditionQueryTypeValue0) {
        NSString *exeSql = [IXDBModel baseSelfDefineDataToSql:nil protocolCommand:cmd sqlIndexKey:nil dataBaseOperateType:DataBaseOperateTypeQuery conditionType:condType userId:userId accountId:accountId];
        NSString *tableSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
        retArr = [[IXDBModel executeCheckExeSql:exeSql createTableSql:tableSql protocolCommand:cmd sqlIndexKey:indexDic userId:userId] mutableCopy];
    } else {
        for (int i = 0; i < condArr.count; i++) {
            NSString *exeSql = [IXDBModel baseSelfDefineDataToSql:condArr[i] protocolCommand:cmd sqlIndexKey:nil dataBaseOperateType:DataBaseOperateTypeQuery conditionType:condType userId:userId accountId:accountId];
            NSString *tableSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
            NSArray *dataArr = [IXDBModel executeCheckExeSql:exeSql createTableSql:tableSql protocolCommand:cmd sqlIndexKey:indexDic userId:userId];
            if (dataArr.count > 0) {
                
                [retArr addObjectsFromArray:dataArr];
            }
        }
    }
    return retArr;
}

#pragma mark 处理模糊数据查询
+ (NSArray *)dealWithDataFuzzyCheck:(id)model
                    protocolCommand:(uint16)cmd
                        sqlIndexKey:(NSDictionary *)indexDic
                            additon:(NSDictionary *)addDic
                             userId:(uint64_t)userId
                      conditionType:(ConditionQueryType)condType

{
    NSString *exeSql = [IXDBModel sqlModelToSql:model protocolCommand:cmd additon:addDic sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeQuery userId:userId conditionType:condType];
    NSString *tableSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    return [IXDBModel executeCheckExeSql:exeSql createTableSql:tableSql protocolCommand:cmd sqlIndexKey:indexDic userId:userId];
}

#pragma mark 处理条件查询
+ (NSArray *)dealWithCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                         sqlIndexKey:(NSDictionary *)indexDic
                                              userId:(uint64_t)userId
{
    NSString *exeSql = [IXDBModel sqlCondCheckUUIDMaxProtocolCommand:cmd userId:userId];
    NSString *tableSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    return [IXDBModel executeCheckExeSql:exeSql createTableSql:tableSql protocolCommand:cmd sqlIndexKey:indexDic userId:userId];
}

+ (NSArray *)dealWithCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                         sqlIndexKey:(NSDictionary *)indexDic
                                             addInfo:(NSDictionary *)infoDic
{
    NSString *exeSql = [IXDBModel sqlCondCheckUUIDMaxProtocolCommand:cmd addInfo:infoDic];
    NSString *tableSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:0];
    return [IXDBModel executeCheckExeSql:exeSql createTableSql:tableSql protocolCommand:cmd sqlIndexKey:indexDic userId:0];
}

#pragma mark  处理数据删除
+ (BOOL)dealWithDataDelete:(id)model
           protocolCommand:(uint16)cmd
                    userId:(uint64_t)userId
             conditionType:(ConditionQueryType)condType
{
    NSString *exeSql = [IXDBModel sqlModelToSql:model protocolCommand:cmd additon:nil sqlIndexKey:nil dataBaseOperateType:DataBaseOperateTypeDelete userId:userId conditionType:condType];
    return [IXDBModel excuteDeleteExeSql:exeSql protocolCommand:cmd userId:userId];
}

#pragma mark  批量处理数据删除

+ (BOOL)dealWithDataBatchDelete:(NSArray *)modelArr
                protocolCommand:(uint16)cmd
                         userId:(uint64_t)userId
{
    NSArray *exeSqlArr = [IXDBModel sqlModelToSqls:modelArr protocolCommand:cmd additonInfos:nil sqlIndexKey:nil dataBaseOperateType:DataBaseOperateTypeDelete userId:userId];
    return [IXDBModel excuteBatchDeleteExeSql:exeSqlArr protocolCommand:cmd userId:userId];
}

#pragma mark 执行查询操作
+ (NSArray *)executeCheckExeSql:(NSString *)exeSql
                 createTableSql:(NSString *)tableSql
                protocolCommand:(uint16)cmd
                    sqlIndexKey:(NSDictionary *)indexDic
                         userId:(uint64_t)userId
{
    return  [[IXDBManager sharedInstance] checkObjects:exeSql indexKeys:indexDic];
}

#pragma mark 执行删除操作
+ (BOOL)excuteDeleteExeSql:(NSString *)sql
           protocolCommand:(uint16)cmd
                    userId:(uint64_t)userId
{
    return [[IXDBManager sharedInstance] deleteObject:sql];
}

#pragma mark 执行批量删除操作
+ (BOOL)excuteBatchDeleteExeSql:(NSArray *)sqlArr
                protocolCommand:(uint16)cmd
                         userId:(uint64_t)userId
{
    return [[IXDBManager sharedInstance] deleteObjects:sqlArr];
}

#pragma mark 查询UUID最大值
+ (NSDictionary *)dealWithCheckUUIDMaxProtocolCommand:(uint16)cmd
                                               userId:(uint64_t)userId
{
    NSDictionary *indexDic = @{kUUID:kSqlInteger,
                               kID:kSqlInteger};
    NSArray *dataArr = [IXDBModel dealWithCondCheckUUIDMaxProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    if (dataArr.count == 1) {
        return dataArr[0];
    } else {
        return [NSDictionary new];
    }
}

+ (NSDictionary *)dealWithCheckUUIDMaxProtocolCommand:(uint16)cmd
                                              addInfo:(NSDictionary *)infoDic {
    NSDictionary *indexDic = @{
                               kUUID:kSqlInteger,
                               kID:kSqlInteger
                               };
    NSArray *dataArr = [IXDBModel dealWithCondCheckUUIDMaxProtocolCommand:cmd sqlIndexKey:indexDic addInfo:infoDic];
    if (dataArr.count == 1) {
        return dataArr[0];
    } else {
        return nil;
    }
}

#pragma mark 组装数据库字段
+ (NSDictionary *)packageIndexProtocolCommand:(uint16)cmd
                                   serverType:(IXDBServerType)serverType
                          dataBaseOperateType:(DataBaseOperateType)type
                                       userId:(uint64_t)userId
{
    if (serverType == IXDBServerTypeTrade) {
        switch (cmd) {
            case CMD_USER_LOGIN_INFO:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kLoginName:kSqlText,
                                 kName:kSqlText,
                                 kCompanyId:kSqlInteger,
                                 kCompany:kSqlText,
                                 kEmail:kSqlText,
                                 kPhone:kSqlText,
                                 kCountry:kSqlText,
                                 kState:kSqlText,
                                 kCity:kSqlText,
                                 kCustomerNo:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kUUID:kSqlInteger,
                                 kUserId:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kLoginName:kSqlText,
                                 kName:kSqlText,
                                 kCompanyId:kSqlInteger,
                                 kCompany:kSqlText,
                                 kEmail:kSqlText,
                                 kPhone:kSqlText,
                                 kCountry:kSqlText,
                                 kState:kSqlText,
                                 kCity:kSqlText,
                                 kCustomerNo:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kUUID:kSqlInteger,
                                 kUserId:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SYMBOL_CATA_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kName:kSqlText,
                                 kSequence:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kMarketId:kSqlInteger,
                                 kparentId:kSqlInteger,
                                 kState:kSqlInteger,
                                 kQuoteDelayMinutes:kSqlInteger,
                                 kScheduleDelayMinutes:kSqlInteger,
                                 kQuoteSubFee:kSqlDouble,
                                 kQuoteSubCurrency:kSqlText};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kName:kSqlText,
                                 kSequence:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kMarketId:kSqlInteger,
                                 kparentId:kSqlInteger,
                                 kState:kSqlInteger,
                                 kQuoteDelayMinutes:kSqlInteger,
                                 kScheduleDelayMinutes:kSqlInteger,
                                 kQuoteSubFee:kSqlDouble,
                                 kQuoteSubCurrency:kSqlText};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_ACCOUNT_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kUserId:kSqlInteger,
                                 kAccGroupId:kSqlInteger,
                                 kEnable:kSqlInteger,
                                 kBalance:kSqlDouble,
                                 kCredit:kSqlDouble,
                                 kBonus:kSqlDouble,
                                 kEquity:kSqlDouble,
                                 kFreeMargin:kSqlDouble,
                                 kCompanyToken:kSqlText,
                                 kType:kSqlInteger,
                                 kSpread:kSqlInteger,
                                 kLpuserid:kSqlInteger,
                                 kClearNegative:kSqlInteger,
                                 kClientType:kSqlInteger,
                                 kMt4Accid:kSqlInteger,
                                 kRefAccid:kSqlInteger,
                                 kNonTradable:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return  @{kID:kSqlInteger,
                                  kUUTime:kSqlText,
                                  kUserId:kSqlInteger,
                                  kAccGroupId:kSqlInteger,
                                  kEnable:kSqlInteger,
                                  kBalance:kSqlDouble,
                                  kCredit:kSqlDouble,
                                  kBonus:kSqlDouble,
                                  kEquity:kSqlDouble,
                                  kFreeMargin:kSqlDouble,
                                  kUUID:kSqlInteger,
                                  kCompanyToken:kSqlText,
                                  kType:kSqlInteger,
                                  kSpread:kSqlInteger,
                                  kLpuserid:kSqlInteger,
                                  kClearNegative:kSqlInteger,
                                  kClientType:kSqlInteger,
                                  kMt4Accid:kSqlInteger,
                                  kRefAccid:kSqlInteger,
                                  kNonTradable:kSqlInteger,
                                  kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SYMBOL_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kSource:kSqlText,
                                 kEnable:kSqlInteger,
                                 kBaseCurrency:kSqlText,
                                 kProfitcurrency:kSqlText,
                                 kCataId:kSqlInteger,
                                 kSequence:kSqlInteger,
                                 kPinYin:kSqlText,
                                 kHolidayCataId:kSqlInteger,
                                 kScheduleCataId:kSqlInteger,
                                 kDigits:kSqlInteger,
                                 kPipsRatio:kSqlInteger,
                                 kVolumesMin:kSqlInteger,
                                 kVolumesStep:kSqlInteger,
                                 kVolumsMax:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kContractSize:kSqlInteger,
                                 kState:kSqlInteger,
                                 kStopLevel:kSqlInteger,
                                 kMaxStopLevel:kSqlInteger,
                                 kSwapDaysPerYear:kSqlInteger,
                                 kSpread:kSqlInteger,
                                 kSpreadBalance:kSqlInteger,
                                 kPositionVolumeMax:kSqlInteger,
                                 kDisplayName:kSqlText,
                                 kLongSwap:kSqlInteger,
                                 kShortSwap:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kExpiryTime:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kScheduleDelayMinutes:kSqlInteger,
                                 kContractSizeNew:kSqlInteger,
                                 kVolDigits:kSqlInteger
                                 };
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlText,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kSource:kSqlText,
                                 kEnable:kSqlInteger,
                                 kBaseCurrency:kSqlText,
                                 kProfitcurrency:kSqlText,
                                 kCataId:kSqlInteger,
                                 kSequence:kSqlInteger,
                                 kPinYin:kSqlText,
                                 kHolidayCataId:kSqlInteger,
                                 kScheduleCataId:kSqlInteger,
                                 kDigits:kSqlInteger,
                                 kPipsRatio:kSqlInteger,
                                 kVolumesMin:kSqlInteger,
                                 kVolumesStep:kSqlInteger,
                                 kVolumsMax:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kContractSize:kSqlInteger,
                                 kState:kSqlInteger,
                                 kStopLevel:kSqlInteger,
                                 kMaxStopLevel:kSqlInteger,
                                 kSwapDaysPerYear:kSqlInteger,
                                 kSpread:kSqlInteger,
                                 kSpreadBalance:kSqlInteger,
                                 kPositionVolumeMax:kSqlInteger,
                                 kDisplayName:kSqlText,
                                 kLongSwap:kSqlInteger,
                                 kShortSwap:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kExpiryTime:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kScheduleDelayMinutes:kSqlInteger,
                                 kContractSizeNew:kSqlInteger,
                                 kVolDigits:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_POSITION_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kDirection:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kSymbol:kSqlText,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kDirection:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kSymbol:kSqlText,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_COMPANY_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kCompanyToken:kSqlText,
                                 kMode:kSqlInteger,
                                 kState:kSqlInteger,
                                 kType:kSqlInteger
                                 };
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kCompanyToken:kSqlText,
                                 kMode:kSqlInteger,
                                 kState:kSqlInteger,
                                 kType:kSqlInteger
                                 };
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_HOLIDAY_CATA_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kMarginType:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kMarginType:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_HOLIDAY_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kEveryYear:kSqlInteger,
                                 kFromTime:kSqlInteger,
                                 kToTime:kSqlInteger,
                                 Kdescription:kSqlText,
                                 kEnable:kSqlInteger,
                                 kNeedStopout:kSqlInteger,
                                 kCancelOrder:kSqlInteger,
                                 kSwapDays:kSqlInteger,
                                 kSettlementDate:kSqlInteger,
                                 kMarginSetpre:kSqlInteger,
                                 kHolidayCataId:kSqlInteger,
                                 kState:kSqlInteger,
                                 kName:kSqlText,
                                 kMarginType:kSqlInteger,
                                 KTradable:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kEveryYear:kSqlInteger,
                                 kFromTime:kSqlInteger,
                                 kToTime:kSqlInteger,
                                 Kdescription:kSqlText,
                                 kEnable:kSqlInteger,
                                 kNeedStopout:kSqlInteger,
                                 kCancelOrder:kSqlInteger,
                                 kSwapDays:kSqlInteger,
                                 kSettlementDate:kSqlInteger,
                                 kMarginSetpre:kSqlInteger,
                                 kHolidayCataId:kSqlInteger,
                                 kState:kSqlInteger,
                                 kName:kSqlText,
                                 kMarginType:kSqlInteger,
                                 KTradable:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SYMBOL_SUB_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kAccountId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kSymbolSubCataid:kSqlInteger,
                                 kUserId:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kState:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kSource:kSqlText,
                                 kEnable:kSqlInteger,
                                 kBaseCurrency:kSqlText,
                                 kProfitcurrency:kSqlText,
                                 kCataId:kSqlInteger,
                                 kSequence:kSqlInteger,
                                 kPinYin:kSqlText,
                                 kHolidayCataId:kSqlInteger,
                                 kScheduleCataId:kSqlInteger,
                                 kDigits:kSqlInteger,
                                 kPipsRatio:kSqlInteger,
                                 kVolumesMin:kSqlInteger,
                                 kVolumesStep:kSqlInteger,
                                 kVolumsMax:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kContractSize:kSqlInteger,
                                 kStopLevel:kSqlInteger,
                                 kMaxStopLevel:kSqlInteger,
                                 kSwapDaysPerYear:kSqlInteger,
                                 kSpread:kSqlInteger,
                                 kSpreadBalance:kSqlInteger,
                                 kPositionVolumeMax:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kExpiryTime:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kContractSizeNew:kSqlInteger,
                                 kDisplayName:kSqlText};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SYMBOL_HOT_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kCompanyId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kName:kSqlText,
                                 kState:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kSource:kSqlText,
                                 kEnable:kSqlInteger,
                                 kBaseCurrency:kSqlText,
                                 kProfitcurrency:kSqlText,
                                 kCataId:kSqlInteger,
                                 kSequence:kSqlInteger,
                                 kPinYin:kSqlText,
                                 kHolidayCataId:kSqlInteger,
                                 kScheduleCataId:kSqlInteger,
                                 kDigits:kSqlInteger,
                                 kPipsRatio:kSqlInteger,
                                 kVolumesMin:kSqlInteger,
                                 kVolumesStep:kSqlInteger,
                                 kVolumsMax:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kContractSize:kSqlInteger,
                                 kStopLevel:kSqlInteger,
                                 kMaxStopLevel:kSqlInteger,
                                 kSwapDaysPerYear:kSqlInteger,
                                 kSpread:kSqlInteger,
                                 kSpreadBalance:kSqlInteger,
                                 kPositionVolumeMax:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kExpiryTime:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kContractSizeNew:kSqlInteger,
                                 kDisplayName:kSqlText};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SCHEDULE_CATA_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kMarginType:kSqlText,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kName:kSqlText,
                                 kMarginType:kSqlText,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SCHEDULE_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kStartTime:kSqlInteger,
                                 kEndTime:kSqlInteger,
                                 kDayOfWeek:kSqlInteger,
                                 kMarginSet:kSqlInteger,
                                 kHoliday:kSqlInteger,
                                 kEnable:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kNeedStopout:kSqlInteger,
                                 kCancelOrder:kSqlInteger,
                                 kScheduleCataId:kSqlInteger,
                                 kState:kSqlInteger,
                                 kMarginType:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kStartTime:kSqlInteger,
                                 kEndTime:kSqlInteger,
                                 kDayOfWeek:kSqlInteger,
                                 kMarginSet:kSqlInteger,
                                 kHoliday:kSqlInteger,
                                 kEnable:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kNeedStopout:kSqlInteger,
                                 kCancelOrder:kSqlInteger,
                                 kScheduleCataId:kSqlInteger,
                                 kState:kSqlInteger,
                                 kMarginType:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_QUOTE_DELAY_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kUserId:kSqlInteger,
                                 KSymbolCataId:kSqlInteger,
                                 kLimitDate:kSqlInteger,
                                 kState:kSqlInteger,
                                 kType:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kUUID:kSqlInteger,
                                 kUUTime:kSqlText,
                                 kUserId:kSqlInteger,
                                 KSymbolCataId:kSqlInteger,
                                 kLimitDate:kSqlInteger,
                                 kState:kSqlInteger,
                                 kType:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_BROWSER_HISTORY:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUTime:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUTime:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SYMBOL_MARKETID:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kMarketId:kSqlInteger,
                                kID:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_NETWORK_FLOW:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kNetworkFlowData:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kCategory:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kNetworkFlowData:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kCategory:kSqlInteger,
                                 kSums:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_LANGUAGE_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kNameSpace:kSqlText,
                                 kKeyId:kSqlInteger,
                                 kValue:kSqlText,
                                 kCountry:kSqlText,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kNameSpace:kSqlText,
                                 kKeyId:kSqlInteger,
                                 kValue:kSqlText,
                                 kCountry:kSqlText,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SYMBOL_MARGIN_SET_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kMarginType:kSqlText,
                                 kRangeNum:kSqlInteger,
                                 kPercent:kSqlInteger,
                                 kState:kSqlInteger,
                                 kRangeLeft:kSqlInteger,
                                 kRangeRight:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kMarginType:kSqlText,
                                 kRangeNum:kSqlInteger,
                                 kPercent:kSqlInteger,
                                 kState:kSqlInteger,
                                 kRangeLeft:kSqlInteger,
                                 kRangeRight:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_ACCOUNT_GROUP_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kServerId:kSqlInteger,
                                 kName:kSqlText,
                                 kCurrency:kSqlText,
                                 kMarginCallLevel:kSqlInteger,
                                 kStopOutLevel:kSqlInteger,
                                 kCompanyId:kSqlInteger,
                                 kSpread:kSqlInteger,
                                 kType:kSqlInteger,
                                 kEnable:kSqlInteger,
                                 kState:kSqlInteger,
                                 kLpuserid:kSqlInteger,
                                 kClientType:kSqlInteger,
                                 kOptions:kSqlInteger,
                                 kDefaultType:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kServerId:kSqlInteger,
                                 kName:kSqlText,
                                 kCurrency:kSqlText,
                                 kMarginCallLevel:kSqlInteger,
                                 kStopOutLevel:kSqlInteger,
                                 kCompanyId:kSqlInteger,
                                 kSpread:kSqlInteger,
                                 kType:kSqlInteger,
                                 kEnable:kSqlInteger,
                                 kState:kSqlInteger,
                                 kLpuserid:kSqlInteger,
                                 kClientType:kSqlInteger,
                                 kOptions:kSqlInteger,
                                 kDefaultType:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_ACCOUNTGROUPSYMBOLCATA_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kAccGroupId:kSqlInteger,
                                 KSymbolCataId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kCommission:kSqlInteger,
                                 kState:kSqlInteger,
                                 kType:kSqlInteger,
                                 kSpread:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kAccGroupId:kSqlInteger,
                                 KSymbolCataId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kCommission:kSqlInteger,
                                 kState:kSqlInteger,
                                 kType:kSqlInteger,
                                 kSpread:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SEARCH_BROWSE_HISTORY:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kName:kSqlText,
                                 kEnable:kSqlInteger,
                                 kUserId:kSqlInteger,
                                 kAccountId:kSqlInteger,
                                 kMarketName:kSqlText,
                                 kLanguageName:kSqlText,
                                 kSource:kSqlText,
                                 kSymbolSubId:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kName:kSqlText,
                                 kEnable:kSqlInteger,
                                 kAccountId:kSqlInteger,
                                 kUserId:kSqlInteger,
                                 kMarketName:kSqlText,
                                 kLanguageName:kSqlText,
                                 kSource:kSqlText,
                                 kSymbolSubId:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SYMBOL_LABEL_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kName:kSqlText,
                                 kUUID:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kColour:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kName:kSqlText,
                                 kUUID:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kColour:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_HOLIDAY_MARGIN_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kHolidayId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kStatus:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kHolidayId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kStatus:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_SCHEDULE_MARGIN_LIST:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kScheduleId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kStatus:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kScheduleId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kMarginType:kSqlInteger,
                                 kStatus:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_YESTERDAY_PRICE:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kMarketId:kSqlInteger,
                                 kPrice:kSqlDouble,
                                 kDeadTimeStamp:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kMarketId:kSqlInteger,
                                 kPrice:kSqlDouble,
                                 kDeadTimeStamp:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_QUOTE_PRICE:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kQuoteData:kSqlOleObject};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kQuoteData:kSqlOleObject};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_DEEP_PRICE:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                kBuyPrice:kSqlDouble,
                                kBuyVolume:kSqlInteger,
                                kSellPrice:kSqlDouble,
                                kSellVolume:kSqlInteger,
                                kDeepIndex:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kBuyPrice:kSqlDouble,
                                 kBuyVolume:kSqlInteger,
                                 kSellPrice:kSqlDouble,
                                 kSellVolume:kSqlInteger,
                                 kDeepIndex:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_GROUP_SYM_CATA:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kGroupId:kSqlInteger,
                                 kLevel:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kGroupId:kSqlInteger,
                                 kLevel:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
                
            case CMD_GROUP_SYM:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kGroupId:kSqlInteger,
                                 kCataId:kSqlInteger,
                                 kNoDispaly:kSqlInteger
                                 };
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kGroupId:kSqlInteger,
                                 kCataId:kSqlInteger,
                                 kNoDispaly:kSqlInteger
                                 };
                        break;
                    default:
                        break;
                }
            }
                break;
            case CMD_EOD_TIME_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kServerId:kSqlInteger,
                                 kWeekDay:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kType:kSqlInteger,
                                 kState:kSqlInteger
                                 };
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kServerId:kSqlInteger,
                                 kWeekDay:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kType:kSqlInteger,
                                 kState:kSqlInteger
                                 };
                        break;
                    default:
                        break;
                }
                break;
            case CMD_SECURE_DEV_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kUserId:kSqlInteger,
                                 kDevToken:kSqlText,
                                 kDevName:kSqlText,
                                 kLastLoginTime:kSqlInteger,
                                 kVerifyTime:kSqlInteger,
                                 kVerifyMode:kSqlInteger,
                                 kVerifyNo:kSqlText,
                                 kPass:kSqlInteger,
                                 kState:kSqlInteger
                                 };
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kUserId:kSqlInteger,
                                 kDevToken:kSqlText,
                                 kDevName:kSqlText,
                                 kLastLoginTime:kSqlInteger,
                                 kVerifyTime:kSqlInteger,
                                 kVerifyMode:kSqlInteger,
                                 kVerifyNo:kSqlText,
                                 kPass:kSqlInteger,
                                 kState:kSqlInteger
                                 };
                        break;
                    default:
                        break;
                }
                break;
            case CMD_GROUP_SYMBOL_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kAccGroupId:kSqlInteger,
                                 KSymbolCataId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kCommission:kSqlText,
                                 kSpread:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kVolumesMin:kSqlInteger,
                                 kVolumsMax:kSqlInteger,
                                 kVolumesStep:kSqlInteger,
                                 kVolumesSidemax:kSqlInteger,
                                 kLongSwap:kSqlInteger,
                                 kShortSwap:kSqlInteger,
                                 kNormalMarginType:kSqlInteger,
                                 kWeekendMarginType:kSqlInteger,
                                 kHolidayMarginType:kSqlInteger,
                                 kState:kSqlInteger,
                                 kCloseOnly:kSqlInteger,
                                 kNoDispaly:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kExpiryTime:kSqlInteger,
                                 kLpuserid:kSqlInteger,
                                 kLpstatus:kSqlInteger,
                                 kLpExpiryTime:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kAccGroupId:kSqlInteger,
                                 KSymbolCataId:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kCommission:kSqlText,
                                 kSpread:kSqlInteger,
                                 KTradable:kSqlInteger,
                                 kVolumesMin:kSqlInteger,
                                 kVolumsMax:kSqlInteger,
                                 kVolumesStep:kSqlInteger,
                                 kVolumesSidemax:kSqlInteger,
                                 kLongSwap:kSqlInteger,
                                 kShortSwap:kSqlInteger,
                                 kNormalMarginType:kSqlInteger,
                                 kWeekendMarginType:kSqlInteger,
                                 kHolidayMarginType:kSqlInteger,
                                 kState:kSqlInteger,
                                 kCloseOnly:kSqlInteger,
                                 kNoDispaly:kSqlInteger,
                                 kStartTime:kSqlInteger,
                                 kExpiryTime:kSqlInteger,
                                 kLpuserid:kSqlInteger,
                                 kLpstatus:kSqlInteger,
                                 kLpExpiryTime:kSqlInteger};
                        break;
                    default:
                        break;
                }
                break;
            case CMD_GROUP_SYMBOLCATA_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kAccGroupId:kSqlInteger,
                                 kCataId:kSqlInteger,
                                 kCommission:kSqlText,
                                 kNormalMarginType:kSqlInteger,
                                 kWeekendMarginType:kSqlInteger,
                                 kHolidayMarginType:kSqlInteger,
                                 kState:kSqlInteger,
                                 kCommissionType:kSqlInteger
                                 };
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kAccGroupId:kSqlInteger,
                                 kCataId:kSqlInteger,
                                 kCommission:kSqlText,
                                 kNormalMarginType:kSqlInteger,
                                 kWeekendMarginType:kSqlInteger,
                                 kHolidayMarginType:kSqlInteger,
                                 kState:kSqlInteger,
                                 kCommissionType:kSqlInteger
                                 };
                        break;
                    default:
                        break;
                }
                break;
            case CMD_LPCHANNEL_ACCOUNT_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kChannelid:kSqlInteger,
                                 kLpCompanyid:kSqlInteger,
                                 kRefAccid:kSqlInteger,
                                 kToken:kSqlText,
                                 kName:kSqlText,
                                 kIp:kSqlText,
                                 kPort:kSqlInteger,
                                 kState:kSqlInteger,
                                 kTargetCompid:kSqlText,
                                 kSenderCompid:kSqlText,
                                 kSenderSubid:kSqlText,
                                 kDeliverToCompid:kSqlText};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kChannelid:kSqlInteger,
                                 kLpCompanyid:kSqlInteger,
                                 kRefAccid:kSqlInteger,
                                 kToken:kSqlText,
                                 kName:kSqlText,
                                 kIp:kSqlText,
                                 kPort:kSqlInteger,
                                 kState:kSqlInteger,
                                 kTargetCompid:kSqlText,
                                 kSenderCompid:kSqlText,
                                 kSenderSubid:kSqlText,
                                 kDeliverToCompid:kSqlText};
                        break;
                    default:
                        break;
                }
                break;
            case CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kLpchaccid:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kLpspread:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kLpchaccid:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kLpspread:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
                break;
            case CMD_LPCHANNEL_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kName:kSqlText,
                                 kType:kSqlInteger,
                                 kIp:kSqlText,
                                 kPort:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kName:kSqlText,
                                 kType:kSqlInteger,
                                 kIp:kSqlText,
                                 kPort:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
                break;
            case CMD_LPCHANNEL_SYMBOL_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kLpCompanyid:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kChspread:kSqlInteger,
                                 kChannelid:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kLpCompanyid:kSqlInteger,
                                 kSymbolId:kSqlInteger,
                                 kChspread:kSqlInteger,
                                 kChannelid:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
                break;
            case CMD_LPIB_BIND_LIST:
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kIbCompanyid:kSqlInteger,
                                 kLpCompanyid:kSqlInteger,
                                 kLpchaccToken:kSqlText,
                                 kLpchaccid:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{
                                 kID:kSqlInteger,
                                 kUUID:kSqlInteger,
                                 kUUTime:kSqlInteger,
                                 kIbCompanyid:kSqlInteger,
                                 kLpCompanyid:kSqlInteger,
                                 kLpchaccToken:kSqlText,
                                 kLpchaccid:kSqlInteger,
                                 kState:kSqlInteger};
                        break;
                    default:
                        break;
                }
                break;
            case CMD_DB_VERSION:{
                switch (type) {
                    case DataBaseOperateTypeSave:
                        return @{kDBVersion:kSqlInteger};
                        break;
                    case DataBaseOperateTypeQuery:
                        return @{kDBVersion:kSqlInteger};
                        break;
                    default:
                        break;
                }
            }
                break;
            default:
                break;
        }
    } else if (serverType == IXDBServerTypeQuote) {
        if (cmd == CMD_KAYLINE_MINUTE ||
            cmd == CMD_KAYLINE_FIVE_MINUTE ||
            cmd == CMD_KAYLINE_DAY) {
            switch (type) {
                case DataBaseOperateTypeSave:
                    return @{kNTime:kSqlInteger,
                             kAmount:kSqlText,
                             kNOpenPrice:kSqlText,
                             kClosePrice:kSqlText,
                             kHighPrice:kSqlText,
                             kLowPrice:kSqlText,
                             kVolume:kSqlText,
                             kSymbolId:kSqlInteger,
                             kMaxNum:kSqlInteger};
                    break;
                case DataBaseOperateTypeQuery:
                    return @{kID:kSqlInteger,
                             kNTime:kSqlInteger,
                             kAmount:kSqlText,
                             kNOpenPrice:kSqlText,
                             kClosePrice:kSqlText,
                             kHighPrice:kSqlText,
                             kLowPrice:kSqlText,
                             kVolume:kSqlText,
                             kSymbolId:kSqlInteger,
                             kMaxNum:kSqlInteger};
                    break;
                default:
                    break;
            }
        }
    }
    return @{};
}

#pragma mark 执行sql语句
+ (BOOL)dealWithSelfDefineData:(NSDictionary *)infoDic
               protocolCommand:(uint16)cmd
                   sqlIndexKey:(NSDictionary *)indexDic
           dataBaseOperateType:(DataBaseOperateType)type
                 conditionType:(ConditionQueryType)condType
                        userId:(uint64_t)userId
                     accountId:(uint64_t)accountId
{
    NSString *exeSql = [IXDBModel baseSelfDefineDataToSql:infoDic protocolCommand:cmd sqlIndexKey:indexDic dataBaseOperateType:type conditionType:condType userId:userId accountId:accountId];
    
    NSString *createSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    if (type == DataBaseOperateTypeSave) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd] && infoDic) {
            return [[IXDBManager sharedInstance] save:exeSql];
        } else {
            return NO;
        }
    } else if (type == DataBaseOperateTypeDelete) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd]) {
            return [[IXDBManager sharedInstance] deleteObject:exeSql];
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

#pragma mark  批量执行sql语句
+ (BOOL)dealWithBatchSelfDefineData:(NSArray *)infoArr
                    protocolCommand:(uint16)cmd
                        sqlIndexKey:(NSDictionary *)indexDic
                dataBaseOperateType:(DataBaseOperateType)type
                      conditionType:(ConditionQueryType)condType
                             userId:(uint64_t)userId
                          accountId:(uint64_t)accountId
{
    NSArray *exeSqlArr = [IXDBModel sqlSelfDefineToSqls:infoArr protocolCommand:cmd additonInfos:nil sqlIndexKey:indexDic dataBaseOperateType:type userId:userId accountId:accountId conditionType:condType];
    NSString *createSql = [IXDBModel tableNameToSqlProtocolCommand:cmd sqlIndexKey:indexDic userId:userId];
    if (type == DataBaseOperateTypeSave) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd]) {
            return [[IXDBManager sharedInstance] saveObjects:exeSqlArr];
        } else {
            return NO;
        }
    } else if (type == DataBaseOperateTypeDelete) {
        if ([[IXDBManager sharedInstance] createDataTableSql:createSql protocolCommand:cmd]) {
            return [[IXDBManager sharedInstance] deleteObjects:exeSqlArr];
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

#pragma mark  清空表数据
+ (BOOL)deleteDBTable:(NSString *)tableName
{
   return  [[IXDBManager sharedInstance] deleteTableTableName:tableName];
}

#pragma mark 中文转拼音
+ (NSString *)chineseToPinyin:(NSString *)cStr
{
    if (cStr.length) {
        NSString *pStr = [[NSMutableString alloc] initWithString:cStr];
        CFStringTransform((__bridge CFMutableStringRef)pStr, 0, kCFStringTransformMandarinLatin, NO);
        CFStringTransform((__bridge CFMutableStringRef)pStr, 0, kCFStringTransformStripCombiningMarks, NO);
        if (pStr) {
            pStr = [pStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        return pStr;
    }
    return nil;
}

@end
