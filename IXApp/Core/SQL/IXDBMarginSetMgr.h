//
//  IXDBMarginSetMgr.h
//  IXApp
//
//  Created by Evn on 16/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBMarginSetMgr : NSObject

/**
 *  保存语言信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveMarginSetInfo:(id)model;

/**
 *  批量保存语言信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchMarginSetInfos:(NSArray *)modelArr;

/**
 *  查询保证金数据
 *  @param marginType 保证金类型
 *
 */
+ (NSArray *)queryMarginSetByMarginType:(NSString *)marginType;

/**
 *  查询保证金UUID最大值数据
 *
 */
+ (NSDictionary *)queryMarginSetUUIDMax;

/**
 *  删除保证金信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteMarginSetInfo:(id)model;

@end
