//
//  IXDBScheduleCataMgr.m
//  IXApp
//
//  Created by Evn on 16/11/23.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBScheduleCataMgr.h"
#import "IXDBModel.h"
#import "IxProtoScheduleCata.pbobjc.h"

@implementation IXDBScheduleCataMgr

#pragma mark   保存时间表分类信息
+ (BOOL)saveScheduleCataInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SCHEDULE_CATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存时间表分类信息
+ (BOOL)saveBatchScheduleCataInfos:(NSArray *)modelArr
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SCHEDULE_CATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
   
}

#pragma mark  查询时间表分类信息
+ (NSArray *)queryAllScheduleCataInfo {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_company new] protocolCommand:CMD_SCHEDULE_CATA_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  根据交易时间分类ID查询交易时间信息
+ (NSDictionary *)queryScheduleCataByScheduleCataId:(uint64_t)scheduleCataId {
    
    NSDictionary *condDic = @{
                              kID:@(scheduleCataId),
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_schedule_cata new] protocolCommand:CMD_SCHEDULE_CATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询时间表分类UUID最大值
+ (NSDictionary *)queryScheduleCataUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SCHEDULE_CATA_LIST userId:0];
}

#pragma mark  删除时间表分类
+ (BOOL)deleteScheduleCata:(id)model
{
    item_schedule_cata *pb = [item_schedule_cata new];
    pb.id_p = ((proto_schedule_cata_delete *)model).id_p;
    return [IXDBModel dealWithDataDelete:pb protocolCommand:CMD_SCHEDULE_CATA_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
