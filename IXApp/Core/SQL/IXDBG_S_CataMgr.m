//
//  IXDBG_S_CataMgr.m
//  IXApp
//
//  Created by Evn on 2017/8/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBG_S_CataMgr.h"
#import "IXDBModel.h"
#import "IXDBAccountMgr.h"
@implementation IXDBG_S_CataMgr

#pragma mark 保存账户组产品分类信息
+ (BOOL)saveG_S_CataInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_GROUP_SYMBOLCATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存账户组产品分类信息
+ (BOOL)saveBatchG_S_CataInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_GROUP_SYMBOLCATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询账户组产品分类信息
+ (NSDictionary *)queryG_S_CataByAccountGroupId:(uint64_t)accGroupId
                                   symbolCataId:(uint64_t)symCataId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kAccGroupId:@(accGroupId),
                             kCataId:@(symCataId)
                             };
    __block NSArray *allArr = [IXDBModel dealWithDataCheck:[item_group_symbol_cata new] protocolCommand:CMD_GROUP_SYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        NSArray *ixArr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                          [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:0];
        
        if (ixArr.count < 2) {
            return nil;
        }
        __block NSMutableArray *arr = [ixArr mutableCopy];
        __block NSInteger rmindex = -1;
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger accGroupId_temp = [obj[@"accountGroupId"] integerValue];
            if (accGroupId_temp == accGroupId) {
                rmindex = idx;
            }
        }];
        if (rmindex >= 0) {
            [arr removeObjectAtIndex:rmindex];
        }
        
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *upperDic = obj;
            //            NSInteger realAccId = [realDic[@"id"] integerValue];
            NSInteger upperAccGroupId = [upperDic[@"accountGroupId"] integerValue];
            NSDictionary *paramDic = @{
                                       kAccGroupId:@(upperAccGroupId),
                                       kCataId:@(symCataId)
                                       };
            
            allArr = [IXDBModel dealWithDataCheck:[item_group_symbol_cata new] protocolCommand:CMD_GROUP_SYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:paramDic userId:0 conditionType:ConditionQueryTypeValue2];
            if (allArr) {
                *stop = YES;
            }
        }];
        if (allArr.count) {
            return allArr[0];
        }
    }
    return nil;
}

#pragma mark 删除账户组产品分类信息
+ (BOOL)deleteG_S_CataInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_GROUP_SYMBOLCATA_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}


#pragma mark  批量删除账户组产品分类信息
+ (BOOL)batchDeleteG_S_CataInfo:(GPBUInt64Array *)modelArr
{
    BOOL ret = NO;
    for (int i = 0; i < modelArr.count; i++) {
        item_group_symbol_cata *group_symbol_cata = [[item_group_symbol_cata alloc] init];
        group_symbol_cata.id_p = [modelArr valueAtIndex:i];
        ret = [IXDBModel dealWithDataDelete:group_symbol_cata protocolCommand:CMD_GROUP_SYMBOLCATA_LIST userId:0 conditionType:ConditionQueryTypeValue0];
        if (!ret) {
            break;
        }
    }
    return ret;
}

#pragma mark  查询账户组产品分类UUID最大值数据
+ (NSDictionary *)queryG_S_CataUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_GROUP_SYMBOLCATA_LIST userId:0];
}


@end
