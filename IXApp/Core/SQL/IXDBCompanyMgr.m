//
//  IXDBCompanyMgr.m
//  IXApp
//
//  Created by Evn on 16/11/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBCompanyMgr.h"
#import "IXDBModel.h"

@implementation IXDBCompanyMgr

#pragma mark   保存账户信息
+ (BOOL)saveCompanyInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_COMPANY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_COMPANY_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存产品分类信息
+ (BOOL)saveBatchCompanyInfos:(NSArray *)modelArr
{
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_COMPANY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_COMPANY_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark   查询公司信息
+ (NSDictionary *)queryCompanyInfoByCompanyId:(uint64_t)companyId
{
    NSDictionary *condDic = @{
                              kCompanyId:@(companyId),
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_COMPANY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_company new] protocolCommand:CMD_COMPANY_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        return nil;
    }
}

#pragma mark  查询公司UUID最大值
+ (NSDictionary *)queryCompanyUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_COMPANY_LIST userId:0];
}

#pragma mark 删除公司信息
+ (BOOL)deleteCompanyInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_COMPANY_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
