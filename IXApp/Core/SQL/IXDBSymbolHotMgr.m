//
//  IXDBSymbolHotMgr.m
//  IXApp
//
//  Created by ixiOSDev on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBSymbolHotMgr.h"
#import "IXDBModel.h"

@implementation IXDBSymbolHotMgr

#pragma mark 热门保存产品信息
+ (BOOL)saveSymbolHotInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_HOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SYMBOL_HOT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存热门产品信息
+ (BOOL)saveBatchSymbolHotInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_HOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SYMBOL_HOT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  分页查询热门产品信息
+ (NSArray *)querySymbolHotStartIndex:(uint64_t)sIndex
                               offset:(uint64_t)offset
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_HOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kStartIndex:@(sIndex),
                             kOffset:@(offset)
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_HOT_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
}


#pragma mark 查询热门产品信息
+ (NSArray *)queryAllSymbolsHot {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_HOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_symbol_hot new] protocolCommand:CMD_SYMBOL_HOT_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

+ (NSArray *)queryAllSymbolsHotAccountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_HOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kGroupId:@(accountGroupid),
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol_hot new] protocolCommand:CMD_SYMBOL_HOT_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1A];
}

+ (NSArray *)queryAllSymbolsHotSymInfoAccountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_HOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kGroupId:@(accountGroupid),
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol_hot new] protocolCommand:CMD_SYMBOL_HOT_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1B];
}

+ (NSArray *)queryAllSymbolsHotSymCataInfoAccountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_HOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kGroupId:@(accountGroupid),
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol_hot new] protocolCommand:CMD_SYMBOL_HOT_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1C];
}

#pragma mark  查询热门产品UUID最大值
+ (NSDictionary *)querySymbolHotUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SYMBOL_HOT_LIST userId:0];
}

#pragma mark  查询产品marketId
+ (NSDictionary *)querySymbolMarketIdsBySymbolId:(uint64_t)symbolId {
    
    NSDictionary *dic = @{
                          @"symbolId":@(symbolId)
                          };
    NSArray *condArr = @[dic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_MARKETID serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_SYMBOL_MARKETID sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue1 userId:0 accountId:0];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  批量查询产品marketId
+ (NSArray *)queryBatchSymbolMarketIdsBySymbolIds:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_MARKETID serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_SYMBOL_MARKETID sqlIndexKey:indexDic queryCond:modelArr conditionType:ConditionQueryTypeValue1 userId:0 accountId:0];
}

@end
