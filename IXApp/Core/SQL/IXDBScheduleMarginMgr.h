//
//  IXDBScheduleMarginMgr.h
//  IXApp
//
//  Created by Evn on 16/12/10.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBScheduleMarginMgr : NSObject

/**
 *  保存交易时间margin
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveScheduleMarginInfo:(id)model;

/**
 *  批量保存交易时间margin
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchScheduleMarginInfos:(NSArray *)modelArr;

/**
 *  通过symbolId查询交易时间margin
 *  @param symbolId 保证金类型
 *  @param scheduleId 交易时间ID
 *
 */
+ (NSString *)queryScheduleMarginBySymbolId:(uint64_t)symbolId scheduleId:(uint64_t)scheduleId;

/**
 *  查询交易时间marginUUID最大值数据
 *
 */
+ (NSDictionary *)queryScheduleMarginUUIDMax;

/**
 *  删除交易时间margin信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteScheduleMarginInfo:(id)model;

@end
