//
//  IXDBYesterdayPriceMgr.h
//  IXApp
//
//  Created by Evn on 16/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBYesterdayPriceMgr : NSObject

/**
 *  保存昨收价信息
 *
 *  @param priceDic 昨收价
 *
 */
+ (BOOL)saveYesterdayPriceInfo:(NSDictionary *)priceDic;

/**
 *  批量保存昨收价信息
 *
 *  @param infoArr 昨收价
 *
 */
+ (BOOL)saveBatchYesterdayPriceInfo:(NSArray *)infoArr;

/**
 *  查询昨收价信息
 *  @param symbolId 产品ID
 *
 */
+ (NSDictionary *)queryYesterdayPriceBySymbolId:(uint64_t)symbolId;

@end
