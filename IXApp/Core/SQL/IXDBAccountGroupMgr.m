//
//  IXDBAccountGroupMgr.m
//  IXApp
//
//  Created by Evn on 16/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBAccountGroupMgr.h"
#import "IXDBModel.h"

@implementation IXDBAccountGroupMgr

#pragma mark 保存账户组信息
+ (BOOL)saveAccountGroupInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_GROUP_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_ACCOUNT_GROUP_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量账户组信息
+ (BOOL)saveBatchAccountGroupInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_GROUP_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_ACCOUNT_GROUP_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询账户组信息
+ (NSDictionary *)queryAccountGroupByAccountGroupId:(uint64_t)accGroupId
{
    NSDictionary *condDic = @{
                              kID:@(accGroupId)                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_GROUP_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_account_group new] protocolCommand:CMD_ACCOUNT_GROUP_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1A];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        return nil;
    }
}

#pragma mark  查询账户组信息
+ (NSArray *)queryAllAccountGroupByClientType:(uint32_t)clientType
                                         type:(uint32_t)type
{
    NSDictionary *condDic = @{
                              kClientType:@(clientType),
                              kType:@(type)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_GROUP_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_account_group new] protocolCommand:CMD_ACCOUNT_GROUP_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2A];
}


#pragma mark  查询账户组信息
+ (NSArray *)queryAllAccountGroupByClientType:(uint32_t)clientType
                                   wipeOffType:(uint32_t)type
{
    NSDictionary *condDic = @{
                              kClientType:@(clientType),
                              kType:@(type)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_GROUP_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_account_group new] protocolCommand:CMD_ACCOUNT_GROUP_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2B];
}

+ (NSString *)currencyCode
{
    NSString    * currencyCode = @"¥";
    NSDictionary    * dic = [IXDBAccountGroupMgr queryAccountGroupByAccountGroupId:[IXUserInfoMgr shareInstance].userLogInfo.account.accountGroupid];
    if ([dic[@"currency"] isKindOfClass:[NSString class]] &&
        [dic[@"currency"] isEqualToString:@"USD"]) {
        currencyCode = @"$";
    }
    
    return currencyCode;
}

#pragma mark  查询账户组信息
+ (NSArray *)queryAllAccountGroup
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_GROUP_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_account_group new] protocolCommand:CMD_ACCOUNT_GROUP_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}


#pragma mark  通过客户端类型查询账户组信息
+ (NSArray *)queryAllAccountGroupByClientType:(uint32_t)clientType
{
    NSDictionary *condDic = @{
                              kClientType:@(clientType)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNT_GROUP_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_account_group new] protocolCommand:CMD_ACCOUNT_GROUP_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark  查询用户UUID最大值数据
+ (NSDictionary *)queryAccountGroupUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_ACCOUNT_GROUP_LIST userId:0];
}

#pragma mark  删除账户组信息
+ (BOOL)deleteAccountGroup:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_ACCOUNT_GROUP_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}
@end
