//
//  IXDBScheduleMarginMgr.m
//  IXApp
//
//  Created by Evn on 16/12/10.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBScheduleMarginMgr.h"
#import "IXDBModel.h"

@implementation IXDBScheduleMarginMgr

#pragma mark 保存交易时间margin
+ (BOOL)saveScheduleMarginInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_MARGIN_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SCHEDULE_MARGIN_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存交易时间margin
+ (BOOL)saveBatchScheduleMarginInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_MARGIN_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SCHEDULE_MARGIN_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  通过symbolId查询交易时间margin
+ (NSString *)queryScheduleMarginBySymbolId:(uint64_t)symbolId scheduleId:(uint64_t)scheduleId {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_MARGIN_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kSymbolId:@(symbolId),
                             kScheduleId:@(scheduleId)
                             };
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_schedule_margin new] protocolCommand:CMD_SCHEDULE_MARGIN_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        
        NSDictionary *retDic = allArr[0];
        if (retDic) {
            
            return retDic[kMarginType];
        } else {
            
            return nil;
        }
    } else {
        
        return nil;
    }
}

#pragma mark  查询交易时间marginUUID最大值数据
+ (NSDictionary *)queryScheduleMarginUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SCHEDULE_MARGIN_LIST userId:0];
}

#pragma mark 删除交易时间margin信息
+ (BOOL)deleteScheduleMarginInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SCHEDULE_MARGIN_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
