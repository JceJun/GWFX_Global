//
//  IXDBScheduleMgr.h
//  IXApp
//
//  Created by Evn on 16/11/23.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBScheduleMgr : NSObject

/**
 *   保存时间表信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveScheduleInfo:(id)model;

/**
 *  批量保存时间表信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchScheduleInfos:(NSArray *)modelArr;

/**
 *  查询时间表信息
 *
 */
+ (NSArray *)queryAllScheduleInfo;

/**
 *  根据交易时间分类ID查询交易时间信息
 *  @param scheduleId 交易时间ID
 *
 */
+ (NSDictionary *)queryScheduleByScheduleId:(uint64_t)scheduleId;

/**
 *  根据交易时间分类ID查询交易时间信息
 *  @param scheduleCataId 交易时间分类ID
 *
 */
+ (NSArray *)queryScheduleByScheduleCataId:(uint64_t)scheduleCataId;


/**
 *  根据交易时间分类ID以及星期查询交易时间信息
 *  @param scheduleCataId 交易时间分类ID
 *
 */
+ (NSArray *)queryScheduleByScheduleCataId:(uint64_t)scheduleCataId dayOfWeek:(uint64_t)dayOfWeek;

/**
 *  通过当前时间查询假期
 *  @param currentTime 当前时间
 *  @param dayOfWeek 周几
 *
 */
+ (NSDictionary *)queryScheduleByCurrentTime:(uint64_t)currentTime dayOfWeek:(uint64_t)dayOfWeek;

/**
 *  查询时间表UUID最大值
 *
 */
+ (NSDictionary *)queryScheduleUUIDMax;

/**
 *  删除时间表
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteSchedule:(id)model;

@end
