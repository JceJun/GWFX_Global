//
//  IXDBSymbolCataMgr.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBSymbolCataMgr : NSObject

/**
 *  保存产品分类
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveSymbolCataInfo:(id)model;

/**
 *  批量保存产品分类信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchSymbolCatasInfos:(NSArray *)modelArr;

/**
 *  通过symbolCataId查询symbolCata记录
 *
 *  @param symbolCataId 产品分类ID
 *
 */
+ (NSDictionary *)querySymbolCataBySymbolCataId:(uint64_t)symbolCataId;

/**
 *  通过marketId查询symbolCata记录
 *
 *  @param marketId 产品市场ID
 *
 */
+ (uint64_t)querySymbolCataByMarketId:(uint64_t)marketId;

/**
 *  未设置查看权限通过parentId查询symbolCata记录
 *
 *  @param parentId 产品父类ID
 *
 */
+ (NSArray *)querySymbolCataByParentId:(uint64_t)parentId;

/**
 *  设置查看权限通过parentId和accountGroupid查询symbolCata记录
 *
 *  @param parentId 产品父类ID
 *  @param accountGroupid 产品父类ID
 *
 */
+ (NSArray *)querySymbolCataByParentId:(uint64_t)parentId accountGroupid:(uint64_t)accountGroupid;

/**
 *  查询产品分类信息
 *
 */
+ (NSArray *)querySymbolCataAllCatas;

/**
 *  查询产品分类UUID最大值
 *
 */
+ (NSDictionary *)querySymbolCatasUUIDMax;

/**
 *  删除产品分类信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteSymbolCataInfo:(id)model;

@end
