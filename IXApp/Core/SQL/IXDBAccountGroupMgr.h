//
//  IXDBAccountGroupMgr.h
//  IXApp
//
//  Created by Evn on 16/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBAccountGroupMgr : NSObject

/**
 *  保存账户组信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveAccountGroupInfo:(id)model;

/**
 *  批量保存账户组信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchAccountGroupInfos:(NSArray *)modelArr;

/**
 *  查询账户组信息
 *  @param accGroupId 账户组ID
 *
 */
+ (NSDictionary *)queryAccountGroupByAccountGroupId:(uint64_t)accGroupId;


/**
 获取货币标识

 @return ¥／$
 */
+ (NSString *)currencyCode;

/**
 *  查询账户组信息
 *
 */
+ (NSArray *)queryAllAccountGroup;

/**
 *  查询账户组信息
 *  @param clientType 客户端类型
 *
 */
+ (NSArray *)queryAllAccountGroupByClientType:(uint32_t)clientType;

/**
 *  查询账户组信息
 *  @param clientType 客户端类型
 *  @param type 账户类型
 *
 */
+ (NSArray *)queryAllAccountGroupByClientType:(uint32_t)clientType
                                               type:(uint32_t)type;

/**
 *  查询账户组信息
 *  @param clientType 客户端类型
 *  @param type 账户类型
 *
 */
+ (NSArray *)queryAllAccountGroupByClientType:(uint32_t)clientType
                                         wipeOffType:(uint32_t)type;


/**
 *  查询账户组UUID最大值数据
 *
 */
+ (NSDictionary *)queryAccountGroupUUIDMax;

/**
 *  删除账户组信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteAccountGroup:(id)model;

@end
