//
//  IXDBEodTimeMgr.h
//  IXApp
//
//  Created by Evn on 2017/6/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBEodTimeMgr : NSObject

/**
 *   保存结算时间信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveEodTimeInfo:(id)model;

/**
 *  批量保存结算时间信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchEodTimeInfos:(NSArray *)modelArr;

/**
 *  查询结算时间信息
 *  @param weekDay 周几
 *  @param type 类型
 *
 */
+ (NSDictionary *)queryEodTimeInfoByWeekDay:(uint64_t)weekDay
                                       type:(uint32)type;

/**
 *  删除结算时间信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)deleteEodTimeInfo:(id)model;

/**
 *  查询结算时间UUID最大值
 *
 */
+ (NSDictionary *)queryEodTimeUUIDMax;

@end
