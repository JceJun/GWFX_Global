//
//  IXDBQuoteDelayMgr.m
//  IXApp
//
//  Created by Evn on 17/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBQuoteDelayMgr.h"
#import "IXDBModel.h"

@implementation IXDBQuoteDelayMgr

#pragma mark   保存订阅行情信息
+ (BOOL)saveQuoteDelayInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_DELAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_QUOTE_DELAY_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存订阅行情信息
+ (BOOL)saveBatchQuoteDelayInfos:(NSArray *)modelArr
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_DELAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_QUOTE_DELAY_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询订阅行情信息
+ (NSArray *)queryAllQuoteDelayInfo{
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_DELAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_quote_delay new] protocolCommand:CMD_QUOTE_DELAY_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

+ (NSDictionary *)queryQuoteDelayByCataId:(uint64_t)cataId userid:(uint64_t)userid
{
    
    NSDictionary *condDic = @{
                              KSymbolCataId:@(cataId),
                              kUserId:@(userid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_DELAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_quote_delay new] protocolCommand:CMD_QUOTE_DELAY_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark 删除产品信息
+ (BOOL)deleteQuoteDelayInfo:(id)model {
    
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_QUOTE_DELAY_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询订阅行情UUID最大值
+ (NSDictionary *)queryQuoteDelayUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_QUOTE_DELAY_LIST userId:0];
}
@end
