//
//  IXDBLanguageMgr.m
//  IXApp
//
//  Created by Evn on 16/12/1.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBLanguageMgr.h"
#import "IXDBModel.h"

@implementation IXDBLanguageMgr

#pragma mark 保存语言信息
+ (BOOL)saveLanguageInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LANGUAGE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_LANGUAGE_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存语言信息
+ (BOOL)saveBatchLanguageInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LANGUAGE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_LANGUAGE_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询语言信息
+ (NSDictionary *)queryLanguageByCounrtry:(NSString *)country nameSpace:(NSString *)nameSpace{
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LANGUAGE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kCountry:country,
                             kNameSpace:nameSpace
                             };
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_language new] protocolCommand:CMD_LANGUAGE_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        @try{
            nameSpace = [IXDBLanguageMgr numberWithHexString:nameSpace];
            NSDictionary *addDic = @{
                                     kCountry:country,
                                     kNameSpace:nameSpace
                                     };
            NSArray *allArr = [IXDBModel dealWithDataCheck:[item_language new] protocolCommand:CMD_LANGUAGE_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
            return allArr[0];
        }@catch(NSException *exception) {
            return nil;
        }
    }
}

// 16进制转10进制
+ (NSString *)numberWithHexString:(NSString *)hexString{
    const char *hexChar = [hexString cStringUsingEncoding:NSUTF8StringEncoding];
    int hexNumber;
    sscanf(hexChar, "%x", &hexNumber);
    return [@((NSInteger)hexNumber) stringValue];
}

#pragma mark  查询用户UUID最大值数据
+ (NSDictionary *)queryLanguageUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_LANGUAGE_LIST userId:0];
}

@end
