//
//  IXDBSymbolLableMgr.m
//  IXApp
//
//  Created by Evn on 16/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBSymbolLableMgr.h"
#import "IXDBModel.h"

@implementation IXDBSymbolLableMgr

#pragma mark 保存产品标签信息
+ (BOOL)saveSymbolLableInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LABEL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SYMBOL_LABEL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存产品标签信息
+ (BOOL)saveBatchSymbolLableInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LABEL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SYMBOL_LABEL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询产品标签信息
+ (NSArray *)querySymbolLableBySymbolId:(uint64_t)symbolId {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LABEL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kSymbolId:@(symbolId)
                             };
    return  [IXDBModel dealWithDataCheck:[item_symbol_label new] protocolCommand:CMD_SYMBOL_LABEL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark 删除标签信息
+ (BOOL)deleteLableInfo:(id)model {
    
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SYMBOL_LABEL_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询用户UUID最大值数据
+ (NSDictionary *)querySymbolLableUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SYMBOL_LABEL_LIST userId:0];
}

@end
