//
//  IXDBCompanyMgr.h
//  IXApp
//
//  Created by Evn on 16/11/15.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBCompanyMgr : NSObject

/**
 *   保存公司信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveCompanyInfo:(id)model;

/**
 *  批量保存公司信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchCompanyInfos:(NSArray *)modelArr;

/**
 *  查询公司信息
 *  @param companyId 公司ID
 *
 */
+ (NSDictionary *)queryCompanyInfoByCompanyId:(uint64_t)companyId;

/**
 *  查询公司UUID最大值
 *  @param userId 用户ID
 *
 */
+ (NSDictionary *)queryCompanyUUIDMax;

/**
 *  删除公司信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteCompanyInfo:(id)model;

@end
