//
//  IXDBSymbolCataMgr.m
//  IXApp
//
//  Created by ixiOSDev on 16/11/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBSymbolCataMgr.h"
#import "IXDBModel.h"

@implementation IXDBSymbolCataMgr

#pragma mark  保存产品分类
+ (BOOL)saveSymbolCataInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SYMBOL_CATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存产品分类信息
+ (BOOL)saveBatchSymbolCatasInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SYMBOL_CATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  通过symbolCataId查询symbolCata记录
+ (NSDictionary *)querySymbolCataBySymbolCataId:(uint64_t)symbolCataId {
    
    NSDictionary *addDic = @{
                             kID:@(symbolCataId)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_symbol_cata new]  protocolCommand:CMD_SYMBOL_CATA_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count >0) {
        
        return  allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark 通过marketId查询symbolCata记录
+ (uint64_t)querySymbolCataByMarketId:(uint64_t)marketId {
    
    NSDictionary *addDic = @{
                             kMarketId:@(marketId)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_symbol_cata new]  protocolCommand:CMD_SYMBOL_CATA_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count >0) {
        
        NSDictionary *cataDic = allArr[0];
        if (cataDic && cataDic.count > 0) {
            
            return [cataDic[kID] doubleValue];
        } else {
            
            return 0;
        }
    } else {
        
        return 0;
    }
}

#pragma mark   通过parentId查询symbolCata记录
+ (NSArray *)querySymbolCataByParentId:(uint64_t)parentId {
    
    NSDictionary *addDic = @{
                             kparentId:@(parentId)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_symbol_cata new]  protocolCommand:CMD_SYMBOL_CATA_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark  通过parentId和accountGroupid查询symbolCata记录
+ (NSArray *)querySymbolCataByParentId:(uint64_t)parentId accountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *addDic = @{
                             kparentId:@(parentId),
                             kAccGroupId:@(accountGroupid)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_symbol_cata new]  protocolCommand:CMD_SYMBOL_CATA_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
}

#pragma mark  查询产品分类信息
+ (NSArray *)querySymbolCataAllCatas {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_symbol_cata new]  protocolCommand:CMD_SYMBOL_CATA_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询产品分类UUID最大值
+ (NSDictionary *)querySymbolCatasUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SYMBOL_CATA_LIST userId:0];
}

#pragma mark 删除产品信息
+ (BOOL)deleteSymbolCataInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SYMBOL_CATA_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
