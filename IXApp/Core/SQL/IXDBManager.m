//
//  IXDataBaseManager.m
//  IXFMDB
//
//  Created by ixiOSDev on 16/11/1.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#import "IXDBManager.h"
#import "IXCpyConfig.h"

@interface IXDBManager ()

@property (nonatomic,retain)FMDatabaseQueue *dbQueue;
@end

@implementation IXDBManager

+ (id)sharedInstance
{
    static IXDBManager *sharedInstace = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        if (!sharedInstace) {
            sharedInstace = [[self alloc]init];
            NSString *dbPath = [sharedInstace documentDirectory];
            dbPath = [dbPath stringByAppendingPathComponent:kDataBaseName];
            dbPath = [IXDBManager copyfilePath:dbPath];
            DLog(@"*********dbPath = %@************",dbPath);
            
            sharedInstace.dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
            [sharedInstace.dbQueue close];
        }
    });
    return sharedInstace;
}

#pragma mark 处理数据库文件路径拷贝
+ (NSString *)copyfilePath:(NSString *)dbPath
{
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isExist = [fm fileExistsAtPath:dbPath];
    if (!isExist)
    {
        NSString *backupDbPath = [[NSBundle mainBundle]
                                  pathForResource:kPlatformPrefix
                                  ofType:kDataBaseSuffix];
        [fm copyItemAtPath:backupDbPath toPath:dbPath error:nil];
        return backupDbPath;
    } else {
        return dbPath;
    }
}
#pragma mark 初始化数据库
- (BOOL)initDataBaseDataName:(NSString *)dbName
{
    if (!dbName.length) {
        return NO;
    }
    return YES;
}

#pragma mark 是否存在数据库
- (BOOL)isExistDataBase:(NSString *)dbName
{
    NSString *localPath = [self documentDirectory];
    NSString *filePath = [localPath  stringByAppendingPathComponent:dbName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:filePath];
}

#pragma mark 是否存在数据表
- (BOOL)isExistTable:(NSString *)dbName
            dataBase:(FMDatabase *)db
{
    NSString *existsSql = [NSString stringWithFormat:@"select count(name) as countNum from sqlite_master where type = 'table' and name = '%@'", dbName ];
    FMResultSet *rs = [db executeQuery:existsSql];
    if ([rs next]) {
        NSInteger count = [rs intForColumn:@"countNum"];
        return count;
    }
    return NO;
}

#pragma mark 创建数据表
- (BOOL)createDataTableSql:(NSString *)data
                 tableName:(NSString *)tableName
{
    __block BOOL ret = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if ([db open]) {
            if ([self isExistTable:tableName dataBase:db]) {
                ret = YES;
            }
            else {
                ret = [db executeUpdate:data];
            }
            [db close];
        }
    }];
    return ret;
}

- (BOOL)createDataTableSql:(NSString *)data
           protocolCommand:(uint16)cmd
{
    return [self createDataTableSql:data tableName:[IXDBManager protocolCommandToTableName:cmd]];
}

#pragma mark  删除数据库
- (BOOL)deleteDataBaseDataName:(NSString *)dbName
{
    BOOL ret = NO;
    NSString *dbPath = [self documentDirectory];
    dbPath = [dbPath stringByAppendingPathComponent:dbName];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if ([fileMgr fileExistsAtPath:dbPath]) {
        ret = [fileMgr removeItemAtPath:dbPath error:nil];
    }
    return ret;
}

#pragma mark 删除表
- (BOOL)deleteTableTableName:(NSString *)tableName
{
    __block BOOL ret = NO;
    NSString *sql = [NSString stringWithFormat:@"delete from %@",tableName];
    if (tableName.length > 0) {
        [_dbQueue inDatabase:^(FMDatabase *db) {
            if ([db open]) {
                if ([self isExistTable:tableName dataBase:db]) {
                    ret = [db executeUpdate:sql];
                } else {
                    ret = YES;
                }
                [db close];
            }
        }];
    }
    return ret;
}

#pragma mark 保存单个数据

- (BOOL)save:(NSString *)data
{
    __block BOOL ret = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if ([db open]) {
            ret = [db executeUpdate:data];
            [db close];
        }
    }];
    return ret;
}

#pragma mark 批量保存数据
- (BOOL)saveObjects:(NSArray *)dataArr
{
    __block BOOL ret = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if ([db open]) {
            [db beginTransaction];
            for (int i = 0; i < dataArr.count; i++) {
                NSString *sql = dataArr[i];
                ret = [db executeUpdate:sql];
                if (!ret) {
                    [db rollback];
                    break;
                }
            }
            [db commit];
        }
    }];
    return ret;
}

#pragma mark 更新单个数据
- (BOOL)update:(NSString *)data
{
    return [self dealWithData:data];
}

#pragma mark 批量更新数据
- (BOOL)updateObjects:(NSArray *)dataArr
{
    return [self dealWithBatchDatas:dataArr];
}

#pragma mark 删除单条数据
- (BOOL)deleteObject:(NSString *)data
{
    return [self dealWithData:data];
}

#pragma mark 批量删除数据
- (BOOL)deleteObjects:(NSArray *)dataArr
{
    return [self dealWithBatchDatas:dataArr];
}

#pragma mark 通过条件删除数据
- (BOOL)deleteObjectsByAddtion:(NSString *)data
{
    return [self dealWithData:data];
}

#pragma mark 处理单个数据
- (BOOL)dealWithData:(NSString *)data
{
    return [self save:data];
}

#pragma mark 批量处理数据
- (BOOL)dealWithBatchDatas:(NSArray *)dataArr
{
    return [self saveObjects:dataArr];
}

#pragma mark 查询数据
- (NSArray *)checkObjects:(NSString *)data
                indexKeys:(NSDictionary *)indexDic {
    
    NSMutableArray *dataArr = [[NSMutableArray alloc]init];
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if ([db open]) {
            FMResultSet * rs = [db executeQuery:data];
            NSArray *allKeys = [indexDic allKeys];
            while (rs.next) {
                NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
                for (NSString *key in allKeys) {
                    NSString *value = [rs stringForColumn:key];
                    [dataDic setObject:[self dealWithNilString:value] forKey:[self dealWithNilString:key]];
                }
                if (dataDic.count) {
                    [dataArr addObject:dataDic];
                }
            }
            [db close];
        }
    }];
    return dataArr;
}

- (NSString *)dealWithNilString:(NSString *)str
{
    if (!str.length) {
        return @"";
    } else {
        return str;
    }
}

- (NSString *)documentDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark 获取表名
+ (NSString *)protocolCommandToTableName:(uint16)cmd
{
    NSString *retStr = [[NSString alloc]init];
    switch (cmd) {
        case CMD_USER_LOGIN_INFO:
            return kTableNameUserInfo;
            break;
        case CMD_ACCOUNT_LIST:
            return kTableNameAccount;
            break;
        case CMD_SYMBOL_CATA_LIST:
            return kTableNameSymbolCatas;
            break;
        case CMD_SYMBOL_LIST:
            return kTableNameSymbol;
            break;
        case CMD_POSITION_LIST:
            return kTableNamePosition;
            break;
        case CMD_HOLIDAY_CATA_LIST:
            return kTableNameHolidayCatas;
            break;
        case CMD_HOLIDAY_LIST:
            return kTableNameHoliday;
            break;
        case CMD_SYMBOL_SUB_LIST:
            return kTableNameSymbolSub;
            break;
        case CMD_SYMBOL_SUB_DELETE:
            return kTableNameSymbolSub;
            break;
        case CMD_SYMBOL_SUB_CATA_LIST:
            return kTableNameSymbolSubCata;
            break;
        case CMD_SYMBOL_HOT_LIST:
            return kTableNameSymbolHot;
            break;
        case CMD_COMPANY_LIST:
            return kTableNameCompany;
            break;
        case CMD_SCHEDULE_LIST:
            return kTableNameSchedule;
            break;
        case CMD_SCHEDULE_CATA_LIST:
            return kTableNameScheduleCata;
            break;
        case CMD_BROWSER_HISTORY:
            return kTableNameBrowserHistory;
            break;
        case CMD_KAYLINE_MINUTE:
            return kTableNameKayLineMinute;
            break;
        case CMD_KAYLINE_FIVE_MINUTE:
            return kTableNameKayLineFiveMinute;
            break;
        case CMD_KAYLINE_DAY:
            return kTableNameKayLineDay;
            break;
        case CMD_NETWORK_FLOW:
            return kTableNameNetworkFlow;
            break;
        case CMD_LANGUAGE_LIST:
            return kTableNameLanguage;
            break;
        case CMD_SYMBOL_MARGIN_SET_LIST:
            return kTableNameMarginSet;
            break;
        case CMD_ACCOUNT_GROUP_LIST:
            return kTableNameAccountGroup;
            break;
        case CMD_ACCOUNTGROUPSYMBOLCATA_LIST:
            return kTableNameAccountGroupSymCate;
            break;
        case CMD_SEARCH_BROWSE_HISTORY:
            return kTableNameSearchBrowse;
            break;
        case CMD_SYMBOL_LABEL_LIST:
            return kTableNameSymbolLable;
            break;
        case CMD_HOLIDAY_MARGIN_LIST:
            return kTableNameHolidayMargin;
            break;
        case CMD_SCHEDULE_MARGIN_LIST:
            return kTableNameScheduleMargin;
            break;
        case CMD_EOD_TIME_LIST:
            return KTableNameEodTime;
            break;
        case CMD_SECURE_DEV_LIST:
            return kTableNameSecureDev;
            break;
        case CMD_YESTERDAY_PRICE:
            return kTableNameYesterdayPrice;
            break;
        case CMD_QUOTE_PRICE:
            return kTableNameQuotePrice;
            break;
        case CMD_DEEP_PRICE:
            return kTableNameDeepPrice;
            break;
        case CMD_QUOTE_DELAY_LIST:
            return kTableNameQuoteDelay;
            break;
        case CMD_GROUP_SYM_CATA:
            return kTableNameGroupSymCata;
            break;
        case CMD_GROUP_SYM:
            return kTableNameGroupSym;
            break;
        case CMD_DB_VERSION:
            return kTableNameDBVersion;
            break;
        case CMD_GROUP_SYMBOL_LIST:
            return kTableNameGroupSymNew;
            break;
        case CMD_GROUP_SYMBOLCATA_LIST:
            return kTableNameGroupSymCataNew;
            break;
        case CMD_LPCHANNEL_ACCOUNT_LIST:
            return kTableNameLpchacc;
            break;
        case CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST:
            return kTableNameLpchaccSymbol;
            break;
        case CMD_LPCHANNEL_LIST:
            return kTableNameLpchannel;
            break;
        case CMD_LPCHANNEL_SYMBOL_LIST:
            return kTableNameLpchannelSymbol;
            break;
        case CMD_LPIB_BIND_LIST:
            return kTableNameLpibBind;
            break;
        default:
            break;
    }
    return retStr;
}

@end
