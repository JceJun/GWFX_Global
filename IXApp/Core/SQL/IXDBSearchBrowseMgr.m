//
//  IXDBSearchBrowseMgr.m
//  IXApp
//
//  Created by Evn on 16/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBSearchBrowseMgr.h"
#import "IXDBModel.h"

@implementation IXDBSearchBrowseMgr

#pragma mark   保存浏览历史信息
+ (BOOL)saveSearchBrowserHistoryInfoSymbolId:(NSDictionary *)infoDic
                                      userId:(uint64_t)userId
                                   accountId:(uint64_t)accountId;
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SEARCH_BROWSE_HISTORY serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:userId];
    return [IXDBModel dealWithSelfDefineData:infoDic protocolCommand:CMD_SEARCH_BROWSE_HISTORY sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:userId accountId:accountId];
}

#pragma mark  批量保存搜索浏览历史信息
+ (BOOL)saveBatchSearchBrowserHistoryInfos:(NSArray *)infoArr userId:(uint64_t)userId
 accountId:(uint64_t)accountId
{
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SEARCH_BROWSE_HISTORY serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:userId];
    return [IXDBModel dealWithBatchSelfDefineData:infoArr protocolCommand:CMD_SEARCH_BROWSE_HISTORY sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue0 userId:userId accountId:accountId];
}

#pragma mark  通过自选ID查询搜索浏览历史信息
+ (NSDictionary *)queryAllSearchBrowserHistoryInfoUserId:(uint64_t)userId accountId:(uint64_t)accountId symbolSubId:(uint64_t)symbolSubId {
    
    NSDictionary *condDic = @{
                              kSymbolSubId:@(symbolSubId),
                              };
    NSArray *condArr = @[condDic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SEARCH_BROWSE_HISTORY serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:userId];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_SEARCH_BROWSE_HISTORY sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue1 userId:userId accountId:accountId];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询搜索浏览历史信息
+ (NSArray *)queryAllSearchBrowserHistoryInfoUserId:(uint64_t)userId
                                          accountId:(uint64_t)accountId{
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SEARCH_BROWSE_HISTORY serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:userId];
    return [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_SEARCH_BROWSE_HISTORY sqlIndexKey:indexDic queryCond:nil conditionType:ConditionQueryTypeValue0 userId:userId accountId:accountId];
}

#pragma mark  删除搜索浏览历史信息
+ (BOOL)deleteAllSearchBrowserHistoryInfoUserId:(uint64_t)userId     accountId:(uint64_t)accountId{
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SEARCH_BROWSE_HISTORY serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeDelete userId:userId];
    return [IXDBModel dealWithSelfDefineData:nil protocolCommand:CMD_SEARCH_BROWSE_HISTORY sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeDelete conditionType:ConditionQueryTypeValue0 userId:userId accountId:accountId];
}

@end
