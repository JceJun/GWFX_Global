//
//  IXDBLpibBindMgr.m
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBLpibBindMgr.h"
#import "IXDBModel.h"

@implementation IXDBLpibBindMgr

#pragma mark 保存IB_Bind信息
+ (BOOL)saveLpibBindInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPIB_BIND_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_LPIB_BIND_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存IB_Bind信息
+ (BOOL)saveBatchLpibBindInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPIB_BIND_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_LPIB_BIND_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询LpIbBind
+ (NSDictionary *)queryLpIbBindByLpUserId:(uint64_t)lpuserid
{
    NSDictionary *condDic = @{
                              kID:@(lpuserid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPIB_BIND_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_lpib_bind new] protocolCommand:CMD_LPIB_BIND_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        return nil;
    }
}

#pragma mark  查询IB_BindUUID最大值数据
+ (NSDictionary *)queryLpibBindUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_LPIB_BIND_LIST userId:0];
}

#pragma mark  删除IB_Bind信息
+ (BOOL)deleteLpibBind:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_LPIB_BIND_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
