//
//  IXDBScheduleCataMgr.h
//  IXApp
//
//  Created by Evn on 16/11/23.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBScheduleCataMgr : NSObject

/**
 *   保存时间表分类信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveScheduleCataInfo:(id)model;

/**
 *  批量保存时间表分类信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchScheduleCataInfos:(NSArray *)modelArr;

/**
 *  查询时间表分类信息
 *
 */
+ (NSArray *)queryAllScheduleCataInfo;

/**
 *  根据交易时间分类ID查询交易时间信息
 *  @param scheduleCataId 交易时间分类ID
 *
 */
+ (NSDictionary *)queryScheduleCataByScheduleCataId:(uint64_t)scheduleCataId;

/**
 *  查询时间表分类UUID最大值
 *
 */
+ (NSDictionary *)queryScheduleCataUUIDMax;

/**
 *  删除时间表分类
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteScheduleCata:(id)model;

@end
