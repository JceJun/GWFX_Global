//
//  IXDBLpchaccMgr.h
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBLpchaccMgr : NSObject

/**
 *  保存LPChannelAccount信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveLpchaccInfo:(id)model;

/**
 *  批量保存LPChannelAccount信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchLpchaccInfos:(NSArray *)modelArr;

/**
 *  查询LPChannelAccount
 *  @param lpchaccid
 *  @param lpCompanyid 代理公司ID
 *
 */
+ (NSDictionary *)queryLpchaccByLpchaccid:(uint64_t)lpchaccid
                              lpCompanyid:(uint64_t)lpCompanyid;

/**
 *  查询LPChannelAccountUUID最大值数据
 *
 */
+ (NSDictionary *)queryLpchaccUUIDMax;

/**
 *  删除LPChannelAccount信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteLpchacc:(id)model;

@end
