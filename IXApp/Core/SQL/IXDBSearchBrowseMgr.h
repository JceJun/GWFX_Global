//
//  IXDBSearchBrowseMgr.h
//  IXApp
//
//  Created by Evn on 16/12/7.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBSearchBrowseMgr : NSObject

/**
 *   保存搜索浏览历史信息
 *
 *  @param infoDic 搜索产品信息
 *  @param userId 用户ID
 *  @param accountId 账户ID
 *
 */
+ (BOOL)saveSearchBrowserHistoryInfoSymbolId:(NSDictionary *)infoDic userId:(uint64_t)userId accountId:(uint64_t)accountId;

/**
 *  批量保存搜索浏览历史信息
 *
 *  @param infoArr 搜索信息
 *  @param userId 用户ID
 *  @param accountId 账户ID
 *
 */
+ (BOOL)saveBatchSearchBrowserHistoryInfos:(NSArray *)infoArr userId:(uint64_t)userId accountId:(uint64_t)accountId;

/**
 *  查询搜索浏览历史信息
 *  @param userId 用户ID
 *  @param accountId 账户ID
 *  @param symbolSubId 自选ID
 *
 */
+ (NSDictionary *)queryAllSearchBrowserHistoryInfoUserId:(uint64_t)userId accountId:(uint64_t)accountId symbolSubId:(uint64_t)symbolSubId;

/**
 *  查询搜索浏览历史信息
 *  @param userId 用户ID
 *  @param accountId 账户ID
 *
 */
+ (NSArray *)queryAllSearchBrowserHistoryInfoUserId:(uint64_t)userId accountId:(uint64_t)accountId;

/**
 *  删除搜索浏览历史信息
 *  @param userId 用户ID
 *  @param accountId 账户ID
 *
 */
+ (BOOL)deleteAllSearchBrowserHistoryInfoUserId:(uint64_t)userId accountId:(uint64_t)accountId;

@end
