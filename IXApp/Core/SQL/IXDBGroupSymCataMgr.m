//
//  IXDBGroupSymCataMgr.m
//  IXApp
//
//  Created by Evn on 17/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBGroupSymCataMgr.h"
#import "IXDBModel.h"

@implementation IXDBGroupSymCataMgr

#pragma mark  保存产品分类
+ (BOOL)saveGroupSymbolCataInfo:(NSDictionary *)infoDic
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYM_CATA serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithSelfDefineData:infoDic protocolCommand:CMD_GROUP_SYM_CATA sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:0 accountId:0];
}

#pragma mark  批量保存产品分类信息
+ (BOOL)saveBatchGroupSymbolCatasInfos:(NSArray *)infoArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYM_CATA serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchSelfDefineData:infoArr protocolCommand:CMD_GROUP_SYM_CATA sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue0 userId:0 accountId:0];
}

#pragma mark  通过symbolCataId查询GroupSymbolCata记录
+ (NSDictionary *)queryGroupSymbolCataBySymbolCataId:(uint64_t)cataId
                                      accountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *condDic = @{
                             kCataId:@(cataId),
                             kGroupId:@(accountGroupid)
                             };
     NSArray *condArr = @[condDic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYM_CATA serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_GROUP_SYM_CATA sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue2 userId:0 accountId:0];
    if (allArr && allArr.count >0) {
        
        return  allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  清空表数据
+ (BOOL)deleteGroupSymbolCataInfos {
    
    return [IXDBModel deleteDBTable:kTableNameGroupSymCata];
}
@end
