//
//  IXDBLpchaccSymbolMgr.m
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBLpchaccSymbolMgr.h"
#import "IXDBModel.h"

@implementation IXDBLpchaccSymbolMgr

#pragma mark 保存LPChannelAccount_symbol信息
+ (BOOL)saveLpchaccSymbolInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存LPChannelAccount_symbol信息
+ (BOOL)saveBatchLpchaccSymbolInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询LPChannelAccount_symbol
+ (NSDictionary *)queryLpchaccSymbolByLpchaccid:(uint64_t)lpchaccid
                                       symbolid:(uint64_t)symbolid
{
    NSDictionary *condDic = @{
                              kLpchaccid:@(lpchaccid),
                              kSymbolId:@(symbolid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_lpchacc_symbol new] protocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        return nil;
    }
}

#pragma mark  查询LPChannelAccount_symbolUUID最大值数据
+ (NSDictionary *)queryLpchaccSymbolUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST userId:0];
}

#pragma mark  删除LPChannelAccount_symbol信息
+ (BOOL)deleteLpchaccSymbol:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_LPCHANNEL_ACCOUNT_SYMBOL_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
