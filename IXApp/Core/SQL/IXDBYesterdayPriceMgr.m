//
//  IXDBYesterdayPriceMgr.m
//  IXApp
//
//  Created by Evn on 16/12/19.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBYesterdayPriceMgr.h"
#import "IXDBModel.h"

@implementation IXDBYesterdayPriceMgr

#pragma mark 保存昨收价信息
+ (BOOL)saveYesterdayPriceInfo:(NSDictionary *)priceDic
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_YESTERDAY_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithSelfDefineData:priceDic protocolCommand:CMD_YESTERDAY_PRICE sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:0 accountId:0];
}

#pragma mark  批量保存昨收价信息
+ (BOOL)saveBatchYesterdayPriceInfo:(NSArray *)infoArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_YESTERDAY_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchSelfDefineData:infoArr protocolCommand:CMD_YESTERDAY_PRICE sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:0 accountId:0];
}

#pragma mark 查询昨收价信息
+ (NSDictionary *)queryYesterdayPriceBySymbolId:(uint64_t)symbolId
{
    NSDictionary *condDic = @{
                              kID:@(symbolId),
                              kDeadTimeStamp:@([IXEntityFormatter getCurrentTimeInterval])
                              };
    NSArray *condArr = @[condDic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_YESTERDAY_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_YESTERDAY_PRICE sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue1 userId:0 accountId:0];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

@end
