//
//  IXDBSymbolMgr.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBSymbolMgr : NSObject

/**
 *  保存产品信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveSymbolInfo:(id)model;

/**
 *  批量保存产品信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchSymbolInfos:(NSArray *)modelArr;

/**
 *  查询产品信息
 *
 */
+ (NSArray *)queryAllSymbols;

/**
 *  根据产品ID查询产品信息
 *  @param symbolId 产品ID
 *
 */
+ (NSDictionary *)querySymbolBySymbolId:(uint64_t)symbolId;

/**
 *  根据产品名称查询产品信息
 *  @param name 产品名称
 *
 */
+ (NSDictionary *)querySymbolByName:(NSString *)name;

/**
 *  根据报价来源查询产品信息
 *  @param source 报价来源
 *
 */
+ (NSDictionary *)querySymbolBySource:(NSString *)source;

/**
 *  根据报价来源查询产品信息
 *  @param profCurrency 盈亏货币
 *  @param baseCurrency 基础货币
 *
 */
+ (NSDictionary *)querySymbolByProfitCurrency:(NSString *)profCurrency baseCurrency:(NSString *)baseCurrency;

/**
 *  根据产品分类查询产品信息
 *  @param cataId 产品分类ID
 *  @param sIndex 起始位置
 *  @param offset 偏移量
 *
 */
+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId
                             startIndex:(uint64_t)sIndex
                                 offset:(uint64_t)offset;

+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId;

/**
 *  根据产品分类查询产品信息
 *  @param cataId 产品分类ID
 *  @param sIndex 起始位置
 *  @param offset 偏移量
 *  @param accountGroupid 账户组ID
 *
 */
+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId
                             startIndex:(uint64_t)sIndex
                                 offset:(uint64_t)offset
                         accountGroupid:(uint64_t)accountGroupid;

+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId
                         accountGroupid:(uint64_t)accountGroupid;



///**
// *  模糊查询产品信息
// *  @param addInfo 查询条件
// *
// */
//+ (NSArray *)fuzzyQuerySymbolAdditon:(NSString *)addInfo;

/**
 *  模糊查询产品信息
 *  @param addDic 查询条件
 *
 */
+ (NSArray *)fuzzyQuerySymbolByContent:(NSString *)search cataId:(uint64_t)cataId;

+ (NSArray *)fuzzyQuerySymbolByContent:(NSString *)search
                                cataId:(uint64_t)cataId
                        accountGroupid:(uint64_t)accountGroupid;

+ (NSArray *)fuzzyQuerySymbolSymInfoByContent:(NSString *)search
                                cataId:(uint64_t)cataId
                        accountGroupid:(uint64_t)accountGroupid;

+ (NSArray *)fuzzyQuerySymbolSymCataByContent:(NSString *)search
                                       cataId:(uint64_t)cataId
                               accountGroupid:(uint64_t)accountGroupid;

/**
 *  查询产品UUID最大值
 *
 */
+ (NSDictionary *)querySymbolUUIDMax;

/**
 *  删除产品信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)deleteSymbolInfo:(id)model;

/**
 *  批量删除产品信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)deleteBatchSymbolInfos:(NSArray *)modelArr;

@end
