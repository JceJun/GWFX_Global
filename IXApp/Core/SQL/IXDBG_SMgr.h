//
//  IXDBG_SMgr.h
//  IXApp
//
//  Created by Evn on 2017/8/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBG_SMgr : NSObject

/**
 *  保存账户组产品信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveG_SInfo:(id)model;

/**
 *  批量保存账户组产品信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchG_SInfos:(NSArray *)modelArr;

/**
 *  查询账户组产品
 *  @param accGroupId  账户组ID
 *  @param symbolId    产品ID
 *
 */
+ (NSDictionary *)queryG_SByAccountGroupId:(uint64_t)accGroupId
                              symbolId:(uint64_t)symbolId;

/**
 *  查询当前账户组产品
 *  @param accGroupId  账户组ID
 *
 */
+ (NSArray *)queryAllG_SByAccountGroupId:(uint64_t)accGroupId;

/**
 *  删除账户组产品信息
 *
 *  @param model protocalModel集合
 *
 */
+ (BOOL)deleteG_SInfo:(id)model;

/**
 *  批量删除账户组产品信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)batchDeleteG_SInfo:(GPBUInt64Array *)modelArr;

/**
 *  查询账户组产品UUID最大值数据
 *
 */
+ (NSDictionary *)queryG_SUUIDMax;

@end
