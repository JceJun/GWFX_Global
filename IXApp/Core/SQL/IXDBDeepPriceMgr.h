//
//  IXDBDeepPriceMgr.h
//  IXApp
//
//  Created by Evn on 17/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBDeepPriceMgr : NSObject

/**
 *  保存深度价信息
 *
 *  @param priceDic 昨收价
 *
 */
+ (BOOL)saveDeepPriceInfo:(NSDictionary *)priceDic;

/**
 *  批量保存深度价信息
 *
 *  @param infoArr 昨收价
 *
 */
+ (BOOL)saveBatchDeepPriceInfo:(NSArray *)infoArr;

/**
 *  查询昨深度价信息
 *  @param symbolId 产品ID
 *
 */
+ (NSArray *)queryDeepPriceBySymbolId:(uint64_t)symbolId;

/**
 *  查询昨深度价信息
 *  @param symbolId 产品ID
 *
 */
+ (NSDictionary *)queryDeepPriceBySymbolId:(uint64_t)symbolId index:(int32_t)index;

@end
