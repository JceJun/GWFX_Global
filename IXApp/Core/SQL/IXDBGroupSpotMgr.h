//
//  IXDBGroupSpotMgr.h
//  IXApp
//
//  Created by Evn on 2018/3/4.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBGroupSpotMgr : NSObject

/**
 *   保存GroupSpot信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveGroupSpotInfo:(id)model;

/**
 *  批量保存GroupSpot信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchGroupSpotInfos:(NSArray *)modelArr;

/**
 *  删除GroupSpot信息
 *
 *  @param model protocalModel集合
 *
 */
+ (BOOL)deleteGroupSpotInfo:(id)model;

/**
 *  通过账户组ID查询GroupSpot信息
 *  @param groupId 账户组ID
 *
 */
+ (NSArray *)queryGroupSpotInfoByGroupId:(uint64_t)groupId;


/**
 *  通过货币类型查询GroupSpot信息
 *  @param type //0:数字货币 1:法币
 *
 */
+ (NSDictionary *)queryGroupSpotInfoByType:(uint32)type;


/**
 *  通过货币名称查询GroupSpot信息
 *  @param name 货币名称
 *
 */
+ (NSDictionary *)queryGroupSpotInfoByName:(NSString *)name;

/**
 *  查询GroupSpotUUID最大值
 *
 */
+ (NSDictionary *)queryGroupSpotUUIDMax;

/**
 *  清空表数据
 *
 *
 */
+ (BOOL)deleteGroupSpotInfos;

@end
