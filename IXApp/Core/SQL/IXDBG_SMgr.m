//
//  IXDBG_SMgr.m
//  IXApp
//
//  Created by Evn on 2017/8/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBG_SMgr.h"
#import "IXDBModel.h"
#import "IXDBAccountMgr.h"
@implementation IXDBG_SMgr

#pragma mark 保存账户组产品信息
+ (BOOL)saveG_SInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_GROUP_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存账户组产品信息
+ (BOOL)saveBatchG_SInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_GROUP_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询账户组产品信息
+ (NSDictionary *)queryG_SByAccountGroupId:(uint64_t)accGroupId
                                  symbolId:(uint64_t)symbolId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kAccGroupId:@(accGroupId),
                             kSymbolId:@(symbolId)
                             };
    __block NSArray *allArr = [IXDBModel dealWithDataCheck:[item_group_symbol new] protocolCommand:CMD_GROUP_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        // 通过 symbolId 查询的产品数据为基础产品数据，通过 accGroupId & symbolId 查询的产品数据为高级产品数据，如果查询到了，则会替换基础数据，如果未查询到，则保留基础数据。由于 demo 和 real 也是不同账户组，所以，此处将同一个用户的 demo 和real 的产品数据都同步到高级产品数据，保持两个账户交易产品数据一致
        NSArray *ixArr = [IXDBAccountMgr queryAllAccountInfoByUserId:
                          [IXUserInfoMgr shareInstance].userLogInfo.account.userid clientType:0];
        
        if (ixArr.count < 2) {
            return nil;
        }
        __block NSMutableArray *arr = [ixArr mutableCopy];
        __block NSInteger rmindex = -1;
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger accGroupId_temp = [obj[@"accountGroupId"] integerValue];
            if (accGroupId_temp == accGroupId) {
                rmindex = idx;
            }
        }];
        if (rmindex >= 0) {
            [arr removeObjectAtIndex:rmindex];
        }
        
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *upperDic = obj;
            //            NSInteger realAccId = [realDic[@"id"] integerValue];
            NSInteger upperAccGroupId = [upperDic[@"accountGroupId"] integerValue];
            NSDictionary *paramDic = @{
                                     kAccGroupId:@(upperAccGroupId),
                                     kSymbolId:@(symbolId)
                                     };
            
            allArr = [IXDBModel dealWithDataCheck:[item_group_symbol new] protocolCommand:CMD_GROUP_SYMBOL_LIST sqlIndexKey:indexDic queryCond:paramDic userId:0 conditionType:ConditionQueryTypeValue2];
            if (allArr) {
                *stop = YES;
            }
        }];
        if (allArr.count) {
            return allArr[0];
        }
    }
    return nil;
}

#pragma mark  查询当前账户组产品
+ (NSArray *)queryAllG_SByAccountGroupId:(uint64_t)accGroupId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kAccGroupId:@(accGroupId)
                             };
    return  [IXDBModel dealWithDataCheck:[item_group_symbol new] protocolCommand:CMD_GROUP_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark 删除结算时间信息
+ (BOOL)deleteG_SInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_GROUP_SYMBOL_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量删除账户组产品信息
+ (BOOL)batchDeleteG_SInfo:(GPBUInt64Array *)modelArr
{
    BOOL ret = NO;
    for (int i = 0; i < modelArr.count; i++) {
        item_group_symbol *group_symbol = [[item_group_symbol alloc] init];
        group_symbol.id_p = [modelArr valueAtIndex:i];
        ret = [IXDBModel dealWithDataDelete:group_symbol protocolCommand:CMD_GROUP_SYMBOL_LIST userId:0 conditionType:ConditionQueryTypeValue0];
        if (!ret) {
            break;
        }
    }
    return ret;
}

#pragma mark  查询账户组产品UUID最大值数据
+ (NSDictionary *)queryG_SUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_GROUP_SYMBOL_LIST userId:0];
}

@end
