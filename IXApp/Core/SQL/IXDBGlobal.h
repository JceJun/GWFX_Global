//
//  IXDataBaseGlobal.h
//  IXFMDB
//
//  Created by ixiOSDev on 16/11/1.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#ifndef IXDataBaseGlobal_h
#define IXDataBaseGlobal_h

/** SQLite数据类型 */
#define kSqlText      @"TEXT"
#define kSqlInteger   @"INTEGER"
#define kSqlLong      @"LONG"
#define kSqlDouble    @"DOUBLE"
#define kSqlLreal     @"REAL"
#define kSqlBlob      @"BLOB"
#define kSqlNull      @"NULL"
#define kSqlOleObject @"OLE OBJECT"
#define kPrimaryKey   @"primary key"

/** 数据库表名 */
#define kTableNameSymbolCatas       @"symbol_catas" //产品分类
#define kTableNameSymbol            @"symbol" //产品
#define kTableNameSymbolSub         @"symbol_sub" //产品自选
#define kTableNameCompany           @"company" //公司
#define kTableNameSymbolSubCata     @"symbol_sub_cata" //产品自选分类
#define kTableNameHolidayCatas      @"holiday_catas"
#define kTableNameSymbolHot         @"symbol_hot" //热门产品
#define kTableNameUserInfo          @"user_info" //用户信息
#define kTableNameAccount           @"user_account" //用户账户
#define kTableNamePosition          @"position" //持仓
#define ktableNameHolidayCata       @"holiday_cata " //假期分类
#define kTableNameHoliday            @"holiday" //假期
#define kTableNameSchedule           @"schedule"//时间表
#define kTableNameScheduleCata       @"schedule_cata"//时间表分类
#define kTableNameBrowserHistory     @"browser_history" //浏览历史表
#define kTableNameKayLineMinute      @"kayLine_minute" //1分K线
#define kTableNameKayLineFiveMinute  @"kayLine_fiveMinute"//5分K线
#define kTableNameKayLineDay        @"kayLine_day"   //日k线
#define kTableNameNetworkFlow       @"networkFlow"  //流量
#define kTableNameLanguage          @"language" //多语言
#define KTableNameEodTime           @"eod_time" //结算时间
#define kTableNameMarginSet         @"marginSet"  //产品保证金
#define kTableNameAccountGroup      @"account_group" //账户组
#define kTableNameSearchBrowse      @"search_browse" //浏览历史
#define kTableNameSymbolLable       @"symbol_lable" //产品标签
#define kTableNameAccountGroupSymCate @"account_group_sym_cata"//账户组的产品分类
#define kTableNameQuoteDelay        @"quote_delay "
#define kTableNameScheduleMargin    @"schedule_margin" //交易时间margin
#define kTableNameHolidayMargin     @"holiday_margin" //假期margin
#define kTableNameSecureDev         @"secure_dev"     //登录设备

#define kTableNameYesterdayPrice    @"yesterday_price" //昨收价
#define kTableNameQuotePrice        @"quote_price" //行情价
#define kTableNameDeepPrice         @"deep_price" //行情价
#define kTableNameGroupSymCata      @"group_sym_cata" //账户组产品分类
#define kTableNameGroupSym          @"group_sym" //账户组产品
#define kTableNameDBVersion         @"db_version"
#define kTableNameGroupSymCataNew   @"group_sym_cata_new" //服务器拆分权限表
#define kTableNameGroupSymNew       @"group_sym_new" //服务器拆分权限表
#define kTableNameLpchacc           @"lpchacc"
#define kTableNameLpchaccSymbol     @"lpchacc_Symbol"
#define kTableNameLpchannel         @"lpchannel"
#define kTableNameLpchannelSymbol   @"lpchannel_symbol"
#define kTableNameLpibBind          @"lpib_bind"


#define kSqlIndexKey                @"sqlIndexKey"//字段索引
#define kSqlValue                   @"sqlValue"//字段键值

#define kStartIndex                 @"startIndex"//查询起点索引
#define KEndIndex                   @"endIndex" //查询终点索引
#define kOffset                     @"offset"//查询区域

/** 数据存储键 */

#define kID         @"id"
#define kUUID       @"uuid"     // 更新标识号
#define kName       @"name"     // 名称
#define kStatus     @"status"   //0. 正常 1. 刪除
#define kUUTime     @"uutime"


/** 用户信息 */
#define kLoginName  @"loginName"
#define kCompanyId  @"companyId"
#define kCompany    @"company"
#define kEmail      @"email"
#define kPhone      @"phone"
#define kCountry    @"country"
#define kState      @"state"
#define kCity       @"city"
#define kCustomerNo @"customerNo"   //后台用户号
#define kUserId     @"userId"       //用户ID

/** 产品分类 */
#define KSymbolCataId   @"symbolCataId"     //产品自选ID
#define kSequence       @"sequence"         //顺序
#define kparentId       @"parentId"         //父类ID（预留，用来做多级分类）
#define kMarketId       @"marketId"         //市场分类
#define kMarketName     @"marketName"       //市场名称
#define kQuoteDelayMinutes @"quoteDelayMinutes"
#define kScheduleDelayMinutes @"scheduleDelayMinutes"
#define kQuoteSubFee    @"quoteSubFee"
#define kQuoteSubCurrency @"quoteSubCurrency"

/** 产品 */
#define kSymbolId       @"symbolId" //产品ID
#define kCataId         @"cataId"   //产品分类ID
#define kSource         @"source"   //报价源
#define kBaseCurrency   @"baseCurrency"     //基础货币
#define kProfitcurrency @"profitCurrency"   //盈亏货币
#define kPinYin         @"pinYin"       //名称拼音
#define kDigits         @"digits"       //小数位数
#define kPipsRatio      @"pipsRatio"    //大点比率
#define kVolumesMin     @"volumesMin"   //最小手术
#define kVolumesStep    @"volumesStep"  //手数步进
#define kVolumsMax      @"volumsMax"    //最大手数
#define kContractSize   @"contractSize" //单位
#define kRelateSource   @"relateSource"
#define kStopLevel      @"stopLevel"    //限价/止损订单间隔点
#define kMaxStopLevel   @"maxStopLevel" //限价/止损订单最高间隔点
#define kSwapDaysPerYear @"swapDaysPerYear" //年利率
#define kSpread         @"spread"           //点差
#define kSpreadBalance  @"spread_balance"   //点差偏差
#define kPositionVolumeMax  @"positionVolumeMax"//最大持仓数
#define kDisplayName        @"display_Name" //显示名称
#define kLongSwap           @"longSwap"     //隔夜利息（买）
#define kShortSwap          @"shortSwap"    //隔夜利息（卖）
#define kExpiryTime         @"expiryTime"   //过期时间
#define kContractSizeNew    @"contractSizeNew" //合约数新
#define kNormalMarginType   @"normalMarginType" //正常交易保证金
#define kWeekendMarginType  @"weekendMarginType" //周末保证金
#define kHolidayMarginType  @"holidayMarginType" //假期保证金
#define kVolDigits          @"volDigits"    //数量小数位
#define kLabel              @"label"        //产品标签
 
/** 产品标签 */
#define kColour         @"colour" //标签颜色 

/** 账户 */
#define kUserId         @"userId"           //用户ID
#define kAccGroupId     @"accountGroupId"   //账户组ID
#define kEnable         @"enable"           //是否可用
#define kBalance        @"balance"          //余额
#define kCredit         @"credit"           //额度
#define kBonus          @"bonus"            //奖金
#define kEquity         @"equity"           //净值
#define kFreeMargin     @"freeMargin"       //可用保证金
#define kCompanyToken   @"companyToken"
#define kType           @"type"             //类型
#define kClearNegative  @"clearNegative"
#define kMt4Accid       @"mt4Accid"         //mt4账户ID
#define kRefAccid       @"refAccid"         //ref账户ID
#define kNonTradable    @"nonTradable"      //是否可交易
#define kDefaultType    @"defaultType"      


/** 账户组  */
#define kLpuserid       @"lpuserid"         //代理用户ID
#define kClientType     @"clientType"       //客户端类型
#define kOptions        @"ptions"           //登录交易权限

/** 公司 */
#define kMode           @"mode"             

/** 持仓 */
#define kGroupId        @"groupid"
#define kAccountId      @"accountid"
#define kDirection      @"direction"        //买卖方向
#define kSymbol         @"symbol"           //产品名
#define kOpenPrice      @"open_price"       //建仓成交价
#define kOpenTime       @"open_time"        //建仓成交时间
#define kPrice          @"price"            //价格

/** 产品自选  */
#define kSymbolSubCataid @"symbol_sub_cataid"
#define kSymbolSubId    @"symbolSubId"


/** 假期 */
#define kMarginType     @"marginType"       //保证金类型（正常和特殊）
#define kEveryYear      @"everyYear"        //是否每年生效
#define kFromTime       @"fromTime"         //假期开始时间
#define kToTime         @"toTime"           //假期结束时间
#define kCurrentTime    @"currentTime"      //当前时间
#define Kdescription    @"description"      //假期说明
#define kHolidayCataId  @"holidayCataId"    //假期分类
#define kNeedStopout    @"needStopout"      //是否需要強平
#define kCancelOrder    @"cancelOrder"      //是否需要取消挂单
#define kSwapDays       @"swapDays"         //计息日数
#define kSettlementDate @"settlementDate"   //恢复结算日
#define kMarginSetpre   @"marginSetpre"     //套用保证金前时间
#define kMarginTime     @"marginTime"       //假前保证金
#define kHolidayCataId  @"holidayCataId"    //假期分类ID
#define kHolidayId      @"holidayId"        //假期ID
#define KTradable       @"tradable"         //交易开关

/** 交易时间 */
#define kStartTime      @"startTime"        //开始时间分钟数
#define kEndTime        @"endTime"          //结束时间分钟数
#define kDayOfWeek      @"dayOfWeek"        //一周的第？天
#define kMarginSet      @"marginSet"        //套用保证金
#define kHoliday        @"holiday"          //是否假期保证金
#define kScheduleCataId @"scheduleCataId"   //交易时间分类ID
#define kScheduleId     @"scheduleId"       //交易时间ID
#define kDeadTimeStamp  @"deadTimeStamp"    //截止时间点

/** 浏览历史 */
#define kBrowserTime    @"browserTime" //浏览时间



/** k线数据 */
#define kMinuteMaxNum        10000
#define kFiveMinuteMaxNum    100000
#define kHourMaxNum          1000000
#define kMaxNum      @"kayLineMaxNum"
#define kAmount      @"nAmount" //交易金额
#define kNOpenPrice  @"nOpen" //开盘价
#define kClosePrice  @"nClose" //收盘价
#define kHighPrice   @"nHigh" //最高价
#define kLowPrice    @"nLow" //最低价
#define kVolume      @"nVolume" //交易笔数
#define kNTime       @"nTime" //时间
#define kNNow        @"nNow" //现价
#define kLong1970Time    @"long1970Time" //当前时间
#define kLong1970Minute  @"long1970Minute" //当前时间分钟数
#define kLastTimeMinute  @"lastTimeMinute" //上一次时间分钟数
#define kKLineType   @"kayLineType" //K线类型
#define kNMinutes    @"nMinutes"
#define kCounts      @"counts"
#define kStartMinute @"startMinute"
#define kEndMinute   @"endMinute"

/** 产品保证金 */
#define kRangeNum           @"rangeNum"     //手数范围
#define kPercent            @"percent"      //百分比
#define kRangeLeft          @"rangeLeft"    //范围开始
#define kRangeRight         @"rangeRight"   //范围结束

/** 账户组 */
#define kServerId           @"serverId"         //所属Trade服务器ID
#define kCurrency           @"currency"         //账户货币
#define kLevel              @"level"            //级别
#define kMarginCallLevel    @"marginCallLevel"  //按金通知水平
#define kStopOutLevel       @"stopOutLevel"     //强平水平
#define kSpread             @"spread"           //账户组点差


/** 账户组的产品分类 */
#define kCommission         @"commission"       //佣金
#define kCommissionType     @"commissionType"   //佣金类型

/** 多语言 */
#define kNameSpace          @"nameSpace"    //命名空间
#define kKeyId              @"keyId"        //语言Key
#define kValue              @"value"        //语言值
#define kLanguageName       @"languageName" //

/** 结算时间 */
#define kWeekDay            @"weekDay"//周几

/** 登录设备 */
#define kDevToken           @"devToken" //设备标识
#define kDevName            @"devName" //设备名称
#define kLastLoginTime      @"lastLoginTime" //最后一次登录时间
#define kVerifyTime         @"verifyTime" //校验时间
#define kVerifyMode         @"verifyMode" //校验模式
#define kVerifyNo           @"verifyNo" //校验号
#define kPass               @"pass"

/** 账户组产品（服务器权限拆分） */
#define kVolumesSidemax     @"volumesSidemax" //单边最大手数
#define kCloseOnly          @"closeOnly" //仅平仓
#define kNoDispaly          @"noDisplay" //产品显示或隐藏
#define kLpstatus           @"lpstatus"  //lp状态
#define kLpExpiryTime       @"lpExpiryTime" //lp产品到期时间

/** LPChacc */
#define kChannelid          @"channelid" //通道ID
#define kLpCompanyid        @"lpCompanyid" //通道公司ID
#define kToken              @"token"
#define kIp                 @"ip"
#define kPort               @"port"
#define kTargetCompid       @"targetCompid"
#define kSenderCompid       @"senderCompid"
#define kSenderSubid        @"senderSubid"
#define kDeliverToCompid    @"deliverToCompid"

/** Lpchacc_symbol */
#define kLpchaccid          @"lpchaccid"
#define kLpspread           @"lpspread"

/** lpchannel_symbol */
#define kChspread           @"chspread"

/** lpib_bind */
#define kIbCompanyid        @"ibCompanyid"
#define kLpchaccToken       @"lpchaccToken"


/** 订阅行情 */
#define kLimitDate          @"limitDate"

/** 流量 */
#define kNetworkFlowData        @"networkFlowData" //流量值
#define kSums                   @"sums" //求和
#define kCategory               @"category" //类别

/** 行情价 */
#define kQuoteData          @"quoteData"
#define kNGmtTime           @"nGmtTime"
#define kNOpen              @"nOpen"
#define kNHight             @"nHight"
#define kNPrice             @"nPrice"

/** 深度价 */
#define kBuyPrice            @"buyPrice"
#define kBuyVolume           @"buyVolume"
#define kSellPrice           @"sellPrice"
#define kSellVolume          @"sellVolume"
#define kDeepIndex           @"deepIndex"

/** 数据库版本 */
#define kDBVersion           @"version"

/** 数据库操作 */
typedef NS_ENUM(NSInteger,DataBaseOperateType) {
    DataBaseOperateTypeSave,
    DataBaseOperateTypeDelete,
    DataBaseOperateTypeQuery
};

//数据库服务器类型
typedef NS_ENUM(NSInteger,IXDBServerType) {
    IXDBServerTypeQuote,    //行情服务器
    IXDBServerTypeTrade     //交易服务器
};

/** 数据表命令类型 */
typedef NS_ENUM(uint16,CommondType) {
    CMD_BROWSER_HISTORY = 0x9000,
    CMD_SYMBOL_MARKETID,
    CMD_KAYLINE_MINUTE,
    CMD_KAYLINE_FIVE_MINUTE,
    CMD_KAYLINE_DAY,
    CMD_NETWORK_FLOW,
    CMD_SEARCH_BROWSE_HISTORY,
    CMD_YESTERDAY_PRICE,
    CMD_QUOTE_PRICE,
    CMD_DEEP_PRICE,
    CMD_GROUP_SYM_CATA,
    CMD_GROUP_SYM,
    CMD_DB_VERSION
};

/** 查询条件类型 */
typedef NS_ENUM(uint16,ConditionQueryType) {
    ConditionQueryTypeValue0,//不带参数
    ConditionQueryTypeValue1, //一个参数
    ConditionQueryTypeValue1A,
    ConditionQueryTypeValue1B,
    ConditionQueryTypeValue1C,
    ConditionQueryTypeValue2, //两个参数
    ConditionQueryTypeValue2A,
    ConditionQueryTypeValue2B,
    ConditionQueryTypeValue3, //三个参数
    ConditionQueryTypeValue3A,
    ConditionQueryTypeValue3B,
    ConditionQueryTypeValue3C,
    ConditionQueryTypeValue4, //四个参数
    ConditionQueryTypeValue4A,
    ConditionQueryTypeOther
};

#endif /* IXDataBaseGlobal_h */
