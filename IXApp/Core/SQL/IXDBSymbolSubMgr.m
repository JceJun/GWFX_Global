//
//  IXDBSymbolSubMgr.m
//  IXApp
//
//  Created by ixiOSDev on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBSymbolSubMgr.h"
#import "IXDBModel.h"

@implementation IXDBSymbolSubMgr

#pragma mark 保存自选产品信息
+ (BOOL)saveSymbolSubInfo:(id)model accountId:(uint64_t)accountId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:accountId];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:accountId conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存自选产品信息
+ (BOOL)saveBatchSymbolSubInfos:(NSArray *)modelArr accountId:(uint64_t)accountId {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:accountId];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:accountId conditionType:ConditionQueryTypeValue0];
}

#pragma mark  分页查询自选产品信息
+ (NSArray *)querySymbolSubStartIndex:(uint64_t)sIndex
                               offset:(uint64_t)offset
                            accountId:(uint64_t)accountId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:accountId];
    NSDictionary *addDic = @{
                             kStartIndex:@(sIndex),
                             kOffset:@(offset),
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic queryCond:addDic userId:accountId conditionType:ConditionQueryTypeValue2];
}

#pragma mark 根据产品ID查询自选ID
+ (NSDictionary *)querySymbolSubBySymbolId:(uint64_t)symbolId
                                 accountId:(uint64_t)accountId {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:accountId];
    NSDictionary *addDic = @{
                             kSymbolId:@(symbolId)
                             };
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic queryCond:addDic userId:accountId conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询所有自选产品信息
+ (NSArray *)querySymbolsSubAccountId:(uint64_t)accountId symbolCataId:(uint64_t)symbolCataId {
    
    NSDictionary *addDic = @{
                             kSymbolSubCataid:@(symbolCataId)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:accountId];
    return [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic queryCond:addDic userId:accountId conditionType:ConditionQueryTypeValue1];
}

#pragma mark 查询自选产品信息
+ (NSArray *)queryAllSymbolsSubAccountId:(uint64_t)accountId {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:accountId];
    return [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic queryCond:nil userId:accountId conditionType:ConditionQueryTypeValue0];
}

+ (NSArray *)queryAllSymbolsSubAccountId:(uint64_t)accountId
                          AccountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *addDic = @{
                             kGroupId:@(accountGroupid)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:accountId];
    return [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic queryCond:addDic userId:accountId conditionType:ConditionQueryTypeValue1A];
}

+ (NSArray *)queryAllSymbolsSymInfoSubAccountId:(uint64_t)accountId
                                 AccountGroupid:(uint64_t)accountGroupid {
    NSDictionary *addDic = @{
                             kGroupId:@(accountGroupid)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:accountId];
    return [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic queryCond:addDic userId:accountId conditionType:ConditionQueryTypeValue1B];
}

+ (NSArray *)queryAllSymbolsSymCataSubAccountId:(uint64_t)accountId
                                 AccountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *addDic = @{
                             kGroupId:@(accountGroupid)
                             };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_SUB_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:accountId];
    return [IXDBModel dealWithDataCheck:[item_symbol_sub new] protocolCommand:CMD_SYMBOL_SUB_LIST sqlIndexKey:indexDic queryCond:addDic userId:accountId conditionType:ConditionQueryTypeValue1C];
}

#pragma mark  查询自选产品UUID最大值
+ (NSDictionary *)querySymbolSubUUIDMaxAccountId:(uint64_t)accountId {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SYMBOL_SUB_LIST userId:accountId];
}

+ (NSDictionary *)querySymbolSubUUIDMaxByAddInfo:(NSDictionary *)infoDic {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SYMBOL_SUB_LIST addInfo:infoDic];
}

#pragma mark 删除自选产品信息
+ (BOOL)deleteSymbolSubInfo:(id)model accountId:(uint64_t)accountId {
    
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SYMBOL_SUB_DELETE userId:accountId conditionType:ConditionQueryTypeValue0];
}

@end
