//
//  IXDBVersionMgr.h
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBVersionMgr : NSObject

/**
 *  保存数据库版本信息
 *
 *  @param verDic 版本信息
 *
 */
+ (BOOL)saveDBVersionInfo:(NSDictionary *)verDic;

/**
 *  查询版本信息
 *
 */
+ (NSDictionary *)queryDBVersion;

@end
