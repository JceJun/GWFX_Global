//
//  IXDBHolidayCataMgr.m
//  IXApp
//
//  Created by ixiOSDev on 16/11/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBHolidayCataMgr.h"
#import "IXDBModel.h"
#import "IxProtoHolidayCata.pbobjc.h"

@implementation IXDBHolidayCataMgr

#pragma mark  保存假日分类
+ (BOOL)saveHolidayCataInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_HOLIDAY_CATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存假日分类信息
+ (BOOL)saveBatchHolidayCatasInfos:(NSArray *)modelArr  {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_HOLIDAY_CATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询假日分类信息
+ (NSArray *)queryHolidayAllCatas {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_holiday_cata new] protocolCommand:CMD_HOLIDAY_CATA_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  根据假期分类ID查询假期信息
+ (NSDictionary *)queryHolidayCataByHolidayCataId:(uint64_t)holidayCataId {
    
    NSDictionary *condDic = @{
                              kID:@(holidayCataId),
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_CATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_holiday_cata new] protocolCommand:CMD_HOLIDAY_CATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询假日分类UUID最大值
+ (NSDictionary *)queryHolidayCatasUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_HOLIDAY_CATA_LIST userId:0];
}

#pragma mark  删除假期分类
+ (BOOL)deleteHolidayCata:(id)model
{
    item_holiday_cata *pb = [item_holiday_cata new];
    pb.id_p = ((proto_holiday_cata_delete *)model).id_p;
    return [IXDBModel dealWithDataDelete:pb protocolCommand:CMD_HOLIDAY_CATA_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
