//
//  IXDBEodTimeMgr.m
//  IXApp
//
//  Created by Evn on 2017/6/22.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBEodTimeMgr.h"
#import "IXDBModel.h"

@implementation IXDBEodTimeMgr

#pragma mark   保存结算时间信息
+ (BOOL)saveEodTimeInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_EOD_TIME_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_EOD_TIME_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存结算时间信息
+ (BOOL)saveBatchEodTimeInfos:(NSArray *)modelArr
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_EOD_TIME_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_EOD_TIME_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark   查询结算时间信息
+ (NSDictionary *)queryEodTimeInfoByWeekDay:(uint64_t)weekDay
                                       type:(uint32)type
{
    NSDictionary *condDic = @{
                                kWeekDay:@(weekDay),
                                kType:@(type)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_EOD_TIME_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_eod_time new] protocolCommand:CMD_EOD_TIME_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        return nil;
    }
}

#pragma mark 删除结算时间信息
+ (BOOL)deleteEodTimeInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_EOD_TIME_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询结算时间UUID最大值
+ (NSDictionary *)queryEodTimeUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_EOD_TIME_LIST userId:0];
}

@end
