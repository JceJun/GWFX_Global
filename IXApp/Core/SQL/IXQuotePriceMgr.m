//
//  IXQuotePriceMgr.m
//  IXApp
//
//  Created by Evn on 17/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXQuotePriceMgr.h"
#import "IXDBModel.h"

@implementation IXQuotePriceMgr

#pragma mark 保存昨收价信息
+ (BOOL)saveQuotePriceInfo:(NSDictionary *)priceDic
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithSelfDefineData:priceDic protocolCommand:CMD_QUOTE_PRICE sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:0 accountId:0];
}

#pragma mark  批量保存昨收价信息
+ (BOOL)saveBatchQuotePriceInfo:(NSArray *)infoArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchSelfDefineData:infoArr protocolCommand:CMD_QUOTE_PRICE sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue0 userId:0 accountId:0];
}

#pragma mark 查询昨收价信息
+ (NSDictionary *)queryQuotePriceBySymbolId:(uint64_t)symbolId
{
    NSDictionary *condDic = @{
                              kID:@(symbolId)
                              };
    NSArray *condArr = @[condDic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_QUOTE_PRICE sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue1 userId:0 accountId:0];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  批量查询行情价信息
+ (NSArray *)queryBatchQuotePriceBySymbolIds:(NSArray *)symbolIdArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_QUOTE_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_QUOTE_PRICE sqlIndexKey:indexDic queryCond:symbolIdArr conditionType:ConditionQueryTypeValue1 userId:0 accountId:0];
}

@end
