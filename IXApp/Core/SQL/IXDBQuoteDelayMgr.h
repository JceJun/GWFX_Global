//
//  IXDBQuoteDelayMgr.h
//  IXApp
//
//  Created by Evn on 17/3/9.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBQuoteDelayMgr : NSObject

/**
 *   保存订阅行情信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveQuoteDelayInfo:(id)model;

/**
 *  批量保存订阅行情信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchQuoteDelayInfos:(NSArray *)modelArr;

/**
 *  查询订阅行情信息
 *
 */
+ (NSArray *)queryAllQuoteDelayInfo;

/**
 *  查询订阅行情信息
 *
 */

+ (NSDictionary *)queryQuoteDelayByCataId:(uint64_t)cataId userid:(uint64_t)userid;

/**
 *  删除订阅行情信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)deleteQuoteDelayInfo:(id)model;

/**
 *  查询订阅行情UUID最大值
 *
 */
+ (NSDictionary *)queryQuoteDelayUUIDMax;
@end
