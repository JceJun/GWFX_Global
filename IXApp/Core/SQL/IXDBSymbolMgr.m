//
//  IXDBSymbolMgr.m
//  IXApp
//
//  Created by ixiOSDev on 16/11/5.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBSymbolMgr.h"
#import "IXDBModel.h"

@implementation IXDBSymbolMgr

#pragma mark 保存产品信息
+ (BOOL)saveSymbolInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存产品信息
+ (BOOL)saveBatchSymbolInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark 查询产品信息
+ (NSArray *)queryAllSymbols {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  根据产品ID查询产品信息
+ (NSDictionary *)querySymbolBySymbolId:(uint64_t)symbolId {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kID:@(symbolId)
                             };
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  根据产品名称查询产品信息
+ (NSDictionary *)querySymbolByName:(NSString *)name {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kName:name
                             };
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  根据报价来源查询产品信息
+ (NSDictionary *)querySymbolBySource:(NSString *)source {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kSource:source
                             };
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}


#pragma mark  根据报价来源查询产品信息
+ (NSDictionary *)querySymbolByProfitCurrency:(NSString *)profCurrency baseCurrency:(NSString *)baseCurrency {
    
    if (!profCurrency || !baseCurrency) {
        
        return nil;
    }
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kProfitcurrency:profCurrency,
                             kBaseCurrency:baseCurrency
                             };
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  根据产品分类查询产品信息
+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId
                             startIndex:(uint64_t)sIndex
                                 offset:(uint64_t)offset
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kCataId:@(cataId),
                             kStartIndex:@(sIndex),
                             kOffset:@(offset)
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue3];
}

+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kCataId:@(cataId)
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
}

+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId
                             startIndex:(uint64_t)sIndex
                                 offset:(uint64_t)offset
                         accountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kCataId:@(cataId),
                             kStartIndex:@(sIndex),
                             kOffset:@(offset),
                             kGroupId:@(accountGroupid)
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue4];
}

+ (NSArray *)querySymbolsBySymbolCataId:(uint64_t)cataId
                         accountGroupid:(uint64_t)accountGroupid
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kCataId:@(cataId),
                             kGroupId:@(accountGroupid)
                             };
    return [IXDBModel dealWithDataCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2A];
}

#pragma mark  模糊查询产品信息
+ (NSArray *)fuzzyQuerySymbolByContent:(NSString *)search cataId:(uint64_t)cataId
{
    NSDictionary *infoDic = @{
                              kValue:search,
                              kCataId:@(cataId)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataFuzzyCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic additon:infoDic userId:0 conditionType:ConditionQueryTypeOther];
}

+ (NSArray *)fuzzyQuerySymbolByContent:(NSString *)search
                                cataId:(uint64_t)cataId
                        accountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *infoDic = @{
                              kValue:search,
                              kCataId:@(cataId),
                              kGroupId:@(accountGroupid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataFuzzyCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic additon:infoDic userId:0 conditionType:ConditionQueryTypeValue3A];
}

+ (NSArray *)fuzzyQuerySymbolSymInfoByContent:(NSString *)search
                                       cataId:(uint64_t)cataId
                               accountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *infoDic = @{
                              kValue:search,
                              kCataId:@(cataId),
                              kGroupId:@(accountGroupid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataFuzzyCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic additon:infoDic userId:0 conditionType:ConditionQueryTypeValue3B];
}

+ (NSArray *)fuzzyQuerySymbolSymCataByContent:(NSString *)search
                                       cataId:(uint64_t)cataId
                               accountGroupid:(uint64_t)accountGroupid {
    
    NSDictionary *infoDic = @{
                              kValue:search,
                              kCataId:@(cataId),
                              kGroupId:@(accountGroupid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataFuzzyCheck:[item_symbol new] protocolCommand:CMD_SYMBOL_LIST sqlIndexKey:indexDic additon:infoDic userId:0 conditionType:ConditionQueryTypeValue3C];
}

#pragma mark  查询产品UUID最大值
+ (NSDictionary *)querySymbolUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SYMBOL_LIST userId:0];
}

#pragma mark 删除产品信息
+ (BOOL)deleteSymbolInfo:(id)model {
    
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SYMBOL_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量删除用户信息
+ (BOOL)deleteBatchSymbolInfos:(NSArray *)modelArr
{
    return [IXDBModel dealWithDataBatchDelete:modelArr protocolCommand:CMD_SYMBOL_LIST userId:0];
}

@end
