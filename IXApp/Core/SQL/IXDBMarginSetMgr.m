//
//  IXDBMarginSetMgr.m
//  IXApp
//
//  Created by Evn on 16/12/6.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBMarginSetMgr.h"
#import "IXDBModel.h"

@implementation IXDBMarginSetMgr

#pragma mark 保存语言信息
+ (BOOL)saveMarginSetInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_MARGIN_SET_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SYMBOL_MARGIN_SET_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存语言信息
+ (BOOL)saveBatchMarginSetInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_MARGIN_SET_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SYMBOL_MARGIN_SET_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询用户UUID最大值数据
+ (NSArray *)queryMarginSetByMarginType:(NSString *)marginType {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SYMBOL_MARGIN_SET_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kMarginType:marginType,
                             };
    return  [IXDBModel dealWithDataCheck:[item_symbol_margin_set new] protocolCommand:CMD_SYMBOL_MARGIN_SET_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark  查询用户UUID最大值数据
+ (NSDictionary *)queryMarginSetUUIDMax
{
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SYMBOL_MARGIN_SET_LIST userId:0];
}

#pragma mark 删除产品信息
+ (BOOL)deleteMarginSetInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SYMBOL_MARGIN_SET_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
