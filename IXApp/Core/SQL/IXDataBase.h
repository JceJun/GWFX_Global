//
//  IXDataBase.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//
#import "IXDBAccountMgr.h"
#import "IXDBSymbolCataMgr.h"
#import "IXDBSymbolMgr.h"
#import "IXDBSymbolHotMgr.h"
#import "IXDBSymbolSubMgr.h"
#import "IXDBHolidayMgr.h"
#import "IXDBHolidayCataMgr.h"
#import "IXDBCompanyMgr.h"
#import "IXDBScheduleCataMgr.h"
#import "IXDBScheduleMgr.h"
#import "IXDBLanguageMgr.h"
#import "IXDBMarginSetMgr.h"
#import "IXDBAccountGroupMgr.h"
#import "IXDBSymbolLableMgr.h"
#import "IXDBScheduleMarginMgr.h"
#import "IXDBHolidayMarginMgr.h"
#import "IXDBSearchBrowseMgr.h"
#import "IXDBYesterdayPriceMgr.h"
#import "IXQuotePriceMgr.h"
#import "IXDBDeepPriceMgr.h"
#import "IXDBAccountGroupSymCataMgr.h"
#import "IXDBQuoteDelayMgr.h"
#import "IXDBGroupSymbolMgr.h"
#import "IXDBGroupSymCataMgr.h"
#import "IXDBVersionMgr.h"
#import "IXDBEodTimeMgr.h"
#import "IXDBSecureDevMgr.h"
#import "IXDBG_SMgr.h"
#import "IXDBG_S_CataMgr.h"
#import "IXDBLpchaccMgr.h"
#import "IXDBLpchaccSymbolMgr.h"
#import "IXDBLpchannelMgr.h"
#import "IXDBLpchannelSymbolMgr.h"
#import "IXDBLpibBindMgr.h"

