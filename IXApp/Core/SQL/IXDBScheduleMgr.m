//
//  IXDBScheduleMgr.m
//  IXApp
//
//  Created by Evn on 16/11/23.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBScheduleMgr.h"
#import "IXDBModel.h"
#import "IxProtoSchedule.pbobjc.h"

@implementation IXDBScheduleMgr

#pragma mark   保存时间表信息
+ (BOOL)saveScheduleInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SCHEDULE_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存时间表信息
+ (BOOL)saveBatchScheduleInfos:(NSArray *)modelArr
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SCHEDULE_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询时间表信息
+ (NSArray *)queryAllScheduleInfo {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_schedule new] protocolCommand:CMD_SCHEDULE_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark   根据交易时间分类ID查询交易时间信息
+ (NSDictionary *)queryScheduleByScheduleId:(uint64_t)scheduleId {
    
    NSDictionary *condDic = @{
                              kID:@(scheduleId)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_schedule new] protocolCommand:CMD_SCHEDULE_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  根据交易时间分类ID查询交易时间信息
+ (NSArray *)queryScheduleByScheduleCataId:(uint64_t)scheduleCataId {
    
    NSDictionary *condDic = @{
                              kScheduleCataId:@(scheduleCataId),
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_schedule new] protocolCommand:CMD_SCHEDULE_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark  根据交易时间分类ID以及星期查询交易时间信息
+ (NSArray *)queryScheduleByScheduleCataId:(uint64_t)scheduleCataId dayOfWeek:(uint64_t)dayOfWeek {
    
    NSDictionary *condDic = @{
                              kScheduleCataId:@(scheduleCataId),
                              kDayOfWeek:@(dayOfWeek)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_schedule new] protocolCommand:CMD_SCHEDULE_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
}

#pragma mark  通过当前时间查询交易时间
+ (NSDictionary *)queryScheduleByCurrentTime:(uint64_t)currentTime dayOfWeek:(uint64_t)dayOfWeek {
    
    NSDictionary *condDic = @{
                              kCurrentTime:@(currentTime),
                              kEnable:@(1),
                              kDayOfWeek:@(dayOfWeek)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SCHEDULE_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_schedule new] protocolCommand:CMD_SCHEDULE_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue3];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询时间表UUID最大值
+ (NSDictionary *)queryScheduleUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SCHEDULE_LIST userId:0];
}

#pragma mark  删除时间表
+ (BOOL)deleteSchedule:(id)model
{
    item_schedule *pb = [item_schedule new];
    pb.id_p = ((proto_schedule_delete *)model).id_p;
   return [IXDBModel dealWithDataDelete:pb protocolCommand:CMD_SCHEDULE_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
