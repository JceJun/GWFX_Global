//
//  IXDataBaseModel.h
//  IXFMDB
//
//  Created by ixiOSDev on 16/11/1.
//  Copyright © 2016年 ixiOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXDBGlobal.h"
#import "IxItemUser.pbobjc.h"
#import "IxItemSymbol.pbobjc.h"
#import "IxItemSymbolCata.pbobjc.h"
#import "IxItemSymbol.pbobjc.h"
#import "IxItemSymbolCata.pbobjc.h"
#import "IxItemAccount.pbobjc.h"
#import "IxItemHolidayCata.pbobjc.h"
#import "IxItemHoliday.pbobjc.h"
#import "IxItemSymbolSubCata.pbobjc.h"
#import "IxItemSymbolSub.pbobjc.h"
#import "IxItemCompany.pbobjc.h"
#import "IxItemSchedule.pbobjc.h"
#import "IxItemScheduleCata.pbobjc.h"
#import "IxItemSymbolHot.pbobjc.h"
#import "IxItemLanguage.pbobjc.h"
#import "IxItemSymbolMarginSet.pbobjc.h"
#import "IxItemAccountGroup.pbobjc.h"
#import "IxItemSymbolLabel.pbobjc.h"
#import "IxItemHolidayMargin.pbobjc.h"
#import "IxItemScheduleMargin.pbobjc.h"
#import "IxItemAccountGroupSymbolCata.pbobjc.h"
#import "IxItemQuoteDelay.pbobjc.h"
#import "IxItemEodTime.pbobjc.h"
#import "IxItemSecureDev.pbobjc.h"
#import "IxItemGroupSymbol.pbobjc.h"
#import "IxItemGroupSymbolCata.pbobjc.h"
#import "IxItemLpchacc.pbobjc.h"
#import "IxItemLpchaccSymbol.pbobjc.h"
#import "IxItemLpchannel.pbobjc.h"
#import "IxItemLpchannelSymbol.pbobjc.h"
#import "IxItemLpibBind.pbobjc.h"


@interface IXDBModel : NSObject

/**
 *  create table sql
 *
 *  @param tableName 表名
 *  @param indexDic 表字段
 *
 *  @return sql语句
 */
+ (NSString *)tableNameToSql:(NSString *)tableName
                 sqlIndexKey:(NSDictionary *)indexDic
                      userId:(uint64_t)userId;;


/**
 *  create table sql
 *
 *  @param cmd protocal commond
 *  @param indexDic 表字段 *
 *  @return sql语句
 */
+ (NSString *)tableNameToSqlProtocolCommand:(uint16)cmd
                                sqlIndexKey:(NSDictionary *)indexDic
                                     userId:(uint64_t)userId;;

/**
 *  create execute sql
 *
 *  @param model     protobuf model
 *  @param tableName 表名
 *  @param addInfos  附加信息
 *  @param indexDic  表字段
 *  @param type      数据库操作类别
 *  @param condType  条件类型
 *
 *  @return sql语句
 */
+ (NSString *)sqlModelToSql:(id)model
                  tableName:(NSString *)tableName
                    additon:(NSDictionary *)addDic
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
              conditionType:(ConditionQueryType)condType;

/**
 *  create execute sql
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param addInfos  附加信息
 *  @param indexDic  表字段
 *  @param type      数据库操作类别
 *  @param condType  条件类型
 *
 *  @return sql语句
 */
+ (NSString *)sqlModelToSql:(id)model
            protocolCommand:(uint16)cmd
                    additon:(NSDictionary *)addDic
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
              conditionType:(ConditionQueryType)condType;

/**
 *  create execute check sql
 *
 *  @param cmd    protocal commond
 *
 *  @return sql语句
 */
+ (NSString *)sqlCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                          userId:(uint64_t)userId;

/**
 *  create execute check sql
 *
 *  @param cmd    protocal commond
 *  @param infoDic 关联信息
 *  @return sql语句
 */
+ (NSString *)sqlCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                          addInfo:(NSDictionary *)infoDic;



/**
 *  create execute sql set
 *
 *  @param modelArr  protobuf model集合
 *  @param tableName 表名
 *  @param addArr    附加信息集合
 *  @param indexArr  表字段集合
 *  @param type      数据库操作类别
 *  @param condType  条件类型
 *
 *  @return sql set
 */
+ (NSArray *)sqlModelToSqls:(NSArray *)modelArr
                  tableName:(NSString *)tableName
               additonInfos:(NSArray *)addArr
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId
              conditionType:(ConditionQueryType)condType;

/**
 *  sql集合值
 *
 *  @param modelArr  protobuf model集合
 *  @param cmd       protocal commond
 *  @param addArr    附加信息集合
 *  @param indexArr  表字段集合
 *  @param type      数据库操作类别
 *
 *  @return sql集合值
 */
+ (NSArray *)sqlModelToSqls:(NSArray *)modelArr
            protocolCommand:(uint16)cmd
               additonInfos:(NSArray *)addArr
                sqlIndexKey:(NSDictionary *)indexDic
        dataBaseOperateType:(DataBaseOperateType)type
                     userId:(uint64_t)userId;

/**
 *  处理数据保存
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param indexDic  表字段
 *  @param type      数据库操作类别
 *  @param userId    用户ID
 *  @param condType  条件类型
 *
 *  @return 保存状态
 */
+ (BOOL)dealWithDataSave:(id)model
         protocolCommand:(uint16)cmd
             sqlIndexKey:(NSDictionary *)indexDic
     dataBaseOperateType:(DataBaseOperateType)type
                  userId:(uint64_t)userId
           conditionType:(ConditionQueryType)condType;

/**
 *  批量处理数据保存
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param indexDic  表字段
 *  @param type      数据库操作类别
 *  @param condType  条件类型
 *
 *  @return 保存状态
 */
+ (BOOL)dealWithBatchDataSave:(NSArray *)modelArr
              protocolCommand:(uint16)cmd
                  sqlIndexKey:(NSDictionary *)indexDic
          dataBaseOperateType:(DataBaseOperateType)type
                       userId:(uint64_t)userId
                conditionType:(ConditionQueryType)condType;

/**
 *  处理数据查询
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param indexDic  表字段
 *  @param condDic   查询条件
 *  @param userId    用户ID
 *  @param condType  条件类型
 *
 *  @return 查询结果
 */
+ (NSArray *)dealWithDataCheck:(id)model
               protocolCommand:(uint16)cmd
                   sqlIndexKey:(NSDictionary *)indexDic
                     queryCond:(NSDictionary *)condDic
                        userId:(uint64_t)userId
                 conditionType:(ConditionQueryType)condType;

/**
 *  处理自定义数据查询
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param indexDic  表字段
 *  @param condArr   查询条件
 *  @param condType  条件类型
 *  @param userId    用户ID
 *
 *  @return 查询结果
 */
+ (NSArray *)dealWithSelfDefineCheckProtocolCommand:(uint16)cmd
                                        sqlIndexKey:(NSDictionary *)indexDic
                                          queryCond:(NSArray *)condArr
                                      conditionType:(ConditionQueryType)condType
                                             userId:(uint64_t)userId
                                          accountId:(uint64_t)accountId;

/**
 *  处理模糊数据查询
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param indexDic  表字段
 *  @param userId    用户ID
 *  @param condType  条件类型
 *
 *  @return 查询结果
 */
+ (NSArray *)dealWithDataFuzzyCheck:(id)model
                    protocolCommand:(uint16)cmd
                        sqlIndexKey:(NSDictionary *)indexDic
                            additon:(NSDictionary *)addInfo
                             userId:(uint64_t)userId
                      conditionType:(ConditionQueryType)condType;

/**
 *  处理条件数据查询
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param indexDic  表字段
 *  @param userId    用户ID
 *
 *  @return 查询结果
 */
+ (NSArray *)dealWithCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                         sqlIndexKey:(NSDictionary *)indexDic
                                              userId:(uint64_t)userId;

+ (NSArray *)dealWithCondCheckUUIDMaxProtocolCommand:(uint16)cmd
                                         sqlIndexKey:(NSDictionary *)indexDic
                                              addInfo:(NSDictionary *)infoDic;

/**
 *  处理数据删除
 *
 *  @param model     protobuf model
 *  @param cmd       protocal commond
 *  @param userId    用户ID
 *  @param condType  条件类型
 *
 *  @return 查询结果
 */
+ (BOOL)dealWithDataDelete:(id)model
                protocolCommand:(uint16)cmd
                         userId:(uint64_t)userId
                  conditionType:(ConditionQueryType)condType;

/**
 *  批量处理数据删除
 *
 *  @param modelArr  protobuf model集合
 *  @param cmd       protocal commond
 *  @param userId    用户ID
 *
 *  @return 查询结果
 */
+ (BOOL)dealWithDataBatchDelete:(NSArray *)modelArr
                protocolCommand:(uint16)cmd
                         userId:(uint64_t)userId;

/**
 *  组装数据库字段
 *
 *  @param cmd       protocal commond
 *  @param indexDic  表字段
 *  @param userId    用户ID
 *
 *  @return 字段数据
 */
+ (NSDictionary *)packageIndexProtocolCommand:(uint16)cmd
                           serverType:(IXDBServerType)serverType
                          dataBaseOperateType:(DataBaseOperateType)type
                                       userId:(uint64_t)userId;

/**
 *  查询用户UUID最大值数据
 *
 *  @param cmd       protocal commond
 *  @param userId    用户ID
 *
 *  @return 用户UUID最大值数据
 */
+ (NSDictionary *)dealWithCheckUUIDMaxProtocolCommand:(uint16)cmd
                                               userId:(uint64_t)userId;

+ (NSDictionary *)dealWithCheckUUIDMaxProtocolCommand:(uint16)cmd
                                               addInfo:(NSDictionary *)infoDic;

/**
 *
 *  执行sql语句
 *  @param infoDic   自定义参数
 *  @param type      数据库操作类型
 *  @param userId    用户ID
 *  @param accountId 账户ID
 *
 *  @return 执行sql语句结果
 */
+ (BOOL)dealWithSelfDefineData:(NSDictionary *)infoDic
               protocolCommand:(uint16)cmd
                   sqlIndexKey:(NSDictionary *)indexDic
           dataBaseOperateType:(DataBaseOperateType)type
                 conditionType:(ConditionQueryType)condType
                        userId:(uint64_t)userId
                     accountId:(uint64_t)accountId;

/**
 *
 *  批量执行sql语句
 *  @param infoArr   自定义参数
 *  @param type      数据库操作类型
 *  @param condType  条件类型
 *  @param userId    用户ID
 *
 *  @return 执行sql语句结果
 */
+ (BOOL)dealWithBatchSelfDefineData:(NSArray *)infoArr
                    protocolCommand:(uint16)cmd
                        sqlIndexKey:(NSDictionary *)indexDic
                dataBaseOperateType:(DataBaseOperateType)type
                      conditionType:(ConditionQueryType)condType
                             userId:(uint64_t)userId
                          accountId:(uint64_t)accountId;

/**
 *  清空表数据
 *  @param tableName   数据表名
 *
 *  @return 执行sql语句结果
 */
+ (BOOL)deleteDBTable:(NSString *)tableName;

/**
 *  中文转拼音
 *
 *  @param cStr 中文
 *
 *  @return 拼音字符串
 */
+ (NSString *)chineseToPinyin:(NSString *)cStr;

@end
