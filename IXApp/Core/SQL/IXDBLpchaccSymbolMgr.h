//
//  IXDBLpchaccSymbolMgr.h
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBLpchaccSymbolMgr : NSObject

/**
 *  保存LPChannelAccount_symbol信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveLpchaccSymbolInfo:(id)model;

/**
 *  批量保存LPChannelAccount_symbol信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchLpchaccSymbolInfos:(NSArray *)modelArr;

/**
 *  查询LPChannelAccount_symbol
 *  @param lpchaccid
 *  @param symbolid 产品ID
 *
 */
+ (NSDictionary *)queryLpchaccSymbolByLpchaccid:(uint64_t)lpchaccid
                                       symbolid:(uint64_t)symbolid;

/**
 *  查询LPChannelAccount_symbolUUID最大值数据
 *
 */
+ (NSDictionary *)queryLpchaccSymbolUUIDMax;

/**
 *  删除LPChannelAccount_symbol信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteLpchaccSymbol:(id)model;

@end
