//
//  IXDBSecureDevMgr.m
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBSecureDevMgr.h"
#import "IXDBModel.h"

@implementation IXDBSecureDevMgr

#pragma mark   保存登录设备信息
+ (BOOL)saveSecureDevInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SECURE_DEV_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SECURE_DEV_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存登录设备信息
+ (BOOL)saveBatchSecureDevInfos:(NSArray *)modelArr
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SECURE_DEV_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SECURE_DEV_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark   查询登录设备信息
+ (NSArray *)querySecureDevInfoByUserid:(uint64_t)userid
{
    NSDictionary *condDic = @{
                              kUserId:@(userid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SECURE_DEV_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_secure_dev new] protocolCommand:CMD_SECURE_DEV_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark 删除登录设备信息
+ (BOOL)deleteSecureDevInfo:(id)model
                     userid:(uint64_t)userid
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SECURE_DEV_LIST userId:userid conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询登录设备UUID最大值
+ (NSDictionary *)querySecureDevUUIDMaxUserid:(uint64_t)userid
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SECURE_DEV_LIST userId:userid];
}

#pragma mark  清空表数据
+ (BOOL)deleteSecureDevInfos
{
    return [IXDBModel deleteDBTable:kTableNameSecureDev];
}

@end
