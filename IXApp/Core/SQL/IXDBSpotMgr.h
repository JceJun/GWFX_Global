//
//  IXDBSpotMgr.h
//  IXApp
//
//  Created by Evn on 2018/3/4.
//  Copyright © 2018年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBSpotMgr : NSObject

/**
 *   保存Spot信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveSpotInfo:(id)model;

/**
 *  批量保存Spot信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchSpotInfos:(NSArray *)modelArr;

/**
 *  删除Spot信息
 *
 */
+ (BOOL)deleteSpotInfo:(id)model;

/**
 *  查询Spot信息
 *  @param accountId 账户ID
 *
 */
+ (NSArray *)querySpotInfoByAccounId:(uint64_t)accountId;

/**
 *  查询Spot信息
 *  @param groupId 账户组ID
 *
 */
+ (NSArray *)querySpotInfoByGroupId:(uint64_t)groupId;

/**
 *  查询Spot信息
 *  @param name 货币名称
 *  @param groupId 账户组ID
 *
 */
+ (NSDictionary *)querySpotInfoByName:(NSString *)name
                              groupId:(uint64_t)groupId;


/**
 *  查询SpotUUID最大值
 *
 */
+ (NSDictionary *)querySpotUUIDMax;

/**
 *  清空表数据
 *
 *
 */
+ (BOOL)deleteSpotInfos;

@end
