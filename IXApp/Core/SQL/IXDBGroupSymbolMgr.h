//
//  IXDBGroupSymbolMgr.h
//  IXApp
//
//  Created by Evn on 17/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBGroupSymbolMgr : NSObject


/**
 *  保存账户组产品信息
 *
 *  @param infoDic 账户组产品
 *
 */
+ (BOOL)saveGroupSymbolInfo:(NSDictionary *)infoDic;

/**
 *  批量保存账户组产品信息
 *
 *  @param infoArr 账户组产品集合
 *
 */
+ (BOOL)saveBatchGroupSymbolInfos:(NSArray *)infoArr;


/**
 *  查询自定义账户组产品
 *  @param accGroupId  账户组ID
 *  @param symbolId    产品ID
 *
 */
+ (NSDictionary *)queryGroupSymbolByAccountGroupId:(uint64_t)accGroupId
                                  symbolId:(uint64_t)symbolId;

/**
 *  清空表数据
 *
 *
 */
+ (BOOL)deleteGroupSymbolInfos;
@end
