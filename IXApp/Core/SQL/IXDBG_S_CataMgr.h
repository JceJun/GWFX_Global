//
//  IXDBG_S_CataMgr.h
//  IXApp
//
//  Created by Evn on 2017/8/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBG_S_CataMgr : NSObject

/**
 *  保存账户组产品分类信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveG_S_CataInfo:(id)model;

/**
 *  批量保存账户组产品分类信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchG_S_CataInfos:(NSArray *)modelArr;

/**
 *  查询账户组产品信息
 *  @param accGroupId  账户组ID
 *  @param symCataId   产品分类ID
 *
 */
+ (NSDictionary *)queryG_S_CataByAccountGroupId:(uint64_t)accGroupId
                                  symbolCataId:(uint64_t)symCataId;

/**
 *  删除账户组产品分类信息
 *
 *  @param model protocalModel集合
 *
 */
+ (BOOL)deleteG_S_CataInfo:(id)model;

/**
 *  批量删除账户组产品分类信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)batchDeleteG_S_CataInfo:(GPBUInt64Array *)modelArr;

/**
 *  查询账户组产品分类UUID最大值数据
 *
 */
+ (NSDictionary *)queryG_S_CataUUIDMax;

@end
