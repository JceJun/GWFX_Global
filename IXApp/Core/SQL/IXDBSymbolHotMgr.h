//
//  IXDBSymbolHotMgr.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBSymbolHotMgr : NSObject


/**
 *  保存热门产品信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveSymbolHotInfo:(id)model;

/**
 *  批量保存热门产品信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchSymbolHotInfos:(NSArray *)modelArr;

/**
 *  分页查询热门产品信息
 *
 */
+ (NSArray *)querySymbolHotStartIndex:(uint64_t)sIndex
                               offset:(uint64_t)offset;

/**
 *  查询热门产品信息
 *
 */
+ (NSArray *)queryAllSymbolsHot;

/**
 *  查询热门产品信息
 *
 */
+ (NSArray *)queryAllSymbolsHotAccountGroupid:(uint64_t)accountGroupid;

+ (NSArray *)queryAllSymbolsHotSymInfoAccountGroupid:(uint64_t)accountGroupid;

+ (NSArray *)queryAllSymbolsHotSymCataInfoAccountGroupid:(uint64_t)accountGroupid;

/**
 *  查询热门产品UUID最大值
 *
 */
+ (NSDictionary *)querySymbolHotUUIDMax;

/**
 *  查询产品marketId
 *
 *  @param symbolId 产品ID
 *
 */
+ (NSDictionary *)querySymbolMarketIdsBySymbolId:(uint64_t)symbolId;

/**
 *  批量查询产品marketId
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (NSArray *)queryBatchSymbolMarketIdsBySymbolIds:(NSArray *)modelArr;

@end
