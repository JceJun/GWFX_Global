//
//  IXDBSecureDevMgr.h
//  IXApp
//
//  Created by Evn on 2017/7/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBSecureDevMgr : NSObject

/**
 *   保存登录设备信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveSecureDevInfo:(id)model;

/**
 *  批量保存登录设备信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchSecureDevInfos:(NSArray *)modelArr;

/**
 *  查询登录设备信息
 *  @param userid 用户ID
 *
 */
+ (NSArray *)querySecureDevInfoByUserid:(uint64_t)userid;

/**
 *  删除登录设备信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)deleteSecureDevInfo:(id)model
                     userid:(uint64_t)userid;

/**
 *  查询登录设备UUID最大值
 *
 */
+ (NSDictionary *)querySecureDevUUIDMaxUserid:(uint64_t)userid;

/**
 *  清空表数据
 *
 *
 */
+ (BOOL)deleteSecureDevInfos;

@end
