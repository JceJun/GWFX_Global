//
//  IXDBSymbolLableMgr.h
//  IXApp
//
//  Created by Evn on 16/12/9.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBSymbolLableMgr : NSObject

/**
 *  保存产品标签信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveSymbolLableInfo:(id)model;

/**
 *  批量保存产品标签信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchSymbolLableInfos:(NSArray *)modelArr;

/**
 *  查询产品标签信息
 *  @param symbolId 产品ID
 *
 */
+ (NSArray *)querySymbolLableBySymbolId:(uint64_t)symbolId;

/**
 *  查询用户UUID最大值数据
 *
 */
+ (NSDictionary *)querySymbolLableUUIDMax;

/**
 *  删除标签信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteLableInfo:(id)model;

@end
