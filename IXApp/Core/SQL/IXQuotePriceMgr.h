//
//  IXQuotePriceMgr.h
//  IXApp
//
//  Created by Evn on 17/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXQuotePriceMgr : NSObject

/**
 *  保存行情价信息
 *
 *  @param priceDic 行情价
 *
 */
+ (BOOL)saveQuotePriceInfo:(NSDictionary *)priceDic;

/**
 *  批量保存行情价信息
 *
 *  @param infoArr 行情价
 *
 */
+ (BOOL)saveBatchQuotePriceInfo:(NSArray *)infoArr;

/**
 *  查询行情价信息
 *  @param symbolId 产品ID
 *
 */
+ (NSDictionary *)queryQuotePriceBySymbolId:(uint64_t)symbolId;

/**
 *  批量查询行情价信息
 *  @param symbolId 产品ID
 *
 */
+ (NSArray *)queryBatchQuotePriceBySymbolIds:(NSArray *)symbolIdArr;

@end
