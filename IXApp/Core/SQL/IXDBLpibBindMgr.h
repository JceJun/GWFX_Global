//
//  IXDBLpibBindMgr.h
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBLpibBindMgr : NSObject

/**
 *  保存IB_Bind信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveLpibBindInfo:(id)model;

/**
 *  批量保存IB_Bind信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchLpibBindInfos:(NSArray *)modelArr;

/**
 *  查询LpIbBind
 *  @param lpuserid 代理用户ID
 *
 */
+ (NSDictionary *)queryLpIbBindByLpUserId:(uint64_t)lpuserid;

/**
 *  查询IB_BindUUID最大值数据
 *
 */
+ (NSDictionary *)queryLpibBindUUIDMax;

/**
 *  删除IB_Bind信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteLpibBind:(id)model;

@end
