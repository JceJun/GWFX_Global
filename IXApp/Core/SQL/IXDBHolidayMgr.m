//
//  IXDBHolidayMgr.m
//  IXApp
//
//  Created by ixiOSDev on 16/11/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBHolidayMgr.h"
#import "IXDBModel.h"
#import "IxProtoHoliday.pbobjc.h"

@implementation IXDBHolidayMgr

#pragma mark  保存假日
+ (BOOL)saveHolidayInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_HOLIDAY_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存假日信息
+ (BOOL)saveBatchHolidayInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_HOLIDAY_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询假日信息
+ (NSArray *)queryAllHoliday {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_holiday new] protocolCommand:CMD_HOLIDAY_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  通过假期ID查询假期信息
+ (NSDictionary *)queryHolidayByHolidayId:(uint64_t)holidayId {
    
    NSDictionary *condDic = @{
                              kID:@(holidayId),
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_holiday new] protocolCommand:CMD_HOLIDAY_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  根据假期分类ID查询假期信息
+ (NSArray *)queryHolidayByHolidayCataId:(uint64_t)holidayCataId {
    
    NSDictionary *condDic = @{
                              kHolidayCataId:@(holidayCataId),
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return [IXDBModel dealWithDataCheck:[item_holiday new] protocolCommand:CMD_HOLIDAY_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
}

#pragma mark  通过当前时间查询假期
+ (NSDictionary *)queryHolidayByCurrentTime:(uint64_t)currentTime {
    
    NSDictionary *condDic = @{
                              kCurrentTime:@(currentTime),
                              kEnable:@(1)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_holiday new] protocolCommand:CMD_HOLIDAY_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询假日UUID最大值
+ (NSDictionary *)queryHolidayUUIDMax {
    
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_HOLIDAY_LIST userId:0];
}

#pragma mark  删除假期
+ (BOOL)deleteHoliday:(id)model
{
    item_holiday *pb = [item_holiday new];
    pb.id_p = ((proto_holiday_delete *)model).id_p;
    return [IXDBModel dealWithDataDelete:pb protocolCommand:CMD_HOLIDAY_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
