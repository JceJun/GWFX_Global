//
//  IXDBSymbolSubMgr.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/11.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXDBGlobal.h"

@interface IXDBSymbolSubMgr : NSObject

/**
 *  保存自选产品信息
 *
 *  @param model protocalModel
 *  @param accountId 账户ID
 *
 */
+ (BOOL)saveSymbolSubInfo:(id)model accountId:(uint64_t)accountId;

/**
 *  批量保存自选产品信息
 *
 *  @param modelArr protocalModel集合
 *  @param accountId 账户ID
 *
 */
+ (BOOL)saveBatchSymbolSubInfos:(NSArray *)modelArr accountId:(uint64_t)accountId;

/**
 *  分页查询自选产品信息
 *  @param accountId 账户ID
 *
 */
+ (NSArray *)querySymbolSubStartIndex:(uint64_t)sIndex
                                 offset:(uint64_t)offset
                                 accountId:(uint64_t)accountId;



/**
 * 根据产品ID查询自选ID
 *  @param symbolId 产品ID
 *  @param accountId 账户ID
 *
 */
+ (NSDictionary *)querySymbolSubBySymbolId:(uint64_t)symbolId
                                    accountId:(uint64_t)accountId;

/**
 *  查询所有自选产品信息
 *  @param accountId 账户ID
 *  @param symbolCataId 产品分类ID
 *
 */
+ (NSArray *)querySymbolsSubAccountId:(uint64_t)accountId symbolCataId:(uint64_t)symbolCataId;

/**
 *  查询所有自选产品信息
 *  @param accountId 账户ID
 *
 */
+ (NSArray *)queryAllSymbolsSubAccountId:(uint64_t)accountId;

+ (NSArray *)queryAllSymbolsSubAccountId:(uint64_t)accountId
                          AccountGroupid:(uint64_t)accountGroupid;

+ (NSArray *)queryAllSymbolsSymInfoSubAccountId:(uint64_t)accountId
                          AccountGroupid:(uint64_t)accountGroupid;

+ (NSArray *)queryAllSymbolsSymCataSubAccountId:(uint64_t)accountId
                                 AccountGroupid:(uint64_t)accountGroupid;

/**
 *  查询自选产品UUID最大值
 *  @param accountId 账户ID
 *
 */
+ (NSDictionary *)querySymbolSubUUIDMaxAccountId:(uint64_t)accountId;


/**
 *  查询自选产品UUID最大值
 *  @param infoDic 附加信息
 *
 */
+ (NSDictionary *)querySymbolSubUUIDMaxByAddInfo:(NSDictionary *)infoDic;

/**
 *  删除自选产品信息
 *
 *  @param model protocalModel
 *  @param accountId 账户ID
 *
 */
+ (BOOL)deleteSymbolSubInfo:(id)model accountId:(uint64_t)accountId;

@end
