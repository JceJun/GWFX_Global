//
//  IXDBLanguageMgr.h
//  IXApp
//
//  Created by Evn on 16/12/1.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBLanguageMgr : NSObject

/**
 *  保存语言信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveLanguageInfo:(id)model;

/**
 *  批量保存语言信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchLanguageInfos:(NSArray *)modelArr;

/**
 *  查询用户UUID最大值数据
 *  @param country 语言类别如简体、英文、繁体
 *  @param nameSpace   语言对应命名空间
 *
 */
+ (NSDictionary *)queryLanguageByCounrtry:(NSString *)country nameSpace:(NSString *)nameSpace;

/**
 *  查询用户UUID最大值数据
 *
 */
+ (NSDictionary *)queryLanguageUUIDMax;

@end
