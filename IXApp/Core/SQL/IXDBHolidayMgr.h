//
//  IXDBHolidayMgr.h
//  IXApp
//
//  Created by ixiOSDev on 16/11/12.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBHolidayMgr : NSObject

/**
 *  保存假期
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveHolidayInfo:(id)model;

/**
 *  批量保存假期信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchHolidayInfos:(NSArray *)modelArr;

/**
 *  查询假期信息
 *
 */
+ (NSArray *)queryAllHoliday;

/**
 *  通过假期ID查询假期信息
 *  @param holidayId 假期ID
 *
 */
+ (NSDictionary *)queryHolidayByHolidayId:(uint64_t)holidayId;

/**
 *  根据假期分类ID查询假期信息
 *  @param holidayCataId 假期分类ID
 *
 */
+ (NSArray *)queryHolidayByHolidayCataId:(uint64_t)holidayCataId;

/**
 *  通过当前时间查询假期
 *  @param currentTime 当前时间
 *
 */
+ (NSDictionary *)queryHolidayByCurrentTime:(uint64_t)currentTime;

/**
 *  查询假期UUID最大值
 *
 */
+ (NSDictionary *)queryHolidayUUIDMax;

/**
 *  删除假期
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteHoliday:(id)model;

@end
