//
//  IXDBGroupSymbolMgr.m
//  IXApp
//
//  Created by Evn on 17/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBGroupSymbolMgr.h"
#import "IXDBModel.h"

@implementation IXDBGroupSymbolMgr

#pragma mark 保存账户组产品信息
+ (BOOL)saveGroupSymbolInfo:(NSDictionary *)infoDic
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYM serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithSelfDefineData:infoDic protocolCommand:CMD_GROUP_SYM sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:0 accountId:0];
}

#pragma mark  批量保存账户组产品信息
+ (BOOL)saveBatchGroupSymbolInfos:(NSArray *)infoArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYM serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchSelfDefineData:infoArr protocolCommand:CMD_GROUP_SYM sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue0 userId:0 accountId:0];
}


#pragma mark  查询自定义账户组产品
+ (NSDictionary *)queryGroupSymbolByAccountGroupId:(uint64_t)accGroupId
                                          symbolId:(uint64_t)symbolId
{
    NSDictionary *condDic = @{
                              kID:@(symbolId),
                              kGroupId:@(accGroupId)
                              };
    NSArray *condArr = @[condDic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_GROUP_SYM serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_GROUP_SYM sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue2 userId:0 accountId:0];
    if (allArr && allArr.count >0) {
        
        return  allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  清空表数据
+ (BOOL)deleteGroupSymbolInfos {
    
    return [IXDBModel deleteDBTable:kTableNameGroupSym];
}

@end
