//
//  IXDBHolidayMarginMgr.m
//  IXApp
//
//  Created by Evn on 16/12/10.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXDBHolidayMarginMgr.h"
#import "IXDBModel.h"

@implementation IXDBHolidayMarginMgr

#pragma mark 保存假期margin
+ (BOOL)saveHolidayMarginInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_MARGIN_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_HOLIDAY_MARGIN_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存假期margin
+ (BOOL)saveBatchHolidayMarginInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_MARGIN_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_HOLIDAY_MARGIN_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  通过symbolId查询假期margin
+ (NSString *)queryHolidayMarginBySymbolId:(uint64_t)symbolId holidayId:(uint16_t)holidayId {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_HOLIDAY_MARGIN_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSDictionary *addDic = @{
                             kSymbolId:@(symbolId),
                             kHolidayId:@(holidayId)
                             };
    NSArray *allArr =  [IXDBModel dealWithDataCheck:[item_holiday_margin new] protocolCommand:CMD_HOLIDAY_MARGIN_LIST sqlIndexKey:indexDic queryCond:addDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        
        NSDictionary *retDic = allArr[0];
        if (retDic) {
            
            return retDic[kMarginType];
        } else {
           
            return nil;
        }
        
    } else {
        
        return nil;
    }
}

#pragma mark  查询假期marginUUID最大值数据
+ (NSDictionary *)queryHolidayMarginUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_HOLIDAY_MARGIN_LIST userId:0];
}

#pragma mark 删除假期margin信息
+ (BOOL)deleteHolidayMarginInfo:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_HOLIDAY_MARGIN_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
