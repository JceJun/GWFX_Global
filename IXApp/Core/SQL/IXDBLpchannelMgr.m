//
//  IXDBLpchannelMgr.m
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBLpchannelMgr.h"
#import "IXDBModel.h"

@implementation IXDBLpchannelMgr

#pragma mark 保存channel信息
+ (BOOL)saveLpchannelInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_LPCHANNEL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存channel信息
+ (BOOL)saveBatchLpchannelInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_LPCHANNEL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}


#pragma mark  查询channelUUID最大值数据
+ (NSDictionary *)queryLpchannelUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_LPCHANNEL_LIST userId:0];
}

#pragma mark  删除channel信息
+ (BOOL)deleteLpchannel:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_LPCHANNEL_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
