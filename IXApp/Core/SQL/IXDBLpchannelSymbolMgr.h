//
//  IXDBLpchannelSymbolMgr.h
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBLpchannelSymbolMgr : NSObject

/**
 *  保存channel_symbol信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveLpchannelSymbolInfo:(id)model;

/**
 *  批量保存channel_symbol信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchLpchannelSymbolInfos:(NSArray *)modelArr;

/**
 *  查询channel_symbol
 *  @param lpCompanyid
 *  @param symbolid 产品ID
 *  @param channelid
 *
 */
+ (NSDictionary *)queryLpchaccSymbolByLpCompanyid:(uint64_t)lpCompanyid
                                       symbolid:(uint64_t)symbolid
                                        channelid:(uint64_t)channelid;

/**
 *  查询channel_symbolUUID最大值数据
 *
 */
+ (NSDictionary *)queryLpchannelSymbolUUIDMax;

/**
 *  删除channel_symbol信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteLpchannelSymbol:(id)model;

@end
