//
//  IXDBLpchannelSymbolMgr.m
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBLpchannelSymbolMgr.h"
#import "IXDBModel.h"

@implementation IXDBLpchannelSymbolMgr

#pragma mark 保存channel_symbol信息
+ (BOOL)saveLpchannelSymbolInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_LPCHANNEL_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存channel_symbol信息
+ (BOOL)saveBatchLpchannelSymbolInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_LPCHANNEL_SYMBOL_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark   查询channel_symbol
+ (NSDictionary *)queryLpchaccSymbolByLpCompanyid:(uint64_t)lpCompanyid
                                         symbolid:(uint64_t)symbolid
                                        channelid:(uint64_t)channelid
{
    NSDictionary *condDic = @{
                              kLpCompanyid:@(lpCompanyid),
                              kSymbolId:@(symbolid),
                              kChannelid:@(channelid)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_LPCHANNEL_SYMBOL_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_lpchannel_symbol new] protocolCommand:CMD_LPCHANNEL_SYMBOL_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue3];
    if (allArr && allArr.count > 0) {
        return allArr[0];
    } else {
        return nil;
    }
}

#pragma mark  查询channel_symbolUUID最大值数据
+ (NSDictionary *)queryLpchannelSymbolUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_LPCHANNEL_SYMBOL_LIST userId:0];
}

#pragma mark  删除channel_symbol信息
+ (BOOL)deleteLpchannelSymbol:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_LPCHANNEL_SYMBOL_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
