//
//  IXDBVersionMgr.m
//  IXApp
//
//  Created by Evn on 2017/4/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBVersionMgr.h"
#import "IXDBModel.h"

@implementation IXDBVersionMgr

#pragma mark  保存数据库版本信息
+ (BOOL)saveDBVersionInfo:(NSDictionary *)verDic {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_DB_VERSION serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithSelfDefineData:verDic protocolCommand:CMD_DB_VERSION sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue1 userId:0 accountId:0];
}

#pragma mark  查询版本信息
+ (NSDictionary *)queryDBVersion
{    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_DB_VERSION serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_DB_VERSION sqlIndexKey:indexDic queryCond:nil conditionType:ConditionQueryTypeValue0 userId:0 accountId:0];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

@end
