//
//  IXDBLpchannelMgr.h
//  IXApp
//
//  Created by Evn on 2017/12/20.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBLpchannelMgr : NSObject

/**
 *  保存channel信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)saveLpchannelInfo:(id)model;

/**
 *  批量保存channel信息
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchLpchannelInfos:(NSArray *)modelArr;

/**
 *  查询channelUUID最大值数据
 *
 */
+ (NSDictionary *)queryLpchannelUUIDMax;

/**
 *  删除channel信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteLpchannel:(id)model;

@end
