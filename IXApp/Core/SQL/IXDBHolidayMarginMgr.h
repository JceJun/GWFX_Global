//
//  IXDBHolidayMarginMgr.h
//  IXApp
//
//  Created by Evn on 16/12/10.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBHolidayMarginMgr : NSObject

/**
*  保存假期margin
*
*  @param model protocalModel
*
*/
+ (BOOL)saveHolidayMarginInfo:(id)model;

/**
 *  批量保存假期margin
 *
 *  @param modelArr protocalModel集合
 *
 */
+ (BOOL)saveBatchHolidayMarginInfos:(NSArray *)modelArr;

/**
 *  通过symbolId查询假期margin
 *  @param symbolId 产品ID
 *  @param holidayId 假期ID
 *
 */
+ (NSString *)queryHolidayMarginBySymbolId:(uint64_t)symbolId holidayId:(uint16_t)holidayId;

/**
 *  查询假期marginUUID最大值数据
 *
 */
+ (NSDictionary *)queryHolidayMarginUUIDMax;

/**
 *  删除假期margin信息
 *
 *  @param model protocalModel
 *
 */
+ (BOOL)deleteHolidayMarginInfo:(id)model;

@end
