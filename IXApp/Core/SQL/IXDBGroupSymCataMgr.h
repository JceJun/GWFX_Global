//
//  IXDBGroupSymCataMgr.h
//  IXApp
//
//  Created by Evn on 17/3/28.
//  Copyright © 2017年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IXDBGroupSymCataMgr : NSObject


/**
 *  保存账户组产品分类
 *
 *  @param infoDic 账户组产品
 *
 */
+ (BOOL)saveGroupSymbolCataInfo:(NSDictionary *)infoDic;

/**
 *  批量保存账户组产品分类信息
 *
 *  @param infoArr 账户组产品集合
 *
 */
+ (BOOL)saveBatchGroupSymbolCatasInfos:(NSArray *)infoArr;


/**
 *  通过symbolCataId查询GroupSymbolCata记录
 *
 *  @param cataId 产品分类ID
 *  @param accountGroupid 账户组ID
 *
 */
+ (NSDictionary *)queryGroupSymbolCataBySymbolCataId:(uint64_t)cataId
                                      accountGroupid:(uint64_t)accountGroupid;
/**
 *  清空表数据
 *
 *
 */
+ (BOOL)deleteGroupSymbolCataInfos;
@end
