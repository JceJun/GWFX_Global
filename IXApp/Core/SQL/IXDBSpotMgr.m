//
//  IXDBSpotMgr.m
//  IXApp
//
//  Created by Evn on 2018/3/4.
//  Copyright © 2018年 IX. All rights reserved.
//

#import "IXDBSpotMgr.h"
#import "IXDBModel.h"

@implementation IXDBSpotMgr

#pragma mark   保存Spot信息
+ (BOOL)saveSpotInfo:(id)model
{
    return YES;
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
//    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_SPOT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存Spot信息
+ (BOOL)saveBatchSpotInfos:(NSArray *)modelArr
{
    return YES;
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
//    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_SPOT_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark 删除Spot信息
+ (BOOL)deleteSpotInfo:(id)model
{
    return YES;
//    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_SPOT_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

/**
 *  查询Spot信息
 *  @param groupId 账户组ID
 *
 */
+ (NSArray *)querySpotInfoByGroupId:(uint64_t)groupId
{
    return [NSArray array];
//    NSDictionary *condDic = @{
//                              kGroupId:@(groupId)
//                              };
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
//    return [IXDBModel dealWithDataCheck:[item_spot new] protocolCommand:CMD_SPOT_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1A];
}

#pragma mark   查询Spot信息
+ (NSArray *)querySpotInfoByAccounId:(uint64_t)accountId
{
    return [NSArray array];
//    NSDictionary *condDic = @{
//                              kAccountId:@(accountId)
//                              };
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
//    return [IXDBModel dealWithDataCheck:[item_spot new] protocolCommand:CMD_SPOT_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
}

+ (NSDictionary *)querySpotInfoByName:(NSString *)name
                              groupId:(uint64_t)groupId
{
    return [NSDictionary dictionary];
//    if (!name) {
//        return nil;
//    }
//    NSDictionary *condDic = @{
//                              kName:name,
//                              kGroupId:@(groupId)
//                              };
//    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_SPOT_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
//    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_spot new] protocolCommand:CMD_SPOT_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
//    if (allArr && allArr.count > 0) {
//        return allArr[0];
//    } else {
//        return nil;
//    }
}

#pragma mark  查询SpotUUID最大值
+ (NSDictionary *)querySpotUUIDMax
{
    return [NSDictionary dictionary];
//    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_SPOT_LIST userId:0];
}

#pragma mark  清空表数据
+ (BOOL)deleteSpotInfos
{
    return [IXDBModel deleteDBTable:KTableNameEodTime];
}

@end
