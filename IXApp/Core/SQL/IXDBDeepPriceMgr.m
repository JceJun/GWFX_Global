//
//  IXDBDeepPriceMgr.m
//  IXApp
//
//  Created by Evn on 17/1/6.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBDeepPriceMgr.h"
#import "IXDBModel.h"

@implementation IXDBDeepPriceMgr

#pragma mark 保存昨收价信息
+ (BOOL)saveDeepPriceInfo:(NSDictionary *)priceDic
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_DEEP_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithSelfDefineData:priceDic protocolCommand:CMD_DEEP_PRICE sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:0 accountId:0];
}

#pragma mark  批量保存昨收价信息
+ (BOOL)saveBatchDeepPriceInfo:(NSArray *)infoArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_DEEP_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchSelfDefineData:infoArr protocolCommand:CMD_DEEP_PRICE sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave conditionType:ConditionQueryTypeValue3 userId:0 accountId:0];
}

#pragma mark  查询昨深度价信息
+ (NSArray *)queryDeepPriceBySymbolId:(uint64_t)symbolId {
    
    NSDictionary *condDic = @{
                              kID:@(symbolId),
                              };
    NSArray *condArr = @[condDic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_DEEP_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
     return  [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_DEEP_PRICE sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue1 userId:0 accountId:0];
}

#pragma mark 查询昨收价信息
+ (NSDictionary *)queryDeepPriceBySymbolId:(uint64_t)symbolId index:(int32_t)index
{
    NSDictionary *condDic = @{
                              kID:@(symbolId),
                              kDeepIndex:@(index)
                              };
    NSArray *condArr = @[condDic];
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_DEEP_PRICE serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithSelfDefineCheckProtocolCommand:CMD_DEEP_PRICE sqlIndexKey:indexDic queryCond:condArr conditionType:ConditionQueryTypeValue2 userId:0 accountId:0];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}


@end
