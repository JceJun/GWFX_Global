//
//  IXDBAccountGroupSymCataMgr.m
//  IXApp
//
//  Created by Evn on 17/1/16.
//  Copyright © 2017年 IX. All rights reserved.
//

#import "IXDBAccountGroupSymCataMgr.h"
#import "IXDBModel.h"

@implementation IXDBAccountGroupSymCataMgr

#pragma mark 保存语言信息
+ (BOOL)saveAccountGroupSymCataInfo:(id)model
{
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithDataSave:model protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  批量保存语言信息
+ (BOOL)saveBatchAccountGroupSymCataInfos:(NSArray *)modelArr {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeSave userId:0];
    return [IXDBModel dealWithBatchDataSave:modelArr protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic dataBaseOperateType:DataBaseOperateTypeSave userId:0 conditionType:ConditionQueryTypeValue0];
}

#pragma mark  查询账户组信息
+ (NSArray *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId {
    
    NSDictionary *condDic = @{
                              kAccGroupId:@(accGroupId)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_account_group_symbol_cata new] protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue1];
    if (allArr && allArr.count > 0) {
        
        return allArr;
    } else {
        
        return nil;
    }
}

#pragma mark 查询账户组产品分类信息
+ (NSArray *)queryAccountGroupSymCataBySymbolIdAccountGroupId:(uint64_t)accGroupId type:(uint64_t)type {
    
    NSDictionary *condDic = @{
                              kAccGroupId:@(accGroupId),
                              kType:@(type)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_account_group_symbol_cata new] protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2A];
}

#pragma mark  查询账户组产品分类信息
+ (NSArray *)queryAccountGroupSymCataBySymbolCataIdAccountGroupId:(uint64_t)accGroupId type:(uint64_t)type{
    
    NSDictionary *condDic = @{
                              kAccGroupId:@(accGroupId),
                              kType:@(type)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    return  [IXDBModel dealWithDataCheck:[item_account_group_symbol_cata new] protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2B];
}

+ (NSDictionary *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId symbolId:(uint64_t)symbolId{
    
    NSDictionary *condDic = @{
                              kAccGroupId:@(accGroupId),
                              kSymbolId:@(symbolId)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_account_group_symbol_cata new] protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

+ (NSDictionary *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId
                                                  symbolId:(uint64_t)symbolId
                                                      type:(uint64_t)type
{
    NSDictionary *condDic = @{
                              kAccGroupId:@(accGroupId),
                              kSymbolId:@(symbolId),
                              kType:@(type)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_account_group_symbol_cata new] protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue3];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

+ (NSDictionary *)queryAccountGroupSymCataByAccountGroupId:(uint64_t)accGroupId symbolCataId:(uint64_t)symbolCataId {
    
    NSDictionary *condDic = @{
                              kAccGroupId:@(accGroupId),
                              KSymbolCataId:@(symbolCataId)
                              };
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_account_group_symbol_cata new] protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:condDic userId:0 conditionType:ConditionQueryTypeValue2];
    if (allArr && allArr.count > 0) {
        
        return allArr[0];
    } else {
        
        return nil;
    }
}

#pragma mark  查询账户组信息
+ (NSArray *)queryAllAccountGroupSymCata {
    
    NSDictionary *indexDic = [IXDBModel packageIndexProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST serverType:IXDBServerTypeTrade dataBaseOperateType:DataBaseOperateTypeQuery userId:0];
    NSArray *allArr = [IXDBModel dealWithDataCheck:[item_account_group_symbol_cata new] protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST sqlIndexKey:indexDic queryCond:nil userId:0 conditionType:ConditionQueryTypeValue0];
    return allArr;
}

#pragma mark  查询用户UUID最大值数据
+ (NSDictionary *)queryAccountGroupSymCataUUIDMax
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST userId:0];
}

+ (NSDictionary *)queryAccountGroupSymCataUUIDMaxByAddInfo:(NSDictionary *)infoDic
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST addInfo:infoDic];
}

+ (NSDictionary *)queryAccountGroupSymCataUUIDMaxByAccountGroupId:(uint64_t)accGroupId
{
    return [IXDBModel dealWithCheckUUIDMaxProtocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST userId:accGroupId];
}

#pragma mark 删除账户组产品分类信息
+ (BOOL)deleteAccountGroupSymCata:(id)model
{
    return [IXDBModel dealWithDataDelete:model protocolCommand:CMD_ACCOUNTGROUPSYMBOLCATA_LIST userId:0 conditionType:ConditionQueryTypeValue0];
}

@end
