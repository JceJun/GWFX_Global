//
//  IXAFType.h
//  IXApp
//
//  Created by Magee on 2016/12/29.
//  Copyright © 2016年 IX. All rights reserved.
//

//#ifndef IXAFType_h
//#define IXAFType_h

#define IXAFRequestTimeOut 10u            //AF请求超时
#define IXAFUploadTimeOut  60             //上传文件超时
#define PLATFORM @"IX"
typedef void(^respResult)(BOOL success1, NSString *errCode1, NSString *errStr1, id obj1);

