//
//  IXAFRequest.m
//  IXApp
//
//  Created by Magee on 2016/12/26.
//  Copyright © 2016年 IX. All rights reserved.
//

#import "IXAFRequest.h"
#import "RSA.h"
#import "IXCpyConfig.h"
@implementation IXAFRequest

#pragma mark -
#pragma mark - 网络层
static  AFHTTPSessionManager *manager = nil;
+ (void)configHttpSessionManager
{
    if (!manager) {
        manager = [AFHTTPSessionManager manager];
        [manager setSecurityPolicy:[IXAFRequest customSecurityPolicy]];
    }
    [manager.requestSerializer setTimeoutInterval:IXAFRequestTimeOut];
}

+ (AFSecurityPolicy *)customSecurityPolicy
{
    //先导入证书，找到证书的路径
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"https" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    
    //AFSSLPinningModeCertificate 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    //allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    //如果是需要验证自建证书，需要设置为YES
    securityPolicy.allowInvalidCertificates = YES;
    
    //validatesDomainName 是否需要验证域名，默认为YES；
    //假如证书的域名与你请求的域名不一致，需把该项设置为NO；如设成NO的话，即服务器使用其他可信任机构颁发的证书，也可以建立连接，这个非常危险，建议打开。
    //置为NO，主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
    //如置为NO，建议自己添加对应域名的校验逻辑。
    securityPolicy.validatesDomainName = NO;
    NSMutableSet *set = [[NSMutableSet alloc] initWithObjects:certData, nil];
    securityPolicy.pinnedCertificates = set;
    
    return securityPolicy;
}


//Get Test
+ (void)doTestGetWithReq:(NSString *)req param:(id)param result:(result)result
{
    [self configHttpSessionManager];
    [manager GET:req parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dic = (NSDictionary *)responseObject;
        if ([[dic allKeys] containsObject:@"code"]) {
            if ([dic[@"code"] isEqualToString:@"success"]) {
                if ([[dic allKeys] containsObject:@"data"]) {
                    result(YES,YES,dic[@"data"]);
                } else {
                    result(YES,YES,nil);
                }
            } else {
                if ([[dic allKeys] containsObject:@"error"]) {
                    result(YES,NO,dic[@"error"]);
                } else {
                    result(YES,NO,nil);
                }
            }
        } else {
            result(NO,NO,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        result(NO,NO,@"Network Disconnected");
    }];
}

//get
+ (void)doGetWithReq:(NSString *)req param:(id)param result:(result)result
{
    //参数加密
    [self configHttpSessionManager];
    NSString *baseUrl = [NSString stringWithFormat:@"%@:%d%@",BOSeverIP,BOSeverPort,req];
    [manager GET:baseUrl parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = (NSDictionary *)responseObject;
        if ([[dic allKeys] containsObject:@"code"]) {
            if ([dic[@"code"] isEqualToString:@"success"]) {
                if ([[dic allKeys] containsObject:@"data"]) {
                    result(YES,YES,dic[@"data"]);
                } else {
                    result(YES,YES,nil);
                }
            } else {
                if ([[dic allKeys] containsObject:@"error"]) {
                    result(YES,NO,dic[@"error"]);
                } else {
                    result(YES,NO,nil);
                }
            }
        } else {
            result(NO,NO,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        result(NO,NO,@"Network Disconnected");
    }];
}

+ (void)doGetWithWholeReq:(NSString *)req param:(id)param result:(result)result
{
    //参数加密
    [self configHttpSessionManager];
    [manager GET:req parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = (NSDictionary *)responseObject;
        if ([[dic allKeys] containsObject:@"newRet"]) {
            if ([dic[@"newRet"] isEqualToString:@"success"] || [dic[@"newRet"] isEqualToString:@"SUCCESS"] || [dic[@"newRet"] isEqualToString:@"OK"]) {
                result(YES,YES,dic);
            } else {
                if ([[dic allKeys] containsObject:@"error"]) {
                    result(YES,NO,dic[@"error"]);
                } else {
                    result(YES,NO,nil);
                }
            }
        } else {
            result(NO,NO,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        result(NO,NO,@"Network Disconnected");
    }];
}

//post
+ (void)doPostWithReq:(NSString *)req param:(id)param result:(result)result
{
    //参数加密
    [self configHttpSessionManager];
    NSMutableString *baseUrl = [NSMutableString stringWithFormat:@"%@:%d",BOSeverIP,BOSeverPort];
    if ( req && [req isKindOfClass:[NSString class]] && req.length != 0 ) {
        [baseUrl appendString:req];
    }
    [manager POST:baseUrl parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = (NSDictionary *)responseObject;
        
        if ([[dic allKeys] containsObject:@"code"]) {
            if ([dic[@"code"] isEqualToString:@"success"] ||
                [dic[@"code"] isEqualToString:@"SUCCESS"] ||
                [dic[@"code"] isEqualToString:@"OK"] ||
                [dic[@"code"] isEqualToString:@"needImgVerifiCode"]) {
                if ([[dic allKeys] containsObject:@"data"]) {
                    result(YES,YES,dic[@"data"]);
                } else if( [[dic allKeys] containsObject:@"result"] ){
                    result(YES,YES,dic[@"result"]);
                }else{
                    result(YES,YES,dic);
                }
            } else {
                if ([[dic allKeys] containsObject:@"error"]) {
                    result(YES,NO,dic[@"error"]);
                } else if( [[dic allKeys] containsObject:@"result"] ){
                    result(YES,NO,dic[@"result"]);
                } else {
                    result(YES,NO,nil);
                }
            }
        } else if ([[dic allKeys] containsObject:@"error"]) {
            result(YES,NO,dic[@"error"]);
        } else {
            result(NO,NO,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        result(NO,NO,@"Network Disconnected");
    }];
}

// 上传文件
+ (void)doUploadFileWithReq:(NSString *)req param:(id)param result:(result)result
{
    //参数加密
    [self configHttpSessionManager];
    NSMutableString *baseUrl = [NSMutableString stringWithFormat:@"%@:%d",BOSeverIP,BOSeverPort];
    if ( req && [req isKindOfClass:[NSString class]] && req.length != 0 ) {
        [baseUrl appendString:req];
    }
    [manager.requestSerializer setTimeoutInterval:IXAFUploadTimeOut];
    [manager POST:baseUrl parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = (NSDictionary *)responseObject;
        
        if ([[dic allKeys] containsObject:@"code"]) {
            if ([dic[@"code"] isEqualToString:@"success"] ||
                [dic[@"code"] isEqualToString:@"SUCCESS"] ||
                [dic[@"code"] isEqualToString:@"OK"] ||
                [dic[@"code"] isEqualToString:@"needImgVerifiCode"]) {
                if ([[dic allKeys] containsObject:@"data"]) {
                    result(YES,YES,dic[@"data"]);
                } else if( [[dic allKeys] containsObject:@"result"] ){
                    result(YES,YES,dic[@"result"]);
                }else{
                    result(YES,YES,dic);
                }
            } else {
                if ([[dic allKeys] containsObject:@"error"]) {
                    result(YES,NO,dic[@"error"]);
                } else if( [[dic allKeys] containsObject:@"result"] ){
                    result(YES,NO,dic[@"result"]);
                } else {
                    result(YES,NO,nil);
                }
            }
        } else if ([[dic allKeys] containsObject:@"error"]) {
            result(YES,NO,dic[@"error"]);
        } else {
            result(NO,NO,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        result(NO,NO,@"Network Disconnected");
    }];
}



// 方式3：
// /invokeApi/invoke
// 固定: loginName _timestamp
// 动态： _url  param
// param 可以由外部组合对象参数形成string，也可以是纯dictionary
+ (void)doReqMethod3Url:(NSString *)url pram:(id)param result:(respResult)rsp{
    url = [NSString formatterUTF8:url];
    
    NSNumber    * timeStamp = [[NSUserDefaults standardUserDefaults] objectForKey:IXServerTime];

    __block NSString    * paramStr = @"";
    // 纯字典
    if ([param isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic =  param;
        [dic.allKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *key = obj;
            id value = dic[key];
            if ([value isKindOfClass:[NSNumber class]]) {
                value = [value stringValue];
            }
            
            paramStr = [paramStr stringByAppendingString:key];
            paramStr = [paramStr stringByAppendingString:@"="];
            paramStr = [paramStr stringByAppendingString:value];
            paramStr = [paramStr stringByAppendingString:@"&"];
        }];
    }else if([param isKindOfClass:[NSString class]]){
        paramStr = param;
//        paramStr = [paramStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//        paramStr = [paramStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    if (![[paramStr substringFromIndex:paramStr.length-1] isEqualToString:@"&"]) {
        paramStr = [paramStr stringByAppendingString:@"&"];
    }
    paramStr = [paramStr stringByAppendingFormat:@"_url=%@&_timestamp=%lld",url,([timeStamp longLongValue] * 1000)];
//    paramStr = [paramStr stringByAppendingFormat:@"_url=%@&_timestamp=%lld",url,1525940974000];
    
    NSString    *paramPublicKey = [RSA encryptString:paramStr publicKey:CompanyKey];
    NSDictionary  *paramRSA = @{
                                @"loginName":CompanyLoginName,
                                @"param":paramPublicKey
                                };
    
    if ([url containsString:@"appUploadFile"]) {
        [[self class] doUploadFileWithReq:@"/invokeApi/invoke" param:paramRSA result:^(BOOL respont, BOOL success, id obj) {
            [IXAFRequest dealWithDataRespont:respont
                                     success:success
                                      result:obj
                                  respResult:rsp];
        }];
    }else{
        [[self class] doPostWithReq:@"/invokeApi/invoke" param:paramRSA result:^(BOOL respont, BOOL success, id obj) {
            [IXAFRequest dealWithDataRespont:respont
                                     success:success
                                      result:obj
                                  respResult:rsp];
        }];
    }
    
    
    
}



#pragma mark -
#pragma mark - 业务层

+ (void)getxxxxxWithParam:(NSDictionary *)dic result:(result)result
{
    [IXAFRequest doGetWithReq:@"/queryMsg/query" param:dic result:result];
}

+ (void)postxxxxWithParam:(NSDictionary *)dic result:(result)result
{
    [IXAFRequest doPostWithReq:@"/queryMsg/query" param:dic result:result];
}

+ (void)postNewsListWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/queryMsg/queryList" param:param result:result];
}

+ (void)postNewsDetailWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/queryMsg/queryOne" param:param result:result];
}

+ (void)postProtocolListWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}
+ (void)postProtocolDetailWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)getPhoneVerifyCodeWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/verificode/getVerifiCode" param:param result:result];
}

+ (void)getPhoneVerifyCodeWithLimitedWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/verificode/getVerifiCodeWithLimited" param:param result:result];
}

+ (void)getEmailVerifyCodeWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/verificode/getEmailVerifiCode" param:param result:result];
}

+ (void)checkVerifyCodeWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/verificode/checkVerifiCode" param:param result:result];
}

+ (void)registWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)bankInfoWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)uploadBankPhotoWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)uploadHeadPhotoWithParam:(id)param result:(result)result;
{
    [IXAFRequest doPostWithReq:@"/uploadCustomerFiles/save" param:param result:result];
}

+ (void)latestVersionWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)submitBankFileInfoWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)customerFilesWithParam:(id)param result:(result)result
{
   [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)drawCashWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)drawCashFeeWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)drawCashFeeRuleWithParam:(id)param result:(result)result
{
   [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)drawCashStateWithParam:(id)param result:(result)result
{
   [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)postUrlListWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)postBankListWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)getBankListWithParam:(NSString *)param result:(result)result
{
    [IXAFRequest doGetWithWholeReq:param param:nil result:result];
}

+(void)postAddBankListWithParam:(NSDictionary *)param result:(result)result {
    
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)postPayMethodWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)getPayMethodWithParam:(NSString *)param result:(result)result
{
    [IXAFRequest doGetWithWholeReq:param param:nil result:result];
}

+(void)postLoginBOWithParam:(NSDictionary *)param result:(result)result {
    
    [IXAFRequest doPostWithReq:@"/loginApi" param:param result:result];
}

+(void)postValidateIncomeWithParam:(NSDictionary *)param result:(result)result{
    
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)postTokenWithParam:(NSDictionary *)param result:(result)result{
    
    [IXAFRequest doPostWithReq:@"/sso/register" param:param result:result];
}

+ (void)postIncashModeWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

/** 获取最优入金方式 */
+ (void)postBestPayModeWithParam:(NSDictionary *)param reslt:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

/** 申请支付 */
+ (void)incash_applyPayment:(NSDictionary *)param reslt:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

/** 确认支付 */
+ (void)incash_confirmPayment:(NSDictionary *)param reslt:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

/** 重新发送验证码 */
+ (void)incash_resendVerifyCode:(NSDictionary *)param reslt:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)postIncomeListWithParam:(NSDictionary *)param result:(result)result{
    
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
    
}
+(void)postDrawCashListWithParam:(NSDictionary *)param result:(result)result {
    
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+(void)postTradeRecordListWithParam:(NSDictionary *)param result:(result)result {
    
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)postOpenAccountWithParam:(NSDictionary *)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//邮箱和手机号验证
+ (void)checkEmailOrPhoneExsitWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//获取省市列表
+ (void)checkCityAndProvinceListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//增量获取省市列表
+ (void)obtainCountryListUpdateWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//获取国家列表
+ (void)checkCountryListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//获取省列表
+ (void)checkProvinceListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//获取市列表
+ (void)checkCityListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//获取银行卡列表
+ (void)checkBankListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)uploadBankFileInfoWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//修改密码
+ (void)modifyPasswdWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//忘记密码
+ (void)findPasswdWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}


//修改用户资料
+ (void)modifyUserInfoWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}


//订阅行情
+ (void)subQuoteWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//取消订阅
+ (void)unsubQuoteWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

 //获取数据字典
+ (void)dictChildListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//解绑银行卡
+ (void)unbindBankWithParam:(id)param result:(result)result
{
   [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

#pragma mark -
#pragma mark - 埋点&消息中心

//埋点数据上传
+ (void)uploadUserStatisticsWithJsondata:(id)jsondata result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:jsondata result:result];
}

//获取未读消息条数
+ (void)getUnreadMsgCountWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//获取消息类型
+ (void)getMsgTypesWithParam:(id)param resule:(result)result
{
     [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//获取消息列表
+ (void)getMsgListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//查看消息详情
+ (void)getMsgDetailWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//全部标记为已读
+ (void)readAllMessageWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//手机号或邮箱绑定
+ (void)bindEmailOrPhoneWithParam:(id)param result:(result)result
{
  [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//用户消息/邮件语种修改
+ (void)updateMessageLangWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

/** 查询当前用户的类型 */
+ (void)acc_checkUserTypeWithParam:(id)param result:(result)result
{
   [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

#pragma mark -
#pragma mark - 签到
//当天是否已经签到
+ (void)alreadySignInWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//签到
+ (void)signInWith:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}


//设备激活
+ (void)deviceActivateStatisticWithParam:(id)param
{
     [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:^(BOOL respont, BOOL success, id obj) {
         if ([obj isKindOfClass:[NSDictionary class]]
              && [obj[@"newRet"] isEqualToString:@"OK"]) {
//                  DLog(@" -- 激活统计接口调用成功 --");
              }else{
                  ELog(@" -- 激活统计接口调用失败 --");
              }
     }];
}

#pragma mark -
#pragma mark - 用户设备

//用户设备绑定
+ (void)addUserDeviceWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//移除绑定的设备
+ (void)deleteUserDeviceWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

#pragma mark -
#pragma mark - 侧边栏模块

//常见问答查询-列表
+ (void)askedAndQuestionsListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//常见问答查询-详情
+ (void)askedAndQuestionsDetailWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//广告-列表
+ (void)bannerAdvertisementListWithParam:(id)param result:(result)result
{
   [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//版本控制
+ (void)versionControlWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//结单信息列表
+ (void)dailyStatementListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

#pragma mark -
#pragma mark - Jpush消息-详情

+ (void)jpushMessageDetailWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

#pragma mark -
#pragma mark - B2C

+ (void)companyListWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

+ (void)companyInfoWithParam:(id)param result:(result)result
{
    [IXAFRequest doPostWithReq:@"/invokeApi/invoke" param:param result:result];
}

//处理响应数据
+ (void)dealWithDataRespont:(BOOL) respont
                    success: (BOOL) success
                     result: (id) obj
                 respResult:(respResult)result
{
    if (!result) {
        return;
    }
    
    if (respont && success && [obj isKindOfClass:[NSDictionary class]]) {
        
        if ([[obj allKeys] containsObject:@"newRet"]) {
            if ([obj[@"newRet"] isKindOfClass:[NSString class]]) {
                if ([obj[@"newRet"] isEqualToString:@"OK"]) {
                    id context = obj[@"context"];
                    if ([context isKindOfClass:[NSDictionary class]]) {
                        id data = context[@"data"];
                        result(YES,nil,nil,data);
                    } else {
                       result(NO,nil,LocalizedString(@"数据异常"),obj);
                    }
                } else {
                    if ([[obj allKeys] containsObject:@"newComment"] && [obj[@"newComment"] length]) {
                        result(NO,obj[@"newRet"],obj[@"newComment"],obj);
                    } else {
                        result(NO,obj[@"newRet"],LocalizedString(@"数据异常"),obj);
                    }
                }
            } else {
                result(NO,nil,LocalizedString(@"数据异常"),obj);
            }
        }
    } else {
        if ([obj isKindOfClass:[NSDictionary class]] && obj[@"newComment"]) {
            result(NO,nil,obj[@"newComment"],obj);
        } else if ([obj isKindOfClass:[NSString class]] && [obj length]) {
            result(NO,nil,obj,obj);
        } else {
            result(NO,nil,LocalizedString(@"请求失败"),obj);
        }
    }
}

@end
