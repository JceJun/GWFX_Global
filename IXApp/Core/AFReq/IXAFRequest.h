//
//  IXAFRequest.h
//  IXApp
//
//  Created by Magee on 2016/12/26.
//  Copyright © 2016年 IX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IXAFType.h"
#import "AFNetworking.h"

/**
 * respont:网络层是否正常响应
 * success:业务层是否请求成功
 * obj    :请求正常的情况下，如果有返回内容数据，则为NSDictionary，如果是错误信息，则为NSString
 **/
typedef void(^result)(BOOL respont, BOOL success, id obj);

@interface IXAFRequest : NSObject

+ (AFSecurityPolicy *)customSecurityPolicy;

// 方式3：/invokeApi/invoke
+ (void)doReqMethod3Url:(NSString *)url pram:(id)param result:(respResult)rsp;
// 上传文件
+ (void)doUploadFileWithReq:(NSString *)req param:(id)param result:(result)result;

//for example
+ (void)getxxxxxWithParam:(NSDictionary *)param result:(result)result;

+ (void)postxxxxWithParam:(NSDictionary *)param result:(result)result;

//资讯列表和资讯详情
+ (void)postNewsListWithParam:(NSDictionary *)param result:(result)result;
+ (void)postNewsDetailWithParam:(NSDictionary *)param result:(result)result;

//客户协议和协议详情
+ (void)postProtocolListWithParam:(NSDictionary *)param result:(result)result;
+ (void)postProtocolDetailWithParam:(NSDictionary *)param result:(result)result;

#pragma mark - 注册

//获取短信验证码
+ (void)getPhoneVerifyCodeWithParam:(NSDictionary *)param result:(result)result;

//获取短信验证码防刷接口
+ (void)getPhoneVerifyCodeWithLimitedWithParam:(NSDictionary *)param result:(result)result;

//获取邮件验证码
+ (void)getEmailVerifyCodeWithParam:(NSDictionary *)param result:(result)result;

//提交验证码
+ (void)checkVerifyCodeWithParam:(NSDictionary *)param result:(result)result;

//注册
+ (void)registWithParam:(NSDictionary *)param result:(result)result;

//获取绑定银行卡信息
+ (void)bankInfoWithParam:(NSDictionary *)param result:(result)result;

//提现
+ (void)drawCashWithParam:(id)param result:(result)result;

//提现手续费
+ (void)drawCashFeeWithParam:(id)param result:(result)result;

//提现手续费规则
+ (void)drawCashFeeRuleWithParam:(id)param result:(result)result;

//用户能否取款
+ (void)drawCashStateWithParam:(id)param result:(result)result;

//上传银行卡图片
+ (void)uploadBankPhotoWithParam:(NSDictionary *)param result:(result)result;

//上传银行卡文件信息
+ (void)uploadBankFileInfoWithParam:(id)param result:(result)result;

//上传头像图片
+ (void)uploadHeadPhotoWithParam:(id)param result:(result)result;

//获取最新版本
+ (void)latestVersionWithParam:(NSDictionary *)param result:(result)result;


//提交银行卡信息
+ (void)submitBankFileInfoWithParam:(id)param result:(result)result;

//获取客户文件信息
+ (void)customerFilesWithParam:(id)param result:(result)result;



#pragma mark - 出金

//获取URL
+ (void)postUrlListWithParam:(NSDictionary *)param result:(result)result;

//获取银行列表
+ (void)postBankListWithParam:(NSDictionary *)param result:(result)result;

+ (void)getBankListWithParam:(NSString *)param result:(result)result;

//获取已添加的银行列表
+ (void)postAddBankListWithParam:(NSDictionary *)param result:(result)result;

//获取支付方式
+ (void)postPayMethodWithParam:(NSDictionary *)param result:(result)result;

+ (void)getPayMethodWithParam:(NSString *)param result:(result)result __deprecated_msg("2.4已弃用");

//验证存款是否允许
+ (void)postValidateIncomeWithParam:(NSDictionary *)param result:(result)result;

//BO登录
+ (void)postLoginBOWithParam:(NSDictionary *)param result:(result)result;

//获取入金token
+ (void)postTokenWithParam:(NSDictionary *)param result:(result)result;

/** 获取入金方式 */
+ (void)postIncashModeWithParam:(NSDictionary *)param result:(result)result;

/** 获取最优入金方式 */
+ (void)postBestPayModeWithParam:(NSDictionary *)param reslt:(result)result;

/** 申请支付 */
+ (void)incash_applyPayment:(NSDictionary *)param reslt:(result)result;

/** 确认支付 */
+ (void)incash_confirmPayment:(NSDictionary *)param reslt:(result)result;

/** 重新发送验证码 */
+ (void)incash_resendVerifyCode:(NSDictionary *)param reslt:(result)result;

#pragma mark - 纪录

//入金明细列表
+ (void)postIncomeListWithParam:(NSDictionary *)param result:(result)result;
//出金明细列表
+ (void)postDrawCashListWithParam:(NSDictionary *)param result:(result)result;
//交易记录列表
+ (void)postTradeRecordListWithParam:(NSDictionary *)param result:(result)result;
//开设账户
+ (void)postOpenAccountWithParam:(NSDictionary *)param result:(result)result;

//邮箱和手机号验证
+ (void)checkEmailOrPhoneExsitWithParam:(id)param result:(result)result;


//增量获取省市列表
+ (void)obtainCountryListUpdateWithParam:(id)param result:(result)result;

//获取国家列表
+ (void)checkCountryListWithParam:(id)param result:(result)result __deprecated_msg("2.3已弃用");
//获取省市列表
+ (void)checkCityAndProvinceListWithParam:(id)param result:(result)result __deprecated_msg("2.3已弃用");
//获取省列表
+ (void)checkProvinceListWithParam:(id)param result:(result)result __deprecated_msg("2.3已弃用");
//获取市列表
+ (void)checkCityListWithParam:(id)param result:(result)result __deprecated_msg("2.3已弃用");

//获取银行卡列表
+ (void)checkBankListWithParam:(id)param result:(result)result;

//修改密码
+ (void)modifyPasswdWithParam:(id)param result:(result)result;

//忘记密码
+ (void)findPasswdWithParam:(id)param result:(result)result;

//修改用户资料
+ (void)modifyUserInfoWithParam:(id)param result:(result)result;

//订阅行情
+ (void)subQuoteWithParam:(id)param result:(result)result;

//取消订阅
+ (void)unsubQuoteWithParam:(id)param result:(result)result;

//获取数据字典
+ (void)dictChildListWithParam:(id)param result:(result)result;

//解绑银行卡
+ (void)unbindBankWithParam:(id)param result:(result)result;


#pragma mark -
#pragma mark -  消息中心

//埋点数据上传
+ (void)uploadUserStatisticsWithJsondata:(id)jsondata result:(result)result;

//获取未读消息条数
+ (void)getUnreadMsgCountWithParam:(id)param result:(result)result;

//获取消息类型
+ (void)getMsgTypesWithParam:(id)param resule:(result)result;

//获取消息列表
+ (void)getMsgListWithParam:(id)param result:(result)result;

//查看消息详情
+ (void)getMsgDetailWithParam:(id)param result:(result)result;

//全部标记为已读
+ (void)readAllMessageWithParam:(id)param result:(result)result;

//手机号或邮箱绑定
+ (void)bindEmailOrPhoneWithParam:(id)param result:(result)result;

//用户消息/邮件语种修改
+ (void)updateMessageLangWithParam:(id)param result:(result)result;

/** 查询当前用户的类型 */
+ (void)acc_checkUserTypeWithParam:(id)param result:(result)result;


#pragma mark -
#pragma mark -  签到

//当天是否已经签到
+ (void)alreadySignInWithParam:(id)param result:(result)result;

//签到
+ (void)signInWith:(id)param result:(result)result;

//设备激活
+ (void)deviceActivateStatisticWithParam:(id)param;

#pragma mark -
#pragma mark - 用户设备

//用户设备绑定
+ (void)addUserDeviceWithParam:(id)param result:(result)result;

//移除绑定的设备
+ (void)deleteUserDeviceWithParam:(id)param result:(result)result;

#pragma mark -
#pragma mark - 侧边栏模块

//常见问答查询-列表
+ (void)askedAndQuestionsListWithParam:(id)param result:(result)result;

//常见问答查询-详情
+ (void)askedAndQuestionsDetailWithParam:(id)param result:(result)result;

//广告-列表
+ (void)bannerAdvertisementListWithParam:(id)param result:(result)result;

//版本控制
+ (void)versionControlWithParam:(id)param result:(result)result;

//结单信息列表
+ (void)dailyStatementListWithParam:(id)param result:(result)result;

#pragma mark -
#pragma mark - Jpush消息-详情

+ (void)jpushMessageDetailWithParam:(id)param result:(result)result;

#pragma mark -
#pragma mark - B2C

//获取公司列表
+ (void)companyListWithParam:(id)param result:(result)result;
//获取公司信息
+ (void)companyInfoWithParam:(id)param result:(result)result;

//处理响应数据
+ (void)dealWithDataRespont:(BOOL) respont
                    success: (BOOL) success
                     result: (id) obj
                 respResult:(respResult)result;


@end
