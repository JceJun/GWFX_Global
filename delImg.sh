#!/bin/sh

# ----------------------- 说明 -----------------------
#
#  ./cpy.sh 资源路径
#  删除./Companys/../Images文件夹中包含的资源路径中的同名文件
#  eg.  ./cpy.sh ./imgs  将./imgs目录下的文件拷贝到./Companys/../Images文件夹下
#
# ----------------------- end -----------------------


# -------------------------------------------------

#脚本开始执行
#检测参数是否正确
if [ $# -lt 1 ]
    then
    echo "请输入正确的命令 ./cpy.sh <资源路径>"
    exit
fi

file_path=$1 #资源路径
aim_path="./Companys"  #目标路径

echo "确定删除./Companys/../Images/文件夹下${file_path}所包含的文件 ？ y/n"
read confirm
confirm=`echo $confirm | tr 'A-Z' 'a-z'`

if [ $confirm != "y" ]
then
    echo "已取消操作"
    exit
fi

for path in `ls ${aim_path}`
do
    path="${aim_path}/${path}/Images/" #拼接目标路径
    if test -d $path #路径存在
    then
        echo "正在删除${path}下的文件..."

        for file in `ls ${file_path}`
        do
            echo "                                               ${file}"
            file="${path}${file}"
            rm -f ${file}
        done
        echo " "
    fi

done

echo "============== 删除完成 ^o^ =============="
echo "\n"


