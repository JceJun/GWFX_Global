    #!/bin/sh

#  ix.sh
#  IXApp
#
#  Created by Magee on 2017/5/9.
#  Copyright © 2017年 IX. All rights reserved.

#1、参数检查

if [ ! $# = 2 ]
then
echo "####Error:参数个数不对,命令格式为：./ix.sh [公司ID] [打包环境]"
exit
fi

cd ../
#工程主路径
pj_path=$(pwd)

#公司配置文件路径
cp_cpyinfo_path=${pj_path}/Config/IXCmpConfig.plist

#公司ID
cp_id=$(echo $1 | tr '[a-z]' '[A-Z]')

#打包环境
pj_env=$(echo $2 | tr '[a-z]' '[A-Z]')

pj_ver=$(/usr/libexec/PlistBuddy -c 'print :AppVersion:VersionLong' ${cp_cpyinfo_path})|| exit
pj_vers=$(/usr/libexec/PlistBuddy -c 'print :AppVersion:VersionShort' ${cp_cpyinfo_path})|| exit

if [ $cp_id = "" ]
then
echo "####Error:公司名称不能为空"
exit
fi

#公司名称必须已经注册在plist##暂未实现

if [ $pj_ver = "" ]
then
echo "####Error:版本号不能为空"
exit
fi

if [ $pj_env = "SIT" -o $pj_env = "UAT" -o $pj_env = "PRD" -o $pj_env = "TST" ]
then
echo "打包环境为$pj_env"
else
echo "####Error:未知的打包环境"
exit
fi

######################2、资源替换

#工程build路径
bd_path=${pj_path}/build

#工程名称
pj_name=$(ls | grep xcodeproj | awk -F . '{print $1}')

#工程info.plist路径
pj_infoplist_path=${pj_path}/${pj_name}/Resource/Plist/info.plist

#ipaName=$1"_V"${project_version}

if [ -d ./IXApp/Resource/Images ];then
rm -rf ./IXApp/Resource/Images
fi

if [ -d ./IXApp/Resource/Assets.xcassets ];then
rm -rf ./IXApp/Resource/Assets.xcassets
fi

cp -r ./Companys/$cp_id/Images ./IXApp/Resource/ || exit
cp -r ./Companys/$cp_id/Assets.xcassets ./IXApp/Resource/ || exit

#替换APP名称多语言
if [ -d ./IXApp/Resource/en.lproj ];then
rm -rf ./IXApp/Resource/en.lproj
fi

cp -r ./Companys/$cp_id/$pj_env/en.lproj ./IXApp/Resource/ || exit

if [ -d ./IXApp/Resource/zh-Hans.lproj ];then
rm -rf ./IXApp/Resource/zh-Hans.lproj
fi

cp -r ./Companys/$cp_id/$pj_env/zh-Hans.lproj ./IXApp/Resource/ || exit

if [ -d ./IXApp/Resource/zh-Hant.lproj ];then
rm -rf ./IXApp/Resource/zh-Hant.lproj
fi

cp -r ./Companys/$cp_id/$pj_env/zh-Hant.lproj ./IXApp/Resource/ || exit

#替换数据库
if [ -d ./IXApp/Resource/DB ];then
rm -rf ./IXApp/Resource/DB
fi

cp -r ./Companys/$cp_id/$pj_env/DB ./IXApp/Resource/ || exit



###########修改宏定义

#宏定义文件路径
pj_IXCpyConfig_path=${pj_path}/Config/IXCpyConfig.h
pj_IXCpyConfig_tmp_path=${pj_path}/Config/IXCpyConfig.hg

#与pj_env无关
CompanyPhone=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:CompanyPhone" ${cp_cpyinfo_path})|| exit
DisplayName=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:DisplayName" ${cp_cpyinfo_path})|| exit
CompanyEmail=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:CompanyEmail" ${cp_cpyinfo_path})|| exit
ServiceKey=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:ServiceKey" ${cp_cpyinfo_path})|| exit
ServicePid=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:ServicePid" ${cp_cpyinfo_path})|| exit
ThemeBgClr=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:ThemeBackgroundColor" ${cp_cpyinfo_path})|| exit
PageIndicatorTintColor=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:PageIndicatorTintColor" ${cp_cpyinfo_path})|| exit
CurrentPageIndicatorTintColor=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:CurrentPageIndicatorTintColor" ${cp_cpyinfo_path})|| exit
OpenHomeView=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:OpenHomeView" ${cp_cpyinfo_path})|| exit
GuidePageNumber=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:GuidePageNumber" ${cp_cpyinfo_path})|| exit
SymbolUnit=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:SymbolUnit" ${cp_cpyinfo_path})|| exit
SymbolCataStyle=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:SymbolCataStyle" ${cp_cpyinfo_path})|| exit
DayNightMode=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:DayNightMode" ${cp_cpyinfo_path})|| exit
LanguageMode=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:LanguageMode" ${cp_cpyinfo_path})|| exit
DefaultLanguage=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:DefaultLanguage" ${cp_cpyinfo_path})|| exit
IncomeMode=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:IncomeMode" ${cp_cpyinfo_path})|| exit
QuoteColor=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:QuoteColor" ${cp_cpyinfo_path})|| exit
NewGuideEnable=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:NewGuideEnable" ${cp_cpyinfo_path})|| exit
UpdateURL=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:UpdateURL" ${cp_cpyinfo_path})|| exit
UMAppKey=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:UMAppKey" ${cp_cpyinfo_path})|| exit




#与pj_env有关
QuoteSeverIP=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:QuoteSeverIP" ${cp_cpyinfo_path})|| exit
QuoteSeverPort=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:QuoteSeverPort" ${cp_cpyinfo_path})|| exit
TradeSeverIP=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:TradeSeverIP" ${cp_cpyinfo_path})|| exit
TradeSeverPort=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:TradeSeverPort" ${cp_cpyinfo_path})|| exit
BOSeverIP=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:BOSeverIP" ${cp_cpyinfo_path})|| exit
BOSeverPort=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:BOSeverPort" ${cp_cpyinfo_path})|| exit

SpareQServerIP=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:SpareQServerIP" ${cp_cpyinfo_path})|| exit
SpareQServerPort=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:SpareQServerPort" ${cp_cpyinfo_path})|| exit
SpareTServerIP=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:SpareTServerIP" ${cp_cpyinfo_path})|| exit
SpareTServerPort=$(/usr/libexec/PlistBuddy -c "print :SeverConfig:$pj_env:SpareTServerPort" ${cp_cpyinfo_path})|| exit

CompanyToken=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:CompanyToken" ${cp_cpyinfo_path}) || exit
CompanyID=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:CompanyID" ${cp_cpyinfo_path})|| exit
CompanyKey=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:CompanyKey" ${cp_cpyinfo_path})|| exit
VerifyCodeEnable=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:VerifyCodeEnable" ${cp_cpyinfo_path})|| exit
CompanyLoginName=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:CompanyLoginName" ${cp_cpyinfo_path})|| exit
#JPUSHAppKey=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:JPUSHAppKey" ${cp_cpyinfo_path})|| exit

defQuoteSeverIP='#define QuoteSeverIP'
defQuoteSeverPort="#define QuoteSeverPort"
defTradeSeverIP="#define TradeSeverIP"
defTradeSeverPort="#define TradeSeverPort"
defBOSeverIP="#define BOSeverIP"
defBOSeverPort="#define BOSeverPort"

defSpareQServerIP='#define SpareQServerIP'
defSpareQServerPort="#define SpareQServerPort"
defSpareTServerIP="#define SpareTServerIP"
defSpareTServerPort="#define SpareTServerPort"

defCompanyName="#define CompanyName"
defCompanyToken="#define CompanyToken"
defCompanyID="#define CompanyID"
defCompanyPhone="#define CompanyPhone"
defCompanyEmail="#define CompanyEmail"
defCompanyKey="#define CompanyKey"
defCompanyLoginName="#define CompanyLoginName"
defThemeBgClr="#define ThemeBackgroundColor"
defPageIndicatorTintColor="#define PageIndicatorTintColor"
defCurrentPageIndicatorTintColor="#define CurrentPageIndicatorTintColor"
defOpenHomeView="#define OpenHomeView"
defGuidePageNumber="#define GuidePageNumber"
defSymbolUnit="#define SymbolUnit"
defSymbolCataStyle="#define SymbolCataStyle"
defDayNightMode="#define DayNightMode"
defLanguageMode="#define LanguageMode"
defDefaultLanguage="#define DefaultLanguage"
defIncomeMode="#define IncomeMode"
defQuoteColor="#define QuoteColor"
defNewGuideEnable="#define NewGuideEnable"

defVerifyCodeEnable="#define VerifyCodeEnable"

defServicePid="#define ServicePid"
defServiceKey="#define ServiceKey"
defUpdateURL="#define UpdateURL"
defUMAppKey="#define UMAppKey"


sed -ig "/$defQuoteSeverIP/ s/^.*/$defQuoteSeverIP @\"$QuoteSeverIP\"/" $pj_IXCpyConfig_path         || exit
sed -ig "/$defQuoteSeverPort/ s/^.*/$defQuoteSeverPort $QuoteSeverPort/" $pj_IXCpyConfig_path   || exit
sed -ig "/$defTradeSeverIP/ s/^.*/$defTradeSeverIP @\"$TradeSeverIP\"/" $pj_IXCpyConfig_path         || exit
sed -ig "/$defTradeSeverPort/ s/^.*/$defTradeSeverPort $TradeSeverPort/" $pj_IXCpyConfig_path   || exit
sed -ig "/$defBOSeverIP/ s/^.*/$defBOSeverIP @\"$BOSeverIP\"/" $pj_IXCpyConfig_path                  || exit
sed -ig "/$defBOSeverPort/ s/^.*/$defBOSeverPort $BOSeverPort/" $pj_IXCpyConfig_path            || exit

sed -ig "/$defSpareQServerIP/ s/^.*/$defSpareQServerIP @\"$SpareQServerIP\"/" $pj_IXCpyConfig_path         || exit
sed -ig "/$defSpareQServerPort/ s/^.*/$defSpareQServerPort $SpareQServerPort/" $pj_IXCpyConfig_path   || exit
sed -ig "/$defSpareTServerIP/ s/^.*/$defSpareTServerIP @\"$SpareTServerIP\"/" $pj_IXCpyConfig_path         || exit
sed -ig "/$defSpareTServerPort/ s/^.*/$defSpareTServerPort $SpareTServerPort/" $pj_IXCpyConfig_path   || exit


sed -ig "/$defCompanyName/ s/^.*/$defCompanyName @\"$DisplayName\"/" $pj_IXCpyConfig_path            || exit
sed -ig "/$defCompanyToken/ s/^.*/$defCompanyToken @\"$CompanyToken\"/" $pj_IXCpyConfig_path         || exit
sed -ig "/$defCompanyID/ s/^.*/$defCompanyID $CompanyID/" $pj_IXCpyConfig_path                  || exit
sed -ig "/$defCompanyPhone/ s/^.*/$defCompanyPhone @\"$CompanyPhone\"/" $pj_IXCpyConfig_path         || exit
sed -ig "/$defCompanyEmail/ s/^.*/$defCompanyEmail @\"$CompanyEmail\"/" $pj_IXCpyConfig_path         || exit

sed -ig "/$defCompanyLoginName/ s/^.*/$defCompanyLoginName @\"$CompanyLoginName\"/" $pj_IXCpyConfig_path || exit

sed -ig "/$defVerifyCodeEnable/ s/^.*/$defVerifyCodeEnable $VerifyCodeEnable/" $pj_IXCpyConfig_path || exit

sed -ig "/$defServicePid/ s/^.*/$defServicePid @\"$ServicePid\"/" $pj_IXCpyConfig_path || exit
sed -ig "/$defServiceKey/ s/^.*/$defServiceKey @\"$ServiceKey\"/" $pj_IXCpyConfig_path || exit
sed -ig "/$defUpdateURL/ s/^.*/$defUpdateURL @\"$UpdateURL\"/" $pj_IXCpyConfig_path || exit
sed -ig "/$defUMAppKey/ s/^.*/$defUMAppKey @\"$UMAppKey\"/" $pj_IXCpyConfig_path || exit
sed -ig "/$defThemeBgClr/ s/^.*/$defThemeBgClr $ThemeBgClr/" $pj_IXCpyConfig_path || exit
sed -ig "/$defPageIndicatorTintColor/ s/^.*/$defPageIndicatorTintColor $PageIndicatorTintColor/" $pj_IXCpyConfig_path || exit
sed -ig "/$defCurrentPageIndicatorTintColor/ s/^.*/$defCurrentPageIndicatorTintColor $CurrentPageIndicatorTintColor/" $pj_IXCpyConfig_path || exit
sed -ig "/$defOpenHomeView/ s/^.*/$defOpenHomeView $OpenHomeView/" $pj_IXCpyConfig_path || exit
sed -ig "/$defGuidePageNumber/ s/^.*/$defGuidePageNumber $GuidePageNumber/" $pj_IXCpyConfig_path || exit
sed -ig "/$defSymbolUnit/ s/^.*/$defSymbolUnit $SymbolUnit/" $pj_IXCpyConfig_path || exit
sed -ig "/$defSymbolCataStyle/ s/^.*/$defSymbolCataStyle $SymbolCataStyle/" $pj_IXCpyConfig_path || exit
sed -ig "/$defDayNightMode/ s/^.*/$defDayNightMode $DayNightMode/" $pj_IXCpyConfig_path || exit
sed -ig "/$defLanguageMode/ s/^.*/$defLanguageMode $LanguageMode/" $pj_IXCpyConfig_path || exit
sed -ig "/$defDefaultLanguage/ s/^.*/$defDefaultLanguage $DefaultLanguage/" $pj_IXCpyConfig_path || exit
sed -ig "/$defIncomeMode/ s/^.*/$defIncomeMode $IncomeMode/" $pj_IXCpyConfig_path || exit
sed -ig "/$defQuoteColor/ s/^.*/$defQuoteColor $QuoteColor/" $pj_IXCpyConfig_path || exit
sed -ig "/$defNewGuideEnable/ s/^.*/$defNewGuideEnable $NewGuideEnable/" $pj_IXCpyConfig_path || exit

###########修改plist文件

/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $pj_vers" ${pj_infoplist_path}|| exit
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $pj_ver" ${pj_infoplist_path}|| exit

bundle_id=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:BundleIdentifier" ${cp_cpyinfo_path})|| exit
/usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier $bundle_id" ${pj_infoplist_path}|| exit

dis_name=$(/usr/libexec/PlistBuddy -c "print :Companys:$cp_id:$pj_env:DisplayName" ${cp_cpyinfo_path})|| exit
/usr/libexec/PlistBuddy -c "Set :CFBundleDisplayName $dis_name" ${pj_infoplist_path}|| exit

#正斜杠需要转义
sed -ig "/$defCompanyKey/ s/^.*/$defCompanyKey @\"$CompanyKey\"/" $pj_IXCpyConfig_path || exit

##特殊数据库替换
#defDataBasePrefix='#define kDataBasePrefix'
#sed -ig "/$defDataBasePrefix/ s/^.*/$defDataBasePrefix @\"${pj_ver//./}\"/" $pj_IXCpyConfig_path || exit

#删除IXCpyConfig.gh 临时文件
rm -rf $pj_IXCpyConfig_tmp_path

#工程build路径
rm -rf ${bd_path}

xcodebuild  clean || exit

exit

#编译工程
xcodebuild  -workspace IXApp.xcworkspace \
-configuration Release \
-scheme ${pj_name} \
ONLY_ACTIVE_ARCH=NO \
TARGETED_DEVICE_FAMILY=1 \
DEPLOYMENT_LOCATION=YES \
CONFIGURATION_BUILD_DIR=${pj_path}/build/Release-iphoneos || exit
#           证书选择 CODE_SIGN_IDENTITY
#           描述文件选择 PROVISIONING_PROFILE

if [ -d ./ipa-build ];then
rm -rf ipa-build
fi

#打包

cd $build_path

mkdir -p ipa-build/Payload
cp -r ./Release-iphoneos/*.app ./ipa-build/Payload/
cp -r ./Release-iphoneos/*.app.dSYM ./
zip -r ${ipaName}.app.dSYM.zip *.app.dSYM
cd ipa-build
zip -r ${ipaName}.ipa *

mkdir -p ~/Desktop/ipaPath
cd ~/Desktop/ipaPath
cp -r ${build_path}/ipa-build/${ipaName}.ipa  $(pwd)
cp -r ${build_path}/${ipaName}.app.dSYM.zip  $(pwd)
rm -rf ${build_path}

