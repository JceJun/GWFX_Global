//
//  IXCpyConfig.h
//  IXApp
//
//  Created by Magee on 2017/5/4.
//  Copyright © 2017年 IX. All rights reserved.
//  公司配置

#ifndef IXCpyConfig_h
#define IXCpyConfig_h

// 数据库名字
#define kDataBasePrefix [NSString stringWithFormat:@"%dIX",CompanyID]
#define kDataBaseSuffix @"sql"
#define kDataBaseName [NSString stringWithFormat:@"%@.%@",kDataBasePrefix,kDataBaseSuffix]
#define kPlatformPrefix @"IX"

// 公司信息
#define CompanyLoginName @"GWFXGlobalApi"
#define CompanyPhone @""
#define CompanyEmail @""
#define CompanyName @"GWFX Global"
#define CompanyToken @"GWFXGlobal"
#define CompanyID 41907
#define CompanyKey @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCm7QaFbkZejacAIHcFrDNe9kbbHF+XK6OXw3lv//yuKo3i70ajijxakPYJTXqXPpAwA9+cbv5kSgq7EgQDjRBvt67PpTW9sgADa/syvE8Q1xpqjVNydq0uCVoucrGdUlnWHTrWXmayc6kPrjo+0TDZIvqhMICuapmRfICYfv9WbQIDAQAB"


// SIT UAT PRD 环境
#define QuoteSeverIP @"quote.gwtrader.net"
#define QuoteSeverPort 10060

#define TradeSeverIP @"proxy.gwtrader.net"
#define TradeSeverPort 10000

//Edit. UAT PRD https://    DEMO  http://
#define BOSeverIP @"https://sso.gwtrader.net"
#define BOSeverPort 10860

#define SpareTServerIP @"proxy.ixtrader.net"
#define SpareTServerPort 10000

#define SpareQServerIP @"quote.ixtrader.net"
#define SpareQServerPort 10060


//Edit.上架改为Release  否则交易会出现debug信息
//Edit.plist版本号改为当前最新版本

#define UMAppKey @"5b0519bcb27b0a6bb90000e3"


// 版本升级
#define UpdateURL @"https://itunes.apple.com/cn/app/id1374123278?mt=8"

// 注册来源
#define AppMarket @"gl22" // 国际正式版:gl22  马甲包:gl44

//AppFlyer
#define AppleAppID @"1374123278"  // 国际正式版:1374123278  马甲包:1386561409
#define AppsFlyerDevKey @"QyieVUYMZ5HaroDRkNoPpS" // 国际正式版&马甲包:QyieVUYMZ5HaroDRkNoPpS


#define ThemeBackgroundColor UIColorHexFromRGB(0x4c6072)
#define PageIndicatorTintColor UIColorHexFromRGB(0xa4bcea)
#define CurrentPageIndicatorTintColor UIColorHexFromRGB(0x3c72dc)

#define OpenHomeView YES
#define GuidePageNumber 4
#define SymbolUnit 1
#define SymbolCataStyle 0
#define DayNightMode NO
#define LanguageMode NO
#define DefaultLanguage 1
#define IncomeMode YES
#define QuoteColor NO
#define NewGuideEnable NO

#define VerifyCodeEnable YES

// 客服
#define ServicePid @"IX17"
#define ServiceKey @"4ApcjjmE4nI56LnGWMc0"
#define kServiceUrl @"https://jms.phgsa.cn:8080/APP.php"

#endif /* IXCpyConfig_h */

