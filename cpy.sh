#!/bin/sh

# ----------------------- 说明 -----------------------
#
#  ./cpy.sh 资源路径
#  将资源路径下的文件，拷贝至./Companys路径下的各子路径的Images文件夹下
#  eg.  ./cpy.sh ./imgs 将./imgs目录下的文件拷贝到./Companys/../Images文件夹下
#
# ----------------------- end -----------------------


#【函数】将参数1路径下的文件拷贝到参数2路径下
copyFile(){
    if [ $# -lt 2 ]
    then
        echo "请正确输入命令./cpy.sh <资源路径>"
        exit
    fi

    f_path=$1
    a_path=$2

#    echo "正在将文件${f_path}拷贝到${a_path}中"
    cp -r $f_path $a_path   #Linux命令，将f_path的文件复制到a_path中
}


# -------------------------------------------------

#脚本开始执行
#检测参数是否正确
if [ $# -lt 1 ]
    then
    echo "请输入正确的命令 ./cpy.sh <资源路径>"
    exit
fi

file_path=$1 #资源路径
aim_path="./Companys/"  #目标路径

echo "确定将${file_path}目录下的文件拷贝至./Companys/.../Images文件夹下 ？ y/n"
read confirm
confirm=`echo $confirm | tr 'A-Z' 'a-z'`

if [ $confirm != "y" ]
then
    echo "已取消操作"
    exit
fi


for path in `ls ${aim_path}`
do
    path="${aim_path}/${path}/Images/" #拼接目标路径

    if ! test -d $path
    then
        echo "文件路径${path}不存在，是否创建？ y/n"
        read confirm
        confirm=`echo $confirm | tr 'A-Z' 'a-z'`

        if [ $confirm = "y" ]
        then
            mkdir $path
        fi
    fi

    echo "正在拷贝文件到：$path"

    for file in `ls ${file_path}`
    do
        file="${file_path}/${file}" #资源路径下的文件路径
        copyFile $file $path  #调用copyFile函数
    done

done

echo "============== 已完成 ^o^ =============="

